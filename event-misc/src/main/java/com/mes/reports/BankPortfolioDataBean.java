/*@lineinfo:filename=BankPortfolioDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/BankPortfolioDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class BankPortfolioDataBean extends ReportSQLJBean
{
  public class PortfolioData
  {
    public  Date           DateActivated   = null;
    public  Date           DateClosed      = null;
    public  Date           DateOpened      = null;
    public  String         DbaName         = null;
    public  long           DdaNumber       = 0L;
    public  long           MerchantId      = 0L;
    private String         MerchantStatus  = null;
    public  long           TransRouting    = 0L;
    
    public PortfolioData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      MerchantId        = resultSet.getLong("merchant_number");
      DbaName           = resultSet.getString("dba_name");
      DateActivated     = resultSet.getDate("date_activated");
      DateClosed        = resultSet.getDate("date_closed");
      DateOpened        = resultSet.getDate("date_opened");
      MerchantStatus    = resultSet.getString("merchant_status");
      DdaNumber         = resultSet.getLong("dda_number");
      TransRouting      = resultSet.getLong("trans_routing");
    }
    
    public String getMerchantStatus()
    {
      StringBuffer  buf = new StringBuffer("");
  
      if(!isBlank(MerchantStatus))
      {
        switch( MerchantStatus.charAt(0) )
        {
          case 'B':
            buf.append("BLOCKED");
            break;
          case 'C':
            buf.append("CLOSED");
            break;
          case 'D':
            buf.append("DELETED");
            break;
          default:
            buf.append(MerchantStatus);
            break;
        }
        buf.append(" (");
        buf.append(DateTimeFormatter.getFormattedDate(DateClosed,"MM/dd/yyyy"));
        buf.append(")");
      }
      else if( DateClosed != null )
      {
        buf.append("STATUSED (");
        buf.append(DateTimeFormatter.getFormattedDate(DateClosed,"MM/dd/yyyy"));
        buf.append(")");
      }
      else if( DateActivated != null )
      {
        buf.append("ACTIVATED (");
        buf.append(DateTimeFormatter.getFormattedDate(DateActivated,"MM/dd/yyyy"));
        buf.append(")");
      }
      else
      {
        buf.append("NOT ACTIVATED");
      }
  
      return( buf.toString() );
    }
  }
  
  public BankPortfolioDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Merchant Id\",");
    line.append("\"DBA Name\",");
    line.append("\"Date Opened\",");
    line.append("\"Status\",");
    line.append("\"DDA Number\",");
    line.append("\"Trans Routing\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    PortfolioData record = (PortfolioData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( encodeHierarchyNode(record.MerchantId) );
    line.append( ",\"" );
    line.append( record.DbaName );
    line.append( "\"," );
    line.append( DateTimeFormatter.getFormattedDate( record.DateOpened, "MM/dd/yyyy" ) );
    line.append( ",\"" );
    line.append( record.getMerchantStatus() );
    line.append( "\"," );
    line.append( record.DdaNumber );
    line.append( "," );
    line.append( record.TransRouting );
  }
  
  protected void encodeLineFixed( Object obj, StringBuffer line )
  {
    PortfolioData   record    = (PortfolioData)obj;
    
    line.setLength(0);
    line.append( encodeFixedField( 16, record.MerchantId ) );
    line.append( encodeFixedField( 32, record.DbaName ) );
    line.append( encodeFixedField(  1, record.MerchantStatus ) );
    line.append( encodeFixedField( 17, record.DdaNumber ) );
    line.append( encodeFixedField(  9, record.TransRouting ) );
    line.append( encodeFixedField( record.DateOpened,"MMddyyyy" ) );
    line.append( encodeFixedField( record.DateActivated, "MMddyyyy" ) );
    line.append( encodeFixedField( record.DateClosed, "MMddyyyy" ) );
    line.append( encodeFixedField( 24, record.getMerchantStatus() ) );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append("portfolio_report_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin, "MMddyyyy" ) );
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
      case FF_FIXED:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    int                           bankNum           = 0;
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
    
      // be sure to pull all of a banks
      // merchants if the login node is
      // a bank level node.  this prevents
      // hierarchy errors from omitting 
      // records for banks with their own
      // bank number at TSYS.
      if ( isOrgBank( orgId ) == true )
      {
        bankNum = getOrgBankId( orgId );
        
        /*@lineinfo:generated-code*//*@lineinfo:211^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                        as merchant_number,
//                    mf.dba_name                               as dba_name,
//                    to_date(decode(mf.date_opened,
//                                   '000000','010100',
//                                   mf.date_opened),'mmddyy')  as date_opened,
//                    mf.activation_date                        as date_activated,
//                    mf.date_stat_chgd_to_dcb                  as date_closed,
//                    mf.dda_num                                as dda_number,
//                    mf.transit_routng_num                     as trans_routing,
//                    mf.merchant_status                        as merchant_status
//            from    mif                      mf
//            where   mf.bank_number = :bankNum
//            order by mf.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                        as merchant_number,\n                  mf.dba_name                               as dba_name,\n                  to_date(decode(mf.date_opened,\n                                 '000000','010100',\n                                 mf.date_opened),'mmddyy')  as date_opened,\n                  mf.activation_date                        as date_activated,\n                  mf.date_stat_chgd_to_dcb                  as date_closed,\n                  mf.dda_num                                as dda_number,\n                  mf.transit_routng_num                     as trans_routing,\n                  mf.merchant_status                        as merchant_status\n          from    mif                      mf\n          where   mf.bank_number =  :1 \n          order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.BankPortfolioDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.BankPortfolioDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:230^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                        as merchant_number,
//                    mf.dba_name                               as dba_name,
//                    to_date(decode(mf.date_opened,
//                                   '000000','010100',
//                                   mf.date_opened),'mmddyy')  as date_opened,
//                    mf.activation_date                        as date_activated,
//                    mf.date_stat_chgd_to_dcb                  as date_closed,
//                    mf.dda_num                                as dda_number,
//                    mf.transit_routng_num                     as trans_routing,
//                    mf.merchant_status                        as merchant_status
//            from    group_merchant           gm,
//                    group_rep_merchant       grm,
//                    mif                      mf
//            where   gm.org_num = :orgId and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    mf.merchant_number = gm.merchant_number
//            order by mf.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                        as merchant_number,\n                  mf.dba_name                               as dba_name,\n                  to_date(decode(mf.date_opened,\n                                 '000000','010100',\n                                 mf.date_opened),'mmddyy')  as date_opened,\n                  mf.activation_date                        as date_activated,\n                  mf.date_stat_chgd_to_dcb                  as date_closed,\n                  mf.dda_num                                as dda_number,\n                  mf.transit_routng_num                     as trans_routing,\n                  mf.merchant_status                        as merchant_status\n          from    group_merchant           gm,\n                  group_rep_merchant       grm,\n                  mif                      mf\n          where   gm.org_num =  :1  and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  mf.merchant_number = gm.merchant_number\n          order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.BankPortfolioDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.BankPortfolioDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:251^9*/
      }
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.add(new PortfolioData(resultSet));
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/