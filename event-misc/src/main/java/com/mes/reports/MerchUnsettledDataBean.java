/*@lineinfo:filename=MerchUnsettledDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchUnsettledDataBean.sqlj $

  Description:  
    
  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2009-05-19 10:22:36 -0700 (Tue, 19 May 2009) $
  Version            : $Revision: 16141 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.api.ApiDb;
import com.mes.api.StoreTransactionTask;
import com.mes.api.TridentApiTransaction;
import com.mes.forms.ButtonField;
import com.mes.forms.ButtonImageField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class MerchUnsettledDataBean extends ReportSQLJBean 
{
  private Vector  ErrMessage   = new Vector();

  public class RowData 
  {    
    public    double            AuthAmount            = 0.0;
    public    String            AuthCode              = null;
    public    String            AuthFileSource        = null;
    public    String            AuthRecId             = null;
    public    String            AVSResult             = null;
    public    String            CardNumber            = null;
    public    String            CardType              = null;
    public    String            CVVResult             = null;
    public    String            CVV2Result            = null;
    public    String            DeveloperInfo         = null;
    public    String            ExpirationDate        = null;
    public    int               FileBankNumber        = 0;
    public    String            HostName              = null;
    public    long              MerchantId            = 0L;
    public    String            PosCondCode           = null;
    public    String            ResponseCode          = null;
    public    int               SicCode               = 0;
    public    String            StandInAdviceCode     = null;
    public    String            TerminalId            = null;
    public    Date              TranDate              = null;
    public    Timestamp         TranTS                = null;
    public    String            TridentTranId         = null;
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AuthAmount          = resultSet.getDouble("auth_amount");
      AuthCode            = processString(resultSet.getString("auth_code"));
      AVSResult           = processString(resultSet.getString("avs_result"));
      CardNumber          = processString(resultSet.getString("card_number"));
      CardType            = processString(resultSet.getString("card_type"));
      CVVResult           = processString(resultSet.getString("cvv_result"));
      CVV2Result          = processString(resultSet.getString("cvv2_result"));
      ExpirationDate      = processString(resultSet.getString("exp_date"));
      PosCondCode         = processString(resultSet.getString("pos_cond_code"));
      ResponseCode        = processString(resultSet.getString("response_code"));
      SicCode             = resultSet.getInt("sic_code");
      StandInAdviceCode   = processString(resultSet.getString("stand_in_code"));
      TerminalId          = processString(resultSet.getString("terminal_id"));
      TranTS              = resultSet.getTimestamp("tran_ts");      
      TridentTranId       = processString(resultSet.getString("trident_tran_id"));
      AuthRecId           = processString(resultSet.getString("rec_id"));
      MerchantId          = resultSet.getLong("merchant_number");
      TranDate            = resultSet.getDate("tran_date");
      AuthFileSource      = processString(resultSet.getString("file_source"));
      FileBankNumber      = resultSet.getInt("file_bank_number");
      DeveloperInfo       = processString(resultSet.getString("developer_info"));
      HostName            = getAuthHost(AuthFileSource,FileBankNumber,DeveloperInfo);
    } 
  }
				  
  public MerchUnsettledDataBean()
  {
    super(true);
  }

  public class TridentProfileValidation implements Validation
  {
    private String  ErrorMessage    = null;
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate(String fdata)
    {
      if( !isTridentProfileId(fdata) )
      {
        ErrorMessage = "Invalid profile id";        
      }

      return( ErrorMessage == null );
    }
  }  
  
  TridentApiTransaction ApiTran = null;
  
  protected boolean autoSubmit()
  {
    String     fname           = getAutoSubmitName();
    boolean    retVal          = false;
    long       authRecId       = 0L;
    double     tranAmount      = 0.0;
    String     amt             = null;
    String     rec             = null;
    String     profileId       = getData("profileId");
    String     nodeId          = ""+getReportHierarchyNodeDefault();

    ApiTran = new TridentApiTransaction();

    if (fname.equals("btnSubmit"))
    {
      if (!getField("authRecIds").isBlank() && !getField("tranAmounts").isBlank() )
      {
        StringTokenizer recIds        = new StringTokenizer (getData("authRecIds"), "-");
        StringTokenizer tranAmounts   = new StringTokenizer (getData("tranAmounts"), "-");
        
        while (recIds.hasMoreTokens())
        {
          rec         = (String)(recIds.nextToken());
          amt         = (String)(tranAmounts.nextToken());
          authRecId   = Long.parseLong( rec );
          tranAmount  = Double.parseDouble(amt);
          
          loadTransaction( authRecId );
          ApiTran.setProfile( ApiDb.loadProfile(profileId,nodeId) );
          ApiTran.setTranAmount( tranAmount );
          
          StoreTransactionTask task = new StoreTransactionTask( ApiTran );
          retVal = task.doTask();
          
          if ( !retVal )
          {
            ErrMessage.add( task.getErrorCode() + " - " + task.getErrorDesc() );
          }
        }
        ApiDb.closeCAB(profileId);   // close the batch
      }
    }
    return (retVal);
  }
    
  protected void createFields(HttpServletRequest request)
  {
    Field       field;
    
    super.createFields(request);
    
    fields.add( new HiddenField("authRecIds"));
    fields.add( new HiddenField("tranAmounts"));
    fields.add( new Field("profileId", "Profile ID (required)", 25, 0, false ) );
    fields.getField("profileId").addValidation( new TridentProfileValidation() );
    fields.add(new ButtonImageField ("searchButton", "Search", "/images/search.gif", fields));
    fields.add( new ButtonField("btnSubmit" , "Create Batch"));

    fields.setHtmlExtra("class=\"formFields\"");
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Terminal Id\",");
    line.append("\"Tran Date\",");
    line.append("\"Tran Time\",");
    line.append("\"POS Cond.\",");
    line.append("\"Card Number\",");
    line.append("\"MCC\",");
    line.append("\"Card Type\",");
    line.append("\"Tran Amount\",");
    line.append("\"Exp Date\",");
    line.append("\"Response Code\",");
    line.append("\"Auth Code\",");
    line.append("\"Host\",");
    line.append("\"SRC\",");
    line.append("\"AVS\",");
    line.append("\"CVV/CVV2\"");
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    
    if( fileFormat == FF_CSV )
    {
      retVal = true;
    }
    return(retVal);
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData   record    = (RowData)obj;
    line.setLength(0);
    line.append("'");
    line.append( record.TerminalId );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.TranTS,"MM/dd/yyyy") );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.TranTS,"HH:mm:ss") );
    line.append( "," );
    line.append( record.PosCondCode );
    line.append( ",\"" );
    line.append( record.CardNumber );
    line.append( "\"," );
    line.append( record.SicCode );
    line.append( ",\"" );
    line.append( record.CardType );
    line.append( "\"," );
    line.append( record.AuthAmount );
    line.append( ",\"" );
    line.append( record.ExpirationDate );
    line.append( "\",\"" );
    line.append( record.ResponseCode );
    line.append( "\",\"" );
    line.append( record.AuthCode );
    line.append( "\",\"" );
    line.append( record.HostName );
    line.append( "\",\"" );
    line.append( record.StandInAdviceCode );
    line.append( "\",\"" );
    line.append( record.AVSResult );
    line.append( "\",\"" );
    line.append( record.CVVResult );
    line.append("/");
    line.append( record.CVV2Result );
    line.append( "\"" );    
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl(buffer,getReportHierarchyNodeDefault(),beginDate,endDate);
    buffer.append("&profileId=");
    buffer.append(getData("profileId"));
  }
  

  
  public String getAuthHost( String authFileSource, int fileBankNumber, String developerInfo )
  {
    AuthActivityDataBean auth = new AuthActivityDataBean();
    return( auth.getAuthHost(authFileSource, fileBankNumber,developerInfo));
  }

  
  public String getDownloadFilenameBase()
  {
    Date                    beginDate  = null;
    Date                    endDate    = null;
    StringBuffer            filename   = new StringBuffer("");
    
    filename.append("merch_unsettled_tran_");
    beginDate = getReportDateBegin();
    endDate   = getReportDateEnd();
    
    filename.append( DateTimeFormatter.getFormattedDate(beginDate,"MMddyy") );
    if ( beginDate != endDate )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(endDate,"MMddyy") );
    }
    
    return ( filename.toString() );
  }
  
  protected String getPosDataCode( ResultSet rs )
    throws java.sql.SQLException
  {
    String    cardType           = ApiTran.getCardType();
    String    fieldValue         = null;
    String    motoEcommInd       = processString( rs.getString("moto_ecommerce_ind") );
    String    posEntryMode       = processString( rs.getString("pos_entry_mode") );
    String    transactionCode    = processString( rs.getString("transaction_code") );
    boolean   isCardSwiped       = ( posEntryMode.equals("02") || posEntryMode.equals("06") || posEntryMode.equals("90") );
    boolean   isMotoTransaction  = transactionCode.equals("56");
    boolean   isCardPresent      = ( isCardSwiped || !isMotoTransaction );
    StringBuffer posDataCode     = new StringBuffer();

    posDataCode.setLength(0);

    for (int i=1; i<=12; i++)
    {
      switch (i)
      {
        case 1:
          fieldValue = "6";             // Key entry only capability   
          if( isCardSwiped || (cardType.equals("MC") && !isMotoTransaction) )
          {
            fieldValue = "2";           // Magnetic stripe readter capability
          }
          break;
          
        case 2:
          fieldValue = "9";             // Unknown
          if( cardType.equals("MC") && !isCardSwiped && !isMotoTransaction )
          {
            fieldValue = "1";           // PIN entry capability
          }
          break;
          
        case 3:
          fieldValue = "1";             // Card capture capability
          if( cardType.equals("MC") && (isCardSwiped || isMotoTransaction) )
          {
            fieldValue = "9";          // Unknown
          }
          break;
          
        case 4:
          fieldValue = "9";             // Unknown
          if( cardType.equals("MC") && !isCardSwiped && !isMotoTransaction )
          {
            fieldValue = "1";           // On card acceptor premises; attended terminal
          }
          break;
          
        case 5:
          
          String ecommValue = "5678";          
          fieldValue = "0";             // Cardholder present
          if( isMotoTransaction )
          {
            if ( !"".equals(motoEcommInd) && ecommValue.indexOf(motoEcommInd) >= 0 )
            {
              fieldValue = "5";         // Cardholder not present; electronic commerce        
            }
            else
            {
              fieldValue = "1";         // Cardholder not present; unspecified reason
            }
          }
          else 
          { 
            if( cardType.equals("AM") && !isCardSwiped )
            {
              fieldValue = "9";           // Unknown
            }
          }
          break;
          
        case 6:
          fieldValue = "1";             // Card present
          if( isMotoTransaction )
          {
            fieldValue = "0";           // Card not present
          }
          break;
          
        case 7:
          fieldValue = "6";             // Key entered input
          if( isCardSwiped )
          {
            fieldValue = "2";           // Magnetic stripe reader input
          }
          break;
          
        case 8:
          fieldValue = "9";             // Unknown
          if( cardType.equals("MC") && !isMotoTransaction )
          {
            fieldValue = "5";           // anual signature verification
          }
          break;
          
        case 9:
          fieldValue = "9";             // Unkown
          if( cardType.equals("MC") && !isMotoTransaction )
          {
            fieldValue = "4";           // Merchant/card acceptor signature
          }
          break;
          
        case 10:
          fieldValue = "1";             // None
          if( cardType.equals("MC") && ( isCardSwiped || isMotoTransaction) )
          {
            fieldValue = "0";           // Unknown
          }
          break;

        case 11:
          fieldValue = "4";             // Pringting and display capability
          if( cardType.equals("MC") && !isCardSwiped && !isMotoTransaction )
          {
            fieldValue = "2";           // Pringting capability only
          }
          break;
        case 12:
          fieldValue = "1";             // Unknown
          if( cardType.equals("MC") && !isCardSwiped && !isMotoTransaction )
          {
            fieldValue = "4";           // PIN capture capability
          }
          break;
      }
      posDataCode.append(fieldValue);
    }
    
    return( posDataCode.toString() );
  }
  
  public Vector getSubmitErrors()
  {
    return (ErrMessage);
  }
    
  public void loadData()
  {
    ResultSetIterator    it               = null;
    ResultSet            resultSet        = null;
    long                 nodeId           = getReportHierarchyNode();
    Date                 beginDate        = getReportDateBegin();
    Date                 endDate          = getReportDateEnd();
    String               profileId        = getData("profileId");
    
    try
    {
      ReportRows.clear();
      /*@lineinfo:generated-code*//*@lineinfo:454^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    auth.rec_id                                   as rec_id,
//                    auth.merchant_number                          as merchant_number,
//                    auth.terminal_id                              as terminal_id,
//                    auth.transaction_date                         as tran_date,
//                    auth.transaction_time                         as tran_ts,
//                    auth.card_number                              as card_number,
//                    auth.card_number_enc                          as card_number_enc,
//                    auth.card_type                                as card_type,
//                    auth.authorization_code                       as auth_code,
//                    auth.authorized_amount                        as auth_amount,
//                    nvl(auth.secondary_amount,-1)                 as secondary_amount,
//                    nvl( auth.expiration_date_incoming,
//                    to_char(auth.expiration_date,'MMyy'))         as exp_date,
//                    auth.merchant_category_code                   as sic_code,
//                    auth.avs_result                               as avs_result,
//                    auth.cvv_result                               as cvv_result,
//                    auth.cvv2_request_data                        as cvv2_request,
//                    auth.cvv2_result                              as cvv2_result,
//                    auth.communication_line_type                  as line_type,
//                    auth.response_code                            as response_code,
//                    auth.payment_service_transaction_id           as tran_id,
//                    auth.validation_code                          as val_code,
//                    auth.aquirer_station_id                       as station_id,
//                    auth.pos_condition_code                       as pos_cond_code,
//                    auth.auth_characteristics_ind                 as auth_char_ind,
//                    auth.terminal_format_code                     as term_fmt_code,
//                    auth.stnd_in_processing_advice_code           as stand_in_code,
//                    'tauth'                                       as file_source,       
//                    '0'                                           as file_bank_number,
//                    auth.trident_transaction_id                   as trident_tran_id,
//                    auth.product_level_result_code                as auth_product_code,
//                    lower( substr(nvl(tauth.developer_info,'unknown'),
//                           1,6) )                                 as developer_info
//            from    organization         o,
//                    group_merchant       gm,
//                    trident_profile      tp,
//                    tc33_authorization   auth,
//                    trident_auth         tauth
//            where   o.org_group = :nodeId
//                    and gm.org_num = o.org_num
//                    and tp.merchant_number = gm.merchant_number
//                    and tp.terminal_id = :profileId
//                    and auth.transaction_date between :beginDate and :endDate
//                    and auth.merchant_number = tp.merchant_number
//                    and auth.terminal_id = tp.terminal_id
//                    and auth.response_code = '00'
//                    and auth.transaction_code in ('54','56')
//                    and substr(auth.load_filename,1,5) = 'tauth'
//                    and nvl(auth.auth_reversed, '0') != 1
//                    and tauth.trident_transaction_id = auth.trident_transaction_id
//                    and not exists
//                    (
//                      select   td.trident_transaction_id
//                      from     trident_detail_lookup  td
//                      where    td.trident_transaction_id = auth.trident_transaction_id
//                    )
//                    and not exists
//                    (
//                      select   tc.trident_tran_id
//                      from     trident_capture_api  tc
//                      where    tc.trident_tran_id = auth.trident_transaction_id
//                    )
//            order by auth.transaction_date, auth.transaction_time
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    auth.rec_id                                   as rec_id,\n                  auth.merchant_number                          as merchant_number,\n                  auth.terminal_id                              as terminal_id,\n                  auth.transaction_date                         as tran_date,\n                  auth.transaction_time                         as tran_ts,\n                  auth.card_number                              as card_number,\n                  auth.card_number_enc                          as card_number_enc,\n                  auth.card_type                                as card_type,\n                  auth.authorization_code                       as auth_code,\n                  auth.authorized_amount                        as auth_amount,\n                  nvl(auth.secondary_amount,-1)                 as secondary_amount,\n                  nvl( auth.expiration_date_incoming,\n                  to_char(auth.expiration_date,'MMyy'))         as exp_date,\n                  auth.merchant_category_code                   as sic_code,\n                  auth.avs_result                               as avs_result,\n                  auth.cvv_result                               as cvv_result,\n                  auth.cvv2_request_data                        as cvv2_request,\n                  auth.cvv2_result                              as cvv2_result,\n                  auth.communication_line_type                  as line_type,\n                  auth.response_code                            as response_code,\n                  auth.payment_service_transaction_id           as tran_id,\n                  auth.validation_code                          as val_code,\n                  auth.aquirer_station_id                       as station_id,\n                  auth.pos_condition_code                       as pos_cond_code,\n                  auth.auth_characteristics_ind                 as auth_char_ind,\n                  auth.terminal_format_code                     as term_fmt_code,\n                  auth.stnd_in_processing_advice_code           as stand_in_code,\n                  'tauth'                                       as file_source,       \n                  '0'                                           as file_bank_number,\n                  auth.trident_transaction_id                   as trident_tran_id,\n                  auth.product_level_result_code                as auth_product_code,\n                  lower( substr(nvl(tauth.developer_info,'unknown'),\n                         1,6) )                                 as developer_info\n          from    organization         o,\n                  group_merchant       gm,\n                  trident_profile      tp,\n                  tc33_authorization   auth,\n                  trident_auth         tauth\n          where   o.org_group =  :1 \n                  and gm.org_num = o.org_num\n                  and tp.merchant_number = gm.merchant_number\n                  and tp.terminal_id =  :2 \n                  and auth.transaction_date between  :3  and  :4 \n                  and auth.merchant_number = tp.merchant_number\n                  and auth.terminal_id = tp.terminal_id\n                  and auth.response_code = '00'\n                  and auth.transaction_code in ('54','56')\n                  and substr(auth.load_filename,1,5) = 'tauth'\n                  and nvl(auth.auth_reversed, '0') != 1\n                  and tauth.trident_transaction_id = auth.trident_transaction_id\n                  and not exists\n                  (\n                    select   td.trident_transaction_id\n                    from     trident_detail_lookup  td\n                    where    td.trident_transaction_id = auth.trident_transaction_id\n                  )\n                  and not exists\n                  (\n                    select   tc.trident_tran_id\n                    from     trident_capture_api  tc\n                    where    tc.trident_tran_id = auth.trident_transaction_id\n                  )\n          order by auth.transaction_date, auth.transaction_time";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchUnsettledDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setString(2,profileId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchUnsettledDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:519^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData()",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadTransaction( long recId )
  {
    ResultSetIterator       it            = null;
    ResultSet               resultSet     = null;
        
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:548^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  auth.trident_transaction_id          as trident_tran_id,
//                  auth.auth_characteristics_ind        as returned_aci,
//                  auth.terminal_id                     as terminal_id,
//                  auth.transaction_date                as tran_date,
//                  auth.authorized_amount               as auth_amount,
//                  auth.authorization_code              as auth_code,
//                  auth.merchant_number                 as merchant_number,
//                  auth.avs_zip                         as avs_zip,
//                  nvl(auth.currency_code,'840')        as currency_code,
//                  auth.card_number                     as card_number,
//                  auth.expiration_date                 as expiration_date,
//                  cntr.country_code                    as country_code,
//                  auth.response_code                   as auth_response_code,
//                  auth.stnd_in_processing_advice_code  as auth_source_code,
//                  auth.avs_result                      as auth_avs_response,
//                  auth.retrieval_refrence_number       as auth_ref_num,
//                  auth.payment_service_transaction_id  as auth_tran_id,
//                  auth.validation_code                 as auth_val_code,
//                  auth.transaction_code                as transaction_code,
//                  auth.pos_condition_code              as pos_condition_code,
//                  null                                 as reference_number,
//                  null                                 as purchase_id,
//                  null                                 as external_tran_id,
//                  'D'                                  as debit_credit_ind,
//                  'D'                                  as tran_type,
//                  decode( auth.stnd_in_processing_advice_code,
//                          '5','N',
//                          '7',decode(substr(auth.authorization_code,0,1),'T','Y','N') )
//                                                       as test_flag,
//                  nvl(auth.moto_ecommerce_indicator,
//                      decode(auth.transaction_code,'56','1',null))  as moto_ecommerce_ind,
//                  dukpt_decrypt_wrapper(auth.card_number_enc)       as card_number_full,
//                  to_char(lpad( substr(auth.pos_entry_mode_code,1,length(auth.pos_entry_mode_code)-1 ), 2, '0' ))   
//                                                       as pos_entry_mode
//          from    tc33_authorization        auth,
//                  trident_profile           tp,
//                  country                   cntr
//          where   auth.rec_id =:recId and
//                  tp.terminal_id = auth.terminal_id and
//                  cntr.iso_country_code = auth.country_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  auth.trident_transaction_id          as trident_tran_id,\n                auth.auth_characteristics_ind        as returned_aci,\n                auth.terminal_id                     as terminal_id,\n                auth.transaction_date                as tran_date,\n                auth.authorized_amount               as auth_amount,\n                auth.authorization_code              as auth_code,\n                auth.merchant_number                 as merchant_number,\n                auth.avs_zip                         as avs_zip,\n                nvl(auth.currency_code,'840')        as currency_code,\n                auth.card_number                     as card_number,\n                auth.expiration_date                 as expiration_date,\n                cntr.country_code                    as country_code,\n                auth.response_code                   as auth_response_code,\n                auth.stnd_in_processing_advice_code  as auth_source_code,\n                auth.avs_result                      as auth_avs_response,\n                auth.retrieval_refrence_number       as auth_ref_num,\n                auth.payment_service_transaction_id  as auth_tran_id,\n                auth.validation_code                 as auth_val_code,\n                auth.transaction_code                as transaction_code,\n                auth.pos_condition_code              as pos_condition_code,\n                null                                 as reference_number,\n                null                                 as purchase_id,\n                null                                 as external_tran_id,\n                'D'                                  as debit_credit_ind,\n                'D'                                  as tran_type,\n                decode( auth.stnd_in_processing_advice_code,\n                        '5','N',\n                        '7',decode(substr(auth.authorization_code,0,1),'T','Y','N') )\n                                                     as test_flag,\n                nvl(auth.moto_ecommerce_indicator,\n                    decode(auth.transaction_code,'56','1',null))  as moto_ecommerce_ind,\n                dukpt_decrypt_wrapper(auth.card_number_enc)       as card_number_full,\n                to_char(lpad( substr(auth.pos_entry_mode_code,1,length(auth.pos_entry_mode_code)-1 ), 2, '0' ))   \n                                                     as pos_entry_mode\n        from    tc33_authorization        auth,\n                trident_profile           tp,\n                country                   cntr\n        where   auth.rec_id = :1  and\n                tp.terminal_id = auth.terminal_id and\n                cntr.iso_country_code = auth.country_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchUnsettledDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchUnsettledDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:590^7*/
      resultSet = it.getResultSet();
      while( resultSet.next() )
      {
        setApiTranFields( resultSet );        
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("loadTransaction()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {} 
      cleanUp();
    }
  }

  protected void setApiTranFields( ResultSet rs )
    throws java.sql.SQLException
  {
    String    motoEcommInd       = processString( rs.getString("moto_ecommerce_ind") );
    String    posEntryMode       = processString( rs.getString("pos_entry_mode") );
    String    transactionCode    = processString( rs.getString("transaction_code") );
    boolean   isCardSwiped       = ( posEntryMode.equals("02") || posEntryMode.equals("06") || posEntryMode.equals("90") );
    boolean   isMotoTransaction  = transactionCode.equals("56");
    boolean   isCardPresent      = ( isCardSwiped || !isMotoTransaction );
    String    cardType           = null;
    
    ApiTran.setTridentTranId( processString(rs.getString("trident_tran_id")) );
    ApiTran.setReturnedAci( processString(rs.getString("returned_aci")) );
    ApiTran.setProfileId( processString(rs.getString("terminal_id")) );
    ApiTran.setDebitCreditInd( processString(rs.getString("debit_credit_ind")) );
    ApiTran.setTranDate( rs.getDate("tran_date") );
    ApiTran.setAuthAmount( rs.getDouble("auth_amount") );
    ApiTran.setAuthCode( processString(rs.getString("auth_code")) );
    ApiTran.setTransactionType( processString(rs.getString("tran_type")) );
    ApiTran.setPosEntryMode( processString(rs.getString("pos_entry_mode")) );
    ApiTran.setAvsZip( processString(rs.getString("avs_zip")) );
    ApiTran.setCurrencyCode( processString(rs.getString("currency_code")) );
    ApiTran.setCardNumber( processString(rs.getString("card_number")) );
    ApiTran.setTestFlag(processString(rs.getString("test_flag")) );
    ApiTran.setBatchRequestFlag( true );
    ApiTran.setCardNumberFull( processString(rs.getString("card_number_full")) );
    ApiTran.setCountryCode( processString(rs.getString("country_code")) );
    ApiTran.setExpDate( processString(rs.getString("expiration_date")) );
    ApiTran.setAuthResponseCode( processString(rs.getString("auth_response_code")) );
    ApiTran.setAuthSourceCode( processString(rs.getString("auth_source_code")) );
    ApiTran.setAuthAvsResponse ( processString(rs.getString("auth_avs_response")) );
    ApiTran.setAuthRefNum ( processString(rs.getString("auth_ref_num")) );
    ApiTran.setAuthTranId ( processString(rs.getString("auth_tran_id")) );
    ApiTran.setAuthValCode( processString(rs.getString("auth_val_code")) );
    
    cardType = ApiTran.getCardType();
        
    if( isCardPresent )
    {
      ApiTran.setCardPresent("1");
      ApiTran.setMotoEcommIndicatorToSpace();
    }
    else
    {
      ApiTran.setMotoEcommIndicator( processString(rs.getString("moto_ecommerce_ind")) );
    }

    if( isMotoTransaction && (cardType.equals("MC") || cardType.equals("VS")) )
    {
      String purchaseId = "123456";
      String tempString = processString( rs.getString("auth_ref_num") );
      
      if( tempString != null && tempString.length() > 6 )
      {
        purchaseId = tempString.substring(tempString.length() - 6, tempString.length());
      }
      ApiTran.setPurchaseId( purchaseId );
    }
    
    if( cardType.equals("MC") )
    {
      ApiTran.setPosDataCode( getPosDataCode(rs) );
    }
  }
}/*@lineinfo:generated-code*/