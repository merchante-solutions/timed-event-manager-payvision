/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/mesUserProfile.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 1/12/01 11:48a $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.text.SimpleDateFormat;
import java.util.Date;
import com.mes.constants.mesConstants;

//C+           MesUserProfile
public class   MesUserProfile
{
  // instance variables
  String                  UserEmail               = "";
  long                    UserId                  = 0;
  String                  UserLoginId             = "";
  String                  UserName                = "";
  long                    UserOrgId               = 0L;
  String                  UserPassword            = "";
  String                  UserPasswordConfirm     = "";
  long                    UserRights              = 0L;
  int                     UserType                = mesConstants.USER_INVALID;
  boolean                 Valid                   = false;
  
    // Sales Rep Specific Data - only valid if UserType == mesConstants.USER_SALES
  public String                  SalesRepId          = "";
  public String                  SalesRepName        = "";
  public String                  SalesRepTitle       = "";
  public int                     SalesRepType        = mesConstants.SRT_INVALID;
  public int                     SalesRepRegion      = mesConstants.SRGN_INVALID;
  public String                  SalesRepPhone       = "";
  public String                  SalesRepFax         = ""; 
  public String                  SalesRepMobile      = "";
  public String                  SalesRepPager       = "";
  public Date                    SalesRepHireDate    = null;
  public String                  SalesRepAddr1       = "";
  public String                  SalesRepAddr2       = "";
  public String                  SalesRepCity        = "";
  public String                  SalesRepState       = "";
  public int                     SalesRepZip         = 0;
  public int                     SalesRepSSN         = 0;
  public int                     SalesRepPlanId;
  public String                  SalesRepReportTo    = "";
  
  public MesUserProfile( )
  {
  }
  
  public MesUserProfile( String userLoginId )
  {
    UserLoginId = userLoginId;
  }
  
  public boolean areUserRightsValid( )
  {
    boolean       retVal = true;
    
    // no rights
    if ( UserRights == mesConstants.FUNC_NONE )
    {
      retVal = false;
    }
    else if ( ( UserRights & mesConstants.FUNC_REPORT_MERCH ) != 0 &&
              ( UserRights & mesConstants.FUNC_REPORT_ADMIN ) != 0 )
    {
      retVal = false;
    }
    else if ( ( UserType != mesConstants.USER_MERCHANT ) && 
              ( UserRights & mesConstants.FUNC_REPORT_MERCH ) != 0 )
    {
      retVal = false;
    }
    else if ( UserType == mesConstants.USER_MERCHANT ) 
    {
      if ( ( UserRights & mesConstants.FUNC_REPORT_MERCH ) == 0  ||
           ( UserRights & mesConstants.FUNC_PROFILE_CHANGE ) == 0 )
      {
        retVal = false;
      }
      if ( ( UserRights & mesConstants.FUNC_REPORT_ADMIN ) != 0 )
      {
        retVal = false;
      }
    }
    return( retVal );
  }
  
  public boolean doPasswordsMatch( )
  {
    return( UserPassword.equals( UserPasswordConfirm ) );
  }
  
  protected Date extractDate( String paramString, String dateFormat, Date defaultValue )
  {
    Date                retVal        = defaultValue;
    SimpleDateFormat    simpleDate    = new SimpleDateFormat( dateFormat );
    
    try
    {
      retVal = simpleDate.parse( paramString );
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  protected int extractInteger( String paramString, int defaultValue )
  {
    int       retVal = defaultValue;
    
    try
    {
      retVal = Integer.parseInt( paramString );
    }
    catch( NumberFormatException e )
    {
    }
    return( retVal );
  }
  
  protected long extractLong( String paramString, long defaultValue )
  {
    long      retVal = defaultValue;
    
    try
    {
      retVal = Long.parseLong( paramString );
    }
    catch( NumberFormatException e )
    {
    }
    return( retVal );
  }
  
  protected String extractString( String paramString, String defaultValue )
  {
    String      retVal = defaultValue;
    
    try
    {
      if ( ! paramString.equals("null" ) )
      {
        retVal = paramString;
      }
    }
    catch( NullPointerException e )
    {
    }      
    return( retVal );
  }
  
  public String getSalesRepSSNFormatted( )
  {
    String              retVal      = "";
    
    try
    {
      if(SalesRepSSN > 0)
      {
        retVal = Integer.toString(SalesRepSSN);
      }
    }
    catch(Exception e)
    {
    }
    
    return retVal;
/*    
    int[]               factors     = { 1000000, 10000, 1 };
    int[]               lengths     = { 3, 2, 4 };
    StringBuffer        numVal      = new StringBuffer("");
    StringBuffer        retVal      = new StringBuffer("");
    int                 ssn         = SalesRepSSN;
    int                 tempValue;
    
    if ( ssn != 0 )
    {
      for ( int i = 0; i < 3; ++i )
      {
        if ( i > 0 )
        {
          retVal.append( "-" );
        }          
        
        // extract the next number and add
        // leading zeros if necessary
        tempValue = ( ssn / factors[i] );
        numVal.setLength(0);
        numVal.append( tempValue );
        while( numVal.length() < lengths[i] )
        {
          numVal.insert(0,0);
        }
        
        // add the new number to the string
        retVal.append( numVal );
        
        // setup to do the next set of digits
        ssn -= ( tempValue * factors[i] );
      } 
    }
    return( retVal.toString() );
*/
  }
    
  public String getUserEmail( )
  {
    return( UserEmail );
  }
  
  public long getUserId( )
  {
    return( UserId );
  }
  
  public String getUserLoginId( )
  {
    return( UserLoginId );
  }
  
  public String getUserName( )
  {
    return( UserName );
  }
  
  public long getUserOrgId( )
  {
    return( UserOrgId );
  }
  
  public String getUserPassword( )
  {
    return( UserPassword );
  }
  
  public String getUserPasswordConfirm( )
  {
    return( UserPasswordConfirm );
  }
  
  public long getUserRights( )
  {
    return( UserRights );
  }
  
  public int getUserType( )
  {
    return( UserType );
  }
  
  public boolean isAddendumDataComplete( )
  {
    boolean             retVal = true;
    
    switch( UserType )
    {
      case mesConstants.USER_BANK:
        break;
      case mesConstants.USER_MES:
        break;
      case mesConstants.USER_MERCHANT:
        break;
      case mesConstants.USER_SALES:
        if ( SalesRepName.equals("") )
        {
          SalesRepName = UserName;
        }
        if (  SalesRepId.equals("")     ||
              SalesRepName.equals("")   ||
              SalesRepTitle.equals("")  ||
              SalesRepAddr1.equals("")  ||
              SalesRepCity.equals("")   ||
              SalesRepPhone.equals("")  ||
              SalesRepState.equals("")  ||
              SalesRepType == mesConstants.SRT_INVALID ||
              SalesRepRegion == mesConstants.SRGN_INVALID ||
              SalesRepZip == 0          ||
//              SalesRepSSN == 0          || 
              SalesRepHireDate == null  ||
              SalesRepPlanId == mesConstants.SPID_INVALID ||
              SalesRepReportTo.equals("") )
        {
          retVal = false;
        }
        break;
    }
    
    return( retVal );
  }
  
  public boolean isBaseDataComplete( )
  {
    boolean       retVal = true;
    
    if (  UserPasswordConfirm == null || UserPasswordConfirm.equals("") ||
          UserPassword == null        || UserPassword.equals("") ||
          UserName == null            || UserName.equals("") ||
          UserLoginId == null         || UserLoginId.equals("") )
    {
      retVal = false;
    }
    return( retVal );
  }
  
  public boolean isValid( )
  {
    return( isBaseDataComplete() && isAddendumDataComplete() && doPasswordsMatch() && areUserRightsValid() );
  }
  
  // Sales Specific Section *** Automatically extracted from request
  public void setSalesRepId( String repId )
  {
    SalesRepId = extractString( repId, "" );
  }
  public void setSalesRepName( String repName )
  {
    SalesRepName = extractString( repName, "" );
  }
  public void setSalesRepTitle( String repTitle )
  {
    SalesRepTitle = extractString( repTitle, "" );
  }
  public void setSalesRepType( String repTypeString )
  {
    SalesRepType = extractInteger( repTypeString, mesConstants.SRT_DIRECT );
  }
  public void setSalesRepRegion( String repRegionString )
  {
    SalesRepRegion = extractInteger( repRegionString, mesConstants.SRGN_INVALID );
  }
  public void setSalesRepPhone( String paramString )
  {
    SalesRepPhone = extractString( paramString, "" );
  }
  public void setSalesRepFax( String paramString )
  {
    SalesRepFax = extractString( paramString, "" );
  }
  public void setSalesRepMobile( String paramString )
  {
    SalesRepMobile = extractString( paramString, "" );
  }
  public void setSalesRepPager( String paramString )
  {
    SalesRepPager = extractString( paramString, "" );
  }
  public void setSalesRepHireDate( String paramObject )
  {
    SalesRepHireDate = extractDate( (String) paramObject, "MM/dd/yyyy", null );
  }
  public void setSalesRepSqlHireDate( java.sql.Date hireDate )
  {
    SalesRepHireDate = hireDate;
  }
  public void setSalesRepAddr1( String paramString )
  {
    SalesRepAddr1 = extractString( paramString, "" );
  }
  public void setSalesRepAddr2( String paramString )
  {
    SalesRepAddr2 = extractString( paramString, "" );
  }
  public void setSalesRepCity( String paramString )
  {
    SalesRepCity = extractString( paramString, "" );
  }
  public void setSalesRepState( String paramString )
  {
    SalesRepState = extractString( paramString, "" );
  }
  public void setSalesRepZip( String paramString )
  {
    SalesRepZip = extractInteger( paramString, 99999 );
  }
  public void setSalesRepSSN( String paramString )
  {
    try
    {
      this.SalesRepSSN = Integer.parseInt(paramString);
    }
    catch(Exception e)
    {
      this.SalesRepSSN = 0;
    }
/*
    int[]               factors     = { 1000000, 10000, 1 };
    int                 tempValue   = 0;
    StringTokenizer     tokenizer   = new StringTokenizer( paramString, "-" );
    
    try
    {
      for ( int i = 0; i < 3; ++i )
      {
        tempValue += ( Integer.parseInt( tokenizer.nextToken() ) * factors[i] );
      }
      SalesRepSSN = tempValue;
    }
    catch( NumberFormatException e )
    {
      SalesRepSSN = 0;
    }
    catch( java.util.NoSuchElementException e )
    {
      SalesRepSSN = 0;
    }
*/
  }
  public void setSalesRepSSN( int ssn )
  {
    SalesRepSSN = ssn;
  }
  public void setSalesRepPlanId( String paramString )
  {
    SalesRepPlanId = extractInteger( paramString, mesConstants.SPID_INVALID );
  }
  public void setSalesRepReportTo( String paramString )
  {
    SalesRepReportTo = extractString( paramString, "" );
  }
  // end sales specific section
  
  public void setUserEmail( String email )
  {
    UserEmail = extractString( email, "" );
  }
  
  public void setUserId( long userId )
  {
    UserId = userId;
  }
  
  public void setUserIdString( String userIdString )
  {
    try
    {
      setUserId( Long.parseLong( userIdString ) );
    }
    catch( NumberFormatException e )
    {
    }
  }
  
  public void setUserLoginId( String userLoginId )
  {
    UserLoginId = userLoginId;
  }
  
  public void setUserName( String userName )
  {
    UserName = userName;
  }
  
  public void setUserOrgId( long orgId )
  {
    UserOrgId = orgId;
  }
  
  public void setUserOrgIdString( String orgIdString )
  {
    try
    {
      setUserOrgId( Long.parseLong( orgIdString ) );
    }
    catch( NumberFormatException e )
    {
    }
  }
  
  public void setUserPassword( String userPassword )
  {
    UserPassword = userPassword;
  }
  
  public void setUserPasswordConfirm( String userPassword )
  {
    UserPasswordConfirm = userPassword;
  }
  
  public void setUserPasswords( String userPassword )
  {
    setUserPassword( userPassword );
    setUserPasswordConfirm( userPassword );
  }
  
  public void setUserRights( long userRights )
  {
    UserRights = userRights;
  }
  
  public void setUserType( String paramString )
  {
    UserType = extractInteger( paramString, mesConstants.USER_INVALID );
  }
  
  public void setValidity( boolean isValid )
  {
    Valid = isValid;
  }
}
//C-           MesUserProfile



