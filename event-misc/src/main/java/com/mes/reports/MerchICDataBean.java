/*@lineinfo:filename=MerchICDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchICDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-02-03 10:39:36 -0800 (Tue, 03 Feb 2009) $
  Version            : $Revision: 15771 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class MerchICDataBean extends OrgSummaryDataBean
{
  public static final int VT_SALES                  = 0;
  public static final int VT_CREDITS                = 1;
  public static final int VT_COUNT                  = 2;
  
  public static final int CT_VISA                   = 0;
  public static final int CT_MC                     = 1;
  public static final int CT_COUNT                  = 2;
  
  public static final String[] VT_FieldPrefix = 
  {
    "sales_",
    "credits_",  
  };
  
  public class DetailRow
  {
    public String                     AuthCode            = null;
    public Date                       BatchDate           = null;
    public String                     CardNumber          = null;
    public String                     CardType            = null;
    public long                       DowngradeRecId      = 0L;
    public String                     EntryMode           = null;
    public int                        IcCat               = -1;
    public String                     IcDesc              = null;
    public double                     IcPerItem           = 0.0;
    public double                     IcRate              = 0.0;
    public long                       MerchantId          = 0L;
    public String                     PurchaseId          = null;
    public String                     ReferenceNumber     = null;
    public double                     TranAmount          = 0.0;
    public Date                       TranDate            = null;
    
    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      AuthCode            = processString( resultSet.getString("auth_code") );
      BatchDate           = resultSet.getDate("batch_date");
      CardNumber          = processString( resultSet.getString("card_number") );
      CardType            = processString( resultSet.getString("card_type") );
      DowngradeRecId      = resultSet.getLong("downgrade_rec_id");
      EntryMode           = processString( resultSet.getString("entry_mode") );
      IcCat               = resultSet.getInt("ic_cat");
      IcDesc              = processString( resultSet.getString("ic_desc") );
      IcPerItem           = resultSet.getDouble("ic_per_item");
      IcRate              = resultSet.getDouble("ic_rate");
      MerchantId          = resultSet.getLong("merchant_number");
      PurchaseId          = processString( resultSet.getString("purchase_id") );
      ReferenceNumber     = processString( resultSet.getString("ref_num") );
      TranAmount          = resultSet.getDouble("tran_amount");
      TranDate            = resultSet.getDate("tran_date");
      
      if( resultSet.getString("debit_credit_ind").equals("C") )
      {
        TranAmount *= -1;
      }
    }
    
    public boolean hasAssocDowngrade()
    {
      return( DowngradeRecId != 0L );
    }
    
    public long getDowngradeRecId()
    {
      return( DowngradeRecId );
    }
    
    public double getIcDue( )
    {
      double          retVal;
      
      retVal = (TranAmount * IcRate * 0.01);
      if ( TranAmount < 0.0 )   // credit
      {
        retVal -= IcPerItem;
      }
      else  // purchase
      {
        retVal += IcPerItem;
      }
      
      return( retVal );
    }
  }
  
  public class IcCategoryDesc
  {
    public String       CardType          = null;
    public String       CatDesc           = null;
    public int          CatId             = -1;
    public double       PerItem           = 0.0;
    public double       Rate              = 0.0;
    public Date         ValidDateBegin    = null;
    public Date         ValidDateEnd      = null;
    
    public IcCategoryDesc( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CardType        = resultSet.getString("card_type");
      CatId           = resultSet.getInt("ic_cat");
      CatDesc         = resultSet.getString("ic_desc");
      PerItem         = resultSet.getDouble("ic_per_item");
      Rate            = resultSet.getDouble("ic_rate");
      ValidDateBegin  = resultSet.getDate("valid_date_begin");
      ValidDateEnd    = resultSet.getDate("valid_date_end");
    }
    
    public IcCategoryDesc( String cardType, int catId, Date batchDate )
    {
      Calendar        cal = Calendar.getInstance();
      
      // setup the calendar to the last day of 9999 (never expires)
      cal.set(Calendar.MONTH,Calendar.DECEMBER);
      cal.set(Calendar.DAY_OF_MONTH,31);
      cal.set(Calendar.YEAR,9999);
      
      // setup an undefined interchange category 
      CardType        = cardType;
      CatId           = catId;
      CatDesc         = "Undefined";
      ValidDateBegin  = batchDate;
      ValidDateEnd    = new java.sql.Date( cal.getTime().getTime() );
    }
    
    public boolean equals( Object obj )
    {
      IcCategoryDesc      desc      = (IcCategoryDesc)obj;
      boolean             retVal    = false;
      
      try
      {
        if (  CardType.equals( desc.CardType ) &&
              CatDesc.equals( desc.CatDesc ) &&
              (CatId == desc.CatId) &&
              (PerItem == desc.PerItem) &&
              (Rate == desc.Rate) &&
              ((ValidDateBegin == null) || ValidDateBegin.equals(desc.ValidDateBegin)) &&
              ((ValidDateEnd == null) || ValidDateEnd.equals(desc.ValidDateEnd)) )
        {
          retVal = true;
        }
      }
      catch( Exception e )
      {
      }        
      return( retVal );
    }
    
    public int getCardTypeIndex( )
    {
      int         retVal        = CT_COUNT;
      if ( CardType.equals("VS") )
      {
        retVal = CT_VISA;
      }
      else    // MC
      {
        retVal = CT_MC;
      }
      return( retVal );
    }
  }
  
  public class IcVolume 
  {
    public IcCategoryDesc     IcCatDesc   = null;
    public double[]           VolAmount   = new double[VT_COUNT];
    public int[]              VolCount    = new int[VT_COUNT];
  
    public IcVolume( ResultSet resultSet )
      throws java.sql.SQLException
    {
      IcCatDesc = getIcDesc(resultSet);
      
      // initialize the data structure                       
      for( int i = 0; i < VT_COUNT; ++i )
      {
        VolCount[i]   = 0;
        VolAmount[i]  = 0.0;
      }
                                   
      // add this row from the result set
      add(resultSet);
    }
    
    public void add( ResultSet resultSet )
      throws java.sql.SQLException
    {
      for( int i = 0; i < VT_COUNT; ++i )
      {
        VolCount[i]   += resultSet.getInt    ( VT_FieldPrefix[i] + "count" );
        VolAmount[i]  += resultSet.getDouble ( VT_FieldPrefix[i] + "amount" );
      }
    }
    
    public int getCardTypeIndex( )
    {
      return( IcCatDesc.getCardTypeIndex() );
    }
  
    public double getCreditsAmount( )
    {
      return( VolAmount[VT_CREDITS] );
    }
    public int getCreditsCount( )
    {
      return( VolCount[VT_CREDITS] );
    }
    
    public double getIcDue( )
    {
      double          retVal        = 0.0;
      
      retVal    += (VolAmount[VT_SALES]   * IcCatDesc.Rate * 0.01);
      retVal    += (VolCount[VT_SALES]    * IcCatDesc.PerItem);
      retVal    -= (VolAmount[VT_CREDITS] * IcCatDesc.Rate * 0.01);
      retVal    -= (VolCount[VT_CREDITS]  * IcCatDesc.PerItem);
      
      return( retVal );
    }
    
    public double getSalesAmount( )
    {
      return( VolAmount[VT_SALES] );
    }
    public int getSalesCount( )
    {
      return( VolCount[VT_SALES] );
    }
    
    public int getTotalCount( )
    {
      return( getSalesCount() + getCreditsCount() );
    }
  }
  
  public class RowData
    implements Comparable
  {
    public long             HierarchyNode       = 0L;
    public Vector           IcData              = new Vector();
    public long             OrgId               = 0L;
    public String           OrgName             = null;
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      HierarchyNode = resultSet.getLong("hierarchy_node");
      OrgId         = resultSet.getLong("org_num");
      OrgName       = resultSet.getString("org_name");
    }
    
    public void addIcData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      IcCategoryDesc    icDesc  = getIcDesc(resultSet);
      IcVolume          icVol   = findIcVol(icDesc);
      
      if ( icVol == null )
      {
        icVol = new IcVolume( resultSet );
        IcData.addElement( icVol );
      }
      else
      {
        icVol.add( resultSet );
      }
    }
    
    public int compareTo( Object obj )
    {
      int           retVal      = 0;
      RowData       temp        = (RowData) obj;
      
      if ( (retVal = OrgName.compareTo(temp.OrgName)) == 0 ) 
      {
        retVal = (int)(HierarchyNode - temp.HierarchyNode);
      }
      return( retVal );
    }
    
    protected IcVolume findIcVol( IcCategoryDesc desc )
    {
      IcVolume            retVal      = null;
      IcVolume            temp        = null;
      
      for( int i = 0; i < IcData.size(); ++i )
      {
        temp = (IcVolume)IcData.elementAt(i);
        if ( desc.equals(temp.IcCatDesc) )
        {
          retVal = temp;
          break;
        }
      }
      return( retVal );
    }
    
    public double getIcCatPercentage( IcVolume icVol )
    {
      int       ctIndex           = icVol.getCardTypeIndex();
      int       icCatCount        = icVol.getTotalCount();
      
      return( ((double)icCatCount/(double)getVolCountTotal(ctIndex)) );
    }
    
    public int getVolCountTotal( int ctIndex )
    {
      IcVolume            icData      = null;
      int                 retVal      = 0;
      
      for( int i = 0; i < IcData.size(); ++i )
      {
        icData = (IcVolume)IcData.elementAt(i);
        if ( icData.getCardTypeIndex() == ctIndex )
        {
          retVal += icData.getTotalCount();
        }
      }
      return( retVal );
    }
  }

  private boolean         AdminUser             = false;
  private Vector          IcCatDescList         = null;
  
  public MerchICDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_SUMMARY )
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"CT\",");
      line.append("\"Cat\",");
      line.append("\"Cat Desc\",");
      if (AdminUser)
      {
        line.append("\"Rate\",");
        line.append("\"Per Item\",");
      }      
      line.append("\"Sales Cnt\",");
      line.append("\"Sales Amt\",");
      line.append("\"Credits Cnt\",");
      line.append("\"Credits Amt\",");
      line.append("\"Vol Percent\"");
      if (AdminUser)
      {
        line.append(",\"IC Due\"");
      }
    }
    else // RT_DETAILS
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"Batch Date\",");
      line.append("\"Tran Date\",");
      line.append("\"CT\",");
      line.append("\"Cat\",");
      line.append("\"Cat Desc\",");
      if (AdminUser)
      {
        line.append("\"Rate\",");
        line.append("\"Per Item\",");
      }      
      line.append("\"Card Number\",");
      line.append("\"Tran Amount\",");
      line.append("\"Auth Code\",");
      line.append("\"Entry Mode\",");
      line.append("\"Purchase ID\",");
      line.append("\"Reference\"");
      if (AdminUser)
      {
        line.append(",\"IC Due\"");
      }
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);
  
    if ( ReportType == RT_SUMMARY )
    {
      IcVolume      icData    = null;
      RowData       record    = (RowData)obj;
    
      // clear the line buffer and add 
      // the merchant specific data.
      for( int i = 0; i < record.IcData.size(); ++i )
      {
        icData = (IcVolume)record.IcData.elementAt(i);
      
        line.append( encodeHierarchyNode(record.HierarchyNode) );
        line.append( ",\"" );
        line.append( record.OrgName );
        line.append( "\",\"" );
        line.append( icData.IcCatDesc.CardType );
        line.append( "\",\"" );
        line.append( icData.IcCatDesc.CatId );
        line.append( "\",\"" );
        line.append( icData.IcCatDesc.CatDesc );
        line.append( "\"," );
        if( AdminUser )
        {
          line.append( icData.IcCatDesc.Rate );
          line.append( "," );
          line.append( icData.IcCatDesc.PerItem );
          line.append( "," );
        }        
        line.append( icData.getSalesCount() );
        line.append( "," );
        line.append( icData.getSalesAmount() );
        line.append( "," );
        line.append( icData.getCreditsCount() );
        line.append( "," );
        line.append( icData.getCreditsAmount() );
        line.append( "," );
        line.append( record.getIcCatPercentage( icData ) );
        if( AdminUser )
        {
          line.append( "," );
          line.append( icData.getIcDue() );
        }
        line.append( "\n" );
      }      
    }
    else      // RT_DETAILS
    {
      DetailRow       record    = (DetailRow)obj;
    
      line.append( encodeHierarchyNode(getReportHierarchyNode()) );
      line.append( ",\"" );
      line.append( getReportOrgName() );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate( record.BatchDate, "MM/dd/yyyy" ) );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate( record.TranDate, "MM/dd/yyyy" ) );
      line.append( ",\"" );
      line.append( record.CardType );
      line.append( "\",\"" );
      line.append( record.IcCat );
      line.append( "\",\"" );
      line.append( record.IcDesc );
      line.append( "\"," );
      if ( AdminUser )
      {
        line.append( record.IcRate );
        line.append( "," );
        line.append( record.IcPerItem );
        line.append( "," );
      }
      line.append( "\"" );
      line.append( record.CardNumber );
      line.append( "\"," );
      line.append( record.TranAmount );
      line.append( ",\"" );
      line.append( record.AuthCode );
      line.append( "\",\"" );
      line.append( record.EntryMode );
      line.append( "\",\"" );
      line.append( record.PurchaseId );
      line.append( "\",\"'" );
      line.append( record.ReferenceNumber );
      line.append( "\"" );
      if ( AdminUser )
      {
        double icDue = (record.TranAmount * record.IcRate * 0.01);
        if( record.TranAmount < 0.0 )
        {
          icDue -= record.IcPerItem;
        }
        else    // purchase
        {
          icDue += record.IcPerItem;
        }
        line.append( "," );
        line.append( icDue );
      }
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    if ( ReportType == RT_SUMMARY )
    {
      filename.append("_ic_summary_");
    }
    else    // RT_DETAILS
    {
      filename.append("_ic_details_");
    }      
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  private IcCategoryDesc getIcDesc( ResultSet rowData )
    throws java.sql.SQLException
  {
    Date                    batchDate     = rowData.getDate("batch_date");
    String                  cardType      = rowData.getString("card_type");
    int                     catId         = rowData.getInt("ic_cat");
    IcCategoryDesc          icDesc        = null;
    ResultSetIterator       it            = null;
    ResultSet               resultSet     = null;
    IcCategoryDesc          retVal        = null;
    
    try
    {
      if ( IcCatDescList == null ) 
      {
        IcCatDescList = new Vector();
        
        /*@lineinfo:generated-code*//*@lineinfo:582^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  icd.card_type         as card_type,
//                    icd.ic_code           as ic_cat,
//                    icd.ic_desc           as ic_desc,
//                    icd.ic_rate           as ic_rate,
//                    icd.ic_per_item       as ic_per_item,
//                    icd.valid_date_begin  as valid_date_begin,
//                    icd.valid_date_end    as valid_date_end
//            from    daily_detail_file_ic_desc icd
//            order by icd.card_type, icd.ic_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  icd.card_type         as card_type,\n                  icd.ic_code           as ic_cat,\n                  icd.ic_desc           as ic_desc,\n                  icd.ic_rate           as ic_rate,\n                  icd.ic_per_item       as ic_per_item,\n                  icd.valid_date_begin  as valid_date_begin,\n                  icd.valid_date_end    as valid_date_end\n          from    daily_detail_file_ic_desc icd\n          order by icd.card_type, icd.ic_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchICDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchICDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:593^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          IcCatDescList.addElement( new IcCategoryDesc( resultSet ) );
        }
        resultSet.close();
        it.close();
      }
      
      for( int i = 0; i < IcCatDescList.size(); ++i )
      {
        icDesc = (IcCategoryDesc)IcCatDescList.elementAt(i);
        
        //
        // current element must match the following:
        //    * card type
        //    * category id
        //    * batch date between valid dates
        //
        if ( icDesc.CardType.equals(cardType) &&
             (icDesc.CatId == catId) &&
             !( batchDate.before(icDesc.ValidDateBegin) ||
                batchDate.after(icDesc.ValidDateEnd) ) )
        {
          retVal = icDesc;
          break;
        }             
      }
      if ( retVal == null )
      {
        retVal = new IcCategoryDesc( cardType, catId, batchDate );
        IcCatDescList.addElement(retVal);
      }
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
    return(retVal);
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:661^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ORDERED USE_NL (dt emd icd) FIRST_ROWS */
//                  dt.merchant_account_number                as merchant_number,
//                  dt.batch_date                             as batch_date,
//                  dt.transaction_date                       as tran_date,
//                  dt.cardholder_account_number              as card_number,
//                  dt.transaction_amount                     as tran_amount,
//                  dt.debit_credit_indicator                 as debit_credit_ind,
//                  dt.downgrade_rec_id                       as downgrade_rec_id,
//                  decode( substr(dt.CARD_TYPE,1,1),
//                          'V','VS',
//                          'M','MC',
//                          dt.card_type )                    as card_type,
//                  dt.submitted_interchange2                 as ic_cat,
//                  dt.reference_number                       as ref_num,
//                  dt.PURCHASE_ID                            as purchase_id,
//                  dt.AUTHORIZATION_NUM                      as auth_code,
//                  nvl(emd.POS_ENTRY_DESC,dt.pos_entry_mode) as entry_mode,
//                  nvl(icd.IC_DESC,
//                      'Undefined (' || dt.submitted_interchange2 ||')' )                     
//                                                            as ic_desc,
//                  nvl(icd.ic_rate,0)                        as ic_rate,
//                  nvl(icd.ic_per_item,0)                    as ic_per_item                
//          from    organization                o,
//                  daily_detail_file_dt        dt,
//                  pos_entry_mode_desc         emd,
//                  daily_detail_file_ic_desc   icd
//          where   o.org_num = :orgId and
//                  dt.merchant_account_number = o.org_group and
//                  dt.batch_date between :beginDate and :endDate and
//                  substr(dt.card_type,1,1) in ('M','V') and
//                  emd.pos_entry_code(+) = dt.pos_entry_mode and
//                  icd.CARD_TYPE(+) = decode(substr(dt.card_type,1,1),
//                                            'M','MC','V','VS',dt.card_type) and
//                  icd.ic_code(+) = dt.submitted_interchange2  and
//                  dt.batch_date between icd.valid_date_begin(+) and icd.valid_date_end(+)
//          order by card_type, dt.submitted_interchange2, dt.batch_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ORDERED USE_NL (dt emd icd) FIRST_ROWS */\n                dt.merchant_account_number                as merchant_number,\n                dt.batch_date                             as batch_date,\n                dt.transaction_date                       as tran_date,\n                dt.cardholder_account_number              as card_number,\n                dt.transaction_amount                     as tran_amount,\n                dt.debit_credit_indicator                 as debit_credit_ind,\n                dt.downgrade_rec_id                       as downgrade_rec_id,\n                decode( substr(dt.CARD_TYPE,1,1),\n                        'V','VS',\n                        'M','MC',\n                        dt.card_type )                    as card_type,\n                dt.submitted_interchange2                 as ic_cat,\n                dt.reference_number                       as ref_num,\n                dt.PURCHASE_ID                            as purchase_id,\n                dt.AUTHORIZATION_NUM                      as auth_code,\n                nvl(emd.POS_ENTRY_DESC,dt.pos_entry_mode) as entry_mode,\n                nvl(icd.IC_DESC,\n                    'Undefined (' || dt.submitted_interchange2 ||')' )                     \n                                                          as ic_desc,\n                nvl(icd.ic_rate,0)                        as ic_rate,\n                nvl(icd.ic_per_item,0)                    as ic_per_item                \n        from    organization                o,\n                daily_detail_file_dt        dt,\n                pos_entry_mode_desc         emd,\n                daily_detail_file_ic_desc   icd\n        where   o.org_num =  :1  and\n                dt.merchant_account_number = o.org_group and\n                dt.batch_date between  :2  and  :3  and\n                substr(dt.card_type,1,1) in ('M','V') and\n                emd.pos_entry_code(+) = dt.pos_entry_mode and\n                icd.CARD_TYPE(+) = decode(substr(dt.card_type,1,1),\n                                          'M','MC','V','VS',dt.card_type) and\n                icd.ic_code(+) = dt.submitted_interchange2  and\n                dt.batch_date between icd.valid_date_begin(+) and icd.valid_date_end(+)\n        order by card_type, dt.submitted_interchange2, dt.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchICDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchICDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:699^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        ReportRows.add( new DetailRow( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          lastNode          = -1L;
    long                          node              = -1L;
    ResultSet                     resultSet         = null;
    RowData                       row               = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( SummaryType == ST_PARENT )
      {
        /*@lineinfo:generated-code*//*@lineinfo:733^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      ordered
//                      INDEX (gm pkgroup_merchant) 
//                      index (ic PK_DAILY_DETAIL_IC_SUMMARY)
//                      use_nl (gm ic)
//                    */
//                    o.org_num                     as org_num,
//                    o.org_group                   as hierarchy_node,
//                    o.org_name                    as org_name,
//                    decode( substr(ic.CARD_TYPE,1,1),
//                            'V','VS',
//                            'M','MC',
//                            ic.card_type )        as card_type,
//                    ic.IC_CAT                     as ic_cat,
//                    ic.batch_date                 as batch_date,
//                    sum(ic.CREDITS_AMOUNT)        as credits_amount,
//                    sum(ic.CREDITS_COUNT)         as credits_count,
//                    sum(ic.SALES_AMOUNT)          as sales_amount, 
//                    sum(ic.SALES_COUNT)           as sales_count
//            from    organization              o,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    daily_detail_ic_summary   ic
//            where   o.org_num           = :orgId and
//                    gm.org_num          = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    ic.merchant_number  = gm.merchant_number and
//                    ic.batch_date between :beginDate and :endDate
//            group by o.org_name, o.org_group, o.org_num, 
//                     ic.card_type, ic.ic_cat, ic.batch_date
//            order by o.org_name, o.org_group, 
//                     ic.card_type, ic.ic_cat, ic.batch_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    ordered\n                    INDEX (gm pkgroup_merchant) \n                    index (ic PK_DAILY_DETAIL_IC_SUMMARY)\n                    use_nl (gm ic)\n                  */\n                  o.org_num                     as org_num,\n                  o.org_group                   as hierarchy_node,\n                  o.org_name                    as org_name,\n                  decode( substr(ic.CARD_TYPE,1,1),\n                          'V','VS',\n                          'M','MC',\n                          ic.card_type )        as card_type,\n                  ic.IC_CAT                     as ic_cat,\n                  ic.batch_date                 as batch_date,\n                  sum(ic.CREDITS_AMOUNT)        as credits_amount,\n                  sum(ic.CREDITS_COUNT)         as credits_count,\n                  sum(ic.SALES_AMOUNT)          as sales_amount, \n                  sum(ic.SALES_COUNT)           as sales_count\n          from    organization              o,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  daily_detail_ic_summary   ic\n          where   o.org_num           =  :1  and\n                  gm.org_num          = o.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  ic.merchant_number  = gm.merchant_number and\n                  ic.batch_date between  :4  and  :5 \n          group by o.org_name, o.org_group, o.org_num, \n                   ic.card_type, ic.ic_cat, ic.batch_date\n          order by o.org_name, o.org_group, \n                   ic.card_type, ic.ic_cat, ic.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchICDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchICDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:769^9*/
      }
      else // ST_CHILD
      {
        /*@lineinfo:generated-code*//*@lineinfo:773^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      ordered
//                      INDEX (gm pkgroup_merchant) 
//                      index (ic PK_DAILY_DETAIL_IC_SUMMARY)
//                      use_nl (gm ic)
//                    */
//                    o.org_num                     as org_num,
//                    o.org_group                   as hierarchy_node,
//                    o.org_name                    as org_name,
//                    decode( substr(ic.CARD_TYPE,1,1),
//                            'V','VS',
//                            'M','MC',
//                            ic.card_type )        as card_type,
//                    ic.IC_CAT                     as ic_cat,
//                    ic.batch_date                 as batch_date,
//                    sum(ic.CREDITS_AMOUNT)        as credits_amount,
//                    sum(ic.CREDITS_COUNT)         as credits_count,
//                    sum(ic.SALES_AMOUNT)          as sales_amount, 
//                    sum(ic.SALES_COUNT)           as sales_count
//            from    parent_org                po,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    organization              o,
//                    daily_detail_ic_summary   ic
//            where   po.parent_org_num   = :orgId and
//                    gm.org_num          = po.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    ic.merchant_number  = gm.merchant_number and
//                    ic.batch_date between :beginDate and :endDate and
//                    o.org_num           = po.org_num 
//            group by o.org_name, o.org_group, o.org_num, 
//                     ic.card_type, ic.ic_cat, ic.batch_date
//            order by o.org_name, o.org_group, 
//                     ic.card_type, ic.ic_cat, ic.batch_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    ordered\n                    INDEX (gm pkgroup_merchant) \n                    index (ic PK_DAILY_DETAIL_IC_SUMMARY)\n                    use_nl (gm ic)\n                  */\n                  o.org_num                     as org_num,\n                  o.org_group                   as hierarchy_node,\n                  o.org_name                    as org_name,\n                  decode( substr(ic.CARD_TYPE,1,1),\n                          'V','VS',\n                          'M','MC',\n                          ic.card_type )        as card_type,\n                  ic.IC_CAT                     as ic_cat,\n                  ic.batch_date                 as batch_date,\n                  sum(ic.CREDITS_AMOUNT)        as credits_amount,\n                  sum(ic.CREDITS_COUNT)         as credits_count,\n                  sum(ic.SALES_AMOUNT)          as sales_amount, \n                  sum(ic.SALES_COUNT)           as sales_count\n          from    parent_org                po,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  organization              o,\n                  daily_detail_ic_summary   ic\n          where   po.parent_org_num   =  :1  and\n                  gm.org_num          = po.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  ic.merchant_number  = gm.merchant_number and\n                  ic.batch_date between  :4  and  :5  and\n                  o.org_num           = po.org_num \n          group by o.org_name, o.org_group, o.org_num, \n                   ic.card_type, ic.ic_cat, ic.batch_date\n          order by o.org_name, o.org_group, \n                   ic.card_type, ic.ic_cat, ic.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchICDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchICDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:811^9*/
      }
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        node = resultSet.getLong("hierarchy_node");
        if ( lastNode != node )
        {
          row = new RowData(resultSet);
          ReportRows.addElement( row );
        }
        row.addIcData(resultSet);
        lastNode = node;
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties( HttpServletRequest request )
  {
    super.setProperties(request);
    
    try
    {
      // download request, determine if the current user has
      // rights to download the rates and ic due columns.
      if ( (HttpHelper.getString(request,"reportSQLJBean",null)) != null )
      {
        UserBean userBean  = ReportUserBean;
        
        // if the user bean has not been established, 
        // attempt to use the bean in the session
        if ( userBean == null )
        {
          userBean = (UserBean)request.getSession(false).getAttribute("UserLogin");
        }
        AdminUser = userBean.hasRight( MesUsers.RIGHT_REPORT_IC_ADMIN );
      }
    }
    catch( Exception e )
    {
      AdminUser = false;
    }      
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/