/*@lineinfo:filename=AcctActivationDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AcctActivationDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class AcctActivationDataBean extends ReportSQLJBean
{
  public static final int           SB_MERCHANT_NUMBER            = 0;
  public static final int           SB_DBA_NAME                   = 1;
  public static final int           SB_ASSOC_NUMBER               = 2;
  public static final int           SB_ACTIVATION_DATE            = 3;
  
  public class DetailRow implements Comparable
  {
    public    Date      ActivationDate      = null;
    public    long      AssocNumber         = 0L;
    public    double    BatchAmount         = 0.0;
    public    String    DbaName             = null;
    public    long      MerchantId          = 0L;
    public    long      OrgId               = 0L;
    private   int       SortOrder           = SB_MERCHANT_NUMBER;
    
    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      ActivationDate      = resultSet.getDate("activation_date");
      AssocNumber         = resultSet.getLong("assoc_number");
      BatchAmount         = resultSet.getDouble("batch_amount");
      DbaName             = processString(resultSet.getString("dba_name"));
      MerchantId          = resultSet.getLong("merchant_number");
      OrgId               = resultSet.getLong("org_num");
      SortOrder           = resultSet.getInt("sort_order");
    }
    
    public int compareTo( Object object )
    {
      DetailRow           entry   = (DetailRow) object;
      int                 retVal;
    
      switch( SortOrder )
      {
        case SB_ASSOC_NUMBER:
          if ( ( retVal = (int)(AssocNumber - entry.AssocNumber) ) == 0 )
          {          
            if ( ( retVal = DbaName.compareTo( entry.DbaName ) ) == 0 )
            {
              if ( ( retVal = (int)(MerchantId - entry.MerchantId) ) == 0 )
              {
                retVal = (int) ActivationDate.compareTo(entry.ActivationDate);
              }
            }
          }
          break;     
           
        case SB_ACTIVATION_DATE:
          if ( ( retVal = (int) ActivationDate.compareTo(entry.ActivationDate) ) == 0 )
          {
            if ( ( retVal = DbaName.compareTo( entry.DbaName ) ) == 0 )
            {
              if ( ( retVal = (int)(AssocNumber - entry.AssocNumber) ) == 0 )
              {              
                retVal = (int)(MerchantId - entry.MerchantId);
              }                
            }
          }
          break;      
        
        case SB_DBA_NAME:
          if ( ( retVal = DbaName.compareTo( entry.DbaName ) ) == 0 )
          {
            if ( ( retVal = (int)(AssocNumber - entry.AssocNumber) ) == 0 )
            {
              if ( ( retVal = (int)(MerchantId - entry.MerchantId) ) == 0 )
              {
                retVal = (int) ActivationDate.compareTo(entry.ActivationDate);
              }                
            }              
          }
          break;      

//        case SB_MERCHANT_NUMBER:
        default:
          if ( ( retVal = (int)(MerchantId - entry.MerchantId) ) == 0 )
          {
            if ( ( retVal = DbaName.compareTo( entry.DbaName ) ) == 0 )
            {
              if ( ( retVal = (int)(AssocNumber - entry.AssocNumber) ) == 0 )
              {
                retVal = (int) ActivationDate.compareTo(entry.ActivationDate);
              }                
            }
          }
          break;
      }
      return( retVal );
    }
  }
  
  private int     SortOrder                     = SB_MERCHANT_NUMBER;
  
  public AcctActivationDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Merchant Id\",");
    line.append("\"DBA Name\",");
    line.append("\"Association\",");
    line.append("\"Activation Date\",");
    line.append("\"Batch Amount\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    DetailRow       record    = (DetailRow)obj;
    
    line.setLength(0);
    line.append( record.MerchantId );
    line.append( ",\"" );
    line.append( record.DbaName );
    line.append( "\"," );
    line.append( record.AssocNumber );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate( record.ActivationDate, "MM/dd/yyyy" ) );
    line.append( "," );
    line.append( record.BatchAmount );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_merch_activations_");
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:217^7*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ index (gm pkgroup_merchant) */
//                 o.org_num                      as org_num,
//                 mf.dba_name                    as dba_name,
//                 mf.MERCHANT_NUMBER             as merchant_number,
//                 mf.ACTIVATION_DATE             as activation_date,
//                 (mf.bank_number || mf.DMAGENT) as assoc_number,
//                 max(sm.bank_amount)            as batch_amount,
//                 :SortOrder                     as sort_order
//          from   group_merchant             gm,
//                 group_rep_merchant         grm,
//                 mif                        mf,
//                 daily_detail_file_summary  sm,
//                 organization               o
//          where  gm.org_num = :orgId and
//                 grm.user_id(+) = :AppFilterUserId and
//                 grm.merchant_number(+) = gm.merchant_number and
//                 ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                 mf.merchant_number = gm.merchant_number and 
//                 mf.ACTIVATION_DATE between :beginDate and :endDate and
//                 sm.merchant_number = mf.merchant_number and
//                 sm.BATCH_DATE      = mf.activation_date and
//                 o.org_group        = mf.merchant_number
//          group by  o.org_num, mf.merchant_number, mf.dba_name, 
//                    mf.activation_date, mf.bank_number, 
//                    mf.dmagent
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ index (gm pkgroup_merchant) */\n               o.org_num                      as org_num,\n               mf.dba_name                    as dba_name,\n               mf.MERCHANT_NUMBER             as merchant_number,\n               mf.ACTIVATION_DATE             as activation_date,\n               (mf.bank_number || mf.DMAGENT) as assoc_number,\n               max(sm.bank_amount)            as batch_amount,\n                :1                      as sort_order\n        from   group_merchant             gm,\n               group_rep_merchant         grm,\n               mif                        mf,\n               daily_detail_file_summary  sm,\n               organization               o\n        where  gm.org_num =  :2  and\n               grm.user_id(+) =  :3  and\n               grm.merchant_number(+) = gm.merchant_number and\n               ( not grm.user_id is null or  :4  = -1 ) and        \n               mf.merchant_number = gm.merchant_number and \n               mf.ACTIVATION_DATE between  :5  and  :6  and\n               sm.merchant_number = mf.merchant_number and\n               sm.BATCH_DATE      = mf.activation_date and\n               o.org_group        = mf.merchant_number\n        group by  o.org_num, mf.merchant_number, mf.dba_name, \n                  mf.activation_date, mf.bank_number, \n                  mf.dmagent";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AcctActivationDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,SortOrder);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AcctActivationDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:244^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        ReportRows.add( new DetailRow( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties( HttpServletRequest request )
  {
    super.setProperties(request);
    
    SortOrder = HttpHelper.getInt(request,"sortOrder",SB_MERCHANT_NUMBER);
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/