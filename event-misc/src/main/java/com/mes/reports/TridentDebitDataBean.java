/*@lineinfo:filename=TridentDebitDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/TridentDebitDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See SVN database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ComboDateField;
import com.mes.forms.DropDownField;
import com.mes.forms.FieldGroup;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class TridentDebitDataBean extends ReportSQLJBean
{
  public static final int         TYPE_ILINK              = 0;
  public static final int         TYPE_OTHER              = 1;
  public static final int         TYPE_ILINK_UNMATCHED    = 2;
  public static final int         TYPE_OTHER_UNMATCHED    = 3;
  public static final int         TYPE_COUNT              = 4;
  
  public static final String      TT_SALE                 = "0200";
  public static final String      TT_VEX_ADJUSTMENT       = "0220";
  public static final String      TT_REVERSAL             = "0400";
  public static final String      TT_CHARGEBACK           = "0422";
  
  private static final String[] SummaryColumnNames =
  {
    "ilink_",
    "other_",
    "unmatched_ilink_",
    "unmatched_other_",
  };
  
  public static class DebitBinsTable extends DropDownTable  
  {
    public DebitBinsTable( long topNode )
      throws java.sql.SQLException
    {
      ResultSetIterator       it              = null;
      ResultSet               resultSet       = null;
      
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:83^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  db.bin                              as bin,
//                    (db.bin || ' - ' || db.description) as bin_desc
//            from    t_hierarchy           th,
//                    trident_debit_bins    db
//            where   th.hier_type = 1 
//                    and th.ancestor = :topNode 
//                    and db.hierarchy_node = th.descendent
//            order by sort_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  db.bin                              as bin,\n                  (db.bin || ' - ' || db.description) as bin_desc\n          from    t_hierarchy           th,\n                  trident_debit_bins    db\n          where   th.hier_type = 1 \n                  and th.ancestor =  :1  \n                  and db.hierarchy_node = th.descendent\n          order by sort_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.TridentDebitDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,topNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.TridentDebitDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:93^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          addElement( resultSet );
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }
  
  public class DetailData
  {
    public String     AuthCode            = null;
    public String     CardNumber          = null;
    public String     DbaName             = null;
    public long       MerchantId          = 0L;
    public String     NetworkId           = null;
    public String     NetworkName         = null;
    public String     ProcessingCode      = null;
    public long       RecId               = 0L;
    public String     ResponseCode        = null;
    public String     RetrievalRefNum     = null;
    public Date       SettlementDate      = null;
    public double     TransactionAmount   = 0.0;
    public String     TransactionId       = null;
    
    public DetailData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      AuthCode            = resultSet.getString("auth_code");
      CardNumber          = resultSet.getString("card_number");
      DbaName             = resultSet.getString("dba_name");
      MerchantId          = resultSet.getLong("merchant_number");
      NetworkId           = resultSet.getString("network_id");
      NetworkName         = resultSet.getString("network_name");
      ProcessingCode      = resultSet.getString("proc_code");
      RecId               = resultSet.getLong("rec_id");
      ResponseCode        = resultSet.getString("resp_code");
      RetrievalRefNum     = resultSet.getString("retrieval_ref_num");
      SettlementDate      = resultSet.getDate("settlement_date");
      TransactionAmount   = resultSet.getDouble("tran_amount");
      TransactionId       = resultSet.getString("tran_id");
    }
  }
  
  public class SummaryData
  {
    public double     InterlinkFees       = 0.0;
    public double     OtherFees           = 0.0;
    public double[]   SalesAmount         = new double[TYPE_COUNT];
    public int[]      SalesCount          = new int[TYPE_COUNT];
    public Date       SettlementDate      = null;
    
    public SummaryData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      SettlementDate  = resultSet.getDate("settlement_date");
      InterlinkFees   = resultSet.getDouble("ilink_fees");
      OtherFees       = resultSet.getDouble("other_fees");
      
      for( int i = 0; i < SummaryColumnNames.length; ++i )
      {
        SalesCount[i]   = resultSet.getInt(SummaryColumnNames[i] + "count");
        SalesAmount[i]  = resultSet.getDouble(SummaryColumnNames[i] + "amount");
      }
    }
    
    public double getInterlinkFees()
    {
      return( InterlinkFees );
    }
    
    public double getInterlinkNetDepositAmount()
    {
      return( getInterlinkSalesAmount() - 
              getInterlinkFees() );
    }
    
    public double getInterlinkSalesAmount( )
    {
      return( getSalesAmount(TYPE_ILINK) );
    }
    
    public int getInterlinkSalesCount( )
    {
      return( getSalesCount(TYPE_ILINK) );
    }
    
    public double getInterlinkUnmatchedSalesAmount( )
    {
      return( getSalesAmount(TYPE_ILINK_UNMATCHED) );
    }
    
    public int getInterlinkUnmatchedSalesCount( )
    {
      return( getSalesCount(TYPE_ILINK_UNMATCHED) );
    }
    
    public double getNetDepositAmount()
    {
      return( getTotalSalesAmount() - 
              getTotalFeesAmount() );
    }
    
    public double getOtherFees()
    {
      return( OtherFees );
    }
    
    public double getOtherNetDepositAmount()
    {
      return( getOtherSalesAmount() - 
              getOtherFees() );
    }
    
    public double getOtherSalesAmount( )
    {
      return( getSalesAmount(TYPE_OTHER) );
    }
    
    public int getOtherSalesCount( )
    {
      return( getSalesCount(TYPE_OTHER) );
    }
    
    public double getOtherUnmatchedSalesAmount( )
    {
      return( getSalesAmount(TYPE_OTHER_UNMATCHED) );
    }
    
    public int getOtherUnmatchedSalesCount( )
    {
      return( getSalesCount(TYPE_OTHER_UNMATCHED) );
    }
        
    public double getSalesAmount( int type )
    {
      return( SalesAmount[type] );
    }
    
    public int getSalesCount( int type )
    {
      return( SalesCount[type] );
    }
    
    public double getTotalFeesAmount( )
    {
      return( getInterlinkFees() + getOtherFees() );
    }
    
    public double getTotalSalesAmount( )
    {
      return( getSalesAmount(TYPE_ILINK) +
              getSalesAmount(TYPE_OTHER) );
    }
    
    public int getTotalSalesCount( )
    {
      return( getSalesCount(TYPE_ILINK) +
              getSalesCount(TYPE_OTHER) );
    }
    
    public double getTotalUnmatchedSalesAmount( )
    {
      return( getSalesAmount(TYPE_ILINK_UNMATCHED) +
              getSalesAmount(TYPE_OTHER_UNMATCHED) );
    }
    
    public int getTotalUnmatchedSalesCount( )
    {
      return( getSalesCount(TYPE_ILINK_UNMATCHED) +
              getSalesCount(TYPE_OTHER_UNMATCHED) );
    }
  }
  
  public TridentDebitDataBean( )
  {
    super(true);   // enable field bean support
  }
  
  protected void createFields(HttpServletRequest request)
  {
    DropDownField   dropDownField     = null;
    
    try
    {
      super.createFields(request);
    
      FieldGroup fgroup = (FieldGroup)getField("searchFields");
      fgroup.deleteField("portfolioId");
      fgroup.deleteField("zip2Node");
      
      // setup the BIN selection field
      dropDownField = new DropDownField("debitBin","Debit BIN", new DebitBinsTable(  getReportHierarchyNodeDefault() ),false);
      fgroup.add( dropDownField );
      dropDownField.setSelectedItem(0);
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( getReportType() == RT_DETAILS )
    {
      line.append("\"Merchant Number\",");
      line.append("\"DBA Name\",");
      line.append("\"Settlement Date\",");
      line.append("\"Card Number\",");
      line.append("\"Tran Id\",");
      line.append("\"Retrieval Ref Num\",");
      line.append("\"Processing Code\",");
      line.append("\"Response Code\",");
      line.append("\"Auth Code\",");
      line.append("\"Network Id\",");
      line.append("\"Network Name\",");
      line.append("\"Tran Amount\"");
    }
    else    // RT_SUMMARY
    {
      line.append("\"Settlement Date\",");
      line.append("\"iLink Count\",");
      line.append("\"iLink Amount\",");
      line.append("\"iLink Fees\",");
      line.append("\"iLink Deposit\",");
      line.append("\"Other Count\",");
      line.append("\"Other Amount\",");
      line.append("\"Other Fees\",");
      line.append("\"Other Deposit\",");
      line.append("\"Total Count\",");
      line.append("\"Total Amount\",");
      line.append("\"iLink Unmatched Count\",");
      line.append("\"iLink Unmatched Amount\",");
      line.append("\"Other Unmatched Count\",");
      line.append("\"Other Unmatched Amount\",");
      line.append("\"Total Unmatched Count\",");
      line.append("\"Total Unmatched Amount\"");
    }      
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    if ( getReportType() == RT_DETAILS )
    {
      DetailData  record  = (DetailData)obj;
      line.append( record.MerchantId );
      line.append( ",\"" );
      line.append( record.DbaName );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate(record.SettlementDate,"MM/dd/yyyy") );
      line.append( "," );
      line.append( record.CardNumber );
      line.append( ",\"" );
      line.append( record.TransactionId );
      line.append( "\",\"" );
      line.append( record.RetrievalRefNum );
      line.append( "\",\"" );
      line.append( record.ProcessingCode );
      line.append( "\",\"" );
      line.append( record.ResponseCode );
      line.append( "\",\"" );
      line.append( record.AuthCode );
      line.append( "\",\"" );
      line.append( record.NetworkId );
      line.append( "\",\"" );
      line.append( record.NetworkName );
      line.append( "\"," );
      line.append( record.TransactionAmount );
    }
    else    // RT_SUMMARY
    {
      SummaryData record  = (SummaryData)obj;
      line.append( DateTimeFormatter.getFormattedDate(record.SettlementDate,"MM/dd/yyyy") );
      line.append( "," );
      line.append( record.getInterlinkSalesCount() );
      line.append( "," );
      line.append( record.getInterlinkSalesAmount() );
      line.append( "," );
      line.append( record.getInterlinkFees() );
      line.append( "," );
      line.append( record.getInterlinkNetDepositAmount() );
      line.append( "," );
      line.append( record.getOtherSalesCount() );
      line.append( "," );
      line.append( record.getOtherSalesAmount() );
      line.append( "," );
      line.append( record.getOtherFees() );
      line.append( "," );
      line.append( record.getOtherNetDepositAmount() );
      line.append( "," );
      line.append( record.getTotalSalesCount() );
      line.append( "," );
      line.append( record.getTotalSalesAmount() );
      line.append( "," );
      line.append( record.getInterlinkUnmatchedSalesCount() );
      line.append( "," );
      line.append( record.getInterlinkUnmatchedSalesAmount() );
      line.append( "," );
      line.append( record.getOtherUnmatchedSalesCount() );
      line.append( "," );
      line.append( record.getOtherUnmatchedSalesAmount() );
      line.append( "," );
      line.append( record.getTotalUnmatchedSalesCount() );
      line.append( "," );
      line.append( record.getTotalUnmatchedSalesAmount() );
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Date                    beginDate = getReportDateBegin();
    Date                    endDate   = getReportDateBegin();
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_trident_debit_");
    
    if ( getReportType() == RT_DETAILS )
    {
      filename.append("details_");
    }
    else  // RT_SUMMARY
    {
      filename.append("summary_");
    }
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( beginDate, "MMddyyyy" ) );
    if ( !beginDate.equals(endDate) )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( endDate, "MMddyyyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    if ( getReportType() == RT_DETAILS )
    {
      loadDetailData(orgId,beginDate,endDate);
    }
    else    // assume RT_SUMMARY
    {
      loadSummaryData(orgId,beginDate,endDate);
    }
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    int                 bin         = getField("debitBin").asInteger();
    ResultSetIterator   it          = null;
    long                nodeId      = orgIdToHierarchyNode( orgId );
    ResultSet           resultSet   = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:482^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  db.merchant_number                          as merchant_number,
//                  mf.dba_name                                 as dba_name,
//                  db.rec_id                                   as rec_id,
//                  db.card_number                              as card_number,
//                  db.transaction_amount                       as tran_amount,
//                  db.settlement_date                          as settlement_date,
//                  db.retrieval_reference_number               as retrieval_ref_num,
//                  db.transaction_identifier                   as tran_id,
//                  db.processing_code                          as proc_code,
//                  db.response_code                            as resp_code,
//                  db.authorization_id_resp_code               as auth_code,
//                  db.network_id                               as network_id,
//                  decode( db.network_id,
//                          3, 'iLink',
//                          'Other' )                           as network_name
//          from    organization                o,
//                  group_merchant              gm,
//                  trident_debit_financial     db,
//                  mif                         mf
//          where   o.org_group = :nodeId and
//                  gm.org_num = o.org_num and
//                  db.merchant_number = gm.merchant_number and
//                  db.settlement_date between :beginDate and :endDate and
//                  db.acquiring_institution_id = :bin and
//                  mf.merchant_number = db.merchant_number
//          order by db.settlement_date, mf.merchant_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  db.merchant_number                          as merchant_number,\n                mf.dba_name                                 as dba_name,\n                db.rec_id                                   as rec_id,\n                db.card_number                              as card_number,\n                db.transaction_amount                       as tran_amount,\n                db.settlement_date                          as settlement_date,\n                db.retrieval_reference_number               as retrieval_ref_num,\n                db.transaction_identifier                   as tran_id,\n                db.processing_code                          as proc_code,\n                db.response_code                            as resp_code,\n                db.authorization_id_resp_code               as auth_code,\n                db.network_id                               as network_id,\n                decode( db.network_id,\n                        3, 'iLink',\n                        'Other' )                           as network_name\n        from    organization                o,\n                group_merchant              gm,\n                trident_debit_financial     db,\n                mif                         mf\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                db.merchant_number = gm.merchant_number and\n                db.settlement_date between  :2  and  :3  and\n                db.acquiring_institution_id =  :4  and\n                mf.merchant_number = db.merchant_number\n        order by db.settlement_date, mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.TridentDebitDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,bin);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.TridentDebitDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:510^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new DetailData( resultSet ) );
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    int                 bin         = getField("debitBin").asInteger();
    ResultSetIterator   it          = null;
    long                nodeId      = orgIdToHierarchyNode( orgId );
    ResultSet           resultSet   = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
    
      /*@lineinfo:generated-code*//*@lineinfo:541^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dbt.settlement_date               as settlement_date,
//                  sum( decode(dbt.network_id,
//                              3,1,
//                              0)
//                     )                              as ilink_count,
//                  sum( decode(dbt.network_id,
//                              3,(dbt.transaction_amount * 
//                                 decode(dbt.debit_credit_indicator,'C',-1,1)),
//                              0)
//                     )                              as ilink_amount,
//                  sum( decode(dbt.network_id,
//                              3,((dbt.reimbursement_fee + nvl(dbt.visa_fees,0)) * 
//                                 decode(dbt.reimburse_fee_debit_credit_ind,'C',-1,1)),
//                              0) 
//                     )                              as ilink_fees,      
//                  sum( decode(dbt.network_id,
//                              3,0,
//                              1)
//                     )                              as other_count,                
//                  sum( decode(dbt.network_id,
//                              3,0,
//                              (dbt.transaction_amount * 
//                               decode(dbt.debit_credit_indicator,'C',-1,1))
//                             )
//                     )                              as other_amount,
//                  sum( decode(dbt.network_id,
//                              3,0,
//                              ((dbt.reimbursement_fee + nvl(dbt.visa_fees,0)) * 
//                               decode(dbt.reimburse_fee_debit_credit_ind,'C',-1,1))
//                             ) 
//                     )                              as other_fees,           
//                  sum( decode(dbt.network_id,
//                              3,decode(dbt.request_message_type,
//                                       '0200',decode(dbt.balanced,'Y',0,'X',0,1),
//                                       0),
//                              0)
//                     )                              as unmatched_ilink_count,                      
//                  sum( decode(dbt.network_id,
//                              3,decode(dbt.request_message_type,
//                                       '0200',decode(dbt.balanced,'Y',0,'X',0,dbt.transaction_amount),
//                                       0),
//                              0)
//                     )                              as unmatched_ilink_amount,
//                  sum( decode(dbt.network_id,
//                              3,0,
//                              decode(dbt.request_message_type,
//                                     '0200',decode(dbt.balanced,'Y',0,'X',0,1),
//                                     0)                    
//                             )
//                     )                              as unmatched_other_count,                      
//                  sum( decode(dbt.network_id,
//                              3,0,
//                              decode(dbt.request_message_type,
//                                     '0200',decode(dbt.balanced,'Y',0,'X',0,dbt.transaction_amount),
//                                     0)                    
//                             )
//                     )                              as unmatched_other_amount
//          from    trident_debit_financial     dbt,
//                  trident_debit_msg_types     mt
//          where   dbt.settlement_date between :beginDate and :endDate and
//                  dbt.response_code = '00' and
//                  dbt.giv_flag = '1' and
//                  dbt.acquiring_institution_id = :bin and
//                  mt.request_message_type(+) = dbt.request_message_type
//          group by dbt.settlement_date
//          order by dbt.settlement_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dbt.settlement_date               as settlement_date,\n                sum( decode(dbt.network_id,\n                            3,1,\n                            0)\n                   )                              as ilink_count,\n                sum( decode(dbt.network_id,\n                            3,(dbt.transaction_amount * \n                               decode(dbt.debit_credit_indicator,'C',-1,1)),\n                            0)\n                   )                              as ilink_amount,\n                sum( decode(dbt.network_id,\n                            3,((dbt.reimbursement_fee + nvl(dbt.visa_fees,0)) * \n                               decode(dbt.reimburse_fee_debit_credit_ind,'C',-1,1)),\n                            0) \n                   )                              as ilink_fees,      \n                sum( decode(dbt.network_id,\n                            3,0,\n                            1)\n                   )                              as other_count,                \n                sum( decode(dbt.network_id,\n                            3,0,\n                            (dbt.transaction_amount * \n                             decode(dbt.debit_credit_indicator,'C',-1,1))\n                           )\n                   )                              as other_amount,\n                sum( decode(dbt.network_id,\n                            3,0,\n                            ((dbt.reimbursement_fee + nvl(dbt.visa_fees,0)) * \n                             decode(dbt.reimburse_fee_debit_credit_ind,'C',-1,1))\n                           ) \n                   )                              as other_fees,           \n                sum( decode(dbt.network_id,\n                            3,decode(dbt.request_message_type,\n                                     '0200',decode(dbt.balanced,'Y',0,'X',0,1),\n                                     0),\n                            0)\n                   )                              as unmatched_ilink_count,                      \n                sum( decode(dbt.network_id,\n                            3,decode(dbt.request_message_type,\n                                     '0200',decode(dbt.balanced,'Y',0,'X',0,dbt.transaction_amount),\n                                     0),\n                            0)\n                   )                              as unmatched_ilink_amount,\n                sum( decode(dbt.network_id,\n                            3,0,\n                            decode(dbt.request_message_type,\n                                   '0200',decode(dbt.balanced,'Y',0,'X',0,1),\n                                   0)                    \n                           )\n                   )                              as unmatched_other_count,                      \n                sum( decode(dbt.network_id,\n                            3,0,\n                            decode(dbt.request_message_type,\n                                   '0200',decode(dbt.balanced,'Y',0,'X',0,dbt.transaction_amount),\n                                   0)                    \n                           )\n                   )                              as unmatched_other_amount\n        from    trident_debit_financial     dbt,\n                trident_debit_msg_types     mt\n        where   dbt.settlement_date between  :1  and  :2  and\n                dbt.response_code = '00' and\n                dbt.giv_flag = '1' and\n                dbt.acquiring_institution_id =  :3  and\n                mt.request_message_type(+) = dbt.request_message_type\n        group by dbt.settlement_date\n        order by dbt.settlement_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.TridentDebitDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,bin);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.TridentDebitDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:609^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new SummaryData( resultSet ) );
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest( request );
  
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000L );
    }
  
    if ( HttpHelper.getString(request,"beginDate.month",null) == null )
    {
      Calendar  cal = Calendar.getInstance();
      
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      ((ComboDateField)getField("beginDate")).setUtilDate( cal.getTime() );
      ((ComboDateField)getField("endDate")).setUtilDate( cal.getTime() );
    }      
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/