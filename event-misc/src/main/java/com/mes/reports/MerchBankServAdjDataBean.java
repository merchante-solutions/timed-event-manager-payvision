/*@lineinfo:filename=MerchBankServAdjDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchBankServAdjDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-08-09 14:57:56 -0700 (Mon, 09 Aug 2010) $
  Version            : $Revision: 17687 $

  Change History:
     See VSS database

  Copyright (C) 2000-2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import com.mes.ach.AchEntryData;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class MerchBankServAdjDataBean extends MerchACHDataBean
{
  public MerchBankServAdjDataBean( )
  {
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    
    if ( ReportType == RT_SUMMARY )
    {
      filename.append("_manual_adj_summary_");
    }
    else  // RT_DETAILS
    {
      filename.append("_manual_adj_details_");
    }      
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"MMddyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"MMddyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // get old BankServ transactions if they exist
      /*@lineinfo:generated-code*//*@lineinfo:88^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                        as hierarchy_node,
//                  mf.dba_name                               as org_name,
//                  bfd.batch_date                            as post_date,
//                  nvl(bds.long_description, 
//                      'Miscellaneous Charge')               as description,
//                  decode(bfd.credit_debit_ind,
//                    'D', decode(bfd.action, 'ACHED', -1, 'CHARGEBACKED', 1, 1),
//                    'C', decode(bfd.action, 'ACHED', 1, 'CHARGEBACKED', -1, -1),
//                    1) * bfd.target_amount                  as amount,
//                  bfd.reference_number                      as ref_num
//          from    group_merchant              gm,
//                  bankserv_ach_detail         bad,
//                  bankserv_file_detail        bfd,
//                  bankserv_ach_descriptions   bds,
//                  mif                         mf
//          where   gm.org_num = :orgId and
//                  bad.merchant_number = gm.merchant_number and
//                  bfd.debtor_id_2 = bad.ach_seq_num and
//                  bfd.batch_date between :beginDate and :endDate and
//                  bfd.action in ('ACHED', 'CHARGEBACKED') and
//                  bds.description(+) = bad.entry_description and
//                  mf.merchant_number = bad.merchant_number
//          order by mf.merchant_number, bfd.batch_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                        as hierarchy_node,\n                mf.dba_name                               as org_name,\n                bfd.batch_date                            as post_date,\n                nvl(bds.long_description, \n                    'Miscellaneous Charge')               as description,\n                decode(bfd.credit_debit_ind,\n                  'D', decode(bfd.action, 'ACHED', -1, 'CHARGEBACKED', 1, 1),\n                  'C', decode(bfd.action, 'ACHED', 1, 'CHARGEBACKED', -1, -1),\n                  1) * bfd.target_amount                  as amount,\n                bfd.reference_number                      as ref_num\n        from    group_merchant              gm,\n                bankserv_ach_detail         bad,\n                bankserv_file_detail        bfd,\n                bankserv_ach_descriptions   bds,\n                mif                         mf\n        where   gm.org_num =  :1  and\n                bad.merchant_number = gm.merchant_number and\n                bfd.debtor_id_2 = bad.ach_seq_num and\n                bfd.batch_date between  :2  and  :3  and\n                bfd.action in ('ACHED', 'CHARGEBACKED') and\n                bds.description(+) = bad.entry_description and\n                mf.merchant_number = bad.merchant_number\n        order by mf.merchant_number, bfd.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchBankServAdjDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchBankServAdjDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        ReportRows.addElement( new DetailRow( resultSet ) );
      }
      it.close();
      
      // get new JPMC transactions if they exist
      /*@lineinfo:generated-code*//*@lineinfo:123^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                        as hierarchy_node,
//                  mf.dba_name                               as org_name,
//                  atd.post_date                             as post_date,
//                  nvl(bds.long_description,
//                    'Micscellaneous Charge')                as description,
//                  decode(adtc.debit_credit_indicator,
//                    'D', 1,
//                    'C', -1,
//                    1) * atd.ach_amount                     as amount,
//                  atd.trace_number                          as ref_num
//          from    group_merchant gm,
//                  ach_trident_detail atd,
//                  ach_detail_tran_code adtc,
//                  bankserv_ach_descriptions bds,
//                  mif mf
//          where   gm.org_num = :orgId
//                  and gm.merchant_number = atd.merchant_number
//                  and atd.post_date between :beginDate and :endDate
//                  and atd.entry_description = :AchEntryData.ED_MANUAL_ADJ
//                  and atd.manual_description_id = bds.description_id
//                  and atd.transaction_code = adtc.ach_detail_trans_code
//                  and gm.merchant_number = mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                        as hierarchy_node,\n                mf.dba_name                               as org_name,\n                atd.post_date                             as post_date,\n                nvl(bds.long_description,\n                  'Micscellaneous Charge')                as description,\n                decode(adtc.debit_credit_indicator,\n                  'D', 1,\n                  'C', -1,\n                  1) * atd.ach_amount                     as amount,\n                atd.trace_number                          as ref_num\n        from    group_merchant gm,\n                ach_trident_detail atd,\n                ach_detail_tran_code adtc,\n                bankserv_ach_descriptions bds,\n                mif mf\n        where   gm.org_num =  :1 \n                and gm.merchant_number = atd.merchant_number\n                and atd.post_date between  :2  and  :3 \n                and atd.entry_description =  :4 \n                and atd.manual_description_id = bds.description_id\n                and atd.transaction_code = adtc.ach_detail_trans_code\n                and gm.merchant_number = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchBankServAdjDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setString(4,AchEntryData.ED_MANUAL_ADJ);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchBankServAdjDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:147^7*/
 
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        ReportRows.addElement( new DetailRow( resultSet ) );
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ resultSet.close(); } catch( Exception e ) { }
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    
    try
    {
      if ( hasAssocDistricts() )
      // returns false unless the current node is an
      // association and it has districts under it.
      {
        if ( District == DISTRICT_NONE )
        {
          /*@lineinfo:generated-code*//*@lineinfo:180^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      o.org_group                               as hierarchy_node,
//                      nvl(mf.district,:DISTRICT_UNASSIGNED)     as district,
//                      nvl(ad.district_desc,decode( mf.district,
//                                  null,'Unassigned',
//                                 ('District ' || to_char(mf.district, '0009')))) as org_name,
//                      count( bfd.target_amount )                as item_count,
//                      sum( decode(bfd.credit_debit_ind,
//                                  'D', decode(bfd.action, 'ACHED', -1, 'CHARGEBACKED', 1, 1),
//                                  'C', decode(bfd.action, 'ACHED', 1, 'CHARGEBACKED', -1, -1),
//                                   1 ) * bfd.target_amount )    as item_amount
//              from    organization                  o,
//                      group_merchant                gm,
//                      group_rep_merchant            grm,
//                      mif                           mf,
//                      bankserv_ach_detail           bad,
//                      bankserv_file_detail          bfd,
//                      assoc_districts               ad
//              where   o.org_num           = :orgId and
//                      gm.org_num          = o.org_num and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      bad.merchant_number = mf.merchant_number and
//                      bfd.debtor_id_2 = bad.ach_seq_num and
//                      bfd.batch_date between :beginDate and :endDate and
//                      bfd.action in ('ACHED', 'CHARGEBACKED') and
//                      ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                      ad.district(+)      = mf.district
//              group by  o.org_num, o.org_group, 
//                        mf.district, ad.district_desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    o.org_group                               as hierarchy_node,\n                    nvl(mf.district, :1 )     as district,\n                    nvl(ad.district_desc,decode( mf.district,\n                                null,'Unassigned',\n                               ('District ' || to_char(mf.district, '0009')))) as org_name,\n                    count( bfd.target_amount )                as item_count,\n                    sum( decode(bfd.credit_debit_ind,\n                                'D', decode(bfd.action, 'ACHED', -1, 'CHARGEBACKED', 1, 1),\n                                'C', decode(bfd.action, 'ACHED', 1, 'CHARGEBACKED', -1, -1),\n                                 1 ) * bfd.target_amount )    as item_amount\n            from    organization                  o,\n                    group_merchant                gm,\n                    group_rep_merchant            grm,\n                    mif                           mf,\n                    bankserv_ach_detail           bad,\n                    bankserv_file_detail          bfd,\n                    assoc_districts               ad\n            where   o.org_num           =  :2  and\n                    gm.org_num          = o.org_num and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    bad.merchant_number = mf.merchant_number and\n                    bfd.debtor_id_2 = bad.ach_seq_num and\n                    bfd.batch_date between  :5  and  :6  and\n                    bfd.action in ('ACHED', 'CHARGEBACKED') and\n                    ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                    ad.district(+)      = mf.district\n            group by  o.org_num, o.org_group, \n                      mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchBankServAdjDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchBankServAdjDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:217^11*/
        }
        else    // a district was specified
        {
          /*@lineinfo:generated-code*//*@lineinfo:221^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                        INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      mf.merchant_number                        as hierarchy_node,
//                      mf.dba_name                               as org_name,
//                      :District                                 as district,
//                      count( bfd.target_amount )                as item_count,
//                      sum( decode(bfd.credit_debit_ind,
//                                  'D', decode(bfd.action, 'ACHED', -1, 'CHARGEBACKED', 1, 1),
//                                  'C', decode(bfd.action, 'ACHED', 1, 'CHARGEBACKED', -1, -1),
//                                   1 ) * bfd.target_amount )    as item_amount
//              from    group_merchant                gm,
//                      group_rep_merchant            grm,
//                      mif                           mf,
//                      bankserv_ach_detail           bad,
//                      bankserv_file_detail          bfd,
//                      organization                  o
//              where   gm.org_num          = :orgId and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      nvl(mf.district,-1) = :District and
//                      bad.merchant_number = mf.merchant_number and
//                      bfd.debtor_id_2 = bad.ach_seq_num and
//                      bfd.batch_date between :beginDate and :endDate and
//                      bfd.action in ('ACHED', 'CHARGEBACKED') and
//                      o.org_group = mf.merchant_number
//              group by  o.org_num, mf.merchant_number, mf.dba_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                      INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    mf.merchant_number                        as hierarchy_node,\n                    mf.dba_name                               as org_name,\n                     :1                                  as district,\n                    count( bfd.target_amount )                as item_count,\n                    sum( decode(bfd.credit_debit_ind,\n                                'D', decode(bfd.action, 'ACHED', -1, 'CHARGEBACKED', 1, 1),\n                                'C', decode(bfd.action, 'ACHED', 1, 'CHARGEBACKED', -1, -1),\n                                 1 ) * bfd.target_amount )    as item_amount\n            from    group_merchant                gm,\n                    group_rep_merchant            grm,\n                    mif                           mf,\n                    bankserv_ach_detail           bad,\n                    bankserv_file_detail          bfd,\n                    organization                  o\n            where   gm.org_num          =  :2  and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    nvl(mf.district,-1) =  :5  and\n                    bad.merchant_number = mf.merchant_number and\n                    bfd.debtor_id_2 = bad.ach_seq_num and\n                    bfd.batch_date between  :6  and  :7  and\n                    bfd.action in ('ACHED', 'CHARGEBACKED') and\n                    o.org_group = mf.merchant_number\n            group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchBankServAdjDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchBankServAdjDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:253^11*/
        }
      }
      else // just standard child report, no districts
      {
        /*@lineinfo:generated-code*//*@lineinfo:258^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                       INDEX (gm pkgroup_merchant) 
//                    */
//                    o.org_num                                 as org_num,
//                    o.org_group                               as hierarchy_node,
//                    o.org_name                                as org_name,
//                    0                                         as district,
//                    count( bfd.target_amount )                as item_count,
//                    sum( decode(bfd.credit_debit_ind,
//                                'D', decode(bfd.action, 'ACHED', -1, 'CHARGEBACKED', 1, 1),
//                                'C', decode(bfd.action, 'ACHED', 1, 'CHARGEBACKED', -1, -1),
//                                 1 ) * bfd.target_amount )    as item_amount
//            from    parent_org                    po,
//                    organization                  o,
//                    group_merchant                gm,
//                    group_rep_merchant            grm,
//                    bankserv_ach_detail           bad,
//                    bankserv_file_detail          bfd
//            where   po.parent_org_num   = :orgId and
//                    o.org_num           = po.org_num and
//                    gm.org_num          = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    bad.merchant_number = gm.merchant_number and
//                    bfd.debtor_id_2 = bad.ach_seq_num and
//                    bfd.batch_date between :beginDate and :endDate and
//                    bfd.action in ('ACHED', 'CHARGEBACKED')
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                     INDEX (gm pkgroup_merchant) \n                  */\n                  o.org_num                                 as org_num,\n                  o.org_group                               as hierarchy_node,\n                  o.org_name                                as org_name,\n                  0                                         as district,\n                  count( bfd.target_amount )                as item_count,\n                  sum( decode(bfd.credit_debit_ind,\n                              'D', decode(bfd.action, 'ACHED', -1, 'CHARGEBACKED', 1, 1),\n                              'C', decode(bfd.action, 'ACHED', 1, 'CHARGEBACKED', -1, -1),\n                               1 ) * bfd.target_amount )    as item_amount\n          from    parent_org                    po,\n                  organization                  o,\n                  group_merchant                gm,\n                  group_rep_merchant            grm,\n                  bankserv_ach_detail           bad,\n                  bankserv_file_detail          bfd\n          where   po.parent_org_num   =  :1  and\n                  o.org_num           = po.org_num and\n                  gm.org_num          = o.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  bad.merchant_number = gm.merchant_number and\n                  bfd.debtor_id_2 = bad.ach_seq_num and\n                  bfd.batch_date between  :4  and  :5  and\n                  bfd.action in ('ACHED', 'CHARGEBACKED')\n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchBankServAdjDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchBankServAdjDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:289^9*/
      }
      processSummaryData(it.getResultSet());
      it.close();
      
      // get JPMC adjustments
      if( hasAssocDistricts() )
      {
        if( District == DISTRICT_NONE )
        {
          /*@lineinfo:generated-code*//*@lineinfo:299^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      o.org_group                               as hierarchy_node,
//                      nvl(mf.district,:DISTRICT_UNASSIGNED)     as district,
//                      nvl(ad.district_desc,decode( mf.district,
//                                  null,'Unassigned',
//                                 ('District ' || to_char(mf.district, '0009')))) as org_name,
//                      count( atd.ach_amount )                   as item_count,
//                      sum( decode(adtc.debit_credit_indicator,
//                                  'D', 1,
//                                  'C', -1,
//                                  1) * atd.ach_amount )         as item_amount        
//              from    organization                  o,
//                      group_merchant                gm,
//                      group_rep_merchant            grm,
//                      mif                           mf,
//                      ach_trident_detail            atd,
//                      ach_detail_tran_code          adtc,
//                      assoc_districts               ad
//              where   o.org_num           = :orgId
//                      and o.org_num = gm.org_num
//                      and grm.user_id(+) = :AppFilterUserId
//                      and grm.merchant_number(+) = gm.merchant_number
//                      and (grm.user_id is not null or :AppFilterUserId = -1) 
//                      and gm.merchant_number = mf.merchant_number
//                      and gm.merchant_number = atd.merchant_number
//                      and atd.post_date between :beginDate and :endDate
//                      and atd.entry_description = :AchEntryData.ED_MANUAL_ADJ
//                      and atd.transaction_code = adtc.ach_detail_trans_code
//                      and mf.association_node = ad.assoc_number(+)
//                      and mf.district = ad.district(+)
//              group by  o.org_num, o.org_group, 
//                        mf.district, ad.district_desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    o.org_group                               as hierarchy_node,\n                    nvl(mf.district, :1 )     as district,\n                    nvl(ad.district_desc,decode( mf.district,\n                                null,'Unassigned',\n                               ('District ' || to_char(mf.district, '0009')))) as org_name,\n                    count( atd.ach_amount )                   as item_count,\n                    sum( decode(adtc.debit_credit_indicator,\n                                'D', 1,\n                                'C', -1,\n                                1) * atd.ach_amount )         as item_amount        \n            from    organization                  o,\n                    group_merchant                gm,\n                    group_rep_merchant            grm,\n                    mif                           mf,\n                    ach_trident_detail            atd,\n                    ach_detail_tran_code          adtc,\n                    assoc_districts               ad\n            where   o.org_num           =  :2 \n                    and o.org_num = gm.org_num\n                    and grm.user_id(+) =  :3 \n                    and grm.merchant_number(+) = gm.merchant_number\n                    and (grm.user_id is not null or  :4  = -1) \n                    and gm.merchant_number = mf.merchant_number\n                    and gm.merchant_number = atd.merchant_number\n                    and atd.post_date between  :5  and  :6 \n                    and atd.entry_description =  :7 \n                    and atd.transaction_code = adtc.ach_detail_trans_code\n                    and mf.association_node = ad.assoc_number(+)\n                    and mf.district = ad.district(+)\n            group by  o.org_num, o.org_group, \n                      mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchBankServAdjDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setString(7,AchEntryData.ED_MANUAL_ADJ);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchBankServAdjDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:336^11*/
        }
        else    // a district was specified
        {
          /*@lineinfo:generated-code*//*@lineinfo:340^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      mf.merchant_number                        as hierarchy_node,
//                      mf.dba_name                               as org_name,
//                      :District                                 as district,
//                      count( atd.ach_amount )                   as item_count,
//                      sum( decode(adtc.debit_credit_indicator,
//                                  'D', 1,
//                                  'C', -1,
//                                  1) * atd.ach_amount )         as item_amount        
//              from    group_merchant                gm,
//                      group_rep_merchant            grm,
//                      mif                           mf,
//                      ach_trident_detail            atd,
//                      ach_detail_tran_code          adtc,
//                      organization                  o
//              where   gm.org_num = :orgId
//                      and grm.user_id(+) = :AppFilterUserId
//                      and grm.merchant_number(+) = gm.merchant_number
//                      and (grm.user_id is not null or :AppFilterUserId = -1) 
//                      and gm.merchant_number = mf.merchant_number
//                      and nvl(mf.district, -1) = :District
//                      and gm.merchant_number = atd.merchant_number
//                      and atd.post_date between :beginDate and :endDate
//                      and atd.entry_description = :AchEntryData.ED_MANUAL_ADJ
//                      and atd.transaction_code = adtc.ach_detail_trans_code
//                      and mf.merchant_number = o.org_group
//              group by  o.org_num, mf.merchant_number, mf.dba_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    mf.merchant_number                        as hierarchy_node,\n                    mf.dba_name                               as org_name,\n                     :1                                  as district,\n                    count( atd.ach_amount )                   as item_count,\n                    sum( decode(adtc.debit_credit_indicator,\n                                'D', 1,\n                                'C', -1,\n                                1) * atd.ach_amount )         as item_amount        \n            from    group_merchant                gm,\n                    group_rep_merchant            grm,\n                    mif                           mf,\n                    ach_trident_detail            atd,\n                    ach_detail_tran_code          adtc,\n                    organization                  o\n            where   gm.org_num =  :2 \n                    and grm.user_id(+) =  :3 \n                    and grm.merchant_number(+) = gm.merchant_number\n                    and (grm.user_id is not null or  :4  = -1) \n                    and gm.merchant_number = mf.merchant_number\n                    and nvl(mf.district, -1) =  :5 \n                    and gm.merchant_number = atd.merchant_number\n                    and atd.post_date between  :6  and  :7 \n                    and atd.entry_description =  :8 \n                    and atd.transaction_code = adtc.ach_detail_trans_code\n                    and mf.merchant_number = o.org_group\n            group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MerchBankServAdjDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,AchEntryData.ED_MANUAL_ADJ);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.MerchBankServAdjDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:372^11*/
        }
      }
      else // just standard child report, no districts
      {
        /*@lineinfo:generated-code*//*@lineinfo:377^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                       INDEX (gm pkgroup_merchant) 
//                    */
//                    o.org_num                                 as org_num,
//                    o.org_group                               as hierarchy_node,
//                    o.org_name                                as org_name,
//                    0                                         as district,
//                    count( atd.ach_amount )                   as item_count,
//                    sum( decode(adtc.debit_credit_indicator,
//                                'D', 1,
//                                'C', -1,
//                                1) * atd.ach_amount )         as item_amount        
//            from    parent_org                    po,
//                    organization                  o,
//                    group_merchant                gm,
//                    group_rep_merchant            grm,
//                    ach_trident_detail            atd,
//                    ach_detail_tran_code          adtc
//            where   po.parent_org_num   = :orgId 
//                    and po.org_num = o.org_num
//                    and o.org_num = gm.org_num
//                    and gm.merchant_number = grm.merchant_number(+)
//                    and grm.user_id(+) = :AppFilterUserId
//                    and (grm.user_id is not null or :AppFilterUserId = -1)
//                    and gm.merchant_number = atd.merchant_number
//                    and atd.post_date between :beginDate and :endDate
//                    and atd.entry_description = :AchEntryData.ED_MANUAL_ADJ
//                    and atd.transaction_code = adtc.ach_detail_trans_code
//            group by  o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                     INDEX (gm pkgroup_merchant) \n                  */\n                  o.org_num                                 as org_num,\n                  o.org_group                               as hierarchy_node,\n                  o.org_name                                as org_name,\n                  0                                         as district,\n                  count( atd.ach_amount )                   as item_count,\n                  sum( decode(adtc.debit_credit_indicator,\n                              'D', 1,\n                              'C', -1,\n                              1) * atd.ach_amount )         as item_amount        \n          from    parent_org                    po,\n                  organization                  o,\n                  group_merchant                gm,\n                  group_rep_merchant            grm,\n                  ach_trident_detail            atd,\n                  ach_detail_tran_code          adtc\n          where   po.parent_org_num   =  :1  \n                  and po.org_num = o.org_num\n                  and o.org_num = gm.org_num\n                  and gm.merchant_number = grm.merchant_number(+)\n                  and grm.user_id(+) =  :2 \n                  and (grm.user_id is not null or  :3  = -1)\n                  and gm.merchant_number = atd.merchant_number\n                  and atd.post_date between  :4  and  :5 \n                  and atd.entry_description =  :6 \n                  and atd.transaction_code = adtc.ach_detail_trans_code\n          group by  o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.MerchBankServAdjDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setString(6,AchEntryData.ED_MANUAL_ADJ);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.MerchBankServAdjDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:408^9*/
      }
      processSummaryData(it.getResultSet());
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/