/*@lineinfo:filename=VMCFeeCollectionDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/reports/VMCFeeCollectionDataBean.sqlj $

  Description:  
    
  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2011-02-11 11:50:12 -0800 (Fri, 11 Feb 2011) $
  Version            : $Revision: 18400 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.Condition;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HierarchyNodeField;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesEncryption;
import com.mes.support.TridentTools;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;


public class VMCFeeCollectionDataBean extends ReportSQLJBean 
{
  public static class ReferenceNumberValidation implements Validation
  {
    private String  ErrorMessage      = null;
    private Field   ReasonCodeField   = null;
    
    public ReferenceNumberValidation( Field reasonCodeField )
    {
      ReasonCodeField = reasonCodeField;
    }
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate( String fdata )
    {
      String  reasonCode  = ReasonCodeField.getData();
        
      ErrorMessage = null;
      
      if ( "7614".equals(reasonCode) )
      {
        if ( fdata == null || fdata.length() < 23 )
        {
          ErrorMessage = "23-digit reference number is required for the selected reason code";
        }          
      }
      return( ErrorMessage == null );
    }
  }


  private String SubmittedDateTime   = null;
  private Vector Errors              = new Vector();
  
  public class CardType extends DropDownTable
  {
    public CardType()
    {
      addElement("",  "select");
      addElement("VS", "VS");
      addElement("MC", "MC");
    }
  }
  
  public class CountryCode extends DropDownTable
  {
    public CountryCode()
    {
      addElement("US", "US");
    }
  }

  public class FunctionCode extends DropDownTable
  {
    public FunctionCode( String cardType )
    {
      addElement("",  "select");
      if( cardType.equals("MC") )
      {
        addElement("700",  "700 - Member generated");
        addElement("780",  "780 - Return");
        addElement("781",  "781 - Resubmission");
        addElement("782",  "782 - Arbitration return");
        addElement("783",  "783 - Clearing system generated");
      }
    }
  }

  public class ReasonCode extends DropDownTable
  {
    public ReasonCode( String cardType )
    {
      addElement("",  "select");
      if( cardType.equals("VS") )
      {
        addElement("0220", "0220 - Arbitration case decision or request fee");
        addElement("0130", "0130 - Lost/stolen card report fees");
        addElement("0250", "0250 - Invalid chargeback handling fee");
        addElement("0350", "0350 - Pre-arbitration settlement");
        addElement("5020", "5020 - Arbitration request/review");
        addElement("5150", "5150 - Chargeback handling fee");
      }
      else if( cardType.equals("MC") )
      {
        addElement("7603",  "7603 - Compliance ruling");
        addElement("7606",  "7606 - Good faith acceptance");
        addElement("7623",  "7623 - Credit to sender (acquirer sends with a representment)");
        addElement("7627",  "7627 - Fees related to the issuer's failure to provide a Merchant Advice Code; for acquirer use only");
        addElement("7954",  "7954 - Invalid fee amount");
        addElement("7955",  "7955 - Duplicate fee processing");
        addElement("7956",  "7956 - Unauthorized fee");
        addElement("7958",  "7958 - Invalid fee collection");
      }
    }
  }
  
  public class SourceIds extends DropDownTable
  {
    public SourceIds( String cardType )
    {
      ResultSetIterator   it        = null;
      ResultSet           rs        = null;
      long        userNodeId        = getReportHierarchyNode();

      try
      {
        connect();

        addElement("",  "select");

        /*@lineinfo:generated-code*//*@lineinfo:169^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(mb.card_type,'MC',ica_number,bin_number) as source_id,
//                    decode(mb.card_type,'MC',ica_number,bin_number) as source_desc
//            from    mbs_bins mb
//            where   mb.bank_number in
//                    (
//                      select distinct
//                             trunc(th.descendent/100000) as bank_number
//                      from   t_hierarchy th
//                      where  th.hier_type = 1
//                             and th.ancestor = :userNodeId
//                             and th.descendent < 1000000000
//                    )
//                    and mb.card_type = :cardType
//                    and mb.processor_id = 1
//            order by source_id                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(mb.card_type,'MC',ica_number,bin_number) as source_id,\n                  decode(mb.card_type,'MC',ica_number,bin_number) as source_desc\n          from    mbs_bins mb\n          where   mb.bank_number in\n                  (\n                    select distinct\n                           trunc(th.descendent/100000) as bank_number\n                    from   t_hierarchy th\n                    where  th.hier_type = 1\n                           and th.ancestor =  :1 \n                           and th.descendent < 1000000000\n                  )\n                  and mb.card_type =  :2 \n                  and mb.processor_id = 1\n          order by source_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.VMCFeeCollectionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userNodeId);
   __sJT_st.setString(2,cardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.VMCFeeCollectionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:186^9*/
        rs = it.getResultSet();

        while( rs.next() )
        {
          addElement(rs);
        }
        rs.close();
        it.close();
      }
      catch( Exception e )
      {
      }
      finally
      {
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }
  
  public class TransactionCode extends DropDownTable
  {
    public TransactionCode( String cardType )
    {
      addElement("",  "select");
      if( cardType.equals("VS") )
      {
        addElement("10", "10 - Outgoing fee collection Debit");
        addElement("20", "20 - Outgoing funds disbursement Credit");
      }
      else if( cardType.equals("MC") )
      {
        addElement("19", "19 - Fee Collection (Credit to Transaction Originator)" );
        addElement("29", "29 - Fee Collection (Debit to Transaction Originator)"  );
      }
    }
  }

  private class FunctionCodeCondition implements Condition
  {
    Field CardType    = null;

    public FunctionCodeCondition( Field cardTypeField )
    {
      CardType = cardTypeField;  
    }

    public boolean isMet() 
    { 
      boolean retVal = true;
    
      if ( CardType.getData().equals("VS") )
      {
        retVal = false;      
      }
      return( retVal );
    }
  }

  public VMCFeeCollectionDataBean()
  {
    super(true);
  }
  
  protected boolean autoSubmit()
  {
    String  fname        = getAutoSubmitName();
    String  cardType     = getData("cardType");
    boolean retVal       = false;    
    SubmittedDateTime    = DateTimeFormatter.getCurDateTimeString("MM/dd/yyyy hh:mm a");

    if (fname.equals("btnSubmit"))
    {
      if( cardType.equals("VS") )
      {
        retVal = insertVsSettlementFee();
      }
      else if(cardType.equals("MC") )
      {
        retVal = insertMcSettlementFee();
      }
    }

    if ( !retVal )
    {
      Errors.add("Internal Error - Try Again");
    }
    return( retVal );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    Field   field;
    String  cardType = HttpHelper.getString(request,"cardType","");
    
    super.createFields(request);
    
    fields.add( new DropDownField("cardType", "Card Type", new CardType(), false) );
    fields.add( new DropDownField("functionCode", "Function Code", new FunctionCode(cardType), false) );
    fields.add( new DropDownField("reasonCode", "Reason Code", new ReasonCode(cardType), false) );
    fields.add( new DropDownField("tranCode", "Transaction Code", new TransactionCode(cardType), false) );
    fields.add( new DropDownField("countryCode", "Country Code", new CountryCode(), false) );
    fields.add( new DropDownField("sourceId", "Source", new SourceIds(cardType), false) );
    fields.add( new NumberField("destId", "Destination", 6, 7, false, 0) );
    fields.add( new HierarchyNodeField(Ctx,getReportHierarchyNode(),"merchantId", "Merchant Number", false) );
    fields.add( new NumberField("cardNumber", "Card Number", 16, 18, false, 0) );
    fields.add( new CurrencyField("tranAmount", "Amount", 8, 8, false ) );
    fields.add( new Field("rolNumber", "ROL Number", 10, 12, true) );
    fields.add( new Field("referenceNumber", "Reference Number (23 digits)", 23, 25, true) );
    fields.add( new Field("userMessage", "User Message", 70, 72, true) );
    fields.add( new ButtonField("btnSubmit" , "Submit") );
    
    fields.getField("functionCode").setOptionalCondition( new FunctionCodeCondition(fields.getField("cardType")) );
    
    if ( "MC".equals(cardType) )
    {
      fields.getField("referenceNumber").addValidation( new ReferenceNumberValidation(fields.getField("reasonCode")) );
    }
    fields.setHtmlExtra("class=\"formFields\"");
  }
  
  public String getSubmittedDateTime()
  {
    return SubmittedDateTime;
  }
  
  public Vector getSubmitErrors()
  {
    return Errors;
  }
    
  private boolean insertMcSettlementFee()
  {
    boolean       retVal        = false;
    String        settleRecId   = null;
    int           sourceBin     = 0;
    
    try
    {
      // convert the specified source ICA to the source BIN
      /*@lineinfo:generated-code*//*@lineinfo:327^7*/

//  ************************************************************
//  #sql [Ctx] { select  mb.bin_number
//          
//          from    mbs_bins    mb
//          where   mb.ica_number = :getInt("sourceId")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1308 = getInt("sourceId");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mb.bin_number\n         \n        from    mbs_bins    mb\n        where   mb.ica_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.VMCFeeCollectionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,__sJT_1308);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sourceBin = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:333^7*/
      
      // if the reason code is "Retrieval Fee Billing"
      // then store the retrieval load sec so the original
      // reason code can be joined during the clearing process
      if ( "7614".equals( getData("reasonCode") ) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:340^9*/

//  ************************************************************
//  #sql [Ctx] { select  rt.retr_load_sec
//            
//            from    network_retrievals    rt
//            where   rt.merchant_number = :getLong("merchantId")
//                    and rt.incoming_date >= (sysdate-180)
//                    and rt.reference_number = :getData("referenceNumber")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1309 = getLong("merchantId");
 String __sJT_1310 = getData("referenceNumber");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rt.retr_load_sec\n           \n          from    network_retrievals    rt\n          where   rt.merchant_number =  :1 \n                  and rt.incoming_date >= (sysdate-180)\n                  and rt.reference_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.VMCFeeCollectionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1309);
   __sJT_st.setString(2,__sJT_1310);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   settleRecId = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:348^9*/
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:351^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mc_settlement_fee_collect 
//          (
//            merchant_number,
//            acquirer_bin,
//            source_ica,
//            dest_ica,
//            transaction_date,
//            tran_type,
//            message_type,
//            function_code,
//            processing_code,
//            reason_code,
//            card_number,
//            transaction_amount,
//            recon_amount,
//            currency_code,
//            recon_currency_code,
//            settlement_flag,
//            message_text,
//            reference_number,
//            settle_rec_id,
//            test_flag,
//            load_file_id
//          )
//          values
//          (
//            :getLong("merchantId"),
//            :sourceBin,
//            lpad(:getInt("sourceId"),6,'0'),
//            lpad(:getInt("destId"),6,'0'),
//            trunc(sysdate),             -- tran date
//            1,     
//            1740,
//            :getInt("functionCode"),
//            :getInt("tranCode"),
//            :getInt("reasonCode"),
//            :getData("cardNumber"),
//            :getDouble("tranAmount"),
//            :getDouble("tranAmount"),
//            '840','840','Y',
//            :getData("userMessage"),
//            :getData("referenceNumber"),
//            :settleRecId,
//            'N',                        -- test flag
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1311 = getLong("merchantId");
 int __sJT_1312 = getInt("sourceId");
 int __sJT_1313 = getInt("destId");
 int __sJT_1314 = getInt("functionCode");
 int __sJT_1315 = getInt("tranCode");
 int __sJT_1316 = getInt("reasonCode");
 String __sJT_1317 = getData("cardNumber");
 double __sJT_1318 = getDouble("tranAmount");
 double __sJT_1319 = getDouble("tranAmount");
 String __sJT_1320 = getData("userMessage");
 String __sJT_1321 = getData("referenceNumber");
   String theSqlTS = "insert into mc_settlement_fee_collect \n        (\n          merchant_number,\n          acquirer_bin,\n          source_ica,\n          dest_ica,\n          transaction_date,\n          tran_type,\n          message_type,\n          function_code,\n          processing_code,\n          reason_code,\n          card_number,\n          transaction_amount,\n          recon_amount,\n          currency_code,\n          recon_currency_code,\n          settlement_flag,\n          message_text,\n          reference_number,\n          settle_rec_id,\n          test_flag,\n          load_file_id\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          lpad( :3 ,6,'0'),\n          lpad( :4 ,6,'0'),\n          trunc(sysdate),             -- tran date\n          1,     \n          1740,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n          '840','840','Y',\n           :11 ,\n           :12 ,\n           :13 ,\n          'N',                        -- test flag\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.reports.VMCFeeCollectionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1311);
   __sJT_st.setInt(2,sourceBin);
   __sJT_st.setInt(3,__sJT_1312);
   __sJT_st.setInt(4,__sJT_1313);
   __sJT_st.setInt(5,__sJT_1314);
   __sJT_st.setInt(6,__sJT_1315);
   __sJT_st.setInt(7,__sJT_1316);
   __sJT_st.setString(8,__sJT_1317);
   __sJT_st.setDouble(9,__sJT_1318);
   __sJT_st.setDouble(10,__sJT_1319);
   __sJT_st.setString(11,__sJT_1320);
   __sJT_st.setString(12,__sJT_1321);
   __sJT_st.setString(13,settleRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:399^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:401^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:404^7*/
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("insertMcSettlementFee(" + getData("merchantId") + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  private boolean insertVsSettlementFee()
  {
    boolean retVal = false;

    try
    {
      String  cardNumberFull  = getData("cardNumber");
      byte[]  cardNumberEnc   = MesEncryption.getClient().encrypt(cardNumberFull.getBytes());
      String  cardNumber      = TridentTools.encodeCardNumber(cardNumberFull);
      String  debitCreditInd  = "10".equals(getData("tranCode")) ? "D" : "C";
      
      if ( isFieldBlank("userMessage") && "0350".equals(getData("reasonCode")) )
      {
        setData("userMessage",
                  "prearb acceptance"
                  + (isFieldBlank("rolNumber")        ? "" : " ROL# " + getData("rolNumber"))
                  + (isFieldBlank("referenceNumber")  ? "" : " Ref# " + getData("referenceNumber"))
                );
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:437^7*/

//  ************************************************************
//  #sql [Ctx] { insert into visa_settlement_fee_collect 
//          (
//            tran_code,
//            merchant_number, 
//            country_code,
//            transaction_date,
//            card_number,
//            card_number_enc,
//            dest_bin,
//            source_bin,
//            reason_code,
//            message_text,
//            tran_type,
//            debit_credit_indicator,
//            currency_code,
//            rol_number,
//            reference_number,
//            transaction_amount,
//            test_flag,
//            load_file_id
//          )
//          values
//          (
//            :getInt("tranCode"),
//            :getLong("merchantId"),
//            :getData("countryCode"),
//            trunc(sysdate),             -- tran date
//            :cardNumber,
//            :cardNumberEnc,
//            :getInt("destId"),
//            :getInt("sourceId"),
//            :getData("reasonCode"),
//            :getData("userMessage"),
//            1,
//            :debitCreditInd,
//            '840',                      -- currency code
//            :getData("rolNumber"),
//            :getData("referenceNumber"),
//            :getDouble("tranAmount"),
//            'N',                        -- test flag
//            0
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1322 = getInt("tranCode");
 long __sJT_1323 = getLong("merchantId");
 String __sJT_1324 = getData("countryCode");
 int __sJT_1325 = getInt("destId");
 int __sJT_1326 = getInt("sourceId");
 String __sJT_1327 = getData("reasonCode");
 String __sJT_1328 = getData("userMessage");
 String __sJT_1329 = getData("rolNumber");
 String __sJT_1330 = getData("referenceNumber");
 double __sJT_1331 = getDouble("tranAmount");
   String theSqlTS = "insert into visa_settlement_fee_collect \n        (\n          tran_code,\n          merchant_number, \n          country_code,\n          transaction_date,\n          card_number,\n          card_number_enc,\n          dest_bin,\n          source_bin,\n          reason_code,\n          message_text,\n          tran_type,\n          debit_credit_indicator,\n          currency_code,\n          rol_number,\n          reference_number,\n          transaction_amount,\n          test_flag,\n          load_file_id\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n          trunc(sysdate),             -- tran date\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n          1,\n           :10 ,\n          '840',                      -- currency code\n           :11 ,\n           :12 ,\n           :13 ,\n          'N',                        -- test flag\n          0\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.reports.VMCFeeCollectionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_1322);
   __sJT_st.setLong(2,__sJT_1323);
   __sJT_st.setString(3,__sJT_1324);
   __sJT_st.setString(4,cardNumber);
   __sJT_st.setBytes(5,cardNumberEnc);
   __sJT_st.setInt(6,__sJT_1325);
   __sJT_st.setInt(7,__sJT_1326);
   __sJT_st.setString(8,__sJT_1327);
   __sJT_st.setString(9,__sJT_1328);
   __sJT_st.setString(10,debitCreditInd);
   __sJT_st.setString(11,__sJT_1329);
   __sJT_st.setString(12,__sJT_1330);
   __sJT_st.setDouble(13,__sJT_1331);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:481^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:483^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:486^7*/
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("insertVsSettlementFee(" + getData("merchantId") + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/