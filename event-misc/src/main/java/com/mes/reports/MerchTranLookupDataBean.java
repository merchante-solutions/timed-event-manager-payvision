/*@lineinfo:filename=MerchTranLookupDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/reports/MerchTranLookupDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-09-19 13:54:03 -0700 (Wed, 19 Sep 2012) $
  Version            : $Revision: 20570 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiTranBase;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonImageField;
import com.mes.forms.ComboDateField;
import com.mes.forms.Condition;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HierarchyNodeField;
import com.mes.reports.TridentBatchDataBean.BatchData;
import com.mes.reports.TridentBatchDataBean.TDLData;
import com.mes.support.TridentTools;
import com.mes.tools.DropDownTable;
import masthead.formats.visak.DetailRecord;
import sqlj.runtime.ResultSetIterator;


public class MerchTranLookupDataBean extends ReportSQLJBean 
{
  private String    CardNumber            = null;
  private String    FindRefNumber         = null;
  private String    FindCardNumber        = null;
  private boolean   FindCardNumberFull    = false;
  private String    FindAuthCode          = null;
  private String    FindTranAmount        = null;
  private double    FindAmountFrom        = 0.00;
  private double    FindAmountTo          = 0.00;
  private String    FindTranType          = null;
  private String    FindPurchaseId        = null;
  private String    FindCardType          = null;
  private String    FindClientRefNumber   = null;
  private String    FindTranId            = null;
  private long      MerchantNumber        = 0L;
  private long      ReportMerchId         = 0L;

  private Vector        DataRows      = new Vector();
  private ReportTotals  ReportTotals  = new ReportTotals();
    
  public class RowData
  {
    public String     ActionCode        = null;
    public Date       ActionDate        = null;
    public double     AuthAmount        = 0.0;
    public String     AuthCode          = null;
    public int        BatchNumber       = 0;
    public Date       BatchDate         = null;
    public long       BatchRecId        = 0L;
    public String     CardNumber        = null;
    public String     CardType          = null;
    public String     ClientRefNumber   = "";
    public long       ControlNumber     = 0L;
    public String     CurrencyCodeAlpha = "USD";
    public String     DataSource        = "";
    public String     EntryMode         = null;
    public double     FxAmount          = 0.0;
    public double     IcDue             = 0.0;
    public double     IcRate            = 0.0;
    public Date       IncomingDate      = null;
    public String     LoadFileId        = null;
    public long       MerchantBatchNumber = 0L;
    public long       MerchantNumber    = 0L;
    public String     PurchaseId        = null;
    public String     ReasonCode        = null;
    public String     ReferenceNumber   = null;
    public String     Response          = null;
    public String     RequestParams     = null;
    public String     TerminalId        = null;
    public double     TranAmount        = 0.0;
    public Date       TranDate          = null;
    public int        TranIndex         = 0;
    public String     TranType          = null;
    public String     TridentTranId     = null;
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      TranDate            = resultSet.getDate("tran_date");
      CardNumber          = processString(resultSet.getString("card_number"));
      CardType            = processString(resultSet.getString("card_type"));
      AuthCode            = processString(resultSet.getString("auth_code"));
      AuthAmount          = resultSet.getDouble("auth_amount");
      ReferenceNumber     = processString(resultSet.getString("reference_number"));
      EntryMode           = processString(resultSet.getString("pos_entry_mode"));
      PurchaseId          = processString(resultSet.getString("purchase_id"));
      TerminalId          = processString(resultSet.getString("terminal_id"));
      MerchantNumber      = resultSet.getLong("merchant_number");
      DataSource          = resultSet.getString("data_source");
      LoadFileId          = resultSet.getString("load_file_id");
      TranAmount          = resultSet.getDouble("tran_amount");
      BatchRecId          = resultSet.getLong("rec_id");

      try
      {
        CurrencyCodeAlpha = resultSet.getString("cc_alpha");
        if( CurrencyCodeAlpha == null )
        {
          CurrencyCodeAlpha = "USD";
        }
      }
      catch( java.sql.SQLException sqle )
      {
      }

      try { TranType = processString(resultSet.getString("tran_type")); } catch( java.sql.SQLException sqle ) { }
      try { TridentTranId = processString(resultSet.getString("trident_tran_id")); } catch( java.sql.SQLException sqle ) { }
      try { IcDue = resultSet.getDouble("ic_due"); } catch( java.sql.SQLException sqle ) { }
      try { IcRate = resultSet.getDouble("ic_rate"); } catch( java.sql.SQLException sqle ) { }
      try { BatchDate = resultSet.getDate("batch_date"); } catch( java.sql.SQLException sqle ) { }
      try { BatchNumber = resultSet.getInt("batch_number"); } catch( java.sql.SQLException sqle ) { }
      try { MerchantBatchNumber = resultSet.getLong("merchant_batch_number"); } catch( java.sql.SQLException sqle ) { }
      try { FxAmount = resultSet.getDouble("fx_amount"); } catch( java.sql.SQLException sqle ) { }
      try { ClientRefNumber = processString(resultSet.getString("client_reference_number")); } catch( java.sql.SQLException sqle ) { }
      try { ActionCode = resultSet.getString("action_code"); } catch( java.sql.SQLException sqle ) { }
      try { ActionDate = resultSet.getDate("action_date"); } catch( java.sql.SQLException sqle ) { }
      try { ControlNumber = resultSet.getLong("control_number"); } catch( java.sql.SQLException sqle ) { }
      try { IncomingDate = resultSet.getDate("incoming_date"); } catch( java.sql.SQLException sqle ) { }
      try { ReasonCode = resultSet.getString("reason_code"); } catch( java.sql.SQLException sqle ) { }
    }
    
    public RowData( String dataSource,
                    Date   tranDate,
                    long   merchantNumber,
                    String terminalId,
                    String tranType,
                    String cardType, 
                    String cardNumber, 
                    String refNumber, 
                    String clientRefNumber, 
                    String purchaseId, 
                    String authCode, 
                    String entryMode, 
                    double tranAmount, 
                    String tridentTranId,
                    String currencyCodeAlpha,
                    Date   batchDate,
                    int    batchNumber,
                    long   batchRecId,
                    int    tranIndex)
    {
      TranDate             = tranDate;
      DataSource           = dataSource;
      MerchantNumber       = merchantNumber;
      TerminalId           = terminalId;
      TranType             = tranType;
      CardType             = cardType;
      CardNumber           = cardNumber;
      ReferenceNumber      = refNumber;
      ClientRefNumber      = clientRefNumber;
      PurchaseId           = purchaseId;
      AuthCode             = authCode;
      EntryMode            = entryMode;
      TranAmount           = tranAmount;
      TridentTranId        = tridentTranId;
      CurrencyCodeAlpha    = currencyCodeAlpha;
      BatchDate            = batchDate;
      BatchNumber          = batchNumber;
      BatchRecId           = batchRecId;
      TranIndex            = tranIndex;
    }
    
    public double getIcDue()
    {
      return( TranAmount * IcRate * 0.01 );
    }   
    
    public boolean isVoid()
    {
      return ( (DataSource.equals("tc_api") || DataSource.equals("v_auth")) && TranType.equals(TridentApiTranBase.TT_VOID) );
    }
  }
    
  public class ReportTotals
  {
    public double  TotalAuthAmount       = 0.0;
    public double  TotalBOAmount         = 0.0;
    public double  TotalBatchAmount      = 0.0;
    public double  TotalCBAmount         = 0.0;
    public double  TotalFxAmount         = 0.0;
    public double  TotalFxIcAmount       = 0.0;
    public double  TotalIcAmount         = 0.0;
    public double  TotalSettlementAmount = 0.0;    
    public double  TotalTPGLogAmount     = 0.0;
    public double  TotalVCAmount         = 0.0;
    public double  TotalTridentBatchAmount  = 0.0;
    public int     TotalAuthCount        = 0;
    public int     TotalBOCount          = 0;
    public int     TotalBatchCount       = 0;
    public int     TotalCBCount          = 0;
    public int     TotalFxCount          = 0;
    public int     TotalFxIcCount        = 0;
    public int     TotalIcCount          = 0;
    public int     TotalSettlementCount  = 0;
    public int     TotalTPGLogCount      = 0;
    public int     TotalVCCount          = 0;
    public int     TotalTridentBatchCount = 0;
    
    public ReportTotals( )
    {
    }
    
    public void add( String dataSource, double tranAmount )
    { 
      if( dataSource.equals("tc33_auth") )
      {
        TotalAuthCount ++;
        TotalAuthAmount += tranAmount;
      }
      else if( dataSource.equals("tc_api") )
      {
        TotalBOCount ++;
        TotalBOAmount += tranAmount;
      }
      else if( dataSource.equals("tc_api_fx") )
      {
        TotalFxCount ++;
        TotalFxAmount += tranAmount;
      }
      else if( dataSource.equals("dd_file_ext_dt") )
      {
        TotalBatchCount ++;
        TotalBatchAmount += tranAmount;
      }
      else if( dataSource.equals("dd_file_dt") )
      {
        TotalSettlementCount ++; 
        TotalSettlementAmount += tranAmount;
      }
      else if( dataSource.equals("payvision_api") )
      {
        TotalFxIcCount ++; 
        TotalFxIcAmount += tranAmount;
      }
      else if( dataSource.equals("dd_file_ic") )
      {
        TotalIcCount ++; 
        TotalIcAmount += tranAmount;
      }
      else if( dataSource.equals("trident_api_log") )
      {
        TotalTPGLogCount ++; 
        TotalTPGLogAmount += tranAmount;
      }
      else if( dataSource.equals("v_auth") )
      {
        TotalVCCount ++; 
        TotalVCAmount += tranAmount;
      }
      else if( dataSource.equals("trident_batch") || dataSource.equals("trident_api_batch") )
      {
        TotalTridentBatchCount ++; 
        TotalTridentBatchAmount += tranAmount;
      }
      else if( dataSource.equals("network_chargebacks") )
      {
        TotalCBCount ++; 
        TotalCBAmount += tranAmount;
      }
    }
    
    public void clear( )
    {
      TotalAuthAmount       = 0.0;
      TotalBOAmount         = 0.0;
      TotalBatchAmount      = 0.0;
      TotalFxAmount         = 0.0;
      TotalFxIcAmount       = 0.0;
      TotalIcAmount         = 0.0;
      TotalSettlementAmount = 0.0;    
      TotalAuthCount        = 0;
      TotalBOCount          = 0;
      TotalBatchCount       = 0;
      TotalFxCount          = 0;
      TotalFxIcCount        = 0;
      TotalIcCount          = 0;
      TotalSettlementCount  = 0;
    }
  }
  
  public class RecordData 
  {
  }

  private class MerchantIdRequiredCondition
    implements Condition
  {
    Field TranId =   null;

    public MerchantIdRequiredCondition( Field tranIdField )
    {
      TranId = tranIdField;
    }
    
    public boolean isMet()
    { 
      boolean retVal   = false;
      if ( TranId.isBlank() )
      {
        retVal = true;
      }
      return( retVal );
    }
  }
  
  public class CardTypeTable extends DropDownTable
  {
    public CardTypeTable( )
    {
      addElement("",   "All");
      addElement("VS", "Visa");        
      addElement("MC", "Master Card");
      addElement("DS", "Discover");
      addElement("AM", "American Express");
      addElement("BL", "Bill Me Later");
      addElement("allOthers", "Others");
    }
  }
  
  public class MerchantTable extends DropDownTable
  {
    private int     MerchantCount      = 0;
  
    public MerchantTable( long nodeId )
    {
      ResultSetIterator   it        = null;
      ResultSet           rs        = null;
      
      try
      {
        connect();
        
        addElement("",  "select");
        MerchantCount = 0;
              
        /*@lineinfo:generated-code*//*@lineinfo:381^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                          as merchant_number,
//                    mf.merchant_number || ' - ' || mf.dba_name  as merchant_desc
//            from    organization        o,
//                    group_merchant      gm,
//                    mif                 mf
//            where   o.org_group = :nodeId 
//                    and gm.org_num = o.ORG_NUM
//                    and mf.merchant_number = gm.merchant_number
//            order by mf.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                          as merchant_number,\n                  mf.merchant_number || ' - ' || mf.dba_name  as merchant_desc\n          from    organization        o,\n                  group_merchant      gm,\n                  mif                 mf\n          where   o.org_group =  :1  \n                  and gm.org_num = o.ORG_NUM\n                  and mf.merchant_number = gm.merchant_number\n          order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:392^9*/
        rs = it.getResultSet();
        
        while( rs.next() )
        {
          addElement(rs);
          ++MerchantCount;          
        }
        rs.close();
        it.close();
      }
      catch( Exception e )
      {
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
    
    public int getMerchantCount()
    {
      return( MerchantCount );
    }
  }

  public MerchTranLookupDataBean()
  {   
    super(true);
  }
  
  public ReportTotals getReportTotals( )
  {
    return(ReportTotals);
  }
  
  public Vector getDataRows()
  {
    return( DataRows );
  }
  
  public long getReportMerchId()
  {
    return( ReportMerchId );
  }
  
  protected void createFields(HttpServletRequest request)
  {    
    super.createFields(request);
    
    FieldGroup      fgroup        = null;

    fgroup = new FieldGroup("tranLookupFields");

    fgroup.add( new ComboDateField("beginDate", "Begin Date", true, false, 2000, false, true));
    fgroup.add( new ComboDateField("endDate", "End Date", true, false, 2000, false, true));
    fgroup.add( new Field("cardNumberFirst6", "Card Number (first 6)", 6, 6, true) );
    fgroup.add( new Field("cardNumberLast4", "Card Number (last 4)", 4, 4, true) );
    fgroup.add( new Field("refNumber", "Reference Number", 25, 25, true) );
    fgroup.add( new Field("clientRefNumber", "Client Reference Number", 28, 28, true) );
    fgroup.add( new Field("authCode", "Auth Code", 8, 8, true ) );
    fgroup.add( new Field("purchaseId", "Purchase ID", 25, 25, true ) );
    fgroup.add( new Field("tranId", "Tran ID", 32, 35, true) );
    fgroup.add( new CurrencyField("amountFrom", "Amount From", 8, 8, true ) );
    fgroup.add( new CurrencyField("amountTo", "Amount To", 8, 8, true ) );
    fgroup.add( new DropDownField("cardType", "Card Type", new CardTypeTable(), true) );            
    fgroup.add( new ButtonImageField ("searchButton", "Search", "/images/search.gif", fields));
    
    if( merchantCount(getReportHierarchyNodeDefault()) <= 40 )
    {
      MerchantTable midTable = new MerchantTable( getReportHierarchyNodeDefault() );
      fgroup.add( new DropDownField("merchantId", "Merchant ID", midTable, false) );            
    }
    else
    {
      fgroup.add( new HierarchyNodeField(Ctx, getReportHierarchyNodeDefault(), "merchantId", "Merchant ID (required)", false ) );      
    }
    
    // Merchant ID not required when searching by Trident Tran ID 
    Condition condition = new MerchantIdRequiredCondition(fgroup.getField("tranId"));
    fgroup.getField("merchantId").setOptionalCondition( condition );

    fields.add(fgroup);
    fields.setHtmlExtra("class=\"formFields\"");
  }  
    
  public void loadData()
  {
  }
    
  public void loadAuthData()
  {
    ResultSetIterator    it               = null;
    ResultSet            resultSet        = null;
    RowData              row              = null;
    
    ReportMerchId = 0L;
    ReportRows.clear();

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:495^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  auth.rec_id                                     as rec_id,
//                  auth.merchant_number                            as merchant_number,
//                  auth.terminal_id                                as terminal_id,
//                  auth.transaction_date                           as tran_date,
//                  auth.transaction_time                           as tran_ts,
//                  auth.card_number                                as card_number,
//                  auth.card_number_enc                            as card_number_enc,
//                  auth.card_type                                  as card_type,
//                  auth.authorization_code                         as auth_code, 
//                  auth.authorized_amount                          as auth_amount,
//                  auth.authorized_amount                          as tran_amount,
//                  auth.retrieval_refrence_number                  as reference_number,
//                  ''                                              as purchase_id,
//                  ''                                              as load_file_id,
//                  nvl(pd.pos_entry_desc,auth.pos_entry_mode_code) as pos_entry_mode,
//                  nvl( auth.expiration_date_incoming,
//                       to_char(auth.expiration_date,'MMyy'))      as exp_date,
//                  lft.file_prefix                                 as file_source,
//                  'tc33_auth'                                     as data_source
//          from    tc33_authorization    auth,
//                  mes_load_file_types   lft,
//                  pos_entry_mode_desc   pd
//          where   auth.transaction_date between :getReportDateBegin() and :getReportDateEnd() and
//                  ( (0 = :MerchantNumber and auth.trident_transaction_id = :getData("tranId")) or
//                    (auth.merchant_number = :MerchantNumber and ('passall' = :FindTranId or auth.trident_transaction_id like :FindTranId))
//                  ) and
//                  ( 'passall' = :FindPurchaseId ) and
//                  ( 'passall' = :FindClientRefNumber ) and
//                  ( 'passall' = :FindCardNumber or auth.card_number like :FindCardNumber ) and
//                  ( 'passall' = :FindAuthCode or auth.authorization_code like :FindAuthCode ) and
//                  ( 'passall' = :FindTranAmount or auth.authorized_amount between :FindAmountFrom and :FindAmountTo ) and
//                  ( 'passall' = :FindRefNumber or auth.retrieval_refrence_number like :FindRefNumber ) and
//                  ( 'passall' = :FindCardType or
//                    ('allOthers' = :FindCardType and auth.card_type not in('VS','MC','AM','DS')) or
//                    auth.card_type = :FindCardType ) and
//                  lft.file_prefix = substr(auth.load_filename,1,length(lft.file_prefix)) and
//                  pd.pos_entry_code(+) = to_char(lpad(substr(auth.pos_entry_mode_code,1,length(auth.pos_entry_mode_code)-1),2,'0'))
//                  and exists
//                  (
//                    select gm.merchant_number
//                    from   organization   o,
//                           group_merchant gm
//                    where  o.org_group = :getReportHierarchyNodeDefault() and
//                           gm.org_num = o.org_num and
//                           gm.merchant_number = auth.merchant_number
//                  )
//          order by auth.transaction_time
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1172 = getReportDateBegin();
 java.sql.Date __sJT_1173 = getReportDateEnd();
 String __sJT_1174 = getData("tranId");
 long __sJT_1175 = getReportHierarchyNodeDefault();
  try {
   String theSqlTS = "select  auth.rec_id                                     as rec_id,\n                auth.merchant_number                            as merchant_number,\n                auth.terminal_id                                as terminal_id,\n                auth.transaction_date                           as tran_date,\n                auth.transaction_time                           as tran_ts,\n                auth.card_number                                as card_number,\n                auth.card_number_enc                            as card_number_enc,\n                auth.card_type                                  as card_type,\n                auth.authorization_code                         as auth_code, \n                auth.authorized_amount                          as auth_amount,\n                auth.authorized_amount                          as tran_amount,\n                auth.retrieval_refrence_number                  as reference_number,\n                ''                                              as purchase_id,\n                ''                                              as load_file_id,\n                nvl(pd.pos_entry_desc,auth.pos_entry_mode_code) as pos_entry_mode,\n                nvl( auth.expiration_date_incoming,\n                     to_char(auth.expiration_date,'MMyy'))      as exp_date,\n                lft.file_prefix                                 as file_source,\n                'tc33_auth'                                     as data_source\n        from    tc33_authorization    auth,\n                mes_load_file_types   lft,\n                pos_entry_mode_desc   pd\n        where   auth.transaction_date between  :1  and  :2  and\n                ( (0 =  :3  and auth.trident_transaction_id =  :4 ) or\n                  (auth.merchant_number =  :5  and ('passall' =  :6  or auth.trident_transaction_id like  :7 ))\n                ) and\n                ( 'passall' =  :8  ) and\n                ( 'passall' =  :9  ) and\n                ( 'passall' =  :10  or auth.card_number like  :11  ) and\n                ( 'passall' =  :12  or auth.authorization_code like  :13  ) and\n                ( 'passall' =  :14  or auth.authorized_amount between  :15  and  :16  ) and\n                ( 'passall' =  :17  or auth.retrieval_refrence_number like  :18  ) and\n                ( 'passall' =  :19  or\n                  ('allOthers' =  :20  and auth.card_type not in('VS','MC','AM','DS')) or\n                  auth.card_type =  :21  ) and\n                lft.file_prefix = substr(auth.load_filename,1,length(lft.file_prefix)) and\n                pd.pos_entry_code(+) = to_char(lpad(substr(auth.pos_entry_mode_code,1,length(auth.pos_entry_mode_code)-1),2,'0'))\n                and exists\n                (\n                  select gm.merchant_number\n                  from   organization   o,\n                         group_merchant gm\n                  where  o.org_group =  :22  and\n                         gm.org_num = o.org_num and\n                         gm.merchant_number = auth.merchant_number\n                )\n        order by auth.transaction_time";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1172);
   __sJT_st.setDate(2,__sJT_1173);
   __sJT_st.setLong(3,MerchantNumber);
   __sJT_st.setString(4,__sJT_1174);
   __sJT_st.setLong(5,MerchantNumber);
   __sJT_st.setString(6,FindTranId);
   __sJT_st.setString(7,FindTranId);
   __sJT_st.setString(8,FindPurchaseId);
   __sJT_st.setString(9,FindClientRefNumber);
   __sJT_st.setString(10,FindCardNumber);
   __sJT_st.setString(11,FindCardNumber);
   __sJT_st.setString(12,FindAuthCode);
   __sJT_st.setString(13,FindAuthCode);
   __sJT_st.setString(14,FindTranAmount);
   __sJT_st.setDouble(15,FindAmountFrom);
   __sJT_st.setDouble(16,FindAmountTo);
   __sJT_st.setString(17,FindRefNumber);
   __sJT_st.setString(18,FindRefNumber);
   __sJT_st.setString(19,FindCardType);
   __sJT_st.setString(20,FindCardType);
   __sJT_st.setString(21,FindCardType);
   __sJT_st.setLong(22,__sJT_1175);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:544^7*/
      
      if ( it != null )
      {
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          boolean addRecord = true;
          if( FindCardNumberFull )
          {
            String cardNumberFull = TridentTools.getCardNumberFull(resultSet.getString("card_number_enc"));
            if( cardNumberFull.indexOf(CardNumber) == -1 )
            {
              addRecord  = false;
            }
          }

          if( addRecord )
          {
            ReportMerchId = resultSet.getLong("merchant_number");
            ReportRows.addElement( new RowData( resultSet ) );
            ReportTotals.add(resultSet.getString("data_source"), resultSet.getDouble("auth_amount"));
          }
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadAuthData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadBOData()
  {
    ResultSetIterator    it               = null;
    ResultSet            resultSet        = null;
    
    ReportMerchId = 0L;
    ReportRows.clear();

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:591^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (tapi idx_trident_capture_api) */
//                  tapi.rec_id                    as rec_id,
//                  tapi.merchant_number           as merchant_number,
//                  tapi.terminal_id               as terminal_id,
//                  tapi.auth_amount               as auth_amount,
//                  tapi.transaction_amount * decode(tapi.debit_credit_indicator,'C',-1,1)
//                                                 as tran_amount,
//                  decode( tapi.batch_id, null, '000000', 'not000000' )
//                                                 as load_file_id,
//                  tapi.auth_date                 as auth_date,
//                  tapi.auth_code                 as auth_code,
//                  tapi.batch_date                as batch_date,
//                  tapi.batch_number              as batch_number,
//                  tapi.card_number               as card_number,
//                  tapi.card_number_enc           as card_number_enc,
//                  tapi.card_type                 as card_type,
//                  nvl(pd.pos_entry_desc,tapi.pos_entry_mode)
//                                                 as pos_entry_mode,
//                  nvl(tapi.purchase_id,' ')      as purchase_id,
//                  tapi.reference_number          as reference_number,
//                  tapi.client_reference_number   as client_reference_number,
//                  tapi.transaction_date          as tran_date,
//                  tapi.transaction_type          as tran_type,
//                  tapi.trident_tran_id           as trident_tran_id,
//                  tapi.fx_amount_base            as fx_amount,
//                  '0'                            as merchant_batch_number,
//                  'tc_api'                       as data_source,
//                  cc.currency_code_alpha         as cc_alpha
//          from    trident_capture_api   tapi,
//                  trident_profile       tp,
//                  pos_entry_mode_desc   pd,
//                  currency_codes        cc
//          where   tapi.transaction_date between :getReportDateBegin() and :getReportDateEnd() and
//                  tapi.merchant_number = :MerchantNumber and
//                  tp.terminal_id = tapi.terminal_id and
//                  tp.product_code != 'TP' and
//                  ( 'passall' = :FindTranId or tapi.trident_tran_id like :FindTranId ) and
//                  ( 'passall' = :FindCardNumber or tapi.card_number like :FindCardNumber ) and
//                  ( 'passall' = :FindAuthCode or tapi.auth_code like :FindAuthCode ) and
//                  ( 'passall' = :FindTranAmount or tapi.transaction_amount between :FindAmountFrom and :FindAmountTo ) and
//                  ( 'passall' = :FindRefNumber or tapi.reference_number like :FindRefNumber ) and
//                  ( 'passall' = :FindPurchaseId or lower(tapi.purchase_id) like :FindPurchaseId ) and
//                  ( 'passall' = :FindClientRefNumber or lower(tapi.client_reference_number) like :FindClientRefNumber ) and
//                  ( 'passall' = :FindCardType or 
//                    ('allOthers' = :FindCardType and tapi.card_type not in('VS','MC','AM','DS','BL')) or
//                    tapi.card_type = :FindCardType ) and
//                  pd.pos_entry_code(+) = tapi.pos_entry_mode and
//                  cc.currency_code(+) = nvl(tapi.currency_code,'840')
//                  and exists
//                  (
//                    select gm.merchant_number
//                    from   organization   o,
//                           group_merchant gm
//                    where  o.org_group = :getReportHierarchyNodeDefault() and
//                           gm.org_num = o.org_num and
//                           gm.merchant_number = tapi.merchant_number
//                  )
//          order by tapi.transaction_date, tapi.mesdb_timestamp
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1176 = getReportDateBegin();
 java.sql.Date __sJT_1177 = getReportDateEnd();
 long __sJT_1178 = getReportHierarchyNodeDefault();
  try {
   String theSqlTS = "select  /*+ index (tapi idx_trident_capture_api) */\n                tapi.rec_id                    as rec_id,\n                tapi.merchant_number           as merchant_number,\n                tapi.terminal_id               as terminal_id,\n                tapi.auth_amount               as auth_amount,\n                tapi.transaction_amount * decode(tapi.debit_credit_indicator,'C',-1,1)\n                                               as tran_amount,\n                decode( tapi.batch_id, null, '000000', 'not000000' )\n                                               as load_file_id,\n                tapi.auth_date                 as auth_date,\n                tapi.auth_code                 as auth_code,\n                tapi.batch_date                as batch_date,\n                tapi.batch_number              as batch_number,\n                tapi.card_number               as card_number,\n                tapi.card_number_enc           as card_number_enc,\n                tapi.card_type                 as card_type,\n                nvl(pd.pos_entry_desc,tapi.pos_entry_mode)\n                                               as pos_entry_mode,\n                nvl(tapi.purchase_id,' ')      as purchase_id,\n                tapi.reference_number          as reference_number,\n                tapi.client_reference_number   as client_reference_number,\n                tapi.transaction_date          as tran_date,\n                tapi.transaction_type          as tran_type,\n                tapi.trident_tran_id           as trident_tran_id,\n                tapi.fx_amount_base            as fx_amount,\n                '0'                            as merchant_batch_number,\n                'tc_api'                       as data_source,\n                cc.currency_code_alpha         as cc_alpha\n        from    trident_capture_api   tapi,\n                trident_profile       tp,\n                pos_entry_mode_desc   pd,\n                currency_codes        cc\n        where   tapi.transaction_date between  :1  and  :2  and\n                tapi.merchant_number =  :3  and\n                tp.terminal_id = tapi.terminal_id and\n                tp.product_code != 'TP' and\n                ( 'passall' =  :4  or tapi.trident_tran_id like  :5  ) and\n                ( 'passall' =  :6  or tapi.card_number like  :7  ) and\n                ( 'passall' =  :8  or tapi.auth_code like  :9  ) and\n                ( 'passall' =  :10  or tapi.transaction_amount between  :11  and  :12  ) and\n                ( 'passall' =  :13  or tapi.reference_number like  :14  ) and\n                ( 'passall' =  :15  or lower(tapi.purchase_id) like  :16  ) and\n                ( 'passall' =  :17  or lower(tapi.client_reference_number) like  :18  ) and\n                ( 'passall' =  :19  or \n                  ('allOthers' =  :20  and tapi.card_type not in('VS','MC','AM','DS','BL')) or\n                  tapi.card_type =  :21  ) and\n                pd.pos_entry_code(+) = tapi.pos_entry_mode and\n                cc.currency_code(+) = nvl(tapi.currency_code,'840')\n                and exists\n                (\n                  select gm.merchant_number\n                  from   organization   o,\n                         group_merchant gm\n                  where  o.org_group =  :22  and\n                         gm.org_num = o.org_num and\n                         gm.merchant_number = tapi.merchant_number\n                )\n        order by tapi.transaction_date, tapi.mesdb_timestamp";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1176);
   __sJT_st.setDate(2,__sJT_1177);
   __sJT_st.setLong(3,MerchantNumber);
   __sJT_st.setString(4,FindTranId);
   __sJT_st.setString(5,FindTranId);
   __sJT_st.setString(6,FindCardNumber);
   __sJT_st.setString(7,FindCardNumber);
   __sJT_st.setString(8,FindAuthCode);
   __sJT_st.setString(9,FindAuthCode);
   __sJT_st.setString(10,FindTranAmount);
   __sJT_st.setDouble(11,FindAmountFrom);
   __sJT_st.setDouble(12,FindAmountTo);
   __sJT_st.setString(13,FindRefNumber);
   __sJT_st.setString(14,FindRefNumber);
   __sJT_st.setString(15,FindPurchaseId);
   __sJT_st.setString(16,FindPurchaseId);
   __sJT_st.setString(17,FindClientRefNumber);
   __sJT_st.setString(18,FindClientRefNumber);
   __sJT_st.setString(19,FindCardType);
   __sJT_st.setString(20,FindCardType);
   __sJT_st.setString(21,FindCardType);
   __sJT_st.setLong(22,__sJT_1178);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:651^7*/
      
      // find transaction does not run if params are bad
      if ( it != null )
      {
        boolean addRecord;
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          addRecord = true;
          if( FindCardNumberFull )
          {
            String cardNumberFull = TridentTools.getCardNumberFull(resultSet.getString("card_number_enc"));
            if( cardNumberFull.indexOf(CardNumber) == -1 )
            {
              addRecord  = false;
            }
          }

          if( addRecord )
          {
            ReportMerchId = resultSet.getLong("merchant_number");
            ReportRows.addElement( new RowData( resultSet ) );
            ReportTotals.add(resultSet.getString("data_source"), resultSet.getDouble("tran_amount"));
          }
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadBOData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadChargebacks()
  {    
    ResultSetIterator    it               = null;
    ResultSet            resultSet        = null;
    
    ReportMerchId = 0L;
    ReportRows.clear();

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:700^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.merchant_number            as merchant_number,
//                  cb.tran_date                  as tran_date,
//                  cb.card_number                as card_number,
//                  cb.card_number_enc            as card_number_enc,
//                  cb.card_type                  as card_type,
//                  cb.auth_code                  as auth_code,
//                  cb.tran_amount                as tran_amount,
//                  cb.reference_number           as reference_number,
//                  cb.dt_purchase_id             as purchase_id,
//                  cb.trident_tran_id            as trident_tran_id,
//                  cb.dt_batch_date              as batch_date,
//                  cb.dt_batch_number            as batch_number,
//                  cb.reason_code                as reason_code,
//                  cb.incoming_date              as incoming_date,
//                  cb.cb_load_sec                as control_number,
//                  cba.action_date               as action_date,
//                  cbc.action_code               as action_code,
//                  '0'                           as rec_id,
//                  '0'                           as auth_amount,
//                  null                          as load_file_id,
//                  null                          as pos_entry_mode,
//                  null                          as terminal_id,
//                  'network_chargebacks'         as data_source
//          from    network_chargebacks           cb,
//                  network_chargeback_activity   cba,
//                  chargeback_action_codes       cbc
//          where   cb.incoming_date between :getReportDateBegin() and :getReportDateEnd() and
//                  cb.merchant_number = :MerchantNumber and
//                  ( 'passall' = :FindTranId or cb.trident_tran_id like :FindTranId ) and
//                  ( 'passall' = :FindCardNumber or cb.card_number like :FindCardNumber ) and
//                  ( 'passall' = :FindAuthCode or cb.auth_code like :FindAuthCode ) and
//                  ( 'passall' = :FindTranAmount or cb.tran_amount between :FindAmountFrom and :FindAmountTo ) and
//                  ( 'passall' = :FindRefNumber or cb.reference_number like :FindRefNumber ) and
//                  ( 'passall' = :FindPurchaseId or lower(cb.dt_purchase_id) like :FindPurchaseId ) and
//                  ( 'passall' = :FindClientRefNumber ) and
//                  ( 'passall' = :FindCardType
//                     or ('allOthers' = :FindCardType and cb.card_type not in('VS','MC','AM','DS','BL'))
//                     or cb.card_type = :FindCardType 
//                  ) and
//                  cba.cb_load_sec(+) = cb.cb_load_sec and
//                  cbc.short_action_code(+) = cba.action_code
//                  and exists
//                  (
//                    select gm.merchant_number
//                    from   organization   o,
//                           group_merchant gm
//                    where  o.org_group = :getReportHierarchyNodeDefault() and
//                           gm.org_num = o.org_num and
//                           gm.merchant_number = cb.merchant_number
//                  )
//          order by cb.incoming_date, cb.card_number, cba.action_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1179 = getReportDateBegin();
 java.sql.Date __sJT_1180 = getReportDateEnd();
 long __sJT_1181 = getReportHierarchyNodeDefault();
  try {
   String theSqlTS = "select  cb.merchant_number            as merchant_number,\n                cb.tran_date                  as tran_date,\n                cb.card_number                as card_number,\n                cb.card_number_enc            as card_number_enc,\n                cb.card_type                  as card_type,\n                cb.auth_code                  as auth_code,\n                cb.tran_amount                as tran_amount,\n                cb.reference_number           as reference_number,\n                cb.dt_purchase_id             as purchase_id,\n                cb.trident_tran_id            as trident_tran_id,\n                cb.dt_batch_date              as batch_date,\n                cb.dt_batch_number            as batch_number,\n                cb.reason_code                as reason_code,\n                cb.incoming_date              as incoming_date,\n                cb.cb_load_sec                as control_number,\n                cba.action_date               as action_date,\n                cbc.action_code               as action_code,\n                '0'                           as rec_id,\n                '0'                           as auth_amount,\n                null                          as load_file_id,\n                null                          as pos_entry_mode,\n                null                          as terminal_id,\n                'network_chargebacks'         as data_source\n        from    network_chargebacks           cb,\n                network_chargeback_activity   cba,\n                chargeback_action_codes       cbc\n        where   cb.incoming_date between  :1  and  :2  and\n                cb.merchant_number =  :3  and\n                ( 'passall' =  :4  or cb.trident_tran_id like  :5  ) and\n                ( 'passall' =  :6  or cb.card_number like  :7  ) and\n                ( 'passall' =  :8  or cb.auth_code like  :9  ) and\n                ( 'passall' =  :10  or cb.tran_amount between  :11  and  :12  ) and\n                ( 'passall' =  :13  or cb.reference_number like  :14  ) and\n                ( 'passall' =  :15  or lower(cb.dt_purchase_id) like  :16  ) and\n                ( 'passall' =  :17  ) and\n                ( 'passall' =  :18 \n                   or ('allOthers' =  :19  and cb.card_type not in('VS','MC','AM','DS','BL'))\n                   or cb.card_type =  :20  \n                ) and\n                cba.cb_load_sec(+) = cb.cb_load_sec and\n                cbc.short_action_code(+) = cba.action_code\n                and exists\n                (\n                  select gm.merchant_number\n                  from   organization   o,\n                         group_merchant gm\n                  where  o.org_group =  :21  and\n                         gm.org_num = o.org_num and\n                         gm.merchant_number = cb.merchant_number\n                )\n        order by cb.incoming_date, cb.card_number, cba.action_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1179);
   __sJT_st.setDate(2,__sJT_1180);
   __sJT_st.setLong(3,MerchantNumber);
   __sJT_st.setString(4,FindTranId);
   __sJT_st.setString(5,FindTranId);
   __sJT_st.setString(6,FindCardNumber);
   __sJT_st.setString(7,FindCardNumber);
   __sJT_st.setString(8,FindAuthCode);
   __sJT_st.setString(9,FindAuthCode);
   __sJT_st.setString(10,FindTranAmount);
   __sJT_st.setDouble(11,FindAmountFrom);
   __sJT_st.setDouble(12,FindAmountTo);
   __sJT_st.setString(13,FindRefNumber);
   __sJT_st.setString(14,FindRefNumber);
   __sJT_st.setString(15,FindPurchaseId);
   __sJT_st.setString(16,FindPurchaseId);
   __sJT_st.setString(17,FindClientRefNumber);
   __sJT_st.setString(18,FindCardType);
   __sJT_st.setString(19,FindCardType);
   __sJT_st.setString(20,FindCardType);
   __sJT_st.setLong(21,__sJT_1181);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:753^7*/
      
      // find transaction does not run if params are bad
      if ( it != null )
      {
        boolean addRecord;
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          addRecord = true;
          if( FindCardNumberFull )
          {
            String cardNumberFull = TridentTools.getCardNumberFull(resultSet.getString("card_number_enc"));
            if( cardNumberFull.indexOf(CardNumber) == -1 )
            {
              addRecord  = false;
            }
          }

          if( addRecord )
          {
            ReportMerchId = resultSet.getLong("merchant_number");
            ReportRows.addElement( new RowData( resultSet ) );
            ReportTotals.add(resultSet.getString("data_source"), resultSet.getDouble("tran_amount"));
          }
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadChargebacks()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadTelePayData()
  {
    ResultSetIterator    it               = null;
    ResultSet            resultSet        = null;
    
    ReportMerchId = 0L;
    ReportRows.clear();
    
    try
    {            
      /*@lineinfo:generated-code*//*@lineinfo:802^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      ordered
//                      index(tapi idx_trident_capture_api)
//                   */
//                  tapi.rec_id                    as rec_id,
//                  tapi.merchant_number           as merchant_number,
//                  tapi.terminal_id               as terminal_id,
//                  tapi.auth_amount               as auth_amount,
//                  tapi.transaction_amount * decode(tapi.debit_credit_indicator,'C',-1,1)
//                                                 as tran_amount,
//                  decode( tapi.batch_id, null, '000000', 'not000000' )
//                                                 as load_file_id,
//                  tapi.auth_date                 as auth_date,
//                  tapi.auth_code                 as auth_code,
//                  tapi.batch_date                as batch_date,
//                  tapi.batch_number              as batch_number,
//                  tapi.card_number               as card_number,
//                  tapi.card_number_enc           as card_number_enc,
//                  tapi.card_type                 as card_type,
//                  nvl(pd.pos_entry_desc,tapi.pos_entry_mode )
//                                                 as pos_entry_mode,
//                  nvl(tapi.purchase_id,' ')      as purchase_id,
//                  tapi.auth_ref_num              as reference_number,
//                  tapi.client_reference_number   as client_reference_number,
//                  tapi.transaction_date          as tran_date,
//                  tapi.transaction_type          as tran_type,
//                  tapi.trident_tran_id           as trident_tran_id,
//                  '0'                            as merchant_batch_number,
//                  'v_auth'                       as data_source,
//                  cc.currency_code_alpha         as cc_alpha
//          from    trident_capture_api   tapi,
//                  trident_profile       tp,
//                  pos_entry_mode_desc   pd,
//                  currency_codes        cc
//          where   tapi.transaction_date between :getReportDateBegin() and :getReportDateEnd() and
//                  tapi.merchant_number = :MerchantNumber and
//                  tp.terminal_id = tapi.terminal_id and
//                  tp.product_code = 'TP' and
//                  ( 'passall' = :FindTranId or tapi.trident_tran_id like :FindTranId ) and
//                  ( 'passall' = :FindCardNumber or tapi.card_number like :FindCardNumber ) and
//                  ( 'passall' = :FindAuthCode or tapi.auth_code like :FindAuthCode ) and
//                  ( 'passall' = :FindTranAmount or tapi.transaction_amount between :FindAmountFrom and :FindAmountTo ) and
//                  ( 'passall' = :FindRefNumber or tapi.auth_ref_num like :FindRefNumber ) and
//                  ( 'passall' = :FindClientRefNumber or lower(tapi.client_reference_number) like :FindClientRefNumber ) and
//                  ( 'passall' = :FindPurchaseId or lower(tapi.purchase_id) like :FindPurchaseId ) and
//                  ( 'passall' = :FindCardType or
//                    ('allOthers' = :FindCardType and tapi.card_type not in('VS','MC','AM','DS','BL')) or
//                    tapi.card_type = :FindCardType ) and
//                  pd.pos_entry_code(+) = tapi.pos_entry_mode and
//                  cc.currency_code(+) = nvl(tapi.currency_code,'840')
//                  and exists
//                  (
//                    select gm.merchant_number
//                    from   organization   o,
//                           group_merchant gm
//                    where  o.org_group = :getReportHierarchyNodeDefault() and
//                           gm.org_num = o.org_num and
//                           gm.merchant_number = tapi.merchant_number
//                  )
//          order by tapi.transaction_date, tapi.mesdb_timestamp
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1182 = getReportDateBegin();
 java.sql.Date __sJT_1183 = getReportDateEnd();
 long __sJT_1184 = getReportHierarchyNodeDefault();
  try {
   String theSqlTS = "select  /*+ \n                    ordered\n                    index(tapi idx_trident_capture_api)\n                 */\n                tapi.rec_id                    as rec_id,\n                tapi.merchant_number           as merchant_number,\n                tapi.terminal_id               as terminal_id,\n                tapi.auth_amount               as auth_amount,\n                tapi.transaction_amount * decode(tapi.debit_credit_indicator,'C',-1,1)\n                                               as tran_amount,\n                decode( tapi.batch_id, null, '000000', 'not000000' )\n                                               as load_file_id,\n                tapi.auth_date                 as auth_date,\n                tapi.auth_code                 as auth_code,\n                tapi.batch_date                as batch_date,\n                tapi.batch_number              as batch_number,\n                tapi.card_number               as card_number,\n                tapi.card_number_enc           as card_number_enc,\n                tapi.card_type                 as card_type,\n                nvl(pd.pos_entry_desc,tapi.pos_entry_mode )\n                                               as pos_entry_mode,\n                nvl(tapi.purchase_id,' ')      as purchase_id,\n                tapi.auth_ref_num              as reference_number,\n                tapi.client_reference_number   as client_reference_number,\n                tapi.transaction_date          as tran_date,\n                tapi.transaction_type          as tran_type,\n                tapi.trident_tran_id           as trident_tran_id,\n                '0'                            as merchant_batch_number,\n                'v_auth'                       as data_source,\n                cc.currency_code_alpha         as cc_alpha\n        from    trident_capture_api   tapi,\n                trident_profile       tp,\n                pos_entry_mode_desc   pd,\n                currency_codes        cc\n        where   tapi.transaction_date between  :1  and  :2  and\n                tapi.merchant_number =  :3  and\n                tp.terminal_id = tapi.terminal_id and\n                tp.product_code = 'TP' and\n                ( 'passall' =  :4  or tapi.trident_tran_id like  :5  ) and\n                ( 'passall' =  :6  or tapi.card_number like  :7  ) and\n                ( 'passall' =  :8  or tapi.auth_code like  :9  ) and\n                ( 'passall' =  :10  or tapi.transaction_amount between  :11  and  :12  ) and\n                ( 'passall' =  :13  or tapi.auth_ref_num like  :14  ) and\n                ( 'passall' =  :15  or lower(tapi.client_reference_number) like  :16  ) and\n                ( 'passall' =  :17  or lower(tapi.purchase_id) like  :18  ) and\n                ( 'passall' =  :19  or\n                  ('allOthers' =  :20  and tapi.card_type not in('VS','MC','AM','DS','BL')) or\n                  tapi.card_type =  :21  ) and\n                pd.pos_entry_code(+) = tapi.pos_entry_mode and\n                cc.currency_code(+) = nvl(tapi.currency_code,'840')\n                and exists\n                (\n                  select gm.merchant_number\n                  from   organization   o,\n                         group_merchant gm\n                  where  o.org_group =  :22  and\n                         gm.org_num = o.org_num and\n                         gm.merchant_number = tapi.merchant_number\n                )\n        order by tapi.transaction_date, tapi.mesdb_timestamp";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1182);
   __sJT_st.setDate(2,__sJT_1183);
   __sJT_st.setLong(3,MerchantNumber);
   __sJT_st.setString(4,FindTranId);
   __sJT_st.setString(5,FindTranId);
   __sJT_st.setString(6,FindCardNumber);
   __sJT_st.setString(7,FindCardNumber);
   __sJT_st.setString(8,FindAuthCode);
   __sJT_st.setString(9,FindAuthCode);
   __sJT_st.setString(10,FindTranAmount);
   __sJT_st.setDouble(11,FindAmountFrom);
   __sJT_st.setDouble(12,FindAmountTo);
   __sJT_st.setString(13,FindRefNumber);
   __sJT_st.setString(14,FindRefNumber);
   __sJT_st.setString(15,FindClientRefNumber);
   __sJT_st.setString(16,FindClientRefNumber);
   __sJT_st.setString(17,FindPurchaseId);
   __sJT_st.setString(18,FindPurchaseId);
   __sJT_st.setString(19,FindCardType);
   __sJT_st.setString(20,FindCardType);
   __sJT_st.setString(21,FindCardType);
   __sJT_st.setLong(22,__sJT_1184);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:864^7*/
      
      // find transaction does not run if params are bad
      if ( it != null )
      {
        boolean addRecord;
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          addRecord = true;
          if( FindCardNumberFull )
          {
            String cardNumberFull = TridentTools.getCardNumberFull(resultSet.getString("card_number_enc"));
            if( cardNumberFull.indexOf(CardNumber) == -1 )
            {
              addRecord  = false;
            }
          }

          if( addRecord )
          {
            ReportMerchId = resultSet.getLong("merchant_number");
            ReportRows.addElement( new RowData( resultSet ) );
            ReportTotals.add(resultSet.getString("data_source"), resultSet.getDouble("tran_amount"));
          }
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadTelePayData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadFxIcData()
  {
    ResultSetIterator    it               = null;
    ResultSet            resultSet        = null;

    ReportMerchId = 0L;
    ReportRows.clear();
    
    try
    {     
      /*@lineinfo:generated-code*//*@lineinfo:913^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  td.merchant_number               as merchant_number,
//                  td.orig_amount * decode(td.debit_credit_indicator,'C',-1,1)
//                                                   as tran_amount,
//                  td.fx_amount_base * decode(td.debit_credit_indicator,'C',-1,1)
//                                                   as fx_amount,
//                  td.transaction_date              as tran_date,
//                  td.settlement_date               as batch_date,
//                  td.card_number                   as card_number,
//                  td.card_number_enc               as card_number_enc,
//                  td.card_type_id                  as card_type,
//                  td.purchase_id                   as purchase_id,
//                  (nvl(td.usd_discount_fee,0)*-1)  as ic_due,
//                  td.reference_number              as reference_number,
//                  td.client_reference_number       as client_reference_number,
//                  td.orig_currency_code            as currency_code,
//                  '0'                              as rec_id,
//                  '0'                              as auth_amount,
//                  '0'                              as ic_rate,
//                  null                             as auth_code,
//                  null                             as terminal_id,
//                  null                             as load_file_id,
//                  'payvision_api'                  as data_source,
//                  nvl(emd.pos_entry_desc,td.pos_entry_mode) as pos_entry_mode
//          from    payvision_api_tran_detail   td,
//                  pos_entry_mode_desc         emd
//          where   td.settlement_date between :getReportDateBegin() and :getReportDateEnd() and
//                  ( (0 = :MerchantNumber and td.trident_tran_id = :getData("tranId")) or
//                    (td.merchant_number = :MerchantNumber and ('passall' = :FindTranId or td.trident_tran_id like :FindTranId))
//                  ) and
//                  ( 'passall' = :FindAuthCode ) and
//                  ( 'passall' = :FindCardNumber or td.card_number like :FindCardNumber)  and
//                  ( 'passall' = :FindTranAmount or td.orig_amount between :FindAmountFrom and :FindAmountTo ) and
//                  ( 'passall' = :FindRefNumber or td.reference_number like :FindRefNumber ) and
//                  ( 'passall' = :FindPurchaseId or lower(td.purchase_id) like :FindPurchaseId ) and
//                  ( 'passall' = :FindCardType or
//                    decode(lower(td.card_type_id),'visa','VS','visaelectron','VS',
//                           'switch','VS','eurocardmastercard','MC',td.card_id) = :FindCardType or
//                    ('allOthers' = :FindCardType and
//                     lower(td.card_type_id) not in ('visa','visaelectron','switch','eurocardmastercard')) ) and
//                  ( 'passall' = :FindClientRefNumber or lower(td.client_reference_number) like :FindClientRefNumber ) and
//                  emd.pos_entry_code(+) = td.pos_entry_mode
//                  and exists
//                  (
//                    select gm.merchant_number
//                    from   organization   o,
//                           group_merchant gm
//                    where  o.org_group = :getReportHierarchyNodeDefault() and
//                           gm.org_num = o.org_num and
//                           gm.merchant_number = td.merchant_number
//                  )
//          order by td.settlement_date, td.transaction_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1185 = getReportDateBegin();
 java.sql.Date __sJT_1186 = getReportDateEnd();
 String __sJT_1187 = getData("tranId");
 long __sJT_1188 = getReportHierarchyNodeDefault();
  try {
   String theSqlTS = "select  td.merchant_number               as merchant_number,\n                td.orig_amount * decode(td.debit_credit_indicator,'C',-1,1)\n                                                 as tran_amount,\n                td.fx_amount_base * decode(td.debit_credit_indicator,'C',-1,1)\n                                                 as fx_amount,\n                td.transaction_date              as tran_date,\n                td.settlement_date               as batch_date,\n                td.card_number                   as card_number,\n                td.card_number_enc               as card_number_enc,\n                td.card_type_id                  as card_type,\n                td.purchase_id                   as purchase_id,\n                (nvl(td.usd_discount_fee,0)*-1)  as ic_due,\n                td.reference_number              as reference_number,\n                td.client_reference_number       as client_reference_number,\n                td.orig_currency_code            as currency_code,\n                '0'                              as rec_id,\n                '0'                              as auth_amount,\n                '0'                              as ic_rate,\n                null                             as auth_code,\n                null                             as terminal_id,\n                null                             as load_file_id,\n                'payvision_api'                  as data_source,\n                nvl(emd.pos_entry_desc,td.pos_entry_mode) as pos_entry_mode\n        from    payvision_api_tran_detail   td,\n                pos_entry_mode_desc         emd\n        where   td.settlement_date between  :1  and  :2  and\n                ( (0 =  :3  and td.trident_tran_id =  :4 ) or\n                  (td.merchant_number =  :5  and ('passall' =  :6  or td.trident_tran_id like  :7 ))\n                ) and\n                ( 'passall' =  :8  ) and\n                ( 'passall' =  :9  or td.card_number like  :10 )  and\n                ( 'passall' =  :11  or td.orig_amount between  :12  and  :13  ) and\n                ( 'passall' =  :14  or td.reference_number like  :15  ) and\n                ( 'passall' =  :16  or lower(td.purchase_id) like  :17  ) and\n                ( 'passall' =  :18  or\n                  decode(lower(td.card_type_id),'visa','VS','visaelectron','VS',\n                         'switch','VS','eurocardmastercard','MC',td.card_id) =  :19  or\n                  ('allOthers' =  :20  and\n                   lower(td.card_type_id) not in ('visa','visaelectron','switch','eurocardmastercard')) ) and\n                ( 'passall' =  :21  or lower(td.client_reference_number) like  :22  ) and\n                emd.pos_entry_code(+) = td.pos_entry_mode\n                and exists\n                (\n                  select gm.merchant_number\n                  from   organization   o,\n                         group_merchant gm\n                  where  o.org_group =  :23  and\n                         gm.org_num = o.org_num and\n                         gm.merchant_number = td.merchant_number\n                )\n        order by td.settlement_date, td.transaction_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1185);
   __sJT_st.setDate(2,__sJT_1186);
   __sJT_st.setLong(3,MerchantNumber);
   __sJT_st.setString(4,__sJT_1187);
   __sJT_st.setLong(5,MerchantNumber);
   __sJT_st.setString(6,FindTranId);
   __sJT_st.setString(7,FindTranId);
   __sJT_st.setString(8,FindAuthCode);
   __sJT_st.setString(9,FindCardNumber);
   __sJT_st.setString(10,FindCardNumber);
   __sJT_st.setString(11,FindTranAmount);
   __sJT_st.setDouble(12,FindAmountFrom);
   __sJT_st.setDouble(13,FindAmountTo);
   __sJT_st.setString(14,FindRefNumber);
   __sJT_st.setString(15,FindRefNumber);
   __sJT_st.setString(16,FindPurchaseId);
   __sJT_st.setString(17,FindPurchaseId);
   __sJT_st.setString(18,FindCardType);
   __sJT_st.setString(19,FindCardType);
   __sJT_st.setString(20,FindCardType);
   __sJT_st.setString(21,FindClientRefNumber);
   __sJT_st.setString(22,FindClientRefNumber);
   __sJT_st.setLong(23,__sJT_1188);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:966^7*/
      
      // find transaction does not run if params are bad
      if ( it != null )
      {
        boolean addRecord;
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          addRecord = true;
          if( FindCardNumberFull )
          {
            String cardNumberFull = TridentTools.getCardNumberFull(resultSet.getString("card_number_enc"));
            if( cardNumberFull.indexOf(CardNumber) == -1 )
            {
              addRecord  = false;
            }
          }

          if( addRecord )
          {
            ReportMerchId = resultSet.getLong("merchant_number");
            ReportRows.addElement( new RowData( resultSet ) );
            ReportTotals.add(resultSet.getString("data_source"), resultSet.getDouble("ic_due"));
          }
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadFxIcData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }

  }

  public void loadFxBatchData()
  {
    ResultSetIterator    it               = null;
    ResultSet            resultSet        = null;

    ReportMerchId = 0L;
    ReportRows.clear();
    
    try
    {            
      /*@lineinfo:generated-code*//*@lineinfo:1016^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index(tapi idx_tapi_merch_batch_date) */
//                  tapi.rec_id                    as rec_id,
//                  tapi.merchant_number           as merchant_number,
//                  tapi.terminal_id               as terminal_id,
//                  tapi.auth_amount               as auth_amount,
//                  tapi.transaction_amount * decode(tapi.debit_credit_indicator,'C',-1,1)
//                                                 as tran_amount,
//                  tapi.fx_amount_base * decode(tapi.debit_credit_indicator,'C',-1,1)
//                                                 as fx_amount,
//                  decode( tapi.batch_id, null, '000000', 'not000000' )
//                                                 as load_file_id,
//                  tapi.auth_date                 as auth_date,
//                  tapi.auth_code                 as auth_code,
//                  tapi.batch_date                as batch_date,
//                  tapi.batch_number              as batch_number,
//                  tapi.card_number               as card_number,
//                  tapi.card_number_enc           as card_number_enc,
//                  tapi.card_type                 as card_type,
//                  nvl(pd.pos_entry_desc, tapi.pos_entry_mode)
//                                                 as pos_entry_mode,
//                  nvl(tapi.purchase_id,' ')      as purchase_id,
//                  tapi.reference_number          as reference_number,
//                  tapi.client_reference_number   as client_reference_number,
//                  tapi.transaction_date          as tran_date,
//                  'tc_api_fx'                    as data_source,
//                  cc.currency_code_alpha         as cc_alpha
//          from    trident_capture_api   tapi,
//                  pos_entry_mode_desc   pd,
//                  currency_codes        cc
//          where   tapi.batch_date between :getReportDateBegin() and :getReportDateEnd() and
//                  tapi.merchant_number = :MerchantNumber and
//                  nvl(tapi.fx_amount_base,0) > 0 and
//                  ( 'passall' = :FindTranId or tapi.trident_tran_id like :FindTranId ) and
//                  ( 'passall' = :FindCardNumber or tapi.card_number like :FindCardNumber ) and
//                  ( 'passall' = :FindAuthCode or tapi.auth_code like :FindAuthCode ) and
//                  ( 'passall' = :FindTranAmount or tapi.transaction_amount between :FindAmountFrom and :FindAmountTo ) and
//                  ( 'passall' = :FindRefNumber or tapi.reference_number like :FindRefNumber ) and
//                  ( 'passall' = :FindPurchaseId or lower(tapi.purchase_id) like :FindPurchaseId ) and
//                  ( 'passall' = :FindCardType or
//                    ('allOthers' = :FindCardType and tapi.card_type not in('VS','MC','AM','DS','BL')) or
//                    tapi.card_type = :FindCardType ) and
//                  ( 'passall' = :FindClientRefNumber or lower(tapi.client_reference_number) like :FindClientRefNumber ) and
//                  pd.pos_entry_code(+) = tapi.pos_entry_mode and
//                  cc.currency_code(+) = nvl(tapi.currency_code,'840')
//                  and exists
//                  (
//                    select gm.merchant_number
//                    from   organization   o,
//                           group_merchant gm
//                    where  o.org_group = :getReportHierarchyNodeDefault() and
//                           gm.org_num = o.org_num and
//                           gm.merchant_number = tapi.merchant_number
//                  )
//          order by tapi.batch_date, tapi.batch_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1189 = getReportDateBegin();
 java.sql.Date __sJT_1190 = getReportDateEnd();
 long __sJT_1191 = getReportHierarchyNodeDefault();
  try {
   String theSqlTS = "select  /*+ index(tapi idx_tapi_merch_batch_date) */\n                tapi.rec_id                    as rec_id,\n                tapi.merchant_number           as merchant_number,\n                tapi.terminal_id               as terminal_id,\n                tapi.auth_amount               as auth_amount,\n                tapi.transaction_amount * decode(tapi.debit_credit_indicator,'C',-1,1)\n                                               as tran_amount,\n                tapi.fx_amount_base * decode(tapi.debit_credit_indicator,'C',-1,1)\n                                               as fx_amount,\n                decode( tapi.batch_id, null, '000000', 'not000000' )\n                                               as load_file_id,\n                tapi.auth_date                 as auth_date,\n                tapi.auth_code                 as auth_code,\n                tapi.batch_date                as batch_date,\n                tapi.batch_number              as batch_number,\n                tapi.card_number               as card_number,\n                tapi.card_number_enc           as card_number_enc,\n                tapi.card_type                 as card_type,\n                nvl(pd.pos_entry_desc, tapi.pos_entry_mode)\n                                               as pos_entry_mode,\n                nvl(tapi.purchase_id,' ')      as purchase_id,\n                tapi.reference_number          as reference_number,\n                tapi.client_reference_number   as client_reference_number,\n                tapi.transaction_date          as tran_date,\n                'tc_api_fx'                    as data_source,\n                cc.currency_code_alpha         as cc_alpha\n        from    trident_capture_api   tapi,\n                pos_entry_mode_desc   pd,\n                currency_codes        cc\n        where   tapi.batch_date between  :1  and  :2  and\n                tapi.merchant_number =  :3  and\n                nvl(tapi.fx_amount_base,0) > 0 and\n                ( 'passall' =  :4  or tapi.trident_tran_id like  :5  ) and\n                ( 'passall' =  :6  or tapi.card_number like  :7  ) and\n                ( 'passall' =  :8  or tapi.auth_code like  :9  ) and\n                ( 'passall' =  :10  or tapi.transaction_amount between  :11  and  :12  ) and\n                ( 'passall' =  :13  or tapi.reference_number like  :14  ) and\n                ( 'passall' =  :15  or lower(tapi.purchase_id) like  :16  ) and\n                ( 'passall' =  :17  or\n                  ('allOthers' =  :18  and tapi.card_type not in('VS','MC','AM','DS','BL')) or\n                  tapi.card_type =  :19  ) and\n                ( 'passall' =  :20  or lower(tapi.client_reference_number) like  :21  ) and\n                pd.pos_entry_code(+) = tapi.pos_entry_mode and\n                cc.currency_code(+) = nvl(tapi.currency_code,'840')\n                and exists\n                (\n                  select gm.merchant_number\n                  from   organization   o,\n                         group_merchant gm\n                  where  o.org_group =  :22  and\n                         gm.org_num = o.org_num and\n                         gm.merchant_number = tapi.merchant_number\n                )\n        order by tapi.batch_date, tapi.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1189);
   __sJT_st.setDate(2,__sJT_1190);
   __sJT_st.setLong(3,MerchantNumber);
   __sJT_st.setString(4,FindTranId);
   __sJT_st.setString(5,FindTranId);
   __sJT_st.setString(6,FindCardNumber);
   __sJT_st.setString(7,FindCardNumber);
   __sJT_st.setString(8,FindAuthCode);
   __sJT_st.setString(9,FindAuthCode);
   __sJT_st.setString(10,FindTranAmount);
   __sJT_st.setDouble(11,FindAmountFrom);
   __sJT_st.setDouble(12,FindAmountTo);
   __sJT_st.setString(13,FindRefNumber);
   __sJT_st.setString(14,FindRefNumber);
   __sJT_st.setString(15,FindPurchaseId);
   __sJT_st.setString(16,FindPurchaseId);
   __sJT_st.setString(17,FindCardType);
   __sJT_st.setString(18,FindCardType);
   __sJT_st.setString(19,FindCardType);
   __sJT_st.setString(20,FindClientRefNumber);
   __sJT_st.setString(21,FindClientRefNumber);
   __sJT_st.setLong(22,__sJT_1191);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1072^7*/
      
      // find transaction does not run if params are bad
      if ( it != null )
      {
        boolean addRecord;
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          addRecord = true;
          if( FindCardNumberFull )
          {
            String cardNumberFull = TridentTools.getCardNumberFull(resultSet.getString("card_number_enc"));
            if( cardNumberFull.indexOf(CardNumber) == -1 )
            {
              addRecord  = false;
            }
          }

          if( addRecord )
          {
            ReportMerchId = resultSet.getLong("merchant_number");
            ReportRows.addElement( new RowData( resultSet ) );
            ReportTotals.add(resultSet.getString("data_source"), resultSet.getDouble("tran_amount"));
          }
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadFxBatchData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadExtBatchData()
  {
    ResultSetIterator    it               = null;
    ResultSet            resultSet        = null;

    ReportMerchId = 0L;
    ReportRows.clear();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1121^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_number                as merchant_number,
//                  dt.reference_number               as reference_number,
//                  dt.card_type                      as card_type,
//                  dt.auth_code                      as auth_code,
//                  dt.transaction_amount * decode(dt.debit_credit_indicator,'C',-1,1)
//                                                    as tran_amount,
//                  dt.transaction_date               as tran_date,
//                  nvl(pd.pos_entry_desc,dt.pos_entry_mode)
//                                                    as pos_entry_mode,
//                  dt.card_number                    as card_number,
//                  dt.card_number_enc                as card_number_enc,
//                  dt.debit_credit_indicator         as debit_credit_ind,
//                  dt.batch_number                   as batch_number,
//                  dt.batch_date                     as batch_date,
//                  dt.purchase_id                    as purchase_id,
//                  decode( instr(lower(dt.load_filename),'gps'),0,dt.terminal_number,-1)
//                                                    as terminal_id,
//                  decode( instr(lower(dt.load_filename),'gps'),0, dt.merchant_batch_number,-1)
//                                                    as merchant_batch_number,
//                  'dd_file_ext_dt'                  as data_source,
//                  '0'                               as auth_amount,
//                  ' '                               as load_file_id,
//                  '0'                               as rec_id
//          from    daily_detail_file_ext_dt  dt,
//                  pos_entry_mode_desc       pd
//          where   dt.batch_date between :getReportDateBegin() and :getReportDateEnd() and
//                  ( (0 = :MerchantNumber and dt.trident_tran_id = :getData("tranId")) or
//                    (dt.merchant_number = :MerchantNumber and ('passall' = :FindTranId or dt.trident_tran_id like :FindTranId))
//                  ) and
//                  ( 'passall' = :FindClientRefNumber ) and
//                  ( 'passall' = :FindCardNumber or dt.card_number like :FindCardNumber ) and
//                  ( 'passall' = :FindAuthCode or dt.auth_code like :FindAuthCode ) and
//                  ( 'passall' = :FindRefNumber or dt.reference_number like :FindRefNumber ) and
//                  ( 'passall' = :FindTranAmount or dt.transaction_amount between :FindAmountFrom and :FindAmountTo ) and
//                  ( 'passall' = :FindPurchaseId or lower(dt.purchase_id) like :FindPurchaseId ) and
//                  ( 'passall' = :FindCardType or
//                    dt.card_type = :FindCardType or
//                    ('allOthers' = :FindCardType and dt.card_type not in ('VS','MC','AM','DS','BL')) ) and
//                  pd.pos_entry_code(+) = dt.pos_entry_mode
//                  and exists
//                  (
//                    select gm.merchant_number
//                    from   organization   o,
//                           group_merchant gm
//                    where  o.org_group = :getReportHierarchyNodeDefault() and
//                           gm.org_num = o.org_num and
//                           gm.merchant_number = dt.merchant_number
//                  )
//          order by dt.batch_date, dt.batch_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1192 = getReportDateBegin();
 java.sql.Date __sJT_1193 = getReportDateEnd();
 String __sJT_1194 = getData("tranId");
 long __sJT_1195 = getReportHierarchyNodeDefault();
  try {
   String theSqlTS = "select  dt.merchant_number                as merchant_number,\n                dt.reference_number               as reference_number,\n                dt.card_type                      as card_type,\n                dt.auth_code                      as auth_code,\n                dt.transaction_amount * decode(dt.debit_credit_indicator,'C',-1,1)\n                                                  as tran_amount,\n                dt.transaction_date               as tran_date,\n                nvl(pd.pos_entry_desc,dt.pos_entry_mode)\n                                                  as pos_entry_mode,\n                dt.card_number                    as card_number,\n                dt.card_number_enc                as card_number_enc,\n                dt.debit_credit_indicator         as debit_credit_ind,\n                dt.batch_number                   as batch_number,\n                dt.batch_date                     as batch_date,\n                dt.purchase_id                    as purchase_id,\n                decode( instr(lower(dt.load_filename),'gps'),0,dt.terminal_number,-1)\n                                                  as terminal_id,\n                decode( instr(lower(dt.load_filename),'gps'),0, dt.merchant_batch_number,-1)\n                                                  as merchant_batch_number,\n                'dd_file_ext_dt'                  as data_source,\n                '0'                               as auth_amount,\n                ' '                               as load_file_id,\n                '0'                               as rec_id\n        from    daily_detail_file_ext_dt  dt,\n                pos_entry_mode_desc       pd\n        where   dt.batch_date between  :1  and  :2  and\n                ( (0 =  :3  and dt.trident_tran_id =  :4 ) or\n                  (dt.merchant_number =  :5  and ('passall' =  :6  or dt.trident_tran_id like  :7 ))\n                ) and\n                ( 'passall' =  :8  ) and\n                ( 'passall' =  :9  or dt.card_number like  :10  ) and\n                ( 'passall' =  :11  or dt.auth_code like  :12  ) and\n                ( 'passall' =  :13  or dt.reference_number like  :14  ) and\n                ( 'passall' =  :15  or dt.transaction_amount between  :16  and  :17  ) and\n                ( 'passall' =  :18  or lower(dt.purchase_id) like  :19  ) and\n                ( 'passall' =  :20  or\n                  dt.card_type =  :21  or\n                  ('allOthers' =  :22  and dt.card_type not in ('VS','MC','AM','DS','BL')) ) and\n                pd.pos_entry_code(+) = dt.pos_entry_mode\n                and exists\n                (\n                  select gm.merchant_number\n                  from   organization   o,\n                         group_merchant gm\n                  where  o.org_group =  :23  and\n                         gm.org_num = o.org_num and\n                         gm.merchant_number = dt.merchant_number\n                )\n        order by dt.batch_date, dt.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1192);
   __sJT_st.setDate(2,__sJT_1193);
   __sJT_st.setLong(3,MerchantNumber);
   __sJT_st.setString(4,__sJT_1194);
   __sJT_st.setLong(5,MerchantNumber);
   __sJT_st.setString(6,FindTranId);
   __sJT_st.setString(7,FindTranId);
   __sJT_st.setString(8,FindClientRefNumber);
   __sJT_st.setString(9,FindCardNumber);
   __sJT_st.setString(10,FindCardNumber);
   __sJT_st.setString(11,FindAuthCode);
   __sJT_st.setString(12,FindAuthCode);
   __sJT_st.setString(13,FindRefNumber);
   __sJT_st.setString(14,FindRefNumber);
   __sJT_st.setString(15,FindTranAmount);
   __sJT_st.setDouble(16,FindAmountFrom);
   __sJT_st.setDouble(17,FindAmountTo);
   __sJT_st.setString(18,FindPurchaseId);
   __sJT_st.setString(19,FindPurchaseId);
   __sJT_st.setString(20,FindCardType);
   __sJT_st.setString(21,FindCardType);
   __sJT_st.setString(22,FindCardType);
   __sJT_st.setLong(23,__sJT_1195);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1172^7*/
      
      // find transaction does not run if params are bad
      if ( it != null )
      {
        boolean addRecord;
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          addRecord = true;
          if( FindCardNumberFull )
          {
            String cardNumberFull = TridentTools.getCardNumberFull(resultSet.getString("card_number_enc"));
            if( cardNumberFull.indexOf(CardNumber) == -1 )
            {
              addRecord  = false;
            }
          }

          if( addRecord )
          {
            ReportMerchId = resultSet.getLong("merchant_number");
            ReportRows.addElement( new RowData( resultSet ) );
            ReportTotals.add(resultSet.getString("data_source"), resultSet.getDouble("tran_amount"));
          }
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadExtBatchData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadIcData()
  {
    ResultSetIterator    it               = null;
    ResultSet            resultSet        = null;
    
    ReportMerchId = 0L;
    ReportRows.clear();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1221^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index(dt idx_ddf_dt_bdate_merch_acct)
//                   */
//                  dt.merchant_account_number        as merchant_number,
//                  dt.reference_number               as reference_number, 
//                  decode( substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type )
//                                                    as card_type,
//                  dt.authorization_num              as auth_code,
//                  dt.auth_amt                       as auth_amount,
//                  dt.transaction_amount * decode(dt.debit_credit_indicator,'C',-1,1)
//                                                    as tran_amount,
//                  nvl(icd.ic_rate,0)                as ic_rate,
//                  dt.transaction_date               as tran_date,
//                  nvl(pd.pos_entry_desc,dt.pos_entry_mode)
//                                                    as pos_entry_mode,
//                  dt.cardholder_account_number      as card_number,
//                  dt.card_number_enc                as card_number_enc,
//                  dt.batch_number                   as batch_number,
//                  dt.batch_date                     as batch_date,
//                  ltrim(rtrim(dt.purchase_id))      as purchase_id,
//                  null                              as terminal_id,
//                  null                              as load_file_id,
//                  ( dt.transaction_amount * 
//                    decode(dt.debit_credit_indicator,'C',-1,1) *
//                    nvl(icd.ic_rate,0) * 0.01 )     as ic_due,
//                  '0'                               as rec_id,
//                  'dd_file_ic'                      as data_source
//          from    daily_detail_file_dt      dt, 
//                  pos_entry_mode_desc       pd,
//                  daily_detail_file_ic_desc icd
//          where   dt.batch_date between :getReportDateBegin() and :getReportDateEnd() and
//                  ( (0 = :MerchantNumber and dt.trident_tran_id = :getData("tranId")) or
//                    (dt.merchant_account_number = :MerchantNumber and ('passall' = :FindTranId or dt.trident_tran_id like :FindTranId))
//                  ) and
//                  pd.pos_entry_code(+) = dt.pos_entry_mode and
//                  icd.card_type(+) = decode(substr(dt.card_type,1,1),'M','MC','V','VS',dt.card_type) and
//                  icd.ic_code(+) = dt.submitted_interchange2 and
//                  dt.batch_date between icd.valid_date_begin(+) and icd.valid_date_end(+) and
//                  ( not (dt.pos_entry_mode = 'NA' and dt.net_deposit = 0) ) and
//                  ( 'passall' = :FindClientRefNumber ) and
//                  ( 'passall' = :FindCardNumber or dt.cardholder_account_number like :FindCardNumber ) and
//                  ( 'passall' = :FindAuthCode or dt.authorization_num like :FindAuthCode ) and
//                  ( 'passall' = :FindRefNumber or dt.reference_number like :FindRefNumber ) and
//                  ( 'passall' = :FindTranAmount or dt.transaction_amount between :FindAmountFrom and :FindAmountTo ) and
//                  ( 'passall' = :FindPurchaseId or lower(dt.purchase_id) like :FindPurchaseId ) and
//                  ( 'passall' = :FindCardType or
//                    decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type) = :FindCardType or
//                    ('allOthers' = :FindCardType and decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type)
//                                                     not in ('VS','MC','AM','DS','BL')) )
//                  and exists
//                  (
//                    select gm.merchant_number
//                    from   organization   o,
//                           group_merchant gm
//                    where  o.org_group = :getReportHierarchyNodeDefault() and
//                           gm.org_num = o.org_num and
//                           gm.merchant_number = dt.merchant_account_number
//                  )
//          order by dt.batch_date, dt.batch_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1196 = getReportDateBegin();
 java.sql.Date __sJT_1197 = getReportDateEnd();
 String __sJT_1198 = getData("tranId");
 long __sJT_1199 = getReportHierarchyNodeDefault();
  try {
   String theSqlTS = "select  /*+ \n                    index(dt idx_ddf_dt_bdate_merch_acct)\n                 */\n                dt.merchant_account_number        as merchant_number,\n                dt.reference_number               as reference_number, \n                decode( substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type )\n                                                  as card_type,\n                dt.authorization_num              as auth_code,\n                dt.auth_amt                       as auth_amount,\n                dt.transaction_amount * decode(dt.debit_credit_indicator,'C',-1,1)\n                                                  as tran_amount,\n                nvl(icd.ic_rate,0)                as ic_rate,\n                dt.transaction_date               as tran_date,\n                nvl(pd.pos_entry_desc,dt.pos_entry_mode)\n                                                  as pos_entry_mode,\n                dt.cardholder_account_number      as card_number,\n                dt.card_number_enc                as card_number_enc,\n                dt.batch_number                   as batch_number,\n                dt.batch_date                     as batch_date,\n                ltrim(rtrim(dt.purchase_id))      as purchase_id,\n                null                              as terminal_id,\n                null                              as load_file_id,\n                ( dt.transaction_amount * \n                  decode(dt.debit_credit_indicator,'C',-1,1) *\n                  nvl(icd.ic_rate,0) * 0.01 )     as ic_due,\n                '0'                               as rec_id,\n                'dd_file_ic'                      as data_source\n        from    daily_detail_file_dt      dt, \n                pos_entry_mode_desc       pd,\n                daily_detail_file_ic_desc icd\n        where   dt.batch_date between  :1  and  :2  and\n                ( (0 =  :3  and dt.trident_tran_id =  :4 ) or\n                  (dt.merchant_account_number =  :5  and ('passall' =  :6  or dt.trident_tran_id like  :7 ))\n                ) and\n                pd.pos_entry_code(+) = dt.pos_entry_mode and\n                icd.card_type(+) = decode(substr(dt.card_type,1,1),'M','MC','V','VS',dt.card_type) and\n                icd.ic_code(+) = dt.submitted_interchange2 and\n                dt.batch_date between icd.valid_date_begin(+) and icd.valid_date_end(+) and\n                ( not (dt.pos_entry_mode = 'NA' and dt.net_deposit = 0) ) and\n                ( 'passall' =  :8  ) and\n                ( 'passall' =  :9  or dt.cardholder_account_number like  :10  ) and\n                ( 'passall' =  :11  or dt.authorization_num like  :12  ) and\n                ( 'passall' =  :13  or dt.reference_number like  :14  ) and\n                ( 'passall' =  :15  or dt.transaction_amount between  :16  and  :17  ) and\n                ( 'passall' =  :18  or lower(dt.purchase_id) like  :19  ) and\n                ( 'passall' =  :20  or\n                  decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type) =  :21  or\n                  ('allOthers' =  :22  and decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type)\n                                                   not in ('VS','MC','AM','DS','BL')) )\n                and exists\n                (\n                  select gm.merchant_number\n                  from   organization   o,\n                         group_merchant gm\n                  where  o.org_group =  :23  and\n                         gm.org_num = o.org_num and\n                         gm.merchant_number = dt.merchant_account_number\n                )\n        order by dt.batch_date, dt.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1196);
   __sJT_st.setDate(2,__sJT_1197);
   __sJT_st.setLong(3,MerchantNumber);
   __sJT_st.setString(4,__sJT_1198);
   __sJT_st.setLong(5,MerchantNumber);
   __sJT_st.setString(6,FindTranId);
   __sJT_st.setString(7,FindTranId);
   __sJT_st.setString(8,FindClientRefNumber);
   __sJT_st.setString(9,FindCardNumber);
   __sJT_st.setString(10,FindCardNumber);
   __sJT_st.setString(11,FindAuthCode);
   __sJT_st.setString(12,FindAuthCode);
   __sJT_st.setString(13,FindRefNumber);
   __sJT_st.setString(14,FindRefNumber);
   __sJT_st.setString(15,FindTranAmount);
   __sJT_st.setDouble(16,FindAmountFrom);
   __sJT_st.setDouble(17,FindAmountTo);
   __sJT_st.setString(18,FindPurchaseId);
   __sJT_st.setString(19,FindPurchaseId);
   __sJT_st.setString(20,FindCardType);
   __sJT_st.setString(21,FindCardType);
   __sJT_st.setString(22,FindCardType);
   __sJT_st.setLong(23,__sJT_1199);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1282^7*/
      
      // find transaction does not run if params are bad
      if ( it != null )
      {
        boolean addRecord;
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          addRecord = true;
          if( FindCardNumberFull )
          {
            String cardNumberFull = TridentTools.getCardNumberFull(resultSet.getString("card_number_enc"));
            if( cardNumberFull.indexOf(CardNumber) == -1 )
            {
              addRecord  = false;
            }
          }

          if( addRecord )
          {
            ReportMerchId = resultSet.getLong("merchant_number");
            ReportRows.addElement( new RowData( resultSet ) );
            ReportTotals.add(resultSet.getString("data_source"), resultSet.getDouble("ic_due"));
          }
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadIcData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSettledData()
  {
    ResultSetIterator    it               = null;
    ResultSet            resultSet        = null;

    ReportMerchId = 0L;
    ReportRows.clear();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1331^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index(dt idx_ddf_dt_bdate_merch_acct)
//                   */
//                  dt.merchant_account_number        as merchant_number,
//                  dt.reference_number               as reference_number,
//                  decode( substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type )
//                                                    as card_type,
//                  dt.authorization_num              as auth_code,
//                  dt.transaction_amount * decode(dt.debit_credit_indicator,'C',-1,1)
//                                                    as tran_amount,
//                  dt.transaction_date               as tran_date,
//                  nvl(pd.pos_entry_desc,dt.pos_entry_mode)
//                                                    as pos_entry_mode,
//                  dt.cardholder_account_number      as card_number,
//                  dt.card_number_enc                as card_number_enc,
//                  dt.batch_number                   as batch_number,
//                  dt.merchant_batch_number          as merchant_batch_number,
//                  dt.batch_date                     as batch_date,
//                  ltrim(rtrim(dt.purchase_id))      as purchase_id,
//                  null                              as terminal_id,
//                  null                              as load_file_id,
//                  '0'                               as rec_id,
//                  '0'                               as auth_amount,
//                  'dd_file_dt'                      as data_source
//          from    daily_detail_file_dt   dt,
//                  pos_entry_mode_desc    pd
//          where   dt.batch_date between :getReportDateBegin() and :getReportDateEnd() and
//                  ( (0 = :MerchantNumber and dt.trident_tran_id = :getData("tranId")) or
//                    (dt.merchant_account_number = :MerchantNumber and ('passall' = :FindTranId or dt.trident_tran_id like :FindTranId))
//                  ) and
//                  pd.pos_entry_code(+) = dt.pos_entry_mode and
//                  ( not (dt.pos_entry_mode = 'NA' and dt.net_deposit = 0) ) and
//                  ( 'passall' = :FindClientRefNumber ) and
//                  ( 'passall' = :FindCardNumber or dt.cardholder_account_number like :FindCardNumber ) and
//                  ( 'passall' = :FindAuthCode or dt.authorization_num like :FindAuthCode ) and
//                  ( 'passall' = :FindRefNumber or dt.reference_number like :FindRefNumber ) and
//                  ( 'passall' = :FindTranAmount or dt.transaction_amount between :FindAmountFrom and :FindAmountTo ) and
//                  ( 'passall' = :FindPurchaseId or lower(dt.purchase_id) like :FindPurchaseId ) and
//                  ( 'passall' = :FindCardType or
//                    decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type) = :FindCardType or
//                    ('allOthers' = :FindCardType and decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type)
//                                                     not in ('VS','MC','AM','DS','BL')) )
//          order by dt.batch_date, dt.batch_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1200 = getReportDateBegin();
 java.sql.Date __sJT_1201 = getReportDateEnd();
 String __sJT_1202 = getData("tranId");
  try {
   String theSqlTS = "select  /*+ \n                    index(dt idx_ddf_dt_bdate_merch_acct)\n                 */\n                dt.merchant_account_number        as merchant_number,\n                dt.reference_number               as reference_number,\n                decode( substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type )\n                                                  as card_type,\n                dt.authorization_num              as auth_code,\n                dt.transaction_amount * decode(dt.debit_credit_indicator,'C',-1,1)\n                                                  as tran_amount,\n                dt.transaction_date               as tran_date,\n                nvl(pd.pos_entry_desc,dt.pos_entry_mode)\n                                                  as pos_entry_mode,\n                dt.cardholder_account_number      as card_number,\n                dt.card_number_enc                as card_number_enc,\n                dt.batch_number                   as batch_number,\n                dt.merchant_batch_number          as merchant_batch_number,\n                dt.batch_date                     as batch_date,\n                ltrim(rtrim(dt.purchase_id))      as purchase_id,\n                null                              as terminal_id,\n                null                              as load_file_id,\n                '0'                               as rec_id,\n                '0'                               as auth_amount,\n                'dd_file_dt'                      as data_source\n        from    daily_detail_file_dt   dt,\n                pos_entry_mode_desc    pd\n        where   dt.batch_date between  :1  and  :2  and\n                ( (0 =  :3  and dt.trident_tran_id =  :4 ) or\n                  (dt.merchant_account_number =  :5  and ('passall' =  :6  or dt.trident_tran_id like  :7 ))\n                ) and\n                pd.pos_entry_code(+) = dt.pos_entry_mode and\n                ( not (dt.pos_entry_mode = 'NA' and dt.net_deposit = 0) ) and\n                ( 'passall' =  :8  ) and\n                ( 'passall' =  :9  or dt.cardholder_account_number like  :10  ) and\n                ( 'passall' =  :11  or dt.authorization_num like  :12  ) and\n                ( 'passall' =  :13  or dt.reference_number like  :14  ) and\n                ( 'passall' =  :15  or dt.transaction_amount between  :16  and  :17  ) and\n                ( 'passall' =  :18  or lower(dt.purchase_id) like  :19  ) and\n                ( 'passall' =  :20  or\n                  decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type) =  :21  or\n                  ('allOthers' =  :22  and decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type)\n                                                   not in ('VS','MC','AM','DS','BL')) )\n        order by dt.batch_date, dt.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1200);
   __sJT_st.setDate(2,__sJT_1201);
   __sJT_st.setLong(3,MerchantNumber);
   __sJT_st.setString(4,__sJT_1202);
   __sJT_st.setLong(5,MerchantNumber);
   __sJT_st.setString(6,FindTranId);
   __sJT_st.setString(7,FindTranId);
   __sJT_st.setString(8,FindClientRefNumber);
   __sJT_st.setString(9,FindCardNumber);
   __sJT_st.setString(10,FindCardNumber);
   __sJT_st.setString(11,FindAuthCode);
   __sJT_st.setString(12,FindAuthCode);
   __sJT_st.setString(13,FindRefNumber);
   __sJT_st.setString(14,FindRefNumber);
   __sJT_st.setString(15,FindTranAmount);
   __sJT_st.setDouble(16,FindAmountFrom);
   __sJT_st.setDouble(17,FindAmountTo);
   __sJT_st.setString(18,FindPurchaseId);
   __sJT_st.setString(19,FindPurchaseId);
   __sJT_st.setString(20,FindCardType);
   __sJT_st.setString(21,FindCardType);
   __sJT_st.setString(22,FindCardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1376^7*/
      
      // find transaction does not run if params are bad
      if ( it != null )
      {
        boolean addRecord;
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          addRecord = true;
          if( FindCardNumberFull )
          {
            String cardNumberFull = TridentTools.getCardNumberFull(resultSet.getString("card_number_enc"));
            if( cardNumberFull.indexOf(CardNumber) == -1 )
            {
              addRecord  = false;
            }
          }

          if( addRecord )
          {
            ReportMerchId = resultSet.getLong("merchant_number");
            ReportRows.addElement( new RowData( resultSet ) );
            ReportTotals.add(resultSet.getString("data_source"), resultSet.getDouble("tran_amount"));
          }
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadSettledData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void loadTPGAccessLog()
  {
    String  amount                    = null;
    String  authCode                  = null;
    String  clientRefNumber           = null;
    String  cardNumber                = null;
    String  cardType                  = null;
    String  purchaseId                = null;
    String  reqDelimChar              = "&";
    String  request                   = null;
    String  respDelimChar             = null;
    String  response                  = null;
    double  tranAmount                = 0.0;
    String  tranType                  = null;
    long    merchId                   = 0L;
    ResultSetIterator    it           = null;
    ResultSet            resultSet    = null;

    ReportMerchId = 0L;
    ReportRows.clear();
    
    if( MerchantNumber == 0L )
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1440^9*/

//  ************************************************************
//  #sql [Ctx] { select  merchant_number 
//            from    trident_api_access_log
//            where   request_date between :getReportDateBegin() and :getReportDateEnd() and
//                    trident_tran_id = :getData("tranId")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1203 = getReportDateBegin();
 java.sql.Date __sJT_1204 = getReportDateEnd();
 String __sJT_1205 = getData("tranId");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merchant_number  \n          from    trident_api_access_log\n          where   request_date between  :1  and  :2  and\n                  trident_tran_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1203);
   __sJT_st.setDate(2,__sJT_1204);
   __sJT_st.setString(3,__sJT_1205);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ReportMerchId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1446^9*/
      }
      catch( Exception e )
      {
      }
    }
    else
    {
      ReportMerchId = MerchantNumber;
    }
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1459^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index(al tapi_log_merch_date) */
//                  al.merchant_number        as merchant_number,
//                  al.profile_id             as profile_id,
//                  al.request_date           as tran_date,
//                  al.transaction_type       as tran_type,
//                  al.request_time           as tran_time,
//                  al.retrieval_ref_num      as reference_number,
//                  al.request_params         as request_params,
//                  al.response               as response,
//                  al.trident_tran_id        as trident_tran_id,
//                  'trident_api_log'         as data_source,
//                  cc.currency_code_alpha    as cc_alpha
//          from    trident_api_access_log    al,
//                  currency_codes            cc
//          where   al.request_date between :getReportDateBegin() and :getReportDateEnd() and
//                  al.merchant_number = :ReportMerchId and
//                  ( 'passall' = :FindTranId or al.request_params like :FindTranId or al.response like :FindTranId) and        
//                  ( 'passall' = :FindCardNumber or al.request_params like :FindCardNumber or al.response like :FindCardNumber ) and
//                  ( 'passall' = :FindPurchaseId or lower(al.request_params) like :FindPurchaseId ) and 
//                  ( 'passall' = :FindAuthCode or al.response like :FindAuthCode ) and  
//                  ( 'passall' = :FindRefNumber or al.retrieval_ref_num like :FindRefNumber ) and
//                  ( 'passall' = :FindClientRefNumber or lower(al.request_params) like :FindClientRefNumber ) and
//                  ( 'passall' = :FindCardType or
//                    :FindCardType = 'BL' and al.transaction_type = 'B' or
//                    (:FindCardType in ('VS','MC','DS','AM','allOthers') and al.transaction_type != 'B') ) and
//                  cc.currency_code(+) = nvl(al.currency_code,'840')
//                  and exists
//                  (
//                    select gm.merchant_number
//                    from   organization   o,
//                           group_merchant gm
//                    where  o.org_group = :getReportHierarchyNodeDefault() and
//                           gm.org_num = o.org_num and
//                           gm.merchant_number = al.merchant_number
//                  )
//          order by al.request_time
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1206 = getReportDateBegin();
 java.sql.Date __sJT_1207 = getReportDateEnd();
 long __sJT_1208 = getReportHierarchyNodeDefault();
  try {
   String theSqlTS = "select  /*+ index(al tapi_log_merch_date) */\n                al.merchant_number        as merchant_number,\n                al.profile_id             as profile_id,\n                al.request_date           as tran_date,\n                al.transaction_type       as tran_type,\n                al.request_time           as tran_time,\n                al.retrieval_ref_num      as reference_number,\n                al.request_params         as request_params,\n                al.response               as response,\n                al.trident_tran_id        as trident_tran_id,\n                'trident_api_log'         as data_source,\n                cc.currency_code_alpha    as cc_alpha\n        from    trident_api_access_log    al,\n                currency_codes            cc\n        where   al.request_date between  :1  and  :2  and\n                al.merchant_number =  :3  and\n                ( 'passall' =  :4  or al.request_params like  :5  or al.response like  :6 ) and        \n                ( 'passall' =  :7  or al.request_params like  :8  or al.response like  :9  ) and\n                ( 'passall' =  :10  or lower(al.request_params) like  :11  ) and \n                ( 'passall' =  :12  or al.response like  :13  ) and  \n                ( 'passall' =  :14  or al.retrieval_ref_num like  :15  ) and\n                ( 'passall' =  :16  or lower(al.request_params) like  :17  ) and\n                ( 'passall' =  :18  or\n                   :19  = 'BL' and al.transaction_type = 'B' or\n                  ( :20  in ('VS','MC','DS','AM','allOthers') and al.transaction_type != 'B') ) and\n                cc.currency_code(+) = nvl(al.currency_code,'840')\n                and exists\n                (\n                  select gm.merchant_number\n                  from   organization   o,\n                         group_merchant gm\n                  where  o.org_group =  :21  and\n                         gm.org_num = o.org_num and\n                         gm.merchant_number = al.merchant_number\n                )\n        order by al.request_time";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1206);
   __sJT_st.setDate(2,__sJT_1207);
   __sJT_st.setLong(3,ReportMerchId);
   __sJT_st.setString(4,FindTranId);
   __sJT_st.setString(5,FindTranId);
   __sJT_st.setString(6,FindTranId);
   __sJT_st.setString(7,FindCardNumber);
   __sJT_st.setString(8,FindCardNumber);
   __sJT_st.setString(9,FindCardNumber);
   __sJT_st.setString(10,FindPurchaseId);
   __sJT_st.setString(11,FindPurchaseId);
   __sJT_st.setString(12,FindAuthCode);
   __sJT_st.setString(13,FindAuthCode);
   __sJT_st.setString(14,FindRefNumber);
   __sJT_st.setString(15,FindRefNumber);
   __sJT_st.setString(16,FindClientRefNumber);
   __sJT_st.setString(17,FindClientRefNumber);
   __sJT_st.setString(18,FindCardType);
   __sJT_st.setString(19,FindCardType);
   __sJT_st.setString(20,FindCardType);
   __sJT_st.setLong(21,__sJT_1208);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1497^7*/
      
      if ( it != null )
      {
        boolean addRecord;
        resultSet = it.getResultSet();  
        while( resultSet.next() )
        {
          addRecord = false;
          request = processString(resultSet.getString("request_params"));
          response = processString(resultSet.getString("response"));
          clientRefNumber = processData( request, "client_reference_number", reqDelimChar );

          if ( !processData(request, TridentApiConstants.FN_AN_VERSION, reqDelimChar).equals("") )
          {
            respDelimChar  = processData( request, TridentApiConstants.FN_AN_DELIM_CHAR, reqDelimChar );
            cardNumber     = processData( request, TridentApiConstants.FN_AN_CARD_NUMBER, reqDelimChar );
            cardType       = TridentTools.decodeVisakCardType(cardNumber);
            amount         = processData( request, TridentApiConstants.FN_AN_AMOUNT, reqDelimChar );
            purchaseId     = processData( request, TridentApiConstants.FN_AN_INV_NUM, reqDelimChar );
            authCode       = processData( response, TridentApiConstants.FN_AN_AUTH_CODE, respDelimChar );
            tranType       = processData( response, TridentApiConstants.FN_AN_TRAN_TYPE, respDelimChar );
          }
          else
          {
            tranType = processString(resultSet.getString("tran_type"));
            if( tranType.equals("B") )
            {
              cardType = "BL";
              cardNumber = TridentTools.encodeCardNumber(processData(request,"account_num",reqDelimChar));
              if(cardNumber.equals("") )
              {
                cardNumber = TridentTools.encodeCardNumber(processData(response,"account_num",reqDelimChar));
              }
            }
            else
            {
              cardNumber = TridentTools.encodeCardNumber(processData( request,TridentApiConstants.FN_CARD_NUMBER,reqDelimChar));
              cardType   = TridentTools.decodeVisakCardType(cardNumber);
            }
            amount       = (tranType.equals("B") ? processData( request, "amount", reqDelimChar ) : processData(request,TridentApiConstants.FN_AMOUNT,reqDelimChar ) );
            purchaseId   = (tranType.equals("B") ? processData( request,"order_num",reqDelimChar): processData(request,TridentApiConstants.FN_INV_NUM,reqDelimChar));
            authCode     = processData( response, TridentApiConstants.FN_AUTH_CODE, reqDelimChar );
          }

          if( cardType.equals("ER") )
          {
            cardType = "";
          }
          
          try { tranAmount = Double.parseDouble(amount); } catch( Exception nfe ) { tranAmount = 0.0; }

          if( ( 
                FindCardType.equals(cardType) || FindCardType.equals("passall") || FindCardType.equals("BL") ||
                (FindCardType.equals("allOthers") && !cardType.equals("") && !cardType.equals("VS") && 
                !cardType.equals("MC") && !cardType.equals("AM") && !cardType.equals("DS"))
              ) &&
              ( 
                FindTranAmount.equals("passall") || (tranAmount >= FindAmountFrom && tranAmount <= FindAmountTo) 
              )
            )
          {
            addRecord = true;
          }
          
          if( addRecord ) 
          {
            ReportRows.addElement( new RowData( processString(resultSet.getString("data_source")), 
                                                resultSet.getDate("tran_date"),
                                                resultSet.getLong("merchant_number"),
                                                processString(resultSet.getString("profile_id")),
                                                tranType,
                                                cardType,
                                                cardNumber,
                                                processString(resultSet.getString("reference_number")),
                                                clientRefNumber,
                                                purchaseId,
                                                authCode,
                                                null,
                                                tranAmount,
                                                processString(resultSet.getString("trident_tran_id")),
                                                processString(resultSet.getString("cc_alpha")),
                                                null,
                                                0,
                                                0L,
                                                0 ) );
            ReportTotals.add("trident_api_log", tranAmount);          
          }
        }
        it.close();
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadTPGAcessLog()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadTridentBatchData()
  {
    ReportMerchId = 0L;
    ReportRows.clear();
    loadTridentBatch();
    loadTridentApiBatch();    
  }
  
  public void loadTridentApiBatch()
  {
    ResultSetIterator    it               = null;
    ResultSet            resultSet        = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1614^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (tapi idx_tapi_merch_batch_date) */
//                  tapi.rec_id                      as rec_id,
//                  tapi.merchant_number             as merchant_number,
//                  tapi.terminal_id                 as terminal_id,
//                  tapi.auth_amount                 as auth_amount,
//                  tapi.transaction_amount * decode(tapi.debit_credit_indicator,'C',-1,1)
//                                                   as tran_amount,
//                  tapi.fx_amount_base              as fx_amount,
//                  decode( tapi.batch_id, null, '000000', 'not000000' )
//                                                   as load_file_id,
//                  tapi.auth_date                   as auth_date,
//                  tapi.auth_code                   as auth_code,
//                  tapi.batch_date                  as batch_date,
//                  tapi.batch_number                as batch_number,
//                  tapi.card_number                 as card_number,
//                  tapi.card_number_enc             as card_number_enc,
//                  tapi.card_type                   as card_type,
//                  nvl(pd.pos_entry_desc,tapi.pos_entry_mode)
//                                                   as pos_entry_mode,
//                  nvl( tapi.purchase_id,' ' )      as purchase_id,
//                  tapi.reference_number            as reference_number,
//                  tapi.client_reference_number     as client_reference_number,
//                  tapi.transaction_date            as tran_date,
//                  tapi.transaction_type            as tran_type,
//                  tapi.trident_tran_id             as trident_tran_id,
//                  'trident_api_batch'              as data_source,
//                  cc.currency_code_alpha           as cc_alpha
//          from    trident_capture_api   tapi,
//                  pos_entry_mode_desc   pd,
//                  currency_codes        cc
//          where   tapi.batch_date between :getReportDateBegin() and :getReportDateEnd() and
//                  tapi.merchant_number = :MerchantNumber and
//                  tapi.transaction_type != 'V' and 
//                  nvl(tapi.currency_code,'840') = '840' and -- USD only
//                  ( 'passall' = :FindTranId or tapi.trident_tran_id like :FindTranId ) and
//                  ( 'passall' = :FindCardNumber or tapi.card_number like :FindCardNumber ) and
//                  ( 'passall' = :FindAuthCode or tapi.auth_code like :FindAuthCode ) and
//                  ( 'passall' = :FindTranAmount or tapi.transaction_amount between :FindAmountFrom and :FindAmountTo ) and
//                  ( 'passall' = :FindRefNumber or tapi.reference_number like :FindRefNumber ) and
//                  ( 'passall' = :FindClientRefNumber or lower(tapi.client_reference_number) like :FindClientRefNumber ) and
//                  ( 'passall' = :FindPurchaseId or lower(tapi.purchase_id) like :FindPurchaseId ) and
//                  ( 'passall' = :FindCardType or
//                    ('allOthers' = :FindCardType and tapi.card_type not in('VS','MC','AM','DS','BL')) or
//                    tapi.card_type = :FindCardType ) and
//                  pd.pos_entry_code(+) = tapi.pos_entry_mode and
//                  cc.currency_code(+) = nvl(tapi.currency_code,'840')
//                  and exists
//                  (
//                    select gm.merchant_number
//                    from   organization   o,
//                           group_merchant gm
//                    where  o.org_group = :getReportHierarchyNodeDefault() and
//                           gm.org_num = o.org_num and
//                           gm.merchant_number = tapi.merchant_number
//                  )
//          order by tapi.batch_date, tapi.batch_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1209 = getReportDateBegin();
 java.sql.Date __sJT_1210 = getReportDateEnd();
 long __sJT_1211 = getReportHierarchyNodeDefault();
  try {
   String theSqlTS = "select  /*+ index (tapi idx_tapi_merch_batch_date) */\n                tapi.rec_id                      as rec_id,\n                tapi.merchant_number             as merchant_number,\n                tapi.terminal_id                 as terminal_id,\n                tapi.auth_amount                 as auth_amount,\n                tapi.transaction_amount * decode(tapi.debit_credit_indicator,'C',-1,1)\n                                                 as tran_amount,\n                tapi.fx_amount_base              as fx_amount,\n                decode( tapi.batch_id, null, '000000', 'not000000' )\n                                                 as load_file_id,\n                tapi.auth_date                   as auth_date,\n                tapi.auth_code                   as auth_code,\n                tapi.batch_date                  as batch_date,\n                tapi.batch_number                as batch_number,\n                tapi.card_number                 as card_number,\n                tapi.card_number_enc             as card_number_enc,\n                tapi.card_type                   as card_type,\n                nvl(pd.pos_entry_desc,tapi.pos_entry_mode)\n                                                 as pos_entry_mode,\n                nvl( tapi.purchase_id,' ' )      as purchase_id,\n                tapi.reference_number            as reference_number,\n                tapi.client_reference_number     as client_reference_number,\n                tapi.transaction_date            as tran_date,\n                tapi.transaction_type            as tran_type,\n                tapi.trident_tran_id             as trident_tran_id,\n                'trident_api_batch'              as data_source,\n                cc.currency_code_alpha           as cc_alpha\n        from    trident_capture_api   tapi,\n                pos_entry_mode_desc   pd,\n                currency_codes        cc\n        where   tapi.batch_date between  :1  and  :2  and\n                tapi.merchant_number =  :3  and\n                tapi.transaction_type != 'V' and \n                nvl(tapi.currency_code,'840') = '840' and -- USD only\n                ( 'passall' =  :4  or tapi.trident_tran_id like  :5  ) and\n                ( 'passall' =  :6  or tapi.card_number like  :7  ) and\n                ( 'passall' =  :8  or tapi.auth_code like  :9  ) and\n                ( 'passall' =  :10  or tapi.transaction_amount between  :11  and  :12  ) and\n                ( 'passall' =  :13  or tapi.reference_number like  :14  ) and\n                ( 'passall' =  :15  or lower(tapi.client_reference_number) like  :16  ) and\n                ( 'passall' =  :17  or lower(tapi.purchase_id) like  :18  ) and\n                ( 'passall' =  :19  or\n                  ('allOthers' =  :20  and tapi.card_type not in('VS','MC','AM','DS','BL')) or\n                  tapi.card_type =  :21  ) and\n                pd.pos_entry_code(+) = tapi.pos_entry_mode and\n                cc.currency_code(+) = nvl(tapi.currency_code,'840')\n                and exists\n                (\n                  select gm.merchant_number\n                  from   organization   o,\n                         group_merchant gm\n                  where  o.org_group =  :22  and\n                         gm.org_num = o.org_num and\n                         gm.merchant_number = tapi.merchant_number\n                )\n        order by tapi.batch_date, tapi.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_1209);
   __sJT_st.setDate(2,__sJT_1210);
   __sJT_st.setLong(3,MerchantNumber);
   __sJT_st.setString(4,FindTranId);
   __sJT_st.setString(5,FindTranId);
   __sJT_st.setString(6,FindCardNumber);
   __sJT_st.setString(7,FindCardNumber);
   __sJT_st.setString(8,FindAuthCode);
   __sJT_st.setString(9,FindAuthCode);
   __sJT_st.setString(10,FindTranAmount);
   __sJT_st.setDouble(11,FindAmountFrom);
   __sJT_st.setDouble(12,FindAmountTo);
   __sJT_st.setString(13,FindRefNumber);
   __sJT_st.setString(14,FindRefNumber);
   __sJT_st.setString(15,FindClientRefNumber);
   __sJT_st.setString(16,FindClientRefNumber);
   __sJT_st.setString(17,FindPurchaseId);
   __sJT_st.setString(18,FindPurchaseId);
   __sJT_st.setString(19,FindCardType);
   __sJT_st.setString(20,FindCardType);
   __sJT_st.setString(21,FindCardType);
   __sJT_st.setLong(22,__sJT_1211);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1672^7*/
      
      if ( it != null )
      {
        boolean addRecord;
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          addRecord = true;
          if( FindCardNumberFull )
          {
            String cardNumberFull = TridentTools.getCardNumberFull(resultSet.getString("card_number_enc"));
            if( cardNumberFull.indexOf(CardNumber) == -1 )
            {
              addRecord  = false;
            }
          }

          if( addRecord )
          {
            ReportMerchId = resultSet.getLong("merchant_number");
            ReportRows.addElement( new RowData( resultSet ) );
            ReportTotals.add(resultSet.getString("data_source"), resultSet.getDouble("tran_amount"));
          }
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadTridentApiBatch()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
    
  public void loadTridentBatch()
  {
    boolean       addRecord       = false;
    String        authCode        = null;
    Date          batchDate       = null;
    int           batchNumber     = 0;
    long          batchRecId      = 0L;
    String        cardNumber      = null;
    String        cardNumberFull  = null;
    String        cardType        = null;
    String        entryMode       = null;
    String        findAuthCode    = getData("authCode").trim();
    String        findPurchaseId  = getData("purchaseId").trim();
    String        findRefNumber   = getData("refNumber").trim();
    long          merchantId      = 0L;
    double        netAmount       = 0.0;
    String        purchaseId      = null;
    String        refNumber       = "";
    TDLData       tdlData         = null;
    double        tranAmount      = 0.0;
    int           tranIndex       = 0;
    List          detailRecs      = null;
    BatchData     batchData       = null;
    DetailRecord  row             = null;
    ResultSetIterator  it         = null;
    ResultSet  resultSet          = null;

    TridentBatchDataBean   TridentBatch = null;
    
    try
    {
      TridentBatch = new TridentBatchDataBean();
      TridentBatch.connect(true);
      TridentBatch.setReportHierarchyNodeDefault(getReportHierarchyNodeDefault());
      TridentBatch.setReportDateBegin(getReportDateBegin());
      TridentBatch.setReportDateEnd(getReportDateEnd());
    
      /*@lineinfo:generated-code*//*@lineinfo:1747^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mb.merchant_batch_date  as batch_date,
//                  mb.batch_number         as batch_number,
//                  mb.batch_id             as batch_id
//          from    organization          o,
//                  group_merchant        gm,
//                  mbs_batches           mb
//          where   o.org_group = :getReportHierarchyNodeDefault() and
//                  gm.org_num = o.org_num and
//                  gm.merchant_number = :MerchantNumber and
//                  mb.merchant_number = gm.merchant_number and
//                  'passall' = :FindClientRefNumber and
//                  'passall' = :FindTranId and
//                  mb.merchant_batch_date between :getReportDateBegin() and :getReportDateEnd() 
//          order by mb.batch_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1212 = getReportHierarchyNodeDefault();
 java.sql.Date __sJT_1213 = getReportDateBegin();
 java.sql.Date __sJT_1214 = getReportDateEnd();
  try {
   String theSqlTS = "select  mb.merchant_batch_date  as batch_date,\n                mb.batch_number         as batch_number,\n                mb.batch_id             as batch_id\n        from    organization          o,\n                group_merchant        gm,\n                mbs_batches           mb\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                gm.merchant_number =  :2  and\n                mb.merchant_number = gm.merchant_number and\n                'passall' =  :3  and\n                'passall' =  :4  and\n                mb.merchant_batch_date between  :5  and  :6  \n        order by mb.batch_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1212);
   __sJT_st.setLong(2,MerchantNumber);
   __sJT_st.setString(3,FindClientRefNumber);
   __sJT_st.setString(4,FindTranId);
   __sJT_st.setDate(5,__sJT_1213);
   __sJT_st.setDate(6,__sJT_1214);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1763^7*/      
      resultSet = it.getResultSet();
      while( resultSet.next() )
      {
        tranIndex     = 0;
        batchDate     = resultSet.getDate("batch_date");
        batchNumber   = resultSet.getInt("batch_number");
        batchRecId    = resultSet.getLong("batch_id");
        
        batchData = TridentBatch.loadData( mesConstants.MBS_BT_VISAK, batchRecId );
        detailRecs = batchData.getDetailRecords();      
        merchantId = batchData.MerchantId;
        
        for( int i = 0; i < detailRecs.size(); ++i )
        {
          tranIndex = i;
          row = (DetailRecord)detailRecs.get(i);
          tdlData        = TridentBatch.getTDLData( batchRecId, i );
          cardType       = TridentTools.decodeVisakCardType( row );
          cardNumberFull = row.getCardholderAccountNumber();
          cardNumber     = TridentTools.encodeCardNumber( row.getCardholderAccountNumber() );
          entryMode      = TridentBatch.decodePosEntryMode(TridentBatch.decodeVisakValue(TridentBatch.FT_DATA_SOURCE, row.getAccountDataSource()));         
          tranAmount     = TridentTools.decodeAmount(row.getSettlementAmount());
          if ( TridentTools.isVisakCredit( row.getTransactionCode() ) )
          {
            tranAmount *= -1;
          }
          authCode = TridentBatch.processString( row.getApprovalCode() );
          purchaseId    = ( ( row.hasGroup( TridentTools.VISA_K_GROUP_HOTEL       ) ) ? row.getHotelPurchaseIdentifier()
                          : ( row.hasGroup( TridentTools.VISA_K_GROUP_AUTO_RENT   ) ) ? row.getAutoPurchaseIdentifier()
                          : ( row.hasGroup( TridentTools.VISA_K_GROUP_DIRECT_MKT  ) ) ? row.getMOTOPurchaseIdentifier()
                          :                                                             "" ).trim();
          
          if ( tdlData != null )
          {
            refNumber = String.valueOf(tdlData.AcqRefNum);
          }
          
          addRecord = false;
          if( (FindAuthCode.equals("passall") || authCode.indexOf(findAuthCode) != -1) &&
              (FindTranAmount.equals("passall") || (tranAmount >= FindAmountFrom && tranAmount <= FindAmountTo)) &&
              (FindPurchaseId.equals("passall") || purchaseId.toLowerCase().indexOf(findPurchaseId.toLowerCase() ) != -1) &&
              (FindRefNumber.equals("passall") || refNumber.indexOf(findRefNumber) != -1) &&
              (FindCardNumber.equals("passall") || cardNumber.indexOf(FindCardNumber) != -1) &&
              (FindCardType.equals("passall") || cardType.equals(FindCardType) || 
              (FindCardType.equals("allOthers") && !cardType.equals("VS") && !cardType.equals("MC") && 
              !cardType.equals("DS") && !cardType.equals("AM") && !cardType.equals("BL"))) )
          {
            addRecord = true;
          }
          
          if( addRecord )
          {
            ReportMerchId = merchantId;
            ReportRows.addElement( new RowData( "trident_batch",
                                                null,
                                                merchantId,
                                                null,
                                                null,
                                                cardType,
                                                cardNumber,
                                                refNumber,
                                                "",
                                                purchaseId,
                                                authCode,
                                                entryMode,
                                                tranAmount,
                                                null,
                                                null,
                                                batchDate,
                                                batchNumber,
                                                batchRecId,
                                                tranIndex ) );
            ReportTotals.add("trident_batch", tranAmount);
          }
        }
      }
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadTridentBatch()", e.toString() );
    }
    finally
    {
      try { TridentBatch.cleanUp(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
 
  public int merchantCount( long nodeId )
  {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    int                 retVal    = 0;
    
    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:1862^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(*) as merchant_count
//          from    organization        o,
//                  group_merchant      gm,
//                  mif                 mf
//          where   o.org_group = :nodeId 
//                  and gm.org_num = o.org_num
//                  and mf.merchant_number = gm.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(*) as merchant_count\n        from    organization        o,\n                group_merchant      gm,\n                mif                 mf\n        where   o.org_group =  :1  \n                and gm.org_num = o.org_num\n                and mf.merchant_number = gm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.MerchTranLookupDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.MerchTranLookupDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1871^7*/
      rs = it.getResultSet();  
      while( rs.next() )
      {
        retVal = rs.getInt("merchant_count");
      }
      rs.close();
      it.close();
    }
    catch( Exception e )
    {
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return(retVal);
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    // full card search
    if( getData("cardNumber").length() >= 15 )
    {
      // force card numbers to match MES truncation
      setData("cardNumber",TridentTools.encodeCardNumber(getData("cardNumber")));
    }
    
    if( !getField("amountFrom").isBlank() && getField("amountTo").isBlank() )
    {
      setData("amountTo",String.valueOf(getData("amountFrom")));
    }

    super.postHandleRequest(request);
  }    
  
  protected String processData(String stringParams, String paramsName, String delimiter)
  {
    StringTokenizer   tokenizer   = null;
    String            name        = null;
    String            nextParams  = null;
    String            retVal      = "";
    
    if (stringParams != null)
    {
      tokenizer = new StringTokenizer( stringParams, delimiter);
      while( tokenizer.hasMoreTokens() )
      {
        try
        {
          nextParams = tokenizer.nextToken();
          StringTokenizer token = new StringTokenizer(nextParams,"=");
          if (token.countTokens() == 2)
          {
            name = token.nextToken();
            if( name.toLowerCase().equals(paramsName) )
            {
              retVal = token.nextToken(); 
            }
          } //end if
        }
        catch( Exception e )
        {
          name = "";
        }
      } //end while
    }
    return(retVal);
  }
  
  public void setProperties( HttpServletRequest request )
  {
    super.setProperties( request );
    setReportHierarchyNode( getLong("merchantId") );
    setSearchParams();
  }
  
  private void setSearchParams()
  {
    boolean hasCardData  = false;
    
    MerchantNumber       = getLong("merchantId");
    FindCardType         = (getField("cardType").isBlank() ? "passall" : (getData("cardType")));
    FindRefNumber        = (getField("refNumber").isBlank() ? "passall" : ("%" + getData("refNumber").trim() + "%"));
    FindAuthCode         = (getField("authCode").isBlank() ? "passall" : ("%" + getData("authCode").trim() + "%"));
    FindPurchaseId       = (getField("purchaseId").isBlank() ? "passall" : ("%" + getData("purchaseId").trim().toLowerCase() + "%"));
    FindClientRefNumber  = (getField("clientRefNumber").isBlank() ? "passall" : ("%" + getData("clientRefNumber").trim().toLowerCase() + "%"));
    FindTranAmount       = (getField("amountFrom").isBlank() && getField("amountTo").isBlank() ? "passall" : "");
    FindAmountFrom       = getDouble("amountFrom");
    FindAmountTo         = getDouble("amountTo");
    FindTranId           = (getField("tranId").isBlank() ? "passall" : ("%" + getData("tranId") + "%"));
    hasCardData          = !getField("cardNumberFirst6").isBlank() || !getField("cardNumberLast4").isBlank();
    FindCardNumber       = (hasCardData ? ("%" + getData("cardNumberFirst6") + "xxxxxx" + getData("cardNumberLast4") + "%") : "passall");
    
    ReportRows.clear();
    ReportTotals.clear();
  }
}/*@lineinfo:generated-code*/