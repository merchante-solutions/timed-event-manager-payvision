/*@lineinfo:filename=AchRejectReportDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AchRejectReportDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-01 11:46:29 -0700 (Tue, 01 Jul 2008) $
  Version            : $Revision: 15021 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class AchRejectReportDataBean extends ReportSQLJBean
{
  public  Vector achReasonCodes = new Vector();
  public  Vector achReasonDescs = new Vector();

  private String searchCriteria = "";
  private String achStatus      = "";
  private String achReason      = "";

  public synchronized void setSearchCriteria(String searchCriteria)
  {
    this.searchCriteria = searchCriteria.trim();
  }

  public synchronized String getSearchCriteria()
  {
    String result = "";
    
    if(!this.searchCriteria.equals("passall"))
    {
      result = this.searchCriteria;
    }

    return result;
  }

  public synchronized void setAchStatus(String achStatus)
  {
    this.achStatus = achStatus.trim();
  }

  public synchronized String getAchStatus()
  {
    return this.achStatus;
  }

  public synchronized void setAchReason(String achReason)
  {
    this.achReason = achReason.trim();
  }

  public synchronized String getAchReason()
  {
    return this.achReason;
  }

  public class RowData
  {
    public String       Adden1            = null;
    public String       Adden2            = null;
    public String       DbaName           = null;
    public String       Dda               = null;
    public String       TransitRouting    = null;
    public String       Description       = null;
    public String       MerchantId        = null;
    public String       ReasonCode        = null;
    public String       ReportDate        = null;
    public int          TransactionCode   = 0;
    public String       AmountString      = null;
    public double       Amount            = 0.0;
    public String       Status            = null;
    public String       StatusLastDate    = null;
    public String       StatusLastSource  = null;
    public String       AppType           = null;
    public String       QueueType         = null;
    public String       QueueItemType     = null;
    public String       RejectSeqNum      = null;

    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      Adden1            = processString(resultSet.getString("adden_1"));
      Adden2            = processString(resultSet.getString("adden_2"));
      DbaName           = processString(resultSet.getString("dba_name"));
      Dda               = processString(resultSet.getString("dda"));
      TransitRouting    = processString(resultSet.getString("transit_routing"));
      Description       = processString(resultSet.getString("description"));
      MerchantId        = processString(resultSet.getString("merchant_number"));
      ReasonCode        = processString(resultSet.getString("reason_code"));
      ReportDate        = processString(DateTimeFormatter.getFormattedDate(resultSet.getDate("report_date"), "MM/dd/yyyy"));
      TransactionCode   = resultSet.getInt("transaction_code");
      Amount            = resultSet.getDouble("amount");
      AmountString      = MesMath.toCurrency(resultSet.getDouble("amount"));
      Status            = processString(resultSet.getString("reject_status"));
      StatusLastDate    = processString(DateTimeFormatter.getFormattedDate(resultSet.getDate("date_status_updated"), "MM/dd/yyyy"));
      StatusLastSource  = processString(resultSet.getString("source_status_updated"));
      AppType           = processString(resultSet.getString("app_type"));
      QueueType         = processString(resultSet.getString("queue_type"));
      QueueItemType     = processString(resultSet.getString("queue_item_type"));
      RejectSeqNum      = processString(resultSet.getString("reject_seq_num"));
    }
  }
  
  protected synchronized void init()
  {
    ResultSetIterator             it                  = null;
    ResultSet                     resultSet           = null;
    
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:153^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    reason_code,
//                    description
//          from      ach_reject_reason_codes
//          order by  reason_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    reason_code,\n                  description\n        from      ach_reject_reason_codes\n        order by  reason_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AchRejectReportDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AchRejectReportDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:159^7*/
      
      resultSet = it.getResultSet();
    
      achReasonCodes.clear();
      achReasonDescs.clear();

      achReasonCodes.add("ALL");
      achReasonDescs.add("All Reasons");

      while( resultSet.next() )
      {
        achReasonCodes.add(resultSet.getString("reason_code"));
        achReasonDescs.add((resultSet.getString("reason_code") + " - " + resultSet.getString("description")));
      }

      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("AchRejectReportDataBean() constructor", e.toString());
    }
    finally
    {
      try { resultSet.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public AchRejectReportDataBean( )
  {
    init();
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Reason Code\",");
    line.append("\"DDA Number\",");
    line.append("\"Transit Routing\",");
    line.append("\"Tran Code\",");
    line.append("\"Acct Number\",");
    line.append("\"DBA\",");
    line.append("\"Description\",");
    line.append("\"Report Date\",");
    line.append("\"Adden_1\",");
    line.append("\"Adden_2\",");
    line.append("\"Amount\",");
    line.append("\"Type\",");
    line.append("\"Status\",");
    line.append("\"Last Status Date\",");
    line.append("\"Last Status Source\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( "\"" );
    line.append( record.ReasonCode );
    line.append( "\",\"" );
    line.append( record.Dda );
    line.append( "\",\"" );
    line.append( record.TransitRouting );
    line.append( "\"," );
    line.append( record.TransactionCode );
    line.append( ",\"" );
    line.append( record.MerchantId );
    line.append( "\",\"" );
    line.append( record.DbaName );
    line.append( "\",\"" );
    line.append( record.Description );
    line.append( "\"," );
    line.append( record.ReportDate );
    line.append( ",\"" );
    line.append( record.Adden1 );
    line.append( "\",\"" );
    line.append( record.Adden2 );
    line.append( "\"," );
    line.append( record.Amount );
    line.append( ",\"" );
    line.append( record.AppType );
    line.append( "\",\"" );
    line.append( record.Status );
    line.append( "\",\"" );
    line.append( record.StatusLastDate );
    line.append( "\",\"" );
    line.append( record.StatusLastSource );
    line.append( "\"" );
  }
  
  public synchronized String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_ach_rejects_status_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public synchronized boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData()
  {
    loadData( ReportUserBean, ReportDateBegin, ReportDateEnd );
  }

  public synchronized void loadData( UserBean user, Date beginDate, Date endDate )
  {
    int                           bankNumber          = 0;
    ResultSetIterator             it                  = null;
    ResultSet                     resultSet           = null;
    String                        likeSearchCriteria  = "";
    long                          longLookup          = -2;

    try
    {
      // empty the current contents
      ReportRows.clear();

      if(searchCriteria.trim().equals(""))
      {
        this.searchCriteria = "passall";
      }

      // set up longLookup value (numeric version of lookup value)
      try
      {
        longLookup = Long.parseLong(searchCriteria.trim());
      }
      catch (Exception e) 
      {
        longLookup = -2;
      }

      
      likeSearchCriteria = "%" + searchCriteria.toUpperCase() + "%";
 
      /*@lineinfo:generated-code*//*@lineinfo:320^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ar.merchant_number      merchant_number,
//                  ar.reason_code          reason_code,
//                  ar.dda                  dda,
//                  ar.transit_routing      transit_routing,
//                  ar.transaction_code     transaction_code,
//                  ar.reject_seq_num       reject_seq_num,
//                  ar.merchant_number      merchant_number,
//                  ar.merchant_name        dba_name,
//                  ar.entry_desc           description,
//                  ar.settled_date         report_date,
//                  ar.adden_1              adden_1,
//                  ar.adden_2              adden_2,
//                  ar.amount               amount,
//                  ars.reject_status       reject_status,
//                  ars.date_last_updated   date_status_updated,
//                  ars.source_last_updated source_status_updated,
//                  qd.affiliate            app_type,
//                  qd.type                 queue_type,
//                  qd.item_type            queue_item_type
//          from    ach_rejects           ar,
//                  ach_reject_status     ars,
//                  q_data                qd
//          where   ar.settled_date between :beginDate and :endDate and
//                  INSTR(ar.reason_code, 'C') <= 0 and
//                  ( 'ALL_STATUS' = :achStatus or ars.reject_status = :achStatus ) and 
//                  ( 'ALL'        = :achReason or ar.reason_code    = :achReason ) and
//                  ars.reject_seq_num = ar.reject_seq_num and
//                  ars.reject_seq_num = qd.id and 
//                  (qd.item_type = :MesQueues.Q_ITEM_TYPE_ACH_REJECTS or 
//                   qd.item_type = :MesQueues.Q_ITEM_TYPE_RISK        or 
//                   qd.item_type = :MesQueues.Q_ITEM_TYPE_LEGAL       or
//                   qd.item_type = :MesQueues.Q_ITEM_TYPE_COLLECTIONS or
//                   qd.item_type = :MesQueues.Q_ITEM_TYPE_NON_RETURNABLE) and
//                  (
//                    'passall'             = :searchCriteria or
//                    ar.merchant_number    = :longLookup     or
//                    ar.dda                = :searchCriteria or
//                    ar.transit_routing    = :searchCriteria or
//                    upper(ar.merchant_name) like :likeSearchCriteria or
//                    upper(ar.entry_desc)    like :likeSearchCriteria
//                  )
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ar.merchant_number      merchant_number,\n                ar.reason_code          reason_code,\n                ar.dda                  dda,\n                ar.transit_routing      transit_routing,\n                ar.transaction_code     transaction_code,\n                ar.reject_seq_num       reject_seq_num,\n                ar.merchant_number      merchant_number,\n                ar.merchant_name        dba_name,\n                ar.entry_desc           description,\n                ar.settled_date         report_date,\n                ar.adden_1              adden_1,\n                ar.adden_2              adden_2,\n                ar.amount               amount,\n                ars.reject_status       reject_status,\n                ars.date_last_updated   date_status_updated,\n                ars.source_last_updated source_status_updated,\n                qd.affiliate            app_type,\n                qd.type                 queue_type,\n                qd.item_type            queue_item_type\n        from    ach_rejects           ar,\n                ach_reject_status     ars,\n                q_data                qd\n        where   ar.settled_date between  :1  and  :2  and\n                INSTR(ar.reason_code, 'C') <= 0 and\n                ( 'ALL_STATUS' =  :3  or ars.reject_status =  :4  ) and \n                ( 'ALL'        =  :5  or ar.reason_code    =  :6  ) and\n                ars.reject_seq_num = ar.reject_seq_num and\n                ars.reject_seq_num = qd.id and \n                (qd.item_type =  :7  or \n                 qd.item_type =  :8         or \n                 qd.item_type =  :9        or\n                 qd.item_type =  :10  or\n                 qd.item_type =  :11 ) and\n                (\n                  'passall'             =  :12  or\n                  ar.merchant_number    =  :13      or\n                  ar.dda                =  :14  or\n                  ar.transit_routing    =  :15  or\n                  upper(ar.merchant_name) like  :16  or\n                  upper(ar.entry_desc)    like  :17 \n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AchRejectReportDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setString(3,achStatus);
   __sJT_st.setString(4,achStatus);
   __sJT_st.setString(5,achReason);
   __sJT_st.setString(6,achReason);
   __sJT_st.setInt(7,MesQueues.Q_ITEM_TYPE_ACH_REJECTS);
   __sJT_st.setInt(8,MesQueues.Q_ITEM_TYPE_RISK);
   __sJT_st.setInt(9,MesQueues.Q_ITEM_TYPE_LEGAL);
   __sJT_st.setInt(10,MesQueues.Q_ITEM_TYPE_COLLECTIONS);
   __sJT_st.setInt(11,MesQueues.Q_ITEM_TYPE_NON_RETURNABLE);
   __sJT_st.setString(12,searchCriteria);
   __sJT_st.setLong(13,longLookup);
   __sJT_st.setString(14,searchCriteria);
   __sJT_st.setString(15,searchCriteria);
   __sJT_st.setString(16,likeSearchCriteria);
   __sJT_st.setString(17,likeSearchCriteria);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AchRejectReportDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:364^7*/
      
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      
      resultSet.close();
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadData", e.toString() );
    }
    finally
    {
      try { resultSet.close(); } catch(Exception e) {}
      try { it.close(); } catch( Exception e ) {}
    }
  }
  
  public synchronized void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    setSearchCriteria(HttpHelper.getString(request, "searchCriteria", "passall"));
    setAchStatus(HttpHelper.getString(request, "achStatus"));
    setAchReason(HttpHelper.getString(request, "achReason"));

    /*
    ignore this for now
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         inc           = -1;
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, inc );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      
      cal.setTime( ReportDateEnd );
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }
    */    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/