/*@lineinfo:filename=ChargebackActionDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ChargebackActionDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 8/03/04 10:54a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class ChargebackActionDataBean extends ReportSQLJBean
{
  public static class AutoActionTable extends DropDownTable
  {
    public AutoActionTable( ) 
    {
      ResultSetIterator it          = null;
      long              nodeId      = 0L;
      int               recCount    = 0;
      ResultSet         resultSet   = null;
    
      try
      {
        connect();
      
        /*@lineinfo:generated-code*//*@lineinfo:66^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ac.action_code,
//                    ac.description
//            from    chargeback_action_codes   ac
//            where   nvl(ac.auto_action,'N') = 'Y'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ac.action_code,\n                  ac.description\n          from    chargeback_action_codes   ac\n          where   nvl(ac.auto_action,'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ChargebackActionDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ChargebackActionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:72^9*/
        resultSet = it.getResultSet();
      
        while(resultSet.next())
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      catch(Exception e)
      {
        logEntry("AutoActionTable", e.toString());
      }
      finally
      {
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }
  
  public class RowData
  {
    public String         AccountType     = null;
    public String         AutoAction      = null;
    public long           NodeId          = 0L;
    public Timestamp      LastModDate     = null;
    public String         LastModUser     = null;
    public String         OrgName         = null;
    public String         PortfolioName   = null;
    public long           PortfolioNode   = 0L;
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AccountType   = resultSet.getString("acct_type");
      AutoAction    = resultSet.getString("action_code");
      NodeId        = resultSet.getLong("hierarchy_node");
      LastModDate   = resultSet.getTimestamp("last_mod_date");
      LastModUser   = resultSet.getString("last_mod_user");
      OrgName       = resultSet.getString("org_name");
      
      loadPortfolio(this);
    }
  }
  
  public ChargebackActionDataBean( )
  {
  }
  
  protected void deleteAutoActionAccount( long id )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:127^7*/

//  ************************************************************
//  #sql [Ctx] { delete  
//          from    network_chargeback_auto_act aa
//          where   aa.hierarchy_node = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  \n        from    network_chargeback_auto_act aa\n        where   aa.hierarchy_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.reports.ChargebackActionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("deleteAutoActionAccount()",e.toString());
    }
    finally
    {
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Portfolio\",");
    line.append("\"Hierarchy Node\",");
    line.append("\"Org Name\",");
    line.append("\"Action Code\",");
    line.append("\"Last Modified\",");
    line.append("\"Last User\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append("\"");
    line.append( record.PortfolioName );
    line.append("\",");
    line.append( encodeHierarchyNode(record.NodeId) );
    line.append(",\"");
    line.append( record.OrgName );
    line.append("\",\"");
    line.append( record.AutoAction );
    line.append("\",");
    line.append( DateTimeFormatter.getFormattedDate(record.LastModDate,"MM/dd/yyyy hh:mm:ss a"));
    line.append(",\"");
    line.append( record.LastModUser );
    line.append("\"");
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_cb_auto_act_accounts_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it            = null;
    long                nodeId        = orgIdToHierarchyNode(orgId);
    ResultSet           resultSet     = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:211^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  aa.hierarchy_node         as hierarchy_node,
//                  o.org_name                as org_name,
//                  decode( o.org_type_code,
//                          'm','Merchant',
//                          'a','Association',
//                          'Group'
//                        )                   as acct_type,
//                  aa.auto_action            as action_code,
//                  aa.last_modified_date     as last_mod_date,
//                  aa.last_modified_user     as last_mod_user
//          from    network_chargeback_auto_act   aa,
//                  organization                  o
//          where   ( aa.hierarchy_node in
//                    (
//                      select th.descendent
//                      from    t_hierarchy th
//                      where   th.hier_type = 1 and
//                              th.ancestor = :nodeId
//                    ) or
//                    aa.hierarchy_node in
//                    (
//                      select  mf.merchant_number
//                      from    t_hierarchy th,
//                              mif         mf
//                      where   th.hier_type = 1 and
//                              th.ancestor = :nodeId and
//                              th.entity_type = 4 and
//                              mf.association_node = th.descendent
//                    ) 
//                  ) and
//                  o.org_group = aa.hierarchy_node
//          order by hierarchy_node
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  aa.hierarchy_node         as hierarchy_node,\n                o.org_name                as org_name,\n                decode( o.org_type_code,\n                        'm','Merchant',\n                        'a','Association',\n                        'Group'\n                      )                   as acct_type,\n                aa.auto_action            as action_code,\n                aa.last_modified_date     as last_mod_date,\n                aa.last_modified_user     as last_mod_user\n        from    network_chargeback_auto_act   aa,\n                organization                  o\n        where   ( aa.hierarchy_node in\n                  (\n                    select th.descendent\n                    from    t_hierarchy th\n                    where   th.hier_type = 1 and\n                            th.ancestor =  :1 \n                  ) or\n                  aa.hierarchy_node in\n                  (\n                    select  mf.merchant_number\n                    from    t_hierarchy th,\n                            mif         mf\n                    where   th.hier_type = 1 and\n                            th.ancestor =  :2  and\n                            th.entity_type = 4 and\n                            mf.association_node = th.descendent\n                  ) \n                ) and\n                o.org_group = aa.hierarchy_node\n        order by hierarchy_node";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ChargebackActionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setLong(2,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.ChargebackActionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:245^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadPortfolio( RowData row )
  {
    long              nodeId    = 0L;
    String            nodeName  = null;
    
    try
    {
      if ( row.AccountType.equals("Merchant") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:273^9*/

//  ************************************************************
//  #sql [Ctx] { select  o.org_group, o.org_name 
//            
//            from    mif             mf,
//                    groups          g,
//                    organization    o
//            where   mf.merchant_number = :row.NodeId and
//                    g.assoc_number = mf.association_node and
//                    o.org_group = group_2
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  o.org_group, o.org_name \n           \n          from    mif             mf,\n                  groups          g,\n                  organization    o\n          where   mf.merchant_number =  :1  and\n                  g.assoc_number = mf.association_node and\n                  o.org_group = group_2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ChargebackActionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,row.NodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   row.PortfolioNode = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   row.PortfolioName = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:283^9*/
      }
      else // assoc or group
      {
        /*@lineinfo:generated-code*//*@lineinfo:287^9*/

//  ************************************************************
//  #sql [Ctx] { select  distinct 
//                    o.org_group, 
//                    o.org_name 
//            
//            from    t_hierarchy     th,
//                    groups          g,
//                    organization    o
//            where   th.hier_type = 1 and
//                    th.ancestor = :row.NodeId and
//                    th.entity_type = 4 and
//                    g.assoc_number = th.descendent and
//                    o.org_group = group_2
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct \n                  o.org_group, \n                  o.org_name \n           \n          from    t_hierarchy     th,\n                  groups          g,\n                  organization    o\n          where   th.hier_type = 1 and\n                  th.ancestor =  :1  and\n                  th.entity_type = 4 and\n                  g.assoc_number = th.descendent and\n                  o.org_group = group_2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.ChargebackActionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,row.NodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   row.PortfolioNode = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   row.PortfolioName = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:301^9*/
      }
    }
    catch( java.sql.SQLException sqe )
    {
      row.PortfolioNode = HierarchyTree.DEFAULT_HIERARCHY_NODE;
      row.PortfolioName = "Bank Portfolios";
    }
    catch( Exception e )
    {
      logEntry( "loadPortfolio()",e.toString() );
    }
    finally
    {
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    long        delId     = 0L;
  
    // load the default report properties
    super.setProperties( request );
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
    
    if ( ( (delId = HttpHelper.getLong(request,"deleteId",-1L)) != -1L ) &&
         ( ReportUserBean.hasRight( MesUsers.RIGHT_QUEUE_CHARGEBACK_ADMIN ) ) )
    {
      deleteAutoActionAccount(delId);
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/