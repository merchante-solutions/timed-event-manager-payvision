/*@lineinfo:filename=GenericRecord*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/StatementRecord.sqlj $

  Description:

    StatementRecord

    Allows storage and retrieval of statement records.

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserAppType;

public abstract class GenericRecord extends SQLJConnectionBase
{
  protected String            merchNum;
  protected long              yearMonth;
  protected long              mystId;
  protected long              recNum;
  protected String            dda;
  protected String            trNum;
  protected String            loadFilename;
  protected int               bankNum;
  protected double            discount;
  protected double            fees;
  protected boolean           silent;

  /*
  ** public boolean submitData()
  **
  ** Stores the contents of the current record in a database.
  **
  ** RETURNS: true if submit successful, else false.
  */
  public abstract boolean submitData();

  // hack to allow existing statements to be modified
  public abstract boolean updateData(Object obj);

  /*
  ** public void getData(String getMerchNum, String getRecNum, long getYearMonth)
  **
  ** Loads a database statement record.
  */
  public abstract void getData(String getMerchNum, long getRecNum, long getYearMonth);

  /*
  ** public void purgeFile(String filename)
  **
  ** Given a filename this method will delete all records from merch_statements
  ** that were loaded with the same load filename.  This is a helper method
  ** for the statement file parser.
  */
  public void purgeFile(String filename)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:84^7*/

//  ************************************************************
//  #sql [Ctx] { delete from merch_statements where load_file_name = :filename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from merch_statements where load_file_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.reports.GenericRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,filename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:87^7*/
    }
    catch (Exception e)
    {
      if (!silent)
      {
        System.out.println(this.getClass().getName() +
          "::purgeFile " + e.toString());
      }
      logEntry("purgeFile",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** public String getLogoFilename(String merchNum)
  **
  ** Looks up a logo filename given a merchNum.
  **
  ** RETURNS: path and filename of the logo graphic.
  */
  public String getLogoFilename(String merchNum)
  {
    String        logoFilename  = "";
    UserAppType   uat           = null;

    try
    {
      logoFilename = MesDefaults.getString(MesDefaults.DK_DEFAULT_LOGO_PATH);

      connect();

      // get org id using merchNum
      int orgId;
      /*@lineinfo:generated-code*//*@lineinfo:124^7*/

//  ************************************************************
//  #sql [Ctx] { select  org_num 
//          from    orgmerchant
//          where   org_merchant_num = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  org_num  \n        from    orgmerchant\n        where   org_merchant_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.GenericRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:129^7*/

      // get closest parent with entry in org_gif2 with org id
      uat = new UserAppType();
      long logoNode = uat.getClosestParent(orgId,"org_gif2");

      try
      {
        // get the filename from org_gif2 using the logo node
        /*@lineinfo:generated-code*//*@lineinfo:138^9*/

//  ************************************************************
//  #sql [Ctx] { select  gif_name 
//            from    org_gif2
//            where   hierarchy_node = :logoNode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  gif_name  \n          from    org_gif2\n          where   hierarchy_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.GenericRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,logoNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   logoFilename = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^9*/
      }
      catch (Exception e)
      {
        //logoFilename = "/logos/meslogo.png";
      }
    }
    catch (Exception e)
    {
      if (!silent)
      {
        System.out.println(this.getClass().getName() +
          "::getLogoFilename " + e.toString());
      }
      logEntry("getLogoFilename",e.toString());
    }
    finally
    {
      cleanUp();
    }

    return logoFilename;
  }


  /*
  ** private boolean isNegative(String numStr)
  **
  ** Takes a string of numeric data and determines if the last
  ** character is a '-' sign.
  **
  ** RETURNS: true if last character is '-'
  */
  public static boolean isNegative(String numStr)
  {
    return( (numStr.charAt(numStr.length()-1) == '-') );
  }

  /*
  ** private String stripNonNumeric(String numStr)
  **
  ** Takes a string of numeric data and returns a version stripped of
  ** anything besides digits and decimal points.
  **
  ** RETURNS: stripped numeric String.
  */
  public static String stripNonNumeric(String numStr)
  {
    StringBuffer parseStr = new StringBuffer("0");
    for (int i = 0, len = numStr.length(); i < len; ++i)
    {
      char c = numStr.charAt(i);
      if ((c >= '0' && c <= '9') || c == '.')
      {
        parseStr.append(c);
      }
    }

    return parseStr.toString();
  }

  /*
  ** private int parseInt(String numStr)
  **
  ** Hack to parse integer strings containing non-numeric (i.e. commas).
  **
  ** RETURNS: the amount in numStr as an Int.
  */
  public static int parseInt(String numStr)
  {
    boolean isNeg = isNegative(numStr); // check before stripping non-numerics
    numStr = stripNonNumeric(numStr);
    int retVal = 0;
    try
    {
      retVal = Integer.parseInt(numStr) * (isNeg ? -1 : 1);
    }
    catch(Exception e)
    {
      System.out.println("parse exception: " + numStr);
    }

    return retVal;
  }

  /*
  ** private double parseDouble(String numStr)
  **
  ** Hack to parse dollar amount strings containing non-numeric (i.e. commas).
  **
  ** RETURNS: the amount in numStr as a double.
  */
  public static double parseDouble(String numStr)
  {
    double retVal = 0;
    try
    {
      Number num = ((DecimalFormat)(NumberFormat.getInstance(Locale.US))).parse(numStr.trim());
      
      retVal = num.doubleValue();
    }
    catch(Exception e)
    {
      if( ! numStr.trim().equals(""))
      {
        System.out.println("parse exception: ->" + numStr.trim() +"<-");
        System.out.println(e.toString());
      }
    }

    return retVal;
  }

  /*
  ** ACCESSORS
  */
  public String getMerchNum()
  {
    return merchNum;
  }
  public void setMerchNum(String newMerchNum)
  {
    merchNum = newMerchNum;
  }
  public String getDda()
  {
    return dda;
  }
  public void setDda(String newDda)
  {
    dda = newDda;
  }
  public String getTrNum()
  {
    return trNum;
  }
  public void setTrNum(String newTrNum)
  {
    trNum = newTrNum;
  }
  public int getBankNum()
  {
    return bankNum;
  }
  public void setBankNum(int newBankNum)
  {
    bankNum = newBankNum;
  }
  public double getDiscount()
  {
    return discount;
  }
  public void setDiscount(double newDiscount)
  {
    discount = newDiscount;
  }
  public double getFees()
  {
    return fees;
  }
  public void setFees(double newFees)
  {
    fees = newFees;
  }
  public long getYearMonth()
  {
    return yearMonth;
  }
  public void setYearMonth(long newYearMonth)
  {
    yearMonth = newYearMonth;
  }
  public long getMystId()
  {
    return mystId;
  }
  public void setMystId(long newMystId)
  {
    mystId = newMystId;
  }
  public long getRecNum()
  {
    return recNum;
  }
  public void setRecNum(long newRecNum)
  {
    recNum = newRecNum;
  }
  public String getLoadFilename()
  {
    return loadFilename;
  }
  public void setLoadFilename(String newLoadFilename)
  {
    loadFilename = newLoadFilename;
  }
  public boolean getSilent()
  {
    return silent;
  }
  public void setSilent(boolean silent)
  {
    this.silent = silent;
  }

}/*@lineinfo:generated-code*/