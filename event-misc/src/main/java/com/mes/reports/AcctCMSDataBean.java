/*@lineinfo:filename=AcctCMSDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AcctCMSDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 3/03/04 9:02a $
  Version            : $Revision: 15 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class AcctCMSDataBean extends ReportSQLJBean
{
  public static final int           CMS_INVALID           = -1;
  public static final int           CMS_INCOMING          = 0;
  public static final int           CMS_OUTGOING          = 1;
  public static final int           CMS_SETTLED           = 2;
  public static final int           CMS_PAYMENT           = 3;
  public static final int           CMS_SPLIT             = 4;
  public static final int           CMS_COUNT             = 5;
  
  public static final int           RT_INCOMING_DETAILS   = (RT_USER + 0);
  public static final int           RT_OUTGOING_DETAILS   = (RT_USER + 1);
  public static final int           RT_UNMATCHED_CREDITS  = (RT_USER + 2);
  
  public class ItineraryData
  {
    public String     CardNumber          = null;
    public double     DCEAmount           = 0.0;
    public String     DCERefNum           = null;
    public Date       DTBatchDate         = null;
    public long       DTBatchNumber       = 0L;
    public Date       FinalFlightDate     = null;
    public String     ItineraryNumber     = null;
    public long       MerchantId          = 0L;
    public String     PaymentItinerary    = null;
    public double     TranAmount          = 0.0;
    public Date       TranDate            = null;
    public int        TranType            = CMS_INVALID;
    
    public ItineraryData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      ItineraryNumber     = processString(resultSet.getString("itinerary_number"));
      TranType            = resultSet.getInt("tran_type");
      CardNumber          = processString(resultSet.getString("card_number"));
      TranAmount          = resultSet.getDouble("tran_amount");
      TranDate            = resultSet.getDate("tran_date");
      PaymentItinerary    = processString(resultSet.getString("payment_it_num"));
      FinalFlightDate     = resultSet.getDate("final_flight_date");
      DCERefNum           = processString(resultSet.getString("dce_ref_num"));
      DCEAmount           = resultSet.getDouble("dce_amount");
      DTBatchNumber       = resultSet.getLong("dt_batch_number");
      DTBatchDate         = resultSet.getDate("dt_batch_date");
      MerchantId          = resultSet.getLong("merchant_number");
    }
    
    public double getBalanceTranAmount( )
    {
      double      retVal      = 0.0;
      
      switch( TranType )
      {
        case CMS_INCOMING:
          retVal = TranAmount;
          break;
          
        case CMS_PAYMENT:  
          retVal = (-1 * TranAmount);
          break;
          
        default:
          // ignore all the outgoing transaction types 
          // for the purpose of balancing the itinerary
          break;
      }
      return( retVal );
    }
    
    public double getNetTranAmount( )
    {
      return( TranAmount * ((TranType == CMS_INCOMING) ? 1 : -1) );
    }
    
    public String getPaymentMethodDesc( )
    {
      StringBuffer      retVal = new StringBuffer("");
      
      switch(TranType)
      {
        case CMS_INCOMING:
          retVal.append("Credit Card: ");
          retVal.append(CardNumber);
          break;
          
        case CMS_PAYMENT:
          if ( DCERefNum == null )    // T&E Payment
          {
            retVal.append("Paid By Issuer");
          }
          else
          {
            retVal.append("DCE: ");
            retVal.append( DCERefNum );
            retVal.append( " for " );
            retVal.append( MesMath.toCurrency(DCEAmount) );
            retVal.append( " on " );
            retVal.append( DateTimeFormatter.getFormattedDate(TranDate,"MM/dd/yyyy") );
          }            
          break;
          
//        case CMS_OUTGOING:
        default:
          break;
      }
      return( retVal.toString() );          
    }
    
    public String getTranTypeDesc( )
    {
      String                  retVal = null;
      
      switch( TranType )
      {
        case CMS_INCOMING:
          retVal = "Incoming";
          break;
          
        case CMS_OUTGOING:
          if ( ( PaymentItinerary == null ) || 
                 PaymentItinerary.equals(DetailItineraryNumber) )
          {
            // the item is a request for payment
            // against funds taken on the current itinerary
            retVal = "Outgoing";
          }
          else
          {
            // the item is informational only
            // the request for payment will be 
            // applied to the original item
            retVal = "Split";
          }
          break;
        
        case CMS_SPLIT:
          retVal = "Split";
          break;
          
        case CMS_PAYMENT:
          retVal = "Payment";
          break;
          
        default:
          retVal = "Unknown";
          break;
      }
      return(retVal);
    }
  }
  
  public class SummaryData
  {
    public String     DbaName             = null;
    public double     DiscountRate        = 0.0;
    public double     DiscountSales       = 0.0;
    public Date       LoadFileDate        = null;
    public String     LoadFilename        = null;
    public long       MerchantId          = 0L;
    public double     TotalAmount         = 0.0;
    public int        TranType            = -1;
    
    public SummaryData( ResultSet       resultSet )
      throws java.sql.SQLException 
    {
      MerchantId      = resultSet.getLong   ("merchant_number");
      DbaName         = processString(resultSet.getString ("dba_name"));
      TotalAmount     = resultSet.getDouble ("total_amount");
      LoadFilename    = processString(resultSet.getString ("load_filename"));
      TranType        = resultSet.getInt    ("tran_type");
      
      if ( TranType == CMS_INCOMING )
      {
        DiscountRate  = resultSet.getDouble("discount_rate");
        DiscountSales = resultSet.getDouble("discount_sales");
      }
      
      LoadFileDate = DateTimeFormatter.parseSQLDate(LoadFilename.substring(12,18),"MMddyy");
    }
    
    public double getDiscountAmount( )
    {
      Calendar    cutOffDate  = null;
      Date        fileDate    = null;  
      double      retVal      = 0.0;
      
      if ( TranType == CMS_INCOMING ) 
      {
        cutOffDate = Calendar.getInstance();
        cutOffDate.set(Calendar.MONTH,Calendar.FEBRUARY);
        cutOffDate.set(Calendar.DAY_OF_MONTH,27);
        cutOffDate.set(Calendar.YEAR,2004);
        cutOffDate.set(Calendar.HOUR,0);
        cutOffDate.set(Calendar.MINUTE,0);
        cutOffDate.set(Calendar.SECOND,0);
        
        if ( LoadFileDate.before(cutOffDate.getTime()) )
        {
          retVal = MesMath.round( (DiscountSales * DiscountRate * 0.01), 2 );
        }
      }
      return( retVal );        
    }
    
    public double getIncomingAmount( )
    {
      double      retVal = 0.0;
      
      if ( TranType == CMS_INCOMING )
      {
        retVal = TotalAmount;
      }
      return( retVal );
    }
    
    public double getOutgoingAmount( )
    {
      double      retVal = 0.0;
      
      if ( TranType == CMS_OUTGOING )
      {
        retVal = TotalAmount;
      }
      return( retVal );
    }
  };
  
  public class RowData
  {
    public String     DbaName             = null;
    public String     ItineraryNumber     = null;
    public long       MerchantId          = 0L;
    public double     TranAmount          = 0.0;
    public Date       TranDate            = null;
    public int        TranType            = -1;
  
    public RowData( )
    {
    }
   
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException 
    {
      MerchantId      = resultSet.getLong   ("merchant_number");
      DbaName         = processString(resultSet.getString ("dba_name"));
      ItineraryNumber = processString(resultSet.getString ("itinerary_number"));
      TranAmount      = resultSet.getDouble ("tran_amount");
      TranDate        = resultSet.getDate   ("tran_date");
    }
    
    public double getAmount()
    {
      return( TranAmount );
    }
  }
  
  public class IncomingData extends RowData
  {
    public String     AuthCode            = null;
    public String     CardNumber          = null;
    
    public IncomingData( ResultSet       resultSet )
      throws java.sql.SQLException 
    {
      super(resultSet);
      
      AuthCode        = processString(resultSet.getString ("auth_code"));
      CardNumber      = processString(resultSet.getString ("card_number"));
      TranType        = CMS_INCOMING;
    }
  }
  
  public class OutgoingData extends RowData
  {
    public Date       FinalFlightDate     = null;
    
    public OutgoingData( ResultSet       resultSet )
      throws java.sql.SQLException 
    {
      super(resultSet);
      
      FinalFlightDate = resultSet.getDate("final_flight_date");
      TranType        = CMS_OUTGOING;
    }
  }
  
  public class SettledData extends RowData
  {
    public double     ClearedAmount       = 0.0;
    public double     ItineraryBalance    = 0.0;
    public double     PaidAmount          = 0.0;
    public double     RequestedAmount     = 0.0;
    public double     SettledAmount       = 0.0;
    
    public SettledData( ResultSet       resultSet )
      throws java.sql.SQLException 
    {
      super();
      
      MerchantId        = resultSet.getLong("merchant_number");
      DbaName           = processString(resultSet.getString("dba_name"));
      ItineraryNumber   = processString(resultSet.getString("itinerary_number"));
      ItineraryBalance    = resultSet.getDouble("itinerary_balance");
      ClearedAmount     = resultSet.getDouble("cleared_amount");
      SettledAmount     = resultSet.getDouble("settled_amount");
      RequestedAmount   = resultSet.getDouble("requested_amount"); 
      PaidAmount        = resultSet.getDouble("paid_amount");
      
//@      //
//@      // requested amount needs to be loaded here because certain circumstances
//@      // can cause an itinerary to have a $0 escrow balance but still have
//@      // outstanding outgoing requests.  when this happens the select statement
//@      // ends up masking some of the outgoing requests and therefore
//@      // causes the payment summary report to show negative values because
//@      // the requested amount is short.
//@      //
//@      RequestedAmount   = loadRequestedAmount( ItineraryNumber, ReportDateEnd );
      
      TranType          = CMS_SETTLED;
    }
    
    // overloaded from row data base class
    public double getAmount()
    {
      return( getPaymentAmount() );
    }
    
    public double getAvailableAmount()
    {
      return( SettledAmount - ( ClearedAmount + PaidAmount ) );
    }
    
    public double getPaymentAmount()
    {
      double      availAmount     = getAvailableAmount();
      double      paidAmount      = (ClearedAmount + PaidAmount);
      double      paymentAmount   = (RequestedAmount - paidAmount);

      if ( paymentAmount > availAmount )
      {
        paymentAmount = availAmount;
      }
      return( paymentAmount );
    }
  }
  
  private double            DCEAmount                 = 0.0;
  private long              DCERefNum                 = 0L;
  private String            DetailItineraryNumber     = null;
  
  public AcctCMSDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Merchant Number\",");
    line.append("\"DBA Name\",");
    line.append("\"Itinerary Number\",");
    line.append("\"Tran Date\",");
    line.append("\"Tran Amount\",");
    line.append("\"Card Number\",");
    line.append("\"Auth Code\",");
    line.append("\"Final Flight Date\"," );
    line.append("\"Batch Date\",");
    line.append("\"MeS Ref Num\",");
    line.append("\"Prev Cleared Amount\",");
    line.append("\"Payment Amount\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    IncomingData    incoming  = null;
    RowData         rowData   = (RowData)obj;
    OutgoingData    outgoing  = null;
    SettledData     settled   = null;
    
    line.setLength(0);
    line.append( encodeHierarchyNode( rowData.MerchantId ) );
    line.append( ",\"" );
    line.append( rowData.DbaName );
    line.append( "\"," );
    line.append( rowData.ItineraryNumber );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate( rowData.TranDate, "MM/dd/yyyy" ) );
    line.append( "," );
    line.append( rowData.TranAmount );
    
    switch( rowData.TranType )
    {
      case CMS_INCOMING:
        incoming = (IncomingData) obj;
        line.append( "," );
        line.append( incoming.CardNumber );
        line.append( "," );
        line.append( incoming.AuthCode );
        break;
        
      case CMS_OUTGOING:
      case CMS_SPLIT:   
        outgoing = (OutgoingData) obj;
        line.append( ",,," );
        line.append( DateTimeFormatter.getFormattedDate( outgoing.FinalFlightDate, "MM/dd/yyyy" ) );
        break;
        
      case CMS_SETTLED:
//@        settled = (SettledData) obj;
//@        line.append( "," );
//@        line.append( settled.CardNumber );
//@        line.append( "," );
//@        line.append( settled.AuthCode );
//@        line.append( "," );
//@        line.append( DateTimeFormatter.getFormattedDate( settled.FinalFlightDate, "MM/dd/yyyy" ) );
//@        line.append( "," );
//@        line.append( DateTimeFormatter.getFormattedDate( settled.BatchDate, "MM/dd/yyyy" ) );
//@        line.append( ",'" );
//@        line.append( settled.ReferenceNumber );
//@        line.append( "," );
//@        line.append( settled.ClearedAmount );
//@        line.append( "," );
//@        line.append( settled.getPaymentAmount() );
        break;
    }
  }
  
  public String getDetailItineraryNumber()
  {
    return( DetailItineraryNumber );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_cms_detail_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin, "MMddyyyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd, "MMddyyyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public double getTranTypeTotal( int tranType )
  {
    RowData         item        = null;
    double          retVal      = 0.0;
    
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      item = (RowData) ReportRows.elementAt(i);
      if ( item.TranType == tranType )
      {
        retVal += item.getAmount();
      }
    }
    return( retVal );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
//@        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    switch( ReportType )
    {
      case RT_SUMMARY:
        loadSummaryData( getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
        break;
        
      case RT_INCOMING_DETAILS:
        loadIncomingDetailData( getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
        break;
        
      case RT_OUTGOING_DETAILS:
        loadOutgoingDetailData( getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
        break;
        
      case RT_UNMATCHED_CREDITS:
        break;
    }
  }
  
  public void loadIncomingDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:552^7*/

//  ************************************************************
//  #sql [Ctx] it = { -- incoming transactions to the escrow account
//          select  cms.merchant_number                 as merchant_number,
//                  mf.dba_name                         as dba_name,
//                  cms.itinerary_number                as itinerary_number,
//                  cms.transaction_amount              as tran_amount,
//                  cms.transaction_date                as tran_date,
//                  cms.card_number                     as card_number,
//                  cms.auth_code                       as auth_code
//          from    daily_detail_file_cms_dt  cms,
//                  mif                       mf
//          where   cms.transaction_date between :beginDate and :endDate and
//                  cms.transaction_type = 'I' and
//                  mf.merchant_number = cms.merchant_number
//          order by cms.merchant_number, cms.transaction_date, cms.card_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "-- incoming transactions to the escrow account\n        select  cms.merchant_number                 as merchant_number,\n                mf.dba_name                         as dba_name,\n                cms.itinerary_number                as itinerary_number,\n                cms.transaction_amount              as tran_amount,\n                cms.transaction_date                as tran_date,\n                cms.card_number                     as card_number,\n                cms.auth_code                       as auth_code\n        from    daily_detail_file_cms_dt  cms,\n                mif                       mf\n        where   cms.transaction_date between  :1  and  :2  and\n                cms.transaction_type = 'I' and\n                mf.merchant_number = cms.merchant_number\n        order by cms.merchant_number, cms.transaction_date, cms.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AcctCMSDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AcctCMSDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:568^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new IncomingData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadIncomingDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadItinerary( )
  {
    loadItinerary( DetailItineraryNumber );
  }
  
  public void loadItinerary( String itNum )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:603^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cms.itinerary_number                as itinerary_number,
//                  decode(cms.transaction_type,
//                         'I',:CMS_INCOMING,
//                         'O',:CMS_OUTGOING,
//                         'P',:CMS_PAYMENT,
//                         'S',:CMS_SPLIT,
//                         :CMS_INVALID)                as tran_type,
//                  cms.card_number                     as card_number,
//                  cms.transaction_amount              as tran_amount,
//                  cms.transaction_date                as tran_date,
//                  nvl(cms.original_itinerary_number,
//                      cms.itinerary_number)           as payment_it_num,
//                  cms.final_flight_date               as final_flight_date,
//                  cms.dce_ref_num                     as dce_ref_num,
//                  cms.dce_amount                      as dce_amount,
//                  cms.dt_batch_number                 as dt_batch_number,
//                  cms.dt_batch_date                   as dt_batch_date,
//                  cms.merchant_number                 as merchant_number
//          from    daily_detail_file_cms_dt      cms,
//                  mif                           mf
//          where   :itNum in 
//                    ( cms.itinerary_number, cms.original_itinerary_number ) and
//                  mf.merchant_number = cms.merchant_number
//          order by cms.merchant_number, 
//                   cms.batch_date,
//                   cms.transaction_date, 
//                   cms.transaction_type,
//                   cms.rec_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cms.itinerary_number                as itinerary_number,\n                decode(cms.transaction_type,\n                       'I', :1 ,\n                       'O', :2 ,\n                       'P', :3 ,\n                       'S', :4 ,\n                        :5 )                as tran_type,\n                cms.card_number                     as card_number,\n                cms.transaction_amount              as tran_amount,\n                cms.transaction_date                as tran_date,\n                nvl(cms.original_itinerary_number,\n                    cms.itinerary_number)           as payment_it_num,\n                cms.final_flight_date               as final_flight_date,\n                cms.dce_ref_num                     as dce_ref_num,\n                cms.dce_amount                      as dce_amount,\n                cms.dt_batch_number                 as dt_batch_number,\n                cms.dt_batch_date                   as dt_batch_date,\n                cms.merchant_number                 as merchant_number\n        from    daily_detail_file_cms_dt      cms,\n                mif                           mf\n        where    :6  in \n                  ( cms.itinerary_number, cms.original_itinerary_number ) and\n                mf.merchant_number = cms.merchant_number\n        order by cms.merchant_number, \n                 cms.batch_date,\n                 cms.transaction_date, \n                 cms.transaction_type,\n                 cms.rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AcctCMSDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,CMS_INCOMING);
   __sJT_st.setInt(2,CMS_OUTGOING);
   __sJT_st.setInt(3,CMS_PAYMENT);
   __sJT_st.setInt(4,CMS_SPLIT);
   __sJT_st.setInt(5,CMS_INVALID);
   __sJT_st.setString(6,itNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AcctCMSDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:633^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new ItineraryData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadItinerary()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadOutgoingDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:663^7*/

//  ************************************************************
//  #sql [Ctx] it = { -- requested outgoing transactions from the escrow account
//          select  cms.merchant_number                 as merchant_number,
//                  mf.dba_name                         as dba_name,
//                  cms.itinerary_number                as itinerary_number,
//                  nvl(cms.original_itinerary_number,
//                      cms.itinerary_number)           as payment_it_num,
//                  cms.transaction_amount              as tran_amount,
//                  cms.transaction_date                as tran_date,
//                  cms.final_flight_date               as final_flight_date
//          from    daily_detail_file_cms_dt      cms,
//                  mif                           mf
//          where   cms.final_flight_date between :beginDate and :endDate and
//                  cms.transaction_type = 'O' and
//  --                cms.original_itinerary_number is null and
//                  mf.merchant_number = cms.merchant_number
//          order by cms.merchant_number, cms.itinerary_number, 
//                    cms.final_flight_date, cms.transaction_date      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "-- requested outgoing transactions from the escrow account\n        select  cms.merchant_number                 as merchant_number,\n                mf.dba_name                         as dba_name,\n                cms.itinerary_number                as itinerary_number,\n                nvl(cms.original_itinerary_number,\n                    cms.itinerary_number)           as payment_it_num,\n                cms.transaction_amount              as tran_amount,\n                cms.transaction_date                as tran_date,\n                cms.final_flight_date               as final_flight_date\n        from    daily_detail_file_cms_dt      cms,\n                mif                           mf\n        where   cms.final_flight_date between  :1  and  :2  and\n                cms.transaction_type = 'O' and\n--                cms.original_itinerary_number is null and\n                mf.merchant_number = cms.merchant_number\n        order by cms.merchant_number, cms.itinerary_number, \n                  cms.final_flight_date, cms.transaction_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.AcctCMSDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.AcctCMSDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:682^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new OutgoingData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadOutgoingDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      // summarize the incoming transactions and 
      // calculate the daily discount due.
      /*@lineinfo:generated-code*//*@lineinfo:714^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cms.load_filename                   as load_filename,
//                  cms.merchant_number                 as merchant_number,
//                  mf.dba_name                         as dba_name,
//                  :CMS_INCOMING                       as tran_type,
//                  sum( decode( (cms.transaction_amount/abs(cms.transaction_amount)),
//                               1, decode(substr(cms.card_type,1,1),
//                                         'V', cms.transaction_amount,
//                                         'M', cms.transaction_amount,
//                                          0),
//                               0 ) )                  as discount_sales,
//                  sum( cms.transaction_amount )       as total_amount,
//                  (mf.visa_disc_rate * 0.001)         as discount_rate
//          from    group_merchant                gm,
//                  daily_detail_file_cms_dt      cms,
//                  mif                           mf
//          where   gm.org_num = :orgId and
//                  cms.merchant_number = gm.merchant_number and
//                  cms.batch_date between :beginDate and :endDate and
//                  cms.transaction_type = 'I' and
//                  mf.merchant_number = cms.merchant_number
//          group by cms.load_filename, cms.merchant_number, mf.dba_name,
//                    mf.visa_disc_rate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cms.load_filename                   as load_filename,\n                cms.merchant_number                 as merchant_number,\n                mf.dba_name                         as dba_name,\n                 :1                        as tran_type,\n                sum( decode( (cms.transaction_amount/abs(cms.transaction_amount)),\n                             1, decode(substr(cms.card_type,1,1),\n                                       'V', cms.transaction_amount,\n                                       'M', cms.transaction_amount,\n                                        0),\n                             0 ) )                  as discount_sales,\n                sum( cms.transaction_amount )       as total_amount,\n                (mf.visa_disc_rate * 0.001)         as discount_rate\n        from    group_merchant                gm,\n                daily_detail_file_cms_dt      cms,\n                mif                           mf\n        where   gm.org_num =  :2  and\n                cms.merchant_number = gm.merchant_number and\n                cms.batch_date between  :3  and  :4  and\n                cms.transaction_type = 'I' and\n                mf.merchant_number = cms.merchant_number\n        group by cms.load_filename, cms.merchant_number, mf.dba_name,\n                  mf.visa_disc_rate";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.AcctCMSDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,CMS_INCOMING);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.AcctCMSDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:738^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new SummaryData( resultSet ) );
      }
      resultSet.close();
      it.close();
      
      // summarize the outgoing transactions
      /*@lineinfo:generated-code*//*@lineinfo:749^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cms.load_filename                   as load_filename,
//                  cms.merchant_number                 as merchant_number,
//                  mf.dba_name                         as dba_name,
//                  :CMS_OUTGOING                       as tran_type,
//                  sum( cms.transaction_amount )       as total_amount
//          from    group_merchant                gm,
//                  daily_detail_file_cms_dt      cms,
//                  mif                           mf
//          where   gm.org_num = :orgId and
//                  cms.merchant_number = gm.merchant_number and
//                  cms.batch_date between :beginDate and :endDate and
//                  cms.transaction_type = 'O' and
//                  --cms.original_itinerary_number is null and
//                  mf.merchant_number = cms.merchant_number
//          group by cms.load_filename, cms.merchant_number, mf.dba_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cms.load_filename                   as load_filename,\n                cms.merchant_number                 as merchant_number,\n                mf.dba_name                         as dba_name,\n                 :1                        as tran_type,\n                sum( cms.transaction_amount )       as total_amount\n        from    group_merchant                gm,\n                daily_detail_file_cms_dt      cms,\n                mif                           mf\n        where   gm.org_num =  :2  and\n                cms.merchant_number = gm.merchant_number and\n                cms.batch_date between  :3  and  :4  and\n                cms.transaction_type = 'O' and\n                --cms.original_itinerary_number is null and\n                mf.merchant_number = cms.merchant_number\n        group by cms.load_filename, cms.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.AcctCMSDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,CMS_OUTGOING);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.AcctCMSDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:766^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new SummaryData( resultSet ) );
      }
      resultSet.close();
      it.close();
      
      // add in the settled items
      /*@lineinfo:generated-code*//*@lineinfo:777^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cms.merchant_number                     as merchant_number,
//                  mf.dba_name                             as dba_name,
//                  nvl( cms.original_itinerary_number,
//                       cms.itinerary_number )             as itinerary_number,
//                  bal.itinerary_balance                   as itinerary_balance,
//                  nvl( ddf.settled_amount,0 )             as settled_amount,
//                  0                                       as cleared_amount,    -- obsolete
//                  sum( decode( cms.dce_ref_num, null, 0,
//                               decode( cms.transaction_type,
//                                       'P',cms.transaction_amount, 0 )
//                             ) )                          as paid_amount,
//                  sum( decode( cms.transaction_type,
//                               'O',cms.transaction_amount,0 
//                              ) )                         as requested_amount
//          from    group_merchant              gm,
//                  daily_detail_file_cms_dt    cms,
//                  daily_detail_file_cms_bal   bal,
//                  ( select  dt.purchase_id                  as itinerary_number,
//                            sum( (dt.transaction_amount *
//                                  decode(dt.debit_credit_indicator,
//                                         'C',-1,
//                                         1)
//                                  ) )                       as settled_amount
//                    from    daily_detail_file_dt dt
//                    where   dt.merchant_account_number = 941000020471 and
//                            substr(dt.card_type,1,1) in  ( 'V','M' ) and
//                            dt.reject_reason is null
//                    group by dt.purchase_id
//                  )                           ddf,
//                  mif                         mf
//          where   gm.org_num = :orgId and
//                  cms.merchant_number = gm.merchant_number and
//                  nvl( cms.original_itinerary_number, cms.itinerary_number ) in 
//                  (
//                    select  distinct nvl( cmso.original_itinerary_number,
//                                          cmso.itinerary_number )
//                    from    daily_detail_file_cms_dt  cmso,
//                            daily_detail_file_cms_bal bali
//                    where   cmso.transaction_type = 'O' and
//                            bali.itinerary_number(+) = nvl( cmso.original_itinerary_number,
//                                                           cmso.itinerary_number ) and
//                            (
//                              cmso.final_flight_date between :beginDate and :endDate or
//                              ( cmso.final_flight_date < :beginDate and 
//                                bali.itinerary_balance != 0 and
//                                nvl(bali.paid_amount,0) != nvl(bali.requested_amount,0) 
//                              )
//                            )                            
//                  ) and
//                  cms.transaction_type in ( 'O', 'P' ) and
//                  bal.itinerary_number(+) = nvl(cms.original_itinerary_number,
//                                                cms.itinerary_number) and
//                  ddf.itinerary_number(+) = nvl(cms.original_itinerary_number,
//                                                cms.itinerary_number) and
//                  mf.merchant_number = cms.merchant_number
//          group by  cms.merchant_number, mf.dba_name, 
//                    nvl(cms.original_itinerary_number, cms.itinerary_number),
//                    bal.itinerary_balance, ddf.settled_amount
//          --          clr.cleared_amount, paid.paid_amount
//          order by  cms.merchant_number, itinerary_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cms.merchant_number                     as merchant_number,\n                mf.dba_name                             as dba_name,\n                nvl( cms.original_itinerary_number,\n                     cms.itinerary_number )             as itinerary_number,\n                bal.itinerary_balance                   as itinerary_balance,\n                nvl( ddf.settled_amount,0 )             as settled_amount,\n                0                                       as cleared_amount,    -- obsolete\n                sum( decode( cms.dce_ref_num, null, 0,\n                             decode( cms.transaction_type,\n                                     'P',cms.transaction_amount, 0 )\n                           ) )                          as paid_amount,\n                sum( decode( cms.transaction_type,\n                             'O',cms.transaction_amount,0 \n                            ) )                         as requested_amount\n        from    group_merchant              gm,\n                daily_detail_file_cms_dt    cms,\n                daily_detail_file_cms_bal   bal,\n                ( select  dt.purchase_id                  as itinerary_number,\n                          sum( (dt.transaction_amount *\n                                decode(dt.debit_credit_indicator,\n                                       'C',-1,\n                                       1)\n                                ) )                       as settled_amount\n                  from    daily_detail_file_dt dt\n                  where   dt.merchant_account_number = 941000020471 and\n                          substr(dt.card_type,1,1) in  ( 'V','M' ) and\n                          dt.reject_reason is null\n                  group by dt.purchase_id\n                )                           ddf,\n                mif                         mf\n        where   gm.org_num =  :1  and\n                cms.merchant_number = gm.merchant_number and\n                nvl( cms.original_itinerary_number, cms.itinerary_number ) in \n                (\n                  select  distinct nvl( cmso.original_itinerary_number,\n                                        cmso.itinerary_number )\n                  from    daily_detail_file_cms_dt  cmso,\n                          daily_detail_file_cms_bal bali\n                  where   cmso.transaction_type = 'O' and\n                          bali.itinerary_number(+) = nvl( cmso.original_itinerary_number,\n                                                         cmso.itinerary_number ) and\n                          (\n                            cmso.final_flight_date between  :2  and  :3  or\n                            ( cmso.final_flight_date <  :4  and \n                              bali.itinerary_balance != 0 and\n                              nvl(bali.paid_amount,0) != nvl(bali.requested_amount,0) \n                            )\n                          )                            \n                ) and\n                cms.transaction_type in ( 'O', 'P' ) and\n                bal.itinerary_number(+) = nvl(cms.original_itinerary_number,\n                                              cms.itinerary_number) and\n                ddf.itinerary_number(+) = nvl(cms.original_itinerary_number,\n                                              cms.itinerary_number) and\n                mf.merchant_number = cms.merchant_number\n        group by  cms.merchant_number, mf.dba_name, \n                  nvl(cms.original_itinerary_number, cms.itinerary_number),\n                  bal.itinerary_balance, ddf.settled_amount\n        --          clr.cleared_amount, paid.paid_amount\n        order by  cms.merchant_number, itinerary_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.AcctCMSDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setDate(4,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.AcctCMSDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:839^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new SettledData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void payItems( )
  {
    payItems( getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
  }
  
  public void payItems( long orgId, Date beginDate, Date endDate )
  {
    double                    availAmount       = 0.0;
    double                    dceRemaining      = DCEAmount;
    int                       itemCount         = 0;
    ResultSetIterator         it                = null;
    String                    itNum             = null;
    long                      merchantId        = 0L;
    double                    paidAmount        = 0.0;
    double                    paymentAmount     = 0.0;
    ResultSet                 resultSet         = null;
    
    BufferedWriter  out   = null;
    StringBuffer    line  = new StringBuffer();
    
    try
    {
      out = new BufferedWriter( new FileWriter("payment_log",false) );
      
      /*@lineinfo:generated-code*//*@lineinfo:883^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cms.merchant_number                     as merchant_number,
//                  nvl(cms.original_itinerary_number,
//                      cms.itinerary_number)               as itinerary_number,
//                  nvl( ddf.settled_amount,0 )             as settled_amount,
//                  0                                       as cleared_amount, -- obsolete
//                  sum( decode( cms.dce_ref_num, null, 0,
//                               decode( cms.transaction_type,
//                                       'P',cms.transaction_amount, 0 )
//                             ) )                          as paid_amount,
//                  sum( decode( cms.transaction_type,
//                               'O',cms.transaction_amount,0 
//                              ) )                         as requested_amount
//          from    daily_detail_file_cms_dt    cms,
//                  ( 
//                    select  dt.purchase_id                                      as itinerary_number,
//                            sum( (dt.transaction_amount *
//                                  decode(dt.debit_credit_indicator,'C',-1,1)) ) as settled_amount
//                    from    daily_detail_file_dt dt
//                    where   dt.merchant_account_number = 941000020471 and
//                            substr(dt.card_type,1,1) in  ( 'V','M' ) and
//                            dt.reject_reason is null
//                    group by dt.purchase_id 
//                  )                           ddf
//          where   nvl( cms.original_itinerary_number, cms.itinerary_number ) in 
//                  (
//                    select  distinct nvl( cmso.original_itinerary_number,
//                                          cmso.itinerary_number )
//                    from    daily_detail_file_cms_dt  cmso,
//                            daily_detail_file_cms_bal bal
//                    where   cmso.transaction_type = 'O' and
//                            bal.itinerary_number(+) = nvl( cmso.original_itinerary_number,
//                                                           cmso.itinerary_number ) and
//                            (
//                              cmso.final_flight_date between :beginDate and :endDate or
//                              ( cmso.final_flight_date < :beginDate and 
//                                bal.itinerary_balance != 0 and
//                                nvl(bal.paid_amount,0) != nvl(bal.requested_amount,0) 
//                              )
//                            )                            
//                  ) and
//                  cms.transaction_type in ( 'O', 'P' ) and
//                  ddf.itinerary_number(+) = nvl(cms.original_itinerary_number,
//                                                cms.itinerary_number)
//          group by  cms.merchant_number, 
//                    nvl(cms.original_itinerary_number,cms.itinerary_number), 
//                    ddf.settled_amount
//          order by  cms.merchant_number, itinerary_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cms.merchant_number                     as merchant_number,\n                nvl(cms.original_itinerary_number,\n                    cms.itinerary_number)               as itinerary_number,\n                nvl( ddf.settled_amount,0 )             as settled_amount,\n                0                                       as cleared_amount, -- obsolete\n                sum( decode( cms.dce_ref_num, null, 0,\n                             decode( cms.transaction_type,\n                                     'P',cms.transaction_amount, 0 )\n                           ) )                          as paid_amount,\n                sum( decode( cms.transaction_type,\n                             'O',cms.transaction_amount,0 \n                            ) )                         as requested_amount\n        from    daily_detail_file_cms_dt    cms,\n                ( \n                  select  dt.purchase_id                                      as itinerary_number,\n                          sum( (dt.transaction_amount *\n                                decode(dt.debit_credit_indicator,'C',-1,1)) ) as settled_amount\n                  from    daily_detail_file_dt dt\n                  where   dt.merchant_account_number = 941000020471 and\n                          substr(dt.card_type,1,1) in  ( 'V','M' ) and\n                          dt.reject_reason is null\n                  group by dt.purchase_id \n                )                           ddf\n        where   nvl( cms.original_itinerary_number, cms.itinerary_number ) in \n                (\n                  select  distinct nvl( cmso.original_itinerary_number,\n                                        cmso.itinerary_number )\n                  from    daily_detail_file_cms_dt  cmso,\n                          daily_detail_file_cms_bal bal\n                  where   cmso.transaction_type = 'O' and\n                          bal.itinerary_number(+) = nvl( cmso.original_itinerary_number,\n                                                         cmso.itinerary_number ) and\n                          (\n                            cmso.final_flight_date between  :1  and  :2  or\n                            ( cmso.final_flight_date <  :3  and \n                              bal.itinerary_balance != 0 and\n                              nvl(bal.paid_amount,0) != nvl(bal.requested_amount,0) \n                            )\n                          )                            \n                ) and\n                cms.transaction_type in ( 'O', 'P' ) and\n                ddf.itinerary_number(+) = nvl(cms.original_itinerary_number,\n                                              cms.itinerary_number)\n        group by  cms.merchant_number, \n                  nvl(cms.original_itinerary_number,cms.itinerary_number), \n                  ddf.settled_amount\n        order by  cms.merchant_number, itinerary_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.AcctCMSDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setDate(3,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.AcctCMSDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:932^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        // 
        // Escrow Payment System
        //
        // paidAmount   : total amount that has been released to the merchant
        // availAmount  : total amount settled (verified) less the paidAmount
        // paymentAmount: total amount requested less the paidAmount
        // 
        // The actual amount to be funded to the merchant account is the lesser 
        // of these values:
        //
        //    * paymentAmount
        //    * availAmount
        //    * dceRemaining (remaining funds for this DCE)
        // 
        paidAmount    = ( resultSet.getDouble("cleared_amount") + resultSet.getDouble("paid_amount") );
        availAmount   = ( resultSet.getDouble("settled_amount") - paidAmount );
        paymentAmount = ( resultSet.getDouble("requested_amount") - paidAmount );
        itNum         = processString(resultSet.getString("itinerary_number"));
        merchantId    = resultSet.getLong("merchant_number");                        
        
        //@+
        line.setLength(0);
        line.append("IT: ");
        line.append(itNum);
        line.append("PA: ");
        line.append(paidAmount);
        line.append(" AA: ");
        line.append(availAmount);
        line.append(" PMT: ");
        line.append(paymentAmount);
        //@-

        // don't go over the amount available
        if ( paymentAmount > availAmount )
        {
          paymentAmount = availAmount;
        }
        
        line.append(" PMT2: ");       //@
        line.append(paymentAmount);   //@
        
        // don't go over the DCE amount
        if ( paymentAmount > dceRemaining )
        {
          paymentAmount = dceRemaining;
        }
        
        line.append(" PMT3: ");       //@
        line.append(paymentAmount);   //@
        out.write(line.toString());
        out.newLine();
        
        // skip any without a payment due
        if ( paymentAmount == 0 )
        {
          continue;   
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:995^9*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_cms_dt
//              ( merchant_number,
//                transaction_date,
//                transaction_type,
//                transaction_amount,
//                itinerary_number,
//                dce_ref_num,
//                dce_amount,
//                load_filename )
//            values
//              ( :merchantId,
//                trunc(sysdate),
//                'P',
//                :paymentAmount,
//                :itNum,
//                :DCERefNum,
//                :DCEAmount,
//                'system' )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into daily_detail_file_cms_dt\n            ( merchant_number,\n              transaction_date,\n              transaction_type,\n              transaction_amount,\n              itinerary_number,\n              dce_ref_num,\n              dce_amount,\n              load_filename )\n          values\n            (  :1 ,\n              trunc(sysdate),\n              'P',\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n              'system' )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.reports.AcctCMSDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDouble(2,paymentAmount);
   __sJT_st.setString(3,itNum);
   __sJT_st.setLong(4,DCERefNum);
   __sJT_st.setDouble(5,DCEAmount);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1015^9*/
        
        // update the available amount
        dceRemaining  -= paymentAmount;
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("payItems()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
      try{ out.close();} catch(Exception e){}//@
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    int               index     = 0;
    char              maskChar;
    String            mask      = "$,";
    StringBuffer      temp      = null;
    
    // load the default report properties
    super.setProperties( request );
    
    // get the detail id 
    DetailItineraryNumber = HttpHelper.getString(request,"itNum",null);
    
    // Extract the user's amount.  Prevent errors if the user
    // typed in acceptable monetary or separation characters.
    // To accept (and remove) addition characters from the
    // user input add the character to the mask String above.    
    try
    {
      temp = new StringBuffer( HttpHelper.getString(request,"dceAmount","") );
      
      if ( temp.length() > 0 )
      {
        for( int i = 0; i < mask.length(); ++i )
        {
          maskChar = mask.charAt(i);
          while( (index = temp.toString().indexOf(maskChar)) >= 0 )
          {
            temp.deleteCharAt(index);
          }
        }
        DCEAmount = Double.parseDouble(temp.toString());
      }
      else    // nothing found in the request
      {
        DCEAmount = 0.0;
      }        
      temp = null;    // encourage the garbage collector
    }
    catch( Exception e )
    {
      logEntry( "setProperties( DCEAmount )", e.toString() );
    }
    
    // extract the DCE ref number if available.
    DCERefNum = HttpHelper.getLong(request,"dceRefNum",0L);
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/