/*@lineinfo:filename=SettlementDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/SettlementDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000-2005,2006 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.CheckboxField;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;

public abstract class SettlementDataBean extends ReportSQLJBean
{
  public static final int         RT_BATCH_SUMMARY      = (RT_USER+0);  // 2
  
  public class BatchSummary
  {
    public  Date            BatchDate         = null;
    public  int             BatchNumber       = 0;
    public  double          CreditsAmount     = 0.0;
    public  int             CreditsCount     = 0;
    public  String          DbaName           = null;
    public  double          DebitsAmount      = 0.0;
    public  int             DebitsCount       = 0;
    public  String          LoadFilename      = null;
    public  long            MerchantId        = 0L;
    public  double          NetAmount         = 0.0;
    public  String          OutputFilename    = null;
    public  String          SettlementId      = null;
    
    public BatchSummary( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      BatchDate         = resultSet.getDate("batch_date");
      BatchNumber       = resultSet.getInt("batch_number");
      CreditsAmount     = resultSet.getDouble("credit_amount");
      CreditsCount      = resultSet.getInt("credit_count");
      DbaName           = processString(resultSet.getString("dba_name"));
      DebitsAmount      = resultSet.getDouble("debit_amount");
      DebitsCount       = resultSet.getInt("debit_count");
      LoadFilename      = resultSet.getString("load_filename");
      MerchantId        = resultSet.getLong("merchant_number");
      NetAmount         = resultSet.getDouble("net_amount");
      OutputFilename    = processString(resultSet.getString("output_filename"));
      SettlementId      = processString(resultSet.getString("settlement_id"));
    }
  }

  public class BatchDetail
  {
    public  String          AuthCode          = null;
    public  Date            BatchDate         = null;
    public  int             BatchNumber       = 0;
    public  String          CardNumber        = null;
    public  String          DbaName           = null;
    public  String          DebitCreditInd    = null;
    public  String          LoadFilename      = null;
    public  long            MerchantId        = 0L;
    public  String          ReferenceNumber   = null;
    public  String          SettlementId      = null;
    public  double          TranAmount        = 0.0;
    public  Date            TranDate          = null;
    
    public BatchDetail( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      AuthCode          = processString(resultSet.getString("auth_code"));
      BatchDate         = resultSet.getDate("batch_date");
      BatchNumber       = resultSet.getInt("batch_number");
      CardNumber        = processString(resultSet.getString("card_number"));
      DbaName           = processString(resultSet.getString("dba_name"));
      DebitCreditInd    = processString(resultSet.getString("debit_credit_ind"));
      LoadFilename      = resultSet.getString("load_filename");
      MerchantId        = resultSet.getLong("merchant_number");
      ReferenceNumber   = processString(resultSet.getString("ref_num"));
      SettlementId      = processString(resultSet.getString("settlement_id"));
      TranAmount        = resultSet.getDouble("tran_amount");
      TranDate          = resultSet.getDate("tran_date");
    }
  }
  public class SummaryData
  {
    public  double          CreditsAmount     = 0.0;
    public  int             CreditsCount      = 0;
    public  double          DebitsAmount      = 0.0;
    public  int             DebitsCount       = 0;
    public  String          LoadFilename      = null;
    public  double          NetAmount         = 0.0;
    public  String          OutputFilename    = null;
    public  Date            TransmitDate      = null;
    
    public SummaryData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      CreditsAmount     = resultSet.getDouble("credit_amount");
      CreditsCount      = resultSet.getInt("credit_count");
      DebitsAmount      = resultSet.getDouble("debit_amount");
      DebitsCount       = resultSet.getInt("debit_count");
      LoadFilename      = resultSet.getString("load_filename");
      NetAmount         = resultSet.getDouble("net_amount");
      OutputFilename    = processString(resultSet.getString("output_filename"));
      TransmitDate      = resultSet.getDate("transmit_date");
    }
  }
  
  public SettlementDataBean( )
  {
    super(true);    // use FieldBean support
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    
    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");
    fgroup.deleteField("portfolioId");
    fgroup.deleteField("zip2Node");
    fgroup.add( new CheckboxField("showPending","Show Pending",false) );
    fgroup.add( new HiddenField("loadFilename") );
    fgroup.add( new HiddenField("batchNumber") );
    fgroup.add( new HiddenField("cardType") );
    fgroup.add( new HiddenField("batchDate") );
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    switch( getReportType() )
    {
      case RT_DETAILS:
        line.append("\"Merchant ID\",");
        line.append("\"DBA Name\",");
        line.append("\"Settlement ID\",");
        line.append("\"Batch Date\",");
        line.append("\"Tran Date\",");
        line.append("\"Card Number\",");
        line.append("\"Auth Code\",");
        line.append("\"Reference Number\",");
        line.append("\"Debit Credit Ind\",");
        line.append("\"Tran Amount\",");
        line.append("\"Incoming Filename\"");
        break;
    
      case RT_BATCH_SUMMARY:
        line.append("\"Merchant ID\",");
        line.append("\"DBA Name\",");
        line.append("\"Settlement ID\",");
        line.append("\"Batch Date\",");
        line.append("\"Debits Count\",");
        line.append("\"Debits Amount\",");
        line.append("\"Credits Count\",");
        line.append("\"Credits Amount\",");
        line.append("\"Net Amount\"");
        break;
    
      default:    // RT_SUMMARY
        line.append("\"Output Filename\",");
        line.append("\"Incoming Filename\",");
        line.append("\"Transmit Date\",");
        line.append("\"Debits Count\",");
        line.append("\"Debits Amount\",");
        line.append("\"Credits Count\",");
        line.append("\"Credits Amount\",");
        line.append("\"Net Amount\"");
        break;
    }        
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);
    switch( getReportType() )
    {
      case RT_DETAILS:
      {
        BatchDetail   record = (BatchDetail)obj;
        line.append(encodeHierarchyNode(record.MerchantId));
        line.append(",\"");
        line.append(record.DbaName);
        line.append("\",");
        line.append(record.SettlementId);
        line.append(",");
        line.append(DateTimeFormatter.getFormattedDate(record.BatchDate,"MM/dd/yyyy"));
        line.append(",");
        line.append(DateTimeFormatter.getFormattedDate(record.TranDate,"MM/dd/yyyy"));
        line.append(",\"");
        line.append(record.CardNumber);
        line.append("\",\"");
        line.append(record.AuthCode);
        line.append("\",\"");
        line.append(record.ReferenceNumber);
        line.append("\",\"");
        line.append(record.DebitCreditInd);
        line.append("\",");
        line.append(record.TranAmount);
        line.append(",\"");
        line.append(record.LoadFilename);
        line.append("\"");
        break;
      }
        
      case RT_BATCH_SUMMARY:
      {
        BatchSummary   record = (BatchSummary)obj;
        line.append(encodeHierarchyNode(record.MerchantId));
        line.append(",\"");
        line.append(record.DbaName);
        line.append("\",");
        line.append(record.SettlementId);
        line.append(",");
        line.append(DateTimeFormatter.getFormattedDate(record.BatchDate,"MM/dd/yyyy"));
        line.append(",");
        line.append(record.DebitsCount);
        line.append(",");
        line.append(record.DebitsAmount);
        line.append(",");
        line.append(record.CreditsCount);
        line.append(",");
        line.append(record.CreditsAmount);
        line.append(",");
        line.append(record.NetAmount);
        break;
      }
    
      default:    // RT_SUMMARY
      {
        SummaryData   record = (SummaryData)obj;
        line.append("\"");
        line.append(record.LoadFilename);
        line.append("\",\"");
        line.append(record.OutputFilename);
        line.append("\",");
        line.append(DateTimeFormatter.getFormattedDate(record.TransmitDate,"MM/dd/yyyy"));
        line.append(",");
        line.append(record.DebitsCount);
        line.append(",");
        line.append(record.DebitsAmount);
        line.append(",");
        line.append(record.CreditsCount);
        line.append(",");
        line.append(record.CreditsAmount);
        line.append(",");
        line.append(record.NetAmount);
        break;
      }        
    }        
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    switch( getReportType() )
    {
      case RT_DETAILS:
        filename.append("settlement_batch_details_");
        break;
        
      case RT_BATCH_SUMMARY:
        filename.append("settlement_batch_summary_");
        break;
        
      case RT_SUMMARY:
        filename.append("settlement_file_summary_");
        break;
    }
    filename.append( DateTimeFormatter.getFormattedDate(getReportDateBegin(),"MMddyy") );
    if ( getReportDateBegin().equals(getReportDateEnd()) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(getReportDateEnd(),"MMddyy") );
    }
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
        
      case FF_FIXED:
        break;
    }
    return(retVal);
  }
  
  protected abstract void loadBatchDetails( long merchantId, Date beginDate, int batchNumber );
  protected abstract void loadBatchSummary( String loadFilename );
  protected abstract void loadFileSummary( Date beginDate, Date endDate );
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( getReportType() == RT_DETAILS )
      {
        loadBatchDetails( getReportHierarchyNode(), 
                          new java.sql.Date(DateTimeFormatter.parseDate( getData("batchDate"), "MMddyyyy" ).getTime()),
                          getInt("batchNumber") );
      }
      else if ( getReportType() == RT_BATCH_SUMMARY )
      {
        loadBatchSummary( getData("loadFilename") );
      }
      else    // RT_SUMMARY 
      {
        loadFileSummary( beginDate, endDate );
      }
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/