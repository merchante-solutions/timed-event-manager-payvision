/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/achFileSummaryEntry.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 10/23/00 9:25a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

//C+    AchFileSummaryEntry
public class   AchFileSummaryEntry implements Comparable
{
  // read-only values
  static public   final byte      SORT_BY_AMOUNT                  = 0;
  static public   final byte      SORT_BY_AMOUNT_2                = 1;
  static public   final byte      SORT_BY_COUNT                   = 2;
  static public   final byte      SORT_BY_COUNT_2                 = 3;
  static public   final byte      SORT_BY_NAME                    = 4;
  static public   final byte      SORT_BY_TOTAL_AMOUNT            = 5;

  static private  final byte      SORT_BY_AMOUNT_ASCENDING        = 6;
  static private  final byte      SORT_BY_AMOUNT_DESCENDING       = 7;
  static private  final byte      SORT_BY_COUNT_ASCENDING         = 8;
  static private  final byte      SORT_BY_COUNT_DESCENDING        = 9;
  static private  final byte      SORT_BY_NAME_ASCENDING          = 10;
  static private  final byte      SORT_BY_NAME_DESCENDING         = 11;

  static public   final int       NUM_AMOUNTS                     = 2;
  static public   final int       NUM_COUNTS                      = NUM_AMOUNTS;
  
  static public   final int       DEBIT_INDEX                     = 0;
  static public   final int       CREDIT_INDEX                    = 1;
  static public   final int       DEFAULT_INDEX                   = 0;
  static public   final int       TOTAL_INDEX                     = NUM_AMOUNTS;
  
  // instance variables
  double          Amount[]                    = new double[ NUM_AMOUNTS ];
  int             Count[]                     = new int   [ NUM_COUNTS  ];
  String          LoadFilename                = "Invalid";
  int             VitalBatchNumber            = -1;
  
  // class variables
  static int      SortIndex      = 0;
  static byte     SortOrder      = SORT_BY_NAME;

  public AchFileSummaryEntry( )
  {
      // initialize the count and amount data structures
    for ( int i = 0; i < NUM_AMOUNTS; ++i )
    {
      Amount[i] = 0.0;
      Count [i] = 0;
    }
  }
  
  public int compareTo( Object object )
  {
    double              amountDelta;
    AchFileSummaryEntry entry = (AchFileSummaryEntry) object;
    int                 retVal;
    
    switch( SortOrder )
    {
      case SORT_BY_COUNT:
      case SORT_BY_COUNT_2:
      case SORT_BY_COUNT_ASCENDING:
      case SORT_BY_COUNT_DESCENDING:
        if ( SortOrder == SORT_BY_COUNT_DESCENDING )
        {
          retVal = ( entry.getCount( SortIndex ) - getCount( SortIndex ) );
        }
        else
        {
          retVal = ( getCount( SortIndex ) - entry.getCount( SortIndex ) );
        }
        if ( retVal == 0 )
        {
          retVal = getLoadFilename().compareTo( entry.getLoadFilename() ); 
        }
        break;

      case SORT_BY_AMOUNT:
      case SORT_BY_AMOUNT_ASCENDING:
      case SORT_BY_AMOUNT_DESCENDING:
        if ( SortOrder == SORT_BY_AMOUNT_DESCENDING )
        {
          amountDelta = ( entry.getAmount( SortIndex ) - getAmount( SortIndex ) );
        }
        else
        {
          amountDelta = ( getAmount( SortIndex ) - entry.getAmount( SortIndex ) );
        }
        if ( amountDelta > 0.0 )
        {
          retVal = 1;
        }
        else if ( amountDelta < 0.0 )
        {
          retVal = -1;
        }
        else
        {
          retVal = getLoadFilename().compareTo( entry.getLoadFilename() );
        }
        break;

//      case SORT_BY_NAME:
      default:
        if ( SortOrder == SORT_BY_NAME_DESCENDING )
        {
          retVal = entry.getLoadFilename().compareTo( getLoadFilename() );
        }
        else    // default is ascending by name
        {
          retVal = getLoadFilename().compareTo( entry.getLoadFilename() );
        }
        if ( retVal == 0 )
        {
          retVal = getBatchNumber() - entry.getBatchNumber();
        }
        break;
    }
    return( retVal );
  }

  public double getAmount( )
  {
    return( getAmount( DEFAULT_INDEX ) );
  }

  public double getAmount( int index )
  {
    double        retVal = 0.0;

    try
    {
      if ( index == TOTAL_INDEX )
      {
        retVal = getTotalAmount();
      }
      else
      {
        retVal = Amount[ index ];
      }        
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      // return the default of 0.0
    }
    return( retVal );
  }
  
  public int getBatchNumber( )
  {
    return( VitalBatchNumber );
  }
  
  public int getCount( )
  {
    return( getCount( DEFAULT_INDEX ) );
  }

  public int getCount( int index )
  {
    int       retVal = -1;
    try
    {
      retVal = Count[ index ];
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      // return default of -1
    }
    return( retVal );
  }
  
  public double getCreditAmount( )
  {
    return( getAmount( CREDIT_INDEX ) );
  }
  
  public int getCreditCount( )
  {
    return( getCount( CREDIT_INDEX ) );
  }
  
  public double getDebitAmount( )
  {
    return( getAmount( DEBIT_INDEX ) );
  }
  
  public int getDebitCount( )
  {
    return( getCount( DEBIT_INDEX ) );
  }
  
  public String getLoadFilename( )
  {
    return( LoadFilename );
  }
  
  public double getTotalAmount( )
  {
    double      total = 0.0;
    
    for ( int i = 0; i < NUM_AMOUNTS; ++i )
    {
      total += Amount[i];
    }
    return( total );
  }
  
  public int getTotalCount( )
  {
    int       count = 0;
    
    for( int i = 0; i < NUM_COUNTS; ++i )
    {
      count += Count[i];
    }
    return( count );
  }
  
  public void setAmount( double newAmount )
  {
    setAmount( DEFAULT_INDEX, newAmount );
  }

  public void setAmount( int index, double newAmount )
  {
    try
    {
      Amount[ index ] = newAmount;
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      // ?
    }
  }
  
  public void setBatchNumber( int newBatchNumber )
  {
    VitalBatchNumber = newBatchNumber;
  }
  
  public void setCount( int newCount )
  {
    setCount( DEFAULT_INDEX, newCount );
  }

  public void setCount( int index, int newCount )
  {
    try
    {
      Count[ index ] = newCount;
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      // ?
    }
  }
  
  public void setCreditAmount( double newAmount )
  {
    setAmount( CREDIT_INDEX, newAmount );
  }
  
  public void setCreditCount( int newCount )
  {
    setCount( CREDIT_INDEX, newCount );
  }
  
  public void setDebitAmount( double newAmount )
  {
    setAmount( DEBIT_INDEX, newAmount );
  }
  
  public void setDebitCount( int newCount )
  {
    setCount( DEBIT_INDEX, newCount );
  }
  
  public void setLoadFilename( String newFilename )
  {
    LoadFilename = newFilename;
  }
  
  static public void setSortOrder( byte newSortOrder )
  {
    switch( newSortOrder )
    {
      case SORT_BY_COUNT:
      case SORT_BY_COUNT_2:
        SortIndex = ( ( newSortOrder == SORT_BY_COUNT ) ? 0 : 1 );
        if ( SortOrder == SORT_BY_COUNT_DESCENDING )
        {
          SortOrder = SORT_BY_COUNT_ASCENDING;
        }
        else
        {
          SortOrder = SORT_BY_COUNT_DESCENDING;
        }
        break;

      case SORT_BY_AMOUNT:
      case SORT_BY_AMOUNT_2:
      case SORT_BY_TOTAL_AMOUNT:
        if ( newSortOrder == SORT_BY_TOTAL_AMOUNT )
        {
          SortIndex = TOTAL_INDEX;
        }
        else
        {
          SortIndex = ( ( newSortOrder == SORT_BY_AMOUNT ) ? 0 : 1 );
        }          
        
        if ( SortOrder == SORT_BY_AMOUNT_DESCENDING )
        {
          SortOrder = SORT_BY_AMOUNT_ASCENDING;
        }
        else
        {
          SortOrder = SORT_BY_AMOUNT_DESCENDING;
        }
        break;

      case SORT_BY_NAME:
        if ( SortOrder == SORT_BY_NAME_ASCENDING )
        {
          SortOrder = SORT_BY_NAME_DESCENDING;
        }
        else
        {
          SortOrder = SORT_BY_NAME_ASCENDING;
        }
        break;

      default:
        SortOrder = newSortOrder;
        break;
    }
  }
}
//C-    AchFileSummaryEntry



