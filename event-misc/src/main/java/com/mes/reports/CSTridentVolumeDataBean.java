/*@lineinfo:filename=CSTridentVolumeDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CSTridentVolumeDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2007-05-31 16:10:09 -0700 (Thu, 31 May 2007) $
  Version            : $Revision: 13767 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ComboDateField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class CSTridentVolumeDataBean extends ReportSQLJBean
{
  public static final int        TYPE_TRIDENT       = 0;
  public static final int        TYPE_VITAL         = 1;
  public static final int        TYPE_OTHER         = 2;
  public static final int        TYPE_TOTAL         = 3;
  public static final int        TYPE_COUNT         = 4;
  
  private static final String[] ColumnNames =
  {
    "trident_",
    "vital_",
    "other_",
    "total_",
  };
  
  public class RowData
  {
    public long       HierarchyNode       = 0L;
    public long       OrgId               = 0L;
    public String     OrgName             = null;
    public double[]   SalesAmount         = new double[TYPE_COUNT];
    public int[]      SalesCount          = new int[TYPE_COUNT];
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      HierarchyNode = resultSet.getLong("hierarchy_node");
      OrgId         = resultSet.getLong("org_num");
      OrgName       = resultSet.getString("org_name");
      
      for( int i = 0; i < ColumnNames.length; ++i )
      {
        SalesCount[i]   = resultSet.getInt(ColumnNames[i] + "count");
        SalesAmount[i]  = resultSet.getDouble(ColumnNames[i] + "amount");
      }
    }
    
    public double getSalesAmount( int type )
    {
      return( SalesAmount[type] );
    }
    
    public int getSalesCount( int type )
    {
      return( SalesCount[type] );
    }
  }
  
  public CSTridentVolumeDataBean( )
  {
    super(true);   // enable field bean support
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Trident Sales Count\",");
    line.append("\"Trident Sales Amount\",");
    line.append("\"Vital Sales Count\",");
    line.append("\"Vital Sales Amount\",");
    line.append("\"Other Sales Count\",");
    line.append("\"Other Sales Amount\",");
    line.append("\"Total Sales Count\",");
    line.append("\"Total Sales Amount\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData       record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.HierarchyNode );
    line.append( ",\"" );
    line.append( record.OrgName );
    line.append( "\"" );
    for( int i = 0; i < TYPE_COUNT; ++i )
    {
      line.append( "," );
      line.append( record.SalesCount[i] );
      line.append( "," );
      line.append( record.SalesAmount[i] );
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Date                    beginDate = getReportDateBegin();
    Date                    endDate   = getReportDateBegin();
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_volume_comparison_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( beginDate, "MMddyyyy" ) );
    if ( !beginDate.equals(endDate) )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( endDate, "MMddyyyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it          = null;
    long                nodeId      = orgIdToHierarchyNode( orgId );
    ResultSet           resultSet   = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:179^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                    index( sm pk_ddf_summary )
//                    ordered  
//                  */
//                  o.org_num                       as org_num,
//                  o.org_group                     as hierarchy_node,
//                  o.org_name                      as org_name,
//                  sum(  decode( substr(load_filename,1,4),
//                                '256m', bank_count,
//                                0 ) )             as trident_count,
//                  sum(  decode( substr(load_filename,1,4),
//                                '256m', bank_amount,
//                                0 ) )             as trident_amount,
//                  sum(  decode( substr(load_filename,1,4),
//                                '256d', bank_count,
//                                0 ) )             as vital_count,
//                  sum(  decode( substr(load_filename,1,4),
//                                '256d', bank_amount,
//                                0 ) )             as vital_amount,
//                  sum(  decode( substr(load_filename,1,4),
//                                '256m', 0, '256d', 0, 
//                                bank_count ) )    as other_count,
//                  sum(  decode( substr(load_filename,1,4),
//                                '256m', 0, '256d', 0, 
//                                bank_amount ) )   as other_amount,                                                                   
//                  sum(sm.bank_count)              as total_count,                
//                  sum(sm.bank_amount)             as total_amount                
//          from    parent_org                    po,
//                  group_merchant                gm,
//                  daily_detail_file_ext_summary sm,
//                  organization                  o
//          where   po.parent_org_num = :orgId and
//                  gm.org_num = po.org_num and
//                  sm.merchant_number = gm.merchant_number and
//                  sm.batch_date between :beginDate and :endDate and
//                  o.org_num = gm.org_num
//          group by o.org_num, o.org_group, o.org_name
//          order by o.org_name, o.org_group      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                  index( sm pk_ddf_summary )\n                  ordered  \n                */\n                o.org_num                       as org_num,\n                o.org_group                     as hierarchy_node,\n                o.org_name                      as org_name,\n                sum(  decode( substr(load_filename,1,4),\n                              '256m', bank_count,\n                              0 ) )             as trident_count,\n                sum(  decode( substr(load_filename,1,4),\n                              '256m', bank_amount,\n                              0 ) )             as trident_amount,\n                sum(  decode( substr(load_filename,1,4),\n                              '256d', bank_count,\n                              0 ) )             as vital_count,\n                sum(  decode( substr(load_filename,1,4),\n                              '256d', bank_amount,\n                              0 ) )             as vital_amount,\n                sum(  decode( substr(load_filename,1,4),\n                              '256m', 0, '256d', 0, \n                              bank_count ) )    as other_count,\n                sum(  decode( substr(load_filename,1,4),\n                              '256m', 0, '256d', 0, \n                              bank_amount ) )   as other_amount,                                                                   \n                sum(sm.bank_count)              as total_count,                \n                sum(sm.bank_amount)             as total_amount                \n        from    parent_org                    po,\n                group_merchant                gm,\n                daily_detail_file_ext_summary sm,\n                organization                  o\n        where   po.parent_org_num =  :1  and\n                gm.org_num = po.org_num and\n                sm.merchant_number = gm.merchant_number and\n                sm.batch_date between  :2  and  :3  and\n                o.org_num = gm.org_num\n        group by o.org_num, o.org_group, o.org_name\n        order by o.org_name, o.org_group";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CSTridentVolumeDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.CSTridentVolumeDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:219^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest( request );
  
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000L );
    }
  
    if ( HttpHelper.getString(request,"beginDate.month",null) == null )
    {
      Calendar  cal = Calendar.getInstance();
      
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      ((ComboDateField)getField("beginDate")).setUtilDate( cal.getTime() );
      ((ComboDateField)getField("endDate")).setUtilDate( cal.getTime() );
    }      
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/