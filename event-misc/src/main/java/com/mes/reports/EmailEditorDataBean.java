/*@lineinfo:filename=EmailEditorDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/EmailEditorDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2007-10-22 $
  Version            : $ $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class EmailEditorDataBean extends ReportSQLJBean
{
  public static final int EMAIL_FROM      = 1;
  public static final int EMAIL_TO        = 2;
  public static final int EMAIL_CC        = 3;
  public static final int EMAIL_BCC       = 4;
 
  public EmailEditorDataBean( )
  {
    super(true);    // use FieldBean support
  }
  
  public class EmailData
  {
	public int           AddrType       = 0;
	public String        AddrDesc       = null;
	public String        EmailAddress   = null;
	public String        AddrEnabled    = null;

	public EmailData( ResultSet       resultSet )
    throws java.sql.SQLException
    {
      AddrType        = resultSet.getInt("address_type");
      AddrDesc        = resultSet.getString("address_desc");
      EmailAddress    = resultSet.getString("email_address");
      AddrEnabled     = resultSet.getString("enabled");
    }
  }
 
  public static class EmailAddresses
  {
	public int           GroupId      = 0;
	public int           AddrType     = 0;
	public String        EmailAddress = null;

	public EmailAddresses(int groupId, int addrType, String emailAddr)
    {
	  GroupId        = groupId;
      AddrType       = addrType;
      EmailAddress   = emailAddr;
    }
  }

  
  public static class EmailGroupTable extends DropDownTable
  {
  
    public EmailGroupTable( )
    {
      
      ResultSetIterator   it        = null;
      ResultSet           rs        = null;

      try
      {
        connect();
        
        addElement("",  "select");
              
        /*@lineinfo:generated-code*//*@lineinfo:95^9*/

//  ************************************************************
//  #sql [Ctx] it = { select *
//            from email_types
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select *\n          from email_types";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.EmailEditorDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.EmailEditorDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:99^9*/
        rs = it.getResultSet();
        while( rs.next() )
        {
          addElement(rs);
        }
        rs.close();
        it.close();
      }
      catch( Exception e )
      {
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }

  
  public static class GroupModeTable extends DropDownTable
  {
	
	public GroupModeTable()
	{
	  addElement("EA", "Enable");
	  addElement("DA", "Disable");
	}
  }
  
  
  private class NotAddGroupCondition
  implements Condition
  {
	String id = null;
	
    public NotAddGroupCondition( String emailId )
    {
      id = emailId;
    }
  
    public boolean isMet() 
    { 
      boolean     retVal    = false;
    
    if (id == null)
    {
      retVal = true;
    }
    return( retVal );
  }
}

  
  protected boolean autoSubmit()
  {
	boolean       retVal      = false;
	String        fname       = getAutoSubmitName();
    int           emailId     = getInt("email_id");
    String        fromAddr    = getData("from_address");
    String        toAddr      = getData("to_address");
    String        ccAddr      = getData("cc_address");
    String        bccAddr     = getData("bcc_address");
    String        groupName   = getData("group_name");
    String        groupMode   = getData("group_mode");
  
    if (fname.equals("btnSave"))
    {
      if (emailId == 0)
      {
    	emailId = updateEmailTypes(groupName);
      }
      if (emailId != -1)
      {
        processEmailData(emailId, EMAIL_FROM, fromAddr);
        processEmailData(emailId, EMAIL_TO, toAddr);
        processEmailData(emailId, EMAIL_CC, ccAddr);
        processEmailData(emailId, EMAIL_BCC, bccAddr);
        retVal = storeData(emailId, groupMode);
      }
    }
    return (retVal);
  }
  
  
  public void deleteEmail(String emailId, int addrType, String emailAddr)
  {
    try
    {
  	  connect();
	  /*@lineinfo:generated-code*//*@lineinfo:191^4*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    email_addresses
//          where   email_id     = :emailId and
//                  address_type = :addrType and
//                  address      = :emailAddr
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    email_addresses\n        where   email_id     =  :1  and\n                address_type =  :2  and\n                address      =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.EmailEditorDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,emailId);
   __sJT_st.setInt(2,addrType);
   __sJT_st.setString(3,emailAddr);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:198^7*/
      /*@lineinfo:generated-code*//*@lineinfo:199^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:202^7*/
    }
    catch( Exception e )
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:206^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:206^34*/ } catch( Exception ee ){}
      logEntry( "deleteEmail()", e.toString() );
      try{ /*@lineinfo:generated-code*//*@lineinfo:208^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:208^32*/ }catch( Exception ee){}   
      System.out.println("deleteEmail(): " + e.toString());
    }
    finally
    {
      cleanUp(); 
    }
  }
  

  public int updateEmailTypes(String emailDesc)
  {
	int emailId   = 0;

	try
    {
  	  connect();
      /*@lineinfo:generated-code*//*@lineinfo:225^7*/

//  ************************************************************
//  #sql [Ctx] { select  (count(email_id) + 1)  
//          from    email_types
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  (count(email_id) + 1)   \n        from    email_types";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.EmailEditorDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   emailId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:229^7*/
      /*@lineinfo:generated-code*//*@lineinfo:230^7*/

//  ************************************************************
//  #sql [Ctx] { insert into email_types
//      	       ( email_id,
//                   email_desc
//                 )
//      	values 
//                 ( :emailId,
//                   :emailDesc
//                 )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into email_types\n    \t       ( email_id,\n                 email_desc\n               )\n    \tvalues \n               (  :1 ,\n                  :2 \n               )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.reports.EmailEditorDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,emailId);
   __sJT_st.setString(2,emailDesc);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:240^7*/
      /*@lineinfo:generated-code*//*@lineinfo:241^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:244^7*/
    }
    catch( Exception e )
    {
      emailId = -1;
      try{ /*@lineinfo:generated-code*//*@lineinfo:249^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:249^34*/ } catch( Exception ee ){}
      logEntry( "updateEmailTypes(): ", e.toString() );
      try{ /*@lineinfo:generated-code*//*@lineinfo:251^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:251^32*/ }catch( Exception ee){}   
      System.out.println("updateEmailTypes() " + e.toString());
    }
    finally
    {
      cleanUp(); 
    }
    return emailId;
  }
  
  
  public void updateMode(String emailId, String mode)
  {
    try
    {
  	connect();
  	/*@lineinfo:generated-code*//*@lineinfo:267^4*/

//  ************************************************************
//  #sql [Ctx] { update   email_addresses
//          set      enabled = :(mode.equals("enable")) ? "Y" : "N"
//          where    email_id = :emailId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1115 = (mode.equals("enable")) ? "Y" : "N";
   String theSqlTS = "update   email_addresses\n        set      enabled =  :1 \n        where    email_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.reports.EmailEditorDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1115);
   __sJT_st.setString(2,emailId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:272^7*/
      /*@lineinfo:generated-code*//*@lineinfo:273^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:276^7*/
     }
    catch( Exception e )
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:280^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:280^34*/ } catch( Exception ee ){}
      logEntry( "updateMode()", e.toString() );
      try{ /*@lineinfo:generated-code*//*@lineinfo:282^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:282^32*/ }catch( Exception ee){}   
      System.out.println("updateMode(): " + e.toString());
    }
    finally
    {
      cleanUp(); 
    }

  }
  
  public boolean storeData(int emailId, String groupMode)
  {
	boolean retVal = true;
	
    try
    {
  	  connect();
      
  	  /*@lineinfo:generated-code*//*@lineinfo:300^6*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    email_addresses
//          where   email_id = :emailId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    email_addresses\n        where   email_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.EmailEditorDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,emailId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:305^7*/
      
      for (int i=0; i<ReportRows.size(); i++)
      {
        EmailAddresses item = (EmailAddresses)ReportRows.elementAt(i);
        /*@lineinfo:generated-code*//*@lineinfo:310^9*/

//  ************************************************************
//  #sql [Ctx] { insert into email_addresses
//              	  ( email_id,
//                      address_type,
//                      address,
//                      enabled
//                    ) 
//                  values 
//                    ( :item.GroupId,
//                      :item.AddrType,
//                      :item.EmailAddress,
//                      :(groupMode.equals("EA")) ? "Y" : "N"
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1116 = (groupMode.equals("EA")) ? "Y" : "N";
   String theSqlTS = "insert into email_addresses\n            \t  ( email_id,\n                    address_type,\n                    address,\n                    enabled\n                  ) \n                values \n                  (  :1 ,\n                     :2 ,\n                     :3 ,\n                     :4 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.reports.EmailEditorDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,item.GroupId);
   __sJT_st.setInt(2,item.AddrType);
   __sJT_st.setString(3,item.EmailAddress);
   __sJT_st.setString(4,__sJT_1116);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:324^9*/
      }
      /*@lineinfo:generated-code*//*@lineinfo:326^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:329^7*/
    }
    catch( Exception e )
    {
      retVal = false;
      try{ /*@lineinfo:generated-code*//*@lineinfo:334^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:334^34*/ } catch( Exception ee ){}
      logEntry( "storeData()", e.toString() );
      try{ /*@lineinfo:generated-code*//*@lineinfo:336^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:336^32*/ }catch( Exception ee){}   
      System.out.println("storeData(): " + e.toString());
    }
    finally
    {
      cleanUp(); 
    }
    return (retVal);
  }
  
  public void processEmailData( int groupId, int type, String emailAddress )
  {
    String          address     = null;
    StringTokenizer tokens      = new StringTokenizer(emailAddress,";");
    
    while( tokens.hasMoreTokens() )
    {
      address = tokens.nextToken();
      ReportRows.addElement( new EmailAddresses(groupId, type, address) );
    }
  }

     
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    Field         field;
    String       emailId  = HttpHelper.getString(request, "email_id", null);

    super.createFields(request);

    fields.add( new DropDownField( "email_id", "Email Group", new EmailGroupTable(), true) );
    fields.add( new Field( "group_name", "Group name (required)",32, 32, false));
    fields.add( new DropDownField( "group_mode", "Mode", new GroupModeTable(), true) );
    fields.add( new EmailField( "from_address", "From (required)", 256, 64, false));
    fields.add( new EmailField( "to_address", "To (required)", 256, 64, false));
    fields.add( new EmailField( "cc_address", "Cc", 256, 64, true));
    fields.add( new EmailField( "bcc_address", "Bcc", 256, 64, true));
    fields.add( new ButtonField( "btnSave", "Save" ) );
    Condition condition;
    condition = new NotAddGroupCondition(emailId);
    fields.getField("group_name").setOptionalCondition(condition);
  }  

  
  public void saveSettings()
  {
    String              emailId       = HttpHelper.getString(request, "email_id", null);
    String              mode          = HttpHelper.getString(request, "mode", null); 
    String              emailAddr     = HttpHelper.getString(request, "email_addr", null);
    int                 addrType      = HttpHelper.getInt(request, "addr_type", -1);
    boolean             changeMode    = HttpHelper.getBoolean(request, "changeMode", false);
    boolean             deleteEmail   = HttpHelper.getBoolean(request, "deleteEmail", false);
    
    if (changeMode == true && (emailId != null || !emailId.equals("")))
    {
      updateMode(emailId, mode);
    }
    else if (deleteEmail == true)
    {
      deleteEmail(emailId, addrType, emailAddr);
    }
  }
  
  
  public void loadData()
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    String              emailId       = HttpHelper.getString(request, "email_id", null);
    boolean             editEmail     = HttpHelper.getBoolean(request, "editEmail", false);
    
    try
    {
      ReportRows.clear();

      /*@lineinfo:generated-code*//*@lineinfo:412^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    em.address_type       as address_type,
//                    emt.description       as address_desc,
//                    em.address            as email_address,
//                    nvl(em.enabled,'N')   as enabled
//          from      email_addresses       em,
//  	              email_Addr_types      emt
//          where     email_id = :emailId and
//  	              emt.address_type = em.address_type 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    em.address_type       as address_type,\n                  emt.description       as address_desc,\n                  em.address            as email_address,\n                  nvl(em.enabled,'N')   as enabled\n        from      email_addresses       em,\n\t              email_Addr_types      emt\n        where     email_id =  :1  and\n\t              emt.address_type = em.address_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.EmailEditorDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,emailId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.EmailEditorDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:422^7*/
      
      resultSet = it.getResultSet();
      while( resultSet.next() )
      {
         ReportRows.addElement ( new EmailData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry( "loadData() ", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
    
    if (editEmail)
    {
      setEmailField();
    }
  }
  

  public void setEmailField()
  {
    StringBuffer        fromAddr      = new StringBuffer();
    StringBuffer        toAddr        = new StringBuffer();
    StringBuffer        ccAddr        = new StringBuffer();
    StringBuffer        bccAddr       = new StringBuffer();
   
    for (int i=0; i<ReportRows.size(); i++)
    {
      EmailData item = (EmailData)ReportRows.elementAt(i);
      
      switch(item.AddrType)
      {
        case EMAIL_FROM:
          fromAddr.append(item.EmailAddress);
          fromAddr.append(";");
          break;
    
        case EMAIL_TO:
          toAddr.append(item.EmailAddress);
          toAddr.append(";");
  	      break;
  	
        case EMAIL_CC:
          ccAddr.append(item.EmailAddress);
          ccAddr.append(";");
  	      break;
  	
        case EMAIL_BCC:
          bccAddr.append(item.EmailAddress);
          bccAddr.append(";");
  	      break;
  	
  	    default:
  	      break;
      }
    }
    setData("from_address", fromAddr.toString());
    setData("to_address", toAddr.toString());
    setData("cc_address", ccAddr.toString());
    setData("bcc_address", bccAddr.toString());
  }

}/*@lineinfo:generated-code*/