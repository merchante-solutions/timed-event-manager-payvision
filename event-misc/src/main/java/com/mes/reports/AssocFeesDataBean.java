/*@lineinfo:filename=AssocFeesDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AssocFeesDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 8/30/02 3:22p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class AssocFeesDataBean extends ReportSQLJBean
{
  public static final int           REC_ID_NEW        = 0;
  public class AssocServiceFee implements Comparable
  {
    public  String      AssocName                   = null;
    public  long        AssocNumber                 = 0L;
    public  boolean     LocalCashAdvance            = false;
    public  double      Rate                        = 0.0;
    public  int         RecordId                    = REC_ID_NEW;
    public  Date        ValidDateBegin              = null;
    public  Date        ValidDateEnd                = null;
    
    public AssocServiceFee( ResultSet     resultSet )
      throws java.sql.SQLException
    {
      AssocName         = resultSet.getString("assoc_name");
      AssocNumber       = resultSet.getLong("assoc_number");
      LocalCashAdvance  = !(resultSet.getInt("local_cash_adv") == 0);
      Rate              = resultSet.getDouble("rate");
      RecordId          = resultSet.getInt("rec_id");
      ValidDateBegin    = resultSet.getDate("valid_date_begin");
      ValidDateEnd      = resultSet.getDate("valid_date_end");
    }
    
    public AssocServiceFee( int recId, long assocNumber, double rate, boolean localCashAdv, java.util.Date validDateBegin )
    {
      AssocName         = getOrgName( hierarchyNodeToOrgId( assocNumber ) );
      AssocNumber       = assocNumber;
      LocalCashAdvance  = localCashAdv;
      Rate              = rate;
      RecordId          = recId;
      ValidDateEnd      = null;
      
      if ( validDateBegin == null )
      {
        ValidDateBegin    = null;
      }
      else
      {
        ValidDateBegin    = new java.sql.Date( validDateBegin.getTime() );
      }        
    }
    
    public int compareTo( Object obj )
    {
      AssocServiceFee     fee       = (AssocServiceFee)obj;
      int                 retVal    = 0;
      
      if ( (retVal = (int)(AssocNumber - fee.AssocNumber)) == 0 )
      {
        if ( (retVal = AssocName.compareTo(fee.AssocName)) == 0 )
        {
          if ( (retVal = ValidDateBegin.compareTo(fee.ValidDateBegin)) == 0 )
          {
            if ( (retVal = ValidDateEnd.compareTo(fee.ValidDateEnd)) == 0 )
            {
              retVal = (RecordId - fee.RecordId);
            }              
          }
        }
      }
      return( retVal );
    }
    
    public boolean isActive( )
    {
      boolean     retVal      = false;
      
      if ( ( ValidDateEnd != null ) &&
           !( ValidDateEnd.before(Calendar.getInstance().getTime()) ) )
      {
        retVal = true;
      }           
      return( retVal );
    }
    
    public boolean isValid( )
    { 
      boolean       retVal        = false;
      
      if( ( AssocNumber != 0L ) &&
          ( Rate >= 0.0 ) &&
          ( ValidDateBegin != null ) )
      {
        retVal = true;
      }
      return( retVal );
    }
  }
  
  private     boolean             ShowingInactive     = false;

  public AssocFeesDataBean( )
  {
  }
  
  protected void deleteFee( int id )
  {
    long              hierarchyNode = getReportHierarchyNode();
    ResultSetIterator it            = null;
    int               lastRecId     = REC_ID_NEW;
    ResultSet         resultSet     = null;
    
    try
    {
      setAutoCommit(false);
      
      /*@lineinfo:generated-code*//*@lineinfo:156^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  asf.rec_id              as rec_id
//          from    assoc_service_fees asf,
//                  assoc_service_fees asf2
//          where   asf2.rec_id = :id and
//                  asf.assoc_number = asf2.assoc_number and
//                  asf.rec_id <> :id
//          order by asf.valid_date_end desc, rec_id desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  asf.rec_id              as rec_id\n        from    assoc_service_fees asf,\n                assoc_service_fees asf2\n        where   asf2.rec_id =  :1  and\n                asf.assoc_number = asf2.assoc_number and\n                asf.rec_id <>  :2 \n        order by asf.valid_date_end desc, rec_id desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AssocFeesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,id);
   __sJT_st.setInt(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AssocFeesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        lastRecId = resultSet.getInt("rec_id");
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:173^7*/

//  ************************************************************
//  #sql [Ctx] { delete  
//          from    assoc_service_fees
//          where   rec_id = :id and
//                  assoc_number in 
//                    ( select assoc_number
//                      from    group_assoc
//                      where   group_number = :hierarchyNode )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  \n        from    assoc_service_fees\n        where   rec_id =  :1  and\n                assoc_number in \n                  ( select assoc_number\n                    from    group_assoc\n                    where   group_number =  :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.reports.AssocFeesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,id);
   __sJT_st.setLong(2,hierarchyNode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:182^7*/
      
      if ( lastRecId != REC_ID_NEW )
      {
        /*@lineinfo:generated-code*//*@lineinfo:186^9*/

//  ************************************************************
//  #sql [Ctx] { update  assoc_service_fees
//            set     valid_date_end = '31-DEC-9999'
//            where   rec_id = :lastRecId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  assoc_service_fees\n          set     valid_date_end = '31-DEC-9999'\n          where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.reports.AssocFeesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,lastRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:191^9*/
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:194^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("deleteFee()",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:202^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:202^34*/ } catch( Exception ee ){}
    }
    finally
    {
      try{ it.close(); } catch( Exception e ){}
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Assoc Number\",");
    line.append("\"Assoc Name\",");
    line.append("\"Rate\",");
    line.append("\"Local Cash Advance\",");
    line.append("\"Valid Date Begin\",");
    line.append("\"Valid Date End\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    AssocServiceFee   record = (AssocServiceFee)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.AssocNumber );
    line.append( ",\"" );
    line.append( record.AssocName );
    line.append( "\"," );
    line.append( record.Rate );
    line.append( "," );
    line.append( ( (record.LocalCashAdvance == true) ? "Yes" : "No" ) );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.ValidDateBegin,"MM/yyyy") );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.ValidDateEnd,"MM/yyyy") );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append("assoc_service_fees_");
    
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    long                          hierarchyNode     = getReportHierarchyNode();
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;

    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:274^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  asf.rec_id              as rec_id,
//                  asf.assoc_number        as assoc_number,
//                  o.org_name              as assoc_name,
//                  asf.fee_rate            as rate,
//                  decode(nvl(asf.local_cash_advance,'N'),
//                         'Y',1,0)         as local_cash_adv,
//                  asf.valid_date_begin    as valid_date_begin,
//                  asf.valid_date_end      as valid_date_end
//          from    group_assoc           ga,
//                  assoc_service_fees    asf,
//                  organization          o
//          where   ga.group_number = :hierarchyNode and
//                  asf.assoc_number = ga.assoc_number and
//                  o.org_group = ga.assoc_number
//          order by asf.assoc_number, asf.valid_date_begin, 
//                   asf.valid_date_end                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  asf.rec_id              as rec_id,\n                asf.assoc_number        as assoc_number,\n                o.org_name              as assoc_name,\n                asf.fee_rate            as rate,\n                decode(nvl(asf.local_cash_advance,'N'),\n                       'Y',1,0)         as local_cash_adv,\n                asf.valid_date_begin    as valid_date_begin,\n                asf.valid_date_end      as valid_date_end\n        from    group_assoc           ga,\n                assoc_service_fees    asf,\n                organization          o\n        where   ga.group_number =  :1  and\n                asf.assoc_number = ga.assoc_number and\n                o.org_group = ga.assoc_number\n        order by asf.assoc_number, asf.valid_date_begin, \n                 asf.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.AssocFeesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.AssocFeesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:292^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.add(new AssocServiceFee(resultSet));
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    int                         delId = REC_ID_NEW;
    AssocServiceFee             fee   = null;
    
    // load the default report properties
    super.setProperties( request );
    
    if ( HttpHelper.getBoolean(request,"saveItem",false) == true )
    {
      fee = new AssocServiceFee( HttpHelper.getInt(request,"recId",REC_ID_NEW),
                                 HttpHelper.getLong(request,"assocNumber",0L),
                                 HttpHelper.getDouble(request,"rate",0.0),
                                 HttpHelper.getBoolean(request,"localCashAdv",false),
                                 HttpHelper.getDate(request,"validDateBegin","MM/yyyy",null) );
      if ( fee.isValid() )
      {
        storeData(fee);
      }
    }
    
    if ( (delId = HttpHelper.getInt(request,"deleteId",REC_ID_NEW)) != REC_ID_NEW )
    {
      deleteFee( delId );
    }
    
    ShowingInactive = HttpHelper.getBoolean(request,"showInactive",false);
  }
  
  public boolean showingInactiveRates()
  {
    return( ShowingInactive );
  }
  
  public void storeData( )
  {
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      storeData( (AssocServiceFee) ReportRows.elementAt(i) );
    }
  }
  
  public void storeData( AssocServiceFee item )
  {
    Date            endDate         = null;
    long            hierarchyNode   = getReportHierarchyNode();
    int             matchCount      = 0;
    
    try
    {
      setAutoCommit(false);
      
      // check for matching records
      if ( item.ValidDateEnd == null )
      {
        Calendar    cal = Calendar.getInstance();
        
        // null will default to 12/31/9999
        cal.set(Calendar.MONTH,Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH,31);
        cal.set(Calendar.YEAR,9999);
        
        endDate = new java.sql.Date( cal.getTime().getTime() );
      }
      else
      {
        endDate = item.ValidDateEnd;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:380^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(asf.assoc_number) 
//          from    assoc_service_fees asf
//          where   asf.assoc_number      = :item.AssocNumber and
//                  asf.fee_rate          = :item.Rate and
//                  asf.valid_date_begin  = :item.ValidDateBegin and
//                  asf.valid_date_end    = :endDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(asf.assoc_number)  \n        from    assoc_service_fees asf\n        where   asf.assoc_number      =  :1  and\n                asf.fee_rate          =  :2  and\n                asf.valid_date_begin  =  :3  and\n                asf.valid_date_end    =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.AssocFeesDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,item.AssocNumber);
   __sJT_st.setDouble(2,item.Rate);
   __sJT_st.setDate(3,item.ValidDateBegin);
   __sJT_st.setDate(4,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   matchCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:388^7*/
      
      // don't waste entries in the table.  if the user made
      // a mistake and submitted an identical entry (i.e. 
      // browser refresh), the prevent duplication in the DB.
      if ( matchCount == 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:395^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(ga.assoc_number)  
//            from    group_assoc ga
//            where   ga.group_number = :hierarchyNode and
//                    ga.assoc_number = :item.AssocNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ga.assoc_number)   \n          from    group_assoc ga\n          where   ga.group_number =  :1  and\n                  ga.assoc_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.AssocFeesDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setLong(2,item.AssocNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   matchCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:401^9*/
        
        // if this association is not a child
        // of the current node, then don't allow
        // this row to be inserted.
        if ( matchCount > 0 )
        {
          // set the expiration date for any items
          // set to expire after the current records
          // beginning date.
          /*@lineinfo:generated-code*//*@lineinfo:411^11*/

//  ************************************************************
//  #sql [Ctx] { update  assoc_service_fees
//              set     valid_date_end = (:item.ValidDateBegin - 1)
//              where   assoc_number = :item.AssocNumber and
//                      valid_date_end >= :item.ValidDateBegin
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  assoc_service_fees\n            set     valid_date_end = ( :1  - 1)\n            where   assoc_number =  :2  and\n                    valid_date_end >=  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.reports.AssocFeesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,item.ValidDateBegin);
   __sJT_st.setLong(2,item.AssocNumber);
   __sJT_st.setDate(3,item.ValidDateBegin);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:417^11*/
    
          // get a new record id for this new entry
          /*@lineinfo:generated-code*//*@lineinfo:420^11*/

//  ************************************************************
//  #sql [Ctx] { select  assoc_service_fee_sequence.nextval  
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  assoc_service_fee_sequence.nextval   \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.AssocFeesDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   item.RecordId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:424^11*/
      
          // insert the new record into the database
          /*@lineinfo:generated-code*//*@lineinfo:427^11*/

//  ************************************************************
//  #sql [Ctx] { insert into assoc_service_fees
//                ( rec_id,
//                  assoc_number,
//                  fee_rate,
//                  local_cash_advance,
//                  valid_date_begin
//                  -- valid_date_end
//                ) 
//              values 
//                ( :item.RecordId,
//                  :item.AssocNumber,
//                  :item.Rate, 
//                  :(item.LocalCashAdvance == true) ? "Y" : "N",
//                  to_date(to_char(:item.ValidDateBegin,'mm/yyyy'),'mm/yyyy')
//                  -- defaults to 12/31/9999
//                )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1107 = (item.LocalCashAdvance == true) ? "Y" : "N";
   String theSqlTS = "insert into assoc_service_fees\n              ( rec_id,\n                assoc_number,\n                fee_rate,\n                local_cash_advance,\n                valid_date_begin\n                -- valid_date_end\n              ) \n            values \n              (  :1 ,\n                 :2 ,\n                 :3 , \n                 :4 ,\n                to_date(to_char( :5 ,'mm/yyyy'),'mm/yyyy')\n                -- defaults to 12/31/9999\n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.reports.AssocFeesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,item.RecordId);
   __sJT_st.setLong(2,item.AssocNumber);
   __sJT_st.setDouble(3,item.Rate);
   __sJT_st.setString(4,__sJT_1107);
   __sJT_st.setDate(5,item.ValidDateBegin);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:445^11*/
        }          
      }        
      
      /*@lineinfo:generated-code*//*@lineinfo:449^7*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:449^27*/
    }
    catch( Exception e )
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:453^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:453^34*/ } catch( Exception ee ){}
      logEntry( "storeData()", e.toString() );
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/