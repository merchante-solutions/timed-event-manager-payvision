/*************************************************************************

  FILE: $Archive: $

  Description:  


  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;

public class ReportsDb extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(ReportsDb.class);

  static String IncCbCountsQs =
    " select  sum( case                   " + 
    "               when cb.incoming_date between ? and ? then 1 " +
    "               else 0                " +
    "              end)  as item_count,     " +
    "         sum( case                   " + 
    "               when cb.incoming_date between trunc(?,'month') and last_day(?) then 1 " +
    "               else 0                " +
    "              end)  as item_count_mtd, " +
    "         sum( case                   " + 
    "               when cb.incoming_date between trunc(?,'Q') and " +
    "                                             (round((trunc(?,'Q')+60),'Q')-1) then 1 " +
    "               else 0                " +
    "              end)  as item_count_qtd, " +
    "         count(1)   as item_count_ytd  " +
    " from    network_chargebacks cb      " +
    " where   cb.merchant_number = ?      " +
    "         and cb.incoming_date between (? - 365) and last_day(?) ";
    
  static String IncRetrCountsQs =
    " select  sum( case                     " + 
    "               when rt.incoming_date between ? and ? then 1 " +
    "               else 0                  " +
    "              end)  as item_count,     " +
    "         sum( case                     " + 
    "               when rt.incoming_date between trunc(?,'month') and last_day(?) then 1 " +
    "               else 0                  " +
    "              end)  as item_count_mtd, " +
    "         sum( case                     " + 
    "               when rt.incoming_date between trunc(?,'Q') and " +
    "                                             (round((trunc(?,'Q')+60),'Q')-1) then 1 " +
    "               else 0                  " +
    "              end)  as item_count_qtd, " +
    "         count(1)   as item_count_ytd  " +
    " from    network_retrievals    rt      " +
    " where   rt.merchant_number = ?        " +
    "         and rt.incoming_date between (? - 365) and last_day(?) ";
    
  static String BankSalesQs =
    " select  nvl( sum( case                            " + 
    "               when sm.batch_date between ? and ?  " +
    "                 then sm.bank_sales_count          " +
    "               else 0                              " +
    "              end), 0 )        as sales_count,     " +
    "         nvl( sum( case                            " + 
    "               when sm.batch_date between ? and ?  " +
    "                 then sm.bank_sales_amount         " +
    "               else 0                              " +
    "              end), 0 )        as sales_amount,    " +
    "         nvl( sum( case                                 " + 
    "               when sm.batch_date between trunc(?,'month') and last_day(?) " +
    "                 then sm.bank_sales_count          " +
    "               else 0                              " +
    "              end),0 )         as sales_count_mtd, " +
    "         nvl( sum( case                                 " + 
    "               when sm.batch_date between trunc(?,'month') and last_day(?) " +
    "                 then sm.bank_sales_amount         " +
    "               else 0                              " +
    "              end),0)          as sales_amount_mtd," +
    "         nvl( sum( case                                 " + 
    "               when sm.batch_date between trunc(?,'Q') and " +
    "                          (round((trunc(?,'Q')+60),'Q')-1) " +
    "                 then sm.bank_sales_count          " +
    "               else 0                              " +
    "              end),0 )         as sales_count_qtd,  " +
    "         nvl( sum( case                                 " + 
    "               when sm.batch_date between trunc(?,'Q') and " +
    "                          (round((trunc(?,'Q')+60),'Q')-1) " +
    "                 then sm.bank_sales_amount         " +
    "               else 0                              " +
    "              end),0 )         as sales_amount_qtd," +
    "         nvl(sum(sm.bank_sales_count),0)   as sales_count_ytd,  " +
    "         nvl(sum(sm.bank_sales_amount),0)  as sales_amount_ytd  " +
    " from    daily_detail_file_ext_summary sm    " +
    " where   sm.merchant_number = ?              " +
    "         and sm.batch_date between (? - 365) and last_day(?) ";

  public static SalesStatistic loadChargebackStatistics( long merchantId, Date beginDate, Date endDate )
  {
    return( (new ReportsDb()).salesStatisticLoad(IncCbCountsQs,merchantId, beginDate, endDate, true) );
  }
  
  public static SalesStatistic loadRetrievalStatistics( long merchantId, Date beginDate, Date endDate )
  {
    return( (new ReportsDb()).salesStatisticLoad(IncRetrCountsQs,merchantId, beginDate, endDate, true) );
  }
  
  public SalesStatistic salesStatisticLoad( String qs, long merchantId, Date beginDate, Date endDate, boolean loadSalesData )
  {
    PreparedStatement     ps          = null;
    ResultSet             rs          = null;
    SalesStatistic        stats       = null;
    
    try
    {
      connect();
      
      stats = new SalesStatistic();
      
      // get the next sequence value
      ps = con.prepareStatement(qs);
      ps.setDate(1,beginDate);
      ps.setDate(2,endDate);
      ps.setDate(3,endDate);
      ps.setDate(4,endDate);
      ps.setDate(5,endDate);
      ps.setDate(6,endDate);
      ps.setLong(7,merchantId);
      ps.setDate(8,endDate);
      ps.setDate(9,endDate);
      rs = ps.executeQuery();
      if(rs.next())
      {
        stats.setCount( rs.getInt("item_count") );
        stats.setCountMTD( rs.getInt("item_count_mtd") );
        stats.setCountQTD( rs.getInt("item_count_qtd") );
        stats.setCountYTD( rs.getInt("item_count_ytd") );
      }        
      rs.close();
      ps.close();
      
      if ( loadSalesData )
      {
        ps = con.prepareStatement( BankSalesQs );
        ps.setDate(1,beginDate);    // begin/end for sales count/amount
        ps.setDate(2,endDate);
        ps.setDate(3,beginDate);
        ps.setDate(4,endDate);
        ps.setDate(5,endDate);      // begin/end for mtd sales count/amount
        ps.setDate(6,endDate);
        ps.setDate(7,endDate);
        ps.setDate(8,endDate);
        ps.setDate(9,endDate);      // begin/end for qtd sales count/amount
        ps.setDate(10,endDate);
        ps.setDate(11,endDate);
        ps.setDate(12,endDate);
        ps.setLong(13,merchantId);   // mid
        ps.setDate(14,endDate);      // full query range
        ps.setDate(15,endDate);
        rs = ps.executeQuery();
        if( rs.next() )
        {
          stats.setVMCSalesCount( rs.getInt("sales_count") );
          stats.setVMCSalesCountMTD( rs.getInt("sales_count_mtd") );
          stats.setVMCSalesCountQTD( rs.getInt("sales_count_qtd") );
          stats.setVMCSalesCountYTD( rs.getInt("sales_count_ytd") );
        }
        rs.close();
        ps.close();
      }        
    }
    catch( Exception e )
    {
      logEntry("salesStatisticLoad(" + merchantId + "," + beginDate + "," + endDate + ")",e.toString());
    }
    finally
    {
      cleanUp(ps,rs); 
    }
    return( stats );
  }
}

