/*@lineinfo:filename=ChargebackActivityDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ChargebackActivityDataBean.sqlj $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-09-12 14:42:06 -0700 (Wed, 12 Sep 2007) $
  Version            : $Revision: 14136 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesChargebacks;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class ChargebackActivityDataBean extends ReportSQLJBean
{
  public  static final int    CB_NONE                 = -1;
  public  static final int    CB_INCOMING             = 0;
  public  static final int    CB_WORKED               = 1;
  public  static final int    CB_REWORKED             = 2;
  public  static final int    CB_DOUBLE_WORKED        = 3;
  public  static final int    CB_PENDING              = 4;

  public  static final int    RT_BALANCE_SHEET        = (RT_USER+1);
  public  static final int    RT_NET_OFFSET_DETAILS   = (RT_USER+2);

  public class AchDetailRow
  {
    public  double            AchAmount     = 0.0;
    public  String            AchType       = null;
    public  String            DbaName       = null;
    public  long              MerchantId    = 0L;
    public  Date              PostDate      = null;

    public AchDetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      // achAmount is merchant account offset,
      // reversing the sign will show the bank offset
      AchAmount   = -(resultSet.getDouble("ach_amount"));
      AchType     = resultSet.getString("ach_type");
      MerchantId  = resultSet.getLong("merchant_number");
      DbaName     = resultSet.getString("dba_name");
      PostDate    = resultSet.getDate("post_date");
    }
  }

  public class WorkedDetailRow
  {
    public  String            AchType         = null;
    public  String            CardNumber      = null;
    public  String            CardNumberEnc   = null;
    public  long              ControlNumber   = 0L;
    public  String            DbaName         = null;
    public  String            FirstTime       = null;
    public  Date              IncomingDate    = null;
    public  long              MerchantId      = 0L;
    public  String            ReferenceNumber = null;
    public  Date              TranDate        = null;
    public  double            WorkedAmount    = 0.0;
    public  String            WorkedAction    = null;
    public  Date              WorkedDate      = null;

    public WorkedDetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ControlNumber   = resultSet.getLong("control_number");
      WorkedAmount    = resultSet.getDouble("worked_amount");
      WorkedDate      = resultSet.getDate("action_date");
      WorkedAction    = resultSet.getString("action_code");
      MerchantId      = resultSet.getLong("merchant_number");
      CardNumber      = resultSet.getString("card_number");
      DbaName         = resultSet.getString("dba_name");
      TranDate        = resultSet.getDate("tran_date");
      IncomingDate    = resultSet.getDate("incoming_date");
      ReferenceNumber = resultSet.getString("ref_num");
      FirstTime       = resultSet.getString("first_time");
      CardNumberEnc   = resultSet.getString("card_number_enc");
    }
  }

  public class RowData
  {
    public long       ControlNumber       = 0L;
    public String     CardNumber          = null;
    public String     CardNumberEnc       = null;
    public String     DbaName             = null;
    public Date       DTBatchDate         = null;
    public long       DTBatchNumber       = 0L;
    public long       DTID                = 0L;
    public int        DaysOut             = 0;
    public String     FirstTime           = null;
    public long       HierarchyNode       = 0L;
    public Date       IncomingDate        = null;
    public String     PortfolioName       = null;
    public long       PortfolioNode       = 0L;
    public String     ReasonCode          = null;
    public String     ReferenceNumber     = null;
    public int        RowType             = CB_NONE;
    public double     TranAmount          = 0.0;
    public String     WorkedCode          = null;
    public Date       WorkedDate          = null;
    public String     WorkedFilename      = null;
    public String     WorkedSource        = null;
    public String     WorkedUser          = null;

    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ControlNumber   = resultSet.getLong("control_number");
      HierarchyNode   = resultSet.getLong("merchant_number");
      DbaName         = resultSet.getString("dba_name");
      IncomingDate    = resultSet.getDate("incoming_date");
      CardNumber      = resultSet.getString("card_number");
      TranAmount      = resultSet.getDouble("tran_amount");
      ReferenceNumber = resultSet.getString("ref_num");
      ReasonCode      = resultSet.getString("reason_code");
      FirstTime       = resultSet.getString("first_time");
      PortfolioNode   = resultSet.getLong("portfolio_node");
      PortfolioName   = resultSet.getString("portfolio_name");
      CardNumberEnc   = resultSet.getString("card_number_enc");

      DTBatchDate     = resultSet.getDate("dt_batch_date");
      DTBatchNumber   = resultSet.getLong("dt_batch_number");
      DTID            = resultSet.getLong("dt_ddf_dt_id");

      // default
      RowType = resultSet.getInt("row_type");

      switch( RowType )
      {
        case CB_WORKED:
        case CB_REWORKED:
        case CB_DOUBLE_WORKED:
          WorkedDate      = resultSet.getDate("worked_date");
          WorkedCode      = resultSet.getString("worked_code");
          WorkedFilename  = resultSet.getString("load_filename");
          WorkedSource    = resultSet.getString("action_source");
          WorkedUser      = resultSet.getString("user_login");
          break;

        case CB_PENDING:
          DbaName = resultSet.getString("dba_name");
          DaysOut = resultSet.getInt("days_out");
          RowType = CB_PENDING;
          break;

        default:
          break;
      }
    }
  }

  public class ReportTotals
  {
    public double        TotalAmount     = 0.0;
    public int           TotalCount      = 0;

    public ReportTotals( )
    {
    }

    public void add( RowData row )
    {
      TotalAmount += row.TranAmount;
      TotalCount++;
    }

    public void clear( )
    {
      TotalAmount = 0.0;
      TotalCount  = 0;
    }
  }

  public class SummaryData
    implements Comparable
  {
    public  double          AchAmount               = 0.0;
    public  int             AchCount                = 0;
    public  long            HierarchyNode           = 0L;
    public  long            LastLoadSec             = 0L;
    public  double          NetWorked               = 0.0;
    public  long            OrgId                   = 0L;
    public  String          OrgName                 = null;
    public  double          PrevPendingAmount       = 0.0;
    public  int             PrevPendingCount        = 0;
    public  Date            ReportDate              = null;
    public  double          TotDoubleWorkedAmount   = 0.0;
    public  int             TotDoubleWorkedCount    = 0;
    public  double          TotIncomingAmexAmount   = 0.0;
    public  int             TotIncomingAmexCount    = 0;
    public  double          TotIncomingDiscAmount   = 0.0;
    public  int             TotIncomingDiscCount    = 0;
    public  double          TotIncomingAmount       = 0.0;
    public  int             TotIncomingCount        = 0;
    public  double          TotIncomingPreArbAmount = 0.0;
    public  int             TotIncomingPreArbCount  = 0;
    public  double          TotPendingAmount        = 0.0;
    public  int             TotPendingCount         = 0;
    public  double          TotReworkedAmount       = 0.0;
    public  int             TotReworkedCount        = 0;
    public  double          TotWorkedAmount         = 0.0;
    public  int             TotWorkedCount          = 0;
    public  double          TotWorkedAmexAmount     = 0.0;
    public  int             TotWorkedAmexCount      = 0;
    public  double          TotWorkedDiscAmount     = 0.0;
    public  int             TotWorkedDiscCount      = 0;

    public  boolean         UnassignedOnly          = false;

    public SummaryData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      setData(resultSet);
    }

    public SummaryData( ResultSet resultSet, boolean unassigned )
      throws java.sql.SQLException
    {
      UnassignedOnly = unassigned;
      setData(resultSet);
    }

    public int compareTo( Object obj )
    {
      SummaryData     compareObj    = (SummaryData)obj;
      int             retVal        = 0;

      if ( (retVal = OrgName.compareTo(compareObj.OrgName)) == 0 )
      {
        if ( (retVal = (int)(HierarchyNode - compareObj.HierarchyNode)) == 0 )
        {
          retVal = (int)(OrgId - compareObj.OrgId);
        }
      }
      return( retVal );
    }

    public boolean isInBalance( )
    {
      boolean     retVal    = true;

      if ( getPendingCount() != TotPendingCount )
      {
        retVal = false;
      }
      return( retVal );
    }

    public double getNetWorked( )
    {
      return( NetWorked );
    }

    public double getPendingAmount( )
    {
      return( PrevPendingAmount + TotIncomingAmount + TotIncomingAmexAmount + TotIncomingDiscAmount - TotWorkedAmount - TotWorkedAmexAmount - TotWorkedDiscAmount + TotReworkedAmount + TotDoubleWorkedAmount );
    }

    public int getPendingCount( )
    {
      return( PrevPendingCount + TotIncomingCount + TotIncomingAmexCount + TotIncomingDiscCount - TotWorkedCount - TotWorkedAmexCount - TotWorkedDiscCount + TotReworkedCount + TotDoubleWorkedCount );
    }

    private void setData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      OrgId             = resultSet.getLong("org_num");
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      OrgName           = resultSet.getString("org_name");
      if ( OrgName == null ) { OrgName = "Unassigned"; }
      ReportDate        = resultSet.getDate("report_date");

      // only load the balance sheet data if the
      // user requested it (eliminates previous query)
      if ( ReportType == RT_BALANCE_SHEET )
      {
        loadPrevPendingTotals( this );
      }
    }

    protected void setDoubleWorked( ResultSet resultSet )
      throws java.sql.SQLException
    {
      long                loadSec     = 0L;

      loadSec = resultSet.getLong("control_number");

      // skip the first for each load sec.  all the rest
      // are duplicate entries
      if ( loadSec == LastLoadSec )
      {
        TotDoubleWorkedCount++;
        TotDoubleWorkedAmount += resultSet.getDouble("cb_amount");
      }
      LastLoadSec = loadSec;
    }

    protected void setIncoming( ResultSet resultSet )
      throws java.sql.SQLException
    {
      TotIncomingAmount       = resultSet.getDouble("tot_incoming_amount");
      TotIncomingCount        = resultSet.getInt("tot_incoming_count");
      TotIncomingAmexAmount   = resultSet.getDouble("tot_incoming_amex_amount");
      TotIncomingAmexCount    = resultSet.getInt("tot_incoming_amex_count");
      TotIncomingDiscAmount   = resultSet.getDouble("tot_incoming_disc_amount");
      TotIncomingDiscCount    = resultSet.getInt("tot_incoming_disc_count");
      TotIncomingPreArbAmount = resultSet.getDouble("tot_incoming_prearb_amount");
      TotIncomingPreArbCount  = resultSet.getInt("tot_incoming_prearb_count");
    }

    protected void setPending( ResultSet resultSet )
      throws java.sql.SQLException
    {
      TotPendingAmount  = resultSet.getDouble("tot_pending_amount");
      TotPendingCount   = resultSet.getInt("tot_pending_count");
    }

    protected void setReworked( ResultSet resultSet )
      throws java.sql.SQLException
    {
      TotReworkedAmount = resultSet.getDouble("tot_worked_amount");
      TotReworkedCount  = resultSet.getInt("tot_worked_count");
    }

    protected void setWorked( ResultSet resultSet )
      throws java.sql.SQLException
    {
      TotWorkedAmount   = resultSet.getDouble("tot_worked_amount");
      TotWorkedCount    = resultSet.getInt("tot_worked_count");
      
      TotWorkedAmexAmount = resultSet.getDouble("amex_worked_amount");
      TotWorkedAmexCount  = resultSet.getInt("amex_worked_count");
      
      TotWorkedDiscAmount = resultSet.getDouble("disc_worked_amount");
      TotWorkedDiscCount  = resultSet.getInt("disc_worked_count");

      NetWorked = (resultSet.getDouble("mchb_amount") -
                   resultSet.getDouble("remc_amount"));
    }

    public boolean isValid( )
    {
      boolean       retVal      = false;

      return( retVal );
    }
  }

  private Date            AchBatchDate      = null;
  private int             AchBatchId        = 0;
  private String          CardType          = null;
  private int             LastRowType       = CB_NONE;

  public ChargebackActivityDataBean( )
  {
  }

  protected void addData( ResultSet resultSet, int dataType )
  {
    addData(resultSet,dataType,false);
  }

  protected void addData( ResultSet resultSet, int dataType, boolean unassignedOnly )
  {
    SummaryData       data    = null;

    try
    {
      data = findSummaryData(resultSet);

      if ( data == null )
      {
        data = new SummaryData( resultSet, unassignedOnly );
        ReportRows.addElement( data );
      }

      switch( dataType )
      {
        case CB_INCOMING:
          data.setIncoming(resultSet);
          break;

        case CB_WORKED:
          data.setWorked(resultSet);
          break;

        case CB_REWORKED:
          data.setReworked(resultSet);
          break;

        case CB_DOUBLE_WORKED:
          data.setDoubleWorked(resultSet);
          break;

        case CB_PENDING:
          data.setPending(resultSet);
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("addData()",e.toString());
    }
  }

  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);

    switch( ReportType )
    {
      case RT_DETAILS:
        line.append("\"Control #\",");
        line.append("\"Incoming Date\",");
        line.append("\"Merchant #\",");
        line.append("\"DBA Name\",");
        line.append("\"Portfolio Node\",");
        line.append("\"Portfolio Name\",");
        line.append("\"Card Number\",");
        line.append("\"Reference #\",");
        line.append("\"Reason Code\",");
        line.append("\"First Time\",");
        line.append("\"Amount\",");
        line.append("\" \",");
        line.append("\" \",");
        line.append("\" \",");
        line.append("\" \",");
        line.append("\" \",");
        break;

      case RT_BALANCE_SHEET:
        line.append("\"Activity Date\",");
        line.append("\"Org ID\",");
        line.append("\"Org Name\",");
        line.append("\"Prev Pend Count\",");
        line.append("\"Prev Pend Amount\",");
        line.append("\"Incoming Count\",");
        line.append("\"Incoming Amount\",");
        line.append("\"Incoming Amex Count\",");
        line.append("\"Incoming Amex Amount\",");
        line.append("\"Pre-Arb Count\",");
        line.append("\"Pre-Arb Amount\",");
        line.append("\"Amex Worked Count\",");
        line.append("\"Amex Worked Amount\",");
        line.append("\"Worked Count\",");
        line.append("\"Worked Amount\",");
        line.append("\"Reworked Count\",");
        line.append("\"Reworked Amount\",");
        line.append("\"Net Worked\",");
        line.append("\"Net ACH\",");
        line.append("\"Pend Count\",");
        line.append("\"Pend Amount\"");
        break;

      case RT_SUMMARY:
        line.append("\"Org ID\",");
        line.append("\"Org Name\",");
        line.append("\"Incoming Count\",");
        line.append("\"Incoming Amount\",");
        line.append("\"Worked Count\",");
        line.append("\"Worked Amount\",");
        line.append("\"Re-Worked Count\",");
        line.append("\"Re-Worked Amount\",");
        line.append("\"Net Amount\"");
        break;
    }
  }

  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    // clear the line buffer and add
    // the merchant specific data.
    line.setLength(0);

    switch( ReportType )
    {
      case RT_DETAILS:
      {
        RowData       record    = (RowData)obj;

        if ( record.RowType != LastRowType )
        {
          switch( record.RowType )
          {
            case CB_INCOMING:
              line.append("\"INCOMING CHARGEBACKS\",,,,,,,,,,,,\n");
              break;
            case CB_WORKED:
              line.append("\"WORKED CHARGEBACKS\",,,,,,,,,,,,,,\n");
              break;
            case CB_REWORKED:
              line.append("\"REWORKED CHARGEBACKS\",,,,,,,,,,,,,,\n");
              break;
            case CB_DOUBLE_WORKED:
              line.append("\"DOUBLE WORKED CHARGEBACKS\",,,,,,,,,,,,,,\n");
              break;
            case CB_PENDING:
              line.append("\"PENDING CHARGEBACKS\",,,,,,,,,,,,\n");
              break;
          }
          LastRowType = record.RowType;
        }
        line.append( record.ControlNumber );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate(record.IncomingDate,"MM/dd/yyyy") );
        line.append( "," );
        line.append( encodeHierarchyNode(record.HierarchyNode) );
        line.append( ",\"" );
        line.append( record.DbaName );
        line.append( "\"," );
        line.append( encodeHierarchyNode(record.PortfolioNode) );
        line.append( ",\"" );
        line.append( record.PortfolioName );
        line.append( "\"," );
        line.append( record.CardNumber );
        line.append( ",\"'" );
        line.append( record.ReferenceNumber );
        line.append( "\"," );
        line.append( record.ReasonCode );
        line.append( ",\"" );
        line.append( record.FirstTime );
        line.append( "\"," );
        line.append( record.TranAmount );
        switch( record.RowType )
        {
          case CB_WORKED:
            line.append(",");
            line.append( DateTimeFormatter.getFormattedDate(record.WorkedDate,"MM/dd/yyyy") );
            line.append(",\"");
            line.append( record.WorkedCode );
            line.append("\",\"");
            line.append( record.WorkedSource );
            line.append( "\",\"" );
            line.append( record.WorkedUser );
            line.append( "\",\"" );
            line.append( record.WorkedFilename );
            line.append("\"");
            break;

          case CB_PENDING:
            line.append(",");
            line.append( record.DaysOut );
            line.append(",,");
            break;

          default:
          //case CB_INCOMING:
            line.append(",,,");
            break;
        }
        break;
      }

      case RT_BALANCE_SHEET:
      {
        SummaryData       summary   = (SummaryData)obj;

        // load the ach data for this entry
        loadNetOffsetEntry( summary );

        line.append( DateTimeFormatter.getFormattedDate( summary.ReportDate, "MM/dd/yyyy" ) );
        line.append( "," );
        line.append( encodeHierarchyNode( summary.HierarchyNode ) );
        line.append( ",\"" );
        line.append( summary.OrgName );
        line.append( "\"," );
        line.append( summary.PrevPendingCount );
        line.append( "," );
        line.append( summary.PrevPendingAmount );
        line.append( "," );
        line.append( summary.TotIncomingCount );
        line.append( "," );
        line.append( summary.TotIncomingAmount );
        line.append( "," );
        line.append( summary.TotIncomingAmexCount );
        line.append( "," );
        line.append( summary.TotIncomingAmexAmount );
        line.append( "," );
        line.append( summary.TotIncomingPreArbCount );
        line.append( "," );
        line.append( summary.TotIncomingPreArbAmount );
        line.append( "," );
        line.append( summary.TotWorkedAmexCount );
        line.append( "," );
        line.append( summary.TotWorkedAmexAmount );
        line.append( "," );
        line.append( summary.TotWorkedCount );
        line.append( "," );
        line.append( summary.TotWorkedAmount );
        line.append( "," );
        line.append( summary.TotReworkedCount );
        line.append( "," );
        line.append( summary.TotReworkedAmount );
        line.append( "," );
        line.append( summary.getNetWorked() );
        line.append( "," );
        line.append( summary.AchAmount );
        line.append( "," );
        line.append( summary.getPendingCount() );
        line.append( "," );
        line.append( summary.getPendingAmount() );
        break;
      }

      case RT_SUMMARY:
      {
        SummaryData       summary   = (SummaryData)obj;
        
        double            totIncomingAmount = 0.0;
        int               totIncomingCount  = 0;
        double            totWorkedAmount   = 0.0;
        int               totWorkedCount    = 0;
        double            totReworkedAmount = 0.0;
        int               totReworkedCount  = 0;

        line.append( encodeHierarchyNode( summary.HierarchyNode ) );
        line.append( ",\"" );
        line.append( summary.OrgName );
        line.append( "\"," );
        
        String cardType = ((CardType == null) ? "" : CardType);
        if ( cardType.equals(mesConstants.VITAL_ME_AP_AMEX) )
        {
          totIncomingCount  = summary.TotIncomingAmexCount;
          totIncomingAmount = summary.TotIncomingAmexAmount;
          totWorkedCount    = summary.TotWorkedAmexCount;
          totWorkedAmount   = summary.TotWorkedAmexAmount;
          totReworkedCount  = 0;
          totReworkedAmount = 0.0;
        }
        else if ( cardType.equals(mesConstants.VITAL_ME_AP_DISC) )
        {
          totIncomingCount  = summary.TotIncomingDiscCount;
          totIncomingAmount = summary.TotIncomingDiscAmount;
          totWorkedCount    = summary.TotWorkedDiscCount;
          totWorkedAmount   = summary.TotWorkedDiscAmount;
          totReworkedCount  = 0;
          totReworkedAmount = 0.0;
        }
        else
        {
          // default incoming/worked/reworked values
          totIncomingCount  = summary.TotIncomingCount;
          totIncomingAmount = summary.TotIncomingAmount;
          totWorkedCount    = summary.TotWorkedCount;
          totWorkedAmount   = summary.TotWorkedAmount;
          totReworkedCount  = summary.TotReworkedCount;
          totReworkedAmount = summary.TotReworkedAmount;
        }
        
        line.append( totIncomingCount );
        line.append( "," );
        line.append( totIncomingAmount );
        line.append( "," );
        line.append( totWorkedCount );
        line.append( "," );
        line.append( totWorkedAmount );
        line.append( "," );
        line.append( totReworkedCount );
        line.append( "," );
        line.append( totReworkedAmount );
        line.append( "," );
        line.append( totIncomingAmount - totWorkedAmount );
        break;
      }
    }
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl(buffer,nodeId,beginDate,endDate);
    if ( CardType != null )
    {
      buffer.append("&cardType=");
      buffer.append(CardType);
    }
  }

  protected SummaryData findSummaryData( ResultSet resultSet )
  {
    long              orgId       = 0L;
    SummaryData       retVal      = null;
    SummaryData       temp        = null;

    try
    {
      orgId = resultSet.getLong("org_num");

      for( int i = 0; i < ReportRows.size(); ++i )
      {
        temp = (SummaryData)ReportRows.elementAt(i);
        if ( temp.OrgId == orgId )
        {
          retVal = temp;
          break;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("findSummaryData()",e.toString());
    }
    return( retVal );
  }

  public int getAchBatchId()
  {
    return( AchBatchId );
  }

  public Date getAchBatchDate()
  {
    return( AchBatchDate );
  }
  
  public String getDisplayName( long node )
  {
    StringBuffer  buffer = new StringBuffer( super.getDisplayName(node) );
    
    if ( CardType != null )
    {
      buffer.append(" (");
      buffer.append( mesConstants.getVitalExtractCardTypeDesc(CardType) );
      buffer.append(" Only)");
    }
    return( buffer.toString() );
  }

  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");

    filename.append( getReportHierarchyNode() );
    filename.append("_cb_activity_");

    switch( ReportType )
    {
      case RT_DETAILS:
        filename.append("details_");
        break;

      case RT_SUMMARY:
        filename.append("summary_");
        break;

      case RT_BALANCE_SHEET:
        filename.append("bal_sheet_");
        break;
    }

    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }
    return ( filename.toString() );
  }

  public ReportTotals getRowTypeTotals( int rowType )
  {
    RowData       row       = null;
    ReportTotals  totals    = new ReportTotals();

    for ( int i = 0; i < ReportRows.size(); ++i )
    {
      row = (RowData) ReportRows.elementAt(i);
      if( row.RowType == rowType )
      {
        totals.add(row);
      }
    }
    return(totals);
  }

  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }

  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    switch( ReportType )
    {
      case RT_DETAILS:
        loadDetailData( orgId, beginDate, endDate );
        break;

      case RT_SUMMARY:
      case RT_BALANCE_SHEET:
        loadSummaryData( orgId, beginDate, endDate );
        break;

      case RT_NET_OFFSET_DETAILS:
        loadNetOffsetDetails();
        break;
    }
  }

  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    int                           bankNumber        = 0;
    ResultSetIterator             it                = null;
    long                          lastLoadSec       = 0L;
    long                          loadSec           = 0L;
    ResultSet                     resultSet         = null;

    try
    {
      // empty the current contents
      ReportRows.clear();

      // set the bank number if the
      // current node is a bank node
      if ( isOrgBank( orgId ) )
      {
        bankNumber = getOrgBankId( orgId );
      }

      //**************************************************************
      // INCOMING
      //**************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:870^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec            as control_number,
//                  cb.incoming_date          as incoming_date,
//                  cb.merchant_number        as merchant_number,
//                  nvl(mf.dba_name,
//                      cb.merchant_name)     as dba_name,
//                  o.org_group               as portfolio_node,
//                  o.org_name                as portfolio_name,
//                  cb.first_time_chargeback  as first_time,
//                  cb.card_number            as card_number,
//                  cb.card_number_enc        as card_number_enc,
//                  cb.reference_number       as ref_num,
//                  cb.reason_code            as reason_code,
//                  cb.tran_amount            as tran_amount,
//                  cb.dt_batch_date          as dt_batch_date,
//                  cb.dt_batch_number        as dt_batch_number,
//                  cb.dt_ddf_dt_id           as dt_ddf_dt_id,
//                  :CB_INCOMING              as row_type
//          from    network_chargebacks cb,
//                  mif                 mf,
//                  groups              g,
//                  organization        o
//          where   ( ( ( cb.merchant_number in
//                        ( select merchant_number
//                          from   group_merchant
//                          where  org_num = :orgId ) ) and
//                    ( :AppFilterUserId = -1 or
//                      cb.merchant_number in
//                        ( select  merchant_number
//                          from    group_rep_merchant
//                          where   user_id = :AppFilterUserId ) ) ) or
//                    ( cb.merchant_number = 0 and
//                      cb.bank_number = :bankNumber ) ) and
//                  cb.incoming_date between :beginDate and :endDate and
//                  ( :CardType is null or 
//                    decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                  mf.merchant_number(+) = cb.merchant_number and
//                  g.assoc_number(+) = mf.association_node and
//                  o.org_group(+) = g.group_2
//          order by cb.incoming_date, cb.merchant_number, cb.card_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec            as control_number,\n                cb.incoming_date          as incoming_date,\n                cb.merchant_number        as merchant_number,\n                nvl(mf.dba_name,\n                    cb.merchant_name)     as dba_name,\n                o.org_group               as portfolio_node,\n                o.org_name                as portfolio_name,\n                cb.first_time_chargeback  as first_time,\n                cb.card_number            as card_number,\n                cb.card_number_enc        as card_number_enc,\n                cb.reference_number       as ref_num,\n                cb.reason_code            as reason_code,\n                cb.tran_amount            as tran_amount,\n                cb.dt_batch_date          as dt_batch_date,\n                cb.dt_batch_number        as dt_batch_number,\n                cb.dt_ddf_dt_id           as dt_ddf_dt_id,\n                 :1               as row_type\n        from    network_chargebacks cb,\n                mif                 mf,\n                groups              g,\n                organization        o\n        where   ( ( ( cb.merchant_number in\n                      ( select merchant_number\n                        from   group_merchant\n                        where  org_num =  :2  ) ) and\n                  (  :3  = -1 or\n                    cb.merchant_number in\n                      ( select  merchant_number\n                        from    group_rep_merchant\n                        where   user_id =  :4  ) ) ) or\n                  ( cb.merchant_number = 0 and\n                    cb.bank_number =  :5  ) ) and\n                cb.incoming_date between  :6  and  :7  and\n                (  :8  is null or \n                  decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :9  ) and\n                mf.merchant_number(+) = cb.merchant_number and\n                g.assoc_number(+) = mf.association_node and\n                o.org_group(+) = g.group_2\n        order by cb.incoming_date, cb.merchant_number, cb.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,CB_INCOMING);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,CardType);
   __sJT_st.setString(9,CardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:911^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet

      //**************************************************************
      // WORKED
      //**************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:923^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec            as control_number,
//                  cb.incoming_date          as incoming_date,
//                  cb.merchant_number        as merchant_number,
//                  nvl(mf.dba_name,
//                      cb.merchant_number)   as dba_name,
//                  o.org_group               as portfolio_node,
//                  o.org_name                as portfolio_name,
//                  cb.first_time_chargeback  as first_time,
//                  cb.card_number            as card_number,
//                  cb.card_number_enc        as card_number_enc,
//                  cb.reason_code            as reason_code,
//                  cb.reference_number       as ref_num,
//                  cb.tran_amount            as tran_amount,
//                  cba.action_date           as worked_date,
//                  ac.action_code            as worked_code,
//                  cb.dt_batch_date          as dt_batch_date,
//                  cb.dt_batch_number        as dt_batch_number,
//                  cb.dt_ddf_dt_id           as dt_ddf_dt_id,
//                  decode( cba.action_source,
//                          :MesChargebacks.AS_TSYS,'TSYS',                         -- 1,'TSYS',
//                          :MesChargebacks.AS_AUTO_LIST,'Auto Action List',        -- 2,'Auto Action List',
//                          :MesChargebacks.AS_AUTO_REASON,'Auto Action Reason',    -- 3,'Auto Action Reason',
//                          :MesChargebacks.AS_WEB,'Web',                           -- 4,'Web',
//                          :MesChargebacks.AS_AUTO_REASON_BY_NODE,
//                            'Auto Action Reason By Node',                           -- 5,'Auto Action Reason By Node',
//                          to_char(cba.action_source)
//                        )                   as action_source,
//                  cba.user_login            as user_login,
//                  cba.load_filename         as load_filename,
//                  :CB_WORKED                as row_type
//          from    network_chargebacks           cb,
//                  network_chargeback_activity   cba,
//                  chargeback_action_codes       ac,
//                  mif                           mf,
//                  groups                        g,
//                  organization                  o
//          where   ( ( ( cb.merchant_number in
//                        ( select merchant_number
//                          from   group_merchant
//                          where  org_num = :orgId ) ) and
//                    ( :AppFilterUserId = -1 or
//                      cb.merchant_number in
//                        ( select  merchant_number
//                          from    group_rep_merchant
//                          where   user_id = :AppFilterUserId ) ) ) or
//                    ( cb.merchant_number = 0 and
//                      cb.bank_number = :bankNumber ) ) and
//                  ( :CardType is null or 
//                    decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.ACTION_DATE between :beginDate and :endDate and
//                  ac.short_action_code = cba.action_code and
//                  mf.merchant_number(+) = cb.merchant_number and
//                  g.assoc_number(+) = mf.association_node and
//                  o.org_group(+) = g.group_2
//          order by cb.incoming_date, cb.merchant_number, cb.card_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec            as control_number,\n                cb.incoming_date          as incoming_date,\n                cb.merchant_number        as merchant_number,\n                nvl(mf.dba_name,\n                    cb.merchant_number)   as dba_name,\n                o.org_group               as portfolio_node,\n                o.org_name                as portfolio_name,\n                cb.first_time_chargeback  as first_time,\n                cb.card_number            as card_number,\n                cb.card_number_enc        as card_number_enc,\n                cb.reason_code            as reason_code,\n                cb.reference_number       as ref_num,\n                cb.tran_amount            as tran_amount,\n                cba.action_date           as worked_date,\n                ac.action_code            as worked_code,\n                cb.dt_batch_date          as dt_batch_date,\n                cb.dt_batch_number        as dt_batch_number,\n                cb.dt_ddf_dt_id           as dt_ddf_dt_id,\n                decode( cba.action_source,\n                         :1 ,'TSYS',                         -- 1,'TSYS',\n                         :2 ,'Auto Action List',        -- 2,'Auto Action List',\n                         :3 ,'Auto Action Reason',    -- 3,'Auto Action Reason',\n                         :4 ,'Web',                           -- 4,'Web',\n                         :5 ,\n                          'Auto Action Reason By Node',                           -- 5,'Auto Action Reason By Node',\n                        to_char(cba.action_source)\n                      )                   as action_source,\n                cba.user_login            as user_login,\n                cba.load_filename         as load_filename,\n                 :6                 as row_type\n        from    network_chargebacks           cb,\n                network_chargeback_activity   cba,\n                chargeback_action_codes       ac,\n                mif                           mf,\n                groups                        g,\n                organization                  o\n        where   ( ( ( cb.merchant_number in\n                      ( select merchant_number\n                        from   group_merchant\n                        where  org_num =  :7  ) ) and\n                  (  :8  = -1 or\n                    cb.merchant_number in\n                      ( select  merchant_number\n                        from    group_rep_merchant\n                        where   user_id =  :9  ) ) ) or\n                  ( cb.merchant_number = 0 and\n                    cb.bank_number =  :10  ) ) and\n                (  :11  is null or \n                  decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :12  ) and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.ACTION_DATE between  :13  and  :14  and\n                ac.short_action_code = cba.action_code and\n                mf.merchant_number(+) = cb.merchant_number and\n                g.assoc_number(+) = mf.association_node and\n                o.org_group(+) = g.group_2\n        order by cb.incoming_date, cb.merchant_number, cb.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesChargebacks.AS_TSYS);
   __sJT_st.setInt(2,MesChargebacks.AS_AUTO_LIST);
   __sJT_st.setInt(3,MesChargebacks.AS_AUTO_REASON);
   __sJT_st.setInt(4,MesChargebacks.AS_WEB);
   __sJT_st.setInt(5,MesChargebacks.AS_AUTO_REASON_BY_NODE);
   __sJT_st.setInt(6,CB_WORKED);
   __sJT_st.setLong(7,orgId);
   __sJT_st.setLong(8,AppFilterUserId);
   __sJT_st.setLong(9,AppFilterUserId);
   __sJT_st.setInt(10,bankNumber);
   __sJT_st.setString(11,CardType);
   __sJT_st.setString(12,CardType);
   __sJT_st.setDate(13,beginDate);
   __sJT_st.setDate(14,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:981^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet

      //**************************************************************
      // REWORKED
      //**************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:993^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec            as control_number,
//                  cb.incoming_date          as incoming_date,
//                  cb.merchant_number        as merchant_number,
//                  o.org_group               as portfolio_node,
//                  o.org_name                as portfolio_name,
//                  nvl(mf.dba_name,
//                      cb.merchant_number)   as dba_name,
//                  cb.first_time_chargeback  as first_time,
//                  cb.card_number            as card_number,
//                  cb.card_number_enc        as card_number_enc,
//                  cb.reference_number       as ref_num,
//                  cb.reason_code            as reason_code,
//                  cb.tran_amount            as tran_amount,
//                  cba.action_date           as worked_date,
//                  ac.action_code            as worked_code,
//                  cb.dt_batch_date          as dt_batch_date,
//                  cb.dt_batch_number        as dt_batch_number,
//                  cb.dt_ddf_dt_id           as dt_ddf_dt_id,
//                  decode( cba.action_source,
//                          :MesChargebacks.AS_TSYS,'TSYS',                         -- 1,'TSYS',
//                          :MesChargebacks.AS_AUTO_LIST,'Auto Action List',        -- 2,'Auto Action List',
//                          :MesChargebacks.AS_AUTO_REASON,'Auto Action Reason',    -- 3,'Auto Action Reason',
//                          :MesChargebacks.AS_WEB,'Web',                           -- 4,'Web',
//                          :MesChargebacks.AS_AUTO_REASON_BY_NODE,
//                            'Auto Action Reason By Node',                           -- 5,'Auto Action Reason By Node',
//                          to_char(cba.action_source)
//                        )                   as action_source,
//                  cba.user_login            as user_login,
//                  cba.load_filename         as load_filename,
//                  :CB_REWORKED              as row_type
//          from    network_chargebacks             cb,
//                  network_chargeback_activity     cba,
//                  chargeback_action_codes         ac,
//                  mif                             mf,
//                  groups                          g,
//                  organization                    o
//          where   ( ( ( cb.merchant_number in
//                        ( select merchant_number
//                          from   group_merchant
//                          where  org_num = :orgId ) ) and
//                    ( :AppFilterUserId = -1 or
//                      cb.merchant_number in
//                        ( select  merchant_number
//                          from    group_rep_merchant
//                          where   user_id = :AppFilterUserId ) ) ) or
//                    ( cb.merchant_number = 0 and
//                      cb.bank_number = :bankNumber ) ) and
//                  ( :CardType is null or 
//                    decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.action_date between :beginDate and :endDate and
//                  exists
//                  (
//                    select  cbai.cb_load_sec
//                    from    network_chargeback_activity cbai
//                    where   cbai.cb_load_sec = cba.cb_load_sec and
//                            cbai.file_load_sec != cba.file_load_sec and
//                            cbai.action_date < cba.action_date
//                  ) and
//                  ac.short_action_code = cba.action_code and
//                  mf.merchant_number(+) = cb.merchant_number and
//                  g.assoc_number(+) = mf.association_node and
//                  o.org_group(+) = g.group_2
//          order by cb.incoming_date, cb.merchant_number, cb.card_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec            as control_number,\n                cb.incoming_date          as incoming_date,\n                cb.merchant_number        as merchant_number,\n                o.org_group               as portfolio_node,\n                o.org_name                as portfolio_name,\n                nvl(mf.dba_name,\n                    cb.merchant_number)   as dba_name,\n                cb.first_time_chargeback  as first_time,\n                cb.card_number            as card_number,\n                cb.card_number_enc        as card_number_enc,\n                cb.reference_number       as ref_num,\n                cb.reason_code            as reason_code,\n                cb.tran_amount            as tran_amount,\n                cba.action_date           as worked_date,\n                ac.action_code            as worked_code,\n                cb.dt_batch_date          as dt_batch_date,\n                cb.dt_batch_number        as dt_batch_number,\n                cb.dt_ddf_dt_id           as dt_ddf_dt_id,\n                decode( cba.action_source,\n                         :1 ,'TSYS',                         -- 1,'TSYS',\n                         :2 ,'Auto Action List',        -- 2,'Auto Action List',\n                         :3 ,'Auto Action Reason',    -- 3,'Auto Action Reason',\n                         :4 ,'Web',                           -- 4,'Web',\n                         :5 ,\n                          'Auto Action Reason By Node',                           -- 5,'Auto Action Reason By Node',\n                        to_char(cba.action_source)\n                      )                   as action_source,\n                cba.user_login            as user_login,\n                cba.load_filename         as load_filename,\n                 :6               as row_type\n        from    network_chargebacks             cb,\n                network_chargeback_activity     cba,\n                chargeback_action_codes         ac,\n                mif                             mf,\n                groups                          g,\n                organization                    o\n        where   ( ( ( cb.merchant_number in\n                      ( select merchant_number\n                        from   group_merchant\n                        where  org_num =  :7  ) ) and\n                  (  :8  = -1 or\n                    cb.merchant_number in\n                      ( select  merchant_number\n                        from    group_rep_merchant\n                        where   user_id =  :9  ) ) ) or\n                  ( cb.merchant_number = 0 and\n                    cb.bank_number =  :10  ) ) and\n                (  :11  is null or \n                  decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :12  ) and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.action_date between  :13  and  :14  and\n                exists\n                (\n                  select  cbai.cb_load_sec\n                  from    network_chargeback_activity cbai\n                  where   cbai.cb_load_sec = cba.cb_load_sec and\n                          cbai.file_load_sec != cba.file_load_sec and\n                          cbai.action_date < cba.action_date\n                ) and\n                ac.short_action_code = cba.action_code and\n                mf.merchant_number(+) = cb.merchant_number and\n                g.assoc_number(+) = mf.association_node and\n                o.org_group(+) = g.group_2\n        order by cb.incoming_date, cb.merchant_number, cb.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesChargebacks.AS_TSYS);
   __sJT_st.setInt(2,MesChargebacks.AS_AUTO_LIST);
   __sJT_st.setInt(3,MesChargebacks.AS_AUTO_REASON);
   __sJT_st.setInt(4,MesChargebacks.AS_WEB);
   __sJT_st.setInt(5,MesChargebacks.AS_AUTO_REASON_BY_NODE);
   __sJT_st.setInt(6,CB_REWORKED);
   __sJT_st.setLong(7,orgId);
   __sJT_st.setLong(8,AppFilterUserId);
   __sJT_st.setLong(9,AppFilterUserId);
   __sJT_st.setInt(10,bankNumber);
   __sJT_st.setString(11,CardType);
   __sJT_st.setString(12,CardType);
   __sJT_st.setDate(13,beginDate);
   __sJT_st.setDate(14,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1059^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet

      //**************************************************************
      // DOUBLE WORKED
      //**************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:1071^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec            as control_number,
//                  cb.incoming_date          as incoming_date,
//                  cb.merchant_number        as merchant_number,
//                  nvl(mf.dba_name,
//                      cb.merchant_number)   as dba_name,
//                  o.org_group               as portfolio_node,
//                  o.org_name                as portfolio_name,
//                  cb.first_time_chargeback  as first_time,
//                  cb.card_number            as card_number,
//                  cb.card_number_enc        as card_number_enc,
//                  cb.reference_number       as ref_num,
//                  cb.reason_code            as reason_code,
//                  cb.tran_amount            as tran_amount,
//                  cba.action_date           as worked_date,
//                  ac.action_code            as worked_code,
//                  cb.dt_batch_date          as dt_batch_date,
//                  cb.dt_batch_number        as dt_batch_number,
//                  cb.dt_ddf_dt_id           as dt_ddf_dt_id,
//                  decode( cba.action_source,
//                          :MesChargebacks.AS_TSYS,'TSYS',                         -- 1,'TSYS',
//                          :MesChargebacks.AS_AUTO_LIST,'Auto Action List',        -- 2,'Auto Action List',
//                          :MesChargebacks.AS_AUTO_REASON,'Auto Action Reason',    -- 3,'Auto Action Reason',
//                          :MesChargebacks.AS_WEB,'Web',                           -- 4,'Web',
//                          :MesChargebacks.AS_AUTO_REASON_BY_NODE,
//                            'Auto Action Reason By Node',                           -- 5,'Auto Action Reason By Node',
//                          to_char(cba.action_source)
//                        )                   as action_source,
//                  cba.user_login            as user_login,
//                  cba.load_filename         as load_filename,
//                  :CB_DOUBLE_WORKED         as row_type
//          from    network_chargebacks             cb,
//                  network_chargeback_activity     cba,
//                  chargeback_action_codes         ac,
//                  mif                             mf,
//                  groups                          g,
//                  organization                    o
//          where   ( ( ( cb.merchant_number in
//                        ( select merchant_number
//                          from   group_merchant
//                          where  org_num = :orgId ) ) and
//                    ( :AppFilterUserId = -1 or
//                      cb.merchant_number in
//                        ( select  merchant_number
//                          from    group_rep_merchant
//                          where   user_id = :AppFilterUserId ) ) ) or
//                    ( cb.merchant_number = 0 and
//                      cb.bank_number = :bankNumber ) ) and
//                  ( :CardType is null or 
//                    decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.action_date between :beginDate and :endDate and
//                  exists
//                  (
//                    select  cbai.cb_load_sec
//                    from    network_chargeback_activity cbai
//                    where   cbai.cb_load_sec = cba.cb_load_sec and
//                            cbai.file_load_sec != cba.file_load_sec and
//                            cbai.action_date = cba.action_date
//                  ) and
//                  ac.short_action_code = cba.action_code and
//                  mf.merchant_number(+) = cb.merchant_number and
//                  g.assoc_number(+) = mf.association_node and
//                  o.org_group(+) = g.group_2
//          order by cb.cb_load_sec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec            as control_number,\n                cb.incoming_date          as incoming_date,\n                cb.merchant_number        as merchant_number,\n                nvl(mf.dba_name,\n                    cb.merchant_number)   as dba_name,\n                o.org_group               as portfolio_node,\n                o.org_name                as portfolio_name,\n                cb.first_time_chargeback  as first_time,\n                cb.card_number            as card_number,\n                cb.card_number_enc        as card_number_enc,\n                cb.reference_number       as ref_num,\n                cb.reason_code            as reason_code,\n                cb.tran_amount            as tran_amount,\n                cba.action_date           as worked_date,\n                ac.action_code            as worked_code,\n                cb.dt_batch_date          as dt_batch_date,\n                cb.dt_batch_number        as dt_batch_number,\n                cb.dt_ddf_dt_id           as dt_ddf_dt_id,\n                decode( cba.action_source,\n                         :1 ,'TSYS',                         -- 1,'TSYS',\n                         :2 ,'Auto Action List',        -- 2,'Auto Action List',\n                         :3 ,'Auto Action Reason',    -- 3,'Auto Action Reason',\n                         :4 ,'Web',                           -- 4,'Web',\n                         :5 ,\n                          'Auto Action Reason By Node',                           -- 5,'Auto Action Reason By Node',\n                        to_char(cba.action_source)\n                      )                   as action_source,\n                cba.user_login            as user_login,\n                cba.load_filename         as load_filename,\n                 :6          as row_type\n        from    network_chargebacks             cb,\n                network_chargeback_activity     cba,\n                chargeback_action_codes         ac,\n                mif                             mf,\n                groups                          g,\n                organization                    o\n        where   ( ( ( cb.merchant_number in\n                      ( select merchant_number\n                        from   group_merchant\n                        where  org_num =  :7  ) ) and\n                  (  :8  = -1 or\n                    cb.merchant_number in\n                      ( select  merchant_number\n                        from    group_rep_merchant\n                        where   user_id =  :9  ) ) ) or\n                  ( cb.merchant_number = 0 and\n                    cb.bank_number =  :10  ) ) and\n                (  :11  is null or \n                  decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :12  ) and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.action_date between  :13  and  :14  and\n                exists\n                (\n                  select  cbai.cb_load_sec\n                  from    network_chargeback_activity cbai\n                  where   cbai.cb_load_sec = cba.cb_load_sec and\n                          cbai.file_load_sec != cba.file_load_sec and\n                          cbai.action_date = cba.action_date\n                ) and\n                ac.short_action_code = cba.action_code and\n                mf.merchant_number(+) = cb.merchant_number and\n                g.assoc_number(+) = mf.association_node and\n                o.org_group(+) = g.group_2\n        order by cb.cb_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesChargebacks.AS_TSYS);
   __sJT_st.setInt(2,MesChargebacks.AS_AUTO_LIST);
   __sJT_st.setInt(3,MesChargebacks.AS_AUTO_REASON);
   __sJT_st.setInt(4,MesChargebacks.AS_WEB);
   __sJT_st.setInt(5,MesChargebacks.AS_AUTO_REASON_BY_NODE);
   __sJT_st.setInt(6,CB_DOUBLE_WORKED);
   __sJT_st.setLong(7,orgId);
   __sJT_st.setLong(8,AppFilterUserId);
   __sJT_st.setLong(9,AppFilterUserId);
   __sJT_st.setInt(10,bankNumber);
   __sJT_st.setString(11,CardType);
   __sJT_st.setString(12,CardType);
   __sJT_st.setDate(13,beginDate);
   __sJT_st.setDate(14,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1137^7*/
      resultSet = it.getResultSet();

      lastLoadSec = 0L;
      while( resultSet.next() )
      {
        loadSec = resultSet.getLong("control_number");

        // skip the first for each load sec.  all the rest
        // are duplicate entries
        if ( loadSec == lastLoadSec )
        {
          ReportRows.addElement( new RowData( resultSet ) );
        }
        lastLoadSec = loadSec;
      }
      it.close();   // this will also close the resultSet

      //**************************************************************
      // PENDING -
      //**************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:1158^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec              as control_number,
//                  cb.incoming_date            as incoming_date,
//                  cb.merchant_number          as merchant_number,
//                  nvl(mf.dba_name,
//                      cb.merchant_name)       as dba_name,
//                  o.org_group                 as portfolio_node,
//                  o.org_name                  as portfolio_name,
//                  cb.first_time_chargeback    as first_time,
//                  cb.card_number              as card_number,
//                  cb.card_number_enc          as card_number_enc,
//                  cb.reference_number         as ref_num,
//                  cb.reason_code              as reason_code,
//                  cb.tran_amount              as tran_amount,
//                  (:endDate-cb.incoming_date) as days_out,
//                  cb.dt_batch_date            as dt_batch_date,
//                  cb.dt_batch_number          as dt_batch_number,
//                  cb.dt_ddf_dt_id             as dt_ddf_dt_id,
//                  :CB_PENDING                 as row_type
//          from    network_chargebacks         cb,
//                  mif                         mf,
//                  groups                      g,
//                  organization                o
//          where   ( ( ( cb.merchant_number in
//                        ( select  gm.merchant_number
//                          from    group_merchant    gm
//                          where   gm.org_num = :orgId ) ) and
//                    ( :AppFilterUserId = -1 or
//                      cb.merchant_number in
//                        ( select  merchant_number
//                          from    group_rep_merchant
//                          where   user_id = :AppFilterUserId ) ) ) or
//                    ( cb.merchant_number = 0 and
//                      cb.bank_number = :bankNumber ) ) and
//                  cb.incoming_date between (:beginDate-180) and :endDate and
//                  ( :CardType is null or 
//                    decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                  not exists
//                  (
//                    select cba.cb_load_sec
//                    from    network_chargeback_activity   cba
//                    where   cba.cb_load_sec = cb.cb_load_sec and
//                            cba.action_date <= :endDate
//                  ) and
//                  mf.merchant_number(+) = cb.merchant_number and
//                  g.assoc_number(+) = mf.association_node and
//                  o.org_group(+) = g.group_2
//          order by cb.incoming_date, cb.merchant_number, cb.card_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec              as control_number,\n                cb.incoming_date            as incoming_date,\n                cb.merchant_number          as merchant_number,\n                nvl(mf.dba_name,\n                    cb.merchant_name)       as dba_name,\n                o.org_group                 as portfolio_node,\n                o.org_name                  as portfolio_name,\n                cb.first_time_chargeback    as first_time,\n                cb.card_number              as card_number,\n                cb.card_number_enc          as card_number_enc,\n                cb.reference_number         as ref_num,\n                cb.reason_code              as reason_code,\n                cb.tran_amount              as tran_amount,\n                ( :1 -cb.incoming_date) as days_out,\n                cb.dt_batch_date            as dt_batch_date,\n                cb.dt_batch_number          as dt_batch_number,\n                cb.dt_ddf_dt_id             as dt_ddf_dt_id,\n                 :2                  as row_type\n        from    network_chargebacks         cb,\n                mif                         mf,\n                groups                      g,\n                organization                o\n        where   ( ( ( cb.merchant_number in\n                      ( select  gm.merchant_number\n                        from    group_merchant    gm\n                        where   gm.org_num =  :3  ) ) and\n                  (  :4  = -1 or\n                    cb.merchant_number in\n                      ( select  merchant_number\n                        from    group_rep_merchant\n                        where   user_id =  :5  ) ) ) or\n                  ( cb.merchant_number = 0 and\n                    cb.bank_number =  :6  ) ) and\n                cb.incoming_date between ( :7 -180) and  :8  and\n                (  :9  is null or \n                  decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :10  ) and\n                not exists\n                (\n                  select cba.cb_load_sec\n                  from    network_chargeback_activity   cba\n                  where   cba.cb_load_sec = cb.cb_load_sec and\n                          cba.action_date <=  :11 \n                ) and\n                mf.merchant_number(+) = cb.merchant_number and\n                g.assoc_number(+) = mf.association_node and\n                o.org_group(+) = g.group_2\n        order by cb.incoming_date, cb.merchant_number, cb.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,endDate);
   __sJT_st.setInt(2,CB_PENDING);
   __sJT_st.setLong(3,orgId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setLong(5,AppFilterUserId);
   __sJT_st.setInt(6,bankNumber);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   __sJT_st.setString(9,CardType);
   __sJT_st.setString(10,CardType);
   __sJT_st.setDate(11,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1207^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void loadNetOffsetBatchId( )
  {
    int               bankNumber      = 0;
    Date              beginDate       = null;
    ResultSetIterator it              = null;
    double            netEntry        = 0.0;
    ResultSet         resultSet       = null;

    try
    {
      bankNumber  = getReportBankId();
      beginDate   = getReportDateBegin();

      netEntry = loadNetWorkedAmount((bankNumber*100000L));

      /*@lineinfo:generated-code*//*@lineinfo:1241^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    ach.batch_id                               as batch_id,
//                    ach.post_date_option                       as ach_date,
//                    1                                          as ach_count,
//                    ( 
//                      ach.amount_of_transaction *
//                      decode(tc.debit_credit_indicator,'D',-1,1)
//                    )                                          as ach_amount,
//                    abs( ( ach.amount_of_transaction *
//                           decode(tc.debit_credit_indicator,'D',-1,1) ) -
//                         :netEntry )                           as amount_delta
//          from      ach_detail                ach,
//                    ach_detail_tran_code      tc
//          where     ach.post_date_option between (:beginDate+1) and (:beginDate+4) and
//                    ach.individual_name = 'NET OFFSET ENTRY' and
//                    substr(ach.load_filename,4,4) = to_char(:bankNumber) and
//                    tc.ach_detail_trans_code(+) = ach.transaction_code and
//                    ( ach.amount_of_transaction *
//                      decode(tc.debit_credit_indicator,'D',-1,1) ) between (:netEntry*0.5) and (:netEntry*1.5)
//          order by amount_delta
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ach.batch_id                               as batch_id,\n                  ach.post_date_option                       as ach_date,\n                  1                                          as ach_count,\n                  ( \n                    ach.amount_of_transaction *\n                    decode(tc.debit_credit_indicator,'D',-1,1)\n                  )                                          as ach_amount,\n                  abs( ( ach.amount_of_transaction *\n                         decode(tc.debit_credit_indicator,'D',-1,1) ) -\n                        :1  )                           as amount_delta\n        from      ach_detail                ach,\n                  ach_detail_tran_code      tc\n        where     ach.post_date_option between ( :2 +1) and ( :3 +4) and\n                  ach.individual_name = 'NET OFFSET ENTRY' and\n                  substr(ach.load_filename,4,4) = to_char( :4 ) and\n                  tc.ach_detail_trans_code(+) = ach.transaction_code and\n                  ( ach.amount_of_transaction *\n                    decode(tc.debit_credit_indicator,'D',-1,1) ) between ( :5 *0.5) and ( :6 *1.5)\n        order by amount_delta";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,netEntry);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setDouble(5,netEntry);
   __sJT_st.setDouble(6,netEntry);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1262^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        AchBatchId   = resultSet.getInt("batch_id");
        AchBatchDate = resultSet.getDate("ach_date");
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadNetOffsetBatchId()",e.toString() );
    }
    finally
    {
      try{ it.close(); } catch (Exception e) {}
    }
  }

  public void loadNetOffsetDetails( )
  {
    Date              beginDate       = ReportDateBegin;
    Calendar          cal             = Calendar.getInstance();
    int               inc             = 0;
    ResultSetIterator it              = null;
    long              orgId           = getReportOrgId();
    ResultSet         resultSet       = null;

    try
    {
      ReportRows.removeAllElements(); // reset rows
      loadNetOffsetBatchId();         // initialize the AchBatchId

      // load the chargebacks that did not make it into the ACH file
      /*@lineinfo:generated-code*//*@lineinfo:1297^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec                        as control_number,
//                  cb.merchant_number                    as merchant_number,
//                  mf.dba_name                           as dba_name,
//                  cb.incoming_date                      as incoming_date,
//                  cb.tran_date                          as tran_date,
//                  cb.first_time_chargeback              as first_time,
//                  cb.card_number                        as card_number,
//                  cb.card_number_enc                    as card_number_enc,
//                  cb.reference_number                   as ref_num,
//                  cb.reason_code                        as reason_code,
//                  (cb.tran_amount*
//                   decode(cba.action_code,'C',-1,1))
//                                                        as worked_amount,
//                  cba.action_date                       as action_date,
//                  ac.action_code                        as action_code
//          from    group_merchant                gm,
//                  network_chargebacks           cb,
//                  network_chargeback_activity   cba,
//                  chargeback_action_codes       ac,
//                  mif                           mf
//          where   gm.org_num = :orgId and
//                  cb.merchant_number = gm.merchant_number and
//                  cb.incoming_date between (:beginDate-180) and :beginDate and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.action_date = :beginDate and
//                  ac.short_action_code = cba.action_code and
//                  not ac.adj_tran_code is null and    -- financial only
//                  not exists
//                  (
//                    select  ach.merchant_number
//                    from    ach_detail            ach,
//                            ach_detail_tran_code  tc
//                    where   ach.merchant_number = cb.merchant_number and
//                            ach.post_date_option between (cba.action_date+1) and (cba.action_date+4) and
//                            tc.ach_detail_trans_code(+) = ach.transaction_code and
//                            ( ach.amount_of_transaction *
//                              decode(tc.debit_credit_indicator,'D',-1,1) ) =
//                            ( cb.tran_amount * decode(cba.action_code,'C',1,-1) )
//                  ) and
//                  mf.merchant_number = cb.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec                        as control_number,\n                cb.merchant_number                    as merchant_number,\n                mf.dba_name                           as dba_name,\n                cb.incoming_date                      as incoming_date,\n                cb.tran_date                          as tran_date,\n                cb.first_time_chargeback              as first_time,\n                cb.card_number                        as card_number,\n                cb.card_number_enc                    as card_number_enc,\n                cb.reference_number                   as ref_num,\n                cb.reason_code                        as reason_code,\n                (cb.tran_amount*\n                 decode(cba.action_code,'C',-1,1))\n                                                      as worked_amount,\n                cba.action_date                       as action_date,\n                ac.action_code                        as action_code\n        from    group_merchant                gm,\n                network_chargebacks           cb,\n                network_chargeback_activity   cba,\n                chargeback_action_codes       ac,\n                mif                           mf\n        where   gm.org_num =  :1  and\n                cb.merchant_number = gm.merchant_number and\n                cb.incoming_date between ( :2 -180) and  :3  and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.action_date =  :4  and\n                ac.short_action_code = cba.action_code and\n                not ac.adj_tran_code is null and    -- financial only\n                not exists\n                (\n                  select  ach.merchant_number\n                  from    ach_detail            ach,\n                          ach_detail_tran_code  tc\n                  where   ach.merchant_number = cb.merchant_number and\n                          ach.post_date_option between (cba.action_date+1) and (cba.action_date+4) and\n                          tc.ach_detail_trans_code(+) = ach.transaction_code and\n                          ( ach.amount_of_transaction *\n                            decode(tc.debit_credit_indicator,'D',-1,1) ) =\n                          ( cb.tran_amount * decode(cba.action_code,'C',1,-1) )\n                ) and\n                mf.merchant_number = cb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1339^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new WorkedDetailRow(resultSet) );
      }
      resultSet.close();
      it.close();

      // setup the number of days back to search for the
      // matching worked chargeback entry.
      cal.setTime( AchBatchDate );
      if ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.SUNDAY )
      {
        inc = 2;    // cb activity should could be 2 days back
      }
      else
      {
        inc = 1;    // default, activity was 1 day back
      }

      // load the additional ACH entries
      /*@lineinfo:generated-code*//*@lineinfo:1362^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ach.merchant_number                       as merchant_number,
//                  mf.dba_name                               as dba_name,
//                  ach.post_date_option                      as post_date,
//                  ( 
//                    ach.amount_of_transaction *
//                    decode(tc.debit_credit_indicator,'D',-1,1)
//                  )                                         as ach_amount,
//                  tc.ach_detail_tran_desc                   as ach_type
//          from    group_merchant        gm,
//                  ach_detail            ach,
//                  ach_detail_tran_code  tc,
//                  mif                   mf
//          where   gm.org_num = :orgId and
//                  ach.merchant_number = gm.merchant_number and
//                  ach.post_date_option = :AchBatchDate and
//                  ach.batch_id = :AchBatchId and
//                  ach.individual_name <> 'NET OFFSET ENTRY' and
//                  tc.ach_detail_trans_code(+) = ach.transaction_code and
//                  not exists
//                  (
//                    select  cb.merchant_number
//                    from    network_chargebacks           cb,
//                            network_chargeback_activity   cba
//                    where   cb.merchant_number = ach.merchant_number and
//                            cb.incoming_date between (ach.post_date_option-185) and ach.post_date_option and
//                            cba.cb_load_sec = cb.cb_load_sec and
//                            cba.action_date between ( ach.post_date_option - :inc ) and
//                                                    ( ach.post_date_option - 1 ) and
//                            ( cb.tran_amount * decode(cba.action_code,'C',1,-1) ) =
//                            ( ach.amount_of_transaction *
//                              decode(tc.debit_credit_indicator,'D',-1,1) )
//                  ) and
//                  mf.merchant_number = ach.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ach.merchant_number                       as merchant_number,\n                mf.dba_name                               as dba_name,\n                ach.post_date_option                      as post_date,\n                ( \n                  ach.amount_of_transaction *\n                  decode(tc.debit_credit_indicator,'D',-1,1)\n                )                                         as ach_amount,\n                tc.ach_detail_tran_desc                   as ach_type\n        from    group_merchant        gm,\n                ach_detail            ach,\n                ach_detail_tran_code  tc,\n                mif                   mf\n        where   gm.org_num =  :1  and\n                ach.merchant_number = gm.merchant_number and\n                ach.post_date_option =  :2  and\n                ach.batch_id =  :3  and\n                ach.individual_name <> 'NET OFFSET ENTRY' and\n                tc.ach_detail_trans_code(+) = ach.transaction_code and\n                not exists\n                (\n                  select  cb.merchant_number\n                  from    network_chargebacks           cb,\n                          network_chargeback_activity   cba\n                  where   cb.merchant_number = ach.merchant_number and\n                          cb.incoming_date between (ach.post_date_option-185) and ach.post_date_option and\n                          cba.cb_load_sec = cb.cb_load_sec and\n                          cba.action_date between ( ach.post_date_option -  :4  ) and\n                                                  ( ach.post_date_option - 1 ) and\n                          ( cb.tran_amount * decode(cba.action_code,'C',1,-1) ) =\n                          ( ach.amount_of_transaction *\n                            decode(tc.debit_credit_indicator,'D',-1,1) )\n                ) and\n                mf.merchant_number = ach.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,AchBatchDate);
   __sJT_st.setInt(3,AchBatchId);
   __sJT_st.setInt(4,inc);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1397^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new AchDetailRow(resultSet) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadNetOffsetDetails()",e.toString() );
    }
    finally
    {
      try{ it.close(); } catch (Exception e) {}
    }
  }

  public void loadNetOffsetEntry( SummaryData record )
  {
    double            achAmount       = 0.0;
    int               achCount        = 0;
    Date              beginDate       = record.ReportDate;
    long              orgId           = record.OrgId;

    try
    {
      if ( !record.UnassignedOnly && (AchBatchId != 0) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1428^9*/

//  ************************************************************
//  #sql [Ctx] { select    count( ach.batch_id ),
//                      nvl( sum( ach.amount_of_transaction *
//                                decode(tc.debit_credit_indicator,'D',-1,1) ), 0 )
//            
//            from      group_merchant        gm,
//                      ach_detail            ach,
//                      ach_detail_tran_code  tc  
//            where     gm.org_num = :orgId and
//                      ach.merchant_number = gm.merchant_number and
//                      ach.post_date_option = :AchBatchDate and
//                      ach.batch_id = :AchBatchId and
//                      tc.ach_detail_trans_code(+) = ach.transaction_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    count( ach.batch_id ),\n                    nvl( sum( ach.amount_of_transaction *\n                              decode(tc.debit_credit_indicator,'D',-1,1) ), 0 )\n           \n          from      group_merchant        gm,\n                    ach_detail            ach,\n                    ach_detail_tran_code  tc  \n          where     gm.org_num =  :1  and\n                    ach.merchant_number = gm.merchant_number and\n                    ach.post_date_option =  :2  and\n                    ach.batch_id =  :3  and\n                    tc.ach_detail_trans_code(+) = ach.transaction_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,AchBatchDate);
   __sJT_st.setInt(3,AchBatchId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   achCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   achAmount = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1442^9*/
      }
      // achAmount is merchant account offset,
      // reversing the sign will show the bank offset
      record.AchAmount  = -achAmount;
      record.AchCount   = achCount;
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadNetOffsetEntry(" + orgId + ", " + AchBatchDate + ", " + AchBatchId + ")",e.toString() );
    }
    finally
    {
    }
  }

  public double loadNetWorkedAmount( )
  {
    return( loadNetWorkedAmount( getReportHierarchyNode() ) );
  }

  public double loadNetWorkedAmount( long nodeId )
  {
    Date        beginDate = getReportDateBegin();
    long        orgId     = getReportOrgId();
    double      retVal    = 0.0;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1471^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum( (cb.tran_amount * decode(cba.action_code,'C',-1,1)) ),0)
//                    
//          from    organization                  o,
//                  group_merchant                gm,
//                  network_chargebacks           cb,
//                  network_chargeback_activity   cba,
//                  chargeback_action_codes       ac
//          where   o.org_group = :nodeId and
//                  gm.org_num = o.org_num and
//                  cb.merchant_number = gm.merchant_number and
//                  cb.incoming_date between (:beginDate-180) and :beginDate and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.action_date = :beginDate and
//                  ac.short_action_code = cba.action_code and
//                  not ac.adj_tran_code is null -- financial only
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum( (cb.tran_amount * decode(cba.action_code,'C',-1,1)) ),0)\n                   \n        from    organization                  o,\n                group_merchant                gm,\n                network_chargebacks           cb,\n                network_chargeback_activity   cba,\n                chargeback_action_codes       ac\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                cb.merchant_number = gm.merchant_number and\n                cb.incoming_date between ( :2 -180) and  :3  and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.action_date =  :4  and\n                ac.short_action_code = cba.action_code and\n                not ac.adj_tran_code is null -- financial only";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1488^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadNetWorkedAmount()",e.toString() );
    }
    return( retVal );
  }

  protected void loadPrevPendingTotals( SummaryData record )
  {
    int               bankNumber      = 0;
    Date              beginDate       = record.ReportDate;
    long              orgId           = record.OrgId;
    double            prevAmount      = 0.0;
    int               prevCount       = 0;
    double            prevREMCAmount  = 0.0;
    int               prevREMCCount   = 0;
    int               unassigned      = (record.UnassignedOnly ? 1 : 0);

    try
    {
      if ( isOrgBank( orgId ) )
      {
        bankNumber = getOrgBankId( orgId );
      }

      if ( bankNumber != 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1517^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(cb.incoming_date),sum(cb.tran_amount)
//            
//            from    network_chargebacks           cb
//            where   cb.bank_number = :bankNumber and
//                    ( :unassigned = 0 or cb.merchant_number = 0 ) and
//                    cb.incoming_date between (:beginDate-181) and (:beginDate-1) and
//                    not exists
//                    (
//                      select  cba.cb_load_sec
//                      from    network_chargeback_activity cba
//                      where   cba.cb_load_sec = cb.cb_load_sec and
//                              cba.action_date < :beginDate
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cb.incoming_date),sum(cb.tran_amount)\n           \n          from    network_chargebacks           cb\n          where   cb.bank_number =  :1  and\n                  (  :2  = 0 or cb.merchant_number = 0 ) and\n                  cb.incoming_date between ( :3 -181) and ( :4 -1) and\n                  not exists\n                  (\n                    select  cba.cb_load_sec\n                    from    network_chargeback_activity cba\n                    where   cba.cb_load_sec = cb.cb_load_sec and\n                            cba.action_date <  :5 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,unassigned);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   prevCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   prevAmount = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1532^9*/
      }
      else    // use group_merchant
      {
        /*@lineinfo:generated-code*//*@lineinfo:1536^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(cb.incoming_date),sum(cb.tran_amount)
//            
//            from    group_merchant                gm,
//                    network_chargebacks           cb
//            where   gm.org_num = :orgId and
//                    cb.merchant_number = gm.merchant_number and
//                    cb.incoming_date between (:beginDate-181) and (:beginDate-1) and
//                    not exists
//                    (
//                      select  cba.cb_load_sec
//                      from    network_chargeback_activity cba
//                      where   cba.cb_load_sec = cb.cb_load_sec and
//                              cba.action_date < :beginDate
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cb.incoming_date),sum(cb.tran_amount)\n           \n          from    group_merchant                gm,\n                  network_chargebacks           cb\n          where   gm.org_num =  :1  and\n                  cb.merchant_number = gm.merchant_number and\n                  cb.incoming_date between ( :2 -181) and ( :3 -1) and\n                  not exists\n                  (\n                    select  cba.cb_load_sec\n                    from    network_chargeback_activity cba\n                    where   cba.cb_load_sec = cb.cb_load_sec and\n                            cba.action_date <  :4 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   prevCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   prevAmount = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1552^9*/
      }

      record.PrevPendingCount   = prevCount;
      record.PrevPendingAmount  = prevAmount;
    }
    catch( java.sql.SQLException e )
    {
      // ignore empty results
    }
  }

  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    int                           bankNumber        = 0;
    ResultSetIterator             it                = null;
    long                          nodeId            = 0L;
    String                        orgName           = null;
    long                          orgNode           = 0L;
    ResultSet                     resultSet         = null;
    long                          ts                = 0L;

    try
    {
      // empty the current contents
      ReportRows.clear();

      // set the bank number if the
      // current node is a bank node
      if ( isOrgBank( orgId ) )
      {
        bankNumber = getOrgBankId( orgId );
      }


      //**********************************************************************
      // INCOMING ITEMS
      //**********************************************************************
      if ( SummaryType == ST_PARENT )
      {
        nodeId    = orgIdToHierarchyNode( orgId );
        orgName   = getOrgName( orgId );

        if ( bankNumber != 0 )
        {
          // select all including the items
          // without a merchant number assigned
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:1600^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (cb idx_network_cb_bank_date) */
//                      :orgId                        as org_num,
//                      :nodeId                       as hierarchy_node,
//                      :orgName                      as org_name,
//                      :beginDate                    as report_date,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'AM',0,
//                                   1 ) 
//                         )                          as tot_incoming_count,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                  'AM',0,
//                                  cb.tran_amount )
//                         )                          as tot_incoming_amount,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'AM',1,
//                                   0 )
//                         )                          as tot_incoming_amex_count,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'AM',cb.tran_amount,
//                                   0 )
//                         )                          as tot_incoming_amex_amount,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'DS',1,
//                                   0 )
//                         )                          as tot_incoming_disc_count,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'DS',cb.tran_amount,
//                                   0 )
//                         )                          as tot_incoming_disc_amount,
//                      sum( decode( nvl(cb.first_time_chargeback,'N'),
//                                   'Y',0,
//                                   1 )
//                         )                          as tot_incoming_prearb_count,
//                      sum( decode( nvl(cb.first_time_chargeback,'N'),
//                                   'Y',0,
//                                   cb.tran_amount )
//                         )                          as tot_incoming_prearb_amount
//              from    network_chargebacks         cb
//              where   cb.bank_number = :bankNumber and
//                      cb.incoming_date between :beginDate and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (cb idx_network_cb_bank_date) */\n                     :1                         as org_num,\n                     :2                        as hierarchy_node,\n                     :3                       as org_name,\n                     :4                     as report_date,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'AM',0,\n                                 1 ) \n                       )                          as tot_incoming_count,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                'AM',0,\n                                cb.tran_amount )\n                       )                          as tot_incoming_amount,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'AM',1,\n                                 0 )\n                       )                          as tot_incoming_amex_count,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'AM',cb.tran_amount,\n                                 0 )\n                       )                          as tot_incoming_amex_amount,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'DS',1,\n                                 0 )\n                       )                          as tot_incoming_disc_count,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'DS',cb.tran_amount,\n                                 0 )\n                       )                          as tot_incoming_disc_amount,\n                    sum( decode( nvl(cb.first_time_chargeback,'N'),\n                                 'Y',0,\n                                 1 )\n                       )                          as tot_incoming_prearb_count,\n                    sum( decode( nvl(cb.first_time_chargeback,'N'),\n                                 'Y',0,\n                                 cb.tran_amount )\n                       )                          as tot_incoming_prearb_amount\n            from    network_chargebacks         cb\n            where   cb.bank_number =  :5  and\n                    cb.incoming_date between  :6  and  :7  and\n                    (  :8  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :9  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setString(3,orgName);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,CardType);
   __sJT_st.setString(9,CardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1644^11*/
          //System.out.println("query 01 time: " + (System.currentTimeMillis() - ts));
        }
        else      //
        {
          // only select those with merchant numbers
          // under the current hierarchy node
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:1652^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                          ordered
//                          use_nl(gm cb)
//                       */
//                      :orgId                        as org_num,
//                      :nodeId                       as hierarchy_node,
//                      :orgName                      as org_name,
//                      :beginDate                    as report_date,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'AM',0,
//                                   1 ) 
//                         )                          as tot_incoming_count,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                  'AM',0,
//                                  cb.tran_amount )
//                         )                          as tot_incoming_amount,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'AM',1,
//                                   0 )
//                         )                          as tot_incoming_amex_count,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'AM',cb.tran_amount,
//                                   0 )
//                         )                          as tot_incoming_amex_amount,
//                      sum( decode( nvl(cb.first_time_chargeback,'N'),
//                                   'Y',0,
//                                   1 )
//                         )                          as tot_incoming_prearb_count,
//                      sum( decode( nvl(cb.first_time_chargeback,'N'),
//                                   'Y',0,
//                                   cb.tran_amount )
//                         )                          as tot_incoming_prearb_amount
//              from    group_merchant              gm,
//                      network_chargebacks         cb
//              where   gm.org_num = :orgId and
//                      cb.merchant_number = gm.merchant_number and
//                      cb.incoming_date between :beginDate and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                        ordered\n                        use_nl(gm cb)\n                     */\n                     :1                         as org_num,\n                     :2                        as hierarchy_node,\n                     :3                       as org_name,\n                     :4                     as report_date,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'AM',0,\n                                 1 ) \n                       )                          as tot_incoming_count,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                'AM',0,\n                                cb.tran_amount )\n                       )                          as tot_incoming_amount,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'AM',1,\n                                 0 )\n                       )                          as tot_incoming_amex_count,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'AM',cb.tran_amount,\n                                 0 )\n                       )                          as tot_incoming_amex_amount,\n                    sum( decode( nvl(cb.first_time_chargeback,'N'),\n                                 'Y',0,\n                                 1 )\n                       )                          as tot_incoming_prearb_count,\n                    sum( decode( nvl(cb.first_time_chargeback,'N'),\n                                 'Y',0,\n                                 cb.tran_amount )\n                       )                          as tot_incoming_prearb_amount\n            from    group_merchant              gm,\n                    network_chargebacks         cb\n            where   gm.org_num =  :5  and\n                    cb.merchant_number = gm.merchant_number and\n                    cb.incoming_date between  :6  and  :7  and\n                    (  :8  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :9  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setString(3,orgName);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setLong(5,orgId);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,CardType);
   __sJT_st.setString(9,CardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1693^11*/
          //System.out.println("query 02 time: " + (System.currentTimeMillis() - ts));
        }
      }
      else    // ST_CHILD
      {
        if ( bankNumber != 0 )
        {
          orgNode = orgIdToHierarchyNode( orgId );

          // if the current node is a bank node then load all
          // the unassigned chargebacks.
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:1706^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (cb idx_network_cb_bank_date) */
//                      :orgId                        as org_num,
//                      :orgNode                      as hierarchy_node,
//                      'Unassigned Chargebacks'      as org_name,
//                      :beginDate                    as report_date,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'AM',0,
//                                   1 ) 
//                         )                          as tot_incoming_count,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                  'AM',0,
//                                  cb.tran_amount )
//                         )                          as tot_incoming_amount,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'AM',1,
//                                   0 )
//                         )                          as tot_incoming_amex_count,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'AM',cb.tran_amount,
//                                   0 )
//                         )                          as tot_incoming_amex_amount,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'DS',1,
//                                   0 )
//                         )                          as tot_incoming_disc_count,
//                      sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                   'DS',cb.tran_amount,
//                                   0 )
//                         )                          as tot_incoming_disc_amount,
//                      sum( decode( nvl(cb.first_time_chargeback,'N'),
//                                   'Y',0,
//                                   1 )
//                         )                          as tot_incoming_prearb_count,
//                      sum( decode( nvl(cb.first_time_chargeback,'N'),
//                                   'Y',0,
//                                   cb.tran_amount )
//                         )                          as tot_incoming_prearb_amount
//              from    network_chargebacks         cb
//              where   cb.bank_number = :bankNumber and
//                      cb.merchant_number = 0 and
//                      cb.incoming_date between :beginDate and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (cb idx_network_cb_bank_date) */\n                     :1                         as org_num,\n                     :2                       as hierarchy_node,\n                    'Unassigned Chargebacks'      as org_name,\n                     :3                     as report_date,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'AM',0,\n                                 1 ) \n                       )                          as tot_incoming_count,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                'AM',0,\n                                cb.tran_amount )\n                       )                          as tot_incoming_amount,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'AM',1,\n                                 0 )\n                       )                          as tot_incoming_amex_count,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'AM',cb.tran_amount,\n                                 0 )\n                       )                          as tot_incoming_amex_amount,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'DS',1,\n                                 0 )\n                       )                          as tot_incoming_disc_count,\n                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                 'DS',cb.tran_amount,\n                                 0 )\n                       )                          as tot_incoming_disc_amount,\n                    sum( decode( nvl(cb.first_time_chargeback,'N'),\n                                 'Y',0,\n                                 1 )\n                       )                          as tot_incoming_prearb_count,\n                    sum( decode( nvl(cb.first_time_chargeback,'N'),\n                                 'Y',0,\n                                 cb.tran_amount )\n                       )                          as tot_incoming_prearb_amount\n            from    network_chargebacks         cb\n            where   cb.bank_number =  :4  and\n                    cb.merchant_number = 0 and\n                    cb.incoming_date between  :5  and  :6  and\n                    (  :7  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :8  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgNode);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setString(7,CardType);
   __sJT_st.setString(8,CardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1751^11*/
          //System.out.println("query 03 time: " + (System.currentTimeMillis() - ts));
          resultSet = it.getResultSet();

          while(resultSet.next())
          {
            addData( resultSet, CB_INCOMING, true );
          }
          resultSet.close();
          it.close();
        }

        ts = System.currentTimeMillis();
        /*@lineinfo:generated-code*//*@lineinfo:1764^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      ordered
//                      use_nl(gm cb)
//                    */
//                    o.org_num                   as org_num,
//                    o.org_group                 as hierarchy_node,
//                    o.org_name                  as org_name,
//                    :beginDate                  as report_date,
//                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                 'AM',0,
//                                 1 ) 
//                       )                        as tot_incoming_count,
//                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                'AM',0,
//                                cb.tran_amount )
//                       )                        as tot_incoming_amount,
//                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                 'AM',1,
//                                 0 )
//                       )                        as tot_incoming_amex_count,
//                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                 'AM',cb.tran_amount,
//                                 0 )
//                       )                        as tot_incoming_amex_amount,
//                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                 'DS',1,
//                                 0 )
//                       )                        as tot_incoming_disc_count,
//                    sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                 'DS',cb.tran_amount,
//                                 0 )
//                       )                        as tot_incoming_disc_amount,
//                    sum( decode( nvl(cb.first_time_chargeback,'N'),
//                                 'Y',0,
//                                 1 )
//                       )                        as tot_incoming_prearb_count,
//                    sum( decode( nvl(cb.first_time_chargeback,'N'),
//                                 'Y',0,
//                                 cb.tran_amount )
//                       )                        as tot_incoming_prearb_amount
//            from    parent_org                  po,
//                    organization                o,
//                    group_merchant              gm,
//                    network_chargebacks         cb
//            where   po.parent_org_num = :orgId and
//                    o.org_num = po.org_num and
//                    gm.org_num = o.org_num and
//                    cb.merchant_number = gm.merchant_number and
//                    cb.incoming_date between :beginDate and :endDate and
//                    ( :CardType is null or 
//                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType )
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    ordered\n                    use_nl(gm cb)\n                  */\n                  o.org_num                   as org_num,\n                  o.org_group                 as hierarchy_node,\n                  o.org_name                  as org_name,\n                   :1                   as report_date,\n                  sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                               'AM',0,\n                               1 ) \n                     )                        as tot_incoming_count,\n                  sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                              'AM',0,\n                              cb.tran_amount )\n                     )                        as tot_incoming_amount,\n                  sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                               'AM',1,\n                               0 )\n                     )                        as tot_incoming_amex_count,\n                  sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                               'AM',cb.tran_amount,\n                               0 )\n                     )                        as tot_incoming_amex_amount,\n                  sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                               'DS',1,\n                               0 )\n                     )                        as tot_incoming_disc_count,\n                  sum( decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                               'DS',cb.tran_amount,\n                               0 )\n                     )                        as tot_incoming_disc_amount,\n                  sum( decode( nvl(cb.first_time_chargeback,'N'),\n                               'Y',0,\n                               1 )\n                     )                        as tot_incoming_prearb_count,\n                  sum( decode( nvl(cb.first_time_chargeback,'N'),\n                               'Y',0,\n                               cb.tran_amount )\n                     )                        as tot_incoming_prearb_amount\n          from    parent_org                  po,\n                  organization                o,\n                  group_merchant              gm,\n                  network_chargebacks         cb\n          where   po.parent_org_num =  :2  and\n                  o.org_num = po.org_num and\n                  gm.org_num = o.org_num and\n                  cb.merchant_number = gm.merchant_number and\n                  cb.incoming_date between  :3  and  :4  and\n                  (  :5  is null or \n                    decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :6  )\n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   __sJT_st.setString(5,CardType);
   __sJT_st.setString(6,CardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1818^9*/
      }
      //System.out.println("query 04 time: " + (System.currentTimeMillis() - ts));
      resultSet = it.getResultSet();

      while(resultSet.next())
      {
        addData( resultSet, CB_INCOMING );
      }
      resultSet.close();
      it.close();

      //**********************************************************************
      // WORKED ITEMS
      //**********************************************************************
      if ( SummaryType == ST_PARENT )
      {
        if ( bankNumber != 0 )
        {
          // select all including the items
          // without a merchant number assigned
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:1840^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  :orgId                                            as org_num,
//                      :nodeId                                           as hierarchy_node,
//                      :orgName                                          as org_name,
//                      :beginDate                                        as report_date,
//                      sum( decode( cb.card_type,'AM', 1, 0 ) )          as amex_worked_count,
//                      sum( decode( cb.card_type,'AM', 
//                                   cb.tran_amount, 0 ) )                as amex_worked_amount,
//                      sum( decode( cb.card_type,'DS', 1, 0 ) )          as disc_worked_count,
//                      sum( decode( cb.card_type,'DS', 
//                                   cb.tran_amount, 0 ) )                as disc_worked_amount,
//                      sum( decode( cb.card_type,
//                                   'AM', 0, 
//                                   'DS', 0, 
//                                   1 ) )                                as tot_worked_count,
//                      sum( decode( cb.card_type,
//                                   'AM', 0, 
//                                   'DS', 0, 
//                                   cb.tran_amount ) )                   as tot_worked_amount,
//                      sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,
//                      sum( decode( cba.action_code,'D', cb.tran_amount, 'B',
//                                   cb.tran_amount, 0 ) )                as mchb_amount,
//                      sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,
//                      sum( decode( cba.action_code,'S',
//                                   cb.tran_amount, 0 ) )                as repr_amount,
//                      sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,
//                      sum( decode( cba.action_code,'C',
//                                   cb.tran_amount, 0 ) )                as remc_amount,
//                      sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,
//                      sum( decode( cba.action_code,'L',
//                                   cb.tran_amount, 0 ) )                as loss_amount,
//                      sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,
//                      sum( decode( cba.action_code,'W',
//                                   cb.tran_amount, 0 ) )                as coll_amount
//              from    network_chargebacks         cb,
//                      network_chargeback_activity cba
//              where   cb.bank_number = :bankNumber and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode( cb.card_type,
//                                null, decode_card_type(cb.card_number,null),
//                                cb.card_type ) = :CardType ) and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.action_date between :beginDate and :endDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                                             as org_num,\n                     :2                                            as hierarchy_node,\n                     :3                                           as org_name,\n                     :4                                         as report_date,\n                    sum( decode( cb.card_type,'AM', 1, 0 ) )          as amex_worked_count,\n                    sum( decode( cb.card_type,'AM', \n                                 cb.tran_amount, 0 ) )                as amex_worked_amount,\n                    sum( decode( cb.card_type,'DS', 1, 0 ) )          as disc_worked_count,\n                    sum( decode( cb.card_type,'DS', \n                                 cb.tran_amount, 0 ) )                as disc_worked_amount,\n                    sum( decode( cb.card_type,\n                                 'AM', 0, \n                                 'DS', 0, \n                                 1 ) )                                as tot_worked_count,\n                    sum( decode( cb.card_type,\n                                 'AM', 0, \n                                 'DS', 0, \n                                 cb.tran_amount ) )                   as tot_worked_amount,\n                    sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,\n                    sum( decode( cba.action_code,'D', cb.tran_amount, 'B',\n                                 cb.tran_amount, 0 ) )                as mchb_amount,\n                    sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,\n                    sum( decode( cba.action_code,'S',\n                                 cb.tran_amount, 0 ) )                as repr_amount,\n                    sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,\n                    sum( decode( cba.action_code,'C',\n                                 cb.tran_amount, 0 ) )                as remc_amount,\n                    sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,\n                    sum( decode( cba.action_code,'L',\n                                 cb.tran_amount, 0 ) )                as loss_amount,\n                    sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,\n                    sum( decode( cba.action_code,'W',\n                                 cb.tran_amount, 0 ) )                as coll_amount\n            from    network_chargebacks         cb,\n                    network_chargeback_activity cba\n            where   cb.bank_number =  :5  and\n                    cb.incoming_date between ( :6 -180) and  :7  and\n                    (  :8  is null or \n                      decode( cb.card_type,\n                              null, decode_card_type(cb.card_number,null),\n                              cb.card_type ) =  :9  ) and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.action_date between  :10  and  :11";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setString(3,orgName);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,CardType);
   __sJT_st.setString(9,CardType);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setDate(11,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1885^11*/
          //System.out.println("query 05 time: " + (System.currentTimeMillis() - ts));
        }
        else      //
        {
          // only select those with merchant numbers
          // under the current hierarchy node
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:1893^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                          ordered
//                          use_nl(gm cb)
//                       */
//                      :orgId                                            as org_num,
//                      :nodeId                                           as hierarchy_node,
//                      :orgName                                          as org_name,
//                      :beginDate                                        as report_date,
//                      sum( decode( cb.card_type,'AM', 1, 0 ) )          as amex_worked_count,
//                      sum( decode( cb.card_type,'AM', 
//                                   cb.tran_amount, 0 ) )                as amex_worked_amount,
//                      sum( decode( cb.card_type,'DS', 1, 0 ) )          as disc_worked_count,
//                      sum( decode( cb.card_type,'DS', 
//                                   cb.tran_amount, 0 ) )                as disc_worked_amount,
//                      sum( decode( cb.card_type,
//                                   'AM', 0, 
//                                   'DS', 0, 
//                                   1 ) )                                as tot_worked_count,
//                      sum( decode( cb.card_type,
//                                   'AM', 0, 
//                                   'DS', 0, 
//                                   cb.tran_amount ) )                   as tot_worked_amount,
//                      sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,
//                      sum( decode( cba.action_code,'D', cb.tran_amount, 'B',
//                                   cb.tran_amount, 0 ) )                as mchb_amount,
//                      sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,
//                      sum( decode( cba.action_code,'S',
//                                   cb.tran_amount, 0 ) )                as repr_amount,
//                      sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,
//                      sum( decode( cba.action_code,'C',
//                                   cb.tran_amount, 0 ) )                as remc_amount,
//                      sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,
//                      sum( decode( cba.action_code,'L',
//                                   cb.tran_amount, 0 ) )                as loss_amount,
//                      sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,
//                      sum( decode( cba.action_code,'W',
//                                   cb.tran_amount, 0 ) )                as coll_amount
//              from    group_merchant              gm,
//                      network_chargebacks         cb,
//                      network_chargeback_activity cba
//              where   gm.org_num = :orgId and
//                      cb.merchant_number = gm.merchant_number and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.action_date between :beginDate and :endDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                        ordered\n                        use_nl(gm cb)\n                     */\n                     :1                                             as org_num,\n                     :2                                            as hierarchy_node,\n                     :3                                           as org_name,\n                     :4                                         as report_date,\n                    sum( decode( cb.card_type,'AM', 1, 0 ) )          as amex_worked_count,\n                    sum( decode( cb.card_type,'AM', \n                                 cb.tran_amount, 0 ) )                as amex_worked_amount,\n                    sum( decode( cb.card_type,'DS', 1, 0 ) )          as disc_worked_count,\n                    sum( decode( cb.card_type,'DS', \n                                 cb.tran_amount, 0 ) )                as disc_worked_amount,\n                    sum( decode( cb.card_type,\n                                 'AM', 0, \n                                 'DS', 0, \n                                 1 ) )                                as tot_worked_count,\n                    sum( decode( cb.card_type,\n                                 'AM', 0, \n                                 'DS', 0, \n                                 cb.tran_amount ) )                   as tot_worked_amount,\n                    sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,\n                    sum( decode( cba.action_code,'D', cb.tran_amount, 'B',\n                                 cb.tran_amount, 0 ) )                as mchb_amount,\n                    sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,\n                    sum( decode( cba.action_code,'S',\n                                 cb.tran_amount, 0 ) )                as repr_amount,\n                    sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,\n                    sum( decode( cba.action_code,'C',\n                                 cb.tran_amount, 0 ) )                as remc_amount,\n                    sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,\n                    sum( decode( cba.action_code,'L',\n                                 cb.tran_amount, 0 ) )                as loss_amount,\n                    sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,\n                    sum( decode( cba.action_code,'W',\n                                 cb.tran_amount, 0 ) )                as coll_amount\n            from    group_merchant              gm,\n                    network_chargebacks         cb,\n                    network_chargeback_activity cba\n            where   gm.org_num =  :5  and\n                    cb.merchant_number = gm.merchant_number and\n                    cb.incoming_date between ( :6 -180) and  :7  and\n                    (  :8  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :9  ) and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.action_date between  :10  and  :11";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setString(3,orgName);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setLong(5,orgId);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,CardType);
   __sJT_st.setString(9,CardType);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setDate(11,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1942^11*/
          //System.out.println("query 06 time: " + (System.currentTimeMillis() - ts));
        }
      }
      else    // ST_CHILD
      {
        if ( bankNumber != 0 )
        {
          orgNode = orgIdToHierarchyNode( orgId );

          // if the current node is a bank node then load all
          // the unassigned chargebacks.
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:1955^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  :orgId                                            as org_num,
//                      :orgNode                                          as hierarchy_node,
//                      'Unassigned Chargebacks'                          as org_name,
//                      :beginDate                                        as report_date,
//                      sum( decode( cb.card_type,'AM', 1, 0 ) )          as amex_worked_count,
//                      sum( decode( cb.card_type,'AM', 
//                                   cb.tran_amount, 0 ) )                as amex_worked_amount,
//                      sum( decode( cb.card_type,'DS', 1, 0 ) )          as disc_worked_count,
//                      sum( decode( cb.card_type,'DS', 
//                                   cb.tran_amount, 0 ) )                as disc_worked_amount,
//                      sum( decode( cb.card_type,
//                                   'AM', 0, 
//                                   'DS', 0, 
//                                   1 ) )                                as tot_worked_count,
//                      sum( decode( cb.card_type,
//                                   'AM', 0, 
//                                   'DS', 0, 
//                                   cb.tran_amount ) )                   as tot_worked_amount,
//                      sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,
//                      sum( decode( cba.action_code,'D', cb.tran_amount, 'B',
//                                   cb.tran_amount, 0 ) )                as mchb_amount,
//                      sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,
//                      sum( decode( cba.action_code,'S',
//                                   cb.tran_amount, 0 ) )                as repr_amount,
//                      sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,
//                      sum( decode( cba.action_code,'C',
//                                   cb.tran_amount, 0 ) )                as remc_amount,
//                      sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,
//                      sum( decode( cba.action_code,'L',
//                                   cb.tran_amount, 0 ) )                as loss_amount,
//                      sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,
//                      sum( decode( cba.action_code,'W',
//                                   cb.tran_amount, 0 ) )                as coll_amount
//              from    network_chargebacks         cb,
//                      network_chargeback_activity cba
//              where   cb.bank_number = :bankNumber and
//                      cb.merchant_number = 0 and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.action_date between :beginDate and :endDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                                             as org_num,\n                     :2                                           as hierarchy_node,\n                    'Unassigned Chargebacks'                          as org_name,\n                     :3                                         as report_date,\n                    sum( decode( cb.card_type,'AM', 1, 0 ) )          as amex_worked_count,\n                    sum( decode( cb.card_type,'AM', \n                                 cb.tran_amount, 0 ) )                as amex_worked_amount,\n                    sum( decode( cb.card_type,'DS', 1, 0 ) )          as disc_worked_count,\n                    sum( decode( cb.card_type,'DS', \n                                 cb.tran_amount, 0 ) )                as disc_worked_amount,\n                    sum( decode( cb.card_type,\n                                 'AM', 0, \n                                 'DS', 0, \n                                 1 ) )                                as tot_worked_count,\n                    sum( decode( cb.card_type,\n                                 'AM', 0, \n                                 'DS', 0, \n                                 cb.tran_amount ) )                   as tot_worked_amount,\n                    sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,\n                    sum( decode( cba.action_code,'D', cb.tran_amount, 'B',\n                                 cb.tran_amount, 0 ) )                as mchb_amount,\n                    sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,\n                    sum( decode( cba.action_code,'S',\n                                 cb.tran_amount, 0 ) )                as repr_amount,\n                    sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,\n                    sum( decode( cba.action_code,'C',\n                                 cb.tran_amount, 0 ) )                as remc_amount,\n                    sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,\n                    sum( decode( cba.action_code,'L',\n                                 cb.tran_amount, 0 ) )                as loss_amount,\n                    sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,\n                    sum( decode( cba.action_code,'W',\n                                 cb.tran_amount, 0 ) )                as coll_amount\n            from    network_chargebacks         cb,\n                    network_chargeback_activity cba\n            where   cb.bank_number =  :4  and\n                    cb.merchant_number = 0 and\n                    cb.incoming_date between ( :5 -180) and  :6  and\n                    (  :7  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :8  ) and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.action_date between  :9  and  :10";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgNode);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setString(7,CardType);
   __sJT_st.setString(8,CardType);
   __sJT_st.setDate(9,beginDate);
   __sJT_st.setDate(10,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1999^11*/
          //System.out.println("query 07 time: " + (System.currentTimeMillis() - ts));
          resultSet = it.getResultSet();

          while(resultSet.next())
          {
            addData(resultSet,CB_WORKED);
          }
          resultSet.close();
          it.close();
        }

        ts = System.currentTimeMillis();
        /*@lineinfo:generated-code*//*@lineinfo:2012^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      ordered
//                      use_nl(gm cb)
//                    */
//                    o.org_num                                         as org_num,
//                    o.org_group                                       as hierarchy_node,
//                    o.org_name                                        as org_name,
//                    :beginDate                                        as report_date,
//                    sum( decode( cb.card_type,'AM', 1, 0 ) )          as amex_worked_count,
//                    sum( decode( cb.card_type,'AM', 
//                                 cb.tran_amount, 0 ) )                as amex_worked_amount,
//                    sum( decode( cb.card_type,'DS', 1, 0 ) )          as disc_worked_count,
//                    sum( decode( cb.card_type,'DS', 
//                                 cb.tran_amount, 0 ) )                as disc_worked_amount,
//                    sum( decode( cb.card_type,
//                                 'AM', 0, 
//                                 'DS', 0, 
//                                 1 ) )                                as tot_worked_count,
//                    sum( decode( cb.card_type,
//                                 'AM', 0, 
//                                 'DS', 0, 
//                                 cb.tran_amount ) )                   as tot_worked_amount,
//                    sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,
//                    sum( decode( cba.action_code,'D', cb.tran_amount, 'B',
//                                 cb.tran_amount, 0 ) )                as mchb_amount,
//                    sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,
//                    sum( decode( cba.action_code,'S',
//                                 cb.tran_amount, 0 ) )                as repr_amount,
//                    sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,
//                    sum( decode( cba.action_code,'C',
//                                 cb.tran_amount, 0 ) )                as remc_amount,
//                    sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,
//                    sum( decode( cba.action_code,'L',
//                                 cb.tran_amount, 0 ) )                as loss_amount,
//                    sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,
//                    sum( decode( cba.action_code,'W',
//                                 cb.tran_amount, 0 ) )                as coll_amount
//            from    parent_org                  po,
//                    organization                o,
//                    group_merchant              gm,
//                    network_chargebacks         cb,
//                    network_chargeback_activity cba
//            where   po.parent_org_num = :orgId and
//                    o.org_num = po.org_num and
//                    gm.org_num = o.org_num and
//                    cb.merchant_number = gm.merchant_number and
//                    cb.incoming_date between (:beginDate-180) and :endDate and
//                    ( :CardType is null or 
//                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                    cba.cb_load_sec = cb.cb_load_sec and
//                    cba.action_date between :beginDate and :endDate
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    ordered\n                    use_nl(gm cb)\n                  */\n                  o.org_num                                         as org_num,\n                  o.org_group                                       as hierarchy_node,\n                  o.org_name                                        as org_name,\n                   :1                                         as report_date,\n                  sum( decode( cb.card_type,'AM', 1, 0 ) )          as amex_worked_count,\n                  sum( decode( cb.card_type,'AM', \n                               cb.tran_amount, 0 ) )                as amex_worked_amount,\n                  sum( decode( cb.card_type,'DS', 1, 0 ) )          as disc_worked_count,\n                  sum( decode( cb.card_type,'DS', \n                               cb.tran_amount, 0 ) )                as disc_worked_amount,\n                  sum( decode( cb.card_type,\n                               'AM', 0, \n                               'DS', 0, \n                               1 ) )                                as tot_worked_count,\n                  sum( decode( cb.card_type,\n                               'AM', 0, \n                               'DS', 0, \n                               cb.tran_amount ) )                   as tot_worked_amount,\n                  sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,\n                  sum( decode( cba.action_code,'D', cb.tran_amount, 'B',\n                               cb.tran_amount, 0 ) )                as mchb_amount,\n                  sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,\n                  sum( decode( cba.action_code,'S',\n                               cb.tran_amount, 0 ) )                as repr_amount,\n                  sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,\n                  sum( decode( cba.action_code,'C',\n                               cb.tran_amount, 0 ) )                as remc_amount,\n                  sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,\n                  sum( decode( cba.action_code,'L',\n                               cb.tran_amount, 0 ) )                as loss_amount,\n                  sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,\n                  sum( decode( cba.action_code,'W',\n                               cb.tran_amount, 0 ) )                as coll_amount\n          from    parent_org                  po,\n                  organization                o,\n                  group_merchant              gm,\n                  network_chargebacks         cb,\n                  network_chargeback_activity cba\n          where   po.parent_org_num =  :2  and\n                  o.org_num = po.org_num and\n                  gm.org_num = o.org_num and\n                  cb.merchant_number = gm.merchant_number and\n                  cb.incoming_date between ( :3 -180) and  :4  and\n                  (  :5  is null or \n                    decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :6  ) and\n                  cba.cb_load_sec = cb.cb_load_sec and\n                  cba.action_date between  :7  and  :8 \n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   __sJT_st.setString(5,CardType);
   __sJT_st.setString(6,CardType);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2066^9*/
      }
      //System.out.println("query 08 time: " + (System.currentTimeMillis() - ts));
      resultSet = it.getResultSet();

      while(resultSet.next())
      {
        addData(resultSet, CB_WORKED);
      }
      resultSet.close();
      it.close();

      //**********************************************************************
      // REWORKED ITEMS
      //**********************************************************************
      if ( SummaryType == ST_PARENT )
      {
        if ( bankNumber != 0 )
        {
          // select all including the items
          // without a merchant number assigned
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:2088^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  :orgId                                            as org_num,
//                      :nodeId                                           as hierarchy_node,
//                      :orgName                                          as org_name,
//                      :beginDate                                        as report_date,
//                      count(cba.action_code)                            as tot_worked_count,
//                      sum(cb.tran_amount)                               as tot_worked_amount,
//                      sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,
//                      sum( decode( cba.action_code,'D', cb.tran_amount, 'B',
//                                   cb.tran_amount, 0 ) )                as mchb_amount,
//                      sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,
//                      sum( decode( cba.action_code,'S',
//                                   cb.tran_amount, 0 ) )                as repr_amount,
//                      sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,
//                      sum( decode( cba.action_code,'C',
//                                   cb.tran_amount, 0 ) )                as remc_amount,
//                      sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,
//                      sum( decode( cba.action_code,'L',
//                                   cb.tran_amount, 0 ) )                as loss_amount,
//                      sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,
//                      sum( decode( cba.action_code,'W',
//                                   cb.tran_amount, 0 ) )                as coll_amount
//              from    network_chargebacks         cb,
//                      network_chargeback_activity cba
//              where   cb.bank_number = :bankNumber and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.action_date between :beginDate and :endDate and
//                      exists
//                      (
//                        select  cbai.cb_load_sec
//                        from    network_chargeback_activity cbai
//                        where   cbai.cb_load_sec = cba.cb_load_sec and
//                                cbai.file_load_sec != cba.file_load_sec and
//                                cbai.action_date < cba.action_date
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                                             as org_num,\n                     :2                                            as hierarchy_node,\n                     :3                                           as org_name,\n                     :4                                         as report_date,\n                    count(cba.action_code)                            as tot_worked_count,\n                    sum(cb.tran_amount)                               as tot_worked_amount,\n                    sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,\n                    sum( decode( cba.action_code,'D', cb.tran_amount, 'B',\n                                 cb.tran_amount, 0 ) )                as mchb_amount,\n                    sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,\n                    sum( decode( cba.action_code,'S',\n                                 cb.tran_amount, 0 ) )                as repr_amount,\n                    sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,\n                    sum( decode( cba.action_code,'C',\n                                 cb.tran_amount, 0 ) )                as remc_amount,\n                    sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,\n                    sum( decode( cba.action_code,'L',\n                                 cb.tran_amount, 0 ) )                as loss_amount,\n                    sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,\n                    sum( decode( cba.action_code,'W',\n                                 cb.tran_amount, 0 ) )                as coll_amount\n            from    network_chargebacks         cb,\n                    network_chargeback_activity cba\n            where   cb.bank_number =  :5  and\n                    cb.incoming_date between ( :6 -180) and  :7  and\n                    (  :8  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :9  ) and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.action_date between  :10  and  :11  and\n                    exists\n                    (\n                      select  cbai.cb_load_sec\n                      from    network_chargeback_activity cbai\n                      where   cbai.cb_load_sec = cba.cb_load_sec and\n                              cbai.file_load_sec != cba.file_load_sec and\n                              cbai.action_date < cba.action_date\n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setString(3,orgName);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,CardType);
   __sJT_st.setString(9,CardType);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setDate(11,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2127^11*/
          //System.out.println("query 09 time: " + (System.currentTimeMillis() - ts));
        }
        else      //
        {
          // only select those with merchant numbers
          // under the current hierarchy node
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:2135^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                        ordered
//                        use_nl(gm cb)
//                      */
//                      :orgId                                            as org_num,
//                      :nodeId                                           as hierarchy_node,
//                      :orgName                                          as org_name,
//                      :beginDate                                        as report_date,
//                      count(cba.action_code)                            as tot_worked_count,
//                      sum(cb.tran_amount)                               as tot_worked_amount,
//                      sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,
//                      sum( decode( cba.action_code,'D', cb.tran_amount, 'B',
//                                   cb.tran_amount, 0 ) )                as mchb_amount,
//                      sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,
//                      sum( decode( cba.action_code,'S',
//                                   cb.tran_amount, 0 ) )                as repr_amount,
//                      sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,
//                      sum( decode( cba.action_code,'C',
//                                   cb.tran_amount, 0 ) )                as remc_amount,
//                      sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,
//                      sum( decode( cba.action_code,'L',
//                                   cb.tran_amount, 0 ) )                as loss_amount,
//                      sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,
//                      sum( decode( cba.action_code,'W',
//                                   cb.tran_amount, 0 ) )                as coll_amount
//              from    group_merchant              gm,
//                      network_chargebacks         cb,
//                      network_chargeback_activity cba
//              where   gm.org_num = :orgId and
//                      cb.merchant_number = gm.merchant_number and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.action_date between :beginDate and :endDate and
//                      exists
//                      (
//                        select  cbai.cb_load_sec
//                        from    network_chargeback_activity cbai
//                        where   cbai.cb_load_sec = cba.cb_load_sec and
//                                cbai.file_load_sec != cba.file_load_sec and
//                                cbai.action_date < cba.action_date
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                      ordered\n                      use_nl(gm cb)\n                    */\n                     :1                                             as org_num,\n                     :2                                            as hierarchy_node,\n                     :3                                           as org_name,\n                     :4                                         as report_date,\n                    count(cba.action_code)                            as tot_worked_count,\n                    sum(cb.tran_amount)                               as tot_worked_amount,\n                    sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,\n                    sum( decode( cba.action_code,'D', cb.tran_amount, 'B',\n                                 cb.tran_amount, 0 ) )                as mchb_amount,\n                    sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,\n                    sum( decode( cba.action_code,'S',\n                                 cb.tran_amount, 0 ) )                as repr_amount,\n                    sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,\n                    sum( decode( cba.action_code,'C',\n                                 cb.tran_amount, 0 ) )                as remc_amount,\n                    sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,\n                    sum( decode( cba.action_code,'L',\n                                 cb.tran_amount, 0 ) )                as loss_amount,\n                    sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,\n                    sum( decode( cba.action_code,'W',\n                                 cb.tran_amount, 0 ) )                as coll_amount\n            from    group_merchant              gm,\n                    network_chargebacks         cb,\n                    network_chargeback_activity cba\n            where   gm.org_num =  :5  and\n                    cb.merchant_number = gm.merchant_number and\n                    cb.incoming_date between ( :6 -180) and  :7  and\n                    (  :8  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :9  ) and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.action_date between  :10  and  :11  and\n                    exists\n                    (\n                      select  cbai.cb_load_sec\n                      from    network_chargeback_activity cbai\n                      where   cbai.cb_load_sec = cba.cb_load_sec and\n                              cbai.file_load_sec != cba.file_load_sec and\n                              cbai.action_date < cba.action_date\n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setString(3,orgName);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setLong(5,orgId);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,CardType);
   __sJT_st.setString(9,CardType);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setDate(11,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2180^11*/
          //System.out.println("query 10 time: " + (System.currentTimeMillis() - ts));
        }
      }
      else    // ST_CHILD
      {
        if ( bankNumber != 0 )
        {
          orgNode = orgIdToHierarchyNode( orgId );

          // if the current node is a bank node then load all
          // the unassigned chargebacks.
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:2193^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  :orgId                                            as org_num,
//                      :orgNode                                          as hierarchy_node,
//                      'Unassigned Chargebacks'                          as org_name,
//                      :beginDate                                        as report_date,
//                      count(cba.action_code)                            as tot_worked_count,
//                      sum(cb.tran_amount)                               as tot_worked_amount,
//                      sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,
//                      sum( decode( cba.action_code,'D', cb.tran_amount, 'B',
//                                   cb.tran_amount, 0 ) )                as mchb_amount,
//                      sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,
//                      sum( decode( cba.action_code,'S',
//                                   cb.tran_amount, 0 ) )                as repr_amount,
//                      sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,
//                      sum( decode( cba.action_code,'C',
//                                   cb.tran_amount, 0 ) )                as remc_amount,
//                      sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,
//                      sum( decode( cba.action_code,'L',
//                                   cb.tran_amount, 0 ) )                as loss_amount,
//                      sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,
//                      sum( decode( cba.action_code,'W',
//                                   cb.tran_amount, 0 ) )                as coll_amount
//              from    network_chargebacks         cb,
//                      network_chargeback_activity cba
//              where   cb.bank_number = :bankNumber and
//                      cb.merchant_number = 0 and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.action_date between :beginDate and :endDate and
//                      exists
//                      (
//                        select  cbai.cb_load_sec
//                        from    network_chargeback_activity cbai
//                        where   cbai.cb_load_sec = cba.cb_load_sec and
//                                cbai.file_load_sec != cba.file_load_sec and
//                                cbai.action_date < cba.action_date
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                                             as org_num,\n                     :2                                           as hierarchy_node,\n                    'Unassigned Chargebacks'                          as org_name,\n                     :3                                         as report_date,\n                    count(cba.action_code)                            as tot_worked_count,\n                    sum(cb.tran_amount)                               as tot_worked_amount,\n                    sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,\n                    sum( decode( cba.action_code,'D', cb.tran_amount, 'B',\n                                 cb.tran_amount, 0 ) )                as mchb_amount,\n                    sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,\n                    sum( decode( cba.action_code,'S',\n                                 cb.tran_amount, 0 ) )                as repr_amount,\n                    sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,\n                    sum( decode( cba.action_code,'C',\n                                 cb.tran_amount, 0 ) )                as remc_amount,\n                    sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,\n                    sum( decode( cba.action_code,'L',\n                                 cb.tran_amount, 0 ) )                as loss_amount,\n                    sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,\n                    sum( decode( cba.action_code,'W',\n                                 cb.tran_amount, 0 ) )                as coll_amount\n            from    network_chargebacks         cb,\n                    network_chargeback_activity cba\n            where   cb.bank_number =  :4  and\n                    cb.merchant_number = 0 and\n                    cb.incoming_date between ( :5 -180) and  :6  and\n                    (  :7  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :8  ) and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.action_date between  :9  and  :10  and\n                    exists\n                    (\n                      select  cbai.cb_load_sec\n                      from    network_chargeback_activity cbai\n                      where   cbai.cb_load_sec = cba.cb_load_sec and\n                              cbai.file_load_sec != cba.file_load_sec and\n                              cbai.action_date < cba.action_date\n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgNode);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setString(7,CardType);
   __sJT_st.setString(8,CardType);
   __sJT_st.setDate(9,beginDate);
   __sJT_st.setDate(10,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2233^11*/
          //System.out.println("query 11 time: " + (System.currentTimeMillis() - ts));
          resultSet = it.getResultSet();

          while(resultSet.next())
          {
            addData(resultSet,CB_REWORKED);
          }
          resultSet.close();
          it.close();
        }

        ts = System.currentTimeMillis();
        /*@lineinfo:generated-code*//*@lineinfo:2246^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      ordered
//                      use_nl(gm cb)
//                    */
//                    o.org_num                                         as org_num,
//                    o.org_group                                       as hierarchy_node,
//                    o.org_name                                        as org_name,
//                    :beginDate                                        as report_date,
//                    count(cba.action_code)                            as tot_worked_count,
//                    sum(cb.tran_amount)                               as tot_worked_amount,
//                    sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,
//                    sum( decode( cba.action_code,'D', cb.tran_amount, 'B',
//                                 cb.tran_amount, 0 ) )                as mchb_amount,
//                    sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,
//                    sum( decode( cba.action_code,'S',
//                                 cb.tran_amount, 0 ) )                as repr_amount,
//                    sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,
//                    sum( decode( cba.action_code,'C',
//                                 cb.tran_amount, 0 ) )                as remc_amount,
//                    sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,
//                    sum( decode( cba.action_code,'L',
//                                 cb.tran_amount, 0 ) )                as loss_amount,
//                    sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,
//                    sum( decode( cba.action_code,'W',
//                                 cb.tran_amount, 0 ) )                as coll_amount
//            from    parent_org                  po,
//                    organization                o,
//                    group_merchant              gm,
//                    network_chargebacks         cb,
//                    network_chargeback_activity cba
//            where   po.parent_org_num = :orgId and
//                    o.org_num = po.org_num and
//                    gm.org_num = o.org_num and
//                    cb.merchant_number = gm.merchant_number and
//                    cb.incoming_date between (:beginDate-180) and :endDate and
//                    ( :CardType is null or 
//                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                    cba.cb_load_sec = cb.cb_load_sec and
//                    cba.action_date between :beginDate and :endDate and
//                    exists
//                    (
//                      select  cbai.cb_load_sec
//                      from    network_chargeback_activity cbai
//                      where   cbai.cb_load_sec = cba.cb_load_sec and
//                              cbai.file_load_sec != cba.file_load_sec and
//                              cbai.action_date < cba.action_date
//                    )
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    ordered\n                    use_nl(gm cb)\n                  */\n                  o.org_num                                         as org_num,\n                  o.org_group                                       as hierarchy_node,\n                  o.org_name                                        as org_name,\n                   :1                                         as report_date,\n                  count(cba.action_code)                            as tot_worked_count,\n                  sum(cb.tran_amount)                               as tot_worked_amount,\n                  sum( decode( cba.action_code,'D', 1,'B',1, 0 ) )  as mchb_count,\n                  sum( decode( cba.action_code,'D', cb.tran_amount, 'B',\n                               cb.tran_amount, 0 ) )                as mchb_amount,\n                  sum( decode( cba.action_code,'S', 1, 0 ) )        as repr_count,\n                  sum( decode( cba.action_code,'S',\n                               cb.tran_amount, 0 ) )                as repr_amount,\n                  sum( decode( cba.action_code,'C', 1, 0 ) )        as remc_count,\n                  sum( decode( cba.action_code,'C',\n                               cb.tran_amount, 0 ) )                as remc_amount,\n                  sum( decode( cba.action_code,'L', 1, 0 ) )        as loss_count,\n                  sum( decode( cba.action_code,'L',\n                               cb.tran_amount, 0 ) )                as loss_amount,\n                  sum( decode( cba.action_code,'W', 1, 0 ) )        as coll_count,\n                  sum( decode( cba.action_code,'W',\n                               cb.tran_amount, 0 ) )                as coll_amount\n          from    parent_org                  po,\n                  organization                o,\n                  group_merchant              gm,\n                  network_chargebacks         cb,\n                  network_chargeback_activity cba\n          where   po.parent_org_num =  :2  and\n                  o.org_num = po.org_num and\n                  gm.org_num = o.org_num and\n                  cb.merchant_number = gm.merchant_number and\n                  cb.incoming_date between ( :3 -180) and  :4  and\n                  (  :5  is null or \n                    decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :6  ) and\n                  cba.cb_load_sec = cb.cb_load_sec and\n                  cba.action_date between  :7  and  :8  and\n                  exists\n                  (\n                    select  cbai.cb_load_sec\n                    from    network_chargeback_activity cbai\n                    where   cbai.cb_load_sec = cba.cb_load_sec and\n                            cbai.file_load_sec != cba.file_load_sec and\n                            cbai.action_date < cba.action_date\n                  )\n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   __sJT_st.setString(5,CardType);
   __sJT_st.setString(6,CardType);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2296^9*/
        //System.out.println("query 12 time: " + (System.currentTimeMillis() - ts));
      }
      resultSet = it.getResultSet();

      while(resultSet.next())
      {
        addData(resultSet, CB_REWORKED);
      }
      resultSet.close();
      it.close();

      //**********************************************************************
      // DOUBLE WORKED ITEMS
      //**********************************************************************
      if ( SummaryType == ST_PARENT )
      {
        if ( bankNumber != 0 )
        {
          // select all including the items
          // without a merchant number assigned
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:2318^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  :orgId                                            as org_num,
//                      :nodeId                                           as hierarchy_node,
//                      :orgName                                          as org_name,
//                      :beginDate                                        as report_date,
//                      cba.cb_load_sec                                   as control_number,
//                      cb.tran_amount                                    as cb_amount
//              from    network_chargebacks         cb,
//                      network_chargeback_activity cba
//              where   cb.bank_number = :bankNumber and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.action_date between :beginDate and :endDate and
//                      exists
//                      (
//                        select  cbai.cb_load_sec
//                        from    network_chargeback_activity cbai
//                        where   cbai.cb_load_sec = cba.cb_load_sec and
//                                cbai.file_load_sec != cba.file_load_sec and
//                                cbai.action_date = cba.action_date
//                      )
//              order by cba.cb_load_sec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                                             as org_num,\n                     :2                                            as hierarchy_node,\n                     :3                                           as org_name,\n                     :4                                         as report_date,\n                    cba.cb_load_sec                                   as control_number,\n                    cb.tran_amount                                    as cb_amount\n            from    network_chargebacks         cb,\n                    network_chargeback_activity cba\n            where   cb.bank_number =  :5  and\n                    cb.incoming_date between ( :6 -180) and  :7  and\n                    (  :8  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :9  ) and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.action_date between  :10  and  :11  and\n                    exists\n                    (\n                      select  cbai.cb_load_sec\n                      from    network_chargeback_activity cbai\n                      where   cbai.cb_load_sec = cba.cb_load_sec and\n                              cbai.file_load_sec != cba.file_load_sec and\n                              cbai.action_date = cba.action_date\n                    )\n            order by cba.cb_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setString(3,orgName);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,CardType);
   __sJT_st.setString(9,CardType);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setDate(11,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2343^11*/
          //System.out.println("query 13 time: " + (System.currentTimeMillis() - ts));
        }
        else      //
        {
          // only select those with merchant numbers
          // under the current hierarchy node
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:2351^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                        ordered
//                        use_nl(gm cb)
//                      */
//                      :orgId                                            as org_num,
//                      :nodeId                                           as hierarchy_node,
//                      :orgName                                          as org_name,
//                      :beginDate                                        as report_date,
//                      cba.cb_load_sec                                   as control_number,
//                      cb.tran_amount                                    as cb_amount
//              from    group_merchant              gm,
//                      network_chargebacks         cb,
//                      network_chargeback_activity cba
//              where   gm.org_num = :orgId and
//                      cb.merchant_number = gm.merchant_number and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.action_date between :beginDate and :endDate and
//                      exists
//                      (
//                        select  cbai.cb_load_sec
//                        from    network_chargeback_activity cbai
//                        where   cbai.cb_load_sec = cba.cb_load_sec and
//                                cbai.file_load_sec != cba.file_load_sec and
//                                cbai.action_date = cba.action_date
//                      )
//              order by cba.cb_load_sec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                      ordered\n                      use_nl(gm cb)\n                    */\n                     :1                                             as org_num,\n                     :2                                            as hierarchy_node,\n                     :3                                           as org_name,\n                     :4                                         as report_date,\n                    cba.cb_load_sec                                   as control_number,\n                    cb.tran_amount                                    as cb_amount\n            from    group_merchant              gm,\n                    network_chargebacks         cb,\n                    network_chargeback_activity cba\n            where   gm.org_num =  :5  and\n                    cb.merchant_number = gm.merchant_number and\n                    cb.incoming_date between ( :6 -180) and  :7  and\n                    (  :8  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :9  ) and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.action_date between  :10  and  :11  and\n                    exists\n                    (\n                      select  cbai.cb_load_sec\n                      from    network_chargeback_activity cbai\n                      where   cbai.cb_load_sec = cba.cb_load_sec and\n                              cbai.file_load_sec != cba.file_load_sec and\n                              cbai.action_date = cba.action_date\n                    )\n            order by cba.cb_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setString(3,orgName);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setLong(5,orgId);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,CardType);
   __sJT_st.setString(9,CardType);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setDate(11,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"25com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2382^11*/
          //System.out.println("query 14 time: " + (System.currentTimeMillis() - ts));
        }
      }
      else    // ST_CHILD
      {
        if ( bankNumber != 0 )
        {
          orgNode = orgIdToHierarchyNode( orgId );

          // if the current node is a bank node then load all
          // the unassigned chargebacks.
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:2395^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  :orgId                                            as org_num,
//                      :orgNode                                          as hierarchy_node,
//                      'Unassigned Chargebacks'                          as org_name,
//                      :beginDate                                        as report_date,
//                      cba.cb_load_sec                                   as control_number,
//                      cb.tran_amount                                    as cb_amount
//              from    network_chargebacks         cb,
//                      network_chargeback_activity cba
//              where   cb.bank_number = :bankNumber and
//                      cb.merchant_number = 0 and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.action_date between :beginDate and :endDate and
//                      exists
//                      (
//                        select  cbai.cb_load_sec
//                        from    network_chargeback_activity cbai
//                        where   cbai.cb_load_sec = cba.cb_load_sec and
//                                cbai.file_load_sec != cba.file_load_sec and
//                                cbai.action_date = cba.action_date
//                      )
//              order by cba.cb_load_sec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                                             as org_num,\n                     :2                                           as hierarchy_node,\n                    'Unassigned Chargebacks'                          as org_name,\n                     :3                                         as report_date,\n                    cba.cb_load_sec                                   as control_number,\n                    cb.tran_amount                                    as cb_amount\n            from    network_chargebacks         cb,\n                    network_chargeback_activity cba\n            where   cb.bank_number =  :4  and\n                    cb.merchant_number = 0 and\n                    cb.incoming_date between ( :5 -180) and  :6  and\n                    (  :7  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :8  ) and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.action_date between  :9  and  :10  and\n                    exists\n                    (\n                      select  cbai.cb_load_sec\n                      from    network_chargeback_activity cbai\n                      where   cbai.cb_load_sec = cba.cb_load_sec and\n                              cbai.file_load_sec != cba.file_load_sec and\n                              cbai.action_date = cba.action_date\n                    )\n            order by cba.cb_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgNode);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setString(7,CardType);
   __sJT_st.setString(8,CardType);
   __sJT_st.setDate(9,beginDate);
   __sJT_st.setDate(10,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2421^11*/
          //System.out.println("query 15 time: " + (System.currentTimeMillis() - ts));
          resultSet = it.getResultSet();

          while(resultSet.next())
          {
            addData(resultSet,CB_DOUBLE_WORKED);
          }
          resultSet.close();
          it.close();
        }

        ts = System.currentTimeMillis();
        /*@lineinfo:generated-code*//*@lineinfo:2434^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      ordered
//                      use_nl(gm cb)
//                    */
//                    o.org_num                                         as org_num,
//                    o.org_group                                       as hierarchy_node,
//                    o.org_name                                        as org_name,
//                    :beginDate                                        as report_date,
//                    cba.cb_load_sec                                   as control_number,
//                    cb.tran_amount                                    as cb_amount
//            from    parent_org                  po,
//                    organization                o,
//                    group_merchant              gm,
//                    network_chargebacks         cb,
//                    network_chargeback_activity cba
//            where   po.parent_org_num = :orgId and
//                    o.org_num = po.org_num and
//                    gm.org_num = o.org_num and
//                    cb.merchant_number = gm.merchant_number and
//                    cb.incoming_date between (:beginDate-180) and :endDate and
//                    ( :CardType is null or 
//                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                    cba.cb_load_sec = cb.cb_load_sec and
//                    cba.action_date between :beginDate and :endDate and
//                    exists
//                    (
//                      select  cbai.cb_load_sec
//                      from    network_chargeback_activity cbai
//                      where   cbai.cb_load_sec = cba.cb_load_sec and
//                              cbai.file_load_sec != cba.file_load_sec and
//                              cbai.action_date = cba.action_date
//                    )
//            order by o.org_num, o.org_group, o.org_name, cba.cb_load_sec
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    ordered\n                    use_nl(gm cb)\n                  */\n                  o.org_num                                         as org_num,\n                  o.org_group                                       as hierarchy_node,\n                  o.org_name                                        as org_name,\n                   :1                                         as report_date,\n                  cba.cb_load_sec                                   as control_number,\n                  cb.tran_amount                                    as cb_amount\n          from    parent_org                  po,\n                  organization                o,\n                  group_merchant              gm,\n                  network_chargebacks         cb,\n                  network_chargeback_activity cba\n          where   po.parent_org_num =  :2  and\n                  o.org_num = po.org_num and\n                  gm.org_num = o.org_num and\n                  cb.merchant_number = gm.merchant_number and\n                  cb.incoming_date between ( :3 -180) and  :4  and\n                  (  :5  is null or \n                    decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :6  ) and\n                  cba.cb_load_sec = cb.cb_load_sec and\n                  cba.action_date between  :7  and  :8  and\n                  exists\n                  (\n                    select  cbai.cb_load_sec\n                    from    network_chargeback_activity cbai\n                    where   cbai.cb_load_sec = cba.cb_load_sec and\n                            cbai.file_load_sec != cba.file_load_sec and\n                            cbai.action_date = cba.action_date\n                  )\n          order by o.org_num, o.org_group, o.org_name, cba.cb_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   __sJT_st.setString(5,CardType);
   __sJT_st.setString(6,CardType);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"27com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2469^9*/
        //System.out.println("query 16 time: " + (System.currentTimeMillis() - ts));
      }
      resultSet = it.getResultSet();

      while(resultSet.next())
      {
        addData(resultSet, CB_DOUBLE_WORKED);
      }
      resultSet.close();
      it.close();

      //**********************************************************************
      // PENDING ITEMS
      //**********************************************************************
      if ( SummaryType == ST_PARENT )
      {
        nodeId    = orgIdToHierarchyNode( orgId );
        orgName   = getOrgName( orgId );

        if ( bankNumber != 0 )
        {
          // select all including the items
          // without a merchant number assigned
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:2494^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  :orgId                    as org_num,
//                      :nodeId                   as hierarchy_node,
//                      :orgName                  as org_name,
//                      :beginDate                as report_date,
//                      count(cb.incoming_date)   as tot_pending_count,
//                      sum(cb.tran_amount)       as tot_pending_amount
//              from    network_chargebacks         cb
//              where   cb.bank_number = :bankNumber and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                      not exists
//                      (
//                        select cba.cb_load_sec
//                        from    network_chargeback_activity cba
//                        where   cba.cb_load_sec = cb.cb_load_sec and
//                                cba.action_date <= :endDate
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                     as org_num,\n                     :2                    as hierarchy_node,\n                     :3                   as org_name,\n                     :4                 as report_date,\n                    count(cb.incoming_date)   as tot_pending_count,\n                    sum(cb.tran_amount)       as tot_pending_amount\n            from    network_chargebacks         cb\n            where   cb.bank_number =  :5  and\n                    cb.incoming_date between ( :6 -180) and  :7  and\n                    (  :8  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :9  ) and\n                    not exists\n                    (\n                      select cba.cb_load_sec\n                      from    network_chargeback_activity cba\n                      where   cba.cb_load_sec = cb.cb_load_sec and\n                              cba.action_date <=  :10 \n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setString(3,orgName);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,CardType);
   __sJT_st.setString(9,CardType);
   __sJT_st.setDate(10,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"28com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2514^11*/
          //System.out.println("query 17 time: " + (System.currentTimeMillis() - ts));
        }
        else      //
        {
          // only select those with merchant numbers
          // under the current hierarchy node
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:2522^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                          ordered
//                          use_nl(gm cb)
//                      */
//                      :orgId                    as org_num,
//                      :nodeId                   as hierarchy_node,
//                      :orgName                  as org_name,
//                      :beginDate                as report_date,
//                      count(cb.incoming_date)   as tot_pending_count,
//                      sum(cb.tran_amount)       as tot_pending_amount
//              from    group_merchant              gm,
//                      network_chargebacks         cb
//              where   gm.org_num = :orgId and
//                      cb.merchant_number = gm.merchant_number and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                      not exists
//                      (
//                        select cba.cb_load_sec
//                        from    network_chargeback_activity cba
//                        where   cba.cb_load_sec = cb.cb_load_sec and
//                                cba.action_date <= :endDate
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                        ordered\n                        use_nl(gm cb)\n                    */\n                     :1                     as org_num,\n                     :2                    as hierarchy_node,\n                     :3                   as org_name,\n                     :4                 as report_date,\n                    count(cb.incoming_date)   as tot_pending_count,\n                    sum(cb.tran_amount)       as tot_pending_amount\n            from    group_merchant              gm,\n                    network_chargebacks         cb\n            where   gm.org_num =  :5  and\n                    cb.merchant_number = gm.merchant_number and\n                    cb.incoming_date between ( :6 -180) and  :7  and\n                    (  :8  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :9  ) and\n                    not exists\n                    (\n                      select cba.cb_load_sec\n                      from    network_chargeback_activity cba\n                      where   cba.cb_load_sec = cb.cb_load_sec and\n                              cba.action_date <=  :10 \n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setString(3,orgName);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setLong(5,orgId);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,CardType);
   __sJT_st.setString(9,CardType);
   __sJT_st.setDate(10,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"29com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2548^11*/
          //System.out.println("query 18 time: " + (System.currentTimeMillis() - ts));
        }
      }
      else    // ST_CHILD
      {
        if ( bankNumber != 0 )
        {
          orgNode = orgIdToHierarchyNode( orgId );

          // if the current node is a bank node then load all
          // the unassigned chargebacks.
          ts = System.currentTimeMillis();
          /*@lineinfo:generated-code*//*@lineinfo:2561^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  :orgId                    as org_num,
//                      :orgNode                  as hierarchy_node,
//                      'Unassigned Chargebacks'  as org_name,
//                      :beginDate                as report_date,
//                      count(cb.incoming_date)   as tot_pending_count,
//                      sum(cb.tran_amount)       as tot_pending_amount
//              from    network_chargebacks         cb
//              where   cb.bank_number = :bankNumber and
//                      cb.merchant_number = 0 and
//                      cb.incoming_date between (:beginDate-180) and :endDate and
//                      ( :CardType is null or 
//                        decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                      not exists
//                      (
//                        select cba.cb_load_sec
//                        from    network_chargeback_activity cba
//                        where   cba.cb_load_sec = cb.cb_load_sec and
//                                cba.action_date <= :endDate
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                     as org_num,\n                     :2                   as hierarchy_node,\n                    'Unassigned Chargebacks'  as org_name,\n                     :3                 as report_date,\n                    count(cb.incoming_date)   as tot_pending_count,\n                    sum(cb.tran_amount)       as tot_pending_amount\n            from    network_chargebacks         cb\n            where   cb.bank_number =  :4  and\n                    cb.merchant_number = 0 and\n                    cb.incoming_date between ( :5 -180) and  :6  and\n                    (  :7  is null or \n                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :8  ) and\n                    not exists\n                    (\n                      select cba.cb_load_sec\n                      from    network_chargeback_activity cba\n                      where   cba.cb_load_sec = cb.cb_load_sec and\n                              cba.action_date <=  :9 \n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgNode);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setString(7,CardType);
   __sJT_st.setString(8,CardType);
   __sJT_st.setDate(9,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"30com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2582^11*/
          //System.out.println("query 19 time: " + (System.currentTimeMillis() - ts));
          resultSet = it.getResultSet();

          while(resultSet.next())
          {
            addData( resultSet, CB_PENDING );
          }
          resultSet.close();
          it.close();
        }

        ts = System.currentTimeMillis();
        /*@lineinfo:generated-code*//*@lineinfo:2595^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      ordered
//                      use_nl(gm cb)
//                    */
//                    o.org_num                   as org_num,
//                    o.org_group                 as hierarchy_node,
//                    o.org_name                  as org_name,
//                    :beginDate                  as report_date,
//                    count(cb.incoming_date)     as tot_pending_count,
//                    sum(cb.tran_amount)         as tot_pending_amount
//            from    parent_org                  po,
//                    organization                o,
//                    group_merchant              gm,
//                    network_chargebacks         cb
//            where   po.parent_org_num = :orgId and
//                    o.org_num = po.org_num and
//                    gm.org_num = o.org_num and
//                    cb.merchant_number = gm.merchant_number and
//                    cb.incoming_date between (:beginDate-180) and :endDate and
//                    ( :CardType is null or 
//                      decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) = :CardType ) and
//                    not exists
//                    (
//                      select cba.cb_load_sec
//                      from    network_chargeback_activity cba
//                      where   cba.cb_load_sec = cb.cb_load_sec and
//                              cba.action_date <= :endDate
//                    )
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    ordered\n                    use_nl(gm cb)\n                  */\n                  o.org_num                   as org_num,\n                  o.org_group                 as hierarchy_node,\n                  o.org_name                  as org_name,\n                   :1                   as report_date,\n                  count(cb.incoming_date)     as tot_pending_count,\n                  sum(cb.tran_amount)         as tot_pending_amount\n          from    parent_org                  po,\n                  organization                o,\n                  group_merchant              gm,\n                  network_chargebacks         cb\n          where   po.parent_org_num =  :2  and\n                  o.org_num = po.org_num and\n                  gm.org_num = o.org_num and\n                  cb.merchant_number = gm.merchant_number and\n                  cb.incoming_date between ( :3 -180) and  :4  and\n                  (  :5  is null or \n                    decode(cb.card_type,null,decode_card_type(cb.card_number,null),cb.card_type) =  :6  ) and\n                  not exists\n                  (\n                    select cba.cb_load_sec\n                    from    network_chargeback_activity cba\n                    where   cba.cb_load_sec = cb.cb_load_sec and\n                            cba.action_date <=  :7 \n                  )\n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.reports.ChargebackActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   __sJT_st.setString(5,CardType);
   __sJT_st.setString(6,CardType);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.reports.ChargebackActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2626^9*/
        //System.out.println("query 20 time: " + (System.currentTimeMillis() - ts));
      }
      resultSet = it.getResultSet();

      while(resultSet.next())
      {
        addData( resultSet, CB_PENDING );
      }
      resultSet.close();
      it.close();

      if ( ReportType == RT_BALANCE_SHEET )
      {
        loadNetOffsetBatchId();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setCardType( String cardType )
  {
    CardType = cardType;
  }

  public void setProperties(HttpServletRequest request)
  {
    Date      sqlDate     = null;

    // load the default report properties
    super.setProperties( request );

    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         inc           = -1;

      if ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.MONDAY )
      {
        // friday data available on monday,
        inc = -3;
      }

      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, inc );
      sqlDate = new java.sql.Date( cal.getTime().getTime() );
      setReportDateBegin( sqlDate );
      setReportDateEnd( sqlDate );
    }

    if ( ReportType == RT_BALANCE_SHEET )
    {
      // balance sheet report only supports a single date
      setReportDateEnd( getReportDateBegin() );
    }
    
    CardType = HttpHelper.getString(request,"cardType",null);
  }

  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/