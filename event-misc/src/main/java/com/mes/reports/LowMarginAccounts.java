/*@lineinfo:filename=LowMarginAccounts*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/LowMarginAccounts.sqlj $

  Description:

  LowMarginAccounts
  
  Report to show accounts that have low estimated discount revenue.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 8/08/03 4:15p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.ComboDateField;
import com.mes.forms.DateRangeReport;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.ops.QueueConstants;
import com.mes.tools.DropDownTable;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class LowMarginAccounts extends DateRangeReport
{
  public class ReportRecord
  {
    public BeanDate  submitDate = new BeanDate();
    public BeanDate  activateDate = new BeanDate();
    public String    merchName;
    public String    repName;
    public String    association;
    public long      merchNum;
    public long      appSeqNum;
    public long      userId;
    public int       gridType;
    public int       planType;
    public int       appStatus;
    public int       appType;
    public double    monthlyVMCSales;
    public double    annualSales;
    public double    averageTicket;
    public double    discRate;
    public double    centsPerTran;
    public double    centsPerTran2;
    public double    centsPerAuth;
    public double    centsPerCapture;
    public double    interchangeRate;
    public double    interchangeFee;
    public double    assocAssessFee;
    public double    commissionPaid;
    public double    amexFee;
    public double    discFee;
    public double    additionalCharges;
    public boolean   isActive;
    public boolean   showRate;
    public boolean   showCentsPerTran;
    public boolean   priorStatements;
    public boolean   amexAccepted;
    public boolean   amexSplitDial;
    public boolean   discAccepted;
    public boolean   hasInternetFee;
    
    public long    revenueInBasisPoints    = 0L;
    
    public double  annualNetDiscount       = 0.0;
    public double  equipRentals            = 0.0;
    public double  equipPurchases          = 0.0;

    public double  totalAnnualSales        = 0.0;
    public double  totalAnnualNetDiscount  = 0.0;
    public double  totalEquipRentals       = 0.0;
    public double  totalEquipPurchases     = 0.0;
    
    public int     appCount                = 0;
    
    public String  statName;
    public String  gridName;
    
    public ReportRecord(ResultSet rs)
    {
      try
      {
        //extract fields
        submitDate.setSqlDate    (rs.getDate  ("app_created_date"));
        merchName             =   rs.getString("merch_business_name");
        merchNum              =   rs.getLong  ("merch_number");
        monthlyVMCSales       =   rs.getDouble("merch_month_visa_mc_sales");
        averageTicket         =   rs.getDouble("merch_average_cc_tran");
        gridType              =   rs.getInt   ("grid_type");
        appStatus             =   rs.getInt   ("merch_credit_status");
        isActive              =  (rs.getDate  ("date_activated") != null);
        planType              =   rs.getInt   ("tranchrg_discrate_type");
        discRate              =   rs.getDouble("tranchrg_disc_rate");
        centsPerTran          =   rs.getDouble("tranchrg_pass_thru");
        repName               =   rs.getString("login_name");
        interchangeRate       =   rs.getDouble("interchange_rate");
        assocAssessFee        =   rs.getDouble("assoc_assess_fee");
        interchangeFee        =   rs.getDouble("interchange_fee");
        appSeqNum             =   rs.getLong  ("app_seq_num");
        appType               =   rs.getInt   ("app_type");
        centsPerTran2         =   rs.getDouble("tranchrg_per_tran");
        centsPerAuth          =   rs.getDouble("tranchrg_per_auth");
        centsPerCapture       =   rs.getDouble("tranchrg_per_capture");
        equipRentals          =   rs.getDouble("rental_total");
        equipPurchases        =   rs.getDouble("purchase_total");
        priorStatements       =  (rs.getString("merch_prior_statements").equals("Y"));
        userId                =   rs.getLong  ("app_user_id");
        commissionPaid        =   rs.getDouble("pay_total");
        association           =   rs.getString("dmagent");
        amexAccepted          =  (rs.getLong  ("amex_app_seq_num") != 0L);
        try { amexSplitDial   =  (rs.getString("split_dial").equals("Y")); }
        catch (Exception e)   {   amexSplitDial = false; }
        discAccepted          =  (rs.getLong  ("disc_app_seq_num") != 0L);
        amexFee               =   rs.getDouble("amex_charges");
        discFee               =   rs.getDouble("disc_charges");
        try { hasInternetFee  =  (rs.getDouble("i_fee") > 0); }
        catch (Exception e)   {   hasInternetFee = false; }
        additionalCharges     =   rs.getDouble("charges");
        if (isActive)
        {
          activateDate.setSqlDate(rs.getDate("date_activated"));
        }
        
        // for now, just total all possible cents per tran fees into one
        centsPerTran   += centsPerTran2 + centsPerAuth + centsPerCapture + additionalCharges;
        
        // calculate some fields
        annualSales = monthlyVMCSales * 12;
        switch (planType)
        {
          // variable rate
          case mesConstants.APP_PS_VARIABLE_RATE:
            // not sure how to handle this
            revenueInBasisPoints = 0L;
            break;
          
          // fixed rate, no cents per tran
          case mesConstants.APP_PS_FIXED_RATE:
          // fixed rate and cents per tran
          case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
          // rate (ignoring non-qual rate)
          case mesConstants.CBT_APP_BUNDLED_RATE_PLAN:
          // rate + (per item + per auth)
          case mesConstants.CBT_APP_UNBUNDLED_RATE_PLAN:
          // rate and cents per tran
          case mesConstants.CBT_APP_PERITEM_DISCRATE_PLAN:
          // new transcom...
          case mesConstants.TRANS_APP_BUCKET_ONLY_PLAN:
          case mesConstants.TRANS_APP_BUCKET_WITH_PERITEM_PLAN:
          case mesConstants.TRANS_APP_DETAILED_STATEMENT_PLAN:
            revenueInBasisPoints = (long)
              ((((discRate / 100) + (centsPerTran / averageTicket)) * 10000) - 
               ((((interchangeRate + assocAssessFee) / 100) + (interchangeFee / averageTicket)) * 10000) 
               + 0.5);
            break;
               
          // cents per tran (interchange passed through so 
          // tran cost component is cancelled out)
          case mesConstants.APP_PS_INTERCHANGE:
          // per item + per auth + per capture
          case mesConstants.CBT_APP_INTERCHANGE_PLUS_PLAN:
            revenueInBasisPoints = (long)
              (((centsPerTran / averageTicket) * 10000) + 0.5);
            break;
        }
        annualNetDiscount = (averageTicket * ((double)revenueInBasisPoints / 10000)) * (annualSales / averageTicket) + 0.005;
        annualNetDiscount = (double)((long)(annualNetDiscount * 100)) / 100;
        
        gridName = "---";
        if (gridType <= java.lang.reflect.Array.getLength(gridNames) && gridType > 0)
        {
          gridName = gridNames[gridType];
        }

        statName = "";
        switch (appStatus)
        {
          default:
            statName = "???";
            break;
          
          case QueueConstants.CREDIT_NEW:
            statName = "NEW";
            break;
          
          case QueueConstants.CREDIT_APPROVE:
            statName = "APRV";
            break;
          
          case QueueConstants.CREDIT_DECLINE:
            statName = "DECL";
            break;
          
          case QueueConstants.CREDIT_PEND:
            statName = "PEND";
            break;
          
          case QueueConstants.CREDIT_CANCEL:
            statName = "CAN";
            break;
        }
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "::ReportRecord(): "
          + e.toString());
        logEntry("ReportRecord()",e.toString());
      }
    }
  }

  private Vector records = new Vector();
  
  protected class SourceTable extends DropDownTable
  {
    public SourceTable()
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      try
      {
        connect();
        
        // determine if user is at top of hierarchy
        boolean isHierarchyTop = 
          user.getHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE;
          
        // if user is mes or is at hierarchy top, add mes app type
        // and mes regional options
        if (isHierarchyTop || user.getAppType() == 0)
        {
          addElement("0","MES");
          
          // load mes regions with source values of 'R1', 'R2', etc.
          /*@lineinfo:generated-code*//*@lineinfo:257^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  'R' || to_char(rownum),
//                      'MES - ' || rep_region_desc
//              from    rep_region
//              order by rep_region_desc asc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  'R' || to_char(rownum),\n                    'MES - ' || rep_region_desc\n            from    rep_region\n            order by rep_region_desc asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.LowMarginAccounts",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.LowMarginAccounts",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:263^11*/
          rs = it.getResultSet();
          while (rs.next())
          {
            addElement(rs);
          }
          it.close();
        }
        
        // add remaining app type is at hierarchy top
        if (isHierarchyTop)
        {
          // get remaining app types
          /*@lineinfo:generated-code*//*@lineinfo:276^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type_code,
//                      app_description
//              from    app_type
//              where   app_type_code > 0
//              order by app_type_code asc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_type_code,\n                    app_description\n            from    app_type\n            where   app_type_code > 0\n            order by app_type_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.LowMarginAccounts",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.LowMarginAccounts",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:283^11*/
          rs = it.getResultSet();
          while (rs.next())
          {
            addElement(rs);
          }
        }
        // otherwise add the user's app type only
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:293^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type_code,
//                      app_description
//              from    app_type
//              where   app_type_code = :user.getAppType()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1117 = user.getAppType();
  try {
   String theSqlTS = "select  app_type_code,\n                    app_description\n            from    app_type\n            where   app_type_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.LowMarginAccounts",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_1117);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.LowMarginAccounts",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:299^11*/
          rs = it.getResultSet();
          if (rs.next())
          {
            addElement(rs);
          }
        }
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "::SourceTable(): "
          + e.toString());
        logEntry("SourceTable()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }
  
  protected class StatusTable extends DropDownTable
  {
    public StatusTable()
    {
      addElement("0","Any Status");
      addElement(Integer.toString(QueueConstants.CREDIT_NEW),     "New");
      addElement(Integer.toString(QueueConstants.CREDIT_APPROVE), "Approved");
      addElement(Integer.toString(QueueConstants.CREDIT_DECLINE), "Declined");
      addElement(Integer.toString(QueueConstants.CREDIT_PEND),    "Pending");
    }
  }
  
  private static final String[][] appTypes = 
  {
    { "Submission Date", "0" },
    { "Activation Date", "1" },
    { "Submission Date (Exclude Activated)", "2" },
  };
  
  protected void createFields(HttpServletRequest request)
  {
    try
    {
      fields.add(new RadioButtonField ("reportType",      appTypes,0,false,"Select a report type"));
      fields.add(new DropDownField    ("source",          new SourceTable(),false));
      fields.add(new DropDownField    ("status",          new StatusTable(),false));
      fields.add(new Field            ("loginName",       32,12,true));
      fields.add(new NumberField      ("basisPoints",     5,5,true,0));
      
      // set default value
      fields.setData("source","0");
      fields.setData("status","0");
      fields.setData("reportType","0");
      fields.setData("basisPoints","15");
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }
  
  protected void preHandleRequest(HttpServletRequest request)
  {
    super.preHandleRequest(request);
    fields.setHtmlExtra("class=\"formFields\"");
  }
  
  public static String gridNames[] = 
  {
    "",
    "RTL",
    "MOTO",
    "HTL",
    "SPMK",
    "INT",
    "EM",
    "HTL",
    "RTL",
    "MOTO",
    "RST"
  };
  
  public Vector getRecords()
  {
    return records;
  }
      
  protected boolean autoLoad()
  {
    boolean loadOk = false;
  
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      connect();
      
      // determine app and region types from source field
      int appType = 0;
      int regionType = -1;
      String source = fields.getData("source");
      if (source.charAt(0) == 'R')
      {
        regionType = source.charAt(1) - '0';
      }
      else
      {
        appType = Integer.parseInt(source);
      }
      
      // get date range fields
      ComboDateField fromDate  = (ComboDateField)fields.getField("fromDate");
      ComboDateField toDate    = (ComboDateField)fields.getField("toDate");
      
      // get login name to filter by
      String loginName = fields.getData("loginName");
      int filterByLogin = (loginName.equals("") ? 0 : 1);
      
      // get report type
      int reportType = fields.getField("reportType").asInteger();
      
      // get status filter
      int status = fields.getField("status").asInteger();
      
      /*@lineinfo:generated-code*//*@lineinfo:428^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  a.app_created_date          app_created_date,
//                  m.merch_business_name       merch_business_name,
//                  m.merch_number              merch_number,
//                  m.merch_month_visa_mc_sales merch_month_visa_mc_sales,
//                  m.merch_average_cc_tran     merch_average_cc_tran,
//                  i.grid_type                 grid_type,
//                  m.merch_credit_status       merch_credit_status,
//                  m.date_activated            date_activated,
//                  t.tranchrg_discrate_type    tranchrg_discrate_type,
//                  t.tranchrg_disc_rate        tranchrg_disc_rate,
//                  t.tranchrg_pass_thru        tranchrg_pass_thru,
//                  u.login_name                login_name,
//                  i.interchange_rate          interchange_rate,
//                  i.assoc_assess_fee          assoc_assess_fee,
//                  i.interchange_fee           interchange_fee,
//                  a.app_seq_num               app_seq_num,
//                  a.app_type                  app_type,
//                  t.tranchrg_per_tran         tranchrg_per_tran,
//                  t.tranchrg_per_auth         tranchrg_per_auth,
//                  t.tranchrg_per_capture      tranchrg_per_capture,
//                  ers.rental_total            rental_total,
//                  eps.purchase_total          purchase_total,
//                  m.merch_prior_statements    merch_prior_statements,
//                  a.app_user_id               app_user_id,
//                  rc.pay_total                pay_total,
//                  mif.dmagent                 dmagent,
//                  aa.app_seq_num              amex_app_seq_num,
//                  aa.split_dial               split_dial,
//                  da.app_seq_num              disc_app_seq_num,
//                  amex.charges                amex_charges,
//                  disc.charges                disc_charges,
//                  it.charge                   i_fee,
//                  f.charges                   charges
//  
//          from    application                 a,
//                  merchant                    m,
//                  tranchrg                    t,
//                  amex_fee_per_tran           amex,
//                  disc_fee_per_tran           disc,
//                  users                       u,
//                  equip_rental_summaries      ers,
//                  equip_purchase_summaries    eps,
//                  additional_per_tran_fees    f,
//                  rep_commissions             rc,
//                  mif                         mif,
//                  amex_accepted               aa,
//                  disc_accepted               da,
//                  internet_per_tran_fees      it,
//                  sales_rep                   s,
//                  interchange_cost            i
//                  
//          where   a.app_type = :appType and
//                  a.app_user_id = u.user_id and
//                  a.app_seq_num = m.app_seq_num and
//                  a.app_seq_num = t.app_seq_num and
//                  instr(lower(m.merch_business_name),'test') = 0 and
//                  ( a.app_user_login = :loginName or
//                    :filterByLogin = 0 ) and
//                  ( m.merch_credit_status = :status or 
//                    ( :status = 0 and m.merch_credit_status <> 4 ) ) and
//                  ( ( ( trunc(a.app_created_date) between 
//                          :fromDate.getSqlDate() and 
//                          :toDate.getSqlDate() ) and
//                      ( :reportType = 0 or 
//                        ( :reportType = 2 and
//                          m.date_activated is null ) ) ) or
//                    ( ( trunc(m.date_activated) between 
//                        :fromDate.getSqlDate() and 
//                        :toDate.getSqlDate() ) and 
//                      :reportType = 1 ) ) and
//                  t.cardtype_code = 1 and
//                  (m.pricing_grid = i.grid_type or
//                   ((m.pricing_grid is null or m.pricing_grid = 0) and
//                     (((m.industype_code = 6 or m.industype_code = 7 or m.loctype_code = 2) and i.grid_type = 2) or
//                      ((m.industype_code <> 6 and m.industype_code <> 7 and m.loctype_code <> 2) and i.grid_type = 1)))) and
//                  a.app_seq_num = ers.app_seq_num(+) and
//                  a.app_seq_num = eps.app_seq_num(+) and
//                  a.app_seq_num = f.app_seq_num(+) and
//                  a.app_seq_num = rc.app_seq_num(+) and
//                  m.merch_number = mif.merchant_number(+) and
//                  a.app_seq_num = aa.app_seq_num(+) and
//                  a.app_seq_num = da.app_seq_num(+) and
//                  a.app_seq_num = amex.app_seq_num(+) and
//                  a.app_seq_num = disc.app_seq_num(+) and
//                  a.app_seq_num = it.app_seq_num(+) and
//                  a.app_user_id = s.user_id(+) and
//                  ( :regionType = -1 or :regionType = s.rep_region )
//           
//          order by  a.app_created_date asc,
//                    m.date_activated asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_1118 = fromDate.getSqlDate();
 java.sql.Date __sJT_1119 = toDate.getSqlDate();
 java.sql.Date __sJT_1120 = fromDate.getSqlDate();
 java.sql.Date __sJT_1121 = toDate.getSqlDate();
  try {
   String theSqlTS = "select  a.app_created_date          app_created_date,\n                m.merch_business_name       merch_business_name,\n                m.merch_number              merch_number,\n                m.merch_month_visa_mc_sales merch_month_visa_mc_sales,\n                m.merch_average_cc_tran     merch_average_cc_tran,\n                i.grid_type                 grid_type,\n                m.merch_credit_status       merch_credit_status,\n                m.date_activated            date_activated,\n                t.tranchrg_discrate_type    tranchrg_discrate_type,\n                t.tranchrg_disc_rate        tranchrg_disc_rate,\n                t.tranchrg_pass_thru        tranchrg_pass_thru,\n                u.login_name                login_name,\n                i.interchange_rate          interchange_rate,\n                i.assoc_assess_fee          assoc_assess_fee,\n                i.interchange_fee           interchange_fee,\n                a.app_seq_num               app_seq_num,\n                a.app_type                  app_type,\n                t.tranchrg_per_tran         tranchrg_per_tran,\n                t.tranchrg_per_auth         tranchrg_per_auth,\n                t.tranchrg_per_capture      tranchrg_per_capture,\n                ers.rental_total            rental_total,\n                eps.purchase_total          purchase_total,\n                m.merch_prior_statements    merch_prior_statements,\n                a.app_user_id               app_user_id,\n                rc.pay_total                pay_total,\n                mif.dmagent                 dmagent,\n                aa.app_seq_num              amex_app_seq_num,\n                aa.split_dial               split_dial,\n                da.app_seq_num              disc_app_seq_num,\n                amex.charges                amex_charges,\n                disc.charges                disc_charges,\n                it.charge                   i_fee,\n                f.charges                   charges\n\n        from    application                 a,\n                merchant                    m,\n                tranchrg                    t,\n                amex_fee_per_tran           amex,\n                disc_fee_per_tran           disc,\n                users                       u,\n                equip_rental_summaries      ers,\n                equip_purchase_summaries    eps,\n                additional_per_tran_fees    f,\n                rep_commissions             rc,\n                mif                         mif,\n                amex_accepted               aa,\n                disc_accepted               da,\n                internet_per_tran_fees      it,\n                sales_rep                   s,\n                interchange_cost            i\n                \n        where   a.app_type =  :1  and\n                a.app_user_id = u.user_id and\n                a.app_seq_num = m.app_seq_num and\n                a.app_seq_num = t.app_seq_num and\n                instr(lower(m.merch_business_name),'test') = 0 and\n                ( a.app_user_login =  :2  or\n                   :3  = 0 ) and\n                ( m.merch_credit_status =  :4  or \n                  (  :5  = 0 and m.merch_credit_status <> 4 ) ) and\n                ( ( ( trunc(a.app_created_date) between \n                         :6  and \n                         :7  ) and\n                    (  :8  = 0 or \n                      (  :9  = 2 and\n                        m.date_activated is null ) ) ) or\n                  ( ( trunc(m.date_activated) between \n                       :10  and \n                       :11  ) and \n                     :12  = 1 ) ) and\n                t.cardtype_code = 1 and\n                (m.pricing_grid = i.grid_type or\n                 ((m.pricing_grid is null or m.pricing_grid = 0) and\n                   (((m.industype_code = 6 or m.industype_code = 7 or m.loctype_code = 2) and i.grid_type = 2) or\n                    ((m.industype_code <> 6 and m.industype_code <> 7 and m.loctype_code <> 2) and i.grid_type = 1)))) and\n                a.app_seq_num = ers.app_seq_num(+) and\n                a.app_seq_num = eps.app_seq_num(+) and\n                a.app_seq_num = f.app_seq_num(+) and\n                a.app_seq_num = rc.app_seq_num(+) and\n                m.merch_number = mif.merchant_number(+) and\n                a.app_seq_num = aa.app_seq_num(+) and\n                a.app_seq_num = da.app_seq_num(+) and\n                a.app_seq_num = amex.app_seq_num(+) and\n                a.app_seq_num = disc.app_seq_num(+) and\n                a.app_seq_num = it.app_seq_num(+) and\n                a.app_user_id = s.user_id(+) and\n                (  :13  = -1 or  :14  = s.rep_region )\n         \n        order by  a.app_created_date asc,\n                  m.date_activated asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.LowMarginAccounts",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   __sJT_st.setString(2,loginName);
   __sJT_st.setInt(3,filterByLogin);
   __sJT_st.setInt(4,status);
   __sJT_st.setInt(5,status);
   __sJT_st.setDate(6,__sJT_1118);
   __sJT_st.setDate(7,__sJT_1119);
   __sJT_st.setInt(8,reportType);
   __sJT_st.setInt(9,reportType);
   __sJT_st.setDate(10,__sJT_1120);
   __sJT_st.setDate(11,__sJT_1121);
   __sJT_st.setInt(12,reportType);
   __sJT_st.setInt(13,regionType);
   __sJT_st.setInt(14,regionType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.LowMarginAccounts",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:520^7*/
      rs = it.getResultSet();
      while (rs.next())
      {
        records.add(new ReportRecord(rs));
      }
      
      loadOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::autoLoad(): "
        + e.toString());
      logEntry("autoLoad()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }

    return loadOk;
  }
  protected boolean autoSubmit()
  {
    if (_isAutoValid)
    {
      return autoLoad();
    }
    return false;
  }
}/*@lineinfo:generated-code*/