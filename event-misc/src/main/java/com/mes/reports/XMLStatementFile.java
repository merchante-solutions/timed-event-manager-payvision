/*@lineinfo:filename=XMLStatementFile*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/reports/XMLStatementFile.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-10-08 10:25:00 -0700 (Wed, 08 Oct 2008) $
  Version            : $Revision: 15432 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import org.apache.log4j.Logger;
//DOM4J
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXEventRecorder;
import org.dom4j.io.SAXWriter;
import oracle.sql.BLOB;

public class XMLStatementFile extends GenericRecord
{
  static Logger log = Logger.getLogger(XMLStatementFile.class);

  private Element snippet;
  private HashMap plans;

  public static String VISA_OLD     = "V";
  public static String VISA         = "VS";
  public static String VISALARGE    = "VL";
  public static String VISABUSINESS = "VB";
  public static String VISACHECK    = "VD";
  public static String VISACASH     = "VA";
  public static String MC_OLD       = "M";
  public static String MC           = "MC";
  public static String MCLARGE      = "ML";
  public static String MCBUSINESS   = "MB";
  public static String MCCHECK      = "MD";
  public static String MCCASH       = "MA";
  public static String DEBIT        = "DB";
  public static String DISC         = "DS";
  public static String AMEX         = "AM";
  public static String DINERS       = "DC";
  public static String JCB          = "JB";
  public static String EBT          = "EB";
  public static String PRIVATE1     = "P1";
  public static String PRIVATE2     = "P2";
  public static String PRIVATE3     = "P3";
  public static String CHECK        = "CK";
  
  private int commitCount = 0;

  public XMLStatementFile()
  {
    loadMap();
  }
  
  public void closeConnection()
  {
    // make sure latest changes are committed
    commit();
    cleanUp();
  }
  
  public void openConnection()
  {
    connect();
  }
  
  private void loadMap()
  {
    if(plans == null)
    {
      plans = new HashMap();
    }

    initPlan(VISA);
    initPlan(VISA_OLD);
    initPlan(VISALARGE);
    initPlan(VISABUSINESS);
    initPlan(VISACHECK);
    initPlan(VISACASH);
    initPlan(MC);
    initPlan(MC_OLD);
    initPlan(MCLARGE);
    initPlan(MCBUSINESS);
    initPlan(MCCHECK);
    initPlan(MCCASH);
    initPlan(DEBIT);
    initPlan(DISC);
    initPlan(AMEX);
    initPlan(DINERS);
    initPlan(JCB);
    initPlan(EBT);
    initPlan(PRIVATE1);
    initPlan(PRIVATE2);
    initPlan(PRIVATE3);
    initPlan(CHECK);
  }

  private void initPlan(String planCode)
  {
    Plan p = (Plan)plans.get(planCode);
    if(p == null)
    {
      plans.put(planCode, new Plan());
    }
    else
    {
      p.reset();
    }
  }

  public void getData(String getMerchNum, long getRecNum, long getYearMonth)
  {
    //TODO
  }

  public boolean updateData(Object obj)
  {
    //TODO
    return false;
  }

  public boolean submitData()
  {
    log.debug("submitting data...");
    boolean isOK = false;

    //get the plans
    Plan visa         = (Plan)plans.get(VISA);
    Plan visaOld      = (Plan)plans.get(VISA_OLD);
    Plan visaLarge    = (Plan)plans.get(VISALARGE);
    Plan visaBusiness = (Plan)plans.get(VISABUSINESS);
    Plan visaCheck    = (Plan)plans.get(VISACHECK);
    Plan visaCash     = (Plan)plans.get(VISACASH);
    Plan mc           = (Plan)plans.get(MC);
    Plan mcOld        = (Plan)plans.get(MC_OLD);
    Plan mcLarge      = (Plan)plans.get(MCLARGE);
    Plan mcBusiness   = (Plan)plans.get(MCBUSINESS);
    Plan mcCheck      = (Plan)plans.get(MCCHECK);
    Plan mcCash       = (Plan)plans.get(MCCASH);
    Plan dinr         = (Plan)plans.get(DINERS);
    Plan disc         = (Plan)plans.get(DISC);
    Plan amex         = (Plan)plans.get(AMEX);
    Plan jcb          = (Plan)plans.get(JCB);
    Plan debit        = (Plan)plans.get(DEBIT);
    Plan ebt          = (Plan)plans.get(EBT);
    Plan p1           = (Plan)plans.get(PRIVATE1);
    Plan p2           = (Plan)plans.get(PRIVATE2);
    Plan p3           = (Plan)plans.get(PRIVATE3);
    
    int     vsCnt = (visa.sCnt + visaLarge.sCnt + visaBusiness.sCnt + visaCheck.sCnt + visaCash.sCnt + visaOld.sCnt);
    double  vsAmt = (visa.sAmt + visaLarge.sAmt + visaBusiness.sAmt + visaCheck.sAmt + visaCash.sAmt + visaOld.sAmt);
    int     vcCnt = (visa.cCnt + visaLarge.cCnt + visaBusiness.cCnt + visaCheck.cCnt + visaCash.cCnt + visaOld.cCnt);
    double  vcAmt = (visa.cAmt + visaLarge.cAmt + visaBusiness.cAmt + visaCheck.cAmt + visaCash.cAmt + visaOld.cAmt);
    double  vdAmt = (visa.dAmt + visaLarge.dAmt + visaBusiness.dAmt + visaCheck.dAmt + visaCash.dAmt + visaOld.dAmt);
    
    int     msCnt = (mc.sCnt + mcLarge.sCnt + mcBusiness.sCnt + mcCheck.sCnt + mcCash.sCnt + mcOld.sCnt);
    double  msAmt = (mc.sAmt + mcLarge.sAmt + mcBusiness.sAmt + mcCheck.sAmt + mcCash.sAmt + mcOld.sAmt);
    int     mcCnt = (mc.cCnt + mcLarge.cCnt + mcBusiness.cCnt + mcCheck.cCnt + mcCash.cCnt + mcOld.cCnt);
    double  mcAmt = (mc.cAmt + mcLarge.cAmt + mcBusiness.cAmt + mcCheck.cAmt + mcCash.cAmt + mcOld.cAmt);
    double  mdAmt = (mc.dAmt + mcLarge.dAmt + mcBusiness.dAmt + mcCheck.dAmt + mcCash.dAmt + mcOld.dAmt);

    try
    {
      connect();
      
      setAutoCommit(false);
      
      // insert the column data along with an empty blob
      /*@lineinfo:generated-code*//*@lineinfo:197^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_statements
//          (
//            merch_num,
//            rec_num,
//            mystery_id,
//            year_month,
//            dda,
//            transit_routing,
//            load_file_name,
//            bank_num,
//            discount,
//            fees,
//  
//            visa_sales_cnt,
//            visa_sales_amt,
//            visa_credit_cnt,
//            visa_credit_amt,
//            visa_disc_amt,
//  
//            mc_sales_cnt,
//            mc_sales_amt,
//            mc_credit_cnt,
//            mc_credit_amt,
//            mc_disc_amt,
//  
//            dinr_sales_cnt,
//            dinr_sales_amt,
//            dinr_credit_cnt,
//            dinr_credit_amt,
//            dinr_disc_amt,
//  
//            disc_sales_cnt,
//            disc_sales_amt,
//            disc_credit_cnt,
//            disc_credit_amt,
//            disc_disc_amt,
//  
//            amex_sales_cnt,
//            amex_sales_amt,
//            amex_credit_cnt,
//            amex_credit_amt,
//            amex_disc_amt,
//  
//            jcb_sales_cnt,
//            jcb_sales_amt,
//            jcb_credit_cnt,
//            jcb_credit_amt,
//            jcb_disc_amt,
//  
//            debit_sales_cnt,
//            debit_sales_amt,
//            debit_credit_cnt,
//            debit_credit_amt,
//            debit_disc_amt,
//            
//            p1_sales_cnt,
//            p1_sales_amt,
//            p1_credit_cnt,
//            p1_credit_amt,
//            p1_disc_amt,
//  
//            p2_sales_cnt,
//            p2_sales_amt,
//            p2_credit_cnt,
//            p2_credit_amt,
//            p2_disc_amt,
//  
//            p3_sales_cnt,
//            p3_sales_amt,
//            p3_credit_cnt,
//            p3_credit_amt,
//            p3_disc_amt,
//  
//            statement_data
//          )
//          values
//          (
//            replace(:merchNum, '-'),
//            :recNum,
//            :mystId,
//            :yearMonth,
//            :dda,
//            :trNum,
//            :loadFilename,
//            :bankNum,
//            :discount,
//            :fees,
//  
//            :vsCnt,
//            :vsAmt,
//            :vcCnt,
//            :vcAmt,
//            :vdAmt,
//  
//            :msCnt,
//            :msAmt,
//            :mcCnt,
//            :mcAmt,
//            :mdAmt,
//  
//            :dinr.sCnt,
//            :dinr.sAmt,
//            :dinr.cCnt,
//            :dinr.cAmt,
//            :dinr.dAmt,
//  
//            :disc.sCnt,
//            :disc.sAmt,
//            :disc.cCnt,
//            :disc.cAmt,
//            :disc.dAmt,
//  
//            :amex.sCnt,
//            :amex.sAmt,
//            :amex.cCnt,
//            :amex.cAmt,
//            :amex.dAmt,
//  
//            :jcb.sCnt,
//            :jcb.sAmt,
//            :jcb.cCnt,
//            :jcb.cAmt,
//            :jcb.dAmt,
//  
//            :debit.sCnt,
//            :debit.sAmt,
//            :debit.cCnt,
//            :debit.cAmt,
//            :debit.dAmt,
//            
//            :p1.sCnt,
//            :p1.sAmt,
//            :p1.cCnt,
//            :p1.cAmt,
//            :p1.dAmt,
//  
//            :p2.sCnt,
//            :p2.sAmt,
//            :p2.cCnt,
//            :p2.cAmt,
//            :p2.dAmt,
//  
//            :p3.sCnt,
//            :p3.sAmt,
//            :p3.cCnt,
//            :p3.cAmt,
//            :p3.dAmt,
//  
//            empty_blob()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_statements\n        (\n          merch_num,\n          rec_num,\n          mystery_id,\n          year_month,\n          dda,\n          transit_routing,\n          load_file_name,\n          bank_num,\n          discount,\n          fees,\n\n          visa_sales_cnt,\n          visa_sales_amt,\n          visa_credit_cnt,\n          visa_credit_amt,\n          visa_disc_amt,\n\n          mc_sales_cnt,\n          mc_sales_amt,\n          mc_credit_cnt,\n          mc_credit_amt,\n          mc_disc_amt,\n\n          dinr_sales_cnt,\n          dinr_sales_amt,\n          dinr_credit_cnt,\n          dinr_credit_amt,\n          dinr_disc_amt,\n\n          disc_sales_cnt,\n          disc_sales_amt,\n          disc_credit_cnt,\n          disc_credit_amt,\n          disc_disc_amt,\n\n          amex_sales_cnt,\n          amex_sales_amt,\n          amex_credit_cnt,\n          amex_credit_amt,\n          amex_disc_amt,\n\n          jcb_sales_cnt,\n          jcb_sales_amt,\n          jcb_credit_cnt,\n          jcb_credit_amt,\n          jcb_disc_amt,\n\n          debit_sales_cnt,\n          debit_sales_amt,\n          debit_credit_cnt,\n          debit_credit_amt,\n          debit_disc_amt,\n          \n          p1_sales_cnt,\n          p1_sales_amt,\n          p1_credit_cnt,\n          p1_credit_amt,\n          p1_disc_amt,\n\n          p2_sales_cnt,\n          p2_sales_amt,\n          p2_credit_cnt,\n          p2_credit_amt,\n          p2_disc_amt,\n\n          p3_sales_cnt,\n          p3_sales_amt,\n          p3_credit_cnt,\n          p3_credit_amt,\n          p3_disc_amt,\n\n          statement_data\n        )\n        values\n        (\n          replace( :1 , '-'),\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n\n           :21 ,\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 ,\n           :30 ,\n\n           :31 ,\n           :32 ,\n           :33 ,\n           :34 ,\n           :35 ,\n\n           :36 ,\n           :37 ,\n           :38 ,\n           :39 ,\n           :40 ,\n\n           :41 ,\n           :42 ,\n           :43 ,\n           :44 ,\n           :45 ,\n          \n           :46 ,\n           :47 ,\n           :48 ,\n           :49 ,\n           :50 ,\n\n           :51 ,\n           :52 ,\n           :53 ,\n           :54 ,\n           :55 ,\n\n           :56 ,\n           :57 ,\n           :58 ,\n           :59 ,\n           :60 ,\n\n          empty_blob()\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.reports.XMLStatementFile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setLong(2,recNum);
   __sJT_st.setLong(3,mystId);
   __sJT_st.setLong(4,yearMonth);
   __sJT_st.setString(5,dda);
   __sJT_st.setString(6,trNum);
   __sJT_st.setString(7,loadFilename);
   __sJT_st.setInt(8,bankNum);
   __sJT_st.setDouble(9,discount);
   __sJT_st.setDouble(10,fees);
   __sJT_st.setInt(11,vsCnt);
   __sJT_st.setDouble(12,vsAmt);
   __sJT_st.setInt(13,vcCnt);
   __sJT_st.setDouble(14,vcAmt);
   __sJT_st.setDouble(15,vdAmt);
   __sJT_st.setInt(16,msCnt);
   __sJT_st.setDouble(17,msAmt);
   __sJT_st.setInt(18,mcCnt);
   __sJT_st.setDouble(19,mcAmt);
   __sJT_st.setDouble(20,mdAmt);
   __sJT_st.setInt(21,dinr.sCnt);
   __sJT_st.setDouble(22,dinr.sAmt);
   __sJT_st.setInt(23,dinr.cCnt);
   __sJT_st.setDouble(24,dinr.cAmt);
   __sJT_st.setDouble(25,dinr.dAmt);
   __sJT_st.setInt(26,disc.sCnt);
   __sJT_st.setDouble(27,disc.sAmt);
   __sJT_st.setInt(28,disc.cCnt);
   __sJT_st.setDouble(29,disc.cAmt);
   __sJT_st.setDouble(30,disc.dAmt);
   __sJT_st.setInt(31,amex.sCnt);
   __sJT_st.setDouble(32,amex.sAmt);
   __sJT_st.setInt(33,amex.cCnt);
   __sJT_st.setDouble(34,amex.cAmt);
   __sJT_st.setDouble(35,amex.dAmt);
   __sJT_st.setInt(36,jcb.sCnt);
   __sJT_st.setDouble(37,jcb.sAmt);
   __sJT_st.setInt(38,jcb.cCnt);
   __sJT_st.setDouble(39,jcb.cAmt);
   __sJT_st.setDouble(40,jcb.dAmt);
   __sJT_st.setInt(41,debit.sCnt);
   __sJT_st.setDouble(42,debit.sAmt);
   __sJT_st.setInt(43,debit.cCnt);
   __sJT_st.setDouble(44,debit.cAmt);
   __sJT_st.setDouble(45,debit.dAmt);
   __sJT_st.setInt(46,p1.sCnt);
   __sJT_st.setDouble(47,p1.sAmt);
   __sJT_st.setInt(48,p1.cCnt);
   __sJT_st.setDouble(49,p1.cAmt);
   __sJT_st.setDouble(50,p1.dAmt);
   __sJT_st.setInt(51,p2.sCnt);
   __sJT_st.setDouble(52,p2.sAmt);
   __sJT_st.setInt(53,p2.cCnt);
   __sJT_st.setDouble(54,p2.cAmt);
   __sJT_st.setDouble(55,p2.dAmt);
   __sJT_st.setInt(56,p3.sCnt);
   __sJT_st.setDouble(57,p3.sAmt);
   __sJT_st.setInt(58,p3.cCnt);
   __sJT_st.setDouble(59,p3.cAmt);
   __sJT_st.setDouble(60,p3.dAmt);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:349^7*/

      // get a stream handler to the blob
      BLOB b;
      /*@lineinfo:generated-code*//*@lineinfo:353^7*/

//  ************************************************************
//  #sql [Ctx] { select  statement_data 
//          from    merch_statements
//          where   merch_num  = replace(:merchNum, '-') and
//                  year_month = :yearMonth and
//                  rec_num    = :recNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  statement_data  \n        from    merch_statements\n        where   merch_num  = replace( :1 , '-') and\n                year_month =  :2  and\n                rec_num    =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.XMLStatementFile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setLong(2,yearMonth);
   __sJT_st.setLong(3,recNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:360^7*/

      OutputStream bOut = b.getBinaryOutputStream();

      //write the element into the recorder for the BLOB
      //will come out ready for XPath navigation
      SAXEventRecorder recorder = new SAXEventRecorder();
      SAXWriter saxWriter = new SAXWriter(recorder, recorder);
      saxWriter.write(snippet);

      //well, first compress the data for storage gains.
      byte[] compressed = compressData(recorder);

      //then write the statement data to the blob
      bOut.write(compressed,0,compressed.length);
      bOut.flush();
      bOut.close();

      isOK = true;
      
      if(++commitCount == 100)
      {
        commitCount = 0;
        commit();
      }
    }
    catch (Exception e)
    {

      logEntry("submitData",e.toString() +" " + loadFilename + ": " +
        merchNum + ", " + recNum);

      try
      {
        rollback();
      }
      catch (Exception e1)
      {
        logEntry("submitData::rollback",e1.toString());
      }
    }
    finally
    {
      cleanUp();
    }

    return isOK;
  }

  public void setElement(Element e, Properties appProps)
    throws Exception
  {
    log.debug("setting Element...");
    if(e == null || appProps == null)
    {
      throw new Exception("Invalid Element or Properties file.");
    }
    else
    {
      reset();

      snippet = e;

      setFields(appProps);
    }
  }

  private void reset()
  {
    merchNum = "";
    yearMonth = 0L;
    mystId = 0L;
    recNum = 0L;
    dda = "";
    trNum = "";
    bankNum = 0;
    discount = 0.0d;
    fees = 0.0d;

    loadMap();
  }


  /* setFields
  ** uses Xpath to parse thru the Element and
  ** set the needed data, save for LoadFileName
  ** which has already been set
  */
  private void setFields(Properties appProperties)
    throws Exception
  {
    log.debug("setting Fields...");
    setMerchNum(getXMLValue(appProperties.getProperty("merch_xpath")));
    setYearMonth(getXMLValue(appProperties.getProperty("date_xpath")));
    setDda(getXMLValue(appProperties.getProperty("dda_xpath")));
    setTrNum(getXMLValue(appProperties.getProperty("transit_xpath")));
    setBankNum(getXMLValue(appProperties.getProperty("bank_xpath")));
    setDiscount(getXMLValue(appProperties.getProperty("total_discount_xpath")));
    if(discount == 0.0)
    {
      // try getting minimum discount value
      setMinDiscount(getXMLValue(appProperties.getProperty("min_discount_xpath")));
    }
    setFees(getXMLValue(appProperties.getProperty("fees_xpath")));
    
    setElementList(appProperties);
  }

  /* setElementList
  ** uses Xpath to parse thru the list of Plans and
  ** set the needed data into the hashmap based on
  ** plantype - if not found all amounts will remain 0
  */
  private void setElementList(Properties appProperties)
    throws Exception
  {
    //walk through nodes to extract the various card info
    log.debug("setting List info...");

    List list = snippet.selectNodes(appProperties.getProperty("plans_xpath"));

    Element child;
    String planCode;
    Plan p;
    
    for(int i = 0; i < list.size(); i++)
    {
      child = (Element)list.get(i);
      
      planCode = child.valueOf(appProperties.getProperty("planCode_xpath")).trim();
      
      log.debug("Getting details for plan: " + planCode);
      
      p = (Plan)plans.get(planCode);

      if(p != null)  // don't throw a NPE just because there's a plan we weren't expecting
      {
        p.sCnt = parseInt(child.valueOf(appProperties.getProperty("nSales_xpath")));
        p.sAmt = parseDouble(child.valueOf(appProperties.getProperty("dSales_xpath")));
        p.cCnt = parseInt(child.valueOf(appProperties.getProperty("nCredits_xpath")));
        p.cAmt = parseDouble(child.valueOf(appProperties.getProperty("dCredits_xpath")));
        p.dAmt = parseDouble(child.valueOf(appProperties.getProperty("discount_xpath")));

        log.debug(p.toString());
      }
      else
      {
        log.debug("Plan was NULL");
      }
    }
  }

  private String getXMLValue(String xpath)
  {
    //log.debug("xpath = "+xpath);
    try
    {
      return snippet.valueOf(xpath).trim();
    }
    catch (Exception e)
    {
      //e.printStackTrace();
      return "";
    }
  }

  private void setYearMonth(String input)
  {
    log.debug("setYearMonth="+input);
    super.setYearMonth(dateFormat(input));
  }

  private void setBankNum(String input)
  {
    log.debug("setBankNum="+input);
    try
    {
      super.setBankNum(Integer.parseInt(input));
    }
    catch (Exception e)
    {
      super.setBankNum(0);
    }
  }

  private void setMinDiscount(String input)
  {
    log.debug("setMinDiscount="+input);
    try
    {
      super.setDiscount(parseDouble(input));
    }
    catch(Exception e)
    {
      super.setDiscount(0);
    }
  }
  
  private void setDiscount(String input)
  {
    log.debug("setDiscount="+input);
    try
    {
      super.setDiscount(parseDouble(input));
    }
    catch (Exception e)
    {
      super.setDiscount(0);
    }
  }

  private void setFees(String input)
  {
    log.debug("setFees="+input);
    try
    {
      super.setFees(parseDouble(input));
    }
    catch (Exception e)
    {
      super.setFees(0);
    }
  }

  private long dateFormat(String input)
  {
    //String is in mm-yy String format, need to change to
    //yyyymm long
    String[] items = input.split("-");
    String month,year;
    long dateVal = 0L;

    if(items.length != 2)
    {
      //TODO - some type of exception here?
      dateVal = 200608;
    }
    else
    {
      month = items[0].trim();
      year = items[1].trim();
      log.debug("month = "+month);
      log.debug("year = "+year);
      try
      {
        dateVal = (Long.parseLong(year)+2000)*100 + Long.parseLong(month);
      }
      catch (Exception e)
      {
        e.printStackTrace();
        //TODO - some type of exception here?
      }
    }

    return dateVal;
  }

  /*
  ** private void compressData()
  **
  ** Given an Object, this routine first serializes it and then compresses
  ** the data into an internal storage buffer.
  */
  private byte[] compressData(Object obj)
  {

    byte[] data = null;

    try
    {
      // serialize the statement data
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(obj);

      data = baos.toByteArray();

      // compress the serialized statement data
      baos.reset();
      DeflaterOutputStream dos = new DeflaterOutputStream(baos);
      dos.write(data,0,data.length);
      dos.finish();

      data = baos.toByteArray();

    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() +
        "::compressData " + e.toString());
      e.printStackTrace();
    }

    return data;
  }

  /*
  ** private Object decompressData(InputStream is)
  **
  ** Inflates and deserializes the inputstream
  **
  ** RETURNS: the decompressed Object
  */
  private Object decompressData(InputStream is)
  {
    Object decompressed = null;

    try
    {
        InflaterInputStream iis = new InflaterInputStream(is);
        ObjectInputStream ois = new ObjectInputStream(iis);
        decompressed = ois.readObject();
    }
    catch (Exception e)
    {
        log.debug(this.getClass().getName() + "::decompressData " + e.toString());
        //e.printStackTrace();
    }

    return decompressed;

  }

  public class Plan
  {
    public int     sCnt = 0;
    public double  sAmt = 0.0d;
    public int     cCnt = 0;
    public double  cAmt = 0.0d;
    public double  dAmt = 0.0d;

    public String toString()
    {
      StringBuffer sb = new StringBuffer("\n");
      sb.append("sCnt = ").append(sCnt).append("\n");
      sb.append("sAmt = ").append(sAmt).append("\n");
      sb.append("cCnt = ").append(cCnt).append("\n");
      sb.append("cAmt = ").append(cAmt).append("\n");
      sb.append("dAmt = ").append(dAmt).append("\n");
      return sb.toString();
    }

    public void reset()
    {
      sCnt = 0;
      sAmt = 0.0d;
      cCnt = 0;
      cAmt = 0.0d;
      dAmt = 0.0d;
    }
  }

  /*
  ** Both of the following methods are for future dev if needed
  ** this "treeWalk" is faster than creating a bunch of nested Iterators
  */
  public void treeWalk(Document document)
  {
    treeWalk(document.getRootElement());
  }

  public void treeWalk(Element element)
  {
    for ( int i = 0, size = element.nodeCount(); i < size; i++ )
    {
      Node node = element.node(i);

      if ( node instanceof Element )
      {
        treeWalk( (Element) node );
      }
      else
      {
        //do something;
      }
    }
  }
}/*@lineinfo:generated-code*/