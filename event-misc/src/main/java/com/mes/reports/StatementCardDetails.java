/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/StatementCardDetails.java $

  Description:
  
    StatementCardDetails
  
    Stores statement line details about activity for a card type.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/19/02 1:55p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.reports;

public class StatementCardDetails
{
  // card types
  public static final int   CT_NONE       = 0;
  public static final int   CT_VISA       = 1;
  public static final int   CT_MC         = 2;
  public static final int   CT_DINERS     = 3;
  public static final int   CT_DISCOVER   = 4;
  public static final int   CT_AMEX       = 5;
  public static final int   CT_JCB        = 6;
  public static final int   CT_DEBIT      = 7;
  
  // the details
  private int               cardType;
  private int               salesCnt;
  private double            salesAmt;
  private int               creditCnt;
  private double            creditAmt;
  private double            discAmt;
  
  public StatementCardDetails(int cardType)
  {
    this.cardType = cardType;
  }
  
  public int getCardType()
  {
    return cardType;
  }
  
  public int getSalesCnt()
  {
    return salesCnt;
  }
  public void setSalesCnt(int salesCnt)
  {
    this.salesCnt = salesCnt;
  }
  
  public double getSalesAmt()
  {
    return salesAmt;
  }
  public void setSalesAmt(double salesAmt)
  {
    this.salesAmt = salesAmt;
  }
  
  public int getCreditCnt()
  {
    return creditCnt;
  }
  public void setCreditCnt(int creditCnt)
  {
    this.creditCnt = creditCnt;
  }
  
  public double getCreditAmt()
  {
    return creditAmt;
  }
  public void setCreditAmt(double creditAmt)
  {
    this.creditAmt = creditAmt;
  }
  
  public double getDiscAmt()
  {
    return discAmt;
  }
  public void setDiscAmt(double discAmt)
  {
    this.discAmt = discAmt;
  }
  
  public String toString()
  {
    String[] types = 
      { "none", "visa", "mc", "diners", "discover", "amex", "jcb", "debit" };
      
    StringBuffer strBuf = new StringBuffer();
    strBuf.append("card type: " + types[cardType]);
    strBuf.append(" sales count: " + salesCnt);
    strBuf.append(" sales amount: " + salesAmt);
    strBuf.append(" credit count: " + creditCnt);
    strBuf.append(" credit amount: " + creditAmt);
    strBuf.append(" disc amount: " + discAmt);
    
    return strBuf.toString();
  }
}