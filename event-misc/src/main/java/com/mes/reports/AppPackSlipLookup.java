/*@lineinfo:filename=AppPackSlipLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/AppPackSlipLookup.sqlj $

  Description:

    AppPackSlipLookup

    Support bean for account packing slip lookup page in reporting

  Last Modified By   : $Author:$
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.MesHierarchy;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesCalendar;
import com.mes.tools.DateSQLJBean;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class AppPackSlipLookup extends DateSQLJBean
{
  private String            lookupValue         = "";
  private String            lastLookupValue     = "";

  private int               action              = mesConstants.LU_ACTION_INVALID;
  private String            actionDescription   = "Invalid Action";

  private AppDataComparator adc                 = new AppDataComparator();
  private Vector            lookupResults       = new Vector();
  private TreeSet           sortedResults       = null;

  private boolean           submitted           = false;
  
  private long              appNode             = 0L;
  public  boolean           needBankcard        = false;

  private int               appType             = -1;
  private int               lastAppType         = -1;
  private int               appStatus           = -1;
  private int               lastAppStatus       = -1;
  private boolean           lastBankcard        = false;

  private int               lastFromMonth       = -1;
  private int               lastFromDay         = -1;
  private int               lastFromYear        = -1;
  private int               lastToMonth         = -1;
  private int               lastToDay           = -1;
  private int               lastToYear          = -1;

  private StringBuffer      errorMessage        = new StringBuffer("");

  public  Vector            appStatuses         = new Vector();
  public  Vector            appStatusValues     = new Vector();
  public  Vector            appTypes            = new Vector();
  public  HashSet           appTypeSet          = new HashSet();
  public  Vector            appTypeValues       = new Vector();

  /*
  ** METHOD fillDropDowns
  **
  ** Fills the "App Status" and "App Type" drop down boxes
  */
  public void fillDropDowns(UserBean user)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();
      
      long  userNode    = user.getHierarchyNode();
      int   extendedId  = 0;
      
      if(appStatuses.size() == 0)
      {
        appStatuses.clear();
        appStatusValues.clear();
        appTypes.clear();
        appTypeValues.clear();
        
        // get extended client status id
        /*@lineinfo:generated-code*//*@lineinfo:111^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mcst.id       id
//            from    merch_credit_status_types mcst,
//                    t_hierarchy th
//            where   th.ancestor = mcst.hierarchy_node and
//                    th.descendent = :userNode
//            order by th.relation asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mcst.id       id\n          from    merch_credit_status_types mcst,\n                  t_hierarchy th\n          where   th.ancestor = mcst.hierarchy_node and\n                  th.descendent =  :1 \n          order by th.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AppPackSlipLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AppPackSlipLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:119^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          // get extended id
          extendedId = rs.getInt("id");
        }
        
        rs.close();
        it.close();

        // app statuses
        /*@lineinfo:generated-code*//*@lineinfo:133^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_credit_status status,
//                    status_desc         description,
//                    item_order          item_order
//            from    merch_credit_statuses
//            where   client_status_id is null or
//                    client_status_id = :extendedId
//            order by item_order asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_credit_status status,\n                  status_desc         description,\n                  item_order          item_order\n          from    merch_credit_statuses\n          where   client_status_id is null or\n                  client_status_id =  :1 \n          order by item_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AppPackSlipLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,extendedId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AppPackSlipLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:142^9*/

        rs = it.getResultSet();
        while(rs.next())
        {
          appStatuses.add(rs.getString("description"));
          appStatusValues.add(rs.getString("status"));
        }

        rs.close();
        it.close();

        // app types
        appTypes.add("All App Types");
        appTypeValues.add("-1");
        
         //CHANGE 1 - CBT accounts only
        /*@lineinfo:generated-code*//*@lineinfo:159^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct  app_type  app_type,
//                    app_name            description
//            from    org_app
//            where   
//            app_type in (1,31)
//            order by app_name asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct  app_type  app_type,\n                  app_name            description\n          from    org_app\n          where   \n          app_type in (1,31)\n          order by app_name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.AppPackSlipLookup",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.AppPackSlipLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^9*/
        
        rs = it.getResultSet();

        while(rs.next())
        {
          appTypeSet.add(rs.getString("app_type"));
          appTypes.add(rs.getString("description"));
          appTypeValues.add(rs.getString("app_type"));
        }

        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }

  public String getErrorMessage()
  {
    return errorMessage.toString();
  }

  /*
  ** METHOD validateLookupData
  **
  ** Checks for lookup parameters that would require too much database power
  */
  public boolean validateLookupData(long userNode)
  {
    int   daysAllowed     = 0;
    int   daysActual      = 0;

    try
    {
      errorMessage.setLength(0);

      daysActual = (int)(MesCalendar.daysBetween(getFromDate(), getToDate()));

      if(lookupValue != null && lookupValue.length() > 4)
      {
        // allow a date range of up to 1 year (a little more for ease of use)
        daysAllowed = 370;
      }
      else
      {
        if(appType != -1)
        {
          // app type set to a value allows 1 month
          daysAllowed = 31;
        }
        else if(appStatus != -1)
        {
          // app status set to a value allows 1 week
          daysAllowed = 7;
        }
        else
        {
          // everything blank only allows 2 days
          daysAllowed = 2;
        }
      }

      // check to see if the date range is more than the allowable days

      if(daysActual > daysAllowed)
      {
        errorMessage.append("The date range is too large for this query. Based ");
        errorMessage.append("on the criteria you have chosen, the system will allow ");
        errorMessage.append("a date range of <strong>");
        errorMessage.append(daysAllowed);
        errorMessage.append("</strong> days, but the date range is set for <strong>");
        errorMessage.append(daysActual);
        errorMessage.append("</strong> days.  Please adjust the date range accordingly.");
      }
    }
    catch(Exception e)
    {
      logEntry("validateLookupData()", e.toString());
    }

    return(errorMessage.length() == 0);
  }

  /*
  ** METHOD public void getData()
  **
  */
  public void getData(long userNode)
  {
    ResultSetIterator it    = null;
    ResultSet         rs    = null;
    try
    {
      connect();

      // set up longLookup value (numeric version of lookup value)
      long longLookup;
      try
      {
        longLookup = Long.parseLong(lookupValue.trim());
      }
      catch (Exception e)
      {
        longLookup = -2;
      }

      // add wildcards to stringLookup so we can match partial strings
      String stringLookup = "";
      if(lookupValue.trim().equals(""))
      {
        stringLookup = "passall";
      }
      else
      {
        stringLookup = "%" + lookupValue.toUpperCase() + "%";
      }

      Date fromDate = getSqlFromDate();
      Date toDate   = getSqlToDate();

      /*@lineinfo:generated-code*//*@lineinfo:310^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ RULE */
//                  ass.date_created                    app_date,
//                  m.app_seq_num                       app_seq_num,
//                  ass.app_type                        app_type,
//                  m.merc_cntrl_number                 control_num,
//                  m.merch_business_name               dba_name,
//                  m.merch_number                      merchant_number,
//                  m.merch_credit_status               merch_credit_status,
//                  ass.user_name                       user_name,
//                  ass.user_node                       hierarchy_node,
//                  ass.login_name                      user_login,
//                  mcs.status_desc                     status_string,
//                  ass.app_type_desc                   app_name,
//                  nvl(ass.discover_status, '---')     disc_status,
//                  nvl(ass.amex_status, '---')         amex_status,
//                  decode(mf.merchant_number,
//                    null, 'N',
//                    'Y')                              mif_exists,
//                  nvl(ass.vnumber_status, '---')      vnumber_status,
//                  sud.tracking_number                 track_number
//          from    merchant                            m,
//                  mif                                 mf,
//                  merch_credit_statuses               mcs,
//                  app_status_summary                  ass,
//                  ship_upload_detail                  sud
//          where   trunc(ass.date_created) between :fromDate and :toDate and
//                  ass.app_seq_num = m.app_seq_num and
//                  m.merch_credit_status = mcs.merch_credit_status and
//                  m.merch_number          = mf.merchant_number(+) and
//                  (-1 = :appStatus or m.merch_credit_status = :appStatus) and
//                  (
//                    'passall'                     = :stringLookup or
//                    m.merch_number                = :longLookup or
//                    m.merc_cntrl_number           = :longLookup or
//                    mod(ass.user_node, 1000000)   = :longLookup or
//                    ass.user_node                 = :longLookup or
//                    ass.app_seq_num               = :longLookup or
//                    m.asso_number                 = :longLookup or
//                    m.asso_number                 = mod(:longLookup, 1000000) or
//                    upper(m.merch_business_name)  like :stringLookup or
//                    upper(ass.user_name)          like :stringLookup or
//                    upper(ass.login_name)         like :stringLookup or
//                    ass.user_node in
//                    (
//                      select  descendent
//                      from    t_hierarchy
//                      where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                              mod(ancestor, 1000000) = :longLookup
//                    )
//                  ) and
//                  -- take apps entered by reps under the user node
//                  -- or accounts setup under user nodes.  this is
//                  -- to allow support for discover referral apps
//                  -- that were entered by reps outside the discover
//                  -- referral hierarchy.
//                  ( ass.user_node in
//                    (
//                      select  descendent
//                      from    t_hierarchy
//                      where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                              ancestor  = :userNode
//                    ) or
//                    m.merch_number in
//                    (
//                      select  gm.merchant_number
//                      from    organization    o,
//                              group_merchant  gm
//                      where   o.org_group = :userNode and
//                              gm.org_num = o.org_num
//                    )
//                  ) 
//                  and
//                  --look for tracking number of welcome kit only
//                  (
//                    m.merch_number = to_mes_number(sud.merchant_number(+)) and
//                    sud.type_key (+) = 'WK' and
//                    sud.process_id (+) is null
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ RULE */\n                ass.date_created                    app_date,\n                m.app_seq_num                       app_seq_num,\n                ass.app_type                        app_type,\n                m.merc_cntrl_number                 control_num,\n                m.merch_business_name               dba_name,\n                m.merch_number                      merchant_number,\n                m.merch_credit_status               merch_credit_status,\n                ass.user_name                       user_name,\n                ass.user_node                       hierarchy_node,\n                ass.login_name                      user_login,\n                mcs.status_desc                     status_string,\n                ass.app_type_desc                   app_name,\n                nvl(ass.discover_status, '---')     disc_status,\n                nvl(ass.amex_status, '---')         amex_status,\n                decode(mf.merchant_number,\n                  null, 'N',\n                  'Y')                              mif_exists,\n                nvl(ass.vnumber_status, '---')      vnumber_status,\n                sud.tracking_number                 track_number\n        from    merchant                            m,\n                mif                                 mf,\n                merch_credit_statuses               mcs,\n                app_status_summary                  ass,\n                ship_upload_detail                  sud\n        where   trunc(ass.date_created) between  :1  and  :2  and\n                ass.app_seq_num = m.app_seq_num and\n                m.merch_credit_status = mcs.merch_credit_status and\n                m.merch_number          = mf.merchant_number(+) and\n                (-1 =  :3  or m.merch_credit_status =  :4 ) and\n                (\n                  'passall'                     =  :5  or\n                  m.merch_number                =  :6  or\n                  m.merc_cntrl_number           =  :7  or\n                  mod(ass.user_node, 1000000)   =  :8  or\n                  ass.user_node                 =  :9  or\n                  ass.app_seq_num               =  :10  or\n                  m.asso_number                 =  :11  or\n                  m.asso_number                 = mod( :12 , 1000000) or\n                  upper(m.merch_business_name)  like  :13  or\n                  upper(ass.user_name)          like  :14  or\n                  upper(ass.login_name)         like  :15  or\n                  ass.user_node in\n                  (\n                    select  descendent\n                    from    t_hierarchy\n                    where   hier_type =  :16  and\n                            mod(ancestor, 1000000) =  :17 \n                  )\n                ) and\n                -- take apps entered by reps under the user node\n                -- or accounts setup under user nodes.  this is\n                -- to allow support for discover referral apps\n                -- that were entered by reps outside the discover\n                -- referral hierarchy.\n                ( ass.user_node in\n                  (\n                    select  descendent\n                    from    t_hierarchy\n                    where   hier_type =  :18  and\n                            ancestor  =  :19 \n                  ) or\n                  m.merch_number in\n                  (\n                    select  gm.merchant_number\n                    from    organization    o,\n                            group_merchant  gm\n                    where   o.org_group =  :20  and\n                            gm.org_num = o.org_num\n                  )\n                ) \n                and\n                --look for tracking number of welcome kit only\n                (\n                  m.merch_number = to_mes_number(sud.merchant_number(+)) and\n                  sud.type_key (+) = 'WK' and\n                  sud.process_id (+) is null\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.AppPackSlipLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,appStatus);
   __sJT_st.setInt(4,appStatus);
   __sJT_st.setString(5,stringLookup);
   __sJT_st.setLong(6,longLookup);
   __sJT_st.setLong(7,longLookup);
   __sJT_st.setLong(8,longLookup);
   __sJT_st.setLong(9,longLookup);
   __sJT_st.setLong(10,longLookup);
   __sJT_st.setLong(11,longLookup);
   __sJT_st.setLong(12,longLookup);
   __sJT_st.setString(13,stringLookup);
   __sJT_st.setString(14,stringLookup);
   __sJT_st.setString(15,stringLookup);
   __sJT_st.setInt(16,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(17,longLookup);
   __sJT_st.setInt(18,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(19,userNode);
   __sJT_st.setLong(20,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.AppPackSlipLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:390^7*/

      lookupResults.clear();

      rs = it.getResultSet();

      int i = 0;
      while(rs.next())
      {
        String discString   = rs.getString("disc_status");
        String amexString   = rs.getString("amex_status");

        if(!needBankcard || (needBankcard && (discString.equals("D?") || amexString.equals("A?"))))
        {
          if( appType == rs.getInt("app_type") ||
              (appType == -1 && appTypeSet.contains(rs.getString("app_type")) )
            )
          {
            lookupResults.add(new AppData( rs, discString, amexString) );
          }            
        }
      }

      rs.close();
      it.close();

      lastAppType     = appType;
      lastAppStatus   = appStatus;
      lastFromMonth   = fromMonth;
      lastFromDay     = fromDay;
      lastFromYear    = fromYear;
      lastToMonth     = toMonth;
      lastToDay       = toDay;
      lastToYear      = toYear;
      lastLookupValue = lookupValue;
      lastBankcard    = needBankcard;

      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }

      sortedResults = null;

      sortedResults = new TreeSet(adc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      System.out.println("in getData");
      e.printStackTrace();
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public AppData loadAppData( long userNode, long controlNumber )
  {
    ResultSetIterator         it      = null;
    AppData                   retVal  = null;
    ResultSet                 rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:462^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ RULE */
//                  ass.date_created                    app_date,
//                  m.app_seq_num                       app_seq_num,
//                  ass.app_type                        app_type,
//                  m.merc_cntrl_number                 control_num,
//                  m.merch_business_name               dba_name,
//                  m.merch_number                      merchant_number,
//                  m.merch_credit_status               merch_credit_status,
//                  ass.user_name                       user_name,
//                  ass.user_node                       hierarchy_node,
//                  ass.login_name                      user_login,
//                  mcs.status_desc                     status_string,
//                  ass.app_type_desc                   app_name,
//                  ass.discover_status                 disc_status,
//                  ass.amex_status                     amex_status,
//                  decode(mf.merchant_number,
//                    null, 'N',
//                    'Y')                              mif_exists,
//                  nvl(ass.vnumber_status, '---')      vnumber_status,
//                  sud.tracking_number                 track_number
//          from    merchant                            m,
//                  mif                                 mf,
//                  merch_credit_statuses               mcs,
//                  app_status_summary                  ass,
//                  ship_upload_detail                  sud
//          where   m.merc_cntrl_number   = :controlNumber and
//                  ass.app_seq_num = m.app_seq_num and
//                  mcs.merch_credit_status = m.merch_credit_status and
//                  mf.merchant_number(+) = m.merch_number and
//                  -- take apps entered by reps under the user node
//                  -- or accounts setup under user nodes.  this is
//                  -- to allow support for discover referral apps
//                  -- that were entered by reps outside the discover
//                  -- referral hierarchy.
//                  ( ass.user_node in
//                    (
//                      select  descendent
//                      from    t_hierarchy
//                      where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                              ancestor  = :userNode
//                    ) or
//                    m.merch_number in
//                    (
//                      select  gm.merchant_number
//                      from    organization    o,
//                              group_merchant  gm
//                      where   o.org_group = :userNode and
//                              gm.org_num = o.org_num
//                    )
//                  ) and
//                  --look for tracking number of welcome kit only
//                  (
//                    m.merch_number = sud.merchant_number(+) and
//                    sud.type_key (+) = 'WK' and
//                    sud.process_id (+) is null
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ RULE */\n                ass.date_created                    app_date,\n                m.app_seq_num                       app_seq_num,\n                ass.app_type                        app_type,\n                m.merc_cntrl_number                 control_num,\n                m.merch_business_name               dba_name,\n                m.merch_number                      merchant_number,\n                m.merch_credit_status               merch_credit_status,\n                ass.user_name                       user_name,\n                ass.user_node                       hierarchy_node,\n                ass.login_name                      user_login,\n                mcs.status_desc                     status_string,\n                ass.app_type_desc                   app_name,\n                ass.discover_status                 disc_status,\n                ass.amex_status                     amex_status,\n                decode(mf.merchant_number,\n                  null, 'N',\n                  'Y')                              mif_exists,\n                nvl(ass.vnumber_status, '---')      vnumber_status,\n                sud.tracking_number                 track_number\n        from    merchant                            m,\n                mif                                 mf,\n                merch_credit_statuses               mcs,\n                app_status_summary                  ass,\n                ship_upload_detail                  sud\n        where   m.merc_cntrl_number   =  :1  and\n                ass.app_seq_num = m.app_seq_num and\n                mcs.merch_credit_status = m.merch_credit_status and\n                mf.merchant_number(+) = m.merch_number and\n                -- take apps entered by reps under the user node\n                -- or accounts setup under user nodes.  this is\n                -- to allow support for discover referral apps\n                -- that were entered by reps outside the discover\n                -- referral hierarchy.\n                ( ass.user_node in\n                  (\n                    select  descendent\n                    from    t_hierarchy\n                    where   hier_type =  :2  and\n                            ancestor  =  :3 \n                  ) or\n                  m.merch_number in\n                  (\n                    select  gm.merchant_number\n                    from    organization    o,\n                            group_merchant  gm\n                    where   o.org_group =  :4  and\n                            gm.org_num = o.org_num\n                  )\n                ) and\n                --look for tracking number of welcome kit only\n                (\n                  m.merch_number = sud.merchant_number(+) and\n                  sud.type_key (+) = 'WK' and\n                  sud.process_id (+) is null\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.AppPackSlipLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,controlNumber);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(3,userNode);
   __sJT_st.setLong(4,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.AppPackSlipLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:520^7*/
      rs = it.getResultSet();

      if(rs.next())
      {
        retVal = new AppData( rs, null, null );
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      System.out.println("in LoadAppData");
      e.printStackTrace();
      logEntry("loadAppData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return( retVal );
  }

  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }

    return result;
  }

  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public String getActionDescription()
  {
    return this.actionDescription;
  }

  /*
  ** ACCESSORS
  */
  public String getLookupValue()
  {
    return lookupValue;
  }
  public void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }

  public int getAction()
  {
    return this.action;
  }
  public void setAction(int action)
  {
    this.actionDescription = "Application Status";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }

    setAction(actionNum);
  }

  public void setSortBy(String sortBy)
  {
    try
    {
      adc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }

  public void setAppType(String appType)
  {
    try
    {
      this.appType = Integer.parseInt(appType);
    }
    catch(Exception e)
    {
    }
  }
  public String getAppType()
  {
    return Integer.toString(this.appType);
  }
  public void setAppStatus(String appStatus)
  {
    try
    {
      this.appStatus = Integer.parseInt(appStatus);
    }
    catch(Exception e)
    {
    }
  }
  public String getAppStatus()
  {
    return Integer.toString(this.appStatus);
  }
  public void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
  public boolean isSubmitted()
  {
    return this.submitted;
  }

  public void setNeedBankcard(String needBankcard)
  {
    if(needBankcard != null)
    {
      if(needBankcard.equals("Y"))
      {
        this.needBankcard = true;
      }
      else
      {
        this.needBankcard = false;
      }
    }
  }

  public void updateAppSource( long controlNum, String newUserName )
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:676^7*/

//  ************************************************************
//  #sql [Ctx] { call change_application_source( :controlNum, :newUserName )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN change_application_source(  :1 ,  :2  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.AppPackSlipLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,controlNum);
   __sJT_st.setString(2,newUserName);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:679^7*/
    }
    catch( java.sql.SQLException e )
    {
      StringBuffer    errorMsg = new StringBuffer();

      errorMsg.append("updateAppSource(");
      errorMsg.append(controlNum);
      errorMsg.append(",");
      errorMsg.append(newUserName);
      errorMsg.append(")");

      logEntry(errorMsg.toString(),e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*********************************************************
  **
  ** SUBCLASS AppData
  **
  **********************************************************/
  public class AppData
  {
    private String      amexStatus        = "";
    private String      appDate           = "";
    private Timestamp   appDateSorted     = null;
    private String      appSeqNum         = "";
    private String      appStatus         = "";
    private int         appStatusCode     = 0;
    private String      appType           = "";
    public  int         AppTypeCode       = 0;
    private String      appUser           = "";
    private String      appUserLogin      = "";
    private String      controlNumber     = "";
    private String      dbaName           = "";
    private String      discStatus        = "";
    private String      merchantNumber    = "";
    private String      vnumber           = "---";
    private boolean     mifExists         = false;
    private String      trackNumber       = "";

    public AppData()
    {
    }

    public AppData( ResultSet rs, String discStatus, String amexStatus)
      throws java.sql.SQLException
    {
      AppTypeCode = rs.getInt("app_type");
      setAppSeqNum( rs.getString("app_seq_num") );
      setMerchantNumber(rs.getString("merchant_number"));
      setAppDate(DateTimeFormatter.getFormattedDate(rs.getTimestamp("app_date"), "MM/dd/yy HH:mm"));
      setAppDateSorted(rs.getTimestamp("app_date"));
      setDbaName(rs.getString("dba_name"));
      setControlNumber(rs.getString("control_num"));
      setAppStatus(rs.getString("status_string"));
      setAppStatusCode(rs.getInt("merch_credit_status"));
      setAppType(rs.getString("app_name"));
      setAppUser(rs.getString("user_name"));
      setAppUserLogin(rs.getString("user_login"));
      setDiscStatus(discStatus);
      setAmexStatus(amexStatus);
      setTrackNumber(rs.getString("track_number"));

      mifExists = rs.getString("mif_exists").equals("Y");
      vnumber = rs.getString("vnumber_status");
    }

    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }

    public boolean mifExists()
    {
      return mifExists;
    }

    public void setAppSeqNum(String appSeqNum)
    {
      this.appSeqNum = processStringField(appSeqNum);
    }
    public String getAppSeqNum()
    {
      return this.appSeqNum;
    }
    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = processStringField(merchantNumber);
    }
    public String getMerchantNumber()
    {
      return this.merchantNumber;
    }
    public void setAppDate(String appDate)
    {
      this.appDate = processStringField(appDate);
    }
    public String getAppDate()
    {
      return this.appDate;
    }
    public void setAppDateSorted(Timestamp appDateSorted)
    {
      this.appDateSorted = appDateSorted;
    }
    public String getAppDateSorted()
    {
      return DateTimeFormatter.getFormattedDate(appDateSorted, "yyyyMMddHHmmss");
    }
    public void setDbaName(String dbaName)
    {
      this.dbaName = processStringField(dbaName);
    }
    public String getDbaName()
    {
      return this.dbaName;
    }
    public String getVnumber()
    {
      return this.vnumber;
    }
    public void setControlNumber(String controlNumber)
    {
      this.controlNumber = processStringField(controlNumber);
    }
    public String getControlNumber()
    {
      return this.controlNumber;
    }
    public void setAppStatusCode(String appStatusCode)
    {
      try
      {
        setAppStatusCode(Integer.parseInt(appStatusCode));
      }
      catch(Exception e)
      {
      }
    }
    public void setAppStatusCode(int appStatusCode)
    {
      this.appStatusCode = appStatusCode;
    }
    public int getAppStatusCode()
    {
      return appStatusCode;
    }
    public void setAppStatus(String appStatus)
    {
      this.appStatus = processStringField(appStatus);
    }
    public String getAppStatus()
    {
      return this.appStatus;
    }
    public void setAppType(String appType)
    {
      this.appType = processStringField(appType);
    }
    public String getAppType()
    {
      return this.appType;
    }
    public void setAppUser(String appUser)
    {
      this.appUser = processStringField(appUser);
    }
    public String getAppUser()
    {
      return this.appUser;
    }
    public void setAppUserLogin(String appUserLogin)
    {
      this.appUserLogin = processStringField(appUserLogin);
    }
    public String getAppUserLogin()
    {
      return this.appUserLogin;
    }
    public void setDiscStatus(String discStatus)
    {
      this.discStatus = processStringField(discStatus);
    }
    public String getDiscStatus()
    {
      return this.discStatus;
    }
    public void setAmexStatus(String amexStatus)
    {
      this.amexStatus = processStringField(amexStatus);
    }
    public String getAmexStatus()
    {
      return this.amexStatus;
    }
    public String getTrackNumber()
    {
      return trackNumber;
    }
    public void setTrackNumber(String tNum)
    {
      if (tNum == null)
      {
        trackNumber = "";
      }
      else
      {
        trackNumber = tNum;
      }
    }
    
  }

  public class AppDataComparator
    implements Comparator
  {
    public final static int   SB_MERCHANT_NUMBER  = 0;
    public final static int   SB_APP_DATE         = 1;
    public final static int   SB_DBA_NAME         = 2;
    public final static int   SB_VNUMBER          = 3;
    public final static int   SB_CONTROL_NUMBER   = 4;
    public final static int   SB_APP_STATUS       = 5;
    public final static int   SB_APP_TYPE         = 6;
    public final static int   SB_APP_USER         = 7;
    public final static int   SB_APP_SEQ_NUM      = 8;
    public final static int   SB_DISC_NUM         = 9;
    public final static int   SB_AMEX_NUM         = 10;

    private int               sortBy;

    private boolean           sortAscending       = false;

    public AppDataComparator()
    {
      this.sortBy = SB_APP_DATE;
    }

    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }

    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_MERCHANT_NUMBER && sortBy <= SB_AMEX_NUM)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }

        this.sortBy = sortBy;
      }
    }

    int compare(AppData o1, AppData o2)
    {
      int result    = 0;

      String compareString1 = "";
      String compareString2 = "";

      try
      {
        switch(sortBy)
        {
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber() + o1.getAppDateSorted();
            compareString2 = o2.getMerchantNumber() + o2.getAppDateSorted();
            break;

          case SB_APP_DATE:
            compareString1 = o1.getAppDateSorted() + o1.getDbaName();
            compareString2 = o2.getAppDateSorted() + o2.getDbaName();
            break;

          case SB_DBA_NAME:
            compareString1 = o1.getDbaName() + o1.getAppDateSorted();
            compareString2 = o2.getDbaName() + o2.getAppDateSorted();
            break;

          case SB_VNUMBER:
            compareString1 = o1.getVnumber() + o1.getAppDateSorted();
            compareString2 = o2.getVnumber() + o2.getAppDateSorted();
            break;

          case SB_CONTROL_NUMBER:
            compareString1 = o1.getControlNumber();
            compareString2 = o2.getControlNumber();
            break;

          case SB_APP_STATUS:
            compareString1 = o1.getAppStatus() + o1.getAppDateSorted();
            compareString2 = o2.getAppStatus() + o2.getAppDateSorted();
            break;

          case SB_APP_TYPE:
            compareString1 = o1.getAppType() + o1.getAppDateSorted();
            compareString2 = o2.getAppType() + o2.getAppDateSorted();
            break;

          case SB_APP_USER:
            compareString1 = o1.getAppUser() + o1.getAppDateSorted();
            compareString2 = o2.getAppUser() + o2.getAppDateSorted();
            break;

          case SB_APP_SEQ_NUM:
            compareString1 = o1.getAppSeqNum();
            compareString2 = o2.getAppSeqNum();

          case SB_DISC_NUM:
            compareString1 = o1.getDiscStatus() + o1.getAppDateSorted();
            compareString2 = o2.getDiscStatus() + o2.getAppDateSorted();
            break;

          case SB_AMEX_NUM:
            compareString1 = o1.getAmexStatus() + o1.getAppDateSorted();
            compareString2 = o2.getAmexStatus() + o2.getAppDateSorted();
            break;

          default:
            break;
        }

        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }

      return result;
    }

    boolean equals(AppData o1, AppData o2)
    {
      boolean result    = false;

      String compareString1 = "";
      String compareString2 = "";

      try
      {
        switch(sortBy)
        {
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber() + o1.getAppDate();
            compareString2 = o2.getMerchantNumber() + o2.getAppDate();
            break;

          case SB_APP_DATE:
            compareString1 = o1.getAppDate() + o1.getDbaName();
            compareString2 = o2.getAppDate() + o2.getDbaName();
            break;

          case SB_DBA_NAME:
            compareString1 = o1.getDbaName() + o1.getAppDate();
            compareString2 = o2.getDbaName() + o2.getAppDate();
            break;

          case SB_VNUMBER:
            compareString1 = o1.getVnumber() + o1.getAppDate();
            compareString2 = o2.getVnumber() + o2.getAppDate();
            break;

          case SB_CONTROL_NUMBER:
            compareString1 = o1.getControlNumber() + o1.getAppDate();
            compareString2 = o2.getControlNumber() + o2.getAppDate();
            break;

          case SB_APP_STATUS:
            compareString1 = o1.getAppStatus() + o1.getAppDate();
            compareString2 = o2.getAppStatus() + o2.getAppDate();
            break;

          case SB_APP_TYPE:
            compareString1 = o1.getAppType() + o1.getAppDate();
            compareString2 = o2.getAppType() + o2.getAppDate();
            break;

          case SB_APP_USER:
            compareString1 = o1.getAppUser() + o1.getAppDate();
            compareString2 = o2.getAppUser() + o2.getAppDate();
            break;

          case SB_DISC_NUM:
            compareString1 = o1.getDiscStatus() + o1.getAppDateSorted();
            compareString2 = o2.getDiscStatus() + o2.getAppDateSorted();
            break;

          case SB_AMEX_NUM:
            compareString1 = o1.getAmexStatus() + o1.getAppDateSorted();
            compareString2 = o2.getAmexStatus() + o2.getAppDateSorted();
            break;

          default:
            break;
        }

        result = compareString1.equals(compareString2);
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }

      return result;
    }

    public int compare(Object o1, Object o2)
    {
      int result;

      try
      {
        result = compare((AppData)o1, (AppData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }

      return result;
    }

    public boolean equals(Object o1, Object o2)
    {
      boolean result;

      try
      {
        result = equals((AppData)o1, (AppData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }

      return result;
    }
  }
}/*@lineinfo:generated-code*/