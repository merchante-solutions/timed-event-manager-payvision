/*@lineinfo:filename=MerchDDFReconcileDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchDDFReconcileDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class MerchDDFReconcileDataBean extends MerchReconcileDataBean
{
  public MerchDDFReconcileDataBean( )
  {
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      for( int i = 0; i < 3; ++i )
      {
        if ( i == 0 )   // transactions
        {
          /*@lineinfo:generated-code*//*@lineinfo:64^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                         INDEX (gm pkgroup_merchant) 
//                         INDEX (sm pk_ddf_summary) 
//                      */
//                      mf.merchant_number              as hierarchy_node,
//                      mf.dba_name                     as org_name,
//                      mf.district                     as district,
//                      :orgId                          as org_num,
//                      sm.batch_date                   as activity_date,
//                      sum(sm.batch_count)             as item_count,
//                      sum(sm.bank_amount)             as item_amount
//              from    group_merchant            gm,
//                      mif                       mf,
//                      daily_detail_file_summary sm
//              where   gm.org_num          = :orgId and
//                      mf.merchant_number  = gm.merchant_number and 
//                      sm.merchant_number = mf.merchant_number and
//                      sm.batch_date between :beginDate and :endDate
//              group by mf.merchant_number, mf.district, mf.dba_name, sm.batch_date                
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                       INDEX (gm pkgroup_merchant) \n                       INDEX (sm pk_ddf_summary) \n                    */\n                    mf.merchant_number              as hierarchy_node,\n                    mf.dba_name                     as org_name,\n                    mf.district                     as district,\n                     :1                           as org_num,\n                    sm.batch_date                   as activity_date,\n                    sum(sm.batch_count)             as item_count,\n                    sum(sm.bank_amount)             as item_amount\n            from    group_merchant            gm,\n                    mif                       mf,\n                    daily_detail_file_summary sm\n            where   gm.org_num          =  :2  and\n                    mf.merchant_number  = gm.merchant_number and \n                    sm.merchant_number = mf.merchant_number and\n                    sm.batch_date between  :3  and  :4 \n            group by mf.merchant_number, mf.district, mf.dba_name, sm.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:85^11*/
        } 
        else if ( i == 1 )    // adjustments
        {
          /*@lineinfo:generated-code*//*@lineinfo:89^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                         INDEX (gm pkgroup_merchant) 
//                      */
//                      mf.merchant_number                      as hierarchy_node,
//                      mf.dba_name                             as org_name,
//                      mf.district                             as district,
//                      :orgId                                  as org_num,
//                      adj.batch_date                          as activity_date,
//                      count( adj.adjustment_amount )          as adj_count,
//                      sum( adj.adjustment_amount *
//                           decode(adj.debit_credit_ind,
//                                  'D',-1,1) )                 as adj_amount
//              from    group_merchant                gm,
//                      mif                           mf,
//                      daily_detail_file_adjustment  adj
//              where   gm.org_num          = :orgId and
//                      mf.merchant_number  = gm.merchant_number and 
//                      adj.merchant_account_number = mf.merchant_number and
//                      adj.batch_date between :beginDate and :endDate
//              group by mf.merchant_number, mf.district, mf.dba_name, adj.batch_date                
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                       INDEX (gm pkgroup_merchant) \n                    */\n                    mf.merchant_number                      as hierarchy_node,\n                    mf.dba_name                             as org_name,\n                    mf.district                             as district,\n                     :1                                   as org_num,\n                    adj.batch_date                          as activity_date,\n                    count( adj.adjustment_amount )          as adj_count,\n                    sum( adj.adjustment_amount *\n                         decode(adj.debit_credit_ind,\n                                'D',-1,1) )                 as adj_amount\n            from    group_merchant                gm,\n                    mif                           mf,\n                    daily_detail_file_adjustment  adj\n            where   gm.org_num          =  :2  and\n                    mf.merchant_number  = gm.merchant_number and \n                    adj.merchant_account_number = mf.merchant_number and\n                    adj.batch_date between  :3  and  :4 \n            group by mf.merchant_number, mf.district, mf.dba_name, adj.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:111^11*/
        }
        else if ( i == 2 )    // deposits
        {
          /*@lineinfo:generated-code*//*@lineinfo:115^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                         INDEX (gm pkgroup_merchant) 
//                         INDEX (sm pk_ddf_summary) 
//                      */
//                      mf.merchant_number              as hierarchy_node,
//                      mf.dba_name                     as org_name,
//                      mf.district                     as district,
//                      :orgId                          as org_num,
//                      sm.batch_date                   as activity_date,
//                      sum(sm.batch_count)             as ach_count,
//                      sum(sm.bank_amount)             as ach_amount
//              from    group_merchant            gm,
//                      mif                       mf,
//                      daily_detail_file_summary sm
//              where   gm.org_num          = :orgId and
//                      mf.merchant_number  = gm.merchant_number and 
//                      sm.merchant_number = mf.merchant_number and
//                      sm.batch_date between :beginDate and :endDate
//              group by mf.merchant_number, mf.district, mf.dba_name, sm.batch_date                
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                       INDEX (gm pkgroup_merchant) \n                       INDEX (sm pk_ddf_summary) \n                    */\n                    mf.merchant_number              as hierarchy_node,\n                    mf.dba_name                     as org_name,\n                    mf.district                     as district,\n                     :1                           as org_num,\n                    sm.batch_date                   as activity_date,\n                    sum(sm.batch_count)             as ach_count,\n                    sum(sm.bank_amount)             as ach_amount\n            from    group_merchant            gm,\n                    mif                       mf,\n                    daily_detail_file_summary sm\n            where   gm.org_num          =  :2  and\n                    mf.merchant_number  = gm.merchant_number and \n                    sm.merchant_number = mf.merchant_number and\n                    sm.batch_date between  :3  and  :4 \n            group by mf.merchant_number, mf.district, mf.dba_name, sm.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^11*/
        }
        else
        {
          continue;   // skip
        }
        resultSet = it.getResultSet();
  
        while( resultSet.next() )
        {
          addDetailResults( resultSet );
        }
        resultSet.close();
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    
    try
    {
      if ( hasAssocDistricts() )
      // returns false unless the current node is an
      // association and it has districts under it.
      {
        if ( District == DISTRICT_NONE )
        {
          for( int i = 0; i < 3; ++i )
          {
            if (i == 0)         // transactions
            {
              /*@lineinfo:generated-code*//*@lineinfo:178^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                           INDEX (ddf_sm pk_ddf_summary) 
//                         */
//                          o.org_num                                         as org_num,
//                          o.org_group                                       as hierarchy_node,
//                          nvl(mf.district,:DISTRICT_UNASSIGNED)             as district,
//                          nvl(ad.district_desc,decode( mf.district,
//                                      null,'Unassigned',
//                                     ('District ' || to_char(mf.district, '0009'))))         as org_name,
//                          sum(ddf_sm.batch_count)                           as item_count,
//                          sum(ddf_sm.bank_amount)                           as item_amount
//                  from    organization                  o,
//                          group_merchant                gm,
//                          group_rep_merchant            grm,
//                          mif                           mf,
//                          assoc_districts               ad,
//                          daily_detail_file_summary     ddf_sm
//                  where   o.org_num           = :orgId and
//                          gm.org_num          = o.org_num and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                          ad.district(+)      = mf.district and
//                          ddf_sm.merchant_number(+)  = gm.merchant_number and
//                          ddf_sm.batch_date(+) between :beginDate and :endDate
//                  group by  o.org_num, o.org_group, 
//                            mf.district, ad.district_desc
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                         INDEX (ddf_sm pk_ddf_summary) \n                       */\n                        o.org_num                                         as org_num,\n                        o.org_group                                       as hierarchy_node,\n                        nvl(mf.district, :1 )             as district,\n                        nvl(ad.district_desc,decode( mf.district,\n                                    null,'Unassigned',\n                                   ('District ' || to_char(mf.district, '0009'))))         as org_name,\n                        sum(ddf_sm.batch_count)                           as item_count,\n                        sum(ddf_sm.bank_amount)                           as item_amount\n                from    organization                  o,\n                        group_merchant                gm,\n                        group_rep_merchant            grm,\n                        mif                           mf,\n                        assoc_districts               ad,\n                        daily_detail_file_summary     ddf_sm\n                where   o.org_num           =  :2  and\n                        gm.org_num          = o.org_num and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                        ad.district(+)      = mf.district and\n                        ddf_sm.merchant_number(+)  = gm.merchant_number and\n                        ddf_sm.batch_date(+) between  :5  and  :6 \n                group by  o.org_num, o.org_group, \n                          mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^15*/
            }
            else if (i == 1)    // adjustments
            {
              /*@lineinfo:generated-code*//*@lineinfo:214^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                         */
//                          o.org_num                                         as org_num,
//                          o.org_group                                       as hierarchy_node,
//                          nvl(mf.district,:DISTRICT_UNASSIGNED)             as district,
//                          nvl(ad.district_desc,decode( mf.district,
//                                      null,'Unassigned',
//                                     ('District ' || to_char(mf.district, '0009'))))         as org_name,
//                          count( adj.adjustment_amount )                    as adj_count,
//                          sum( adj.adjustment_amount *
//                               decode(adj.debit_credit_ind,
//                                      'D',-1,1) )                           as adj_amount
//                  from    organization                  o,
//                          group_merchant                gm,
//                          group_rep_merchant            grm,
//                          mif                           mf,
//                          assoc_districts               ad,
//                          daily_detail_file_adjustment  adj
//                  where   o.org_num           = :orgId and
//                          gm.org_num          = o.org_num and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                          ad.district(+)      = mf.district and
//                          adj.merchant_account_number(+) = gm.merchant_number and
//                          adj.batch_date(+) between :beginDate and :endDate
//                  group by  o.org_num, o.org_group, 
//                            mf.district, ad.district_desc
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                       */\n                        o.org_num                                         as org_num,\n                        o.org_group                                       as hierarchy_node,\n                        nvl(mf.district, :1 )             as district,\n                        nvl(ad.district_desc,decode( mf.district,\n                                    null,'Unassigned',\n                                   ('District ' || to_char(mf.district, '0009'))))         as org_name,\n                        count( adj.adjustment_amount )                    as adj_count,\n                        sum( adj.adjustment_amount *\n                             decode(adj.debit_credit_ind,\n                                    'D',-1,1) )                           as adj_amount\n                from    organization                  o,\n                        group_merchant                gm,\n                        group_rep_merchant            grm,\n                        mif                           mf,\n                        assoc_districts               ad,\n                        daily_detail_file_adjustment  adj\n                where   o.org_num           =  :2  and\n                        gm.org_num          = o.org_num and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                        ad.district(+)      = mf.district and\n                        adj.merchant_account_number(+) = gm.merchant_number and\n                        adj.batch_date(+) between  :5  and  :6 \n                group by  o.org_num, o.org_group, \n                          mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:247^15*/
            }
            else if (i == 2)    // deposits
            {
              /*@lineinfo:generated-code*//*@lineinfo:251^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                             INDEX (gm pkgroup_merchant) 
//                             INDEX (sm pk_ddf_summary) 
//                          */
//                          o.org_num                                         as org_num,
//                          o.org_group                                       as hierarchy_node,
//                          nvl(mf.district,:DISTRICT_UNASSIGNED)             as district,
//                          nvl(ad.district_desc,decode( mf.district,
//                                      null,'Unassigned',
//                                     ('District ' || to_char(mf.district, '0009'))))         as org_name,
//                          sum(sm.batch_count)                               as ach_count,
//                          sum(sm.bank_amount)                               as ach_amount
//                  from    organization              o,
//                          group_merchant            gm,
//                          group_rep_merchant        grm,
//                          mif                       mf,
//                          assoc_districts           ad,
//                          daily_detail_file_summary sm
//                  where   o.org_num           = :orgId and
//                          gm.org_num          = o.org_num and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                          ad.district(+)      = mf.district and
//                          sm.merchant_number(+) = mf.merchant_number and
//                          sm.batch_date(+) between :beginDate and :endDate
//                  group by  o.org_num, o.org_group, 
//                            mf.district, ad.district_desc
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                           INDEX (gm pkgroup_merchant) \n                           INDEX (sm pk_ddf_summary) \n                        */\n                        o.org_num                                         as org_num,\n                        o.org_group                                       as hierarchy_node,\n                        nvl(mf.district, :1 )             as district,\n                        nvl(ad.district_desc,decode( mf.district,\n                                    null,'Unassigned',\n                                   ('District ' || to_char(mf.district, '0009'))))         as org_name,\n                        sum(sm.batch_count)                               as ach_count,\n                        sum(sm.bank_amount)                               as ach_amount\n                from    organization              o,\n                        group_merchant            gm,\n                        group_rep_merchant        grm,\n                        mif                       mf,\n                        assoc_districts           ad,\n                        daily_detail_file_summary sm\n                where   o.org_num           =  :2  and\n                        gm.org_num          = o.org_num and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                        ad.district(+)      = mf.district and\n                        sm.merchant_number(+) = mf.merchant_number and\n                        sm.batch_date(+) between  :5  and  :6 \n                group by  o.org_num, o.org_group, \n                          mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:283^15*/
            }
            else
            {           
              continue;     // skip
            }
            processSummaryData(it.getResultSet(),true);
            it.close();
          }
        }
        else    // a district was specified
        {
          for( int i = 0; i < 3; ++i )
          {
            if (i == 0)         // transactions
            {
              /*@lineinfo:generated-code*//*@lineinfo:299^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                           INDEX (ddf_sm pk_ddf_summary) 
//                         */
//                          o.org_num                                         as org_num,
//                          mf.merchant_number                                as hierarchy_node,
//                          mf.dba_name                                       as org_name,
//                          :District                                         as district,
//                          sum(ddf_sm.batch_count)                           as item_count,
//                          sum(ddf_sm.bank_amount)                           as item_amount
//                  from    group_merchant                gm,
//                          group_rep_merchant            grm,
//                          mif                           mf,
//                          organization                  o,
//                          daily_detail_file_summary     ddf_sm
//                  where   gm.org_num          = :orgId and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          nvl(mf.district,-1) = :District and
//                          o.org_group = mf.merchant_number and
//                          ddf_sm.merchant_number(+)  = gm.merchant_number and
//                          ddf_sm.batch_date(+) between :beginDate and :endDate
//                  group by  o.org_num, mf.merchant_number, mf.dba_name
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                         INDEX (ddf_sm pk_ddf_summary) \n                       */\n                        o.org_num                                         as org_num,\n                        mf.merchant_number                                as hierarchy_node,\n                        mf.dba_name                                       as org_name,\n                         :1                                          as district,\n                        sum(ddf_sm.batch_count)                           as item_count,\n                        sum(ddf_sm.bank_amount)                           as item_amount\n                from    group_merchant                gm,\n                        group_rep_merchant            grm,\n                        mif                           mf,\n                        organization                  o,\n                        daily_detail_file_summary     ddf_sm\n                where   gm.org_num          =  :2  and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        nvl(mf.district,-1) =  :5  and\n                        o.org_group = mf.merchant_number and\n                        ddf_sm.merchant_number(+)  = gm.merchant_number and\n                        ddf_sm.batch_date(+) between  :6  and  :7 \n                group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:326^15*/
            }
            else if (i == 1)    // adjustments
            {
              /*@lineinfo:generated-code*//*@lineinfo:330^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                         */
//                          o.org_num                                         as org_num,
//                          mf.merchant_number                                as hierarchy_node,
//                          mf.dba_name                                       as org_name,
//                          :District                                         as district,
//                          count( adj.adjustment_amount )                    as adj_count,
//                          sum( adj.adjustment_amount *
//                               decode(adj.debit_credit_ind,
//                                      'D',-1,1) )                           as adj_amount
//                  from    group_merchant                gm,
//                          group_rep_merchant            grm,
//                          mif                           mf,
//                          organization                  o,
//                          daily_detail_file_adjustment  adj
//                  where   gm.org_num          = :orgId and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          nvl(mf.district,-1) = :District and
//                          o.org_group = mf.merchant_number and
//                          adj.merchant_account_number(+) = gm.merchant_number and
//                          adj.batch_date(+) between :beginDate and :endDate
//                  group by  o.org_num, mf.merchant_number, mf.dba_name
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                       */\n                        o.org_num                                         as org_num,\n                        mf.merchant_number                                as hierarchy_node,\n                        mf.dba_name                                       as org_name,\n                         :1                                          as district,\n                        count( adj.adjustment_amount )                    as adj_count,\n                        sum( adj.adjustment_amount *\n                             decode(adj.debit_credit_ind,\n                                    'D',-1,1) )                           as adj_amount\n                from    group_merchant                gm,\n                        group_rep_merchant            grm,\n                        mif                           mf,\n                        organization                  o,\n                        daily_detail_file_adjustment  adj\n                where   gm.org_num          =  :2  and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        nvl(mf.district,-1) =  :5  and\n                        o.org_group = mf.merchant_number and\n                        adj.merchant_account_number(+) = gm.merchant_number and\n                        adj.batch_date(+) between  :6  and  :7 \n                group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:358^15*/
            }
            else if (i == 2)    // deposits
            {
              /*@lineinfo:generated-code*//*@lineinfo:362^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                             INDEX (gm pkgroup_merchant) 
//                             INDEX (sm pk_ddf_summary) 
//                          */
//                          o.org_num                                         as org_num,
//                          mf.merchant_number                                as hierarchy_node,
//                          mf.dba_name                                       as org_name,
//                          :District                                         as district,
//                          sum(sm.batch_count)                               as ach_count,
//                          sum(sm.bank_amount)                               as ach_amount
//                  from    organization              o,
//                          group_merchant            gm,
//                          group_rep_merchant        grm,
//                          mif                       mf,
//                          daily_detail_file_summary sm
//                  where   gm.org_num          = :orgId and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          nvl(mf.district,-1) = :District and
//                          o.org_group = mf.merchant_number and
//                          sm.merchant_number(+) = gm.merchant_number and
//                          sm.batch_date(+) between :beginDate and :endDate
//                  group by  o.org_num, mf.merchant_number, mf.dba_name
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                           INDEX (gm pkgroup_merchant) \n                           INDEX (sm pk_ddf_summary) \n                        */\n                        o.org_num                                         as org_num,\n                        mf.merchant_number                                as hierarchy_node,\n                        mf.dba_name                                       as org_name,\n                         :1                                          as district,\n                        sum(sm.batch_count)                               as ach_count,\n                        sum(sm.bank_amount)                               as ach_amount\n                from    organization              o,\n                        group_merchant            gm,\n                        group_rep_merchant        grm,\n                        mif                       mf,\n                        daily_detail_file_summary sm\n                where   gm.org_num          =  :2  and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        nvl(mf.district,-1) =  :5  and\n                        o.org_group = mf.merchant_number and\n                        sm.merchant_number(+) = gm.merchant_number and\n                        sm.batch_date(+) between  :6  and  :7 \n                group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:389^15*/
            }
            else
            {           
              continue;     // skip
            }
            processSummaryData(it.getResultSet(),true);
            it.close();
          }
        }
      }
      else // just standard child report, no districts
      {
        for( int i = 0; i < 3; ++i )
        {
          if (i == 0)         // transactions
          {
            /*@lineinfo:generated-code*//*@lineinfo:406^13*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                           INDEX (ddf_sm pk_ddf_summary) 
//                        */
//                        o.org_num                                         as org_num,
//                        o.org_group                                       as hierarchy_node,
//                        o.org_name                                        as org_name,
//                        0                                                 as district,
//                        sum(ddf_sm.batch_count)                           as item_count,
//                        sum(ddf_sm.bank_amount)                           as item_amount
//                from    parent_org                    po,
//                        organization                  o,
//                        group_merchant                gm,
//                        group_rep_merchant            grm,
//                        daily_detail_file_summary     ddf_sm
//                where   po.parent_org_num   = :orgId and
//                        o.org_num           = po.org_num and
//                        gm.org_num(+)       = o.org_num and
//                        grm.user_id(+) = :AppFilterUserId and
//                        grm.merchant_number(+) = gm.merchant_number and
//                        ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                        ddf_sm.merchant_number(+)  = gm.merchant_number and
//                        ddf_sm.batch_date(+) between :beginDate and :endDate
//                group by o.org_num, o.org_group, o.org_name
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                         INDEX (ddf_sm pk_ddf_summary) \n                      */\n                      o.org_num                                         as org_num,\n                      o.org_group                                       as hierarchy_node,\n                      o.org_name                                        as org_name,\n                      0                                                 as district,\n                      sum(ddf_sm.batch_count)                           as item_count,\n                      sum(ddf_sm.bank_amount)                           as item_amount\n              from    parent_org                    po,\n                      organization                  o,\n                      group_merchant                gm,\n                      group_rep_merchant            grm,\n                      daily_detail_file_summary     ddf_sm\n              where   po.parent_org_num   =  :1  and\n                      o.org_num           = po.org_num and\n                      gm.org_num(+)       = o.org_num and\n                      grm.user_id(+) =  :2  and\n                      grm.merchant_number(+) = gm.merchant_number and\n                      ( not grm.user_id is null or  :3  = -1 ) and        \n                      ddf_sm.merchant_number(+)  = gm.merchant_number and\n                      ddf_sm.batch_date(+) between  :4  and  :5 \n              group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:432^13*/
          }
          else if (i == 1)    // adjustments
          {
            /*@lineinfo:generated-code*//*@lineinfo:436^13*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                        */
//                        o.org_num                                         as org_num,
//                        o.org_group                                       as hierarchy_node,
//                        o.org_name                                        as org_name,
//                        0                                                 as district,
//                        count( adj.adjustment_amount )                    as adj_count,
//                        sum( adj.adjustment_amount *
//                             decode(adj.debit_credit_ind,
//                                    'D',-1,1) )                           as adj_amount
//                from    parent_org                    po,
//                        organization                  o,
//                        group_merchant                gm,
//                        group_rep_merchant            grm,
//                        daily_detail_file_adjustment  adj
//                where   po.parent_org_num   = :orgId and
//                        o.org_num           = po.org_num and
//                        gm.org_num(+)       = o.org_num and
//                        grm.user_id(+) = :AppFilterUserId and
//                        grm.merchant_number(+) = gm.merchant_number and
//                        ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                        adj.merchant_account_number(+) = gm.merchant_number and
//                        adj.batch_date(+) between :beginDate and :endDate
//                group by o.org_num, o.org_group, o.org_name
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                      */\n                      o.org_num                                         as org_num,\n                      o.org_group                                       as hierarchy_node,\n                      o.org_name                                        as org_name,\n                      0                                                 as district,\n                      count( adj.adjustment_amount )                    as adj_count,\n                      sum( adj.adjustment_amount *\n                           decode(adj.debit_credit_ind,\n                                  'D',-1,1) )                           as adj_amount\n              from    parent_org                    po,\n                      organization                  o,\n                      group_merchant                gm,\n                      group_rep_merchant            grm,\n                      daily_detail_file_adjustment  adj\n              where   po.parent_org_num   =  :1  and\n                      o.org_num           = po.org_num and\n                      gm.org_num(+)       = o.org_num and\n                      grm.user_id(+) =  :2  and\n                      grm.merchant_number(+) = gm.merchant_number and\n                      ( not grm.user_id is null or  :3  = -1 ) and        \n                      adj.merchant_account_number(+) = gm.merchant_number and\n                      adj.batch_date(+) between  :4  and  :5 \n              group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:463^13*/
          }
          else if (i == 2)    // deposits
          {
            /*@lineinfo:generated-code*//*@lineinfo:467^13*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                           INDEX (sm pk_ddf_summary) 
//                        */
//                        o.org_num                                         as org_num,
//                        o.org_group                                       as hierarchy_node,
//                        o.org_name                                        as org_name,
//                        0                                                 as district,
//                        sum(sm.batch_count)                               as ach_count,
//                        sum(sm.bank_amount)                               as ach_amount
//                from    parent_org                    po,
//                        organization                  o,
//                        group_merchant                gm,
//                        group_rep_merchant            grm,
//                        daily_detail_file_summary     sm
//                where   po.parent_org_num   = :orgId and
//                        o.org_num           = po.org_num and
//                        gm.org_num(+)       = o.org_num and
//                        grm.user_id(+) = :AppFilterUserId and
//                        grm.merchant_number(+) = gm.merchant_number and
//                        ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                        sm.merchant_number(+) = gm.merchant_number and
//                        sm.batch_date(+) between :beginDate and :endDate
//                group by o.org_num, o.org_group, o.org_name
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                         INDEX (sm pk_ddf_summary) \n                      */\n                      o.org_num                                         as org_num,\n                      o.org_group                                       as hierarchy_node,\n                      o.org_name                                        as org_name,\n                      0                                                 as district,\n                      sum(sm.batch_count)                               as ach_count,\n                      sum(sm.bank_amount)                               as ach_amount\n              from    parent_org                    po,\n                      organization                  o,\n                      group_merchant                gm,\n                      group_rep_merchant            grm,\n                      daily_detail_file_summary     sm\n              where   po.parent_org_num   =  :1  and\n                      o.org_num           = po.org_num and\n                      gm.org_num(+)       = o.org_num and\n                      grm.user_id(+) =  :2  and\n                      grm.merchant_number(+) = gm.merchant_number and\n                      ( not grm.user_id is null or  :3  = -1 ) and        \n                      sm.merchant_number(+) = gm.merchant_number and\n                      sm.batch_date(+) between  :4  and  :5 \n              group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.MerchDDFReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.MerchDDFReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:493^13*/
          }
          else
          {           
            continue;     // skip
          }
          processSummaryData(it.getResultSet(),true);
          it.close();
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
}/*@lineinfo:generated-code*/