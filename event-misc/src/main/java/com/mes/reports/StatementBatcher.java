/*@lineinfo:filename=StatementBatcher*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/StatementBatcher.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 3/13/03 9:10a $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class StatementBatcher extends ReportSQLJBean
{
  public static final int     ST_DATE_SELECTION         = 0;
  public static final int     ST_CONFIRMATION           = 1;
  
  private int         ReportState             = ST_DATE_SELECTION;
  
  public StatementBatcher()
  {
  }
  
  public byte[] encodePDF( )
  {
    ResultSetIterator         it            = null;
    StringBuffer              logoFilename  = new StringBuffer("");
    byte[]                    pdfData       = null;
    ResultSet                 rs            = null;
    StatementRecord           sr            = new StatementRecord();
    
    try
    {
      sr.connect();
      
      // create yyyymm formatted longs out of the to and from sql dates
      BeanDate date = new BeanDate();
      date.setSqlDate(ReportDateBegin);
      long fromYm = date.getYear() * 100 + date.getMonth() + 1;
      date.setSqlDate(ReportDateEnd);
      long toYm = date.getYear() * 100 + date.getMonth() + 1;
    
      // select statements
      /*@lineinfo:generated-code*//*@lineinfo:73^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ INDEX (gm pkgroup_merchant) */
//                  s.merch_num,
//                  s.rec_num,
//                  s.year_month
//          from    merch_statements    s,
//                  group_merchant      gm,
//                  group_rep_merchant  grm
//          where   gm.org_num      = :getReportOrgId() and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 )
//                  and s.merch_num = gm.merchant_number
//                  and s.year_month between :fromYm and :toYm
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1301 = getReportOrgId();
  try {
   String theSqlTS = "select  /*+ INDEX (gm pkgroup_merchant) */\n                s.merch_num,\n                s.rec_num,\n                s.year_month\n        from    merch_statements    s,\n                group_merchant      gm,\n                group_rep_merchant  grm\n        where   gm.org_num      =  :1  and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 )\n                and s.merch_num = gm.merchant_number\n                and s.year_month between  :4  and  :5";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.StatementBatcher",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1301);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,fromYm);
   __sJT_st.setLong(5,toYm);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.StatementBatcher",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^7*/
      rs = it.getResultSet();
      
      // iterate throught result set, load statements, output to buffer
      Document doc = new Document(PageSize.A4,18,18,18,18);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      PdfWriter.getInstance(doc,baos);
      doc.open();
      
      Image logo = null;
      int statementCount = 0;
      while (rs.next())
      {
        String  merchNum      = rs.getString("merch_num");
        long    recNum        = rs.getLong  ("rec_num");
        long    yearMonth     = rs.getLong  ("year_month");
        
        if (statementCount > 0)
        {
          doc.newPage();
        }
        
        if (logo == null)
        {
          StringBuffer logoURL = new StringBuffer("");
          
          logoURL.append("http://");
          logoURL.append(ServletRequest.getHeader("HOST"));
          logoURL.append(sr.getLogoFilename(merchNum));
          
          URL theURL = new URL(logoURL.toString());
          logo = Image.getInstance(theURL);
        }
      
        sr.getData(merchNum,recNum,yearMonth);
        com.mes.reports.Statement s = sr.getStatement();
        
        for (int i = 0, pgCnt = s.getPageCount(); i < pgCnt; ++i)
        {
          doc.add(logo);
          StatementPage p = s.getPage(i);
          for (int j = 0, lnCnt = p.getLineCount(); j < lnCnt; ++j)
          {
            Paragraph para =
              new Paragraph(11,p.getLine(j).toString(),new Font(Font.COURIER,9));
            para.setAlignment(Element.ALIGN_CENTER);
            doc.add(para);
          }
    
          if (i < pgCnt - 1)
          {
            doc.newPage();
          }
          
          ++statementCount;
        }
      }
        
      doc.close();
      baos.flush();
      baos.close();
      
      pdfData = baos.toByteArray();
    }
    catch (Exception e)
    {
      logEntry( "encodePDF()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
      try{ sr.cleanUp(); } catch(Exception e) { }
    }
    
    return( pdfData );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_statements_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"MMMyyyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"MMMyyyy" ) );
    }      
    
    // build the first date into the filename
    return ( filename.toString() );
  }
  
  public int getReportState( )
  {
    return( ReportState );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_PDF:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void setProperties(HttpServletRequest request)
  {
    Calendar    cal           = Calendar.getInstance();
    
    super.setProperties(request);
    
    if ( HttpHelper.getString(request,"submit",null) != null )
    {
      setSubmit(true);
    }
    
    ReportState  = HttpHelper.getInt(request,"state",ST_DATE_SELECTION);
    
    // if the from date is today, then set the default to rolling previous week
    if ( usingDefaultReportDates() )
    {
      // setup the default date range
      cal.setTime( ReportDateBegin );
      
      if ( cal.get( Calendar.MONTH ) == Calendar.JANUARY )
      {
        cal.set( Calendar.MONTH, Calendar.DECEMBER );
        cal.set( Calendar.YEAR,  ( cal.get( Calendar.YEAR ) - 1 ) );
      }
      else
      {
        cal.set( Calendar.MONTH, ( cal.get( Calendar.MONTH ) - 1 ) );
      }
      
      // since this report is for one month at a time,
      // make sure that the from date always starts with
      // the first day of the specified month.
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      // set both the begin and end dates
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }    
    
    // always set the report end date to the begin date
    // because this report only support one month at a time
    setReportDateEnd  ( getReportDateBegin() );
  }
}/*@lineinfo:generated-code*/