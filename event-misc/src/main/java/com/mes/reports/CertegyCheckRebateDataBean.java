/*@lineinfo:filename=CertegyCheckRebateDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CertegyCheckRebateDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 10/07/04 1:30p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class CertegyCheckRebateDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public Date               ActiveDate        = null;
    public double             AdjustedAmount    = 0.0;
    public String             DbaName           = null;
    public long               MerchantId        = 0L;
    public double             RebateAmount      = 0.0;
    public double             RebateRate        = 0.0;
    public String             StationId         = null;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      ActiveDate        = resultSet.getDate("active_date");
      AdjustedAmount    = resultSet.getDouble("adj_amount");
      DbaName           = resultSet.getString("dba_name");
      MerchantId        = resultSet.getLong("merchant_number");
      RebateAmount      = resultSet.getDouble("rebate_amount");
      RebateRate        = resultSet.getDouble("rebate_rate");
      StationId         = resultSet.getString("station_id");
    }
  }
  
  public class SummaryData
  {
    public double             AdjustedAmount  = 0.0;
    public long               HierarchyNode   = 0L;
    public long               OrgId           = 0L;
    public String             OrgName         = null;
    public double             RebateAmount    = 0.0;
    
    public SummaryData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      OrgId           = resultSet.getLong("org_num");
      HierarchyNode   = resultSet.getLong("node_id");
      OrgName         = resultSet.getString("org_name");
      AdjustedAmount  = resultSet.getDouble("adj_amount");
      RebateAmount    = resultSet.getDouble("rebate_amount");
    }
  }
  
  public CertegyCheckRebateDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_DETAILS )
    {
      line.append("\"Merchant Number\",");
      line.append("\"DBA Name\",");
      line.append("\"Station ID\",");
      line.append("\"Active Date\",");
      if ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_CERTEGY_REBATE_ADMIN ) )
      {
        line.append("\"Adjusted Amount\",");
        line.append("\"Rebate Rate\",");
      }        
      line.append("\"Rebate Amount\"");
    }
    else    // default is summary
    {
      line.append("\"Org ID\",");
      line.append("\"Org Name\",");
      if ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_CERTEGY_REBATE_ADMIN ) )
      {
        line.append("\"Adjusted Amount\",");
      }
      line.append("\"Rebate Amount\"");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    if ( ReportType == RT_DETAILS )
    {
      RowData         record    = (RowData)obj;
      
      line.append( encodeHierarchyNode( record.MerchantId ) );
      line.append( ",\"" );
      line.append( record.DbaName );
      line.append( "\"," );                                                
      line.append( record.StationId );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate(record.ActiveDate,"MM/dd/yyyy") );
      line.append( "," );
      if ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_CERTEGY_REBATE_ADMIN ) )
      {
        line.append( record.AdjustedAmount );
        line.append( "," );
        line.append( record.RebateRate );
        line.append( "," );
      }        
      line.append( record.RebateAmount );
    }
    else  // summary mode
    {
      SummaryData   record    = (SummaryData)obj;
      
      line.append( encodeHierarchyNode( record.HierarchyNode ) );
      line.append( ",\"" );
      line.append( record.OrgName );
      line.append( "\"," );
      if ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_CERTEGY_REBATE_ADMIN ) )
      {
        line.append( record.AdjustedAmount );
        line.append( "," );
      }        
      line.append( record.RebateAmount );
    }      
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    
    if ( ReportType == RT_DETAILS )
    {
      filename.append("_ccr_details_");
    }
    else // summary
    {
      filename.append("_ccr_summary_");
    }
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( ReportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:219^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ccr.merchant_number         as merchant_number,
//                    ccr.dba_name                as dba_name,
//                    ccr.active_date             as active_date,
//                    ccr.station_id              as station_id,
//                    ccr.adjusted_amount         as adj_amount,
//                    .5                          as rebate_rate,
//                    (ccr.adjusted_amount * 0.5) as rebate_amount
//            from    organization            o,
//                    group_merchant          gm,
//                    certegy_check_rebate    ccr
//            where   o.org_num = :orgId and
//                    -- o.org_group = :nodeId and
//                    gm.org_num = o.org_num and
//                    ccr.merchant_number = gm.merchant_number and
//                    ccr.active_date between :beginDate and :endDate
//            order by ccr.dba_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ccr.merchant_number         as merchant_number,\n                  ccr.dba_name                as dba_name,\n                  ccr.active_date             as active_date,\n                  ccr.station_id              as station_id,\n                  ccr.adjusted_amount         as adj_amount,\n                  .5                          as rebate_rate,\n                  (ccr.adjusted_amount * 0.5) as rebate_amount\n          from    organization            o,\n                  group_merchant          gm,\n                  certegy_check_rebate    ccr\n          where   o.org_num =  :1  and\n                  -- o.org_group = :nodeId and\n                  gm.org_num = o.org_num and\n                  ccr.merchant_number = gm.merchant_number and\n                  ccr.active_date between  :2  and  :3 \n          order by ccr.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CertegyCheckRebateDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.CertegyCheckRebateDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^9*/
      }
      else  // default is summary
      {
        /*@lineinfo:generated-code*//*@lineinfo:241^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                   as org_num,
//                    o.org_group                 as node_id,
//                    o.org_name                  as org_name,
//                    sum(ccr.adjusted_amount)    as adj_amount,
//                    sum(ccr.mes_rebate_amount)  as rebate_amount
//            from    parent_org              po,
//                    organization            o,
//                    group_merchant          gm,
//                    certegy_check_rebate    ccr
//            where   po.parent_org_num = :orgId and
//                    o.org_num = po.org_num and
//                    gm.org_num = o.org_num and
//                    ccr.merchant_number = gm.merchant_number and
//                    ccr.active_date between :beginDate and :endDate
//            group by o.org_num, o.org_group, o.org_name
//            order by o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                   as org_num,\n                  o.org_group                 as node_id,\n                  o.org_name                  as org_name,\n                  sum(ccr.adjusted_amount)    as adj_amount,\n                  sum(ccr.mes_rebate_amount)  as rebate_amount\n          from    parent_org              po,\n                  organization            o,\n                  group_merchant          gm,\n                  certegy_check_rebate    ccr\n          where   po.parent_org_num =  :1  and\n                  o.org_num = po.org_num and\n                  gm.org_num = o.org_num and\n                  ccr.merchant_number = gm.merchant_number and\n                  ccr.active_date between  :2  and  :3 \n          group by o.org_num, o.org_group, o.org_name\n          order by o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.CertegyCheckRebateDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.CertegyCheckRebateDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:259^9*/
      }
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        if ( ReportType == RT_DETAILS )
        {
          ReportRows.addElement( new RowData( resultSet ) );
        }
        else  // summary row
        {
          ReportRows.addElement( new SummaryData( resultSet ) );
        }
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar      cal     = Calendar.getInstance();
    
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, -5 );
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/