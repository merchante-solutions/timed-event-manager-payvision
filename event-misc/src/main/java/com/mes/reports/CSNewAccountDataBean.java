/*@lineinfo:filename=CSNewAccountDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CSNewAccountDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 7/17/03 1:33p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class CSNewAccountDataBean extends ReportSQLJBean
{
  public class RowDataDetail
  {
    public String      AcceptList           = null;
    public String      Address1             = null;
    public String      Address2             = null;
    public long        AssocNumber          = 0L;
    public String      City                 = null;
    public Date        DateOpened           = null;
    public String      DbaName              = null;
    public String      DDANumber            = null;
    public long        MerchantId           = 0L;
    public double      MCDiscountRate       = 0.0;
    public double      MCPerItemRate        = 0.0;
    public long        ParentOrgId          = 0L;
    public String      PhoneNumber          = null;
    public int         SicCode              = 0;
    public String      State                = null;
    public double      VisaDiscountRate     = 0.0;
    public double      VisaPerItemRate      = 0.0;
    public String      Zip                  = null;
    
    
    public RowDataDetail( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ParentOrgId       = resultSet.getLong("parent_org");
      AssocNumber       = resultSet.getLong("assoc_number");
      MerchantId        = resultSet.getLong("merchant_number");
      DateOpened        = resultSet.getDate("date_opened");
      DbaName           = resultSet.getString("dba_name");
      Address1          = resultSet.getString("address");
      Address2          = resultSet.getString("address2");
      City              = resultSet.getString("city");
      State             = resultSet.getString("state");
      Zip               = resultSet.getString("zip");
      DDANumber         = resultSet.getString("dda_number");
      SicCode           = resultSet.getInt("sic_code");
      PhoneNumber       = resultSet.getString("phone_number");
      AcceptList        = resultSet.getString("accept_list");
      VisaDiscountRate  = resultSet.getDouble("visa_disc_rate");
      VisaPerItemRate   = resultSet.getDouble("visa_per_item");
      MCDiscountRate    = resultSet.getDouble("mc_disc_rate");
      MCPerItemRate     = resultSet.getDouble("mc_per_item");
    }                          
    
    public String getAddressString()
    {
      StringBuffer      retVal = new StringBuffer("");
      
      if(Address1 != null)
      {
        retVal.append(Address1);
      }
      
      if ( Address2 != null )
      {
        retVal.append(" ");
        retVal.append(Address2);
      }
      return(retVal.toString());
    }
  }    

  public class RowDataSummary
  {
    public long       HierarchyNode       = 0L;
    public int        MerchCount          = 0;
    public long       OrgId               = 0L;
    public String     OrgName             = null;
    
    public RowDataSummary(  long            orgId,
                            long            node,
                            String          orgName,
                            int             count )
    {
      HierarchyNode       = node;
      OrgId               = orgId;
      OrgName             = orgName;
      MerchCount          = count;
    }
  }
  
  public CSNewAccountDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    if ( ReportType == RT_SUMMARY )
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"# Opened\",");
    }
    else // assume RT_DETAILS
    {
      line.append("\"Assoc #\",");
      line.append("\"Merchant Id\",");
      line.append("\"DBA Name\",");
      line.append("\"Address\",");
      line.append("\"DDA #\",");
      line.append("\"SIC\",");
      line.append("\"Phone\",");
      line.append("\"Plans\",");
      line.append("\"Visa Discount\",");
      line.append("\"Visa Per Item\",");
      line.append("\"MC Discount\",");
      line.append("\"MC PerItem\",");
      line.append("\"Date Opened\"");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
  
    if ( ReportType == RT_SUMMARY )
    {
      RowDataSummary       record    = (RowDataSummary)obj;
    
      line.append( record.HierarchyNode );
      line.append( ",\"" );
      line.append( record.OrgName );
      line.append( "\"," );
      line.append( record.MerchCount );
    }
    else // assume RT_DETAILS
    {
      RowDataDetail        record    = (RowDataDetail)obj;
    
      line.append( record.AssocNumber );
      line.append( ",");
      line.append( record.MerchantId );
      line.append( ",\"" );
      line.append( record.DbaName );
      line.append( "\",\"" );
      line.append( record.getAddressString() );
      line.append( "  " );
      line.append( record.City );
      line.append( ",  " );
      line.append( record.State );
      line.append( "  " );
      line.append( record.Zip );
      line.append( "\"," );
      line.append( record.DDANumber );
      line.append( "," );
      line.append( record.SicCode );
      line.append( ",\"" );
      line.append( record.PhoneNumber );
      line.append( "\",\"" );
      line.append( record.AcceptList );
      line.append( "\"," );
      line.append( record.VisaDiscountRate );
      line.append( "," );
      line.append( record.VisaPerItemRate );
      line.append( "," );
      line.append( record.MCDiscountRate );
      line.append( "," );
      line.append( record.MCPerItemRate );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate(record.DateOpened,"MM/dd/yyyy") );
    }
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long childNodeId, Date beginDate, Date endDate )
  {
    encodeNodeUrl( buffer, childNodeId, beginDate, endDate, ReportType );
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long childNodeId, Date beginDate, Date endDate, int reportType )
  {
    super.encodeNodeUrl( buffer, childNodeId, beginDate, endDate );
    buffer.append("&reportType=");
    buffer.append(reportType);
  }
  
  public void encodeToggleOrgUrl( StringBuffer buffer )
  {
    encodeNodeUrl( buffer, getReportHierarchyNode(), getReportDateBegin(), getReportDateEnd(), ((getReportType() == RT_SUMMARY) ? RT_DETAILS : RT_SUMMARY) );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_cs_opened_accounts_");
    
    if ( ReportType == RT_SUMMARY )
    {
      filename.append("_summary_");
    }
    else    // RT_DETAILS
    {
      filename.append("_details_");
    }
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public String getParentNameString( long orgId )
  {
    StringBuffer      retVal = new StringBuffer("");
    
    retVal.append( orgIdToHierarchyNode( orgId ) );
    retVal.append( " " );
    retVal.append( getOrgName( orgId ) );
    
    return( retVal.toString() );
  }
  
  public String getReportTypeString( )
  {
    return( ((ReportType == RT_SUMMARY) ? "Summary" : "Details") );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    loadData( getReportOrgId(), getReportDateBegin(), getReportDateEnd(), getReportType() );
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate, int reportType )
  {
    ResultSetIterator             it                = null;
    long                          lastNode          = 0L;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if( reportType == RT_SUMMARY )
      {
        /*@lineinfo:generated-code*//*@lineinfo:307^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.org_num                  as org_num,
//                    o.org_group                 as hierarchy_node,
//                    o.org_name                  as org_name,
//                    count(mf.merchant_number)   as num_new_merchants
//            from    parent_org          po,
//                    group_merchant      gm,
//                    group_rep_merchant  grm,
//                    organization        o,
//                    mif                 mf
//            where   po.parent_org_num = :orgId and
//                    gm.org_num = po.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    o.org_num = gm.org_num and        
//                    mf.merchant_number = gm.merchant_number and
//                    to_date(decode(mf.date_opened,
//                                   '000000','010100',
//                                   mf.date_opened),'mmddyy') between :beginDate and :endDate
//            group by po.org_num, o.org_name, o.org_group
//            order by o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.org_num                  as org_num,\n                  o.org_group                 as hierarchy_node,\n                  o.org_name                  as org_name,\n                  count(mf.merchant_number)   as num_new_merchants\n          from    parent_org          po,\n                  group_merchant      gm,\n                  group_rep_merchant  grm,\n                  organization        o,\n                  mif                 mf\n          where   po.parent_org_num =  :1  and\n                  gm.org_num = po.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  o.org_num = gm.org_num and        \n                  mf.merchant_number = gm.merchant_number and\n                  to_date(decode(mf.date_opened,\n                                 '000000','010100',\n                                 mf.date_opened),'mmddyy') between  :4  and  :5 \n          group by po.org_num, o.org_name, o.org_group\n          order by o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CSNewAccountDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.CSNewAccountDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:330^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          ReportRows.addElement( new RowDataSummary( resultSet.getLong("org_num"),
                                                     resultSet.getLong("hierarchy_node"),
                                                     resultSet.getString("org_name"),
                                                     resultSet.getInt("num_new_merchants") ) );
        }
        it.close();   // this will also close the resultSet
      }
      else // RT_DETAILS
      {
        /*@lineinfo:generated-code*//*@lineinfo:344^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  po.org_num                  as parent_org,
//                    mf.dmagent                  as assoc_number,
//                    mf.merchant_number          as merchant_number,
//                    to_date(decode(mf.date_opened,
//                                   '000000','010100',
//                                   mf.date_opened),'mmddyy') as date_opened,                  
//                    mf.dba_name                 as dba_name,
//                    mf.ADDR1_LINE_1             as address,
//                    mf.ADDR1_LINE_2             as address2,
//                    mf.DMCITY                   as city,
//                    mf.DMSTATE                  as state,
//                    substr(mf.DMZIP,1,5)        as zip,
//                    mf.DDA_NUM                  as dda_number,
//                    mf.sic_code                 as sic_code,
//                    (
//                      substr(mf.PHONE_1,1,3) || '-' ||
//                      substr(mf.phone_1,4,3) || '-' ||
//                      substr(mf.phone_1,7,4)                   
//                    )                           as phone_number,
//                    (
//                      decode(mf.DVSACCPT,'Y','VS ','') ||
//                      decode(mf.DV$ACCPT,'Y','V$ ','') ||
//                      decode(mf.DMCACCPT,'Y','MC ','') ||
//                      decode(mf.DM$ACCPT,'Y','M$ ','') ||                                        
//                      decode(mf.DAMACCPT,'Y','AM ','') ||
//                      decode(mf.DDCACCPT,'Y','DC ','') || 
//                      decode(mf.DDSACCPT,'Y','DS ','')
//                    )                           as accept_list,
//                    (mf.VISA_DISC_RATE * 0.001) as visa_disc_rate,
//                    (mf.VISA_PER_ITEM * 0.001)  as visa_per_item,
//                    (mf.MASTCD_DISC_RATE * 0.001) as mc_disc_rate,
//                    (mf.mastcd_per_item * 0.001)  as mc_per_item
//            from    parent_org          po,
//                    group_merchant      gm,
//                    group_rep_merchant  grm,
//                    mif                 mf
//            where   po.parent_org_num = :orgId and
//                    gm.org_num = po.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    mf.merchant_number = gm.merchant_number and
//                    to_date(decode(mf.date_opened,
//                                   '000000','010100',
//                                   mf.date_opened),'mmddyy') between :beginDate and :endDate
//            order by mf.dmagent, mf.dba_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  po.org_num                  as parent_org,\n                  mf.dmagent                  as assoc_number,\n                  mf.merchant_number          as merchant_number,\n                  to_date(decode(mf.date_opened,\n                                 '000000','010100',\n                                 mf.date_opened),'mmddyy') as date_opened,                  \n                  mf.dba_name                 as dba_name,\n                  mf.ADDR1_LINE_1             as address,\n                  mf.ADDR1_LINE_2             as address2,\n                  mf.DMCITY                   as city,\n                  mf.DMSTATE                  as state,\n                  substr(mf.DMZIP,1,5)        as zip,\n                  mf.DDA_NUM                  as dda_number,\n                  mf.sic_code                 as sic_code,\n                  (\n                    substr(mf.PHONE_1,1,3) || '-' ||\n                    substr(mf.phone_1,4,3) || '-' ||\n                    substr(mf.phone_1,7,4)                   \n                  )                           as phone_number,\n                  (\n                    decode(mf.DVSACCPT,'Y','VS ','') ||\n                    decode(mf.DV$ACCPT,'Y','V$ ','') ||\n                    decode(mf.DMCACCPT,'Y','MC ','') ||\n                    decode(mf.DM$ACCPT,'Y','M$ ','') ||                                        \n                    decode(mf.DAMACCPT,'Y','AM ','') ||\n                    decode(mf.DDCACCPT,'Y','DC ','') || \n                    decode(mf.DDSACCPT,'Y','DS ','')\n                  )                           as accept_list,\n                  (mf.VISA_DISC_RATE * 0.001) as visa_disc_rate,\n                  (mf.VISA_PER_ITEM * 0.001)  as visa_per_item,\n                  (mf.MASTCD_DISC_RATE * 0.001) as mc_disc_rate,\n                  (mf.mastcd_per_item * 0.001)  as mc_per_item\n          from    parent_org          po,\n                  group_merchant      gm,\n                  group_rep_merchant  grm,\n                  mif                 mf\n          where   po.parent_org_num =  :1  and\n                  gm.org_num = po.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  mf.merchant_number = gm.merchant_number and\n                  to_date(decode(mf.date_opened,\n                                 '000000','010100',\n                                 mf.date_opened),'mmddyy') between  :4  and  :5 \n          order by mf.dmagent, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.CSNewAccountDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.CSNewAccountDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:392^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          ReportRows.addElement( new RowDataDetail( resultSet ) );
        }
        it.close();   // this will also close the resultSet
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, -7 );
      
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/