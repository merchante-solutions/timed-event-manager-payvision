/*@lineinfo:filename=SalesActivityBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/SalesActivityBean.sqlj $

  Description:

  Extends the date range report bean to generate a sales activity report.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-02-03 10:39:36 -0800 (Tue, 03 Feb 2009) $
  Version            : $Revision: 15771 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import sqlj.runtime.ResultSetIterator;

public class SalesActivityBean extends DateRangeReportBean
{
  public static final int     REPORT_TYPE_ALL         = 1;
  public static final int     REPORT_TYPE_ACTIVATED   = 2;

  private String  sourceTypeFilter  = "";
  //private int     sourceTypeFilter  = 0;
  private Vector  sourceTypes       = null;
  private int     mesSourceCount    = 0;
  private int     salesReportType   = REPORT_TYPE_ALL;
  private int     statusTypeFilter  = 0;
  private boolean nonactivatedOnly  = false;
  
  private String  loginFilter;

  /*
  ** CONSTRUCTOR public SalesActivityBean()
  */
  public SalesActivityBean()
  {
    ResultSetIterator it    = null;
    ResultSet         rs    = null;

    sourceTypes = new Vector();
    sourceTypes.add("MES Applications - All");

    try
    {
      connect();
      
      // load the region vectors
      /*@lineinfo:generated-code*//*@lineinfo:68^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rep_region_desc
//          from    rep_region
//          order by rep_region asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rep_region_desc\n        from    rep_region\n        order by rep_region asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.SalesActivityBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.SalesActivityBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:73^7*/
      
      rs = it.getResultSet();
      
      mesSourceCount = 0;
      while (rs.next())
      {
        sourceTypes.add("MES Applications - " + rs.getString(1));
        ++mesSourceCount;
      }

      rs.close();
      it.close();

      // get application source types besides mes direct (0) which
      // is broken into regions above
      /*@lineinfo:generated-code*//*@lineinfo:89^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_description
//          from    app_type
//          where   app_type_code != 0
//          order by app_description asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_description\n        from    app_type\n        where   app_type_code != 0\n        order by app_description asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.SalesActivityBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.SalesActivityBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:95^7*/
      
      rs = it.getResultSet();
      
      while (rs.next())
      {
        sourceTypes.add(rs.getString(1));
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      addError("SalesActivityBean constructor: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(),
               "SalesActivityBean constructor: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private int getAppType(String appDescription)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    int                 appType = -1;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:129^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type_code
//          from    app_type
//          where   app_description = :appDescription
//          order by app_type_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_type_code\n        from    app_type\n        where   app_description =  :1 \n        order by app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.SalesActivityBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,appDescription);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.SalesActivityBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:135^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        appType = rs.getInt("app_type_code");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getAppType(" + appDescription + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( appType );
  }
  
  private int getRegionType(String regionDesc)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    int                 regionType  = -1;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:171^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rep_region
//          from    rep_region
//          where   rep_region_desc = :regionDesc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rep_region\n        from    rep_region\n        where   rep_region_desc =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.SalesActivityBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,regionDesc);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.SalesActivityBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:176^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        regionType = rs.getInt("rep_region");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getRegionType(" + regionDesc + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( regionType );
  }
  
  /*
  ** METHOD public ResultSet executeQuery()
  **
  ** Query's database for sales activity data.  This is basically all
  ** applications submitted within a given date range.  Data can be filtered
  ** by sales rep sourceTypes.  Query may be filtered by sales rep region
  ** type if this is for MES direct sales.  Query may be filtered by
  ** application source type to break out applications by bank type
  ** (SVB, Orange, etc.)
  **
  ** RETURNS: the query result set.
  */
  public ResultSet executeQuery()
  {
    StringBuffer      qs  = new StringBuffer("");
    PreparedStatement ps  = null;
    ResultSet         rs  = null;

    int appType = 0;
    int regionType = 0;
    System.out.println("source type filter: " + sourceTypeFilter);
    System.out.println("mesSourceCount: " + mesSourceCount);
    
    appType = getAppType(sourceTypeFilter);
    
    if(appType == -1)
    {
      regionType = getRegionType(sourceTypeFilter);
    }
    
    /*
    if (sourceTypeFilter > mesSourceCount)
    {
      appType = sourceTypeFilter - mesSourceCount;
      System.out.println("app type: " + appType);
    }
    else if (sourceTypeFilter > 0)
    {
      regionType = sourceTypeFilter;
    }
    */

    try
    {
      // make this connection immune to big brother garbage collection
      connect(true);
      
      qs.append("select  ");
      qs.append(" a.app_created_date          app_created_date, ");
      qs.append(" m.merch_business_name       merch_business_name, ");
      qs.append(" m.merch_number              merch_number, ");
      qs.append(" m.merch_month_visa_mc_sales merch_month_visa_mc_sales, ");
      qs.append(" m.merch_average_cc_tran     merch_average_cc_tran, ");
      qs.append(" i.grid_type                 grid_type, ");
      qs.append(" m.merch_credit_status       merch_credit_status, ");
      qs.append(" m.date_activated            date_activated, ");
      qs.append(" t.tranchrg_discrate_type    tranchrg_discrate_type, ");
      qs.append(" t.tranchrg_disc_rate        tranchrg_disc_rate, ");
      qs.append(" t.tranchrg_pass_thru        tranchrg_pass_thru, ");
      qs.append(" u.login_name                login_name, ");
      qs.append(" i.interchange_rate          interchange_rate, ");
      qs.append(" i.assoc_assess_fee          assoc_assess_fee, ");
      qs.append(" i.interchange_fee           interchange_fee, ");
      qs.append(" a.app_seq_num               app_seq_num, ");
      qs.append(" a.app_type                  app_type, ");
      qs.append(" t.tranchrg_per_tran         tranchrg_per_tran, ");
      qs.append(" t.tranchrg_per_auth         tranchrg_per_auth, ");
      qs.append(" t.tranchrg_per_capture      tranchrg_per_capture, ");
      qs.append(" ers.rental_total            rental_total, ");
      qs.append(" eps.purchase_total          purchase_total, ");
      qs.append(" m.merch_prior_statements    merch_prior_statements, ");
      qs.append(" a.app_user_id               app_user_id, ");
      qs.append(" rc.pay_total                pay_total, ");
      qs.append(" mif.dmagent                 dmagent, ");
      qs.append(" aa.app_seq_num              amex_app_seq_num, ");
      qs.append(" aa.split_dial               split_dial, ");
      qs.append(" da.app_seq_num              disc_app_seq_num, ");
      qs.append(" amex.charges                amex_charges, ");
      qs.append(" disc.charges                disc_charges, ");
      qs.append(" it.charge                   i_fee, ");
      qs.append(" f.charges                   charges ");

      qs.append("from ");
      qs.append(" application a, ");
      qs.append(" merchant m, ");
      qs.append(" tranchrg t, ");
      qs.append(" amex_fee_per_tran amex, ");
      qs.append(" disc_fee_per_tran disc, ");
      qs.append(" users u, ");
      qs.append(" equip_rental_summaries ers, ");
      qs.append(" equip_purchase_summaries eps, ");
      qs.append(" additional_per_tran_fees f, ");
      qs.append(" rep_commissions rc, ");
      qs.append(" mif, ");
      qs.append(" amex_accepted aa, ");
      qs.append(" disc_accepted da, ");
      qs.append(" internet_per_tran_fees it, ");
      if (regionType > 0)
      {
        qs.append(" sales_rep s, ");
      }
      qs.append(" interchange_cost i ");

      qs.append("where ");

      switch(salesReportType)
      {
        case REPORT_TYPE_ALL:
          qs.append(" trunc(a.app_created_date) between ? and ? ");
          break;

        case REPORT_TYPE_ACTIVATED:
          qs.append(" trunc(m.date_activated) between ? and ? ");
          qs.append(" and m.date_activated is not null ");
          break;
      }
      
      if (nonactivatedOnly)
      {
        qs.append(" and m.date_activated is null ");
      }

      if (appType != 31)
      {
        qs.append(" and a.app_type = " + appType);
      }
      else
      {
        qs.append(" and a.app_type in ( 1, 31 ) ");
      }
      
      qs.append(" and a.app_user_id = u.user_id ");
      qs.append(" and a.app_seq_num = m.app_seq_num ");
      qs.append(" and a.app_seq_num = t.app_seq_num ");
      qs.append(" and instr(lower(m.merch_business_name),'test') = 0 ");
      if (loginFilter != null && loginFilter.length() > 0)
      {
        qs.append(" and a.app_user_login = '" + loginFilter + "' ");
      }
      else if (statusTypeFilter > 0)
      {
        qs.append(" and m.merch_credit_status = " + statusTypeFilter + " ");
      }
      else
      {
        qs.append(" and m.merch_credit_status != 4 ");
      }
      qs.append(" and t.cardtype_code = 1 ");
      qs.append(" and (m.pricing_grid = i.grid_type or ");
      qs.append("      ((m.pricing_grid is null or m.pricing_grid = 0) and ");
      qs.append("        (((m.industype_code = 6 or m.industype_code = 7 or m.loctype_code = 2) and i.grid_type = 2) or ");
      qs.append("         ((m.industype_code <> 6 and m.industype_code <> 7 and m.loctype_code <> 2) and i.grid_type = 1)))) ");
      qs.append(" and a.app_seq_num = ers.app_seq_num(+) ");
      qs.append(" and a.app_seq_num = eps.app_seq_num(+) ");
      qs.append(" and a.app_seq_num = f.app_seq_num(+) ");
      qs.append(" and a.app_seq_num = rc.app_seq_num(+) ");
      qs.append(" and m.merch_number = mif.merchant_number(+) ");
      qs.append(" and a.app_seq_num = aa.app_seq_num(+) ");
      qs.append(" and a.app_seq_num = da.app_seq_num(+) ");
      qs.append(" and a.app_seq_num = amex.app_seq_num(+) ");
      qs.append(" and a.app_seq_num = disc.app_seq_num(+) ");
      qs.append(" and a.app_seq_num = it.app_seq_num(+) ");

      if (regionType > 0)
      {
        qs.append(" and s.user_id = a.app_user_id ");
        qs.append(" and s.rep_region = ? ");
      }

      switch(salesReportType)
      {
        case REPORT_TYPE_ALL:
          qs.append("order by a.app_created_date asc");
          break;

        case REPORT_TYPE_ACTIVATED:
          qs.append("order by m.date_activated asc");
          break;
      }
      
      ps = getPreparedStatement(qs.toString());
      int bindVar = 1;
      ps.setDate(1,fromDate.getSqlDate());
      ps.setDate(2,toDate.getSqlDate());

      if (regionType > 0)
      {
        ps.setInt(3,regionType);
      }
      
      System.out.println(qs.toString());

      rs = ps.executeQuery();
    }
    catch (Exception e)
    {
      addError("executeQuery: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(),
               "executeQuery: " + e.toString());
    }

    return rs;
  }

  /*
  ** METHOD public void setProperties(HttpServletRequest request)
  **
  ** Called by the download servlet to load internal members of bean prior
  ** to a call to executeQuery().
  */
  public void setProperties(HttpServletRequest request)
  {
    try
    {
      sourceTypeFilter = request.getParameter("sourceTypeFilter");
      salesReportType = Integer.parseInt(request.getParameter("salesReportType"));
      setFromDay  (Integer.parseInt(request.getParameter("fromDay")));
      setFromMonth(Integer.parseInt(request.getParameter("fromMonth")));
      setFromYear (Integer.parseInt(request.getParameter("fromYear")));
      setToDay    (Integer.parseInt(request.getParameter("toDay")));
      setToMonth  (Integer.parseInt(request.getParameter("toMonth")));
      setToYear   (Integer.parseInt(request.getParameter("toYear")));
    }
    catch (Exception e)
    {
    }
  }

  /*
  ** METHOD public void displayTopCustomControls(JspWriter out)
  **
  ** Adds the date type drop down to the date range controls on the page.
  */
  public void displayTopCustomControls(JspWriter out)
  {
    try
    {
      out.println("          <tr>");
      out.println("            <td height=\"30\" width=\"15%\">");
      out.println("              <strong class=\"headingSub\">Type</strong>");
      out.println("            </td>");
      out.println("            <td colspan=\"2\" width=\"70%\">");
      out.println("              <select class=\"formFields\" name=\"salesReportType\">");
      out.println("                <option value=\"" + REPORT_TYPE_ALL + "\" " +
                                   getSalesReportType(REPORT_TYPE_ALL) +
                                   " >Submission Date</option>");
      out.println("                <option value=\"" + REPORT_TYPE_ACTIVATED + "\" " +
                                   getSalesReportType(REPORT_TYPE_ACTIVATED) +
                                   " >Activation Date</option>");
      out.println("              </select>");
      out.println("            </td>");
      out.println("          </tr>");
    }
    catch (Exception e)
    {
      addError("displayDateControls: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(),
               "displayDateControls: " + e.toString());
    }
  }

  /*
  ** METHOD public String getDownloadFilenameBase()
  **
  ** Called by the download servlet to determine the download filename
  ** (without the extension).  Returns a file name in the format of
  ** "SalesActivity<fromDate>to<toDate>".
  **
  ** RETURNS: download filename base.
  */
  public String getDownloadFilenameBase()
  {
    return "SalesActivitity" + fromDate.toString() + "to" + toDate.toString();
  }

  /*
  ** ACCESSORS
  */
  public String getSourceTypeFilter()
  {
    return sourceTypeFilter;
  }
  public void setSourceTypeFilter(String newSourceTypeFilter)
  {
    sourceTypeFilter = newSourceTypeFilter;
  }

  public Vector getSourceTypesVector()
  {
    return sourceTypes;
  }

  public void setSalesReportType(int salesReportType)
  {
    this.salesReportType = salesReportType;
  }
  public String getSalesReportType(int salesReportType)
  {
    String result = "";

    if(this.salesReportType == salesReportType)
    {
      result = "selected";
    }

    return result;
  }
  public int getSalesReportType()
  {
    return this.salesReportType;
  }

  public int getStatusTypeFilter()
  {
    return statusTypeFilter;
  }
  public void setStatusTypeFilter(int newStatusTypeFilter)
  {
    statusTypeFilter = newStatusTypeFilter;
  }
  
  public String getLoginFilter()
  {
    return loginFilter;
  }
  public void setLoginFilter(String loginFilter)
  {
    this.loginFilter = loginFilter;
  }

  public String getNonactivatedOnly()
  {
    return (nonactivatedOnly ? "y" : "n");
  }
  public void setNonactivatedOnly(String value)
  {
    nonactivatedOnly = value.toLowerCase().equals("y");
  }
}/*@lineinfo:generated-code*/