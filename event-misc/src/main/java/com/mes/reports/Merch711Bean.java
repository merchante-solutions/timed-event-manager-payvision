/*@lineinfo:filename=Merch711Bean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/Merch711Bean.sqlj $

  Description:

    Merch711Bean

    Looks up 711 report information.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-11-14 11:12:34 -0800 (Fri, 14 Nov 2008) $
  Version            : $Revision: 15530 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.reports;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeMap;
import com.mes.forms.ButtonField;
import com.mes.forms.ComboDateField;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.ZipField;
import com.mes.support.DateTimeFormatter;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class Merch711Bean extends FieldBean
{
  /*
  ** public class ReportField
  **
  ** Represents a field in a report record.
  */
  public class ReportField
  {
    public String data;
  }

  /*
  ** public class ReportRecord
  **
  ** Represents a report row.
  */
  public class ReportRecord
  {
    Hashtable fields = new Hashtable();

    public ReportField addField(String fieldName)
    {
      ReportField newField = new ReportField();
      fields.put(fieldName,newField);
      return newField;
    }

    public ReportField getField(String fieldName)
    {
      return (ReportField)fields.get(fieldName);
    }

    public String getData(String fieldName)
    {
      return (String)(getField(fieldName).data);
    }

    public String setData(String fieldName, String fieldData)
    {
      ReportField field = getField(fieldName);
      if (field != null)
      {
        field.data = fieldData;
        return fieldData;
      }

      return null;
    }
    
    public String toString()
    {
      StringBuffer sb = new StringBuffer("[ ");
      for (Iterator i = fields.keySet().iterator(); i.hasNext(); )
      {
        String fieldName = (String)i.next();
        String fieldData = (String)fields.get(fieldName);
        sb.append("( " + fieldName + ": " + fieldData + ") ");
      }
      sb.append("]");
      return sb.toString();
    }
  }

  /*
  ** public class RecordMap
  **
  ** Convenience Hashtable wrapper class, has a get that casts type as
  ** ReportRecord.
  */
  public class RecordMap extends TreeMap
  {
    public ReportRecord getRec(Object key)
    {
      return (ReportRecord)super.get(key);
    }
  }
                                              
  private ComboDateField reportDate;
  
  public Merch711Bean(UserBean user)
  {
    super(user);
    
    fields.add(new ComboDateField("date",false,false));
    fields.add(new HiddenField("merchant"));
    fields.add(new HiddenField("merchDba"));
    fields.add(new HiddenField("name"));
    fields.add(new HiddenField("address1"));
    fields.add(new HiddenField("address2"));
    fields.add(new HiddenField("city"));
    fields.add(new HiddenField("state"));
    fields.add(new ZipField("zip",true,fields.getField("state")));
    fields.add(new HiddenField("sic"));
    fields.add(new HiddenField("assoc"));
    fields.add(new HiddenField("openDate"));
    fields.add(new HiddenField("lastDeposit"));
    fields.add(new ButtonField("select"));
    fields.setGroupHtmlExtra("class=\"formFields\"");

    // date should default to prior month
    Calendar cal = Calendar.getInstance();
    cal.setTime(((ComboDateField)fields.getField("date")).getUtilDate());
    cal.add(Calendar.MONTH,-1);
    reportDate = (ComboDateField)fields.getField("date");
    reportDate.setUtilDate(cal.getTime());
  }
  
  private java.sql.Date activeDate;
  private java.sql.Date yearBegin;
  private String rollupAmountFlag;
  private String rollupCountFlag;
  private long merchNum;
  
  /*
  ** public class AcctProfRecord
  **
  ** ReportRecord extension for account profitability report section.
  */
  public class AcctProfRecord extends ReportRecord
  {
    public AcctProfRecord()
    {
      addField("units");
      setData("units","0");
      addField("volume");
      setData("volume","0");
      addField("expense");
      setData("expense","0");
      addField("income");
      setData("income","0");
      addField("ytd");
      setData("ytd","0");
    }
  }

  public RecordMap apRecs = new RecordMap();
  {
    apRecs.put("discount",    new AcctProfRecord());
    apRecs.put("sales",       new AcctProfRecord());
    apRecs.put("credits",     new AcctProfRecord());
    apRecs.put("chargebacks", new AcctProfRecord());
    apRecs.put("totalIC",     new AcctProfRecord());
    apRecs.put("totalFee",    new AcctProfRecord());
    apRecs.put("header",      new AcctProfRecord());
    apRecs.put("capture",     new AcctProfRecord());
    apRecs.put("imprinter",   new AcctProfRecord());
    apRecs.put("terminal",    new AcctProfRecord());
    apRecs.put("membership",  new AcctProfRecord());
    apRecs.put("misc",        new AcctProfRecord());
    apRecs.put("auths",       new AcctProfRecord());
    apRecs.put("duesAsmt",    new AcctProfRecord());
    apRecs.put("total",       new AcctProfRecord());
  }
  
  /*
  ** private void loadAccountProfitability()
  **
  ** Loads account profitability section data.
  */
  private void loadAccountProfitability()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      // ytd
      /*@lineinfo:generated-code*//*@lineinfo:216^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(t1_tot_amount_of_sales)       ytd_sales,
//                  sum(t1_tot_amount_of_credits)     ytd_credits,
//                  sum(t1_tot_amount_of_chargeback)  ytd_chargebacks
//                  
//          from    monthly_extract_gn gn
//          
//          where   gn.hh_merchant_number = :merchNum
//                  and gn.hh_active_date between :yearBegin and :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(t1_tot_amount_of_sales)       ytd_sales,\n                sum(t1_tot_amount_of_credits)     ytd_credits,\n                sum(t1_tot_amount_of_chargeback)  ytd_chargebacks\n                \n        from    monthly_extract_gn gn\n        \n        where   gn.hh_merchant_number =  :1 \n                and gn.hh_active_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,yearBegin);
   __sJT_st.setDate(3,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^7*/
      rs = it.getResultSet();
      
      double ytdSalesAmount   = 0;
      double ytdCreditsAmount = 0;
      double ytdCbAmount      = 0;
      
      if (rs.next())
      {
        ytdSalesAmount    = rs.getDouble("ytd_sales");
        ytdCreditsAmount  = rs.getDouble("ytd_credits");
        ytdCbAmount       = rs.getDouble("ytd_chargebacks");
      }
        
      try
      {
        rs.close();
        it.close();
      }
      catch (Exception e) {}
        
      /*@lineinfo:generated-code*//*@lineinfo:247^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  hh_dba_name,
//                  t1_tot_calculated_discount,
//                  t1_tot_number_of_sales,
//                  t1_tot_amount_of_sales,
//                  t1_tot_exp_ind_plans,
//                  t1_tot_inc_ind_plans,
//                  t1_tot_number_of_credits,
//                  t1_tot_amount_of_credits,
//                  t1_tot_inc_interchange,
//                  t1_tot_exp_capture,
//                  t1_tot_inc_capture,
//                  t1_tot_number_of_chargeback,
//                  t1_tot_amount_of_chargeback,
//                  s1_num_imprinters,
//                  s1_imprinter_income_fee,
//                  s1_num_terminals,
//                  s1_pos_terminal_income,
//                  s1_num_membership_fee_charges,
//                  s1_chargeback_fee_income,
//                  s1_membership_income,
//                  ( s1_misc_income1 
//                    + s1_misc_income2
//                    + s1_misc_income3
//                    + s1_misc_income4
//                    + s1_misc_income5
//                    + s1_misc_income6 ) misc_income,
//                  s2_num_misc_fee_charges,
//                  t1_tot_exp_authorization,
//                  t1_tot_inc_authorization,
//                  g2_intch_net_gross_f$,
//                  g2_intch_net_gross_f#,
//                  t1_tot_income,
//                  t1_tot_expense,
//                  c1_num_edc_transmittals,
//                  c1_num_edc_items_captured,
//                  c1_inc_edc_transmittals,
//                  c1_inc_edc_items_captured,
//                  name1_line_1,
//                  addr1_line_1,
//                  addr1_line_2,
//                  city1_line_4,
//                  state1_line_4,
//                  zip1_line_4,
//                  sic_code,
//                  dmagent,
//                  mmddyy_to_date(date_opened) open_date,
//                  last_deposit_date
//                  
//          from    monthly_extract_gn gn,
//                  monthly_extract_c1 c1,
//                  mif
//          
//          where   gn.hh_merchant_number = :merchNum
//                  and gn.hh_active_date = :activeDate
//                  and gn.hh_load_sec = c1.hh_load_sec(+)
//                  and gn.hh_merchant_number = mif.merchant_number(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  hh_dba_name,\n                t1_tot_calculated_discount,\n                t1_tot_number_of_sales,\n                t1_tot_amount_of_sales,\n                t1_tot_exp_ind_plans,\n                t1_tot_inc_ind_plans,\n                t1_tot_number_of_credits,\n                t1_tot_amount_of_credits,\n                t1_tot_inc_interchange,\n                t1_tot_exp_capture,\n                t1_tot_inc_capture,\n                t1_tot_number_of_chargeback,\n                t1_tot_amount_of_chargeback,\n                s1_num_imprinters,\n                s1_imprinter_income_fee,\n                s1_num_terminals,\n                s1_pos_terminal_income,\n                s1_num_membership_fee_charges,\n                s1_chargeback_fee_income,\n                s1_membership_income,\n                ( s1_misc_income1 \n                  + s1_misc_income2\n                  + s1_misc_income3\n                  + s1_misc_income4\n                  + s1_misc_income5\n                  + s1_misc_income6 ) misc_income,\n                s2_num_misc_fee_charges,\n                t1_tot_exp_authorization,\n                t1_tot_inc_authorization,\n                g2_intch_net_gross_f$,\n                g2_intch_net_gross_f#,\n                t1_tot_income,\n                t1_tot_expense,\n                c1_num_edc_transmittals,\n                c1_num_edc_items_captured,\n                c1_inc_edc_transmittals,\n                c1_inc_edc_items_captured,\n                name1_line_1,\n                addr1_line_1,\n                addr1_line_2,\n                city1_line_4,\n                state1_line_4,\n                zip1_line_4,\n                sic_code,\n                dmagent,\n                mmddyy_to_date(date_opened) open_date,\n                last_deposit_date\n                \n        from    monthly_extract_gn gn,\n                monthly_extract_c1 c1,\n                mif\n        \n        where   gn.hh_merchant_number =  :1 \n                and gn.hh_active_date =  :2 \n                and gn.hh_load_sec = c1.hh_load_sec(+)\n                and gn.hh_merchant_number = mif.merchant_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:305^7*/
      rs = it.getResultSet();
      
      if (rs.next())
      {
        fields.setData("merchDba",    rs.getString("hh_dba_name"));
        fields.setData("name",        rs.getString("name1_line_1"));
        fields.setData("address1",    rs.getString("addr1_line_1"));
        fields.setData("address2",    rs.getString("addr1_line_2"));
        fields.setData("city",        rs.getString("city1_line_4"));
        fields.setData("state",       rs.getString("state1_line_4"));
        fields.setData("zip",         rs.getString("zip1_line_4"));
        fields.setData("sic",         rs.getString("sic_code"));
        fields.setData("assoc",       rs.getString("dmagent"));
        fields.setData("openDate",
          DateTimeFormatter.getFormattedDate(rs.getDate("open_date"), 
            "MM/dd/yy"));
        fields.setData("lastDeposit", 
          DateTimeFormatter.getFormattedDate(rs.getDate("last_deposit_date"), 
            "MM/dd/yy"));
        
        String rollupAmountFlag   = rs.getString("g2_intch_net_gross_f$");
        String rollupCountFlag    = rs.getString("g2_intch_net_gross_f#");

        int     salesCount        = rs.getInt("t1_tot_number_of_sales");
        double  salesAmount       = rs.getDouble("t1_tot_amount_of_sales");
        int     creditCount       = rs.getInt("t1_tot_number_of_credits");
        double  creditAmount      = rs.getDouble("t1_tot_amount_of_credits");
        int     cbCount           = rs.getInt("t1_tot_number_of_chargeback");
        double  cbAmount          = rs.getDouble("t1_tot_amount_of_chargeback");
        
        int     discountCount     = salesCount;
        double  discountAmount    = salesAmount;
        double  ytdDiscountAmount = ytdSalesAmount;
        
        double  totalAmount       = salesAmount - creditAmount - cbAmount;
        double  ytdTotalAmount    = ytdSalesAmount - ytdCreditsAmount - ytdCbAmount;
        
        // account profitability
        ReportRecord rec = apRecs.getRec("discount");
        rec.setData("units",    Integer.toString(discountCount));
        rec.setData("volume",   Double.toString(discountAmount));
        rec.setData("income",   rs.getString("t1_tot_calculated_discount"));
        rec.setData("ytd",      Double.toString(ytdDiscountAmount));
        
        rec = apRecs.getRec("sales");
        rec.setData("units",    rs.getString("t1_tot_number_of_sales"));
        rec.setData("volume",   rs.getString("t1_tot_amount_of_sales"));
        rec.setData("expense",  rs.getString("t1_tot_exp_ind_plans"));
        rec.setData("income",   rs.getString("t1_tot_inc_ind_plans"));
        rec.setData("ytd",      Double.toString(ytdSalesAmount));
        
        rec = apRecs.getRec("credits");
        rec.setData("units",    rs.getString("t1_tot_number_of_credits"));
        rec.setData("volume",   rs.getString("t1_tot_amount_of_credits"));
        rec.setData("ytd",      Double.toString(ytdCreditsAmount));
        
        rec = apRecs.getRec("chargebacks");
        rec.setData("units",    rs.getString("t1_tot_number_of_chargeback"));
        rec.setData("volume",   rs.getString("t1_tot_amount_of_chargeback"));
        rec.setData("income",   rs.getString("s1_chargeback_fee_income"));
        rec.setData("ytd",      Double.toString(ytdCbAmount));
        
        rec = apRecs.getRec("totalIC");
        rec.setData("income",   rs.getString("t1_tot_inc_interchange"));

        rec = apRecs.getRec("header");
        rec.setData("units",    rs.getString("c1_num_edc_transmittals"));
        rec.setData("income",   rs.getString("c1_inc_edc_transmittals"));
        
        rec = apRecs.getRec("capture");
        rec.setData("units",    rs.getString("c1_num_edc_items_captured"));
        rec.setData("income",   rs.getString("c1_inc_edc_items_captured"));
        
        rec = apRecs.getRec("imprinter");
        rec.setData("units",    rs.getString("s1_num_imprinters"));
        rec.setData("income",   rs.getString("s1_imprinter_income_fee"));
        
        rec = apRecs.getRec("terminal");
        rec.setData("units",    rs.getString("s1_num_terminals"));
        rec.setData("income",   rs.getString("s1_pos_terminal_income"));
        
        rec = apRecs.getRec("membership");
        rec.setData("units",    rs.getString("s1_num_membership_fee_charges"));
        rec.setData("income",   rs.getString("s1_membership_income"));
        
        rec = apRecs.getRec("misc");
        rec.setData("units",    rs.getString("s2_num_misc_fee_charges"));
        rec.setData("income",   rs.getString("misc_income"));
        
        rec = apRecs.getRec("auths");
        rec.setData("expense",  rs.getString("t1_tot_exp_authorization"));
        rec.setData("income",   rs.getString("t1_tot_inc_authorization"));
        
        rec = apRecs.getRec("total");
        rec.setData("units",    "0");
        rec.setData("volume",   Double.toString(totalAmount));
        rec.setData("expense",  rs.getString("t1_tot_expense"));
        rec.setData("income",   rs.getString("t1_tot_income"));
        rec.setData("ytd",      Double.toString(ytdTotalAmount));
      }
    }
    catch (Exception e)
    {
      logEntry("loadAccountProfitability()", e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
    }
  }
  
  /*
  ** public class AuthActivityRecord
  **
  ** ReportRecord extension for account authorization activity report section.
  */
  public class AuthActivityRecord extends ReportRecord
  {
    public AuthActivityRecord()
    {
      addField("description");
      setData("description","0");
      addField("units");
      setData("units","0");
      addField("local");
      setData("local","0");
      addField("intra");
      setData("intra","0");
      addField("inter");
      setData("inter","0");
      addField("fixed");
      setData("fixed","0");
      addField("other");
      setData("other","0");
      addField("ac950");
      setData("ac950","0");
      addField("surchg");
      setData("surchg","0");
    }
  }

  public RecordMap aaRecs = new RecordMap();

  /*
  ** private void loadAuthorizationActivity()
  **
  ** Loads authorization activity section data.
  */
  private void loadAuthorizationActivity()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
      
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:462^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    mt.description,
//                    ap.a1_media_type,
//                    sum(ap.a1_interstate_number1)   inter,
//                    sum(ap.a1_interstate_number2)   intra,
//                    sum(ap.a1_local_number)         local,
//                    sum(ap.a1_950_number)           ac950,
//                    sum(ap.a1_other_number)         other,
//                    sum(ap.a1_number_fixed_auths)   fixed,
//                    sum(ap.a1_avs_number)           surchg,
//                    sum(ap.auth_count_total)        units
//                        
//          from      monthly_extract_ap ap,
//                    monthly_extract_media_types mt,
//                    monthly_extract_gn gn
//                    
//          where     gn.hh_merchant_number = :merchNum
//                    and gn.hh_active_date = :activeDate
//                    and gn.hh_load_sec = ap.hh_load_sec(+)
//                    and ap.a1_media_type = mt.type(+)
//                    
//          group by  mt.description,
//                    ap.a1_media_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mt.description,\n                  ap.a1_media_type,\n                  sum(ap.a1_interstate_number1)   inter,\n                  sum(ap.a1_interstate_number2)   intra,\n                  sum(ap.a1_local_number)         local,\n                  sum(ap.a1_950_number)           ac950,\n                  sum(ap.a1_other_number)         other,\n                  sum(ap.a1_number_fixed_auths)   fixed,\n                  sum(ap.a1_avs_number)           surchg,\n                  sum(ap.auth_count_total)        units\n                      \n        from      monthly_extract_ap ap,\n                  monthly_extract_media_types mt,\n                  monthly_extract_gn gn\n                  \n        where     gn.hh_merchant_number =  :1 \n                  and gn.hh_active_date =  :2 \n                  and gn.hh_load_sec = ap.hh_load_sec(+)\n                  and ap.a1_media_type = mt.type(+)\n                  \n        group by  mt.description,\n                  ap.a1_media_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:486^7*/
      rs = it.getResultSet();
      
      while (rs.next())
      {
        ReportRecord rec = new AuthActivityRecord();
        String key = rs.getString("description");
        if (key != null && key.length() > 0)
        {
          rec.setData("description",key);
          rec.setData("units",rs.getString("units"));
          rec.setData("local",rs.getString("local"));
          rec.setData("intra",rs.getString("intra"));
          rec.setData("inter",rs.getString("inter"));
          rec.setData("fixed",rs.getString("fixed"));
          rec.setData("other",rs.getString("other"));
          rec.setData("ac950",rs.getString("ac950"));
          rec.setData("surchg",rs.getString("surchg"));
          aaRecs.put(key,rec);
        }
      }
      
      try
      {
        rs.close();
        it.close();
      }
      catch (Exception e) {}
      
      // total
      /*@lineinfo:generated-code*//*@lineinfo:516^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    sum(ap.a1_interstate_number1)   inter,
//                    sum(ap.a1_interstate_number2)   intra,
//                    sum(ap.a1_local_number)         local,
//                    sum(ap.a1_950_number)           ac950,
//                    sum(ap.a1_other_number)         other,
//                    sum(ap.a1_number_fixed_auths)   fixed,
//                    sum(ap.a1_avs_number)           surchg,
//                    sum(ap.auth_count_total)        units
//                        
//          from      monthly_extract_ap ap,
//                    monthly_extract_gn gn
//                    
//          where     gn.hh_merchant_number = :merchNum
//                    and gn.hh_active_date = :activeDate
//                    and gn.hh_load_sec = ap.hh_load_sec(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sum(ap.a1_interstate_number1)   inter,\n                  sum(ap.a1_interstate_number2)   intra,\n                  sum(ap.a1_local_number)         local,\n                  sum(ap.a1_950_number)           ac950,\n                  sum(ap.a1_other_number)         other,\n                  sum(ap.a1_number_fixed_auths)   fixed,\n                  sum(ap.a1_avs_number)           surchg,\n                  sum(ap.auth_count_total)        units\n                      \n        from      monthly_extract_ap ap,\n                  monthly_extract_gn gn\n                  \n        where     gn.hh_merchant_number =  :1 \n                  and gn.hh_active_date =  :2 \n                  and gn.hh_load_sec = ap.hh_load_sec(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:533^7*/
      rs = it.getResultSet();
      
      if (rs.next())
      {
        ReportRecord rec = new AuthActivityRecord();
        rec.setData("description","total");
        rec.setData("units",rs.getString("units"));
        rec.setData("local",rs.getString("local"));
        rec.setData("intra",rs.getString("intra"));
        rec.setData("inter",rs.getString("inter"));
        rec.setData("fixed",rs.getString("fixed"));
        rec.setData("other",rs.getString("other"));
        rec.setData("ac950",rs.getString("ac950"));
        rec.setData("surchg",rs.getString("surchg"));
        aaRecs.put("total",rec);

        rec = apRecs.getRec("auths");
        rec.setData("units",rs.getString("units"));
      }
      
      try
      {
        rs.close();
        it.close();
      }
      catch (Exception e) {}
      
      // ytd
      /*@lineinfo:generated-code*//*@lineinfo:562^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    sum(ap.a1_interstate_number2)   intra,
//                    sum(ap.a1_interstate_number1)   inter,
//                    sum(ap.a1_local_number)         local,
//                    sum(ap.a1_950_number)           ac950,
//                    sum(ap.a1_other_number)         other,
//                    sum(ap.a1_number_fixed_auths)   fixed,
//                    sum(ap.a1_avs_number)           surchg,
//                    sum(ap.auth_count_total)        units
//                        
//          from      monthly_extract_ap ap,
//                    monthly_extract_gn gn
//                    
//          where     gn.hh_merchant_number = :merchNum
//                    and gn.hh_active_date between :yearBegin and :activeDate
//                    and gn.hh_load_sec = ap.hh_load_sec(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sum(ap.a1_interstate_number2)   intra,\n                  sum(ap.a1_interstate_number1)   inter,\n                  sum(ap.a1_local_number)         local,\n                  sum(ap.a1_950_number)           ac950,\n                  sum(ap.a1_other_number)         other,\n                  sum(ap.a1_number_fixed_auths)   fixed,\n                  sum(ap.a1_avs_number)           surchg,\n                  sum(ap.auth_count_total)        units\n                      \n        from      monthly_extract_ap ap,\n                  monthly_extract_gn gn\n                  \n        where     gn.hh_merchant_number =  :1 \n                  and gn.hh_active_date between  :2  and  :3 \n                  and gn.hh_load_sec = ap.hh_load_sec(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,yearBegin);
   __sJT_st.setDate(3,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:579^7*/
      rs = it.getResultSet();
      
      if (rs.next())
      {
        ReportRecord rec = new AuthActivityRecord();
        rec.setData("description","ytd");
        rec.setData("units",rs.getString("units"));
        rec.setData("local",rs.getString("local"));
        rec.setData("intra",rs.getString("intra"));
        rec.setData("inter",rs.getString("inter"));
        rec.setData("fixed",rs.getString("fixed"));
        rec.setData("other",rs.getString("other"));
        rec.setData("ac950",rs.getString("ac950"));
        rec.setData("surchg",rs.getString("surchg"));
        aaRecs.put("ytd",rec);
      }
    }
    catch (Exception e)
    {
      logEntry("loadAuthorizationActivity()", e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
    }
  }
  
  /*
  ** public class ChargebackRecord
  **
  ** ReportRecord extension for chargeback report section.
  */
  public class ChargebackRecord extends ReportRecord
  {
    public ChargebackRecord()
    {
      addField("debitCnt");
      setData("debitCnt","0");
      addField("debitAmt");
      setData("debitAmt","0");
      addField("creditCnt");
      setData("creditCnt","0");
      addField("creditAmt");
      setData("creditAmt","0");
      addField("reversalCnt");
      setData("reversalCnt","0");
      addField("reversalAmt");
      setData("reversalAmt","0");
    }
  }

  public RecordMap cbRecs = new RecordMap();
  {
    cbRecs.put("total", new ChargebackRecord());
    cbRecs.put("ytd",   new ChargebackRecord());
  }

  /*
  ** private void loadChargebacks()
  **
  ** Loads chargebacks section data.
  */
  private void loadChargebacks()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
      
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:650^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  t1_tot_number_of_chargeback,
//                  t1_tot_amount_of_chargeback
//          from    monthly_extract_gn
//          where   hh_merchant_number = :merchNum
//                  and hh_active_date = :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  t1_tot_number_of_chargeback,\n                t1_tot_amount_of_chargeback\n        from    monthly_extract_gn\n        where   hh_merchant_number =  :1 \n                and hh_active_date =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:657^7*/
      rs = it.getResultSet();
      
      ReportRecord rec = cbRecs.getRec("total");
      if (rs.next())
      {
        rec.setData("debitCnt",     rs.getString("t1_tot_number_of_chargeback"));
        rec.setData("debitAmt",     rs.getString("t1_tot_amount_of_chargeback"));
        rec.setData("creditCnt",    "0");
        rec.setData("creditAmt",    "0");
        rec.setData("reversalCnt",  "0");
        rec.setData("reversalAmt",  "0");
      }
      
      try
      {
        rs.close();
        it.close();
      }
      catch (Exception e) {}
      
      /*@lineinfo:generated-code*//*@lineinfo:678^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(t1_tot_number_of_chargeback)  ytd_count,
//                  sum(t1_tot_amount_of_chargeback)  ytd_amount
//          from    monthly_extract_gn
//          where   hh_merchant_number = :merchNum
//                  and hh_active_date between :yearBegin and :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(t1_tot_number_of_chargeback)  ytd_count,\n                sum(t1_tot_amount_of_chargeback)  ytd_amount\n        from    monthly_extract_gn\n        where   hh_merchant_number =  :1 \n                and hh_active_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,yearBegin);
   __sJT_st.setDate(3,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:685^7*/
      rs = it.getResultSet();
      
      rec = cbRecs.getRec("ytd");
      if (rs.next())
      {
        rec.setData("debitCnt",     rs.getString("ytd_count"));
        rec.setData("debitAmt",     rs.getString("ytd_amount"));
      }
    }
    catch (Exception e)
    {
      logEntry("loadChargebacks()", e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
    }
  }
  
  /*
  ** public class PlanActivityRecord
  **
  ** ReportRecord extension for plan activity report section.
  */
  public class PlanActivityRecord extends ReportRecord
  {
    public PlanActivityRecord()
    {
      addField("type");
      setData("type","0");
      addField("units");
      setData("units","0");
      addField("volume");
      setData("volume","0");
      addField("discount");
      setData("discount","0");
      addField("rate");
      addField("avgTicket");
      setData("avgTicket","0");
      addField("perUnit");
      setData("perUnit","0");
    }
  }

  public RecordMap planRecs = new RecordMap();

  /*
  ** private void loadPlanActivity()
  **
  ** Loads plan activity section data.
  */
  private void loadPlanActivity()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
      
    try
    {
      // plan activity
      /*@lineinfo:generated-code*//*@lineinfo:746^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pl.pl_plan_type,
//                  pl.pl_number_of_sales,
//                  pl.pl_sales_amount,
//                  ( pl.pl_sales_amount
//                    - pl.pl_credits_amount)   pl_net_amount,
//                  pl.pl_correct_disc_amt,
//                  pl.pl_disc_rate,
//                  pl.pl_disc_rate_per_item
//          from    monthly_extract_pl pl,
//                  monthly_extract_gn gn
//          where   gn.hh_merchant_number = :merchNum
//                  and gn.hh_active_date = :activeDate
//                  and gn.hh_load_sec = pl.hh_load_sec(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pl.pl_plan_type,\n                pl.pl_number_of_sales,\n                pl.pl_sales_amount,\n                ( pl.pl_sales_amount\n                  - pl.pl_credits_amount)   pl_net_amount,\n                pl.pl_correct_disc_amt,\n                pl.pl_disc_rate,\n                pl.pl_disc_rate_per_item\n        from    monthly_extract_pl pl,\n                monthly_extract_gn gn\n        where   gn.hh_merchant_number =  :1 \n                and gn.hh_active_date =  :2 \n                and gn.hh_load_sec = pl.hh_load_sec(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:761^7*/
      rs = it.getResultSet();
      
      int     planCount           = 0;
      int     planUnitTotal       = 0;
      double  planVolumeTotal     = 0;
      double  planDiscountTotal   = 0;
      double  planNetVolumeTotal  = 0;
      
      while (rs.next())
      {
        String  planType  = rs.getString("pl_plan_type");
        int     salesCnt  = rs.getInt("pl_number_of_sales");
        double  salesAmt  = rs.getDouble("pl_sales_amount");
        double  netAmt    = rs.getDouble("pl_net_amount");
        double  discAmt   = rs.getDouble("pl_correct_disc_amt");
        double  discRate  = rs.getDouble("pl_disc_rate");
        double  avgTicket = 0;
        double  perUnit   = 0;
        if (salesCnt != 0 && salesAmt != 0)
        {
          avgTicket = salesAmt / salesCnt;
        }
        if (salesCnt != 0 && discAmt != 0)
        {
          perUnit = discAmt / salesCnt;
        }
        
        ReportRecord rec = new PlanActivityRecord();
        try
        {
          rec.setData("type",       planType);
          rec.setData("units",      Integer.toString(salesCnt));
          rec.setData("volume",     Double.toString(salesAmt));
          rec.setData("discount",   Double.toString(discAmt));
          rec.setData("rate",       Double.toString(discRate));
          rec.setData("avgTicket",  Double.toString(avgTicket));
          rec.setData("perUnit",    Double.toString(perUnit));
        }
        catch (Exception e)
        {
          logEntry("loadPlanActivity(parsing plan record data)", e.toString());
        }
        
        planRecs.put(planType,rec);
        
        planUnitTotal += salesCnt;
        planVolumeTotal += salesAmt;
        planNetVolumeTotal += netAmt;
        if (discAmt > 0)
        {
          ++planCount;
          planDiscountTotal += discAmt;
        }
      }
      
      // plan activity totals
      ReportRecord rec = new PlanActivityRecord();
      planRecs.put("total",rec);
      
      rec.setData("units",  Integer.toString(planUnitTotal));
      rec.setData("volume", Double.toString(planVolumeTotal));
      rec.setData("discount",Double.toString(planDiscountTotal));
      
      if (planUnitTotal > 0)
      {
        rec.setData("avgTicket",  Double.toString(planVolumeTotal / planUnitTotal));
      }
      else
      {
        rec.setData("avgTicket","0");
      }
      
      // remove rate totalling by request
      /*
      if (planVolumeTotal > 0)
      {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(3);
        rec.setData("rate",nf.format((planDiscountTotal / planNetVolumeTotal) * 100));
      }
      else
      {
        rec.setData("rate","0");
      }
      */
    
      try
      {
        rs.close();
        it.close();
      }
      catch (Exception e) {}

      // plan activity ytd
      /*@lineinfo:generated-code*//*@lineinfo:856^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(pl.pl_number_of_sales)  ytd_sales_unit,
//                  sum(pl.pl_sales_amount)     ytd_sales_amount,
//                  sum(pl.pl_sales_amount
//                      - pl.pl_credits_amount) ytd_net_amount,
//                  sum(pl.pl_correct_disc_amt) ytd_disc_amount
//          from    monthly_extract_pl pl,
//                  monthly_extract_gn gn
//          where   gn.hh_merchant_number = :merchNum
//                  and gn.hh_active_date between :yearBegin and :activeDate
//                  and gn.hh_load_sec = pl.hh_load_sec(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(pl.pl_number_of_sales)  ytd_sales_unit,\n                sum(pl.pl_sales_amount)     ytd_sales_amount,\n                sum(pl.pl_sales_amount\n                    - pl.pl_credits_amount) ytd_net_amount,\n                sum(pl.pl_correct_disc_amt) ytd_disc_amount\n        from    monthly_extract_pl pl,\n                monthly_extract_gn gn\n        where   gn.hh_merchant_number =  :1 \n                and gn.hh_active_date between  :2  and  :3 \n                and gn.hh_load_sec = pl.hh_load_sec(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,yearBegin);
   __sJT_st.setDate(3,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:868^7*/
      rs = it.getResultSet();
      
      ReportRecord ytdRec  = new PlanActivityRecord();
      planRecs.put("ytd",ytdRec);
      if (rs.next())
      {
        ytdRec.setData("units",rs.getString("ytd_sales_unit"));
        ytdRec.setData("volume",rs.getString("ytd_sales_amount"));
        ytdRec.setData("discount",rs.getString("ytd_disc_amount"));
        
        int     planYtdUnits        = rs.getInt("ytd_sales_unit");
        double  planYtdVolume       = rs.getDouble("ytd_sales_amount");
        double  planYtdDiscount     = rs.getDouble("ytd_disc_amount");
        double  planYtdNetVolume    = rs.getDouble("ytd_net_amount");
        
        if (planYtdUnits > 0)
        {
          ytdRec.setData("avgTicket",Double.toString(planYtdVolume / planYtdUnits));
        }
        else
        {
          ytdRec.setData("avgTicket","0");
        }
      
        // remove rate totalling by request
        /*
        if (planYtdVolume > 0)
        {
          NumberFormat nf = NumberFormat.getInstance();
          nf.setMaximumFractionDigits(3);
          ytdRec.setData("rate",nf.format((planYtdDiscount / planYtdNetVolume) * 100));
        }
        else
        {
          ytdRec.setData("rate","0");
        }
        */
      }
    }
    catch (Exception e)
    {
      logEntry("loadPlanActivity()", e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
    }
  }
  
  /*
  ** public class OutgoingInterchangeRecord
  **
  ** ReportRecord extension for outgoing interchange report section.
  */
  public class OutgoingInterchangeRecord extends ReportRecord
  {
    public OutgoingInterchangeRecord()
    {
      addField("plan");
      setData("plan","0");
      addField("type");
      setData("type","0");
      addField("units");
      setData("units","0");
      addField("amount");
      setData("amount","0.0");
      addField("fees");
      setData("fees","0.0");
    }
  }

  public RecordMap visaOutRecs = new RecordMap();
  public RecordMap mcOutRecs   = new RecordMap();
  
  /*
  ** private void loadOutgoingInterchange()
  **
  ** Loads outgoing interchange section data.
  */
  private void loadOutgoingInterchange()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
      
    try
    {
      // reset outgoing interchange hashes
      visaOutRecs = new RecordMap();
      mcOutRecs   = new RecordMap();
      
      // load category descriptors
      Hashtable visaCats  = new Hashtable();
      Hashtable mcCats    = new Hashtable();
      /*@lineinfo:generated-code*//*@lineinfo:963^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  card_type,
//                  category,
//                  description
//          from    ic_category_desc
//          where   card_type in ( 'MC', 'VS' )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  card_type,\n                category,\n                description\n        from    ic_category_desc\n        where   card_type in ( 'MC', 'VS' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.Merch711Bean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:970^7*/
      rs = it.getResultSet();
      while (rs.next())
      {
        String  category    = rs.getString("category");
        String  description = rs.getString("description");
        if (rs.getString("card_type").equals("VS"))
        {
          visaCats.put(category,description);
        }
        else
        {
          mcCats.put(category,description);
        }
      }
      
      try
      {
        rs.close();
        it.close();
      }
      catch (Exception e) {}
      
      // visa categories
      /*@lineinfo:generated-code*//*@lineinfo:994^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    monthly_extract_visa  vs,
//                  monthly_extract_mc    mc,
//                  monthly_extract_gn    gn
//          where   gn.hh_merchant_number = :merchNum
//                  and gn.hh_active_date = :activeDate
//                  and gn.hh_load_sec = vs.hh_load_sec(+)
//                  and gn.hh_load_sec = mc.hh_load_sec(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    monthly_extract_visa  vs,\n                monthly_extract_mc    mc,\n                monthly_extract_gn    gn\n        where   gn.hh_merchant_number =  :1 \n                and gn.hh_active_date =  :2 \n                and gn.hh_load_sec = vs.hh_load_sec(+)\n                and gn.hh_load_sec = mc.hh_load_sec(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1004^7*/
      
      // extract visa category data
      rs = it.getResultSet();
      if (rs.next())
      {
        ResultSetMetaData md = rs.getMetaData();
        for (int i = 1; i <= md.getColumnCount(); ++i)
        {
          // see if this column contains interchange fee data
          String column   = md.getColumnName(i);
          boolean isVisa  = column.charAt(0) == 'V';
          boolean isMc    = column.charAt(0) == 'M';
          ReportRecord outRec = null;
          if ((isVisa || isMc) && column.indexOf("_CAT_") == 2)
          {
            // make sure a record for this category exists
            String category = 
              Integer.toString(Integer.parseInt(column.substring(7,9)));
            String planName;
            if (isVisa)
            {
              planName  = (String)visaCats.get(category);
              outRec    = (ReportRecord)visaOutRecs.get(planName);
              if (outRec == null)
              {
                outRec = new OutgoingInterchangeRecord();
                outRec.setData("plan",planName);
                outRec.setData("type","V");
                visaOutRecs.put(planName,outRec);
              }
            }
            else
            {
              planName  = (String)mcCats.get(category);
              outRec    = (ReportRecord)mcOutRecs.get(planName);
              if (outRec == null)
              {
                outRec = new OutgoingInterchangeRecord();
                outRec.setData("plan",planName);
                outRec.setData("type","M");
                mcOutRecs.put(planName,outRec);
              }
            }
            
            // interchange fees
            if (column.endsWith("INTCH"))
            {
              double fees = rs.getDouble(i);
              outRec.setData("fees",Double.toString(fees));
              
            }
            // sales/credits transaction counts
            else if (column.endsWith("SALES_COUNT"))
            {
              int units = 0;
              try
              {
                units = Integer.parseInt(outRec.getData("units"));
              } catch (Exception e) {}
                
              units += rs.getInt(i);
              outRec.setData("units",Integer.toString(units));
            }
            // sales/credits transaction amounts
            else if (column.endsWith("SALES_AMOUNT"))
            {
              double amount = 0;
              try
              {
                amount = Double.parseDouble(outRec.getData("amount"));
              } catch (Exception e) {}

              amount += rs.getDouble(i);
              outRec.setData("amount",Double.toString(amount));
            }
          }
        }
      }

      // remove any empty visa records
      for (Iterator i = visaOutRecs.keySet().iterator(); i.hasNext(); )
      {
        String key = (String)i.next();

        ReportRecord rec = (ReportRecord)visaOutRecs.getRec(key);
        boolean isEmpty = true;
        try
        {
          if (!rec.getData("units").equals("0")
              || !rec.getData("amount").equals("0.0")
              || !rec.getData("fees").equals("0.0"))
          {
            isEmpty = false;
          }
        }
        catch (Exception e)
        {
          logEntry("loadOutgoingInterchange(removing visa records)", e.toString());
        }
        
        if (isEmpty)
        {
          i.remove(); 
        }
      }
      
      // remove any empty mc records
      for (Iterator i = mcOutRecs.keySet().iterator(); i.hasNext(); )
      {
        String key = (String)i.next();
        ReportRecord rec = (ReportRecord)mcOutRecs.getRec(key);
        boolean isEmpty = true;
        try
        {
          if (!rec.getData("units").equals("0")
              || !rec.getData("amount").equals("0.0")
              || !rec.getData("fees").equals("0.0"))
          {
            isEmpty = false;
          }
        }
        catch (Exception e)
        {
          logEntry("loadOutgoingInterchange(removing mc records)", e.toString());
        }
        
        if (isEmpty)
        {
          i.remove(); 
        }
      }
      
      // get assessment expense and income
      double assessmentExpense  = rs.getDouble("v3_expense_assessment")
                                + rs.getDouble("m2_expense_assessment");
      double assessmentIncome   = rs.getDouble("v1_income_assessment")
                                + rs.getDouble("m1_income_assessment");
      ReportRecord rec = apRecs.getRec("duesAsmt");
      rec.setData("expense",Double.toString(assessmentExpense));
      rec.setData("income",Double.toString(assessmentIncome));

      // adjust interchange
      rec = apRecs.getRec("totalIC");
      double ticIncome = Double.parseDouble(rec.getData("income"));
      ticIncome -= assessmentIncome;
      rec.setData("income",Double.toString(ticIncome));
        
      try
      {
        rs.close();
        it.close();
      }
      catch (Exception e) {}

      // get total interchante counts and amounts
      /*@lineinfo:generated-code*//*@lineinfo:1160^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(vmc_vol_count)                      as total_ic_count,
//                  sum(vmc_sales_amount
//                      + vmc_credits_amount)               as total_ic_amount,
//                  sum( decode( sm.interchange_expense,
//                               0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),
//                               sm.interchange_expense ))  as total_ic_fees
//          from    monthly_extract_summary   sm
//          where   merchant_number = :merchNum
//                  and active_date = :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(vmc_vol_count)                      as total_ic_count,\n                sum(vmc_sales_amount\n                    + vmc_credits_amount)               as total_ic_amount,\n                sum( decode( sm.interchange_expense,\n                             0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),\n                             sm.interchange_expense ))  as total_ic_fees\n        from    monthly_extract_summary   sm\n        where   merchant_number =  :1 \n                and active_date =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1171^7*/
      rs = it.getResultSet();
      
      ReportRecord outRec = new OutgoingInterchangeRecord();
      if (rs.next())
      {
        outRec.setData("units",rs.getString("total_ic_count"));
        outRec.setData("amount",rs.getString("total_ic_amount"));
        outRec.setData("fees",rs.getString("total_ic_fees"));
        
        ReportRecord apRec = apRecs.getRec("totalIC");
        apRec.setData("units",rs.getString("total_ic_count"));
        apRec.setData("volume",rs.getString("total_ic_amount"));
      
        apRec = apRecs.getRec("totalFee");
        apRec.setData("units",rs.getString("total_ic_count"));
        apRec.setData("volume",rs.getString("total_ic_amount"));
        apRec.setData("expense",rs.getString("total_ic_fees"));
      }
      visaOutRecs.put("total",outRec);
      
      try
      {
        rs.close();
        it.close();
      }
      catch (Exception e) {}
      
      // get ytd total interchante counts and amounts
      /*@lineinfo:generated-code*//*@lineinfo:1200^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(vmc_vol_count)                      as ytd_ic_count,
//                  sum(vmc_sales_amount
//                      + vmc_credits_amount)               as ytd_ic_amount,
//                  sum( decode( sm.interchange_expense,
//                               0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),
//                               sm.interchange_expense ))  as ytd_ic_fees
//          from    monthly_extract_summary       sm
//          where   merchant_number = :merchNum
//                  and active_date between :yearBegin and :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(vmc_vol_count)                      as ytd_ic_count,\n                sum(vmc_sales_amount\n                    + vmc_credits_amount)               as ytd_ic_amount,\n                sum( decode( sm.interchange_expense,\n                             0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),\n                             sm.interchange_expense ))  as ytd_ic_fees\n        from    monthly_extract_summary       sm\n        where   merchant_number =  :1 \n                and active_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.Merch711Bean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setDate(2,yearBegin);
   __sJT_st.setDate(3,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.Merch711Bean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1211^7*/
      rs = it.getResultSet();
      
      outRec = new OutgoingInterchangeRecord();
      if (rs.next())
      {
        outRec.setData("units",rs.getString("ytd_ic_count"));
        outRec.setData("amount",rs.getString("ytd_ic_amount"));
        outRec.setData("fees",rs.getString("ytd_ic_fees"));

        ReportRecord apRec = apRecs.getRec("totalIC");
        apRec.setData("ytd",rs.getString("ytd_ic_amount"));
      
        apRec = apRecs.getRec("totalFee");
        apRec.setData("ytd",rs.getString("ytd_ic_amount"));
      }
      visaOutRecs.put("ytd",outRec);
    }
    catch (Exception e)
    {
      logEntry("loadOutgoingInterchange()", e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
    }
    
  }
  
  /*
  ** public void finishUp()
  **
  ** Fill in some blanks.
  */
  public void finishUp()
  {
    try
    {
    }
    catch (Exception e)
    {
    }
  }
  
  /*
  ** public void load()
  **
  ** Main entry point for loading report data.
  */
  public void load()
  {
    try
    {
      merchNum = Long.parseLong(fields.getData("merchant"));
    }
    catch (Exception e) {}
    
    if (merchNum != -1)
    {
      try
      {
        Calendar cal = Calendar.getInstance();
        cal.setTime(((ComboDateField)fields.getField("date")).getUtilDate());
        cal.set(Calendar.DAY_OF_MONTH,1);
        activeDate = new java.sql.Date(cal.getTime().getTime());
        cal.set(Calendar.MONTH,Calendar.JANUARY);
        yearBegin = new java.sql.Date(cal.getTime().getTime());
        
        connect();
        
        loadAccountProfitability();
        loadAuthorizationActivity();
        loadChargebacks();
        loadPlanActivity();
        loadOutgoingInterchange();
        finishUp();
      }
      catch (Exception e)
      {
        logEntry("load(" + merchNum + ")", e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }
}/*@lineinfo:generated-code*/