/*@lineinfo:filename=ProfIcCertegyDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ProfIcCertegyDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-04-23 08:36:57 -0700 (Thu, 23 Apr 2009) $
  Version            : $Revision: 16022 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import sqlj.runtime.ResultSetIterator;

public class ProfIcCertegyDataBean extends ProfIcDataBean
{
  public static final int         IC_CAT_CASH_ADVANCE               = 1100;
  public static final int         IC_CAT_CASH_ADVANCE_DOMESTIC      = 1103;
  public static final int         IC_CAT_CASH_ADVANCE_INTL          = 1104;
  public static final int         IC_CAT_CASH_ADVANCE_ATM           = 1110;
  public static final int         IC_CAT_CASH_ADVANCE_CONVERTED     = 1199;
  
  protected static final int[] IC_CASH_ADVANCE_CATS =
  {
    IC_CAT_CASH_ADVANCE,
    IC_CAT_CASH_ADVANCE_DOMESTIC,
    IC_CAT_CASH_ADVANCE_INTL,
    IC_CAT_CASH_ADVANCE_ATM,
    IC_CAT_CASH_ADVANCE_CONVERTED
  };
  
  public ProfIcCertegyDataBean( )
  {
  }
  
  protected boolean isCashAdvanceCharge( int chargeType )
  {
    boolean         retVal        = false;
    
    for( int i = 0; i < IC_CASH_ADVANCE_CATS.length; ++i )
    {
      if ( IC_CASH_ADVANCE_CATS[i] == chargeType )
      {
        retVal = true;
        break;
      }
    }
    return( retVal );
  }
  
  public void loadCashAdvanceData( long orgId, Date beginDate, Date endDate )
  {
    long                    assocId       = 0L;
    String                  cardType      = "";
    int                     catIdx        = 0;
    int                     chargeType    = -1;
    int                     icCardType    = -1;
    double                  icExp         = 0.0;
    double                  icInc         = 0.0;
    InterchangeData         icRecord      = null;
    ResultSetIterator       it            = null;
    long                    merchantId    = 0L;
    ResultSet               resultSet     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:100^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.assoc_hierarchy_node                 as assoc_number,
//                  sm.merchant_number                      as merchant_number,
//                  decode( pl.card_type,
//                          '30', 'AM',
//                          '31', 'DC',
//                          '32', 'DS',
//                          '33', 'JC',
//                          decode( substr(pl.card_type,1,1),
//                                  '4','VS',
//                                  '5','MC',
//                                  '6','DB',
//                                  'NA' ) )                as card_type,
//                  cct.charge_description                  as charge_desc,
//                  sum(pl.sales_count)                     as sales_count,
//                  sum(pl.sales_amount)                    as sales_amount,
//                  sum(pl.credits_count)                   as credits_count,
//                  sum(pl.credits_amount)                  as credits_amount,
//                  sum(pl.tot_inc_interchange)             as ic_inc,
//                  sum(pl.tot_exp_interchange)             as ic_exp
//          from    group_merchant                gm,
//                  monthly_extract_summary       sm,
//                  monthly_extract_certegy_pl    pl,
//                  certegy_charge_types          cct
//          where   gm.org_num = :getReportOrgId() and
//                  sm.merchant_number = gm.merchant_number and
//                  sm.active_date between :getReportDateBegin() and :getReportDateEnd() and
//                  pl.load_sec = sm.hh_load_sec and
//                  pl.charge_type in 
//                  (
//                    :IC_CAT_CASH_ADVANCE,           -- 1100,
//                    :IC_CAT_CASH_ADVANCE_DOMESTIC,  -- 1103,
//                    :IC_CAT_CASH_ADVANCE_INTL,      -- 1104,
//                    :IC_CAT_CASH_ADVANCE_ATM,       -- 1110,
//                    :IC_CAT_CASH_ADVANCE_CONVERTED  -- 1199
//                  ) and
//                  cct.charge_type = pl.charge_type 
//          group by  sm.assoc_hierarchy_node, sm.merchant_number,
//                    decode( pl.card_type,
//                            '30', 'AM',
//                            '31', 'DC',
//                            '32', 'DS',
//                            '33', 'JC',
//                            decode( substr(pl.card_type,1,1),
//                                    '4','VS',
//                                    '5','MC',
//                                    '6','DB',
//                                    'NA' ) ),
//                    cct.charge_description
//          order by sm.merchant_number, card_type, charge_desc                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1251 = getReportOrgId();
 java.sql.Date __sJT_1252 = getReportDateBegin();
 java.sql.Date __sJT_1253 = getReportDateEnd();
  try {
   String theSqlTS = "select  sm.assoc_hierarchy_node                 as assoc_number,\n                sm.merchant_number                      as merchant_number,\n                decode( pl.card_type,\n                        '30', 'AM',\n                        '31', 'DC',\n                        '32', 'DS',\n                        '33', 'JC',\n                        decode( substr(pl.card_type,1,1),\n                                '4','VS',\n                                '5','MC',\n                                '6','DB',\n                                'NA' ) )                as card_type,\n                cct.charge_description                  as charge_desc,\n                sum(pl.sales_count)                     as sales_count,\n                sum(pl.sales_amount)                    as sales_amount,\n                sum(pl.credits_count)                   as credits_count,\n                sum(pl.credits_amount)                  as credits_amount,\n                sum(pl.tot_inc_interchange)             as ic_inc,\n                sum(pl.tot_exp_interchange)             as ic_exp\n        from    group_merchant                gm,\n                monthly_extract_summary       sm,\n                monthly_extract_certegy_pl    pl,\n                certegy_charge_types          cct\n        where   gm.org_num =  :1  and\n                sm.merchant_number = gm.merchant_number and\n                sm.active_date between  :2  and  :3  and\n                pl.load_sec = sm.hh_load_sec and\n                pl.charge_type in \n                (\n                   :4 ,           -- 1100,\n                   :5 ,  -- 1103,\n                   :6 ,      -- 1104,\n                   :7 ,       -- 1110,\n                   :8   -- 1199\n                ) and\n                cct.charge_type = pl.charge_type \n        group by  sm.assoc_hierarchy_node, sm.merchant_number,\n                  decode( pl.card_type,\n                          '30', 'AM',\n                          '31', 'DC',\n                          '32', 'DS',\n                          '33', 'JC',\n                          decode( substr(pl.card_type,1,1),\n                                  '4','VS',\n                                  '5','MC',\n                                  '6','DB',\n                                  'NA' ) ),\n                  cct.charge_description\n        order by sm.merchant_number, card_type, charge_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ProfIcCertegyDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1251);
   __sJT_st.setDate(2,__sJT_1252);
   __sJT_st.setDate(3,__sJT_1253);
   __sJT_st.setInt(4,IC_CAT_CASH_ADVANCE);
   __sJT_st.setInt(5,IC_CAT_CASH_ADVANCE_DOMESTIC);
   __sJT_st.setInt(6,IC_CAT_CASH_ADVANCE_INTL);
   __sJT_st.setInt(7,IC_CAT_CASH_ADVANCE_ATM);
   __sJT_st.setInt(8,IC_CAT_CASH_ADVANCE_CONVERTED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ProfIcCertegyDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:151^7*/
      resultSet = it.getResultSet();
      
      
      while( resultSet.next() )
      {
        // store the merchant specific data
        merchantId  = resultSet.getLong("merchant_number");
        assocId     = resultSet.getLong("assoc_number");
      
        if ( !cardType.equals(resultSet.getString("card_type")) )
        {
          if ( icRecord != null )
          {
            // add the card interchange data to the appropriate merchant
            // interchange collection.
            addMerchantIcData( assocId, merchantId, icCardType, icRecord );
          }            
        
          // store the new card type
          cardType = resultSet.getString("card_type");
          
          if ( cardType.equals("VS") )
          {
            icCardType = IC_VISA;
          }
          else if ( cardType.equals("MC") )
          {
            icCardType = IC_MC;
          }
          else if ( cardType.equals("DC") )
          {
            icCardType = IC_DINERS;
          }
          else
          {
            continue;
          }
          
          // create a new object to store this categories data
          icRecord = new InterchangeData( icCardType, IC_CASH_ADVANCE_CATS.length );
        }          
      
        // get the income and expense for this row
        icInc = resultSet.getDouble("ic_inc");
        icExp = resultSet.getDouble("ic_exp");
        
        // if there is either income or expense, then store the data
        if ( (icInc != 0.0) || (icExp != 0.0) )
        {
          // convert the certegy charge type into an array index
          chargeType = resultSet.getInt("charge_type");
          for ( catIdx = 0; catIdx < IC_CASH_ADVANCE_CATS.length; ++catIdx )
          {
            if ( IC_CASH_ADVANCE_CATS[catIdx] == chargeType )
            {
              break;
            }
          }
          // store the data for this array index
          icRecord.setCategoryData( catIdx,
                                    resultSet.getString( "charge_desc" ),
                                    resultSet.getInt   ( "sales_count" ),
                                    resultSet.getDouble( "sales_amount" ),
                                    resultSet.getInt   ( "credits_count" ),
                                    resultSet.getDouble( "credits_amount" ),
                                    icInc, icExp );
        }
      }
      // boundary condition, add the last icRecord
      if ( icRecord != null )
      {
        // add the card interchange data to the appropriate merchant
        // interchange collection.
        addMerchantIcData( assocId, merchantId, icCardType, icRecord );
      }            
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }

  public void loadData( long orgId, Date beginDate, Date endDate, boolean maskCashAdvanceIc )
  {
    double                  assessmentExp     = 0.0;
    long                    assocId           = 0L;
    String                  cardType          = null;
    int                     catIdx            = 0;
    String                  chargeType        = null;
    int                     icCardType        = -1;
    double                  icExp             = 0.0;
    double                  icInc             = 0.0;
    InterchangeData         icRecord          = null;
    ResultSetIterator       it                = null;
    long                    merchantId        = 0L;
    Vector                  monetaryCharges   = new Vector();
    ResultSet               resultSet         = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:255^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  charge_type
//          from    certegy_charge_types 
//          where   substr(charge_type,1,1) = 1  -- monetary charges
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  charge_type\n        from    certegy_charge_types \n        where   substr(charge_type,1,1) = 1  -- monetary charges";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ProfIcCertegyDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.ProfIcCertegyDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:260^7*/
      resultSet = it.getResultSet();
      
      // store all the possible charge types in a vector
      while( resultSet.next() )
      {
        monetaryCharges.addElement(resultSet.getString("charge_type"));
      }
      resultSet.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:271^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.assoc_hierarchy_node                 as assoc_number,
//                  sm.merchant_number                      as merchant_number,
//                  decode( pl.card_type,
//                          '30', 'AM',
//                          '31', 'DC',
//                          '32', 'DS',
//                          '33', 'JC',
//                          decode( substr(pl.card_type,1,1),
//                                  '4','VS',
//                                  '5','MC',
//                                  '6','DB',
//                                  'NA' ) )                as card_type,
//                  pl.charge_type                          as charge_type,                                
//                  cct.charge_description                  as charge_desc,
//                  sum(pl.sales_count)                     as sales_count,
//                  sum(pl.sales_amount)                    as sales_amount,
//                  sum(pl.credits_count)                   as credits_count,
//                  sum(pl.credits_amount)                  as credits_amount,
//                  sum(pl.tot_inc_interchange)             as ic_inc,
//                  sum(pl.tot_exp_interchange)             as ic_exp
//          from    group_merchant                gm,
//                  monthly_extract_summary       sm,
//                  monthly_extract_certegy_pl    pl,
//                  certegy_charge_types          cct
//          where   gm.org_num = :orgId and
//                  sm.merchant_number = gm.merchant_number and
//                  sm.active_date between :beginDate and :endDate and
//                  pl.load_sec = sm.hh_load_sec and
//                  cct.charge_type = pl.charge_type 
//          group by  sm.assoc_hierarchy_node, sm.merchant_number,
//                    decode( pl.card_type,
//                            '30', 'AM',
//                            '31', 'DC',
//                            '32', 'DS',
//                            '33', 'JC',
//                            decode( substr(pl.card_type,1,1),
//                                    '4','VS',
//                                    '5','MC',
//                                    '6','DB',
//                                    'NA' ) ),
//                    pl.charge_type,                                  
//                    cct.charge_description
//          order by sm.merchant_number, card_type, charge_desc                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.assoc_hierarchy_node                 as assoc_number,\n                sm.merchant_number                      as merchant_number,\n                decode( pl.card_type,\n                        '30', 'AM',\n                        '31', 'DC',\n                        '32', 'DS',\n                        '33', 'JC',\n                        decode( substr(pl.card_type,1,1),\n                                '4','VS',\n                                '5','MC',\n                                '6','DB',\n                                'NA' ) )                as card_type,\n                pl.charge_type                          as charge_type,                                \n                cct.charge_description                  as charge_desc,\n                sum(pl.sales_count)                     as sales_count,\n                sum(pl.sales_amount)                    as sales_amount,\n                sum(pl.credits_count)                   as credits_count,\n                sum(pl.credits_amount)                  as credits_amount,\n                sum(pl.tot_inc_interchange)             as ic_inc,\n                sum(pl.tot_exp_interchange)             as ic_exp\n        from    group_merchant                gm,\n                monthly_extract_summary       sm,\n                monthly_extract_certegy_pl    pl,\n                certegy_charge_types          cct\n        where   gm.org_num =  :1  and\n                sm.merchant_number = gm.merchant_number and\n                sm.active_date between  :2  and  :3  and\n                pl.load_sec = sm.hh_load_sec and\n                cct.charge_type = pl.charge_type \n        group by  sm.assoc_hierarchy_node, sm.merchant_number,\n                  decode( pl.card_type,\n                          '30', 'AM',\n                          '31', 'DC',\n                          '32', 'DS',\n                          '33', 'JC',\n                          decode( substr(pl.card_type,1,1),\n                                  '4','VS',\n                                  '5','MC',\n                                  '6','DB',\n                                  'NA' ) ),\n                  pl.charge_type,                                  \n                  cct.charge_description\n        order by sm.merchant_number, card_type, charge_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ProfIcCertegyDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.ProfIcCertegyDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:316^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        // store the merchant specific data
        merchantId  = resultSet.getLong("merchant_number");
        assocId     = resultSet.getLong("assoc_number");
      
        if ( cardType == null || !cardType.equals(resultSet.getString("card_type")) )
        {
          if ( icRecord != null )
          {
            // add the card interchange data to the appropriate merchant
            // interchange collection.
            addMerchantIcData( assocId, merchantId, icCardType, icRecord );
          }            
        
          // store the new card type
          cardType = resultSet.getString("card_type");
          assessmentExp = 0.0;
          
          if ( cardType.equals("VS") )
          {
            icCardType = IC_VISA;
            
            /*@lineinfo:generated-code*//*@lineinfo:342^13*/

//  ************************************************************
//  #sql [Ctx] { select nvl(sum(nvl(visa_assessment,0)),0) 
//                from    monthly_extract_summary     sm
//                where   sm.merchant_number = :merchantId and
//                        sm.active_date between :beginDate and :endDate
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl(sum(nvl(visa_assessment,0)),0)  \n              from    monthly_extract_summary     sm\n              where   sm.merchant_number =  :1  and\n                      sm.active_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ProfIcCertegyDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   assessmentExp = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:348^13*/
          }
          else if ( cardType.equals("MC") )
          {
            icCardType = IC_MC;
            
            /*@lineinfo:generated-code*//*@lineinfo:354^13*/

//  ************************************************************
//  #sql [Ctx] { select nvl(sum(nvl(mc_assessment,0)),0) 
//                from    monthly_extract_summary     sm
//                where   sm.merchant_number = :merchantId and
//                        sm.active_date between :beginDate and :endDate
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl(sum(nvl(mc_assessment,0)),0)  \n              from    monthly_extract_summary     sm\n              where   sm.merchant_number =  :1  and\n                      sm.active_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.ProfIcCertegyDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   assessmentExp = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:360^13*/
          }
          else if ( cardType.equals("DC") )
          {
            icCardType = IC_DINERS;
          }
          else
          {
            icRecord = null;
            continue;
          }
          
          // create a new object to store this categories data
          icRecord = new InterchangeData( icCardType, monetaryCharges.size() );
          
          // set the fixed expenses
          icRecord.setFixedData( 0, assessmentExp,  // only exp for assessment
                                 0, 0,    // no bulletin
                                 0, 0 );  // no intra-change
        }          
      
        // get the income and expense for this row
        icInc = resultSet.getDouble("ic_inc");
        icExp = resultSet.getDouble("ic_exp");
        
        // if there is either income or expense, then store the data
        if ( (icInc != 0.0) || (icExp != 0.0) )
        {
          // convert the certegy charge type into an array index
          chargeType = resultSet.getString("charge_type");
          for ( catIdx = 0; catIdx < monetaryCharges.size(); ++catIdx )
          {
            if ( chargeType.equals((String)monetaryCharges.elementAt(catIdx)) )
            {
              break;
            }
          }
          
          // skip this category if it is cash advance and we are masking
          // cash advance interchange from the data set.
          if ( isCashAdvanceCharge( Integer.parseInt(chargeType) ) &&
               (maskCashAdvanceIc == true) )
          {
            continue;
          }
          
          // store the data for this array index
          icRecord.setCategoryData( catIdx,
                                    resultSet.getString( "charge_desc" ),
                                    resultSet.getInt   ( "sales_count" ),
                                    resultSet.getDouble( "sales_amount" ),
                                    resultSet.getInt   ( "credits_count" ),
                                    resultSet.getDouble( "credits_amount" ),
                                    icInc, icExp );
        }
      }
      
      // boundary condition, add the last icRecord
      if ( icRecord != null )
      {
      
        // add the card interchange data to the appropriate merchant
        // interchange collection.
        addMerchantIcData( assocId, merchantId, icCardType, icRecord );
      }            
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }
  
  public double loadSummaryExpense( long orgId, Date beginDate, Date endDate, boolean maskCashAdvanceIc )
  {
    double          actualIcExpense   = 0.0;
    double          assessmentExpense = 0.0;
    double          cashAdvIcExpense  = 0.0;
    double          estIcExpense      = 0.0;
    double          retVal            = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:446^7*/

//  ************************************************************
//  #sql [Ctx] { select sum( summary.BET_INTERCHANGE_EXPENSE ),
//                 sum( summary.INTERCHANGE_EXPENSE ),
//                 sum( summary.CASH_ADV_INTERCHANGE_EXPENSE ),
//                 sum( summary.VMC_ASSESSMENT_EXPENSE)
//                  
//          from   monthly_extract_summary summary
//          where  summary.merchant_number = 
//                    ( select org_merchant_num 
//                      from orgmerchant 
//                      where org_num = :orgId ) and
//                 summary.active_date between :beginDate and :endDate                 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum( summary.BET_INTERCHANGE_EXPENSE ),\n               sum( summary.INTERCHANGE_EXPENSE ),\n               sum( summary.CASH_ADV_INTERCHANGE_EXPENSE ),\n               sum( summary.VMC_ASSESSMENT_EXPENSE)\n                 \n        from   monthly_extract_summary summary\n        where  summary.merchant_number = \n                  ( select org_merchant_num \n                    from orgmerchant \n                    where org_num =  :1  ) and\n               summary.active_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.ProfIcCertegyDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   estIcExpense = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   actualIcExpense = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cashAdvIcExpense = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   assessmentExpense = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:459^7*/
      if ( actualIcExpense == 0.0 )
      {
        retVal = estIcExpense;  // est includes assessment
      }
      else  // use actual if possible
      {
        retVal = actualIcExpense + assessmentExpense;
      }
      
      // if the login user has cash advance interchange
      // masked and this merchant is a cash advance merchant
      // (i.e. has items in cash advance ic categories)
      // then remove the cash advance interchange and assessment.
      if ( ( maskCashAdvanceIc == true ) &&
           ( cashAdvIcExpense != 0.0 ) )
      {
        retVal -= ( cashAdvIcExpense + assessmentExpense );
      }
    }        
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public double loadSummaryIncome( long orgId, Date beginDate, Date endDate, boolean maskCashAdvanceIc )
  {
    return(0.0);
  }
}/*@lineinfo:generated-code*/