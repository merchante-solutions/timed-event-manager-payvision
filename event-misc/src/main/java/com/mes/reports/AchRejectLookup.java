/*@lineinfo:filename=AchRejectLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AchRejectLookup.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-01 11:46:29 -0700 (Tue, 01 Jul 2008) $
  Version            : $Revision: 15021 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class AchRejectLookup extends ReportSQLJBean
{
  public static final int QUEUE_ACH_OPEN              = 1;
  public static final int QUEUE_ACH_COMPLETED         = 2;
  public static final int QUEUE_RISK_OPEN             = 3;
  public static final int QUEUE_RISK_COMPLETED        = 4;
  public static final int QUEUE_COLLECTIONS_OPEN      = 5;
  public static final int QUEUE_COLLECTIONS_COMPLETED = 6;
  public static final int QUEUE_LEGAL_OPEN            = 7;
  public static final int QUEUE_LEGAL_COMPLETED       = 8;
  public static final int QUEUE_NON_RETURNABLE_OPEN   = 9;

  private String                searchCriteria        = "";

  private int                   achOpen               = -1;
  private int                   achCompleted          = -1;

  private int                   riskOpen              = -1;
  private int                   riskCompleted         = -1;

  private int                   collectionsOpen       = -1;
  private int                   collectionsCompleted  = -1;

  private int                   legalOpen             = -1;
  private int                   legalCompleted        = -1;

  private int                   nonReturnableOpen     = -1;
 
  public void AchRejectLookup()
  {
  }

  public synchronized void loadData()
  {
    // empty the current contents
    ReportRows.clear();

    if(searchQueue(QUEUE_ACH_OPEN))
    {
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_ACH_REJECT_CATEGORY_1   );
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_ACH_REJECT_CATEGORY_2   );
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_ACH_REJECT_CATEGORY_3   );
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_ACH_REJECT_CATEGORY_URO );

      //we dont want to look up comment stuff.. c?? codes
      //getIndividualData(user, MesQueues.Q_ACH_REJECT_CATEGORY_MISC);
    }

    if(searchQueue(QUEUE_ACH_COMPLETED))
    {
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_ACH_REJECT_COMPLETED );
    }

    if(searchQueue(QUEUE_RISK_OPEN))
    {
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_RISK_NEW );
    }

    if(searchQueue(QUEUE_RISK_COMPLETED))
    {
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_RISK_COMPLETED );
    }

    if(searchQueue(QUEUE_COLLECTIONS_OPEN))
    {
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_COLLECTIONS_NEW );
    }

    if(searchQueue(QUEUE_COLLECTIONS_COMPLETED))
    {
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_COLLECTIONS_COMPLETED );
    }

    if(searchQueue(QUEUE_LEGAL_OPEN))
    {
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_LEGAL_NEW );
    }

    if(searchQueue(QUEUE_LEGAL_COMPLETED))
    {
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_LEGAL_COMPLETED );
    }

    if(searchQueue(QUEUE_NON_RETURNABLE_OPEN))
    {
      loadData( ReportUserBean, ReportDateBegin, ReportDateEnd, MesQueues.Q_NON_RETURNABLE_NEW );
    }
 
    orderReportRows();

  }

  private void orderReportRows()
  {
    RejectDataComparator  rdc                   = new RejectDataComparator();
    TreeSet               sortedResults         = null;
    Iterator              result                = null;
    Vector                tempVec               = new Vector();

    sortedResults = new TreeSet(rdc);
    sortedResults.addAll(ReportRows);

    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    while(result != null && result.hasNext())
    {
      RowData rd = (RowData)(result.next());
      tempVec.add(rd);
    }

    ReportRows = tempVec;

  }

  public class RejectDataComparator
    implements Comparator
  {
    public final static int   SB_DATE_CREATED         = 1;

    private int               sortBy;
    
    private boolean           sortAscending           = true;
    
    public RejectDataComparator()
    {
      this.sortBy = SB_DATE_CREATED;
    }
    
    int compare(RowData o1, RowData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_DATE_CREATED:
            compareString1 = o1.DateCreated + o1.Id;
            compareString2 = o2.DateCreated + o2.Id;
            break;

          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(RowData o1, RowData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_DATE_CREATED:
            compareString1 = o1.DateCreated + o1.Id;
            compareString2 = o2.DateCreated + o2.Id;
            break;
          
          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((RowData)o1, (RowData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((RowData)o1, (RowData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }

  private void loadData(UserBean user, Date beginDate, Date endDate, int queueType)
  {
    ResultSetIterator             it                  = null;
    ResultSet                     rs                  = null;
    String                        likeSearchCriteria  = "";
    long                          longLookup          = -2;

    try
    {
      String appTypeDesc = user.getAppDesc();
      
      if(appTypeDesc.equals("MES") || appTypeDesc.equals("MESR"))
      {
        appTypeDesc = "MESN";
      }
      
      if(searchCriteria.trim().equals(""))
      {
        this.searchCriteria = "passall";
      }

      // set up longLookup value (numeric version of lookup value)
      try
      {
        longLookup = Long.parseLong(searchCriteria.trim());
      }
      catch (Exception e) 
      {
        longLookup = -2;
      }

      likeSearchCriteria = "%" + searchCriteria.toUpperCase() + "%";
      
      /*@lineinfo:generated-code*//*@lineinfo:322^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  qd.owner                owner,
//                  ar.settled_date         date_created,
//                  qd.source               source,
//                  qd.affiliate            affiliate,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  ar.merchant_name        description,
//                  ar.reject_seq_num       control,
//                  qd.locked_by            locked_by,
//                  get_queue_note_count(ar.reject_seq_num,qd.type)   note_count,
//                  qt.status               status,
//                  qt.description          type_desc,
//                  ar.merchant_number      merchant_number,
//                  ar.reason_code          reason_code,
//                  arrc.description        code_description,
//                  ar.amount               amount,
//                  decode(ars.reject_status, null, 'NEW', ars.reject_status) reject_status,
//                  m.dmagent               association,
//                  m.transit_routng_num    transit_routing_num,
//                  m.dda_num               dda_num
//          from    q_data                  qd,
//                  q_types                 qt,
//                  ach_rejects             ar,
//                  ach_reject_reason_codes arrc,
//                  ach_reject_status       ars,
//                  mif                     m
//          where   ar.settled_date between :beginDate and :endDate and
//                  INSTR(ar.reason_code, 'C') <= 0                 and
//                  :queueType             = qd.type                and
//                  qd.id                  = ar.reject_seq_num      and
//                  ar.reject_seq_num      = ars.reject_seq_num(+)  and
//                  ar.reason_code         = arrc.reason_code(+)    and
//                  ar.merchant_number     = m.merchant_number      and
//                  qd.type                = qt.type                and
//                  ('MESN' = :appTypeDesc or qd.affiliate = :appTypeDesc) and
//                  (
//                    'passall'             = :searchCriteria or 
//                    ar.merchant_number    = :longLookup     or
//                    m.dda_num             = :searchCriteria or
//                    m.transit_routng_num  = :longLookup     or
//                    upper(ar.merchant_name) like :likeSearchCriteria
//                  )
//          group by qd.id,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//                   ar.settled_date,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   ar.merchant_name,
//                   ar.reject_seq_num,
//                   qd.locked_by,
//                   qt.status,
//                   qt.description,
//                   ar.merchant_number,
//                   ar.reason_code,
//                   arrc.description,
//                   ar.amount,
//                   ar.merchant_number,
//                   ars.reject_status,
//                   m.dmagent,
//                   m.transit_routng_num,
//                   m.dda_num
//          order by ar.settled_date asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                qd.type                 type,\n                qd.item_type            item_type,\n                qd.owner                owner,\n                ar.settled_date         date_created,\n                qd.source               source,\n                qd.affiliate            affiliate,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                ar.merchant_name        description,\n                ar.reject_seq_num       control,\n                qd.locked_by            locked_by,\n                get_queue_note_count(ar.reject_seq_num,qd.type)   note_count,\n                qt.status               status,\n                qt.description          type_desc,\n                ar.merchant_number      merchant_number,\n                ar.reason_code          reason_code,\n                arrc.description        code_description,\n                ar.amount               amount,\n                decode(ars.reject_status, null, 'NEW', ars.reject_status) reject_status,\n                m.dmagent               association,\n                m.transit_routng_num    transit_routing_num,\n                m.dda_num               dda_num\n        from    q_data                  qd,\n                q_types                 qt,\n                ach_rejects             ar,\n                ach_reject_reason_codes arrc,\n                ach_reject_status       ars,\n                mif                     m\n        where   ar.settled_date between  :1  and  :2  and\n                INSTR(ar.reason_code, 'C') <= 0                 and\n                 :3              = qd.type                and\n                qd.id                  = ar.reject_seq_num      and\n                ar.reject_seq_num      = ars.reject_seq_num(+)  and\n                ar.reason_code         = arrc.reason_code(+)    and\n                ar.merchant_number     = m.merchant_number      and\n                qd.type                = qt.type                and\n                ('MESN' =  :4  or qd.affiliate =  :5 ) and\n                (\n                  'passall'             =  :6  or \n                  ar.merchant_number    =  :7      or\n                  m.dda_num             =  :8  or\n                  m.transit_routng_num  =  :9      or\n                  upper(ar.merchant_name) like  :10 \n                )\n        group by qd.id,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n                 ar.settled_date,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 ar.merchant_name,\n                 ar.reject_seq_num,\n                 qd.locked_by,\n                 qt.status,\n                 qt.description,\n                 ar.merchant_number,\n                 ar.reason_code,\n                 arrc.description,\n                 ar.amount,\n                 ar.merchant_number,\n                 ars.reject_status,\n                 m.dmagent,\n                 m.transit_routng_num,\n                 m.dda_num\n        order by ar.settled_date asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AchRejectLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,queueType);
   __sJT_st.setString(4,appTypeDesc);
   __sJT_st.setString(5,appTypeDesc);
   __sJT_st.setString(6,searchCriteria);
   __sJT_st.setLong(7,longLookup);
   __sJT_st.setString(8,searchCriteria);
   __sJT_st.setLong(9,longLookup);
   __sJT_st.setString(10,likeSearchCriteria);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AchRejectLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:393^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        ReportRows.addElement( new RowData( rs ) );
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  public synchronized void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    setSearchCriteria(HttpHelper.getString(request, "searchCriteria", "passall"));

    if(HttpHelper.getString(request, "allQueues", "N").equals("Y"))
    {
      setAchOpen();
      setAchCompleted();
      setRiskOpen();
      setRiskCompleted();
      setCollectionsOpen();
      setCollectionsCompleted();
      setLegalOpen();
      setLegalCompleted();
    }

    if(HttpHelper.getString(request, "achOpen", "N").equals("Y"))
    {
      setAchOpen();
    }
    
    if(HttpHelper.getString(request, "achCompleted", "N").equals("Y"))
    {
      setAchCompleted();
    }

    if(HttpHelper.getString(request, "riskOpen", "N").equals("Y"))
    {
      setRiskOpen();
    }

    if(HttpHelper.getString(request, "riskCompleted", "N").equals("Y"))
    {
      setRiskCompleted();
    }

    if(HttpHelper.getString(request, "collectionsOpen", "N").equals("Y"))
    {
      setCollectionsOpen();
    }

    if(HttpHelper.getString(request, "collectionsCompleted", "N").equals("Y"))
    {
      setCollectionsCompleted();
    }

    if(HttpHelper.getString(request, "legalOpen", "N").equals("Y"))
    {
      setLegalOpen();
    }

    if(HttpHelper.getString(request, "legalCompleted", "N").equals("Y"))
    {
      setLegalCompleted();
    }

    if(HttpHelper.getString(request, "nonReturnableOpen", "N").equals("Y"))
    {
      setNonReturnableOpen();
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }

  /*
  ** ACCESSORS
  */
  public synchronized void setSearchCriteria(String searchCriteria)
  {
    this.searchCriteria = searchCriteria.trim();
  }

  public synchronized String getSearchCriteria()
  {
    String result = "";
    
    if(!this.searchCriteria.equals("passall"))
    {
      result = this.searchCriteria;
    }

    return result;
  }
  
 
  public synchronized void resetFlags()
  {
    this.achOpen              = -1;
    this.achCompleted         = -1;

    this.riskOpen             = -1;
    this.riskCompleted        = -1;

    this.collectionsOpen      = -1;
    this.collectionsCompleted = -1;

    this.legalOpen            = -1;
    this.legalCompleted       = -1;

    this.nonReturnableOpen    = -1;
  }

  public synchronized boolean searchQueue(int queue)
  {
    switch(queue)
    {
      case QUEUE_ACH_OPEN:
        return (this.achOpen == 1);

      case QUEUE_ACH_COMPLETED:
        return (this.achCompleted == 1);

      case QUEUE_RISK_OPEN:
        return (this.riskOpen == 1);

      case QUEUE_RISK_COMPLETED:
        return (this.riskCompleted == 1);

      case QUEUE_COLLECTIONS_OPEN:
        return (this.collectionsOpen == 1);

      case QUEUE_COLLECTIONS_COMPLETED:
        return (this.collectionsCompleted == 1);

      case QUEUE_LEGAL_OPEN:
        return (this.legalOpen == 1);

      case QUEUE_LEGAL_COMPLETED:
        return (this.legalCompleted == 1);

      case QUEUE_NON_RETURNABLE_OPEN:
        return (this.nonReturnableOpen == 1);

      default:
        return false;
    }
  }

  public synchronized void setAchOpen()
  {
    this.achOpen = 1;
  }

  public synchronized void setAchCompleted()
  {
    this.achCompleted = 1;
  }

  public synchronized void  setRiskOpen()
  {
    this.riskOpen = 1;
  }

  public synchronized void setRiskCompleted()
  {
    this.riskCompleted = 1;
  }
  
  public synchronized void setCollectionsOpen()
  {
    this.collectionsOpen = 1;
  }

  public synchronized void setCollectionsCompleted()
  {
    this.collectionsCompleted = 1;
  }

  public synchronized void setLegalOpen()
  {
    this.legalOpen = 1;
  }

  public synchronized void setLegalCompleted()
  {
    this.legalCompleted = 1;
  }
  
  public synchronized void setNonReturnableOpen()
  {
    this.nonReturnableOpen = 1;
  }

  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Date Originated\",");
    line.append("\"Queue\",");
    line.append("\"Control\",");
    line.append("\"Description\",");
    line.append("\"Source\",");
    line.append("\"Type\",");
    line.append("\"Last Worked\",");
    line.append("\"Worked By\",");
    line.append("\"Notes\",");
    line.append("\"Merchant #\",");
    line.append("\"Association\",");
    line.append("\"Transit Rtg. #\",");
    line.append("\"DDA #\",");
    line.append("\"Reason Code\",");
    line.append("\"Code Description\",");
    line.append("\"Amount\",");
    line.append("\"Reject Status\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    

    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.DateCreated );
    line.append( ",\"" );
    line.append( record.TypeDesc );
    line.append( "\",\"" );
    line.append( record.Id );
    line.append( "\",\"" );
    line.append( record.Description );
    line.append( "\",\"" );
    line.append( record.Source );
    line.append( "\",\"" );
    line.append( record.Affiliate );
    line.append( "\"," );
    line.append( record.LastChanged );
    line.append( ",\"" );
    line.append( record.LastUser );
    line.append( "\",\"" );
    line.append( record.NoteCount );
    line.append( "\",\"" );
    line.append( record.MerchantNumber );
    line.append( "\",\"" );
    line.append( record.Association );
    line.append( "\",\"" );
    line.append( record.TransitRoutingNum );
    line.append( "\",\"" );
    line.append( record.DdaNum );
    line.append( "\",\"" );
    line.append( record.ReasonCode );
    line.append( "\",\"" );
    line.append( record.CodeDescription );
    line.append( "\"," );
    line.append( record.Amount );
    line.append( ",\"" );
    line.append( record.RejectStatus );
    line.append( "\"" );
  }
  
  public synchronized String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_ach_rejects_lookup_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public synchronized boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  /*********************************************************
  **
  ** SUBCLASS RowData
  **
  **********************************************************/ 
  public class RowData
  {
    public String Id                = null;
    public String Type              = null;
    public String TypeDesc          = null;
    public String ItemType          = null;
    public String Owner             = null;
    public String DateCreated       = null;
    public String Source            = null;
    public String Affiliate         = null;
    public String LastChanged       = null;
    public String LastUser          = null;
    public String Description       = null;
    public String Control           = null;
    public String LockedBy          = null;
    public String NoteCount         = null;
    public String Status            = null;
    public String ReasonCode        = null;
    public String CodeDescription   = null;
    public double Amount            = 0.0;
    public String AmountString      = null;
    public String MerchantNumber    = null;
    public String RejectStatus      = null;
    public String Association       = null;
    public String TransitRoutingNum = null;
    public String DdaNum            = null;

    public RowData( ResultSet       rs )
      throws java.sql.SQLException
    {

      Id                   = processStringField(rs.getString("id"));
      Type                 = processStringField(rs.getString("type"));
      TypeDesc             = processStringField(rs.getString("type_desc"));
      ItemType             = processStringField(rs.getString("item_type"));
      Owner                = processStringField(rs.getString("owner"));
      DateCreated          = DateTimeFormatter.getFormattedDate(rs.getTimestamp("date_created"), "MM/dd/yy HH:mm");
      Source               = processStringField(rs.getString("source"));
      Affiliate            = processStringField(rs.getString("affiliate"));
      LastChanged          = DateTimeFormatter.getFormattedDate(rs.getTimestamp("last_changed"), "MM/dd/yy HH:mm");
      LastUser             = processStringField(rs.getString("last_user"));
      Description          = processStringField(rs.getString("description"));
      Control              = processStringField(rs.getString("control"));
      LockedBy             = processStringField(rs.getString("locked_by"));
      NoteCount            = processStringField(rs.getString("note_count"));
      Status               = processStringField(rs.getString("status"));
      ReasonCode           = processStringField(rs.getString("reason_code"));
      CodeDescription      = processStringField(rs.getString("code_description"));
      Amount               = rs.getDouble("amount");
      AmountString         = MesMath.toCurrency(rs.getDouble("amount"));
      MerchantNumber       = processStringField(rs.getString("merchant_number"));
      RejectStatus         = processStringField(rs.getString("reject_status"));
      Association          = processStringField(rs.getString("association"));
      TransitRoutingNum    = processStringField(rs.getString("transit_routing_num"));
      DdaNum               = processStringField(rs.getString("dda_num"));
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "" : fieldData);
    }
  }
}/*@lineinfo:generated-code*/