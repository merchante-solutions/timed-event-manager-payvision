/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ReportDataBean.java $

  Description:  
    Base class for report data beans
    
    This class should maintain any data that is common to all of the 
    possible report data beans.  The inheritors of this class should 
    maintain any page-specific report data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.database.SQLJConnectionBase;

public class ReportDataBean extends SQLJConnectionBase
{
  // common data items
  private boolean               Submit            = false;
  
  // utility variables
  private String                DbConnectionString      = "";
  private Vector                ErrorList               = new Vector();
  
  /*
  ** CONSTRUCTOR
  */  
  public ReportDataBean()
  {
    super();
  }
  
  public ReportDataBean( boolean autoCommit )
  {  
    super(autoCommit);
  }    
  
  /*
  ** FUNCTION getErrors
  */
  public Vector getErrors()
  {
    return( ErrorList );
  }
  
  /*
  ** FUNCTION addError
  */
  public void addError(String error)
  {
    try
    {
      ErrorList.add(error);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "addError: " + e.toString());
    }
  }
  
  /*
  ** FUNCTION hasErrors
  */
  public boolean hasErrors()
  {
    return(ErrorList.size() > 0);
  }
  
  /***************************************************************************
  * Overridable methods
  ***************************************************************************/
  /*
  ** FUNCTION executeQuery
  *
  *   This method is overloaded by individual reports beans (if necessary)
  *   to read a result set from the database and return the result set
  *   to the page for display.  This is primarily used by simple data
  *   only reports.
  */
  public ResultSet executeQuery( long orgId, Date beginDate, Date endDate )
  {
    return( null );
  }
  public ResultSet executeQuery()
  {
    return( null );
  }
  
  /*
  ** FUNCTION loadData
  *
  *   This method is overloaded by individual reports beans (if necessary)
  *   to read the data from the database into the bean.
  */
  public void loadData( long orgId )
  {
  }
  
  /*
  ** FUNCTION storeData
  *
  *   This method is overloaded by individual report beans (if necessary)
  *   to write teh data from the bean to the database.
  *
  */
  public void storeData( long orgId )
  {
  }
  
  /*
  ** FUNCTION validate
  */
  public boolean validate()
  {
    return(true);
  }
  
  /***************************************************************************
  * DownloadServlet support
  ***************************************************************************/

  /*
  ** FUNCTION setProperties
  *
  *   This method should be overloaded by report beans that support
  *   exporting report data via the download servlet.  It allows a
  *   bean to set internal members from the attributes in a 
  *   HttpServletRequest.
  */
  public void setProperties(HttpServletRequest request)
  {
  }
  
  /*
  ** FUNCTION getDownloadFilenameBase()
  *
  *   This method should be overloaded by report beans that support
  *   exporting report data via the download servlet.  It allows a
  *   bean to specify a download filename.
  */
  public String getDownloadFilenameBase()
  {
    return null;
  }
  
  /***************************************************************************
  * Helper functions
  ***************************************************************************/
  public boolean isBlank(String test)
  {
    boolean retVal = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      retVal = true;
    }
    
    return( retVal );
  }
  
  public boolean isNumber(String test)
  {
    boolean retVal = false;
    
    try
    {
      long i = Long.parseLong(test);
      retVal = true;
    }
    catch(Exception e)
    {
    }
    
    return( retVal );
  }
  
  public boolean isEmail(String test)
  {
    boolean retVal = false;
    
    if(!isBlank(test) && test.indexOf('@') > 0)
    {
      retVal = true;
    }
    
    return( retVal );
  }
  
  public long getDigits(String raw)
  {
    long          result      = 0L;
    StringBuffer  digits      = new StringBuffer("");
    
    for(int i=0; i<raw.length(); ++i)
    {
      if(Character.isDigit(raw.charAt(i)))
      {
        digits.append(raw.charAt(i));
      }
    }
    
    try
    {
      result = Long.parseLong(digits.toString());
    }
    catch(Exception e)
    {
      result = 0L;
    }
    
    return result;
  }
  
  public boolean isSubmitted()
  {
    return( Submit );
  }
  
  /***************************************************************************
  * Getters and Setters
  ***************************************************************************/
  public void setSubmit(String submit)
  {
    Submit = true;
  }
  public boolean getSubmit()
  {
    return( Submit );
  }
}
