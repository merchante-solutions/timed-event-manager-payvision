/*@lineinfo:filename=ComdataTranDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ComdataTranDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 3/12/02 2:32p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ComboDateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.NumberFormatter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class ComdataTranDataBean extends ReportSQLJBean
{
  public static final int       RT_VOL_BY_LOC_SUMMARY     = (RT_USER + 0);
  public static final int       RT_VOL_BY_LOC_DETAILS     = (RT_USER + 1);
  public static final int       RT_SUMMARY_BY_BATCH       = (RT_USER + 2);
  public static final int       RT_VOL_BY_ACCT_SUMMARY    = (RT_USER + 3);
  public static final int       RT_PAID_SUMMARY           = (RT_USER + 4);
  public static final int       RT_PAID_DETAILS           = (RT_USER + 5);
  public static final int       RT_ITEM_SEARCH            = (RT_USER + 6);
  
  public static final long      SETTLE_GROUP_MAX          = 999999999L;
  
  // payment status
  public static final int       PT_ALL                  = 0;
  public static final int       PT_PAID                 = 1;
  public static final int       PT_UNPAID               = 2;
  
  // code type values
  public static final int       CT_NONE                 = 0;
  public static final int       CT_LOC                  = 1;
  public static final int       CT_CHAIN                = 2;
  
  // location types
  public static final int       LT_SERVICE_CENTER       = 0;
  public static final int       LT_CHAIN                = 1;
  public static final int       LT_RECEIVER             = 2;
  
  // total group indexes
  public static final int       TGI_DB                  = 0;
  public static final int       TGI_FUNDED              = 1;
  public static final int       TGI_GROSS               = 2;
  public static final int       TGI_PEND                = 3;
  public static final int       TGI_TRANSLINK           = 4;
  public static final int       TGI_PREF_FUND           = 5;
  public static final int       TGI_PREF_DB             = 6;
  public static final int       TGI_CREDIT              = 7;
  public static final int       TGI_COUNT               = 8;
  
  // total group column indexes
  public static final int       TCI_GALLONS             = 0;
  public static final int       TCI_AMOUNT              = 1;
  public static final int       TCI_INVOICE             = 2;
  public static final int       TCI_DISCOUNT            = 3;
  public static final int       TCI_FEES                = 4;
  public static final int       TCI_PAYMENT             = 5;
  public static final int       TCI_PROD_TOTAL          = 6;
  public static final int       TCI_COUNT               = 7;
  
  // fuel related tran detail values
  public static final int       TD_DIESEL_1             = 0;
  public static final int       TD_DIESEL_2             = 1;
  public static final int       TD_REEFER               = 2;
  public static final int       TD_OTHER                = 3;
  public static final int       TD_OIL                  = 4;
  public static final int       TD_COUNT                = 5;
  
  // fuel related index ( gallons, price per gallon, total amount )
  public static final int       TDI_GALLONS             = 0;
  public static final int       TDI_COST_PER_GAL        = 1;
  public static final int       TDI_TOTAL_COST          = 2;
  public static final int       TDI_COUNT               = 3;
  
  // non-fuel related detail amounts
  public static final int       TD_NF_CASH_ADVANCE      = 0;
  public static final int       TD_NF_PRODUCT_1_AMOUNT  = 1;
  public static final int       TD_NF_PRODUCT_2_AMOUNT  = 2;
  public static final int       TD_NF_PRODUCT_3_AMOUNT  = 3;
  public static final int       TD_NF_COUNT             = 4;
  
  // fee related amounts
  public static final int       TD_FEE_DISCOUNT_AMOUNT  = 0;
  public static final int       TD_FEE_FEE_AMOUNT       = 1;
  public static final int       TD_FEE_COUNT            = 2;
  
  // direct bill flags
  public static final int       DB_FUEL                 = 0;
  public static final int       DB_OIL                  = 1;
  public static final int       DB_CASH                 = 2;
  public static final int       DB_P1                   = 3;
  public static final int       DB_P2                   = 4;
  public static final int       DB_P3                   = 5;
  public static final int       DB_COUNT                = 6;
  
  // summary columns
  public static final int       TDS_DIESEL_1_GALS       = 0;
  public static final int       TDS_DIESEL_1_COST       = 1;
  public static final int       TDS_DIESEL_2_GALS       = 2;
  public static final int       TDS_DIESEL_2_COST       = 3;
  public static final int       TDS_REEFER_GALS         = 4;
  public static final int       TDS_REEFER_COST         = 5;
  public static final int       TDS_OTHER_GALS          = 6;
  public static final int       TDS_OTHER_COST          = 7;
  public static final int       TDS_OIL_QUARTS          = 8;
  public static final int       TDS_OIL_COST            = 9;
  public static final int       TDS_CASH_ADVANCE        = 10;
  public static final int       TDS_PRODUCT_1_AMOUNT    = 11;
  public static final int       TDS_PRODUCT_2_AMOUNT    = 12;
  public static final int       TDS_PRODUCT_3_AMOUNT    = 13;
  public static final int       TDS_DISCOUNT_AMOUNT     = 14;
  public static final int       TDS_FEE_AMOUNT          = 15;
  public static final int       TDS_ADJUSTMENT_AMOUNT   = 16;
  public static final int       TDS_PAYMENT_AMOUNT      = 17;
  public static final int       TDS_COUNT               = 18;
  
  private static final String[][] LocationTypes = 
  {
    { "Service Center", "0" },
    { "Chain", "1" },
    { "Receiver", "2" },
  };
  
  private static final String[][] SummaryTypes = 
  {
    { "By Location", "0" },
    { "By Fleet", "1" },
  };
  
  // detail data fuel related column names
  protected static final String[][] DetailFuelColNames =
  {
      // TDI_GALLONS        // TDI_COST_PER_GAL         // TDI_TOTAL_COST
    { "diesel_1_gallons", "diesel_1_cost_per_gallon", "diesel_1_total_cost" },
    { "diesel_2_gallons", "diesel_2_cost_per_gallon", "diesel_2_total_cost" },
    { "reefer_gallons",   "reefer_cost_per_gallon",   "reefer_total_cost" },
    { "other_gallons",    "other_cost_per_gallon",    "other_total_cost" },
    { "oil_quarts",       "oil_cost_per_quart",       "oil_total_cost" },
  };
  
  protected static final String[] DetailNonFuelColNames = 
  {
    "cash_advance_amount",    // TD_NF_CASH_ADVANCE      
    "product_code_1_amount",  // TD_NF_PRODUCT_1_AMOUNT  
    "product_code_2_amount",  // TD_NF_PRODUCT_2_AMOUNT  
    "product_code_3_amount",  // TD_NF_PRODUCT_3_AMOUNT  
  };
  
  protected static final String[] DetailFeeColNames = 
  {
    "discount_amount",        // TD_FEE_DISCOUNT_AMOUNT   
    "fee_amount",             // TD_FEE_FEE_AMOUNT        
  };
  
  protected static final String[] DetailFeeTypeColNames = 
  {
    "discount_type",          // TD_FEE_DISCOUNT_AMOUNT   
    "fee_type",               // TD_FEE_FEE_AMOUNT        
  };
  
  protected static final String[] FuelItemDesc =
  {
    "Diesel 1",
    "Diesel 2",
    "Reefer",
    "Other",
    "Oil",
  };
  
  protected static final String[] NonFuelItemDesc =
  {
    "Cash",
    "Product Code 1",
    "Product Code 2",
    "Product Code 3",
  };
  
  protected static final String[] SummaryDataColNames =
  {
    "diesel_1_gallons",       // TDS_DIESEL_1_GALS    
    "diesel_1_total_cost",    // TDS_DIESEL_1_COST    
    "diesel_2_gallons",       // TDS_DIESEL_2_GALS    
    "diesel_2_total_cost",    // TDS_DIESEL_2_COST    
    "reefer_gallons",         // TDS_REEFER_GALS      
    "reefer_total_cost",      // TDS_REEFER_COST      
    "other_gallons",          // TDS_OTHER_GALS       
    "other_total_cost",       // TDS_OTHER_COST       
    "oil_purchased",          // TDS_OIL_QUARTS       
    "oil_total_cost",         // TDS_OIL_COST         
    "cash_advance_amount",    // TDS_CASH_ADVANCE     
    "product_code_1_amount",  // TDS_PRODUCT_1_AMOUNT 
    "product_code_2_amount",  // TDS_PRODUCT_2_AMOUNT 
    "product_code_3_amount",  // TDS_PRODUCT_3_AMOUNT 
    "discount_amount",        // TDS_DISCOUNT_AMOUNT  
    "fee_amount",             // TDS_FEE_AMOUNT       
    "adjustment_amount",      // TDS_ADJUSTMENT_AMOUNT
    "payment_amount",         // TDS_PAYMENT_AMOUNT   
  };
  
  protected static final String[] DirectBillColNames = 
  {
    "fuel_direct_bill",           // DB_FUEL
    "oil_direct_bill",            // DB_OIL
    "cash_direct_bill",           // DB_CASH
    "product_code_1_direct_bill", // DB_P1
    "product_code_2_direct_bill", // DB_P2
    "product_code_3_direct_bill", // DB_P3
  };
  
  public static final int[] FuelDirectBillMap =
  {
    DB_FUEL,    // TD_DIESEL_1
    DB_FUEL,    // TD_DIESEL_2
    DB_FUEL,    // TD_REEFER 
    DB_FUEL,    // TD_OTHER
    DB_OIL,     // TD_OIL
  };              

  public static final int[] NonFuelDirectBillMap =
  {
    DB_CASH,    // D_NF_CASH_ADVANCE    
    DB_P1,      // D_NF_PRODUCT_1_AMOUNT
    DB_P2,      // D_NF_PRODUCT_2_AMOUNT
    DB_P3,      // D_NF_PRODUCT_3_AMOUNT
  };    
  
  public static final String[] SummaryDataHtmlColNames =
  {
    "DSL1 Gals",    // TDS_DIESEL_1_GALS    
    "DSL1 Cost",    // TDS_DIESEL_1_COST    
    "DSL2 Gals",    // TDS_DIESEL_2_GALS    
    "DSL2 Cost",    // TDS_DIESEL_2_COST    
    "RFR Gals",     // TDS_REEFER_GALS      
    "RFR Cost",     // TDS_REEFER_COST      
    "OT Gals",      // TDS_OTHER_GALS       
    "OT Cost",      // TDS_OTHER_COST       
    "Oil Quarts",   // TDS_OIL_QUARTS       
    "Oil Cost",     // TDS_OIL_COST         
    "Cash",         // TDS_CASH_ADVANCE     
    "$ P1",         // TDS_PRODUCT_1_AMOUNT 
    "$ P2",         // TDS_PRODUCT_2_AMOUNT 
    "$ P3",         // TDS_PRODUCT_3_AMOUNT 
    "$ Disc",       // TDS_DISCOUNT_AMOUNT  
    "$ Fee",        // TDS_FEE_AMOUNT       
    "$ Adj",        // TDS_ADJUSTMENT_AMOUNT
    "$ Pmt",        // TDS_PAYMENT_AMOUNT   
  };
  
  public static class PaidStatusTable extends DropDownTable
  {
    public PaidStatusTable( )
    {
      addElement(String.valueOf(PT_ALL)   ,"All");
      addElement(String.valueOf(PT_PAID)  ,"Paid");
      addElement(String.valueOf(PT_UNPAID),"Unpaid");
    }
  }    
  
  protected static final int[] SummaryCurrencyColumns = 
  {
    TDS_DIESEL_1_COST,
    TDS_DIESEL_2_COST,
    TDS_REEFER_COST,
    TDS_OTHER_COST,
    TDS_OIL_COST,
    TDS_CASH_ADVANCE,
    TDS_PRODUCT_1_AMOUNT,
    TDS_PRODUCT_2_AMOUNT,
    TDS_PRODUCT_3_AMOUNT,
    TDS_DISCOUNT_AMOUNT,
    TDS_FEE_AMOUNT,
    TDS_ADJUSTMENT_AMOUNT,
    TDS_PAYMENT_AMOUNT,
  };
  
  public class SettlementSummaryData
  {
    public long               CheckNumber       = 0L;
    public String             ControlNumber     = null;
    public String             LocationCode      = null;
    public double             PaymentAmount     = 0.0;
    public Timestamp          PaymentCloseTime  = null;
    public Date               PaymentDate       = null;
    public String             ReceiverCode      = null;
    public Date               SettlementDate    = null;
    public long               SettlementGroupId = 0L;
    
    public SettlementSummaryData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CheckNumber       = resultSet.getLong("check_number");
      ControlNumber     = processString(resultSet.getString("control_number"));
      LocationCode      = resultSet.getString("location_code");
      PaymentAmount     = resultSet.getDouble("payment_amount");
      PaymentCloseTime  = resultSet.getTimestamp("close_time");
      PaymentDate       = resultSet.getDate("paid_date");
      ReceiverCode      = resultSet.getString("receiver_code");
      SettlementDate    = resultSet.getDate("settlement_date");
      SettlementGroupId = resultSet.getLong("settlement_group_number");
    }
  }
  
  public class SettlementChainSummaryData
  {
    public String             LocationCity      = null;
    public String             LocationCode      = null;
    public String             LocationName      = null;
    public double             PaymentAmount     = 0.0;
    public int                PaymentCount      = 0;
    public String             ReceiverCode      = null;
    
    public SettlementChainSummaryData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      LocationCity  = resultSet.getString("location_city");
      LocationCode  = resultSet.getString("location_code");
      LocationName  = resultSet.getString("location_name");
      ReceiverCode  = resultSet.getString("receiver_code");
      PaymentCount  = resultSet.getInt("payment_count");
      PaymentAmount = resultSet.getDouble("payment_amount");
    }
  }
  
  public class Location
  {
    public String           LocationCity      = null;
    public String           LocationCode      = null;
    public String           LocationName      = null;
    public String           LocationPhone     = null;
    public long             MerchantId        = 0L;
    
    public Location( ResultSet resultSet )
      throws java.sql.SQLException
    {
      MerchantId    = resultSet.getLong("merchant_number");
      LocationCode  = resultSet.getString("location_code");
      LocationName  = resultSet.getString("location_name");
      
      try
      {
        // attempt to load the city and phone data
        // for this location
        /*@lineinfo:generated-code*//*@lineinfo:381^9*/

//  ************************************************************
//  #sql [Ctx] { select  mfc.city,
//                    substr(mfc.phone1,1,3) || '-' ||
//                    substr(mfc.phone1,4,3) || '-' ||
//                    substr(mfc.phone1,7)
//                              
//            from    mif_comdata mfc
//            where   mfc.merchant_Number = :MerchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mfc.city,\n                  substr(mfc.phone1,1,3) || '-' ||\n                  substr(mfc.phone1,4,3) || '-' ||\n                  substr(mfc.phone1,7)\n                             \n          from    mif_comdata mfc\n          where   mfc.merchant_Number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ComdataTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   LocationCity = (String)__sJT_rs.getString(1);
   LocationPhone = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:390^9*/
      }
      catch( Exception e )
      {
        // ignore
      }
    }
  }
  
  public class RowData extends Location
  {
    public double       AdjustmentAmount        = 0.0;
    public String       CardNumber              = null;
    public double       CashAdvanceAmount       = 0.0;
    public String       ChainCode               = null;
    public String       ControlNumber           = null;
    public String       CustomerCode            = null;
    public int          BatchNumber             = 0;
    public int          BatchTranId             = 0;
    public String[]     DirectBillFlags         = new String[6];
    public double[]     FeeData                 = new double[TD_FEE_COUNT];
    public double       FeeTotal                = 0.0;
    public String[]     FeeType                 = new String[TD_FEE_COUNT];
    public double[][]   FuelData                = new double[TD_COUNT][TDI_COUNT];
    public Date         InvoiceDate             = null;
    public String       InvoiceNumber           = null;
    public double       InvoiceTotal            = 0.0;
    public Timestamp    InvoiceTS               = null;
    public String       LocationCode            = null;
    public double[]     NonFuelData             = new double[TD_NF_COUNT];
    public double       PaymentAmount           = 0.0;
    public String       PaymentCycle            = null;
    public String       PaymentInd              = null;
    public String       PaymentMethod           = null;
    public String       PaymentPending          = null;
    public String       PaymentType             = null;
    public String[]     ProductDesc             = new String[3];
    public String       SettlementControlNumber = "";
    public Date         SettlementDate          = null;
    public long         SettlementGroupId       = 0L;
    public String       TranType                = null;
    
    public RowData( ResultSet resultSet, int reportType )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      switch( reportType )
      {
        case RT_DETAILS:
          CardNumber      = processString( resultSet.getString("card_number") );
          BatchNumber     = resultSet.getInt("batch_number");
          BatchTranId     = resultSet.getInt("batch_tran_id");
          PaymentMethod   = processString( resultSet.getString("payment_method") );
          PaymentType     = processString( resultSet.getString("payment_type") );
          break;
          
        case RT_PAID_DETAILS:
          SettlementControlNumber = processString( resultSet.getString("settlement_control_number") );
          SettlementDate          = resultSet.getDate("settlement_date");
          SettlementGroupId       = resultSet.getLong("settlement_group_number");
          break;
      }        
      
      LocationCode    = processString( resultSet.getString("location_code") );
      ChainCode       = processString( resultSet.getString("chain_code") );
      InvoiceNumber   = processString( resultSet.getString("invoice_number") );
      ControlNumber   = processString( resultSet.getString("control_number") );
      InvoiceDate     = resultSet.getDate("invoice_date");
      CustomerCode    = processString( resultSet.getString("customer_account_code") );
      InvoiceTS       = resultSet.getTimestamp("invoice_time");
      AdjustmentAmount= resultSet.getDouble("adjustment_amount");
      PaymentAmount   = resultSet.getDouble("payment_amount");
      PaymentCycle    = processString( resultSet.getString("payment_cycle") );
      PaymentInd      = processString( resultSet.getString("paid_ind") );
      PaymentPending  = processString( resultSet.getString("payment_pending") );
      TranType        = processString( resultSet.getString("tran_type") );
      
      for( int i = 0; i < DirectBillColNames.length; ++i )
      {
        DirectBillFlags[i] = processString( resultSet.getString(DirectBillColNames[i]) );
      }
     
      // extract the 3 product code descriptions 
      for( int i = 0; i < 3; ++i )
      {
        ProductDesc[i] = processString( resultSet.getString(("product_code_" + (i+1))) );
      }
      
      // load the fuel related transaction data
      for( int td = 0; td < TD_COUNT; ++td )
      {
        for( int tdi = 0; tdi < TDI_COUNT; ++tdi )
        {
          FuelData[td][tdi] = resultSet.getDouble(DetailFuelColNames[td][tdi]);
        }
        InvoiceTotal += FuelData[td][TDI_TOTAL_COST];
      }
      
      // load the non-fuel amounts
      for( int td = 0; td < TD_NF_COUNT; ++td )
      {
        NonFuelData[td] = resultSet.getDouble(DetailNonFuelColNames[td]);
        InvoiceTotal   += NonFuelData[td];
      }
      
      // load the fee amounts
      for( int td = 0; td < TD_FEE_COUNT; ++td )
      {
        FeeData[td] = resultSet.getDouble(DetailFeeColNames[td]);
        FeeType[td] = processString( resultSet.getString(DetailFeeTypeColNames[td]) );
        FeeTotal   += FeeData[td];
      }
    }
  }
  
  public class SummaryData extends Location
  {
    public Date             BatchDate         = null;
    public int              BatchNumber       = 0;
    public String           ChainCode         = null;
    public String           CorpCode          = null;
    public double[]         SummaryData       = new double[TDS_COUNT];
    
    public SummaryData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      CorpCode      = processString( resultSet.getString("corp_code") );
      ChainCode     = processString( resultSet.getString("chain_code") );
      
      // if summarizing by batch (settlement) then
      // extract the batch number and date 
      if ( resultSet.getInt("by_batch") == 1 )
      {
        BatchDate   = resultSet.getDate("batch_date");
        BatchNumber = resultSet.getInt("batch_number"); 
      }
      
      // initialize array
      for( int tds = 0; tds < SummaryData.length; ++tds )
      {
        SummaryData[tds] = 0.0;
      }
      
      addBatch( resultSet );
    }
    
    public void addBatch( ResultSet resultSet )
      throws java.sql.SQLException
    {
      for( int tds = 0; tds < TDS_COUNT; ++tds )
      {
        SummaryData[tds] += resultSet.getDouble(SummaryDataColNames[tds]);
      }
    }
    
    public double getAdjustmentAmount( )
    {              
      return( SummaryData[TDS_ADJUSTMENT_AMOUNT] );
    }
    
    public double getAmount( int amountId )
    {
      double    retVal    = 0.0;
      
      try
      {
        retVal = SummaryData[amountId];
      }
      catch( Exception e )
      {
        logEntry("getAmount(" + amountId + ")", e.toString());
      }
      return( retVal );
    }
    
    public double getCashAmount( )
    {
      return( getAmount(TDS_CASH_ADVANCE) );
    }
    
    public double getDiscAmount( )
    {
      return( SummaryData[TDS_DISCOUNT_AMOUNT] );
    }
    
    public double getFeesAmount( )
    {
      return( SummaryData[TDS_FEE_AMOUNT] );
    }
    
    public double getFuelAmount( )
    {
      return( SummaryData[TDS_DIESEL_1_COST ] +
              SummaryData[TDS_DIESEL_2_COST ] +
              SummaryData[TDS_REEFER_COST   ] +
              SummaryData[TDS_OTHER_COST    ] );
    }
    
    public double getFuelQuantity( )
    {
      return( SummaryData[TDS_DIESEL_1_GALS] +
              SummaryData[TDS_DIESEL_2_GALS] +
              SummaryData[TDS_REEFER_GALS  ] +
              SummaryData[TDS_OTHER_GALS   ] );
    }              
    
    public double getInvoiceAmount( )
    {
      return( SummaryData[TDS_DIESEL_1_COST    ] +
              SummaryData[TDS_DIESEL_2_COST    ] +
              SummaryData[TDS_REEFER_COST      ] +
              SummaryData[TDS_OTHER_COST       ] +
              SummaryData[TDS_OIL_COST         ] +
              SummaryData[TDS_CASH_ADVANCE     ] +
              SummaryData[TDS_PRODUCT_1_AMOUNT ] +
              SummaryData[TDS_PRODUCT_2_AMOUNT ] +
              SummaryData[TDS_PRODUCT_3_AMOUNT ] );
    }
    
    public double getNonFuelAmount( )
    {
      return( SummaryData[TDS_OIL_COST         ] +
              SummaryData[TDS_CASH_ADVANCE     ] +
              SummaryData[TDS_PRODUCT_1_AMOUNT ] +
              SummaryData[TDS_PRODUCT_2_AMOUNT ] +
              SummaryData[TDS_PRODUCT_3_AMOUNT ] );
    }
    
    public double getOtherAmount( )
    {
      return( getAmount( TDS_OTHER_COST ) );
    }
    
    public double getPaymentAmount( )
    {
      return(SummaryData[TDS_PAYMENT_AMOUNT]);
    }
  }
  
  public class BatchTotalsRecord
  {
    public double       EquipmentCharge     = 0.0;
    public double       ExcessCharge        = 0.0;
    public double       MiscCharge          = 0.0;
    public double       ServiceCharge       = 0.0;
    
    public BatchTotalsRecord( )
    {
    }
    
    public BatchTotalsRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      setData(resultSet);
    }
    
    public void setData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      EquipmentCharge = resultSet.getDouble("equip_charge");
      ExcessCharge    = resultSet.getDouble("excess_charge");
      ServiceCharge   = resultSet.getDouble("service_charge");
      MiscCharge      = resultSet.getDouble("misc_charge");
    }
  }
  
  public class GroupTotalsRecord
  {
    public double             AdjustmentTotal     = 0.0;
    public long               CheckNumber         = 0L;
    public String             ControlNumber       = null;
    public String             CorpChainCode       = null;
    public double             CostPlusAmount      = 0.0;
    public double             EquipmentCharge     = 0.0;
    public double             ExcessCharge        = 0.0;
    public double             FocusAmount         = 0.0;
    public String             LocationCode        = null;
    public double             MiscCharge          = 0.0;
    public Date               PaidDate            = null;
    public double             PaymentAmount       = 0.0;
    public Timestamp          PaymentCloseTime    = null;
    public String             ReceiverCode        = null;
    public long               SettlementGroup     = 0L;
    public double[][]         SegmentTotals       = null;
    public double             SelectAmount        = 0.0;
    public double             ServiceCharge       = 0.0;
    
    public GroupTotalsRecord()
    {
    }
    
    public GroupTotalsRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      setData(resultSet);
    }
    
    public void setData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      String[]    prefixes  = { 
                                "db",
                                "funded",
                                "gross",
                                "pend",
                                "translink",
                                "pref_fund",
                                "pref_db",
                                "credit" 
                              };
                              
      SettlementGroup   = resultSet.getLong("settlement_group");
      PaidDate          = resultSet.getDate("paid_date");
      LocationCode      = processString( resultSet.getString("location_code") );
      ReceiverCode      = processString( resultSet.getString("receiver_code") );
      CorpChainCode     = processString( resultSet.getString("corp_chain_code") );
      PaymentCloseTime  = resultSet.getTimestamp("close_time");
      ControlNumber     = processString( resultSet.getString("control_number") );
      CheckNumber       = resultSet.getLong("check_number");
      
      EquipmentCharge   = resultSet.getDouble("equip_charge");
      ExcessCharge      = resultSet.getDouble("settle_charge");
      ServiceCharge     = resultSet.getDouble("service_charge");
      MiscCharge        = resultSet.getDouble("misc_charge");
      
      PaymentAmount     = resultSet.getDouble("payment_amount");
      
      SelectAmount      = resultSet.getDouble("select_total");
      FocusAmount       = resultSet.getDouble("focus_total");
      CostPlusAmount    = resultSet.getDouble("cost_plus_total");
      
      AdjustmentTotal   = resultSet.getDouble("adj_total");
                              
      SegmentTotals = new double[prefixes.length][TGI_COUNT];
      for( int i = 0; i < prefixes.length; ++i )
      {
        SegmentTotals[i][TCI_GALLONS]    = resultSet.getDouble( prefixes[i] + "_gallons" );
        SegmentTotals[i][TCI_AMOUNT]     = resultSet.getDouble( prefixes[i] + "_amount" );
        SegmentTotals[i][TCI_INVOICE]    = resultSet.getDouble( prefixes[i] + "_invoice" );
        SegmentTotals[i][TCI_DISCOUNT]   = resultSet.getDouble( prefixes[i] + "_discount" );
        SegmentTotals[i][TCI_FEES]       = resultSet.getDouble( prefixes[i] + "_fees" );
        SegmentTotals[i][TCI_PAYMENT]    = resultSet.getDouble( prefixes[i] + "_payment" );
        SegmentTotals[i][TCI_PROD_TOTAL] = resultSet.getDouble( prefixes[i] + "_product_total" );
      }
    }
  }
  
  public class VolumeByLocationSummary extends Location
  {
    public String           AccountCode   = null;
    public double           CashAmount    = 0.0;
    public String           ChainCode     = null;
    public String           CorpCode      = null;
    public String           FleetCode     = null;
    public String           FleetName     = null;
    public double           FuelAmount    = 0.0;
    public double           FuelQuantity  = 0.0;
    public double           OtherAmount   = 0.0;
    public double           TotalAmount   = 0.0;
    public int              TranCount     = 0;
    
    public VolumeByLocationSummary( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      AccountCode   = processString( resultSet.getString("account_code") );
      
      CorpCode      = processString( resultSet.getString("corp_code") );
      ChainCode     = processString( resultSet.getString("chain_code") );
      
      // this object serves double duty, both to
      // summarize volume by location and... 
      // ...volume by fleet
      FleetCode     = processString( resultSet.getString("fleet_code") );
      FleetName     = processString( resultSet.getString("fleet_name") );
      
      // add the volume for this fleet
      addData(resultSet);
    }
    
    public void addData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      // these columns contain data for the rows that have
      // an account number.  they are used by the summary
      // level volume by truck stop reporting.
      TranCount     += resultSet.getInt("tran_count");
      FuelAmount    += resultSet.getDouble("fuel_cost");
      FuelQuantity  += resultSet.getDouble("fuel_quantity");
      CashAmount    += resultSet.getDouble("cash_amount");
      OtherAmount   += resultSet.getDouble("other_amount");
      
      // update the new total amount based on the 
      // incremented individual amounts.
      TotalAmount   = (FuelAmount + OtherAmount + CashAmount);
    }
  }
  
  public class VolumeByLocation extends Location
  {
    public String               AccountCode         = null;
    public double               CashAmount          = 0.0;
    public String               FleetCity           = null;
    public String               FleetCode           = null;
    public String               FleetName           = null;
    public String               FleetPhone          = null;
    public double               FocusAmount         = 0.0;
    public double               FuelAmount          = 0.0;
    public double               FuelGrossPrice      = 0.0;
    public double               FuelNetPrice        = 0.0;
    public double               FuelQuantity        = 0.0;
    public double               OtherAmount         = 0.0;
    public double               SelectAmount        = 0.0;
    public String               TFlag               = null;
    public double               TotalAmount         = 0.0;
    public int                  TranCount           = 0;
  
    public VolumeByLocation( ResultSet resultSet )  
      throws java.sql.SQLException
    {
      super( resultSet );
    
      AccountCode         = processString( resultSet.getString("acct_code") );
      CashAmount          = resultSet.getDouble("cash_amount");
      FleetCity           = processString( resultSet.getString("fleet_city") );
      FleetCode           = processString( resultSet.getString("fleet_code") );
      FleetName           = processString( resultSet.getString("fleet_name") );
      FleetPhone          = processString( resultSet.getString("fleet_phone") );
      FuelAmount          = resultSet.getDouble("fuel_cost");
      FuelQuantity        = resultSet.getDouble("fuel_quantity");
      OtherAmount         = resultSet.getDouble("other_amount");
      TFlag               = processString( resultSet.getString("t_flag") );
      TotalAmount         = (FuelAmount + OtherAmount + CashAmount);
      TranCount           = resultSet.getInt("tran_count");
      
      try
      {
        FocusAmount = resultSet.getDouble("focus_amount");
        SelectAmount = resultSet.getDouble("select_amount");
      }
      catch(Exception e)
      {
      }        
      
      if ( FuelQuantity > 0.0 )
      {
        FuelGrossPrice = ( FuelAmount/FuelQuantity );
        FuelNetPrice   = ((FuelAmount - (SelectAmount + FocusAmount))/FuelQuantity);
      }        
    }
  }
  
  // member variables
  protected int             DetailBatchNumber   = 0;
  protected boolean         SummaryByBatch      = false;
  
  public ComdataTranDataBean( )
  {
    super(true);    // use FieldBean support
  }
  
  protected void addFleetData( ResultSet resultSet )
  {
    String                        chain   = null;
    VolumeByLocationSummary       dataObj = null;
    String                        fleet   = null;
    VolumeByLocationSummary       temp    = null;
    
    try
    {
      chain = processString( resultSet.getString("chain_code") );
      fleet = processString( resultSet.getString("fleet_code") );
      
      for( int i = 0; i < ReportRows.size(); ++i )
      {
        temp = (VolumeByLocationSummary)ReportRows.elementAt(i);
        
        // summary is by corp by fleet
        if ( temp.ChainCode.equals(chain) &&
             temp.FleetCode.equals(fleet) )
        {
          dataObj = temp;
          break;
        }
      }
      
      // did not find an existing summary
      // object for this fleet code, create one
      if ( dataObj == null )
      {
        dataObj = new VolumeByLocationSummary( resultSet );
        ReportRows.addElement(dataObj);
      }
      else
      {
        // add the data to the existing object
        dataObj.addData(resultSet);
      }
    }
    catch(Exception e)
    {
      logEntry("addFleetData()",e.toString());
    }
    finally
    {
    }
  }
  
  protected void addLocationData( ResultSet resultSet )
  {
    String                        loc     = null;
    VolumeByLocationSummary       dataObj = null;
    VolumeByLocationSummary       temp    = null;
    
    try
    {
      loc = resultSet.getString("location_code");
      
      for( int i = 0; i < ReportRows.size(); ++i )
      {
        temp = (VolumeByLocationSummary)ReportRows.elementAt(i);
        
        if ( temp.LocationCode.equals(loc) )
        {
          dataObj = temp;
          break;
        }
      }
      
      // did not find an existing summary
      // object for this fleet code, create one
      if ( dataObj == null )
      {
        dataObj = new VolumeByLocationSummary( resultSet );
        ReportRows.addElement(dataObj);
      }
      else    
      {
        dataObj.addData(resultSet);
      }
    }
    catch(Exception e)
    {
      logEntry("addLocationData()",e.toString());
    }
    finally
    {
    }
  }
  
  protected void addSearchItem( ResultSet resultSet, int reportType )
  {
    String    controlNumber   = null;
    RowData   dataObj         = null;
    Date      invoiceDate     = null;
    RowData   temp            = null;
    
    try
    {
      controlNumber = processString(resultSet.getString("control_number"));
      invoiceDate   = resultSet.getDate("invoice_date");
      
      for( int i = 0; i < ReportRows.size(); ++i )
      {
        temp = (RowData)ReportRows.elementAt(i);
        
        if ( temp.ControlNumber.equals(controlNumber) &&
             temp.InvoiceDate.equals(invoiceDate) )
        {
          dataObj = temp;
          break;
        }
      }
      
      // did not find an existing entry, so add this one
      if ( dataObj == null )
      {
        dataObj = new RowData( resultSet, reportType );
        ReportRows.addElement(dataObj);
      }
      // else, newer entry exists, keep it
    }
    catch(Exception e)
    {
      logEntry("addSearchItem()",e.toString());
    }
    finally
    {
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    Field         field;
    int           reportType = HttpHelper.getInt(request,"reportType",-1);
    
    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");
    fgroup.deleteField("beginDate");
    fgroup.deleteField("endDate");
    fgroup.deleteField("portfolioId");
    fgroup.deleteField("zip2Node");
    
    switch( reportType )
    {
      case RT_VOL_BY_LOC_DETAILS:
        fgroup.add( new HiddenField("summaryType") );
        fgroup.add( new HiddenField("summaryCode") );
        break;
        
      case RT_PAID_SUMMARY:
      case RT_PAID_DETAILS:
        field = new ComboDateField("beginDate", "Start Date", true, false, 2000, false);
        ((ComboDateField)field).setExpansionFlag(false);
        ((ComboDateField)field).setDayOfWeekVisibleFlag(true);
        fgroup.add(field);
        field = new ComboDateField("endDate", "End Date", true, false, 2000, false);
        ((ComboDateField)field).setExpansionFlag(false);
        ((ComboDateField)field).setDayOfWeekVisibleFlag(true);
        fgroup.add( field );
        fgroup.add( new Field("locationCode", "Location Code (required)", 5,5, false ) );
        fgroup.add( new RadioButtonField("locationType","",LocationTypes,0,false,"",RadioButtonField.LAYOUT_SPEC_HORIZONTAL) );
        fgroup.add( new Field("controlNumber", "Control Number (optional)", 9,9, true ) );
        fgroup.add( new Field("checkNumber", "Check Number (optional)", 9,9, true ) );
        fgroup.add( new HiddenField("settleGroup") );
        break;
        
      case RT_ITEM_SEARCH:
        field = new ComboDateField("beginDate", "Start Date", true, false, 2000, false);
        ((ComboDateField)field).setExpansionFlag(false);
        ((ComboDateField)field).setDayOfWeekVisibleFlag(true);
        fgroup.add(field);
        field = new ComboDateField("endDate", "End Date", true, false, 2000, false);
        ((ComboDateField)field).setExpansionFlag(false);
        ((ComboDateField)field).setDayOfWeekVisibleFlag(true);
        fgroup.add( field );
        fgroup.add( new Field("locationCode", "Location Code (required)", 5,5, false ) );
        fgroup.add( new RadioButtonField("locationType","",LocationTypes,0,false,"",RadioButtonField.LAYOUT_SPEC_HORIZONTAL) );
        fgroup.add( new NumberField("tranControlNumber", "Comdata Transaction Number (optional; last 6 digits)", 6,6, true,0 ) );
        fgroup.add( new NumberField("invoiceNumber", "Invoice Number (optional)", 9,9, true,0 ) );
        fgroup.add( new NumberField("searchAmount", "Amount (optional)", 9,9, true,2 ) );
        fgroup.add( new DropDownField("paidStatus", "Paid Status", new PaidStatusTable(),true) );
        break;
      
      default:
        fgroup.add( new RadioButtonField("summaryType","Summary Type",SummaryTypes,0,false,"") );
        fgroup.add( new Field("summaryCode", "Location or Chain Code", 5,5, false ) );
        break;
    }      
    
    if ( fgroup.getField("beginDate") == null )
    {
      // default is a single report date 
      field = new ComboDateField("beginDate", "Report Date", false, false, 2000, false);
      ((ComboDateField)field).setExpansionFlag(false);
      fgroup.add(field);
    }
    
    fgroup.add( new HiddenField("loc") );
    fgroup.add( new HiddenField("chain") );
    fgroup.add( new HiddenField("corp") );
    fgroup.add( new HiddenField("fleetCode") );
    
    // set the field defaults
    Calendar    cal = Calendar.getInstance();
    switch( reportType )
    {
      case RT_VOL_BY_LOC_SUMMARY:
      case RT_VOL_BY_LOC_DETAILS:
      case RT_VOL_BY_ACCT_SUMMARY:
        cal.add(Calendar.MONTH,-1);
        cal.set(Calendar.DAY_OF_MONTH,1);
        ((ComboDateField)getField("beginDate")).setUtilDate(cal.getTime());
        break;
        
      case RT_PAID_SUMMARY:
      case RT_PAID_DETAILS:
      case RT_ITEM_SEARCH:
        cal.add(Calendar.DAY_OF_MONTH,-1);
        ((ComboDateField)getField("beginDate")).setUtilDate(cal.getTime());
        ((ComboDateField)getField("endDate")).setUtilDate(cal.getTime());
        break;
        
      default:
        break;
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    int   reportType    = getReportType();
    
    line.setLength(0);
    
    if ( reportType == RT_DETAILS )
    {
      line.append("\"Account Code\",");
      line.append("\"Invoice #\",");
      line.append("\"Control #\",");
      line.append("\"Invoice Date\",");
      line.append("\"Tran Type\",");
      line.append("\"Payment Type\",");
      line.append("\"Payment Cycle\",");
      line.append("\"Card Number\",");
      line.append("\"Description\",");
      line.append("\"Quantity\",");
      line.append("\"Price\",");
      line.append("\"Amount\",");
      line.append("\"Invoice Total\",");
      line.append("\"Fees\",");
      line.append("\"Adjustments\",");
      line.append("\"Payment\",");
      line.append("\"Payment Method\"");
      
    }
    else if ( reportType == RT_VOL_BY_LOC_DETAILS )
    {
      line.append("\"Loc Code\",");
      line.append("\"Loc Name\",");
      line.append("\"Acct Code\",");
      line.append("\"Fleet/Company Name\",");
      line.append("\"Fleet/Company City\",");
      line.append("\"Fleet/Company Phone\",");
      line.append("\"Tran Count\",");
      line.append("\"Quantity\",");
      line.append("\"Gross Price\",");
      line.append("\"Total Fuel Cost\",");
      line.append("\"Select\",");
      line.append("\"Focus\",");
      line.append("\"Net Price\",");
      line.append("\"Total Cash\",");
      line.append("\"Total Other\",");
      line.append("\"Total Dollars\",");
      line.append("\"T\"");
    }
    else if ( reportType == RT_VOL_BY_LOC_SUMMARY )
    {
//@      line.append("\"Corp Code\",");
//@      line.append("\"Chain Code\",");
      if ( getData("summaryType").equals("1") )
      {
        line.append("\"Fleet Code\",");
        line.append("\"Fleet Name\",");
      }
      else
      {
        line.append("\"Loc Code\",");
        line.append("\"Loc Name\",");
      }        
      line.append("\"Tran Count\",");
      line.append("\"Quantity\",");
      line.append("\"Total Fuel Cost\",");
      line.append("\"Total Cash\",");
      line.append("\"Total Other\",");
      line.append("\"Total Dollars\"");
    }
    else if ( reportType == RT_VOL_BY_ACCT_SUMMARY )
    {
      line.append("\"Fleet Code\",");
      line.append("\"Account Code\",");
      line.append("\"Account Name\",");
      line.append("\"Tran Count\",");
      line.append("\"Quantity\",");
      line.append("\"Total Fuel Cost\",");
      line.append("\"Total Cash\",");
      line.append("\"Total Other\",");
      line.append("\"Total Dollars\"");
    }
    else if ( reportType == RT_PAID_SUMMARY ) 
    {
      switch( getInt("locationType",0) )
      {
        case LT_SERVICE_CENTER:
        case LT_RECEIVER:
          line.append("\"Location\",");
          line.append("\"Receiver\",");
          line.append("\"Paid Date\",");
          line.append("\"Close Time\",");
          line.append("\"Control Number\",");
          line.append("\"Check Number\",");
          line.append("\"Amount\"");
          break;
        
        case LT_CHAIN:
          line.append("\"Location\",");
          line.append("\"Name\",");
          line.append("\"City\",");
          line.append("\"Receiver\",");
          line.append("\"Payment Count\",");
          line.append("\"Payment Total\"");
          break;
      }
    }
    else if ( reportType == RT_PAID_DETAILS )
    {
      line.append("\"Settlement Details\",\"");
      line.append( getDisplayName() );
      line.append("\",,,,,,,,,,,,,,,,\n");
      line.append("\"Account Code\",");
      line.append("\"Invoice #\",");
      line.append("\"Invoice Date\",");
      line.append("\"Control #\",");
      line.append("\"Description\",");
      line.append("\"Quantity\",");
      line.append("\"Price\",");
      line.append("\"Amount\",");
      line.append("\"DB\",");
      line.append("\"Invoice Total\",");
      line.append("\"Discount Amount\",");
      line.append("\"Discount Type\",");
      line.append("\"Fee Amount\",");
      line.append("\"Fee Type\",");
      line.append("\"Payment Amount\",");
      line.append("\"Payment Cycle\",");
      line.append("\"Payment Pending\",");
      line.append("\"Payment Type\"");
    }
    else if ( reportType == RT_ITEM_SEARCH )
    {
      line.append("\"Account Code\",");
      line.append("\"Chain Code\",");
      line.append("\"Loc Code\",");
      line.append("\"Invoice #\",");
      line.append("\"Invoice Date\",");
      line.append("\"Tran Control #\",");
      line.append("\"Description\",");
      line.append("\"Quantity\",");
      line.append("\"Price\",");
      line.append("\"Amount\",");
      line.append("\"DB\",");
      line.append("\"Invoice Total\",");
      line.append("\"Discount Amount\",");
      line.append("\"Discount Type\",");
      line.append("\"Settlement Control #\",");
      line.append("\"Paid\"");
    }
    else    // RT_SUMMARY
    {
      line.append("\"Corp Code\",");
      line.append("\"Chain Code\",");
      line.append("\"Loc Code\",");
      line.append("\"Loc Name\",");
      line.append("\"Fuel Qty\",");
      line.append("\"$ Fuel\",");
      line.append("\"$ Non-Fuel\",");
      line.append("\"$ Invoice\",");
      line.append("\"$ Disc\",");
      line.append("\"$ Fees\",");
      line.append("\"$ Adj\",");
      if ( SummaryByBatch )
      {
        line.append("\"Settlement Date\",");
      }        
      line.append("\"$ Payment\",");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    int     reportType      = getReportType();
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    if ( reportType == RT_DETAILS )
    {
      String    desc        = null;
      int       fuelIdx     = 0;
      int       nonFuelIdx  = 0;
      RowData   record      = (RowData)obj;
      
      line.append(record.CustomerCode);
      line.append(",\"");
      line.append(record.InvoiceNumber);
      line.append("\",\"");
      line.append(record.ControlNumber);
      line.append("\",");
      line.append(DateTimeFormatter.getFormattedDate(record.InvoiceTS,"MM/dd/yyyy HH:mm"));
      line.append(",");
      line.append(record.TranType);
      line.append(",");
      line.append(record.PaymentType);
      line.append(",");
      line.append(record.PaymentCycle);
      line.append(",");
      line.append(record.CardNumber);
      line.append(",");
        
      for( fuelIdx = 0; fuelIdx < TD_COUNT; ++fuelIdx )
      {
        // locate the first entry with data
        if ( record.FuelData[fuelIdx][TDI_TOTAL_COST] != 0.0 )
        {
          break;
        }
      }
      if ( fuelIdx < TD_COUNT )
      {
        line.append("\"");
        line.append(getDescFuelItem(fuelIdx));
        line.append("\",");
        line.append(NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_GALLONS],"#########0.00"));
        line.append(",");
        line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_COST_PER_GAL],"#########0.00") );
        line.append(",");
        line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_TOTAL_COST],"#########0.00") );
      }
      else    // no fuel, find first non-fuel item
      {
        for( nonFuelIdx = 0; nonFuelIdx < TD_NF_COUNT; ++nonFuelIdx )
        {
          desc = getDescNonFuelItem(nonFuelIdx);
        
          if ( desc != null && record.NonFuelData[nonFuelIdx] != 0.0 )
          {
            break;
          }
        }
        if ( nonFuelIdx < TD_NF_COUNT )
        {
          line.append(desc);
          line.append(",");
          line.append(",");
          line.append(",");
          line.append(NumberFormatter.getDoubleString(record.NonFuelData[nonFuelIdx],"#########0.00"));
        }
        else  // no detail, must be a service fee or like item
        {
          line.append(",,,");
          line.append(NumberFormatter.getDoubleString(record.InvoiceTotal,"#########0.00"));
        }
      }
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.InvoiceTotal,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FeeTotal,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.AdjustmentAmount,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.PaymentAmount,"#########0.00"));
      line.append(",");
      line.append(record.PaymentMethod);

      // add additional rows for detail items (if necessary)
      while( ++fuelIdx < TD_COUNT )
      {
        if ( record.FuelData[fuelIdx][TDI_TOTAL_COST] != 0.0 )
        {
          line.append("\n,,,,,,,,");
          line.append("\"");
          line.append(getDescFuelItem(fuelIdx));
          line.append("\",");
          line.append(NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_GALLONS],"#########0.00"));
          line.append(",");
          line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_COST_PER_GAL],"#########0.00") );
          line.append(",");
          line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_TOTAL_COST],"#########0.00") );
          line.append(",,,,,");
        }
      }
      
      while( ++nonFuelIdx < TD_NF_COUNT )
      {
        desc = getDescNonFuelItem(nonFuelIdx);
      
        if ( desc != null && record.NonFuelData[nonFuelIdx] != 0.0 )
        {
          line.append("\n,,,,,,,,");
          line.append("\"");
          line.append(desc);
          line.append(",");
          line.append(",");
          line.append(",");
          line.append(NumberFormatter.getDoubleString(record.NonFuelData[nonFuelIdx],"#########0.00"));
          line.append(",,,,,");
        }            
      }
    }
    else if ( reportType == RT_VOL_BY_LOC_DETAILS ) 
    {
      VolumeByLocation        record = (VolumeByLocation)obj;
      
      line.append("\""); 
      line.append(record.LocationCode);
      line.append("\",\""); 
      line.append(record.LocationName);
      line.append("\",\"");
      line.append(record.AccountCode);
      line.append("\",\"");
      line.append(record.FleetName);
      line.append("\",\"");
      line.append(record.FleetCity);
      line.append("\",\"");
      line.append(record.FleetPhone);
      line.append("\",");
      line.append(record.TranCount);
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FuelQuantity,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FuelGrossPrice,"#########0.000"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FuelAmount,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.SelectAmount,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FocusAmount,"#########0.00"));
      line.append(",");                                                            
      line.append(NumberFormatter.getDoubleString(record.FuelNetPrice,"#########0.000"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.CashAmount,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.OtherAmount,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.TotalAmount,"#########0.00"));
      line.append(",\"");
      line.append(record.TFlag);
      line.append("\"");
    }
    else if ( reportType == RT_VOL_BY_LOC_SUMMARY ) 
    {
      VolumeByLocationSummary record = (VolumeByLocationSummary)obj;
    
//@      line.append(record.CorpCode);
//@      line.append(",");
//@      line.append(record.ChainCode);
//@      line.append(",");
      
      if ( getData("summaryType").equals("1") )
      {
        line.append(record.FleetCode);
        line.append(",");
        line.append("\"");
        line.append(record.FleetName);
        line.append("\",");
      }
      else
      {
        line.append(record.LocationCode);
        line.append(",");
        line.append("\"");
        line.append(record.LocationName);
        line.append("\",");
      }
      line.append(record.TranCount);
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FuelQuantity,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FuelAmount,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.CashAmount,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.OtherAmount,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString((record.FuelAmount + record.CashAmount + record.OtherAmount),"#########0.00"));
    }
    else if ( reportType == RT_VOL_BY_ACCT_SUMMARY ) 
    {
      VolumeByLocationSummary record = (VolumeByLocationSummary)obj;
    
      line.append(record.FleetCode);
      line.append(",");
      line.append(record.AccountCode);
      line.append(",");
      line.append("\"");
      line.append(record.FleetName);
      line.append("\",");
      line.append(record.TranCount);
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FuelQuantity,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FuelAmount,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.CashAmount,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.OtherAmount,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString((record.FuelAmount + record.CashAmount + record.OtherAmount),"#########0.00"));
    }
    else if ( reportType == RT_PAID_SUMMARY ) 
    {
      switch( getInt("locationType") )
      {
        case LT_SERVICE_CENTER:
        case LT_RECEIVER:
        {
          SettlementSummaryData record = (SettlementSummaryData)obj;
          line.append("\"");
          line.append(record.LocationCode);
          line.append("\",\"");
          line.append(record.ReceiverCode);
          line.append("\",");
          line.append( DateTimeFormatter.getFormattedDate( record.PaymentDate,"MM/dd/yyyy" ) );
          line.append(",\"");
          line.append( DateTimeFormatter.getFormattedDate( record.PaymentCloseTime,"MM/dd/yyyy HH:mm:ss" ) );
          line.append("\",");
          line.append( record.ControlNumber );          
          line.append(",");
          line.append( record.CheckNumber );          
          line.append(",");
          line.append( record.PaymentAmount );
          break;
        }
        
        case LT_CHAIN:
        {
          SettlementChainSummaryData record = (SettlementChainSummaryData)obj;
          
          line.append("\"");
          line.append(record.LocationCode);
          line.append("\",\"");
          line.append(record.LocationName);
          line.append("\",\"");
          line.append(record.LocationCity);
          line.append("\",\"");
          line.append(record.ReceiverCode);
          line.append("\",");
          line.append(record.PaymentCount);
          line.append(",");
          line.append(record.PaymentAmount);
          break;
        }
      }
    }
    else if ( reportType == RT_PAID_DETAILS )
    {
      String    desc        = null;
      int       fuelIdx     = -1;
      int       nonFuelIdx  = -1;
      RowData   record      = (RowData)obj;
      
      line.append(record.CustomerCode);
      line.append(",\"");
      line.append(record.InvoiceNumber);
      line.append("\",\"");
      line.append(DateTimeFormatter.getFormattedDate(record.InvoiceTS,"MM/dd/yyyy HH:mm"));
      line.append("\",\"");
      line.append(record.ControlNumber);
      line.append("\",");
        
      for( fuelIdx = 0; fuelIdx < TD_COUNT; ++fuelIdx )
      {
        // locate the first entry with data
        if ( record.FuelData[fuelIdx][TDI_TOTAL_COST] != 0.0 )
        {
          break;
        }
      }
      if ( fuelIdx < TD_COUNT )
      {
        line.append("\"");
        line.append(getDescFuelItem(fuelIdx));
        line.append("\",");
        line.append(NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_GALLONS],"#########0.00"));
        line.append(",");
        line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_COST_PER_GAL],"#########0.00") );
        line.append(",");
        line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_TOTAL_COST],"#########0.00") );
        line.append(",\"");
        line.append(record.DirectBillFlags[ FuelDirectBillMap[fuelIdx] ]);
        line.append("\"");
      }
      else    // no fuel, find first non-fuel item
      {
        for( nonFuelIdx = 0; nonFuelIdx < TD_NF_COUNT; ++nonFuelIdx )
        {
          desc = getDescNonFuelItem(nonFuelIdx);
        
          if ( desc != null && record.NonFuelData[nonFuelIdx] != 0.0 )
          {
            break;
          }
        }
        if ( nonFuelIdx < TD_NF_COUNT )
        {
          line.append(desc);
          line.append(",");
          line.append(",");
          line.append(",");
          line.append(NumberFormatter.getDoubleString(record.NonFuelData[nonFuelIdx],"#########0.00"));
          line.append(",\"");
          line.append(record.DirectBillFlags[ NonFuelDirectBillMap[nonFuelIdx] ]);
          line.append("\"");
        }
        else  // no detail, must be a service fee or like item
        {
          line.append(",,,");
          line.append(NumberFormatter.getDoubleString(record.InvoiceTotal,"#########0.00"));
          line.append(",");
          // no direct bill ?
        }
      }
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.InvoiceTotal,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FeeData[TD_FEE_DISCOUNT_AMOUNT],"#########0.00"));
      line.append(",");
      line.append(record.FeeType[TD_FEE_DISCOUNT_AMOUNT]);
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FeeData[TD_FEE_FEE_AMOUNT],"#########0.00"));
      line.append(",");
      line.append(record.FeeType[TD_FEE_FEE_AMOUNT]);
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.PaymentAmount,"#########0.00"));
      line.append(",\"");
      line.append(record.PaymentCycle);
      line.append("\",\"");
      line.append(record.PaymentPending);
      line.append("\",\"");
      line.append(record.TranType);
      line.append("\"");

      // add additional rows for detail items (if necessary)
      while( ++fuelIdx < TD_COUNT )
      {
        if ( record.FuelData[fuelIdx][TDI_TOTAL_COST] != 0.0 )
        {
          line.append("\n,,,,\"");
          line.append(getDescFuelItem(fuelIdx));
          line.append("\",");
          line.append(NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_GALLONS],"#########0.00"));
          line.append(",");
          line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_COST_PER_GAL],"#########0.00") );
          line.append(",");
          line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_TOTAL_COST],"#########0.00") );
          line.append(",\"");
          line.append(record.DirectBillFlags[ FuelDirectBillMap[fuelIdx] ]);
          line.append("\",,,,,,,,,");
        }
      }
      
      while( ++nonFuelIdx < TD_NF_COUNT )
      {
        desc = getDescNonFuelItem(nonFuelIdx);
      
        if ( desc != null && record.NonFuelData[nonFuelIdx] != 0.0 )
        {
          line.append("\n,,,,\"");
          line.append(desc);
          line.append("\",,,");
          line.append(NumberFormatter.getDoubleString(record.NonFuelData[nonFuelIdx],"#########0.00"));
          line.append(",\"");
          line.append(record.DirectBillFlags[ NonFuelDirectBillMap[nonFuelIdx] ]);
          line.append("\",,,,,,,,,");
        }
      }
    }
    else if ( reportType == RT_ITEM_SEARCH )
    {
      String    desc        = null;
      int       fuelIdx     = -1;
      int       nonFuelIdx  = -1;
      RowData   record      = (RowData)obj;
      
      line.append(record.CustomerCode);
      line.append(",\"");
      line.append((record.ChainCode.equals("nochain") ? "" : record.ChainCode));
      line.append("\",\"");
      line.append(record.LocationCode);
      line.append("\",\"");
      line.append(record.InvoiceNumber);
      line.append("\",\"");
      line.append(DateTimeFormatter.getFormattedDate(record.InvoiceTS,"MM/dd/yyyy HH:mm"));
      line.append("\",\"");
      line.append(record.ControlNumber);
      line.append("\",");
        
      for( fuelIdx = 0; fuelIdx < TD_COUNT; ++fuelIdx )
      {
        // locate the first entry with data
        if ( record.FuelData[fuelIdx][TDI_TOTAL_COST] != 0.0 )
        {
          break;
        }
      }
      if ( fuelIdx < TD_COUNT )
      {
        line.append("\"");
        line.append(getDescFuelItem(fuelIdx));
        line.append("\",");
        line.append(NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_GALLONS],"#########0.00"));
        line.append(",");
        line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_COST_PER_GAL],"#########0.00") );
        line.append(",");
        line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_TOTAL_COST],"#########0.00") );
        line.append(",\"");
        line.append(record.DirectBillFlags[ FuelDirectBillMap[fuelIdx] ]);
        line.append("\"");
      }
      else    // no fuel, find first non-fuel item
      {
        for( nonFuelIdx = 0; nonFuelIdx < TD_NF_COUNT; ++nonFuelIdx )
        {
          desc = getDescNonFuelItem(nonFuelIdx);
        
          if ( desc != null && record.NonFuelData[nonFuelIdx] != 0.0 )
          {
            break;
          }
        }
        if ( nonFuelIdx < TD_NF_COUNT )
        {
          line.append(desc);
          line.append(",,,");
          line.append(NumberFormatter.getDoubleString(record.NonFuelData[nonFuelIdx],"#########0.00"));
          line.append(",\"");
          line.append(record.DirectBillFlags[ NonFuelDirectBillMap[nonFuelIdx] ]);
          line.append("\"");
        }
        else  // no detail, must be a service fee or like item
        {
          line.append(",,,");
          line.append(NumberFormatter.getDoubleString(record.InvoiceTotal,"#########0.00"));
          line.append(",");
          // no direct bill ?
        }
      }
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.InvoiceTotal,"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.FeeData[TD_FEE_DISCOUNT_AMOUNT],"#########0.00"));
      line.append(",");
      line.append(record.FeeType[TD_FEE_DISCOUNT_AMOUNT]);
      line.append(",");
      line.append("\"");
      line.append(record.SettlementControlNumber);
      line.append("\",");
      line.append(record.PaymentInd);
      line.append(",,,,");  // filler columns

      // add additional rows for detail items (if necessary)
      while( ++fuelIdx < TD_COUNT )
      {
        if ( record.FuelData[fuelIdx][TDI_TOTAL_COST] != 0.0 )
        {
          line.append("\n,,,,,,\"");
          line.append(getDescFuelItem(fuelIdx));
          line.append("\",");
          line.append(NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_GALLONS],"#########0.00"));
          line.append(",");
          line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_COST_PER_GAL],"#########0.00") );
          line.append(",");
          line.append( NumberFormatter.getDoubleString(record.FuelData[fuelIdx][TDI_TOTAL_COST],"#########0.00") );
          line.append(",\"");
          line.append(record.DirectBillFlags[ FuelDirectBillMap[fuelIdx] ]);
          line.append("\",,,,,");
        }
      }
      
      while( ++nonFuelIdx < TD_NF_COUNT )
      {
        desc = getDescNonFuelItem(nonFuelIdx);
      
        if ( desc != null && record.NonFuelData[nonFuelIdx] != 0.0 )
        {
          line.append("\n,,,,,,\"");
          line.append(desc);
          line.append("\",,,");
          line.append(NumberFormatter.getDoubleString(record.NonFuelData[nonFuelIdx],"#########0.00"));
          line.append(",\"");
          line.append(record.DirectBillFlags[ NonFuelDirectBillMap[nonFuelIdx] ]);
          line.append("\",,,,,");
        }
      }
    }
    else    // RT_SUMMARY 
    {
      SummaryData  record = (SummaryData)obj;
      
      line.append((record.CorpCode.equals("nocorp") ? "" : record.CorpCode));
      line.append(",");
      line.append((record.ChainCode.equals("nochain") ? "" : record.ChainCode));
      line.append(",");
      line.append(record.LocationCode);
      line.append(",");
      line.append("\"");
      line.append(record.LocationName);
      line.append("\",");
      line.append(NumberFormatter.getDoubleString(record.getFuelQuantity(),"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.getFuelAmount(),"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.getNonFuelAmount(),"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.getInvoiceAmount(),"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.getDiscAmount(),"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.getFeesAmount(),"#########0.00"));
      line.append(",");
      line.append(NumberFormatter.getDoubleString(record.getAdjustmentAmount(),"#########0.00"));
      line.append(",");
      if ( SummaryByBatch )
      {
        line.append(DateTimeFormatter.getFormattedDate(record.BatchDate,"MM/dd/yyyy"));
        line.append(",");
      }
      line.append(NumberFormatter.getDoubleString(record.getPaymentAmount(),"#########0.00"));
    }
  }
  
  protected void encodeTrailerCSV( StringBuffer line )
  {
    GroupTotalsRecord   totalsRec = loadSettlementGroupTotals();
  
    line.setLength(0);
    line.append("\n,,,,,,,,,,,,,,,,,");
    line.append("\n\"Direct Bill\",,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_DB][TCI_GALLONS],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_DB][TCI_AMOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_DB][TCI_INVOICE],"########0.00"));
    line.append(",");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_DB][TCI_DISCOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_DB][TCI_FEES],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_DB][TCI_PAYMENT],"########0.00"));
    line.append(",,,");
    line.append("\n,\"Product Amount\",,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_DB][TCI_PROD_TOTAL],"########0.00"));
    line.append(",,,,,,,,,,");

    line.append("\n\"Funded\",,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_FUNDED][TCI_GALLONS],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_FUNDED][TCI_AMOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_FUNDED][TCI_INVOICE],"########0.00"));
    line.append(",");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_FUNDED][TCI_DISCOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_FUNDED][TCI_FEES],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_FUNDED][TCI_PAYMENT],"########0.00"));
    line.append(",,,");
    line.append("\n,\"Product Amount\",,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_FUNDED][TCI_PROD_TOTAL],"########0.00"));
    line.append(",,,,,,,,,,");

    line.append("\n\"Gross Total\",,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_GROSS][TCI_GALLONS],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_GROSS][TCI_AMOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_GROSS][TCI_INVOICE],"########0.00"));
    line.append(",");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_GROSS][TCI_DISCOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_GROSS][TCI_FEES],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_GROSS][TCI_PAYMENT],"########0.00"));
    line.append(",,,");

    line.append("\n,,,,,,,,,,,,,,,,,");

    line.append("\n\"Equipment Charges\",,,,,,,,,,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.EquipmentCharge,"########0.00"));
    line.append(",,,");
    line.append("\n\"Excessive Settlement Fees\",,,,,,,,,,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.ExcessCharge,"########0.00"));
    line.append(",,,");
    line.append("\n\"Service Fees\",,,,,,,,,,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.ServiceCharge,"########0.00"));
    line.append(",,,");
    line.append("\n\"Misc Charge\",,,,,,,,,,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.MiscCharge,"########0.00"));
    line.append(",,,");

    line.append("\n,,,,,,,,,,,,,,,,,");
    line.append("\n\"Total Payment\",,,,");
    line.append("\"Ctl #\",");
    line.append(totalsRec.ControlNumber);
    line.append(",\"Check #\",");
    line.append(totalsRec.CheckNumber);
    line.append(",,,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.PaymentAmount,"########0.00"));
    line.append(",,,");
    
    line.append("\n,,,,,,,,,,,,,,,,,");

    line.append("\n\"Pending\",,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PEND][TCI_GALLONS],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PEND][TCI_AMOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PEND][TCI_INVOICE],"########0.00"));
    line.append(",");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PEND][TCI_DISCOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PEND][TCI_FEES],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PEND][TCI_PAYMENT],"########0.00"));
    line.append(",,,");

    line.append("\n\"Pay To Translink\",,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_TRANSLINK][TCI_GALLONS],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_TRANSLINK][TCI_AMOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_TRANSLINK][TCI_INVOICE],"########0.00"));
    line.append(",");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_TRANSLINK][TCI_DISCOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_TRANSLINK][TCI_FEES],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_TRANSLINK][TCI_PAYMENT],"########0.00"));
    line.append(",,,");

    line.append("\n,,,,,,,,,,,,,,,,,");

    line.append("\n\"Preferred Funded\",,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_FUND][TCI_GALLONS],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_FUND][TCI_AMOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_FUND][TCI_INVOICE],"########0.00"));
    line.append(",");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_FUND][TCI_DISCOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_FUND][TCI_FEES],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_FUND][TCI_PAYMENT],"########0.00"));
    line.append(",,,");
    line.append("\n,\"Pref F Products\",,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_FUND][TCI_PROD_TOTAL],"########0.00"));
    line.append(",,,,,,,,,,");

    line.append("\n\"Preferred Direct Bill\",,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_DB][TCI_GALLONS],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_DB][TCI_AMOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_DB][TCI_INVOICE],"########0.00"));
    line.append(",");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_DB][TCI_DISCOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_DB][TCI_FEES],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_DB][TCI_PAYMENT],"########0.00"));
    line.append(",,,");
    line.append("\n,\"Pref DB Products\",,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_PREF_DB][TCI_PROD_TOTAL],"########0.00"));
    line.append(",,,,,,,,,,");

    line.append("\n\"Credit\",,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_CREDIT][TCI_GALLONS],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_CREDIT][TCI_AMOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_CREDIT][TCI_INVOICE],"########0.00"));
    line.append(",");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_CREDIT][TCI_DISCOUNT],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_CREDIT][TCI_FEES],"########0.00"));
    line.append(",,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SegmentTotals[TGI_CREDIT][TCI_PAYMENT],"########0.00"));
    line.append(",,,");

    line.append("\n,,,,,,,,,,,,,,,,,");

    line.append("\n\"Total Select\",,,,,,,,,,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.SelectAmount,"########0.00"));
    line.append(",,,");
    line.append("\n\"Total Focus\",,,,,,,,,,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.FocusAmount,"########0.00"));
    line.append(",,,");
    line.append("\n\"Total Cost Plus\",,,,,,,,,,,,,,");
    line.append(NumberFormatter.getDoubleString(totalsRec.CostPlusAmount,"########0.00"));
    line.append(",,,");
  }
  
  public void encodeLocUrl( StringBuffer buffer )
  {
    if ( !getData("loc").equals("") )
    {
      buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
      buffer.append( "loc=" );
      buffer.append( getData("loc") );
      buffer.append( "&chain=" );
      buffer.append( getData("chain") );
      buffer.append( "&corp=" );
      buffer.append( getData("corp") );
    }
  }

  public String getComdataNodeId( long nodeId )
  {
    String        retVal    = "";
    
    try
    {
      if ( isNodeMerchant(nodeId) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1991^9*/

//  ************************************************************
//  #sql [Ctx] { -- merchant
//            select  mfc.location_code   
//            from    mif_comdata     mfc
//            where   mfc.merchant_number = :nodeId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "-- merchant\n          select  mfc.location_code    \n          from    mif_comdata     mfc\n          where   mfc.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ComdataTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1997^9*/
      }
      else if ( isNodeAssociation(nodeId) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2001^9*/

//  ************************************************************
//  #sql [Ctx] { select  mfch.chain_code 
//            from    mif_comdata_hierarchy   mfch
//            where   mfch.association = substr(:nodeId,5) and
//                    mfch.group_1 = 
//                    (
//                      select  substr(th.ancestor,5)
//                      from    t_hierarchy   th
//                      where   th.hier_type = 1 and
//                              th.descendent = :nodeId and
//                              th.relation = 1
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mfch.chain_code  \n          from    mif_comdata_hierarchy   mfch\n          where   mfch.association = substr( :1 ,5) and\n                  mfch.group_1 = \n                  (\n                    select  substr(th.ancestor,5)\n                    from    t_hierarchy   th\n                    where   th.hier_type = 1 and\n                            th.descendent =  :2  and\n                            th.relation = 1\n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ComdataTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setLong(2,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2014^9*/
      }
      else    // group 1
      {        
        /*@lineinfo:generated-code*//*@lineinfo:2018^9*/

//  ************************************************************
//  #sql [Ctx] { select  distinct mfch.corporate_code 
//            from    mif_comdata_hierarchy   mfch
//            where   mfch.group_1 = substr(:nodeId,5) and
//                    mfch.association in 
//                    (
//                      select  substr(th.descendent,5)
//                      from    t_hierarchy   th
//                      where   th.hier_type = 1 and
//                              th.ancestor = :nodeId and
//                              th.entity_type = 4
//                    )  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct mfch.corporate_code  \n          from    mif_comdata_hierarchy   mfch\n          where   mfch.group_1 = substr( :1 ,5) and\n                  mfch.association in \n                  (\n                    select  substr(th.descendent,5)\n                    from    t_hierarchy   th\n                    where   th.hier_type = 1 and\n                            th.ancestor =  :2  and\n                            th.entity_type = 4\n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ComdataTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setLong(2,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2031^9*/
      }        
    }
    catch(Exception e)
    {
      logEntry("getComdataNodeId(" + nodeId + ")",e.toString());
    }                
    return( retVal );
  }
  
  public String getDescFuelItem( int td )
  {
    String    retVal      = null;
    
    try
    {
      retVal = FuelItemDesc[td];
    }
    catch(Exception e)
    {
    }
    return( retVal );
  }
  
  public String getDescNonFuelItem( int td )
  {
    String    retVal      = null;
    
    try
    {
      retVal = NonFuelItemDesc[td];
    }
    catch(Exception e)
    {
    }
    return( retVal );
  }
  
  public String getDisplayName( )
  {
    StringBuffer      buffer          = new StringBuffer("");
    long              nodeId          = getReportHierarchyNode();
    
    if ( isNodeMerchant(nodeId) )
    {
      buffer.append( super.getDisplayName() );
      buffer.delete(0,(Long.toString(nodeId)).length());
      buffer.insert(0,getComdataNodeId(nodeId));
    }
    else 
    {
      switch( getReportType() )
      {
        case RT_VOL_BY_ACCT_SUMMARY:
          if ( getData("fleetCode").equals("nofleet") )
          {
            buffer.append( "Non-Fleet Accounts" );
          }
          else
          {
            buffer.append( getData("fleetCode") );
          }
          break;
          
        case RT_PAID_DETAILS:
          String loc = getData("locationCode");
          buffer.append(loc);
          buffer.append(" - ");
          buffer.append( getLocationName(loc) );
          break;
      }
    }
    return( buffer.toString() );
  }
  
  public String getDownloadFilenameBase()
  {       
    StringBuffer    filename    = new StringBuffer("");
    int             reportType  = getReportType();
    
    filename.append("comdata_");
    if ( reportType == RT_DETAILS )
    {
      filename.append("details_");
    }
    else if ( reportType == RT_VOL_BY_LOC_DETAILS )
    {
      filename.append("vol_by_ts_");
    }
    else if ( reportType == RT_VOL_BY_LOC_SUMMARY )
    {
      filename.append("vol_by_ts_summary_");
    }
    else if ( reportType == RT_VOL_BY_ACCT_SUMMARY )
    {
      filename.append("vol_by_acct_summary_");
    }
    else if ( reportType == RT_PAID_SUMMARY )
    {
      filename.append("paid_summary_");
    }
    else if ( reportType == RT_PAID_DETAILS )
    {
      filename.append("paid_detail_");
      
      String loc = getData("locationCode");
      if ( !loc.equals("") )
      {
        filename.append(loc);
        filename.append("_");
      }
    }
    else if ( reportType == RT_ITEM_SEARCH )
    {
      filename.append("tran_search_");
    }
    else    // RT_SUMMARY 
    {
      filename.append("summary_");
    }
    
    // build the first date into the filename
    switch( reportType )
    {
      case RT_VOL_BY_LOC_DETAILS:
      case RT_VOL_BY_LOC_SUMMARY:
      case RT_VOL_BY_ACCT_SUMMARY:
        filename.append( DateTimeFormatter.getFormattedDate(getReportDateBegin(),"MMMyyyy") );
        break;
      
      default:
        filename.append( DateTimeFormatter.getFormattedDate(getReportDateBegin(),"MMddyy") );
        if ( getReportDateBegin().equals(getReportDateEnd()) == false )
        {
          filename.append("_to_");
          filename.append( DateTimeFormatter.getFormattedDate(getReportDateEnd(),"MMddyy") );
        }      
        break;
    }        
    return ( filename.toString() );
  }
  
  public String getLocationName( String locCode )
  {
    String      retVal    = "";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2179^7*/

//  ************************************************************
//  #sql [Ctx] { select mfc.location_name  
//          from   mif_comdata mfc
//          where  mfc.location_code = :locCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select mfc.location_name   \n        from   mif_comdata mfc\n        where  mfc.location_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.ComdataTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,locCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2184^7*/
    }
    catch(Exception e)
    {
      // ignore
    }
    return( retVal );
  }
  
  public String getSummaryCode()
  {
    return( getData("summaryCode") );
  }      
  
  public int getSummaryCodeType( String summaryCode )
  {
    int     summaryCodeType     = CT_NONE;
    
    try
    {
      connect();
      
      if ( summaryCode != null && !summaryCode.equals("") )
      {
        // enforce all upper case
        summaryCode = summaryCode.toUpperCase();
        
        int recCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:2212^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(mfc.merchant_number) 
//            from    mif_comdata   mfc
//            where   mfc.location_code = :summaryCode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mfc.merchant_number)  \n          from    mif_comdata   mfc\n          where   mfc.location_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.ComdataTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,summaryCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2217^9*/
        if ( recCount > 0 )
        {
          summaryCodeType = CT_LOC;
        }
        else  // assume chain code
        {
          summaryCodeType = CT_CHAIN;
        }
      }
      else
      {
        summaryCodeType = CT_NONE;
      }
    }
    catch( Exception e )
    {
      summaryCodeType = CT_NONE;
    }      
    finally
    {
      cleanUp();
    }
    return( summaryCodeType );
  }
  
  public boolean isChain( String chainCode )
  {
    String    loginName   = ReportUserBean.getLoginName();
    int       recCount    = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2250^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( ucd.code ) 
//          from    users         u,
//                  user_comdata  ucd
//          where   u.login_name = :loginName and
//                  ucd.user_id = u.user_id and
//                  ucd.code_type = 'CHAIN' and
//                  ucd.code = :chainCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( ucd.code )  \n        from    users         u,\n                user_comdata  ucd\n        where   u.login_name =  :1  and\n                ucd.user_id = u.user_id and\n                ucd.code_type = 'CHAIN' and\n                ucd.code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.ComdataTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setString(2,chainCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2259^7*/
    }
    catch( Exception e )
    {
      logEntry("isChain(" + chainCode + ")",e.toString());
    }
    return( recCount > 0 );
  }
  
  public boolean isCurrencyField( int fieldId )
  {
    boolean       retVal      = false;
    
    for( int i = 0; i < SummaryCurrencyColumns.length; ++i )
    {
      if ( SummaryCurrencyColumns[i] == fieldId )
      {
        retVal = true;
        break;
      }
    }
    return( retVal );
  }
  
  public boolean isDetailReport( )
  {
    return( isDetailReport( getReportType() ) );
  }
  
  public boolean isDetailReport( int rtype )
  {
    boolean       retVal      = false;
    
    switch( rtype )
    {
      case RT_DETAILS:
      case RT_VOL_BY_LOC_DETAILS:
        retVal = true;
        break;
    }
    
    return( retVal );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public boolean isFleetDetail()
  {
    return( getData("summaryType").equals("1") &&
            !getData("fleetCode").equals("") );
  }
  
  public boolean isLocation( String code )
  {
    int       recCount      = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2327^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( mfc.location_code ) 
//          from    mif_comdata     mfc
//          where   mfc.location_code = :code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( mfc.location_code )  \n        from    mif_comdata     mfc\n        where   mfc.location_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.ComdataTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,code);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2332^7*/
    }
    catch( Exception e )
    {
      logEntry("isLocation(" + code + ")", e.toString());
    }
    return( recCount > 0 );
  }
  
  public boolean isTopLevelUser( )
  {
    int       recCount    = 0;
    long      userId      = ReportUserBean.getUserId();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2348^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(ucd.code) 
//          from    user_comdata ucd
//          where   ucd.user_id = :userId and
//                  ucd.code = '99999'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ucd.code)  \n        from    user_comdata ucd\n        where   ucd.user_id =  :1  and\n                ucd.code = '99999'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.ComdataTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2354^7*/
    }
    catch(Exception e)
    {
      logEntry("isTopLevelUser()",e.toString());
    }
    return( recCount > 0 );
  }
  
  public BatchTotalsRecord loadBatchTotals( int batchNumber )
  {
    ResultSetIterator         it          = null;
    ResultSet                 resultSet   = null;
    BatchTotalsRecord         retVal      = new BatchTotalsRecord();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2371^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bt.loc_excess_charge      as excess_charge, 
//                  bt.loc_equip_charge       as equip_charge, 
//                  bt.loc_misc_charge        as misc_charge, 
//                  bt.loc_service_charge     as service_charge
//          from    comdata_detail_file_bt  bt
//          where   bt.batch_number = :batchNumber      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bt.loc_excess_charge      as excess_charge, \n                bt.loc_equip_charge       as equip_charge, \n                bt.loc_misc_charge        as misc_charge, \n                bt.loc_service_charge     as service_charge\n        from    comdata_detail_file_bt  bt\n        where   bt.batch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,batchNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2379^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        retVal.setData(resultSet);
      }
    }
    catch(Exception e)
    {
      logEntry("loadBatchTotals(" + batchNumber + ")", e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception ee) {}
    }
    return( retVal );
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    Object                        dataObj           = null;
    String                        detailChainCode   = null;
    String                        detailCorpCode    = null;
    String                        detailLocCode     = null;
    ResultSetIterator             it                = null;
    long                          lastMerchantId    = 0L;
    int                           locType           = -1;
    long                          merchantId        = 0L;
    long                          nodeId            = 0L;
    ResultSet                     resultSet         = null;
    String                        summaryCode       = null;
    int                           summaryCodeType   = CT_NONE;
    long                          userId            = 0L;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      nodeId  = orgIdToHierarchyNode(orgId);
      if ( isNodeMerchant(nodeId) )
      {
        merchantId = nodeId;
      }
      userId  = ReportUserBean.getUserId();
      
      // extract field values into locals for use
      // in the queries.
      summaryCode     = getData("summaryCode");
      summaryCodeType = getSummaryCodeType(summaryCode);
      detailChainCode = getData("chain");
      detailCorpCode  = getData("corp");
      detailLocCode   = getData("loc");
      
      if ( getReportType() == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2436^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mfc.merchant_number             as merchant_number,
//                    dt.location_code                as location_code,
//                    dt.chain_code                   as chain_code,
//                    mfc.location_name               as location_name,
//                    dt.customer_account_code        as customer_account_code,
//                    dt.card_number                  as card_number,
//                    dt.invoice_number               as invoice_number,
//                    dt.invoice_date                 as invoice_date,
//                    dt.invoice_time                 as invoice_time,
//                    dt.control_number               as control_number,
//                    dt.diesel_1_gallons             as diesel_1_gallons,
//                    dt.diesel_1_cost_per_gallon     as diesel_1_cost_per_gallon,
//                    dt.diesel_1_total_cost          as diesel_1_total_cost,
//                    dt.diesel_2_gallons             as diesel_2_gallons,
//                    dt.diesel_2_cost_per_gallon     as diesel_2_cost_per_gallon,
//                    dt.diesel_2_total_cost          as diesel_2_total_cost,
//                    dt.reefer_gallons               as reefer_gallons,
//                    dt.reefer_cost_per_gallon       as reefer_cost_per_gallon,
//                    dt.reefer_total_cost            as reefer_total_cost,
//                    dt.other_gallons                as other_gallons,
//                    dt.other_cost_per_gallon        as other_cost_per_gallon,
//                    dt.other_total_cost             as other_total_cost,
//                    nvl(dt.fuel_direct_bill,'N')    as fuel_direct_bill,
//                    dt.oil_purchased                as oil_quarts,
//                    dt.oil_cost_per_quart           as oil_cost_per_quart,
//                    dt.oil_total_cost               as oil_total_cost,
//                    nvl(dt.oil_direct_bill,'N')     as oil_direct_bill,
//                    dt.cash_advance_amount          as cash_advance_amount,
//                    nvl(dt.cash_direct_bill,'N')    as cash_direct_bill,
//                    nvl(pc1.product_desc, 'Not Available')
//                                                    as product_code_1,
//                    dt.product_code_1_sign          as product_code_1_sign,
//                    dt.product_code_1_amount        as product_code_1_amount,
//                    nvl(dt.product_code_1_direct_bill,'N')
//                                                    as product_code_1_direct_bill,
//                    nvl(pc2.product_desc, 'Not Available')
//                                                    as product_code_2,
//                    dt.product_code_2_sign          as product_code_2_sign,
//                    dt.product_code_2_amount        as product_code_2_amount,
//                    nvl(dt.product_code_2_direct_bill,'N')
//                                                    as product_code_2_direct_bill,
//                    nvl(pc3.product_desc, 'Not Available')
//                                                    as product_code_3,
//                    dt.product_code_3_sign          as product_code_3_sign,
//                    dt.product_code_3_amount        as product_code_3_amount,
//                    nvl(dt.product_code_3_direct_bill,'N')
//                                                    as product_code_3_direct_bill,
//                    dt.discount_amount              as discount_amount,
//                    dt.discount_type                as discount_type,
//                    dt.fee_type                     as fee_type,
//                    dt.fee_percent                  as fee_percent,
//                    dt.fee_amount                   as fee_amount,
//                    dt.payment_cycle                as payment_cycle,
//                    dt.payment_pending              as payment_pending,
//                    dt.tran_type                    as tran_type,
//                    dt.payment_sign                 as payment_sign,
//                    dt.payment_amount               as payment_amount,
//                    dt.adjustment_sign              as adjustment_sign,
//                    dt.adjustment_amount            as adjustment_amount,
//                    dt.customer_id                  as customer_id,
//                    dt.payment_type                 as payment_type,
//                    dt.credit_flag                  as credit_flag,
//                    dt.balanced_ind                 as balanced_ind,
//                    dt.closed_ind                   as closed_ind,
//                    dt.paid_ind                     as paid_ind,
//                    bt.loc_payment_type             as payment_method,
//                    dt.batch_number                 as batch_number,
//                    dt.batch_tran_id                as batch_tran_id               
//            from    comdata_detail_file_dt  dt,
//                    comdata_detail_file_bt  bt,
//                    comdata_product_codes   pc1,
//                    comdata_product_codes   pc2,
//                    comdata_product_codes   pc3,
//                    mif_comdata             mfc
//            where   dt.location_code = :detailLocCode and
//                    nvl(dt.chain_code,'nochain') = :detailChainCode and
//                    nvl(dt.corp_code,'nocorp') = :detailCorpCode and
//                    (
//                      dt.location_code in
//                        (
//                          select  ucd.code
//                          from    user_comdata    ucd
//                          where   ucd.user_id = :userId and
//                                  ucd.code_type = 'LOC' 
//                        ) or
//                      nvl(dt.chain_code,'nochain') in
//                        ( 
//                          select  ucd.code
//                          from    user_comdata    ucd
//                          where   ucd.user_id = :userId and
//                                  ucd.code_type = 'CHAIN'                      
//                        ) or 
//                      nvl(dt.corp_code,'nocorp') in
//                        (
//                          select  decode(ucd.code,
//                                         '99999',nvl(dt.corp_code,'nocorp'), 
//                                         ucd.code)
//                          from    user_comdata    ucd
//                          where   ucd.user_id = :userId and
//                                  ucd.code_type = 'CORP'                      
//                        )                                  
//                    ) and
//                    dt.batch_date between :beginDate and :endDate and
//                    dt.data_type = 'PAID' and
//                    ( :DetailBatchNumber = 0 or 
//                      dt.batch_number = :DetailBatchNumber ) and
//                    bt.batch_number = dt.batch_number and
//                    pc1.product_code(+) = dt.product_code_1 and
//                    pc2.product_code(+) = dt.product_code_2 and
//                    pc3.product_code(+) = dt.product_code_3 and
//                    mfc.location_code = dt.location_code
//            order by  dt.location_code, mfc.location_name, 
//                      dt.batch_number, dt.batch_tran_id                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mfc.merchant_number             as merchant_number,\n                  dt.location_code                as location_code,\n                  dt.chain_code                   as chain_code,\n                  mfc.location_name               as location_name,\n                  dt.customer_account_code        as customer_account_code,\n                  dt.card_number                  as card_number,\n                  dt.invoice_number               as invoice_number,\n                  dt.invoice_date                 as invoice_date,\n                  dt.invoice_time                 as invoice_time,\n                  dt.control_number               as control_number,\n                  dt.diesel_1_gallons             as diesel_1_gallons,\n                  dt.diesel_1_cost_per_gallon     as diesel_1_cost_per_gallon,\n                  dt.diesel_1_total_cost          as diesel_1_total_cost,\n                  dt.diesel_2_gallons             as diesel_2_gallons,\n                  dt.diesel_2_cost_per_gallon     as diesel_2_cost_per_gallon,\n                  dt.diesel_2_total_cost          as diesel_2_total_cost,\n                  dt.reefer_gallons               as reefer_gallons,\n                  dt.reefer_cost_per_gallon       as reefer_cost_per_gallon,\n                  dt.reefer_total_cost            as reefer_total_cost,\n                  dt.other_gallons                as other_gallons,\n                  dt.other_cost_per_gallon        as other_cost_per_gallon,\n                  dt.other_total_cost             as other_total_cost,\n                  nvl(dt.fuel_direct_bill,'N')    as fuel_direct_bill,\n                  dt.oil_purchased                as oil_quarts,\n                  dt.oil_cost_per_quart           as oil_cost_per_quart,\n                  dt.oil_total_cost               as oil_total_cost,\n                  nvl(dt.oil_direct_bill,'N')     as oil_direct_bill,\n                  dt.cash_advance_amount          as cash_advance_amount,\n                  nvl(dt.cash_direct_bill,'N')    as cash_direct_bill,\n                  nvl(pc1.product_desc, 'Not Available')\n                                                  as product_code_1,\n                  dt.product_code_1_sign          as product_code_1_sign,\n                  dt.product_code_1_amount        as product_code_1_amount,\n                  nvl(dt.product_code_1_direct_bill,'N')\n                                                  as product_code_1_direct_bill,\n                  nvl(pc2.product_desc, 'Not Available')\n                                                  as product_code_2,\n                  dt.product_code_2_sign          as product_code_2_sign,\n                  dt.product_code_2_amount        as product_code_2_amount,\n                  nvl(dt.product_code_2_direct_bill,'N')\n                                                  as product_code_2_direct_bill,\n                  nvl(pc3.product_desc, 'Not Available')\n                                                  as product_code_3,\n                  dt.product_code_3_sign          as product_code_3_sign,\n                  dt.product_code_3_amount        as product_code_3_amount,\n                  nvl(dt.product_code_3_direct_bill,'N')\n                                                  as product_code_3_direct_bill,\n                  dt.discount_amount              as discount_amount,\n                  dt.discount_type                as discount_type,\n                  dt.fee_type                     as fee_type,\n                  dt.fee_percent                  as fee_percent,\n                  dt.fee_amount                   as fee_amount,\n                  dt.payment_cycle                as payment_cycle,\n                  dt.payment_pending              as payment_pending,\n                  dt.tran_type                    as tran_type,\n                  dt.payment_sign                 as payment_sign,\n                  dt.payment_amount               as payment_amount,\n                  dt.adjustment_sign              as adjustment_sign,\n                  dt.adjustment_amount            as adjustment_amount,\n                  dt.customer_id                  as customer_id,\n                  dt.payment_type                 as payment_type,\n                  dt.credit_flag                  as credit_flag,\n                  dt.balanced_ind                 as balanced_ind,\n                  dt.closed_ind                   as closed_ind,\n                  dt.paid_ind                     as paid_ind,\n                  bt.loc_payment_type             as payment_method,\n                  dt.batch_number                 as batch_number,\n                  dt.batch_tran_id                as batch_tran_id               \n          from    comdata_detail_file_dt  dt,\n                  comdata_detail_file_bt  bt,\n                  comdata_product_codes   pc1,\n                  comdata_product_codes   pc2,\n                  comdata_product_codes   pc3,\n                  mif_comdata             mfc\n          where   dt.location_code =  :1  and\n                  nvl(dt.chain_code,'nochain') =  :2  and\n                  nvl(dt.corp_code,'nocorp') =  :3  and\n                  (\n                    dt.location_code in\n                      (\n                        select  ucd.code\n                        from    user_comdata    ucd\n                        where   ucd.user_id =  :4  and\n                                ucd.code_type = 'LOC' \n                      ) or\n                    nvl(dt.chain_code,'nochain') in\n                      ( \n                        select  ucd.code\n                        from    user_comdata    ucd\n                        where   ucd.user_id =  :5  and\n                                ucd.code_type = 'CHAIN'                      \n                      ) or \n                    nvl(dt.corp_code,'nocorp') in\n                      (\n                        select  decode(ucd.code,\n                                       '99999',nvl(dt.corp_code,'nocorp'), \n                                       ucd.code)\n                        from    user_comdata    ucd\n                        where   ucd.user_id =  :6  and\n                                ucd.code_type = 'CORP'                      \n                      )                                  \n                  ) and\n                  dt.batch_date between  :7  and  :8  and\n                  dt.data_type = 'PAID' and\n                  (  :9  = 0 or \n                    dt.batch_number =  :10  ) and\n                  bt.batch_number = dt.batch_number and\n                  pc1.product_code(+) = dt.product_code_1 and\n                  pc2.product_code(+) = dt.product_code_2 and\n                  pc3.product_code(+) = dt.product_code_3 and\n                  mfc.location_code = dt.location_code\n          order by  dt.location_code, mfc.location_name, \n                    dt.batch_number, dt.batch_tran_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,detailLocCode);
   __sJT_st.setString(2,detailChainCode);
   __sJT_st.setString(3,detailCorpCode);
   __sJT_st.setLong(4,userId);
   __sJT_st.setLong(5,userId);
   __sJT_st.setLong(6,userId);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   __sJT_st.setInt(9,DetailBatchNumber);
   __sJT_st.setInt(10,DetailBatchNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2551^9*/
      }
      else if ( getReportType() == RT_VOL_BY_LOC_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2555^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mfc.merchant_number                     as merchant_number, 
//                    dt.location_code                        as location_code, 
//                    mfc.location_name                       as location_name, 
//                    :beginDate                              as active_date, 
//                    dt.customer_account_code                as acct_code,
//                    dt.company_name                         as fleet_name,
//                    dt.company_city                         as fleet_city,
//                    dt.company_phone                        as fleet_phone,                                     
//                    nvl(dt.company_corp_code,'nofleet')     as fleet_code,
//                    decode( instr((nvl(dt.fuel_direct_bill,'N') ||
//                                 nvl(dt.oil_direct_bill,'N')  ||
//                                 nvl(dt.cash_direct_bill,'N') ||
//                                 nvl(dt.product_code_1_direct_bill,'N') ||
//                                 nvl(dt.product_code_2_direct_bill,'N') ||
//                                 nvl(dt.product_code_3_direct_bill,'N')
//                                 ),'Y'),
//                          0,dt.tran_type,
//                          'DB')                             as t_flag,
//                    count(dt.customer_account_code)         as tran_count, 
//                    sum( nvl(dt.diesel_1_gallons,0) + 
//                         nvl(dt.diesel_2_gallons,0) + 
//                         nvl(dt.reefer_gallons,0) + 
//                         nvl(dt.other_gallons,0) 
//                       )                                    as fuel_quantity, 
//                    sum( nvl(dt.diesel_1_total_cost,0) + 
//                         nvl(dt.diesel_2_total_cost,0) + 
//                         nvl(dt.reefer_total_cost,0) + 
//                         nvl(dt.other_total_cost,0) 
//                       )                                    as fuel_cost, 
//                    sum( nvl(dt.oil_purchased,0) )          as oil_quantity, 
//                    sum( nvl(dt.oil_total_cost,0) )         as oil_cost, 
//                    sum( nvl(dt.cash_advance_amount,0) )    as cash_amount, 
//                    sum( nvl(dt.oil_total_cost,0) +
//                         nvl(dt.product_code_1_amount,0) + 
//                         nvl(dt.product_code_2_amount,0) + 
//                         nvl(dt.product_code_3_amount,0) )  as other_amount, 
//                    sum( nvl(dt.fee_amount,0) )             as fee_amount, 
//                    sum( nvl(dt.payment_amount,0) )         as pmt_amount, 
//                    sum( nvl(dt.adjustment_amount,0) )      as adj_amount,
//                    sum( decode(dt.discount_type,
//                                'S',nvl(dt.discount_amount,0),
//                                0 ) )                       as select_amount,
//                    sum( decode(dt.discount_type,                                
//                                'F',nvl(dt.discount_amount,0),
//                                0) )                        as focus_amount
//            from    comdata_detail_file_dt  dt, 
//                    mif_comdata             mfc 
//            where   (
//                      dt.location_code = :detailLocCode and 
//                      nvl(dt.chain_code,'nochain') = :detailChainCode and 
//                      nvl(dt.corp_code,'nocorp') = :detailCorpCode
//                    ) and
//                    ( 
//                      dt.location_code in 
//                        ( 
//                          select  ucd.code 
//                          from    user_comdata    ucd 
//                          where   ucd.user_id = :userId and 
//                                  ucd.code_type = 'LOC' 
//                        ) or 
//                      nvl(dt.chain_code,'nochain') in 
//                        ( 
//                          select  ucd.code 
//                          from    user_comdata    ucd 
//                          where   ucd.user_id = :userId and 
//                                  ucd.code_type = 'CHAIN' 
//                        ) or 
//                      nvl(dt.corp_code,'nocorp') in 
//                        ( 
//                          select  decode(ucd.code, 
//                                         '99999',nvl(dt.corp_code,'nocorp'), 
//                                         ucd.code) 
//                          from    user_comdata    ucd 
//                          where   ucd.user_id = :userId and 
//                                  ucd.code_type = 'CORP' 
//                        ) 
//                    ) and 
//                    not dt.customer_account_code is null and 
//                    dt.data_type = 'ALLT' and
//                    not exists
//                    (
//                      select  evi.invoice_number
//                      from    comdata_excl_vol_invoice evi
//                      where   evi.invoice_number = dt.invoice_number
//                    ) and
//                    dt.tran_type != 'CNL' and
//                    trunc(dt.batch_date,'month') = :beginDate and 
//                    mfc.location_code = dt.location_code 
//            group by mfc.merchant_number, dt.location_code, 
//                      mfc.location_name, nvl(dt.company_corp_code,'nofleet'),
//                      dt.customer_account_code,
//                      dt.company_name, 
//                      dt.company_city,dt.company_phone,
//                      decode( instr((nvl(dt.fuel_direct_bill,'N') ||
//                                   nvl(dt.oil_direct_bill,'N')  ||
//                                   nvl(dt.cash_direct_bill,'N') ||
//                                   nvl(dt.product_code_1_direct_bill,'N') ||
//                                   nvl(dt.product_code_2_direct_bill,'N') ||
//                                   nvl(dt.product_code_3_direct_bill,'N')
//                                   ),'Y'),
//                            0,dt.tran_type,
//                            'DB')
//            order by  decode(fleet_code,'nofleet','ZZZ',fleet_code),acct_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mfc.merchant_number                     as merchant_number, \n                  dt.location_code                        as location_code, \n                  mfc.location_name                       as location_name, \n                   :1                               as active_date, \n                  dt.customer_account_code                as acct_code,\n                  dt.company_name                         as fleet_name,\n                  dt.company_city                         as fleet_city,\n                  dt.company_phone                        as fleet_phone,                                     \n                  nvl(dt.company_corp_code,'nofleet')     as fleet_code,\n                  decode( instr((nvl(dt.fuel_direct_bill,'N') ||\n                               nvl(dt.oil_direct_bill,'N')  ||\n                               nvl(dt.cash_direct_bill,'N') ||\n                               nvl(dt.product_code_1_direct_bill,'N') ||\n                               nvl(dt.product_code_2_direct_bill,'N') ||\n                               nvl(dt.product_code_3_direct_bill,'N')\n                               ),'Y'),\n                        0,dt.tran_type,\n                        'DB')                             as t_flag,\n                  count(dt.customer_account_code)         as tran_count, \n                  sum( nvl(dt.diesel_1_gallons,0) + \n                       nvl(dt.diesel_2_gallons,0) + \n                       nvl(dt.reefer_gallons,0) + \n                       nvl(dt.other_gallons,0) \n                     )                                    as fuel_quantity, \n                  sum( nvl(dt.diesel_1_total_cost,0) + \n                       nvl(dt.diesel_2_total_cost,0) + \n                       nvl(dt.reefer_total_cost,0) + \n                       nvl(dt.other_total_cost,0) \n                     )                                    as fuel_cost, \n                  sum( nvl(dt.oil_purchased,0) )          as oil_quantity, \n                  sum( nvl(dt.oil_total_cost,0) )         as oil_cost, \n                  sum( nvl(dt.cash_advance_amount,0) )    as cash_amount, \n                  sum( nvl(dt.oil_total_cost,0) +\n                       nvl(dt.product_code_1_amount,0) + \n                       nvl(dt.product_code_2_amount,0) + \n                       nvl(dt.product_code_3_amount,0) )  as other_amount, \n                  sum( nvl(dt.fee_amount,0) )             as fee_amount, \n                  sum( nvl(dt.payment_amount,0) )         as pmt_amount, \n                  sum( nvl(dt.adjustment_amount,0) )      as adj_amount,\n                  sum( decode(dt.discount_type,\n                              'S',nvl(dt.discount_amount,0),\n                              0 ) )                       as select_amount,\n                  sum( decode(dt.discount_type,                                \n                              'F',nvl(dt.discount_amount,0),\n                              0) )                        as focus_amount\n          from    comdata_detail_file_dt  dt, \n                  mif_comdata             mfc \n          where   (\n                    dt.location_code =  :2  and \n                    nvl(dt.chain_code,'nochain') =  :3  and \n                    nvl(dt.corp_code,'nocorp') =  :4 \n                  ) and\n                  ( \n                    dt.location_code in \n                      ( \n                        select  ucd.code \n                        from    user_comdata    ucd \n                        where   ucd.user_id =  :5  and \n                                ucd.code_type = 'LOC' \n                      ) or \n                    nvl(dt.chain_code,'nochain') in \n                      ( \n                        select  ucd.code \n                        from    user_comdata    ucd \n                        where   ucd.user_id =  :6  and \n                                ucd.code_type = 'CHAIN' \n                      ) or \n                    nvl(dt.corp_code,'nocorp') in \n                      ( \n                        select  decode(ucd.code, \n                                       '99999',nvl(dt.corp_code,'nocorp'), \n                                       ucd.code) \n                        from    user_comdata    ucd \n                        where   ucd.user_id =  :7  and \n                                ucd.code_type = 'CORP' \n                      ) \n                  ) and \n                  not dt.customer_account_code is null and \n                  dt.data_type = 'ALLT' and\n                  not exists\n                  (\n                    select  evi.invoice_number\n                    from    comdata_excl_vol_invoice evi\n                    where   evi.invoice_number = dt.invoice_number\n                  ) and\n                  dt.tran_type != 'CNL' and\n                  trunc(dt.batch_date,'month') =  :8  and \n                  mfc.location_code = dt.location_code \n          group by mfc.merchant_number, dt.location_code, \n                    mfc.location_name, nvl(dt.company_corp_code,'nofleet'),\n                    dt.customer_account_code,\n                    dt.company_name, \n                    dt.company_city,dt.company_phone,\n                    decode( instr((nvl(dt.fuel_direct_bill,'N') ||\n                                 nvl(dt.oil_direct_bill,'N')  ||\n                                 nvl(dt.cash_direct_bill,'N') ||\n                                 nvl(dt.product_code_1_direct_bill,'N') ||\n                                 nvl(dt.product_code_2_direct_bill,'N') ||\n                                 nvl(dt.product_code_3_direct_bill,'N')\n                                 ),'Y'),\n                          0,dt.tran_type,\n                          'DB')\n          order by  decode(fleet_code,'nofleet','ZZZ',fleet_code),acct_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setString(2,detailLocCode);
   __sJT_st.setString(3,detailChainCode);
   __sJT_st.setString(4,detailCorpCode);
   __sJT_st.setLong(5,userId);
   __sJT_st.setLong(6,userId);
   __sJT_st.setLong(7,userId);
   __sJT_st.setDate(8,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2660^9*/
      }
      else if ( getReportType() == RT_VOL_BY_LOC_SUMMARY )
      {
        int byFleet = (getData("summaryType").equals("1") ? 1 : 0);
       
        if ( !isTopLevelUser() || !summaryCode.equals("") )
        {
          /*@lineinfo:generated-code*//*@lineinfo:2668^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.corp_code                            as corp_code,
//                      sm.chain_code                           as chain_code,
//                      mfc.merchant_number                     as merchant_number, 
//                      sm.location_code                        as location_code, 
//                      mfc.location_name                       as location_name, 
//                      :beginDate                              as active_date, 
//                      sm.fleet_code                           as fleet_code,
//                      sm.fleet_name                           as fleet_name,
//                      null                                    as account_code,
//                      :byFleet                                as by_fleet,
//                      ' '                                     as t_flag,
//                      sum(sm.tran_count)                      as tran_count, 
//                      sum(sm.fuel_quantity)                   as fuel_quantity, 
//                      sum(sm.fuel_cost)                       as fuel_cost, 
//                      sum(sm.oil_quantity)                    as oil_quantity, 
//                      sum(sm.oil_cost)                        as oil_cost, 
//                      sum(sm.cash_advance_amount)             as cash_amount, 
//                      sum(sm.other_amount)                    as other_amount, 
//                      sum(sm.fee_amount)                      as fee_amount, 
//                      sum(sm.pmt_amount)                      as pmt_amount, 
//                      sum(sm.adj_amount)                      as adj_amount,
//                      sum(sm.select_amount)                   as select_amount,
//                      sum(sm.focus_amount)                    as focus_amount
//              from    comdata_detail_vol_summary    sm, 
//                      mif_comdata                   mfc 
//              where   (
//                        sm.location_code in 
//                          ( 
//                            select  ucd.code 
//                            from    user_comdata    ucd 
//                            where   ucd.user_id = :userId and 
//                                    ucd.code_type = 'LOC' 
//                          ) or 
//                        nvl(sm.chain_code,'nochain') in 
//                          ( 
//                            select  ucd.code 
//                            from    user_comdata    ucd 
//                            where   ucd.user_id = :userId and 
//                                    ucd.code_type = 'CHAIN' 
//                          ) or 
//                        nvl(sm.corp_code,'nocorp') in 
//                          ( 
//                            select  decode(ucd.code, 
//                                           '99999',nvl(sm.corp_code,'nocorp'), 
//                                           ucd.code) 
//                            from    user_comdata    ucd 
//                            where   ucd.user_id = :userId and 
//                                    ucd.code_type = 'CORP' 
//                          ) 
//                      ) and 
//                      nvl(:summaryCode,'none') = decode( :summaryCodeType,
//                                                         :CT_LOC,sm.location_code,
//                                                         :CT_CHAIN,sm.chain_code,
//                                                         'none') and
//                      trunc(sm.batch_date,'month') = :beginDate and 
//                      mfc.location_code = sm.location_code and
//                      ( :merchantId = 0 or mfc.merchant_number = :merchantId )
//              group by  sm.corp_code,
//                        sm.chain_code,
//                        sm.location_code,
//                        sm.fleet_code,
//                        sm.fleet_name,
//                        mfc.merchant_number,
//                        mfc.location_name
//              order by decode(chain_code,'nochain','ZZZ',chain_code), 
//                       decode(:byFleet,0,location_code,
//                              decode(fleet_code,'nofleet','ZZZ',fleet_code)),
//                       decode(corp_code,'nocorp','ZZZ',corp_code),
//                       mfc.merchant_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.corp_code                            as corp_code,\n                    sm.chain_code                           as chain_code,\n                    mfc.merchant_number                     as merchant_number, \n                    sm.location_code                        as location_code, \n                    mfc.location_name                       as location_name, \n                     :1                               as active_date, \n                    sm.fleet_code                           as fleet_code,\n                    sm.fleet_name                           as fleet_name,\n                    null                                    as account_code,\n                     :2                                 as by_fleet,\n                    ' '                                     as t_flag,\n                    sum(sm.tran_count)                      as tran_count, \n                    sum(sm.fuel_quantity)                   as fuel_quantity, \n                    sum(sm.fuel_cost)                       as fuel_cost, \n                    sum(sm.oil_quantity)                    as oil_quantity, \n                    sum(sm.oil_cost)                        as oil_cost, \n                    sum(sm.cash_advance_amount)             as cash_amount, \n                    sum(sm.other_amount)                    as other_amount, \n                    sum(sm.fee_amount)                      as fee_amount, \n                    sum(sm.pmt_amount)                      as pmt_amount, \n                    sum(sm.adj_amount)                      as adj_amount,\n                    sum(sm.select_amount)                   as select_amount,\n                    sum(sm.focus_amount)                    as focus_amount\n            from    comdata_detail_vol_summary    sm, \n                    mif_comdata                   mfc \n            where   (\n                      sm.location_code in \n                        ( \n                          select  ucd.code \n                          from    user_comdata    ucd \n                          where   ucd.user_id =  :3  and \n                                  ucd.code_type = 'LOC' \n                        ) or \n                      nvl(sm.chain_code,'nochain') in \n                        ( \n                          select  ucd.code \n                          from    user_comdata    ucd \n                          where   ucd.user_id =  :4  and \n                                  ucd.code_type = 'CHAIN' \n                        ) or \n                      nvl(sm.corp_code,'nocorp') in \n                        ( \n                          select  decode(ucd.code, \n                                         '99999',nvl(sm.corp_code,'nocorp'), \n                                         ucd.code) \n                          from    user_comdata    ucd \n                          where   ucd.user_id =  :5  and \n                                  ucd.code_type = 'CORP' \n                        ) \n                    ) and \n                    nvl( :6 ,'none') = decode(  :7 ,\n                                                        :8 ,sm.location_code,\n                                                        :9 ,sm.chain_code,\n                                                       'none') and\n                    trunc(sm.batch_date,'month') =  :10  and \n                    mfc.location_code = sm.location_code and\n                    (  :11  = 0 or mfc.merchant_number =  :12  )\n            group by  sm.corp_code,\n                      sm.chain_code,\n                      sm.location_code,\n                      sm.fleet_code,\n                      sm.fleet_name,\n                      mfc.merchant_number,\n                      mfc.location_name\n            order by decode(chain_code,'nochain','ZZZ',chain_code), \n                     decode( :13 ,0,location_code,\n                            decode(fleet_code,'nofleet','ZZZ',fleet_code)),\n                     decode(corp_code,'nocorp','ZZZ',corp_code),\n                     mfc.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setInt(2,byFleet);
   __sJT_st.setLong(3,userId);
   __sJT_st.setLong(4,userId);
   __sJT_st.setLong(5,userId);
   __sJT_st.setString(6,summaryCode);
   __sJT_st.setInt(7,summaryCodeType);
   __sJT_st.setInt(8,CT_LOC);
   __sJT_st.setInt(9,CT_CHAIN);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setLong(11,merchantId);
   __sJT_st.setLong(12,merchantId);
   __sJT_st.setInt(13,byFleet);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2739^11*/
        }
      }
      else if ( getReportType() == RT_VOL_BY_ACCT_SUMMARY )
      {
        String    detailFleetCode = getData("fleetCode");
        
        /*@lineinfo:generated-code*//*@lineinfo:2746^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  null                                    as corp_code,
//                    null                                    as chain_code,
//                    0                                       as merchant_number, 
//                    null                                    as location_code, 
//                    null                                    as location_name, 
//                    :beginDate                              as active_date, 
//                    sm.fleet_code                           as fleet_code,
//                    sm.account_name                         as fleet_name,
//                    sm.account_code                         as account_code,
//                    1                                       as by_fleet,
//                    ' '                                     as t_flag,
//                    sum(sm.tran_count)                      as tran_count, 
//                    sum(sm.fuel_quantity)                   as fuel_quantity, 
//                    sum(sm.fuel_cost)                       as fuel_cost, 
//                    sum(sm.oil_quantity)                    as oil_quantity, 
//                    sum(sm.oil_cost)                        as oil_cost, 
//                    sum(sm.cash_advance_amount)             as cash_amount, 
//                    sum(sm.other_amount)                    as other_amount, 
//                    sum(sm.fee_amount)                      as fee_amount, 
//                    sum(sm.pmt_amount)                      as pmt_amount, 
//                    sum(sm.adj_amount)                      as adj_amount,
//                    sum(sm.select_amount)                   as select_amount,
//                    sum(sm.focus_amount)                    as focus_amount
//            from    comdata_detail_acct_summary   sm
//            where   (
//                      sm.location_code in 
//                        ( 
//                          select  ucd.code 
//                          from    user_comdata    ucd 
//                          where   ucd.user_id = :userId and 
//                                  ucd.code_type = 'LOC' 
//                        ) or 
//                      nvl(sm.chain_code,'nochain') in 
//                        ( 
//                          select  ucd.code 
//                          from    user_comdata    ucd 
//                          where   ucd.user_id = :userId and 
//                                  ucd.code_type = 'CHAIN' 
//                        ) or 
//                      nvl(sm.corp_code,'nocorp') in 
//                        ( 
//                          select  decode(ucd.code, 
//                                         '99999',nvl(sm.corp_code,'nocorp'), 
//                                         ucd.code) 
//                          from    user_comdata    ucd 
//                          where   ucd.user_id = :userId and 
//                                  ucd.code_type = 'CORP' 
//                        ) 
//                    ) and 
//                    sm.chain_code = nvl(:detailChainCode,'none') and
//                    trunc(sm.batch_date,'month') = :beginDate and 
//                    sm.fleet_code = :detailFleetCode
//            group by  sm.fleet_code,
//                      sm.account_name,
//                      sm.account_code
//            order by decode(fleet_code,'nofleet','ZZZ',fleet_code),
//                     sm.account_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  null                                    as corp_code,\n                  null                                    as chain_code,\n                  0                                       as merchant_number, \n                  null                                    as location_code, \n                  null                                    as location_name, \n                   :1                               as active_date, \n                  sm.fleet_code                           as fleet_code,\n                  sm.account_name                         as fleet_name,\n                  sm.account_code                         as account_code,\n                  1                                       as by_fleet,\n                  ' '                                     as t_flag,\n                  sum(sm.tran_count)                      as tran_count, \n                  sum(sm.fuel_quantity)                   as fuel_quantity, \n                  sum(sm.fuel_cost)                       as fuel_cost, \n                  sum(sm.oil_quantity)                    as oil_quantity, \n                  sum(sm.oil_cost)                        as oil_cost, \n                  sum(sm.cash_advance_amount)             as cash_amount, \n                  sum(sm.other_amount)                    as other_amount, \n                  sum(sm.fee_amount)                      as fee_amount, \n                  sum(sm.pmt_amount)                      as pmt_amount, \n                  sum(sm.adj_amount)                      as adj_amount,\n                  sum(sm.select_amount)                   as select_amount,\n                  sum(sm.focus_amount)                    as focus_amount\n          from    comdata_detail_acct_summary   sm\n          where   (\n                    sm.location_code in \n                      ( \n                        select  ucd.code \n                        from    user_comdata    ucd \n                        where   ucd.user_id =  :2  and \n                                ucd.code_type = 'LOC' \n                      ) or \n                    nvl(sm.chain_code,'nochain') in \n                      ( \n                        select  ucd.code \n                        from    user_comdata    ucd \n                        where   ucd.user_id =  :3  and \n                                ucd.code_type = 'CHAIN' \n                      ) or \n                    nvl(sm.corp_code,'nocorp') in \n                      ( \n                        select  decode(ucd.code, \n                                       '99999',nvl(sm.corp_code,'nocorp'), \n                                       ucd.code) \n                        from    user_comdata    ucd \n                        where   ucd.user_id =  :4  and \n                                ucd.code_type = 'CORP' \n                      ) \n                  ) and \n                  sm.chain_code = nvl( :5 ,'none') and\n                  trunc(sm.batch_date,'month') =  :6  and \n                  sm.fleet_code =  :7 \n          group by  sm.fleet_code,\n                    sm.account_name,\n                    sm.account_code\n          order by decode(fleet_code,'nofleet','ZZZ',fleet_code),\n                   sm.account_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setLong(2,userId);
   __sJT_st.setLong(3,userId);
   __sJT_st.setLong(4,userId);
   __sJT_st.setString(5,detailChainCode);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setString(7,detailFleetCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2805^9*/                          
      }
      else if ( getReportType() == RT_PAID_SUMMARY )
      {
        if ( isSubmitted() )
        {
          String  checkNumber     = getData("checkNumber");
          String  controlNumber   = getData("controlNumber");
          String  locCode         = getData("locationCode");
          
          locType = getField("locationType").asInteger();

          // replace * wildcard chars with SQL % wildcard cards
          checkNumber   = checkNumber.replace('*','%');
          controlNumber = controlNumber.replace('*','%');
          
          // if the control number is not using wildcards, then pad
          // to 9 bytes with leading zeros
          if ( !controlNumber.equals("") &&
               controlNumber.indexOf("%") < 0 && 
               controlNumber.length() < 9 )
          {
            StringBuffer temp = new StringBuffer(controlNumber);
            while( temp.length() < 9 )
            {
              temp.insert(0,'0');
            }
            controlNumber = temp.toString();
          }
          
          System.out.println("checkNumber   : " + checkNumber);//@
          System.out.println("controlNumber : " + controlNumber);//@
          
          switch( locType )
          {
            case LT_SERVICE_CENTER:   // service center
            case LT_RECEIVER:   // receiver
              /*@lineinfo:generated-code*//*@lineinfo:2842^15*/

//  ************************************************************
//  #sql [Ctx] it = { select  sgt.receiver_code                   as receiver_code,
//                          sgt.location_code                   as location_code,
//                          sgt.settlement_paid_date            as paid_date,
//                          sgt.settlement_close_time           as close_time,
//                          sgt.settlement_control_number       as control_number,
//                          sgt.settlement_check_number         as check_number,
//                          sgt.settlement_payment_amount       as payment_amount,
//                          sgt.settlement_group_number         as settlement_group_number,
//                          sgt.settlement_paid_date            as settlement_date
//                  from    comdata_detail_file_paid_sgt    sgt
//                  where   (
//                            ( :locType = 0 and
//                              sgt.location_code = :locCode ) or
//                            ( :locType = 2 and
//                              sgt.receiver_code = :locCode )
//                          ) and
//                          sgt.settlement_paid_date between :beginDate and :endDate and
//                          (
//                            sgt.location_code in
//                            (
//                              select  ucd.code
//                              from    user_comdata    ucd
//                              where   ucd.user_id = :userId and
//                                      ucd.code_type = 'LOC' 
//                            ) or
//                            nvl(sgt.corp_chain_code,'nochain') in
//                            ( 
//                              select  ucd.code
//                              from    user_comdata    ucd
//                              where   ucd.user_id = :userId and
//                                      ucd.code_type = 'CHAIN'                      
//                            ) or 
//                            nvl(sgt.corp_chain_code,'nocorp') in
//                            (
//                              select  ucd.code
//                              from    user_comdata    ucd
//                              where   ucd.user_id = :userId and
//                                      ucd.code_type = 'CORP'                      
//                            ) or
//                            exists
//                            (
//                              select  ucd.code
//                              from    user_comdata ucd
//                              where   ucd.user_id = :userId and
//                                      ucd.code_type = 'CORP' and
//                                      ucd.code = '99999'
//                            )
//                          ) and
//                          (
//                            nvl(:controlNumber,'none') = 'none' or
//                            sgt.settlement_control_number like :controlNumber
//                          ) and
//                          (
//                            nvl(:checkNumber,'none') = 'none' or
//                            sgt.settlement_check_number like :checkNumber
//                          )
//                  order by paid_date desc, close_time desc, control_number              
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sgt.receiver_code                   as receiver_code,\n                        sgt.location_code                   as location_code,\n                        sgt.settlement_paid_date            as paid_date,\n                        sgt.settlement_close_time           as close_time,\n                        sgt.settlement_control_number       as control_number,\n                        sgt.settlement_check_number         as check_number,\n                        sgt.settlement_payment_amount       as payment_amount,\n                        sgt.settlement_group_number         as settlement_group_number,\n                        sgt.settlement_paid_date            as settlement_date\n                from    comdata_detail_file_paid_sgt    sgt\n                where   (\n                          (  :1  = 0 and\n                            sgt.location_code =  :2  ) or\n                          (  :3  = 2 and\n                            sgt.receiver_code =  :4  )\n                        ) and\n                        sgt.settlement_paid_date between  :5  and  :6  and\n                        (\n                          sgt.location_code in\n                          (\n                            select  ucd.code\n                            from    user_comdata    ucd\n                            where   ucd.user_id =  :7  and\n                                    ucd.code_type = 'LOC' \n                          ) or\n                          nvl(sgt.corp_chain_code,'nochain') in\n                          ( \n                            select  ucd.code\n                            from    user_comdata    ucd\n                            where   ucd.user_id =  :8  and\n                                    ucd.code_type = 'CHAIN'                      \n                          ) or \n                          nvl(sgt.corp_chain_code,'nocorp') in\n                          (\n                            select  ucd.code\n                            from    user_comdata    ucd\n                            where   ucd.user_id =  :9  and\n                                    ucd.code_type = 'CORP'                      \n                          ) or\n                          exists\n                          (\n                            select  ucd.code\n                            from    user_comdata ucd\n                            where   ucd.user_id =  :10  and\n                                    ucd.code_type = 'CORP' and\n                                    ucd.code = '99999'\n                          )\n                        ) and\n                        (\n                          nvl( :11 ,'none') = 'none' or\n                          sgt.settlement_control_number like  :12 \n                        ) and\n                        (\n                          nvl( :13 ,'none') = 'none' or\n                          sgt.settlement_check_number like  :14 \n                        )\n                order by paid_date desc, close_time desc, control_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,locType);
   __sJT_st.setString(2,locCode);
   __sJT_st.setInt(3,locType);
   __sJT_st.setString(4,locCode);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setLong(7,userId);
   __sJT_st.setLong(8,userId);
   __sJT_st.setLong(9,userId);
   __sJT_st.setLong(10,userId);
   __sJT_st.setString(11,controlNumber);
   __sJT_st.setString(12,controlNumber);
   __sJT_st.setString(13,checkNumber);
   __sJT_st.setString(14,checkNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2901^15*/
              break;
              
            case LT_CHAIN:   // chain
              /*@lineinfo:generated-code*//*@lineinfo:2905^15*/

//  ************************************************************
//  #sql [Ctx] it = { select  sgt.location_code         as location_code,
//                          lh.location_name          as location_name,
//                          mfc.city                  as location_city,
//                          sgt.receiver_code         as receiver_code,
//                          count(sgt.location_code)  as payment_count,
//                          sum(sgt.settlement_payment_amount)  as payment_amount
//                  from    comdata_detail_file_paid_sgt  sgt,
//                          comdata_detail_file_paid_lh   lh,
//                          mif_comdata                   mfc
//                  where   sgt.corp_chain_code = :locCode and
//                          (
//                            sgt.location_code in
//                            (
//                              select  ucd.code
//                              from    user_comdata    ucd
//                              where   ucd.user_id = :userId and
//                                      ucd.code_type = 'LOC' 
//                            ) or
//                            nvl(sgt.corp_chain_code,'nochain') in
//                            ( 
//                              select  ucd.code
//                              from    user_comdata    ucd
//                              where   ucd.user_id = :userId and
//                                      ucd.code_type = 'CHAIN'                      
//                            ) or 
//                            nvl(sgt.corp_chain_code,'nocorp') in
//                            (
//                              select  ucd.code
//                              from    user_comdata    ucd
//                              where   ucd.user_id = :userId and
//                                      ucd.code_type = 'CORP'                      
//                            ) or
//                            exists
//                            (
//                              select  ucd.code
//                              from    user_comdata ucd
//                              where   ucd.user_id = :userId and
//                                      ucd.code_type = 'CORP' and
//                                      ucd.code = '99999'
//                            )
//                          ) and
//                          sgt.settlement_paid_date between :beginDate and :endDate and
//                          (
//                            nvl(:controlNumber,'none') = 'none' or
//                            sgt.settlement_control_number like :controlNumber
//                          ) and
//                          (
//                            nvl(:checkNumber,'none') = 'none' or
//                            sgt.settlement_check_number like :checkNumber
//                          ) and
//                          lh.batch_number = sgt.batch_number and
//                          mfc.location_code = sgt.location_code
//                  group by sgt.location_code,lh.location_name,mfc.city,sgt.receiver_code              
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sgt.location_code         as location_code,\n                        lh.location_name          as location_name,\n                        mfc.city                  as location_city,\n                        sgt.receiver_code         as receiver_code,\n                        count(sgt.location_code)  as payment_count,\n                        sum(sgt.settlement_payment_amount)  as payment_amount\n                from    comdata_detail_file_paid_sgt  sgt,\n                        comdata_detail_file_paid_lh   lh,\n                        mif_comdata                   mfc\n                where   sgt.corp_chain_code =  :1  and\n                        (\n                          sgt.location_code in\n                          (\n                            select  ucd.code\n                            from    user_comdata    ucd\n                            where   ucd.user_id =  :2  and\n                                    ucd.code_type = 'LOC' \n                          ) or\n                          nvl(sgt.corp_chain_code,'nochain') in\n                          ( \n                            select  ucd.code\n                            from    user_comdata    ucd\n                            where   ucd.user_id =  :3  and\n                                    ucd.code_type = 'CHAIN'                      \n                          ) or \n                          nvl(sgt.corp_chain_code,'nocorp') in\n                          (\n                            select  ucd.code\n                            from    user_comdata    ucd\n                            where   ucd.user_id =  :4  and\n                                    ucd.code_type = 'CORP'                      \n                          ) or\n                          exists\n                          (\n                            select  ucd.code\n                            from    user_comdata ucd\n                            where   ucd.user_id =  :5  and\n                                    ucd.code_type = 'CORP' and\n                                    ucd.code = '99999'\n                          )\n                        ) and\n                        sgt.settlement_paid_date between  :6  and  :7  and\n                        (\n                          nvl( :8 ,'none') = 'none' or\n                          sgt.settlement_control_number like  :9 \n                        ) and\n                        (\n                          nvl( :10 ,'none') = 'none' or\n                          sgt.settlement_check_number like  :11 \n                        ) and\n                        lh.batch_number = sgt.batch_number and\n                        mfc.location_code = sgt.location_code\n                group by sgt.location_code,lh.location_name,mfc.city,sgt.receiver_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,locCode);
   __sJT_st.setLong(2,userId);
   __sJT_st.setLong(3,userId);
   __sJT_st.setLong(4,userId);
   __sJT_st.setLong(5,userId);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setString(8,controlNumber);
   __sJT_st.setString(9,controlNumber);
   __sJT_st.setString(10,checkNumber);
   __sJT_st.setString(11,checkNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2960^15*/
              break;
          }
        }
      }
      else if ( getReportType() == RT_PAID_DETAILS )
      {
        long groupId = getLong("settleGroup");
        /*@lineinfo:generated-code*//*@lineinfo:2968^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mfc.merchant_number             as merchant_number,
//                    dt.location_code                as location_code,
//                    dt.corp_chain_code              as chain_code,
//                    mfc.location_name               as location_name,
//                    sgt.settlement_control_number   as settlement_control_number,
//                    sgt.settlement_group_number     as settlement_group_number,
//                    sgt.settlement_paid_date        as settlement_date,
//                    dt.customer_account_code        as customer_account_code,
//                    dt.invoice_number               as invoice_number,
//                    dt.invoice_date                 as invoice_date,
//                    dt.invoice_time                 as invoice_time,
//                    dt.control_number               as control_number,
//                    dt.diesel_1_gallons             as diesel_1_gallons,
//                    dt.diesel_1_cost_per_gallon     as diesel_1_cost_per_gallon,
//                    dt.diesel_1_total_cost          as diesel_1_total_cost,
//                    dt.diesel_2_gallons             as diesel_2_gallons,
//                    dt.diesel_2_cost_per_gallon     as diesel_2_cost_per_gallon,
//                    dt.diesel_2_total_cost          as diesel_2_total_cost,
//                    dt.reefer_gallons               as reefer_gallons,
//                    dt.reefer_cost_per_gallon       as reefer_cost_per_gallon,
//                    dt.reefer_total_cost            as reefer_total_cost,
//                    dt.other_gallons                as other_gallons,
//                    dt.other_cost_per_gallon        as other_cost_per_gallon,
//                    dt.other_total_cost             as other_total_cost,
//                    nvl(dt.fuel_direct_bill,'N')    as fuel_direct_bill,
//                    dt.oil_purchased                as oil_quarts,
//                    dt.oil_cost_per_quart           as oil_cost_per_quart,
//                    dt.oil_total_cost               as oil_total_cost,
//                    nvl(dt.oil_direct_bill,'N')     as oil_direct_bill,
//                    dt.cash_advance_amount          as cash_advance_amount,
//                    nvl(dt.cash_direct_bill,'N')    as cash_direct_bill,
//                    nvl(pc1.product_desc, 'Not Available')
//                                                    as product_code_1,
//                    dt.product_code_1_sign          as product_code_1_sign,
//                    dt.product_code_1_amount        as product_code_1_amount,
//                    nvl(dt.product_code_1_direct_bill,'N')
//                                                    as product_code_1_direct_bill,
//                    nvl(pc2.product_desc, 'Not Available')
//                                                    as product_code_2,
//                    dt.product_code_2_sign          as product_code_2_sign,
//                    dt.product_code_2_amount        as product_code_2_amount,
//                    nvl(dt.product_code_2_direct_bill,'N')
//                                                    as product_code_2_direct_bill,
//                    nvl(pc3.product_desc, 'Not Available')
//                                                    as product_code_3,
//                    dt.product_code_3_sign          as product_code_3_sign,
//                    dt.product_code_3_amount        as product_code_3_amount,
//                    nvl(dt.product_code_3_direct_bill,'N')
//                                                    as product_code_3_direct_bill,
//                    dt.discount_amount              as discount_amount,
//                    dt.discount_type                as discount_type,
//                    dt.fee_type                     as fee_type,
//                    dt.fee_amount                   as fee_amount,
//                    dt.payment_cycle                as payment_cycle,
//                    dt.payment_pending              as payment_pending,
//                    dt.tran_type                    as tran_type,
//                    dt.payment_sign                 as payment_sign,
//                    ( dt.payment_amount *
//                      decode(nvl(dt.payment_sign,'+'),'-',-1,1) )            
//                                                    as payment_amount,
//                    dt.adjustment_sign              as adjustment_sign,
//                    dt.adjustment_amount            as adjustment_amount,
//                    dt.credit_flag                  as credit_flag,
//                    dt.balanced_ind                 as balanced_ind,
//                    dt.closed_ind                   as closed_ind,
//                    dt.paid_ind                     as paid_ind
//            from    comdata_detail_file_paid_sgt  sgt,
//                    comdata_detail_file_paid_dt   dt,
//                    comdata_product_codes         pc1,
//                    comdata_product_codes         pc2,
//                    comdata_product_codes         pc3,
//                    mif_comdata                   mfc
//            where   sgt.settlement_group_number = :groupId and
//                    sgt.settlement_paid_date between :beginDate and :endDate and
//                    (
//                      sgt.location_code in
//                      (
//                        select  ucd.code
//                        from    user_comdata    ucd
//                        where   ucd.user_id = :userId and
//                                ucd.code_type = 'LOC' 
//                      ) or
//                      nvl(sgt.corp_chain_code,'nochain') in
//                      ( 
//                        select  ucd.code
//                        from    user_comdata    ucd
//                        where   ucd.user_id = :userId and
//                                ucd.code_type = 'CHAIN'                      
//                      ) or 
//                      nvl(sgt.corp_chain_code,'nocorp') in
//                      (
//                        select  ucd.code
//                        from    user_comdata    ucd
//                        where   ucd.user_id = :userId and
//                                ucd.code_type = 'CORP'                      
//                      ) or
//                      exists
//                      (
//                        select  ucd.code
//                        from    user_comdata ucd
//                        where   ucd.user_id = :userId and
//                                ucd.code_type = 'CORP' and
//                                ucd.code = '99999'
//                      )
//                    ) and
//                    (
//                      (
//                        sgt.settlement_group_number <= :SETTLE_GROUP_MAX and
//                        dt.settlement_group_number = sgt.settlement_group_number and
//                        nvl(dt.settlement_date,dt.paid_settlement_date) = sgt.settlement_paid_date 
//                      )or
//                      (
//                        sgt.settlement_group_number > :SETTLE_GROUP_MAX and
//                        dt.paid_settlement_group_number = sgt.settlement_group_number and
//                        dt.paid_settlement_date = sgt.settlement_paid_date 
//                      )
//                    ) and
//                    pc1.product_code(+) = dt.product_code_1 and
//                    pc2.product_code(+) = dt.product_code_2 and
//                    pc3.product_code(+) = dt.product_code_3 and
//                    mfc.location_code = sgt.location_code
//            order by  sgt.location_code, mfc.location_name, 
//                      dt.invoice_time, dt.invoice_number        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mfc.merchant_number             as merchant_number,\n                  dt.location_code                as location_code,\n                  dt.corp_chain_code              as chain_code,\n                  mfc.location_name               as location_name,\n                  sgt.settlement_control_number   as settlement_control_number,\n                  sgt.settlement_group_number     as settlement_group_number,\n                  sgt.settlement_paid_date        as settlement_date,\n                  dt.customer_account_code        as customer_account_code,\n                  dt.invoice_number               as invoice_number,\n                  dt.invoice_date                 as invoice_date,\n                  dt.invoice_time                 as invoice_time,\n                  dt.control_number               as control_number,\n                  dt.diesel_1_gallons             as diesel_1_gallons,\n                  dt.diesel_1_cost_per_gallon     as diesel_1_cost_per_gallon,\n                  dt.diesel_1_total_cost          as diesel_1_total_cost,\n                  dt.diesel_2_gallons             as diesel_2_gallons,\n                  dt.diesel_2_cost_per_gallon     as diesel_2_cost_per_gallon,\n                  dt.diesel_2_total_cost          as diesel_2_total_cost,\n                  dt.reefer_gallons               as reefer_gallons,\n                  dt.reefer_cost_per_gallon       as reefer_cost_per_gallon,\n                  dt.reefer_total_cost            as reefer_total_cost,\n                  dt.other_gallons                as other_gallons,\n                  dt.other_cost_per_gallon        as other_cost_per_gallon,\n                  dt.other_total_cost             as other_total_cost,\n                  nvl(dt.fuel_direct_bill,'N')    as fuel_direct_bill,\n                  dt.oil_purchased                as oil_quarts,\n                  dt.oil_cost_per_quart           as oil_cost_per_quart,\n                  dt.oil_total_cost               as oil_total_cost,\n                  nvl(dt.oil_direct_bill,'N')     as oil_direct_bill,\n                  dt.cash_advance_amount          as cash_advance_amount,\n                  nvl(dt.cash_direct_bill,'N')    as cash_direct_bill,\n                  nvl(pc1.product_desc, 'Not Available')\n                                                  as product_code_1,\n                  dt.product_code_1_sign          as product_code_1_sign,\n                  dt.product_code_1_amount        as product_code_1_amount,\n                  nvl(dt.product_code_1_direct_bill,'N')\n                                                  as product_code_1_direct_bill,\n                  nvl(pc2.product_desc, 'Not Available')\n                                                  as product_code_2,\n                  dt.product_code_2_sign          as product_code_2_sign,\n                  dt.product_code_2_amount        as product_code_2_amount,\n                  nvl(dt.product_code_2_direct_bill,'N')\n                                                  as product_code_2_direct_bill,\n                  nvl(pc3.product_desc, 'Not Available')\n                                                  as product_code_3,\n                  dt.product_code_3_sign          as product_code_3_sign,\n                  dt.product_code_3_amount        as product_code_3_amount,\n                  nvl(dt.product_code_3_direct_bill,'N')\n                                                  as product_code_3_direct_bill,\n                  dt.discount_amount              as discount_amount,\n                  dt.discount_type                as discount_type,\n                  dt.fee_type                     as fee_type,\n                  dt.fee_amount                   as fee_amount,\n                  dt.payment_cycle                as payment_cycle,\n                  dt.payment_pending              as payment_pending,\n                  dt.tran_type                    as tran_type,\n                  dt.payment_sign                 as payment_sign,\n                  ( dt.payment_amount *\n                    decode(nvl(dt.payment_sign,'+'),'-',-1,1) )            \n                                                  as payment_amount,\n                  dt.adjustment_sign              as adjustment_sign,\n                  dt.adjustment_amount            as adjustment_amount,\n                  dt.credit_flag                  as credit_flag,\n                  dt.balanced_ind                 as balanced_ind,\n                  dt.closed_ind                   as closed_ind,\n                  dt.paid_ind                     as paid_ind\n          from    comdata_detail_file_paid_sgt  sgt,\n                  comdata_detail_file_paid_dt   dt,\n                  comdata_product_codes         pc1,\n                  comdata_product_codes         pc2,\n                  comdata_product_codes         pc3,\n                  mif_comdata                   mfc\n          where   sgt.settlement_group_number =  :1  and\n                  sgt.settlement_paid_date between  :2  and  :3  and\n                  (\n                    sgt.location_code in\n                    (\n                      select  ucd.code\n                      from    user_comdata    ucd\n                      where   ucd.user_id =  :4  and\n                              ucd.code_type = 'LOC' \n                    ) or\n                    nvl(sgt.corp_chain_code,'nochain') in\n                    ( \n                      select  ucd.code\n                      from    user_comdata    ucd\n                      where   ucd.user_id =  :5  and\n                              ucd.code_type = 'CHAIN'                      \n                    ) or \n                    nvl(sgt.corp_chain_code,'nocorp') in\n                    (\n                      select  ucd.code\n                      from    user_comdata    ucd\n                      where   ucd.user_id =  :6  and\n                              ucd.code_type = 'CORP'                      \n                    ) or\n                    exists\n                    (\n                      select  ucd.code\n                      from    user_comdata ucd\n                      where   ucd.user_id =  :7  and\n                              ucd.code_type = 'CORP' and\n                              ucd.code = '99999'\n                    )\n                  ) and\n                  (\n                    (\n                      sgt.settlement_group_number <=  :8  and\n                      dt.settlement_group_number = sgt.settlement_group_number and\n                      nvl(dt.settlement_date,dt.paid_settlement_date) = sgt.settlement_paid_date \n                    )or\n                    (\n                      sgt.settlement_group_number >  :9  and\n                      dt.paid_settlement_group_number = sgt.settlement_group_number and\n                      dt.paid_settlement_date = sgt.settlement_paid_date \n                    )\n                  ) and\n                  pc1.product_code(+) = dt.product_code_1 and\n                  pc2.product_code(+) = dt.product_code_2 and\n                  pc3.product_code(+) = dt.product_code_3 and\n                  mfc.location_code = sgt.location_code\n          order by  sgt.location_code, mfc.location_name, \n                    dt.invoice_time, dt.invoice_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,groupId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setLong(4,userId);
   __sJT_st.setLong(5,userId);
   __sJT_st.setLong(6,userId);
   __sJT_st.setLong(7,userId);
   __sJT_st.setLong(8,SETTLE_GROUP_MAX);
   __sJT_st.setLong(9,SETTLE_GROUP_MAX);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3093^9*/
      }
      else if ( getReportType() == RT_ITEM_SEARCH )
      {
        if ( isSubmitted() ) 
        {
          String  controlNumber   = getData("tranControlNumber");
          String  invoiceNumber   = getData("invoiceNumber");
          String  searchAmount    = getData("searchAmount").trim();
          String  locCode         = getData("locationCode");
          int     paidStatus      = getField("paidStatus").asInteger();
          
          // update the search amount
          invoiceNumber = invoiceNumber.replace('*','%');
          searchAmount  = searchAmount.replace('*','%');
          
          locType = getField("locationType").asInteger();
      
          /*@lineinfo:generated-code*//*@lineinfo:3111^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  mfc.merchant_number             as merchant_number,
//                      dt.location_code                as location_code,
//                      dt.corp_chain_code              as chain_code,
//                      mfc.location_name               as location_name,
//                      sgt.settlement_control_number   as settlement_control_number,
//                      sgt.settlement_group_number     as settlement_group_number,
//                      sgt.settlement_paid_date        as settlement_date,
//                      dt.customer_account_code        as customer_account_code,
//                      dt.invoice_number               as invoice_number,
//                      dt.invoice_date                 as invoice_date,
//                      dt.invoice_time                 as invoice_time,
//                      dt.control_number               as control_number,
//                      dt.diesel_1_gallons             as diesel_1_gallons,
//                      dt.diesel_1_cost_per_gallon     as diesel_1_cost_per_gallon,
//                      dt.diesel_1_total_cost          as diesel_1_total_cost,
//                      dt.diesel_2_gallons             as diesel_2_gallons,
//                      dt.diesel_2_cost_per_gallon     as diesel_2_cost_per_gallon,
//                      dt.diesel_2_total_cost          as diesel_2_total_cost,
//                      dt.reefer_gallons               as reefer_gallons,
//                      dt.reefer_cost_per_gallon       as reefer_cost_per_gallon,
//                      dt.reefer_total_cost            as reefer_total_cost,
//                      dt.other_gallons                as other_gallons,
//                      dt.other_cost_per_gallon        as other_cost_per_gallon,
//                      dt.other_total_cost             as other_total_cost,
//                      nvl(dt.fuel_direct_bill,'N')    as fuel_direct_bill,
//                      dt.oil_purchased                as oil_quarts,
//                      dt.oil_cost_per_quart           as oil_cost_per_quart,
//                      dt.oil_total_cost               as oil_total_cost,
//                      nvl(dt.oil_direct_bill,'N')     as oil_direct_bill,
//                      dt.cash_advance_amount          as cash_advance_amount,
//                      nvl(dt.cash_direct_bill,'N')    as cash_direct_bill,
//                      nvl(pc1.product_desc, 'Not Available')
//                                                      as product_code_1,
//                      dt.product_code_1_sign          as product_code_1_sign,
//                      dt.product_code_1_amount        as product_code_1_amount,
//                      nvl(dt.product_code_1_direct_bill,'N')
//                                                      as product_code_1_direct_bill,
//                      nvl(pc2.product_desc, 'Not Available')
//                                                      as product_code_2,
//                      dt.product_code_2_sign          as product_code_2_sign,
//                      dt.product_code_2_amount        as product_code_2_amount,
//                      nvl(dt.product_code_2_direct_bill,'N')
//                                                      as product_code_2_direct_bill,
//                      nvl(pc3.product_desc, 'Not Available')
//                                                      as product_code_3,
//                      dt.product_code_3_sign          as product_code_3_sign,
//                      dt.product_code_3_amount        as product_code_3_amount,
//                      nvl(dt.product_code_3_direct_bill,'N')
//                                                      as product_code_3_direct_bill,
//                      dt.discount_amount              as discount_amount,
//                      dt.discount_type                as discount_type,
//                      dt.fee_type                     as fee_type,
//                      dt.fee_amount                   as fee_amount,
//                      dt.payment_cycle                as payment_cycle,
//                      dt.payment_pending              as payment_pending,
//                      dt.tran_type                    as tran_type,
//                      dt.payment_sign                 as payment_sign,
//                      ( dt.payment_amount *
//                        decode(nvl(dt.payment_sign,'+'),'-',-1,1) )            
//                                                      as payment_amount,
//                      dt.adjustment_sign              as adjustment_sign,
//                      dt.adjustment_amount            as adjustment_amount,
//                      dt.credit_flag                  as credit_flag,
//                      dt.balanced_ind                 as balanced_ind,
//                      dt.closed_ind                   as closed_ind,
//                      decode( nvl(dt.payment_pending,'N'),
//                              'N','Y',
//                              'N')                    as paid_ind
//              from    comdata_detail_file_paid_dt   dt,
//                      comdata_detail_file_paid_sgt  sgt,
//                      comdata_product_codes         pc1,
//                      comdata_product_codes         pc2,
//                      comdata_product_codes         pc3,
//                      mif_comdata                   mfc
//              where   (
//                        ( :locType = :LT_SERVICE_CENTER and -- 0 and
//                          dt.location_code = :locCode ) or
//                        ( :locType = :LT_CHAIN and -- 1 and
//                          dt.corp_chain_code = :locCode ) or
//                        ( :locType = :LT_RECEIVER and -- 2 and
//                          dt.receiver_code = :locCode )
//                      ) and
//                      dt.invoice_date between :beginDate and :endDate and
//                      ( 
//                        nvl(:controlNumber,'none') = 'none' or
//                        dt.control_number = (lpad(to_char(dt.invoice_date,'dd'),2,'0') || lpad(:controlNumber,7,'0'))                   
//                      ) and
//                      (
//                        nvl(:invoiceNumber,'none') = 'none' or
//                        dt.invoice_number like :invoiceNumber
//                      ) and 
//                      (
//                        nvl(:searchAmount,'none') = 'none' or 
//                        dt.invoice_total like :searchAmount
//                      ) and 
//                      (
//                        :paidStatus = 0 or -- all
//                        (
//                          :paidStatus = 1 and -- paid only
//                          nvl(dt.payment_pending,'N') = 'N'
//                        ) or
//                        (
//                          :paidStatus = 2 and -- unpaid only
//                          nvl(dt.payment_pending,'N') = 'Y'
//                        )
//                      ) and
//                      sgt.settlement_group_number = nvl(dt.paid_settlement_group_number,dt.settlement_group_number) and
//                      sgt.settlement_paid_date = nvl(dt.paid_settlement_date,dt.settlement_date) and
//                      (
//                        sgt.location_code in
//                        (
//                          select  ucd.code
//                          from    user_comdata    ucd
//                          where   ucd.user_id = :userId and
//                                  ucd.code_type = 'LOC' 
//                        ) or
//                        nvl(sgt.corp_chain_code,'nochain') in
//                        ( 
//                          select  ucd.code
//                          from    user_comdata    ucd
//                          where   ucd.user_id = :userId and
//                                  ucd.code_type = 'CHAIN'                      
//                        ) or 
//                        nvl(sgt.corp_chain_code,'nocorp') in
//                        (
//                          select  ucd.code
//                          from    user_comdata    ucd
//                          where   ucd.user_id = :userId and
//                                  ucd.code_type = 'CORP'                      
//                        ) or
//                        exists
//                        (
//                          select  ucd.code
//                          from    user_comdata ucd
//                          where   ucd.user_id = :userId and
//                                  ucd.code_type = 'CORP' and
//                                  ucd.code = '99999'
//                        )
//                      ) and
//                      pc1.product_code(+) = dt.product_code_1 and
//                      pc2.product_code(+) = dt.product_code_2 and
//                      pc3.product_code(+) = dt.product_code_3 and
//                      mfc.location_code = sgt.location_code
//              order by  sgt.location_code, mfc.location_name, 
//                        dt.invoice_time,dt.invoice_number        
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mfc.merchant_number             as merchant_number,\n                    dt.location_code                as location_code,\n                    dt.corp_chain_code              as chain_code,\n                    mfc.location_name               as location_name,\n                    sgt.settlement_control_number   as settlement_control_number,\n                    sgt.settlement_group_number     as settlement_group_number,\n                    sgt.settlement_paid_date        as settlement_date,\n                    dt.customer_account_code        as customer_account_code,\n                    dt.invoice_number               as invoice_number,\n                    dt.invoice_date                 as invoice_date,\n                    dt.invoice_time                 as invoice_time,\n                    dt.control_number               as control_number,\n                    dt.diesel_1_gallons             as diesel_1_gallons,\n                    dt.diesel_1_cost_per_gallon     as diesel_1_cost_per_gallon,\n                    dt.diesel_1_total_cost          as diesel_1_total_cost,\n                    dt.diesel_2_gallons             as diesel_2_gallons,\n                    dt.diesel_2_cost_per_gallon     as diesel_2_cost_per_gallon,\n                    dt.diesel_2_total_cost          as diesel_2_total_cost,\n                    dt.reefer_gallons               as reefer_gallons,\n                    dt.reefer_cost_per_gallon       as reefer_cost_per_gallon,\n                    dt.reefer_total_cost            as reefer_total_cost,\n                    dt.other_gallons                as other_gallons,\n                    dt.other_cost_per_gallon        as other_cost_per_gallon,\n                    dt.other_total_cost             as other_total_cost,\n                    nvl(dt.fuel_direct_bill,'N')    as fuel_direct_bill,\n                    dt.oil_purchased                as oil_quarts,\n                    dt.oil_cost_per_quart           as oil_cost_per_quart,\n                    dt.oil_total_cost               as oil_total_cost,\n                    nvl(dt.oil_direct_bill,'N')     as oil_direct_bill,\n                    dt.cash_advance_amount          as cash_advance_amount,\n                    nvl(dt.cash_direct_bill,'N')    as cash_direct_bill,\n                    nvl(pc1.product_desc, 'Not Available')\n                                                    as product_code_1,\n                    dt.product_code_1_sign          as product_code_1_sign,\n                    dt.product_code_1_amount        as product_code_1_amount,\n                    nvl(dt.product_code_1_direct_bill,'N')\n                                                    as product_code_1_direct_bill,\n                    nvl(pc2.product_desc, 'Not Available')\n                                                    as product_code_2,\n                    dt.product_code_2_sign          as product_code_2_sign,\n                    dt.product_code_2_amount        as product_code_2_amount,\n                    nvl(dt.product_code_2_direct_bill,'N')\n                                                    as product_code_2_direct_bill,\n                    nvl(pc3.product_desc, 'Not Available')\n                                                    as product_code_3,\n                    dt.product_code_3_sign          as product_code_3_sign,\n                    dt.product_code_3_amount        as product_code_3_amount,\n                    nvl(dt.product_code_3_direct_bill,'N')\n                                                    as product_code_3_direct_bill,\n                    dt.discount_amount              as discount_amount,\n                    dt.discount_type                as discount_type,\n                    dt.fee_type                     as fee_type,\n                    dt.fee_amount                   as fee_amount,\n                    dt.payment_cycle                as payment_cycle,\n                    dt.payment_pending              as payment_pending,\n                    dt.tran_type                    as tran_type,\n                    dt.payment_sign                 as payment_sign,\n                    ( dt.payment_amount *\n                      decode(nvl(dt.payment_sign,'+'),'-',-1,1) )            \n                                                    as payment_amount,\n                    dt.adjustment_sign              as adjustment_sign,\n                    dt.adjustment_amount            as adjustment_amount,\n                    dt.credit_flag                  as credit_flag,\n                    dt.balanced_ind                 as balanced_ind,\n                    dt.closed_ind                   as closed_ind,\n                    decode( nvl(dt.payment_pending,'N'),\n                            'N','Y',\n                            'N')                    as paid_ind\n            from    comdata_detail_file_paid_dt   dt,\n                    comdata_detail_file_paid_sgt  sgt,\n                    comdata_product_codes         pc1,\n                    comdata_product_codes         pc2,\n                    comdata_product_codes         pc3,\n                    mif_comdata                   mfc\n            where   (\n                      (  :1  =  :2  and -- 0 and\n                        dt.location_code =  :3  ) or\n                      (  :4  =  :5  and -- 1 and\n                        dt.corp_chain_code =  :6  ) or\n                      (  :7  =  :8  and -- 2 and\n                        dt.receiver_code =  :9  )\n                    ) and\n                    dt.invoice_date between  :10  and  :11  and\n                    ( \n                      nvl( :12 ,'none') = 'none' or\n                      dt.control_number = (lpad(to_char(dt.invoice_date,'dd'),2,'0') || lpad( :13 ,7,'0'))                   \n                    ) and\n                    (\n                      nvl( :14 ,'none') = 'none' or\n                      dt.invoice_number like  :15 \n                    ) and \n                    (\n                      nvl( :16 ,'none') = 'none' or \n                      dt.invoice_total like  :17 \n                    ) and \n                    (\n                       :18  = 0 or -- all\n                      (\n                         :19  = 1 and -- paid only\n                        nvl(dt.payment_pending,'N') = 'N'\n                      ) or\n                      (\n                         :20  = 2 and -- unpaid only\n                        nvl(dt.payment_pending,'N') = 'Y'\n                      )\n                    ) and\n                    sgt.settlement_group_number = nvl(dt.paid_settlement_group_number,dt.settlement_group_number) and\n                    sgt.settlement_paid_date = nvl(dt.paid_settlement_date,dt.settlement_date) and\n                    (\n                      sgt.location_code in\n                      (\n                        select  ucd.code\n                        from    user_comdata    ucd\n                        where   ucd.user_id =  :21  and\n                                ucd.code_type = 'LOC' \n                      ) or\n                      nvl(sgt.corp_chain_code,'nochain') in\n                      ( \n                        select  ucd.code\n                        from    user_comdata    ucd\n                        where   ucd.user_id =  :22  and\n                                ucd.code_type = 'CHAIN'                      \n                      ) or \n                      nvl(sgt.corp_chain_code,'nocorp') in\n                      (\n                        select  ucd.code\n                        from    user_comdata    ucd\n                        where   ucd.user_id =  :23  and\n                                ucd.code_type = 'CORP'                      \n                      ) or\n                      exists\n                      (\n                        select  ucd.code\n                        from    user_comdata ucd\n                        where   ucd.user_id =  :24  and\n                                ucd.code_type = 'CORP' and\n                                ucd.code = '99999'\n                      )\n                    ) and\n                    pc1.product_code(+) = dt.product_code_1 and\n                    pc2.product_code(+) = dt.product_code_2 and\n                    pc3.product_code(+) = dt.product_code_3 and\n                    mfc.location_code = sgt.location_code\n            order by  sgt.location_code, mfc.location_name, \n                      dt.invoice_time,dt.invoice_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,locType);
   __sJT_st.setInt(2,LT_SERVICE_CENTER);
   __sJT_st.setString(3,locCode);
   __sJT_st.setInt(4,locType);
   __sJT_st.setInt(5,LT_CHAIN);
   __sJT_st.setString(6,locCode);
   __sJT_st.setInt(7,locType);
   __sJT_st.setInt(8,LT_RECEIVER);
   __sJT_st.setString(9,locCode);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setDate(11,endDate);
   __sJT_st.setString(12,controlNumber);
   __sJT_st.setString(13,controlNumber);
   __sJT_st.setString(14,invoiceNumber);
   __sJT_st.setString(15,invoiceNumber);
   __sJT_st.setString(16,searchAmount);
   __sJT_st.setString(17,searchAmount);
   __sJT_st.setInt(18,paidStatus);
   __sJT_st.setInt(19,paidStatus);
   __sJT_st.setInt(20,paidStatus);
   __sJT_st.setLong(21,userId);
   __sJT_st.setLong(22,userId);
   __sJT_st.setLong(23,userId);
   __sJT_st.setLong(24,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3258^11*/
          resultSet = it.getResultSet();
        
          while( resultSet.next() )
          {
            ReportRows.addElement( new RowData( resultSet, RT_PAID_DETAILS ) );
          }
          resultSet.close();
          it.close();
          it = null;
          
          if ( (paidStatus != PT_PAID) && (locType != LT_RECEIVER) )
          {            
            // load the unsettled items
            /*@lineinfo:generated-code*//*@lineinfo:3272^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  mfc.merchant_number             as merchant_number,
//                        dt.location_code                as location_code,
//                        dt.chain_code                   as chain_code,
//                        mfc.location_name               as location_name,
//                        dt.customer_account_code        as customer_account_code,
//                        dt.card_number                  as card_number,
//                        dt.invoice_number               as invoice_number,
//                        dt.invoice_date                 as invoice_date,
//                        dt.invoice_time                 as invoice_time,
//                        dt.control_number               as control_number,
//                        dt.diesel_1_gallons             as diesel_1_gallons,
//                        dt.diesel_1_cost_per_gallon     as diesel_1_cost_per_gallon,
//                        dt.diesel_1_total_cost          as diesel_1_total_cost,
//                        dt.diesel_2_gallons             as diesel_2_gallons,
//                        dt.diesel_2_cost_per_gallon     as diesel_2_cost_per_gallon,
//                        dt.diesel_2_total_cost          as diesel_2_total_cost,
//                        dt.reefer_gallons               as reefer_gallons,
//                        dt.reefer_cost_per_gallon       as reefer_cost_per_gallon,
//                        dt.reefer_total_cost            as reefer_total_cost,
//                        dt.other_gallons                as other_gallons,
//                        dt.other_cost_per_gallon        as other_cost_per_gallon,
//                        dt.other_total_cost             as other_total_cost,
//                        nvl(dt.fuel_direct_bill,'N')    as fuel_direct_bill,
//                        dt.oil_purchased                as oil_quarts,
//                        dt.oil_cost_per_quart           as oil_cost_per_quart,
//                        dt.oil_total_cost               as oil_total_cost,
//                        nvl(dt.oil_direct_bill,'N')     as oil_direct_bill,
//                        dt.cash_advance_amount          as cash_advance_amount,
//                        nvl(dt.cash_direct_bill,'N')    as cash_direct_bill,
//                        nvl(pc1.product_desc, 'Not Available')
//                                                        as product_code_1,
//                        dt.product_code_1_sign          as product_code_1_sign,
//                        dt.product_code_1_amount        as product_code_1_amount,
//                        nvl(dt.product_code_1_direct_bill,'N')
//                                                        as product_code_1_direct_bill,
//                        nvl(pc2.product_desc, 'Not Available')
//                                                        as product_code_2,
//                        dt.product_code_2_sign          as product_code_2_sign,
//                        dt.product_code_2_amount        as product_code_2_amount,
//                        nvl(dt.product_code_2_direct_bill,'N')
//                                                        as product_code_2_direct_bill,
//                        nvl(pc3.product_desc, 'Not Available')
//                                                        as product_code_3,
//                        dt.product_code_3_sign          as product_code_3_sign,
//                        dt.product_code_3_amount        as product_code_3_amount,
//                        nvl(dt.product_code_3_direct_bill,'N')
//                                                        as product_code_3_direct_bill,
//                        dt.discount_amount              as discount_amount,
//                        dt.discount_type                as discount_type,
//                        dt.fee_type                     as fee_type,
//                        dt.fee_percent                  as fee_percent,
//                        dt.fee_amount                   as fee_amount,
//                        dt.payment_cycle                as payment_cycle,
//                        dt.payment_pending              as payment_pending,
//                        dt.tran_type                    as tran_type,
//                        dt.payment_sign                 as payment_sign,
//                        ( dt.payment_amount *
//                          decode(nvl(dt.payment_sign,'+'),'-',-1,1) )            
//                                                        as payment_amount,
//                        dt.adjustment_sign              as adjustment_sign,
//                        dt.adjustment_amount            as adjustment_amount,
//                        dt.customer_id                  as customer_id,
//                        dt.payment_type                 as payment_type,
//                        dt.credit_flag                  as credit_flag,
//                        dt.balanced_ind                 as balanced_ind,
//                        dt.closed_ind                   as closed_ind,
//                        -- always consider these as unpaid
//                        'N'                             as paid_ind,
//                        bt.loc_payment_type             as payment_method,
//                        dt.batch_number                 as batch_number,
//                        dt.batch_tran_id                as batch_tran_id               
//                from    comdata_detail_file_dt  dt,
//                        comdata_detail_file_bt  bt,
//                        comdata_product_codes   pc1,
//                        comdata_product_codes   pc2,
//                        comdata_product_codes   pc3,
//                        mif_comdata             mfc
//                where   (
//                          ( :locType = :LT_SERVICE_CENTER and -- 0 and
//                            dt.location_code = :locCode ) or
//                          ( :locType = :LT_CHAIN and -- 1 and
//                            dt.chain_code = :locCode )
//                        ) and
//                        dt.batch_date between :beginDate and :endDate and
//                        ( 
//                          nvl(:controlNumber,'none') = 'none' or
//                          dt.control_number = (lpad(to_char(dt.invoice_date,'dd'),2,'0') || lpad(:controlNumber,7,'0'))                   
//                        ) and
//                        (
//                          nvl(:invoiceNumber,'none') = 'none' or
//                          dt.invoice_number = :invoiceNumber
//                        ) and 
//                        (
//                          :searchAmount = 0 or
//                          dt.invoice_total = :searchAmount
//                        ) and 
//                        (
//                          :paidStatus = 0 or  -- all
//                          :paidStatus = 2     -- unpaid only
//                        ) and
//                        (
//                          dt.location_code in
//                            (
//                              select  ucd.code
//                              from    user_comdata    ucd
//                              where   ucd.user_id = :userId and
//                                      ucd.code_type = 'LOC' 
//                            ) or
//                          nvl(dt.chain_code,'nochain') in
//                            ( 
//                              select  ucd.code
//                              from    user_comdata    ucd
//                              where   ucd.user_id = :userId and
//                                      ucd.code_type = 'CHAIN'                      
//                            ) or 
//                          nvl(dt.corp_code,'nocorp') in
//                            (
//                              select  decode(ucd.code,
//                                             '99999',nvl(dt.corp_code,'nocorp'), 
//                                             ucd.code)
//                              from    user_comdata    ucd
//                              where   ucd.user_id = :userId and
//                                      ucd.code_type = 'CORP'                      
//                            )                                  
//                        ) and
//                        dt.data_type = 'ALLT' and
//                        bt.batch_number = dt.batch_number and
//                        pc1.product_code(+) = dt.product_code_1 and
//                        pc2.product_code(+) = dt.product_code_2 and
//                        pc3.product_code(+) = dt.product_code_3 and
//                        mfc.location_code = dt.location_code
//                order by  dt.location_code, mfc.location_name, 
//                          dt.batch_number, dt.batch_tran_id                
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mfc.merchant_number             as merchant_number,\n                      dt.location_code                as location_code,\n                      dt.chain_code                   as chain_code,\n                      mfc.location_name               as location_name,\n                      dt.customer_account_code        as customer_account_code,\n                      dt.card_number                  as card_number,\n                      dt.invoice_number               as invoice_number,\n                      dt.invoice_date                 as invoice_date,\n                      dt.invoice_time                 as invoice_time,\n                      dt.control_number               as control_number,\n                      dt.diesel_1_gallons             as diesel_1_gallons,\n                      dt.diesel_1_cost_per_gallon     as diesel_1_cost_per_gallon,\n                      dt.diesel_1_total_cost          as diesel_1_total_cost,\n                      dt.diesel_2_gallons             as diesel_2_gallons,\n                      dt.diesel_2_cost_per_gallon     as diesel_2_cost_per_gallon,\n                      dt.diesel_2_total_cost          as diesel_2_total_cost,\n                      dt.reefer_gallons               as reefer_gallons,\n                      dt.reefer_cost_per_gallon       as reefer_cost_per_gallon,\n                      dt.reefer_total_cost            as reefer_total_cost,\n                      dt.other_gallons                as other_gallons,\n                      dt.other_cost_per_gallon        as other_cost_per_gallon,\n                      dt.other_total_cost             as other_total_cost,\n                      nvl(dt.fuel_direct_bill,'N')    as fuel_direct_bill,\n                      dt.oil_purchased                as oil_quarts,\n                      dt.oil_cost_per_quart           as oil_cost_per_quart,\n                      dt.oil_total_cost               as oil_total_cost,\n                      nvl(dt.oil_direct_bill,'N')     as oil_direct_bill,\n                      dt.cash_advance_amount          as cash_advance_amount,\n                      nvl(dt.cash_direct_bill,'N')    as cash_direct_bill,\n                      nvl(pc1.product_desc, 'Not Available')\n                                                      as product_code_1,\n                      dt.product_code_1_sign          as product_code_1_sign,\n                      dt.product_code_1_amount        as product_code_1_amount,\n                      nvl(dt.product_code_1_direct_bill,'N')\n                                                      as product_code_1_direct_bill,\n                      nvl(pc2.product_desc, 'Not Available')\n                                                      as product_code_2,\n                      dt.product_code_2_sign          as product_code_2_sign,\n                      dt.product_code_2_amount        as product_code_2_amount,\n                      nvl(dt.product_code_2_direct_bill,'N')\n                                                      as product_code_2_direct_bill,\n                      nvl(pc3.product_desc, 'Not Available')\n                                                      as product_code_3,\n                      dt.product_code_3_sign          as product_code_3_sign,\n                      dt.product_code_3_amount        as product_code_3_amount,\n                      nvl(dt.product_code_3_direct_bill,'N')\n                                                      as product_code_3_direct_bill,\n                      dt.discount_amount              as discount_amount,\n                      dt.discount_type                as discount_type,\n                      dt.fee_type                     as fee_type,\n                      dt.fee_percent                  as fee_percent,\n                      dt.fee_amount                   as fee_amount,\n                      dt.payment_cycle                as payment_cycle,\n                      dt.payment_pending              as payment_pending,\n                      dt.tran_type                    as tran_type,\n                      dt.payment_sign                 as payment_sign,\n                      ( dt.payment_amount *\n                        decode(nvl(dt.payment_sign,'+'),'-',-1,1) )            \n                                                      as payment_amount,\n                      dt.adjustment_sign              as adjustment_sign,\n                      dt.adjustment_amount            as adjustment_amount,\n                      dt.customer_id                  as customer_id,\n                      dt.payment_type                 as payment_type,\n                      dt.credit_flag                  as credit_flag,\n                      dt.balanced_ind                 as balanced_ind,\n                      dt.closed_ind                   as closed_ind,\n                      -- always consider these as unpaid\n                      'N'                             as paid_ind,\n                      bt.loc_payment_type             as payment_method,\n                      dt.batch_number                 as batch_number,\n                      dt.batch_tran_id                as batch_tran_id               \n              from    comdata_detail_file_dt  dt,\n                      comdata_detail_file_bt  bt,\n                      comdata_product_codes   pc1,\n                      comdata_product_codes   pc2,\n                      comdata_product_codes   pc3,\n                      mif_comdata             mfc\n              where   (\n                        (  :1  =  :2  and -- 0 and\n                          dt.location_code =  :3  ) or\n                        (  :4  =  :5  and -- 1 and\n                          dt.chain_code =  :6  )\n                      ) and\n                      dt.batch_date between  :7  and  :8  and\n                      ( \n                        nvl( :9 ,'none') = 'none' or\n                        dt.control_number = (lpad(to_char(dt.invoice_date,'dd'),2,'0') || lpad( :10 ,7,'0'))                   \n                      ) and\n                      (\n                        nvl( :11 ,'none') = 'none' or\n                        dt.invoice_number =  :12 \n                      ) and \n                      (\n                         :13  = 0 or\n                        dt.invoice_total =  :14 \n                      ) and \n                      (\n                         :15  = 0 or  -- all\n                         :16  = 2     -- unpaid only\n                      ) and\n                      (\n                        dt.location_code in\n                          (\n                            select  ucd.code\n                            from    user_comdata    ucd\n                            where   ucd.user_id =  :17  and\n                                    ucd.code_type = 'LOC' \n                          ) or\n                        nvl(dt.chain_code,'nochain') in\n                          ( \n                            select  ucd.code\n                            from    user_comdata    ucd\n                            where   ucd.user_id =  :18  and\n                                    ucd.code_type = 'CHAIN'                      \n                          ) or \n                        nvl(dt.corp_code,'nocorp') in\n                          (\n                            select  decode(ucd.code,\n                                           '99999',nvl(dt.corp_code,'nocorp'), \n                                           ucd.code)\n                            from    user_comdata    ucd\n                            where   ucd.user_id =  :19  and\n                                    ucd.code_type = 'CORP'                      \n                          )                                  \n                      ) and\n                      dt.data_type = 'ALLT' and\n                      bt.batch_number = dt.batch_number and\n                      pc1.product_code(+) = dt.product_code_1 and\n                      pc2.product_code(+) = dt.product_code_2 and\n                      pc3.product_code(+) = dt.product_code_3 and\n                      mfc.location_code = dt.location_code\n              order by  dt.location_code, mfc.location_name, \n                        dt.batch_number, dt.batch_tran_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,locType);
   __sJT_st.setInt(2,LT_SERVICE_CENTER);
   __sJT_st.setString(3,locCode);
   __sJT_st.setInt(4,locType);
   __sJT_st.setInt(5,LT_CHAIN);
   __sJT_st.setString(6,locCode);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   __sJT_st.setString(9,controlNumber);
   __sJT_st.setString(10,controlNumber);
   __sJT_st.setString(11,invoiceNumber);
   __sJT_st.setString(12,invoiceNumber);
   __sJT_st.setString(13,searchAmount);
   __sJT_st.setString(14,searchAmount);
   __sJT_st.setInt(15,paidStatus);
   __sJT_st.setInt(16,paidStatus);
   __sJT_st.setLong(17,userId);
   __sJT_st.setLong(18,userId);
   __sJT_st.setLong(19,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3407^13*/
            resultSet = it.getResultSet();
          
            while( resultSet.next() )
            {
              addSearchItem(resultSet,RT_DETAILS);
            }
            resultSet.close();
            it.close();
            it = null;
          }            
        }   // end if( isSubmitted() )
      }
      else    // RT_SUMMARY
      {
        // convert the boolean to an int for use in the query
        int byBatch = (SummaryByBatch ? 1 : 0);
        
        if ( !isTopLevelUser() || !summaryCode.equals("") )
        {
          /*@lineinfo:generated-code*//*@lineinfo:3427^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(sm.corp_code,'nocorp')            as corp_code,
//                      nvl(sm.chain_code,'nochain')          as chain_code,
//                      sm.location_code                      as location_code,
//                      mfc.location_name                     as location_name,
//                      mfc.merchant_number                   as merchant_number,
//                      sm.batch_date                         as batch_date,
//                      sm.batch_number                       as batch_number,
//                      :byBatch                              as by_batch,
//                      sum(sm.diesel_1_gallons)              as diesel_1_gallons,
//                      sum(sm.diesel_1_total_cost)           as diesel_1_total_cost,
//                      sum(sm.diesel_2_gallons)              as diesel_2_gallons,
//                      sum(sm.diesel_2_total_cost)           as diesel_2_total_cost,
//                      sum(sm.reefer_gallons)                as reefer_gallons,
//                      sum(sm.reefer_total_cost)             as reefer_total_cost,
//                      sum(sm.other_gallons)                 as other_gallons,
//                      sum(sm.other_total_cost)              as other_total_cost,
//                      sum(sm.oil_purchased)                 as oil_purchased,
//                      sum(sm.oil_total_cost)                as oil_total_cost,
//                      sum(sm.cash_advance_amount)           as cash_advance_amount,
//                      sum(sm.product_code_1_amount)         as product_code_1_amount,
//                      sum(sm.product_code_2_amount)         as product_code_2_amount,
//                      sum(sm.product_code_3_amount)         as product_code_3_amount,
//                      sum(sm.discount_amount)               as discount_amount,
//                      sum(sm.fee_amount)                    as fee_amount,
//                      sum(sm.payment_amount)                as payment_amount,
//                      sum(sm.adjustment_amount)             as adjustment_amount
//              from    comdata_detail_file_summary   sm,
//                      mif_comdata                   mfc
//              where   (
//                        sm.location_code in
//                          (
//                            select  ucd.code
//                            from    user_comdata    ucd
//                            where   ucd.user_id = :userId and
//                                    ucd.code_type = 'LOC' 
//                          ) or
//                        nvl(sm.chain_code,'nochain') in
//                          ( 
//                            select  ucd.code
//                            from    user_comdata    ucd
//                            where   ucd.user_id = :userId and
//                                    ucd.code_type = 'CHAIN'                      
//                          ) or 
//                        nvl(sm.corp_code,'nocorp') in
//                          (
//                            select  decode(ucd.code,'99999',sm.corp_code, ucd.code)
//                            from    user_comdata    ucd
//                            where   ucd.user_id = :userId and
//                                    ucd.code_type = 'CORP'                      
//                          )                                  
//                      ) and
//                      nvl(:summaryCode,'none') = decode( :summaryCodeType,
//                                                         :CT_LOC,sm.location_code,
//                                                         :CT_CHAIN,sm.chain_code,
//                                                         'none') and
//                      sm.batch_date between :beginDate and :endDate and
//                      sm.data_type = 'PAID' and
//                      mfc.location_code = sm.location_code and
//                      ( :merchantId = 0 or mfc.merchant_number = :merchantId )
//              group by  nvl(sm.corp_code,'nocorp'),
//                        nvl(sm.chain_code,'nochain'),
//                        sm.location_code, mfc.location_name , mfc.merchant_number,
//                        sm.batch_date, sm.batch_number
//              order by decode(chain_code,'nochain','ZZZ',chain_code), 
//                       decode(corp_code,'nocorp','ZZZ',corp_code),
//                       location_code,
//                       merchant_number,
//                       sm.batch_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(sm.corp_code,'nocorp')            as corp_code,\n                    nvl(sm.chain_code,'nochain')          as chain_code,\n                    sm.location_code                      as location_code,\n                    mfc.location_name                     as location_name,\n                    mfc.merchant_number                   as merchant_number,\n                    sm.batch_date                         as batch_date,\n                    sm.batch_number                       as batch_number,\n                     :1                               as by_batch,\n                    sum(sm.diesel_1_gallons)              as diesel_1_gallons,\n                    sum(sm.diesel_1_total_cost)           as diesel_1_total_cost,\n                    sum(sm.diesel_2_gallons)              as diesel_2_gallons,\n                    sum(sm.diesel_2_total_cost)           as diesel_2_total_cost,\n                    sum(sm.reefer_gallons)                as reefer_gallons,\n                    sum(sm.reefer_total_cost)             as reefer_total_cost,\n                    sum(sm.other_gallons)                 as other_gallons,\n                    sum(sm.other_total_cost)              as other_total_cost,\n                    sum(sm.oil_purchased)                 as oil_purchased,\n                    sum(sm.oil_total_cost)                as oil_total_cost,\n                    sum(sm.cash_advance_amount)           as cash_advance_amount,\n                    sum(sm.product_code_1_amount)         as product_code_1_amount,\n                    sum(sm.product_code_2_amount)         as product_code_2_amount,\n                    sum(sm.product_code_3_amount)         as product_code_3_amount,\n                    sum(sm.discount_amount)               as discount_amount,\n                    sum(sm.fee_amount)                    as fee_amount,\n                    sum(sm.payment_amount)                as payment_amount,\n                    sum(sm.adjustment_amount)             as adjustment_amount\n            from    comdata_detail_file_summary   sm,\n                    mif_comdata                   mfc\n            where   (\n                      sm.location_code in\n                        (\n                          select  ucd.code\n                          from    user_comdata    ucd\n                          where   ucd.user_id =  :2  and\n                                  ucd.code_type = 'LOC' \n                        ) or\n                      nvl(sm.chain_code,'nochain') in\n                        ( \n                          select  ucd.code\n                          from    user_comdata    ucd\n                          where   ucd.user_id =  :3  and\n                                  ucd.code_type = 'CHAIN'                      \n                        ) or \n                      nvl(sm.corp_code,'nocorp') in\n                        (\n                          select  decode(ucd.code,'99999',sm.corp_code, ucd.code)\n                          from    user_comdata    ucd\n                          where   ucd.user_id =  :4  and\n                                  ucd.code_type = 'CORP'                      \n                        )                                  \n                    ) and\n                    nvl( :5 ,'none') = decode(  :6 ,\n                                                        :7 ,sm.location_code,\n                                                        :8 ,sm.chain_code,\n                                                       'none') and\n                    sm.batch_date between  :9  and  :10  and\n                    sm.data_type = 'PAID' and\n                    mfc.location_code = sm.location_code and\n                    (  :11  = 0 or mfc.merchant_number =  :12  )\n            group by  nvl(sm.corp_code,'nocorp'),\n                      nvl(sm.chain_code,'nochain'),\n                      sm.location_code, mfc.location_name , mfc.merchant_number,\n                      sm.batch_date, sm.batch_number\n            order by decode(chain_code,'nochain','ZZZ',chain_code), \n                     decode(corp_code,'nocorp','ZZZ',corp_code),\n                     location_code,\n                     merchant_number,\n                     sm.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,byBatch);
   __sJT_st.setLong(2,userId);
   __sJT_st.setLong(3,userId);
   __sJT_st.setLong(4,userId);
   __sJT_st.setString(5,summaryCode);
   __sJT_st.setInt(6,summaryCodeType);
   __sJT_st.setInt(7,CT_LOC);
   __sJT_st.setInt(8,CT_CHAIN);
   __sJT_st.setDate(9,beginDate);
   __sJT_st.setDate(10,endDate);
   __sJT_st.setLong(11,merchantId);
   __sJT_st.setLong(12,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3497^11*/
        }          
      }
      if ( it != null )
      {
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          switch( getReportType() )
          {
            case RT_PAID_DETAILS:
            case RT_DETAILS:
              dataObj = new RowData( resultSet, getReportType() );
              break;
            
            case RT_VOL_BY_LOC_DETAILS:
              dataObj = new VolumeByLocation( resultSet );
              break;
              
            case RT_VOL_BY_ACCT_SUMMARY:
              dataObj = new VolumeByLocationSummary( resultSet );
              break;
            
            case RT_VOL_BY_LOC_SUMMARY:
              if ( getData("summaryType").equals("1") )
              {
                addFleetData( resultSet );
              }
              else    // summary by location
              {
                addLocationData( resultSet );
              }
              break;
              
            case RT_PAID_SUMMARY:
              switch( locType )
              {
                case LT_SERVICE_CENTER:   // service center
                case LT_RECEIVER:   // receiver
                  dataObj = new SettlementSummaryData( resultSet );
                  break;
                
                case LT_CHAIN:   // chain
                  dataObj = new SettlementChainSummaryData( resultSet );
                  break;
              }
              break;
            
            default: // RT_SUMMARY or RT_VOL_BY_LOC_SUMMARY
              if ( !SummaryByBatch )
              {
                merchantId = resultSet.getLong("merchant_number");
              
                if ( merchantId != lastMerchantId )
                {
                  dataObj = new SummaryData( resultSet );
                  lastMerchantId = merchantId;
                }
                else  // add another batch and continue
                {
                  ((SummaryData)dataObj).addBatch(resultSet);
                  continue;   // do not need to add the object to vector
                }
              }
              else  // by batch, add one object for each result set row
              {
                dataObj = new SummaryData( resultSet );
              }
              break;
          }
        
          // only add the data object if it was created
          if ( dataObj != null )
          {
            ReportRows.addElement( dataObj );
          }          
        }
        resultSet.close();
        it.close();
      }        
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public GroupTotalsRecord loadSettlementGroupTotals( )
  {
    return( loadSettlementGroupTotals( getLong("settleGroup") ) );
  }
  
  public GroupTotalsRecord loadSettlementGroupTotals( long groupId )
  {
    return( loadSettlementGroupTotals(groupId, getReportDateBegin(), getReportDateEnd()) );
  }
  
  public GroupTotalsRecord loadSettlementGroupTotals( long groupId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    GroupTotalsRecord   totals      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:3607^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sgt.settlement_group_number         as settlement_group,
//                  sgt.settlement_paid_date            as paid_date,
//                  sgt.location_code                   as location_code,
//                  sgt.receiver_code                   as receiver_code,
//                  sgt.corp_chain_code                 as corp_chain_code,
//                  sgt.settlement_close_date           as close_date,
//                  sgt.settlement_close_time           as close_time,
//                  sgt.settlement_control_number       as control_number,
//                  sgt.settlement_check_number         as check_number,
//                  -- direct bill
//                  sgt.direct_bill_gallon_total        as db_gallons,
//                  sgt.direct_bill_gross_amount_total  as db_amount,
//                  sgt.direct_bill_invoice_total       as db_invoice,
//                  sgt.direct_bill_discount_total      as db_discount,
//                  sgt.direct_bill_fee_total           as db_fees,
//                  sgt.direct_bill_payment_total       as db_payment,
//                  sgt.direct_bill_product_total       as db_product_total,
//                  -- funded
//                  sgt.funded_gallon_total             as funded_gallons,
//                  sgt.funded_gross_amount_total       as funded_amount,
//                  sgt.funded_invoice_total            as funded_invoice,
//                  sgt.funded_discount_total           as funded_discount,
//                  sgt.funded_fee_total                as funded_fees,
//                  sgt.funded_payment_total            as funded_payment,
//                  sgt.funded_product_total            as funded_product_total,
//                  -- gross
//                  sgt.gross_fuel_gallons_total        as gross_gallons,
//                  sgt.gross_fuel_amount_total         as gross_amount,
//                  sgt.gross_invoice_total             as gross_invoice,
//                  sgt.gross_discount_total            as gross_discount,
//                  sgt.gross_total_fee_amount          as gross_fees,
//                  sgt.gross_total_payment_amount      as gross_payment,
//                  sgt.gross_product_amount            as gross_product_total,
//                  -- fees
//                  sgt.total_equipment_charge          as equip_charge,
//                  sgt.excess_settle_charge            as settle_charge,
//                  sgt.service_charge                  as service_charge,
//                  sgt.miscellaneous_charge            as misc_charge,
//                  -- payment total
//                  sgt.settlement_payment_amount       as payment_amount,
//                  -- pending 
//                  sgt.pending_payment_total_gallons   as pend_gallons,   
//                  sgt.pending_payment_gross_total     as pend_amount,
//                  sgt.pending_payment_invoice_amount  as pend_invoice,
//                  sgt.pending_payment_discount_total  as pend_discount,
//                  sgt.pending_payment_fee_amount      as pend_fees,
//                  sgt.pending_payment_payment_amount  as pend_payment,
//                  sgt.pending_payment_product_amount  as pend_product_total,
//                  -- translink
//                  sgt.translink_gallon_total          as translink_gallons,   
//                  sgt.translink_amount_total          as translink_amount,
//                  sgt.translink_invoice_total         as translink_invoice,
//                  sgt.translink_discount_total        as translink_discount,
//                  sgt.translink_fee_total             as translink_fees,
//                  sgt.translink_payment_total         as translink_payment,
//                  sgt.translink_product_total         as translink_product_total,
//                  -- preferred funded
//                  sgt.pref_funded_gallon_total        as pref_fund_gallons,   
//                  sgt.pref_funded_amount_total        as pref_fund_amount,
//                  sgt.pref_funded_invoice_total       as pref_fund_invoice,
//                  sgt.pref_funded_discount_total      as pref_fund_discount,
//                  sgt.pref_funded_fee_total           as pref_fund_fees,
//                  sgt.pref_funded_payment_total       as pref_fund_payment,
//                  sgt.pref_funded_prod_amount_total   as pref_fund_product_total,
//                  -- preferred direct bill
//                  sgt.pref_db_gallon_total            as pref_db_gallons,   
//                  sgt.pref_db_amount_total            as pref_db_amount,
//                  sgt.pref_db_invoice_total           as pref_db_invoice,
//                  sgt.pref_db_discount_total          as pref_db_discount,
//                  sgt.pref_db_fee_total               as pref_db_fees,
//                  sgt.pref_db_payment_total           as pref_db_payment,
//                  sgt.pref_db_product_total           as pref_db_product_total,
//                  -- credit
//                  sgt.credit_gallon_total             as credit_gallons,   
//                  sgt.credit_amount_total             as credit_amount,
//                  sgt.credit_invoice_total            as credit_invoice,
//                  sgt.credit_discount_total           as credit_discount,
//                  sgt.credit_fee_total                as credit_fees,
//                  sgt.credit_payment_amount           as credit_payment,
//                  sgt.credit_product_amount           as credit_product_total,
//                  -- sel focus cost-plus
//                  sgt.select_total                    as select_total,
//                  sgt.focus_total                     as focus_total,
//                  sgt.cost_plus_total                 as cost_plus_total,
//                  -- misc
//                  sgt.adjustment_total                as adj_total
//          from    comdata_detail_file_paid_sgt    sgt
//          where   sgt.settlement_group_number = :groupId and
//                  sgt.settlement_paid_date between :beginDate and :endDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sgt.settlement_group_number         as settlement_group,\n                sgt.settlement_paid_date            as paid_date,\n                sgt.location_code                   as location_code,\n                sgt.receiver_code                   as receiver_code,\n                sgt.corp_chain_code                 as corp_chain_code,\n                sgt.settlement_close_date           as close_date,\n                sgt.settlement_close_time           as close_time,\n                sgt.settlement_control_number       as control_number,\n                sgt.settlement_check_number         as check_number,\n                -- direct bill\n                sgt.direct_bill_gallon_total        as db_gallons,\n                sgt.direct_bill_gross_amount_total  as db_amount,\n                sgt.direct_bill_invoice_total       as db_invoice,\n                sgt.direct_bill_discount_total      as db_discount,\n                sgt.direct_bill_fee_total           as db_fees,\n                sgt.direct_bill_payment_total       as db_payment,\n                sgt.direct_bill_product_total       as db_product_total,\n                -- funded\n                sgt.funded_gallon_total             as funded_gallons,\n                sgt.funded_gross_amount_total       as funded_amount,\n                sgt.funded_invoice_total            as funded_invoice,\n                sgt.funded_discount_total           as funded_discount,\n                sgt.funded_fee_total                as funded_fees,\n                sgt.funded_payment_total            as funded_payment,\n                sgt.funded_product_total            as funded_product_total,\n                -- gross\n                sgt.gross_fuel_gallons_total        as gross_gallons,\n                sgt.gross_fuel_amount_total         as gross_amount,\n                sgt.gross_invoice_total             as gross_invoice,\n                sgt.gross_discount_total            as gross_discount,\n                sgt.gross_total_fee_amount          as gross_fees,\n                sgt.gross_total_payment_amount      as gross_payment,\n                sgt.gross_product_amount            as gross_product_total,\n                -- fees\n                sgt.total_equipment_charge          as equip_charge,\n                sgt.excess_settle_charge            as settle_charge,\n                sgt.service_charge                  as service_charge,\n                sgt.miscellaneous_charge            as misc_charge,\n                -- payment total\n                sgt.settlement_payment_amount       as payment_amount,\n                -- pending \n                sgt.pending_payment_total_gallons   as pend_gallons,   \n                sgt.pending_payment_gross_total     as pend_amount,\n                sgt.pending_payment_invoice_amount  as pend_invoice,\n                sgt.pending_payment_discount_total  as pend_discount,\n                sgt.pending_payment_fee_amount      as pend_fees,\n                sgt.pending_payment_payment_amount  as pend_payment,\n                sgt.pending_payment_product_amount  as pend_product_total,\n                -- translink\n                sgt.translink_gallon_total          as translink_gallons,   \n                sgt.translink_amount_total          as translink_amount,\n                sgt.translink_invoice_total         as translink_invoice,\n                sgt.translink_discount_total        as translink_discount,\n                sgt.translink_fee_total             as translink_fees,\n                sgt.translink_payment_total         as translink_payment,\n                sgt.translink_product_total         as translink_product_total,\n                -- preferred funded\n                sgt.pref_funded_gallon_total        as pref_fund_gallons,   \n                sgt.pref_funded_amount_total        as pref_fund_amount,\n                sgt.pref_funded_invoice_total       as pref_fund_invoice,\n                sgt.pref_funded_discount_total      as pref_fund_discount,\n                sgt.pref_funded_fee_total           as pref_fund_fees,\n                sgt.pref_funded_payment_total       as pref_fund_payment,\n                sgt.pref_funded_prod_amount_total   as pref_fund_product_total,\n                -- preferred direct bill\n                sgt.pref_db_gallon_total            as pref_db_gallons,   \n                sgt.pref_db_amount_total            as pref_db_amount,\n                sgt.pref_db_invoice_total           as pref_db_invoice,\n                sgt.pref_db_discount_total          as pref_db_discount,\n                sgt.pref_db_fee_total               as pref_db_fees,\n                sgt.pref_db_payment_total           as pref_db_payment,\n                sgt.pref_db_product_total           as pref_db_product_total,\n                -- credit\n                sgt.credit_gallon_total             as credit_gallons,   \n                sgt.credit_amount_total             as credit_amount,\n                sgt.credit_invoice_total            as credit_invoice,\n                sgt.credit_discount_total           as credit_discount,\n                sgt.credit_fee_total                as credit_fees,\n                sgt.credit_payment_amount           as credit_payment,\n                sgt.credit_product_amount           as credit_product_total,\n                -- sel focus cost-plus\n                sgt.select_total                    as select_total,\n                sgt.focus_total                     as focus_total,\n                sgt.cost_plus_total                 as cost_plus_total,\n                -- misc\n                sgt.adjustment_total                as adj_total\n        from    comdata_detail_file_paid_sgt    sgt\n        where   sgt.settlement_group_number =  :1  and\n                sgt.settlement_paid_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.ComdataTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,groupId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.reports.ComdataTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3698^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        totals = new GroupTotalsRecord(resultSet);
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadSettlementGroupTotals(" + groupId + ")", e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
    return( totals );
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    String      value   = null;
    
    super.postHandleRequest( request );
    
    switch( getReportType() )
    {
      case RT_VOL_BY_LOC_SUMMARY:
      case RT_VOL_BY_LOC_DETAILS:
      case RT_VOL_BY_ACCT_SUMMARY:
        setData("endDate",getData("beginDate"));
        break;
        
      default:
        break;
    }
    setData("summaryCode", getData("summaryCode").toUpperCase() );
    setData("locationCode", getData("locationCode").toUpperCase() );

    // legacy JSP support    
    if ( HttpHelper.getString(request,"byFleet",null) != null )
    {
      if ( HttpHelper.getBoolean(request,"byFleet",false) == true )
      {
        value = "1";
      }
      else
      {
        value = "0";
      }
      setData("summaryType", value );
    }
    
    if ( (value = HttpHelper.getString(request,"fleetChainCode",null)) != null )
    {
      setData("chain",value);
    }
    // end legacy JSP support
  }
  
  public void setProperties(HttpServletRequest request)
  {
    autoSetFields(request);   // stubbed so download servlet works
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/