/*@lineinfo:filename=IntlChargebackRetrievalDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/IntlChargebackRetrievalDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000-2007,2008 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class IntlChargebackRetrievalDataBean extends ReportSQLJBean
{
  public static final int     DT_CHARGEBACKS              = 0;
  public static final int     DT_RETRIEVALS               = 1;
  public static final int     DT_CHARGEBACK_ADJUSTMENTS   = 2;
  
  public static class DetailRow
  {
    public    double    AdjustmentAmount      = 0.0;
    public    Date      AdjustmentDate        = null;
    public    String    CardNumber            = null;
    private   long      CbId                  = 0L;
    public    String    ControlNumber         = null;
    public    String    CurrencyCode          = null;
    public    String    CurrencyCodeAlpha     = null;
    public    String    CurrencyName          = null;
    public    String    DbaName               = null;
    public    double    FxAmount              = 0.0;
    public    Date      IncomingDate          = null;
    public    String    ItemType              = null;
    public    long      MerchantId            = 0L;
    public    String    ReasonCode            = null;
    public    String    ReasonDesc            = null;
    public    String    ReferenceNumber       = null;
    public    double    TranAmount            = 0.0;
    public    Date      TranDate              = null;
    public    String    TridentTranId         = null;
  
    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CbId                  = resultSet.getLong("cb_id");
      ControlNumber         = resultSet.getString("control_number");
      MerchantId            = resultSet.getLong("merchant_number");
      DbaName               = resultSet.getString("dba_name");
      IncomingDate          = resultSet.getDate("incoming_date");
      ReasonCode            = resultSet.getString("reason_code");
      ReasonDesc            = processString( resultSet.getString("reason_desc") );
      CurrencyCode          = resultSet.getString("currency_code");
      CurrencyCodeAlpha     = resultSet.getString("currency_code_alpha");
      CurrencyName          = resultSet.getString("currency_name");
      TranAmount            = resultSet.getDouble("tran_amount");
      FxAmount              = resultSet.getDouble("fx_amount");
      CardNumber            = resultSet.getString("card_number");
      TranDate              = resultSet.getDate("tran_date");
      ReferenceNumber       = resultSet.getString("ref_num");
      ItemType              = resultSet.getString("item_type");
      TridentTranId         = resultSet.getString("trident_tran_id");
      
      try
      {
        AdjustmentAmount    = resultSet.getDouble("adjustment_amount");
        AdjustmentDate      = resultSet.getDate("adjustment_date");
      }
      catch( Exception e )
      {
        // does not contain adjustment data, ignore
      }
    }
    
    public long getCbId()
    {
      return( CbId );
    }
  }
  
  public static class SummaryRow
  {
    public    String      CurrencyCode      = null;
    public    String      CurrencyCodeAlpha = null;
    public    String      CurrencyName      = null;
    public    double      FxAmount          = 0.0;
    public    long        HierarchyNode     = 0L;
    public    double      ItemAmount        = 0.0;
    public    int         ItemCount         = 0;
    public    String      OrgName           = null;
    
    public SummaryRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      OrgName           = resultSet.getString("org_name");
      CurrencyCode      = resultSet.getString("currency_code");
      CurrencyCodeAlpha = resultSet.getString("currency_code_alpha");
      CurrencyName      = resultSet.getString("currency_name");
      ItemCount         = resultSet.getInt("item_count");
      ItemAmount        = resultSet.getDouble("item_amount");
      FxAmount          = resultSet.getDouble("fx_amount");
    }
  }
  
  public IntlChargebackRetrievalDataBean( )
  {
    super(true);    // use field bean
  }
  
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup      fgroup    = null;
    
    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");
    fgroup.add( new HiddenField("reportDataType") );
    setData("reportDataType",String.valueOf(DT_CHARGEBACKS));
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl( buffer, nodeId, beginDate, endDate );
    buffer.append("&reportDataType=");
    buffer.append(getData("reportDataType"));
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( getReportType() == RT_SUMMARY )
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"Currency Code\",");
      line.append("\"Item Count\",");
      line.append("\"Item Amount\"");
      
      if ( hasFxData() )
      {
        line.append(",\"USD Amount\"");
      }        
    }
    else  // RT_DETAILS
    {
      line.append("\"Merchant ID\",");
      line.append("\"Dba Name\",");
      line.append("\"Case #\",");
      line.append("\"Incoming Date\",");
      line.append("\"Card Number\",");
      line.append("\"Reference Number\",");
      line.append("\"Trident Tran ID\",");
      line.append("\"Tran Date\",");
      line.append("\"Currency Code\",");
      line.append("\"Tran Amount\",");
      if ( hasFxData() )
      {
        line.append("\"USD Amount\",");
      }
      line.append("\"Reason Code\",");
      line.append("\"Reason\",");
      line.append("\"Type\",");
      line.append("\"Adjustment Date\"");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    if ( getReportType() == RT_SUMMARY )
    {
      SummaryRow  row = (SummaryRow) obj;
      
      line.append( encodeHierarchyNode(row.HierarchyNode) );
      line.append( ",\"" );
      line.append( row.OrgName );
      line.append( "\",\"" );
      line.append( row.CurrencyCodeAlpha );
      line.append( "\"," );
      line.append( row.ItemCount );
      line.append( "," );
      line.append( row.ItemAmount );
      if ( hasFxData() )
      {
        line.append(",");
        line.append( row.FxAmount );
      }        
    }
    else    // RT_DETAILS
    {
      DetailRow       row = (DetailRow) obj;
      
      line.append( encodeHierarchyNode(row.MerchantId) );
      line.append( ",\"" );
      line.append( row.DbaName );
      line.append( "\",\"" );
      line.append( row.ControlNumber );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate(row.IncomingDate,"MM/dd/yyyy") );
      line.append( ",\"" );
      line.append( row.CardNumber );
      line.append( "\",\"" );
      line.append( row.ReferenceNumber );
      line.append( "\",\"" );
      line.append( row.TridentTranId );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate(row.TranDate,"MM/dd/yyyy") );
      line.append( ",\"" );
      line.append( row.CurrencyCodeAlpha );
      line.append( "\"," );
      line.append( row.TranAmount );
      if ( hasFxData() )
      {
        line.append( "," );
        line.append( row.FxAmount );
      }
      line.append(",\"");
      line.append( row.ReasonCode );
      line.append("\",\"");
      line.append( row.ReasonDesc );
      line.append("\",\"");
      line.append( row.ItemType );
      line.append("\",");
      line.append( DateTimeFormatter.getFormattedDate(row.AdjustmentDate,"MM/dd/yyyy") );
    }
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    
    if( fileFormat == FF_CSV )
    {
      retVal = true;
    }
    return(retVal);
  }
  
  public String getDownloadFilenameBase()
  {
    Date                    beginDate = getReportDateBegin();
    Calendar                cal       = Calendar.getInstance();  
    Date                    endDate   = getReportDateEnd();
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append( "_intl_");
    
    switch( getInt("reportDataType") )
    {
      case DT_RETRIEVALS:
        filename.append("retrieval");
        break;
        
      case DT_CHARGEBACK_ADJUSTMENTS:
        filename.append("cb_adj");
        break;
        
      default:
        filename.append("chargeback");
        break;
    }
    
    if ( getReportType() == RT_SUMMARY )
    {
      filename.append("_summary_");
    }
    else  // RT_DETAILS
    {
      filename.append("_details_");
    }      
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( beginDate,"MMddyy" ) );
    if ( beginDate.equals(endDate) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( endDate,"MMddyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public String getReportTitle()
  {
    String        retVal    = "No Title";
    
    switch( getInt("reportDataType") )
    {
      case DT_RETRIEVALS:
        retVal = "International Retrieval Request Details";
        break;
        
      case DT_CHARGEBACK_ADJUSTMENTS:
        retVal = "International Chargeback Adjustments";
        break;
        
      case DT_CHARGEBACKS:
        retVal = "International Chargeback Details";
        break;
    }
    return( retVal );
  }
  
  public void loadData( )
  {
    loadData( getInt("reportDataType"), getReportType(), getReportHierarchyNode(), getReportDateBegin(), getReportDateEnd() );
  }

  public void loadData( int reportDataType, int reportType )
  {
    loadData( reportDataType, reportType, getReportHierarchyNode(), getReportDateBegin(), getReportDateEnd() );
  }

  public void loadData( int reportDataType, int reportType, long nodeId, Date beginDate, Date endDate )
  {
    String                dateColumnName    = null;
    double                fxAmount          = 0.0;
    ResultSetIterator     it                = null;
    String                itemType          = null;
    long                  merchantId        = 0L;
    ResultSet             resultSet         = null;
  
    try
    {
      switch( reportDataType )
      {
        case DT_RETRIEVALS:
          dateColumnName = "cb.incoming_date";
          itemType = "R";
          break;
          
        case DT_CHARGEBACK_ADJUSTMENTS:
          dateColumnName = "cb.adjustment_date";
          itemType = "C";
          break;
          
        default:
          itemType = "C";
          dateColumnName = "cb.incoming_date";
          break;
      }
      itemType = ((reportDataType == DT_RETRIEVALS) ? "R" : "C");
    
      // empty the current contents
      ReportRows.clear();
    
      if ( reportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:388^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_id                          as cb_id,
//                    cb.case_id                        as control_number,
//                    cb.merchant_number                as merchant_number,
//                    mf.dba_name                       as dba_name,
//                    cb.incoming_date                  as incoming_date,
//                    cb.reason_code                    as reason_code,
//                    rd.reason_desc                    as reason_desc,
//                    cb.currency_code                  as currency_code,
//                    cc.currency_code_alpha            as currency_code_alpha,
//                    cc.currency_name                  as currency_name,
//                    cb.amount                         as tran_amount,
//                    cb.fx_amount_base                 as fx_amount,
//                    cb.card_number                    as card_number,
//                    trunc(cb.tran_date)               as tran_date,
//                    cb.reference_number               as ref_num,
//                    nvl(cbt.type_desc,cb.cb_type_id)  as item_type,
//                    cb.trident_tran_id                as trident_tran_id,
//                    cb.adjustment_date                as adjustment_date,
//                    cb.adjustment_amount              as adjustment_amount
//            from    organization                 o,
//                    group_merchant               gm,
//                    payvision_api_cb_records     cb,
//                    payvision_api_cb_types       cbt,
//                    chargeback_reason_desc       rd,
//                    currency_codes               cc,
//                    mif                          mf
//            where   o.org_group = :nodeId and
//                    gm.org_num = o.org_num and
//                    cb.merchant_number = gm.merchant_number and
//                    :dateColumnName between :beginDate and :endDate and
//                    cbt.type_id = cb.cb_type_id and
//                    cbt.item_type = :itemType and
//                    rd.reason_code(+) = cb.reason_code and
//                    rd.card_type(+) = cb.card_type and
//                    rd.item_type(+) = :itemType and
//                    cc.currency_code(+) = cb.currency_code and
//                    mf.merchant_number(+) = cb.merchant_number
//            order by cb.merchant_number, cb.currency_code, cb.incoming_date desc        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  cb.cb_id                          as cb_id,\n                  cb.case_id                        as control_number,\n                  cb.merchant_number                as merchant_number,\n                  mf.dba_name                       as dba_name,\n                  cb.incoming_date                  as incoming_date,\n                  cb.reason_code                    as reason_code,\n                  rd.reason_desc                    as reason_desc,\n                  cb.currency_code                  as currency_code,\n                  cc.currency_code_alpha            as currency_code_alpha,\n                  cc.currency_name                  as currency_name,\n                  cb.amount                         as tran_amount,\n                  cb.fx_amount_base                 as fx_amount,\n                  cb.card_number                    as card_number,\n                  trunc(cb.tran_date)               as tran_date,\n                  cb.reference_number               as ref_num,\n                  nvl(cbt.type_desc,cb.cb_type_id)  as item_type,\n                  cb.trident_tran_id                as trident_tran_id,\n                  cb.adjustment_date                as adjustment_date,\n                  cb.adjustment_amount              as adjustment_amount\n          from    organization                 o,\n                  group_merchant               gm,\n                  payvision_api_cb_records     cb,\n                  payvision_api_cb_types       cbt,\n                  chargeback_reason_desc       rd,\n                  currency_codes               cc,\n                  mif                          mf\n          where   o.org_group =  ?  and\n                  gm.org_num = o.org_num and\n                  cb.merchant_number = gm.merchant_number and\n                   ");
   __sjT_sb.append(dateColumnName);
   __sjT_sb.append("  between  ?  and  ?  and\n                  cbt.type_id = cb.cb_type_id and\n                  cbt.item_type =  ?  and\n                  rd.reason_code(+) = cb.reason_code and\n                  rd.card_type(+) = cb.card_type and\n                  rd.item_type(+) =  ?  and\n                  cc.currency_code(+) = cb.currency_code and\n                  mf.merchant_number(+) = cb.merchant_number\n          order by cb.merchant_number, cb.currency_code, cb.incoming_date desc");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "0com.mes.reports.IntlChargebackRetrievalDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setString(4,itemType);
   __sJT_st.setString(5,itemType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:428^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          ReportRows.add( new DetailRow( resultSet ) );
          fxAmount += resultSet.getDouble("fx_amount");
        }
        resultSet.close();
        it.close();
      }
      else    // RT_SUMMARY
      {
        /*@lineinfo:generated-code*//*@lineinfo:441^9*/

//  ************************************************************
//  #sql [Ctx] it = { select o.org_group                as hierarchy_node,
//                   o.org_name                 as org_name,
//                   cb.currency_code           as currency_code,
//                   cc.currency_code_alpha     as currency_code_alpha,
//                   cc.currency_name           as currency_name,
//                   count( cb.amount )         as item_count,
//                   sum( cb.amount )           as item_amount,
//                   sum( cb.fx_amount_base )   as fx_amount
//            from   organization             op,
//                   parent_org               po,
//                   group_merchant           gm,
//                   payvision_api_cb_records cb,
//                   payvision_api_cb_types   cbt,
//                   currency_codes           cc,
//                   organization             o
//            where   op.org_group = :nodeId and
//                    po.parent_org_num = op.org_num and
//                    gm.org_num = po.org_num and
//                    cb.merchant_number = gm.merchant_number and
//                    cb.incoming_date between :beginDate and :endDate and
//                    cbt.type_id = cb.cb_type_id and
//                    cbt.item_type = :itemType and
//                    cc.currency_code(+) = cb.currency_code and
//                    o.org_num = po.org_num
//            group by o.org_group, o.org_name, cb.currency_code,
//                      cc.currency_code_alpha, cc.currency_name
//            order by o.org_name, cb.currency_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select o.org_group                as hierarchy_node,\n                 o.org_name                 as org_name,\n                 cb.currency_code           as currency_code,\n                 cc.currency_code_alpha     as currency_code_alpha,\n                 cc.currency_name           as currency_name,\n                 count( cb.amount )         as item_count,\n                 sum( cb.amount )           as item_amount,\n                 sum( cb.fx_amount_base )   as fx_amount\n          from   organization             op,\n                 parent_org               po,\n                 group_merchant           gm,\n                 payvision_api_cb_records cb,\n                 payvision_api_cb_types   cbt,\n                 currency_codes           cc,\n                 organization             o\n          where   op.org_group =  :1  and\n                  po.parent_org_num = op.org_num and\n                  gm.org_num = po.org_num and\n                  cb.merchant_number = gm.merchant_number and\n                  cb.incoming_date between  :2  and  :3  and\n                  cbt.type_id = cb.cb_type_id and\n                  cbt.item_type =  :4  and\n                  cc.currency_code(+) = cb.currency_code and\n                  o.org_num = po.org_num\n          group by o.org_group, o.org_name, cb.currency_code,\n                    cc.currency_code_alpha, cc.currency_name\n          order by o.org_name, cb.currency_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.IntlChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setString(4,itemType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.IntlChargebackRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:470^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          ReportRows.add( new SummaryRow( resultSet ) );
          fxAmount += resultSet.getDouble("fx_amount");
        }
        resultSet.close();
        it.close();
      }
      setFxDataIncluded( (fxAmount == 0.0) ? false : true );
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
}/*@lineinfo:generated-code*/