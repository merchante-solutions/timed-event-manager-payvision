/*@lineinfo:filename=RiskMerchAuthDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskMerchAuthDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 2/03/04 4:37p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class RiskMerchAuthDataBean extends RiskMerchHistoryBase
{
  public class RowData
  {
    public Date           ActiveDate            = null;
    public Date           MonthBeginDate        = null;
    public Date           MonthEndDate          = null;
    
    public double         ApprovedAmount        = 0.0;
    public int            ApprovedCount         = 0;   
    public double         AVSAmount             = 0.0;
    public int            AVSCount              = 0;        
    public double         AVSRatio              = 0.0;
    public double         CaptureRatio          = 0.0;
    public double         DeclineAmount         = 0.0;
    public int            DeclineCount          = 0;    
    public double         DeclineCaptureRatio   = 0.0;
    public double         DeclineRatio          = 0.0;
    public double         SalesAmount           = 0.0;
    public int            SalesCount            = 0;      
    public double         TotalAmount           = 0.0;
    public int            TotalCount            = 0;      
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      ActiveDate        = resultSet.getDate("active_date");
      MonthBeginDate    = resultSet.getDate("month_begin_date");
      MonthEndDate      = resultSet.getDate("month_end_date");
      
      ApprovedCount     = resultSet.getInt("approved_count");
      ApprovedAmount    = resultSet.getDouble("approved_amount");
      AVSCount          = resultSet.getInt("avs_count");
      AVSAmount         = resultSet.getDouble("avs_amount");
      DeclineCount      = resultSet.getInt("decline_count");
      DeclineAmount     = resultSet.getDouble("decline_amount");
      TotalCount        = resultSet.getInt("total_count");
      TotalAmount       = resultSet.getDouble("total_amount");
      SalesCount        = resultSet.getInt("sales_count");
      SalesAmount       = resultSet.getDouble("sales_amount");
      
      // calculate the decline/auth ratio      
      if ( TotalCount > 0 )
      {
        DeclineRatio = (double)((double)DeclineCount/(double)TotalCount);
      }
      else if ( DeclineCount != 0 )     // should never happen
      {
        DeclineRatio = 1;
      }
      else
      {
        DeclineRatio = 0;
      }
      
      // calculate the capture/auth ratio      
      if ( TotalCount > 0 )
      {
        CaptureRatio = (double)((double)SalesCount/(double)TotalCount);
      }
      else if ( SalesCount != 0 ) 
      {
        CaptureRatio = 1;
      }
      else
      {
        CaptureRatio = 0;
      }
      
      // calculate the decline/capture ratio      
      if ( SalesCount > 0 )
      {
        DeclineCaptureRatio = (double)((double)DeclineCount/(double)SalesCount);
      }
      else
      {
        DeclineCaptureRatio = 0;
      }
      
      // calculate the avs/auth ratio      
      if ( TotalCount > 0 )
      {
        AVSRatio = (double)((double)AVSCount/(double)TotalCount);
      }
      else if ( AVSCount != 0 )     // should never happen
      {
        AVSRatio = 1;
      }
      else
      {
        AVSRatio = 0;
      }
    }
  }
  
  public RiskMerchAuthDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    super.encodeHeaderCSV(line);
    
    line.append("\"Active Date\",");
    line.append("\"Total Auth Count\",");
    line.append("\"Declined Count\",");
    line.append("\"Decl/Auth Ratio\",");
    line.append("\"Decl/Capt Ratio\",");
    line.append("\"Capt/Auth Ratio\",");
    line.append("\"AVS Count\",");
    line.append("\"AVS Ratio\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( DateTimeFormatter.getFormattedDate(record.ActiveDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( record.TotalCount );
    line.append( "," );
    line.append( record.DeclineCount );
    line.append( "," );
    line.append( record.DeclineRatio );
    line.append( "," );
    line.append( record.DeclineCaptureRatio );
    line.append( "," );
    line.append( record.CaptureRatio );
    line.append( "," );
    line.append( record.AVSCount );
    line.append( "," );
    line.append( record.AVSRatio );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_auth_summary_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    loadData( getReportMerchantId(), ReportDateBegin, ReportDateEnd );
  }
  
  public void loadData( long merchantId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      MerchData = null;
      
      /*@lineinfo:generated-code*//*@lineinfo:230^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  auth.merchant_number                        as merchant_number,
//                  mf.dba_name                                 as dba_name,
//                  mr.merch_legal_name                         as legal_name,
//                  to_date(mf.date_opened,'mmddrr')            as date_opened,
//                  mf.sic_code                                 as sic_code,
//                  trunc(auth.transaction_date,'month')        as active_date,
//                  trunc(auth.transaction_date,'month')        as month_begin_date,
//                  last_day(auth.transaction_date)             as month_end_date,
//                  sum(nvl(auth.vmc_auth_approved_count,0))    as approved_count,                
//                  sum(nvl(auth.vmc_auth_approved_amount,0))   as approved_amount, 
//                  sum(nvl(auth.vmc_auth_avs_count,0))         as avs_count,                 
//                  sum(nvl(auth.vmc_auth_avs_amount,0))        as avs_amount, 
//                  sum(nvl(auth.vmc_auth_decline_count,0))     as decline_count,                 
//                  sum(nvl(auth.vmc_auth_decline_amount,0))    as decline_amount, 
//                  sum(nvl(auth.vmc_auth_total_count,0))       as total_count,                 
//                  sum(nvl(auth.vmc_auth_total_amount,0))      as total_amount, 
//                  sum(nvl(auth.vmc_sales_count,0))            as sales_count,                 
//                  sum(nvl(auth.vmc_sales_amount,0))           as sales_amount
//          from    tc33_risk_summary           auth,
//                  mif                         mf,
//                  merchant                    mr
//          where   auth.merchant_number = :merchantId and
//                  auth.transaction_date between trunc( (:ReportDateBegin - 515), 'month') and :ReportDateBegin and
//                  mf.merchant_number = auth.merchant_number and
//                  mr.merch_number(+) = mf.merchant_number
//          group by auth.merchant_number, mf.dba_name,
//                   mr.merch_legal_name,   
//                   to_date(mf.date_opened,'mmddrr'),
//                   mf.sic_code, trunc(auth.transaction_date,'month'),
//                   last_day(auth.transaction_date)
//          order by active_date desc      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  auth.merchant_number                        as merchant_number,\n                mf.dba_name                                 as dba_name,\n                mr.merch_legal_name                         as legal_name,\n                to_date(mf.date_opened,'mmddrr')            as date_opened,\n                mf.sic_code                                 as sic_code,\n                trunc(auth.transaction_date,'month')        as active_date,\n                trunc(auth.transaction_date,'month')        as month_begin_date,\n                last_day(auth.transaction_date)             as month_end_date,\n                sum(nvl(auth.vmc_auth_approved_count,0))    as approved_count,                \n                sum(nvl(auth.vmc_auth_approved_amount,0))   as approved_amount, \n                sum(nvl(auth.vmc_auth_avs_count,0))         as avs_count,                 \n                sum(nvl(auth.vmc_auth_avs_amount,0))        as avs_amount, \n                sum(nvl(auth.vmc_auth_decline_count,0))     as decline_count,                 \n                sum(nvl(auth.vmc_auth_decline_amount,0))    as decline_amount, \n                sum(nvl(auth.vmc_auth_total_count,0))       as total_count,                 \n                sum(nvl(auth.vmc_auth_total_amount,0))      as total_amount, \n                sum(nvl(auth.vmc_sales_count,0))            as sales_count,                 \n                sum(nvl(auth.vmc_sales_amount,0))           as sales_amount\n        from    tc33_risk_summary           auth,\n                mif                         mf,\n                merchant                    mr\n        where   auth.merchant_number =  :1  and\n                auth.transaction_date between trunc( ( :2  - 515), 'month') and  :3  and\n                mf.merchant_number = auth.merchant_number and\n                mr.merch_number(+) = mf.merchant_number\n        group by auth.merchant_number, mf.dba_name,\n                 mr.merch_legal_name,   \n                 to_date(mf.date_opened,'mmddrr'),\n                 mf.sic_code, trunc(auth.transaction_date,'month'),\n                 last_day(auth.transaction_date)\n        order by active_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.RiskMerchAuthDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateBegin);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.RiskMerchAuthDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:263^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        if ( MerchData == null )
        {
          MerchData = new MerchantData( resultSet );
        }
        ReportRows.addElement( new RowData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",merchantId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/