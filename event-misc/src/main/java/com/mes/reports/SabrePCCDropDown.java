/*@lineinfo:filename=SabrePCCDropDown*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/SabrePCCDropDown.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 3/04/04 5:10p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class SabrePCCDropDown extends DropDownTable
{
  public SabrePCCDropDown( )
  {
  }
  
  public void initialize( Object[] params )
  {
    ResultSetIterator it          = null;
    long              nodeId      = 0L;
    int               recCount    = 0;
    ResultSet         resultSet   = null;
    
    try
    {
      connect();
      
      nodeId = ((Long)params[0]).longValue();
      
      /*@lineinfo:generated-code*//*@lineinfo:50^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(ms.pcc)     
//          from    organization      o,
//                  group_merchant    gm,
//                  mif               mf,
//                  merchant          mr,
//                  merchant_sabre    ms
//          where   o.org_group = :nodeId and
//                  gm.org_num = o.org_num and
//                  mf.merchant_number = gm.merchant_number and
//                  mr.merch_number = mf.merchant_number and
//                  ms.app_seq_num = mr.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ms.pcc)      \n        from    organization      o,\n                group_merchant    gm,\n                mif               mf,\n                merchant          mr,\n                merchant_sabre    ms\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                mf.merchant_number = gm.merchant_number and\n                mr.merch_number = mf.merchant_number and\n                ms.app_seq_num = mr.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.SabrePCCDropDown",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:63^7*/
      
      // only need all if there is more than one choice
      if ( recCount > 1 )
      {
        addElement("-1", "All PCCs");
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:71^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ms.pcc        as name,
//                  ms.pcc        as value
//          from    organization      o,
//                  group_merchant    gm,
//                  mif               mf,
//                  merchant          mr,
//                  merchant_sabre    ms
//          where   o.org_group = :nodeId and
//                  gm.org_num = o.org_num and
//                  mf.merchant_number = gm.merchant_number and
//                  mr.merch_number = mf.merchant_number and
//                  ms.app_seq_num = mr.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ms.pcc        as name,\n                ms.pcc        as value\n        from    organization      o,\n                group_merchant    gm,\n                mif               mf,\n                merchant          mr,\n                merchant_sabre    ms\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                mf.merchant_number = gm.merchant_number and\n                mr.merch_number = mf.merchant_number and\n                ms.app_seq_num = mr.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.SabrePCCDropDown",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.SabrePCCDropDown",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:85^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        addElement(resultSet);
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("initialize()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/