/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/SortByType.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 1/29/02 4:32p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

public class SortByType
{
  static public   final int       SB_NONE                     = 0;

  private boolean ReverseSort = false;
  private int     SortBy      = SB_NONE;
  
  public SortByType()
  {
  }
  
  public SortByType(int sortBy)
  {
    setSortBy(sortBy);
  }
  
  public int getSortBy()
  {
    return( SortBy );
  }
  
  public int getSortOrder( int newOrder ) 
  {
    int       retVal      = newOrder;
    
    // if we are currently sorted by this type and
    // the sort is not a reverse sort, then invert
    // the passed in value to reverse the sort.
    if ( (SortBy == newOrder) && (isReverseSort() == false) )
    {
      retVal *= -1;
    }
    return( retVal );
  }
  
  public boolean isReverseSort()
  {
    return( ReverseSort );
  }
  
  public void setSortBy(int sortBy)
  {
    SortBy      = sortBy;
    ReverseSort = (sortBy < 0);
    
    if (ReverseSort)
    {
      SortBy *= (-1);
    }
  }
}
