/*************************************************************************

  FILE: $Archive: $

  Description:  


  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

public class SalesStatistic
{
  private int         Count               = 0;
  private int         CountMTD            = 0;
  private int         CountQTD            = 0;
  private int         CountYTD            = 0;
  
  private double      VMCSalesAmount      = 0.0;
  private int         VMCSalesCount       = 0;
  private double      VMCSalesAmountMTD   = 0.0;
  private int         VMCSalesCountMTD    = 0;
  private double      VMCSalesAmountQTD   = 0.0;
  private int         VMCSalesCountQTD    = 0;
  private double      VMCSalesAmountYTD   = 0.0;
  private int         VMCSalesCountYTD    = 0;

  public SalesStatistic()
  {
  }
  
  public double getRatioMTD()
  {
    double      retVal      = 0.0;
    
    if ( VMCSalesCountMTD != 0 )
    {
      retVal = (double)CountMTD/VMCSalesCountMTD;
    }
    return( retVal );
  }
  
  public double getRatioQTD()
  {
    double      retVal      = 0.0;
    
    if ( VMCSalesCountQTD != 0 )
    {
      retVal = (double)CountQTD/VMCSalesCountQTD;
    }
    return( retVal );
  }
  
  public double getRatioYTD()
  {
    double      retVal      = 0.0;
    
    if ( VMCSalesCountYTD != 0 )
    {
      retVal = (double)CountYTD/VMCSalesCountYTD;
    }
    return( retVal );
  }
  
  public int getCount()    { return( Count ); }
  public int getCountMTD() { return( CountMTD ); }
  public int getCountQTD() { return( CountQTD ); }
  public int getCountYTD() { return( CountYTD ); }

  public int getVMCSalesCount()    { return( VMCSalesCount ); }
  public int getVMCSalesCountMTD() { return( VMCSalesCountMTD ); }
  public int getVMCSalesCountQTD() { return( VMCSalesCountQTD ); }
  public int getVMCSalesCountYTD() { return( VMCSalesCountYTD ); }
  
  public void setCount(int newValue)    { Count = newValue; }
  public void setCountMTD(int newValue) { CountMTD = newValue; }
  public void setCountQTD(int newValue) { CountQTD = newValue; }
  public void setCountYTD(int newValue) { CountYTD = newValue; }
  
  public void setVMCSalesCount(int newValue)    { VMCSalesCount = newValue; }
  public void setVMCSalesCountMTD(int newValue) { VMCSalesCountMTD = newValue; }
  public void setVMCSalesCountQTD(int newValue) { VMCSalesCountQTD = newValue; }
  public void setVMCSalesCountYTD(int newValue) { VMCSalesCountYTD = newValue; }
  
  
}