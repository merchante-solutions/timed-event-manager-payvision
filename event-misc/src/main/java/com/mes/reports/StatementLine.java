/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/StatementLine.java $

  Description:
  
    StatementLine
    
    Contains one line of a merchant statement.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 6/08/01 5:14p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.Serializable;

public class StatementLine implements Serializable
{
  private int                   lineNum;
  private String                lineData;
  
  /*
  ** CONSTRUCTORS
  */
  public StatementLine(int lineNum, String lineData)
  {
    this.lineNum = lineNum;
    this.lineData = lineData;
  }
  
  /*
  ** public String toString()
  **
  ** RETURNS: line data as a String.
  */
  public String toString()
  {
    return lineData;
  }
  
  /*
  ** public int getLineNum()
  **
  ** RETURNS: the line number.
  */
  public int getLineNum()
  {
    return lineNum;
  }
}