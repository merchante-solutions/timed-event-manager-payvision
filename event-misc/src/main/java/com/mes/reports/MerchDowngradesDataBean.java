/*@lineinfo:filename=MerchDowngradesDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchDowngradesDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-08-11 11:42:03 -0700 (Mon, 11 Aug 2008) $
  Version            : $Revision: 15253 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Calendar;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class MerchDowngradesDataBean extends OrgSummaryDataBean
{
  public class DetailRow
  {
    protected   String         CardNumber          = null;
    protected   String         CardType            = null;
    protected   String         DowngradeReason     = null;
    protected   long           HierarchyNode       = 0L;
    protected   String         OrgName             = null;
    protected   long           RecId               = 0L;
    protected   String         ReferenceNumber     = null;
    protected   String         SettledIC           = null;
    protected   String         SubmittedIC         = null;
    protected   double         TranAmount          = 0.0;
    protected   Date           TranDate            = null;
    
    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CardNumber        = resultSet.getString("card_number");
      CardType          = resultSet.getString("card_type");
      DowngradeReason   = resultSet.getString("downgrade_reason");
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      OrgName           = resultSet.getString("org_name");
      RecId             = resultSet.getLong("load_sec");
      ReferenceNumber   = resultSet.getString("ref_num");
      SettledIC         = resultSet.getString("settled_ic");
      SubmittedIC       = resultSet.getString("submitted_ic");
      TranAmount        = resultSet.getDouble("tran_amount");
      TranDate          = resultSet.getDate("tran_date");
    }
    
    public String getCardNumber()      { return( (CardNumber == null) ? "" : CardNumber ); }
    public String getCardType()        { return( (CardType   == null) ? "" : CardType   ); }
    public String getDowngradeReason() { return( (DowngradeReason == null) ? "" : DowngradeReason ); }
    public long   getHierarchyNode()   { return( HierarchyNode ); }
    public String getOrgName()         { return( (OrgName == null) ? "" : OrgName ); }
    public long   getRecId()           { return( RecId ); }
    public String getReferenceNumber() { return( (ReferenceNumber == null) ? "" : ReferenceNumber ); }
    public String getSettledIC()       { return( (SettledIC  == null) ? "" : SettledIC  ); }
    public String getSubmittedIC()     { return( (SubmittedIC == null) ? "" : SubmittedIC ); }
    public double getTranAmount()      { return( TranAmount ); }
    public Date   getTranDate()        { return( TranDate ); }
  }
  
  public class DowngradeDetailRecord
  {
    public    Date          BatchDate     = null;
    public    long          BatchNumber   = 0L;
    public    long          DdfDtId       = 0L;
    private   HashMap       Fields        = new HashMap();
    public    long          MerchantId    = 0L;
  
    public DowngradeDetailRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      StringBuffer          fieldName     = new StringBuffer();
      int                   index         = 0;
      ResultSetMetaData     metaData      = resultSet.getMetaData();
      
      // store the primary key to the DDF item
      MerchantId  = resultSet.getLong("merchant_number");
      BatchDate   = resultSet.getDate("dt_batch_date");
      BatchNumber = resultSet.getLong("dt_batch_number");
      DdfDtId     = resultSet.getLong("dt_ddf_dt_id");
    
      for (int i = 1; i <= metaData.getColumnCount(); ++i)
      {
        fieldName.setLength(0);
        fieldName.append( metaData.getColumnName(i) );

        while( (index = fieldName.toString().indexOf("_")) >= 0 )
        {
          fieldName.deleteCharAt(index);
        }
        Fields.put( fieldName.toString().toLowerCase(), resultSet.getString(i) );
      }    
    }
    
    public String getCurrency(String fname)
    {
      return( MesMath.toCurrency(getDouble(fname)) );
    }
    
    public double getDouble(String fname)
    {
      double    retVal    = 0.0;
      
      try
      { 
        retVal = Double.parseDouble(getString(fname));
      }
      catch( Exception e )
      {
      }
      return( retVal );
    }
    
    public int getInt(String fname)
    {
      int    retVal    = 0;
      
      try
      { 
        retVal = Integer.parseInt(getString(fname));
      }
      catch( Exception e )
      {
      }
      return( retVal );
    }
    
    public String getString(String fname)
    {
      String    retVal    = (String)Fields.get(fname.toLowerCase());
      return( (retVal == null) ? "" : retVal );
    }
  }
  
  protected   String      CardType              = null;
  protected   long        DowngradeRecId        = 0L;
  
  public MerchDowngradesDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_SUMMARY )
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"Downgrade Cnt\",");
      line.append("\"Downgrade Amt\"");
    }
    else  // RT_DETAILS
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"Date\",");
      line.append("\"CT\",");
      line.append("\"Card Number\",");
      line.append("\"Tran Amount\",");
      line.append("\"Reference\",");
      line.append("\"Submitted IC\",");
      line.append("\"Settled IC\",");
      line.append("\"Downgrade Reason\"");
    }      
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    if ( ReportType == RT_SUMMARY )
    {
      MesOrgSummaryEntry  record    = (MesOrgSummaryEntry)obj;
    
      line.append( encodeHierarchyNode(record.getHierarchyNode()) );
      line.append( ",\"" );
      line.append( record.getOrgName() );
      line.append( "\"," );
      line.append( record.getCount() );
      line.append( "," );
      line.append( record.getAmount() );
    }
    else    // RT_DETAILS
    {
      DetailRow  record    = (DetailRow)obj;
    
      line.append( encodeHierarchyNode(record.HierarchyNode) );
      line.append( ",\"" );
      line.append( record.OrgName );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate(record.TranDate,"MM/dd/yyyy") );
      line.append( ",\"" );
      line.append( record.CardType );
      line.append( "\",\"" );
      line.append( record.CardNumber );
      line.append( "\"," );
      line.append( record.TranAmount );
      line.append( ",\"'" );
      line.append( record.ReferenceNumber );
      line.append( "\",\"" );
      line.append( record.SubmittedIC );
      line.append( "\",\"" );
      line.append( record.SettledIC );
      line.append( "\",\"" );
      line.append( record.DowngradeReason );
      line.append( "\"" );
    }
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl( buffer, nodeId, beginDate, endDate );
    buffer.append("&cardType=");
    buffer.append(getCardType());
  }
  
  public String getCardType()
  {
    return( CardType );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    
    if ( ReportType == RT_SUMMARY )
    {
      filename.append("_downgrade_summary_");
    }
    else  // RT_DETAILS
    {
      filename.append("_downgrade_details_");
    }      
    
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"MMddyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"MMddyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      if ( CardType.equals("VS") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:305^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                       INDEX (gm pkgroup_merchant) 
//                    */
//                    vd.load_sec                               as load_sec,
//                    mf.merchant_number                        as hierarchy_node,
//                    mf.dba_name                               as org_name,
//                    'VS'                                      as card_type,
//                    vd.account_number                         as card_number,
//                    vd.submitted_irf_desc                     as submitted_ic,
//                    vd.settled_irf_desc                       as settled_ic,
//                    to_date((lpad(vd.purchase_date,4,'0') || 
//                            to_char(vd.source_batch_date,'yyyy')),
//                          'mmddyyyy')                         as tran_date,
//                    vd.source_amount                          as tran_amount,                      
//                    (
//                      decode(vd.fee_reclassification_reason,
//                             '000',decode(vd.paymt_svc_reclass_reason,
//                                          '000',vd.merchant_vm_reclass_reason,
//                                          vd.paymt_svc_reclass_reason),
//                             vd.fee_reclassification_reason) 
//                      || ' - ' ||
//                      nvl(drd.DESCRIPTION,'No description available') 
//                    )                                         as downgrade_reason,                      
//                    vd.acquirer_ref_num                       as ref_num                                                          
//            from    group_merchant            gm,
//                    mif                       mf,
//                    visa_downgrades           vd,
//                    downgrade_reason_desc     drd
//            where   gm.org_num = :orgId 
//                    and mf.merchant_number = gm.merchant_number 
//                    and vd.card_acceptor_id = mf.merchant_number 
//                    and vd.source_batch_date between :beginDate and :endDate
//                    and drd.card_type(+) = 'VS'
//                    and drd.REASON_CODE(+) = decode(vd.fee_reclassification_reason,
//                                                    '000',decode(vd.paymt_svc_reclass_reason,
//                                                                 '000',vd.merchant_vm_reclass_reason,
//                                                                 vd.paymt_svc_reclass_reason),
//                                                    vd.fee_reclassification_reason)
//            order by mf.merchant_number, vd.source_batch_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                     INDEX (gm pkgroup_merchant) \n                  */\n                  vd.load_sec                               as load_sec,\n                  mf.merchant_number                        as hierarchy_node,\n                  mf.dba_name                               as org_name,\n                  'VS'                                      as card_type,\n                  vd.account_number                         as card_number,\n                  vd.submitted_irf_desc                     as submitted_ic,\n                  vd.settled_irf_desc                       as settled_ic,\n                  to_date((lpad(vd.purchase_date,4,'0') || \n                          to_char(vd.source_batch_date,'yyyy')),\n                        'mmddyyyy')                         as tran_date,\n                  vd.source_amount                          as tran_amount,                      \n                  (\n                    decode(vd.fee_reclassification_reason,\n                           '000',decode(vd.paymt_svc_reclass_reason,\n                                        '000',vd.merchant_vm_reclass_reason,\n                                        vd.paymt_svc_reclass_reason),\n                           vd.fee_reclassification_reason) \n                    || ' - ' ||\n                    nvl(drd.DESCRIPTION,'No description available') \n                  )                                         as downgrade_reason,                      \n                  vd.acquirer_ref_num                       as ref_num                                                          \n          from    group_merchant            gm,\n                  mif                       mf,\n                  visa_downgrades           vd,\n                  downgrade_reason_desc     drd\n          where   gm.org_num =  :1  \n                  and mf.merchant_number = gm.merchant_number \n                  and vd.card_acceptor_id = mf.merchant_number \n                  and vd.source_batch_date between  :2  and  :3 \n                  and drd.card_type(+) = 'VS'\n                  and drd.REASON_CODE(+) = decode(vd.fee_reclassification_reason,\n                                                  '000',decode(vd.paymt_svc_reclass_reason,\n                                                               '000',vd.merchant_vm_reclass_reason,\n                                                               vd.paymt_svc_reclass_reason),\n                                                  vd.fee_reclassification_reason)\n          order by mf.merchant_number, vd.source_batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchDowngradesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchDowngradesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:346^9*/
      }
      else if ( CardType.equals("MC") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:350^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                       INDEX (gm pkgroup_merchant) 
//                    */
//                    md.load_sec                               as load_sec,
//                    mf.merchant_number                        as hierarchy_node,
//                    mf.dba_name                               as org_name,
//                    'MC'                                      as card_type,
//                    md.card_number                            as card_number,
//                    ( md.ic_rate_designator || ' - ' ||
//                      nvl(icd_req.ic_desc,'Unknown') )        as submitted_ic,
//                    ( md.ic_rate_designator_adjusted || ' - ' ||
//                      nvl(icd.ic_desc,'Unknown') )            as settled_ic,
//                    md.transaction_date                       as tran_date,
//                    md.transaction_amount                     as tran_amount,                      
//                    'See Item Detail'                         as downgrade_reason,                      
//                    md.reference_number                       as ref_num
//            from    group_merchant            gm,
//                    mif                       mf,
//                    mc_downgrades             md,
//                    mc_interchange_desc       icd_req,
//                    mc_interchange_desc       icd,
//                    mc_interchange_desc       icd_time
//            where   gm.org_num = :orgId 
//                    and mf.merchant_number = gm.merchant_number 
//                    and md.merchant_number = mf.merchant_number 
//                    and md.business_date between :beginDate and :endDate
//                    and icd_req.ic_code(+) = md.ic_rate_designator
//                    and icd.ic_code(+) = md.ic_rate_designator_adjusted
//                    and icd_time.ic_code(+) = md.timeliness_ic_rate_designator
//            order by mf.merchant_number, md.business_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                     INDEX (gm pkgroup_merchant) \n                  */\n                  md.load_sec                               as load_sec,\n                  mf.merchant_number                        as hierarchy_node,\n                  mf.dba_name                               as org_name,\n                  'MC'                                      as card_type,\n                  md.card_number                            as card_number,\n                  ( md.ic_rate_designator || ' - ' ||\n                    nvl(icd_req.ic_desc,'Unknown') )        as submitted_ic,\n                  ( md.ic_rate_designator_adjusted || ' - ' ||\n                    nvl(icd.ic_desc,'Unknown') )            as settled_ic,\n                  md.transaction_date                       as tran_date,\n                  md.transaction_amount                     as tran_amount,                      \n                  'See Item Detail'                         as downgrade_reason,                      \n                  md.reference_number                       as ref_num\n          from    group_merchant            gm,\n                  mif                       mf,\n                  mc_downgrades             md,\n                  mc_interchange_desc       icd_req,\n                  mc_interchange_desc       icd,\n                  mc_interchange_desc       icd_time\n          where   gm.org_num =  :1  \n                  and mf.merchant_number = gm.merchant_number \n                  and md.merchant_number = mf.merchant_number \n                  and md.business_date between  :2  and  :3 \n                  and icd_req.ic_code(+) = md.ic_rate_designator\n                  and icd.ic_code(+) = md.ic_rate_designator_adjusted\n                  and icd_time.ic_code(+) = md.timeliness_ic_rate_designator\n          order by mf.merchant_number, md.business_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchDowngradesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchDowngradesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:382^9*/
      }
      
      if ( it != null )
      {
        resultSet = it.getResultSet();
  
        while( resultSet.next() )
        {
          ReportRows.addElement( new DetailRow( resultSet ) );
        }
        resultSet.close();
        it.close();
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public DowngradeDetailRecord loadDowngradeDetail( String cardType, long recId )
  {
    DowngradeDetailRecord   retVal      = null;
    
    if ( cardType.equals("VS") )
    {
      retVal = loadDowngradeDetailVisa(recId);
    }
    else if ( cardType.equals("MC") )
    {
      retVal = loadDowngradeDetailMasterCard(recId);
    }
    return( retVal );
  }
  
  public DowngradeDetailRecord loadDowngradeDetailMasterCard( long recId )
  {
    Date                    beginDate   = getReportDateBegin();
    Date                    endDate     = getReportDateEnd();
    ResultSetIterator       it          = null;
    long                    nodeId      = getReportHierarchyNodeDefault();
    ResultSet               resultSet   = null;
    DowngradeDetailRecord   retVal      = null;
  
    try
    {
      System.out.println("Loading " + recId + "...");
      System.out.println("  nodeId    : " + nodeId );
      System.out.println("  beginDate : " + beginDate );
      System.out.println("  endDate   : " + endDate );
      
      /*@lineinfo:generated-code*//*@lineinfo:438^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  md.load_sec                       as load_sec,
//                  md.merchant_number                as merchant_number,
//                  to_char(md.transaction_date,'mm/dd/yyyy') as tran_date,
//                  md.mc_member_id                   as mc_member_id,
//                  md.cat_id                         as cat_id,
//                  to_char(md.business_date,'mm/dd/yyyy')    as batch_date,
//                  md.business_cycle                 as business_cycle,
//                  md.reference_number               as ref_num,
//                  md.card_number                    as card_number,
//                  md.sic_code                       as sic_code,
//                  md.approval_code                  as approval_code,
//                  md.transaction_amount             as tran_amount,
//                  md.authorization_amount           as auth_amount,
//                  to_char(md.authorization_time,
//                          'mm/dd/yyyy hh24:mi:ss')  as auth_timestamp,
//                  md.ic_rate_designator             as ic_code,
//                  nvl(icd_req.ic_desc,'Unknown')    as ic_desc,
//                  md.gcms_ic_fee_amount             as ic_amount,
//                  md.ic_rate_designator_adjusted    as ic_code_adj,
//                  nvl(icd.ic_desc,'Unknown')        as ic_desc_adj,
//                  md.gcms_ic_fee_amount_adjusted    as ic_amount_adj,
//                  md.ic_fee_amount_adjusted         as ic_amount_diff,
//                  md.dba_name                       as dba_name,
//                  md.dba_city                       as dba_city,
//                  md.dba_state                      as dba_state,
//                  md.country_code                   as country_code,
//                  md.timeliness_ic_rate_designator  as timeliness_ic_code,
//                  nvl(icd_time.ic_desc,'Unknown')   as timeliness_ic_desc,
//                  md.banknet_ref_num                as banknet_ref_num,
//                  md.banknet_date                   as banknet_date,
//                  md.timeliness_code                as rr_timeliness_code,
//                  decode(md.timeliness_code,
//                         0,'Failed',
//                         1,'Passed',
//                         'Unknown')                 as rr_timeliness_desc,
//                  md.mag_stripe_code                as rr_mag_stripe_code,
//                  nvl(msd.mag_stripe_desc,'Unknown')as rr_mag_stripe_desc,
//                  md.card_acceptor_business_code    as rr_business_code,
//                  decode(md.card_acceptor_business_code,
//                         0,'Failed',
//                         1,'Passed',
//                         'Unknown')                 as rr_business_desc,
//                  md.authorization_code             as rr_auth_code,
//                  decode(md.authorization_code,
//                         0,'Failed',
//                         1,'Passed',
//                         'Unknown')                 as rr_auth_desc,
//                  md.transaction_amount_code        as rr_tran_amount_code,
//                  decode(md.transaction_amount_code,
//                         0,'Failed',
//                         1,'Passed',
//                         'Unknown')                 as rr_tran_amount_desc,
//                  md.acceptance_brand_id_code       as acceptance_brand_id_code,
//                  md.business_service_type_code     as business_service_type_code,
//                  md.business_service_id            as business_service_id,
//                  md.card_number_enc                as card_number_enc,
//                  md.dt_batch_date                  as dt_batch_date,
//                  md.dt_batch_number                as dt_batch_number,
//                  md.dt_ddf_dt_id                   as dt_ddf_dt_id,
//                  md.load_filename                  as load_filename,
//                  md.load_file_id                   as load_file_id
//          from    mc_downgrades             md,
//                  mc_interchange_desc       icd_req,
//                  mc_interchange_desc       icd,
//                  mc_interchange_desc       icd_time,
//                  mc_mag_stripe_code_desc   msd
//          where   md.load_sec = :recId 
//                  and exists
//                  (
//                    select  gm.merchant_number
//                    from    organization              o,
//                            group_merchant            gm
//                    where   o.org_group = :nodeId 
//                            and gm.org_num = o.org_num
//                            and gm.merchant_number = md.merchant_number
//                  )
//                  and icd_req.ic_code(+) = ic_rate_designator
//                  and icd.ic_code(+) = md.ic_rate_designator_adjusted
//                  and icd_time.ic_code(+) = md.timeliness_ic_rate_designator
//                  and msd.mag_stripe_code(+) = md.mag_stripe_code
//          order by merchant_number, load_sec        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  md.load_sec                       as load_sec,\n                md.merchant_number                as merchant_number,\n                to_char(md.transaction_date,'mm/dd/yyyy') as tran_date,\n                md.mc_member_id                   as mc_member_id,\n                md.cat_id                         as cat_id,\n                to_char(md.business_date,'mm/dd/yyyy')    as batch_date,\n                md.business_cycle                 as business_cycle,\n                md.reference_number               as ref_num,\n                md.card_number                    as card_number,\n                md.sic_code                       as sic_code,\n                md.approval_code                  as approval_code,\n                md.transaction_amount             as tran_amount,\n                md.authorization_amount           as auth_amount,\n                to_char(md.authorization_time,\n                        'mm/dd/yyyy hh24:mi:ss')  as auth_timestamp,\n                md.ic_rate_designator             as ic_code,\n                nvl(icd_req.ic_desc,'Unknown')    as ic_desc,\n                md.gcms_ic_fee_amount             as ic_amount,\n                md.ic_rate_designator_adjusted    as ic_code_adj,\n                nvl(icd.ic_desc,'Unknown')        as ic_desc_adj,\n                md.gcms_ic_fee_amount_adjusted    as ic_amount_adj,\n                md.ic_fee_amount_adjusted         as ic_amount_diff,\n                md.dba_name                       as dba_name,\n                md.dba_city                       as dba_city,\n                md.dba_state                      as dba_state,\n                md.country_code                   as country_code,\n                md.timeliness_ic_rate_designator  as timeliness_ic_code,\n                nvl(icd_time.ic_desc,'Unknown')   as timeliness_ic_desc,\n                md.banknet_ref_num                as banknet_ref_num,\n                md.banknet_date                   as banknet_date,\n                md.timeliness_code                as rr_timeliness_code,\n                decode(md.timeliness_code,\n                       0,'Failed',\n                       1,'Passed',\n                       'Unknown')                 as rr_timeliness_desc,\n                md.mag_stripe_code                as rr_mag_stripe_code,\n                nvl(msd.mag_stripe_desc,'Unknown')as rr_mag_stripe_desc,\n                md.card_acceptor_business_code    as rr_business_code,\n                decode(md.card_acceptor_business_code,\n                       0,'Failed',\n                       1,'Passed',\n                       'Unknown')                 as rr_business_desc,\n                md.authorization_code             as rr_auth_code,\n                decode(md.authorization_code,\n                       0,'Failed',\n                       1,'Passed',\n                       'Unknown')                 as rr_auth_desc,\n                md.transaction_amount_code        as rr_tran_amount_code,\n                decode(md.transaction_amount_code,\n                       0,'Failed',\n                       1,'Passed',\n                       'Unknown')                 as rr_tran_amount_desc,\n                md.acceptance_brand_id_code       as acceptance_brand_id_code,\n                md.business_service_type_code     as business_service_type_code,\n                md.business_service_id            as business_service_id,\n                md.card_number_enc                as card_number_enc,\n                md.dt_batch_date                  as dt_batch_date,\n                md.dt_batch_number                as dt_batch_number,\n                md.dt_ddf_dt_id                   as dt_ddf_dt_id,\n                md.load_filename                  as load_filename,\n                md.load_file_id                   as load_file_id\n        from    mc_downgrades             md,\n                mc_interchange_desc       icd_req,\n                mc_interchange_desc       icd,\n                mc_interchange_desc       icd_time,\n                mc_mag_stripe_code_desc   msd\n        where   md.load_sec =  :1  \n                and exists\n                (\n                  select  gm.merchant_number\n                  from    organization              o,\n                          group_merchant            gm\n                  where   o.org_group =  :2  \n                          and gm.org_num = o.org_num\n                          and gm.merchant_number = md.merchant_number\n                )\n                and icd_req.ic_code(+) = ic_rate_designator\n                and icd.ic_code(+) = md.ic_rate_designator_adjusted\n                and icd_time.ic_code(+) = md.timeliness_ic_rate_designator\n                and msd.mag_stripe_code(+) = md.mag_stripe_code\n        order by merchant_number, load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchDowngradesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setLong(2,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchDowngradesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:521^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        retVal = new DowngradeDetailRecord(resultSet);
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("loadDowngradeDetailMasterCard()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ){}
    }
    return( retVal );
  }
  
  public DowngradeDetailRecord loadDowngradeDetailVisa( long recId )
  {
    Date                    beginDate   = getReportDateBegin();
    Date                    endDate     = getReportDateEnd();
    ResultSetIterator       it          = null;
    long                    nodeId      = getReportHierarchyNodeDefault();
    ResultSet               resultSet   = null;
    DowngradeDetailRecord   retVal      = null;
  
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:552^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vd.load_sec                         as load_sec,
//                  vd.card_acceptor_id                 as merchant_number,
//                  vd.code                             as code,
//                  vd.account_number                   as card_number,
//                  vd.account_number_ext               as account_number_ext,
//                  vd.floor_limit_ind                  as floor_limit_ind,
//                  vd.crb_exception_file_ind           as crb_exception_file_ind,
//                  vd.pcas_ind                         as pcas_ind,
//                  vd.acquirer_ref_num                 as ref_num,
//                  vd.acquirers_business_id            as acquirers_business_id,
//                  lpad(vd.purchase_date,4,'0')        as tran_date,
//                  vd.destination_amount               as dest_amount,
//                  vd.destination_currency_code        as dest_currency_code,
//                  vd.source_amount                    as source_amount,
//                  vd.source_currency_code             as source_currency_code,
//                  vd.merchant_name                    as merchant_name,
//                  vd.merchant_city                    as merchant_city,
//                  vd.merchant_country_code            as merchant_country_code,
//                  vd.merchant_category_code           as merchant_category_code,
//                  vd.merchant_zip_code                as merchant_zip,
//                  vd.merchant_state_code              as merchant_state,
//                  vd.requested_payment_service        as rps,
//                  vdrps.data_desc                     as rps_desc,
//                  vd.usage_code                       as usage_code,
//                  vd.reason_code                      as reason_code,
//                  vd.sellement_flag                   as settlement_flag,
//                  vd.authorization_cha_ind            as auth_aci,
//                  acid.aci_desc                       as auth_aci_desc,
//                  vd.authorization_code               as approval_code,
//                  vd.pos_terminal_capability          as pos_term_cap_code,
//                  vdptc.data_desc                     as pos_term_cap_desc,
//                  vd.international_fee_ind            as intl_fee_ind_code,
//                  vd.cardholder_id_method             as cardholder_id_method,
//                  vdcid.data_desc                     as cardholder_id_desc,
//                  vd.collection_only_flag             as collection_only_flag,
//                  vd.pos_entry_mode                   as pos_entry_mode,
//                  nvl(pd.pos_entry_desc,vd.pos_entry_mode)  as pos_entry_mode_desc, 
//                  lpad(vd.central_process_date,4,'0') as central_process_date,
//                  vd.reimbursement_att                as reimburse_attr,
//                  vdra.data_desc                      as reimburse_attr_desc,
//                  vd.issuer_workstation_bin           as issuer_workstation_bin,
//                  vd.acquirer_workstation_bin         as acquirer_workstation_bin,
//                  vd.chargeback_ref_num               as chargeback_ref_num,
//                  vd.documentation_ind                as documentation_ind,
//                  vd.member_message_text              as member_message_text,
//                  vd.special_condition_ind            as special_condition_ind,
//                  vd.terminal_id                      as terminal_id,
//                  vd.national_reimbursement_fee       as national_reimbursement_fee,
//                  vd.mail_tel_ecommerce_ind           as moto_ecomm_ind,
//                  med.moto_ecommerce_desc             as moto_ecomm_ind_desc,
//                  vd.special_chargeback_ind           as special_chargeback_ind,
//                  vd.interface_trace_num              as interface_trace_num,
//                  vd.cardholder_activated_term_ind    as cardholder_activated_term_ind,
//                  vd.prepaid_card_ind                 as prepaid_card_ind,
//                  vd.service_development_field        as service_development_field,
//                  vd.avs_response_code                as avs_response_code,
//                  avs.response_desc                   as avs_response_desc,
//                  vd.authorization_source_code        as auth_source_code,
//                  sicd.stand_in_desc                  as auth_source_desc,
//                  vd.atm_account_selection            as atm_account_selection,
//                  vd.installment_payment_count        as installment_payment_count,
//                  vd.purchase_identifier_format       as purchase_id_format,
//                  vd.purchase_identifier              as purchase_id,
//                  vd.cashback                         as cashback,
//                  vd.chip_condition_code              as chip_condition_code,
//                  vd.transaction_identifier           as transaction_id,
//                  vd.validation_code                  as val_code,
//                  vd.authorized_amount                as auth_amount,
//                  nvl(vd.authorization_currency_code,'840') as auth_currency_code,
//                  vd.authorization_response_code      as auth_resp_code,
//                  arc.response_desc                   as auth_resp_desc,
//                  vd.excluded_tran_id_reason          as excluded_tran_id_reason,
//                  vd.crs_process_code                 as crs_process_code,
//                  vd.chargeback_rights_ind            as chargeback_rights_ind,
//                  vd.multiple_clearing_seq_num        as multiple_clearing_seq_num,
//                  vd.multiple_clearing_seq_count      as multiple_clearing_seq_count,
//                  vd.market_specific_auth_data_ind    as market_specific_auth_data_ind,
//                  vd.total_authorized_amount          as total_auth_amount,
//                  vd.information_ind                  as information_ind,
//                  vd.merchant_telephone_num           as merchant_phone,
//                  vd.additional_data_ind              as additional_data_ind,
//                  vd.merchant_volume_ind              as merchant_volume_ind,
//                  vd.local_tax                        as local_tax,
//                  vd.local_tax_included               as local_tax_included,
//                  vd.national_tax                     as national_tax,
//                  vd.national_tax_included            as national_tax_included,
//                  vd.merchant_vat_reg_sin_bus_ref_n   as merchant_vat_reg_sin_bus_ref_n,
//                  vd.customer_vat_reg_num             as customer_vat_reg_num,
//                  vd.summary_commodity_code           as summary_commodity_code,
//                  vd.other_tax                        as other_tax,
//                  vd.message_identifier               as message_identifier,
//                  vd.time_purchase                    as tran_time,
//                  vd.cri                              as cri,
//                  vd.product_code1                    as product_code1,
//                  vd.product_code2                    as product_code2,
//                  vd.product_code3                    as product_code3,
//                  vd.product_code4                    as product_code4,
//                  vd.product_code5                    as product_code5,
//                  vd.product_code6                    as product_code6,
//                  vd.product_code7                    as product_code7,
//                  vd.product_code8                    as product_code8,
//                  vd.destination_bin                  as dest_bin,
//                  vd.source_bin                       as source_bin,
//                  vd.original_transaction_code        as original_tran_code,
//                  vd.original_transaction_code_qfi    as original_tran_code_qfi,
//                  vd.original_transaction_com_seq_n   as original_tran_com_seq_num,
//                  to_char(vd.source_batch_date,'mm/dd/yyyy')  as batch_date,
//                  vd.source_batch_num                 as source_batch_num,
//                  vd.item_seq_num                     as item_seq_num,
//                  vd.settled_ifi                      as settled_ifi,
//                  vd.settled_aci                      as settled_aci,
//                  sacid.aci_desc                      as settled_aci_desc,
//                  vd.settled_requested_paymt_svc      as settled_rps,
//                  vdsrps.data_desc                    as settled_rps_desc,
//                  vd.settled_reimbursement_attr       as settled_reimburse_attr,
//                  vdsra.data_desc                     as settled_reimburse_attr_desc,
//                  vd.submitted_irf_desc               as submitted_ic_desc,
//                  vd.settled_irf_desc                 as settled_ic_desc,
//                  vd.paymt_svc_reclass_reason         as paymt_svc_reclass_reason,
//                  drd_pmt.description                 as paymt_svc_reclass_desc,
//                  vd.fee_reclassification_reason      as fee_reclass_reason,
//                  drd_fee.description                 as fee_reclass_desc,
//                  vd.merchant_vm_reclass_reason       as merchant_vm_reclass_reason,
//                  drd_merch.description               as merchant_vm_reclass_desc,
//                  vd.dt_batch_date                    as dt_batch_date,
//                  vd.dt_batch_number                  as dt_batch_number,
//                  vd.dt_ddf_dt_id                     as dt_ddf_dt_id,
//                  vd.load_filename                    as load_filename,
//                  vd.load_file_id                     as load_file_id
//          from    visa_downgrades               vd,
//                  tc33_authorization_aci_desc   acid,
//                  tc33_authorization_aci_desc   sacid,
//                  pos_entry_mode_desc           pd,
//                  tc33_auth_moto_ecomm_desc     med,
//                  tc33_authorization_avs_code   avs,
//                  tc33_auth_stand_in_code_desc  sicd,
//                  tc33_authorization_resp_code  arc,
//                  downgrade_reason_desc         drd_pmt,
//                  downgrade_reason_desc         drd_fee,
//                  downgrade_reason_desc         drd_merch,
//                  visa_data_code_desc           vdcid,
//                  visa_data_code_desc           vdptc,
//                  visa_data_code_desc           vdrps,
//                  visa_data_code_desc           vdsrps,
//                  visa_data_code_desc           vdra,
//                  visa_data_code_desc           vdsra
//          where   vd.load_sec = :recId 
//                  and exists
//                  (
//                    select  gm.merchant_number
//                    from    organization                  o,
//                            group_merchant                gm
//                    where   o.org_group = :nodeId 
//                            and gm.org_num = o.org_num
//                            and gm.merchant_number = vd.card_acceptor_id
//                  )
//                  and acid.aci(+) = nvl(vd.authorization_cha_ind,'none') 
//                  and sacid.aci(+) = nvl(vd.settled_aci,'none') 
//                  and pd.pos_entry_code(+) = vd.pos_entry_mode 
//                  and med.moto_ecommerce_indicator(+) = vd.mail_tel_ecommerce_ind
//                  and avs.avs_result_code(+) = vd.avs_response_code
//                  and sicd.stand_in_code(+) = nvl(vd.authorization_source_code,'none') 
//                  and arc.response_code(+) = nvl(vd.authorization_response_code,'none') 
//                  and drd_pmt.card_type(+) = 'VS' and drd_pmt.reason_code(+) = vd.paymt_svc_reclass_reason
//                  and drd_fee.card_type(+) = 'VS' and drd_fee.reason_code(+) = vd.fee_reclassification_reason
//                  and drd_merch.card_type(+) = 'VS' and drd_merch.reason_code(+) = vd.merchant_vm_reclass_reason                
//                  and vdcid.data_type(+) = 1 and vdcid.data_code(+) = vd.cardholder_id_method
//                  and vdptc.data_type(+) = 2 and vdptc.data_code(+) = vd.pos_terminal_capability
//                  and vdrps.data_type(+) = 3 and vdrps.data_code(+) = vd.requested_payment_service
//                  and vdsrps.data_type(+) = 3 and vdsrps.data_code(+) = vd.settled_requested_paymt_svc
//                  and vdra.data_type(+) = 4 and vdra.data_code(+) = vd.reimbursement_att
//                  and vdsra.data_type(+) = 4 and vdsra.data_code(+) = vd.settled_reimbursement_attr
//          order by card_acceptor_id, load_sec        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vd.load_sec                         as load_sec,\n                vd.card_acceptor_id                 as merchant_number,\n                vd.code                             as code,\n                vd.account_number                   as card_number,\n                vd.account_number_ext               as account_number_ext,\n                vd.floor_limit_ind                  as floor_limit_ind,\n                vd.crb_exception_file_ind           as crb_exception_file_ind,\n                vd.pcas_ind                         as pcas_ind,\n                vd.acquirer_ref_num                 as ref_num,\n                vd.acquirers_business_id            as acquirers_business_id,\n                lpad(vd.purchase_date,4,'0')        as tran_date,\n                vd.destination_amount               as dest_amount,\n                vd.destination_currency_code        as dest_currency_code,\n                vd.source_amount                    as source_amount,\n                vd.source_currency_code             as source_currency_code,\n                vd.merchant_name                    as merchant_name,\n                vd.merchant_city                    as merchant_city,\n                vd.merchant_country_code            as merchant_country_code,\n                vd.merchant_category_code           as merchant_category_code,\n                vd.merchant_zip_code                as merchant_zip,\n                vd.merchant_state_code              as merchant_state,\n                vd.requested_payment_service        as rps,\n                vdrps.data_desc                     as rps_desc,\n                vd.usage_code                       as usage_code,\n                vd.reason_code                      as reason_code,\n                vd.sellement_flag                   as settlement_flag,\n                vd.authorization_cha_ind            as auth_aci,\n                acid.aci_desc                       as auth_aci_desc,\n                vd.authorization_code               as approval_code,\n                vd.pos_terminal_capability          as pos_term_cap_code,\n                vdptc.data_desc                     as pos_term_cap_desc,\n                vd.international_fee_ind            as intl_fee_ind_code,\n                vd.cardholder_id_method             as cardholder_id_method,\n                vdcid.data_desc                     as cardholder_id_desc,\n                vd.collection_only_flag             as collection_only_flag,\n                vd.pos_entry_mode                   as pos_entry_mode,\n                nvl(pd.pos_entry_desc,vd.pos_entry_mode)  as pos_entry_mode_desc, \n                lpad(vd.central_process_date,4,'0') as central_process_date,\n                vd.reimbursement_att                as reimburse_attr,\n                vdra.data_desc                      as reimburse_attr_desc,\n                vd.issuer_workstation_bin           as issuer_workstation_bin,\n                vd.acquirer_workstation_bin         as acquirer_workstation_bin,\n                vd.chargeback_ref_num               as chargeback_ref_num,\n                vd.documentation_ind                as documentation_ind,\n                vd.member_message_text              as member_message_text,\n                vd.special_condition_ind            as special_condition_ind,\n                vd.terminal_id                      as terminal_id,\n                vd.national_reimbursement_fee       as national_reimbursement_fee,\n                vd.mail_tel_ecommerce_ind           as moto_ecomm_ind,\n                med.moto_ecommerce_desc             as moto_ecomm_ind_desc,\n                vd.special_chargeback_ind           as special_chargeback_ind,\n                vd.interface_trace_num              as interface_trace_num,\n                vd.cardholder_activated_term_ind    as cardholder_activated_term_ind,\n                vd.prepaid_card_ind                 as prepaid_card_ind,\n                vd.service_development_field        as service_development_field,\n                vd.avs_response_code                as avs_response_code,\n                avs.response_desc                   as avs_response_desc,\n                vd.authorization_source_code        as auth_source_code,\n                sicd.stand_in_desc                  as auth_source_desc,\n                vd.atm_account_selection            as atm_account_selection,\n                vd.installment_payment_count        as installment_payment_count,\n                vd.purchase_identifier_format       as purchase_id_format,\n                vd.purchase_identifier              as purchase_id,\n                vd.cashback                         as cashback,\n                vd.chip_condition_code              as chip_condition_code,\n                vd.transaction_identifier           as transaction_id,\n                vd.validation_code                  as val_code,\n                vd.authorized_amount                as auth_amount,\n                nvl(vd.authorization_currency_code,'840') as auth_currency_code,\n                vd.authorization_response_code      as auth_resp_code,\n                arc.response_desc                   as auth_resp_desc,\n                vd.excluded_tran_id_reason          as excluded_tran_id_reason,\n                vd.crs_process_code                 as crs_process_code,\n                vd.chargeback_rights_ind            as chargeback_rights_ind,\n                vd.multiple_clearing_seq_num        as multiple_clearing_seq_num,\n                vd.multiple_clearing_seq_count      as multiple_clearing_seq_count,\n                vd.market_specific_auth_data_ind    as market_specific_auth_data_ind,\n                vd.total_authorized_amount          as total_auth_amount,\n                vd.information_ind                  as information_ind,\n                vd.merchant_telephone_num           as merchant_phone,\n                vd.additional_data_ind              as additional_data_ind,\n                vd.merchant_volume_ind              as merchant_volume_ind,\n                vd.local_tax                        as local_tax,\n                vd.local_tax_included               as local_tax_included,\n                vd.national_tax                     as national_tax,\n                vd.national_tax_included            as national_tax_included,\n                vd.merchant_vat_reg_sin_bus_ref_n   as merchant_vat_reg_sin_bus_ref_n,\n                vd.customer_vat_reg_num             as customer_vat_reg_num,\n                vd.summary_commodity_code           as summary_commodity_code,\n                vd.other_tax                        as other_tax,\n                vd.message_identifier               as message_identifier,\n                vd.time_purchase                    as tran_time,\n                vd.cri                              as cri,\n                vd.product_code1                    as product_code1,\n                vd.product_code2                    as product_code2,\n                vd.product_code3                    as product_code3,\n                vd.product_code4                    as product_code4,\n                vd.product_code5                    as product_code5,\n                vd.product_code6                    as product_code6,\n                vd.product_code7                    as product_code7,\n                vd.product_code8                    as product_code8,\n                vd.destination_bin                  as dest_bin,\n                vd.source_bin                       as source_bin,\n                vd.original_transaction_code        as original_tran_code,\n                vd.original_transaction_code_qfi    as original_tran_code_qfi,\n                vd.original_transaction_com_seq_n   as original_tran_com_seq_num,\n                to_char(vd.source_batch_date,'mm/dd/yyyy')  as batch_date,\n                vd.source_batch_num                 as source_batch_num,\n                vd.item_seq_num                     as item_seq_num,\n                vd.settled_ifi                      as settled_ifi,\n                vd.settled_aci                      as settled_aci,\n                sacid.aci_desc                      as settled_aci_desc,\n                vd.settled_requested_paymt_svc      as settled_rps,\n                vdsrps.data_desc                    as settled_rps_desc,\n                vd.settled_reimbursement_attr       as settled_reimburse_attr,\n                vdsra.data_desc                     as settled_reimburse_attr_desc,\n                vd.submitted_irf_desc               as submitted_ic_desc,\n                vd.settled_irf_desc                 as settled_ic_desc,\n                vd.paymt_svc_reclass_reason         as paymt_svc_reclass_reason,\n                drd_pmt.description                 as paymt_svc_reclass_desc,\n                vd.fee_reclassification_reason      as fee_reclass_reason,\n                drd_fee.description                 as fee_reclass_desc,\n                vd.merchant_vm_reclass_reason       as merchant_vm_reclass_reason,\n                drd_merch.description               as merchant_vm_reclass_desc,\n                vd.dt_batch_date                    as dt_batch_date,\n                vd.dt_batch_number                  as dt_batch_number,\n                vd.dt_ddf_dt_id                     as dt_ddf_dt_id,\n                vd.load_filename                    as load_filename,\n                vd.load_file_id                     as load_file_id\n        from    visa_downgrades               vd,\n                tc33_authorization_aci_desc   acid,\n                tc33_authorization_aci_desc   sacid,\n                pos_entry_mode_desc           pd,\n                tc33_auth_moto_ecomm_desc     med,\n                tc33_authorization_avs_code   avs,\n                tc33_auth_stand_in_code_desc  sicd,\n                tc33_authorization_resp_code  arc,\n                downgrade_reason_desc         drd_pmt,\n                downgrade_reason_desc         drd_fee,\n                downgrade_reason_desc         drd_merch,\n                visa_data_code_desc           vdcid,\n                visa_data_code_desc           vdptc,\n                visa_data_code_desc           vdrps,\n                visa_data_code_desc           vdsrps,\n                visa_data_code_desc           vdra,\n                visa_data_code_desc           vdsra\n        where   vd.load_sec =  :1  \n                and exists\n                (\n                  select  gm.merchant_number\n                  from    organization                  o,\n                          group_merchant                gm\n                  where   o.org_group =  :2  \n                          and gm.org_num = o.org_num\n                          and gm.merchant_number = vd.card_acceptor_id\n                )\n                and acid.aci(+) = nvl(vd.authorization_cha_ind,'none') \n                and sacid.aci(+) = nvl(vd.settled_aci,'none') \n                and pd.pos_entry_code(+) = vd.pos_entry_mode \n                and med.moto_ecommerce_indicator(+) = vd.mail_tel_ecommerce_ind\n                and avs.avs_result_code(+) = vd.avs_response_code\n                and sicd.stand_in_code(+) = nvl(vd.authorization_source_code,'none') \n                and arc.response_code(+) = nvl(vd.authorization_response_code,'none') \n                and drd_pmt.card_type(+) = 'VS' and drd_pmt.reason_code(+) = vd.paymt_svc_reclass_reason\n                and drd_fee.card_type(+) = 'VS' and drd_fee.reason_code(+) = vd.fee_reclassification_reason\n                and drd_merch.card_type(+) = 'VS' and drd_merch.reason_code(+) = vd.merchant_vm_reclass_reason                \n                and vdcid.data_type(+) = 1 and vdcid.data_code(+) = vd.cardholder_id_method\n                and vdptc.data_type(+) = 2 and vdptc.data_code(+) = vd.pos_terminal_capability\n                and vdrps.data_type(+) = 3 and vdrps.data_code(+) = vd.requested_payment_service\n                and vdsrps.data_type(+) = 3 and vdsrps.data_code(+) = vd.settled_requested_paymt_svc\n                and vdra.data_type(+) = 4 and vdra.data_code(+) = vd.reimbursement_att\n                and vdsra.data_type(+) = 4 and vdsra.data_code(+) = vd.settled_reimbursement_attr\n        order by card_acceptor_id, load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchDowngradesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setLong(2,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchDowngradesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:727^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        retVal = new DowngradeDetailRecord(resultSet);
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("loadDowngradeDetailVisa()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ){}
    }
    return( retVal );
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    if ( CardType.equals("VS") )
    {
      loadSummaryDataVisa( orgId, beginDate, endDate );
    }
    else if ( CardType.equals("MC") )
    {
      loadSummaryDataMasterCard( orgId, beginDate, endDate );
    }
  }
  
  public void loadSummaryDataMasterCard( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    
    try
    {
      if ( hasAssocDistricts() )
      // returns false unless the current node is an
      // association and it has districts under it.
      {
        if ( District == DISTRICT_NONE )
        {
          /*@lineinfo:generated-code*//*@lineinfo:771^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      o.org_group                               as hierarchy_node,
//                      nvl(mf.district,:DISTRICT_UNASSIGNED)     as district,
//                      nvl(ad.district_desc,decode( mf.district,
//                                  null,'Unassigned',
//                                 ('District ' || to_char(mf.district, '0009')))) as org_name,
//                      count(1)                                  as item_count,
//                      sum( md.transaction_amount )              as item_amount
//              from    organization              o,
//                      group_merchant            gm,
//                      group_rep_merchant        grm,
//                      mc_downgrades             md,
//                      mif                       mf,
//                      assoc_districts           ad
//              where   o.org_num = :orgId and
//                      gm.org_num = o.org_num and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      md.merchant_number = mf.merchant_number and
//                      md.business_date between :beginDate and :endDate and
//                      ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                      ad.district(+)      = mf.district
//              group by  o.org_num, o.org_group, 
//                        mf.district, ad.district_desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    o.org_group                               as hierarchy_node,\n                    nvl(mf.district, :1 )     as district,\n                    nvl(ad.district_desc,decode( mf.district,\n                                null,'Unassigned',\n                               ('District ' || to_char(mf.district, '0009')))) as org_name,\n                    count(1)                                  as item_count,\n                    sum( md.transaction_amount )              as item_amount\n            from    organization              o,\n                    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    mc_downgrades             md,\n                    mif                       mf,\n                    assoc_districts           ad\n            where   o.org_num =  :2  and\n                    gm.org_num = o.org_num and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    md.merchant_number = mf.merchant_number and\n                    md.business_date between  :5  and  :6  and\n                    ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                    ad.district(+)      = mf.district\n            group by  o.org_num, o.org_group, \n                      mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchDowngradesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchDowngradesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:802^11*/
        }
        else    // a district was specified
        {
          /*@lineinfo:generated-code*//*@lineinfo:806^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                        INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      mf.merchant_number                        as hierarchy_node,
//                      mf.dba_name                               as org_name,
//                      :District                                 as district,
//                      count(1)                                  as item_count,
//                      sum( md.transaction_amount )              as item_amount
//              from    group_merchant            gm,
//                      group_rep_merchant        grm,
//                      mc_downgrades             md,
//                      mif                       mf,
//                      organization              o
//              where   gm.org_num = :orgId and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      nvl(mf.district,-1) = :District and
//                      md.merchant_number  = mf.merchant_number and
//                      md.business_date between :beginDate and :endDate and
//                      o.org_group = mf.merchant_number
//              group by  o.org_num, mf.merchant_number, mf.dba_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                      INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    mf.merchant_number                        as hierarchy_node,\n                    mf.dba_name                               as org_name,\n                     :1                                  as district,\n                    count(1)                                  as item_count,\n                    sum( md.transaction_amount )              as item_amount\n            from    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    mc_downgrades             md,\n                    mif                       mf,\n                    organization              o\n            where   gm.org_num =  :2  and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    nvl(mf.district,-1) =  :5  and\n                    md.merchant_number  = mf.merchant_number and\n                    md.business_date between  :6  and  :7  and\n                    o.org_group = mf.merchant_number\n            group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchDowngradesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchDowngradesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:832^11*/
        }
      }
      else // just standard child report, no districts
      {
        /*@lineinfo:generated-code*//*@lineinfo:837^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                       INDEX (gm pkgroup_merchant) 
//                    */
//                    o.org_num                                 as org_num,
//                    o.org_group                               as hierarchy_node,
//                    o.org_name                                as org_name,
//                    0                                         as district,
//                    count(1)                                  as item_count,
//                    sum( md.transaction_amount )              as item_amount
//            from    parent_org                po,
//                    organization              o,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    mc_downgrades             md
//            where   po.parent_org_num   = :orgId and
//                    o.org_num           = po.org_num and
//                    gm.org_num          = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    md.merchant_number  = gm.merchant_number and
//                    md.business_date between :beginDate and :endDate
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                     INDEX (gm pkgroup_merchant) \n                  */\n                  o.org_num                                 as org_num,\n                  o.org_group                               as hierarchy_node,\n                  o.org_name                                as org_name,\n                  0                                         as district,\n                  count(1)                                  as item_count,\n                  sum( md.transaction_amount )              as item_amount\n          from    parent_org                po,\n                  organization              o,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  mc_downgrades             md\n          where   po.parent_org_num   =  :1  and\n                  o.org_num           = po.org_num and\n                  gm.org_num          = o.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  md.merchant_number  = gm.merchant_number and\n                  md.business_date between  :4  and  :5 \n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MerchDowngradesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.MerchDowngradesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:862^9*/
      }
      processSummaryData(it.getResultSet());
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryDataMasterCard",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryDataVisa( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    
    try
    {
      if ( hasAssocDistricts() )
      // returns false unless the current node is an
      // association and it has districts under it.
      {
        if ( District == DISTRICT_NONE )
        {
          /*@lineinfo:generated-code*//*@lineinfo:889^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      o.org_group                               as hierarchy_node,
//                      nvl(mf.district,:DISTRICT_UNASSIGNED)     as district,
//                      nvl(ad.district_desc,decode( mf.district,
//                                  null,'Unassigned',
//                                 ('District ' || to_char(mf.district, '0009')))) as org_name,
//                      count( vd.source_amount )                 as item_count,
//                      sum( vd.source_amount )                   as item_amount
//              from    organization              o,
//                      group_merchant            gm,
//                      group_rep_merchant        grm,
//                      visa_downgrades           vd,
//                      mif                       mf,
//                      assoc_districts           ad
//              where   o.org_num           = :orgId and
//                      gm.org_num          = o.org_num and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      vd.card_acceptor_id = mf.merchant_number and
//                      vd.source_batch_date between :beginDate and :endDate and
//                      ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                      ad.district(+)      = mf.district
//              group by  o.org_num, o.org_group, 
//                        mf.district, ad.district_desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    o.org_group                               as hierarchy_node,\n                    nvl(mf.district, :1 )     as district,\n                    nvl(ad.district_desc,decode( mf.district,\n                                null,'Unassigned',\n                               ('District ' || to_char(mf.district, '0009')))) as org_name,\n                    count( vd.source_amount )                 as item_count,\n                    sum( vd.source_amount )                   as item_amount\n            from    organization              o,\n                    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    visa_downgrades           vd,\n                    mif                       mf,\n                    assoc_districts           ad\n            where   o.org_num           =  :2  and\n                    gm.org_num          = o.org_num and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    vd.card_acceptor_id = mf.merchant_number and\n                    vd.source_batch_date between  :5  and  :6  and\n                    ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                    ad.district(+)      = mf.district\n            group by  o.org_num, o.org_group, \n                      mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.MerchDowngradesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.MerchDowngradesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:920^11*/
        }
        else    // a district was specified
        {
          /*@lineinfo:generated-code*//*@lineinfo:924^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                        INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      mf.merchant_number                        as hierarchy_node,
//                      mf.dba_name                               as org_name,
//                      :District                                 as district,
//                      count( vd.source_amount )                 as item_count,
//                      sum( vd.source_amount )                   as item_amount
//              from    group_merchant            gm,
//                      group_rep_merchant        grm,
//                      visa_downgrades           vd,
//                      mif                       mf,
//                      organization              o
//              where   gm.org_num          = :orgId and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      nvl(mf.district,-1) = :District and
//                      vd.card_acceptor_id  = mf.merchant_number and
//                      vd.source_batch_date between :beginDate and :endDate and
//                      o.org_group = mf.merchant_number
//              group by  o.org_num, mf.merchant_number, mf.dba_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                      INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    mf.merchant_number                        as hierarchy_node,\n                    mf.dba_name                               as org_name,\n                     :1                                  as district,\n                    count( vd.source_amount )                 as item_count,\n                    sum( vd.source_amount )                   as item_amount\n            from    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    visa_downgrades           vd,\n                    mif                       mf,\n                    organization              o\n            where   gm.org_num          =  :2  and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    nvl(mf.district,-1) =  :5  and\n                    vd.card_acceptor_id  = mf.merchant_number and\n                    vd.source_batch_date between  :6  and  :7  and\n                    o.org_group = mf.merchant_number\n            group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.MerchDowngradesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.MerchDowngradesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:950^11*/
        }
      }
      else // just standard child report, no districts
      {
        /*@lineinfo:generated-code*//*@lineinfo:955^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                       INDEX (gm pkgroup_merchant) 
//                    */
//                    o.org_num                                 as org_num,
//                    o.org_group                               as hierarchy_node,
//                    o.org_name                                as org_name,
//                    0                                         as district,
//                    count( vd.source_amount )                 as item_count,
//                    sum( vd.source_amount )                   as item_amount
//            from    parent_org                po,
//                    organization              o,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    visa_downgrades           vd
//            where   po.parent_org_num   = :orgId and
//                    o.org_num           = po.org_num and
//                    gm.org_num          = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    vd.card_acceptor_id  = gm.merchant_number and
//                    vd.source_batch_date between :beginDate and :endDate
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                     INDEX (gm pkgroup_merchant) \n                  */\n                  o.org_num                                 as org_num,\n                  o.org_group                               as hierarchy_node,\n                  o.org_name                                as org_name,\n                  0                                         as district,\n                  count( vd.source_amount )                 as item_count,\n                  sum( vd.source_amount )                   as item_amount\n          from    parent_org                po,\n                  organization              o,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  visa_downgrades           vd\n          where   po.parent_org_num   =  :1  and\n                  o.org_num           = po.org_num and\n                  gm.org_num          = o.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  vd.card_acceptor_id  = gm.merchant_number and\n                  vd.source_batch_date between  :4  and  :5 \n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.MerchDowngradesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.MerchDowngradesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:980^9*/
      }
      processSummaryData(it.getResultSet());
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryDataVisa",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setCardType( String ct )
  {
    CardType = ct;
  }
  
  public void setDowngradeRecId( long recId )
  {
    DowngradeRecId = recId;
  }
  
  public void setProperties( HttpServletRequest request )
  {
    super.setProperties(request);
    
    setDowngradeRecId( HttpHelper.getLong(request,"detailRecId",0L) );
    setCardType( HttpHelper.getString(request,"cardType","NONE") );
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/