/*@lineinfo:filename=CustomQueryDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CustomQueryDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-08-11 11:42:03 -0700 (Mon, 11 Aug 2008) $
  Version            : $Revision: 15253 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mes.constants.mesConstants;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HierarchyNodeField;
import com.mes.forms.NumberField;
import com.mes.maintenance.EditorDropDownTable;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class CustomQueryDataBean extends ReportSQLJBean
{
  public static final int               QID_NONE          = 0;
  
  protected static final char[] TerminationChars =
  {
    ' ',
    '-',
    '=',
    '\t',
    ',',
    ')',
    '\n',
    '\r'
  };
  
  public class CustomQuery
  {
    public String         QueryDesc   = null;
    public int            QueryId     = QID_NONE;

    public CustomQuery( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      QueryDesc = resultSet.getString("query_desc");
      QueryId   = resultSet.getInt("query_id");
    }
  }
  
  public class CustomQueryParam
  {
    public Field          HtmlField   = null;
    public String         Label       = null;
    public int            ParamType   = mesConstants.PT_INVALID;
    
    public CustomQueryParam( int pType, String fieldLabel, Field htmlField )
    {
      HtmlField   = htmlField;
      Label       = fieldLabel;
      ParamType   = pType;
    }
  }
  
  public class RowData
  {
    public Vector         Contents    = new Vector();
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      StringBuffer          buf           = new StringBuffer();
      ResultSetMetaData     metaData      = resultSet.getMetaData();
      
      for (int i = 1; i <= metaData.getColumnCount(); ++i)
      {
        buf.setLength(0);
        
        switch ( metaData.getColumnType(i) )
        {
            // SQL Type             // Java Type
          case Types.CHAR:          // String - fixed length string
          case Types.VARCHAR:       // String - variable length string
          case Types.LONGVARCHAR:   // String - variable length string - large (BLOB data)
            buf.append( "\"" );
            buf.append( resultSet.getString(i) );
            buf.append( "\"" );
            break;
    
          case Types.REAL:          // float  ( 32-bit floating point )
          case Types.FLOAT:         // double ( 64-bit floating point )
          case Types.DOUBLE:        // ""
          case Types.NUMERIC:
          case Types.DECIMAL:
            buf.append( resultSet.getDouble(i) );
            break;
    
          case Types.BIT:           // boolean
            buf.append( resultSet.getBoolean(i) );
            break;
    
          case Types.TINYINT:       // byte   (  8-bit signed )
            buf.append( resultSet.getByte(i) );
            break;
    
          case Types.SMALLINT:      // short  ( 16-bit signed )
            buf.append( resultSet.getShort(i) );
            break;
    
          case Types.INTEGER:       // int    ( 32-bit signed )
          case Types.BIGINT:        // long   ( 32-bit signed )
            buf.append( resultSet.getLong(i) );
            
            // if the number of digits is more than 15, then
            // it needs to have a tick in front so that MS Excel
            // will treat it as a string instead of a number.
            // Excel cannot import numbers larger than 15 digits
            if ( buf.length() > 15 )
            {
              buf.insert(0,"'");
            }
            break;
    
          case Types.DATE:          // java.sql.Date - date data
          case Types.TIMESTAMP:     // java.sql.Timestamp - date and time data
            // treat time stamp fields like dates
            buf.append( DateTimeFormatter.getFormattedDate( resultSet.getDate(i), "MM/dd/yyyy" ) );
            break;
    
          case Types.TIME:          // java.sql.Time - time data
            buf.append( resultSet.getTime(i).toString() );
            break;
    
    //@            case Types.BINARY:        // byte[] - fixed length binary
    //@            case Types.VARBINARY:     // byte[] - variable length binary
    //@            case Types.LONGVARBINARY: // byte[] - variable length binary
          default:
            break;
        }
        Contents.addElement( buf.toString() );
      }
    }
  }
  
  private Vector            ColumnHeaderNames       = new Vector();
  private int               QueryId                 = QID_NONE;
  private boolean           Compressed              = false;
  private Vector            QueryParams             = new Vector();
  private int               UpdateRowCount          = 0;
  
  public CustomQueryDataBean( )
  {
  }
  
  protected String convertQueryParams( String baseQuery )
  {
    int                   beginOffset       = 0;
    CustomQueryParam      cqParam           = null;
    int                   endOffset         = 0;
    Field                 formField         = null;
    boolean               ignoreColons      = false;
    int                   offset            = 0;
    int                   paramIndex        = 0;
    HashMap               paramMap          = new HashMap();
    StringBuffer          paramName         = new StringBuffer();
    StringBuffer          sqlText           = new StringBuffer(baseQuery);
    StringBuffer          tempBuffer        = new StringBuffer();
  
    paramMap.put(":ReportNode", Long.toString( getReportHierarchyNode() ) );
    paramMap.put(":UserNode", Long.toString( getUser().getHierarchyNode() ) );
    paramMap.put(":UserAppType", Integer.toString( getUser().getAppType() ) );
    
    for( beginOffset = 0; beginOffset < baseQuery.length(); ++beginOffset )
    {
      switch( baseQuery.charAt( beginOffset ) )
      {
        case '\'':        // string begin/end indicator
          ignoreColons = !ignoreColons;
          break;
          
        case ':':         // param indicator
          if ( ignoreColons == false )
          {
            // reset the param name and 
            // add the leading ':' character
            paramName.setLength(0);
            paramName.append(':');
            
            endOffset = -1;
            offset    = (beginOffset + 1);
            while( (offset < baseQuery.length()) && (endOffset == -1) )
            {
              for( int termCharIndex = 0; termCharIndex < TerminationChars.length; ++termCharIndex )
              {
                if ( baseQuery.charAt(offset) == TerminationChars[termCharIndex] )
                {
                  endOffset = offset;
                  break;
                }
              }
              
              // still searching for the end, save char
              if ( endOffset == -1 )    
              {
                paramName.append(baseQuery.charAt(offset));
              }                
              ++offset;
            }
            if ( endOffset == -1 )
            {
              endOffset = baseQuery.length();
            }
            
            if ( ! paramMap.containsKey(paramName.toString()) )
            {
              // params all have generic names ranging from param0..paramN
              cqParam = (CustomQueryParam)QueryParams.elementAt(paramIndex);
              formField = cqParam.HtmlField;
      
              if( formField instanceof DateField )
              {
                // dates require special treatment when performing text substitution in SQL
                tempBuffer.setLength(0);
                tempBuffer.append("to_date('");
                tempBuffer.append(((DateField)formField).getSqlDateString());
                tempBuffer.append("','dd-mon-yyyy')");
              }
              else    // number and string fields
              {
                tempBuffer.setLength(0);
                
                if(formField.getData().equals(""))
                {
                  tempBuffer.append("null");
                }
                else
                {
                  tempBuffer.append( formField.getData() );
            
                  // add quotes to all strings except SQL list params
                  switch( cqParam.ParamType )
                  {
                    case mesConstants.PT_NUMBER:
                    case mesConstants.PT_HIERARCHY_NODE:
                    case mesConstants.PT_CURRENCY:
                    case mesConstants.PT_SQL_LIST:
                      break;
                    
                    default:
                      tempBuffer.insert(0,"'");
                      tempBuffer.append("'");
                      break;
                  }
                }
              }
            
              paramMap.put( paramName.toString(), tempBuffer.toString() );
              ++paramIndex;
            }     // end if ( ! paramMap.containsKey(...) )               
          }
          break;
      }
    }
    return( replaceKeywords( baseQuery, paramMap ) );
  }
  
  public void encodeDownloadUrl( StringBuffer buf )
  {
    Field         field     = null;
    
    encodeOrgUrl( buf );
    buf.append("&queryId=");
    buf.append(QueryId);
    buf.append("&deliverCompressed=");
    buf.append(Compressed ? "y" : "n");
    buf.append("&reportSQLJBean=");
    buf.append(this.getClass().getName());
    
    // add the query params
    for( int i = 0; i < QueryParams.size(); ++i )
    {
      field = ((CustomQueryParam)QueryParams.elementAt(i)).HtmlField;
      buf.append("&param");
      buf.append(i);
      buf.append("=");
      buf.append(field.getData());
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    for( int i = 0; i < ColumnHeaderNames.size(); ++i )
    {
      if ( i > 0 )
      {
        line.append(",");
      }
      line.append("\"");
      line.append( (String)ColumnHeaderNames.elementAt(i) );
      line.append("\"");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    Vector          contents  = ((RowData)obj).Contents;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    for( int i = 0; i < contents.size(); ++i )
    {
      if ( i > 0 )
      {
        line.append(",");
      }
      line.append( (String)contents.elementAt(i) );
    }
  }
  
  protected void extractColumnHeaderNames( ResultSet resultSet )
  {
    ResultSetMetaData       metaData      = null;
    
    try
    {
      metaData = resultSet.getMetaData();
      
      for (int i = 1; i <= metaData.getColumnCount(); ++i)
      {
        ColumnHeaderNames.addElement( metaData.getColumnName(i) );
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("extractColumnHeaderNames()",e.toString());
    }
  }
  
  public String getDownloadFilenameBase()
  {
    String        retVal      = "error";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:395^7*/

//  ************************************************************
//  #sql [Ctx] { select  cq.download_filename      
//          from    custom_queries    cq
//          where   cq.id = :QueryId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cq.download_filename       \n        from    custom_queries    cq\n        where   cq.id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CustomQueryDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,QueryId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:400^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("getDownloadFilenameBase()",e.toString());
    }      
    
    return ( retVal );
  }
  
  public int getQueryId()
  {
    return( QueryId );
  }
  public String getDeliverCompressed()
  {
    return( Compressed ? "y" : "n" );
  }
  
  public String getQueryName( )
  {
    String        retVal      = "";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:425^7*/

//  ************************************************************
//  #sql [Ctx] { select  cq.description      
//          from    custom_queries    cq
//          where   cq.id = :QueryId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cq.description       \n        from    custom_queries    cq\n        where   cq.id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.CustomQueryDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,QueryId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:430^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("getDisplayName()",e.toString());
    }      
    
    return ( retVal );
  }
  
  public Vector getQueryParams( )
  {
    return( QueryParams );
  }
  
  public void initQueryParams( )
  {
    Field               field         = null;
    String              fieldName     = null;
    int                 fieldType     = 0;
    ResultSetIterator   it            = null;
    String              label         = null;
    int                 maxLen        = 0;
    int                 paramIndex    = 0;
    ResultSet           resultSet     = null;
    
    try
    { 
      QueryParams.removeAllElements();
      
      /*@lineinfo:generated-code*//*@lineinfo:460^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cqp.label                   as label,
//                  cqp.type                    as type,
//                  cqp.maxlength               as maxlength,
//                  cqp.decimal_count           as decimal_count,
//                  cqp.drop_down_data_class    as drop_down_class,
//                  cqp.lookup_table_name       as lookup_table,
//                  cqp.lookup_column_name      as lookup_col,
//                  cqp.lookup_desc_column_name as lookup_desc_col,
//                  cqp.description             as description
//          from    custom_query_params   cqp
//          where   cqp.id = :QueryId
//          order by cqp.display_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cqp.label                   as label,\n                cqp.type                    as type,\n                cqp.maxlength               as maxlength,\n                cqp.decimal_count           as decimal_count,\n                cqp.drop_down_data_class    as drop_down_class,\n                cqp.lookup_table_name       as lookup_table,\n                cqp.lookup_column_name      as lookup_col,\n                cqp.lookup_desc_column_name as lookup_desc_col,\n                cqp.description             as description\n        from    custom_query_params   cqp\n        where   cqp.id =  :1 \n        order by cqp.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.CustomQueryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueryId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.CustomQueryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:474^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        fieldName = ("param" + paramIndex);
        label     = resultSet.getString("label");
        maxLen    = resultSet.getInt("maxlength");
        fieldType = resultSet.getInt("type");
        
        switch( fieldType )
        {
          case mesConstants.PT_SQL_LIST:
            maxLen = 1024;    // allow 1K lists
            // fall through, treat like string
            
          case mesConstants.PT_STRING:
            field = new Field(fieldName,maxLen,25,false);
            break;
            
          case mesConstants.PT_OPTIONAL_STRING:
            field = new Field(fieldName,maxLen,25,true);
            break;
            
          case mesConstants.PT_NUMBER:
            field = new NumberField(fieldName,maxLen,maxLen,false,resultSet.getInt("decimal_count"));
            break;
            
          case mesConstants.PT_DATE:
            field = new DateField(fieldName,false);
            ((DateField)field).setDateFormat("MM/dd/yyyy");
            break;
            
          case mesConstants.PT_CURRENCY:
            field = new CurrencyField(fieldName,10,10,false);
            break;
            
          case mesConstants.PT_HIERARCHY_NODE:
            field = new HierarchyNodeField(Ctx, getReportHierarchyNodeDefault(), fieldName,false);
            break;
            
          case mesConstants.PT_DROP_DOWN_TABLE:
          {
            String      className = null;

            if ( (className = resultSet.getString("drop_down_class")) == null )
            {
              field = new DropDownField( fieldName, label, new EditorDropDownTable(resultSet), false );
            }
            else
            {
              DropDownTable table = (DropDownTable)(Class.forName(className).newInstance());
              table.initialize(new Object[] {getReportHierarchyNodeDefault()});
              field = new DropDownField( fieldName, label, table, false );
            }              
            break;
          }            
          
          default:
            continue;
        }
        field.addHtmlExtra("class=\"formFields\"");
        QueryParams.addElement( new CustomQueryParam( fieldType, label, field ) );
        
        ++paramIndex;
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("initQueryParams()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }
  
  public int getUpdateRowCount()
  {
    return( UpdateRowCount );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public boolean isInsertUpdateQuery( )
  {
    return( isInsertUpdateQuery(null) );
  }
  
  public boolean isInsertUpdateQuery( String temp )
  {
    StringBuffer          buf         = new StringBuffer();
    char                  ch;
    boolean               retVal      = false;

    try
    {
      if ( temp == null )
      {
        /*@lineinfo:generated-code*//*@lineinfo:585^9*/

//  ************************************************************
//  #sql [Ctx] { select  cq.text  
//            from    custom_queries      cq
//            where   cq.id = :QueryId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cq.text   \n          from    custom_queries      cq\n          where   cq.id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.CustomQueryDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,QueryId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   temp = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:590^9*/
      }        
    
      // check to see what type of query this
      // is by reading the first word from the query
      for( int i = 0; i < temp.length(); ++i )
      {
        ch = temp.charAt(i);
      
        if ( buf.length() == 0 ) 
        {
          if ( isWhiteSpace(ch) )
          {
            continue;
          }
        }
        if ( isWhiteSpace(ch) )
        {
          break;
        }
        buf.append(ch);
      }
      if ( buf.toString().equalsIgnoreCase("insert") ||
           buf.toString().equalsIgnoreCase("update") )
      {
        retVal = true;
      }
    }
    catch(Exception e)
    {
      logEntry( "isInsertUpdateQuery()", e.toString() );
    }
    return( retVal );
  }
  
  public boolean isStoredProcedure()
  {
    return isStoredProcedure(null);
  }
  public boolean isStoredProcedure(String temp)
  {
    StringBuffer          buf         = new StringBuffer();
    char                  ch;
    boolean               retVal      = false;

    try
    {
      if ( temp == null )
      {
        /*@lineinfo:generated-code*//*@lineinfo:639^9*/

//  ************************************************************
//  #sql [Ctx] { select  cq.text  
//            from    custom_queries      cq
//            where   cq.id = :QueryId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cq.text   \n          from    custom_queries      cq\n          where   cq.id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.CustomQueryDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,QueryId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   temp = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:644^9*/
      }        
    
      // check to see what type of query this
      // is by reading the first word from the query
      for( int i = 0; i < temp.length(); ++i )
      {
        ch = temp.charAt(i);
      
        if ( buf.length() == 0 ) 
        {
          if ( isWhiteSpace(ch) )
          {
            continue;
          }
        }
        if ( isWhiteSpace(ch) )
        {
          break;
        }
        buf.append(ch);
      }
      if ( buf.toString().equalsIgnoreCase("call") )
      {
        retVal = true;
      }
    }
    catch(Exception e)
    {
      logEntry("isStoredProdedure(" + temp + ")", e.toString());
    }
    return( retVal );
  }
  
  public boolean isValid( )
  {
    Field       field     = null;
    boolean     retVal    = true;
    
    for( int i = 0; i < QueryParams.size(); ++i )
    {
      field = ((CustomQueryParam)QueryParams.elementAt(i)).HtmlField;
      
      if( field.isValid() != true )
      {
        retVal = false;
        break;
      }
    }
    return( retVal );
  }
  
  protected boolean isWhiteSpace( char ch )
  {
    boolean   retVal      = false;
    
    switch( ch )
    {
      case '\n':
      case ' ':
      case '\t':
      case '\r':
        retVal = true;
        break;
    }
    return( retVal );
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    Blob                  blobData          = null;
    int                   numTokens         = 0;
    PreparedStatement     preparedStatement = null;
    ResultSet             resultSet         = null;
    String                rights            = null;
    int                   rowCount          = 0;
    int                   rowCountMax       = 0;
    String                temp              = null;
    
    try
    {
      ReportRows.removeAllElements();  
      ColumnHeaderNames.removeAllElements();
      
      /*@lineinfo:generated-code*//*@lineinfo:728^7*/

//  ************************************************************
//  #sql [Ctx] { select  cq.required_right, cq.text, 
//                  nvl(row_count_limit,0), cqs.sql_data
//          
//          from    custom_queries            cq,
//                  custom_query_sql_blobs    cqs
//          where   cq.id = :QueryId and
//                  cqs.id(+) = cq.id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cq.required_right, cq.text, \n                nvl(row_count_limit,0), cqs.sql_data\n         \n        from    custom_queries            cq,\n                custom_query_sql_blobs    cqs\n        where   cq.id =  :1  and\n                cqs.id(+) = cq.id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.CustomQueryDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,QueryId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rights = (String)__sJT_rs.getString(1);
   temp = (String)__sJT_rs.getString(2);
   rowCountMax = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   blobData = (java.sql.Blob)__sJT_rs.getBlob(4);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:737^7*/
      
      // only allow the query to run if the user has the 
      // necessary rights to run this particular query.
      if ( userHasRights( rights ) )
      {
        if ( blobData != null )
        {
          // use the blob ref to read the large sql query into a buffer
          int           chunkSize   = 0;
          int           dataLen     = (int)blobData.length();
          int           dataRead    = 0;
          InputStream   is          = blobData.getBinaryStream();
          byte[]        sqlBytes    = new byte[dataLen];
          
          while (chunkSize != -1)
          {
            chunkSize = is.read(sqlBytes, dataRead, (dataLen - dataRead) );
            dataRead += chunkSize;
          }
          is.close();
          
          temp = new String(sqlBytes);
        }
        
        //@System.out.println("---CUSTOM QUERY---");
        //@System.out.println(convertQueryParams(temp));
        //@System.out.println("---CUSTOM QUERY---");
        
        preparedStatement = getPreparedStatement( convertQueryParams(temp) );
      
        if ( isInsertUpdateQuery(temp) )
        {
          UpdateRowCount = preparedStatement.executeUpdate();
        }
        else if( isStoredProcedure(temp) )
        {
          UpdateRowCount = preparedStatement.executeUpdate();
        }
        else   // select statement
        {
          resultSet = preparedStatement.executeQuery();
    
          while( resultSet.next() )
          {
            // first time, store all the column header
            // names to use when building the first line
            // of the CSV file.
            if ( ColumnHeaderNames.size() == 0 )
            {
              extractColumnHeaderNames( resultSet );
            }
            ReportRows.addElement( new RowData( resultSet ) );
            
            
            if ( (++rowCount >= rowCountMax) && (rowCountMax != 0) )
            {
              break;    // limit the number of rows
            }
          }
          resultSet.close();
        }          
        preparedStatement.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:801^9*/

//  ************************************************************
//  #sql [Ctx] { update custom_queries
//            set access_count = ( nvl(access_count,0) + 1 ),
//                last_access_login = :ReportUserBean.getLoginName(),
//                last_access_date = sysdate
//            where id = :QueryId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1114 = ReportUserBean.getLoginName();
   String theSqlTS = "update custom_queries\n          set access_count = ( nvl(access_count,0) + 1 ),\n              last_access_login =  :1 ,\n              last_access_date = sysdate\n          where id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.reports.CustomQueryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1114);
   __sJT_st.setInt(2,QueryId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:808^9*/
      }        
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ resultSet.close(); } catch( Exception e ) { }
      try{ preparedStatement.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadQueryList( )
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    long                rightId       = 0L;
    StringTokenizer     tokenizer     = null;
    
    try
    { 
      ReportRows.removeAllElements();
      
      /*@lineinfo:generated-code*//*@lineinfo:833^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cq.id                 as query_id,
//                  cq.required_right     as rights,
//                  cq.description        as query_desc
//          from    custom_queries    cq
//          order by cq.description
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cq.id                 as query_id,\n                cq.required_right     as rights,\n                cq.description        as query_desc\n        from    custom_queries    cq\n        order by cq.description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.CustomQueryDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.CustomQueryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:840^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        tokenizer = new StringTokenizer( resultSet.getString("rights"), "," );
    
        while( tokenizer.hasMoreTokens() )
        {
          try
          {
            rightId = Long.parseLong(tokenizer.nextToken());
          }
          catch(Exception ee)
          {
            continue;
          }
        
          if ( ReportUserBean.hasRight( rightId ) == true )
          {
            ReportRows.addElement( new CustomQuery( resultSet ) );
            break;
          }          
        }
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }
  
  protected String replaceKeywords( String baseQuery, HashMap keywordList )
  {
    int           offsetBegin       = 0;
    int           offsetEnd         = 0;
    String        keyword           = null;
    StringBuffer  sqlText           = new StringBuffer(baseQuery);
    
    for ( Iterator it = keywordList.keySet().iterator(); it.hasNext(); )
    {
      keyword = (String)it.next();
      
      while ( ( offsetBegin = sqlText.toString().indexOf(keyword) ) != -1 )
      {
        offsetEnd = (offsetBegin + keyword.length());
        sqlText.replace(offsetBegin,offsetEnd,(String)keywordList.get(keyword));
      }
    }      
    return( sqlText.toString() );
  }
  
  public void setProperties(HttpServletRequest request)
  {
    Field       field         = null;
    String      name          = null;
    
    // load the default report properties
    super.setProperties( request );
    
    // extract the id for the custom query
    QueryId = HttpHelper.getInt(request,"queryId",QID_NONE);
    
    Compressed = HttpHelper.getBoolean(request, "deliverCompressed", false);
  
    // initialize and form fields for parameter entry
    // this must be done in setProperties after the QueryId
    // has been loaded because the params depend on which
    // query has been requested by the user.
    initQueryParams();  
    
    // apply request parameters to the fields
    for (Enumeration names = request.getParameterNames(); 
         names.hasMoreElements(); )
    {
      name  = (String)names.nextElement();
    
      // if the edit field exists, store the value
      for( int i = 0; i < QueryParams.size(); ++i )
      {
        field = ((CustomQueryParam)QueryParams.elementAt(i)).HtmlField;
        
        if ( field.getName().equals(name) )
        {
          field.setData(request.getParameter(name));
        }
      }
    }      
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
  
  protected boolean userHasRights( String rights )
  {
    long                  rightId   = 0L;
    StringTokenizer       tokenizer = null;
    boolean               validUser = false;
    
    tokenizer = new StringTokenizer( rights, "," );
  
    while( tokenizer.hasMoreTokens() )
    {
      try
      {
        rightId = Long.parseLong(tokenizer.nextToken());
      }
      catch(Exception ee)
      {
        continue;
      }
      if ( ( validUser = ReportUserBean.hasRight(rightId) ) == true )
      {
        break;
      }          
    }
    return( validUser );
  }
  
  public boolean encodeData( int fileFormat, HttpServletResponse response )
    throws java.io.IOException
  {
    boolean retVal = true;
    
    try
    {
      if( Compressed == true )
      {
        StringBuffer            line        = null;
        ServletOutputStream     BrowserOut  = response.getOutputStream();
        
        String zippedFileName = getDownloadFilenameBase() + getDownloadFilenameExtension(fileFormat);
        
        
        ZipOutputStream out = new ZipOutputStream(BrowserOut);
        ((ZipOutputStream)out).setMethod(ZipOutputStream.DEFLATED);
        ZipEntry  ZipFileEntry = new ZipEntry(zippedFileName);
        
        ((ZipOutputStream)out).putNextEntry(ZipFileEntry);
        
        // mark outgoing file as type zip
        response.setContentType("application/zip");
        response.setHeader("Content-Disposition", ( "attachment; filename=" + getDownloadFilenameBase() + ".zip" ) );

        for ( int i = 0; i < ReportRows.size(); ++i )
        {
          if ( line == null )
          {
            // initialize the line buffer
            line = new StringBuffer("");
            
            // first time, output header
            encodeHeaderCSV( line );
            out.write( line.toString().getBytes() );
            out.write( (new String("\n")).getBytes() );
          } 
          encodeLineCSV( ReportRows.elementAt(i), line );
          if ( line.length() > 0 )
          {
            out.write( line.toString().getBytes() );
            out.write( (new String("\n")).getBytes() );
          }              
        }
        
        if ( line != null )
        {
          encodeTrailerCSV( line );
          if ( line.length() > 0 )
          {
            out.write( line.toString().getBytes() );
            out.write( (new String("\n")).getBytes() );
          }              
        }
        
        // clean up resources
        ((ZipOutputStream)out).closeEntry();
        ((ZipOutputStream)out).finish();
      }
      else
      {
        // just do normal encoding
        retVal = super.encodeData( fileFormat, response );
      }
    }
    catch(Exception e)
    {
      logEntry( "encodeData()",e.toString() );
      addError( "encodeData(): " + e.toString() );
      retVal = false;
    }
    
    return( retVal );
  }
}/*@lineinfo:generated-code*/