/*@lineinfo:filename=MbsAssocReportDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/reports/MbsAssocReportDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2013-03-13 14:11:55 -0700 (Wed, 13 Mar 2013) $
  Version            : $Revision: 20985 $

  Change History:
     See SVN database

  Copyright (C) 2000-2012,2013 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ComboDateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;

public class MbsAssocReportDataBean extends ReportSQLJBean
{
  public class CardTypeDropDownTable extends DropDownTable
  {
    public CardTypeDropDownTable( )
    {
      addElement("VS" ,"Visa");
      addElement("MC" ,"MasterCard");
    }
  }

  public MbsAssocReportDataBean( )
  {
    super(true);   // enable field bean support
  }
  
  protected void createFields(HttpServletRequest request)
  {
    DropDownField   dropDownField     = null;
    
    try
    {
      super.createFields(request);
    
      FieldGroup fgroup = (FieldGroup)getField("searchFields");
      fgroup.deleteField("endDate");
      fgroup.deleteField("portfolioId");
      fgroup.deleteField("zip2Node");
      
      fgroup.add(new DropDownField( "bankNumber","Bank Number", new MbsBankNumberDropDownTable(getReportHierarchyNode()), true ));
      fgroup.add(new DropDownField( "cardType","Card Type", new CardTypeDropDownTable(), true ));
      fgroup.add(new Field( "reportId","Report ID", 32, 20, false ));
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
  }
  
  protected void encodeLineFixed( Object obj, StringBuffer line )
  {
    line.setLength(0);
    line.append((String)obj);
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer    filename        = new StringBuffer("");
    Date            reportDate      = getReportDateBegin();
    
    filename.append( getData("bankNumber") );
    filename.append( "_" );
    filename.append( getData("reportId") );
    filename.append( "_" );
    filename.append( DateTimeFormatter.getFormattedDate(reportDate,"MMddyy") );
    
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_FIXED:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    int                 bankNumber    = getInt("bankNumber");
    Blob                blobData      = null;
    byte[]              buffer        = null;
    String              cardType      = getData("cardType");
    String              reportId      = getData("reportId");
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:126^7*/

//  ************************************************************
//  #sql [Ctx] { select  car.report_text
//          
//          from    card_association_reports  car
//          where   car.bank_number = :bankNumber
//                  and car.settlement_date = :beginDate
//                  and car.card_type = :cardType
//                  and car.report_id = upper(:reportId)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  car.report_text\n         \n        from    card_association_reports  car\n        where   car.bank_number =  :1 \n                and car.settlement_date =  :2 \n                and car.card_type =  :3 \n                and car.report_id = upper( :4 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MbsAssocReportDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setString(3,cardType);
   __sJT_st.setString(4,reportId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blobData = (java.sql.Blob)__sJT_rs.getBlob(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:135^7*/
      
      // use the blob ref to read the statement data into internal storage
      int dataLen = (int)blobData.length();
      int chunkSize = 0;
      int dataRead = 0;
      InputStream is = blobData.getBinaryStream();
      buffer = new byte[dataLen];
      while (chunkSize != -1)
      {
        chunkSize = is.read(buffer,dataRead,(dataLen - dataRead));
        dataRead += chunkSize;
      }
      is.close();
      ReportRows.addElement(new String(buffer));      
    }
    catch( java.sql.SQLException sqle )
    {
      // ignore, no data found
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",bankNumber,beginDate,beginDate), e.toString() );
    }
    finally
    {
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest( request );
 
    if ( HttpHelper.getString(request,"beginDate.month",null) == null )
    {
      Calendar cal = Calendar.getInstance();
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      ((ComboDateField)getField("beginDate")).setUtilDate( cal.getTime() );
    }
  }
}/*@lineinfo:generated-code*/