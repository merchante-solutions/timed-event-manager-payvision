/*@lineinfo:filename=AcctBorrowBaseDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AcctBorrowBaseDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 2/12/03 3:24p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class AcctBorrowBaseDataBean extends ReportSQLJBean
{
  public class RowData implements Comparable
  {
    public    double                CreditsAmount     = 0.0;
    public    int                   CreditsCount      = 0;
    public    double                EstDiscInc        = 0.0;
    public    double                EstIcInc          = 0.0;
    public    double                EstFeesInc        = 0.0;
    public    long                  NodeId            = 0L;
    public    long                  OrgId             = 0L;
    public    String                OrgName           = null;
    public    double                SalesAmount       = 0.0;
    public    int                   SalesCount        = 0;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      CreditsAmount     = resultSet.getDouble("credits_amount");
      CreditsCount      = resultSet.getInt("credits_count");
      EstDiscInc        = resultSet.getDouble("disc_inc_est");
      NodeId            = resultSet.getLong("child_node");
      OrgId             = resultSet.getLong("child_org_num");
      OrgName           = processString(resultSet.getString("child_name"));
      SalesAmount       = resultSet.getDouble("sales_amount");
      SalesCount        = resultSet.getInt("sales_count");
    }
    
    public void addMonthEndEstRevenue( ResultSet resultSet )
      throws java.sql.SQLException
    {
      double            dayCount        = resultSet.getDouble("day_count");
      
      EstIcInc    = MesMath.round((resultSet.getDouble("ic_inc_est")/dayCount),2);
      EstFeesInc  = MesMath.round((resultSet.getDouble("fees_inc_est")/dayCount),2);
    }      
    
    public int compareTo( Object obj )
    {
      RowData       compareRow      = (RowData)obj;
      int           retVal          = 0;
      
      if ( ( retVal = OrgName.compareTo(compareRow.OrgName) ) == 0 )
      {
        if ( ( retVal = (int)( NodeId - compareRow.NodeId ) ) == 0 )
        {
          retVal = (int)(OrgId - compareRow.OrgId);
        }
      }
      return( retVal );
    }
    
    public double getBorrowingBaseAmount()
    {
      double       retVal        = 0;
      
      retVal = (((EstDiscInc + EstIcInc + EstFeesInc) * 4 ) / 5 );
      
      return( (retVal < 0.0) ? 0.0 : retVal );
    }
  }
  
  public AcctBorrowBaseDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Sales Count\",");
    line.append("\"Sales Amount\",");
    line.append("\"Credits Count\",");
    line.append("\"Credits Amount\",");
    line.append("\"Est Disc Inc\",");
    line.append("\"Est IC Inc\",");
    line.append("\"Est Fees Inc\",");
    line.append("\"Est Total Inc\",");
    line.append("\"Borrow Base\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( encodeHierarchyNode(record.NodeId) );
    line.append( ",\"" );
    line.append( record.OrgName );
    line.append( "\"," );
    line.append( record.SalesCount );
    line.append( "," );
    line.append( record.SalesAmount );
    line.append( "," );
    line.append( record.CreditsCount );
    line.append( "," );
    line.append( record.CreditsAmount );
    line.append( "," );
    line.append( record.EstDiscInc );
    line.append( "," );
    line.append( record.EstIcInc );
    line.append( "," );
    line.append( record.EstFeesInc );
    line.append( "," );
    line.append( record.EstDiscInc + record.EstIcInc + record.EstFeesInc );
    line.append( "," );
    line.append( record.getBorrowingBaseAmount() );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_borrowing_base_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    long                          childId           = 0L;
    int                           i;
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    RowData                       row               = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:203^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                      index (po idx_parent_org_num)
//                      index (gm pkgroup_merchant)
//                   */
//                  o.org_num                           as child_org_num,
//                  o.org_group                         as child_node,
//                  o.org_name                          as child_name,
//                  sum( nvl(sm.visa_sales_count,0) +
//                       nvl(sm.mc_sales_count,0) )    as sales_count,
//                  sum( nvl(sm.visa_sales_amount,0) +
//                       nvl(sm.mc_sales_amount,0) )   as sales_amount,
//                  sum( nvl(sm.visa_credits_count,0) +
//                       nvl(sm.mc_credits_count,0) )  as credits_count,
//                  sum( nvl(sm.visa_credits_amount,0) +
//                       nvl(sm.mc_credits_amount,0) ) as credits_amount,
//                  round( sum( ( ( nvl(sm.VISA_SALES_AMOUNT,0) * mf.VISA_DISC_RATE * 0.00001 ) +
//                                ( nvl(sm.mc_sales_amount,0) * mf.MASTCD_DISC_RATE * 0.00001 ) +
//                                ( nvl(sm.visa_sales_count,0) * mf.VISA_PER_ITEM * 0.001 ) +
//                                ( nvl(sm.mc_sales_count,0) * mf.mastcd_per_item * 0.001 ) ) ), 
//                         2 )                        as disc_inc_est
//          from    parent_org                    po,
//                  organization                  o,
//                  group_merchant                gm,
//                  mif                           mf,
//                  groups                        g,                                
//                  daily_detail_file_summary     sm,
//                  merchant                      mr
//          where   po.parent_org_num = :orgId and
//                  o.org_num = po.org_num and
//                  gm.org_num = o.org_num and
//                  mf.merchant_number = gm.merchant_number and
//                  mr.merch_number(+) = mf.merchant_number and
//                  nvl(mr.merch_dly_discount_flag,'N') = 'N' and                
//                  g.assoc_number = (mf.bank_number || mf.dmagent) and
//                  not exists ( select le.hierarchy_node 
//                               from   daily_detail_ic_liab_exempt le
//                               where  le.hierarchy_node in 
//                                      ( g.group_1, g.group_2, g.group_3,
//                                        g.group_4, g.group_5, g.group_6,
//                                        g.assoc_number ) ) and                
//                  sm.merchant_number = mf.merchant_number and
//                  sm.batch_date between :beginDate and :endDate
//          group by o.org_num, o.org_group, o.org_name      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                    index (po idx_parent_org_num)\n                    index (gm pkgroup_merchant)\n                 */\n                o.org_num                           as child_org_num,\n                o.org_group                         as child_node,\n                o.org_name                          as child_name,\n                sum( nvl(sm.visa_sales_count,0) +\n                     nvl(sm.mc_sales_count,0) )    as sales_count,\n                sum( nvl(sm.visa_sales_amount,0) +\n                     nvl(sm.mc_sales_amount,0) )   as sales_amount,\n                sum( nvl(sm.visa_credits_count,0) +\n                     nvl(sm.mc_credits_count,0) )  as credits_count,\n                sum( nvl(sm.visa_credits_amount,0) +\n                     nvl(sm.mc_credits_amount,0) ) as credits_amount,\n                round( sum( ( ( nvl(sm.VISA_SALES_AMOUNT,0) * mf.VISA_DISC_RATE * 0.00001 ) +\n                              ( nvl(sm.mc_sales_amount,0) * mf.MASTCD_DISC_RATE * 0.00001 ) +\n                              ( nvl(sm.visa_sales_count,0) * mf.VISA_PER_ITEM * 0.001 ) +\n                              ( nvl(sm.mc_sales_count,0) * mf.mastcd_per_item * 0.001 ) ) ), \n                       2 )                        as disc_inc_est\n        from    parent_org                    po,\n                organization                  o,\n                group_merchant                gm,\n                mif                           mf,\n                groups                        g,                                \n                daily_detail_file_summary     sm,\n                merchant                      mr\n        where   po.parent_org_num =  :1  and\n                o.org_num = po.org_num and\n                gm.org_num = o.org_num and\n                mf.merchant_number = gm.merchant_number and\n                mr.merch_number(+) = mf.merchant_number and\n                nvl(mr.merch_dly_discount_flag,'N') = 'N' and                \n                g.assoc_number = (mf.bank_number || mf.dmagent) and\n                not exists ( select le.hierarchy_node \n                             from   daily_detail_ic_liab_exempt le\n                             where  le.hierarchy_node in \n                                    ( g.group_1, g.group_2, g.group_3,\n                                      g.group_4, g.group_5, g.group_6,\n                                      g.assoc_number ) ) and                \n                sm.merchant_number = mf.merchant_number and\n                sm.batch_date between  :2  and  :3 \n        group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AcctBorrowBaseDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AcctBorrowBaseDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:248^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      resultSet.close();
      it.close();
      
      // setup report rows for children that had no data 
      // in the original query.
      /*@lineinfo:generated-code*//*@lineinfo:260^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group         as child_node,
//                  o.org_name          as child_name,
//                  o.org_num           as child_org_num,
//                  0                   as sales_count,
//                  0                   as sales_amount,
//                  0                   as credits_count,
//                  0                   as credits_amount,
//                  0                   as disc_inc_est
//          from    parent_org              po,
//                  organization            o
//          where   po.parent_org_num = :orgId and
//                  o.org_num = po.org_num                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group         as child_node,\n                o.org_name          as child_name,\n                o.org_num           as child_org_num,\n                0                   as sales_count,\n                0                   as sales_amount,\n                0                   as credits_count,\n                0                   as credits_amount,\n                0                   as disc_inc_est\n        from    parent_org              po,\n                organization            o\n        where   po.parent_org_num =  :1  and\n                o.org_num = po.org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AcctBorrowBaseDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AcctBorrowBaseDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:274^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        childId = resultSet.getLong("child_org_num");
      
        for( i = 0; i < ReportRows.size(); ++i )
        {
          row = (RowData) ReportRows.elementAt(i);
          if ( row.OrgId == childId )
          {
            break;
          }
        }
        if ( i >= ReportRows.size() )
        {
          ReportRows.addElement( new RowData( resultSet ) );
        }
      }
      resultSet.close();
      it.close();
      
      // add the necessary projections using last m/e data
      /*@lineinfo:generated-code*//*@lineinfo:298^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                      index (po idx_parent_org_num)
//                      index (gm pkgroup_merchant)
//                      index (sm pk_monthly_extract_summary)
//                   */
//                  o.org_num                                     as child_org_num,
//                  avg( (sm.month_end_date - sm.month_begin_date) )/
//                       (:endDate - :beginDate)                  as day_count,
//                  sum( nvl(sm.tot_inc_interchange,0) )          as ic_inc_est,
//                  sum( nvl(sm.tot_inc_authorization,0) +
//                       nvl(sm.tot_inc_capture,0) +
//                       nvl(sm.tot_inc_debit,0) +
//                       nvl(sm.tot_inc_ind_plans,0) +
//                       nvl(sm.tot_inc_sys_generated,0) +
//                       nvl(sm.tot_ndr_liability,0) +
//                       nvl(sm.tot_authorization_liability,0) +
//                       nvl(sm.tot_capture_liability,0) +
//                       nvl(sm.tot_debit_liability,0) +
//                       nvl(sm.tot_ind_plans_liability,0) +
//                       nvl(sm.tot_sys_generated_liability,0) +
//                       nvl(sm.tot_equip_rental_liability,0) +
//                       nvl(sm.tot_equip_sales_liability,0) )    as fees_inc_est        
//          from    parent_org                    po,
//                  organization                  o,
//                  group_merchant                gm,
//                  mif                           mf,
//                  groups                        g,
//                  monthly_extract_summary       sm
//          where   po.parent_org_num = :orgId and
//                  o.org_num = po.org_num and
//                  gm.org_num = o.org_num and
//                  mf.merchant_number = gm.merchant_number and
//                  g.assoc_number = (mf.bank_number || mf.dmagent) and
//                  not exists ( select le.hierarchy_node 
//                               from   daily_detail_ic_liab_exempt le
//                               where  le.hierarchy_node in 
//                                      ( g.group_1, g.group_2, g.group_3,
//                                        g.group_4, g.group_5, g.group_6,
//                                        g.assoc_number ) ) and                
//                  sm.merchant_number = mf.merchant_number and
//                  sm.active_date = trunc( trunc(:beginDate,'month')-1, 'month' )
//          group by o.org_num                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                    index (po idx_parent_org_num)\n                    index (gm pkgroup_merchant)\n                    index (sm pk_monthly_extract_summary)\n                 */\n                o.org_num                                     as child_org_num,\n                avg( (sm.month_end_date - sm.month_begin_date) )/\n                     ( :1  -  :2 )                  as day_count,\n                sum( nvl(sm.tot_inc_interchange,0) )          as ic_inc_est,\n                sum( nvl(sm.tot_inc_authorization,0) +\n                     nvl(sm.tot_inc_capture,0) +\n                     nvl(sm.tot_inc_debit,0) +\n                     nvl(sm.tot_inc_ind_plans,0) +\n                     nvl(sm.tot_inc_sys_generated,0) +\n                     nvl(sm.tot_ndr_liability,0) +\n                     nvl(sm.tot_authorization_liability,0) +\n                     nvl(sm.tot_capture_liability,0) +\n                     nvl(sm.tot_debit_liability,0) +\n                     nvl(sm.tot_ind_plans_liability,0) +\n                     nvl(sm.tot_sys_generated_liability,0) +\n                     nvl(sm.tot_equip_rental_liability,0) +\n                     nvl(sm.tot_equip_sales_liability,0) )    as fees_inc_est        \n        from    parent_org                    po,\n                organization                  o,\n                group_merchant                gm,\n                mif                           mf,\n                groups                        g,\n                monthly_extract_summary       sm\n        where   po.parent_org_num =  :3  and\n                o.org_num = po.org_num and\n                gm.org_num = o.org_num and\n                mf.merchant_number = gm.merchant_number and\n                g.assoc_number = (mf.bank_number || mf.dmagent) and\n                not exists ( select le.hierarchy_node \n                             from   daily_detail_ic_liab_exempt le\n                             where  le.hierarchy_node in \n                                    ( g.group_1, g.group_2, g.group_3,\n                                      g.group_4, g.group_5, g.group_6,\n                                      g.assoc_number ) ) and                \n                sm.merchant_number = mf.merchant_number and\n                sm.active_date = trunc( trunc( :4 ,'month')-1, 'month' )\n        group by o.org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.AcctBorrowBaseDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,endDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setLong(3,orgId);
   __sJT_st.setDate(4,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.AcctBorrowBaseDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:342^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        childId = resultSet.getLong("child_org_num");
        for( i = 0; i < ReportRows.size(); ++i )
        {
          row = (RowData) ReportRows.elementAt(i);
          if ( row.OrgId == childId )
          {
            row.addMonthEndEstRevenue( resultSet );
            break;
          }
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, -7 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      
      cal.setTime( ReportDateEnd );
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/