/*@lineinfo:filename=InventoryReportDownloadBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/InventoryReportDownloadBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 4/23/03 12:07p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class InventoryReportDownloadBean extends ReportSQLJBean
{

  private String              lookupValue         = "";
  private int                 client              = -1;
  private int                 deploymentType      = -1;
  private int                 condition           = -1;
  private int                 status              = -1;
  private int                 equipName           = -1;
  private int                 equipType           = -1;
  private String              equipModel          = "-1";

  public class RowData
  {
    public String  serialNum;
    public String  client;
    public String  prodType;
    public String  prodName;
    public String  prodDesc;
    public String  receiveDate;
    public String  prodCondition;
    public String  prodStatus;
    public String  cost;
    public String  deployType;
    public String  deployDate;
    public String  dbaName;
    public String  merchantNumber;
    public String  associationNumber;
    public String  deployPrice;
    public String  partNumber;


    public RowData( ResultSet resultSet ) throws java.sql.SQLException
    {
      serialNum         = processString(resultSet.getString("serial_num"));
      merchantNumber    = processString(resultSet.getString("merchant_number"));
      associationNumber = processString(resultSet.getString("association_number"));
      dbaName           = processString(resultSet.getString("dba_name"));
      client            = processString(resultSet.getString("client"));
      deployDate        = processString(DateTimeFormatter.getFormattedDate(resultSet.getTimestamp("deploy_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
      receiveDate       = processString(DateTimeFormatter.getFormattedDate(resultSet.getTimestamp("receive_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
      deployType        = processString(resultSet.getString("deploy_type"));
      prodType          = processString(resultSet.getString("prod_type"));
      prodName          = processString(resultSet.getString("prod_name"));
      prodDesc          = processString(resultSet.getString("prod_desc"));
      prodCondition     = processString(resultSet.getString("prod_condition"));
      prodStatus        = processString(resultSet.getString("prod_status"));
      cost              = processString(resultSet.getString("cost"));
      deployPrice       = processString(resultSet.getString("deploy_price"));
      partNumber        = processString(resultSet.getString("part_number"));

    }
  }

  public InventoryReportDownloadBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Serial #\",");
    line.append("\"Client\",");
    line.append("\"Equip Type\",");
    line.append("\"Mfgr Name\",");
    line.append("\"Equip Desc\",");
    line.append("\"Received Date\",");
    line.append("\"Condition\",");
    line.append("\"Status\",");
    line.append("\"Unit Cost\",");
    line.append("\"Deploy Type\",");
    line.append("\"Deploy Date\",");
    line.append("\"DBA Name\",");
    line.append("\"Merchant #\",");
    line.append("\"Assoc #\",");
    line.append("\"Deploy Price\"");
  }
  

  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( "\"" );
    line.append( record.serialNum );
    line.append( "\",\"" );
    line.append( record.client );
    line.append( "\",\"" );
    line.append( record.prodType );
    line.append( "\",\"" );
    line.append( record.prodName );
    line.append( "\",\"" );
    line.append( record.prodDesc );
    line.append( "\",\"" );
    line.append( record.receiveDate );
    line.append( "\",\"" );
    line.append( record.prodCondition );
    line.append( "\",\"" );
    line.append( record.prodStatus );
    line.append( "\",\"" );
    line.append( record.cost );
    line.append( "\",\"" );
    line.append( record.deployType );
    line.append( "\",\"" );
    line.append( record.deployDate );
    line.append( "\",\"" );
    line.append( record.dbaName );
    line.append( "\",\"" );
    line.append( record.merchantNumber );
    line.append( "\",\"" );
    line.append( record.associationNumber );
    line.append( "\",\"" );
    line.append( record.deployPrice );
    line.append( "\"" );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_inventory_report_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MMddyy") );
    
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData()
  {
    loadData( ReportUserBean );
  }

  public void loadData( UserBean user )
  {
    ResultSetIterator             it                  = null;
    ResultSet                     resultSet           = null;

    try
    {
      // empty the current contents
      ReportRows.clear();

      // set up longLookup value (numeric version of lookup value)
      long longLookup;
      try
      {
        longLookup = Long.parseLong(lookupValue.trim());
      }
      catch (Exception e) 
      {
        longLookup = -1;
      }
    
      // add wildcards to stringLookup so we can match partial strings
      String stringLookup = "";
      if(lookupValue.trim().equals(""))
      {
        stringLookup = "passall";
      }
      else
      {
        stringLookup = "%" + lookupValue.toUpperCase() + "%";
      }

      // no wildcards to stringLookup
      String stringLookup2 = "";
      if(lookupValue.trim().equals(""))
      {
        stringLookup2 = "passall";
      }
      else
      {
        stringLookup2 = lookupValue.toUpperCase();
      }
     

      /*@lineinfo:generated-code*//*@lineinfo:244^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.EI_SERIAL_NUMBER             serial_num,
//                  ei.ei_received_date             receive_date,
//                  ei.ei_deployed_date             deploy_date,
//                  ei.ei_merchant_number           merchant_number,
//                  ei.ei_lrb_price                 deploy_price,
//                  ei.ei_unit_cost                 cost,
//                  ei.ei_part_number               part_number,
//                  ec.ec_class_name                prod_condition,
//                  mf.dba_name                     dba_name,
//                  mf.dmagent                      association_number,
//                  elt.equiplendtype_description   deploy_type,
//                  es.equip_status_desc            prod_status,
//                  em.equipmfgr_mfr_name           prod_name,
//                  et.equiptype_description        prod_type,
//                  eq.equip_descriptor             prod_desc,
//                  ap.inventory_owner_name         client
//  
//          from    equip_inventory                 ei,
//                  mif                             mf,
//                  equip_status                    es,
//                  equipmfgr                       em,
//                  app_type                        ap,
//                  equipment                       eq,
//                  equiplendtype                   elt,
//                  equiptype                       et,
//                  equip_class                     ec
//  
//          where   ei.ei_merchant_number = mf.merchant_number(+)   and
//                  ei.ei_status          = es.equip_status_id      and
//                  ei.ei_lrb             = elt.equiplendtype_code  and
//                  ei.ei_owner           = ap.app_type_code        and
//                  ei.ei_class           = ec.ec_class_id          and
//                  ei.ei_part_number     = eq.equip_model          and
//                  eq.equiptype_code     = et.equiptype_code       and 
//                  eq.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE   and
//                  (-1 = :client           or ei.ei_owner = :client)               and
//                  (-1 = :condition        or ei.ei_class = :condition)            and
//                  (-1 = :status           or ei.ei_status = :status)              and
//                  (99 != :deploymentType  or ei.ei_lrb != 0)                      and
//                  (-1 = :deploymentType   or 99 = :deploymentType 
//                                          or ei.ei_lrb = :deploymentType)         and
//                  (-1 = :equipType        or eq.equiptype_code = :equipType)      and
//                  (-1 = :equipName        or eq.EQUIPMFGR_MFR_CODE = :equipName)  and
//                  ('-1' = :equipModel     or eq.equip_model = :equipModel)        and
//                  (
//                    'passall'                   = :stringLookup   or
//                    ei.ei_merchant_number       = :longLookup     or
//                    ei.EI_TRANSACTION_ID        = :longLookup     or
//                    mf.dmagent                  = :longLookup     or
//                    upper(ei.EI_SERIAL_NUMBER)  = :stringLookup2  or 
//                    upper(mf.dba_name) like :stringLookup
//                  )          
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.EI_SERIAL_NUMBER             serial_num,\n                ei.ei_received_date             receive_date,\n                ei.ei_deployed_date             deploy_date,\n                ei.ei_merchant_number           merchant_number,\n                ei.ei_lrb_price                 deploy_price,\n                ei.ei_unit_cost                 cost,\n                ei.ei_part_number               part_number,\n                ec.ec_class_name                prod_condition,\n                mf.dba_name                     dba_name,\n                mf.dmagent                      association_number,\n                elt.equiplendtype_description   deploy_type,\n                es.equip_status_desc            prod_status,\n                em.equipmfgr_mfr_name           prod_name,\n                et.equiptype_description        prod_type,\n                eq.equip_descriptor             prod_desc,\n                ap.inventory_owner_name         client\n\n        from    equip_inventory                 ei,\n                mif                             mf,\n                equip_status                    es,\n                equipmfgr                       em,\n                app_type                        ap,\n                equipment                       eq,\n                equiplendtype                   elt,\n                equiptype                       et,\n                equip_class                     ec\n\n        where   ei.ei_merchant_number = mf.merchant_number(+)   and\n                ei.ei_status          = es.equip_status_id      and\n                ei.ei_lrb             = elt.equiplendtype_code  and\n                ei.ei_owner           = ap.app_type_code        and\n                ei.ei_class           = ec.ec_class_id          and\n                ei.ei_part_number     = eq.equip_model          and\n                eq.equiptype_code     = et.equiptype_code       and \n                eq.EQUIPMFGR_MFR_CODE = em.EQUIPMFGR_MFR_CODE   and\n                (-1 =  :1            or ei.ei_owner =  :2 )               and\n                (-1 =  :3         or ei.ei_class =  :4 )            and\n                (-1 =  :5            or ei.ei_status =  :6 )              and\n                (99 !=  :7   or ei.ei_lrb != 0)                      and\n                (-1 =  :8    or 99 =  :9  \n                                        or ei.ei_lrb =  :10 )         and\n                (-1 =  :11         or eq.equiptype_code =  :12 )      and\n                (-1 =  :13         or eq.EQUIPMFGR_MFR_CODE =  :14 )  and\n                ('-1' =  :15      or eq.equip_model =  :16 )        and\n                (\n                  'passall'                   =  :17    or\n                  ei.ei_merchant_number       =  :18      or\n                  ei.EI_TRANSACTION_ID        =  :19      or\n                  mf.dmagent                  =  :20      or\n                  upper(ei.EI_SERIAL_NUMBER)  =  :21   or \n                  upper(mf.dba_name) like  :22 \n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.InventoryReportDownloadBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,client);
   __sJT_st.setInt(2,client);
   __sJT_st.setInt(3,condition);
   __sJT_st.setInt(4,condition);
   __sJT_st.setInt(5,status);
   __sJT_st.setInt(6,status);
   __sJT_st.setInt(7,deploymentType);
   __sJT_st.setInt(8,deploymentType);
   __sJT_st.setInt(9,deploymentType);
   __sJT_st.setInt(10,deploymentType);
   __sJT_st.setInt(11,equipType);
   __sJT_st.setInt(12,equipType);
   __sJT_st.setInt(13,equipName);
   __sJT_st.setInt(14,equipName);
   __sJT_st.setString(15,equipModel);
   __sJT_st.setString(16,equipModel);
   __sJT_st.setString(17,stringLookup);
   __sJT_st.setLong(18,longLookup);
   __sJT_st.setLong(19,longLookup);
   __sJT_st.setLong(20,longLookup);
   __sJT_st.setString(21,stringLookup2);
   __sJT_st.setString(22,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.InventoryReportDownloadBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:298^7*/
      
      resultSet = it.getResultSet();
    
      while(resultSet.next())
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
    
      it.close();
   
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadData", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    this.lookupValue      = HttpHelper.getString(request, "lookupValue",    "");
    this.client           = HttpHelper.getInt(request,    "client",         -1);
    this.deploymentType   = HttpHelper.getInt(request,    "deploymentType", -1);
    this.condition        = HttpHelper.getInt(request,    "condition",      -1);
    this.status           = HttpHelper.getInt(request,    "status",         -1);
    this.equipName        = HttpHelper.getInt(request,    "equipName",      -1);
    this.equipType        = HttpHelper.getInt(request,    "equipType",      -1);
    this.equipModel       = HttpHelper.getString(request, "equipModel",     "-1");
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/