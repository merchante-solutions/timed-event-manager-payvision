/*@lineinfo:filename=CertegyCheckDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CertegyCheckDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/20/03 3:47p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesHierarchy;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class CertegyCheckDataBean extends OrgSummaryDataBean
{
  public class DetailRow
  {
    public String         ChainName           = null;
    public String         ChainNumber         = null;
    public String         CheckNumber         = null;
    public String         CustNumber          = null;
    public String         MerchantDDA         = null;
    public String         PrintTime           = null;
    public String         RefNum              = null;
    public String         Region              = null;
    public Date           ReportDate          = null;
    public Date           SettlementDate      = null;
    public String         StationName         = null;
    public long           StationNumber       = 0L;
    public String         Store               = null;
    public double         TranAmount          = 0.0;
    public Date           TranDate            = null;

    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      StationNumber   = resultSet.getLong("station_number");
      StationName     = processString(resultSet.getString("station_name"));
      ChainNumber     = processString(resultSet.getString("chain_number"));
      ChainName       = processString(resultSet.getString("chain_name"));
      TranDate        = resultSet.getDate("tran_date");
      TranAmount      = resultSet.getDouble("tran_amount");
      CustNumber      = processString(resultSet.getString("cust_number"));
      CheckNumber     = processString(resultSet.getString("check_number"));
      MerchantDDA     = processString(resultSet.getString("merchant_dda"));
      PrintTime       = processString(resultSet.getString("print_time"));
      RefNum          = processString(resultSet.getString("ref_num"));
      Region          = processString(resultSet.getString("region"));
      ReportDate      = resultSet.getDate("report_date");
      SettlementDate  = resultSet.getDate("settlement_date");
      Store           = processString(resultSet.getString("store"));
    }
  }
  
  public CertegyCheckDataBean()
  {
  }
  
//@  protected void createFields(HttpServletRequest request)
//@  {
//@    FieldGroup        fgroup;
//@    Field             field;
//@    
//@    super.createFields(request);
//@    
//@    fgroup = (FieldGroup)getField("searchFields");
//@    fgroup.add( new HiddenField("hierType") );
//@    fgroup.deleteField("zip2Node");
//@  }

  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_SUMMARY )
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"Deposit Cnt\",");
      line.append("\"Deposit Amt\"");
    }
    else  // RT_DETAILS
    {
      line.append("\"Station Number\",");
      line.append("\"Station Name\",");
      line.append("\"Report Date\",");
      line.append("\"Settled Date\",");
      line.append("\"Tran Date\",");
      line.append("\"Customer Account\",");
      line.append("\"Check Number\",");
      line.append("\"Reference Number\",");
      line.append("\"Amount\"");
    }      
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    if ( ReportType == RT_SUMMARY )
    {
      MesOrgSummaryEntry  record    = (MesOrgSummaryEntry)obj;
    
      line.append( encodeHierarchyNode(record.getHierarchyNode()) );
      line.append( ",\"" );
      line.append( record.getOrgName() );
      line.append( "\"," );
      line.append( record.getCount() );
      line.append( "," );
      line.append( record.getAmount() );
    }
    else    // RT_DETAILS
    {
      DetailRow  record    = (DetailRow)obj;
    
      line.append( encodeHierarchyNode(record.StationNumber) );
      line.append( ",\"" );
      line.append( record.StationName );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate(record.ReportDate,"MM/dd/yyyy") );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate(record.SettlementDate,"MM/dd/yyyy") );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate(record.TranDate,"MM/dd/yyyy") );
      line.append( ",\"" );
      line.append( record.CustNumber );
      line.append( "\",\"" );
      line.append( record.CheckNumber );
      line.append( "\",\"" );
      line.append( record.RefNum );
      line.append( "\"," );
      line.append( record.TranAmount );
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    if ( ReportType == RT_SUMMARY )
    {
      filename.append("cert_check_summary_");
    }
    else  // RT_DETAILS
    {
      filename.append("cert_check_details_");
    }      
    
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"MMddyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"MMddyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadDetailData(long orgId, Date beginDate, Date endDate)
  {
    ResultSetIterator     it      = null;
    long                  nodeId  = 0L;
    ResultSet             rs      = null;
    
    try
    {
      if ( HierType == MesHierarchy.HT_CERTEGY_MERCHANT )
      {
        nodeId = orgId;
      }
      else
      {
        nodeId = orgIdToHierarchyNode(orgId);
      }
    
      /*@lineinfo:generated-code*//*@lineinfo:229^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cc.station_number             as station_number, 
//                  nvl(mf.dba_name,cc.station_name)
//                                                as station_name,
//                  cc.chain_number               as chain_number, 
//                  cc.chain_name                 as chain_name, 
//                  cc.transaction_date           as tran_date, 
//                  cc.transaction_amount         as tran_amount, 
//                  cc.customer_account_number    as cust_number, 
//                  cc.check_number               as check_number,   
//                  cc.transaction_amount         as tran_amount, 
//                  cc.merchant_dda               as merchant_dda, 
//                  cc.print_time                 as print_time, 
//                  cc.reference_number           as ref_num, 
//                  cc.region                     as region, 
//                  cc.report_date                as report_date, 
//                  cc.settlement_date            as settlement_date, 
//                  cc.store                      as store
//          from    daily_detail_file_cert_check  cc,
//                  mif                           mf
//          where   cc.station_number = :nodeId and
//                  cc.report_date between :beginDate and :endDate and 
//                  mf.merchant_number = cc.station_number
//          order by cc.station_number, cc.store
//        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cc.station_number             as station_number, \n                nvl(mf.dba_name,cc.station_name)\n                                              as station_name,\n                cc.chain_number               as chain_number, \n                cc.chain_name                 as chain_name, \n                cc.transaction_date           as tran_date, \n                cc.transaction_amount         as tran_amount, \n                cc.customer_account_number    as cust_number, \n                cc.check_number               as check_number,   \n                cc.transaction_amount         as tran_amount, \n                cc.merchant_dda               as merchant_dda, \n                cc.print_time                 as print_time, \n                cc.reference_number           as ref_num, \n                cc.region                     as region, \n                cc.report_date                as report_date, \n                cc.settlement_date            as settlement_date, \n                cc.store                      as store\n        from    daily_detail_file_cert_check  cc,\n                mif                           mf\n        where   cc.station_number =  :1  and\n                cc.report_date between  :2  and  :3  and \n                mf.merchant_number = cc.station_number\n        order by cc.station_number, cc.store";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CertegyCheckDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.CertegyCheckDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^7*/
      rs = it.getResultSet();
      
      while(rs.next())
      {
        ReportRows.addElement(new DetailRow(rs));
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry(buildMethodName("loadDetailData", orgId, beginDate, endDate), e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  public void loadSummaryData(long orgId, Date beginDate, Date endDate)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      if ( HierType == MesHierarchy.HT_CERTEGY_MERCHANT )
      {
        /*@lineinfo:generated-code*//*@lineinfo:285^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (cc  IDX_DDF_CERT_CHECK_DATE_MERCH) */
//                    th.descendent                   as org_num,
//                    th.descendent                   as hierarchy_node,
//                    nvl(thn.name,'Not Named')       as org_name,
//                    0                               as district,
//                    count(cc.station_number)        as item_count,
//                    sum(cc.transaction_amount)      as item_amount
//            from    t_hierarchy                     th,
//                    t_hierarchy                     thc,
//                    t_hierarchy_names               thn,
//                    daily_detail_file_cert_check    cc
//            where   th.hier_type = 10 and
//                    th.ancestor = :orgId and
//                    th.relation = 1 and
//                    thc.hier_type = 10 and
//                    thc.ancestor = th.descendent and
//                    cc.station_number = thc.descendent and
//                    cc.report_date between :beginDate and :endDate and
//                    thn.hier_type = 10 and
//                    thn.hier_id = th.descendent
//            group by th.descendent, nvl(thn.name,'Not Named')        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (cc  IDX_DDF_CERT_CHECK_DATE_MERCH) */\n                  th.descendent                   as org_num,\n                  th.descendent                   as hierarchy_node,\n                  nvl(thn.name,'Not Named')       as org_name,\n                  0                               as district,\n                  count(cc.station_number)        as item_count,\n                  sum(cc.transaction_amount)      as item_amount\n          from    t_hierarchy                     th,\n                  t_hierarchy                     thc,\n                  t_hierarchy_names               thn,\n                  daily_detail_file_cert_check    cc\n          where   th.hier_type = 10 and\n                  th.ancestor =  :1  and\n                  th.relation = 1 and\n                  thc.hier_type = 10 and\n                  thc.ancestor = th.descendent and\n                  cc.station_number = thc.descendent and\n                  cc.report_date between  :2  and  :3  and\n                  thn.hier_type = 10 and\n                  thn.hier_id = th.descendent\n          group by th.descendent, nvl(thn.name,'Not Named')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.CertegyCheckDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.CertegyCheckDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:308^9*/
      }
      else    // assume std bank hierarchy
      {
        long nodeId = orgIdToHierarchyNode(orgId);
        
        if ( isNodeAssociation(nodeId) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:316^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                       as org_num,
//                      cc.station_number               as hierarchy_node,
//                      mf.dba_name                     as org_name,
//                      0                               as district,
//                      count(cc.station_number)        as item_count,
//                      sum(cc.transaction_amount)      as item_amount
//              from    mif                             mf,
//                      daily_detail_file_cert_check    cc,
//                      organization                    o
//              where   mf.association_node = :nodeId and
//                      cc.station_number = mf.merchant_number and
//                      cc.report_date between :beginDate and :endDate and
//                      o.org_group = mf.merchant_number
//              group by o.org_num, cc.station_number, mf.dba_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                       as org_num,\n                    cc.station_number               as hierarchy_node,\n                    mf.dba_name                     as org_name,\n                    0                               as district,\n                    count(cc.station_number)        as item_count,\n                    sum(cc.transaction_amount)      as item_amount\n            from    mif                             mf,\n                    daily_detail_file_cert_check    cc,\n                    organization                    o\n            where   mf.association_node =  :1  and\n                    cc.station_number = mf.merchant_number and\n                    cc.report_date between  :2  and  :3  and\n                    o.org_group = mf.merchant_number\n            group by o.org_num, cc.station_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.CertegyCheckDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.CertegyCheckDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:332^11*/        
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:336^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                       as org_num,
//                      th.descendent                   as hierarchy_node,
//                      nvl(o.org_name,'Not Named')     as org_name,
//                      0                               as district,
//                      count(cc.station_number)        as item_count,
//                      sum(cc.transaction_amount)      as item_amount
//              from    t_hierarchy                     th,
//                      t_hierarchy                     thc,
//                      mif                             mf,
//                      daily_detail_file_cert_check    cc,
//                      organization                    o
//              where   th.hier_type = 1 and
//                      th.ancestor = :nodeId and
//                      th.relation = 1 and
//                      thc.hier_type = 1 and
//                      thc.ancestor = th.descendent and
//                      mf.association_node = thc.descendent and
//                      cc.station_number = mf.merchant_number and
//                      cc.report_date between :beginDate and :endDate and
//                      o.org_group(+) = th.descendent
//              group by o.org_num, th.descendent, nvl(o.org_name,'Not Named')
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                       as org_num,\n                    th.descendent                   as hierarchy_node,\n                    nvl(o.org_name,'Not Named')     as org_name,\n                    0                               as district,\n                    count(cc.station_number)        as item_count,\n                    sum(cc.transaction_amount)      as item_amount\n            from    t_hierarchy                     th,\n                    t_hierarchy                     thc,\n                    mif                             mf,\n                    daily_detail_file_cert_check    cc,\n                    organization                    o\n            where   th.hier_type = 1 and\n                    th.ancestor =  :1  and\n                    th.relation = 1 and\n                    thc.hier_type = 1 and\n                    thc.ancestor = th.descendent and\n                    mf.association_node = thc.descendent and\n                    cc.station_number = mf.merchant_number and\n                    cc.report_date between  :2  and  :3  and\n                    o.org_group(+) = th.descendent\n            group by o.org_num, th.descendent, nvl(o.org_name,'Not Named')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.CertegyCheckDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.CertegyCheckDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:359^11*/        
        }          
      }        
        
      if ( it != null )
      {
        rs = it.getResultSet();
        processSummaryData(rs);
        rs.close();
        it.close();
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    int             defaultType = -1;
    long            nodeId      = 0L;
    int             recCount    = 0;
    UserBean        user        = getUser();
    
    try
    {
      super.setProperties(request);
    
      if ( HierType == MesHierarchy.HT_CERTEGY_MERCHANT )
      {
        System.out.println("defaultNode = " + user.getHierarchyNode( MesHierarchy.HT_CERTEGY_MERCHANT ) );//@
        //@ ReportOrgIdDefault = user.getHierarchyNode( MesHierarchy.HT_CERTEGY_MERCHANT );
      
        if ( ( nodeId = HttpHelper.getLong(request,"com.mes.HierarchyNode",0L) ) == 0L )
        {
          if ( ( nodeId = HttpHelper.getLong(request,"com.mes.MerchantId",0L) ) == 0L )
          {
            nodeId = HttpHelper.getLong(request,"com.mes.OrgId", user.getHierarchyNode( MesHierarchy.HT_CERTEGY_MERCHANT ) );
          }
        }

        System.out.println("nodeId = " + user.getHierarchyNode( MesHierarchy.HT_CERTEGY_MERCHANT ) );//@
        setReportHierarchyNode(nodeId);
      
        /*@lineinfo:generated-code*//*@lineinfo:409^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(th.ancestor)  
//            from    t_hierarchy   th
//            where   th.hier_type  = :HierType and
//                    th.ancestor   = :ReportNodeIdDefault and
//                    th.descendent = :ReportNodeId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(th.ancestor)   \n          from    t_hierarchy   th\n          where   th.hier_type  =  :1  and\n                  th.ancestor   =  :2  and\n                  th.descendent =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.CertegyCheckDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,HierType);
   __sJT_st.setLong(2,ReportNodeIdDefault);
   __sJT_st.setLong(3,ReportNodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:416^9*/
      
        if ( recCount == 0 )
        {
          ReportNodeId = ReportNodeIdDefault;
        }
      }
    }
    catch( Exception e )
    {
      logEntry("setProperties()",e.toString());
    }
  }
}/*@lineinfo:generated-code*/