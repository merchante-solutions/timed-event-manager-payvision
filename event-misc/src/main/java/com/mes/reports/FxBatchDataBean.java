/*@lineinfo:filename=FxBatchDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/FxBatchDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000-2007,2008 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;



public class FxBatchDataBean extends ReportSQLJBean
{
  public static class DetailRow
  {
    public    String    AuthCode              = null;
    public    Date      BatchDate             = null;
    public    int       BatchNumber           = 0;
    public    String    CardNumber            = null;
    public    String    CardNumberEnc         = null;
    public    String    CardType              = null;
    public    String    ClientRefNum          = null;
    public    String    CurrencyCode          = null;
    public    String    CurrencyCodeAlpha     = null;
    public    String    CurrencyName          = null;
    public    String    DbaName               = null;
    public    String    DebitCreditIndicator  = null;
    public    String    EntryMode             = null;
    public    double    FxAmount              = 0.0;
    public    long      MerchantId            = 0L;
    public    String    PurchaseId            = null;
    public    String    ReferenceNumber       = null;
    public    int       TerminalNumber        = 0;
    public    double    TranAmount            = 0.0;
    public    Date      TranDate              = null;
    public    String    TridentTranId         = null;
  
    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      MerchantId            = resultSet.getLong  ( "merchant_number" );
      DbaName               = resultSet.getString( "dba_name" );
      ReferenceNumber       = resultSet.getString( "ref_num" );
      CardType              = resultSet.getString( "card_type" );
      ClientRefNum          = processString( resultSet.getString( "client_ref_num" ) );
      CurrencyName          = resultSet.getString( "currency_name" );
      CurrencyCode          = resultSet.getString( "currency_code" );
      CurrencyCodeAlpha     = resultSet.getString( "currency_code_alpha" );
      AuthCode              = processString( resultSet.getString( "auth_code" ) );
      FxAmount              = resultSet.getDouble( "fx_amount" );
      TranAmount            = resultSet.getDouble( "tran_amount" );
      TranDate              = resultSet.getDate  ( "tran_date" );
      EntryMode             = resultSet.getString( "pos_entry_mode" );
      CardNumber            = resultSet.getString( "card_number" );
      CardNumberEnc         = resultSet.getString( "card_number_enc" );
      DebitCreditIndicator  = resultSet.getString( "debit_credit_ind" );
      BatchNumber           = resultSet.getInt   ( "batch_number" );
      BatchDate             = resultSet.getDate  ( "batch_date" );
      PurchaseId            = processString( resultSet.getString( "purchase_id" ) );
      TerminalNumber        = resultSet.getInt   ( "terminal_number" );
      TridentTranId         = resultSet.getString( "trident_tran_id" );
    }
  }
  
  public static class SummaryRow
  {
    public    long        HierarchyNode     = 0L;
    public    double      NetAmount         = 0.0;
    public    String      OrgName           = null;
    public    int         TranCount         = 0;
    
    public SummaryRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      NetAmount         = resultSet.getDouble("net_amount");
      OrgName           = resultSet.getString("org_name");
      TranCount         = resultSet.getInt("tran_count");
    }
  }
  
  public FxBatchDataBean( )
  {
    super(true);    // use field bean
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    // hidden fields
    fields.add(new HiddenField("batchNumber"));
    setData("batchNumber","0");
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( getReportType() == RT_SUMMARY )
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"Tran Count\",");
      line.append("\"USD Amount\"");
    }
    else  // RT_DETAILS
    {
      line.append("\"Merchant ID\",");
      line.append("\"Dba Name\",");
      line.append("\"Term #\",");
      line.append("\"Batch Number\",");
      line.append("\"Batch Date\",");
      line.append("\"Tran Date\",");
      line.append("\"Card Type\",");
      line.append("\"Card Number\",");
      line.append("\"Reference Number\",");
      line.append("\"Purchase ID\",");
      line.append("\"Trident Tran ID\",");
      line.append("\"Client Reference\",");
      line.append("\"Auth Code\",");
      line.append("\"Entry Mode\",");
      line.append("\"Debit Credit Ind\",");
      line.append("\"Currency Code\",");
      line.append("\"Amount\",");
      line.append("\"Amount USD\"");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    if ( getReportType() == RT_SUMMARY )
    {
      SummaryRow  row = (SummaryRow) obj;
      
      line.append( encodeHierarchyNode(row.HierarchyNode) );
      line.append( ",\"" );
      line.append( row.OrgName );
      line.append( "\"," );
      line.append( row.TranCount );
      line.append( "," );
      line.append( row.NetAmount );
    }
    else    // RT_DETAILS
    {
      DetailRow       row = (DetailRow) obj;
      
      line.append( encodeHierarchyNode(row.MerchantId) );
      line.append( ",\"" );
      line.append( row.DbaName );
      line.append( "\",\"" );
      line.append( row.TerminalNumber );
      line.append( "\"," );
      line.append( row.BatchNumber );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate(row.BatchDate,"MM/dd/yyyy") );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate(row.TranDate,"MM/dd/yyyy") );
      line.append( ",\"" );
      line.append( row.CardType );
      line.append( "\",\"" );
      line.append( row.CardNumber );
      line.append( "\",\"'" );
      line.append( row.ReferenceNumber );
      line.append( "\",\"" );
      line.append( row.PurchaseId );
      line.append( "\",\"" );
      line.append( row.TridentTranId );
      line.append( "\",\"" );
      line.append( row.ClientRefNum );
      line.append( "\",\"" );
      line.append( row.AuthCode );
      line.append( "\",\"" );
      line.append( row.EntryMode );
      line.append( "\",\"" );
      line.append( row.DebitCreditIndicator );
      line.append( "\",\"" );
      line.append( row.CurrencyCodeAlpha );
      line.append( "\"," );
      line.append( row.TranAmount );
      line.append( "," );
      line.append( row.FxAmount );
    }
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    
    if( fileFormat == FF_CSV )
    {
      retVal = true;
    }
    return(retVal);
  }
  
  public String getDownloadFilenameBase()
  {
    Date                    beginDate = getReportDateBegin();
    Calendar                cal       = Calendar.getInstance();  
    Date                    endDate   = getReportDateEnd();
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    
    if ( getReportType() == RT_SUMMARY )
    {
      filename.append("_fx_batch_summary_");
    }
    else  // RT_DETAILS
    {
      filename.append("_fx_batch_details_");
    }      
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( beginDate,"MMddyy" ) );
    if ( beginDate.equals(endDate) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( endDate,"MMddyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    if ( getReportType() == RT_SUMMARY )
    {
      loadSummaryData(orgId,beginDate,endDate);
    }
    else    // RT_DETAILS
    {
      loadDetailData(orgId,beginDate,endDate);
    }
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    int                 batchNumber       = getInt("batchNumber",0);
    ResultSetIterator   it                = null;
    ResultSet           resultSet         = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:286^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      ordered 
//                      use_nl (dt pd) 
//                   */
//                  dt.merchant_number                  as merchant_number,
//                  dt.dba_name                         as dba_name,
//                  dt.reference_number                 as ref_num,
//                  dt.card_type                        as card_type,
//                  dt.auth_code                        as auth_code,
//                  nvl( cc.currency_name,
//                       dt.currency_code )             as currency_name,
//                  dt.currency_code                    as currency_code,
//                  cc.currency_code_alpha              as currency_code_alpha,
//                  ( dt.transaction_amount *
//                    decode(dt.debit_credit_indicator,
//                           'C',-1,1) )                as tran_amount,
//                  ( dt.fx_amount_base *
//                    decode(dt.debit_credit_indicator,
//                           'C',-1,1) )                as fx_amount,
//                  dt.transaction_date                 as tran_date,
//                  nvl(pd.pos_entry_desc,
//                      dt.pos_entry_mode )             as pos_entry_mode,
//                  dt.card_number                      as card_number,
//                  dt.card_number_enc                  as card_number_enc,
//                  dt.debit_credit_indicator           as debit_credit_ind,
//                  dt.batch_number                     as batch_number,
//                  dt.batch_date                       as batch_date,
//                  dt.purchase_id                      as purchase_id,
//                  substr(dt.terminal_id,-4)           as terminal_number,
//                  dt.batch_number                     as batch_number,
//                  dt.trident_tran_id                  as trident_tran_id,
//                  dt.client_reference_number          as client_ref_num
//          from    group_merchant            gm,
//                  trident_capture_api       dt,
//                  trident_profile           tp,
//                  pos_entry_mode_desc       pd,
//                  currency_codes            cc
//          where   gm.org_num                  = :orgId and
//                  dt.merchant_number          = gm.merchant_number and
//                  dt.batch_date between :beginDate and :endDate and
//                  nvl(dt.fx_amount_base,0) > 0 and
//                  tp.terminal_id = dt.terminal_id and
//                  pd.pos_entry_code(+) = dt.pos_entry_mode and
//                  ( :batchNumber = 0 or dt.batch_number = :batchNumber ) and
//                  cc.currency_code(+) = dt.currency_code
//          order by dt.currency_code, dt.merchant_number, dt.batch_date, dt.batch_number, dt.card_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    ordered \n                    use_nl (dt pd) \n                 */\n                dt.merchant_number                  as merchant_number,\n                dt.dba_name                         as dba_name,\n                dt.reference_number                 as ref_num,\n                dt.card_type                        as card_type,\n                dt.auth_code                        as auth_code,\n                nvl( cc.currency_name,\n                     dt.currency_code )             as currency_name,\n                dt.currency_code                    as currency_code,\n                cc.currency_code_alpha              as currency_code_alpha,\n                ( dt.transaction_amount *\n                  decode(dt.debit_credit_indicator,\n                         'C',-1,1) )                as tran_amount,\n                ( dt.fx_amount_base *\n                  decode(dt.debit_credit_indicator,\n                         'C',-1,1) )                as fx_amount,\n                dt.transaction_date                 as tran_date,\n                nvl(pd.pos_entry_desc,\n                    dt.pos_entry_mode )             as pos_entry_mode,\n                dt.card_number                      as card_number,\n                dt.card_number_enc                  as card_number_enc,\n                dt.debit_credit_indicator           as debit_credit_ind,\n                dt.batch_number                     as batch_number,\n                dt.batch_date                       as batch_date,\n                dt.purchase_id                      as purchase_id,\n                substr(dt.terminal_id,-4)           as terminal_number,\n                dt.batch_number                     as batch_number,\n                dt.trident_tran_id                  as trident_tran_id,\n                dt.client_reference_number          as client_ref_num\n        from    group_merchant            gm,\n                trident_capture_api       dt,\n                trident_profile           tp,\n                pos_entry_mode_desc       pd,\n                currency_codes            cc\n        where   gm.org_num                  =  :1  and\n                dt.merchant_number          = gm.merchant_number and\n                dt.batch_date between  :2  and  :3  and\n                nvl(dt.fx_amount_base,0) > 0 and\n                tp.terminal_id = dt.terminal_id and\n                pd.pos_entry_code(+) = dt.pos_entry_mode and\n                (  :4  = 0 or dt.batch_number =  :5  ) and\n                cc.currency_code(+) = dt.currency_code\n        order by dt.currency_code, dt.merchant_number, dt.batch_date, dt.batch_number, dt.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.FxBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,batchNumber);
   __sJT_st.setInt(5,batchNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.FxBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:334^7*/
      
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new DetailRow( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:363^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group                         as hierarchy_node,
//                  o.org_name                          as org_name,
//                  count(1)                            as tran_count,
//                  sum( dt.fx_amount_base *
//                       decode(dt.debit_credit_indicator,'C',-1,1)
//                     )                                as net_amount
//          from    parent_org                po,
//                  group_merchant            gm,
//                  trident_capture_api       dt,
//                  organization              o
//          where   po.parent_org_num = :orgId and
//                  gm.org_num = po.org_num and
//                  dt.merchant_number = gm.merchant_number and
//                  dt.batch_date between :beginDate and :endDate and
//                  nvl(dt.fx_amount_base,0) > 0 and
//                  o.org_num = po.org_num
//          group by o.org_group, o.org_name
//          order by o.org_name, o.org_group      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group                         as hierarchy_node,\n                o.org_name                          as org_name,\n                count(1)                            as tran_count,\n                sum( dt.fx_amount_base *\n                     decode(dt.debit_credit_indicator,'C',-1,1)\n                   )                                as net_amount\n        from    parent_org                po,\n                group_merchant            gm,\n                trident_capture_api       dt,\n                organization              o\n        where   po.parent_org_num =  :1  and\n                gm.org_num = po.org_num and\n                dt.merchant_number = gm.merchant_number and\n                dt.batch_date between  :2  and  :3  and\n                nvl(dt.fx_amount_base,0) > 0 and\n                o.org_num = po.org_num\n        group by o.org_group, o.org_name\n        order by o.org_name, o.org_group";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.FxBatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.FxBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:383^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new SummaryRow( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try { resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) { }
    }
  }
}/*@lineinfo:generated-code*/