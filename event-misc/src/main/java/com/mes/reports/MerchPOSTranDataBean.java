/*@lineinfo:filename=MerchPOSTranDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchPOSTranDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-01-26 15:28:40 -0800 (Mon, 26 Jan 2009) $
  Version            : $Revision: 15743 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import sqlj.runtime.ResultSetIterator;

public class MerchPOSTranDataBean extends MerchTranDataBean
{
  public MerchPOSTranDataBean( )
  {
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_pos_tran_summary_");
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          merchantId        = 0L;
    ResultSet                     resultSet         = null;
    StringBuffer                  temp              = null;
    String                        tempCard          = null;
    
    try
    {
      if ( FindTransaction == true )
      {
        if ( FindDataValid == true )
        {
          if ( FindCardNumberString != null )
          {
            temp = new StringBuffer(FindCardNumberString);
            for( int i = 4; i < (int)Math.min(6,FindCardNumberString.length()); ++i )
            {
              if ( Character.isDigit( temp.charAt(i) ) )
              {
                temp.setCharAt(i,'x');
              }
            }
            tempCard = temp.toString();
          }            
        
          /*@lineinfo:generated-code*//*@lineinfo:100^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                          ordered
//                          use_nl (dt pd) 
//                          index(dt idx_ddf_ext_dt_merch_numer)
//                       */
//                      mf.merchant_number                as hierarchy_node,
//                      mf.dba_name                       as org_name,
//                      dt.reference_number               as ref_num,
//                      dt.card_type                      as card_type,
//                      dt.auth_code                      as auth_code,
//                      ( dt.transaction_amount *
//                        decode(dt.debit_credit_indicator,
//                               'C',-1,1) )              as tran_amount,
//                      dt.transaction_date               as tran_date,
//                      nvl(pd.pos_entry_desc,
//                          dt.pos_entry_mode )           as pos_entry_mode,
//                      dt.card_number                    as card_number,
//                      dt.card_number_enc                as card_number_enc,
//                      dt.passenger_name                 as passenger_name,
//                      dt.pos_terminal_id                as pos_tid,
//                      lft.file_prefix                   as file_prefix,
//                      dt.debit_credit_indicator         as debit_credit_ind,
//                      dt.batch_number                   as batch_number,
//                      dt.batch_date                     as batch_date,
//                      dt.purchase_id                    as purchase_id,
//                      decode( instr(lower(dt.load_filename),'gps'),
//                              0, dt.terminal_number,
//                              -1 )                      as terminal_number,
//                      decode( instr(lower(dt.load_filename),'gps'),
//                              0, dt.merchant_batch_number,
//                              -1 )                      as merchant_batch_number,
//                      decode(length(nvl(dt.purchase_id,'bad')) +
//                             is_number(nvl(dt.purchase_id,'bad')),
//                             14, dt.purchase_id,
//                             null)                      as ticket_number,
//                      dt.tran_code                      as tran_code,
//                      dt.acq_invoice_number             as acq_invoice,
//                      dt.trident_tran_id                as trident_tran_id,
//                      tapi.client_reference_number      as client_ref_num
//              from    group_merchant            gm,
//                      daily_detail_file_ext_dt  dt,
//                      mif                       mf,
//                      pos_entry_mode_desc       pd,
//                      mes_load_file_types       lft,
//                      trident_capture_api       tapi
//              where   gm.org_num                  = :orgId and
//                      dt.batch_date between :beginDate and :endDate and
//                      dt.merchant_number          = gm.merchant_number and
//                      mf.merchant_number          = dt.merchant_number and
//                      pd.pos_entry_code(+)        = dt.pos_entry_mode and
//                      ( dt.card_number like :FindCardNumberString or
//                        dt.card_number like :tempCard or
//                        dt.reference_number = :FindValue or
//                        dt.purchase_id = :FindValue or
//                        dt.acq_invoice_number = :FindValue ) and
//                      (lft.file_prefix || mf.bank_number) =
//                        substr(dt.load_filename,1,length(lft.file_prefix)+4)
//                      and tapi.trident_tran_id(+) = dt.trident_tran_id
//              order by dt.merchant_number, dt.batch_date, dt.batch_number, dt.card_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                        ordered\n                        use_nl (dt pd) \n                        index(dt idx_ddf_ext_dt_merch_numer)\n                     */\n                    mf.merchant_number                as hierarchy_node,\n                    mf.dba_name                       as org_name,\n                    dt.reference_number               as ref_num,\n                    dt.card_type                      as card_type,\n                    dt.auth_code                      as auth_code,\n                    ( dt.transaction_amount *\n                      decode(dt.debit_credit_indicator,\n                             'C',-1,1) )              as tran_amount,\n                    dt.transaction_date               as tran_date,\n                    nvl(pd.pos_entry_desc,\n                        dt.pos_entry_mode )           as pos_entry_mode,\n                    dt.card_number                    as card_number,\n                    dt.card_number_enc                as card_number_enc,\n                    dt.passenger_name                 as passenger_name,\n                    dt.pos_terminal_id                as pos_tid,\n                    lft.file_prefix                   as file_prefix,\n                    dt.debit_credit_indicator         as debit_credit_ind,\n                    dt.batch_number                   as batch_number,\n                    dt.batch_date                     as batch_date,\n                    dt.purchase_id                    as purchase_id,\n                    decode( instr(lower(dt.load_filename),'gps'),\n                            0, dt.terminal_number,\n                            -1 )                      as terminal_number,\n                    decode( instr(lower(dt.load_filename),'gps'),\n                            0, dt.merchant_batch_number,\n                            -1 )                      as merchant_batch_number,\n                    decode(length(nvl(dt.purchase_id,'bad')) +\n                           is_number(nvl(dt.purchase_id,'bad')),\n                           14, dt.purchase_id,\n                           null)                      as ticket_number,\n                    dt.tran_code                      as tran_code,\n                    dt.acq_invoice_number             as acq_invoice,\n                    dt.trident_tran_id                as trident_tran_id,\n                    tapi.client_reference_number      as client_ref_num\n            from    group_merchant            gm,\n                    daily_detail_file_ext_dt  dt,\n                    mif                       mf,\n                    pos_entry_mode_desc       pd,\n                    mes_load_file_types       lft,\n                    trident_capture_api       tapi\n            where   gm.org_num                  =  :1  and\n                    dt.batch_date between  :2  and  :3  and\n                    dt.merchant_number          = gm.merchant_number and\n                    mf.merchant_number          = dt.merchant_number and\n                    pd.pos_entry_code(+)        = dt.pos_entry_mode and\n                    ( dt.card_number like  :4  or\n                      dt.card_number like  :5  or\n                      dt.reference_number =  :6  or\n                      dt.purchase_id =  :7  or\n                      dt.acq_invoice_number =  :8  ) and\n                    (lft.file_prefix || mf.bank_number) =\n                      substr(dt.load_filename,1,length(lft.file_prefix)+4)\n                    and tapi.trident_tran_id(+) = dt.trident_tran_id\n            order by dt.merchant_number, dt.batch_date, dt.batch_number, dt.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchPOSTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setString(4,FindCardNumberString);
   __sJT_st.setString(5,tempCard);
   __sJT_st.setString(6,FindValue);
   __sJT_st.setString(7,FindValue);
   __sJT_st.setString(8,FindValue);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchPOSTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^11*/
        }          
      }
      else    // FindTransaction is false
      {
        /* this is the main query for merchant detail */
        /*@lineinfo:generated-code*//*@lineinfo:167^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                        ordered 
//                        use_nl (dt pd) 
//                        index(dt idx_ddf_ext_dt_merch_numer)
//                     */
//                    mf.merchant_number                  as hierarchy_node,
//                    mf.dba_name                         as org_name,
//                    dt.reference_number                 as ref_num,
//                    dt.card_type                        as card_type,
//                    dt.auth_code                        as auth_code,
//                    ( dt.transaction_amount *
//                      decode(dt.debit_credit_indicator,
//                             'C',-1,1) )                as tran_amount,
//                    dt.transaction_date                 as tran_date,
//                    nvl(pd.pos_entry_desc,
//                        dt.pos_entry_mode )             as pos_entry_mode,
//                    dt.card_number                      as card_number,
//                    dt.card_number_enc                  as card_number_enc,
//                    dt.passenger_name                   as passenger_name,
//                    dt.pos_terminal_id                  as pos_tid,
//                    lft.file_prefix                     as file_prefix,
//                    dt.debit_credit_indicator           as debit_credit_ind,
//                    dt.batch_number                     as batch_number,
//                    dt.batch_date                       as batch_date,
//                    dt.purchase_id                      as purchase_id,
//                    decode( instr(lower(dt.load_filename),'gps'),
//                            0, dt.terminal_number,
//                            -1 )                        as terminal_number,
//                    decode( instr(lower(dt.load_filename),'gps'),
//                            0, dt.merchant_batch_number,
//                            -1 )                        as merchant_batch_number,
//                    decode(length(nvl(dt.purchase_id,'bad')) +
//                           is_number(nvl(dt.purchase_id,'bad')),
//                           14, dt.purchase_id,
//                           null)                        as ticket_number,
//                    dt.tran_code                        as tran_code,
//                    dt.acq_invoice_number               as acq_invoice,
//                    dt.trident_tran_id                  as trident_tran_id,
//                    tapi.client_reference_number        as client_ref_num
//            from    group_merchant            gm,
//                    daily_detail_file_ext_dt  dt,
//                    mif                       mf,
//                    pos_entry_mode_desc       pd,
//                    mes_load_file_types       lft,
//                    trident_capture_api       tapi
//            where   gm.org_num                  = :orgId and
//                    dt.batch_date between :beginDate and :endDate and
//                    dt.merchant_number          = gm.merchant_number and
//                    mf.merchant_number          = dt.merchant_number and
//                    pd.pos_entry_code(+)        = dt.pos_entry_mode and
//                    ( :BatchNumber = 0 or dt.batch_number = :BatchNumber ) and 
//                    ( 
//                      :Filter.isEnabled(TransactionFilter.FT_POS_ENTRY_MODE) = 0 or
//                      dt.pos_entry_mode = :Filter.getString(TransactionFilter.FT_POS_ENTRY_MODE) 
//                    ) and
//                    ( :Filter.isEnabled(TransactionFilter.FT_CARD_TYPE) = 0 or 
//                      ( :Filter.getString(TransactionFilter.FT_CARD_TYPE) = 'BANK' and
//                        nvl(decode( dt.card_type,
//                                    'VS','D',  -- visa/mc/debit always settled
//                                    'VD','D',
//                                    'VB','D',
//                                    'MC','D',
//                                    'MB','D',
//                                    'MD','D',
//                                    'DB','D',
//                                    'EB','D',
//                                    'BL','D',
//                                    'AM',substr(mf.amex_plan,1,1),
//                                    'DS',substr(mf.discover_plan,1,1),
//                                    'DC',substr(mf.diners_plan,1,1),
//                                    'JC',substr(mf.jcb_plan,1,1),
//                                    'PL',substr(mf.private_label_1_plan,1,1),
//                                    null ),'N') = 'D' ) or
//                      ( :Filter.getString(TransactionFilter.FT_CARD_TYPE) = 'NONBANK' and
//                        nvl(decode( dt.card_type,
//                                    'VS','D',  -- visa/mc/debit always settled
//                                    'VD','D',
//                                    'VB','D',
//                                    'MC','D',
//                                    'MB','D',
//                                    'MD','D',
//                                    'DB','D',
//                                    'EB','D',
//                                    'BL','D',
//                                    'AM',substr(mf.amex_plan,1,1),
//                                    'DS',substr(mf.discover_plan,1,1),
//                                    'DC',substr(mf.diners_plan,1,1),
//                                    'JC',substr(mf.jcb_plan,1,1),
//                                    'PL',substr(mf.private_label_1_plan,1,1),
//                                     null),'N') = 'N' ) or
//                      decode( dt.card_type,
//                              'EB','DB',      -- ebt same as debit
//                              'VD','VS',      -- all visa together
//                              'VB','VS',
//                              'MB','MC',      -- all mc together
//                              'MD','MC',
//                              dt.card_type ) = :Filter.getString(TransactionFilter.FT_CARD_TYPE)
//                    ) and
//                    ( 
//                      :Filter.isEnabled(TransactionFilter.FT_TRAN_TYPE) = 0 or
//                      dt.debit_credit_indicator = :Filter.getString(TransactionFilter.FT_TRAN_TYPE) 
//                    ) and
//                    ( 
//                      :Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_EQUAL) = 0 or
//                      dt.transaction_amount = :Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_EQUAL) 
//                    ) and
//                    ( 
//                      :Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_LESS) = 0 or
//                      dt.transaction_amount < :Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_LESS) 
//                    ) and
//                    ( 
//                      :Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_GREATER) = 0 or
//                      dt.transaction_amount > :Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_GREATER) 
//                    ) and
//                    (lft.file_prefix || mf.bank_number) =
//                      substr(dt.load_filename,1,length(lft.file_prefix)+4) 
//                    and tapi.trident_tran_id(+) = dt.trident_tran_id
//            order by dt.merchant_number, dt.batch_date, dt.batch_number, dt.card_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1143 = Filter.isEnabled(TransactionFilter.FT_POS_ENTRY_MODE);
 String __sJT_1144 = Filter.getString(TransactionFilter.FT_POS_ENTRY_MODE);
 int __sJT_1145 = Filter.isEnabled(TransactionFilter.FT_CARD_TYPE);
 String __sJT_1146 = Filter.getString(TransactionFilter.FT_CARD_TYPE);
 String __sJT_1147 = Filter.getString(TransactionFilter.FT_CARD_TYPE);
 String __sJT_1148 = Filter.getString(TransactionFilter.FT_CARD_TYPE);
 int __sJT_1149 = Filter.isEnabled(TransactionFilter.FT_TRAN_TYPE);
 String __sJT_1150 = Filter.getString(TransactionFilter.FT_TRAN_TYPE);
 int __sJT_1151 = Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_EQUAL);
 double __sJT_1152 = Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_EQUAL);
 int __sJT_1153 = Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_LESS);
 double __sJT_1154 = Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_LESS);
 int __sJT_1155 = Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_GREATER);
 double __sJT_1156 = Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_GREATER);
  try {
   String theSqlTS = "select  /*+ \n                      ordered \n                      use_nl (dt pd) \n                      index(dt idx_ddf_ext_dt_merch_numer)\n                   */\n                  mf.merchant_number                  as hierarchy_node,\n                  mf.dba_name                         as org_name,\n                  dt.reference_number                 as ref_num,\n                  dt.card_type                        as card_type,\n                  dt.auth_code                        as auth_code,\n                  ( dt.transaction_amount *\n                    decode(dt.debit_credit_indicator,\n                           'C',-1,1) )                as tran_amount,\n                  dt.transaction_date                 as tran_date,\n                  nvl(pd.pos_entry_desc,\n                      dt.pos_entry_mode )             as pos_entry_mode,\n                  dt.card_number                      as card_number,\n                  dt.card_number_enc                  as card_number_enc,\n                  dt.passenger_name                   as passenger_name,\n                  dt.pos_terminal_id                  as pos_tid,\n                  lft.file_prefix                     as file_prefix,\n                  dt.debit_credit_indicator           as debit_credit_ind,\n                  dt.batch_number                     as batch_number,\n                  dt.batch_date                       as batch_date,\n                  dt.purchase_id                      as purchase_id,\n                  decode( instr(lower(dt.load_filename),'gps'),\n                          0, dt.terminal_number,\n                          -1 )                        as terminal_number,\n                  decode( instr(lower(dt.load_filename),'gps'),\n                          0, dt.merchant_batch_number,\n                          -1 )                        as merchant_batch_number,\n                  decode(length(nvl(dt.purchase_id,'bad')) +\n                         is_number(nvl(dt.purchase_id,'bad')),\n                         14, dt.purchase_id,\n                         null)                        as ticket_number,\n                  dt.tran_code                        as tran_code,\n                  dt.acq_invoice_number               as acq_invoice,\n                  dt.trident_tran_id                  as trident_tran_id,\n                  tapi.client_reference_number        as client_ref_num\n          from    group_merchant            gm,\n                  daily_detail_file_ext_dt  dt,\n                  mif                       mf,\n                  pos_entry_mode_desc       pd,\n                  mes_load_file_types       lft,\n                  trident_capture_api       tapi\n          where   gm.org_num                  =  :1  and\n                  dt.batch_date between  :2  and  :3  and\n                  dt.merchant_number          = gm.merchant_number and\n                  mf.merchant_number          = dt.merchant_number and\n                  pd.pos_entry_code(+)        = dt.pos_entry_mode and\n                  (  :4  = 0 or dt.batch_number =  :5  ) and \n                  ( \n                     :6  = 0 or\n                    dt.pos_entry_mode =  :7  \n                  ) and\n                  (  :8  = 0 or \n                    (  :9  = 'BANK' and\n                      nvl(decode( dt.card_type,\n                                  'VS','D',  -- visa/mc/debit always settled\n                                  'VD','D',\n                                  'VB','D',\n                                  'MC','D',\n                                  'MB','D',\n                                  'MD','D',\n                                  'DB','D',\n                                  'EB','D',\n                                  'BL','D',\n                                  'AM',substr(mf.amex_plan,1,1),\n                                  'DS',substr(mf.discover_plan,1,1),\n                                  'DC',substr(mf.diners_plan,1,1),\n                                  'JC',substr(mf.jcb_plan,1,1),\n                                  'PL',substr(mf.private_label_1_plan,1,1),\n                                  null ),'N') = 'D' ) or\n                    (  :10  = 'NONBANK' and\n                      nvl(decode( dt.card_type,\n                                  'VS','D',  -- visa/mc/debit always settled\n                                  'VD','D',\n                                  'VB','D',\n                                  'MC','D',\n                                  'MB','D',\n                                  'MD','D',\n                                  'DB','D',\n                                  'EB','D',\n                                  'BL','D',\n                                  'AM',substr(mf.amex_plan,1,1),\n                                  'DS',substr(mf.discover_plan,1,1),\n                                  'DC',substr(mf.diners_plan,1,1),\n                                  'JC',substr(mf.jcb_plan,1,1),\n                                  'PL',substr(mf.private_label_1_plan,1,1),\n                                   null),'N') = 'N' ) or\n                    decode( dt.card_type,\n                            'EB','DB',      -- ebt same as debit\n                            'VD','VS',      -- all visa together\n                            'VB','VS',\n                            'MB','MC',      -- all mc together\n                            'MD','MC',\n                            dt.card_type ) =  :11 \n                  ) and\n                  ( \n                     :12  = 0 or\n                    dt.debit_credit_indicator =  :13  \n                  ) and\n                  ( \n                     :14  = 0 or\n                    dt.transaction_amount =  :15  \n                  ) and\n                  ( \n                     :16  = 0 or\n                    dt.transaction_amount <  :17  \n                  ) and\n                  ( \n                     :18  = 0 or\n                    dt.transaction_amount >  :19  \n                  ) and\n                  (lft.file_prefix || mf.bank_number) =\n                    substr(dt.load_filename,1,length(lft.file_prefix)+4) \n                  and tapi.trident_tran_id(+) = dt.trident_tran_id\n          order by dt.merchant_number, dt.batch_date, dt.batch_number, dt.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchPOSTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setLong(4,BatchNumber);
   __sJT_st.setLong(5,BatchNumber);
   __sJT_st.setInt(6,__sJT_1143);
   __sJT_st.setString(7,__sJT_1144);
   __sJT_st.setInt(8,__sJT_1145);
   __sJT_st.setString(9,__sJT_1146);
   __sJT_st.setString(10,__sJT_1147);
   __sJT_st.setString(11,__sJT_1148);
   __sJT_st.setInt(12,__sJT_1149);
   __sJT_st.setString(13,__sJT_1150);
   __sJT_st.setInt(14,__sJT_1151);
   __sJT_st.setDouble(15,__sJT_1152);
   __sJT_st.setInt(16,__sJT_1153);
   __sJT_st.setDouble(17,__sJT_1154);
   __sJT_st.setInt(18,__sJT_1155);
   __sJT_st.setDouble(19,__sJT_1156);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchPOSTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:287^9*/
      }
      
      // find transaction does not run if params are bad
      if ( it != null )
      {
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          ReportRows.addElement( new DetailRow( resultSet ) );
        }
        resultSet.close();
        it.close();
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    int                           bankId            = getReportBankId();
    int                           byBatch           = 0;
    long                          dummy             = 0L;
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    MesTransactionSummary         tranSummary       = null;
    
    try
    {
      byBatch = (ShowBatchSummary ? 1 : 0);

      if ( (getReportMerchantId() != 0L) || ShowCardSummary || (byBatch != 0) )
      {
        /* this query is the card type summary */
        // load the external (GPS, TC57, PTI) data
        
        /*@lineinfo:generated-code*//*@lineinfo:332^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                        ordered 
//                        use_nl (gm dt mf) 
//                        index(dt idx_ddf_ext_dt_merch_numer)
//                     */
//                    :byBatch                              as by_batch,
//                    mf.merchant_number                    as merchant_number,
//                    mf.dba_name                           as dba_name,
//                    dt.batch_date                         as batch_date,
//                    dt.batch_number                       as batch_number,
//                    dt.card_type                          as card_type, 
//                    decode(dt.card_type,
//                           'AM', decode(substr(mf.amex_plan,1,1),'D',0,1),
//                           'DS', decode(substr(mf.discover_plan,1,1),'D',0,1),
//                           'DC', decode(substr(mf.diners_plan,1,1),'D',0,1),
//                           'JC', decode(substr(mf.jcb_plan,1,1),'D',0,1),
//                           'PL', decode(substr(mf.private_label_1_plan,1,1),'D',0,1),
//                           0 )                            as settled_by_issuer,
//                    decode(dt.debit_credit_indicator,'C',1,0)
//                                                          as tran_type,                         
//                    dt.debit_credit_indicator             as debit_credit_ind,
//                    count(dt.card_type)                   as item_count,
//                    sum(dt.transaction_amount)            as item_amount,
//                    sum(dt.transaction_amount *
//                        decode(dt.debit_credit_indicator,'C',-1,1))
//                                                          as item_amount_signed
//            from    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    daily_detail_file_ext_dt  dt,
//                    mif                       mf
//            where   gm.org_num = :orgId and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    dt.merchant_number = gm.merchant_number and
//                    dt.batch_date between :beginDate and :endDate and
//                    mf.merchant_number = dt.merchant_number and
//                    ( :District = :DISTRICT_NONE or 
//                      nvl(mf.district,:DISTRICT_UNASSIGNED) = :District )
//            group by mf.merchant_number, mf.dba_name, 
//                      dt.batch_date, dt.batch_number,
//                      dt.card_type, 
//                      decode(dt.card_type,
//                           'AM', decode(substr(mf.amex_plan,1,1),'D',0,1),
//                           'DS', decode(substr(mf.discover_plan,1,1),'D',0,1),
//                           'DC', decode(substr(mf.diners_plan,1,1),'D',0,1),
//                           'JC', decode(substr(mf.jcb_plan,1,1),'D',0,1),
//                           'PL', decode(substr(mf.private_label_1_plan,1,1),'D',0,1),
//                           0 ),
//                      decode(dt.debit_credit_indicator,'C',1,0),
//                      dt.debit_credit_indicator
//            order by mf.merchant_number, dt.batch_date, dt.batch_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                      ordered \n                      use_nl (gm dt mf) \n                      index(dt idx_ddf_ext_dt_merch_numer)\n                   */\n                   :1                               as by_batch,\n                  mf.merchant_number                    as merchant_number,\n                  mf.dba_name                           as dba_name,\n                  dt.batch_date                         as batch_date,\n                  dt.batch_number                       as batch_number,\n                  dt.card_type                          as card_type, \n                  decode(dt.card_type,\n                         'AM', decode(substr(mf.amex_plan,1,1),'D',0,1),\n                         'DS', decode(substr(mf.discover_plan,1,1),'D',0,1),\n                         'DC', decode(substr(mf.diners_plan,1,1),'D',0,1),\n                         'JC', decode(substr(mf.jcb_plan,1,1),'D',0,1),\n                         'PL', decode(substr(mf.private_label_1_plan,1,1),'D',0,1),\n                         0 )                            as settled_by_issuer,\n                  decode(dt.debit_credit_indicator,'C',1,0)\n                                                        as tran_type,                         \n                  dt.debit_credit_indicator             as debit_credit_ind,\n                  count(dt.card_type)                   as item_count,\n                  sum(dt.transaction_amount)            as item_amount,\n                  sum(dt.transaction_amount *\n                      decode(dt.debit_credit_indicator,'C',-1,1))\n                                                        as item_amount_signed\n          from    group_merchant            gm,\n                  group_rep_merchant        grm,\n                  daily_detail_file_ext_dt  dt,\n                  mif                       mf\n          where   gm.org_num =  :2  and\n                  grm.user_id(+) =  :3  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :4  = -1 ) and        \n                  dt.merchant_number = gm.merchant_number and\n                  dt.batch_date between  :5  and  :6  and\n                  mf.merchant_number = dt.merchant_number and\n                  (  :7  =  :8  or \n                    nvl(mf.district, :9 ) =  :10  )\n          group by mf.merchant_number, mf.dba_name, \n                    dt.batch_date, dt.batch_number,\n                    dt.card_type, \n                    decode(dt.card_type,\n                         'AM', decode(substr(mf.amex_plan,1,1),'D',0,1),\n                         'DS', decode(substr(mf.discover_plan,1,1),'D',0,1),\n                         'DC', decode(substr(mf.diners_plan,1,1),'D',0,1),\n                         'JC', decode(substr(mf.jcb_plan,1,1),'D',0,1),\n                         'PL', decode(substr(mf.private_label_1_plan,1,1),'D',0,1),\n                         0 ),\n                    decode(dt.debit_credit_indicator,'C',1,0),\n                    dt.debit_credit_indicator\n          order by mf.merchant_number, dt.batch_date, dt.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchPOSTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,byBatch);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setInt(7,District);
   __sJT_st.setInt(8,DISTRICT_NONE);
   __sJT_st.setInt(9,DISTRICT_UNASSIGNED);
   __sJT_st.setInt(10,District);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchPOSTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:386^9*/
        resultSet   = it.getResultSet();
        tranSummary = new MesTransactionSummary( bankId, resultSet );
        
        if( byBatch == 0 )
        {
          // add this summary to the report
          ReportRows.addElement( tranSummary );
        }
        else    // process the rest of the rows in the set
        {
          while( byBatch == 1 )
          {
            // add this summary to the report
            ReportRows.addElement( tranSummary );
        
            try
            {
              dummy       = resultSet.getLong("batch_number");   // test if exhausted
              tranSummary = new MesTransactionSummary( bankId, resultSet, false );
            }
            catch( Exception e )
            {
              break;    // done, exit the loop
            }
          }
        }
        resultSet.close();
        it.close();
      }
      else
      {
        if ( hasAssocDistricts() && !IgnoreDistricts )
        // returns false unless the current node is an
        // association and it has districts under it.
        {
          if ( District == DISTRICT_NONE )
          {
            /*@lineinfo:generated-code*//*@lineinfo:424^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                            ordered 
//                            use_nl (gm mf sm) 
//                         */
//                        o.org_num                             as org_num,
//                        o.org_group                           as hierarchy_node,
//                        nvl(mf.district,:DISTRICT_UNASSIGNED) as district,
//                        nvl(ad.district_desc,decode( mf.district,
//                                    null,'Unassigned',
//                                   ('District ' || to_char(mf.district, '0009')))) as org_name,
//                        sum(bank_count)                       as item_count,
//                        sum(bank_amount)                      as item_amount,
//                        sum(nonbank_count)                    as non_bank_count,
//                        sum(nonbank_amount)                   as non_bank_amount
//                from    organization                  o,
//                        group_merchant                gm,
//                        group_rep_merchant            grm,
//                        daily_detail_file_ext_summary sm,
//                        mif                           mf,
//                        assoc_districts               ad
//                where   o.org_num           = :orgId and
//                        gm.org_num          = o.org_num and
//                        grm.user_id(+) = :AppFilterUserId and
//                        grm.merchant_number(+) = gm.merchant_number and
//                        ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                        mf.merchant_number  = gm.merchant_number and
//                        sm.merchant_number(+) = mf.merchant_number and
//                        sm.batch_date(+) between :beginDate and :endDate and
//                        ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                        ad.district(+)      = mf.district
//                group by  o.org_num, o.org_group,
//                          mf.district, ad.district_desc
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                          ordered \n                          use_nl (gm mf sm) \n                       */\n                      o.org_num                             as org_num,\n                      o.org_group                           as hierarchy_node,\n                      nvl(mf.district, :1 ) as district,\n                      nvl(ad.district_desc,decode( mf.district,\n                                  null,'Unassigned',\n                                 ('District ' || to_char(mf.district, '0009')))) as org_name,\n                      sum(bank_count)                       as item_count,\n                      sum(bank_amount)                      as item_amount,\n                      sum(nonbank_count)                    as non_bank_count,\n                      sum(nonbank_amount)                   as non_bank_amount\n              from    organization                  o,\n                      group_merchant                gm,\n                      group_rep_merchant            grm,\n                      daily_detail_file_ext_summary sm,\n                      mif                           mf,\n                      assoc_districts               ad\n              where   o.org_num           =  :2  and\n                      gm.org_num          = o.org_num and\n                      grm.user_id(+) =  :3  and\n                      grm.merchant_number(+) = gm.merchant_number and\n                      ( not grm.user_id is null or  :4  = -1 ) and        \n                      mf.merchant_number  = gm.merchant_number and\n                      sm.merchant_number(+) = mf.merchant_number and\n                      sm.batch_date(+) between  :5  and  :6  and\n                      ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                      ad.district(+)      = mf.district\n              group by  o.org_num, o.org_group,\n                        mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchPOSTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchPOSTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:458^13*/
          }
          else    // a district was specified
          {
            /*@lineinfo:generated-code*//*@lineinfo:462^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                            ordered 
//                            use_nl (gm mf sm) 
//                        */
//                        o.org_num                     as org_num,
//                        mf.merchant_number            as hierarchy_node,
//                        mf.dba_name                   as org_name,
//                        :District                     as district,
//                        sum(bank_count)               as item_count,
//                        sum(bank_amount)              as item_amount,
//                        sum(nonbank_count)            as non_bank_count,
//                        sum(nonbank_amount)           as non_bank_amount
//                from    group_merchant                gm,
//                        group_rep_merchant            grm,
//                        daily_detail_file_ext_summary sm,
//                        mif                           mf,
//                        organization                  o
//                where   gm.org_num          = :orgId and
//                        grm.user_id(+) = :AppFilterUserId and
//                        grm.merchant_number(+) = gm.merchant_number and
//                        ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                        mf.merchant_number  = gm.merchant_number and
//                        nvl(mf.district,:DISTRICT_UNASSIGNED) = :District and
//                        sm.merchant_number(+) = mf.merchant_number and
//                        sm.batch_date(+) between :beginDate and :endDate and
//                        o.org_group = mf.merchant_number
//                group by  o.org_num, mf.merchant_number, mf.dba_name
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                          ordered \n                          use_nl (gm mf sm) \n                      */\n                      o.org_num                     as org_num,\n                      mf.merchant_number            as hierarchy_node,\n                      mf.dba_name                   as org_name,\n                       :1                      as district,\n                      sum(bank_count)               as item_count,\n                      sum(bank_amount)              as item_amount,\n                      sum(nonbank_count)            as non_bank_count,\n                      sum(nonbank_amount)           as non_bank_amount\n              from    group_merchant                gm,\n                      group_rep_merchant            grm,\n                      daily_detail_file_ext_summary sm,\n                      mif                           mf,\n                      organization                  o\n              where   gm.org_num          =  :2  and\n                      grm.user_id(+) =  :3  and\n                      grm.merchant_number(+) = gm.merchant_number and\n                      ( not grm.user_id is null or  :4  = -1 ) and        \n                      mf.merchant_number  = gm.merchant_number and\n                      nvl(mf.district, :5 ) =  :6  and\n                      sm.merchant_number(+) = mf.merchant_number and\n                      sm.batch_date(+) between  :7  and  :8  and\n                      o.org_group = mf.merchant_number\n              group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchPOSTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,DISTRICT_UNASSIGNED);
   __sJT_st.setInt(6,District);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchPOSTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:491^13*/
          }
        }
        else // just standard child report, no districts
        {
          /* standard summary report for organizations */
          /*@lineinfo:generated-code*//*@lineinfo:497^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                          ordered 
//                          use_nl (gm mf sm) 
//                      */
//                      o.org_num                     as org_num,
//                      o.org_group                   as hierarchy_node,
//                      o.org_name                    as org_name,
//                      0                             as district,
//                      sum(bank_count)               as item_count,
//                      sum(bank_amount)              as item_amount,
//                      sum(nonbank_count)            as non_bank_count,
//                      sum(nonbank_amount)           as non_bank_amount
//              from    parent_org                    po,
//                      organization                  o,
//                      group_merchant                gm,
//                      group_rep_merchant            grm,
//                      daily_detail_file_ext_summary sm
//              where   po.parent_org_num   = :orgId and
//                      o.org_num           = po.org_num and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      gm.org_num(+)       = o.org_num and
//                      sm.merchant_number(+) = gm.merchant_number and
//                      sm.batch_date(+) between :beginDate and :endDate
//              group by o.org_num, o.org_group, o.org_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                        ordered \n                        use_nl (gm mf sm) \n                    */\n                    o.org_num                     as org_num,\n                    o.org_group                   as hierarchy_node,\n                    o.org_name                    as org_name,\n                    0                             as district,\n                    sum(bank_count)               as item_count,\n                    sum(bank_amount)              as item_amount,\n                    sum(nonbank_count)            as non_bank_count,\n                    sum(nonbank_amount)           as non_bank_amount\n            from    parent_org                    po,\n                    organization                  o,\n                    group_merchant                gm,\n                    group_rep_merchant            grm,\n                    daily_detail_file_ext_summary sm\n            where   po.parent_org_num   =  :1  and\n                    o.org_num           = po.org_num and\n                    grm.user_id(+) =  :2  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :3  = -1 ) and        \n                    gm.org_num(+)       = o.org_num and\n                    sm.merchant_number(+) = gm.merchant_number and\n                    sm.batch_date(+) between  :4  and  :5 \n            group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchPOSTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchPOSTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:525^11*/
        }
        resultSet = it.getResultSet();
        processSummaryData( resultSet );
        resultSet.close();
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try { resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) { }
    }
  }
}/*@lineinfo:generated-code*/