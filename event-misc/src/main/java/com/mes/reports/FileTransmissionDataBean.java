/*@lineinfo:filename=FileTransmissionDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/FileTransmissionDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 7/16/03 11:01a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class FileTransmissionDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public String           LoadFilename      = null;
    public String           FileDesc          = null;
    public long             FileSize          = 0;
    public int              BankNumber        = 0;
    public Timestamp        TSReceived        = null;
    public Timestamp        TSLoaded          = null;
    public Timestamp        TSCopied          = null;
    public Timestamp        TSTranSummarized  = null;
    public Timestamp        TSRiskSummarized  = null;
  
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      LoadFilename      = resultSet.getString("load_filename");
      FileDesc          = resultSet.getString("file_desc");
      FileSize          = resultSet.getLong("file_size");
      BankNumber        = resultSet.getInt("bank_number");
      TSReceived        = resultSet.getTimestamp("received");
      TSLoaded          = resultSet.getTimestamp("loaded");
      TSCopied          = resultSet.getTimestamp("copied");
      TSTranSummarized  = resultSet.getTimestamp("tran_summarized");
      TSRiskSummarized  = resultSet.getTimestamp("risk_summarized");
    }
  }
  
  public FileTransmissionDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Bank Number\",");
    line.append("\"File Type\",");
    line.append("\"Load Filename\",");
    line.append("\"File Size\",");
    line.append("\"Received\",");
    line.append("\"Loaded\",");
    line.append("\"Copied\",");
    line.append("\"Tran Summarized\",");
    line.append("\"Risk Summarized\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append(record.BankNumber);
    line.append(",\"");
    line.append(record.FileDesc);
    line.append("\",\"");            
    line.append(record.LoadFilename);
    line.append("\",");    
    line.append(record.FileSize);
    line.append(",\"");    
    line.append(DateTimeFormatter.getFormattedDate(record.TSReceived,"MM/dd/yyyy hh:mm:ss a"));
    line.append("\",\"");    
    line.append(DateTimeFormatter.getFormattedDate(record.TSLoaded,"MM/dd/yyyy hh:mm:ss a"));
    line.append("\",\"");    
    line.append(DateTimeFormatter.getFormattedDate(record.TSCopied,"MM/dd/yyyy hh:mm:ss a"));
    line.append("\",\"");    
    line.append(DateTimeFormatter.getFormattedDate(record.TSTranSummarized,"MM/dd/yyyy hh:mm:ss a"));
    line.append("\",\"");    
    line.append(DateTimeFormatter.getFormattedDate(record.TSRiskSummarized,"MM/dd/yyyy hh:mm:ss a"));
    line.append("\"");    
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_file_transmissions_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    int                           bankNumber        = getOrgBankId( orgId );
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:163^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ft.load_filename        as load_filename,
//                  lft.file_desc           as file_desc,
//                  ft.file_size            as file_size,
//                  ft.bank_number          as bank_number,
//                  ft.received             as received,
//                  ft.loaded               as loaded, 
//                  ft.copied               as copied,
//                  ft.tran_summarized      as tran_summarized, 
//                  ft.risk_summarized      as risk_summarized
//          from    file_Transmissions      ft,
//                  mes_load_file_types     lft 
//          where   ( :bankNumber = 9999 or ft.bank_number = :bankNumber ) and 
//                  trunc(ft.received) between :beginDate and :endDate and
//                  lft.file_prefix = substr(ft.load_filename,1,length(lft.file_prefix)) and
//                  is_number(substr(ft.load_filename,length(lft.file_prefix)+1,4)) = 1                
//          order by ft.received
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ft.load_filename        as load_filename,\n                lft.file_desc           as file_desc,\n                ft.file_size            as file_size,\n                ft.bank_number          as bank_number,\n                ft.received             as received,\n                ft.loaded               as loaded, \n                ft.copied               as copied,\n                ft.tran_summarized      as tran_summarized, \n                ft.risk_summarized      as risk_summarized\n        from    file_Transmissions      ft,\n                mes_load_file_types     lft \n        where   (  :1  = 9999 or ft.bank_number =  :2  ) and \n                trunc(ft.received) between  :3  and  :4  and\n                lft.file_prefix = substr(ft.load_filename,1,length(lft.file_prefix)) and\n                is_number(substr(ft.load_filename,length(lft.file_prefix)+1,4)) = 1                \n        order by ft.received";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.FileTransmissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.FileTransmissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:181^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         inc           = -1;
      
      if ( ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.MONDAY ) ||
           ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.TUESDAY ) )
      {
        // friday data available on monday,
        // sat,sun,mon data available on tuesday
        inc = -3;     
      }
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, inc );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      
      cal.setTime( ReportDateEnd );
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/