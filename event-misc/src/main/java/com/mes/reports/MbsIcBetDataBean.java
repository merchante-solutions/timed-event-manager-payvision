/*@lineinfo:filename=MbsIcBetDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/reports/MbsIcBetDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-11-30 11:37:25 -0800 (Fri, 30 Nov 2012) $
  Version            : $Revision: 20747 $

  Change History:
     See SVN database

  Copyright (C) 2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.DropDownField;
import com.mes.forms.FieldGroup;
import com.mes.forms.HierarchyNodeField;
import com.mes.forms.NumberField;
import com.mes.mbs.IcBillingData;
import com.mes.mbs.IcVolume;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class MbsIcBetDataBean extends ReportSQLJBean
{
  public class BetData
  {
    public    String      CardType            = null;
    public    String      IcCode              = null;
    public    String      IcDesc              = null;
    public    double      Rate                = 0.0;
    public    double      PerItem             = 0.0;
    public    String      StatementMsg        = "";
    public    String      ApplyToReturns      = "";
    public    String      UseDbRates          = "";

    public    int         SalesCount          = 0;
    public    double      SalesAmount         = 0.0;
    public    int         CreditsCount        = 0;
    public    double      CreditsAmount       = 0.0;
    public    int         IcCount             = 0;
    public    double      IcAmount            = 0.0;
    
    public    double      BillingRate         = 0.0;
    public    double      BillingPerItem      = 0.0;
    public    double      FeesDue             = 0.0;
    
    public BetData()
    {
    }
  }
  
  public class BetGroupData
  {
    public    int         BankNumber          = 0;
    public    int         BetNumber           = 0;
    public    int         GroupId             = 0;
    public    String      GroupLabel          = null;
    public    String      IcCode              = null;
    public    String      IcCodeExp           = null;
    public    String      IcDescExp           = null;
    public    String      PlanType            = null;
    public    long        RecId               = 0L;
    public    Date        ValidDateBegin      = null;
    public    Date        ValidDateEnd        = null;
    
    public BetGroupData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BankNumber          = resultSet.getInt    ("bank_number");
      BetNumber           = resultSet.getInt    ("bet_number");
      RecId               = resultSet.getLong   ("rec_id");
      GroupId             = resultSet.getInt    ("group_id");
      PlanType            = processString( resultSet.getString ("plan_type") );
      IcCode              = processString( resultSet.getString ("ic_code") );
      GroupLabel          = processString( resultSet.getString ("group_label") );
      ValidDateBegin      = resultSet.getDate   ("valid_date_begin");
      ValidDateEnd        = resultSet.getDate   ("valid_date_end");
      IcCodeExp           = processString( resultSet.getString ("ic_code_exp") );
      IcDescExp           = processString( resultSet.getString ("ic_desc_exp") );
    }
  }
  
  public class BetMarkupData
  {
    public    int         BankNumber          = 0;
    public    int         BetNumber           = 0;
    public    int         MarkupId            = 0;
    public    String      GroupLabel          = null;
    public    String      AbsRate             = null;
    public    String      AbsPerItem          = null;
    public    String      RelRate             = null;
    public    String      RelPerItem          = null;
    public    String      RelRateDb           = null;
    public    String      RelPerItemDb        = null;
    public    String      AddlRate            = null;
    public    String      AddlPerItem         = null;
    public    long        RecId               = 0L;
    public    Date        ValidDateBegin      = null;
    public    Date        ValidDateEnd        = null;
    
    public BetMarkupData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BankNumber          = resultSet.getInt    ("bank_number");
      BetNumber           = resultSet.getInt    ("bet_number");
      MarkupId            = resultSet.getInt    ("markup_id");
      GroupLabel          = resultSet.getString ("group_label");
      AbsRate             = getRateString       (resultSet,"abs_rate");
      AbsPerItem          = getPerItemString    (resultSet,"abs_per_item");
      RelRate             = getRateString       (resultSet,"rel_rate");
      RelPerItem          = getPerItemString    (resultSet,"rel_per_item");
      RelRateDb           = getRateString       (resultSet,"rel_rate_db");
      RelPerItemDb        = getPerItemString    (resultSet,"rel_per_item_db");
      AddlRate            = getRateString       (resultSet,"addl_rate");
      AddlPerItem         = getPerItemString    (resultSet,"addl_per_item");
      RecId               = resultSet.getLong   ("rec_id");
      ValidDateBegin      = resultSet.getDate   ("valid_date_begin");
      ValidDateEnd        = resultSet.getDate   ("valid_date_end");
    }
    
    protected String getPerItemString(ResultSet resultSet, String colName)
      throws java.sql.SQLException
    {
      String    retVal    = "";
      
      if ( !isBlank(resultSet.getString(colName)) )
      {
        retVal = MesMath.toFractionalCurrency(resultSet.getDouble(colName),3);
      }
      return( retVal );
    }
    
    protected String getRateString(ResultSet resultSet, String colName)
      throws java.sql.SQLException
    {
      String    retVal    = "";
      
      if ( !isBlank(resultSet.getString(colName)) )
      {
        retVal = NumberFormatter.getPercentString(resultSet.getDouble(colName)/100.0,3);
      }
      return( retVal );
    }
  }
  
  public class BetToGroupMarkupData
  {
    // number of rows & columns for getLabel() and getData()
    public    int         numRows             = 3;
    public    int         numCols             = 4;

    public    int         BankNumber          = 0;
    public    int         BetNumber           = 0;
    public    int         GroupId             = 0;
    public    int         MarkupId            = 0;
    public    String      ApplyToReturns      = null;
    public    String      ApplyPerItemWhRate0 = null;
    public    int         PercentNegDbToReturn= 0;
    public    String      ShowRateOnStatement = null;
    public    String      UseAltRates         = null;
    public    Date        ValidDateBegin      = null;
    public    Date        ValidDateEnd        = null;
    
    public BetToGroupMarkupData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BankNumber          = resultSet.getInt    ("bank_number");
      BetNumber           = resultSet.getInt    ("bet_number");
      GroupId             = resultSet.getInt    ("group_id");
      MarkupId            = resultSet.getInt    ("markup_id");
      ApplyToReturns      = resultSet.getString ("apply_to_returns");
      ApplyPerItemWhRate0 = resultSet.getString ("apply_per_item_when_rate_zero");
      PercentNegDbToReturn= resultSet.getInt    ("percent_neg_db_m_up_to_return");
      ShowRateOnStatement = resultSet.getString ("show_rate_on_statement");
      UseAltRates         = resultSet.getString ("use_alt_rates");
      ValidDateBegin      = resultSet.getDate   ("valid_date_begin");
      ValidDateEnd        = resultSet.getDate   ("valid_date_end");
    }
    
    public String getLabel(int colRow)
      throws java.sql.SQLException
    {
      String    retVal    = "";
      
      switch ( colRow )
      {
        case 11:  retVal = "Bank ~ BET"                                                   ; break;
        case 12:  retVal = "Apply To Returns"                                             ; break;
        case 13:  retVal = "Use Alternate Rates"                                          ; break;

        case 21:  retVal = "Show Rate On Statement"                                       ; break;
        case 22:  retVal = "Apply PerItem When Rate Zero"                                 ; break;
        case 23:  retVal = "Percent Negative Debit Markup To Return"                      ; break;

        case 31:  retVal = "Markup ID"                                                    ; break;
        case 32:  retVal = "Group ID"                                                     ; break;

        case 41:  retVal = "Valid Date Begin"                                             ; break;
        case 42:  retVal = "Valid Date End"                                               ; break;
      }
      return( retVal );
    }
    
    public String getData(int colRow)
      throws java.sql.SQLException
    {
      String    retVal    = "";
      
      switch ( colRow )
      {
        case 11:  retVal = String.valueOf(BankNumber) + " ~ " + String.valueOf(BetNumber) ; break;
        case 12:  retVal = ApplyToReturns                                                 ; break;
        case 13:  retVal = UseAltRates                                                    ; break;

        case 21:  retVal = ShowRateOnStatement                                            ; break;
        case 22:  retVal = ApplyPerItemWhRate0                                            ; break;
        case 23:  retVal = String.valueOf(PercentNegDbToReturn)                           ; break;

        case 31:  retVal = String.valueOf(MarkupId)                                       ; break;
        case 32:  retVal = String.valueOf(GroupId)                                        ; break;

        case 41:  retVal = DateTimeFormatter.getFormattedDate(ValidDateBegin,"MMM yyyy")  ; break;
        case 42:  retVal = DateTimeFormatter.getFormattedDate(ValidDateEnd  ,"MMM yyyy")  ; break;
      }
      return( retVal );
    }
  }
  
  public class BetListData
  {
    public    int         BankNumber          = 0;
    public    int         BetNumber           = 0;
    public    int         GroupId             = 0;
    public    int         MarkupId            = 0;
    public    String      OpenOnReportDate    = null;
    public    Date        ValidDateBegin      = null;
    public    Date        ValidDateEnd        = null;
    
    public BetListData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BankNumber          = resultSet.getInt    ("bank_number");
      BetNumber           = resultSet.getInt    ("bet_number");
      GroupId             = resultSet.getInt    ("group_id");
      MarkupId            = resultSet.getInt    ("markup_id");
      OpenOnReportDate    = resultSet.getString ("open_on_report_date");
      ValidDateBegin      = resultSet.getDate   ("valid_date_begin");
      ValidDateEnd        = resultSet.getDate   ("valid_date_end");
    }
  }
  
  public class BetPricingData
  {
    public    int         BankNumber          = 0;
    public    int         BetNumber           = 0;
    public    String      BillingMonths       = null;
    public    int         ItemType            = 0;
    public    String      ItemSubclass        = null;
    public    long        MerchantNumber      = 0L;
    public    String      PricingRate         = null;
    public    String      PricingPerItem      = null;
    public    long        RecId               = 0L;
    public    Date        ValidDateBegin      = null;
    public    Date        ValidDateEnd        = null;
    public    int         VolumeType          = 0;
    
    public BetPricingData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BankNumber          = resultSet.getInt    ("bank_number");
      BetNumber           = resultSet.getInt    ("bet_number");
      BillingMonths       = resultSet.getString ("billing_months");
      ItemType            = resultSet.getInt    ("item_type");
      ItemSubclass        = resultSet.getString ("item_subclass");
      MerchantNumber      = resultSet.getLong   ("merchant_number");
      PricingRate         = getRateString       (resultSet,"rate");
      PricingPerItem      = getPerItemString    (resultSet,"per_item");
      RecId               = resultSet.getLong   ("rec_id");
      ValidDateBegin      = resultSet.getDate   ("valid_date_begin");
      ValidDateEnd        = resultSet.getDate   ("valid_date_end");
      VolumeType          = resultSet.getInt    ("volume_type");
    }
    
    protected String getPerItemString(ResultSet resultSet, String colName)
      throws java.sql.SQLException
    {
      String    retVal    = "";
      
      if ( !isBlank(resultSet.getString(colName)) )
      {
        retVal = MesMath.toFractionalCurrency(resultSet.getDouble(colName),3);
      }
      return( retVal );
    }
    
    protected String getRateString(ResultSet resultSet, String colName)
      throws java.sql.SQLException
    {
      String    retVal    = "";
      
      if ( !isBlank(resultSet.getString(colName)) )
      {
        retVal = NumberFormatter.getPercentString(resultSet.getDouble(colName)/100.0,3);
      }
      return( retVal );
    }
  }
  
  public class CardTypeDropDownTable extends DropDownTable
  {
    public CardTypeDropDownTable( )
    {
      addElement("VS,MC,DS","Visa, MC, Discover");
      addElement("ALL","All Card Types");
      addElement("VS" ,"Visa");
      addElement("MC" ,"MasterCard");
      addElement("AM" ,"Amercian Express");
      addElement("DS" ,"Discover");
    }
  }
  
  protected HashMap     BetVolume         = null;
  protected boolean     EndDateIsInPast   = true;
  
  public MbsIcBetDataBean( )
  {
    super(true);   // enable field bean support
  }
  
  public boolean canCopyGroup( int groupId )
  {
    int     recCount      = 0;
    Date    reportDate    = getReportDateBegin();

    try
    {
      // can copy any group id IFF it is used by more than one bet on or after reportDate
      /*@lineinfo:generated-code*//*@lineinfo:372^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    mbs_bet_to_group_markup bgm
//          where   bgm.group_id = :groupId
//              and bgm.valid_date_end >= :reportDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    mbs_bet_to_group_markup bgm\n        where   bgm.group_id =  :1 \n            and bgm.valid_date_end >=  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,groupId);
   __sJT_st.setDate(2,reportDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:379^7*/
    }
    finally
    {
      return( recCount > 1 );
    }
  }

  public boolean canCopyMarkup( int markupId )
  {
    int     recCount      = 0;
    Date    reportDate    = getReportDateBegin();

    try
    {
      // can copy any markup id IFF it is used by more than one bet on or after reportDate
      /*@lineinfo:generated-code*//*@lineinfo:395^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    mbs_bet_to_group_markup bgm
//          where   bgm.markup_id = :markupId
//              and bgm.valid_date_end >= :reportDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    mbs_bet_to_group_markup bgm\n        where   bgm.markup_id =  :1 \n            and bgm.valid_date_end >=  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,markupId);
   __sJT_st.setDate(2,reportDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:402^7*/
    }
    finally
    {
      return( recCount > 1 );
    }
  }

  public boolean canDeleteBet(int markupId, int groupId)
  {
    int     recCount      = -1;
    Date    reportDate    = getReportDateBegin();

    try
    {
      // can delete bet IFF that does not orphan the bet's non-special markup and/or group
      if( markupId > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:420^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    mbs_ic_markup           icm
//            where   icm.markup_id = :markupId
//                and icm.valid_date_end >= :reportDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    mbs_ic_markup           icm\n          where   icm.markup_id =  :1 \n              and icm.valid_date_end >=  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,markupId);
   __sJT_st.setDate(2,reportDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:427^9*/

        if( recCount > 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:431^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    mbs_bet_to_group_markup bgm
//              where   bgm.markup_id = :markupId
//                  and bgm.valid_date_end >= :reportDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    mbs_bet_to_group_markup bgm\n            where   bgm.markup_id =  :1 \n                and bgm.valid_date_end >=  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,markupId);
   __sJT_st.setDate(2,reportDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:438^11*/
        }
      }
      // recCount = -1 means                           special markup ,  ok to delete bet
      // recCount =  0 means           markup dead beyond report date ,  ok to delete bet
      // recCount =  1 means  one bet using markup beyond report date , DO NOT delete bet
      // recCount >  1 means >one bet using markup beyond report date ,  ok to delete bet

      if(  groupId > 0 && recCount != 1 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:448^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    mbs_ic_groups           icg
//            where   icg.group_id  =  :groupId
//                and icm.valid_date_end >= :reportDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    mbs_ic_groups           icg\n          where   icg.group_id  =   :1 \n              and icm.valid_date_end >=  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,groupId);
   __sJT_st.setDate(2,reportDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:455^9*/

        if( recCount > 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:459^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    mbs_bet_to_group_markup bgm
//              where   bgm.group_id  =  :groupId
//                  and bgm.valid_date_end >= :reportDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    mbs_bet_to_group_markup bgm\n            where   bgm.group_id  =   :1 \n                and bgm.valid_date_end >=  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,groupId);
   __sJT_st.setDate(2,reportDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:466^11*/
        }
      }
      // recCount meanings similar to above, but both markup & group have been considered;
      // recCount = 1 is still the only value that means the bet cannot be deleted
    }
    catch( java.sql.SQLException e )
    {
      recCount = 1;
    }
    finally
    {
      return( recCount != 1 );
    }
  }

  public boolean canDeleteGroup( int groupId )
  {
    // can delete any non-special group id
    return( groupId > 0 );
  }

  public boolean canDeleteMarkup( int markupId, String groupLabel )
  {
    int     recCount      = -1;
    Date    reportDate    = getReportDateBegin();

    try
    {
      // can delete any non-special markup id
      //    IFF the given label / all labels are not being used by any group paired with this markup
      if( markupId > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:499^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    mbs_bet_to_group_markup bgm,
//                    mbs_ic_groups           icg
//            where   bgm.markup_id = :markupId
//                and bgm.valid_date_end >= :reportDate
//                and icg.group_id = bgm.group_id
//                and icg.valid_date_end >= :reportDate
//                and icg.group_label = nvl(:groupLabel,icg.group_label)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    mbs_bet_to_group_markup bgm,\n                  mbs_ic_groups           icg\n          where   bgm.markup_id =  :1 \n              and bgm.valid_date_end >=  :2 \n              and icg.group_id = bgm.group_id\n              and icg.valid_date_end >=  :3 \n              and icg.group_label = nvl( :4 ,icg.group_label)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,markupId);
   __sJT_st.setDate(2,reportDate);
   __sJT_st.setDate(3,reportDate);
   __sJT_st.setString(4,groupLabel);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:510^9*/
      }
    }
    finally
    {
      return( recCount == 0 );
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    DropDownField   dropDownField     = null;
    
    try
    {
      super.createFields(request);
    
      FieldGroup fgroup = (FieldGroup)getField("searchFields");
      
      fgroup.deleteField("endDate");
      fgroup.deleteField("portfolioId");
      fgroup.deleteField("zip2Node");
      
      fgroup.add(new DropDownField( "bankNumber","Bank Number", new MbsBankNumberDropDownTable(getReportHierarchyNode()), true ));
      fgroup.add(new NumberField( "betNumber","BET Number", 4, 6, false, 0 ));
      fgroup.add(new HierarchyNodeField( Ctx,getReportHierarchyNode(),"nodeId","Hierarchy Node",true ));
      fgroup.add(new DropDownField( "cardType","Card Type", new CardTypeDropDownTable(), true ));
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Card Type\",");
    line.append("\"IC Code\",");
    line.append("\"IC Desc\",");
    line.append("\"IC Rate\",");
    line.append("\"IC P/I\",");
    line.append("\"Statement Msg\",");
    line.append("\"Returns\",");
    line.append("\"Debit\",");
    line.append("\"Sales Count\",");
    line.append("\"Sales Amount\",");
    line.append("\"Credits Count\",");
    line.append("\"Credits Amount\",");
    line.append("\"IC Count\",");
    line.append("\"IC Amount\",");
    line.append("\"Billing Rate\",");
    line.append("\"Billing P/I\",");
    line.append("\"Fees Due\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    BetData record  = (BetData)obj;
  
    line.setLength(0);
    line.append( "\"" );
    line.append(record.CardType);
    line.append( "\",\"" );
    line.append(record.IcCode);
    line.append( "\",\"" );
    line.append(record.IcDesc);
    line.append( "\"," );
    line.append(record.Rate * 0.01);
    line.append( "," );
    line.append(record.PerItem);
    line.append( ",\"" );
    line.append(record.StatementMsg);
    line.append( "\",\"" );
    line.append(record.ApplyToReturns);
    line.append( "\",\"" );
    line.append(record.UseDbRates);
    line.append( "\"," );
    line.append(record.SalesCount);
    line.append( "," );
    line.append(record.SalesAmount);
    line.append( "," );
    line.append(record.CreditsCount);
    line.append( "," );
    line.append(record.CreditsAmount);
    line.append( "," );
    line.append(record.IcCount);
    line.append( "," );
    line.append(record.IcAmount);
    line.append( "," );
    line.append(record.BillingRate * 0.01);
    line.append( "," );
    line.append(record.BillingPerItem);
    line.append( "," );
    line.append(record.FeesDue);
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer    filename        = new StringBuffer("");
    Date            reportDate      = getReportDateBegin();
    
    filename.append(getInt("bankNumber"));
    filename.append("_");
    filename.append(getInt("betNumber"));
    filename.append("_");
    filename.append( DateTimeFormatter.getFormattedDate( reportDate, "MMddyyyy" ) );
    
    return ( filename.toString() );
  }
  
  public HashMap getBetVolume()
  {
    return(BetVolume);
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public boolean isEndDateInThePast()
  {
    return( EndDateIsInPast );
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    switch( getReportType() )
    {
      //case RT_SUMMARY:
      default:
        loadAnalysisData( orgId, beginDate, endDate );
        break;
    }        
  }
  
  public void loadAnalysisData( long orgId, Date beginDate, Date endDate )
  {
    int                 bankNumber        = getInt("bankNumber",3941);
    Date                betDate           = null;
    int                 betNumber         = getInt("betNumber",0);
    String              cardType          = getString("cardType","ALL");
    IcVolume            icVolume          = null;
    ResultSetIterator   it                = null;
    long                nodeId            = getLong("nodeId",0L);
    long                meLoadFileId      = 0L;
    ResultSet           resultSet         = null;
    BetData             row               = null;
    
    int                 creditsCount      = 5;
    double              creditsAmount     = 50.00;
    int                 salesCount        = 10;
    double              salesAmount       = 100.00;
    int                 netCount          = (salesCount - creditsCount);
    double              netAmount         = (salesAmount - creditsAmount);
    double              totalAmount       = 0.0;
    
    int[]               counts            = new int[3];
    double[]            amounts           = new double[3];
    HashMap             betData           = null;
    IcBillingData       betItem           = null;
    double              perItem           = 0.0;
    double              rate              = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:684^7*/

//  ************************************************************
//  #sql [Ctx] { select  last_day(:beginDate)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  last_day( :1 )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   betDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:689^7*/
      
      // empty the current contents
      ReportRows.clear();
      
      if ( BetVolume == null )
      {
        BetVolume = new HashMap();
      }
      BetVolume.clear();
      
      // only run the analysis if the bet exists
      int   recCnt  = 0;
      /*@lineinfo:generated-code*//*@lineinfo:702^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    mbs_bet_to_group_markup   bgm
//          where   bgm.bank_number = :bankNumber
//                  and bgm.bet_number = :betNumber
//                  and :betDate between bgm.valid_date_begin and bgm.valid_date_end
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    mbs_bet_to_group_markup   bgm\n        where   bgm.bank_number =  :1 \n                and bgm.bet_number =  :2 \n                and  :3  between bgm.valid_date_begin and bgm.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,betNumber);
   __sJT_st.setDate(3,betDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:710^7*/

      if ( recCnt > 0 )
      {
        betData = com.mes.mbs.BillingDb.loadIcBillingData(bankNumber,betNumber,betDate);
      
        if ( nodeId != 0L )
        {
          /*@lineinfo:generated-code*//*@lineinfo:718^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  icd.card_type,
//                      icd.issuer_ic_level,
//                      icd.ic_code,
//                      icd.ic_desc,
//                      icd.ic_rate,
//                      icd.ic_per_item,
//                      (icd.card_type || lpad(icd.ic_code,5,'0'))    as ic_label,
//                      icd.credits_only,
//                      icd.use_db_rates,
//                      sum(sm.credits_count)         as credits_count,
//                      sum(sm.credits_amount)        as credits_amount,
//                      sum(sm.sales_count)           as sales_count,
//                      sum(sm.sales_amount)          as sales_amount
//              from    mbs_daily_summary           sm,
//                      daily_detail_file_ic_desc   icd
//              where   sm.me_load_file_id = :meLoadFileId
//                      and sm.merchant_number in
//                      (
//                        select  gm.merchant_number
//                        from    organization        o,
//                                group_merchant      gm
//                        where   o.org_group = :nodeId
//                                and gm.org_num = o.org_num
//                      )
//                      and sm.item_type = 111
//                      and (:cardType = 'ALL' or :cardType like '%' || sm.item_subclass || '%' ) 
//                      and sm.activity_date between :beginDate-30 and :beginDate+45
//                      and icd.card_type = sm.item_subclass
//                      and icd.ic_code = sm.ic_cat
//                      and sm.activity_date between icd.valid_date_begin and icd.valid_date_end
//              group by icd.card_type,icd.issuer_ic_level,icd.ic_code,
//                        icd.ic_desc, icd.ic_rate,icd.ic_per_item,
//                        (icd.card_type || lpad(icd.ic_code,5,'0')),
//                        icd.credits_only,icd.use_db_rates
//              order by icd.card_type, icd.ic_desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  icd.card_type,\n                    icd.issuer_ic_level,\n                    icd.ic_code,\n                    icd.ic_desc,\n                    icd.ic_rate,\n                    icd.ic_per_item,\n                    (icd.card_type || lpad(icd.ic_code,5,'0'))    as ic_label,\n                    icd.credits_only,\n                    icd.use_db_rates,\n                    sum(sm.credits_count)         as credits_count,\n                    sum(sm.credits_amount)        as credits_amount,\n                    sum(sm.sales_count)           as sales_count,\n                    sum(sm.sales_amount)          as sales_amount\n            from    mbs_daily_summary           sm,\n                    daily_detail_file_ic_desc   icd\n            where   sm.me_load_file_id =  :1 \n                    and sm.merchant_number in\n                    (\n                      select  gm.merchant_number\n                      from    organization        o,\n                              group_merchant      gm\n                      where   o.org_group =  :2 \n                              and gm.org_num = o.org_num\n                    )\n                    and sm.item_type = 111\n                    and ( :3  = 'ALL' or  :4  like '%' || sm.item_subclass || '%' ) \n                    and sm.activity_date between  :5 -30 and  :6 +45\n                    and icd.card_type = sm.item_subclass\n                    and icd.ic_code = sm.ic_cat\n                    and sm.activity_date between icd.valid_date_begin and icd.valid_date_end\n            group by icd.card_type,icd.issuer_ic_level,icd.ic_code,\n                      icd.ic_desc, icd.ic_rate,icd.ic_per_item,\n                      (icd.card_type || lpad(icd.ic_code,5,'0')),\n                      icd.credits_only,icd.use_db_rates\n            order by icd.card_type, icd.ic_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.MbsIcBetDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,meLoadFileId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setString(3,cardType);
   __sJT_st.setString(4,cardType);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.MbsIcBetDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:755^11*/
        }
        else    // default to a full analysis
        {
          /*@lineinfo:generated-code*//*@lineinfo:759^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  icd.card_type,
//                      icd.issuer_ic_level,
//                      icd.ic_code,
//                      icd.ic_desc,
//                      icd.ic_rate,
//                      icd.ic_per_item,
//                      (icd.card_type || lpad(icd.ic_code,5,'0'))    as ic_label,
//                      icd.credits_only,
//                      icd.use_db_rates,
//                      5                   as credits_count,
//                      50.00               as credits_amount,
//                      10                  as sales_count,
//                      100.00              as sales_amount
//              from    daily_detail_file_ic_desc   icd
//              where   trunc(:betDate) between icd.valid_date_begin and icd.valid_date_end
//                      and not icd.issuer_ic_level is null
//                      and not lower(icd.issuer_ic_level) like '%dup'
//                      and (:cardType = 'ALL' or :cardType like '%' || icd.card_type || '%' ) 
//              order by icd.card_type, icd.ic_desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  icd.card_type,\n                    icd.issuer_ic_level,\n                    icd.ic_code,\n                    icd.ic_desc,\n                    icd.ic_rate,\n                    icd.ic_per_item,\n                    (icd.card_type || lpad(icd.ic_code,5,'0'))    as ic_label,\n                    icd.credits_only,\n                    icd.use_db_rates,\n                    5                   as credits_count,\n                    50.00               as credits_amount,\n                    10                  as sales_count,\n                    100.00              as sales_amount\n            from    daily_detail_file_ic_desc   icd\n            where   trunc( :1 ) between icd.valid_date_begin and icd.valid_date_end\n                    and not icd.issuer_ic_level is null\n                    and not lower(icd.issuer_ic_level) like '%dup'\n                    and ( :2  = 'ALL' or  :3  like '%' || icd.card_type || '%' ) \n            order by icd.card_type, icd.ic_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.MbsIcBetDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,betDate);
   __sJT_st.setString(2,cardType);
   __sJT_st.setString(3,cardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.MbsIcBetDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:780^11*/
        }
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          betItem = (IcBillingData)betData.get(resultSet.getString("ic_label"));
        
          row = new BetData();
          ReportRows.addElement(row);
          
          // establish the sales for this comparison
          creditsCount  = resultSet.getInt("credits_count");
          creditsAmount = resultSet.getDouble("credits_amount");
          salesCount    = resultSet.getInt("sales_count");
          salesAmount   = resultSet.getDouble("sales_amount");
          netCount      = (salesCount - creditsCount);
          netAmount     = (salesAmount - creditsAmount);
        
          // set the common data
          row.CardType  = resultSet.getString("card_type");
          row.IcCode    = resultSet.getString("ic_code");
          row.IcDesc    = resultSet.getString("ic_desc");
          row.Rate      = resultSet.getDouble("ic_rate");
          row.PerItem   = resultSet.getDouble("ic_per_item");
        
          if ( betItem != null ) 
          {
            if ( "Y".equals(resultSet.getString("credits_only")) )
            {
              counts[0] = 0;            amounts[0] = 0.0;           // sales
              counts[1] = creditsCount; amounts[1] = creditsAmount; // credits
            
              if ( betItem.ApplyToReturns == false )
              {
                counts[2] = 0;  amounts[2] = 0.0;
              }
              else
              {
                counts[2] = -creditsCount;  amounts[2] = -creditsAmount;
              }
            }
            else
            {
              counts[0]  = salesCount;
              amounts[0] = salesAmount;
              counts[1]  = creditsCount;
              amounts[1] = creditsAmount;
            
              if ( betItem.ApplyToReturns == true )
              {
                counts[2]  = netCount;
                amounts[2] = netAmount;
              }          
              else  // sales only
              {  
                counts[2]  = salesCount;
                amounts[2] = salesAmount;
              }              
            }
        
            boolean isDebitCard = "Y".equals(resultSet.getString("use_db_rates"));
          
            rate    = betItem.getRate();
            perItem = betItem.getPerItem();
        
            /*@lineinfo:generated-code*//*@lineinfo:846^13*/

//  ************************************************************
//  #sql [Ctx] { select  round(((:counts[2] * :perItem) + (:amounts[2] * :rate * 0.01)),2)
//                
//                from    dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1122 = counts[2];
 double __sJT_1123 = amounts[2];
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  round((( :1  *  :2 ) + ( :3  *  :4  * 0.01)),2)\n               \n              from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,__sJT_1122);
   __sJT_st.setDouble(2,perItem);
   __sJT_st.setDouble(3,__sJT_1123);
   __sJT_st.setDouble(4,rate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   totalAmount = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:851^13*/
          
            // set the billing data
            row.StatementMsg    = betItem.StatementMsg;
            row.ApplyToReturns  = betItem.ApplyToReturns ? "Y" : "N";
            row.UseDbRates      = isDebitCard ? "Y" : "N";
            row.SalesCount      = counts[0];
            row.SalesAmount     = amounts[0];
            row.CreditsCount    = counts[1];
            row.CreditsAmount   = amounts[1];
            row.IcCount         = counts[2];
            row.IcAmount        = amounts[2];
            row.BillingRate     = rate;
            row.BillingPerItem  = perItem;
            row.FeesDue         = totalAmount;
            
            if ( rate != 0.0 || perItem != 0.0 )
            {
              icVolume = (IcVolume)BetVolume.get(betItem.StatementMsg);
              if ( icVolume == null )
              {
                icVolume = new IcVolume(nodeId,betItem.StatementMsg);
                BetVolume.put(betItem.StatementMsg,icVolume);
              }
              icVolume.addVolume(row.IcCount,row.IcAmount,rate,perItem);
            }
          }
        }
        resultSet.close();
        it.close();
      }   // end if bet exists
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadData(" + beginDate + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadBetGroupData( )
  {
    int                 bankNumber        = getInt("bankNumber",3941);
    Date                betDate           = getReportDateBegin();
    int                 betNumber         = getInt("betNumber",0);
    String              cardType          = getString("cardType","ALL");
    ResultSetIterator   it                = null;
    ResultSet           resultSet         = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:904^7*/

//  ************************************************************
//  #sql [Ctx] { select  last_day(:betDate)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  last_day( :1 )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,betDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   betDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:909^7*/
      
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:914^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  icg.*,
//                  bgm.bank_number,
//                  bgm.bet_number,
//                  nvl(icd.ic_code,'*')    as ic_code_exp,
//                  icd.ic_desc             as ic_desc_exp
//          from    mbs_bet_to_group_markup   bgm,
//                  mbs_ic_groups             icg,
//                  daily_detail_file_ic_desc icd
//          where   bgm.bank_number = :bankNumber
//                  and bgm.bet_number = :betNumber
//                  and :betDate between bgm.valid_date_begin and bgm.valid_date_end
//                  and icg.group_id = bgm.group_id
//                  and :betDate between icg.valid_date_begin and icg.valid_date_end
//                  and (:cardType = 'ALL' or :cardType like '%' || icg.plan_type || '%' ) 
//                  and icd.card_type(+) = icg.plan_type
//                  and icd.ic_code(+) = icg.ic_code
//                  and :betDate between icd.valid_date_begin(+) and icd.valid_date_end(+)
//          order by icg.plan_type, icg.group_label, icd.ic_desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  icg.*,\n                bgm.bank_number,\n                bgm.bet_number,\n                nvl(icd.ic_code,'*')    as ic_code_exp,\n                icd.ic_desc             as ic_desc_exp\n        from    mbs_bet_to_group_markup   bgm,\n                mbs_ic_groups             icg,\n                daily_detail_file_ic_desc icd\n        where   bgm.bank_number =  :1 \n                and bgm.bet_number =  :2 \n                and  :3  between bgm.valid_date_begin and bgm.valid_date_end\n                and icg.group_id = bgm.group_id\n                and  :4  between icg.valid_date_begin and icg.valid_date_end\n                and ( :5  = 'ALL' or  :6  like '%' || icg.plan_type || '%' ) \n                and icd.card_type(+) = icg.plan_type\n                and icd.ic_code(+) = icg.ic_code\n                and  :7  between icd.valid_date_begin(+) and icd.valid_date_end(+)\n        order by icg.plan_type, icg.group_label, icd.ic_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.MbsIcBetDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,betNumber);
   __sJT_st.setDate(3,betDate);
   __sJT_st.setDate(4,betDate);
   __sJT_st.setString(5,cardType);
   __sJT_st.setString(6,cardType);
   __sJT_st.setDate(7,betDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.reports.MbsIcBetDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:934^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement(new BetGroupData(resultSet));
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadBetGroupData(" + betNumber + "," + betDate + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadBetMarkupData( )
  {
    int                 bankNumber        = getInt("bankNumber",3941);
    Date                betDate           = getReportDateBegin();
    int                 betNumber         = getInt("betNumber",0);
    ResultSetIterator   it                = null;
    ResultSet           resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:967^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  icm.*,
//                  bgm.bank_number,
//                  bgm.bet_number
//          from    mbs_bet_to_group_markup   bgm,
//                  mbs_ic_markup             icm
//          where   bgm.bank_number = :bankNumber
//                  and bgm.bet_number = :betNumber
//                  and :betDate between bgm.valid_date_begin and bgm.valid_date_end
//                  and icm.markup_id = bgm.markup_id
//                  and :betDate between icm.valid_date_begin and icm.valid_date_end
//          order by icm.group_label
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  icm.*,\n                bgm.bank_number,\n                bgm.bet_number\n        from    mbs_bet_to_group_markup   bgm,\n                mbs_ic_markup             icm\n        where   bgm.bank_number =  :1 \n                and bgm.bet_number =  :2 \n                and  :3  between bgm.valid_date_begin and bgm.valid_date_end\n                and icm.markup_id = bgm.markup_id\n                and  :4  between icm.valid_date_begin and icm.valid_date_end\n        order by icm.group_label";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.MbsIcBetDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,betNumber);
   __sJT_st.setDate(3,betDate);
   __sJT_st.setDate(4,betDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.MbsIcBetDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:980^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement(new BetMarkupData(resultSet));
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadBetMarkupData(" + betNumber + "," + betDate + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadBetData( )
  {
    int                 bankNumber        = getInt("bankNumber",0);
    Date                betDate           = getReportDateBegin();
    int                 betNumber         = getInt("betNumber",0);
    ResultSetIterator   it                = null;
    ResultSet           resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();

      /*@lineinfo:generated-code*//*@lineinfo:1013^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bgm.*
//          from    mbs_bet_to_group_markup   bgm
//          where   bgm.bank_number = :bankNumber
//                  and bgm.bet_number = :betNumber
//                  and :betDate between bgm.valid_date_begin and bgm.valid_date_end
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bgm.*\n        from    mbs_bet_to_group_markup   bgm\n        where   bgm.bank_number =  :1 \n                and bgm.bet_number =  :2 \n                and  :3  between bgm.valid_date_begin and bgm.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.MbsIcBetDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,betNumber);
   __sJT_st.setDate(3,betDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.reports.MbsIcBetDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1020^7*/
      resultSet = it.getResultSet();
      
      if( resultSet.next() )
      {
        ReportRows.addElement(new BetToGroupMarkupData(resultSet));
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadBetToGroupMarkupData(" + betNumber + "," + betDate + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadBetListData( )
  {
    int                 bankNumber        = getInt("bankNumber",3941);
    Date                betDate           = getReportDateBegin();
    ResultSetIterator   it                = null;
    ResultSet           resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:1052^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  case when (:betDate between bgm.valid_date_begin and bgm.valid_date_end)
//                       then 'Y' else 'N' end as open_on_report_date,
//                  bgm.*
//          from    mbs_bet_to_group_markup   bgm
//          where   bgm.bank_number = :bankNumber
//          order by bgm.bet_number, bgm.valid_date_end, bgm.bank_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  case when ( :1  between bgm.valid_date_begin and bgm.valid_date_end)\n                     then 'Y' else 'N' end as open_on_report_date,\n                bgm.*\n        from    mbs_bet_to_group_markup   bgm\n        where   bgm.bank_number =  :2 \n        order by bgm.bet_number, bgm.valid_date_end, bgm.bank_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.MbsIcBetDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,betDate);
   __sJT_st.setInt(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.reports.MbsIcBetDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1060^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement(new BetListData(resultSet));
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadBetListData(" + bankNumber + "," + betDate + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadBetPricingData( )
  {
    int                 bankNumber        = getInt("bankNumber",3941);
    Date                betDate           = getReportDateBegin();
    int                 betNumber         = getInt("betNumber",0);
    ResultSetIterator   it                = null;
    ResultSet           resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:1093^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  :bankNumber as bank_number,
//                  :betNumber  as bet_number,
//                  mp.*
//          from    mbs_pricing               mp
//          where   mp.merchant_number = (:bankNumber || lpad(:betNumber,4,'0'))
//                  and :betDate between mp.valid_date_begin and mp.valid_date_end
//          order by mp.item_type, mp.item_subclass
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1  as bank_number,\n                 :2   as bet_number,\n                mp.*\n        from    mbs_pricing               mp\n        where   mp.merchant_number = ( :3  || lpad( :4 ,4,'0'))\n                and  :5  between mp.valid_date_begin and mp.valid_date_end\n        order by mp.item_type, mp.item_subclass";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.MbsIcBetDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,betNumber);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setInt(4,betNumber);
   __sJT_st.setDate(5,betDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.reports.MbsIcBetDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1102^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement(new BetPricingData(resultSet));
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadBetPricingData(" + betNumber + "," + betDate + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest( request );
  
    // force "begin date" to first day of the selected (or default) month
    Calendar  cal = Calendar.getInstance();
    cal.setTime( getReportDateBegin() );
    cal.set( Calendar.DAY_OF_MONTH, 1 );
    setReportDateBegin( new java.sql.Date(cal.getTime().getTime()) );

    // if first day of next month is in the past, the bet ends in the past
    cal.clear(Calendar.HOUR);
    cal.clear(Calendar.HOUR_OF_DAY);
    cal.clear(Calendar.MINUTE);
    cal.clear(Calendar.SECOND);
    cal.clear(Calendar.MILLISECOND);
    cal.add( Calendar.MONTH, +1 );
    EndDateIsInPast = cal.before( Calendar.getInstance() );
    
    if ( getInt("betNumber",0) == 0 && getLong("nodeId",0L) != 0L )
    {
      try
      {
        int betNumber   = 0;
        int bankNumber  = 0;
        /*@lineinfo:generated-code*//*@lineinfo:1147^9*/

//  ************************************************************
//  #sql [Ctx] { select  ic_bet_visa,bank_number
//            
//            from    mif   
//            where   merchant_number = :getLong("nodeId")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1124 = getLong("nodeId");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ic_bet_visa,bank_number\n           \n          from    mif   \n          where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.MbsIcBetDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1124);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   betNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   bankNumber = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1153^9*/
        if ( betNumber != 0 )
        {
          setData("betNumber",String.valueOf(betNumber));
          setData("bankNumber",String.valueOf(bankNumber));
        }
      }
      catch( java.sql.SQLException sqle )
      {
        // ignore, not that important
      }
    }

    // force 3943 to 3941
    if ( getInt("bankNumber",0) == 3943 )
    {
      setData("bankNumber","3941");
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/