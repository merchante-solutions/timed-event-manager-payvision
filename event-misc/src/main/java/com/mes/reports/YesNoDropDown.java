/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CQAppTypeDropDown.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 4/20/04 4:09p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import com.mes.tools.DropDownTable;

public class YesNoDropDown extends DropDownTable
{
  public YesNoDropDown()
  {
  }
  
  public void initialize( Object[] params )
  {
    addElement("y", "Yes");
    addElement("n", "No");
  }
}