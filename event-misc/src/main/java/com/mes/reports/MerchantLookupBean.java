/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchantLookupBean.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 1/04/01 3:31p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MerchantLookupBean extends ReportDataBean
{
  // private data members
  private long                DdaNumber             = -1;
  private ResultSet           Results               = null;
  private int                 SearchCriteria        = -1;
  private PreparedStatement   Statement             = null;
  private long                TransitRoutingNumber  = -1;
  
  public void cleanUp()
  {
    try { Results.close(); } catch(Exception e) { }
    try { Statement.close(); } catch(Exception e) { }
    
    super.cleanUp();
  }
  
  public String getDdaNumber()
  {
    String result = "";
    if( DdaNumber != -1 )
    {
      result = Long.toString( DdaNumber );
    }
    return result;
  }

  public ResultSet getResultSet()
  {
    StringBuffer        qs            = new StringBuffer("");
  
    try
    {
      // get connection
      qs.setLength(0);
      qs.append("select merchant_number, dba_name ");
      qs.append("from mif ");
      qs.append("where transit_routng_num = ? and dda_num = ? order by merchant_number ");
      
      // make sure that any existing statement gets closed
      // ignore any errors.
      try { Results.close(); } catch(Exception e) { }
      try { Statement.close(); } catch(Exception e) { }

      // prepared the new statement
      Statement  = getPreparedStatement( qs.toString() );
      Statement.setLong(1, TransitRoutingNumber);
      Statement.setLong(2, DdaNumber);
  
      Results = Statement.executeQuery();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      addError("getData: " + e.toString());
    }
    return( Results );
  }  
  
  public int getSearchCriteria()
  {
    int result = 0;
    if( SearchCriteria != -1 )
    {
      result = SearchCriteria;
    }
    return( result );
  }
  
  public String getTransitRoutingNumber()
  {
    String result = "";
    if( TransitRoutingNumber != -1 )
    {
      result = Long.toString( TransitRoutingNumber );
    }
    return( result );
  }

  public void setDdaNumber(String ddaNumber)
  {
    try
    {
      DdaNumber = Long.parseLong(ddaNumber); 
    }
    catch(Exception e)
    {
      DdaNumber = -1;
    }
  }
  
  public void setSearchCriteria(String searchCriteria)
  {
    try
    {
      SearchCriteria = Integer.parseInt(searchCriteria); 
    }
    catch(Exception e)
    {
      SearchCriteria = -1;
    }
  }

  public void setTransitRoutingNumber(String transitRoutingNumber)
  {
    try
    {
      TransitRoutingNumber = Long.parseLong(transitRoutingNumber); 
    }
    catch(Exception e)
    {
      TransitRoutingNumber = -1;
    }
  }

  public boolean validate()
  {
    if( TransitRoutingNumber == -1 )
    {
      addError( "Please enter a valid Transit Routing Number" );
    }
    if( DdaNumber == -1)
    {
      addError( "Please enter a valid DDA Number" );
    }
    return( !hasErrors() );
  }
}
