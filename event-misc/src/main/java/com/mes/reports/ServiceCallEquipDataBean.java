/*@lineinfo:filename=ServiceCallEquipDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ServiceCallEquipDataBean.sqlj $

  Description:  
  
  Extends the date range report bean to generate a sales activity report.

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 12/11/01 2:14p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class ServiceCallEquipDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public String     AppId         = null;
    public String     Disposition   = null;
    public double     EquipCost     = 0.0;
    public String     EquipDesc     = null;
    public Date       InstallDate   = null;
    public String     VNumber       = null;
    
    public RowData( ResultSet resultSet )
    {
      try
      {
         AppId         =  resultSet.getString("app_id");
         Disposition   =  resultSet.getString("disposition");
         EquipCost     =  resultSet.getDouble("equip_cost");
         EquipDesc     =  resultSet.getString("equip_desc");
         InstallDate   =  resultSet.getDate("install_date");
         VNumber       =  resultSet.getString("v_number");
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::constructor", e.toString());
      }
    }
  }
  
  public ServiceCallEquipDataBean()
  {
  }
  
  public void loadData( long merchantId )
  {
    setReportHierarchyNode(merchantId);
    loadData();
  }
  
  public void loadData()
  {
    ResultSetIterator     it      = null;
    try
    {
      ReportRows.removeAllElements();
      
      /*@lineinfo:generated-code*//*@lineinfo:82^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ips.SERIAL_NUMBER                           as app_id,
//                  nvl(eq.equip_descriptor,ips.terminal_type)  as equip_desc,
//                  ips.install_date                            as install_date,
//                  ips.COST                                    as equip_cost,
//                  substr(ips.disposition,1,1)                 as disposition,
//                  decode( instr(ips.disposition,'V'),
//                          0,null,
//                          substr(ips.disposition,
//                                  instr(ips.disposition,'V'),
//                                 length(ips.disposition)) )   as v_number        
//          from    ips_file    ips,
//                  equipment   eq
//          where   ips.merchant_number = :getReportHierarchyNode() and
//                  eq.equip_model(+)   = ips.TERMINAL_TYPE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1300 = getReportHierarchyNode();
  try {
   String theSqlTS = "select  ips.SERIAL_NUMBER                           as app_id,\n                nvl(eq.equip_descriptor,ips.terminal_type)  as equip_desc,\n                ips.install_date                            as install_date,\n                ips.COST                                    as equip_cost,\n                substr(ips.disposition,1,1)                 as disposition,\n                decode( instr(ips.disposition,'V'),\n                        0,null,\n                        substr(ips.disposition,\n                                instr(ips.disposition,'V'),\n                               length(ips.disposition)) )   as v_number        \n        from    ips_file    ips,\n                equipment   eq\n        where   ips.merchant_number =  :1  and\n                eq.equip_model(+)   = ips.TERMINAL_TYPE";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ServiceCallEquipDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1300);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ServiceCallEquipDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:98^7*/
      
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {
        ReportRows.add(new RowData(rs));
      }
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }
  }
}/*@lineinfo:generated-code*/