/*@lineinfo:filename=MerchICQualDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchICQualDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-01-10 11:28:45 -0800 (Wed, 10 Jan 2007) $
  Version            : $Revision: 13279 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class MerchICQualDataBean extends OrgSummaryDataBean
{
  private static final String UrlReplacements = "!\"#$,%&'()*+,-./:;<=>@";
  
  public class DetailRow
  {
    public String                     AuthCode            = null;
    public Date                       BatchDate           = null;
    public String                     BetStatementMsg     = null;
    public String                     CardNumber          = null;
    public String                     CardType            = null;
    public String                     DbaName             = null;
    public String                     EntryMode           = null;
    public String                     IcDesc              = null;
    public long                       MerchantId          = 0L;
    public String                     PurchaseId          = null;
    public String                     ReferenceNumber     = null;
    public double                     TranAmount          = 0.0;
    public Date                       TranDate            = null;
    
    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      AuthCode            = processString( resultSet.getString("auth_code") );
      BatchDate           = resultSet.getDate("batch_date");
      BetStatementMsg     = processString( resultSet.getString("statement_msg") );
      CardNumber          = processString( resultSet.getString("card_number") );
      CardType            = processString( resultSet.getString("card_type") );
      DbaName             = processString( resultSet.getString("dba_name") );
      EntryMode           = processString( resultSet.getString("entry_mode") );
      IcDesc              = processString( resultSet.getString("ic_desc") );
      MerchantId          = resultSet.getLong("merchant_number");
      PurchaseId          = processString( resultSet.getString("purchase_id") );
      ReferenceNumber     = processString( resultSet.getString("ref_num") );
      TranAmount          = resultSet.getDouble("tran_amount");
      TranDate            = resultSet.getDate("tran_date");
    }
  }
  
  public static class QualSummary
    implements Comparable
  {
    public String       BetGroup          = null;
    public String       CardType          = null;
    public long         HierarchyNode     = 0L;
    public String       OrgName           = null;
    public long         OrgId             = 0L;
    public double       SalesAmount       = 0.0;
    public int          SalesCount        = 0;
    public String       StatementKey      = null;
    public String       StatementMsg      = null;
    
    public QualSummary( ResultSet resultSet )
      throws java.sql.SQLException
    {
      char          ch;
      StringBuffer  temp = new StringBuffer();
      
      
      CardType      = resultSet.getString("card_type");
      OrgName       = resultSet.getString("org_name");
      BetGroup      = resultSet.getString("bet_group");
      if ( BetGroup == null )
      {
        BetGroup = "";
      }        
      StatementMsg  = resultSet.getString("statement_msg");
      HierarchyNode = resultSet.getLong("node_id");
      OrgId         = resultSet.getLong("org_num");
      SalesAmount   = resultSet.getDouble("sales_amount");
      SalesCount    = resultSet.getInt("sales_count");
      
      for( int i = 0; i < StatementMsg.length(); ++i )
      {
        ch = StatementMsg.charAt(i);
        if ( UrlReplacements.indexOf(ch) >= 0 )
        {
          temp.append("%");
          temp.append(Integer.toHexString((int)ch));
        }
        else
        {
          temp.append(ch);
        }
      }
      StatementKey = temp.toString();
    }
    
    public int compareTo( Object obj )
    {
      QualSummary     compareObj    = (QualSummary)obj;
      int             retVal        = 0;
      
      if ( (retVal = OrgName.compareTo(compareObj.OrgName)) == 0 )
      {
        if ( (retVal = (int)(HierarchyNode - compareObj.HierarchyNode)) == 0 )
        {
          if ( (retVal = CardType.compareTo(compareObj.CardType)) == 0 )
          {
            if ( (retVal = BetGroup.compareTo(compareObj.BetGroup)) == 0 )
            {
              retVal = StatementMsg.compareTo(compareObj.StatementMsg);
            }
          }
        }
      }
      return( retVal );
    }
    
    public boolean hasCategoryChanged( QualSummary lastCategory )
    {
      boolean     retVal    = false;
      
      {
        if ( (lastCategory == null) ||
             !CardType.equals(lastCategory.CardType) ||
             !BetGroup.equals(lastCategory.BetGroup) ||
             !StatementMsg.equals(lastCategory.StatementMsg) )
        {
          retVal = true;
        }             
        return( retVal );
      }
    }
  }
  
  private String        BetStatementMsg       = null;
  
  public MerchICQualDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_SUMMARY )
    {
      line.append("\"Node ID\",");
      line.append("\"Org Name\",");
      line.append("\"Card Type\",");
      line.append("\"Statement Message\",");
      line.append("\"Sales Count\",");
      line.append("\"Sales Amount\"");
    }
    else // RT_DETAILS
    {
      line.append("\"Merchant Id\",");
      line.append("\"DBA Name\",");
      line.append("\"Batch Date\",");
      line.append("\"Tran Date\",");
      line.append("\"CT\",");
      line.append("\"Qual Desc\",");
      line.append("\"Card Number\",");
      line.append("\"Tran Amount\",");
      line.append("\"Auth Code\",");
      line.append("\"Entry Mode\",");
      line.append("\"Purchase ID\",");
      line.append("\"Reference\"");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);
  
    if ( ReportType == RT_SUMMARY )
    {
      QualSummary               summary = (QualSummary)obj;
      
      line.append( encodeHierarchyNode(summary.HierarchyNode) );
      line.append( ",\"" );
      line.append( summary.OrgName );
      line.append( "\",\"" );
      line.append( summary.CardType );
      line.append( "\",\"" );
      line.append( summary.StatementMsg );
      line.append( "\"," );
      line.append( summary.SalesCount );
      line.append( "," );
      line.append( summary.SalesAmount );
    }
    else      // RT_DETAILS
    {
      DetailRow       record    = (DetailRow)obj;
    
      line.append( encodeHierarchyNode(record.MerchantId) );
      line.append( ",\"" );
      line.append( record.DbaName );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate( record.BatchDate, "MM/dd/yyyy" ) );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate( record.TranDate, "MM/dd/yyyy" ) );
      line.append( ",\"" );
      line.append( record.CardType );
      line.append( "\",\"" );
      line.append( record.BetStatementMsg );
      line.append( "\",\"" );
      line.append( record.CardNumber );
      line.append( "\"," );
      line.append( record.TranAmount );
      line.append( ",\"" );
      line.append( record.AuthCode );
      line.append( "\",\"" );
      line.append( record.EntryMode );
      line.append( "\",\"" );
      line.append( record.PurchaseId );
      line.append( "\",\"'" );
      line.append( record.ReferenceNumber );
      line.append( "\"" );
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    if ( ReportType == RT_SUMMARY )
    {
      filename.append("_ic_qual_summary_");
    }
    else    // RT_DETAILS
    {
      filename.append("_ic_qual_details_");
    }      
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it          = null;
    long                          nodeId      = orgIdToHierarchyNode(orgId);
    ResultSet                     resultSet   = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:312^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      FIRST_ROWS INDEX_FFS (dt idx_ddf_dt_bdate_merch_acct) 
//                      INDEX_FFS (sm pk_monthly_extract_summary) 
//                  */
//                  sm.merchant_number                  as merchant_number,
//                  mf.dba_name                         as dba_name,
//                  dt.batch_date                       as batch_date,
//                  decode( bet.enabled,
//                          'Y',bet.print_group,
//                          null )                      as bet_group,
//                  decode( bet.enabled,
//                          'Y', bet.STATEMENT_MESSAGE,
//                          decode(substr(dt.card_type,1,1),'V','VISA','MC') || ' QUALIFIED' )  
//                                                      as statement_msg,
//                  nvl(icd.IC_DESC,
//                      'Undefined (' || dt.submitted_interchange2 ||')' )                     
//                                                      as ic_desc,
//                  decode(substr(dt.card_type,1,1),
//                         'V','VS','MC')               as card_type,
//                  dt.transaction_date                 as tran_date,
//                  dt.cardholder_account_number        as card_number,
//                  dt.transaction_amount               as tran_amount,
//                  dt.authorization_num                as auth_code,
//                  dt.purchase_id                      as purchase_id,
//                  dt.reference_number                 as ref_num,
//                  nvl(emd.POS_ENTRY_DESC,dt.pos_entry_mode) as entry_mode
//          from    organization                  o,
//                  group_merchant                gm,
//                  monthly_extract_summary       sm,
//                  monthly_extract_visa          vs,
//                  monthly_extract_mc            mc,
//                  daily_detail_file_dt          dt,
//                  daily_detail_file_ic_desc     icd,
//                  tsys_bet_file                 bet,
//                  pos_entry_mode_desc           emd, 
//                  mif                           mf
//          where   o.org_group = :nodeId and
//                  gm.org_num = o.org_num and
//                  sm.merchant_number = gm.merchant_number and
//                  sm.active_date = :beginDate and
//                  vs.hh_load_sec(+) = sm.hh_load_sec and
//                  mc.hh_load_sec(+) = sm.hh_load_sec and
//                  dt.merchant_account_number = sm.merchant_number and
//                  dt.batch_date between sm.MONTH_BEGIN_DATE+1 and sm.MONTH_END_DATE+1 and
//                  dt.debit_credit_indicator = 'D' and
//                  substr(dt.card_type,1,1) in ('V','M') and 
//                  icd.card_type = decode(substr(dt.card_type,1,1),'V','VS','MC') and 
//                  icd.IC_CODE = dt.SUBMITTED_INTERCHANGE2 and
//                  dt.batch_date between icd.valid_date_begin and icd.valid_date_end and
//                  bet.bank_number = sm.bank_number and
//                  bet.active_date = sm.active_date and
//                  bet.bet_number = ( decode( substr(dt.card_type,1,1),
//                                             'V',vs.v1_bet_table_number,
//                                             'M',mc.m1_bet_table_number,
//                                             0 ) ) and
//                  bet.segment_id = icd.BET_SEGMENT_ID and
//                  bet.segment_type = 1 and
//                  ( :BetStatementMsg is null or
//                    decode( bet.enabled,
//                            'Y', bet.statement_message,
//                            decode(substr(dt.card_type,1,1),'V','VISA','MC') || ' QUALIFIED' ) = :BetStatementMsg
//                  ) and                
//                  emd.pos_entry_code(+) = dt.pos_entry_mode and
//                  mf.merchant_number = sm.merchant_number
//          order by bet.print_group, bet.statement_message, dt.batch_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    FIRST_ROWS INDEX_FFS (dt idx_ddf_dt_bdate_merch_acct) \n                    INDEX_FFS (sm pk_monthly_extract_summary) \n                */\n                sm.merchant_number                  as merchant_number,\n                mf.dba_name                         as dba_name,\n                dt.batch_date                       as batch_date,\n                decode( bet.enabled,\n                        'Y',bet.print_group,\n                        null )                      as bet_group,\n                decode( bet.enabled,\n                        'Y', bet.STATEMENT_MESSAGE,\n                        decode(substr(dt.card_type,1,1),'V','VISA','MC') || ' QUALIFIED' )  \n                                                    as statement_msg,\n                nvl(icd.IC_DESC,\n                    'Undefined (' || dt.submitted_interchange2 ||')' )                     \n                                                    as ic_desc,\n                decode(substr(dt.card_type,1,1),\n                       'V','VS','MC')               as card_type,\n                dt.transaction_date                 as tran_date,\n                dt.cardholder_account_number        as card_number,\n                dt.transaction_amount               as tran_amount,\n                dt.authorization_num                as auth_code,\n                dt.purchase_id                      as purchase_id,\n                dt.reference_number                 as ref_num,\n                nvl(emd.POS_ENTRY_DESC,dt.pos_entry_mode) as entry_mode\n        from    organization                  o,\n                group_merchant                gm,\n                monthly_extract_summary       sm,\n                monthly_extract_visa          vs,\n                monthly_extract_mc            mc,\n                daily_detail_file_dt          dt,\n                daily_detail_file_ic_desc     icd,\n                tsys_bet_file                 bet,\n                pos_entry_mode_desc           emd, \n                mif                           mf\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                sm.merchant_number = gm.merchant_number and\n                sm.active_date =  :2  and\n                vs.hh_load_sec(+) = sm.hh_load_sec and\n                mc.hh_load_sec(+) = sm.hh_load_sec and\n                dt.merchant_account_number = sm.merchant_number and\n                dt.batch_date between sm.MONTH_BEGIN_DATE+1 and sm.MONTH_END_DATE+1 and\n                dt.debit_credit_indicator = 'D' and\n                substr(dt.card_type,1,1) in ('V','M') and \n                icd.card_type = decode(substr(dt.card_type,1,1),'V','VS','MC') and \n                icd.IC_CODE = dt.SUBMITTED_INTERCHANGE2 and\n                dt.batch_date between icd.valid_date_begin and icd.valid_date_end and\n                bet.bank_number = sm.bank_number and\n                bet.active_date = sm.active_date and\n                bet.bet_number = ( decode( substr(dt.card_type,1,1),\n                                           'V',vs.v1_bet_table_number,\n                                           'M',mc.m1_bet_table_number,\n                                           0 ) ) and\n                bet.segment_id = icd.BET_SEGMENT_ID and\n                bet.segment_type = 1 and\n                (  :3  is null or\n                  decode( bet.enabled,\n                          'Y', bet.statement_message,\n                          decode(substr(dt.card_type,1,1),'V','VISA','MC') || ' QUALIFIED' ) =  :4 \n                ) and                \n                emd.pos_entry_code(+) = dt.pos_entry_mode and\n                mf.merchant_number = sm.merchant_number\n        order by bet.print_group, bet.statement_message, dt.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchICQualDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setString(3,BetStatementMsg);
   __sJT_st.setString(4,BetStatementMsg);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchICQualDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:379^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        ReportRows.add( new DetailRow( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          merchantId        = 0L;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( (merchantId = getReportMerchantId()) != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:411^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                      index (sm pk_monthly_extract_summary)
//                      index (ic PK_DAILY_DETAIL_IC_SUMMARY)
//                      index (bet idx_tsys_bet_bank_date_num)
//                    */
//                    o.org_num                               as org_num,
//                    mf.merchant_number                      as node_id,
//                    mf.dba_name                             as org_name,
//                    decode(substr(ic.card_type,1,1),
//                           'V','Visa',
//                           'MasterCard')                    as card_type,
//                    decode( bet.enabled,
//                            'Y',bet.print_group,
//                            null )                          as bet_group,
//                    decode( bet.enabled,
//                            'Y', bet.STATEMENT_MESSAGE,
//                            decode(substr(ic.card_type,1,1),'V','VISA','MC') || 
//                            ' QUALIFIED' )                  as statement_msg,
//                    sum( ic.sales_count )                   as sales_count,
//                    sum( ic.sales_amount )                  as sales_amount
//            from    mif                           mf,
//                    monthly_extract_summary       sm,
//                    daily_detail_ic_summary       ic,
//                    daily_detail_file_ic_desc     icd,                
//                    tsys_bet_file                 bet,
//                    organization                  o        
//            where   mf.merchant_number = :merchantId and
//                    sm.merchant_number = mf.merchant_number and
//                    sm.active_date = :beginDate and 
//                    ic.merchant_number = sm.merchant_number and
//                    ic.batch_date between sm.month_begin_date+1 and sm.month_end_date+1 and
//                    substr(ic.card_type,1,1) in ( 'V','M' ) and
//                    nvl(ic.sales_count,0) > 0 and                
//                    icd.card_type = decode(substr(ic.card_type,1,1),'V','VS','MC') and
//                    icd.IC_CODE = ic.ic_cat and
//                    ic.batch_date between icd.valid_date_begin and icd.valid_date_end and
//                    bet.bank_number = sm.bank_number and
//                    bet.active_date = sm.active_date and
//                    bet.bet_number = decode( substr(ic.card_type,1,1),
//                                             'V', sm.visa_ic_bet_number,
//                                             sm.mc_ic_bet_number ) and
//                    bet.segment_type = 1 and
//                    bet.segment_id = icd.BET_SEGMENT_ID and        
//                    o.org_group = mf.merchant_number        
//            group by  mf.dba_name, mf.merchant_number, o.org_num, 
//                       substr(ic.card_type,1,1),
//                      decode( bet.enabled,
//                              'Y',bet.print_group,
//                              null ),
//                      decode( bet.enabled,
//                              'Y', bet.STATEMENT_MESSAGE,
//                              decode(substr(ic.card_type,1,1),'V','VISA','MC') || 
//                              ' QUALIFIED' )        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                    index (sm pk_monthly_extract_summary)\n                    index (ic PK_DAILY_DETAIL_IC_SUMMARY)\n                    index (bet idx_tsys_bet_bank_date_num)\n                  */\n                  o.org_num                               as org_num,\n                  mf.merchant_number                      as node_id,\n                  mf.dba_name                             as org_name,\n                  decode(substr(ic.card_type,1,1),\n                         'V','Visa',\n                         'MasterCard')                    as card_type,\n                  decode( bet.enabled,\n                          'Y',bet.print_group,\n                          null )                          as bet_group,\n                  decode( bet.enabled,\n                          'Y', bet.STATEMENT_MESSAGE,\n                          decode(substr(ic.card_type,1,1),'V','VISA','MC') || \n                          ' QUALIFIED' )                  as statement_msg,\n                  sum( ic.sales_count )                   as sales_count,\n                  sum( ic.sales_amount )                  as sales_amount\n          from    mif                           mf,\n                  monthly_extract_summary       sm,\n                  daily_detail_ic_summary       ic,\n                  daily_detail_file_ic_desc     icd,                \n                  tsys_bet_file                 bet,\n                  organization                  o        \n          where   mf.merchant_number =  :1  and\n                  sm.merchant_number = mf.merchant_number and\n                  sm.active_date =  :2  and \n                  ic.merchant_number = sm.merchant_number and\n                  ic.batch_date between sm.month_begin_date+1 and sm.month_end_date+1 and\n                  substr(ic.card_type,1,1) in ( 'V','M' ) and\n                  nvl(ic.sales_count,0) > 0 and                \n                  icd.card_type = decode(substr(ic.card_type,1,1),'V','VS','MC') and\n                  icd.IC_CODE = ic.ic_cat and\n                  ic.batch_date between icd.valid_date_begin and icd.valid_date_end and\n                  bet.bank_number = sm.bank_number and\n                  bet.active_date = sm.active_date and\n                  bet.bet_number = decode( substr(ic.card_type,1,1),\n                                           'V', sm.visa_ic_bet_number,\n                                           sm.mc_ic_bet_number ) and\n                  bet.segment_type = 1 and\n                  bet.segment_id = icd.BET_SEGMENT_ID and        \n                  o.org_group = mf.merchant_number        \n          group by  mf.dba_name, mf.merchant_number, o.org_num, \n                     substr(ic.card_type,1,1),\n                    decode( bet.enabled,\n                            'Y',bet.print_group,\n                            null ),\n                    decode( bet.enabled,\n                            'Y', bet.STATEMENT_MESSAGE,\n                            decode(substr(ic.card_type,1,1),'V','VISA','MC') || \n                            ' QUALIFIED' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchICQualDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchICQualDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:466^9*/
      }
      else    // standard org child situation
      {
        /*@lineinfo:generated-code*//*@lineinfo:470^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                      index (sm pk_monthly_extract_summary)
//                      index (ic PK_DAILY_DETAIL_IC_SUMMARY)
//                      index (bet idx_tsys_bet_bank_date_num)
//                      ordered
//                      use_nl(sm ic)
//                    */
//                    o.org_num                           as org_num,
//                    o.org_group                         as node_id,
//                    o.org_name                          as org_name,
//                    decode(substr(ic.card_type,1,1),
//                           'V','Visa',
//                           'MasterCard')                as card_type,
//                    decode( bet.enabled,
//                            'Y',bet.print_group,
//                            null )                      as bet_group,
//                    decode( bet.enabled,
//                            'Y', bet.STATEMENT_MESSAGE,
//                            decode(substr(ic.card_type,1,1),'V','VISA','MC') || 
//                            ' QUALIFIED' )              as statement_msg,
//                    sum( ic.sales_count )               as sales_count,
//                    sum( ic.sales_amount )              as sales_amount
//            from    parent_org                    po,
//                    group_merchant                gm,
//                    monthly_extract_summary       sm,
//                    daily_detail_ic_summary       ic,
//                    daily_detail_file_ic_desc     icd,                
//                    tsys_bet_file                 bet,
//                    organization                  o        
//            where   po.parent_org_num = :orgId and
//                    gm.org_num = po.org_num and
//                    sm.merchant_number = gm.merchant_number and
//                    sm.active_date = :beginDate and
//                    ic.merchant_number = sm.merchant_number and
//                    ic.batch_date between sm.month_begin_date+1 and sm.month_end_date+1 and
//                    substr(ic.card_type,1,1) in ( 'V','M' ) and
//                    nvl(ic.sales_count,0) > 0 and                
//                    icd.card_type = decode(substr(ic.card_type,1,1),'V','VS','MC') and
//                    icd.IC_CODE = ic.ic_cat and
//                    ic.batch_date between icd.valid_date_begin and icd.valid_date_end and
//                    bet.bank_number = sm.bank_number and
//                    bet.active_date = sm.active_date and
//                    bet.bet_number = decode( substr(ic.card_type,1,1),
//                                             'V', sm.visa_ic_bet_number,
//                                             sm.mc_ic_bet_number ) and
//                    bet.segment_type = 1 and
//                    bet.segment_id = icd.BET_SEGMENT_ID and
//                    o.org_num = po.org_num        
//            group by  o.org_name, o.org_group, o.org_num, 
//                      substr(ic.card_type,1,1),
//                      decode( bet.enabled,
//                              'Y',bet.print_group,
//                              null ),
//                      decode( bet.enabled,
//                              'Y', bet.STATEMENT_MESSAGE,
//                              decode(substr(ic.card_type,1,1),'V','VISA','MC') || 
//                              ' QUALIFIED' )        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                    index (sm pk_monthly_extract_summary)\n                    index (ic PK_DAILY_DETAIL_IC_SUMMARY)\n                    index (bet idx_tsys_bet_bank_date_num)\n                    ordered\n                    use_nl(sm ic)\n                  */\n                  o.org_num                           as org_num,\n                  o.org_group                         as node_id,\n                  o.org_name                          as org_name,\n                  decode(substr(ic.card_type,1,1),\n                         'V','Visa',\n                         'MasterCard')                as card_type,\n                  decode( bet.enabled,\n                          'Y',bet.print_group,\n                          null )                      as bet_group,\n                  decode( bet.enabled,\n                          'Y', bet.STATEMENT_MESSAGE,\n                          decode(substr(ic.card_type,1,1),'V','VISA','MC') || \n                          ' QUALIFIED' )              as statement_msg,\n                  sum( ic.sales_count )               as sales_count,\n                  sum( ic.sales_amount )              as sales_amount\n          from    parent_org                    po,\n                  group_merchant                gm,\n                  monthly_extract_summary       sm,\n                  daily_detail_ic_summary       ic,\n                  daily_detail_file_ic_desc     icd,                \n                  tsys_bet_file                 bet,\n                  organization                  o        \n          where   po.parent_org_num =  :1  and\n                  gm.org_num = po.org_num and\n                  sm.merchant_number = gm.merchant_number and\n                  sm.active_date =  :2  and\n                  ic.merchant_number = sm.merchant_number and\n                  ic.batch_date between sm.month_begin_date+1 and sm.month_end_date+1 and\n                  substr(ic.card_type,1,1) in ( 'V','M' ) and\n                  nvl(ic.sales_count,0) > 0 and                \n                  icd.card_type = decode(substr(ic.card_type,1,1),'V','VS','MC') and\n                  icd.IC_CODE = ic.ic_cat and\n                  ic.batch_date between icd.valid_date_begin and icd.valid_date_end and\n                  bet.bank_number = sm.bank_number and\n                  bet.active_date = sm.active_date and\n                  bet.bet_number = decode( substr(ic.card_type,1,1),\n                                           'V', sm.visa_ic_bet_number,\n                                           sm.mc_ic_bet_number ) and\n                  bet.segment_type = 1 and\n                  bet.segment_id = icd.BET_SEGMENT_ID and\n                  o.org_num = po.org_num        \n          group by  o.org_name, o.org_group, o.org_num, \n                    substr(ic.card_type,1,1),\n                    decode( bet.enabled,\n                            'Y',bet.print_group,\n                            null ),\n                    decode( bet.enabled,\n                            'Y', bet.STATEMENT_MESSAGE,\n                            decode(substr(ic.card_type,1,1),'V','VISA','MC') || \n                            ' QUALIFIED' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchICQualDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchICQualDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:529^9*/
      }        
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new QualSummary( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties( HttpServletRequest request )
  {
    super.setProperties(request);
    
    BetStatementMsg = HttpHelper.getString(request,"stmtMsg",null);
    
    // if the from date is today, then set the default to rolling previous week
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range
      cal.setTime( ReportDateBegin );
      
      if ( cal.get( Calendar.MONTH ) == Calendar.JANUARY )
      {
        cal.set( Calendar.MONTH, Calendar.DECEMBER );
        cal.set( Calendar.YEAR,  ( cal.get( Calendar.YEAR ) - 1 ) );
      }
      else
      {
        cal.set( Calendar.MONTH, ( cal.get( Calendar.MONTH ) - 1 ) );
      }
      
      // since this report is for one month at a time,
      // make sure that the from date always starts with
      // the first day of the specified month.
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      // set both the begin and end dates
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/