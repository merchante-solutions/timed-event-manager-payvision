/*@lineinfo:filename=ProfIcVitalDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ProfIcVitalDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-04-23 08:36:57 -0700 (Thu, 23 Apr 2009) $
  Version            : $Revision: 16022 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class ProfIcVitalDataBean extends ProfIcDataBean
{
  protected static final int    IC_CASH_ADVANCE_DOMESTIC      = 0;
  protected static final int    IC_CASH_ADVANCE_FOREIGN       = 1;
  protected static final int    IC_CASH_ADVANCE_CAT_COUNT     = 2;

  public ProfIcVitalDataBean( )
  {
  }
  
  public void encodeCSVHeader( StringBuffer line )
  {
    String                cardType          = "";
    int                   catCount          = 0;
    StringBuffer          desc              = new StringBuffer("");
    int                   descBaseLength    = 0;
  
    line.setLength(0);
    line.append("\"Assoc Number\",");
    line.append("\"Merchant Number\",");
    line.append("\"DBA Name\"");
  
    // only show visa and mc
    for( int ct = 0; ct < IC_CARD_TYPES; ++ct )
    {
      desc.setLength(0);
      switch( ct )
      {
        case IC_VISA:       // visa sales/fees
          cardType  = mesConstants.VITAL_ME_AP_VISA;
          catCount  = 25;
          desc.append( "VS " );
          break;

        case IC_MC:         // mc sales/fees
          cardType  = mesConstants.VITAL_ME_AP_MC;
          catCount  = 12;
          desc.append( "MC " );
          break;

        default:
          continue;
      }

      descBaseLength = desc.length();

      for( int cat = 0; cat < catCount; ++cat )
      {
        desc.setLength( descBaseLength );
        desc.append( loadVitalIcCatDesc( cardType, (cat+1) ) );
      
        line.append( ",\"" );
        line.append("# Sales "    );
        line.append( desc.toString() );
        line.append( "\",\"" );
        line.append("$ Sales "    );
        line.append( desc.toString() );
        line.append( "\",\"" );
        line.append("# Credits "  );
        line.append( desc.toString() );
        line.append( "\",\"" );
        line.append("$ Credits "  );
        line.append( desc.toString() );
        line.append( "\",\"" );
        line.append("Income "     );
        line.append( desc.toString() );
        line.append( "\",\"" );
        line.append("Expense "    );
        line.append( desc.toString() );
        line.append( "\"" );
      }
    }
  }
  
  protected void encodeCSVLine( Object obj, StringBuffer line )
  {
    InterchangeData             cardData      = null;
    MerchantInterchangeData     merchIcData   = (MerchantInterchangeData)obj;
  
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( merchIcData.getAssocNumber() );
    line.append( "," );
    line.append( merchIcData.getMerchantId() );
    line.append(",\"");
    line.append( merchIcData.getMerchantName() );
    line.append("\"");
    
    for( int ct = 0; ct < IC_CARD_TYPES; ++ct )
    {
      //
      // skip any card types that have not been loaded with data.
      // currently, only Visa and MasterCard are loaded, but the
      // data bean supports a third card type for debit interchange.
      //
      if ( merchIcData.isCardDataValid( ct ) != true )
      {
        continue;
      }
      cardData = merchIcData.getCardData( ct );

      for( int cat = 0; cat < cardData.getCategoryCount(); ++cat )
      {
        line.append(",");
        line.append( cardData.getCategorySalesCount(cat) );
        line.append( "," );
        line.append( MesMath.toCurrencyCompressed( cardData.getCategorySalesAmount(cat) ) );
        line.append( "," );
        line.append( cardData.getCategoryCreditsCount(cat) );
        line.append( "," );
        line.append( MesMath.toCurrencyCompressed( cardData.getCategoryCreditsAmount(cat) ) );
        line.append( "," );
        line.append( MesMath.toCurrencyCompressed( cardData.getCategoryIncome(cat) ) );
        line.append( "," );
        line.append( MesMath.toCurrencyCompressed( cardData.getCategoryExpense(cat) ) );
      }
    }
  }
  
  public void loadCashAdvanceData( long orgId, Date beginDate, Date endDate )
  {
    long                    assocId       = 0L;
    String                  cardType      = null;
    int                     catCount      = 0;
    int                     cat           = 0;
    String                  desc          = null;
    String                  fieldPrefix   = null;
    InterchangeData         icRecord      = null;
    ResultSetIterator       it            = null;
    long                    merchantId    = 0L;
    ResultSet               resultSet     = null;
    int                     salesCount    = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:184^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ( gn.hh_bank_number || gn.g1_association_number )               assoc_number,
//                  gn.hh_merchant_number                                           merchant_number,
//                  sum( visa.V1_INCOME_BULLETIN )                                  VS_INC_BULLETIN,
//                  sum( decode( gn.hh_bank_number, 
//                               :mesConstants.BANK_ID_MES, 0,
//                               visa.V1_INCOME_INTRA_CHANGE ) )                    VS_INC_INTRA_CHANGE,
//                  sum( visa.V1_INCOME_ASSESSMENT )                                VS_INC_ASSESSMENT,
//                  sum( visa.V1_INC_CAT_02_SALES + visa.V1_INC_CAT_02_FEES )       VS_CAT_02_INC,
//                  sum( visa.V1_INC_CAT_05_SALES + visa.V1_INC_CAT_05_FEES )       VS_CAT_05_INC,
//                  sum( mc.M1_INC_CAT_02_SALES + mc.M1_INC_CAT_02_FEES )           MC_CAT_02_INC,
//                  sum( mc.M1_INC_CAT_05_SALES + mc.M1_INC_CAT_05_FEES )           MC_CAT_05_INC,
//                  sum( visa.V5_CAT_02_SALES_COUNT )                               VS_CAT_02_SALES_COUNT,
//                  sum( visa.V5_CAT_02_SALES_AMOUNT )                              VS_CAT_02_SALES_AMOUNT,
//                  sum( visa.V5_CAT_02_CRED_COUNT )                                VS_CAT_02_CRED_COUNT,
//                  sum( visa.V5_CAT_02_CRED_AMOUNT )                               VS_CAT_02_CRED_AMOUNT,
//                  sum( visa.V5_CAT_02_INTCH )                                     VS_CAT_02_INTCH,
//                  sum( visa.V5_CAT_05_SALES_COUNT )                               VS_CAT_05_SALES_COUNT,
//                  sum( visa.V5_CAT_05_SALES_AMOUNT )                              VS_CAT_05_SALES_AMOUNT,
//                  sum( visa.V5_CAT_05_CRED_COUNT )                                VS_CAT_05_CRED_COUNT,
//                  sum( visa.V5_CAT_05_CRED_AMOUNT )                               VS_CAT_05_CRED_AMOUNT,
//                  sum( visa.V5_CAT_05_INTCH )                                     VS_CAT_05_INTCH,
//                  sum( mc.M1_INCOME_BULLETIN )                                    MC_INC_BULLETIN,
//                  sum( visa.V3_EXPENSE_BULLETIN )                                 VS_EXP_BULLETIN,
//                  sum( decode( gn.hh_bank_number,
//                               :mesConstants.BANK_ID_MES, 0,
//                               visa.V3_EXPENSE_INTRA_CHANGE ) )                   VS_EXP_INTRA_CHANGE,
//                  sum( visa.V3_EXPENSE_ASSESSMENT )                               VS_EXP_ASSESSMENT,
//                  sum( decode( gn.hh_bank_number,
//                               :mesConstants.BANK_ID_MES, 0,
//                               mc.M1_INCOME_INTRA_CHANGE ) )                      MC_INC_INTRA_CHANGE,
//                  sum( mc.M1_INCOME_ASSESSMENT )                                  MC_INC_ASSESSMENT,
//                  sum( mc.M2_EXPENSE_BULLETIN )                                   MC_EXP_BULLETIN,
//                  sum( decode( gn.hh_bank_number,
//                               :mesConstants.BANK_ID_MES, 0,
//                               mc.M2_EXPENSE_INTRA_CHANGE ) )                     MC_EXP_INTRA_CHANGE,
//                  sum( mc.M2_EXPENSE_ASSESSMENT )                                 MC_EXP_ASSESSMENT,
//                  sum( mc.M3_CAT_02_SALES_COUNT )                                 MC_CAT_02_SALES_COUNT,
//                  sum( mc.M3_CAT_02_SALES_AMOUNT )                                MC_CAT_02_SALES_AMOUNT,
//                  sum( mc.M3_CAT_02_CRED_COUNT )                                  MC_CAT_02_CRED_COUNT,
//                  sum( mc.M3_CAT_02_CRED_AMOUNT )                                 MC_CAT_02_CRED_AMOUNT,
//                  sum( mc.M3_CAT_02_INTCH )                                       MC_CAT_02_INTCH,
//                  sum( mc.M3_CAT_05_SALES_COUNT )                                 MC_CAT_05_SALES_COUNT,
//                  sum( mc.M3_CAT_05_SALES_AMOUNT )                                MC_CAT_05_SALES_AMOUNT,
//                  sum( mc.M3_CAT_05_CRED_COUNT )                                  MC_CAT_05_CRED_COUNT,
//                  sum( mc.M3_CAT_05_CRED_AMOUNT )                                 MC_CAT_05_CRED_AMOUNT,
//                  sum( mc.M3_CAT_05_INTCH )                                       MC_CAT_05_INTCH
//          from    monthly_extract_gn   gn,
//                  monthly_extract_visa visa,
//                  monthly_extract_mc   mc
//          where   gn.hh_merchant_number = 
//                    ( select org_merchant_num 
//                      from orgmerchant 
//                      where org_num = :orgId ) and
//                  gn.hh_active_date between :beginDate and last_day( :endDate ) and
//                  visa.hh_load_sec(+) = gn.hh_load_sec and
//                  mc.hh_load_sec(+) = gn.hh_load_sec
//          group by (gn.hh_bank_number || gn.g1_association_number), gn.hh_merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ( gn.hh_bank_number || gn.g1_association_number )               assoc_number,\n                gn.hh_merchant_number                                           merchant_number,\n                sum( visa.V1_INCOME_BULLETIN )                                  VS_INC_BULLETIN,\n                sum( decode( gn.hh_bank_number, \n                              :1 , 0,\n                             visa.V1_INCOME_INTRA_CHANGE ) )                    VS_INC_INTRA_CHANGE,\n                sum( visa.V1_INCOME_ASSESSMENT )                                VS_INC_ASSESSMENT,\n                sum( visa.V1_INC_CAT_02_SALES + visa.V1_INC_CAT_02_FEES )       VS_CAT_02_INC,\n                sum( visa.V1_INC_CAT_05_SALES + visa.V1_INC_CAT_05_FEES )       VS_CAT_05_INC,\n                sum( mc.M1_INC_CAT_02_SALES + mc.M1_INC_CAT_02_FEES )           MC_CAT_02_INC,\n                sum( mc.M1_INC_CAT_05_SALES + mc.M1_INC_CAT_05_FEES )           MC_CAT_05_INC,\n                sum( visa.V5_CAT_02_SALES_COUNT )                               VS_CAT_02_SALES_COUNT,\n                sum( visa.V5_CAT_02_SALES_AMOUNT )                              VS_CAT_02_SALES_AMOUNT,\n                sum( visa.V5_CAT_02_CRED_COUNT )                                VS_CAT_02_CRED_COUNT,\n                sum( visa.V5_CAT_02_CRED_AMOUNT )                               VS_CAT_02_CRED_AMOUNT,\n                sum( visa.V5_CAT_02_INTCH )                                     VS_CAT_02_INTCH,\n                sum( visa.V5_CAT_05_SALES_COUNT )                               VS_CAT_05_SALES_COUNT,\n                sum( visa.V5_CAT_05_SALES_AMOUNT )                              VS_CAT_05_SALES_AMOUNT,\n                sum( visa.V5_CAT_05_CRED_COUNT )                                VS_CAT_05_CRED_COUNT,\n                sum( visa.V5_CAT_05_CRED_AMOUNT )                               VS_CAT_05_CRED_AMOUNT,\n                sum( visa.V5_CAT_05_INTCH )                                     VS_CAT_05_INTCH,\n                sum( mc.M1_INCOME_BULLETIN )                                    MC_INC_BULLETIN,\n                sum( visa.V3_EXPENSE_BULLETIN )                                 VS_EXP_BULLETIN,\n                sum( decode( gn.hh_bank_number,\n                              :2 , 0,\n                             visa.V3_EXPENSE_INTRA_CHANGE ) )                   VS_EXP_INTRA_CHANGE,\n                sum( visa.V3_EXPENSE_ASSESSMENT )                               VS_EXP_ASSESSMENT,\n                sum( decode( gn.hh_bank_number,\n                              :3 , 0,\n                             mc.M1_INCOME_INTRA_CHANGE ) )                      MC_INC_INTRA_CHANGE,\n                sum( mc.M1_INCOME_ASSESSMENT )                                  MC_INC_ASSESSMENT,\n                sum( mc.M2_EXPENSE_BULLETIN )                                   MC_EXP_BULLETIN,\n                sum( decode( gn.hh_bank_number,\n                              :4 , 0,\n                             mc.M2_EXPENSE_INTRA_CHANGE ) )                     MC_EXP_INTRA_CHANGE,\n                sum( mc.M2_EXPENSE_ASSESSMENT )                                 MC_EXP_ASSESSMENT,\n                sum( mc.M3_CAT_02_SALES_COUNT )                                 MC_CAT_02_SALES_COUNT,\n                sum( mc.M3_CAT_02_SALES_AMOUNT )                                MC_CAT_02_SALES_AMOUNT,\n                sum( mc.M3_CAT_02_CRED_COUNT )                                  MC_CAT_02_CRED_COUNT,\n                sum( mc.M3_CAT_02_CRED_AMOUNT )                                 MC_CAT_02_CRED_AMOUNT,\n                sum( mc.M3_CAT_02_INTCH )                                       MC_CAT_02_INTCH,\n                sum( mc.M3_CAT_05_SALES_COUNT )                                 MC_CAT_05_SALES_COUNT,\n                sum( mc.M3_CAT_05_SALES_AMOUNT )                                MC_CAT_05_SALES_AMOUNT,\n                sum( mc.M3_CAT_05_CRED_COUNT )                                  MC_CAT_05_CRED_COUNT,\n                sum( mc.M3_CAT_05_CRED_AMOUNT )                                 MC_CAT_05_CRED_AMOUNT,\n                sum( mc.M3_CAT_05_INTCH )                                       MC_CAT_05_INTCH\n        from    monthly_extract_gn   gn,\n                monthly_extract_visa visa,\n                monthly_extract_mc   mc\n        where   gn.hh_merchant_number = \n                  ( select org_merchant_num \n                    from orgmerchant \n                    where org_num =  :5  ) and\n                gn.hh_active_date between  :6  and last_day(  :7  ) and\n                visa.hh_load_sec(+) = gn.hh_load_sec and\n                mc.hh_load_sec(+) = gn.hh_load_sec\n        group by (gn.hh_bank_number || gn.g1_association_number), gn.hh_merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ProfIcVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.BANK_ID_MES);
   __sJT_st.setInt(2,mesConstants.BANK_ID_MES);
   __sJT_st.setInt(3,mesConstants.BANK_ID_MES);
   __sJT_st.setInt(4,mesConstants.BANK_ID_MES);
   __sJT_st.setLong(5,orgId);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ProfIcVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        // store the merchant specific data
        assocId     = resultSet.getLong("ASSOC_NUMBER");
        merchantId  = resultSet.getLong("MERCHANT_NUMBER");
        
        for ( int icCardType = 0; icCardType < IC_CARD_TYPES; ++icCardType )
        {
          switch( icCardType )
          {
            case IC_VISA:       // visa sales/fees
              cardType  = mesConstants.VITAL_ME_AP_VISA;
              break;
          
            case IC_MC:       // mc sales/fees
              cardType  = mesConstants.VITAL_ME_AP_MC;
              break;
            
            default:
              continue;
          }
          icRecord = new InterchangeData( icCardType, IC_CASH_ADVANCE_CAT_COUNT );
          
          // BET driven interchange expense are considered part of 
          // cash advance interchange expense
          icRecord.setFixedData( resultSet.getDouble( cardType + "_INC_ASSESSMENT" ),
                                 resultSet.getDouble( cardType + "_EXP_ASSESSMENT" ),
                                 resultSet.getDouble( cardType + "_INC_BULLETIN" ),
                                 resultSet.getDouble( cardType + "_EXP_BULLETIN" ),
                                 resultSet.getDouble( cardType + "_INC_INTRA_CHANGE" ),
                                 resultSet.getDouble( cardType + "_EXP_INTRA_CHANGE" ) );
          
          // cats are 1 based index, so go from 1 to the catCount
          for( int i = 0; i < IC_CASH_ADVANCE_CAT_COUNT; ++i )
          {
            switch( i )
            {
              case IC_CASH_ADVANCE_DOMESTIC:
                if ( icCardType == IC_VISA ) 
                {
                  cat = mesConstants.IC_VS_CAT_DOMESTIC_CASH;
                }
                else // IC_MC
                {
                  cat = mesConstants.IC_MC_CAT_DOMESTIC_CASH;
                }                  
                break;
                
              case IC_CASH_ADVANCE_FOREIGN: 
                if ( icCardType == IC_VISA ) 
                {
                  cat = mesConstants.IC_VS_CAT_FOREIGN_CASH;
                }
                else // IC_MC
                {
                  cat = mesConstants.IC_MC_CAT_FOREIGN_CASH;
                }
                break;
            }
            
            fieldPrefix  = cardType + "_CAT_" + NumberFormatter.getPaddedInt(cat,2);
            
            // this is necessary because vital sends # transactions
            // in the # sales column.  Occasionally (due to system error)
            // the # credits > # sales so it is necessary to guard
            // against underflow.
            //
            salesCount = resultSet.getInt( fieldPrefix + "_SALES_COUNT" ) -
                         resultSet.getInt( fieldPrefix + "_CRED_COUNT" );
            if ( salesCount < 0 )
            {
              salesCount = 0;
            } 
            
            // the InterchangeData object expects
            // cat to be zero based, so subtract one to
            // prevent an ArrayIndexOutOfBoundsException 
            // from being thrown.
            icRecord.setCategoryData( i,
                                      loadVitalIcCatDesc ( cardType, cat ),
                                      salesCount,
                                      resultSet.getDouble( fieldPrefix + "_SALES_AMOUNT" ),
                                      resultSet.getInt   ( fieldPrefix + "_CRED_COUNT" ),
                                      resultSet.getDouble( fieldPrefix + "_CRED_AMOUNT" ),
                                      resultSet.getDouble( fieldPrefix + "_INC" ),
                                      resultSet.getDouble( fieldPrefix + "_INTCH" ) );
          }
          
          // add the card interchange data to the appropriate merchant
          // interchange collection.
          addMerchantIcData( assocId, merchantId, icCardType, icRecord );
        }
      }
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }

  public void loadData( long orgId, Date beginDate, Date endDate, boolean maskCashAdvanceIc )
  {
    long                    assocId           = 0L;
    String                  cardType          = null;
    int                     catCount          = 0;
    String                  desc              = null;
    String                  fieldPrefix       = null;
    boolean                 hasCashAdvanceIc  = false;
    InterchangeData         icRecord          = null;
    ResultSetIterator       it                = null;
    long                    merchantId        = 0L;
    ResultSet               resultSet         = null;
    int                     salesCount        = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:365^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ( gn.hh_bank_number || gn.g1_association_number )               assoc_number,
//                  gn.hh_merchant_number                                           merchant_number,
//                  sum( visa.V1_INCOME_BULLETIN )                                  VS_INC_BULLETIN,
//                  sum( decode( gn.hh_bank_number, 
//                               :mesConstants.BANK_ID_MES, 0,
//                               visa.V1_INCOME_INTRA_CHANGE ) )                    VS_INC_INTRA_CHANGE,
//                  sum( visa.V1_INCOME_ASSESSMENT )                                VS_INC_ASSESSMENT,
//                  sum( visa.V1_INC_CAT_01_SALES + visa.V1_INC_CAT_01_FEES )       VS_CAT_01_INC,
//                  sum( visa.V1_INC_CAT_02_SALES + visa.V1_INC_CAT_02_FEES )       VS_CAT_02_INC,
//                  sum( visa.V1_INC_CAT_03_SALES + visa.V1_INC_CAT_03_FEES )       VS_CAT_03_INC,
//                  sum( visa.V1_INC_CAT_04_SALES + visa.V1_INC_CAT_04_FEES )       VS_CAT_04_INC,
//                  sum( visa.V1_INC_CAT_05_SALES + visa.V1_INC_CAT_05_FEES )       VS_CAT_05_INC,
//                  sum( visa.V1_INC_CAT_06_SALES + visa.V1_INC_CAT_06_FEES )       VS_CAT_06_INC,
//                  sum( visa.V1_INC_CAT_07_SALES + visa.V1_INC_CAT_07_FEES )       VS_CAT_07_INC,
//                  sum( visa.V1_INC_CAT_08_SALES + visa.V1_INC_CAT_08_FEES )       VS_CAT_08_INC,
//                  sum( visa.V1_INC_CAT_09_SALES + visa.V1_INC_CAT_09_FEES )       VS_CAT_09_INC,
//                  sum( visa.V1_INC_CAT_10_SALES + visa.V1_INC_CAT_10_FEES )       VS_CAT_10_INC,
//                  sum( visa.V1_INC_CAT_11_SALES + visa.V1_INC_CAT_11_FEES )       VS_CAT_11_INC,
//                  sum( visa.V1_INC_CAT_12_SALES + visa.V1_INC_CAT_12_FEES )       VS_CAT_12_INC,
//                  sum( visa.V1_INC_CAT_13_SALES + visa.V1_INC_CAT_13_FEES )       VS_CAT_13_INC,
//                  sum( visa.V1_INC_CAT_14_SALES + visa.V1_INC_CAT_14_FEES )       VS_CAT_14_INC,
//                  sum( visa.V1_INC_CAT_15_SALES + visa.V1_INC_CAT_15_FEES )       VS_CAT_15_INC,
//                  sum( visa.V1_INC_CAT_16_SALES + visa.V1_INC_CAT_16_FEES )       VS_CAT_16_INC,
//                  sum( visa.V1_INC_CAT_17_SALES + visa.V2_INC_CAT_17_FEES )       VS_CAT_17_INC,
//                  sum( visa.V2_INC_CAT_18_SALES + visa.V2_INC_CAT_18_FEES )       VS_CAT_18_INC,
//                  sum( visa.V2_INC_CAT_19_SALES + visa.V2_INC_CAT_19_FEES )       VS_CAT_19_INC,
//                  sum( visa.V2_INC_CAT_20_SALES + visa.V2_INC_CAT_20_FEES )       VS_CAT_20_INC,
//                  sum( visa.V2_INC_CAT_21_SALES + visa.V2_INC_CAT_21_FEES )       VS_CAT_21_INC,
//                  sum( visa.V2_INC_CAT_22_SALES + visa.V2_INC_CAT_22_FEES )       VS_CAT_22_INC,
//                  sum( visa.V2_INC_CAT_23_SALES + visa.V2_INC_CAT_23_FEES )       VS_CAT_23_INC,
//                  sum( visa.V2_INC_CAT_24_SALES + visa.V2_INC_CAT_24_FEES )       VS_CAT_24_INC,
//                  sum( /* no cat 25 sales */      visa.V2_INC_CAT_25_FEES )       VS_CAT_25_INC,
//                  sum( visa.V3_EXPENSE_BULLETIN )                                 VS_EXP_BULLETIN,
//                  sum( decode( gn.hh_bank_number,
//                               :mesConstants.BANK_ID_MES, 0,
//                               visa.V3_EXPENSE_INTRA_CHANGE ) )                   VS_EXP_INTRA_CHANGE,
//                  sum( visa.V3_EXPENSE_ASSESSMENT )                               VS_EXP_ASSESSMENT,
//                  sum( mc.M1_INCOME_BULLETIN )                                    MC_INC_BULLETIN,
//                  sum( decode( gn.hh_bank_number,
//                               :mesConstants.BANK_ID_MES, 0,
//                               mc.M1_INCOME_INTRA_CHANGE ) )                      MC_INC_INTRA_CHANGE,
//                  sum( mc.M1_INCOME_ASSESSMENT )                                  MC_INC_ASSESSMENT,
//                  sum( mc.M1_INC_CAT_01_SALES + mc.M1_INC_CAT_01_FEES )           MC_CAT_01_INC,
//                  sum( mc.M1_INC_CAT_02_SALES + mc.M1_INC_CAT_02_FEES )           MC_CAT_02_INC,
//                  sum( mc.M1_INC_CAT_03_SALES + mc.M1_INC_CAT_03_FEES )           MC_CAT_03_INC,
//                  sum( mc.M1_INC_CAT_04_SALES + mc.M1_INC_CAT_04_FEES )           MC_CAT_04_INC,
//                  sum( mc.M1_INC_CAT_05_SALES + mc.M1_INC_CAT_05_FEES )           MC_CAT_05_INC,
//                  sum( mc.M1_INC_CAT_06_SALES + mc.M1_INC_CAT_06_FEES )           MC_CAT_06_INC,
//                  sum( mc.M1_INC_CAT_07_SALES + mc.M1_INC_CAT_07_FEES )           MC_CAT_07_INC,
//                  sum( mc.M1_INC_CAT_08_SALES + mc.M1_INC_CAT_08_FEES )           MC_CAT_08_INC,
//                  sum( mc.M1_INC_CAT_09_SALES + mc.M1_INC_CAT_09_FEES )           MC_CAT_09_INC,
//                  sum( mc.M1_INC_CAT_10_SALES + mc.M1_INC_CAT_10_FEES )           MC_CAT_10_INC,
//                  sum( mc.M1_INC_CAT_11_SALES + mc.M1_INC_CAT_11_FEES )           MC_CAT_11_INC,
//                  sum( mc.M1_INC_CAT_12_SALES + mc.M1_INC_CAT_12_FEES )           MC_CAT_12_INC,
//                  sum( mc.M2_EXPENSE_BULLETIN )                                   MC_EXP_BULLETIN,
//                  sum( decode( gn.hh_bank_number,
//                               :mesConstants.BANK_ID_MES, 0,
//                               mc.M2_EXPENSE_INTRA_CHANGE ) )                     MC_EXP_INTRA_CHANGE,
//                  sum( mc.M2_EXPENSE_ASSESSMENT )                                 MC_EXP_ASSESSMENT,
//                  sum( visa.V5_CAT_01_SALES_COUNT )                               VS_CAT_01_SALES_COUNT,
//                  sum( visa.V5_CAT_01_SALES_AMOUNT )                              VS_CAT_01_SALES_AMOUNT,
//                  sum( visa.V5_CAT_01_CRED_COUNT )                                VS_CAT_01_CRED_COUNT,
//                  sum( visa.V5_CAT_01_CRED_AMOUNT )                               VS_CAT_01_CRED_AMOUNT,
//                  sum( visa.V5_CAT_01_INTCH )                                     VS_CAT_01_INTCH,
//                  sum( visa.V5_CAT_02_SALES_COUNT )                               VS_CAT_02_SALES_COUNT,
//                  sum( visa.V5_CAT_02_SALES_AMOUNT )                              VS_CAT_02_SALES_AMOUNT,
//                  sum( visa.V5_CAT_02_CRED_COUNT )                                VS_CAT_02_CRED_COUNT,
//                  sum( visa.V5_CAT_02_CRED_AMOUNT )                               VS_CAT_02_CRED_AMOUNT,
//                  sum( visa.V5_CAT_02_INTCH )                                     VS_CAT_02_INTCH,
//                  sum( visa.V5_CAT_03_SALES_COUNT )                               VS_CAT_03_SALES_COUNT,
//                  sum( visa.V5_CAT_03_SALES_AMOUNT )                              VS_CAT_03_SALES_AMOUNT,
//                  sum( visa.V5_CAT_03_CRED_COUNT )                                VS_CAT_03_CRED_COUNT,
//                  sum( visa.V5_CAT_03_CRED_AMOUNT )                               VS_CAT_03_CRED_AMOUNT,
//                  sum( visa.V5_CAT_03_INTCH )                                     VS_CAT_03_INTCH,
//                  sum( visa.V5_CAT_04_SALES_COUNT )                               VS_CAT_04_SALES_COUNT,
//                  sum( visa.V5_CAT_04_SALES_AMOUNT )                              VS_CAT_04_SALES_AMOUNT,
//                  sum( visa.V5_CAT_04_CRED_COUNT )                                VS_CAT_04_CRED_COUNT,
//                  sum( visa.V5_CAT_04_CRED_AMOUNT )                               VS_CAT_04_CRED_AMOUNT,
//                  sum( visa.V5_CAT_04_INTCH )                                     VS_CAT_04_INTCH,
//                  sum( visa.V5_CAT_05_SALES_COUNT )                               VS_CAT_05_SALES_COUNT,
//                  sum( visa.V5_CAT_05_SALES_AMOUNT )                              VS_CAT_05_SALES_AMOUNT,
//                  sum( visa.V5_CAT_05_CRED_COUNT )                                VS_CAT_05_CRED_COUNT,
//                  sum( visa.V5_CAT_05_CRED_AMOUNT )                               VS_CAT_05_CRED_AMOUNT,
//                  sum( visa.V5_CAT_05_INTCH )                                     VS_CAT_05_INTCH,
//                  sum( visa.V5_CAT_06_SALES_COUNT )                               VS_CAT_06_SALES_COUNT,
//                  sum( visa.V5_CAT_06_SALES_AMOUNT )                              VS_CAT_06_SALES_AMOUNT,
//                  sum( visa.V5_CAT_06_CRED_COUNT )                                VS_CAT_06_CRED_COUNT,
//                  sum( visa.V5_CAT_06_CRED_AMOUNT )                               VS_CAT_06_CRED_AMOUNT,
//                  sum( visa.V6_CAT_06_INTCH )                                     VS_CAT_06_INTCH,
//                  sum( visa.V6_CAT_07_SALES_COUNT )                               VS_CAT_07_SALES_COUNT,
//                  sum( visa.V6_CAT_07_SALES_AMOUNT )                              VS_CAT_07_SALES_AMOUNT,
//                  sum( visa.V6_CAT_07_CRED_COUNT )                                VS_CAT_07_CRED_COUNT,
//                  sum( visa.V6_CAT_07_CRED_AMOUNT )                               VS_CAT_07_CRED_AMOUNT,
//                  sum( visa.V6_CAT_07_INTCH )                                     VS_CAT_07_INTCH,
//                  sum( visa.V6_CAT_08_SALES_COUNT )                               VS_CAT_08_SALES_COUNT,
//                  sum( visa.V6_CAT_08_SALES_AMOUNT )                              VS_CAT_08_SALES_AMOUNT,
//                  sum( visa.V6_CAT_08_CRED_COUNT )                                VS_CAT_08_CRED_COUNT,
//                  sum( visa.V6_CAT_08_CRED_AMOUNT )                               VS_CAT_08_CRED_AMOUNT,
//                  sum( visa.V6_CAT_08_INTCH )                                     VS_CAT_08_INTCH,
//                  sum( visa.V6_CAT_09_SALES_COUNT )                               VS_CAT_09_SALES_COUNT,
//                  sum( visa.V6_CAT_09_SALES_AMOUNT )                              VS_CAT_09_SALES_AMOUNT,
//                  sum( visa.V6_CAT_09_CRED_COUNT )                                VS_CAT_09_CRED_COUNT,
//                  sum( visa.V6_CAT_09_CRED_AMOUNT )                               VS_CAT_09_CRED_AMOUNT,
//                  sum( visa.V6_CAT_09_INTCH )                                     VS_CAT_09_INTCH,
//                  sum( visa.V6_CAT_10_SALES_COUNT )                               VS_CAT_10_SALES_COUNT,
//                  sum( visa.V6_CAT_10_SALES_AMOUNT )                              VS_CAT_10_SALES_AMOUNT,
//                  sum( visa.V6_CAT_10_CRED_COUNT )                                VS_CAT_10_CRED_COUNT,
//                  sum( visa.V6_CAT_10_CRED_AMOUNT )                               VS_CAT_10_CRED_AMOUNT,
//                  sum( visa.V6_CAT_10_INTCH )                                     VS_CAT_10_INTCH,
//                  sum( visa.V6_CAT_11_SALES_COUNT )                               VS_CAT_11_SALES_COUNT,
//                  sum( visa.V6_CAT_11_SALES_AMOUNT )                              VS_CAT_11_SALES_AMOUNT,
//                  sum( visa.V6_CAT_11_CRED_COUNT )                                VS_CAT_11_CRED_COUNT,
//                  sum( visa.V6_CAT_11_CRED_AMOUNT )                               VS_CAT_11_CRED_AMOUNT,
//                  sum( visa.V6_CAT_11_INTCH )                                     VS_CAT_11_INTCH,
//                  sum( visa.V6_CAT_12_SALES_COUNT )                               VS_CAT_12_SALES_COUNT,
//                  sum( visa.V6_CAT_12_SALES_AMOUNT )                              VS_CAT_12_SALES_AMOUNT,
//                  sum( visa.V6_CAT_12_CRED_COUNT )                                VS_CAT_12_CRED_COUNT,
//                  sum( visa.V7_CAT_12_CRED_AMOUNT )                               VS_CAT_12_CRED_AMOUNT,
//                  sum( visa.V7_CAT_12_INTCH )                                     VS_CAT_12_INTCH,
//                  sum( visa.V7_CAT_13_SALES_COUNT )                               VS_CAT_13_SALES_COUNT,
//                  sum( visa.V7_CAT_13_SALES_AMOUNT )                              VS_CAT_13_SALES_AMOUNT,
//                  sum( visa.V7_CAT_13_CRED_COUNT )                                VS_CAT_13_CRED_COUNT,
//                  sum( visa.V7_CAT_13_CRED_AMOUNT )                               VS_CAT_13_CRED_AMOUNT,
//                  sum( visa.V7_CAT_13_INTCH )                                     VS_CAT_13_INTCH,
//                  sum( visa.V7_CAT_14_SALES_COUNT )                               VS_CAT_14_SALES_COUNT,
//                  sum( visa.V7_CAT_14_SALES_AMOUNT )                              VS_CAT_14_SALES_AMOUNT,
//                  sum( visa.V7_CAT_14_CRED_COUNT )                                VS_CAT_14_CRED_COUNT,
//                  sum( visa.V7_CAT_14_CRED_AMOUNT )                               VS_CAT_14_CRED_AMOUNT,
//                  sum( visa.V7_CAT_14_INTCH )                                     VS_CAT_14_INTCH,
//                  sum( visa.V7_CAT_15_SALES_COUNT )                               VS_CAT_15_SALES_COUNT,
//                  sum( visa.V7_CAT_15_SALES_AMOUNT )                              VS_CAT_15_SALES_AMOUNT,
//                  sum( visa.V7_CAT_15_CRED_COUNT )                                VS_CAT_15_CRED_COUNT,
//                  sum( visa.V7_CAT_15_CRED_AMOUNT )                               VS_CAT_15_CRED_AMOUNT,
//                  sum( visa.V7_CAT_15_INTCH )                                     VS_CAT_15_INTCH,
//                  sum( visa.V7_CAT_16_SALES_COUNT )                               VS_CAT_16_SALES_COUNT,
//                  sum( visa.V7_CAT_16_SALES_AMOUNT )                              VS_CAT_16_SALES_AMOUNT,
//                  sum( visa.V7_CAT_16_CRED_COUNT )                                VS_CAT_16_CRED_COUNT,
//                  sum( visa.V7_CAT_16_CRED_AMOUNT )                               VS_CAT_16_CRED_AMOUNT,
//                  sum( visa.V7_CAT_16_INTCH )                                     VS_CAT_16_INTCH,
//                  sum( visa.V7_CAT_17_SALES_COUNT )                               VS_CAT_17_SALES_COUNT,
//                  sum( visa.V7_CAT_17_SALES_AMOUNT )                              VS_CAT_17_SALES_AMOUNT,
//                  sum( visa.V7_CAT_17_CRED_COUNT )                                VS_CAT_17_CRED_COUNT,
//                  sum( visa.V7_CAT_17_CRED_AMOUNT )                               VS_CAT_17_CRED_AMOUNT,
//                  sum( visa.V7_CAT_17_INTCH )                                     VS_CAT_17_INTCH,
//                  sum( visa.V7_CAT_18_SALES_COUNT )                               VS_CAT_18_SALES_COUNT,
//                  sum( visa.V8_CAT_18_SALES_AMOUNT )                              VS_CAT_18_SALES_AMOUNT,
//                  sum( visa.V8_CAT_18_CRED_COUNT )                                VS_CAT_18_CRED_COUNT,
//                  sum( visa.V8_CAT_18_CRED_AMOUNT )                               VS_CAT_18_CRED_AMOUNT,
//                  sum( visa.V8_CAT_18_INTCH )                                     VS_CAT_18_INTCH,
//                  sum( visa.V8_CAT_19_SALES_COUNT )                               VS_CAT_19_SALES_COUNT,
//                  sum( visa.V8_CAT_19_SALES_AMOUNT )                              VS_CAT_19_SALES_AMOUNT,
//                  sum( visa.V8_CAT_19_CRED_COUNT )                                VS_CAT_19_CRED_COUNT,
//                  sum( visa.V8_CAT_19_CRED_AMOUNT )                               VS_CAT_19_CRED_AMOUNT,
//                  sum( visa.V8_CAT_19_INTCH )                                     VS_CAT_19_INTCH,
//                  sum( visa.V8_CAT_20_SALES_COUNT )                               VS_CAT_20_SALES_COUNT,
//                  sum( visa.V8_CAT_20_SALES_AMOUNT )                              VS_CAT_20_SALES_AMOUNT,
//                  sum( visa.V8_CAT_20_CRED_COUNT )                                VS_CAT_20_CRED_COUNT,
//                  sum( visa.V8_CAT_20_CRED_AMOUNT )                               VS_CAT_20_CRED_AMOUNT,
//                  sum( visa.V8_CAT_20_INTCH )                                     VS_CAT_20_INTCH,
//                  sum( visa.V8_CAT_21_SALES_COUNT )                               VS_CAT_21_SALES_COUNT,
//                  sum( visa.V8_CAT_21_SALES_AMOUNT )                              VS_CAT_21_SALES_AMOUNT,
//                  sum( visa.V8_CAT_21_CRED_COUNT )                                VS_CAT_21_CRED_COUNT,
//                  sum( visa.V8_CAT_21_CRED_AMOUNT )                               VS_CAT_21_CRED_AMOUNT,
//                  sum( visa.V8_CAT_21_INTCH )                                     VS_CAT_21_INTCH,
//                  sum( visa.V8_CAT_22_SALES_COUNT )                               VS_CAT_22_SALES_COUNT,
//                  sum( visa.V8_CAT_22_SALES_AMOUNT )                              VS_CAT_22_SALES_AMOUNT,
//                  sum( visa.V8_CAT_22_CRED_COUNT )                                VS_CAT_22_CRED_COUNT,
//                  sum( visa.V8_CAT_22_CRED_AMOUNT )                               VS_CAT_22_CRED_AMOUNT,
//                  sum( visa.V8_CAT_22_INTCH )                                     VS_CAT_22_INTCH,
//                  sum( visa.V8_CAT_23_SALES_COUNT )                               VS_CAT_23_SALES_COUNT,
//                  sum( visa.V8_CAT_23_SALES_AMOUNT )                              VS_CAT_23_SALES_AMOUNT,
//                  sum( visa.V8_CAT_23_CRED_COUNT )                                VS_CAT_23_CRED_COUNT,
//                  sum( visa.V8_CAT_23_CRED_AMOUNT )                               VS_CAT_23_CRED_AMOUNT,
//                  sum( visa.V9_CAT_23_INTCH )                                     VS_CAT_23_INTCH,
//                  sum( visa.V9_CAT_24_SALES_COUNT )                               VS_CAT_24_SALES_COUNT,
//                  sum( visa.V9_CAT_24_SALES_AMOUNT )                              VS_CAT_24_SALES_AMOUNT,
//                  sum( visa.V9_CAT_24_CRED_COUNT )                                VS_CAT_24_CRED_COUNT,
//                  sum( visa.V9_CAT_24_CRED_AMOUNT )                               VS_CAT_24_CRED_AMOUNT,
//                  sum( visa.V9_CAT_24_INTCH )                                     VS_CAT_24_INTCH,
//                  0                                                               VS_CAT_25_SALES_COUNT,
//                  0                                                               VS_CAT_25_SALES_AMOUNT,
//                  0                                                               VS_CAT_25_CRED_COUNT,
//                  0                                                               VS_CAT_25_CRED_AMOUNT,
//                  sum( visa.V9_CAT_25_INTCH )                                     VS_CAT_25_INTCH,
//                  sum( mc.M3_CAT_01_SALES_COUNT )                                 MC_CAT_01_SALES_COUNT,
//                  sum( mc.M3_CAT_01_SALES_AMOUNT )                                MC_CAT_01_SALES_AMOUNT,
//                  sum( mc.M3_CAT_01_CRED_COUNT )                                  MC_CAT_01_CRED_COUNT,
//                  sum( mc.M3_CAT_01_CRED_AMOUNT )                                 MC_CAT_01_CRED_AMOUNT,
//                  sum( mc.M3_CAT_01_INTCH )                                       MC_CAT_01_INTCH,
//                  sum( mc.M3_CAT_02_SALES_COUNT )                                 MC_CAT_02_SALES_COUNT,
//                  sum( mc.M3_CAT_02_SALES_AMOUNT )                                MC_CAT_02_SALES_AMOUNT,
//                  sum( mc.M3_CAT_02_CRED_COUNT )                                  MC_CAT_02_CRED_COUNT,
//                  sum( mc.M3_CAT_02_CRED_AMOUNT )                                 MC_CAT_02_CRED_AMOUNT,
//                  sum( mc.M3_CAT_02_INTCH )                                       MC_CAT_02_INTCH,
//                  sum( mc.M3_CAT_03_SALES_COUNT )                                 MC_CAT_03_SALES_COUNT,
//                  sum( mc.M3_CAT_03_SALES_AMOUNT )                                MC_CAT_03_SALES_AMOUNT,
//                  sum( mc.M3_CAT_03_CRED_COUNT )                                  MC_CAT_03_CRED_COUNT,
//                  sum( mc.M3_CAT_03_CRED_AMOUNT )                                 MC_CAT_03_CRED_AMOUNT,
//                  sum( mc.M3_CAT_03_INTCH )                                       MC_CAT_03_INTCH,
//                  sum( mc.M3_CAT_04_SALES_COUNT )                                 MC_CAT_04_SALES_COUNT,
//                  sum( mc.M3_CAT_04_SALES_AMOUNT )                                MC_CAT_04_SALES_AMOUNT,
//                  sum( mc.M3_CAT_04_CRED_COUNT )                                  MC_CAT_04_CRED_COUNT,
//                  sum( mc.M3_CAT_04_CRED_AMOUNT )                                 MC_CAT_04_CRED_AMOUNT,
//                  sum( mc.M3_CAT_04_INTCH )                                       MC_CAT_04_INTCH,
//                  sum( mc.M3_CAT_05_SALES_COUNT )                                 MC_CAT_05_SALES_COUNT,
//                  sum( mc.M3_CAT_05_SALES_AMOUNT )                                MC_CAT_05_SALES_AMOUNT,
//                  sum( mc.M3_CAT_05_CRED_COUNT )                                  MC_CAT_05_CRED_COUNT,
//                  sum( mc.M3_CAT_05_CRED_AMOUNT )                                 MC_CAT_05_CRED_AMOUNT,
//                  sum( mc.M3_CAT_05_INTCH )                                       MC_CAT_05_INTCH,
//                  sum( mc.M3_CAT_06_SALES_COUNT )                                 MC_CAT_06_SALES_COUNT,
//                  sum( mc.M3_CAT_06_SALES_AMOUNT )                                MC_CAT_06_SALES_AMOUNT,
//                  sum( mc.M3_CAT_06_CRED_COUNT )                                  MC_CAT_06_CRED_COUNT,
//                  sum( mc.M3_CAT_06_CRED_AMOUNT )                                 MC_CAT_06_CRED_AMOUNT,
//                  sum( mc.M4_CAT_06_INTCH )                                       MC_CAT_06_INTCH,
//                  sum( mc.M4_CAT_07_SALES_COUNT )                                 MC_CAT_07_SALES_COUNT,
//                  sum( mc.M4_CAT_07_SALES_AMOUNT )                                MC_CAT_07_SALES_AMOUNT,
//                  sum( mc.M4_CAT_07_CRED_COUNT )                                  MC_CAT_07_CRED_COUNT,
//                  sum( mc.M4_CAT_07_CRED_AMOUNT )                                 MC_CAT_07_CRED_AMOUNT,
//                  sum( mc.M4_CAT_07_INTCH )                                       MC_CAT_07_INTCH,
//                  sum( mc.M4_CAT_08_SALES_COUNT )                                 MC_CAT_08_SALES_COUNT,
//                  sum( mc.M4_CAT_08_SALES_AMOUNT )                                MC_CAT_08_SALES_AMOUNT,
//                  sum( mc.M4_CAT_08_CRED_COUNT )                                  MC_CAT_08_CRED_COUNT,
//                  sum( mc.M4_CAT_08_CRED_AMOUNT )                                 MC_CAT_08_CRED_AMOUNT,
//                  sum( mc.M4_CAT_08_INTCH )                                       MC_CAT_08_INTCH,
//                  sum( mc.M4_CAT_09_SALES_COUNT )                                 MC_CAT_09_SALES_COUNT,
//                  sum( mc.M4_CAT_09_SALES_AMOUNT )                                MC_CAT_09_SALES_AMOUNT,
//                  sum( mc.M4_CAT_09_CRED_COUNT )                                  MC_CAT_09_CRED_COUNT,
//                  sum( mc.M4_CAT_09_CRED_AMOUNT )                                 MC_CAT_09_CRED_AMOUNT,
//                  sum( mc.M4_CAT_09_INTCH )                                       MC_CAT_09_INTCH,
//                  sum( mc.M4_CAT_10_SALES_COUNT )                                 MC_CAT_10_SALES_COUNT,
//                  sum( mc.M4_CAT_10_SALES_AMOUNT )                                MC_CAT_10_SALES_AMOUNT,
//                  sum( mc.M4_CAT_10_CRED_COUNT )                                  MC_CAT_10_CRED_COUNT,
//                  sum( mc.M4_CAT_10_CRED_AMOUNT )                                 MC_CAT_10_CRED_AMOUNT,
//                  sum( mc.M4_CAT_10_INTCH )                                       MC_CAT_10_INTCH,
//                  sum( mc.M4_CAT_11_SALES_COUNT )                                 MC_CAT_11_SALES_COUNT,
//                  sum( mc.M4_CAT_11_SALES_AMOUNT )                                MC_CAT_11_SALES_AMOUNT,
//                  sum( mc.M4_CAT_11_CRED_COUNT )                                  MC_CAT_11_CRED_COUNT,
//                  sum( mc.M4_CAT_11_CRED_AMOUNT )                                 MC_CAT_11_CRED_AMOUNT,
//                  sum( mc.M4_CAT_11_INTCH )                                       MC_CAT_11_INTCH,
//                  sum( mc.M4_CAT_12_SALES_COUNT )                                 MC_CAT_12_SALES_COUNT,
//                  sum( mc.M4_CAT_12_SALES_AMOUNT )                                MC_CAT_12_SALES_AMOUNT,
//                  sum( mc.M4_CAT_12_CRED_COUNT )                                  MC_CAT_12_CRED_COUNT,
//                  sum( mc.M5_CAT_12_CRED_AMOUNT )                                 MC_CAT_12_CRED_AMOUNT,
//                  sum( mc.M5_CAT_12_INTCH )                                       MC_CAT_12_INTCH
//          from    monthly_extract_gn   gn,
//                  monthly_extract_visa visa,
//                  monthly_extract_mc   mc
//          where   gn.hh_merchant_number in
//                    ( select merchant_number
//                      from   group_merchant
//                      where  org_num = :orgId ) and
//                  gn.hh_active_date between :beginDate and last_day( :endDate ) and
//                  visa.hh_load_sec(+) = gn.hh_load_sec and
//                  mc.hh_load_sec(+) = gn.hh_load_sec
//          group by (gn.hh_bank_number || gn.g1_association_number), gn.hh_merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ( gn.hh_bank_number || gn.g1_association_number )               assoc_number,\n                gn.hh_merchant_number                                           merchant_number,\n                sum( visa.V1_INCOME_BULLETIN )                                  VS_INC_BULLETIN,\n                sum( decode( gn.hh_bank_number, \n                              :1 , 0,\n                             visa.V1_INCOME_INTRA_CHANGE ) )                    VS_INC_INTRA_CHANGE,\n                sum( visa.V1_INCOME_ASSESSMENT )                                VS_INC_ASSESSMENT,\n                sum( visa.V1_INC_CAT_01_SALES + visa.V1_INC_CAT_01_FEES )       VS_CAT_01_INC,\n                sum( visa.V1_INC_CAT_02_SALES + visa.V1_INC_CAT_02_FEES )       VS_CAT_02_INC,\n                sum( visa.V1_INC_CAT_03_SALES + visa.V1_INC_CAT_03_FEES )       VS_CAT_03_INC,\n                sum( visa.V1_INC_CAT_04_SALES + visa.V1_INC_CAT_04_FEES )       VS_CAT_04_INC,\n                sum( visa.V1_INC_CAT_05_SALES + visa.V1_INC_CAT_05_FEES )       VS_CAT_05_INC,\n                sum( visa.V1_INC_CAT_06_SALES + visa.V1_INC_CAT_06_FEES )       VS_CAT_06_INC,\n                sum( visa.V1_INC_CAT_07_SALES + visa.V1_INC_CAT_07_FEES )       VS_CAT_07_INC,\n                sum( visa.V1_INC_CAT_08_SALES + visa.V1_INC_CAT_08_FEES )       VS_CAT_08_INC,\n                sum( visa.V1_INC_CAT_09_SALES + visa.V1_INC_CAT_09_FEES )       VS_CAT_09_INC,\n                sum( visa.V1_INC_CAT_10_SALES + visa.V1_INC_CAT_10_FEES )       VS_CAT_10_INC,\n                sum( visa.V1_INC_CAT_11_SALES + visa.V1_INC_CAT_11_FEES )       VS_CAT_11_INC,\n                sum( visa.V1_INC_CAT_12_SALES + visa.V1_INC_CAT_12_FEES )       VS_CAT_12_INC,\n                sum( visa.V1_INC_CAT_13_SALES + visa.V1_INC_CAT_13_FEES )       VS_CAT_13_INC,\n                sum( visa.V1_INC_CAT_14_SALES + visa.V1_INC_CAT_14_FEES )       VS_CAT_14_INC,\n                sum( visa.V1_INC_CAT_15_SALES + visa.V1_INC_CAT_15_FEES )       VS_CAT_15_INC,\n                sum( visa.V1_INC_CAT_16_SALES + visa.V1_INC_CAT_16_FEES )       VS_CAT_16_INC,\n                sum( visa.V1_INC_CAT_17_SALES + visa.V2_INC_CAT_17_FEES )       VS_CAT_17_INC,\n                sum( visa.V2_INC_CAT_18_SALES + visa.V2_INC_CAT_18_FEES )       VS_CAT_18_INC,\n                sum( visa.V2_INC_CAT_19_SALES + visa.V2_INC_CAT_19_FEES )       VS_CAT_19_INC,\n                sum( visa.V2_INC_CAT_20_SALES + visa.V2_INC_CAT_20_FEES )       VS_CAT_20_INC,\n                sum( visa.V2_INC_CAT_21_SALES + visa.V2_INC_CAT_21_FEES )       VS_CAT_21_INC,\n                sum( visa.V2_INC_CAT_22_SALES + visa.V2_INC_CAT_22_FEES )       VS_CAT_22_INC,\n                sum( visa.V2_INC_CAT_23_SALES + visa.V2_INC_CAT_23_FEES )       VS_CAT_23_INC,\n                sum( visa.V2_INC_CAT_24_SALES + visa.V2_INC_CAT_24_FEES )       VS_CAT_24_INC,\n                sum( /* no cat 25 sales */      visa.V2_INC_CAT_25_FEES )       VS_CAT_25_INC,\n                sum( visa.V3_EXPENSE_BULLETIN )                                 VS_EXP_BULLETIN,\n                sum( decode( gn.hh_bank_number,\n                              :2 , 0,\n                             visa.V3_EXPENSE_INTRA_CHANGE ) )                   VS_EXP_INTRA_CHANGE,\n                sum( visa.V3_EXPENSE_ASSESSMENT )                               VS_EXP_ASSESSMENT,\n                sum( mc.M1_INCOME_BULLETIN )                                    MC_INC_BULLETIN,\n                sum( decode( gn.hh_bank_number,\n                              :3 , 0,\n                             mc.M1_INCOME_INTRA_CHANGE ) )                      MC_INC_INTRA_CHANGE,\n                sum( mc.M1_INCOME_ASSESSMENT )                                  MC_INC_ASSESSMENT,\n                sum( mc.M1_INC_CAT_01_SALES + mc.M1_INC_CAT_01_FEES )           MC_CAT_01_INC,\n                sum( mc.M1_INC_CAT_02_SALES + mc.M1_INC_CAT_02_FEES )           MC_CAT_02_INC,\n                sum( mc.M1_INC_CAT_03_SALES + mc.M1_INC_CAT_03_FEES )           MC_CAT_03_INC,\n                sum( mc.M1_INC_CAT_04_SALES + mc.M1_INC_CAT_04_FEES )           MC_CAT_04_INC,\n                sum( mc.M1_INC_CAT_05_SALES + mc.M1_INC_CAT_05_FEES )           MC_CAT_05_INC,\n                sum( mc.M1_INC_CAT_06_SALES + mc.M1_INC_CAT_06_FEES )           MC_CAT_06_INC,\n                sum( mc.M1_INC_CAT_07_SALES + mc.M1_INC_CAT_07_FEES )           MC_CAT_07_INC,\n                sum( mc.M1_INC_CAT_08_SALES + mc.M1_INC_CAT_08_FEES )           MC_CAT_08_INC,\n                sum( mc.M1_INC_CAT_09_SALES + mc.M1_INC_CAT_09_FEES )           MC_CAT_09_INC,\n                sum( mc.M1_INC_CAT_10_SALES + mc.M1_INC_CAT_10_FEES )           MC_CAT_10_INC,\n                sum( mc.M1_INC_CAT_11_SALES + mc.M1_INC_CAT_11_FEES )           MC_CAT_11_INC,\n                sum( mc.M1_INC_CAT_12_SALES + mc.M1_INC_CAT_12_FEES )           MC_CAT_12_INC,\n                sum( mc.M2_EXPENSE_BULLETIN )                                   MC_EXP_BULLETIN,\n                sum( decode( gn.hh_bank_number,\n                              :4 , 0,\n                             mc.M2_EXPENSE_INTRA_CHANGE ) )                     MC_EXP_INTRA_CHANGE,\n                sum( mc.M2_EXPENSE_ASSESSMENT )                                 MC_EXP_ASSESSMENT,\n                sum( visa.V5_CAT_01_SALES_COUNT )                               VS_CAT_01_SALES_COUNT,\n                sum( visa.V5_CAT_01_SALES_AMOUNT )                              VS_CAT_01_SALES_AMOUNT,\n                sum( visa.V5_CAT_01_CRED_COUNT )                                VS_CAT_01_CRED_COUNT,\n                sum( visa.V5_CAT_01_CRED_AMOUNT )                               VS_CAT_01_CRED_AMOUNT,\n                sum( visa.V5_CAT_01_INTCH )                                     VS_CAT_01_INTCH,\n                sum( visa.V5_CAT_02_SALES_COUNT )                               VS_CAT_02_SALES_COUNT,\n                sum( visa.V5_CAT_02_SALES_AMOUNT )                              VS_CAT_02_SALES_AMOUNT,\n                sum( visa.V5_CAT_02_CRED_COUNT )                                VS_CAT_02_CRED_COUNT,\n                sum( visa.V5_CAT_02_CRED_AMOUNT )                               VS_CAT_02_CRED_AMOUNT,\n                sum( visa.V5_CAT_02_INTCH )                                     VS_CAT_02_INTCH,\n                sum( visa.V5_CAT_03_SALES_COUNT )                               VS_CAT_03_SALES_COUNT,\n                sum( visa.V5_CAT_03_SALES_AMOUNT )                              VS_CAT_03_SALES_AMOUNT,\n                sum( visa.V5_CAT_03_CRED_COUNT )                                VS_CAT_03_CRED_COUNT,\n                sum( visa.V5_CAT_03_CRED_AMOUNT )                               VS_CAT_03_CRED_AMOUNT,\n                sum( visa.V5_CAT_03_INTCH )                                     VS_CAT_03_INTCH,\n                sum( visa.V5_CAT_04_SALES_COUNT )                               VS_CAT_04_SALES_COUNT,\n                sum( visa.V5_CAT_04_SALES_AMOUNT )                              VS_CAT_04_SALES_AMOUNT,\n                sum( visa.V5_CAT_04_CRED_COUNT )                                VS_CAT_04_CRED_COUNT,\n                sum( visa.V5_CAT_04_CRED_AMOUNT )                               VS_CAT_04_CRED_AMOUNT,\n                sum( visa.V5_CAT_04_INTCH )                                     VS_CAT_04_INTCH,\n                sum( visa.V5_CAT_05_SALES_COUNT )                               VS_CAT_05_SALES_COUNT,\n                sum( visa.V5_CAT_05_SALES_AMOUNT )                              VS_CAT_05_SALES_AMOUNT,\n                sum( visa.V5_CAT_05_CRED_COUNT )                                VS_CAT_05_CRED_COUNT,\n                sum( visa.V5_CAT_05_CRED_AMOUNT )                               VS_CAT_05_CRED_AMOUNT,\n                sum( visa.V5_CAT_05_INTCH )                                     VS_CAT_05_INTCH,\n                sum( visa.V5_CAT_06_SALES_COUNT )                               VS_CAT_06_SALES_COUNT,\n                sum( visa.V5_CAT_06_SALES_AMOUNT )                              VS_CAT_06_SALES_AMOUNT,\n                sum( visa.V5_CAT_06_CRED_COUNT )                                VS_CAT_06_CRED_COUNT,\n                sum( visa.V5_CAT_06_CRED_AMOUNT )                               VS_CAT_06_CRED_AMOUNT,\n                sum( visa.V6_CAT_06_INTCH )                                     VS_CAT_06_INTCH,\n                sum( visa.V6_CAT_07_SALES_COUNT )                               VS_CAT_07_SALES_COUNT,\n                sum( visa.V6_CAT_07_SALES_AMOUNT )                              VS_CAT_07_SALES_AMOUNT,\n                sum( visa.V6_CAT_07_CRED_COUNT )                                VS_CAT_07_CRED_COUNT,\n                sum( visa.V6_CAT_07_CRED_AMOUNT )                               VS_CAT_07_CRED_AMOUNT,\n                sum( visa.V6_CAT_07_INTCH )                                     VS_CAT_07_INTCH,\n                sum( visa.V6_CAT_08_SALES_COUNT )                               VS_CAT_08_SALES_COUNT,\n                sum( visa.V6_CAT_08_SALES_AMOUNT )                              VS_CAT_08_SALES_AMOUNT,\n                sum( visa.V6_CAT_08_CRED_COUNT )                                VS_CAT_08_CRED_COUNT,\n                sum( visa.V6_CAT_08_CRED_AMOUNT )                               VS_CAT_08_CRED_AMOUNT,\n                sum( visa.V6_CAT_08_INTCH )                                     VS_CAT_08_INTCH,\n                sum( visa.V6_CAT_09_SALES_COUNT )                               VS_CAT_09_SALES_COUNT,\n                sum( visa.V6_CAT_09_SALES_AMOUNT )                              VS_CAT_09_SALES_AMOUNT,\n                sum( visa.V6_CAT_09_CRED_COUNT )                                VS_CAT_09_CRED_COUNT,\n                sum( visa.V6_CAT_09_CRED_AMOUNT )                               VS_CAT_09_CRED_AMOUNT,\n                sum( visa.V6_CAT_09_INTCH )                                     VS_CAT_09_INTCH,\n                sum( visa.V6_CAT_10_SALES_COUNT )                               VS_CAT_10_SALES_COUNT,\n                sum( visa.V6_CAT_10_SALES_AMOUNT )                              VS_CAT_10_SALES_AMOUNT,\n                sum( visa.V6_CAT_10_CRED_COUNT )                                VS_CAT_10_CRED_COUNT,\n                sum( visa.V6_CAT_10_CRED_AMOUNT )                               VS_CAT_10_CRED_AMOUNT,\n                sum( visa.V6_CAT_10_INTCH )                                     VS_CAT_10_INTCH,\n                sum( visa.V6_CAT_11_SALES_COUNT )                               VS_CAT_11_SALES_COUNT,\n                sum( visa.V6_CAT_11_SALES_AMOUNT )                              VS_CAT_11_SALES_AMOUNT,\n                sum( visa.V6_CAT_11_CRED_COUNT )                                VS_CAT_11_CRED_COUNT,\n                sum( visa.V6_CAT_11_CRED_AMOUNT )                               VS_CAT_11_CRED_AMOUNT,\n                sum( visa.V6_CAT_11_INTCH )                                     VS_CAT_11_INTCH,\n                sum( visa.V6_CAT_12_SALES_COUNT )                               VS_CAT_12_SALES_COUNT,\n                sum( visa.V6_CAT_12_SALES_AMOUNT )                              VS_CAT_12_SALES_AMOUNT,\n                sum( visa.V6_CAT_12_CRED_COUNT )                                VS_CAT_12_CRED_COUNT,\n                sum( visa.V7_CAT_12_CRED_AMOUNT )                               VS_CAT_12_CRED_AMOUNT,\n                sum( visa.V7_CAT_12_INTCH )                                     VS_CAT_12_INTCH,\n                sum( visa.V7_CAT_13_SALES_COUNT )                               VS_CAT_13_SALES_COUNT,\n                sum( visa.V7_CAT_13_SALES_AMOUNT )                              VS_CAT_13_SALES_AMOUNT,\n                sum( visa.V7_CAT_13_CRED_COUNT )                                VS_CAT_13_CRED_COUNT,\n                sum( visa.V7_CAT_13_CRED_AMOUNT )                               VS_CAT_13_CRED_AMOUNT,\n                sum( visa.V7_CAT_13_INTCH )                                     VS_CAT_13_INTCH,\n                sum( visa.V7_CAT_14_SALES_COUNT )                               VS_CAT_14_SALES_COUNT,\n                sum( visa.V7_CAT_14_SALES_AMOUNT )                              VS_CAT_14_SALES_AMOUNT,\n                sum( visa.V7_CAT_14_CRED_COUNT )                                VS_CAT_14_CRED_COUNT,\n                sum( visa.V7_CAT_14_CRED_AMOUNT )                               VS_CAT_14_CRED_AMOUNT,\n                sum( visa.V7_CAT_14_INTCH )                                     VS_CAT_14_INTCH,\n                sum( visa.V7_CAT_15_SALES_COUNT )                               VS_CAT_15_SALES_COUNT,\n                sum( visa.V7_CAT_15_SALES_AMOUNT )                              VS_CAT_15_SALES_AMOUNT,\n                sum( visa.V7_CAT_15_CRED_COUNT )                                VS_CAT_15_CRED_COUNT,\n                sum( visa.V7_CAT_15_CRED_AMOUNT )                               VS_CAT_15_CRED_AMOUNT,\n                sum( visa.V7_CAT_15_INTCH )                                     VS_CAT_15_INTCH,\n                sum( visa.V7_CAT_16_SALES_COUNT )                               VS_CAT_16_SALES_COUNT,\n                sum( visa.V7_CAT_16_SALES_AMOUNT )                              VS_CAT_16_SALES_AMOUNT,\n                sum( visa.V7_CAT_16_CRED_COUNT )                                VS_CAT_16_CRED_COUNT,\n                sum( visa.V7_CAT_16_CRED_AMOUNT )                               VS_CAT_16_CRED_AMOUNT,\n                sum( visa.V7_CAT_16_INTCH )                                     VS_CAT_16_INTCH,\n                sum( visa.V7_CAT_17_SALES_COUNT )                               VS_CAT_17_SALES_COUNT,\n                sum( visa.V7_CAT_17_SALES_AMOUNT )                              VS_CAT_17_SALES_AMOUNT,\n                sum( visa.V7_CAT_17_CRED_COUNT )                                VS_CAT_17_CRED_COUNT,\n                sum( visa.V7_CAT_17_CRED_AMOUNT )                               VS_CAT_17_CRED_AMOUNT,\n                sum( visa.V7_CAT_17_INTCH )                                     VS_CAT_17_INTCH,\n                sum( visa.V7_CAT_18_SALES_COUNT )                               VS_CAT_18_SALES_COUNT,\n                sum( visa.V8_CAT_18_SALES_AMOUNT )                              VS_CAT_18_SALES_AMOUNT,\n                sum( visa.V8_CAT_18_CRED_COUNT )                                VS_CAT_18_CRED_COUNT,\n                sum( visa.V8_CAT_18_CRED_AMOUNT )                               VS_CAT_18_CRED_AMOUNT,\n                sum( visa.V8_CAT_18_INTCH )                                     VS_CAT_18_INTCH,\n                sum( visa.V8_CAT_19_SALES_COUNT )                               VS_CAT_19_SALES_COUNT,\n                sum( visa.V8_CAT_19_SALES_AMOUNT )                              VS_CAT_19_SALES_AMOUNT,\n                sum( visa.V8_CAT_19_CRED_COUNT )                                VS_CAT_19_CRED_COUNT,\n                sum( visa.V8_CAT_19_CRED_AMOUNT )                               VS_CAT_19_CRED_AMOUNT,\n                sum( visa.V8_CAT_19_INTCH )                                     VS_CAT_19_INTCH,\n                sum( visa.V8_CAT_20_SALES_COUNT )                               VS_CAT_20_SALES_COUNT,\n                sum( visa.V8_CAT_20_SALES_AMOUNT )                              VS_CAT_20_SALES_AMOUNT,\n                sum( visa.V8_CAT_20_CRED_COUNT )                                VS_CAT_20_CRED_COUNT,\n                sum( visa.V8_CAT_20_CRED_AMOUNT )                               VS_CAT_20_CRED_AMOUNT,\n                sum( visa.V8_CAT_20_INTCH )                                     VS_CAT_20_INTCH,\n                sum( visa.V8_CAT_21_SALES_COUNT )                               VS_CAT_21_SALES_COUNT,\n                sum( visa.V8_CAT_21_SALES_AMOUNT )                              VS_CAT_21_SALES_AMOUNT,\n                sum( visa.V8_CAT_21_CRED_COUNT )                                VS_CAT_21_CRED_COUNT,\n                sum( visa.V8_CAT_21_CRED_AMOUNT )                               VS_CAT_21_CRED_AMOUNT,\n                sum( visa.V8_CAT_21_INTCH )                                     VS_CAT_21_INTCH,\n                sum( visa.V8_CAT_22_SALES_COUNT )                               VS_CAT_22_SALES_COUNT,\n                sum( visa.V8_CAT_22_SALES_AMOUNT )                              VS_CAT_22_SALES_AMOUNT,\n                sum( visa.V8_CAT_22_CRED_COUNT )                                VS_CAT_22_CRED_COUNT,\n                sum( visa.V8_CAT_22_CRED_AMOUNT )                               VS_CAT_22_CRED_AMOUNT,\n                sum( visa.V8_CAT_22_INTCH )                                     VS_CAT_22_INTCH,\n                sum( visa.V8_CAT_23_SALES_COUNT )                               VS_CAT_23_SALES_COUNT,\n                sum( visa.V8_CAT_23_SALES_AMOUNT )                              VS_CAT_23_SALES_AMOUNT,\n                sum( visa.V8_CAT_23_CRED_COUNT )                                VS_CAT_23_CRED_COUNT,\n                sum( visa.V8_CAT_23_CRED_AMOUNT )                               VS_CAT_23_CRED_AMOUNT,\n                sum( visa.V9_CAT_23_INTCH )                                     VS_CAT_23_INTCH,\n                sum( visa.V9_CAT_24_SALES_COUNT )                               VS_CAT_24_SALES_COUNT,\n                sum( visa.V9_CAT_24_SALES_AMOUNT )                              VS_CAT_24_SALES_AMOUNT,\n                sum( visa.V9_CAT_24_CRED_COUNT )                                VS_CAT_24_CRED_COUNT,\n                sum( visa.V9_CAT_24_CRED_AMOUNT )                               VS_CAT_24_CRED_AMOUNT,\n                sum( visa.V9_CAT_24_INTCH )                                     VS_CAT_24_INTCH,\n                0                                                               VS_CAT_25_SALES_COUNT,\n                0                                                               VS_CAT_25_SALES_AMOUNT,\n                0                                                               VS_CAT_25_CRED_COUNT,\n                0                                                               VS_CAT_25_CRED_AMOUNT,\n                sum( visa.V9_CAT_25_INTCH )                                     VS_CAT_25_INTCH,\n                sum( mc.M3_CAT_01_SALES_COUNT )                                 MC_CAT_01_SALES_COUNT,\n                sum( mc.M3_CAT_01_SALES_AMOUNT )                                MC_CAT_01_SALES_AMOUNT,\n                sum( mc.M3_CAT_01_CRED_COUNT )                                  MC_CAT_01_CRED_COUNT,\n                sum( mc.M3_CAT_01_CRED_AMOUNT )                                 MC_CAT_01_CRED_AMOUNT,\n                sum( mc.M3_CAT_01_INTCH )                                       MC_CAT_01_INTCH,\n                sum( mc.M3_CAT_02_SALES_COUNT )                                 MC_CAT_02_SALES_COUNT,\n                sum( mc.M3_CAT_02_SALES_AMOUNT )                                MC_CAT_02_SALES_AMOUNT,\n                sum( mc.M3_CAT_02_CRED_COUNT )                                  MC_CAT_02_CRED_COUNT,\n                sum( mc.M3_CAT_02_CRED_AMOUNT )                                 MC_CAT_02_CRED_AMOUNT,\n                sum( mc.M3_CAT_02_INTCH )                                       MC_CAT_02_INTCH,\n                sum( mc.M3_CAT_03_SALES_COUNT )                                 MC_CAT_03_SALES_COUNT,\n                sum( mc.M3_CAT_03_SALES_AMOUNT )                                MC_CAT_03_SALES_AMOUNT,\n                sum( mc.M3_CAT_03_CRED_COUNT )                                  MC_CAT_03_CRED_COUNT,\n                sum( mc.M3_CAT_03_CRED_AMOUNT )                                 MC_CAT_03_CRED_AMOUNT,\n                sum( mc.M3_CAT_03_INTCH )                                       MC_CAT_03_INTCH,\n                sum( mc.M3_CAT_04_SALES_COUNT )                                 MC_CAT_04_SALES_COUNT,\n                sum( mc.M3_CAT_04_SALES_AMOUNT )                                MC_CAT_04_SALES_AMOUNT,\n                sum( mc.M3_CAT_04_CRED_COUNT )                                  MC_CAT_04_CRED_COUNT,\n                sum( mc.M3_CAT_04_CRED_AMOUNT )                                 MC_CAT_04_CRED_AMOUNT,\n                sum( mc.M3_CAT_04_INTCH )                                       MC_CAT_04_INTCH,\n                sum( mc.M3_CAT_05_SALES_COUNT )                                 MC_CAT_05_SALES_COUNT,\n                sum( mc.M3_CAT_05_SALES_AMOUNT )                                MC_CAT_05_SALES_AMOUNT,\n                sum( mc.M3_CAT_05_CRED_COUNT )                                  MC_CAT_05_CRED_COUNT,\n                sum( mc.M3_CAT_05_CRED_AMOUNT )                                 MC_CAT_05_CRED_AMOUNT,\n                sum( mc.M3_CAT_05_INTCH )                                       MC_CAT_05_INTCH,\n                sum( mc.M3_CAT_06_SALES_COUNT )                                 MC_CAT_06_SALES_COUNT,\n                sum( mc.M3_CAT_06_SALES_AMOUNT )                                MC_CAT_06_SALES_AMOUNT,\n                sum( mc.M3_CAT_06_CRED_COUNT )                                  MC_CAT_06_CRED_COUNT,\n                sum( mc.M3_CAT_06_CRED_AMOUNT )                                 MC_CAT_06_CRED_AMOUNT,\n                sum( mc.M4_CAT_06_INTCH )                                       MC_CAT_06_INTCH,\n                sum( mc.M4_CAT_07_SALES_COUNT )                                 MC_CAT_07_SALES_COUNT,\n                sum( mc.M4_CAT_07_SALES_AMOUNT )                                MC_CAT_07_SALES_AMOUNT,\n                sum( mc.M4_CAT_07_CRED_COUNT )                                  MC_CAT_07_CRED_COUNT,\n                sum( mc.M4_CAT_07_CRED_AMOUNT )                                 MC_CAT_07_CRED_AMOUNT,\n                sum( mc.M4_CAT_07_INTCH )                                       MC_CAT_07_INTCH,\n                sum( mc.M4_CAT_08_SALES_COUNT )                                 MC_CAT_08_SALES_COUNT,\n                sum( mc.M4_CAT_08_SALES_AMOUNT )                                MC_CAT_08_SALES_AMOUNT,\n                sum( mc.M4_CAT_08_CRED_COUNT )                                  MC_CAT_08_CRED_COUNT,\n                sum( mc.M4_CAT_08_CRED_AMOUNT )                                 MC_CAT_08_CRED_AMOUNT,\n                sum( mc.M4_CAT_08_INTCH )                                       MC_CAT_08_INTCH,\n                sum( mc.M4_CAT_09_SALES_COUNT )                                 MC_CAT_09_SALES_COUNT,\n                sum( mc.M4_CAT_09_SALES_AMOUNT )                                MC_CAT_09_SALES_AMOUNT,\n                sum( mc.M4_CAT_09_CRED_COUNT )                                  MC_CAT_09_CRED_COUNT,\n                sum( mc.M4_CAT_09_CRED_AMOUNT )                                 MC_CAT_09_CRED_AMOUNT,\n                sum( mc.M4_CAT_09_INTCH )                                       MC_CAT_09_INTCH,\n                sum( mc.M4_CAT_10_SALES_COUNT )                                 MC_CAT_10_SALES_COUNT,\n                sum( mc.M4_CAT_10_SALES_AMOUNT )                                MC_CAT_10_SALES_AMOUNT,\n                sum( mc.M4_CAT_10_CRED_COUNT )                                  MC_CAT_10_CRED_COUNT,\n                sum( mc.M4_CAT_10_CRED_AMOUNT )                                 MC_CAT_10_CRED_AMOUNT,\n                sum( mc.M4_CAT_10_INTCH )                                       MC_CAT_10_INTCH,\n                sum( mc.M4_CAT_11_SALES_COUNT )                                 MC_CAT_11_SALES_COUNT,\n                sum( mc.M4_CAT_11_SALES_AMOUNT )                                MC_CAT_11_SALES_AMOUNT,\n                sum( mc.M4_CAT_11_CRED_COUNT )                                  MC_CAT_11_CRED_COUNT,\n                sum( mc.M4_CAT_11_CRED_AMOUNT )                                 MC_CAT_11_CRED_AMOUNT,\n                sum( mc.M4_CAT_11_INTCH )                                       MC_CAT_11_INTCH,\n                sum( mc.M4_CAT_12_SALES_COUNT )                                 MC_CAT_12_SALES_COUNT,\n                sum( mc.M4_CAT_12_SALES_AMOUNT )                                MC_CAT_12_SALES_AMOUNT,\n                sum( mc.M4_CAT_12_CRED_COUNT )                                  MC_CAT_12_CRED_COUNT,\n                sum( mc.M5_CAT_12_CRED_AMOUNT )                                 MC_CAT_12_CRED_AMOUNT,\n                sum( mc.M5_CAT_12_INTCH )                                       MC_CAT_12_INTCH\n        from    monthly_extract_gn   gn,\n                monthly_extract_visa visa,\n                monthly_extract_mc   mc\n        where   gn.hh_merchant_number in\n                  ( select merchant_number\n                    from   group_merchant\n                    where  org_num =  :5  ) and\n                gn.hh_active_date between  :6  and last_day(  :7  ) and\n                visa.hh_load_sec(+) = gn.hh_load_sec and\n                mc.hh_load_sec(+) = gn.hh_load_sec\n        group by (gn.hh_bank_number || gn.g1_association_number), gn.hh_merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ProfIcVitalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.BANK_ID_MES);
   __sJT_st.setInt(2,mesConstants.BANK_ID_MES);
   __sJT_st.setInt(3,mesConstants.BANK_ID_MES);
   __sJT_st.setInt(4,mesConstants.BANK_ID_MES);
   __sJT_st.setLong(5,orgId);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.ProfIcVitalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:622^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        // store the merchant specific data
        assocId     = resultSet.getLong("ASSOC_NUMBER");
        merchantId  = resultSet.getLong("MERCHANT_NUMBER");
      
        for ( int icCardType = 0; icCardType < IC_CARD_TYPES; ++icCardType )
        {
          switch( icCardType )
          {
            case IC_VISA:       // visa sales/fees
              cardType  = mesConstants.VITAL_ME_AP_VISA;
              catCount  = 25;
              break;
          
            case IC_MC:       // mc sales/fees
              cardType  = mesConstants.VITAL_ME_AP_MC;
              catCount  = 12;
              break;
          
//            case IC_DINERS:
//              desc      = "Diners Interchange";
//              cardType  = mesConstants.VITAL_ME_AP_DINERS;
//              catCount  = 5;
//              break;
            
            default:
              continue;
          }
          icRecord = new InterchangeData( icCardType, catCount );
          
          // cats are 1 based index, so go from 1 to the catCount
          for( int cat = 1; cat <= catCount; ++cat )
          {
            fieldPrefix  = cardType + "_CAT_" + NumberFormatter.getPaddedInt(cat,2);
      
            // this is necessary because vital sends # transactions
            // in the # sales column.  Occasionally (due to system error)
            // the # credits > # sales so it is necessary to guard
            // against underflow.
            //
            salesCount = resultSet.getInt( fieldPrefix + "_SALES_COUNT" ) -
                         resultSet.getInt( fieldPrefix + "_CRED_COUNT" );
            if ( salesCount < 0 )
            {
              salesCount = 0;
            } 
          
            if ( maskCashAdvanceIc == true )
            {
              switch( icCardType )
              {
                case IC_VISA:
                  if ( ( cat == mesConstants.IC_VS_CAT_DOMESTIC_CASH ) ||
                       ( cat == mesConstants.IC_VS_CAT_FOREIGN_CASH ) )
                  {
                    if ( salesCount > 0 )
                    {
                      hasCashAdvanceIc = true;
                    }
                    continue;     // skip visa cash advances
                  }                       
                  break;
                  
                case IC_MC:
                  if ( ( cat == mesConstants.IC_MC_CAT_DOMESTIC_CASH ) ||
                       ( cat == mesConstants.IC_MC_CAT_FOREIGN_CASH ) )
                  {
                    if ( salesCount > 0 )
                    {
                      hasCashAdvanceIc = true;
                    }
                    continue;     // skip mastercard cash advances
                  }                       
                  break;
                  
                default:
                  break;
              }
            }
            
            // the InterchangeData object expects
            // cat to be zero based, so subtract one to
            // prevent an ArrayIndexOutOfBoundsException 
            // from being thrown.
            icRecord.setCategoryData( (cat - 1),
                                      loadVitalIcCatDesc ( cardType, (cat) ),
                                      salesCount,
                                      resultSet.getDouble( fieldPrefix + "_SALES_AMOUNT" ),
                                      resultSet.getInt   ( fieldPrefix + "_CRED_COUNT" ),
                                      resultSet.getDouble( fieldPrefix + "_CRED_AMOUNT" ),
                                      resultSet.getDouble( fieldPrefix + "_INC" ),
                                      resultSet.getDouble( fieldPrefix + "_INTCH" ) );
          }
          
          // if there is cash advance interchange present or
          // the request is to load all interchange data,
          // then all BET interchange fees are also masked.
          if ( ( hasCashAdvanceIc == false ) ||
               ( maskCashAdvanceIc == false ) )
          {
            icRecord.setFixedData( resultSet.getDouble( cardType + "_INC_ASSESSMENT" ),
                                   resultSet.getDouble( cardType + "_EXP_ASSESSMENT" ),
                                   resultSet.getDouble( cardType + "_INC_BULLETIN" ),
                                   resultSet.getDouble( cardType + "_EXP_BULLETIN" ),
                                   resultSet.getDouble( cardType + "_INC_INTRA_CHANGE" ),
                                   resultSet.getDouble( cardType + "_EXP_INTRA_CHANGE" ) );
          }
          
          // store the interchange data
          addMerchantIcData( assocId, merchantId, icCardType, icRecord );
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }
  
  public double loadSummaryExpense( long orgId, Date beginDate, Date endDate, boolean maskCashAdvanceIc )
  {
    double          actualIcExpense   = 0.0;
    double          assessmentExpense = 0.0;
    double          cashAdvIcExpense  = 0.0;
    double          estIcExpense      = 0.0;
    double          retVal            = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:759^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum( sm.BET_INTERCHANGE_EXPENSE ),
//                  sum( sm.INTERCHANGE_EXPENSE ),
//                  sum( sm.CASH_ADV_INTERCHANGE_EXPENSE ),
//                  sum( sm.VMC_ASSESSMENT_EXPENSE)
//                    
//          from    group_merchant          gm,
//                  monthly_extract_summary sm
//          where   gm.org_num = :orgId and
//                  sm.merchant_number = gm.merchant_number and 
//                  sm.active_date between :beginDate and :endDate                 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( sm.BET_INTERCHANGE_EXPENSE ),\n                sum( sm.INTERCHANGE_EXPENSE ),\n                sum( sm.CASH_ADV_INTERCHANGE_EXPENSE ),\n                sum( sm.VMC_ASSESSMENT_EXPENSE)\n                   \n        from    group_merchant          gm,\n                monthly_extract_summary sm\n        where   gm.org_num =  :1  and\n                sm.merchant_number = gm.merchant_number and \n                sm.active_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ProfIcVitalDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   estIcExpense = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   actualIcExpense = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cashAdvIcExpense = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   assessmentExpense = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:771^7*/
      if ( actualIcExpense == 0.0 )
      {
        retVal = estIcExpense;  // bet expense includes assessment
      }
      else  // use actual if possible
      {
        retVal = (actualIcExpense + assessmentExpense);
      }
      
      // if the login user has cash advance interchange
      // masked and this merchant is a cash advance merchant
      // (i.e. has items in cash advance ic categories)
      // then remove the cash advance interchange and assessment.
      if ( ( maskCashAdvanceIc == true ) &&
           ( cashAdvIcExpense != 0.0 ) )
      {
        retVal -= ( cashAdvIcExpense + assessmentExpense );
      }
    }        
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public double loadSummaryIncome( long orgId, Date beginDate, Date endDate, boolean maskCashAdvanceIc )
  {
    double          retVal            = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:803^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum( sm.tot_inc_interchange )
//          
//          from    group_merchant          gm,
//                  monthly_extract_summary sm
//          where   gm.org_num = :orgId and
//                  sm.merchant_number = gm.merchant_number and 
//                  sm.active_date between :beginDate and :endDate                 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( sm.tot_inc_interchange )\n         \n        from    group_merchant          gm,\n                monthly_extract_summary sm\n        where   gm.org_num =  :1  and\n                sm.merchant_number = gm.merchant_number and \n                sm.active_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ProfIcVitalDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:812^7*/
    }        
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public String loadVitalIcCatDesc( String cardType, int cat )
  {
    String        retVal = "Undefined IC Category";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:826^7*/

//  ************************************************************
//  #sql [Ctx] { select description 
//          from   ic_category_desc
//          where  card_type = :cardType and
//                 category  = :cat
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select description  \n        from   ic_category_desc\n        where  card_type =  :1  and\n               category  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.ProfIcVitalDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,cardType);
   __sJT_st.setInt(2,cat);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:832^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/