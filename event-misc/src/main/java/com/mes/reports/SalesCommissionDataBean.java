/*@lineinfo:filename=SalesCommissionDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/SalesCommissionDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-10-04 12:40:20 -0700 (Tue, 04 Oct 2011) $
  Version            : $Revision: 19354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesCommission;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import com.mes.tools.DropDownTable;
import com.mes.user.SalesRepRecord;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class SalesCommissionDataBean extends ReportSQLJBean
{
  public static final int   RT_REP_SUMMARY                = (RT_USER + 0);
  public static final int   RT_REP_DETAILS                = (RT_USER + 1);
  public static final int   RT_PAYMENT_SUMMARY            = (RT_USER + 2);
  public static final int   RT_REP_PAYMENT_HISTORY        = (RT_USER + 3);
  public static final int   RT_MANAGER_SUMMARY            = (RT_USER + 4);
  

  public static int PGROUP_ACTIVATION                 = 0;
  public static int PGROUP_MONTH_END                  = 1;
  public static int PGROUP_THIRD_PARTY                = 2;
  public static int PGROUP_TRUE_UP                    = 3;
  public static int PGROUP_COUNT                      = 4;
  
  private static final String[] PAYMENT_GROUP_NAMES =
  {
    "Activation Commission",      // PGROUP_ACTIVATION
    "Month End Commission",       // PGROUP_MONTH_END  
    "Third Party Commission",     // PGROUP_THIRD_PARTY
    "13 Month True Up",           // PGROUP_TRUE_UP    
  };
  
  private static final int[][] PAYMENT_GROUPS =
  {
    // PGROUP_ACTIVATION  
    {
      mesConstants.COMM_TYPE_APP_FEES,
      mesConstants.COMM_TYPE_STATEMENT_FEES_ADVANCE,
      mesConstants.COMM_TYPE_EQUIP_SERVICE_FEE_ADVANCE,
      mesConstants.COMM_TYPE_SW_FEES,
      mesConstants.COMM_TYPE_ECOMMERCE_SETUP_FEES,
      mesConstants.COMM_TYPE_EQUIP_RENTAL_ADVANCE,
      mesConstants.COMM_TYPE_EQUIP_SALES,
      mesConstants.COMM_TYPE_VMC_DISC_ADVANCED,
    },
    // PGROUP_MONTH_END   
    {
      mesConstants.COMM_TYPE_VMC_DISC_MONTHLY,
      mesConstants.COMM_TYPE_NON_BANK_FEES,
      mesConstants.COMM_TYPE_DEBIT_FEES,
      mesConstants.COMM_TYPE_ECOMMERCE_MONTHLY_FEES,
      mesConstants.COMM_TYPE_RETENTION,
      mesConstants.COMM_TYPE_VMC_DISC_ADV_MONTHLY,
      mesConstants.COMM_TYPE_EQUIP_SERVICE_FEE_MONTHLY,
      mesConstants.COMM_TYPE_STATEMENT_FEES_MONTHLY,
      mesConstants.COMM_TYPE_EQUIP_RENTAL_MONTHLY,
      mesConstants.COMM_TYPE_PERFORMANCE_BONUS,
    },
    // PGROUP_THIRD_PARTY 
    {
      mesConstants.COMM_TYPE_AMEX_ESA_BONUS,
      mesConstants.COMM_TYPE_DISCOVER_RAP_FEES,
      mesConstants.COMM_TYPE_THIRD_PARTY_OTHER,
    
    },
    // PGROUP_TRUE_UP     
    {
      mesConstants.COMM_TYPE_VMC_DISC_TRUE_UP,
      mesConstants.COMM_TYPE_EQUIP_SERVICE_FEE_TRUE_UP,
      mesConstants.COMM_TYPE_STATEMENT_FEES_TRUE_UP,
      mesConstants.COMM_TYPE_EQUIP_RENTAL_TRUE_UP,
    },
  };
  
//@  public class CommissionRecord
//@  {
//@    public    double            Advance             = 0.0;
//@    public    double            Amount              = 0.0;
//@    public    Date              CommissionDate      = null;
//@    public    int               CommissionType      = mesConstants.COMM_TYPE_NONE;
//@    public    Date              DeletedDate         = null;
//@    public    String            Description         = null;
//@    public    double            Expense             = 0.0;
//@    public    double            Income              = 0.0;
//@    public    long              ManagerId           = 0L;
//@    public    long              MerchantId          = 0L;
//@    public    Date              PayDate             = null;
//@    public    double            PayRate             = 0.0;
//@    public    double            PayRateAdvance      = 0.0;
//@    public    long              RecId               = 0L;
//@    public    String            RefNum              = null;
//@    public    long              UserId              = 0L;
//@    public    double            VolAmount           = 0.0;
//@    public    int               VolCount            = 0;
//@    
//@    public void addDesc( String desc )
//@    {
//@      StringBuffer        buf   = new StringBuffer();
//@      
//@      if ( Description != null )
//@      {
//@        buf.append( Description );
//@        buf.append( " - " );
//@      }
//@      buf.append( desc );
//@      
//@      Description = buf.toString();
//@    }
//@    
//@    public void load( ResultSet resultSet )
//@      throws java.sql.SQLException 
//@    {
//@      String        temp    = null;
//@      
//@      reset();
//@      
//@      CommissionDate    = resultSet.getDate("comm_date");
//@      CommissionType    = resultSet.getInt("comm_type");
//@    }
//@    
//@    protected void reset( )
//@    {
//@      Amount            = 0.0;
//@      CommissionDate    = null;
//@      CommissionType    = mesConstants.COMM_TYPE_NONE;
//@      DeletedDate       = null;
//@      Description       = null;
//@      Expense           = 0.0;
//@      Income            = 0.0;
//@      ManagerId         = 0L;
//@      MerchantId        = 0L;
//@      PayDate           = null;
//@      PayRate           = 0.0;
//@      PayRateAdvance    = 0.0;
//@      RefNum            = null;
//@      UserId            = 0L;
//@      VolAmount         = 0.0;
//@      VolCount          = 0;
//@    }
//@    
//@    public void store( )
//@    {
//@      try
//@      {
//@        #sql [Ctx]
//@        {
//@          insert into commissions
//@          (
//@            user_id, 
//@            manager_user_id, 
//@            commission_amount, 
//@            commission_date, 
//@            commission_type, 
//@            transaction_type,
//@            expense, 
//@            income, 
//@            merchant_number, 
//@            pay_rate, 
//@            vol_amount, 
//@            vol_count,
//@            description
//@          )
//@          values
//@          (
//@            :UserId,
//@            :ManagerId,
//@            :Amount,
//@            :CommissionDate,
//@            :CommissionType,
//@            'I',
//@            :Expense,
//@            :Income,
//@            :MerchantId,
//@            :PayRate,
//@            :VolAmount,
//@            :VolCount,
//@            substr(:Description,1,96)
//@          )
//@        };
//@      }
//@      catch( java.sql.SQLException e )
//@      {
//@        logEntry("store()",e.toString());
//@      }
//@    }
//@  }

  public class MerchantNumberValidation
    extends SQLJConnectionBase
    implements Validation
  {
    String          ErrorMessage      = null;
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }
    
    public boolean validate( String fieldData )
    {
      long        merchantId    = 0L;
      boolean     retVal        = true;
      int         rowCount      = 0;

      ErrorMessage = null;
      try
      {
        merchantId = Long.parseLong(fieldData);
        
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:259^9*/

//  ************************************************************
//  #sql [Ctx] { select count(merchant_number) 
//            from   mif      mf
//            where  mf.bank_number = :mesConstants.BANK_ID_MES and 
//                   mf.merchant_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(merchant_number)  \n          from   mif      mf\n          where  mf.bank_number =  :1  and \n                 mf.merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.SalesCommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.BANK_ID_MES);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:265^9*/
        if ( rowCount == 0 )
        {
          ErrorMessage = "Merchant number does not exist";
        }
      }
      catch( NumberFormatException nfe )
      {
        ErrorMessage = "Merchant number must be a number";
      }
      catch( Exception e )
      {
        logEntry( "validate()", e.toString() );
        ErrorMessage = "Merchant Number validation failed: " + e.toString();
      }
      finally
      {
        cleanUp();
      }
      return( ErrorMessage == null );
    }
  }

  protected class CommissionTypeTable extends DropDownTable
  {
    public CommissionTypeTable()
      throws java.sql.SQLException
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;

      try
      {      
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:299^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ct.commission_type,
//                    ct.commission_desc
//            from    commission_type ct
//            where   ct.commission_type > 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ct.commission_type,\n                  ct.commission_desc\n          from    commission_type ct\n          where   ct.commission_type > 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:305^9*/
        resultSet = it.getResultSet();

        addElement("","select one");
        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }
  
  protected class SalesUserTable extends DropDownTable
  {
    public SalesUserTable()
      throws java.sql.SQLException
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;

      try
      {      
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:335^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    u.user_id                             as user_id,
//                      (u.NAME || ' [' || sr.rep_id || ']')  as rep_name
//            from      sales_rep sr,
//                      users     u
//            where     u.user_id = sr.user_id                  
//            order by  u.name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    u.user_id                             as user_id,\n                    (u.NAME || ' [' || sr.rep_id || ']')  as rep_name\n          from      sales_rep sr,\n                    users     u\n          where     u.user_id = sr.user_id                  \n          order by  u.name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:343^9*/
        resultSet = it.getResultSet();

        addElement("","select one");
        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }
  
  public class PaymentSummaryData
  {
    public  long                  DdaNumber               = 0L;
    public  double                CommissionAmount        = 0.0;
    public  Date                  HireDate                = null;
    public  String                HoldPayments            = "N";
    public  double                HeldAmount              = 0.0;
    public  double                PaidAmount              = 0.0;
    public  int                   PlanId                  = 0;
    public  String                RepId                   = null;
    public  long                  RepMerchantId           = 0L;
    public  String                RepName                 = null;
    public  Date                  TerminateDate           = null;
    public  int                   TransitRouting          = 0;
    public  long                  UserId                  = 0L;
    
    public PaymentSummaryData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      DdaNumber         = resultSet.getLong("dda_num");
      CommissionAmount  = resultSet.getDouble("comm_amount");
      HireDate          = resultSet.getDate("hire_date");
      HeldAmount        = resultSet.getDouble("held_amount");
      HoldPayments      = resultSet.getString("hold_payments");
      PaidAmount        = resultSet.getDouble("paid_amount");
      PlanId            = resultSet.getInt("plan_id");
      RepId             = resultSet.getString("rep_id");
      RepMerchantId     = resultSet.getLong("rep_merchant_number");
      RepName           = resultSet.getString("rep_name");
      TerminateDate     = resultSet.getDate("terminate_date");
      TransitRouting    = resultSet.getInt("trans_routing");
      UserId            = resultSet.getLong("user_id");
    }
    
    public boolean holdPayments( )
    {
      return( HoldPayments.equals("Y") );
    }
  }
  
  public class RetentionSummaryData
  {
    public  Date              ActiveDate              = null;
    public  double            AuthIncome              = 0.0;
    public  double            MiscIncome              = 0.0;
    public  double            NdrExpense              = 0.0;
    public  double            NdrIncome               = 0.0;
    public  String            RepId                   = null;
    public  long              RepMerchantId           = 0L;
    public  String            RepName                 = null;
    public  double[]          RetentionAuthIncome     = new double[2];
    public  double[]          RetentionMiscIncome     = new double[2];
    public  double[]          RetentionNdrExpense     = new double[2];
    public  double[]          RetentionNdrIncome      = new double[2];
    public  Date              TerminateDate           = null;
    public  long              UserId                  = 0L;
    
    public RetentionSummaryData( long userId, Date activeDate )
    {
      UserId      = userId;
      ActiveDate  = activeDate;
    }
    
    public void addRevenueAuth( double inc )
    {
      AuthIncome = inc;
    }
    
    public void addRevenueMisc( double inc )
    {
      MiscIncome = inc;
    }
    
    public void addRevenueNdr( double inc, double exp )
    {
      NdrIncome   = inc;
      NdrExpense  = exp;
    }
    
    public void addRetentionAuth( double[] inc )
    {
      RetentionAuthIncome[0] = inc[0];
      RetentionAuthIncome[1] = inc[1];
    }
    
    public void addRetentionMisc( double[] inc )
    {
      RetentionMiscIncome[0] = inc[0];
      RetentionMiscIncome[1] = inc[1];
    }
    
    public void addRetentionNdr( double[] inc, double[] exp )
    {
      RetentionNdrIncome[0]   = inc[0];
      RetentionNdrIncome[1]   = inc[1];
      RetentionNdrExpense[0]  = exp[0];
      RetentionNdrExpense[1]  = exp[1];
    }
    
    public boolean hasReqRevenue()
    {
      return(((NdrIncome + AuthIncome + MiscIncome) - NdrExpense) > 5000);
    }
    
    public boolean hasReqRetention()
    {
      double[]      net     = new double[2];
      boolean       retVal  = false;
      
      for( int i = 0; i < 2; ++i )
      {
        net[i] = ( (RetentionNdrIncome[i] + 
                    RetentionAuthIncome[i] + 
                    RetentionMiscIncome[i]) -
                   RetentionNdrExpense[i]
                 );
      }
              
      // if the current net is greater than
      // the previous net or it is at 
      // least 85% of the current then they qualify
      if ( (net[1] >= net[0]) || 
           ((net[0] != 0.0) && (net[1]/net[0] >= 0.850)) )
      {             
        retVal = true;
      }
      return( retVal );
    }
  }
  
      
  

  public class RepDetailData
  {
    public    Date                ActivationDate      = null;
    public    double              Advance             = 0.0;
    public    double              CommissionAmount    = 0.0;
    public    Date                CommissionDate      = null;
    public    int                 CommissionType      = mesConstants.COMM_TYPE_NONE;
    public    String              Description         = null;
    public    String              DbaName             = null;
    public    double              Expense             = 0.0;
    public    double              Income              = 0.0;
    public    long                MerchantId          = 0L;
    public    double              PayRate             = 0.0;
    public    long                RecId               = 0L;
    public    String              RepId               = null;
    public    String              TranType            = null;
    public    long                UserId              = 0L;
    public    double              VolAmount           = 0.0;
    public    int                 VolCount            = 0;
    
    private   DefaultContext      myCtx               = null;
    
    public RepDetailData(DefaultContext myCtx, ResultSet       resultSet )
      throws java.sql.SQLException
    {
      ActivationDate      = resultSet.getDate("activation_date");
      Advance             = resultSet.getDouble("advance");
      CommissionAmount    = resultSet.getDouble("amount");
      CommissionDate      = resultSet.getDate("comm_date");
      CommissionType      = resultSet.getInt("comm_type");
      DbaName             = resultSet.getString("dba_name");
      Description         = resultSet.getString("comm_desc");
      Expense             = resultSet.getDouble("expense");
      Income              = resultSet.getDouble("income");
      MerchantId          = resultSet.getLong("merchant_number");
      RecId               = resultSet.getLong("rec_id");
      RepId               = resultSet.getString("rep_id");
      PayRate             = resultSet.getDouble("pay_rate");
      TranType            = resultSet.getString("tran_type");
      UserId              = resultSet.getLong("user_id");
      VolAmount           = resultSet.getDouble("vol_amount");
      VolCount            = resultSet.getInt("vol_count");
      
      this.myCtx = myCtx;
    }
    
    public double getNetIncome( )
    {
      return( Income - Expense );
    }
    
    public String getTranDesc( )
    {
      String      retVal  = "Error";
      
      switch( TranType.charAt(0) )
      {
        case 'P':
          retVal = "Payment";
          break;
          
        case 'I':
          retVal = "Incoming";
          break;
          
        case 'H':
          retVal = "Held";
          
        default:
          retVal = TranType;
          break;
      }
        
      return(retVal);
    }
    
    public boolean isHeld( )
    {
      return( TranType.equals("H") );
    }
    
    public void loadFields( FieldGroup fields )
    {
      fields.getField("saveId").setData( Long.toString(RecId) );
      fields.getField("income").setData( Double.toString(Income) );
      fields.getField("expense").setData( Double.toString(Expense) );
      fields.getField("advance").setData( Double.toString(Advance) );
      fields.getField("payRate").setData( Double.toString(PayRate*100) );
    }
    
    public void storeFields( FieldGroup fields )
    {
      double        amount;
      
      Income  = fields.getField("income").asDouble();
      Expense = fields.getField("expense").asDouble();
      Advance = fields.getField("advance").asDouble();
      PayRate = fields.getField("payRate").asDouble();
      PayRate *= 0.01;
      
      // update the commission amount
      amount  = ((Income - Expense) * PayRate);
//@      if ( amount < 0.0 )
//@      {
//@        amount = 0.0;
//@      }
      amount -= Advance;
      
      try
      {
        // perform the rounding in SQL to be consistent
        // with the trigger.
        /*@lineinfo:generated-code*//*@lineinfo:607^9*/

//  ************************************************************
//  #sql [myCtx] { select  round(:amount,2) 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = myCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  round( :1 ,2)  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.SalesCommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,amount);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   CommissionAmount = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:611^9*/
      }
      catch( java.sql.SQLException e )
      {
        // fall back to Java rounding
        CommissionAmount = MesMath.round(amount,2);
      }
      
      store();
    }
    
    public void store( )
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:626^9*/

//  ************************************************************
//  #sql [myCtx] { update  commissions
//            set     income            = :Income,
//                    expense           = :Expense,
//                    advance           = :Advance,
//                    pay_rate          = :PayRate,
//                    commission_amount = :CommissionAmount
//            where   rec_id = :RecId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = myCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  commissions\n          set     income            =  :1 ,\n                  expense           =  :2 ,\n                  advance           =  :3 ,\n                  pay_rate          =  :4 ,\n                  commission_amount =  :5 \n          where   rec_id =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,Income);
   __sJT_st.setDouble(2,Expense);
   __sJT_st.setDouble(3,Advance);
   __sJT_st.setDouble(4,PayRate);
   __sJT_st.setDouble(5,CommissionAmount);
   __sJT_st.setLong(6,RecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:635^9*/
      }
      catch( java.sql.SQLException e )
      {
        logEntry("store()",e.toString());
      }
    }
  }
  
  public class RepPaymentData
  {
    public RepPaymentData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
    }
  }

  public class RepSummaryData 
    implements Comparable
  {
    public  double            CommissionAmount  = 0.0;
    public  String            CommissionDesc    = null;
    public  int               CommissionType    = mesConstants.COMM_TYPE_NONE;
    public  double            HeldAmount        = 0.0;
    public  double            PaymentAmount     = 0.0;
    public  int               PaymentGroup      = -1;
    
    public RepSummaryData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      CommissionAmount  = resultSet.getDouble("comm_amount");
      HeldAmount        = resultSet.getDouble("held_amount");
      CommissionType    = resultSet.getInt("comm_type");
      CommissionDesc    = resultSet.getString("comm_desc");
//@      PaymentAmount     = resultSet.getDouble("paid_amount");
      PaymentGroup      = getPaymentGroup(CommissionType);
    }
    
    public int compareTo( Object obj )
    {
      RepSummaryData      compareObj    = (RepSummaryData)obj;
      int                 retVal        = 0;
      
      if ( (retVal = (PaymentGroup - compareObj.PaymentGroup)) == 0 )
      {
        if ( (retVal = CommissionDesc.compareTo(compareObj.CommissionDesc)) == 0 )
        {
          retVal = (CommissionType - compareObj.CommissionType);
        }
      }
      return( retVal );
    }
  }
  
  public class NDRAdjustmentDetail
  {
    public Date           ActiveDate          = null;
    public double         DiscountIncome      = 0.0;
    public double         InterchangeIncome   = 0.0;
    public double         InterchangeExpense  = 0.0;
    public long           MerchantId          = 0L;
    public double         ReferralExpense     = 0.0;
    public double         VolAmount           = 0.0;
    public int            VolCount            = 0;
    
    public NDRAdjustmentDetail( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ActiveDate          = resultSet.getDate("active_date");
      DiscountIncome      = resultSet.getDouble("inc_disc");
      InterchangeIncome   = resultSet.getDouble("inc_ic");
      InterchangeExpense  = resultSet.getDouble("exp_ic");
      MerchantId          = resultSet.getLong("merchant_number");
      VolAmount           = resultSet.getDouble("vol_amount");
      VolCount            = resultSet.getInt("vol_count");
      
      loadReferralExpense(this);
    }
  }
  
  public class ManagerSummaryData 
  {
    public  double                CommissionAmount        = 0.0;
    public  double                HeldAmount              = 0.0;
    public  Date                  HireDate                = null;
    public  String                HoldPayments            = "N";
    public  double                PaidAmount              = 0.0;
    public  int                   PlanId                  = -1;
    public  String                RepId                   = null;
    public  String                RepName                 = null;
    public  Date                  TerminateDate           = null;
    public  long                  UserId                  = 0L;
    
    
    public ManagerSummaryData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CommissionAmount  = resultSet.getDouble("comm_amount");
      HeldAmount        = resultSet.getDouble("held_amount");
      HireDate          = resultSet.getDate("hire_date");
      HoldPayments      = resultSet.getString("hold_payments");
      PaidAmount        = resultSet.getDouble("paid_amount");
      PlanId            = resultSet.getInt("plan_id");
      RepId             = resultSet.getString("rep_id");
      RepName           = resultSet.getString("rep_name");
      TerminateDate     = resultSet.getDate("terminate_date");
      UserId            = resultSet.getLong("user_id");
    }
    
    public boolean holdPayments( )
    {
      return( HoldPayments.equals("Y") );
    }
  }
  
  public class SalesRepEntry
  {
    public        String      RepId       = null;
    public        String      RepName     = null;
    public        long        UserId      = 0L;
    
    public SalesRepEntry( ResultSet resultSet )
      throws java.sql.SQLException
    {
      RepId     = resultSet.getString("rep_id");
      RepName   = resultSet.getString("rep_name");
      UserId    = resultSet.getLong("user_id");
    }

    public String getDisplayName( )
    {
      StringBuffer      retVal = new StringBuffer("");

      if ( RepName != null )
      {
        retVal.append(RepName);
        retVal.append("  ");
      }
      retVal.append("[");
      retVal.append(RepId);
      retVal.append("]");
      return( retVal.toString() );
    }
  }
  
  private FieldGroup    AddFields       = null;
  private boolean       AddRecord       = false;
  private int           CommissionType  = 0;
  private long          DeleteRecId     = 0L;
  private FieldGroup    EditFields      = null;
  private long          EditRecId       = 0L;
  private long          ReportOrgId     = 0L;

  public SalesCommissionDataBean( )
  {
  }
  
  public void deleteRecord( )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:796^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    commissions
//          where   rec_id = :DeleteRecId 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    commissions\n        where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,DeleteRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:801^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("deleteRecord()",e.toString());
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
//@    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
  }
  
  public void encodeOrgUrl( StringBuffer buffer, long childOrgId, Date beginDate, Date endDate )
  {
    buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
    buffer.append("com.mes.OrgId=");
    buffer.append(ReportOrgId);
    
    encodeReportDate(buffer,beginDate,endDate);
  }
  
  public String getCommissionDesc( )
  {
    return( getCommissionDesc(CommissionType) );
  }
  
  public String getCommissionDesc( int commType )
  {
    String        retVal        = "N/A (" + commType + ")";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:843^7*/

//  ************************************************************
//  #sql [Ctx] { select  commission_desc 
//          from    commission_type 
//          where   commission_type = :commType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  commission_desc  \n        from    commission_type \n        where   commission_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.SalesCommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,commType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:848^7*/
    }
    catch(java.sql.SQLException e)
    {
    }
    return( retVal );
  }
  
  public long getDeleteId( )
  {
    return( DeleteRecId );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    SalesRepRecord          repRec    = new SalesRepRecord(Ctx);
    
    repRec.getData( ReportOrgId );
    
    filename.append( repRec.getRepId() );
    filename.append("_summary_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public Vector getFormErrors()
  {
    return( AddFields.getErrors() );
  }
  
  public double getIncomingAmount( int commType, long userId, long merchantId )
  {
    double      retVal        = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:892^7*/

//  ************************************************************
//  #sql { select sum(c.commission_amount)  
//          from    commissions     c
//          where   c.user_id = :userId and
//                  c.merchant_number = :merchantId and
//                  c.commission_type = :commType and
//                  c.transaction_type = 'I'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = sqlj.runtime.ref.DefaultContext.getDefaultContext(); if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum(c.commission_amount)   \n        from    commissions     c\n        where   c.user_id =  :1  and\n                c.merchant_number =  :2  and\n                c.commission_type =  :3  and\n                c.transaction_type = 'I'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.SalesCommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setInt(3,commType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:900^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
    }
    return( retVal );
  }
  
  public double getAdvanceAmount( long merchantId )
  {
    return( getAdvanceAmount( CommissionType, ReportOrgId, merchantId ) );
  }
  
  public double getAdvanceAmount( int commType, long userId, long merchantId )
  {
    double              advanceAmount   = 0.0;
    int[]               commTypes       = null;
    
    switch( commType )
    {
      case mesConstants.COMM_TYPE_VMC_DISC_TRUE_UP:
        commTypes = new int[] { mesConstants.COMM_TYPE_VMC_DISC_ADVANCED,
                                mesConstants.COMM_TYPE_VMC_DISC_ADV_MONTHLY };
        break;
        
      case mesConstants.COMM_TYPE_STATEMENT_FEES_TRUE_UP:
        commTypes = new int[] { mesConstants.COMM_TYPE_STATEMENT_FEES_ADVANCE };
        break;

      case mesConstants.COMM_TYPE_EQUIP_SERVICE_FEE_TRUE_UP:
        commTypes = new int[] { mesConstants.COMM_TYPE_EQUIP_SERVICE_FEE_TRUE_UP };
        break;
    }
    
    if( commTypes != null )
    {
      advanceAmount = getAdvanceAmount(commTypes, userId, merchantId );
    }
    
    return( advanceAmount );
  }
  
  protected double getAdvanceAmount( int[] commTypes, long userId, long merchantId )
  {
    double    retVal          = 0.0;
    
    for( int i = 0; i < commTypes.length; ++i )
    {
      retVal += getIncomingAmount( commTypes[i], userId, merchantId );
    }
    return( retVal );
  }
  
  public int getPaymentGroup( int commType )
  {
    int     retVal       = -1;
    
    for ( int groupId = 0; groupId < PAYMENT_GROUPS.length; ++groupId )
    {
      for ( int i = 0; i < PAYMENT_GROUPS[groupId].length; ++i )
      {
        if ( PAYMENT_GROUPS[groupId][i] == commType ) 
        {
          retVal = groupId;
          break;
        }
      }
    }
    return( retVal );
  }
  
  public String getPaymentGroupName( int pgroup )
  {
    return( PAYMENT_GROUP_NAMES[pgroup] );
  }
  
  public String getPlanDesc( int planId )
  {
    String      planDesc      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:985^7*/

//  ************************************************************
//  #sql [Ctx] { select  plan_desc 
//          from    commission_plans
//          where   plan_id = :planId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  plan_desc  \n        from    commission_plans\n        where   plan_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.SalesCommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,planId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   planDesc = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:990^7*/
    }
    catch( java.sql.SQLException e )
    {
      // description does not exist
      planDesc = "N/A (" + planId + ")";
    }
    return( planDesc );
  }
  
  public long getReferralNode( long merchantId )
  {
    long        nodeId      = merchantId;
    long        retVal      = 0L;
    int         rowCount    = 0;
    
    try
    {
      while( nodeId != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1010^9*/

//  ************************************************************
//  #sql [Ctx] { select  count( abc.hierarchy_node ) 
//            from    agent_bank_contract     abc
//            where   abc.hierarchy_node = :nodeId and
//                    abc.contract_type = :ContractTypes.CONTRACT_SOURCE_REFERRAL --2
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( abc.hierarchy_node )  \n          from    agent_bank_contract     abc\n          where   abc.hierarchy_node =  :1  and\n                  abc.contract_type =  :2  --2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.SalesCommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1016^9*/
        
        if ( rowCount > 0 )
        {
          // found a match, break from the loop
          retVal = nodeId;
          break;
        }
        
        // no match, get the parent node id
        nodeId = getParentNode(nodeId);
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( e.toString(), "getReferralNode()" );
    }
    return( retVal );
  }
  
  public boolean hasAdvanceData( )
  {
    return( hasAdvanceData( CommissionType ) );
  }
  
  public boolean hasAdvanceData( int commType )
  {
    boolean       retVal        = false;
    
    switch( commType )
    {
      case mesConstants.COMM_TYPE_VMC_DISC_TRUE_UP:
      case mesConstants.COMM_TYPE_STATEMENT_FEES_TRUE_UP:
      case mesConstants.COMM_TYPE_EQUIP_SERVICE_FEE_TRUE_UP:
        retVal = true;
        break;
    }
    
    return(retVal);
  }
  
  public boolean hasVolumeData()
  {
    return( hasVolumeData( CommissionType ) );
  }
  
  public boolean hasVolumeData( int commType )
  {
    boolean       retVal        = false;
    
    switch( commType )
    {
      case mesConstants.COMM_TYPE_VMC_DISC_ADVANCED:
      case mesConstants.COMM_TYPE_VMC_DISC_MONTHLY:
      case mesConstants.COMM_TYPE_VMC_DISC_TRUE_UP:
      case mesConstants.COMM_TYPE_VMC_DISC_ADV_MONTHLY:
        retVal = true;
        break;
    }
    
    return(retVal);
  }
  
  public void initialize( )
  {
    Vector        allFields = null;
    Field         field     = null;
    
    super.initialize();
    
    try
    {
      // create the record addition fields
      AddFields  = new FieldGroup("addFields");
      AddFields.add(new DropDownField("userId",new SalesUserTable(),false));
      AddFields.add(new DropDownField("commType",new CommissionTypeTable(),false));
      AddFields.add( new NumberField("merchantId",16,16,false,0) );
      AddFields.getField("merchantId").addValidation( new MerchantNumberValidation() );
      AddFields.add( new DateField("commDate",false) );
      AddFields.getField("commDate").setData( DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy") );
      AddFields.add( new NumberField("volCount",6,6,true,0) );
      AddFields.add( new CurrencyField("volAmount",10,10,true) );
      AddFields.add( new CurrencyField("income",10,10,false) );
      AddFields.add( new CurrencyField("expense",10,10,false) );
      AddFields.add( new CurrencyField("advance",10,10,true) );
      AddFields.add( new NumberField("payRate",6,3,false,3) );
      AddFields.add( new Field("desc",96,25,true) );
      fields.add( AddFields );
      
      // set the extra html data for each field
      allFields = AddFields.getFieldsVector();
    
      for( int i = 0; i < allFields.size(); ++i )
      {
        field = (Field)allFields.elementAt(i);
        field.addHtmlExtra("class=\"formFields\"");
      }
    
      // add the fields
      EditFields = new FieldGroup("editFields");
      EditFields.add( new HiddenField("saveId") );
      EditFields.add( new CurrencyField("income",10,10,false) );
      EditFields.add( new CurrencyField("expense",10,10,false) );
      EditFields.add( new CurrencyField("advance",10,10,true) );
      EditFields.add( new NumberField("payRate",6,3,false,3) );
      fields.add( EditFields );
    
      // set the extra html data for each field
      allFields = EditFields.getFieldsVector();
    
      for( int i = 0; i < allFields.size(); ++i )
      {
        field = (Field)allFields.elementAt(i);
        field.addHtmlExtra("class=\"formFields\"");
      }
    }
    catch( Exception e )
    {
      logEntry( "initialize()",e.toString() );
    }      
  }
  
  public boolean isBonusCommission( int commType )
  {
    boolean       retVal      = false;
    
    switch( commType )
    {
      case mesConstants.COMM_TYPE_PERFORMANCE_BONUS:
        retVal = true;
        break;
        
      default:
        break;
    }          
    return( retVal );  
  }
  
  public boolean isEditMode()
  {
    return( (EditRecId != 0L) || (AddRecord == true) );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public RepDetailData loadCommissionEntry( long recId )
  {
    RepDetailData       entry     = null;
    ResultSetIterator   it        = null;
    ResultSet           resultSet = null;
    
    try 
    {
      /*@lineinfo:generated-code*//*@lineinfo:1179^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    c.commission_type             as comm_type,
//                      c.merchant_number             as merchant_number,
//                      c.commission_date             as comm_date,
//                      c.rec_id                      as rec_id,
//                      sr.rep_id                     as rep_id,
//                      mf.dba_name                   as dba_name,
//                      mf.activation_date            as activation_date,
//                      c.transaction_type            as tran_type,
//                      c.commission_amount           as amount,
//                      c.pay_rate                    as pay_rate,
//                      c.vol_count                   as vol_count,
//                      c.vol_amount                  as vol_amount,
//                      c.income                      as income,
//                      c.expense                     as expense,
//                      c.advance                     as advance,
//                      c.DESCRIPTION                 as comm_desc,
//                      c.user_id                     as user_id
//            from      commissions       c,
//                      mif               mf,
//                      sales_rep         sr
//            where     c.rec_id = :recId and
//                      mf.merchant_number(+) = c.merchant_number and
//                      sr.user_id(+) = c.user_id
//            order by mf.dba_name, c.merchant_number, c.commission_date, tran_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    c.commission_type             as comm_type,\n                    c.merchant_number             as merchant_number,\n                    c.commission_date             as comm_date,\n                    c.rec_id                      as rec_id,\n                    sr.rep_id                     as rep_id,\n                    mf.dba_name                   as dba_name,\n                    mf.activation_date            as activation_date,\n                    c.transaction_type            as tran_type,\n                    c.commission_amount           as amount,\n                    c.pay_rate                    as pay_rate,\n                    c.vol_count                   as vol_count,\n                    c.vol_amount                  as vol_amount,\n                    c.income                      as income,\n                    c.expense                     as expense,\n                    c.advance                     as advance,\n                    c.DESCRIPTION                 as comm_desc,\n                    c.user_id                     as user_id\n          from      commissions       c,\n                    mif               mf,\n                    sales_rep         sr\n          where     c.rec_id =  :1  and\n                    mf.merchant_number(+) = c.merchant_number and\n                    sr.user_id(+) = c.user_id\n          order by mf.dba_name, c.merchant_number, c.commission_date, tran_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1205^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        entry = new RepDetailData( Ctx, resultSet );
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadCommissionEntry",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( entry );
  }
  
  public void loadData( )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( ReportType == RT_MANAGER_SUMMARY )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1235^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    sr.rep_id                             as rep_id,
//                      sr.user_id                            as user_id,
//                      u.name                                as rep_name,
//                      sr.rep_plan_id                        as plan_id,  
//                      sr.REP_HIRE_DATE                      as hire_date,
//                      sr.rep_terminate_date                 as terminate_date,
//                      nvl(sr.hold_payments,'N')             as hold_payments,
//                      sum( decode( c.transaction_type,
//                                   'I', c.commission_amount,
//                                   0 ) )                    as comm_amount,
//                      sum( decode( c.transaction_type,
//                                   'H',c.commission_amount,
//                                    0 ) )                   as held_amount,
//                      sum( decode( c.transaction_type,
//                                   'P', c.commission_amount,
//                                   0 ) )                    as paid_amount
//            from      t_hierarchy       th,
//                      users             u,
//                      sales_rep         sr,
//                      commissions       c
//            where     th.ancestor   = :ReportOrgId and
//                      th.hier_type  = :MesHierarchy.HT_MES_SALES and --3 and
//                      u.user_id     = th.descendent and
//                      sr.user_id    = u.user_id and
//                      c.user_id     = sr.user_id and
//                      c.commission_date between :ReportDateBegin and :ReportDateEnd
//            group by  sr.rep_id, sr.user_id, u.name, 
//                      sr.rep_plan_id, sr.rep_hire_date, sr.rep_terminate_date,
//                      nvl(sr.hold_payments,'N')
//            order by  sr.rep_plan_id, u.name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sr.rep_id                             as rep_id,\n                    sr.user_id                            as user_id,\n                    u.name                                as rep_name,\n                    sr.rep_plan_id                        as plan_id,  \n                    sr.REP_HIRE_DATE                      as hire_date,\n                    sr.rep_terminate_date                 as terminate_date,\n                    nvl(sr.hold_payments,'N')             as hold_payments,\n                    sum( decode( c.transaction_type,\n                                 'I', c.commission_amount,\n                                 0 ) )                    as comm_amount,\n                    sum( decode( c.transaction_type,\n                                 'H',c.commission_amount,\n                                  0 ) )                   as held_amount,\n                    sum( decode( c.transaction_type,\n                                 'P', c.commission_amount,\n                                 0 ) )                    as paid_amount\n          from      t_hierarchy       th,\n                    users             u,\n                    sales_rep         sr,\n                    commissions       c\n          where     th.ancestor   =  :1  and\n                    th.hier_type  =  :2  and --3 and\n                    u.user_id     = th.descendent and\n                    sr.user_id    = u.user_id and\n                    c.user_id     = sr.user_id and\n                    c.commission_date between  :3  and  :4 \n          group by  sr.rep_id, sr.user_id, u.name, \n                    sr.rep_plan_id, sr.rep_hire_date, sr.rep_terminate_date,\n                    nvl(sr.hold_payments,'N')\n          order by  sr.rep_plan_id, u.name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ReportOrgId);
   __sJT_st.setInt(2,MesHierarchy.HT_MES_SALES);
   __sJT_st.setDate(3,ReportDateBegin);
   __sJT_st.setDate(4,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1267^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          ReportRows.addElement( new ManagerSummaryData( resultSet ) );
        }
        resultSet.close();
        it.close();
      }
      else if ( ReportType == RT_PAYMENT_SUMMARY )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1279^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    upper(sr.rep_id)                      as rep_id,
//                      sr.user_id                            as user_id,
//                      u.name                                as rep_name,
//                      sr.rep_merchant_number                as rep_merchant_number,
//                      sr.rep_plan_id                        as plan_id,
//                      sr.REP_HIRE_DATE                      as hire_date,
//                      sr.rep_terminate_date                 as terminate_date,
//                      nvl(sr.hold_payments,'N')             as hold_payments,
//                      mf.dda_num                            as dda_num,
//                      mf.TRANSIT_ROUTNG_NUM                 as trans_routing,
//                      sum( decode( c.transaction_type,
//                                   'P', 0,
//                                   c.commission_amount ) )  as comm_amount,
//                      sum( decode( c.transaction_type,
//                                   'H',c.commission_amount,
//                                    0 ) )                   as held_amount,
//                      sum( decode( c.transaction_type,
//                                   'P', c.commission_amount,
//                                   0 ) )                    as paid_amount
//            from      commissions       c,
//                      sales_rep         sr,
//                      users             u,
//                      mif               mf
//            where     ( ( c.commission_date between :ReportDateBegin and :ReportDateEnd and
//                          c.transaction_type in ('I','P') ) or
//                        ( c.commission_date <= :ReportDateEnd and c.transaction_type = 'H' )  ) and
//                      sr.user_id      = c.user_id and
//                      u.user_id       = sr.user_id and
//                      mf.merchant_number(+) = sr.rep_merchant_number
//            group by  sr.rep_id, sr.user_id, u.name, sr.rep_merchant_Number,
//                      sr.rep_plan_id, sr.rep_hire_date, sr.rep_terminate_date,
//                      nvl(sr.hold_payments,'N'),
//                      mf.dda_Num, mf.transit_routng_num
//            order by  sr.rep_plan_id, u.name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    upper(sr.rep_id)                      as rep_id,\n                    sr.user_id                            as user_id,\n                    u.name                                as rep_name,\n                    sr.rep_merchant_number                as rep_merchant_number,\n                    sr.rep_plan_id                        as plan_id,\n                    sr.REP_HIRE_DATE                      as hire_date,\n                    sr.rep_terminate_date                 as terminate_date,\n                    nvl(sr.hold_payments,'N')             as hold_payments,\n                    mf.dda_num                            as dda_num,\n                    mf.TRANSIT_ROUTNG_NUM                 as trans_routing,\n                    sum( decode( c.transaction_type,\n                                 'P', 0,\n                                 c.commission_amount ) )  as comm_amount,\n                    sum( decode( c.transaction_type,\n                                 'H',c.commission_amount,\n                                  0 ) )                   as held_amount,\n                    sum( decode( c.transaction_type,\n                                 'P', c.commission_amount,\n                                 0 ) )                    as paid_amount\n          from      commissions       c,\n                    sales_rep         sr,\n                    users             u,\n                    mif               mf\n          where     ( ( c.commission_date between  :1  and  :2  and\n                        c.transaction_type in ('I','P') ) or\n                      ( c.commission_date <=  :3  and c.transaction_type = 'H' )  ) and\n                    sr.user_id      = c.user_id and\n                    u.user_id       = sr.user_id and\n                    mf.merchant_number(+) = sr.rep_merchant_number\n          group by  sr.rep_id, sr.user_id, u.name, sr.rep_merchant_Number,\n                    sr.rep_plan_id, sr.rep_hire_date, sr.rep_terminate_date,\n                    nvl(sr.hold_payments,'N'),\n                    mf.dda_Num, mf.transit_routng_num\n          order by  sr.rep_plan_id, u.name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ReportDateBegin);
   __sJT_st.setDate(2,ReportDateEnd);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1315^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          ReportRows.addElement( new PaymentSummaryData( resultSet ) );
        }
        resultSet.close();
        it.close();
      }
      else if ( ReportType == RT_REP_SUMMARY )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1327^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    c.commission_type             as comm_type,
//                      nvl(ct.commission_desc,
//                          'N/A (' || c.commission_type || ')' ) 
//                                                    as comm_desc,
//                      sum( c.commission_amount )    as comm_amount,                                                  
//                      sum( decode( c.transaction_type,
//                                  'H', c.commission_amount,
//                                  0 ) )             as held_amount
//    --                  get_comm_paid_amount(c.merchant_number,c.commission_type) as paid_amount
//            from      commissions     c,
//                      commission_type ct
//            where     ( :ReportOrgId = 0 or
//                        c.user_id = :ReportOrgId ) and
//                      ( ( c.commission_date between :ReportDateBegin and :ReportDateEnd and
//                          c.transaction_type = 'I' ) or
//                        ( c.commission_date <= :ReportDateEnd and c.transaction_type = 'H' )  ) and
//                      ct.commission_type(+) = c.commission_type
//            group by  c.commission_type, 
//                      nvl(ct.commission_desc,
//                          'N/A (' || c.commission_type || ')' ) 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    c.commission_type             as comm_type,\n                    nvl(ct.commission_desc,\n                        'N/A (' || c.commission_type || ')' ) \n                                                  as comm_desc,\n                    sum( c.commission_amount )    as comm_amount,                                                  \n                    sum( decode( c.transaction_type,\n                                'H', c.commission_amount,\n                                0 ) )             as held_amount\n  --                  get_comm_paid_amount(c.merchant_number,c.commission_type) as paid_amount\n          from      commissions     c,\n                    commission_type ct\n          where     (  :1  = 0 or\n                      c.user_id =  :2  ) and\n                    ( ( c.commission_date between  :3  and  :4  and\n                        c.transaction_type = 'I' ) or\n                      ( c.commission_date <=  :5  and c.transaction_type = 'H' )  ) and\n                    ct.commission_type(+) = c.commission_type\n          group by  c.commission_type, \n                    nvl(ct.commission_desc,\n                        'N/A (' || c.commission_type || ')' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ReportOrgId);
   __sJT_st.setLong(2,ReportOrgId);
   __sJT_st.setDate(3,ReportDateBegin);
   __sJT_st.setDate(4,ReportDateEnd);
   __sJT_st.setDate(5,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1349^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          ReportRows.addElement( new RepSummaryData( resultSet ) );
        }
        resultSet.close();
        it.close();
      }        
      else if ( ReportType == RT_REP_DETAILS )
      {
        RepDetailData       record      = null;
        
        /*@lineinfo:generated-code*//*@lineinfo:1363^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    c.commission_type             as comm_type,
//                      c.merchant_number             as merchant_number,
//                      c.commission_date             as comm_date,
//                      c.rec_id                      as rec_id,
//                      sr.rep_id                     as rep_id,
//                      mf.dba_name                   as dba_name,
//                      mf.activation_date            as activation_date,
//                      c.transaction_type            as tran_type,
//                      c.commission_amount           as amount,
//                      c.pay_rate                    as pay_rate,
//                      c.vol_count                   as vol_count,
//                      c.vol_amount                  as vol_amount,
//                      c.income                      as income,
//                      c.expense                     as expense,
//                      c.advance                     as advance,
//                      c.DESCRIPTION                 as comm_desc,
//                      c.user_id                     as user_id,
//                      c.transaction_type            as tran_type
//            from      commissions       c,
//                      mif               mf,
//                      sales_rep         sr,
//                      users             u
//            where     ( :ReportOrgId = 0 or
//                        c.user_id = :ReportOrgId ) and
//                      ( ( c.commission_date between :ReportDateBegin and :ReportDateEnd and
//                          c.transaction_type = 'I' ) or
//                        ( c.commission_date <= :ReportDateEnd and c.transaction_type = 'H' )  ) and
//                      c.commission_type = :CommissionType and
//                      mf.merchant_number(+) = c.merchant_number and
//                      sr.user_id(+) = c.user_id and
//                      u.user_id(+) = c.user_id
//            order by mf.dba_name, c.merchant_number, c.commission_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    c.commission_type             as comm_type,\n                    c.merchant_number             as merchant_number,\n                    c.commission_date             as comm_date,\n                    c.rec_id                      as rec_id,\n                    sr.rep_id                     as rep_id,\n                    mf.dba_name                   as dba_name,\n                    mf.activation_date            as activation_date,\n                    c.transaction_type            as tran_type,\n                    c.commission_amount           as amount,\n                    c.pay_rate                    as pay_rate,\n                    c.vol_count                   as vol_count,\n                    c.vol_amount                  as vol_amount,\n                    c.income                      as income,\n                    c.expense                     as expense,\n                    c.advance                     as advance,\n                    c.DESCRIPTION                 as comm_desc,\n                    c.user_id                     as user_id,\n                    c.transaction_type            as tran_type\n          from      commissions       c,\n                    mif               mf,\n                    sales_rep         sr,\n                    users             u\n          where     (  :1  = 0 or\n                      c.user_id =  :2  ) and\n                    ( ( c.commission_date between  :3  and  :4  and\n                        c.transaction_type = 'I' ) or\n                      ( c.commission_date <=  :5  and c.transaction_type = 'H' )  ) and\n                    c.commission_type =  :6  and\n                    mf.merchant_number(+) = c.merchant_number and\n                    sr.user_id(+) = c.user_id and\n                    u.user_id(+) = c.user_id\n          order by mf.dba_name, c.merchant_number, c.commission_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ReportOrgId);
   __sJT_st.setLong(2,ReportOrgId);
   __sJT_st.setDate(3,ReportDateBegin);
   __sJT_st.setDate(4,ReportDateEnd);
   __sJT_st.setDate(5,ReportDateEnd);
   __sJT_st.setInt(6,CommissionType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1397^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          record = new RepDetailData( Ctx, resultSet );
          
          // if the current records is the one to be
          // editted, then load the edit fields will
          // the data from this record.
          if ( record.RecId == EditRecId )
          {
            record.loadFields(EditFields);  
          }
          
          // if this is the records that is to be stored
          // then set the fields and update the database.
          if ( record.RecId == EditFields.getField("saveId").asLong() )
          {
            record.storeFields(EditFields);
          }
          
          ReportRows.addElement( record );
        }
        resultSet.close();
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public RepDetailData loadDeleteEntry( )
  {
    return( loadCommissionEntry( DeleteRecId ) );
  }
  
  public Vector loadNDRAdjustmentDetails( long recId )
  {
    Date                activeDate  = null;
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    Vector              retVal      = new Vector();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1449^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    sm.active_date                as active_date,
//                    sm.merchant_number            as merchant_number,
//                    sm.discount_inc_calc          as inc_disc,
//                    sm.tot_inc_interchange        as inc_ic,
//                    ( sm.interchange_expense  +
//                      sm.vmc_assessment_expense ) as exp_ic,
//                    sm.vmc_sales_amount           as vol_amount,
//                    sm.vmc_sales_count            as vol_count
//          from      commissions                 c,
//                    mif                         mf,
//                    monthly_extract_summary     sm
//          where     c.rec_id = :recId and
//                    mf.merchant_number = c.merchant_number and
//                    sm.merchant_number = mf.merchant_number and
//                    months_between((last_day(sm.active_date)+1),mf.activation_date) between 0 and 13
//          order by  sm.active_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sm.active_date                as active_date,\n                  sm.merchant_number            as merchant_number,\n                  sm.discount_inc_calc          as inc_disc,\n                  sm.tot_inc_interchange        as inc_ic,\n                  ( sm.interchange_expense  +\n                    sm.vmc_assessment_expense ) as exp_ic,\n                  sm.vmc_sales_amount           as vol_amount,\n                  sm.vmc_sales_count            as vol_count\n        from      commissions                 c,\n                  mif                         mf,\n                  monthly_extract_summary     sm\n        where     c.rec_id =  :1  and\n                  mf.merchant_number = c.merchant_number and\n                  sm.merchant_number = mf.merchant_number and\n                  months_between((last_day(sm.active_date)+1),mf.activation_date) between 0 and 13\n        order by  sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1467^7*/
      resultSet = it.getResultSet();
      while(resultSet.next())
      {
        retVal.add( new NDRAdjustmentDetail(resultSet) );
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadNDRAdjustmentDetails()",e.toString());
    }      
    return(retVal);
  }
  
  protected void loadReferralExpense( NDRAdjustmentDetail ndr )
  {
    double                  expense             = 0.0;
    BankContractBean        referralContract    = null;
    long                    referralNode        = 0L;
    
    try
    {
      // add the referral expense if necessary
      if ( (referralNode = getReferralNode(ndr.MerchantId)) != 0L )
      {
        double    refPerItem;
        double    refRate;
        
        referralContract = new BankContractBean();
        referralContract.connect();
        referralContract.loadContract( hierarchyNodeToOrgId(referralNode), 
                                       ContractTypes.CONTRACT_SOURCE_REFERRAL,
                                       ndr.ActiveDate );
                                   
        refRate     = referralContract.getContractItemRate( BankContractBean.BET_PROCESSING_RATE );
        refPerItem  = referralContract.getContractItemRate( BankContractBean.BET_PROCESSING_PER_ITEM );
        
        /*@lineinfo:generated-code*//*@lineinfo:1504^9*/

//  ************************************************************
//  #sql [Ctx] { select  round( ( (:ndr.VolAmount * :refRate * 0.01) +
//                             (:ndr.VolCount * :refPerItem) ),2)   
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  round( ( ( :1  *  :2  * 0.01) +\n                           ( :3  *  :4 ) ),2)    \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.SalesCommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,ndr.VolAmount);
   __sJT_st.setDouble(2,refRate);
   __sJT_st.setInt(3,ndr.VolCount);
   __sJT_st.setDouble(4,refPerItem);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   expense = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1509^9*/
        ndr.ReferralExpense = expense;
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadReferralExpense()",e.toString() );
    }
    finally
    {
      try{ referralContract.cleanUp(); } catch( Exception e ) {}
    }
  }
  
  public RetentionSummaryData loadRetentionData( )
  {
    Date                        activeDate          = null;
    MesCommission.ChargeRecord  chargeRec           = null;
    double                      exp                 = 0.0;
    double[]                    expense             = new double[2];
    double                      inc                 = 0.0;
    double[]                    income              = new double[2];
    ResultSetIterator           it                  = null;
    RetentionSummaryData        rData               = null;
    ResultSet                   resultSet           = null;
    String                      stage               = "entry";//@
    long                        userId              = ReportOrgId;
    
    try
    {
      // get the active date
      /*@lineinfo:generated-code*//*@lineinfo:1540^7*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(:ReportDateBegin,'month') 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc( :1 ,'month')  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.SalesCommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,ReportDateBegin);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1544^7*/
      
      rData = new RetentionSummaryData(userId,activeDate);
      
      // reset the tracking variables
      for ( int i = 0; i < income.length; ++i )
      {
        income[i] = 0;
        expense[i] = 0;
      }
      inc = 0.0;
      
      //
      // perform two calculations to determine if this sales
      // rep qualifies for the retention bonus.
      //   
      // 1) $60K annualized revenue
      //
      //    This is calculated by taking all the accounts that
      //    have activated in the last rolling 12 month period
      //    and calculating their average monthly revenue.
      //    The sum of these merchant monthly averages is 
      //    compared to $5K to determine if the annualized 
      //    revenue is sufficient.    
      //
      // 2) 85% retention rate
      //
      //    This is calculated by using the accounts on file 
      //    in the same month of the previous year (i.e. for 01/04 
      //    use accounts on file in 01/03).  The net revenue from
      //    both months are compared.  If the current months revenue
      //    is at least 85% of the same month a year ago for the
      //    same accounts then the rep qualifies.
      //
      stage = "1:ndr";//@
      /*@lineinfo:generated-code*//*@lineinfo:1579^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl( round(sum( dt.income/dt.month_count ),2), 0 ),
//                  nvl( round(sum( dt.expense/dt.month_count ),2), 0 )
//          
//          from    mif               mf,    
//                  (            
//                    select  sm.merchant_number,
//                            nvl( sum(sm.discount_inc_calc +
//                                     sm.tot_inc_debit +
//                                     sm.tot_inc_interchange +
//                                     nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0)
//                                    ), 0 )                                 as income,
//                            nvl( sum(sm.interchange_expense +
//                                     sm.vmc_assessment_expense + 
//                                     nvl(sm.tot_ndr_referral,0) +
//                                     nvl(sm.tot_authorization_referral,0) +
//                                     nvl(sm.tot_capture_referral,0) +
//                                     nvl(sm.tot_debit_referral,0) +
//                                     nvl(sm.tot_ind_plans_referral,0) +
//                                     nvl(sm.tot_sys_generated_referral,0) +
//                                     nvl(sm.tot_equip_rental_referral,0) +
//                                     nvl(sm.tot_equip_sales_referral,0) 
//                                    ),0 )                                  as expense,
//                            count(sm.merchant_number)                      as month_count
//                    from    application                 app,
//                            merchant                    mr,
//                            mif                         mf,
//                            monthly_extract_summary     sm,
//                            sales_rep                   sr
//                    where   app.app_user_id = :userId and
//                            mr.app_seq_num = app.app_seq_num and
//                            mf.merchant_number = mr.merch_number and
//                            mf.activation_date between (last_day(:activeDate-350)+1) and
//                                                       last_day(:activeDate) and
//                            sm.merchant_number = mf.merchant_number and
//                            sm.active_date between (:activeDate-366) and :activeDate and
//                            sr.user_id = app.app_user_id and
//                            is_direct_commission_plan(sr.rep_plan_id) = 1
//                    group by sm.merchant_number
//                  )                 dt
//          where mf.bank_number = 3941 and
//                dt.merchant_number = mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl( round(sum( dt.income/dt.month_count ),2), 0 ),\n                nvl( round(sum( dt.expense/dt.month_count ),2), 0 )\n         \n        from    mif               mf,    \n                (            \n                  select  sm.merchant_number,\n                          nvl( sum(sm.discount_inc_calc +\n                                   sm.tot_inc_debit +\n                                   sm.tot_inc_interchange +\n                                   nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0)\n                                  ), 0 )                                 as income,\n                          nvl( sum(sm.interchange_expense +\n                                   sm.vmc_assessment_expense + \n                                   nvl(sm.tot_ndr_referral,0) +\n                                   nvl(sm.tot_authorization_referral,0) +\n                                   nvl(sm.tot_capture_referral,0) +\n                                   nvl(sm.tot_debit_referral,0) +\n                                   nvl(sm.tot_ind_plans_referral,0) +\n                                   nvl(sm.tot_sys_generated_referral,0) +\n                                   nvl(sm.tot_equip_rental_referral,0) +\n                                   nvl(sm.tot_equip_sales_referral,0) \n                                  ),0 )                                  as expense,\n                          count(sm.merchant_number)                      as month_count\n                  from    application                 app,\n                          merchant                    mr,\n                          mif                         mf,\n                          monthly_extract_summary     sm,\n                          sales_rep                   sr\n                  where   app.app_user_id =  :1  and\n                          mr.app_seq_num = app.app_seq_num and\n                          mf.merchant_number = mr.merch_number and\n                          mf.activation_date between (last_day( :2 -350)+1) and\n                                                     last_day( :3 ) and\n                          sm.merchant_number = mf.merchant_number and\n                          sm.active_date between ( :4 -366) and  :5  and\n                          sr.user_id = app.app_user_id and\n                          is_direct_commission_plan(sr.rep_plan_id) = 1\n                  group by sm.merchant_number\n                )                 dt\n        where mf.bank_number = 3941 and\n              dt.merchant_number = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.SalesCommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setDate(4,activeDate);
   __sJT_st.setDate(5,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   inc = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   exp = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1622^7*/
      rData.addRevenueNdr(inc,exp);
      
      inc = 0.0;
      exp = 0.0;
      
      stage = "1:t&e";//@
      // load T&E auths      
      /*@lineinfo:generated-code*//*@lineinfo:1630^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl( round(sum( dt.income/dt.month_count ),2), 0 )
//          
//          from    mif               mf,    
//                  (            
//                    select  sm.merchant_number                  as merchant_number,
//                            nvl(sum( ap.AUTH_INCOME_TOTAL ),0)  as income,
//                            count(distinct sm.hh_load_sec)      as month_count
//                    from    application                 app,
//                            monthly_extract_summary     sm,
//                            monthly_extract_ap          ap,
//                            merchant                    mr,
//                            mif                         mf,
//                            sales_rep                   sr
//                    where   app.app_user_id = :userId and
//                            mr.app_seq_num = app.app_seq_num and
//                            mf.merchant_number = mr.merch_number and 
//                            mf.activation_date between (last_day(:activeDate-350)+1) and
//                                                        last_day(:activeDate) and
//                            sm.merchant_number = mf.merchant_number and
//                            sm.active_date between (:activeDate-366) and :activeDate and
//                            sr.user_id      = app.app_user_id and
//                            is_direct_commission_plan(sr.rep_plan_id) = 1 and
//                            ap.hh_load_sec = sm.hh_load_sec and
//                            not ap.a1_plan_type in ( 'VS','MC','DB' )
//                    group by sm.merchant_number                            
//                  )                 dt
//          where mf.bank_number = 3941 and
//                dt.merchant_number = mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl( round(sum( dt.income/dt.month_count ),2), 0 )\n         \n        from    mif               mf,    \n                (            \n                  select  sm.merchant_number                  as merchant_number,\n                          nvl(sum( ap.AUTH_INCOME_TOTAL ),0)  as income,\n                          count(distinct sm.hh_load_sec)      as month_count\n                  from    application                 app,\n                          monthly_extract_summary     sm,\n                          monthly_extract_ap          ap,\n                          merchant                    mr,\n                          mif                         mf,\n                          sales_rep                   sr\n                  where   app.app_user_id =  :1  and\n                          mr.app_seq_num = app.app_seq_num and\n                          mf.merchant_number = mr.merch_number and \n                          mf.activation_date between (last_day( :2 -350)+1) and\n                                                      last_day( :3 ) and\n                          sm.merchant_number = mf.merchant_number and\n                          sm.active_date between ( :4 -366) and  :5  and\n                          sr.user_id      = app.app_user_id and\n                          is_direct_commission_plan(sr.rep_plan_id) = 1 and\n                          ap.hh_load_sec = sm.hh_load_sec and\n                          not ap.a1_plan_type in ( 'VS','MC','DB' )\n                  group by sm.merchant_number                            \n                )                 dt\n        where mf.bank_number = 3941 and\n              dt.merchant_number = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.reports.SalesCommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setDate(4,activeDate);
   __sJT_st.setDate(5,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   inc = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1660^7*/
      rData.addRevenueAuth(inc);
      
      income[0] = 0.0;
      inc       = 0.0;
    
      stage = "1:chg";//@
      // load all the revenue generated through charge records
      for ( int i = 0; i < MesCommission.MonthlyChargeRecords.length; ++i )
      {
        chargeRec = MesCommission.MonthlyChargeRecords[i];
      
        /*@lineinfo:generated-code*//*@lineinfo:1672^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl( round(sum( dt.income/dt.month_count ),2), 0 )
//            
//            from    mif               mf,    
//                    (            
//                      select    sm.merchant_number              as merchant_number,
//                                nvl(sum( st.ST_FEE_AMOUNT ),0)  as income,
//                                count(distinct sm.hh_load_sec)  as month_count
//                      from      application               app,
//                                merchant                  mr,
//                                mif                       mf,
//                                monthly_extract_summary   sm,
//                                monthly_extract_st        st,
//                                sales_rep                 sr
//                      where     app.app_user_id = :userId and
//                                mr.app_seq_num = app.app_seq_num and
//                                mf.merchant_number = mr.merch_number and 
//                                mf.activation_date between (last_day(:activeDate-350)+1) and
//                                                            last_day(:activeDate) and
//                                sm.merchant_number = mf.merchant_number and
//                                sm.active_date between (:activeDate-366) and :activeDate and
//                                ( :chargeRec.ProductCode is null or 
//                                  sm.product_code = :chargeRec.ProductCode ) and
//                                sr.USER_ID = app.APP_USER_ID and
//                                is_direct_commission_plan(sr.rep_plan_id) = 1 and
//                                st.hh_load_sec = sm.hh_load_sec and
//                                st.ST_STATEMENT_DESC like :chargeRec.SearchMask
//                      group by sm.merchant_number
//                    )                 dt
//            where mf.bank_number = 3941 and
//                  dt.merchant_number = mf.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl( round(sum( dt.income/dt.month_count ),2), 0 )\n           \n          from    mif               mf,    \n                  (            \n                    select    sm.merchant_number              as merchant_number,\n                              nvl(sum( st.ST_FEE_AMOUNT ),0)  as income,\n                              count(distinct sm.hh_load_sec)  as month_count\n                    from      application               app,\n                              merchant                  mr,\n                              mif                       mf,\n                              monthly_extract_summary   sm,\n                              monthly_extract_st        st,\n                              sales_rep                 sr\n                    where     app.app_user_id =  :1  and\n                              mr.app_seq_num = app.app_seq_num and\n                              mf.merchant_number = mr.merch_number and \n                              mf.activation_date between (last_day( :2 -350)+1) and\n                                                          last_day( :3 ) and\n                              sm.merchant_number = mf.merchant_number and\n                              sm.active_date between ( :4 -366) and  :5  and\n                              (  :6  is null or \n                                sm.product_code =  :7  ) and\n                              sr.USER_ID = app.APP_USER_ID and\n                              is_direct_commission_plan(sr.rep_plan_id) = 1 and\n                              st.hh_load_sec = sm.hh_load_sec and\n                              st.ST_STATEMENT_DESC like  :8 \n                    group by sm.merchant_number\n                  )                 dt\n          where mf.bank_number = 3941 and\n                dt.merchant_number = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.SalesCommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setDate(4,activeDate);
   __sJT_st.setDate(5,activeDate);
   __sJT_st.setString(6,chargeRec.ProductCode);
   __sJT_st.setString(7,chargeRec.ProductCode);
   __sJT_st.setString(8,chargeRec.SearchMask);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   inc = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1704^9*/
        income[0] += inc;
      }
      rData.addRevenueMisc(income[0]);
      
      System.out.println("Stage 1: " + userId + " : " + income + " : " + expense);
      
      // reset the tracking variables
      for ( int i = 0; i < income.length; ++i )
      {
        income[i] = 0;
        expense[i] = 0;
      }
      
      stage = "2:ndr";//@
      // second portion 
      /*@lineinfo:generated-code*//*@lineinfo:1720^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                                as active_date,
//                  nvl( sum(sm.discount_inc_calc +
//                           sm.tot_inc_debit +
//                           sm.tot_inc_interchange +
//                           nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) +
//                           nvl(ca.income_adjustment,0)
//                          ), 0 )                                as income,
//                  nvl( sum(sm.interchange_expense +
//                           sm.vmc_assessment_expense + 
//                           nvl(sm.tot_ndr_referral,0) +
//                           nvl(sm.tot_authorization_referral,0) +
//                           nvl(sm.tot_capture_referral,0) +
//                           nvl(sm.tot_debit_referral,0) +
//                           nvl(sm.tot_ind_plans_referral,0) +
//                           nvl(sm.tot_sys_generated_referral,0) +
//                           nvl(sm.tot_equip_rental_referral,0) +
//                           nvl(sm.tot_equip_sales_referral,0) 
//                          ),0 )                                 as expense
//          from    application                 app,
//                  merchant                    mr,
//                  monthly_extract_summary     sm,
//                  commission_adjustment       ca,
//                  sales_rep                   sr
//          where   app.app_user_id = :userId and
//                  mr.app_seq_num = app.app_seq_num and
//                  sm.merchant_number = mr.merch_number and
//                  sm.active_date in ( trunc(:activeDate-350,'month'),:activeDate ) and
//                  exists    -- must have been in the extract a year ago
//                  (
//                    select  smi.merchant_number 
//                    from    monthly_extract_summary smi
//                    where   smi.merchant_number = sm.merchant_number and
//                            smi.active_date = trunc(:activeDate-350,'month')
//                  ) and                  
//                  sr.user_id = app.app_user_id and
//                  is_direct_commission_plan(sr.rep_plan_id) = 1 and
//                  ca.merchant_number(+) = sm.merchant_number and
//                  ca.active_date(+) = sm.active_date
//          group by sm.active_date
//          order by sm.active_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                                as active_date,\n                nvl( sum(sm.discount_inc_calc +\n                         sm.tot_inc_debit +\n                         sm.tot_inc_interchange +\n                         nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) +\n                         nvl(ca.income_adjustment,0)\n                        ), 0 )                                as income,\n                nvl( sum(sm.interchange_expense +\n                         sm.vmc_assessment_expense + \n                         nvl(sm.tot_ndr_referral,0) +\n                         nvl(sm.tot_authorization_referral,0) +\n                         nvl(sm.tot_capture_referral,0) +\n                         nvl(sm.tot_debit_referral,0) +\n                         nvl(sm.tot_ind_plans_referral,0) +\n                         nvl(sm.tot_sys_generated_referral,0) +\n                         nvl(sm.tot_equip_rental_referral,0) +\n                         nvl(sm.tot_equip_sales_referral,0) \n                        ),0 )                                 as expense\n        from    application                 app,\n                merchant                    mr,\n                monthly_extract_summary     sm,\n                commission_adjustment       ca,\n                sales_rep                   sr\n        where   app.app_user_id =  :1  and\n                mr.app_seq_num = app.app_seq_num and\n                sm.merchant_number = mr.merch_number and\n                sm.active_date in ( trunc( :2 -350,'month'), :3  ) and\n                exists    -- must have been in the extract a year ago\n                (\n                  select  smi.merchant_number \n                  from    monthly_extract_summary smi\n                  where   smi.merchant_number = sm.merchant_number and\n                          smi.active_date = trunc( :4 -350,'month')\n                ) and                  \n                sr.user_id = app.app_user_id and\n                is_direct_commission_plan(sr.rep_plan_id) = 1 and\n                ca.merchant_number(+) = sm.merchant_number and\n                ca.active_date(+) = sm.active_date\n        group by sm.active_date\n        order by sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setDate(4,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1762^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        if ( resultSet.getDate("active_date").equals(activeDate) )
        {
          income[1]   += resultSet.getDouble("income");
          expense[1]  += resultSet.getDouble("expense");
        }
        else  // previous year
        {
          income[0]   += resultSet.getDouble("income");
          expense[0]  += resultSet.getDouble("expense");
        }
      }
      resultSet.close();
      it.close();
      
      rData.addRetentionNdr(income,expense);
      
      // reset the tracking variables
      for ( int i = 0; i < income.length; ++i )
      {
        income[i] = 0;
        expense[i] = 0;
      }
     
      stage = "2:t&e";//@
      // load T&E auths
      /*@lineinfo:generated-code*//*@lineinfo:1792^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                      as active_date,
//                  nvl(sum( ap.AUTH_INCOME_TOTAL ),0)  as income
//          from    application                 app,
//                  monthly_extract_summary     sm,
//                  monthly_extract_ap          ap,
//                  merchant                    mr,
//                  sales_rep                   sr
//          where   app.app_user_id = :userId and
//                  mr.app_seq_num = app.app_seq_num and
//                  sm.merchant_number = mr.merch_number and
//                  sm.active_date in ( trunc(:activeDate-350,'month'),:activeDate ) and
//                  exists
//                  (
//                    select  smi.merchant_number 
//                    from    monthly_extract_summary smi
//                    where   smi.merchant_number = sm.merchant_number and
//                            smi.active_date = trunc(:activeDate-350,'month')
//                  ) and
//                  sr.user_id      = app.app_user_id and
//                  is_direct_commission_plan(sr.rep_plan_id) = 1 and
//                  ap.hh_load_sec = sm.hh_load_sec and
//                  not ap.a1_plan_type in ( 'VS','MC','DB' )
//          group by sm.active_date
//          order by sm.active_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                      as active_date,\n                nvl(sum( ap.AUTH_INCOME_TOTAL ),0)  as income\n        from    application                 app,\n                monthly_extract_summary     sm,\n                monthly_extract_ap          ap,\n                merchant                    mr,\n                sales_rep                   sr\n        where   app.app_user_id =  :1  and\n                mr.app_seq_num = app.app_seq_num and\n                sm.merchant_number = mr.merch_number and\n                sm.active_date in ( trunc( :2 -350,'month'), :3  ) and\n                exists\n                (\n                  select  smi.merchant_number \n                  from    monthly_extract_summary smi\n                  where   smi.merchant_number = sm.merchant_number and\n                          smi.active_date = trunc( :4 -350,'month')\n                ) and\n                sr.user_id      = app.app_user_id and\n                is_direct_commission_plan(sr.rep_plan_id) = 1 and\n                ap.hh_load_sec = sm.hh_load_sec and\n                not ap.a1_plan_type in ( 'VS','MC','DB' )\n        group by sm.active_date\n        order by sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setDate(4,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1818^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        if ( resultSet.getDate("active_date").equals(activeDate) )
        {
          income[1] += resultSet.getDouble("income");
        }
        else  // previous year
        {
          income[0] += resultSet.getDouble("income");
        }              
      }
      resultSet.close();
      it.close();
      
      rData.addRetentionAuth(income);
      
      // reset the tracking variables
      for ( int i = 0; i < income.length; ++i )
      {
        income[i] = 0;
        expense[i] = 0;
      }
      
      stage = "2:chg";//@
      // load all the revenue generated through charge records
      for ( int i = 0; i < MesCommission.MonthlyChargeRecords.length; ++i )
      {
        chargeRec = MesCommission.MonthlyChargeRecords[i];
      
        /*@lineinfo:generated-code*//*@lineinfo:1850^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    sm.active_date                  as active_date,
//                      nvl(sum( st.ST_FEE_AMOUNT ),0)  as income
//            from      application               app,
//                      merchant                  mr,
//                      monthly_extract_summary   sm,
//                      monthly_extract_st        st,
//                      sales_rep                 sr
//            where     app.app_user_id = :userId and
//                      mr.app_seq_num = app.app_seq_num and
//                      sm.merchant_number = mr.merch_number and
//                      sm.active_date in ( trunc(:activeDate-350,'month'),:activeDate ) and
//                      exists
//                      (
//                        select  smi.merchant_number 
//                        from    monthly_extract_summary smi
//                        where   smi.merchant_number = sm.merchant_number and
//                                smi.active_date = trunc(:activeDate-350,'month')
//                      ) and
//                      ( :chargeRec.ProductCode is null or 
//                        sm.product_code = :chargeRec.ProductCode ) and
//                      sr.USER_ID = app.APP_USER_ID and
//                      is_direct_commission_plan(sr.rep_plan_id) = 1 and
//                      st.hh_load_sec = sm.hh_load_sec and
//                      st.ST_STATEMENT_DESC like :chargeRec.SearchMask
//            group by sm.active_date
//            order by sm.active_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sm.active_date                  as active_date,\n                    nvl(sum( st.ST_FEE_AMOUNT ),0)  as income\n          from      application               app,\n                    merchant                  mr,\n                    monthly_extract_summary   sm,\n                    monthly_extract_st        st,\n                    sales_rep                 sr\n          where     app.app_user_id =  :1  and\n                    mr.app_seq_num = app.app_seq_num and\n                    sm.merchant_number = mr.merch_number and\n                    sm.active_date in ( trunc( :2 -350,'month'), :3  ) and\n                    exists\n                    (\n                      select  smi.merchant_number \n                      from    monthly_extract_summary smi\n                      where   smi.merchant_number = sm.merchant_number and\n                              smi.active_date = trunc( :4 -350,'month')\n                    ) and\n                    (  :5  is null or \n                      sm.product_code =  :6  ) and\n                    sr.USER_ID = app.APP_USER_ID and\n                    is_direct_commission_plan(sr.rep_plan_id) = 1 and\n                    st.hh_load_sec = sm.hh_load_sec and\n                    st.ST_STATEMENT_DESC like  :7 \n          group by sm.active_date\n          order by sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setDate(4,activeDate);
   __sJT_st.setString(5,chargeRec.ProductCode);
   __sJT_st.setString(6,chargeRec.ProductCode);
   __sJT_st.setString(7,chargeRec.SearchMask);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1878^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          if ( resultSet.getDate("active_date").equals(activeDate) )
          {
            income[1] += resultSet.getDouble("income");
          }
          else  // previous year
          {
            income[0] += resultSet.getDouble("income");
          }              
        }
        resultSet.close();
        it.close();
      }
      rData.addRetentionMisc(income);
    }
    catch(Exception e)
    {
      logEntry("loadRetentionData()",e.toString());
    }
    finally
    {
      try { it.close(); } catch( Exception e ) {}
    }
    return(rData);
  }
  
  public Vector loadSalesRepList( )
  {
    ResultSetIterator     it                = null;
    Vector                reps              = new Vector();
    ResultSet             resultSet         = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1916^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    u.user_id   as user_id,
//                    sr.rep_id   as rep_id, 
//                    u.NAME      as rep_name
//          from      users     u,
//                    sales_rep sr
//          where     sr.user_id = u.user_id                  
//          order by u.name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    u.user_id   as user_id,\n                  sr.rep_id   as rep_id, \n                  u.NAME      as rep_name\n        from      users     u,\n                  sales_rep sr\n        where     sr.user_id = u.user_id                  \n        order by u.name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.reports.SalesCommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1925^7*/      
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        reps.add( new SalesRepEntry( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadSalesRepList()",e.toString());
    }
    finally
    {
      try { it.close(); } catch( Exception e ) {}
    }
    return( reps );
  }
  
  public String renderHtml(String fname)
  {
    return( AddFields.renderHtml(fname) );
  }
  
  public String renderEditHtml(String fname)
  {
    return( EditFields.renderHtml(fname) );
  }
  
  public void setProperties(HttpServletRequest request)
  {
    int         recCount      = 0;
    
    // load the default report properties
    super.setProperties( request );

    setFields( request );
    
    CommissionType = HttpHelper.getInt(request,"commType",mesConstants.COMM_TYPE_NONE);
    DeleteRecId    = HttpHelper.getLong(request,"deleteId",0L);
    EditRecId      = HttpHelper.getLong(request,"editId",0L);
    AddRecord      = HttpHelper.getBoolean(request,"addRecord",false);
    
    // overload the default use of ReportOrgId.  load with the user id 
    // when there was not one available in the request.
    ReportOrgId    = HttpHelper.getLong(request,"com.mes.OrgId",ReportUserBean.getUserId());
    
    if ( ReportOrgId == 0L )
    {
      // does not have rights to see all the commission data, force
      // back to their user login user id.
      if ( ReportUserBean.hasRight(MesUsers.RIGHT_SALES_ADMIN) == false )
      {
        ReportOrgId = ReportUserBean.getUserId();
      }
    }
    
    if ( ReportUserBean.hasRight(MesUsers.RIGHT_SALES_ADMIN) )
    {
      long      recId   = HttpHelper.getLong(request,"releaseId",0L);
      
      if ( recId != 0L )
      {
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1992^11*/

//  ************************************************************
//  #sql [Ctx] { update commissions
//              set transaction_type = 'I',
//                  commission_date = :ReportDateEnd
//              where rec_id = :recId and
//                    transaction_type = 'H'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update commissions\n            set transaction_type = 'I',\n                commission_date =  :1 \n            where rec_id =  :2  and\n                  transaction_type = 'H'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ReportDateEnd);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1999^11*/
          /*@lineinfo:generated-code*//*@lineinfo:2000^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2003^11*/
        }
        catch( java.sql.SQLException e )
        {
          logEntry("setProperties():releaseId=" + recId,e.toString());
        }
      }
      
      recId = HttpHelper.getLong(request,"holdId",0L);
      if ( recId != 0L )
      {
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:2016^11*/

//  ************************************************************
//  #sql [Ctx] { update commissions
//              set transaction_type = 'H'
//              where rec_id = :recId and
//                    transaction_type = 'I'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update commissions\n            set transaction_type = 'H'\n            where rec_id =  :1  and\n                  transaction_type = 'I'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2022^11*/
          /*@lineinfo:generated-code*//*@lineinfo:2023^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2026^11*/
        }
        catch( java.sql.SQLException e )
        {
          logEntry("setProperties():holdId=" + recId,e.toString());
        }
      }
    }
    

//@+  enable for production
//@    try
//@    {
//@      # sql [Ctx]
//@      {
//@        select  count(th.descendent)  into :recCount
//@        from    t_hierarchy   th
//@        where   th.ancestor = :(ReportUserBean.getUserId()) and
//@                th.descendent = :ReportOrgId and
//@                th.hier_type = :(MesHierarchy.HT_MES_SALES)
//@      };
//@    }
//@    catch( java.sql.SQLException e )
//@    {
//@      logEntry("setProperties()",e.toString() );
//@    }      
//@    
//@    // the specified sales rep is not available to 
//@    // the login user, set back to the default
//@    if ( recCount == 0 )
//@    {
//@      ReportOrgId = ReportUserBean.getUserId();
//@    }
//@
//@-    
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         dayOfWeek     = -1;
      int         inc           = 0;
    
      // pay period is sunday through monday.  Calendar values are:
      //
      //    Sunday          1
      //    Monday          2
      //    Tuesday         3
      //    Wednesday       4
      //    Thursday        5
      //    Friday          6
      //    Saturday        7
      //
      // Since we want the previous pay period we need to move the 
      // begin date back a scaling number of days depending on the 
      // current data of the week.  
      //
      dayOfWeek = cal.get( Calendar.DAY_OF_WEEK );
    
      if ( dayOfWeek > Calendar.MONDAY )
      {
        inc = -( dayOfWeek - Calendar.MONDAY );
      }
      else if ( dayOfWeek < Calendar.MONDAY )
      {
        inc = -( dayOfWeek + 1 );
      }
      // else it is Monday, do nothing.
    
      cal.add( Calendar.DAY_OF_MONTH, inc );
    
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    
      cal.add( Calendar.DAY_OF_MONTH, -6 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
  
  public boolean storeNewRecord( )
  {
    double          amount;
    boolean         retVal    = false;
    
    try
    {
      if ( AddFields.isValid() )
      {
        amount = AddFields.getField("income").asDouble();
        amount -= AddFields.getField("expense").asDouble();
        amount *= (AddFields.getField("payRate").asDouble() * 0.01);
//@        if ( amount < 0.0 )
//@        {
//@          amount = 0.0;
//@        }
        amount -= AddFields.getField("advance").asDouble();
        
        /*@lineinfo:generated-code*//*@lineinfo:2125^9*/

//  ************************************************************
//  #sql [Ctx] { insert into commissions
//            (
//              user_id, 
//              commission_amount, 
//              commission_date, 
//              commission_type, 
//              transaction_type,
//              expense, 
//              income, 
//              advance,
//              merchant_number, 
//              pay_rate, 
//              vol_amount, 
//              vol_count,
//              description,
//              load_filename
//            )
//            values
//            (
//              :AddFields.getField("userId").getData(),
//              round(:amount,2),
//              to_date(:AddFields.getField("commDate").getData(),'mm/dd/yyyy'),
//              :AddFields.getField("commType").getData(),
//              'I',
//              :AddFields.getField("expense").getData(),
//              :AddFields.getField("income").getData(),
//              :AddFields.getField("advance").getData(),
//              :AddFields.getField("merchantId").getData(),
//              (:AddFields.getField("payRate").asDouble() * 0.01),
//              :AddFields.getField("volAmount").getData(),
//              :AddFields.getField("volCount").getData(),
//              :AddFields.getField("desc").getData(),
//              'manual entry'
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1289 = AddFields.getField("userId").getData();
 String __sJT_1290 = AddFields.getField("commDate").getData();
 String __sJT_1291 = AddFields.getField("commType").getData();
 String __sJT_1292 = AddFields.getField("expense").getData();
 String __sJT_1293 = AddFields.getField("income").getData();
 String __sJT_1294 = AddFields.getField("advance").getData();
 String __sJT_1295 = AddFields.getField("merchantId").getData();
 double __sJT_1296 = AddFields.getField("payRate").asDouble();
 String __sJT_1297 = AddFields.getField("volAmount").getData();
 String __sJT_1298 = AddFields.getField("volCount").getData();
 String __sJT_1299 = AddFields.getField("desc").getData();
   String theSqlTS = "insert into commissions\n          (\n            user_id, \n            commission_amount, \n            commission_date, \n            commission_type, \n            transaction_type,\n            expense, \n            income, \n            advance,\n            merchant_number, \n            pay_rate, \n            vol_amount, \n            vol_count,\n            description,\n            load_filename\n          )\n          values\n          (\n             :1 ,\n            round( :2 ,2),\n            to_date( :3 ,'mm/dd/yyyy'),\n             :4 ,\n            'I',\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n            ( :9  * 0.01),\n             :10 ,\n             :11 ,\n             :12 ,\n            'manual entry'\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.reports.SalesCommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1289);
   __sJT_st.setDouble(2,amount);
   __sJT_st.setString(3,__sJT_1290);
   __sJT_st.setString(4,__sJT_1291);
   __sJT_st.setString(5,__sJT_1292);
   __sJT_st.setString(6,__sJT_1293);
   __sJT_st.setString(7,__sJT_1294);
   __sJT_st.setString(8,__sJT_1295);
   __sJT_st.setDouble(9,__sJT_1296);
   __sJT_st.setString(10,__sJT_1297);
   __sJT_st.setString(11,__sJT_1298);
   __sJT_st.setString(12,__sJT_1299);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2161^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:2163^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2166^9*/
        
        retVal = true;
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("storeNewRecord()",e.toString());
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/