/*@lineinfo:filename=RiskScoreEditorBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskScoreEditorBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 5/10/04 1:38p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class RiskScoreEditorBean extends ReportSQLJBean
{
  public class RowData
  {
    public long             HierarchyNode   = 0L;
    public long             LowerRange      = 0;
    public String           OrgName         = null;
    public int              Points          = 0;
    public long             RecId           = 0L;
    public String           RiskCatDesc     = null;
    public int              RiskCatId       = 0;
    public long             Threshold       = 0;
    public long             UpperRange      = 0;
    
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      RecId           = resultSet.getLong("rec_id");
      HierarchyNode   = resultSet.getLong("hierarchy_node");
      OrgName         = resultSet.getString("org_name");
      RiskCatId       = resultSet.getInt("risk_cat_id");
      RiskCatDesc     = resultSet.getString("risk_cat_desc");
      LowerRange      = resultSet.getLong("lower_range");
      UpperRange      = resultSet.getLong("upper_range");
      Threshold       = resultSet.getLong("threshold");
      Points          = resultSet.getInt("points");
    }
  }
  
  public RiskScoreEditorBean( )
  {
  }
  
  protected void deleteScoreElement( long id )
  {
    try
    {
      setAutoCommit(false);
      
      /*@lineinfo:generated-code*//*@lineinfo:95^7*/

//  ************************************************************
//  #sql [Ctx] { delete  
//          from    risk_score_system rss
//          where   rss.rec_id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  \n        from    risk_score_system rss\n        where   rss.rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.reports.RiskScoreEditorBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:100^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:102^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:105^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("deleteScoreElement()",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:110^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:110^34*/ } catch( Exception ee ){}
    }
    finally
    {
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Hierarchy Node\",");
    line.append("\"Org Name\",");
    line.append("\"Risk Cat ID\",");
    line.append("\"Risk Cat Desc\",");
    line.append("\"Lower Range\",");
    line.append("\"Upper Range\",");
    line.append("\"Threshold\",");
    line.append("\"Points\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( encodeHierarchyNode( record.HierarchyNode ) );
    line.append( ",\"" );
    line.append( record.OrgName );
    line.append( "\"," );
    line.append( record.RiskCatId );
    line.append( ",\"" );
    line.append( record.RiskCatDesc );
    line.append( "\"," );
    line.append( record.LowerRange );
    line.append( "," );
    line.append( record.UpperRange );
    line.append( "," );
    line.append( record.Threshold );
    line.append( "," );
    line.append( record.Points );
  }

  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append("risk_score_system_settings");
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    loadData( getReportHierarchyNode(), ReportDateBegin, ReportDateEnd );
  }
  
  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator       it              = null;
    ResultSet               resultSet       = null;
    
    try
    {
      ReportRows.removeAllElements();
      
      /*@lineinfo:generated-code*//*@lineinfo:188^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rss.rec_id                            as rec_id,
//                  rss.hierarchy_node                    as hierarchy_node,
//                  o.org_name                            as org_name,
//                  rss.risk_cat_id                       as risk_cat_id,
//                  nvl(rc.risk_cat_desc,rss.risk_cat_id) as risk_cat_desc,
//                  rss.lower_range                       as lower_range,
//                  rss.upper_range                       as upper_range,
//                  rss.threshold                         as threshold,
//                  rss.points                            as points
//          from    risk_score_system   rss,
//                  risk_categories     rc,
//                  organization        o
//          where   ( 
//                    rss.hierarchy_node in
//                    (
//                      select  th.descendent
//                      from    t_hierarchy th
//                      where   th.hier_type = 1 and
//                              th.ancestor = :nodeId 
//                    ) or
//                    rss.hierarchy_node in 
//                    (
//                      select  gm.merchant_number
//                      from    organization    o,
//                              group_merchant  gm
//                      where   o.org_group = :nodeId and
//                              gm.org_num = o.org_num
//                    )
//                  ) and
//                  rc.risk_cat_id(+) = rss.risk_cat_id and
//                  o.org_group = rss.hierarchy_node
//          order by  o.org_name, 
//                    nvl( rc.risk_cat_desc, ('Undefined ' || rss.risk_cat_id)),
//                    rss.points,
//                    rss.lower_range,
//                    rss.upper_range
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rss.rec_id                            as rec_id,\n                rss.hierarchy_node                    as hierarchy_node,\n                o.org_name                            as org_name,\n                rss.risk_cat_id                       as risk_cat_id,\n                nvl(rc.risk_cat_desc,rss.risk_cat_id) as risk_cat_desc,\n                rss.lower_range                       as lower_range,\n                rss.upper_range                       as upper_range,\n                rss.threshold                         as threshold,\n                rss.points                            as points\n        from    risk_score_system   rss,\n                risk_categories     rc,\n                organization        o\n        where   ( \n                  rss.hierarchy_node in\n                  (\n                    select  th.descendent\n                    from    t_hierarchy th\n                    where   th.hier_type = 1 and\n                            th.ancestor =  :1  \n                  ) or\n                  rss.hierarchy_node in \n                  (\n                    select  gm.merchant_number\n                    from    organization    o,\n                            group_merchant  gm\n                    where   o.org_group =  :2  and\n                            gm.org_num = o.org_num\n                  )\n                ) and\n                rc.risk_cat_id(+) = rss.risk_cat_id and\n                o.org_group = rss.hierarchy_node\n        order by  o.org_name, \n                  nvl( rc.risk_cat_desc, ('Undefined ' || rss.risk_cat_id)),\n                  rss.points,\n                  rss.lower_range,\n                  rss.upper_range";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.RiskScoreEditorBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setLong(2,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.RiskScoreEditorBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    long        delId     = 0L;
  
    // load the default report properties
    super.setProperties( request );
    
    if ( ( (delId = HttpHelper.getLong(request,"deleteId",-1L)) != -1L ) &&
         ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_RISK_QUEUE_ADMIN ) ) )
    {
      deleteScoreElement(delId);
    }
  }
}/*@lineinfo:generated-code*/