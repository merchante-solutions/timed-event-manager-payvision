/*@lineinfo:filename=CQAppTypeDropDown*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CQAppTypeDropDown.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 4/20/04 4:09p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class CQAppTypeDropDown extends DropDownTable
{
  public CQAppTypeDropDown( )
  {
  }
  
  public void initialize( Object[] params )
  {
    ResultSetIterator it          = null;
    long              nodeId      = 0L;
    int               recCount    = 0;
    ResultSet         resultSet   = null;
    
    try
    {
      connect();
      
      nodeId = ((Long)params[0]).longValue();
      
      /*@lineinfo:generated-code*//*@lineinfo:50^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  asat.app_type as name,
//                  at.app_description   as value
//          from    app_status_app_types    asat,
//                  app_type                at
//          where   ( :nodeId = 9999999999 or
//                    asat.hierarchy_node = :nodeId ) and
//                  at.app_type_code = asat.app_type      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  asat.app_type as name,\n                at.app_description   as value\n        from    app_status_app_types    asat,\n                app_type                at\n        where   (  :1  = 9999999999 or\n                  asat.hierarchy_node =  :2  ) and\n                at.app_type_code = asat.app_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CQAppTypeDropDown",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setLong(2,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.CQAppTypeDropDown",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:59^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        addElement(resultSet);
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("initialize()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/