/*@lineinfo:filename=AcctICRatesDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AcctICRatesDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 2/10/04 3:23p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class AcctICRatesDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public  String          BetSegmentId        = null;
    public  String          CardType            = null;
    public  String          IcCat               = null;
    public  String          IcDesc              = null;
    public  double          IcPerItem           = 0.0;
    public  double          IcRate              = 0.0;
    public  long            RecId               = 0L;
    public  Date            ValidDateBegin      = null;
    public  Date            ValidDateEnd        = null;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      BetSegmentId        = processString(resultSet.getString("bet_segment_id"));
      CardType            = processString(resultSet.getString("card_type"));
      IcCat               = processString(resultSet.getString("ic_cat"));
      IcDesc              = processString(resultSet.getString("ic_desc"));
      IcPerItem           = resultSet.getDouble("ic_per_item");
      IcRate              = resultSet.getDouble("ic_rate");
      RecId               = resultSet.getLong("rec_id");
      ValidDateBegin      = resultSet.getDate("valid_date_begin");
      ValidDateEnd        = resultSet.getDate("valid_date_end");
    }
  }
  
  protected boolean       ShowHistoricRates       = false;
  
  public AcctICRatesDataBean( )
  {
  }
  
  protected void deleteRate( long id )
  {
    try
    {
      setAutoCommit(false);
      
      /*@lineinfo:generated-code*//*@lineinfo:90^7*/

//  ************************************************************
//  #sql [Ctx] { delete  
//          from    daily_detail_file_ic_desc icd
//          where   icd.rec_id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  \n        from    daily_detail_file_ic_desc icd\n        where   icd.rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.reports.AcctICRatesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:95^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:97^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:100^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("deleteRate()",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:105^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:105^34*/ } catch( Exception ee ){}
    }
    finally
    {
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Card Type\",");
    line.append("\"IC Cat\",");
    line.append("\"IC Description\",");
    line.append("\"Rate\",");
    line.append("\"Per Item\",");
    line.append("\"Valid Date Begin\",");
    line.append("\"Valid Date End\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append("\"");
    line.append(record.CardType);
    line.append("\",");
    line.append(record.IcCat);
    line.append(",\"");
    line.append(record.IcDesc);
    line.append("\",");
    line.append(record.IcPerItem);
    line.append(",");
    line.append(record.IcRate);
    line.append(",");
    line.append(DateTimeFormatter.getFormattedDate(record.ValidDateBegin,"MM/dd/yyyy"));
    line.append(",");
    line.append(DateTimeFormatter.getFormattedDate(record.ValidDateEnd,"MM/dd/yyyy"));
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append("ic_rates_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator       it                = null;
    ResultSet               resultSet         = null;
    int                     showAllRates      = (ShowHistoricRates ? 1 : 0);
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:186^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  icd.rec_id                  as rec_id,
//                  icd.card_type               as card_type,
//                  lpad(icd.ic_code,3,'0')     as ic_cat,
//                  icd.ic_desc                 as ic_desc,
//                  icd.ic_rate                 as ic_rate,
//                  icd.ic_per_item             as ic_per_item,
//                  icd.valid_date_begin        as valid_date_begin,
//                  icd.valid_date_end          as valid_date_end,
//                  icd.bet_segment_id          as bet_segment_id
//          from    daily_detail_file_ic_desc     icd
//          where   ( :showAllRates = 1 or
//                    trunc(sysdate) between icd.valid_date_begin and 
//                                           icd.valid_date_end )
//          order by icd.card_type, lpad(icd.ic_code,3,'0'), 
//                   icd.valid_date_begin, icd.valid_date_end
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  icd.rec_id                  as rec_id,\n                icd.card_type               as card_type,\n                lpad(icd.ic_code,3,'0')     as ic_cat,\n                icd.ic_desc                 as ic_desc,\n                icd.ic_rate                 as ic_rate,\n                icd.ic_per_item             as ic_per_item,\n                icd.valid_date_begin        as valid_date_begin,\n                icd.valid_date_end          as valid_date_end,\n                icd.bet_segment_id          as bet_segment_id\n        from    daily_detail_file_ic_desc     icd\n        where   (  :1  = 1 or\n                  trunc(sysdate) between icd.valid_date_begin and \n                                         icd.valid_date_end )\n        order by icd.card_type, lpad(icd.ic_code,3,'0'), \n                 icd.valid_date_begin, icd.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AcctICRatesDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,showAllRates);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AcctICRatesDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:203^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    long        delId     = 0L;
    
    // load the default report properties
    super.setProperties( request );
    
    ShowHistoricRates = HttpHelper.getBoolean(request,"allRates",false);
    
    if ( ( (delId = HttpHelper.getLong(request,"deleteId",-1L)) != -1L ) &&
         ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_IC_EDITOR ) ) )
    {
      deleteRate(delId);
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
  
  public boolean showingInactiveRates( )
  {
    return( ShowHistoricRates );
  }
}/*@lineinfo:generated-code*/