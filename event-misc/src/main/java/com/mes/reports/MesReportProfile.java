/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/mesReportProfile.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 1/29/02 5:05p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.util.Calendar;
import java.util.Date;

//C+    MesReportProfile
public class   MesReportProfile
{
  // instance variables
  String          DisplayName     = "UNINITIALIZED";
  long            MerchantId      = 0;
  long            OrgId           = 0;
  Calendar        ReportFromDate  = Calendar.getInstance();
  Calendar        ReportToDate    = Calendar.getInstance();
  String          ReportFilename  = "UNINITIALIZED";
  byte            SortOrder       = -1;   //@ obsolete

  public MesReportProfile( )
  {
    ReportFromDate.setTime( new Date() );   // set to now
    ReportToDate.setTime( new Date() );     // set to now
  }

  public void encodeReportDate( StringBuffer buffer )
  {
    if ( buffer != null )
    {
      buffer.append( "com.mes.fromMonth=" );
      buffer.append( ReportFromDate.get( Calendar.MONTH ) );
      buffer.append( "&com.mes.fromDay=" );
      buffer.append( ReportFromDate.get( Calendar.DAY_OF_MONTH ) );
      buffer.append( "&com.mes.fromYear=" );
      buffer.append( ReportFromDate.get( Calendar.YEAR ) );
      buffer.append( "&com.mes.toMonth=" );
      buffer.append( ReportToDate.get( Calendar.MONTH ) );
      buffer.append( "&com.mes.toDay=" );
      buffer.append( ReportToDate.get( Calendar.DAY_OF_MONTH ) );
      buffer.append( "&com.mes.toYear=" );
      buffer.append( ReportToDate.get( Calendar.YEAR ) );
    }
  }
  
  public String getDisplayName( )
  {
    return( DisplayName );
  }
  
  public long getMerchantId( )
  {
    return( MerchantId );
  }
  
  public long getOrgId( )
  {
    return( OrgId );
  }

  public String getOrgUrl( String pageName )
  {
    StringBuffer        buffer = new StringBuffer( pageName );

    buffer.append( "?" );

    encodeReportDate( buffer );

    buffer.append( "&com.mes.OrgId=" );
    buffer.append( OrgId );

    return( buffer.toString() );
  }
  
  public String getReportFilename( )
  {
    return( ReportFilename );
  }
  
  public Date getReportFromDate( )
  {
    return( ReportFromDate.getTime() );
  }
  
  public Date getReportToDate( )
  {
    return( ReportToDate.getTime() );
  }
  
  public byte getSortOrder( )
  {
    return( SortOrder );
  }
  
  public void setDisplayName( String newDisplayName )
  {
    DisplayName = newDisplayName;
  }

  public void setOrgId( long newOrgId )
  {
    OrgId = newOrgId;
  }

  public void setMerchantId( long newMerchId )
  {
    MerchantId = newMerchId;
  }

  public void setReportDate( Date fromDate, Date toDate )
  {
    if ( fromDate != null )
    {
      ReportFromDate.setTime( fromDate );
    }
    
    if ( toDate != null)
    {
      ReportToDate.setTime( toDate );
    }
  }
  
  public void setReportFilename( String newReportFilename )
  {
    ReportFilename = newReportFilename;
  }
  
  public void setSortOrder( byte newSortOrder )
  {
    SortOrder = newSortOrder;
  }
}
//C-    MesReportProfile



