/*@lineinfo:filename=MerchBillingDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchBillingDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 5/30/03 4:48p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class MerchBillingDataBean extends ReportSQLJBean
{
  public class RowData
    implements Comparable
  {
    public double           AuthInc         = 0.0;
    public double           CaptureInc      = 0.0;
    public double           DebitInc        = 0.0;
    public double           DiscountInc     = 0.0;
    public long             DiscoverId      = 0L;
    public double           EquipRentalInc  = 0.0;
    public double           EquipSalesInc   = 0.0;
    public long             HierarchyNode   = 0L;
    public double           ICInc           = 0.0;
    public double           PlanInc         = 0.0;
    public long             OrgId           = 0L;
    public String           OrgName         = null;
    public double           SysGenInc       = 0.0;
  
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      DiscoverId      = resultSet.getLong("discover_id");
      OrgId           = resultSet.getLong("org_num");
      HierarchyNode   = resultSet.getLong("hierarchy_node");
      OrgName         = resultSet.getString("org_name");
      DiscountInc     = resultSet.getDouble("disc_inc");
      ICInc           = resultSet.getDouble("ic_inc");
      AuthInc         = resultSet.getDouble("auth_inc");
      CaptureInc      = resultSet.getDouble("capture_inc");
      DebitInc        = resultSet.getDouble("debit_inc");
      PlanInc         = resultSet.getDouble("plan_inc");
      SysGenInc       = resultSet.getDouble("sys_gen_inc");
      EquipSalesInc   = resultSet.getDouble("equip_sales_inc");
      EquipRentalInc  = resultSet.getDouble("equip_rental_inc");
    }
    
    public int compareTo( Object obj )
    {
      RowData         compareObj    = (RowData)obj;
      int             retVal        = 0;
      
      if ( (retVal = OrgName.compareTo(compareObj.OrgName)) == 0 )
      {
        if ( (retVal = (int)(HierarchyNode - compareObj.HierarchyNode)) == 0 )
        {
          retVal = (int)(OrgId - compareObj.OrgId);
        }
      }
      return( retVal );
    }
    
    public double getTotalIncome( )
    {
      return( DiscountInc +
              ICInc +
              AuthInc +
              CaptureInc +
              DebitInc +
              PlanInc +
              SysGenInc +
              EquipSalesInc +
              EquipRentalInc );
    }
  }
  
  public MerchBillingDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    // add the discover merchant number for discover
    if ( (getReportHierarchyNodeDefault() == 3941400019L) &&
         (ReportType == RT_DETAILS) ) 
    {
      line.append("\"Discover Merchant ID\",");
    }
    
    if ( ReportType == RT_DETAILS )
    {
      line.append("\"Merchant ID\",");
      line.append("\"DBA Name\",");
    }
    else    // RT_SUMMARY
    {
      line.append("\"Org ID\",");
      line.append("\"Org Name\",");
    }
    line.append("\"Discount Inc\",");
    line.append("\"IC Inc\",");
    line.append("\"Auth Inc\",");
    line.append("\"Capture Inc\",");
    line.append("\"Debit Inc\",");
    line.append("\"Ind. Plan Inc\",");
    line.append("\"Misc Inc\",");
    line.append("\"Equip Sales\",");
    line.append("\"Equip Rental\",");
    line.append("\"Total Inc\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    // add the discover merchant number for discover
    if ( (getReportHierarchyNodeDefault() == 3941400019L) &&
         (ReportType == RT_DETAILS) ) 
    {
      line.append( encodeHierarchyNode( record.DiscoverId ) );
      line.append( "," );
    }
    
    line.append( encodeHierarchyNode(record.HierarchyNode) );
    line.append( ",\"" );
    line.append( record.OrgName );
    line.append( "\"," );
    line.append( record.DiscountInc );
    line.append( "," );
    line.append( record.ICInc );
    line.append( "," );
    line.append( record.AuthInc );
    line.append( "," );
    line.append( record.CaptureInc );
    line.append( "," );
    line.append( record.DebitInc );
    line.append( "," );
    line.append( record.PlanInc );
    line.append( "," );
    line.append( record.SysGenInc );
    line.append( "," );
    line.append( record.EquipSalesInc );
    line.append( "," );
    line.append( record.EquipRentalInc );
    line.append( "," );
    line.append( record.getTotalIncome() );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_merchant_billing_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( ReportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:233^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                       as org_num,
//                    sm.merchant_number              as hierarchy_node,
//                    mf.dba_name                     as org_name,
//                    mf.dmdsnum                      as discover_id,
//                    sm.tot_inc_discount             as disc_inc,
//                    sm.tot_inc_interchange          as ic_inc,
//                    sm.tot_inc_authorization        as auth_inc,
//                    sm.tot_inc_capture              as capture_inc,
//                    sm.tot_inc_debit                as debit_inc,
//                    sm.tot_inc_ind_plans            as plan_inc,
//                    ( sm.tot_inc_sys_generated -
//                      ( sm.equip_rental_income +
//                        sm.equip_sales_income ) )   as sys_gen_inc,
//                    sm.equip_rental_income          as equip_rental_inc,
//                    sm.equip_sales_income           as equip_sales_inc
//            from    group_merchant              gm,
//                    mif                         mf,
//                    monthly_extract_summary     sm,
//                    organization                o
//            where   gm.org_num = :orgId and
//                    mf.merchant_number = gm.merchant_number and
//                    sm.merchant_number = mf.merchant_number and
//                    sm.active_date = :beginDate and
//                    o.org_group = mf.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                       as org_num,\n                  sm.merchant_number              as hierarchy_node,\n                  mf.dba_name                     as org_name,\n                  mf.dmdsnum                      as discover_id,\n                  sm.tot_inc_discount             as disc_inc,\n                  sm.tot_inc_interchange          as ic_inc,\n                  sm.tot_inc_authorization        as auth_inc,\n                  sm.tot_inc_capture              as capture_inc,\n                  sm.tot_inc_debit                as debit_inc,\n                  sm.tot_inc_ind_plans            as plan_inc,\n                  ( sm.tot_inc_sys_generated -\n                    ( sm.equip_rental_income +\n                      sm.equip_sales_income ) )   as sys_gen_inc,\n                  sm.equip_rental_income          as equip_rental_inc,\n                  sm.equip_sales_income           as equip_sales_inc\n          from    group_merchant              gm,\n                  mif                         mf,\n                  monthly_extract_summary     sm,\n                  organization                o\n          where   gm.org_num =  :1  and\n                  mf.merchant_number = gm.merchant_number and\n                  sm.merchant_number = mf.merchant_number and\n                  sm.active_date =  :2  and\n                  o.org_group = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchBillingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchBillingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:259^9*/
      }
      else    // RT_SUMMARY
      {
        if ( SummaryType == ST_PARENT )
        {
          /*@lineinfo:generated-code*//*@lineinfo:265^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                           as org_num, 
//                      o.org_group                         as hierarchy_node,
//                      o.org_name                          as org_name,
//                      0                                   as discover_id,
//                      sum( sm.tot_inc_discount )          as disc_inc,
//                      sum( sm.tot_inc_interchange )       as ic_inc,
//                      sum( sm.tot_inc_authorization )     as auth_inc,
//                      sum( sm.tot_inc_capture )           as capture_inc,
//                      sum( sm.tot_inc_debit )             as debit_inc,
//                      sum( sm.tot_inc_ind_plans )         as plan_inc,
//                      sum( ( sm.tot_inc_sys_generated -
//                             ( sm.equip_rental_income +
//                               sm.equip_sales_income ) ) )as sys_gen_inc,
//                      sum( sm.equip_rental_income )       as equip_rental_inc,
//                      sum( sm.equip_sales_income )        as equip_sales_inc
//              from    organization                o,
//                      group_merchant              gm,
//                      monthly_extract_summary     sm
//              where   o.org_num = :orgId and
//                      gm.org_num = o.org_num and
//                      sm.merchant_number = gm.merchant_number and
//                      sm.active_date = :beginDate
//              group by o.org_num, o.org_group, o.org_name        
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                           as org_num, \n                    o.org_group                         as hierarchy_node,\n                    o.org_name                          as org_name,\n                    0                                   as discover_id,\n                    sum( sm.tot_inc_discount )          as disc_inc,\n                    sum( sm.tot_inc_interchange )       as ic_inc,\n                    sum( sm.tot_inc_authorization )     as auth_inc,\n                    sum( sm.tot_inc_capture )           as capture_inc,\n                    sum( sm.tot_inc_debit )             as debit_inc,\n                    sum( sm.tot_inc_ind_plans )         as plan_inc,\n                    sum( ( sm.tot_inc_sys_generated -\n                           ( sm.equip_rental_income +\n                             sm.equip_sales_income ) ) )as sys_gen_inc,\n                    sum( sm.equip_rental_income )       as equip_rental_inc,\n                    sum( sm.equip_sales_income )        as equip_sales_inc\n            from    organization                o,\n                    group_merchant              gm,\n                    monthly_extract_summary     sm\n            where   o.org_num =  :1  and\n                    gm.org_num = o.org_num and\n                    sm.merchant_number = gm.merchant_number and\n                    sm.active_date =  :2 \n            group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchBillingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchBillingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:290^11*/
        }
        else    // ST_CHILD
        {
          /*@lineinfo:generated-code*//*@lineinfo:294^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                           as org_num, 
//                      o.org_group                         as hierarchy_node,
//                      o.org_name                          as org_name,
//                      0                                   as discover_id,
//                      sum( sm.tot_inc_discount )          as disc_inc,
//                      sum( sm.tot_inc_interchange )       as ic_inc,
//                      sum( sm.tot_inc_authorization )     as auth_inc,
//                      sum( sm.tot_inc_capture )           as capture_inc,
//                      sum( sm.tot_inc_debit )             as debit_inc,
//                      sum( sm.tot_inc_ind_plans )         as plan_inc,
//                      sum( ( sm.tot_inc_sys_generated -
//                             ( sm.equip_rental_income +
//                               sm.equip_sales_income ) ) )as sys_gen_inc,
//                      sum( sm.equip_rental_income )       as equip_rental_inc,
//                      sum( sm.equip_sales_income )        as equip_sales_inc
//              from    parent_org                  po,
//                      organization                o,
//                      group_merchant              gm,
//                      monthly_extract_summary     sm
//              where   po.parent_org_num = :orgId and
//                      o.org_num = po.org_num and
//                      gm.org_num = o.org_num and
//                      sm.merchant_number = gm.merchant_number and
//                      sm.active_date = :beginDate
//              group by o.org_num, o.org_group, o.org_name        
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                           as org_num, \n                    o.org_group                         as hierarchy_node,\n                    o.org_name                          as org_name,\n                    0                                   as discover_id,\n                    sum( sm.tot_inc_discount )          as disc_inc,\n                    sum( sm.tot_inc_interchange )       as ic_inc,\n                    sum( sm.tot_inc_authorization )     as auth_inc,\n                    sum( sm.tot_inc_capture )           as capture_inc,\n                    sum( sm.tot_inc_debit )             as debit_inc,\n                    sum( sm.tot_inc_ind_plans )         as plan_inc,\n                    sum( ( sm.tot_inc_sys_generated -\n                           ( sm.equip_rental_income +\n                             sm.equip_sales_income ) ) )as sys_gen_inc,\n                    sum( sm.equip_rental_income )       as equip_rental_inc,\n                    sum( sm.equip_sales_income )        as equip_sales_inc\n            from    parent_org                  po,\n                    organization                o,\n                    group_merchant              gm,\n                    monthly_extract_summary     sm\n            where   po.parent_org_num =  :1  and\n                    o.org_num = po.org_num and\n                    gm.org_num = o.org_num and\n                    sm.merchant_number = gm.merchant_number and\n                    sm.active_date =  :2 \n            group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchBillingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchBillingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:321^11*/
        }          
      }        
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    // if the from date is today, then set the default to rolling previous week
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range
      cal.setTime( ReportDateBegin );
      
      if ( cal.get( Calendar.MONTH ) == Calendar.JANUARY )
      {
        cal.set( Calendar.MONTH, Calendar.DECEMBER );
        cal.set( Calendar.YEAR,  ( cal.get( Calendar.YEAR ) - 1 ) );
      }
      else
      {
        cal.set( Calendar.MONTH, ( cal.get( Calendar.MONTH ) - 1 ) );
      }
      
      // since this report is for one month at a time,
      // make sure that the from date always starts with
      // the first day of the specified month.
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      // set both the begin and end dates
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
    }    
    
    // if the current node is an association,
    // then force the report into details mode
    if ( isOrgAssociation() )
    {
      ReportType = RT_DETAILS;
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/