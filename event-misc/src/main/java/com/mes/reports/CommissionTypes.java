/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CommissionTypes.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 2/16/01 5:22p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

//
// This class allows common vector entries to be shared
// between the report bean (which retrieves the data from 
// the database) and the data bean (which only holds the
// data and does not have any database access).  It was 
// necessary to seperate the data storage from the database
// routines so that the data class could be used inside of
// java stored procedures without the overhead of all the 
// database support classes.
//
public class CommissionTypes
{
  public class AdvanceTrueUpMapEntry
  {
    private int         TypeAdvance;
    private int         TypeTrueUp;
  
    public AdvanceTrueUpMapEntry( int advType, int trueUpType )
    {
      TypeAdvance = advType;
      TypeTrueUp  = trueUpType;
    }
    public int getAdvanceType( )
    {
      return( TypeAdvance );
    }
    public int getTrueUpType( )
    {
      return( TypeTrueUp );
    }
  }
  
  public class DescriptionEntry
  {
    private String      Description;
    private int         Type;
    
    public DescriptionEntry( int type, String desc )
    {
      Description = desc;
      Type        = type;
    }
    
    public String getDescription( )
    {
      return( Description );
    }
    
    public int getType( )
    {
      return( Type );
    }
  }

  // the one and only instance of CommissionTypes within the JVM  
  private static CommissionTypes      TheCommissionTypes = new CommissionTypes();
  
  //
  // Constructor is private so no one can create one
  // Makes this object a guaranteed singleton.
  //
  private CommissionTypes( )
  {
  }
  
  public static CommissionTypes getInstance()
  {
    return( TheCommissionTypes );
  }
}
