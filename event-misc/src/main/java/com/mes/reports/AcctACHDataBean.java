/*@lineinfo:filename=AcctACHDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AcctACHDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-06-01 09:21:14 -0700 (Wed, 01 Jun 2011) $
  Version            : $Revision: 18870 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class AcctACHDataBean extends ReportSQLJBean
{
  public static final int       TT_DEBIT            = 0;
  public static final int       TT_CREDIT           = 1;
  public static final int       TT_COUNT            = 2;
  
  public class DetailRow
  {
    public String           AccountId             = null;
    public double           AchAmount             = 0.0;
    public String           DbaName               = null;
    public String           DdaAccount            = null;
    public Date             PostDate              = null;
    public String           TranRouting           = null;
    public int              TranType              = 0;
    public String           BatchDescriptor       = null;
    
    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      try
      {
        // try to get as long so that any leading zeros are
        // stripped from the account number.
        AccountId = Long.toString( resultSet.getLong("account_id") );
      }
      catch( Exception e )
      {
        // value is not a number, so just treat it as a string
        AccountId = processString(resultSet.getString("account_id"));
      }
      
      AchAmount       = resultSet.getDouble("ach_amount");
      DbaName         = processString(resultSet.getString("dba_name"));
      DdaAccount      = processString(resultSet.getString("dda_account"));
      PostDate        = resultSet.getDate("post_date");
      TranRouting     = processString(resultSet.getString("tran_routing"));
      TranType        = resultSet.getInt("tran_type");
      BatchDescriptor = processString(resultSet.getString("company_entry_description"));
    }
  }
  
  public class SummaryRow
  {
    public int        BatchId             = 0;
    public int        BatchNumber         = 0;
    public String     BatchDescriptor     = null;
    private double[]  ItemAmounts         = new double[TT_COUNT];
    private int[]     ItemCounts          = new int[TT_COUNT];
    public String     LoadFilename        = null;
    
    
    public SummaryRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      LoadFilename            = processString(resultSet.getString("load_filename"));
      BatchNumber             = resultSet.getInt("batch_number");
      BatchId                 = resultSet.getInt("batch_id");
      BatchDescriptor         = processString(resultSet.getString("company_entry_description"));
      
      ItemAmounts[TT_DEBIT]   = resultSet.getDouble("debit_amount");
      ItemCounts [TT_DEBIT]   = resultSet.getInt   ("debit_count");
      ItemAmounts[TT_CREDIT]  = resultSet.getDouble("credit_amount");
      ItemCounts [TT_CREDIT]  = resultSet.getInt   ("credit_count");
    }
    
    public double getAmountCredits()
    {
      return( ItemAmounts[TT_CREDIT] );
    }
    public double getAmountDebits()
    {
      return( ItemAmounts[TT_DEBIT] );
    }
    public double getAmountNet()
    {
      return( ItemAmounts[TT_CREDIT] - ItemAmounts[TT_DEBIT] );
    }
    public int getCountCredits()
    {
      return( ItemCounts[TT_CREDIT] );
    }
    public int getCountDebits()
    {
      return( ItemCounts[TT_DEBIT] );
    }
  }
  
  private   int               BatchId         = 0;

  public AcctACHDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_SUMMARY )
    {
      line.append("\"Filename\",");
      line.append("\"Batch Number\",");
      line.append("\"Credit Cnt\",");
      line.append("\"Credit Amt\",");
      line.append("\"Debit Cnt\",");
      line.append("\"Debit Amt\",");
      line.append("\"Net Amount\"");
    }
    else // RT_DETAILS
    {
      line.append("\"Account Id\",");
      line.append("\"DBA Name\",");
      line.append("\"Post Date\",");
      line.append("\"DDA Account\",");
      line.append("\"Tran Routing\",");
      line.append("\"Tran Code\",");
      line.append("\"Amount\"");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);
  
    if ( ReportType == RT_SUMMARY )
    {
      SummaryRow  record    = (SummaryRow)obj;
    
      line.append( "\"" );
      line.append( record.LoadFilename );
      line.append( "\"," );
      line.append( record.BatchNumber );
      line.append( "," );
      line.append( record.getCountCredits() );
      line.append( "," );
      line.append( record.getAmountCredits() );
      line.append( "," );
      line.append( record.getCountDebits() );
      line.append( "," );
      line.append( record.getAmountDebits() );
      line.append( "," );
      line.append( record.getAmountNet() );
    }
    else      // RT_DETAILS
    {
      DetailRow       record    = (DetailRow)obj;
    
      line.append( "\"" );
      line.append( record.AccountId );
      line.append( "\",\"" );
      line.append( record.DbaName );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate( record.PostDate, "MM/dd/yyyy" ) );
      line.append( ",\"" );
      line.append( record.DdaAccount );
      line.append( "\",\"" );
      line.append( record.TranRouting );
      line.append( "\"," );
      line.append( record.TranType );
      line.append( "," );
      line.append( record.AchAmount );
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    if ( ReportType == RT_SUMMARY )
    {
      filename.append("_ach_file_summary_");
    }
    else    // RT_DETAILS
    {
      filename.append("_ach_file_details_");
    }      
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public int getReportBatchId( )
  {
    return( BatchId );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( ReportType == RT_SUMMARY )
      {
        loadSummaryData( orgId, beginDate, endDate );
      }
      else    // RT_DETAILS
      {
        loadDetailData( orgId, beginDate, endDate );
      }
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
    }
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:293^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ach.dfi_account_number_dda_number     as dda_account, 
//                  ach.transaction_code                  as tran_type,
//                  ach.internal_account_number           as account_id, 
//                  ( ach.amount_of_transaction *
//                    decode(tc.debit_credit_indicator,
//                           'D',-1,
//                           1) )                         as ach_amount,
//                  ach.receiving_dfi_id_ttttaaaac        as tran_routing,
//                  ach.post_date_option                  as post_date,
//                  ab.company_entry_description          as company_entry_description,
//                  ach.individual_name                   as dba_name
//          from    ach_detail            ach,
//                  ach_batch             ab,
//                  ach_detail_tran_code  tc
//          where   ach.batch_id = :BatchId and
//                  tc.ach_detail_trans_code(+) = ach.transaction_code
//                  and ach.load_filename = ab.load_filename
//                  and ach.batch_id = ab.batch_id
//          order by ach.batch_detail_id      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ach.dfi_account_number_dda_number     as dda_account, \n                ach.transaction_code                  as tran_type,\n                ach.internal_account_number           as account_id, \n                ( ach.amount_of_transaction *\n                  decode(tc.debit_credit_indicator,\n                         'D',-1,\n                         1) )                         as ach_amount,\n                ach.receiving_dfi_id_ttttaaaac        as tran_routing,\n                ach.post_date_option                  as post_date,\n                ab.company_entry_description          as company_entry_description,\n                ach.individual_name                   as dba_name\n        from    ach_detail            ach,\n                ach_batch             ab,\n                ach_detail_tran_code  tc\n        where   ach.batch_id =  :1  and\n                tc.ach_detail_trans_code(+) = ach.transaction_code\n                and ach.load_filename = ab.load_filename\n                and ach.batch_id = ab.batch_id\n        order by ach.batch_detail_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AcctACHDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,BatchId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AcctACHDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:314^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        ReportRows.add( new DetailRow( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:343^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ach.load_filename                             as load_filename, 
//                  ach.batch_number                              as batch_number, 
//                  ab.company_entry_description                  as company_entry_description,
//                  ach.batch_id                                  as batch_id,
//                  sum( decode(adtc.debit_credit_indicator,
//                              'C', 1, 0) )                      as credit_count,
//                  sum( decode(adtc.debit_credit_indicator,
//                              'C', ach.amount_of_transaction, 
//                                0) )                            as credit_amount,                     
//                  sum( decode(adtc.debit_credit_indicator,
//                              'D', 1, 0) )                      as debit_count,                     
//                  sum( decode(adtc.debit_credit_indicator,
//                              'D', ach.amount_of_transaction) ) as debit_amount
//          from    ach_detail      ach,
//                  ach_batch       ab,
//                  ach_detail_tran_code adtc
//          where   ach.post_date_option between :beginDate and :endDate
//                  and ach.load_filename = ab.load_filename
//                  and ach.batch_id = ab.batch_id
//                  and ach.transaction_code = adtc.ach_detail_trans_code
//          group by ach.load_filename, ach.batch_number, ach.batch_id, ab.company_entry_description
//          order by ach.load_filename, ach.batch_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ach.load_filename                             as load_filename, \n                ach.batch_number                              as batch_number, \n                ab.company_entry_description                  as company_entry_description,\n                ach.batch_id                                  as batch_id,\n                sum( decode(adtc.debit_credit_indicator,\n                            'C', 1, 0) )                      as credit_count,\n                sum( decode(adtc.debit_credit_indicator,\n                            'C', ach.amount_of_transaction, \n                              0) )                            as credit_amount,                     \n                sum( decode(adtc.debit_credit_indicator,\n                            'D', 1, 0) )                      as debit_count,                     \n                sum( decode(adtc.debit_credit_indicator,\n                            'D', ach.amount_of_transaction) ) as debit_amount\n        from    ach_detail      ach,\n                ach_batch       ab,\n                ach_detail_tran_code adtc\n        where   ach.post_date_option between  :1  and  :2 \n                and ach.load_filename = ab.load_filename\n                and ach.batch_id = ab.batch_id\n                and ach.transaction_code = adtc.ach_detail_trans_code\n        group by ach.load_filename, ach.batch_number, ach.batch_id, ab.company_entry_description\n        order by ach.load_filename, ach.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AcctACHDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AcctACHDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:367^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new SummaryRow( resultSet ) );
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties( HttpServletRequest request )
  {
    super.setProperties(request);
    
    BatchId     = HttpHelper.getInt(request,"batchId",0);
    ReportType  = ((BatchId == 0) ? RT_SUMMARY : RT_DETAILS);
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/