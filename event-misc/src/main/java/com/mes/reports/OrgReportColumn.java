/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/OrgReportColumn.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 10/31/00 8:15a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

//C+            OrgReportColumn
public class    OrgReportColumn
{
  protected String            ColumnDataAlignment     = ALIGN_DATA_DEFAULT;
  protected String            ColumnName              = "";
  protected String            ColumnNameAlignment     = ALIGN_NAME_DEFAULT;
  protected int               SortKey                 = -1;
  
  // read only values
  protected static final String   ALIGN_NAME_DEFAULT  = "center";
  protected static final String   ALIGN_DATA_DEFAULT  = "right";
  
  public OrgReportColumn( String columnName )
  {
    init( columnName, -1, ALIGN_NAME_DEFAULT, ALIGN_DATA_DEFAULT );
  }
  
  public OrgReportColumn( String columnName, int sortKey )
  {
    init( columnName, sortKey, ALIGN_NAME_DEFAULT, ALIGN_DATA_DEFAULT );
  }
  
  public OrgReportColumn( String columnName, int sortKey, String nameAlign, String dataAlign )
  {
    init( columnName, sortKey, nameAlign, dataAlign );
  }
  
  public String getColumnDataAlignment( )
  {
    return( ColumnDataAlignment );
  }
  
  public String getColumnName( )
  {
    return( ColumnName );
  }
  
  public String getColumnNameAlignment( )
  {
    return( ColumnNameAlignment );
  }
  
  public int getSortKey( )
  {
    return( SortKey );
  }
  
  public void init( String columnName, int sortKey, String nameAlign, String dataAlign )
  {
    setColumnDataAlignment( dataAlign );
    setColumnName( columnName );
    setColumnNameAlignment( nameAlign );
    setSortKey( sortKey );
  }
  
  public boolean isColumnSortable( )
  {
    return( ( SortKey == -1 ) ? false : true );
  }
  
  public void setColumnDataAlignment( String alignment )
  {
    ColumnDataAlignment = alignment;
  }
  
  public void setColumnName( String colName )
  {
    ColumnName = colName;  
  }
  
  public void setColumnNameAlignment( String alignment )
  {
    ColumnNameAlignment = alignment;
  }
  
  public void setSortKey( int sortKey )
  {
    SortKey = sortKey;
  }
}
//C-            OrgReportColumn



