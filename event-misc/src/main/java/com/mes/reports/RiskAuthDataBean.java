/*@lineinfo:filename=RiskAuthDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskAuthDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 5/13/03 4:13p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.config.MesDefaults;
import com.mes.forms.NumberField;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class RiskAuthDataBean extends RiskBaseDataBean
{
  public static final int     E_CAPTURE_AUTH_RATIO        = 0;
  public static final int     E_DECL_AUTH_RATIO           = 1;
  public static final int     E_AVS_AUTH_RATIO            = 2;
  public static final int     E_COUNT                     = 3;  // must be last
  
  public static final int     NUMERATOR                   = 0;
  public static final int     DENOMINATOR                 = 1;
  
  public class RiskAuthRecord extends RiskRecordBase
  {
    public double           ApprovalAmount    = 0.0;
    public int              ApprovalCount     = 0;
    public double           AuthAmount        = 0.0;
    public int              AuthCount         = 0;
    public double           AVSAmount         = 0.0;
    public int              AVSCount          = 0;
    public double           CaptureAmount     = 0.0;
    public int              CaptureCount      = 0;
    public double           DeclineAmount     = 0.0;
    public int              DeclineCount      = 0;
    private boolean[]       ExceptionFlags    = null;
    private boolean         MotoMerchant      = false;
  
    public RiskAuthRecord( ResultSet resultSet, boolean[] flags )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      String motoValue = resultSet.getString("moto");
      
      if ((motoValue != null) && motoValue.equals("Y"))
      {
        MotoMerchant = true;
      }
             
      // extract the counts from the result set.             
      AuthCount       = resultSet.getInt("auth_total_count");
      AuthAmount      = resultSet.getDouble("auth_total_amount");
      ApprovalCount   = resultSet.getInt("auth_appr_count");
      ApprovalAmount  = resultSet.getDouble("auth_appr_amount");
      DeclineCount    = resultSet.getInt("auth_decl_count");
      DeclineAmount   = resultSet.getDouble("auth_decl_amount");
      CaptureCount    = resultSet.getInt("sales_count");
      CaptureAmount   = resultSet.getDouble("sales_amount");
      AVSCount        = resultSet.getInt("avs_count");
      AVSAmount       = resultSet.getDouble("avs_amount");
      
      ExceptionFlags  = flags;
    }
    
    public double getAmount( int flag, int numDenom )
    {
      double      retVal      = 0.0;
      
      switch( flag )
      {
        case E_CAPTURE_AUTH_RATIO:
          retVal = ((numDenom == NUMERATOR) ? CaptureAmount : AuthAmount);
          break;
          
        case E_DECL_AUTH_RATIO:
          retVal = ((numDenom == NUMERATOR) ? DeclineAmount : AuthAmount);
          break;
          
        case E_AVS_AUTH_RATIO:
          retVal = ((numDenom == NUMERATOR) ? AVSAmount : AuthAmount);
          break;
          
        default:
          break;
      }
      return( retVal );
    }
    
    private double getAuthCaptureRatio()
    {
      double        retVal      = 0.0;
      
      if ( AuthCount == 0 )
      {
        if ( CaptureCount != 0 )
        {
          retVal = 1.0;
        }
      }
      else
      {
        retVal = ((double)CaptureCount/(double)AuthCount);
      }
      return( retVal );
    }
    
    private double getAvsAuthRatio()
    {
      double        retVal      = 0.0;
      
      if ( AuthCount != 0 )
      {
        retVal = (double)( (double)AVSCount / (double)AuthCount );
      }
      return( retVal );
    }
    
    private double getDeclAuthRatio()
    {
      double        retVal      = 0.0;
      
      if ( AuthCount == 0 )
      {
        if ( DeclineCount != 0 )
        {
          retVal = 1.0;
        }
      }
      else
      {
        retVal = (double)( (double)DeclineCount / (double)AuthCount );
      }
      return( retVal );
    }
    
    public int getCount( int flag, int numDenom )
    {
      int         retVal      = 0;
      
      switch( flag )
      {
        case E_CAPTURE_AUTH_RATIO:
          retVal = ((numDenom == NUMERATOR) ? CaptureCount : AuthCount);
          break;
          
        case E_DECL_AUTH_RATIO:
          retVal = ((numDenom == NUMERATOR) ? DeclineCount : AuthCount);
          break;
          
        case E_AVS_AUTH_RATIO:
          retVal = ((numDenom == NUMERATOR) ? AVSCount : AuthCount);
          break;
          
        default:
          break;
      }
      return( retVal );
    }
    
    public double getRatio( int flag )
    {
      double      retVal    = 0.0;
      
      switch( flag )
      {
        case E_CAPTURE_AUTH_RATIO:
          retVal = getAuthCaptureRatio();
          break;
          
        case E_DECL_AUTH_RATIO:
          retVal = getDeclAuthRatio();
          break;
          
        case E_AVS_AUTH_RATIO:
          retVal = getAvsAuthRatio();
          break;
          
        default:
          break;
      }
      return( retVal );
    }
    
    public boolean isException( int flag )
    {
      boolean       retVal      = false;
      
      try
      {
        retVal = ExceptionFlags[flag];
      }
      catch( Exception e )
      {
      }
      return(retVal);
    }
    
    public boolean isMotoMerchant()
    {
      return( MotoMerchant );
    }
  }
  
  public RiskAuthDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    switch( ReportStyle )
    {
      case RISK_STYLE_ITEM_RATIO:
        line.append("\"Merchant Id\",");
        line.append("\"DBA Name\",");
        line.append("\"Auth Count\",");
        line.append("\"Auth Amount\",");
        line.append("\"Capture Count\",");
        line.append("\"Capture Amount\",");
        line.append("\"Auth/Capture Ratio\",");
        line.append("\"Approval Count\",");
        line.append("\"Approval Amount\",");
        line.append("\"Decline Count\",");
        line.append("\"Decline Amount\",");
        line.append("\"Decl/Appr Ratio\",");
        line.append("\"AVS Count\",");
        line.append("\"AVS Amount\",");
        line.append("\"AVS/Auth Ratio\"");
        break;
        
      default:
        break;
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RiskAuthRecord     detail     = (RiskAuthRecord)obj;

    line.setLength(0);

    switch( ReportStyle )
    {
      case RISK_STYLE_ITEM_RATIO:
        line.append( encodeHierarchyNode(detail.MerchantId) );
        line.append( ",\"" );
        line.append( detail.DbaName );
        line.append( "\"," );
        line.append( detail.AuthCount );
        line.append( "," );
        line.append( detail.AuthAmount );
        line.append( "," );
        line.append( detail.CaptureCount );
        line.append( "," );
        line.append( detail.CaptureAmount );
        line.append( "," );
        line.append( detail.getAuthCaptureRatio() );
        line.append( "," );
        line.append( detail.ApprovalCount );
        line.append( "," );
        line.append( detail.ApprovalAmount );
        line.append( "," );
        line.append( detail.DeclineCount );
        line.append( "," );
        line.append( detail.DeclineAmount );
        line.append( "," );
        line.append( detail.getDeclAuthRatio() );
        line.append( "," );
        break;
        
      default:
        break;
    }
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );

    switch( ReportStyle )
    {
      case RISK_STYLE_ITEM_RATIO:
        filename.append("_auth_risk_");
        break;
        
      default:
        break;
    }
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"MMddyyyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"MMddyyyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public String getExceptionDesc( int flag )
  {
    String        retVal      = "Invalid";
    
    switch( flag )
    {
      case E_CAPTURE_AUTH_RATIO:
        retVal = "Capture/Authorization";
        break;
        
      case E_DECL_AUTH_RATIO:
        retVal = "Decline/Authorization";
        break;
        
      case E_AVS_AUTH_RATIO:
        retVal = "AVS/Authorization";
        break;
        
      default:
        break;
    }
    return( retVal );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    if ( fileFormat == FF_CSV )
    {
      switch( ReportStyle )
      {
        case RISK_STYLE_ITEM_RATIO:
          retVal = true;
          break;
          
        default:
          break;
      }
    }
    return(retVal);
  }
  
  public void loadData( int reportStyle, long orgId, Date beginDate, Date endDate, String[] args )
  {
    double                        amountThreshold           = 0;
    double                        apprCount                 = 0.0;
    double                        authCaptureRatio          = 0;
    boolean                       authCaptureRatioException = false;
    double                        authCount                 = 0.0;
    double                        avsAuthRatio              = 0;
    boolean                       avsAuthRatioException     = false;
    double                        avsCount                  = 0.0;
    double                        declAuthRatio             = 0;
    boolean                       declAuthRatioException    = false;
    double                        declCount                 = 0.0;
    ResultSetIterator             it                        = null;
    int                           minCount                  = 0;
    long                          nodeId                    = orgIdToHierarchyNode(orgId);
    ResultSet                     resultSet                 = null;
    double                        salesCount                = 0.0;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( reportStyle == RISK_STYLE_ITEM_RATIO )
      {
        try
        { 
          authCaptureRatio = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          try
          {
            authCaptureRatio = MesDefaults.getDouble( MesDefaults.DK_RISK_CAPT_AUTH_RATIO );
          }
          catch( Exception ee )
          {
            authCaptureRatio = 0.0;
          }            
        }
        try
        { 
          declAuthRatio = Double.parseDouble( args[1] ); 
        } 
        catch( Exception e ) 
        {
          try
          {
            declAuthRatio = MesDefaults.getDouble( MesDefaults.DK_RISK_DECL_APPR_RATIO );
          }
          catch( Exception ee )
          {
            declAuthRatio = 0.0;
          }            
        }
        try
        { 
          avsAuthRatio = Double.parseDouble( args[2] ); 
        } 
        catch( Exception e ) 
        {
          try
          {
            avsAuthRatio = MesDefaults.getDouble( MesDefaults.DK_RISK_AVS_AUTH_RATIO );
          }
          catch( Exception ee )
          {
            avsAuthRatio = 0.0;
          }            
        }
        try
        { 
          minCount = Integer.parseInt( args[3] ); 
        } 
        catch( Exception e ) 
        {
          try
          {
            minCount = MesDefaults.getInt( MesDefaults.DK_RISK_AUTH_MIN_COUNT );
          }
          catch( Exception ee )
          {
            minCount = 50;
          }
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:456^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                  as merchant_number,
//                    mf.sic_code                         as sic_code,
//                    mf.dba_name                         as dba_name,
//                    (mf.bank_number || mf.dmagent)      as assoc_number,
//                    'N'                                 as moto,
//                    sum(sm.vmc_auth_total_count)        as auth_total_count,
//                    sum(sm.vmc_auth_total_amount)       as auth_total_amount,
//                    sum(sm.vmc_auth_approved_count)     as auth_appr_count,
//                    sum(sm.vmc_auth_approved_amount)    as auth_appr_amount,
//                    sum(sm.vmc_auth_decline_count)      as auth_decl_count,
//                    sum(sm.vmc_auth_decline_amount)     as auth_decl_amount,
//                    sum(sm.VMC_SALES_COUNT)             as sales_count,
//                    sum(sm.VMC_SALES_AMOUNT)            as sales_amount,
//                    sum(sm.VMC_AUTH_AVS_COUNT)          as avs_count,
//                    sum(sm.VMC_AUTH_AVS_AMOUNT)         as avs_amount
//            from    organization          o,
//                    group_merchant        gm,
//                    group_rep_merchant    grm,
//                    tc33_risk_summary     sm,
//                    mif                   mf
//            where   o.org_group = :nodeId and
//                    gm.org_num  = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    sm.merchant_number = gm.merchant_number and
//                    sm.transaction_date between :beginDate and :endDate and
//                    mf.merchant_number = sm.merchant_number
//            group by mf.merchant_number, mf.sic_code, 
//                    (mf.bank_number || mf.dmagent), mf.dba_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                  as merchant_number,\n                  mf.sic_code                         as sic_code,\n                  mf.dba_name                         as dba_name,\n                  (mf.bank_number || mf.dmagent)      as assoc_number,\n                  'N'                                 as moto,\n                  sum(sm.vmc_auth_total_count)        as auth_total_count,\n                  sum(sm.vmc_auth_total_amount)       as auth_total_amount,\n                  sum(sm.vmc_auth_approved_count)     as auth_appr_count,\n                  sum(sm.vmc_auth_approved_amount)    as auth_appr_amount,\n                  sum(sm.vmc_auth_decline_count)      as auth_decl_count,\n                  sum(sm.vmc_auth_decline_amount)     as auth_decl_amount,\n                  sum(sm.VMC_SALES_COUNT)             as sales_count,\n                  sum(sm.VMC_SALES_AMOUNT)            as sales_amount,\n                  sum(sm.VMC_AUTH_AVS_COUNT)          as avs_count,\n                  sum(sm.VMC_AUTH_AVS_AMOUNT)         as avs_amount\n          from    organization          o,\n                  group_merchant        gm,\n                  group_rep_merchant    grm,\n                  tc33_risk_summary     sm,\n                  mif                   mf\n          where   o.org_group =  :1  and\n                  gm.org_num  = o.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  sm.merchant_number = gm.merchant_number and\n                  sm.transaction_date between  :4  and  :5  and\n                  mf.merchant_number = sm.merchant_number\n          group by mf.merchant_number, mf.sic_code, \n                  (mf.bank_number || mf.dmagent), mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.RiskAuthDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.RiskAuthDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:488^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          authCount   = (double)resultSet.getInt("auth_total_count");
          if( authCount < minCount )
          {
            continue;
          }
          
          // treat these integers as doubles
          apprCount   = (double)resultSet.getInt("auth_appr_count");
          declCount   = (double)resultSet.getInt("auth_decl_count");
          salesCount  = (double)resultSet.getInt("sales_count");
          avsCount    = (double)resultSet.getInt("avs_count");
          
          // if the approval decline ratio was set to 0
          // then disable the check for exception.
          if ( declAuthRatio != 0 )
          {
            if ( authCount == 0 )
            {
              declAuthRatioException = ((declCount == 0) ? false : true);
            }
            else
            {
              declAuthRatioException = 
                ((((declCount/authCount)*100) < declAuthRatio) ? false : true);
            }
          }            
          
          if ( authCaptureRatio != 0 )
          {
            authCaptureRatioException = 
              ((((salesCount/authCount)*100) < authCaptureRatio) ? true : false);
          }              
            
          if ( ( resultSet.getString("moto").equals("Y") ) &&
               ( avsAuthRatio != 0 ) )
          {
            avsAuthRatioException = 
              ((((avsCount/authCount)*100) < avsAuthRatio) ? true : false);
          }
          
          if ( avsAuthRatioException ||
               declAuthRatioException ||
               authCaptureRatioException )
          {
            ReportRows.addElement( 
              new RiskAuthRecord( resultSet,
                                   new boolean[] 
                                   {
                                     authCaptureRatioException,
                                     declAuthRatioException,
                                     authCaptureRatioException 
                                   } ) );
          }               
        }
        it.close();   // this will also close the resultSet
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    Calendar        cal           = Calendar.getInstance();
    
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      // setup the default date range
      cal.setTime( ReportDateEnd );
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }
  }
  
  protected void setReportStyle( int reportStyle )
  {
    super.setReportStyle(reportStyle);   // set in base class

    // there is only one type of auth report at present    
    ReportTitle = "Authorization Risk Exceptions";
    
    // setup the query param labels
    QueryParamLabels = 
      new String[]
        { "Capture/Auth Ratio Less Than: %<br><span class=\"smalltext\">(0=disable)</span>",
          "Decline/Auth Ratio Exceeds: %<br><span class=\"smalltext\">(0=disable)</span>",
          "AVS/Auth (MOTO Only) Ratio Less Than : %<br><span class=\"smalltext\">(0=disable)</span>",
          "Minimum Number Of Items: " };
          
    // create the input fields
    fields.add( new NumberField("queryParam0",6,6,false,0) );
    fields.add( new NumberField("queryParam1",6,6,false,0) );
    fields.add( new NumberField("queryParam2",6,6,false,0) );
    fields.add( new NumberField("queryParam3",6,6,false,0) );
    
    // set the default values
    fields.getField("queryParam0").setData("10");
    fields.getField("queryParam1").setData("10");
    fields.getField("queryParam2").setData("10");
    fields.getField("queryParam3").setData("50");
  }
  
  public boolean showContactInfo( )
  {
    return(true);
  }
}/*@lineinfo:generated-code*/