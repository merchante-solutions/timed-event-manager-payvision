/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/mesOrgSummaryEntry.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-11-04 12:18:38 -0700 (Thu, 04 Nov 2010) $
  Version            : $Revision: 18071 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.support.SyncLog;

//C+    MesOrgSummaryEntry
public class   MesOrgSummaryEntry implements Comparable
{
  // static sort fields values (must be > 0)
  static public   final int       SB_NAME                     = 1;
  static public   final int       SB_NODE                     = 2;
  static public   final int       SB_USER                     = 3;

  // user defined report indexes
  static public   final int       SB_AMOUNT                   = SB_USER + 0;
  static public   final int       SB_AMOUNT_2                 = SB_USER + 1;
  static public   final int       SB_COUNT                    = SB_USER + 2;
  static public   final int       SB_COUNT_2                  = SB_USER + 3;
  static public   final int       SB_TOTAL_AMOUNT             = SB_USER + 4;
  static public   final int       SB_TOTAL_COUNT              = SB_USER + 5;
  static public   final int       SB_ENTRY_DATE               = SB_USER + 6;
  
  // statement summary sort types
  public static final int         SB_MS_VMC_SALES             = SB_USER + 7;
  public static final int         SB_MS_VMC_CREDITS           = SB_USER + 8;
  public static final int         SB_MS_OTHER_SALES           = SB_USER + 9;
  public static final int         SB_MS_OTHER_CREDITS         = SB_USER + 10;
  public static final int         SB_MS_DISCOUNT              = SB_USER + 11;
  public static final int         SB_MS_FEES                  = SB_USER + 12;
  public static final int         SB_MS_NET                   = SB_USER + 13;
  
  // certegy statement summary sort types
  public static final int         SB_MSC_VMC_NET              = SB_USER + 14;
  public static final int         SB_MSC_OTHER_NET            = SB_USER + 15;
  public static final int         SB_MSC_DISCOUNT             = SB_USER + 16;
  public static final int         SB_MSC_FEES                 = SB_USER + 17;
  public static final int         SB_MSC_NET                  = SB_USER + 18;
  
  // statement details sort by types (which report column to sort by)
  public static final int         SB_MS_DDA                   = SB_USER + 14;
  public static final int         SB_MS_TR_NUM                = SB_USER + 15;

  // number of amounts and counts to allocate.  
  // NOTE: must be sufficient to handle the largest index specified below.
  static public   final int       NUM_AMOUNTS                     = 7;
  static public   final int       NUM_COUNTS                      = NUM_AMOUNTS;
  
  // reconcile indexes
  static public   final int       TRAN_INDEX                      = 0;
  static public   final int       ADJ_INDEX                       = 1;
  static public   final int       ACH_INDEX                       = 2;
  
  // statement indexes
  static public   final int       MS_VMC_SALES_INDEX              = 0;
  static public   final int       MS_VMC_CREDITS_INDEX            = 1;
  static public   final int       MS_OTHER_SALES_INDEX            = 2;
  static public   final int       MS_OTHER_CREDITS_INDEX          = 3;
  static public   final int       MS_DISCOUNT_INDEX               = 4;
  static public   final int       MS_FEES_INDEX                   = 5;
  static public   final int       MS_NET_INDEX                    = 6;
  
  // statement indexes
  static public   final int       MSC_VMC_NET_INDEX               = 0;
  static public   final int       MSC_OTHER_NET_INDEX             = 1;
  static public   final int       MSC_DISCOUNT_INDEX              = 2;
  static public   final int       MSC_FEES_INDEX                  = 3;
  static public   final int       MSC_NET_INDEX                   = 4;
  
  // transaction indexes
  static public   final int       BANK_INDEX                      = 0;
  static public   final int       NONBANK_INDEX                   = 1;
  
  // auth log summary indexes
  static public   final int       VMC_AUTH_APPR_INDEX             = 0;
  static public   final int       VMC_AUTH_DECL_INDEX             = 1;
  static public   final int       VMC_AUTH_TOTAL_INDEX            = 2;
  
  // default index 
  static public   final int       DEFAULT_INDEX                   = 0;
  
  // total index count
  static public   final int       TOTAL_INDEX                     = NUM_AMOUNTS;
  
  static private final int[][]  SortKeyToIndexMap = 
  {
      // sort by mnemonic           index mnemonic
    { SB_AMOUNT               ,   BANK_INDEX                },
    { SB_AMOUNT_2             ,   NONBANK_INDEX             },
    { SB_COUNT                ,   BANK_INDEX                },
    { SB_COUNT_2              ,   NONBANK_INDEX             },
    { SB_TOTAL_AMOUNT         ,   TOTAL_INDEX               },
    { SB_TOTAL_COUNT          ,   TOTAL_INDEX               },
    { SB_MS_VMC_SALES         ,   MS_VMC_SALES_INDEX        },
    { SB_MS_VMC_CREDITS       ,   MS_VMC_CREDITS_INDEX      },
    { SB_MS_OTHER_SALES       ,   MS_OTHER_SALES_INDEX      },
    { SB_MS_OTHER_CREDITS     ,   MS_OTHER_CREDITS_INDEX    },
    { SB_MS_DISCOUNT          ,   MS_DISCOUNT_INDEX         },
    { SB_MS_FEES              ,   MS_FEES_INDEX             },
    { SB_MS_NET               ,   MS_NET_INDEX              },
    { SB_MSC_VMC_NET          ,   MSC_VMC_NET_INDEX         },
    { SB_MSC_OTHER_NET        ,   MSC_OTHER_NET_INDEX       },
    { SB_MSC_DISCOUNT         ,   MSC_DISCOUNT_INDEX        },
    { SB_MSC_FEES             ,   MSC_FEES_INDEX            },
    { SB_MSC_NET              ,   MSC_NET_INDEX             },
  };
  
  // instance variables
  double      Amount[]                    = new double[ NUM_AMOUNTS ];
  int         Count[]                     = new int   [ NUM_COUNTS  ];
  int         District                    = 0;  // ReportSQLJBean.DISTRICT_NONE
  Date        EntryDate                   = null;
  long        HierarchyNode               = 0L;
  long        OrgId                       = 0L;
  String      OrgName                     = "Unknown";
  SortByType  SortOrder                   = null;
  
  String      OrgAssocId                  = "";   //@ obsolete
  long        OrgMerchantId               = 0;    //@ obsolete
  
  public MesOrgSummaryEntry( )
  {
    initialize(null);
  }
  
  public MesOrgSummaryEntry( ResultSet resultSet )
  {
    initialize(resultSet);
  }

  public MesOrgSummaryEntry( ResultSet resultSet, SortByType sortOrder )
  {
    initialize(resultSet);
    setSortOrder(sortOrder);
  }
  
  public void addData( ResultSet resultSet )
  {
    try
    {
      // default queries should have the item count/amount
      Count [DEFAULT_INDEX] += resultSet.getInt("item_count");
      Amount[DEFAULT_INDEX] += resultSet.getDouble("item_amount");
    }
    catch( Exception e )
    {
    }      
  
    try
    {  
      Count [NONBANK_INDEX] += resultSet.getInt("non_bank_count");
      Amount[NONBANK_INDEX] += resultSet.getDouble("non_bank_amount");
    }
    catch( Exception e )
    {
      // ignore, only tran summary has these fields
    }          
  
    try
    {  
      Count [ACH_INDEX]     += resultSet.getInt("ach_count");
      Amount[ACH_INDEX]     += resultSet.getDouble("ach_amount");
    }
    catch( Exception e )
    {
      // ignore, only reconcile has these fields
    }          
    
    try
    {  
      Count [ADJ_INDEX]     += resultSet.getInt("adj_count");
      Amount[ADJ_INDEX]     += resultSet.getDouble("adj_amount");
    }
    catch( Exception e )
    {
      // ignore, only reconcile has these fields
    }          
    
    try
    {
      boolean isCertegy = false;
      try
      {
        isCertegy = (resultSet.getInt("bank_num") == 1000);
      }
      catch( Exception e )
      {
        // ignore, certegy flag not present for some hierarchy types
      }
      
      if (isCertegy)
      {
        Amount[MSC_VMC_NET_INDEX]       += resultSet.getDouble("vmc_sales_amount");
        Amount[MSC_OTHER_NET_INDEX]     += resultSet.getDouble("other_sales_amount");
        Amount[MSC_DISCOUNT_INDEX]      += resultSet.getDouble("discount");
        Amount[MSC_FEES_INDEX]          += resultSet.getDouble("fees");
        Amount[MSC_NET_INDEX]           += resultSet.getDouble("total");
      }
      else
      {
        Amount[MS_VMC_SALES_INDEX]      += resultSet.getDouble("vmc_sales_amount");
        Amount[MS_VMC_CREDITS_INDEX]    += resultSet.getDouble("vmc_credits_amount");
        Amount[MS_OTHER_SALES_INDEX]    += resultSet.getDouble("other_sales_amount");
        Amount[MS_OTHER_CREDITS_INDEX]  += resultSet.getDouble("other_credits_amount");
        Amount[MS_DISCOUNT_INDEX]       += resultSet.getDouble("discount");
        Amount[MS_FEES_INDEX]           += resultSet.getDouble("fees");
        Amount[MS_NET_INDEX]            += resultSet.getDouble("total");
      }
    }
    catch( Exception e )
    {
      // ignore, only statements have these columns
    }          
    
    try
    {  
      Count [VMC_AUTH_APPR_INDEX]   += resultSet.getInt("auth_appr_count");
      Amount[VMC_AUTH_APPR_INDEX]   += resultSet.getDouble("auth_appr_amount");
      Count [VMC_AUTH_DECL_INDEX]   += resultSet.getInt("auth_decl_count");
      Amount[VMC_AUTH_DECL_INDEX]   += resultSet.getDouble("auth_decl_amount");
      Count [VMC_AUTH_TOTAL_INDEX]  += resultSet.getInt("auth_total_count");
      Amount[VMC_AUTH_TOTAL_INDEX]  += resultSet.getDouble("auth_total_amount");
    }
    catch( Exception e )
    {
      // ignore, only auth reports have these columns
    }          
  }
  
  public int compareTo( Object object )
  {
    double              amountDelta   = 0.0;
    MesOrgSummaryEntry  entry         = (MesOrgSummaryEntry) object;
    int                 retVal        = 0;
    int                 sortIndex     = 0;
    
    if ( SortOrder != null )
    {
    
      switch( SortOrder.getSortBy() )
      {
        case SB_COUNT:
        case SB_COUNT_2:
        case SB_TOTAL_COUNT:
          sortIndex = getSortIndex( SortOrder.getSortBy() );
          if ( ( retVal = ( getCount( sortIndex ) - entry.getCount( sortIndex ) ) ) == 0 )
          {
            if ( ( retVal = getOrgName().compareTo( entry.getOrgName() ) ) == 0 )
            {
              if ( ( retVal = (int)(getHierarchyNode() - entry.getHierarchyNode()) ) == 0 )
              {
                retVal = (int) ( getOrgId() - entry.getOrgId() );
              }                
            }
          }
          break;

        case SB_AMOUNT:
        case SB_AMOUNT_2:
        case SB_TOTAL_AMOUNT:
        case SB_MS_VMC_SALES:
        case SB_MS_VMC_CREDITS:
        case SB_MS_OTHER_SALES:
        case SB_MS_OTHER_CREDITS:
        case SB_MS_DISCOUNT:
        case SB_MS_FEES:
        case SB_MS_NET:
        case SB_MSC_VMC_NET:
        case SB_MSC_OTHER_NET:
        case SB_MSC_DISCOUNT:
        case SB_MSC_FEES:
        case SB_MSC_NET:
          sortIndex = getSortIndex( SortOrder.getSortBy() );
          amountDelta = ( getAmount( sortIndex ) - entry.getAmount( sortIndex ) );
          if ( amountDelta > 0.0 )
          {
            retVal = 1;
          }
          else if ( amountDelta < 0.0 )
          {
            retVal = -1;
          }
          else
          {
            if ( ( retVal = getOrgName().compareTo( entry.getOrgName() ) ) == 0 )
            {
              if ( ( retVal = (int)(getHierarchyNode() - entry.getHierarchyNode()) ) == 0 )
              {
                retVal = (int) ( getOrgId() - entry.getOrgId() );
              }                
            }
          }
          break;
        
        case SB_ENTRY_DATE:
          if ( ( retVal = getEntryDate().compareTo(entry.getEntryDate()) ) == 0 )
          {
            if ( ( retVal = (int)(getHierarchyNode() - entry.getHierarchyNode()) ) == 0 )
            {
              if ( ( retVal = entry.getOrgName().compareTo( getOrgName() ) ) == 0 )
              {
                retVal = (int) ( getOrgId() - entry.getOrgId() );
              }
            }
          }
          break;
        
        case SB_NODE:
          if ( ( retVal = (int)(getHierarchyNode() - entry.getHierarchyNode()) ) == 0 )
          {
            if ( ( retVal = entry.getOrgName().compareTo( getOrgName() ) ) == 0 )
            {
              retVal = (int) ( getOrgId() - entry.getOrgId() );
            }
          }
          break;      

  //      case SB_NAME:
        default:
          if ( ( retVal = getOrgName().compareTo( entry.getOrgName() ) ) == 0 )
          {
            if ( ( retVal = (int)(getHierarchyNode() - entry.getHierarchyNode()) ) == 0 )
            {
              retVal = (int) ( getOrgId() - entry.getOrgId() );
            }              
          }
          break;
      }
    }      
    
    // if the sort is reversed, the flip the value
    retVal *= ( SortOrder.isReverseSort() ? -1 : 1 );
    
    return( retVal );
  }

  public double getAmount( )
  {
    return( getAmount( DEFAULT_INDEX ) );
  }

  public double getAmount( int index )
  {
    double        retVal = 0.0;

    try
    {
      if ( index == TOTAL_INDEX )
      {
        retVal = getTotalAmount();
      }
      else
      {
        retVal = Amount[ index ];
      }        
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      // return the default of 0.0
    }
    return( retVal );
  }
  
  public double getAmountRatio( int[] numIndex, int[] denomIndex )
  {
    double            numerator       = 0.0;
    double            denominator     = 0.0;
    double            retVal          = 0.0;
    
    for( int i = 0; i < numIndex.length; ++i )
    {
      numerator += getAmount( numIndex[i] );
    }
    for( int i = 0; i < denomIndex.length; ++i )
    {
      denominator += getAmount( denomIndex[i] );
    }
    
    if ( denominator == 0.0 )
    {
      if ( numerator != 0.0 )     // there were some values
      {
        retVal = 1.0;             // so 100%
      }
    }
    else
    {
      retVal = ( numerator / denominator );
    }      
    return( retVal );
  }
  
  public double getBalance( int[] posIndex, int[] negIndex )
  {
    double            retVal      = 0.0;
    
    for( int i = 0; i < posIndex.length; ++i )
    {
      retVal += getAmount( posIndex[i] );
    }
    for( int i = 0; i < negIndex.length; ++i )
    {
      retVal -= getAmount( negIndex[i] );
    }
    return( retVal );
  }
  
  public int getCount( )
  {
    return( getCount( DEFAULT_INDEX ) );
  }

  public int getCount( int index )
  {
    int       retVal = -1;
    try
    {
      retVal = Count[ index ];
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      // return default of -1
    }
    return( retVal );
  }
  
  public double getCountRatio( int[] numIndex, int[] denomIndex )
  {
    double            numerator       = 0.0;
    double            denominator     = 0.0;
    double            retVal          = 0.0;
    
    for( int i = 0; i < numIndex.length; ++i )
    {
      numerator += getCount( numIndex[i] );
    }
    for( int i = 0; i < denomIndex.length; ++i )
    {
      denominator += getCount( denomIndex[i] );
    }
    
    if ( denominator == 0.0 )
    {
      if ( numerator != 0.0 )     // there were some values
      {
        retVal = 1.0;             // so 100%
      }
    }
    else
    {
      retVal = ( numerator / denominator );
    }      
    return( retVal );
  }
  
  public int getDistrict( )
  {
    return(District);
  }
  
  public Date getEntryDate( )
  {
    return( EntryDate );
  }
  
  public long getHierarchyNode()
  {
    return(HierarchyNode);
  }
  
  public long getOrgId( )
  {
    return( OrgId );
  }
  
  public String getOrgName( )
  {
    return( OrgName );
  }
  
  private int getSortIndex( int sortById )
  {
    int       retVal      = 0;    
    
    for( int i = 0; i < SortKeyToIndexMap.length; ++i )
    {
      if ( SortKeyToIndexMap[i][0] == sortById )
      {
        retVal = SortKeyToIndexMap[i][1];
        break;
      }
    }
    return( retVal );
  }
  
  public SortByType getSortOrder( )
  {
    return( SortOrder );
  }
  
  public double getTotalAmount( )
  {
    double      total = 0.0;
    
    for ( int i = 0; i < NUM_AMOUNTS; ++i )
    {
      total += Amount[i];
    }
    return( total );
  }
  
  public double getTotalAmount( int[] indexes )
  {
    double      total = 0.0;
    
    for ( int i = 0; i < indexes.length; ++i )
    {
      total += Amount[indexes[i]];
    }
    return( total );
  }
  
  public int getTotalCount( )
  {
    int       count = 0;
    
    for( int i = 0; i < NUM_COUNTS; ++i )
    {
      count += Count[i];
    }
    return( count );
  }
  
  private void initialize( ResultSet resultSet )
  {
      // initialize the count and amount data structures
    for ( int i = 0; i < NUM_AMOUNTS; ++i )
    {
      Amount[i] = 0.0;
      Count [i] = 0;
    }
    
    if ( resultSet != null )
    {
      try
      {
        District      = resultSet.getInt("district");
        OrgId         = resultSet.getLong("org_num");
        OrgName       = resultSet.getString("org_name");
        HierarchyNode = resultSet.getLong("hierarchy_node");
        
        // null will cause an exception
        // when sorting the result set (see compareTo)
        if ( OrgName == null )
        {
          OrgName = "Not Named";
        }
        
        try
        {
          EntryDate   = resultSet.getDate("entry_date");
        }
        catch( Exception e )
        {
          // only statements have entry dates
        }
        
        // add the row count/amount data
        addData( resultSet );
      }
      catch( java.sql.SQLException e )
      {
        SyncLog.LogEntry(0, (getClass().getName() + "::initialize()"), e.toString());
      }
    }
  }
  
  public void setAmount( double newAmount )
  {
    setAmount( DEFAULT_INDEX, newAmount );
  }

  public void setAmount( int index, double newAmount )
  {
    try
    {
      Amount[ index ] = newAmount;
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      // ?
    }
  }
  
  public void setCount( int newCount )
  {
    setCount( DEFAULT_INDEX, newCount );
  }

  public void setCount( int index, int newCount )
  {
    try
    {
      Count[ index ] = newCount;
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      // ?
    }
  }
  
  public void setOrgId( long newOrgId )
  {
    OrgId = newOrgId;
  }
  
  public void setOrgName( String newOrgName )
  {
    OrgName = newOrgName;
    
    if ( OrgName == null )
    {
      OrgName = "Not Named";
    }
  }
  
  protected void setSortOrder( SortByType newSortOrder )
  {
    // store the sort order object
    SortOrder = newSortOrder;
  }
  //@+ OBSOLETE
  public String getOrgAssocId( )
  {
    return( OrgAssocId );
  }
  public long getOrgMerchantId( )
  {
    return( OrgMerchantId );
  }
  public void setOrgAssocId( String orgAssocId )
  {
    OrgAssocId = orgAssocId;
  }
  public void setOrgMerchantId( long newMerchantId )
  {
    OrgMerchantId = newMerchantId;
    HierarchyNode = newMerchantId;
  }
  //@- OBSOLETE
}
//C-    MesOrgSummaryEntry



