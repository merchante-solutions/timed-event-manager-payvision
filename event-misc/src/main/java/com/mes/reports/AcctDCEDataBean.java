/*@lineinfo:filename=AcctDCEDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AcctDCEDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class AcctDCEDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public double     AdjAmount           = 0.0;
    public Date       AdjDate             = null;
    public String     AppUserLogin        = null;
    public int        BankNumber          = 0;
    public long       BankGLNumber        = 0L;
    public int        DceRefNum           = 0;
    public long       MerchantId          = 0L;
    public int        MesGLNumber         = 0;
    public long       ReferenceNumber     = 0L;
    public int        TranCode            = 0;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException 
    {
      AdjAmount       = resultSet.getDouble ("adj_amount");
      AdjDate         = resultSet.getDate   ("adj_date");
      AppUserLogin    = processString(resultSet.getString ("app_user_login"));
      BankNumber      = resultSet.getInt    ("bank_number");
      BankGLNumber    = resultSet.getLong   ("gl_number");
      MerchantId      = resultSet.getLong   ("merchant_number");
      MesGLNumber     = resultSet.getInt    ("mes_gl_number");
      ReferenceNumber = resultSet.getLong   ("reference_number");
      TranCode        = resultSet.getInt    ("adj_type");
    }
  }
  
  public AcctDCEDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"GL Number\",");
    line.append("\"Merchant Id\",");
    line.append("\"Sales Rep\",");
    line.append("\"Tran Code\",");
    line.append("\"GL Code\",");
    line.append("\"Reference\",");
    line.append("\"Adj Date\",");
    line.append("\"Adj Amount\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData       record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.BankGLNumber );
    line.append( "," );
    line.append( record.MerchantId );
    line.append( ",\"" );
    line.append( record.AppUserLogin );
    line.append( "\"," );
    line.append( record.TranCode );
    line.append( "," );
    line.append( record.MesGLNumber );
    line.append( "," );
    line.append( record.ReferenceNumber );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.AdjDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( record.AdjAmount );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_dce_detail_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin, "MMddyyyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd, "MMddyyyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:155^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ INDEX (gm pkgroup_merchant) */
//                  adj.merchant_account_number                 as merchant_number,
//                  mf.bank_number                              as bank_number,
//                  ( adj.adjustment_amount *
//                    decode(adj.debit_credit_ind,'C',-1,1))    as adj_amount,
//                  adj.transaction_date_mmddccyy               as adj_date,
//                  transacction_code                           as adj_type,
//                  substr(adj.reference_number,
//                         length(adj.reference_number)-1,2)    as mes_gl_number,
//                  nvl(gl.GL_NUMBER,111111)                    as gl_number,
//                  adj.reference_number                        as reference_number,
//                  nvl(app.app_user_login,'N/A')               as app_user_login
//          from    group_merchant                gm,
//                  group_rep_merchant            grm,
//                  daily_detail_file_adjustment  adj,
//                  bank_dce_ref_to_gl            gl,
//                  vital_ach_tran_desc           td,
//                  mif                           mf,
//                  merchant                      mr,
//                  application                   app
//          where   gm.org_num = :orgId and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  adj.merchant_account_number = gm.merchant_number and
//                  adj.batch_date between :beginDate and :endDate and
//                  adj.transacction_code in (9072,9078) and
//  --                ( adj.transacction_code in (9072,9078) or -- always take 9072 and 9078
//  --                  (adj.transacction_code = 9071 and       -- only take 9071 if the acct
//  --                   mf.sic_code in ( 6010,6011 )) )  and   -- is cash advance (6010,6011)
//                  not substr(adj.reference_number,1,3) in (111) and
//                  td.ACH_TRAN_CODE(+) = adj.transacction_code and
//                  mf.merchant_number  = adj.merchant_account_number and
//                  ( gl.bank_number = mf.bank_number or 
//                    gl.bank_number is null ) and
//                  gl.DCE_REF_NUM(+)   = substr(adj.reference_number,length(adj.reference_number)-1,2) and
//                  mr.merch_number(+) = mf.merchant_number and
//                  app.app_seq_num(+) = mr.app_seq_num
//          order by gl.GL_NUMBER, substr(adj.reference_number,length(adj.reference_number)-1,2), adj_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ INDEX (gm pkgroup_merchant) */\n                adj.merchant_account_number                 as merchant_number,\n                mf.bank_number                              as bank_number,\n                ( adj.adjustment_amount *\n                  decode(adj.debit_credit_ind,'C',-1,1))    as adj_amount,\n                adj.transaction_date_mmddccyy               as adj_date,\n                transacction_code                           as adj_type,\n                substr(adj.reference_number,\n                       length(adj.reference_number)-1,2)    as mes_gl_number,\n                nvl(gl.GL_NUMBER,111111)                    as gl_number,\n                adj.reference_number                        as reference_number,\n                nvl(app.app_user_login,'N/A')               as app_user_login\n        from    group_merchant                gm,\n                group_rep_merchant            grm,\n                daily_detail_file_adjustment  adj,\n                bank_dce_ref_to_gl            gl,\n                vital_ach_tran_desc           td,\n                mif                           mf,\n                merchant                      mr,\n                application                   app\n        where   gm.org_num =  :1  and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and        \n                adj.merchant_account_number = gm.merchant_number and\n                adj.batch_date between  :4  and  :5  and\n                adj.transacction_code in (9072,9078) and\n--                ( adj.transacction_code in (9072,9078) or -- always take 9072 and 9078\n--                  (adj.transacction_code = 9071 and       -- only take 9071 if the acct\n--                   mf.sic_code in ( 6010,6011 )) )  and   -- is cash advance (6010,6011)\n                not substr(adj.reference_number,1,3) in (111) and\n                td.ACH_TRAN_CODE(+) = adj.transacction_code and\n                mf.merchant_number  = adj.merchant_account_number and\n                ( gl.bank_number = mf.bank_number or \n                  gl.bank_number is null ) and\n                gl.DCE_REF_NUM(+)   = substr(adj.reference_number,length(adj.reference_number)-1,2) and\n                mr.merch_number(+) = mf.merchant_number and\n                app.app_seq_num(+) = mr.app_seq_num\n        order by gl.GL_NUMBER, substr(adj.reference_number,length(adj.reference_number)-1,2), adj_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AcctDCEDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AcctDCEDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:196^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData(resultSet) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public String getGLDesc( int bankId, int dceRef )
  {
    String      desc      = "Description not available";
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:220^7*/

//  ************************************************************
//  #sql [Ctx] { select  gl_desc  
//          from    bank_dce_ref_to_gl
//          where   bank_number = :bankId and
//                  dce_ref_num = :dceRef
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  gl_desc   \n        from    bank_dce_ref_to_gl\n        where   bank_number =  :1  and\n                dce_ref_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AcctDCEDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankId);
   __sJT_st.setInt(2,dceRef);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   desc = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( desc );
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, -2 );
      
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/