/*@lineinfo:filename=SVBProfitLossDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/SVBProfitLossDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 3/30/04 2:24p $
  Version            : $Revision: 11 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class SVBProfitLossDataBean extends ReportSQLJBean
{
  public class RowData
  {
    Date                    ActiveDate        = null;
    String                  CifNum            = null;
    String                  DbaName           = null;
    String                  DdaNum            = null;
    long                    MerchantId        = 0L;
    double                  NetProfitLoss     = 0.0;
    
    public RowData(  ResultSet resultSet )
      throws java.sql.SQLException
    {
      ActiveDate    = resultSet.getDate("active_date");
      MerchantId    = resultSet.getLong("merchant_number");
      CifNum        = resultSet.getString("cif_num");
      DbaName       = resultSet.getString("dba_name");
      DdaNum        = resultSet.getString("dda_num");
      NetProfitLoss = resultSet.getDouble("net_profit_loss");
    }
  }
  
  public SVBProfitLossDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);

    line.append("\"Str_BillingDate_t\",");
    line.append("\"Str_RelatedDDA_t\",");
    line.append("\"Str_Key_t\",");
    line.append("\"Str_SVBDDA_t\",");
    line.append("\"Str_DBA_t\",");
    line.append("\"Str_NetProfitLoss_t\",");
    line.append("\"CORRECTED_KEY\"");
  }
  
  protected void encodeHeaderFixed( StringBuffer line )
  {
    line.setLength(0);
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData      record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( DateTimeFormatter.getFormattedDate(record.ActiveDate,"MMM yyyy") );
    line.append( "," );
    line.append( record.MerchantId );
    line.append( ",\"" );
    line.append( record.CifNum );
    line.append( "\",\"" );
    line.append( record.DdaNum );
    line.append( "\",\"" );
    line.append( record.DbaName );
    line.append( "\"," );
    line.append( record.NetProfitLoss );
    line.append( "," );   // last field (corrected_key) is blank?
  }
  
  protected void encodeLineFixed( Object obj, StringBuffer line )
  {
    RowData       record            = (RowData)obj;
    
    line.setLength(0);
    line.append( encodeFixedField( record.ActiveDate,"MMyyyy" ) );
    line.append( encodeFixedField( 16, record.MerchantId ) );
    line.append( encodeFixedField( 12, record.CifNum ) );
    line.append( encodeFixedField( 17, record.DdaNum ) );
    line.append( encodeFixedField( 32, record.DbaName ) );
    line.append( encodeFixedField( 12, record.NetProfitLoss, 2 ) );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append("svb_pl_");    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMMyyyy") );
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
      case FF_FIXED:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator       it            = null;
    long                    nodeId        = orgIdToHierarchyNode(orgId);
    ResultSet               resultSet     = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:160^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                          as active_date,
//                  mf.merchant_number                      as merchant_number,
//                  substr(mf.user_acount_1, 1, 10)         as cif_num,
//                  mf.dda_num                              as dda_num,
//                  mf.dba_name                             as dba_name,
//                  ( ( sm.tot_inc_discount +
//                      sm.tot_inc_interchange +
//                      sm.tot_inc_authorization +
//                      sm.tot_inc_capture +
//                      sm.tot_inc_debit +
//                      sm.tot_inc_ind_plans +
//                      sm.tot_inc_sys_generated +
//                      sm.tot_ndr_referral +
//                      sm.tot_authorization_referral +
//                      sm.tot_capture_referral +
//                      sm.tot_debit_referral +
//                      sm.tot_ind_plans_referral +
//                      sm.tot_sys_generated_referral +
//                      nvl(sm.fee_dce_adj_amount,0) +
//                      nvl(sm.disc_ic_dce_adj_amount,0) ) -
//                    ( decode( nvl(sm.interchange_expense, 0),
//                              0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),
//                              sm.interchange_expense ) +
//                      sm.vmc_assessment_expense +
//                      sm.tot_ndr_liability +
//                      sm.tot_authorization_liability +
//                      sm.tot_capture_liability +
//                      sm.tot_debit_liability +
//                      sm.tot_ind_plans_liability +
//                      sm.tot_sys_generated_liability +
//                      nvl(sm.tot_equip_rental_liability,0) +
//                      nvl(sm.tot_equip_sales_liability,0) +
//                      nvl(sm.equip_transfer_cost,0) ) )   as net_profit_loss
//          from    t_hierarchy               th,
//                  monthly_extract_summary   sm,
//                  mif                       mf
//          where   th.hier_type = 1 and
//                  th.ancestor = :nodeId and
//                  th.entity_type = 4 and
//                  sm.assoc_hierarchy_node = th.descendent and
//                  sm.active_date     = :beginDate and
//                  mf.merchant_number = sm.merchant_number
//          order by mf.merchant_number                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                          as active_date,\n                mf.merchant_number                      as merchant_number,\n                substr(mf.user_acount_1, 1, 10)         as cif_num,\n                mf.dda_num                              as dda_num,\n                mf.dba_name                             as dba_name,\n                ( ( sm.tot_inc_discount +\n                    sm.tot_inc_interchange +\n                    sm.tot_inc_authorization +\n                    sm.tot_inc_capture +\n                    sm.tot_inc_debit +\n                    sm.tot_inc_ind_plans +\n                    sm.tot_inc_sys_generated +\n                    sm.tot_ndr_referral +\n                    sm.tot_authorization_referral +\n                    sm.tot_capture_referral +\n                    sm.tot_debit_referral +\n                    sm.tot_ind_plans_referral +\n                    sm.tot_sys_generated_referral +\n                    nvl(sm.fee_dce_adj_amount,0) +\n                    nvl(sm.disc_ic_dce_adj_amount,0) ) -\n                  ( decode( nvl(sm.interchange_expense, 0),\n                            0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),\n                            sm.interchange_expense ) +\n                    sm.vmc_assessment_expense +\n                    sm.tot_ndr_liability +\n                    sm.tot_authorization_liability +\n                    sm.tot_capture_liability +\n                    sm.tot_debit_liability +\n                    sm.tot_ind_plans_liability +\n                    sm.tot_sys_generated_liability +\n                    nvl(sm.tot_equip_rental_liability,0) +\n                    nvl(sm.tot_equip_sales_liability,0) +\n                    nvl(sm.equip_transfer_cost,0) ) )   as net_profit_loss\n        from    t_hierarchy               th,\n                monthly_extract_summary   sm,\n                mif                       mf\n        where   th.hier_type = 1 and\n                th.ancestor =  :1  and\n                th.entity_type = 4 and\n                sm.assoc_hierarchy_node = th.descendent and\n                sm.active_date     =  :2  and\n                mf.merchant_number = sm.merchant_number\n        order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.SVBProfitLossDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.SVBProfitLossDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:205^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        ReportRows.addElement(new RowData(resultSet));
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.MONTH, -1 );
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/