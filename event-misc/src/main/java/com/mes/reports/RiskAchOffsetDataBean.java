/*@lineinfo:filename=RiskAchOffsetDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskAchOffsetDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 3/09/04 3:56p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class RiskAchOffsetDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public Date       ActivityDate        = null;
    public String     DbaName             = null;
    public double     DepositCredits      = 0.0;
    public double     DepositDebits       = 0.0;
    public double     DepositNet          = 0.0;
    public long       MerchantId          = 0L;
    public String     PortfolioName       = null;
    public long       PortfolioNode       = 0L;
    public int        RiskScore           = 0;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      ActivityDate  = resultSet.getDate("activity_date");
      DbaName       = resultSet.getString("dba_name");
      DepositNet    = resultSet.getDouble("deposit_net");
      DepositCredits= resultSet.getDouble("deposit_credits");
      DepositDebits = resultSet.getDouble("deposit_debits");
      MerchantId    = resultSet.getLong("merchant_number");
      RiskScore     = resultSet.getInt("risk_score");
      PortfolioNode = resultSet.getLong("portfolio_node");
      PortfolioName = resultSet.getString("portfolio_name");
    }
  }
  
  public RiskAchOffsetDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Portfolio Node\",");
    line.append("\"Portfolio Name\",");
    line.append("\"Merchant Number\",");
    line.append("\"DBA Name\",");
    line.append("\"Activity Date\",");
    line.append("\"Risk Score\",");
    line.append("\"Deposit Credits\",");
    line.append("\"Deposit Debits\",");
    line.append("\"Deposit Net\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append(encodeHierarchyNode(record.PortfolioNode));
    line.append(",\"");
    line.append(record.PortfolioName);
    line.append("\",");
    line.append(encodeHierarchyNode(record.MerchantId));
    line.append(",\"");
    line.append(record.DbaName);
    line.append("\",");
    line.append(DateTimeFormatter.getFormattedDate(record.ActivityDate,"MM/dd/yyyy"));
    line.append(",");
    line.append(record.RiskScore);
    line.append(",");
    line.append(record.DepositCredits);
    line.append(",");
    line.append(record.DepositDebits);
    line.append(",");
    line.append(record.DepositNet);
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_risk_deposits_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          nodeId            = orgIdToHierarchyNode(orgId);
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:161^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group                 as portfolio_node,
//                  o.org_name                  as portfolio_name,
//                  mf.merchant_number          as merchant_number,
//                  mf.dba_name                 as dba_name,
//                  rss.total_points            as risk_score,
//                  rss.activity_date           as activity_date,
//                  ach.credit_amount           as deposit_credits,
//                  ach.debit_amount            as deposit_debits,
//                  (ach.credit_amount - 
//                   ach.debit_amount)          as deposit_net
//          from    t_hierarchy         th,
//                  mif                 mf,
//                  risk_score_summary  rss,
//                  ach_summary         ach,
//                  groups              g,
//                  organization        o
//          where   th.hier_type = 1 and
//                  th.ancestor = :nodeId and
//                  th.entity_type = 4 and
//                  mf.association_node = th.descendent and
//                  rss.merchant_number = mf.merchant_number and
//                  rss.activity_date between :beginDate and :endDate and
//                  rss.total_points >= 200 and
//                  ach.merchant_number = rss.merchant_number and
//                  ach.post_date = rss.activity_date and
//                  g.assoc_number = mf.association_node and
//                  o.org_group = g.group_2
//          order by o.org_name,
//                   o.org_group,
//                   deposit_credits desc, 
//                   risk_score desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group                 as portfolio_node,\n                o.org_name                  as portfolio_name,\n                mf.merchant_number          as merchant_number,\n                mf.dba_name                 as dba_name,\n                rss.total_points            as risk_score,\n                rss.activity_date           as activity_date,\n                ach.credit_amount           as deposit_credits,\n                ach.debit_amount            as deposit_debits,\n                (ach.credit_amount - \n                 ach.debit_amount)          as deposit_net\n        from    t_hierarchy         th,\n                mif                 mf,\n                risk_score_summary  rss,\n                ach_summary         ach,\n                groups              g,\n                organization        o\n        where   th.hier_type = 1 and\n                th.ancestor =  :1  and\n                th.entity_type = 4 and\n                mf.association_node = th.descendent and\n                rss.merchant_number = mf.merchant_number and\n                rss.activity_date between  :2  and  :3  and\n                rss.total_points >= 200 and\n                ach.merchant_number = rss.merchant_number and\n                ach.post_date = rss.activity_date and\n                g.assoc_number = mf.association_node and\n                o.org_group = g.group_2\n        order by o.org_name,\n                 o.org_group,\n                 deposit_credits desc, \n                 risk_score desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.RiskAchOffsetDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.RiskAchOffsetDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:194^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         inc           = -1;
      
      if ( ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.MONDAY ) ||
           ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.TUESDAY ) )
      {
        // friday data available on monday,
        // sat,sun,mon data available on tuesday
        inc = -3;     
      }
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, inc );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      
      cal.setTime( ReportDateEnd );
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/