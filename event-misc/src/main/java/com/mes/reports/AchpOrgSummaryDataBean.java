package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ComboDateField;
import com.mes.forms.DropDownField;
import com.mes.forms.FieldGroup;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;

/**
 * Extends OrgSummaryDataBean to add ACHP specific reporting support, see
 * ACHP settlement and return report beans.
 */

public abstract class AchpOrgSummaryDataBean extends OrgSummaryDataBean
{
  static Logger log = Logger.getLogger(AchpOrgSummaryDataBean.class);

  public static SimpleDateFormat oraSdf
                                  = new SimpleDateFormat("''d-MMM-yyyy''");

  public AchpOrgSummaryDataBean(boolean fieldsEnabled)
  {
    super(fieldsEnabled);   // enable field bean support?
  }

  /**
   * FIELD BEAN
   */

  public class AchpProfileTable extends DropDownTable
  {
    public AchpProfileTable(String merchNum)
    {
      addElement("","All");

      Statement s = null;

      try
      {
        connect();
        s = con.createStatement();
        ResultSet rs = s.executeQuery(
          " select                          " +
          "  profile_id,                    " +
          "  company_name                   " +
          " from                            " +
          "  achp_profiles                  " +
          " where                           " +
          "  merch_num = '" + merchNum + "' ");
        while (rs.next())
        {
          addElement(rs.getString("profile_id"), 
            rs.getString("profile_id") + " - " + rs.getString("company_name"));
        }
      }
      catch (Exception e)
      {
        log.error("Error: " + e);
        e.printStackTrace();
        throw new RuntimeException(e);
      }
      finally
      {
        try { s.close(); } catch (Exception e) { }
        cleanUp();
      }
    }
  }

  protected void createStandardAchpFields(HttpServletRequest request)
  {
    FieldGroup g = new FieldGroup("achpFields");

    // create profil drop down for use on the detail mode report
    String merchNum = request.getParameter("com.mes.HierarchyNode");
    g.add(
      new DropDownField("profileId","Profile ID",
        new AchpProfileTable(merchNum),true));

    // create date fields, default to yesterday unless after 3pm
    // (3pm = achp daily processing 
    Calendar cal = Calendar.getInstance();
    if (cal.get(cal.HOUR) <= 15)
    {
      cal.add(cal.DATE,-1);
    }

    ComboDateField f = new ComboDateField("beginDate","From",true,false,2000,false);
    f.setUtilDate(cal.getTime());
    g.add(f);

    f = new ComboDateField("endDate","To",true,false,2000,false);
    f.setUtilDate(cal.getTime());
    g.add(f);

    g.setHtmlExtra("class=\"formFields\"");

    fields.add(g);
  }
  
  public static String formatOraDate(Date d)
  {
    return oraSdf.format(d);
  }

  public static java.util.Date toDate(Timestamp ts)
  {
    return ts != null ? new java.util.Date(ts.getTime()) : null;
  }

  /**
   * Set report specific properties.
   */
  public void setProperties(HttpServletRequest request)
  {
    super.setProperties(request);
  }

  /**
   * Overridden to set default sort order if setProperties() doesn't get 
   * called due to this setting with FieldBean.autoSetFields().
   */
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest(request);
    setSortOrder(HttpHelper.getInt(request,"sortOrder",SortByType.SB_NONE));
  }

  /**
   * CSV DOWNLOAD utility methods
   */

  public boolean isEncodingSupported(int fileFormat)
  {
    return fileFormat == FF_CSV;
  }

  public abstract String getBeanSpecificFilenameFragment();

  public String getDownloadFilenameBase()
  {
    SimpleDateFormat sdf = new SimpleDateFormat("MMddyy");
    StringBuffer filename = new StringBuffer("");

    // append merch id, may be profile id if present, else merch num
    String profileId = fields.getData("profileId");
    if (profileId.length() > 0)
    {
      filename.append(profileId);
    }
    else
    {
      filename.append(getReportHierarchyNode());
    }

    // child class must supply a report specific descriptor
    filename.append("_" + getBeanSpecificFilenameFragment() + "_");

    // add a report type descriptor: org, sum or dtl
    if (getReportType() == RT_DETAILS)
    {
      filename.append("dtl_");
    }
    else if (getReportMerchantId() != 0L)
    {
      filename.append("sum_");
    }
    else
    {
      filename.append("org_");
    }

    // use accessors as they will determine if dates 
    // are available in the field forms
    Date beginDate = getReportDateBegin();
    Date endDate = getReportDateEnd();

    // build the begin date into the filename
    filename.append(sdf.format(beginDate));

    // add second date if available
    if (beginDate.equals(endDate) == false)
    {
      filename.append("_to_");
      filename.append(sdf.format(endDate));
    }
    return filename.toString();
  }

  protected void encodeCSV(String value, StringBuffer line, boolean quoteFlag)
  {
    line.append(quoteFlag ? "\"" : "");
    line.append(value);
    line.append(quoteFlag ? "\"" : "");
  }

  protected void encodeCSV(String value, StringBuffer line)
  {
    encodeCSV((value != null ? value : ""),line,true);
  }

  protected void encodeCSV(int value, StringBuffer line)
  {
    line.append(""+value);
  }

  protected void encodeCSV(double value, StringBuffer line)
  {
    line.append(""+value);
  }

  protected void encodeCSV(java.util.Date value, StringBuffer line)
  {
    line.append(DateTimeFormatter.getFormattedDate(value,"MM/dd/yyyy"));
  }
}