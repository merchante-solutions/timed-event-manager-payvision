/*@lineinfo:filename=RiskFraudCardEditorBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskFraudCardEditorBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 5/17/04 5:45p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class RiskFraudCardEditorBean extends ReportSQLJBean
{
  public class RowData
  {
    public int    CardNumberBegin   = 0;
    public int    CardNumberEnd     = 0;
    
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CardNumberBegin = resultSet.getInt("card_number_begin");
      CardNumberEnd   = resultSet.getInt("card_number_end");
    }
  }
  
  public RiskFraudCardEditorBean( )
  {
  }
  
  protected void deleteCardElement( int cardBeg, int cardEnd )
  {
    try
    {
      setAutoCommit(false);
      
      /*@lineinfo:generated-code*//*@lineinfo:81^7*/

//  ************************************************************
//  #sql [Ctx] { delete  
//          from    risk_fraud_card_numbers fcn
//          where   fcn.card_number_begin = :cardBeg and
//                  fcn.card_number_end = :cardEnd
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  \n        from    risk_fraud_card_numbers fcn\n        where   fcn.card_number_begin =  :1  and\n                fcn.card_number_end =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.reports.RiskFraudCardEditorBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,cardBeg);
   __sJT_st.setInt(2,cardEnd);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:87^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:89^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("deleteCardElement()",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:97^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:97^34*/ } catch( Exception ee ){}
    }
    finally
    {
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Card Number Begin\",");
    line.append("\"Card Number End\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.CardNumberBegin );
    line.append( "," );
    line.append( record.CardNumberEnd );
  }

  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append("fraud_card_numbers_");
    filename.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MMddyyyy"));
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    loadData( getReportHierarchyNode(), ReportDateBegin, ReportDateEnd );
  }
  
  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator       it              = null;
    ResultSet               resultSet       = null;
    
    try
    {
      ReportRows.removeAllElements();
      
      /*@lineinfo:generated-code*//*@lineinfo:158^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  fcn.card_number_begin                 as card_number_begin,
//                  fcn.card_number_end                   as card_number_end
//          from    risk_fraud_card_numbers fcn
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  fcn.card_number_begin                 as card_number_begin,\n                fcn.card_number_end                   as card_number_end\n        from    risk_fraud_card_numbers fcn";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.RiskFraudCardEditorBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.RiskFraudCardEditorBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:163^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    int         delBeg  = 0;
    int         delEnd  = 0;
  
    // load the default report properties
    super.setProperties( request );
    
    if ( ( (delBeg = HttpHelper.getInt(request,"deleteBegin",-1)) != -1 ) &&
         ( (delEnd = HttpHelper.getInt(request,"deleteEnd",-1)) != -1 ) &&
         ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_RISK_QUEUE_ADMIN ) ) )
    {
      deleteCardElement(delBeg,delEnd);
    }
  }
}/*@lineinfo:generated-code*/