/*@lineinfo:filename=EquipPriceDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/EquipPriceDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 3/04/04 9:52a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class EquipPriceDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public String         EquipDesc       = null;
    public String         EquipModel      = null;
    public Timestamp      LastModDate     = null;
    public String         LastModUser     = null;
    public long           NodeId          = 0L;
    public String         OrgName         = null;
    public double         PriceSales      = 0.0;
    public double         PriceSalesUsed  = 0.0;
    public double         PriceRental     = 0.0;
    public double         PriceRentalUsed = 0.0;
    public long           RecId           = 0L;
    public Date           ValidDateBegin  = null;
    public Date           ValidDateEnd    = null;
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      LastModDate     = resultSet.getTimestamp("last_mod_date");
      LastModUser     = resultSet.getString("last_mod_user");
      RecId           = resultSet.getLong("rec_id");
      NodeId          = resultSet.getLong("org_node");
      OrgName         = resultSet.getString("org_name");
      EquipModel      = resultSet.getString("equip_model");
      EquipDesc       = resultSet.getString("equip_desc");
      ValidDateBegin  = resultSet.getDate("valid_date_begin");
      ValidDateEnd    = resultSet.getDate("valid_date_end");
      PriceSales      = resultSet.getDouble("price_sales");
      PriceSalesUsed  = resultSet.getDouble("price_sales_used");
      PriceRental     = resultSet.getDouble("price_rental");
      PriceRentalUsed = resultSet.getDouble("price_rental_used");
    }
  }
  
  protected boolean       ViewHistoricPrices          = false;
  
  public EquipPriceDataBean( )
  {
  }
  
  protected void deleteEquipPriceEntry( long id )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:98^7*/

//  ************************************************************
//  #sql [Ctx] { delete  
//          from    equip_price       ep
//          where   ep.rec_id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  \n        from    equip_price       ep\n        where   ep.rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.reports.EquipPriceDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:103^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("deleteEquipPriceEntry()",e.toString());
    }
    finally
    {
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Hierarchy Node\",");
    line.append("\"Org Name\",");
    line.append("\"Valid Date Begin\",");
    line.append("\"Valid Date End\",");
    line.append("\"Equip Model\",");
    line.append("\"Equip Description\",");
    line.append("\"Price Sales\",");
    line.append("\"Price Sales Used\",");
    line.append("\"Price Rental\",");
    line.append("\"Price Rental Used\",");
    line.append("\"Last Modified\",");
    line.append("\"Last User\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( encodeHierarchyNode(record.NodeId) );
    line.append(",\"");
    line.append( record.OrgName );
    line.append("\",");
    line.append( DateTimeFormatter.getFormattedDate(record.ValidDateBegin,"MM/dd/yyyy") );
    line.append(",");
    line.append( DateTimeFormatter.getFormattedDate(record.ValidDateEnd,"MM/dd/yyyy") );
    line.append(",\"");
    line.append( record.EquipModel );
    line.append("\",\"");
    line.append( record.EquipDesc );
    line.append("\",");
    line.append( record.PriceSales );
    line.append(",");
    line.append( record.PriceSalesUsed );
    line.append(",");
    line.append( record.PriceRental );
    line.append(",");
    line.append( record.PriceRentalUsed );
    line.append(",");
    line.append( DateTimeFormatter.getFormattedDate(record.LastModDate,"MM/dd/yyyy hh:mm:ss a"));
    line.append(",\"");
    line.append( record.LastModUser );
    line.append("\"");
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_equip_price_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it            = null;
    long                nodeId        = orgIdToHierarchyNode(orgId);
    ResultSet           resultSet     = null;
    int                 showAll       = (ViewHistoricPrices ? 1 : 0);
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:200^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ep.rec_id                                 as rec_id,
//                  ep.hierarchy_node                         as org_node,
//                  nvl(o.org_name,'Not Available')           as org_name,
//                  ep.equip_model                            as equip_model,
//                  nvl(eq.equip_descriptor,'Not Available')  as equip_desc,
//                  ep.valid_date_begin                       as valid_date_begin,
//                  ep.valid_date_end                         as valid_date_end,
//                  ep.price_base                             as price_sales,
//                  ep.price_used                             as price_sales_used,         
//                  ep.price_rental                           as price_rental,
//                  ep.price_rental_used                      as price_rental_used,
//                  ep.last_modified_date                     as last_mod_date,
//                  ep.last_modified_user                     as last_mod_user
//          from    t_hierarchy     th,
//                  equip_price     ep,
//                  equipment       eq,
//                  organization    o
//          where   th.hier_type = 1 and
//                  th.ancestor = :nodeId and
//                  ep.hierarchy_node = th.descendent and
//                  eq.equip_model = ep.equip_model and
//                  o.org_group = ep.hierarchy_node and
//                  ( :showAll = 1 or ep.valid_date_end >= sysdate )
//          order by o.org_name, ep.equip_model, ep.valid_date_end      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ep.rec_id                                 as rec_id,\n                ep.hierarchy_node                         as org_node,\n                nvl(o.org_name,'Not Available')           as org_name,\n                ep.equip_model                            as equip_model,\n                nvl(eq.equip_descriptor,'Not Available')  as equip_desc,\n                ep.valid_date_begin                       as valid_date_begin,\n                ep.valid_date_end                         as valid_date_end,\n                ep.price_base                             as price_sales,\n                ep.price_used                             as price_sales_used,         \n                ep.price_rental                           as price_rental,\n                ep.price_rental_used                      as price_rental_used,\n                ep.last_modified_date                     as last_mod_date,\n                ep.last_modified_user                     as last_mod_user\n        from    t_hierarchy     th,\n                equip_price     ep,\n                equipment       eq,\n                organization    o\n        where   th.hier_type = 1 and\n                th.ancestor =  :1  and\n                ep.hierarchy_node = th.descendent and\n                eq.equip_model = ep.equip_model and\n                o.org_group = ep.hierarchy_node and\n                (  :2  = 1 or ep.valid_date_end >= sysdate )\n        order by o.org_name, ep.equip_model, ep.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.EquipPriceDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,showAll);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.EquipPriceDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    long        delId     = 0L;
  
    // load the default report properties
    super.setProperties( request );
    
    ViewHistoricPrices = HttpHelper.getBoolean(request,"viewHistoric",false);
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
    
    if ( ( (delId = HttpHelper.getLong(request,"deleteId",-1L)) != -1L ) &&
         ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_MANAGEMENT ) ) )
    {
      deleteEquipPriceEntry(delId);
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/