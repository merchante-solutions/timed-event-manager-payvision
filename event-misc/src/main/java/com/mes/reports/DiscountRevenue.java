/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/DiscountRevenue.java $

  Description:  
  
  Extends the date range report bean to generate a sales activity report.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 8/06/03 3:26p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesHierarchy;
import com.mes.constants.mesConstants;

public class DiscountRevenue extends DateRangeReportBean
{
  private long      userId        = 0;
  private long      hid           = -1L;
  private int       hierType      = 0;
  private int       appType       = 0;
  private int       creditStatus  = 0;
  private String    nodeName      = null;
  private BeanDate  pmActiveDate  = new BeanDate();
  
  /*
  ** CONSTRUCTOR public DiscountRevenue()
  */
  public DiscountRevenue()
  {
    // determine the active date of the prior month
    pmActiveDate.add(Calendar.MONTH,-1);
    pmActiveDate.setDay(1);
  }
  
  public class ReportRow
  {
    private BeanDate  submitDate              = new BeanDate();
    private BeanDate  activateDate            = new BeanDate();
    
    private String    merchName;
    private String    repUserName;
    private String    gridName;
    
    private long      merchNum;
    private long      appSeqNum;
    private long      repUserId;
    private long      managerUserId;
    
    private int       entityType;
    private int       appType;
    private int       ytdMonthCount;
    
    private double    centsPerTran;
    private double    discountRate;
    private double    appVmcMonthlyVolume;
    private double    appVmcAverageTicket;
    private double    appAnnualDiscount;
    private double    appEquipMonthlyRental;
    private double    pmVmcVolume;
    private double    pmVmcAverageTicket;
    private double    pmDiscount;
    private double    pmEquipRental;
    private double    pmDebitRevenue;
    private double    pmOtherRevenue;
    private double    pmCBRevenue;
    private double    ytdVmcVolume;
    private double    ytdVmcAverageTicket;
    private double    ytdDiscount;
    private double    ytdEquipRental;
    private double    ytdDebitRevenue;
    private double    ytdOtherRevenue;
    private double    ytdCBRevenue;
    
    private boolean   pmIsNull;
    
    public ReportRow(ResultSet rs)
    {
      int prog = 0;
      try
      {
        submitDate.setSqlDate    (rs.getDate  ("submit_date"));
        activateDate.setSqlDate  (rs.getDate  ("activate_date"));
        
        merchName                   =   rs.getString("merch_name");        
        repUserName                 =   rs.getString("rep_user_name");

        merchNum                    =   rs.getLong  ("merch_num");               
        appSeqNum                   =   rs.getLong  ("app_seq_num");                
        repUserId                   =   rs.getLong  ("rep_user_id");
        managerUserId               =   rs.getLong  ("manager_user_id");

        entityType                  =   rs.getInt   ("hier_entity_type");
        appType                     =   rs.getInt   ("app_type");
        int gridType                =   rs.getInt   ("grid_type");
        int planType                =   rs.getInt   ("plan_type");
        int pmVmcCount              =   rs.getInt   ("pm_vmc_count");
        
        centsPerTran                =   rs.getDouble("cents_per_tran1");
        centsPerTran               +=   rs.getDouble("cents_per_tran2");
        centsPerTran               +=   rs.getDouble("cents_per_tran3");
        centsPerTran               +=   rs.getDouble("cents_per_tran4");
        centsPerTran               +=   rs.getDouble("cents_per_tran5");
        discountRate                =   rs.getDouble("discount_rate");
        appVmcMonthlyVolume         =   rs.getDouble("app_vmc_monthly");
        appVmcAverageTicket         =   rs.getDouble("app_avg_ticket");
        appEquipMonthlyRental       =   rs.getDouble("app_equip_rental");
        
        pmIsNull                    =   rs.getString("pm_is_null").equals("y");
        pmVmcVolume                 =   rs.getDouble("pm_vmc_sales");
        pmEquipRental               =   rs.getDouble("pm_equip_rental");
        pmCBRevenue                 =   rs.getDouble("pm_cb_income");
        double pmPlanIncome         =   rs.getDouble("pm_plan_income");
        double pmInterchangeIncome  =   rs.getDouble("pm_intrchg_income");
        double pmAuthIncome         =   rs.getDouble("pm_auth_income");
        double pmCapIncome          =   rs.getDouble("pm_cap_income");
        double pmSysIncome          =   rs.getDouble("pm_sys_income");
        double pmDebitIncome        =   rs.getDouble("pm_debit_income");
        double pmDiscountIncome     =   rs.getDouble("pm_discount_income");
        double pmPlanExpense        =   rs.getDouble("pm_plan_expense");
        double pmInterchangeExpense =   rs.getDouble("pm_intrchg_expense");
        double pmAuthExpense        =   rs.getDouble("pm_auth_expense");
        double pmCapExpense         =   rs.getDouble("pm_cap_expense");
        double pmSysExpense         =   rs.getDouble("pm_sys_expense");
        double pmDebitExpense       =   rs.getDouble("pm_debit_expense");
        
        double interchangeRate      =   rs.getDouble("interchange_rate");
        double assocAssessFee       =   rs.getDouble("assoc_assess_fee");
        double interchangeFee       =   rs.getDouble("interchange_fee");
        
        // net discount   =   ( discount income + interchange income + plan income )
        //                  - ( interchange expense + plan expense )
        // debit revenue  = debit income - debit expense
        // other revenue  =   ( auth income + cap income + sys income )
        //                  - ( auth expense + cap expense + sys expense + equip rental )
        // (sys income includes equip rental which we are breaking out
        // separately, so need to subtract it from the income here)
        //
        // new scheme: do not subtract expense from debit or other revenue
        //
        pmDiscount      =   ( pmDiscountIncome + pmInterchangeIncome + pmPlanIncome )
                          - ( pmInterchangeExpense + pmPlanExpense );
        pmDebitRevenue  =   pmDebitIncome;
        pmOtherRevenue  =   ( pmAuthIncome + pmCapIncome + pmSysIncome ) - pmEquipRental;
                          
        // pick a grid label based on grid type
        String gridNames[] = 
        {
          "",
          "RTL",
          "MOTO",
          "HTL",
          "SPMK",
          "INT",
          "EM",
          "HTL",
          "RTL",
          "MOTO",
          "RST"
        };
        gridName = "--";
        if (gridType < gridNames.length && gridType > 0)
        {
          gridName = gridNames[gridType];
        }

        // calculate discount
        long appDiscountInBasisPoints = 0L;
        switch (planType)
        {
          // variable rate
          case mesConstants.APP_PS_VARIABLE_RATE:
            // not sure how to handle this
            break;
        
          // fixed rate, no cents per tran
          case mesConstants.APP_PS_FIXED_RATE:
          // fixed rate and cents per tran
          case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
          // rate (ignoring non-qual rate)
          case mesConstants.CBT_APP_BUNDLED_RATE_PLAN:
          // rate + (per item + per auth)
          case mesConstants.CBT_APP_UNBUNDLED_RATE_PLAN:
          // new transcom...
          case mesConstants.TRANS_APP_BUCKET_ONLY_PLAN:
          case mesConstants.TRANS_APP_BUCKET_WITH_PERITEM_PLAN:
          case mesConstants.TRANS_APP_DETAILED_STATEMENT_PLAN:
            appDiscountInBasisPoints = (long)
              ((((discountRate / 100) + (centsPerTran / appVmcAverageTicket)) * 10000) - 
               ((((interchangeRate + assocAssessFee) / 100) + (interchangeFee / appVmcAverageTicket)) * 10000) 
               + 0.5);
            break;
               
          // cents per tran (interchange passed through so 
          // tran cost component is cancelled out)
          case mesConstants.APP_PS_INTERCHANGE:
          // per item + per auth + per capture
          case mesConstants.CBT_APP_INTERCHANGE_PLUS_PLAN:
            appDiscountInBasisPoints = (long)
              (((centsPerTran / appVmcAverageTicket) * 10000) + 0.5);
            break;
        }

        appAnnualDiscount = (appVmcAverageTicket * (((double)appDiscountInBasisPoints) / 10000)) 
            * ((appVmcMonthlyVolume * 12) / appVmcAverageTicket) + 0.005;
        appAnnualDiscount = (double) ((long)(appAnnualDiscount * 100)) / 100;
        
        // calculate prior month average ticket
        if (pmVmcCount > 0)
        {
          pmVmcAverageTicket = pmVmcVolume / pmVmcCount;
        }
        else
        {
          pmVmcAverageTicket = 0;
        }
      }
      catch (Exception e)
      {
        addError("ReportRow constructor (prog=" + prog + "): " + e.toString());
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
                 "ReportRow constructor (prog=" + prog + "): " + e.toString());
      }
    }
    
    public void addYearToDate(ResultSet rs)
    {
      try
      {
        ytdMonthCount             =   rs.getInt   ("ytd_months");
        int vmcCount              =   rs.getInt   ("ytd_vmc_count");
        double vmcVolume          =   rs.getDouble("ytd_vmc_sales");
        double equipRental        =   rs.getDouble("ytd_equip_rental");
        double cbRevenue          =   rs.getDouble("ytd_cb_income");
        
        double planIncome         =   rs.getDouble("ytd_plan_income");
        double interchangeIncome  =   rs.getDouble("ytd_intrchg_income");
        double authIncome         =   rs.getDouble("ytd_auth_income");
        double capIncome          =   rs.getDouble("ytd_cap_income");
        double sysIncome          =   rs.getDouble("ytd_sys_income");
        double debitIncome        =   rs.getDouble("ytd_debit_income");
        double discountIncome     =   rs.getDouble("ytd_discount_income");
        double planExpense        =   rs.getDouble("ytd_plan_expense");
        double interchangeExpense =   rs.getDouble("ytd_intrchg_expense");
        double authExpense        =   rs.getDouble("ytd_auth_expense");
        double capExpense         =   rs.getDouble("ytd_cap_expense");
        double sysExpense         =   rs.getDouble("ytd_sys_expense");
        double debitExpense       =   rs.getDouble("ytd_debit_expense");
        
        // net discount   =   ( discount income + interchange income + plan income )
        //                  - ( interchange expense + plan expense )
        // debit revenue  = debit income - debit expense
        // other revenue  =   ( auth income + cap income + sys income )
        //                  - ( auth expense + cap expense + sys expense + equip rental )
        // (sys income includes equip rental which we are breaking out
        // separately, so need to subtract it from the income here)
        //
        // new scheme: do not subtract expense from debit or other revenue
        //
        double discount       =     ( discountIncome + interchangeIncome + planIncome )
                                  - ( interchangeExpense + planExpense );
        double debitRevenue   =     debitIncome;
        double otherRevenue   =     ( authIncome + capIncome + sysIncome ) - equipRental;
        
        // calculate annual amounts based on year to date totals
        ytdVmcVolume    = (vmcVolume    / ytdMonthCount) * 12;
        ytdEquipRental  = (equipRental  / ytdMonthCount) * 12;
        ytdDiscount     = (discount     / ytdMonthCount) * 12;
        ytdDebitRevenue = (debitRevenue / ytdMonthCount) * 12;
        ytdOtherRevenue = (otherRevenue / ytdMonthCount) * 12;
        ytdCBRevenue    = (cbRevenue    / ytdMonthCount) * 12;
        
        // calculate prior month average ticket
        if (vmcCount > 0)
        {
          ytdVmcAverageTicket = vmcVolume / vmcCount;
        }
        else
        {
          ytdVmcAverageTicket = 0;
        }
      }
      catch (Exception e)
      {
        addError("ReportRow addYearToDate(): " + e.toString());
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
                 "ReportRow addYearToDate(): " + e.toString());
      }
    }
    
    // accessors
    public String   getSubmitDate()             { return submitDate.toString(); }
    public String   getActivateDate()           { return activateDate.toString(); }
    
    public String   getMerchName()              { return merchName;}
    public String   getRepUserName()            { return repUserName; }
    public String   getGridName()               { return gridName; }

    public long     getMerchNum()               { return merchNum; }
    public long     getAppSeqNum()              { return appSeqNum; }
    public long     getRepUserId()              { return repUserId; }
    public Long     getManagerUserId()          { return managerUserId; }
    
    public int      getEntityType()             { return entityType; }
    public int      getAppType()                { return appType; }
    public int      getYtdMonthCount()          { return ytdMonthCount; }
    
    public boolean  getPmIsNull()               { return pmIsNull; }
    
    public double   getCentsPerTran()           { return centsPerTran; }
    public double   getDiscountRate()           { return discountRate; }
    public double   getAppVmcMonthlyVolume()    { return appVmcMonthlyVolume; }
    public double   getAppVmcAnnualVolume()     { return appVmcMonthlyVolume * 12; }
    public double   getAppVmcAverageTicket()    { return appVmcAverageTicket; }
    public double   getAppAnnualDiscount()      { return appAnnualDiscount; }
    public double   getAppMonthlyDiscount()     { return appAnnualDiscount / 12; }
    public double   getAppEquipMonthlyRental()  { return appEquipMonthlyRental; }
    public double   getAppEquipAnnualRental()   { return appEquipMonthlyRental * 12; }
    public double   getPmVmcVolume()            { return pmVmcVolume; }
    public double   getPmVmcAverageTicket()     { return pmVmcAverageTicket; }
    public double   getPmDiscount()             { return pmDiscount; }
    public double   getPmEquipRental()          { return pmEquipRental; }
    public double   getPmDebitRevenue()         { return pmDebitRevenue; }
    public double   getPmOtherRevenue()         { return pmOtherRevenue; }
    public double   getPmCBRevenue()            { return pmCBRevenue; }
    public double   getYtdVmcVolume()           { return ytdVmcVolume; }
    public double   getYtdVmcAverageTicket()    { return ytdVmcAverageTicket; }
    public double   getYtdDiscount()            { return ytdDiscount; }
    public double   getYtdEquipRental()         { return ytdEquipRental; }
    public double   getYtdDebitRevenue()        { return ytdDebitRevenue; }
    public double   getYtdOtherRevenue()        { return ytdOtherRevenue; }
    public double   getYtdCBRevenue()           { return ytdCBRevenue; }
  }
  
  public class SubReport extends Vector
  {
    String managerName;
    int entityType;
    public SubReport(String managerName, int entityType)
    {
      this.managerName = managerName;
      this.entityType = entityType;
    }
    public String getManagerName()
    {
      return managerName;
    }
    public int getEntityType()
    {
      return entityType;
    }
    public ReportRow get(long merchNum)
    {
      for (Iterator i = iterator(); i.hasNext(); )
      {
        ReportRow row = (ReportRow)i.next();
        if (row.getMerchNum() == merchNum)
        {
          return row;
        }
      }
      return null;
    }
  }

  /*
  ** private long getDefaultHid(long userId, int hierType) throws Exception
  **
  ** Attempts to load a users default hierarchy start node for a particular
  ** hierarchy type.
  **
  ** RETURNS: user's default hid.
  */
  private long getDefaultHid(long userId, int hierType) throws Exception
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    
    long hid = -1L;
    
    try
    {
      // look up the user's default in the defaults table
      qs.append("select ");
      qs.append("  hier_id ");
      qs.append("from ");
      qs.append("  t_hierarchy_defaults ");
      qs.append("where ");
      qs.append("  hier_type = " + hierType + " ");
      qs.append("  and user_id = " + userId);

      ps = getPreparedStatement(qs.toString());
      rs = ps.executeQuery();
      if (rs.next())
      {
        hid = rs.getLong("hier_id");
      }
      // if none specified for this user, 
      // get the root node of this hierarchy type
      else
      {
        qs.setLength(0);
        qs.append("select ");
        qs.append("  root_id ");
        qs.append("from ");
        qs.append("  t_hierarchy_types ");
        qs.append("where ");
        qs.append("  hier_type = " + hierType);

        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();
        if (rs.next())
        {
          hid = rs.getLong("root_id");
        }
        else
        {
          throw new Exception("No default hierarchy node found for user "
            + userId + " , type " + hierType);
        }
      }
    }
    catch (Exception e)
    {
      throw new Exception("getDefaultHid: " + e.toString());
    }
    
    return hid;
  }
  
  /*
  ** private Hashtable loadDirectChildren(long hid) throws Exception
  **
  ** Given a hierarchy node id a set of the direct descendents of the
  ** node are loaded into a hashtable.  The key used is a Long constructed
  ** from the node id of each child.  The node itself is included in this
  ** set, but is loaded into the table with a Long key value of -1.
  **
  ** RETURNS: Hashtable containing SubReport objects corresponding with
  **          each direct child.
  */
  private Hashtable loadDirectChildren(long hid) throws Exception
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    
    Hashtable directChildren = null;
    
    try
    {
      // get direct children of node, including the node
      // excluding direct merchant children
      qs.setLength(0);
      qs.append("select ");
      qs.append("  h.descendent, ");
      qs.append("  h.entity_type, ");
      qs.append("  hn.name ");
      qs.append("from ");
      qs.append("  t_hierarchy h, ");
      qs.append("  t_hierarchy_names hn ");
      qs.append("where ");
      qs.append("  h.ancestor = " + hid + " ");
      qs.append("  and h.relation < 2 ");
      qs.append("  and h.entity_type <> ? ");
      qs.append("  and h.descendent = hn.hier_id ");
      qs.append("order by ");
      qs.append("  h.relation desc");
    
      ps = getPreparedStatement(qs.toString());
      ps.setInt(1,MesHierarchy.ET_MERCHANT);
      rs = ps.executeQuery();
    
      
      // populate the report data vector with 
      // sub reports for each direct child
      if (rs != null)
      {
        directChildren = new Hashtable();
        while (rs.next())
        {
          Long    managerUserId = rs.getLong("descendent");
          String  managerName   = rs.getString("name");
          int     entityType    = rs.getInt("entity_type");
        
          if (managerUserId.longValue() == hid)
          {
            managerUserId = -1L;
          }
          directChildren.put(managerUserId,new SubReport(managerName,entityType));
        }
      }
    }
    catch (Exception e)
    {
      throw new Exception("loadDirectChildren, hid " + hid + ": " 
        + e.toString());
    }
    
    return directChildren;
  }

  /*
  ** METHOD public ResultSet loadAppAndPriorMonth()
  **
  ** Loads merchant data for merchants that activated between fromDate
  ** and toDate.  Data elements are the projected annual revenue from the
  ** application and the prior month's activity.
  **
  ** RETURNS: query results in a ResultSet.
  */
  public ResultSet loadAppAndPriorMonth()
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    
    try
    {
      qs.append("select                                                 ");
      qs.append("  h1.ancestor                      manager_user_id,    ");
      qs.append("  h2.entity_type                   hier_entity_type,   ");
      qs.append("  a.app_seq_num                    app_seq_num,        ");
      qs.append("  a.app_created_date               submit_date,        ");
      qs.append("  a.app_user_id                    rep_user_id,        ");
      qs.append("  a.app_type                       app_type,           ");
      qs.append("  m.merch_business_name            merch_name,         ");
      qs.append("  m.merch_number                   merch_num,          ");
      qs.append("  m.date_activated                 activate_date,      ");
      qs.append("  m.merch_month_visa_mc_sales      app_vmc_monthly,    ");
      qs.append("  m.merch_average_cc_tran          app_avg_ticket,     ");
      qs.append("  u.name                           rep_user_name,      ");
      qs.append("  t.tranchrg_disc_rate             discount_rate,      ");
      qs.append("  t.tranchrg_discrate_type         plan_type,          ");
      qs.append("  t.tranchrg_per_tran              cents_per_tran1,    ");
      qs.append("  t.tranchrg_pass_thru             cents_per_tran2,    ");
      qs.append("  t.tranchrg_per_auth              cents_per_tran3,    ");
      qs.append("  t.tranchrg_per_capture           cents_per_tran4,    ");
      qs.append("  f.charges                        cents_per_tran5,    ");
      qs.append("  i.grid_type                      grid_type,          ");
      qs.append("  i.interchange_rate               interchange_rate,   ");
      qs.append("  i.assoc_assess_fee               assoc_assess_fee,   ");
      qs.append("  i.interchange_fee                interchange_fee,    ");
      qs.append("  ers.rental_total                 app_equip_rental,   ");
      qs.append("  mgn.t1_tot_inc_ind_plans         pm_plan_income,     ");
      qs.append("  mgn.t1_tot_inc_interchange       pm_intrchg_income,  ");
      qs.append("  mgn.t1_tot_inc_authorization     pm_auth_income,     ");
      qs.append("  mgn.t1_tot_inc_capture           pm_cap_income,      ");
      qs.append("  mgn.t1_tot_inc_sys_generated     pm_sys_income,      ");
      qs.append("  mgn.t1_tot_inc_debit_networks    pm_debit_income,    ");
      qs.append("  mgn.t1_tot_calculated_discount   pm_discount_income, ");
      qs.append("  mgn.t1_tot_exp_ind_plans         pm_plan_expense,    ");
      qs.append("  mgn.t1_tot_exp_interchange       pm_intrchg_expense, ");
      qs.append("  mgn.t1_tot_exp_authorization     pm_auth_expense,    ");
      qs.append("  mgn.t1_tot_exp_capture           pm_cap_expense,     ");
      qs.append("  mgn.t1_tot_exp_sys_generated     pm_sys_expense,     ");
      qs.append("  mgn.t1_tot_exp_debit_networks    pm_debit_expense,   ");
      qs.append("  mgn.s1_chargeback_fee_income     pm_cb_income,       ");
      qs.append("  mes.vmc_sales_amount             pm_vmc_sales,       ");
      qs.append("  mes.vmc_sales_count              pm_vmc_count,       ");
      qs.append("  mes.equip_rental_income          pm_equip_rental,    ");
      qs.append("  nvl(mes.bank_number,'y')         pm_is_null          ");
      qs.append("from                                                   ");
      qs.append("  t_hierarchy                      h1,                 ");
      qs.append("  t_hierarchy                      h2,                 ");
      qs.append("  users                            u,                  ");
      qs.append("  application                      a,                  ");
      qs.append("  merchant                         m,                  ");
      qs.append("  tranchrg                         t,                  ");
      qs.append("  additional_per_tran_fees         f,                  ");
      qs.append("  interchange_cost                 i,                  ");
      qs.append("  equip_rental_summaries           ers,                ");
      qs.append("  monthly_extract_summary          mes,                ");
      qs.append("  monthly_extract_gn               mgn                 ");
      qs.append("where                                                  ");
      qs.append("  h1.entity_type = ?                                   ");
      qs.append("  and h1.descendent = m.merch_number                   ");
      qs.append("  and h1.ancestor = h2.descendent                      ");
      qs.append("  and h2.ancestor = ?                                  ");
      qs.append("  and h2.relation = 1                                  ");
      qs.append("  and m.app_seq_num = a.app_seq_num                    ");
      qs.append("  and ( m.pricing_grid = i.grid_type                   ");
      qs.append("        or ( ( m.pricing_grid is null or m.pricing_grid = 0 ) ");
      qs.append("             and ( ( ( m.industype_code = 6 or m.industype_code = 7 or m.loctype_code = 2 ) and i.grid_type = 2 ) ");
      qs.append("                   or ( ( m.industype_code <> 6 and m.industype_code <> 7 and m.loctype_code <> 2 ) and i.grid_type = 1 ) ) ) ) ");
      qs.append("  and a.app_type in ( 0, 9, 2, 7, 12 )                 ");
      qs.append("  and a.app_user_id = u.user_id (+)                    ");
      qs.append("  and trunc( m.date_activated ) between ? and ?        ");
      qs.append("  and a.app_seq_num = t.app_seq_num                    ");
      qs.append("  and t.cardtype_code = 1                              ");
      qs.append("  and a.app_seq_num = f.app_seq_num(+)                 ");
      qs.append("  and a.app_seq_num = ers.app_seq_num(+)               ");
      qs.append("  and m.merch_number = mes.merchant_number(+)          ");
      qs.append("  and mes.active_date(+) = ?                           ");
      qs.append("  and mes.merchant_number = mgn.hh_merchant_number(+)  ");
      qs.append("  and mes.active_date = mgn.hh_active_date(+)          ");
      qs.append("order by h1.relation, u.name, a.app_created_date       ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setInt (1,MesHierarchy.ET_MERCHANT);
      ps.setLong(2,hid);
      ps.setDate(3,fromDate.getSqlDate());
      ps.setDate(4,toDate.getSqlDate());
      ps.setDate(5,pmActiveDate.getSqlDate());
      
      rs = ps.executeQuery();
    }
    catch (Exception e)
    {
      addError("loadAppAndPriorMonth: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
               "loadAppAndPriorMonth: " + e.toString());
    }
    
    return rs;
  }
  
  /*
  ** public ResultSet loadYearToDate() throws Exception
  **
  ** Loads year-to-date data from monthly extract summary tables.  Sums up
  ** up to 12 prior months of activity in the table starting with the most
  ** recent month of activity.
  **
  ** RETURNS: ResultSet containing the year to date data.
  */
  public ResultSet loadYearToDate()
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    
    try
    {
      qs.append("select count(mes.active_date)                ytd_months,           ");
      qs.append("       sum(mgn.t1_tot_inc_ind_plans)         ytd_plan_income,      ");
      qs.append("       sum(mgn.t1_tot_inc_interchange)       ytd_intrchg_income,   ");
      qs.append("       sum(mgn.t1_tot_inc_authorization)     ytd_auth_income,      ");
      qs.append("       sum(mgn.t1_tot_inc_capture)           ytd_cap_income,       ");
      qs.append("       sum(mgn.t1_tot_inc_sys_generated)     ytd_sys_income,       ");
      qs.append("       sum(mgn.t1_tot_inc_debit_networks)    ytd_debit_income,     ");
      qs.append("       sum(mgn.t1_tot_calculated_discount)   ytd_discount_income,  ");
      qs.append("       sum(mgn.t1_tot_exp_ind_plans)         ytd_plan_expense,     ");
      qs.append("       sum(mgn.t1_tot_exp_interchange)       ytd_intrchg_expense,  ");
      qs.append("       sum(mgn.t1_tot_exp_authorization)     ytd_auth_expense,     ");
      qs.append("       sum(mgn.t1_tot_exp_capture)           ytd_cap_expense,      ");
      qs.append("       sum(mgn.t1_tot_exp_sys_generated)     ytd_sys_expense,      ");
      qs.append("       sum(mgn.t1_tot_exp_debit_networks)    ytd_debit_expense,    ");
      qs.append("       sum(mgn.s1_chargeback_fee_income)     ytd_cb_income,        ");
      qs.append("       sum(mes.equip_rental_income)          ytd_equip_rental,     ");
      qs.append("       sum(mes.vmc_sales_count)              ytd_vmc_count,        ");
      qs.append("       sum(mes.vmc_sales_amount)             ytd_vmc_sales,        ");
      qs.append("       mes.merchant_number                   merch_num,            ");
      qs.append("       hd.hier_entity_type                   hier_entity_type,     ");
      qs.append("       hd.manager_user_id                    manager_user_id       ");
      qs.append("from   monthly_extract_summary          mes,                       ");
      qs.append("       monthly_extract_gn               mgn,                       ");
      qs.append("       ( select  m.merch_number    merch_num,                      ");
      qs.append("                 m.date_activated  date_activated,                 ");
      qs.append("                 h1.ancestor       manager_user_id,                ");
      qs.append("                 h2.entity_type    hier_entity_type                ");
      qs.append("         from    t_hierarchy   h1,                                 ");
      qs.append("                 t_hierarchy   h2,                                 ");
      qs.append("                 application   a,                                  ");
      qs.append("                 merchant      m                                   ");
      qs.append("         where   h1.entity_type = ?                                ");
      qs.append("                 and h1.descendent = m.merch_number                ");
      qs.append("                 and h1.ancestor = h2.descendent                   ");
      qs.append("                 and h2.ancestor = ?                               ");
      qs.append("                 and h2.relation = 1                               ");
      qs.append("                 and m.app_seq_num = a.app_seq_num                 ");
      qs.append("                 and a.app_type in ( 0, 9, 2, 7, 12 )              ");
      qs.append("                 and trunc( m.date_activated )                     ");
      qs.append("                    between ? and ? )   hd                         ");
      qs.append("where  months_between( (  select  max(active_date)                 ");
      qs.append("                         from    monthly_extract_summary           ");
      qs.append("                         where   merchant_number = mes.merchant_number ), ");
      qs.append("                      mes.active_date ) < 12                       ");
      qs.append("       and mes.active_date > hd.date_activated                     ");
      qs.append("       and mes.merchant_number = hd.merch_num                      ");
      qs.append("       and mes.active_date = mgn.hh_active_date                    ");
      qs.append("       and mes.merchant_number = mgn.hh_merchant_number            ");
      qs.append("group by mes.merchant_number,                                      ");
      qs.append("         hd.hier_entity_type,                                      ");
      qs.append("         hd.manager_user_id                                        ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setInt (1,MesHierarchy.ET_MERCHANT);
      ps.setLong(2,hid);
      ps.setDate(3,fromDate.getSqlDate());
      ps.setDate(4,toDate.getSqlDate());
      
      rs = ps.executeQuery();
    }
    catch (Exception e)
    {
      addError("loadYearToDate: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
               "loadYearToDate: " + e.toString());
    }
    
    return rs;
  }
  
  /*
  ** public Hashtable getReportData()
  **
  ** Loads report data.  First a default user node is loaded if a node has not
  ** already been set.  Next the node's direct children are loaded into the
  ** report data hashtable.  Next a first query gathers most of the report 
  ** data and creates report rows which are loaded into their corresponding
  ** subreports.  A second query gathers year-to-date data and updates the 
  ** corresponding subreport rows with the additional data elements.
  **
  ** RETURNS: Hashtable containing a set of subreport objects which in turn
  **          contain individual rows of data.
  */
  public Hashtable getReportData()
  {
    Hashtable reportData  = null;
    
    try
    {
      // if hid is not specified load the default based on the user
      if (hid == -1L)
      {
        hid = getDefaultHid(userId,hierType);
      }

      // get a set of direct children subreports in a hash table
      reportData = loadDirectChildren(hid);
      if (reportData != null)
      {
        // get all activity and load it into the sub reports
        ResultSet rs = loadAppAndPriorMonth();
        if (rs != null)
        {
          while (rs.next())
          {
            ReportRow row = new ReportRow(rs);
            SubReport subReport = null;
            // direct child merchants of the node itself go in subreport -1
            if (row.getEntityType() == MesHierarchy.ET_MERCHANT)
            {
              subReport = (SubReport)reportData.get(-1L);
            }
            // 
            else
            {
              subReport = (SubReport)reportData.get(row.getManagerUserId());
            }
            subReport.add(row);
          }
          
          // now load year-to-date data into rows
          rs = loadYearToDate();
          if (rs != null)
          {
            while (rs.next())
            {
              // create a manager id to lookup the sub report
              Long mgrId = rs.getLong("manager_user_id");
              
              // get a reference to the subreport
              SubReport subReport = null;
              if (rs.getInt("hier_entity_type") == MesHierarchy.ET_MERCHANT)
              {
                // direct child merchants go are in subreport -1
                subReport = (SubReport)reportData.get(-1L);
              }
              else
              {
                subReport = (SubReport)reportData.get(mgrId);
              }
              
              if (subReport != null)
              {
                // get a reference to the row 
                // in the subreport based on merch num
                ReportRow row = (ReportRow)subReport.get(rs.getLong("merch_num"));

                // add the year to date data to the row 
                if (row != null)
                {
                  row.addYearToDate(rs);
                }
              }
            }
          }
        }
      }
    }
    catch (Exception e)
    {
      String desc = "getReportData: " + e.toString();
      System.out.println(desc);
      addError(desc);
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), desc);
    }
    
    return reportData;
  }
  
  /*
  ** METHOD public void setProperties(HttpServletRequest request)
  **
  ** Called by the download servlet to load internal members of bean prior
  ** to a call to executeQuery().
  */
  public void setProperties(HttpServletRequest request)
  {
    try
    {
      setFromMonth    (Integer.parseInt(request.getParameter("fromMonth")));
      setFromYear     (Integer.parseInt(request.getParameter("fromYear")));
      setToMonth      (Integer.parseInt(request.getParameter("toMonth")));
      setToYear       (Integer.parseInt(request.getParameter("toYear")));
    }
    catch (Exception e)
    {
    }
  }
  
  /*
  ** METHOD public String getDownloadFilenameBase()
  **
  ** Called by the download servlet to determine the download filename
  ** (without the extension).  Returns a file name in the format of
  ** "DiscountRevenue<fromDate>to<toDate>".
  **
  ** RETURNS: download filename base.
  */
  public String getDownloadFilenameBase()
  {
    return "DiscountRevenue" + fromDate.toString() + "to" + toDate.toString();
  }
  
  /*
  ** ACCESSORS
  */
  public long getHid()
  {
    return hid;
  }
  public void setHid(long hid)
  {
    this.hid = hid;
  }
  
  public int getHierType()
  {
    return hierType;
  }
  public void setHierType(int hierType)
  {
    this.hierType = hierType;
  }
  
  public int getAppType()
  {
    return appType;
  }
  public void setAppType(int appType)
  {
    this.appType = appType;
  }
  
  public int getCreditStatus()
  {
    return creditStatus;
  }
  public void setCreditStatus(int creditStatus)
  {
    this.creditStatus = creditStatus;
  }
  
  public long getUserId()
  {
    return userId;
  }
  public void setUserId(long userId)
  {
    this.userId = userId;
  }
  
  public BeanDate getPmActiveDate()
  {
    return pmActiveDate;
  }
  
  public String getNodeName()
  {
    if (nodeName == null)
    {
      try
      {
        StringBuffer      qs = new StringBuffer("");
        PreparedStatement ps = null;
        ResultSet         rs = null;

        qs.append("select ");
        qs.append("  name ");
        qs.append("from ");
        qs.append("  t_hierarchy_names ");
        qs.append("where ");
        qs.append("  hier_id = " + hid + " ");
        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();
        if (rs.next())
        {
          nodeName = rs.getString("name");
        }
        else
        {
          nodeName = "unknown";
        }
      }
      catch (Exception e)
      {
        String desc = "getNodeName (hid=" + hid + ") " + e.toString();
        System.out.println(desc);
        addError(desc);
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(),desc);
      }
    }
    
    return nodeName;
  }
  
  private long parentHid = -1L;
  public long getParentHid()
  {
    if (parentHid == -1L)
    {
      try
      {
        StringBuffer      qs = new StringBuffer("");
        PreparedStatement ps = null;
        ResultSet         rs = null;

        // look for an ancestor relationship between user's
        // default (top allowed) node and the current hid
        qs.append("select ");
        qs.append("  count(*) ");
        qs.append("from ");
        qs.append("  t_hierarchy h, ");
        qs.append("  t_hierarchy_defaults hd ");
        qs.append("where ");
        qs.append("  hd.user_id = " + userId + " ");
        qs.append("  and hd.hier_id = h.ancestor ");
        qs.append("  and h.descendent = " + hid + " ");
        qs.append("  and h.relation > 0");
        
        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();
        boolean uplinkOk = false;
        if (rs.next())
        {
          uplinkOk = (rs.getInt(1) > 0);
        }
      
        // if user's highest accessible node is above this node
        // then look up this node's parent
        if (uplinkOk)
        {
          qs.setLength(0);
          qs.append("select ");
          qs.append("  ancestor ");
          qs.append("from ");
          qs.append("  t_hierarchy ");
          qs.append("where ");
          qs.append("  descendent = " + hid + " ");
          qs.append("  and relation = 1");
          ps = getPreparedStatement(qs.toString());
          rs = ps.executeQuery();
          if (rs.next())
          {
            parentHid = rs.getLong("ancestor");
          }
        }
      }
      catch (Exception e)
      {
        String desc = "getParentHid (hid=" + hid + ") " + e.toString();
        System.out.println(desc);
        addError(desc);
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(),desc);
      }
    }
    
    return parentHid;
  }

  /*
  ** Date manipulation routines to adjust the date range beans to reflect a
  ** period of time actually 2 months before the actual selected dates.  This
  ** is because the activation date of an account has to be up to 2 months 
  ** prior in order for a merchant to have been active for at least a month
  ** prior to the selected date range.  This ensures that at least one month
  ** of activity is available for a merchant.
  */
  private BeanDate realFromDate = new BeanDate();
  private BeanDate realToDate   = new BeanDate();
  
  private void adjustFromDate()
  {
    fromDate.setUtilDate(realFromDate.getUtilDate());
    fromDate.setDay(1);
    fromDate.add(Calendar.MONTH,-2);
  }
    
  private void adjustToDate()
  {
    toDate.setUtilDate(realToDate.getUtilDate());
    toDate.add(Calendar.MONTH,-2);
    Calendar cal = Calendar.getInstance();
    cal.setTime(toDate.getUtilDate());
    toDate.setDay(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
  }
  
  private void adjustDates()
  {
    adjustFromDate();
    adjustToDate();
  }
    
  public void setFromDay(int newVal)
  {
    realFromDate.setDay(newVal);
    adjustDates();
  }
  
  public void setFromMonth(int newVal) 
  {
    realFromDate.setMonth(newVal);
    adjustDates();
  }
  
  public void setFromYear(int newVal)
  {
    realFromDate.setYear(newVal);
    adjustDates();
  }
  
  public void setToDay(int newVal)
  {
    realToDate.setDay(newVal);
    adjustDates();
  }
  
  public void setToMonth(int newVal)
  {
    realToDate.setMonth(newVal);
    adjustDates();
  }
  
  public void setToYear(int newVal)
  {
    realToDate.setYear(newVal);
    adjustDates();
  }
  
  public int  getFromDay  ()  { return realFromDate.getDay();   }
  public int  getFromMonth()  { return realFromDate.getMonth(); }
  public int  getFromYear ()  { return realFromDate.getYear();  }
  public int  getToDay    ()  { return realToDate.  getDay();   }
  public int  getToMonth  ()  { return realToDate.  getMonth(); }
  public int  getToYear   ()  { return realToDate.  getYear();  }
}
