/*@lineinfo:filename=AchDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/reports/AchDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: mduyck $
  Last Modified Date : $Date: 2013-04-25 17:14:57 -0700 (Thu, 25 Apr 2013) $
  Version            : $Revision: 21054 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.ach.AchEntryData;
import com.mes.constants.mesConstants;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class AchDataBean extends ReportSQLJBean
{
  public class RowData implements Comparable
  {
    private   String    AchFilename       = null;
    private   String    DataSource        = null;
    private   String    LoadFilename      = null;
    private   double    TotalAmount       = 0.0;
  
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AchFilename   = resultSet.getString("ach_filename");
      DataSource    = resultSet.getString("data_source");
      LoadFilename  = resultSet.getString("load_filename");
      TotalAmount   = resultSet.getDouble("total_amount");
    }
    
    public int compareTo( Object obj )
    {
      RowData   compareObj  = (RowData)obj;
      int       retVal      = 0;
      
      if ( (retVal = LoadFilename.compareTo(compareObj.LoadFilename)) == 0 )
      {
        retVal = DataSource.compareTo(compareObj.DataSource);
      }
      return( retVal );
    }
    
    public String getAchFilename()
    {
      return( AchFilename );
    }
    
    public String getDataSource()
    {
      return( DataSource );
    }
    
    public String getSettlementFilename()
    {
      return( LoadFilename );
    }
    
    public double getTotalAmount()
    {
      return( TotalAmount );
    }
  }
  
  public class SummaryData
  {
    private   double    CreditsAmount     = 0.0;
    private   int       CreditsCount      = 0;
    private   double    DebitsAmount      = 0.0;
    private   int       DebitsCount       = 0;
    private   long      LoadFileId        = 0L;
    private   String    LoadFilename      = null;
    private   double    TotalAmount       = 0.0;
    private   int       TotalCount        = 0;
  
    public SummaryData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      LoadFileId    = resultSet.getLong("load_file_id");
      LoadFilename  = resultSet.getString("load_filename");
      CreditsCount  = resultSet.getInt("credits_count");
      CreditsAmount = resultSet.getDouble("credits_amount");
      DebitsCount   = resultSet.getInt("debits_count");
      DebitsAmount  = resultSet.getDouble("debits_amount");
      TotalCount    = resultSet.getInt("total_count");
      TotalAmount   = resultSet.getDouble("total_amount");
    }
    
    public double getCreditsAmount()
    {
      return( CreditsAmount );
    }
    
    public int getCreditsCount()
    {
      return( CreditsCount );
    }
    
    public double getDebitsAmount()
    {
      return( DebitsAmount );
    }
    
    public int getDebitsCount()
    {
      return( DebitsCount );
    }
    
    public long getLoadFileId()
    {
      return( LoadFileId );
    }
    
    public String getLoadFilename()
    {
      return( LoadFilename );
    }
    
    public double getTotalAmount()
    {
      return( TotalAmount );
    }
    
    public int getTotalCount()
    {
      return( TotalCount );
    }
  }
  
  public AchDataBean( )
  {
    super(true);
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup        fgroup    = null;
  
    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");
    fgroup.add( new HiddenField("achFilename") );
    fgroup.add( new HiddenField("achFileId") );
  }    
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    if ( getReportType() == RT_DETAILS )   
    {
      line.append("\"ACH Filename\",");
      line.append("\"Outgoing Filename\",");
      line.append("\"Data Source\",");
      line.append("\"ACH Amount\"");
    }      
    else  // RT_SUMMARY
    {
      line.append("\"ACH Filename\",");
      line.append("\"Credits Count\",");
      line.append("\"Credits Amount\",");
      line.append("\"Debits Count\",");
      line.append("\"Debits Amount\",");
      line.append("\"Total Amount\"");
    }      
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);
    
    if ( getReportType() == RT_DETAILS )   
    {
      RowData rec = (RowData)obj;
      line.append("\"");
      line.append(rec.getAchFilename());
      line.append("\",\"");
      line.append(rec.getSettlementFilename());
      line.append("\",\"");
      line.append(rec.getDataSource());
      line.append("\",");
      line.append(rec.getTotalAmount());
    }      
    else  // RT_SUMMARY
    {
      SummaryData rec = (SummaryData)obj;
      line.append("\"");
      line.append(rec.getLoadFilename());
      line.append("\",");
      line.append(rec.getCreditsCount());
      line.append(",");
      line.append(rec.getCreditsAmount());
      line.append(",");
      line.append(rec.getDebitsCount());
      line.append(",");
      line.append(rec.getDebitsAmount());
      line.append(",");
      line.append(rec.getTotalAmount());
    }      
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");

    if ( getReportType() == RT_DETAILS )   
    {
      filename.append("ach_file_detail");
    }      
    else  // RT_SUMMARY
    {
      Date beginDate = getReportDateBegin();
      Date endDate   = getReportDateEnd();
    
      filename.append("ach_file_summary_");
      filename.append(DateTimeFormatter.getFormattedDate(beginDate,"MMddyy"));
    
      if ( !beginDate.equals(endDate) )
      {
        filename.append("_to_");
        filename.append(DateTimeFormatter.getFormattedDate(endDate,"MMddyy"));
      }
    }
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          nodeId            = 0L;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      nodeId = orgIdToHierarchyNode(orgId);
      
      if ( getReportType() == RT_DETAILS )
      {
        String  achFilename = getData("achFilename");
        long    achFileId   = getLong("achFileId",-1L);
        
        /*@lineinfo:generated-code*//*@lineinfo:294^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index (atd IDX_ACH_TRID_LOAD_FILE_ID)
//                      index (ats IDX_ACH_TRIDENT_STMT_TRACE)
//                      ordered
//                    */
//                    atd.load_filename                           as ach_filename,
//                    ats.load_filename                           as load_filename,
//                    decode( mb.mbs_batch_type,
//                            :mesConstants.MBS_BT_VISAK, 'Visa-K',
//                            :mesConstants.MBS_BT_PG, 'Payment Gateway',
//                            :mesConstants.MBS_BT_CIELO, 'Cielo',
//                            null                        , 'Vital',
//                                                          '?' ) as data_source,
//                    count(ats.rec_id)                           as batch_count,
//                    sum(decode(substr(ats.transaction_code,-1),'7',-1,1)
//                        * ats.ach_amount)                       as total_amount
//            from    ach_trident_detail      atd,
//                    ach_trident_statement   ats,
//                    mbs_batches             mb
//            where   atd.load_file_id = :achFileId
//                    and atd.post_date between (:beginDate-7) and :endDate
//                    and atd.entry_description in 
//                    ( 
//                      :AchEntryData.ED_MERCH_DEP, 
//                      :AchEntryData.ED_AMEX_DEP,
//                      :AchEntryData.ED_ADJUSTMENTS
//                    )
//                    and ats.post_date between (:beginDate-7) and :endDate
//                    and ats.trace_number = atd.trace_number
//                    and mb.batch_id(+) = ats.rec_id
//            group by atd.load_filename, ats.load_filename, mb.mbs_batch_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    index (atd IDX_ACH_TRID_LOAD_FILE_ID)\n                    index (ats IDX_ACH_TRIDENT_STMT_TRACE)\n                    ordered\n                  */\n                  atd.load_filename                           as ach_filename,\n                  ats.load_filename                           as load_filename,\n                  decode( mb.mbs_batch_type,\n                           :1 , 'Visa-K',\n                           :2 , 'Payment Gateway',\n                           :3 , 'Cielo',\n                          null                        , 'Vital',\n                                                        '?' ) as data_source,\n                  count(ats.rec_id)                           as batch_count,\n                  sum(decode(substr(ats.transaction_code,-1),'7',-1,1)\n                      * ats.ach_amount)                       as total_amount\n          from    ach_trident_detail      atd,\n                  ach_trident_statement   ats,\n                  mbs_batches             mb\n          where   atd.load_file_id =  :4 \n                  and atd.post_date between ( :5 -7) and  :6 \n                  and atd.entry_description in \n                  ( \n                     :7 , \n                     :8 ,\n                     :9 \n                  )\n                  and ats.post_date between ( :10 -7) and  :11 \n                  and ats.trace_number = atd.trace_number\n                  and mb.batch_id(+) = ats.rec_id\n          group by atd.load_filename, ats.load_filename, mb.mbs_batch_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.MBS_BT_VISAK);
   __sJT_st.setInt(2,mesConstants.MBS_BT_PG);
   __sJT_st.setInt(3,mesConstants.MBS_BT_CIELO);
   __sJT_st.setLong(4,achFileId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setString(7,AchEntryData.ED_MERCH_DEP);
   __sJT_st.setString(8,AchEntryData.ED_AMEX_DEP);
   __sJT_st.setString(9,AchEntryData.ED_ADJUSTMENTS);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setDate(11,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:327^9*/
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          ReportRows.addElement( new RowData( resultSet ) );
        }
        resultSet.close();
        it.close();
      }
      else    // assume summary
      {
        /*@lineinfo:generated-code*//*@lineinfo:338^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index(atd idx_ach_trident_dtl_mid_tdate)
//                    */
//                    atd.load_file_id                              as load_file_id,
//                    atd.load_filename                             as load_filename,
//                    sum( decode(tc.debit_credit_indicator,'C',1) )as credits_count,
//                    sum( decode(tc.debit_credit_indicator,
//                                'C',atd.ach_amount) )             as credits_amount,
//                    sum( decode(tc.debit_credit_indicator,'D',1) )as debits_count,            
//                    sum( decode(tc.debit_credit_indicator,
//                                'D',atd.ach_amount) )             as debits_amount,               
//                    count(1)                                      as total_count,
//                    sum( decode(tc.debit_credit_indicator,'C',1,-1) * 
//                         atd.ach_amount )                         as total_amount
//            from    organization            o,
//                    group_merchant          gm,
//                    ach_trident_detail      atd,
//                    ach_detail_tran_code    tc
//            where   o.org_group = :nodeId
//                    and gm.org_num = o.org_num
//                    and atd.merchant_number = gm.merchant_number 
//                    and atd.transmission_date between :beginDate and :endDate
//                    and atd.post_date between (:beginDate-7) and :endDate
//                    and atd.entry_description in 
//                    ( 
//                      :AchEntryData.ED_MERCH_DEP, 
//                      :AchEntryData.ED_AMEX_DEP,
//                      :AchEntryData.ED_ADJUSTMENTS
//                    )
//                    and tc.ach_detail_trans_code = atd.transaction_code
//            group by atd.load_file_id, atd.load_filename
//            order by atd.load_file_id desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    index(atd idx_ach_trident_dtl_mid_tdate)\n                  */\n                  atd.load_file_id                              as load_file_id,\n                  atd.load_filename                             as load_filename,\n                  sum( decode(tc.debit_credit_indicator,'C',1) )as credits_count,\n                  sum( decode(tc.debit_credit_indicator,\n                              'C',atd.ach_amount) )             as credits_amount,\n                  sum( decode(tc.debit_credit_indicator,'D',1) )as debits_count,            \n                  sum( decode(tc.debit_credit_indicator,\n                              'D',atd.ach_amount) )             as debits_amount,               \n                  count(1)                                      as total_count,\n                  sum( decode(tc.debit_credit_indicator,'C',1,-1) * \n                       atd.ach_amount )                         as total_amount\n          from    organization            o,\n                  group_merchant          gm,\n                  ach_trident_detail      atd,\n                  ach_detail_tran_code    tc\n          where   o.org_group =  :1 \n                  and gm.org_num = o.org_num\n                  and atd.merchant_number = gm.merchant_number \n                  and atd.transmission_date between  :2  and  :3 \n                  and atd.post_date between ( :4 -7) and  :5 \n                  and atd.entry_description in \n                  ( \n                     :6 , \n                     :7 ,\n                     :8 \n                  )\n                  and tc.ach_detail_trans_code = atd.transaction_code\n          group by atd.load_file_id, atd.load_filename\n          order by atd.load_file_id desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setString(6,AchEntryData.ED_MERCH_DEP);
   __sJT_st.setString(7,AchEntryData.ED_AMEX_DEP);
   __sJT_st.setString(8,AchEntryData.ED_ADJUSTMENTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:372^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          ReportRows.addElement( new SummaryData( resultSet ) );
        }
        resultSet.close();
        it.close();
      }
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/