/*@lineinfo:filename=AcctICRevenueDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AcctICRevenueDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class AcctICRevenueDataBean extends ReportSQLJBean
{
  public static final int       IC_VISA                       = 0;
  public static final int       IC_MC                         = 1;
  public static final int       IC_DINERS                     = 2;
  public static final int       IC_CARD_TYPES                 = 3;

  public class RowData
  {
    public Date       ActiveDate          = null;
    public int        BetTableId          = 0;
    public String     CardType            = null;
    public int        IcCat               = -1;
    public String     IcCatDesc           = null;
    public double     IcExpense           = 0.0;
    public double     IcRevenueFees       = 0.0;
    public double     IcRevenueSales      = 0.0;
    public double     SalesAmount         = 0.0;
    public int        SalesCount          = 0;
    
    public RowData( Date activeDate, String cardType, int betId, int cat, 
                    String catDesc,  int salesCount, double salesAmount, 
                    double icSales, double icFees, double icExp )
    {
      ActiveDate          = activeDate;
      BetTableId          = betId;
      CardType            = cardType;
      IcCat               = cat;
      IcCatDesc           = catDesc;
      IcExpense           = icExp;
      IcRevenueFees       = icFees;
      IcRevenueSales      = icSales;
      SalesAmount         = salesAmount;
      SalesCount          = salesCount;
    }
    
    public double getIcNet( )
    {
      return( getTotalRevenue() - IcExpense );
    }
    
    public double getTotalRevenue( )
    {
      return( IcRevenueFees + IcRevenueSales );
    }
  }
  
  public AcctICRevenueDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Active Date\",");
    line.append("\"Card Type\",");
    line.append("\"Bet Number\",");
    line.append("\"IC Cat\",");
    line.append("\"IC Desc\",");
    line.append("\"Sales Count\",");
    line.append("\"Sales Amount\",");
    line.append("\"IC Sales Inc\",");
    line.append("\"IC Fees Inc\",");
    line.append("\"IC Total Inc\",");
    line.append("\"IC Expense\",");
    line.append("\"IC Net Revenue\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData       record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( DateTimeFormatter.getFormattedDate(record.ActiveDate,"MM/dd/yyyy") );
    line.append( ",\"" );
    line.append( record.CardType );
    line.append( "\"," );
    line.append( record.BetTableId );
    line.append( "," );
    line.append( record.IcCat );
    line.append( ",\"" );
    line.append( record.IcCatDesc );
    line.append( "\"," );
    line.append( record.SalesCount );
    line.append( "," );
    line.append( record.SalesAmount );
    line.append( "," );
    line.append( record.IcRevenueSales );
    line.append( "," );
    line.append( record.IcRevenueFees );
    line.append( "," );
    line.append( record.getTotalRevenue() );
    line.append( "," );
    line.append( record.IcExpense );
    line.append( "," );
    line.append( record.getIcNet() );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_ic_revenue_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin, "MMddyyyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd, "MMddyyyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    String                        cardType          = null;
    int                           catCount          = 0;
    NumberFormat                  catFormat         = NumberFormat.getNumberInstance();
    double                        feesInc           = 0.0;
    String                        fieldPrefix       = null;
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    double                        revenue           = 0.0;
    double                        salesAmount       = 0.0;
    int                           salesCount        = 0;
    double                        salesInc          = 0.0;
    
    try
    {
      catFormat.setMinimumIntegerDigits(2);
    
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:197^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                                               as org_num,
//                  o.org_group                                             as hierarchy_node,
//                  gn.hh_active_date                                       as active_date,
//                  vs.v1_bet_table_number                                  as vs_bet_table,
//                  mc.m1_bet_table_number                                  as mc_bet_table,
//                  sum( vs.V1_INCOME_BULLETIN )                            as VS_INC_BULLETIN,
//                  sum( vs.V1_INCOME_INTRA_CHANGE )                        as VS_INC_INTRA_CHANGE,
//                  sum( vs.V1_INCOME_ASSESSMENT )                          as VS_INC_ASSESSMENT,
//                  sum( vs.V1_INC_CAT_01_SALES  )                          as VS_CAT_01_SALES_INC,
//                  sum( vs.V1_INC_CAT_02_SALES  )                          as VS_CAT_02_SALES_INC,
//                  sum( vs.V1_INC_CAT_03_SALES  )                          as VS_CAT_03_SALES_INC,
//                  sum( vs.V1_INC_CAT_04_SALES  )                          as VS_CAT_04_SALES_INC,
//                  sum( vs.V1_INC_CAT_05_SALES  )                          as VS_CAT_05_SALES_INC,
//                  sum( vs.V1_INC_CAT_06_SALES  )                          as VS_CAT_06_SALES_INC,
//                  sum( vs.V1_INC_CAT_07_SALES  )                          as VS_CAT_07_SALES_INC,
//                  sum( vs.V1_INC_CAT_08_SALES  )                          as VS_CAT_08_SALES_INC,
//                  sum( vs.V1_INC_CAT_09_SALES  )                          as VS_CAT_09_SALES_INC,
//                  sum( vs.V1_INC_CAT_10_SALES  )                          as VS_CAT_10_SALES_INC,
//                  sum( vs.V1_INC_CAT_11_SALES  )                          as VS_CAT_11_SALES_INC,
//                  sum( vs.V1_INC_CAT_12_SALES  )                          as VS_CAT_12_SALES_INC,
//                  sum( vs.V1_INC_CAT_13_SALES  )                          as VS_CAT_13_SALES_INC,
//                  sum( vs.V1_INC_CAT_14_SALES  )                          as VS_CAT_14_SALES_INC,
//                  sum( vs.V1_INC_CAT_15_SALES  )                          as VS_CAT_15_SALES_INC,
//                  sum( vs.V1_INC_CAT_16_SALES  )                          as VS_CAT_16_SALES_INC,
//                  sum( vs.V1_INC_CAT_17_SALES  )                          as VS_CAT_17_SALES_INC,
//                  sum( vs.V2_INC_CAT_18_SALES  )                          as VS_CAT_18_SALES_INC,
//                  sum( vs.V2_INC_CAT_19_SALES  )                          as VS_CAT_19_SALES_INC,
//                  sum( vs.V2_INC_CAT_20_SALES  )                          as VS_CAT_20_SALES_INC,
//                  sum( vs.V2_INC_CAT_21_SALES  )                          as VS_CAT_21_SALES_INC,
//                  sum( vs.V2_INC_CAT_22_SALES  )                          as VS_CAT_22_SALES_INC,
//                  sum( vs.V2_INC_CAT_23_SALES  )                          as VS_CAT_23_SALES_INC,
//                  sum( vs.V2_INC_CAT_24_SALES  )                          as VS_CAT_24_SALES_INC,
//                  0 /* no cat 25 sales */                                 as VS_CAT_25_SALES_INC,
//                  sum( vs.V1_INC_CAT_01_FEES )                            as VS_CAT_01_FEES_INC,
//                  sum( vs.V1_INC_CAT_02_FEES )                            as VS_CAT_02_FEES_INC,
//                  sum( vs.V1_INC_CAT_03_FEES )                            as VS_CAT_03_FEES_INC,
//                  sum( vs.V1_INC_CAT_04_FEES )                            as VS_CAT_04_FEES_INC,
//                  sum( vs.V1_INC_CAT_05_FEES )                            as VS_CAT_05_FEES_INC,
//                  sum( vs.V1_INC_CAT_06_FEES )                            as VS_CAT_06_FEES_INC,
//                  sum( vs.V1_INC_CAT_07_FEES )                            as VS_CAT_07_FEES_INC,
//                  sum( vs.V1_INC_CAT_08_FEES )                            as VS_CAT_08_FEES_INC,
//                  sum( vs.V1_INC_CAT_09_FEES )                            as VS_CAT_09_FEES_INC,
//                  sum( vs.V1_INC_CAT_10_FEES )                            as VS_CAT_10_FEES_INC,
//                  sum( vs.V1_INC_CAT_11_FEES )                            as VS_CAT_11_FEES_INC,
//                  sum( vs.V1_INC_CAT_12_FEES )                            as VS_CAT_12_FEES_INC,
//                  sum( vs.V1_INC_CAT_13_FEES )                            as VS_CAT_13_FEES_INC,
//                  sum( vs.V1_INC_CAT_14_FEES )                            as VS_CAT_14_FEES_INC,
//                  sum( vs.V1_INC_CAT_15_FEES )                            as VS_CAT_15_FEES_INC,
//                  sum( vs.V1_INC_CAT_16_FEES )                            as VS_CAT_16_FEES_INC,
//                  sum( vs.V2_INC_CAT_17_FEES )                            as VS_CAT_17_FEES_INC,
//                  sum( vs.V2_INC_CAT_18_FEES )                            as VS_CAT_18_FEES_INC,
//                  sum( vs.V2_INC_CAT_19_FEES )                            as VS_CAT_19_FEES_INC,
//                  sum( vs.V2_INC_CAT_20_FEES )                            as VS_CAT_20_FEES_INC,
//                  sum( vs.V2_INC_CAT_21_FEES )                            as VS_CAT_21_FEES_INC,
//                  sum( vs.V2_INC_CAT_22_FEES )                            as VS_CAT_22_FEES_INC,
//                  sum( vs.V2_INC_CAT_23_FEES )                            as VS_CAT_23_FEES_INC,
//                  sum( vs.V2_INC_CAT_24_FEES )                            as VS_CAT_24_FEES_INC,
//                  sum( vs.V2_INC_CAT_25_FEES )                            as VS_CAT_25_FEES_INC,
//                  sum( vs.V3_EXPENSE_BULLETIN )                           as VS_EXP_BULLETIN,
//                  sum( vs.V3_EXPENSE_INTRA_CHANGE )                       as VS_EXP_INTRA_CHANGE,
//                  sum( vs.V3_EXPENSE_ASSESSMENT )                         as VS_EXP_ASSESSMENT,
//                  sum( mc.M1_INCOME_BULLETIN )                            as MC_INC_BULLETIN,
//                  sum( mc.M1_INCOME_INTRA_CHANGE )                        as MC_INC_INTRA_CHANGE,
//                  sum( mc.M1_INCOME_ASSESSMENT )                          as MC_INC_ASSESSMENT,
//                  sum( mc.M1_INC_CAT_01_SALES )                           as MC_CAT_01_SALES_INC,
//                  sum( mc.M1_INC_CAT_02_SALES )                           as MC_CAT_02_SALES_INC,
//                  sum( mc.M1_INC_CAT_03_SALES )                           as MC_CAT_03_SALES_INC,
//                  sum( mc.M1_INC_CAT_04_SALES )                           as MC_CAT_04_SALES_INC,
//                  sum( mc.M1_INC_CAT_05_SALES )                           as MC_CAT_05_SALES_INC,
//                  sum( mc.M1_INC_CAT_06_SALES )                           as MC_CAT_06_SALES_INC,
//                  sum( mc.M1_INC_CAT_07_SALES )                           as MC_CAT_07_SALES_INC,
//                  sum( mc.M1_INC_CAT_08_SALES )                           as MC_CAT_08_SALES_INC,
//                  sum( mc.M1_INC_CAT_09_SALES )                           as MC_CAT_09_SALES_INC,
//                  sum( mc.M1_INC_CAT_10_SALES )                           as MC_CAT_10_SALES_INC,
//                  sum( mc.M1_INC_CAT_11_SALES )                           as MC_CAT_11_SALES_INC,
//                  sum( mc.M1_INC_CAT_12_SALES )                           as MC_CAT_12_SALES_INC,
//                  sum( mc.M1_INC_CAT_01_FEES )                            as MC_CAT_01_FEES_INC,
//                  sum( mc.M1_INC_CAT_02_FEES )                            as MC_CAT_02_FEES_INC,
//                  sum( mc.M1_INC_CAT_03_FEES )                            as MC_CAT_03_FEES_INC,
//                  sum( mc.M1_INC_CAT_04_FEES )                            as MC_CAT_04_FEES_INC,
//                  sum( mc.M1_INC_CAT_05_FEES )                            as MC_CAT_05_FEES_INC,
//                  sum( mc.M1_INC_CAT_06_FEES )                            as MC_CAT_06_FEES_INC,
//                  sum( mc.M1_INC_CAT_07_FEES )                            as MC_CAT_07_FEES_INC,
//                  sum( mc.M1_INC_CAT_08_FEES )                            as MC_CAT_08_FEES_INC,
//                  sum( mc.M1_INC_CAT_09_FEES )                            as MC_CAT_09_FEES_INC,
//                  sum( mc.M1_INC_CAT_10_FEES )                            as MC_CAT_10_FEES_INC,
//                  sum( mc.M1_INC_CAT_11_FEES )                            as MC_CAT_11_FEES_INC,
//                  sum( mc.M1_INC_CAT_12_FEES )                            as MC_CAT_12_FEES_INC,
//                  sum( mc.M2_EXPENSE_BULLETIN )                           as MC_EXP_BULLETIN,
//                  sum( mc.M2_EXPENSE_INTRA_CHANGE )                       as MC_EXP_INTRA_CHANGE,
//                  sum( mc.M2_EXPENSE_ASSESSMENT )                         as MC_EXP_ASSESSMENT,
//                  sum( vs.V5_CAT_01_SALES_COUNT )                         as VS_CAT_01_SALES_COUNT,
//                  sum( vs.V5_CAT_01_SALES_AMOUNT )                        as VS_CAT_01_SALES_AMOUNT,
//                  sum( vs.V5_CAT_01_CRED_COUNT )                          as VS_CAT_01_CRED_COUNT,
//                  sum( vs.V5_CAT_01_CRED_AMOUNT )                         as VS_CAT_01_CRED_AMOUNT,
//                  sum( vs.V5_CAT_01_INTCH )                               as VS_CAT_01_INTCH,
//                  sum( vs.V5_CAT_02_SALES_COUNT )                         as VS_CAT_02_SALES_COUNT,
//                  sum( vs.V5_CAT_02_SALES_AMOUNT )                        as VS_CAT_02_SALES_AMOUNT,
//                  sum( vs.V5_CAT_02_CRED_COUNT )                          as VS_CAT_02_CRED_COUNT,
//                  sum( vs.V5_CAT_02_CRED_AMOUNT )                         as VS_CAT_02_CRED_AMOUNT,
//                  sum( vs.V5_CAT_02_INTCH )                               as VS_CAT_02_INTCH,
//                  sum( vs.V5_CAT_03_SALES_COUNT )                         as VS_CAT_03_SALES_COUNT,
//                  sum( vs.V5_CAT_03_SALES_AMOUNT )                        as VS_CAT_03_SALES_AMOUNT,
//                  sum( vs.V5_CAT_03_CRED_COUNT )                          as VS_CAT_03_CRED_COUNT,
//                  sum( vs.V5_CAT_03_CRED_AMOUNT )                         as VS_CAT_03_CRED_AMOUNT,
//                  sum( vs.V5_CAT_03_INTCH )                               as VS_CAT_03_INTCH,
//                  sum( vs.V5_CAT_04_SALES_COUNT )                         as VS_CAT_04_SALES_COUNT,
//                  sum( vs.V5_CAT_04_SALES_AMOUNT )                        as VS_CAT_04_SALES_AMOUNT,
//                  sum( vs.V5_CAT_04_CRED_COUNT )                          as VS_CAT_04_CRED_COUNT,
//                  sum( vs.V5_CAT_04_CRED_AMOUNT )                         as VS_CAT_04_CRED_AMOUNT,
//                  sum( vs.V5_CAT_04_INTCH )                               as VS_CAT_04_INTCH,
//                  sum( vs.V5_CAT_05_SALES_COUNT )                         as VS_CAT_05_SALES_COUNT,
//                  sum( vs.V5_CAT_05_SALES_AMOUNT )                        as VS_CAT_05_SALES_AMOUNT,
//                  sum( vs.V5_CAT_05_CRED_COUNT )                          as VS_CAT_05_CRED_COUNT,
//                  sum( vs.V5_CAT_05_CRED_AMOUNT )                         as VS_CAT_05_CRED_AMOUNT,
//                  sum( vs.V5_CAT_05_INTCH )                               as VS_CAT_05_INTCH,
//                  sum( vs.V5_CAT_06_SALES_COUNT )                         as VS_CAT_06_SALES_COUNT,
//                  sum( vs.V5_CAT_06_SALES_AMOUNT )                        as VS_CAT_06_SALES_AMOUNT,
//                  sum( vs.V5_CAT_06_CRED_COUNT )                          as VS_CAT_06_CRED_COUNT,
//                  sum( vs.V5_CAT_06_CRED_AMOUNT )                         as VS_CAT_06_CRED_AMOUNT,
//                  sum( vs.V6_CAT_06_INTCH )                               as VS_CAT_06_INTCH,
//                  sum( vs.V6_CAT_07_SALES_COUNT )                         as VS_CAT_07_SALES_COUNT,
//                  sum( vs.V6_CAT_07_SALES_AMOUNT )                        as VS_CAT_07_SALES_AMOUNT,
//                  sum( vs.V6_CAT_07_CRED_COUNT )                          as VS_CAT_07_CRED_COUNT,
//                  sum( vs.V6_CAT_07_CRED_AMOUNT )                         as VS_CAT_07_CRED_AMOUNT,
//                  sum( vs.V6_CAT_07_INTCH )                               as VS_CAT_07_INTCH,
//                  sum( vs.V6_CAT_08_SALES_COUNT )                         as VS_CAT_08_SALES_COUNT,
//                  sum( vs.V6_CAT_08_SALES_AMOUNT )                        as VS_CAT_08_SALES_AMOUNT,
//                  sum( vs.V6_CAT_08_CRED_COUNT )                          as VS_CAT_08_CRED_COUNT,
//                  sum( vs.V6_CAT_08_CRED_AMOUNT )                         as VS_CAT_08_CRED_AMOUNT,
//                  sum( vs.V6_CAT_08_INTCH )                               as VS_CAT_08_INTCH,
//                  sum( vs.V6_CAT_09_SALES_COUNT )                         as VS_CAT_09_SALES_COUNT,
//                  sum( vs.V6_CAT_09_SALES_AMOUNT )                        as VS_CAT_09_SALES_AMOUNT,
//                  sum( vs.V6_CAT_09_CRED_COUNT )                          as VS_CAT_09_CRED_COUNT,
//                  sum( vs.V6_CAT_09_CRED_AMOUNT )                         as VS_CAT_09_CRED_AMOUNT,
//                  sum( vs.V6_CAT_09_INTCH )                               as VS_CAT_09_INTCH,
//                  sum( vs.V6_CAT_10_SALES_COUNT )                         as VS_CAT_10_SALES_COUNT,
//                  sum( vs.V6_CAT_10_SALES_AMOUNT )                        as VS_CAT_10_SALES_AMOUNT,
//                  sum( vs.V6_CAT_10_CRED_COUNT )                          as VS_CAT_10_CRED_COUNT,
//                  sum( vs.V6_CAT_10_CRED_AMOUNT )                         as VS_CAT_10_CRED_AMOUNT,
//                  sum( vs.V6_CAT_10_INTCH )                               as VS_CAT_10_INTCH,
//                  sum( vs.V6_CAT_11_SALES_COUNT )                         as VS_CAT_11_SALES_COUNT,
//                  sum( vs.V6_CAT_11_SALES_AMOUNT )                        as VS_CAT_11_SALES_AMOUNT,
//                  sum( vs.V6_CAT_11_CRED_COUNT )                          as VS_CAT_11_CRED_COUNT,
//                  sum( vs.V6_CAT_11_CRED_AMOUNT )                         as VS_CAT_11_CRED_AMOUNT,
//                  sum( vs.V6_CAT_11_INTCH )                               as VS_CAT_11_INTCH,
//                  sum( vs.V6_CAT_12_SALES_COUNT )                         as VS_CAT_12_SALES_COUNT,
//                  sum( vs.V6_CAT_12_SALES_AMOUNT )                        as VS_CAT_12_SALES_AMOUNT,
//                  sum( vs.V6_CAT_12_CRED_COUNT )                          as VS_CAT_12_CRED_COUNT,
//                  sum( vs.V7_CAT_12_CRED_AMOUNT )                         as VS_CAT_12_CRED_AMOUNT,
//                  sum( vs.V7_CAT_12_INTCH )                               as VS_CAT_12_INTCH,
//                  sum( vs.V7_CAT_13_SALES_COUNT )                         as VS_CAT_13_SALES_COUNT,
//                  sum( vs.V7_CAT_13_SALES_AMOUNT )                        as VS_CAT_13_SALES_AMOUNT,
//                  sum( vs.V7_CAT_13_CRED_COUNT )                          as VS_CAT_13_CRED_COUNT,
//                  sum( vs.V7_CAT_13_CRED_AMOUNT )                         as VS_CAT_13_CRED_AMOUNT,
//                  sum( vs.V7_CAT_13_INTCH )                               as VS_CAT_13_INTCH,
//                  sum( vs.V7_CAT_14_SALES_COUNT )                         as VS_CAT_14_SALES_COUNT,
//                  sum( vs.V7_CAT_14_SALES_AMOUNT )                        as VS_CAT_14_SALES_AMOUNT,
//                  sum( vs.V7_CAT_14_CRED_COUNT )                          as VS_CAT_14_CRED_COUNT,
//                  sum( vs.V7_CAT_14_CRED_AMOUNT )                         as VS_CAT_14_CRED_AMOUNT,
//                  sum( vs.V7_CAT_14_INTCH )                               as VS_CAT_14_INTCH,
//                  sum( vs.V7_CAT_15_SALES_COUNT )                         as VS_CAT_15_SALES_COUNT,
//                  sum( vs.V7_CAT_15_SALES_AMOUNT )                        as VS_CAT_15_SALES_AMOUNT,
//                  sum( vs.V7_CAT_15_CRED_COUNT )                          as VS_CAT_15_CRED_COUNT,
//                  sum( vs.V7_CAT_15_CRED_AMOUNT )                         as VS_CAT_15_CRED_AMOUNT,
//                  sum( vs.V7_CAT_15_INTCH )                               as VS_CAT_15_INTCH,
//                  sum( vs.V7_CAT_16_SALES_COUNT )                         as VS_CAT_16_SALES_COUNT,
//                  sum( vs.V7_CAT_16_SALES_AMOUNT )                        as VS_CAT_16_SALES_AMOUNT,
//                  sum( vs.V7_CAT_16_CRED_COUNT )                          as VS_CAT_16_CRED_COUNT,
//                  sum( vs.V7_CAT_16_CRED_AMOUNT )                         as VS_CAT_16_CRED_AMOUNT,
//                  sum( vs.V7_CAT_16_INTCH )                               as VS_CAT_16_INTCH,
//                  sum( vs.V7_CAT_17_SALES_COUNT )                         as VS_CAT_17_SALES_COUNT,
//                  sum( vs.V7_CAT_17_SALES_AMOUNT )                        as VS_CAT_17_SALES_AMOUNT,
//                  sum( vs.V7_CAT_17_CRED_COUNT )                          as VS_CAT_17_CRED_COUNT,
//                  sum( vs.V7_CAT_17_CRED_AMOUNT )                         as VS_CAT_17_CRED_AMOUNT,
//                  sum( vs.V7_CAT_17_INTCH )                               as VS_CAT_17_INTCH,
//                  sum( vs.V7_CAT_18_SALES_COUNT )                         as VS_CAT_18_SALES_COUNT,
//                  sum( vs.V8_CAT_18_SALES_AMOUNT )                        as VS_CAT_18_SALES_AMOUNT,
//                  sum( vs.V8_CAT_18_CRED_COUNT )                          as VS_CAT_18_CRED_COUNT,
//                  sum( vs.V8_CAT_18_CRED_AMOUNT )                         as VS_CAT_18_CRED_AMOUNT,
//                  sum( vs.V8_CAT_18_INTCH )                               as VS_CAT_18_INTCH,
//                  sum( vs.V8_CAT_19_SALES_COUNT )                         as VS_CAT_19_SALES_COUNT,
//                  sum( vs.V8_CAT_19_SALES_AMOUNT )                        as VS_CAT_19_SALES_AMOUNT,
//                  sum( vs.V8_CAT_19_CRED_COUNT )                          as VS_CAT_19_CRED_COUNT,
//                  sum( vs.V8_CAT_19_CRED_AMOUNT )                         as VS_CAT_19_CRED_AMOUNT,
//                  sum( vs.V8_CAT_19_INTCH )                               as VS_CAT_19_INTCH,
//                  sum( vs.V8_CAT_20_SALES_COUNT )                         as VS_CAT_20_SALES_COUNT,
//                  sum( vs.V8_CAT_20_SALES_AMOUNT )                        as VS_CAT_20_SALES_AMOUNT,
//                  sum( vs.V8_CAT_20_CRED_COUNT )                          as VS_CAT_20_CRED_COUNT,
//                  sum( vs.V8_CAT_20_CRED_AMOUNT )                         as VS_CAT_20_CRED_AMOUNT,
//                  sum( vs.V8_CAT_20_INTCH )                               as VS_CAT_20_INTCH,
//                  sum( vs.V8_CAT_21_SALES_COUNT )                         as VS_CAT_21_SALES_COUNT,
//                  sum( vs.V8_CAT_21_SALES_AMOUNT )                        as VS_CAT_21_SALES_AMOUNT,
//                  sum( vs.V8_CAT_21_CRED_COUNT )                          as VS_CAT_21_CRED_COUNT,
//                  sum( vs.V8_CAT_21_CRED_AMOUNT )                         as VS_CAT_21_CRED_AMOUNT,
//                  sum( vs.V8_CAT_21_INTCH )                               as VS_CAT_21_INTCH,
//                  sum( vs.V8_CAT_22_SALES_COUNT )                         as VS_CAT_22_SALES_COUNT,
//                  sum( vs.V8_CAT_22_SALES_AMOUNT )                        as VS_CAT_22_SALES_AMOUNT,
//                  sum( vs.V8_CAT_22_CRED_COUNT )                          as VS_CAT_22_CRED_COUNT,
//                  sum( vs.V8_CAT_22_CRED_AMOUNT )                         as VS_CAT_22_CRED_AMOUNT,
//                  sum( vs.V8_CAT_22_INTCH )                               as VS_CAT_22_INTCH,
//                  sum( vs.V8_CAT_23_SALES_COUNT )                         as VS_CAT_23_SALES_COUNT,
//                  sum( vs.V8_CAT_23_SALES_AMOUNT )                        as VS_CAT_23_SALES_AMOUNT,
//                  sum( vs.V8_CAT_23_CRED_COUNT )                          as VS_CAT_23_CRED_COUNT,
//                  sum( vs.V8_CAT_23_CRED_AMOUNT )                         as VS_CAT_23_CRED_AMOUNT,
//                  sum( vs.V9_CAT_23_INTCH )                               as VS_CAT_23_INTCH,
//                  sum( vs.V9_CAT_24_SALES_COUNT )                         as VS_CAT_24_SALES_COUNT,
//                  sum( vs.V9_CAT_24_SALES_AMOUNT )                        as VS_CAT_24_SALES_AMOUNT,
//                  sum( vs.V9_CAT_24_CRED_COUNT )                          as VS_CAT_24_CRED_COUNT,
//                  sum( vs.V9_CAT_24_CRED_AMOUNT )                         as VS_CAT_24_CRED_AMOUNT,
//                  sum( vs.V9_CAT_24_INTCH )                               as VS_CAT_24_INTCH,
//                  0                                                       as VS_CAT_25_SALES_COUNT,
//                  0                                                       as VS_CAT_25_SALES_AMOUNT,
//                  0                                                       as VS_CAT_25_CRED_COUNT,
//                  0                                                       as VS_CAT_25_CRED_AMOUNT,
//                  sum( vs.V9_CAT_25_INTCH )                               as VS_CAT_25_INTCH,
//                  sum( mc.M3_CAT_01_SALES_COUNT )                         as MC_CAT_01_SALES_COUNT,
//                  sum( mc.M3_CAT_01_SALES_AMOUNT )                        as MC_CAT_01_SALES_AMOUNT,
//                  sum( mc.M3_CAT_01_CRED_COUNT )                          as MC_CAT_01_CRED_COUNT,
//                  sum( mc.M3_CAT_01_CRED_AMOUNT )                         as MC_CAT_01_CRED_AMOUNT,
//                  sum( mc.M3_CAT_01_INTCH )                               as MC_CAT_01_INTCH,
//                  sum( mc.M3_CAT_02_SALES_COUNT )                         as MC_CAT_02_SALES_COUNT,
//                  sum( mc.M3_CAT_02_SALES_AMOUNT )                        as MC_CAT_02_SALES_AMOUNT,
//                  sum( mc.M3_CAT_02_CRED_COUNT )                          as MC_CAT_02_CRED_COUNT,
//                  sum( mc.M3_CAT_02_CRED_AMOUNT )                         as MC_CAT_02_CRED_AMOUNT,
//                  sum( mc.M3_CAT_02_INTCH )                               as MC_CAT_02_INTCH,
//                  sum( mc.M3_CAT_03_SALES_COUNT )                         as MC_CAT_03_SALES_COUNT,
//                  sum( mc.M3_CAT_03_SALES_AMOUNT )                        as MC_CAT_03_SALES_AMOUNT,
//                  sum( mc.M3_CAT_03_CRED_COUNT )                          as MC_CAT_03_CRED_COUNT,
//                  sum( mc.M3_CAT_03_CRED_AMOUNT )                         as MC_CAT_03_CRED_AMOUNT,
//                  sum( mc.M3_CAT_03_INTCH )                               as MC_CAT_03_INTCH,
//                  sum( mc.M3_CAT_04_SALES_COUNT )                         as MC_CAT_04_SALES_COUNT,
//                  sum( mc.M3_CAT_04_SALES_AMOUNT )                        as MC_CAT_04_SALES_AMOUNT,
//                  sum( mc.M3_CAT_04_CRED_COUNT )                          as MC_CAT_04_CRED_COUNT,
//                  sum( mc.M3_CAT_04_CRED_AMOUNT )                         as MC_CAT_04_CRED_AMOUNT,
//                  sum( mc.M3_CAT_04_INTCH )                               as MC_CAT_04_INTCH,
//                  sum( mc.M3_CAT_05_SALES_COUNT )                         as MC_CAT_05_SALES_COUNT,
//                  sum( mc.M3_CAT_05_SALES_AMOUNT )                        as MC_CAT_05_SALES_AMOUNT,
//                  sum( mc.M3_CAT_05_CRED_COUNT )                          as MC_CAT_05_CRED_COUNT,
//                  sum( mc.M3_CAT_05_CRED_AMOUNT )                         as MC_CAT_05_CRED_AMOUNT,
//                  sum( mc.M3_CAT_05_INTCH )                               as MC_CAT_05_INTCH,
//                  sum( mc.M3_CAT_06_SALES_COUNT )                         as MC_CAT_06_SALES_COUNT,
//                  sum( mc.M3_CAT_06_SALES_AMOUNT )                        as MC_CAT_06_SALES_AMOUNT,
//                  sum( mc.M3_CAT_06_CRED_COUNT )                          as MC_CAT_06_CRED_COUNT,
//                  sum( mc.M3_CAT_06_CRED_AMOUNT )                         as MC_CAT_06_CRED_AMOUNT,
//                  sum( mc.M4_CAT_06_INTCH )                               as MC_CAT_06_INTCH,
//                  sum( mc.M4_CAT_07_SALES_COUNT )                         as MC_CAT_07_SALES_COUNT,
//                  sum( mc.M4_CAT_07_SALES_AMOUNT )                        as MC_CAT_07_SALES_AMOUNT,
//                  sum( mc.M4_CAT_07_CRED_COUNT )                          as MC_CAT_07_CRED_COUNT,
//                  sum( mc.M4_CAT_07_CRED_AMOUNT )                         as MC_CAT_07_CRED_AMOUNT,
//                  sum( mc.M4_CAT_07_INTCH )                               as MC_CAT_07_INTCH,
//                  sum( mc.M4_CAT_08_SALES_COUNT )                         as MC_CAT_08_SALES_COUNT,
//                  sum( mc.M4_CAT_08_SALES_AMOUNT )                        as MC_CAT_08_SALES_AMOUNT,
//                  sum( mc.M4_CAT_08_CRED_COUNT )                          as MC_CAT_08_CRED_COUNT,
//                  sum( mc.M4_CAT_08_CRED_AMOUNT )                         as MC_CAT_08_CRED_AMOUNT,
//                  sum( mc.M4_CAT_08_INTCH )                               as MC_CAT_08_INTCH,
//                  sum( mc.M4_CAT_09_SALES_COUNT )                         as MC_CAT_09_SALES_COUNT,
//                  sum( mc.M4_CAT_09_SALES_AMOUNT )                        as MC_CAT_09_SALES_AMOUNT,
//                  sum( mc.M4_CAT_09_CRED_COUNT )                          as MC_CAT_09_CRED_COUNT,
//                  sum( mc.M4_CAT_09_CRED_AMOUNT )                         as MC_CAT_09_CRED_AMOUNT,
//                  sum( mc.M4_CAT_09_INTCH )                               as MC_CAT_09_INTCH,
//                  sum( mc.M4_CAT_10_SALES_COUNT )                         as MC_CAT_10_SALES_COUNT,
//                  sum( mc.M4_CAT_10_SALES_AMOUNT )                        as MC_CAT_10_SALES_AMOUNT,
//                  sum( mc.M4_CAT_10_CRED_COUNT )                          as MC_CAT_10_CRED_COUNT,
//                  sum( mc.M4_CAT_10_CRED_AMOUNT )                         as MC_CAT_10_CRED_AMOUNT,
//                  sum( mc.M4_CAT_10_INTCH )                               as MC_CAT_10_INTCH,
//                  sum( mc.M4_CAT_11_SALES_COUNT )                         as MC_CAT_11_SALES_COUNT,
//                  sum( mc.M4_CAT_11_SALES_AMOUNT )                        as MC_CAT_11_SALES_AMOUNT,
//                  sum( mc.M4_CAT_11_CRED_COUNT )                          as MC_CAT_11_CRED_COUNT,
//                  sum( mc.M4_CAT_11_CRED_AMOUNT )                         as MC_CAT_11_CRED_AMOUNT,
//                  sum( mc.M4_CAT_11_INTCH )                               as MC_CAT_11_INTCH,
//                  sum( mc.M4_CAT_12_SALES_COUNT )                         as MC_CAT_12_SALES_COUNT,
//                  sum( mc.M4_CAT_12_SALES_AMOUNT )                        as MC_CAT_12_SALES_AMOUNT,
//                  sum( mc.M4_CAT_12_CRED_COUNT )                          as MC_CAT_12_CRED_COUNT,
//                  sum( mc.M5_CAT_12_CRED_AMOUNT )                         as MC_CAT_12_CRED_AMOUNT,
//                  sum( mc.M5_CAT_12_INTCH )                               as MC_CAT_12_INTCH
//          from    parent_org              po,
//                  organization            o,
//                  group_merchant          gm,
//                  group_rep_merchant      grm,
//                  monthly_extract_gn      gn,
//                  monthly_extract_visa    vs,
//                  monthly_extract_mc      mc
//          where   po.parent_org_num = :orgId and
//                  o.org_num = po.org_num and
//                  gm.org_num = o.org_num and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  gn.hh_active_date between :beginDate and :endDate and
//                  gn.hh_merchant_number = gm.merchant_number and
//                  vs.hh_load_sec(+) = gn.hh_load_sec and
//                  mc.hh_load_sec(+) = gn.hh_load_sec
//          group by o.org_num, o.org_group, gn.hh_active_date, 
//                    vs.v1_bet_table_number, mc.m1_bet_table_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                                               as org_num,\n                o.org_group                                             as hierarchy_node,\n                gn.hh_active_date                                       as active_date,\n                vs.v1_bet_table_number                                  as vs_bet_table,\n                mc.m1_bet_table_number                                  as mc_bet_table,\n                sum( vs.V1_INCOME_BULLETIN )                            as VS_INC_BULLETIN,\n                sum( vs.V1_INCOME_INTRA_CHANGE )                        as VS_INC_INTRA_CHANGE,\n                sum( vs.V1_INCOME_ASSESSMENT )                          as VS_INC_ASSESSMENT,\n                sum( vs.V1_INC_CAT_01_SALES  )                          as VS_CAT_01_SALES_INC,\n                sum( vs.V1_INC_CAT_02_SALES  )                          as VS_CAT_02_SALES_INC,\n                sum( vs.V1_INC_CAT_03_SALES  )                          as VS_CAT_03_SALES_INC,\n                sum( vs.V1_INC_CAT_04_SALES  )                          as VS_CAT_04_SALES_INC,\n                sum( vs.V1_INC_CAT_05_SALES  )                          as VS_CAT_05_SALES_INC,\n                sum( vs.V1_INC_CAT_06_SALES  )                          as VS_CAT_06_SALES_INC,\n                sum( vs.V1_INC_CAT_07_SALES  )                          as VS_CAT_07_SALES_INC,\n                sum( vs.V1_INC_CAT_08_SALES  )                          as VS_CAT_08_SALES_INC,\n                sum( vs.V1_INC_CAT_09_SALES  )                          as VS_CAT_09_SALES_INC,\n                sum( vs.V1_INC_CAT_10_SALES  )                          as VS_CAT_10_SALES_INC,\n                sum( vs.V1_INC_CAT_11_SALES  )                          as VS_CAT_11_SALES_INC,\n                sum( vs.V1_INC_CAT_12_SALES  )                          as VS_CAT_12_SALES_INC,\n                sum( vs.V1_INC_CAT_13_SALES  )                          as VS_CAT_13_SALES_INC,\n                sum( vs.V1_INC_CAT_14_SALES  )                          as VS_CAT_14_SALES_INC,\n                sum( vs.V1_INC_CAT_15_SALES  )                          as VS_CAT_15_SALES_INC,\n                sum( vs.V1_INC_CAT_16_SALES  )                          as VS_CAT_16_SALES_INC,\n                sum( vs.V1_INC_CAT_17_SALES  )                          as VS_CAT_17_SALES_INC,\n                sum( vs.V2_INC_CAT_18_SALES  )                          as VS_CAT_18_SALES_INC,\n                sum( vs.V2_INC_CAT_19_SALES  )                          as VS_CAT_19_SALES_INC,\n                sum( vs.V2_INC_CAT_20_SALES  )                          as VS_CAT_20_SALES_INC,\n                sum( vs.V2_INC_CAT_21_SALES  )                          as VS_CAT_21_SALES_INC,\n                sum( vs.V2_INC_CAT_22_SALES  )                          as VS_CAT_22_SALES_INC,\n                sum( vs.V2_INC_CAT_23_SALES  )                          as VS_CAT_23_SALES_INC,\n                sum( vs.V2_INC_CAT_24_SALES  )                          as VS_CAT_24_SALES_INC,\n                0 /* no cat 25 sales */                                 as VS_CAT_25_SALES_INC,\n                sum( vs.V1_INC_CAT_01_FEES )                            as VS_CAT_01_FEES_INC,\n                sum( vs.V1_INC_CAT_02_FEES )                            as VS_CAT_02_FEES_INC,\n                sum( vs.V1_INC_CAT_03_FEES )                            as VS_CAT_03_FEES_INC,\n                sum( vs.V1_INC_CAT_04_FEES )                            as VS_CAT_04_FEES_INC,\n                sum( vs.V1_INC_CAT_05_FEES )                            as VS_CAT_05_FEES_INC,\n                sum( vs.V1_INC_CAT_06_FEES )                            as VS_CAT_06_FEES_INC,\n                sum( vs.V1_INC_CAT_07_FEES )                            as VS_CAT_07_FEES_INC,\n                sum( vs.V1_INC_CAT_08_FEES )                            as VS_CAT_08_FEES_INC,\n                sum( vs.V1_INC_CAT_09_FEES )                            as VS_CAT_09_FEES_INC,\n                sum( vs.V1_INC_CAT_10_FEES )                            as VS_CAT_10_FEES_INC,\n                sum( vs.V1_INC_CAT_11_FEES )                            as VS_CAT_11_FEES_INC,\n                sum( vs.V1_INC_CAT_12_FEES )                            as VS_CAT_12_FEES_INC,\n                sum( vs.V1_INC_CAT_13_FEES )                            as VS_CAT_13_FEES_INC,\n                sum( vs.V1_INC_CAT_14_FEES )                            as VS_CAT_14_FEES_INC,\n                sum( vs.V1_INC_CAT_15_FEES )                            as VS_CAT_15_FEES_INC,\n                sum( vs.V1_INC_CAT_16_FEES )                            as VS_CAT_16_FEES_INC,\n                sum( vs.V2_INC_CAT_17_FEES )                            as VS_CAT_17_FEES_INC,\n                sum( vs.V2_INC_CAT_18_FEES )                            as VS_CAT_18_FEES_INC,\n                sum( vs.V2_INC_CAT_19_FEES )                            as VS_CAT_19_FEES_INC,\n                sum( vs.V2_INC_CAT_20_FEES )                            as VS_CAT_20_FEES_INC,\n                sum( vs.V2_INC_CAT_21_FEES )                            as VS_CAT_21_FEES_INC,\n                sum( vs.V2_INC_CAT_22_FEES )                            as VS_CAT_22_FEES_INC,\n                sum( vs.V2_INC_CAT_23_FEES )                            as VS_CAT_23_FEES_INC,\n                sum( vs.V2_INC_CAT_24_FEES )                            as VS_CAT_24_FEES_INC,\n                sum( vs.V2_INC_CAT_25_FEES )                            as VS_CAT_25_FEES_INC,\n                sum( vs.V3_EXPENSE_BULLETIN )                           as VS_EXP_BULLETIN,\n                sum( vs.V3_EXPENSE_INTRA_CHANGE )                       as VS_EXP_INTRA_CHANGE,\n                sum( vs.V3_EXPENSE_ASSESSMENT )                         as VS_EXP_ASSESSMENT,\n                sum( mc.M1_INCOME_BULLETIN )                            as MC_INC_BULLETIN,\n                sum( mc.M1_INCOME_INTRA_CHANGE )                        as MC_INC_INTRA_CHANGE,\n                sum( mc.M1_INCOME_ASSESSMENT )                          as MC_INC_ASSESSMENT,\n                sum( mc.M1_INC_CAT_01_SALES )                           as MC_CAT_01_SALES_INC,\n                sum( mc.M1_INC_CAT_02_SALES )                           as MC_CAT_02_SALES_INC,\n                sum( mc.M1_INC_CAT_03_SALES )                           as MC_CAT_03_SALES_INC,\n                sum( mc.M1_INC_CAT_04_SALES )                           as MC_CAT_04_SALES_INC,\n                sum( mc.M1_INC_CAT_05_SALES )                           as MC_CAT_05_SALES_INC,\n                sum( mc.M1_INC_CAT_06_SALES )                           as MC_CAT_06_SALES_INC,\n                sum( mc.M1_INC_CAT_07_SALES )                           as MC_CAT_07_SALES_INC,\n                sum( mc.M1_INC_CAT_08_SALES )                           as MC_CAT_08_SALES_INC,\n                sum( mc.M1_INC_CAT_09_SALES )                           as MC_CAT_09_SALES_INC,\n                sum( mc.M1_INC_CAT_10_SALES )                           as MC_CAT_10_SALES_INC,\n                sum( mc.M1_INC_CAT_11_SALES )                           as MC_CAT_11_SALES_INC,\n                sum( mc.M1_INC_CAT_12_SALES )                           as MC_CAT_12_SALES_INC,\n                sum( mc.M1_INC_CAT_01_FEES )                            as MC_CAT_01_FEES_INC,\n                sum( mc.M1_INC_CAT_02_FEES )                            as MC_CAT_02_FEES_INC,\n                sum( mc.M1_INC_CAT_03_FEES )                            as MC_CAT_03_FEES_INC,\n                sum( mc.M1_INC_CAT_04_FEES )                            as MC_CAT_04_FEES_INC,\n                sum( mc.M1_INC_CAT_05_FEES )                            as MC_CAT_05_FEES_INC,\n                sum( mc.M1_INC_CAT_06_FEES )                            as MC_CAT_06_FEES_INC,\n                sum( mc.M1_INC_CAT_07_FEES )                            as MC_CAT_07_FEES_INC,\n                sum( mc.M1_INC_CAT_08_FEES )                            as MC_CAT_08_FEES_INC,\n                sum( mc.M1_INC_CAT_09_FEES )                            as MC_CAT_09_FEES_INC,\n                sum( mc.M1_INC_CAT_10_FEES )                            as MC_CAT_10_FEES_INC,\n                sum( mc.M1_INC_CAT_11_FEES )                            as MC_CAT_11_FEES_INC,\n                sum( mc.M1_INC_CAT_12_FEES )                            as MC_CAT_12_FEES_INC,\n                sum( mc.M2_EXPENSE_BULLETIN )                           as MC_EXP_BULLETIN,\n                sum( mc.M2_EXPENSE_INTRA_CHANGE )                       as MC_EXP_INTRA_CHANGE,\n                sum( mc.M2_EXPENSE_ASSESSMENT )                         as MC_EXP_ASSESSMENT,\n                sum( vs.V5_CAT_01_SALES_COUNT )                         as VS_CAT_01_SALES_COUNT,\n                sum( vs.V5_CAT_01_SALES_AMOUNT )                        as VS_CAT_01_SALES_AMOUNT,\n                sum( vs.V5_CAT_01_CRED_COUNT )                          as VS_CAT_01_CRED_COUNT,\n                sum( vs.V5_CAT_01_CRED_AMOUNT )                         as VS_CAT_01_CRED_AMOUNT,\n                sum( vs.V5_CAT_01_INTCH )                               as VS_CAT_01_INTCH,\n                sum( vs.V5_CAT_02_SALES_COUNT )                         as VS_CAT_02_SALES_COUNT,\n                sum( vs.V5_CAT_02_SALES_AMOUNT )                        as VS_CAT_02_SALES_AMOUNT,\n                sum( vs.V5_CAT_02_CRED_COUNT )                          as VS_CAT_02_CRED_COUNT,\n                sum( vs.V5_CAT_02_CRED_AMOUNT )                         as VS_CAT_02_CRED_AMOUNT,\n                sum( vs.V5_CAT_02_INTCH )                               as VS_CAT_02_INTCH,\n                sum( vs.V5_CAT_03_SALES_COUNT )                         as VS_CAT_03_SALES_COUNT,\n                sum( vs.V5_CAT_03_SALES_AMOUNT )                        as VS_CAT_03_SALES_AMOUNT,\n                sum( vs.V5_CAT_03_CRED_COUNT )                          as VS_CAT_03_CRED_COUNT,\n                sum( vs.V5_CAT_03_CRED_AMOUNT )                         as VS_CAT_03_CRED_AMOUNT,\n                sum( vs.V5_CAT_03_INTCH )                               as VS_CAT_03_INTCH,\n                sum( vs.V5_CAT_04_SALES_COUNT )                         as VS_CAT_04_SALES_COUNT,\n                sum( vs.V5_CAT_04_SALES_AMOUNT )                        as VS_CAT_04_SALES_AMOUNT,\n                sum( vs.V5_CAT_04_CRED_COUNT )                          as VS_CAT_04_CRED_COUNT,\n                sum( vs.V5_CAT_04_CRED_AMOUNT )                         as VS_CAT_04_CRED_AMOUNT,\n                sum( vs.V5_CAT_04_INTCH )                               as VS_CAT_04_INTCH,\n                sum( vs.V5_CAT_05_SALES_COUNT )                         as VS_CAT_05_SALES_COUNT,\n                sum( vs.V5_CAT_05_SALES_AMOUNT )                        as VS_CAT_05_SALES_AMOUNT,\n                sum( vs.V5_CAT_05_CRED_COUNT )                          as VS_CAT_05_CRED_COUNT,\n                sum( vs.V5_CAT_05_CRED_AMOUNT )                         as VS_CAT_05_CRED_AMOUNT,\n                sum( vs.V5_CAT_05_INTCH )                               as VS_CAT_05_INTCH,\n                sum( vs.V5_CAT_06_SALES_COUNT )                         as VS_CAT_06_SALES_COUNT,\n                sum( vs.V5_CAT_06_SALES_AMOUNT )                        as VS_CAT_06_SALES_AMOUNT,\n                sum( vs.V5_CAT_06_CRED_COUNT )                          as VS_CAT_06_CRED_COUNT,\n                sum( vs.V5_CAT_06_CRED_AMOUNT )                         as VS_CAT_06_CRED_AMOUNT,\n                sum( vs.V6_CAT_06_INTCH )                               as VS_CAT_06_INTCH,\n                sum( vs.V6_CAT_07_SALES_COUNT )                         as VS_CAT_07_SALES_COUNT,\n                sum( vs.V6_CAT_07_SALES_AMOUNT )                        as VS_CAT_07_SALES_AMOUNT,\n                sum( vs.V6_CAT_07_CRED_COUNT )                          as VS_CAT_07_CRED_COUNT,\n                sum( vs.V6_CAT_07_CRED_AMOUNT )                         as VS_CAT_07_CRED_AMOUNT,\n                sum( vs.V6_CAT_07_INTCH )                               as VS_CAT_07_INTCH,\n                sum( vs.V6_CAT_08_SALES_COUNT )                         as VS_CAT_08_SALES_COUNT,\n                sum( vs.V6_CAT_08_SALES_AMOUNT )                        as VS_CAT_08_SALES_AMOUNT,\n                sum( vs.V6_CAT_08_CRED_COUNT )                          as VS_CAT_08_CRED_COUNT,\n                sum( vs.V6_CAT_08_CRED_AMOUNT )                         as VS_CAT_08_CRED_AMOUNT,\n                sum( vs.V6_CAT_08_INTCH )                               as VS_CAT_08_INTCH,\n                sum( vs.V6_CAT_09_SALES_COUNT )                         as VS_CAT_09_SALES_COUNT,\n                sum( vs.V6_CAT_09_SALES_AMOUNT )                        as VS_CAT_09_SALES_AMOUNT,\n                sum( vs.V6_CAT_09_CRED_COUNT )                          as VS_CAT_09_CRED_COUNT,\n                sum( vs.V6_CAT_09_CRED_AMOUNT )                         as VS_CAT_09_CRED_AMOUNT,\n                sum( vs.V6_CAT_09_INTCH )                               as VS_CAT_09_INTCH,\n                sum( vs.V6_CAT_10_SALES_COUNT )                         as VS_CAT_10_SALES_COUNT,\n                sum( vs.V6_CAT_10_SALES_AMOUNT )                        as VS_CAT_10_SALES_AMOUNT,\n                sum( vs.V6_CAT_10_CRED_COUNT )                          as VS_CAT_10_CRED_COUNT,\n                sum( vs.V6_CAT_10_CRED_AMOUNT )                         as VS_CAT_10_CRED_AMOUNT,\n                sum( vs.V6_CAT_10_INTCH )                               as VS_CAT_10_INTCH,\n                sum( vs.V6_CAT_11_SALES_COUNT )                         as VS_CAT_11_SALES_COUNT,\n                sum( vs.V6_CAT_11_SALES_AMOUNT )                        as VS_CAT_11_SALES_AMOUNT,\n                sum( vs.V6_CAT_11_CRED_COUNT )                          as VS_CAT_11_CRED_COUNT,\n                sum( vs.V6_CAT_11_CRED_AMOUNT )                         as VS_CAT_11_CRED_AMOUNT,\n                sum( vs.V6_CAT_11_INTCH )                               as VS_CAT_11_INTCH,\n                sum( vs.V6_CAT_12_SALES_COUNT )                         as VS_CAT_12_SALES_COUNT,\n                sum( vs.V6_CAT_12_SALES_AMOUNT )                        as VS_CAT_12_SALES_AMOUNT,\n                sum( vs.V6_CAT_12_CRED_COUNT )                          as VS_CAT_12_CRED_COUNT,\n                sum( vs.V7_CAT_12_CRED_AMOUNT )                         as VS_CAT_12_CRED_AMOUNT,\n                sum( vs.V7_CAT_12_INTCH )                               as VS_CAT_12_INTCH,\n                sum( vs.V7_CAT_13_SALES_COUNT )                         as VS_CAT_13_SALES_COUNT,\n                sum( vs.V7_CAT_13_SALES_AMOUNT )                        as VS_CAT_13_SALES_AMOUNT,\n                sum( vs.V7_CAT_13_CRED_COUNT )                          as VS_CAT_13_CRED_COUNT,\n                sum( vs.V7_CAT_13_CRED_AMOUNT )                         as VS_CAT_13_CRED_AMOUNT,\n                sum( vs.V7_CAT_13_INTCH )                               as VS_CAT_13_INTCH,\n                sum( vs.V7_CAT_14_SALES_COUNT )                         as VS_CAT_14_SALES_COUNT,\n                sum( vs.V7_CAT_14_SALES_AMOUNT )                        as VS_CAT_14_SALES_AMOUNT,\n                sum( vs.V7_CAT_14_CRED_COUNT )                          as VS_CAT_14_CRED_COUNT,\n                sum( vs.V7_CAT_14_CRED_AMOUNT )                         as VS_CAT_14_CRED_AMOUNT,\n                sum( vs.V7_CAT_14_INTCH )                               as VS_CAT_14_INTCH,\n                sum( vs.V7_CAT_15_SALES_COUNT )                         as VS_CAT_15_SALES_COUNT,\n                sum( vs.V7_CAT_15_SALES_AMOUNT )                        as VS_CAT_15_SALES_AMOUNT,\n                sum( vs.V7_CAT_15_CRED_COUNT )                          as VS_CAT_15_CRED_COUNT,\n                sum( vs.V7_CAT_15_CRED_AMOUNT )                         as VS_CAT_15_CRED_AMOUNT,\n                sum( vs.V7_CAT_15_INTCH )                               as VS_CAT_15_INTCH,\n                sum( vs.V7_CAT_16_SALES_COUNT )                         as VS_CAT_16_SALES_COUNT,\n                sum( vs.V7_CAT_16_SALES_AMOUNT )                        as VS_CAT_16_SALES_AMOUNT,\n                sum( vs.V7_CAT_16_CRED_COUNT )                          as VS_CAT_16_CRED_COUNT,\n                sum( vs.V7_CAT_16_CRED_AMOUNT )                         as VS_CAT_16_CRED_AMOUNT,\n                sum( vs.V7_CAT_16_INTCH )                               as VS_CAT_16_INTCH,\n                sum( vs.V7_CAT_17_SALES_COUNT )                         as VS_CAT_17_SALES_COUNT,\n                sum( vs.V7_CAT_17_SALES_AMOUNT )                        as VS_CAT_17_SALES_AMOUNT,\n                sum( vs.V7_CAT_17_CRED_COUNT )                          as VS_CAT_17_CRED_COUNT,\n                sum( vs.V7_CAT_17_CRED_AMOUNT )                         as VS_CAT_17_CRED_AMOUNT,\n                sum( vs.V7_CAT_17_INTCH )                               as VS_CAT_17_INTCH,\n                sum( vs.V7_CAT_18_SALES_COUNT )                         as VS_CAT_18_SALES_COUNT,\n                sum( vs.V8_CAT_18_SALES_AMOUNT )                        as VS_CAT_18_SALES_AMOUNT,\n                sum( vs.V8_CAT_18_CRED_COUNT )                          as VS_CAT_18_CRED_COUNT,\n                sum( vs.V8_CAT_18_CRED_AMOUNT )                         as VS_CAT_18_CRED_AMOUNT,\n                sum( vs.V8_CAT_18_INTCH )                               as VS_CAT_18_INTCH,\n                sum( vs.V8_CAT_19_SALES_COUNT )                         as VS_CAT_19_SALES_COUNT,\n                sum( vs.V8_CAT_19_SALES_AMOUNT )                        as VS_CAT_19_SALES_AMOUNT,\n                sum( vs.V8_CAT_19_CRED_COUNT )                          as VS_CAT_19_CRED_COUNT,\n                sum( vs.V8_CAT_19_CRED_AMOUNT )                         as VS_CAT_19_CRED_AMOUNT,\n                sum( vs.V8_CAT_19_INTCH )                               as VS_CAT_19_INTCH,\n                sum( vs.V8_CAT_20_SALES_COUNT )                         as VS_CAT_20_SALES_COUNT,\n                sum( vs.V8_CAT_20_SALES_AMOUNT )                        as VS_CAT_20_SALES_AMOUNT,\n                sum( vs.V8_CAT_20_CRED_COUNT )                          as VS_CAT_20_CRED_COUNT,\n                sum( vs.V8_CAT_20_CRED_AMOUNT )                         as VS_CAT_20_CRED_AMOUNT,\n                sum( vs.V8_CAT_20_INTCH )                               as VS_CAT_20_INTCH,\n                sum( vs.V8_CAT_21_SALES_COUNT )                         as VS_CAT_21_SALES_COUNT,\n                sum( vs.V8_CAT_21_SALES_AMOUNT )                        as VS_CAT_21_SALES_AMOUNT,\n                sum( vs.V8_CAT_21_CRED_COUNT )                          as VS_CAT_21_CRED_COUNT,\n                sum( vs.V8_CAT_21_CRED_AMOUNT )                         as VS_CAT_21_CRED_AMOUNT,\n                sum( vs.V8_CAT_21_INTCH )                               as VS_CAT_21_INTCH,\n                sum( vs.V8_CAT_22_SALES_COUNT )                         as VS_CAT_22_SALES_COUNT,\n                sum( vs.V8_CAT_22_SALES_AMOUNT )                        as VS_CAT_22_SALES_AMOUNT,\n                sum( vs.V8_CAT_22_CRED_COUNT )                          as VS_CAT_22_CRED_COUNT,\n                sum( vs.V8_CAT_22_CRED_AMOUNT )                         as VS_CAT_22_CRED_AMOUNT,\n                sum( vs.V8_CAT_22_INTCH )                               as VS_CAT_22_INTCH,\n                sum( vs.V8_CAT_23_SALES_COUNT )                         as VS_CAT_23_SALES_COUNT,\n                sum( vs.V8_CAT_23_SALES_AMOUNT )                        as VS_CAT_23_SALES_AMOUNT,\n                sum( vs.V8_CAT_23_CRED_COUNT )                          as VS_CAT_23_CRED_COUNT,\n                sum( vs.V8_CAT_23_CRED_AMOUNT )                         as VS_CAT_23_CRED_AMOUNT,\n                sum( vs.V9_CAT_23_INTCH )                               as VS_CAT_23_INTCH,\n                sum( vs.V9_CAT_24_SALES_COUNT )                         as VS_CAT_24_SALES_COUNT,\n                sum( vs.V9_CAT_24_SALES_AMOUNT )                        as VS_CAT_24_SALES_AMOUNT,\n                sum( vs.V9_CAT_24_CRED_COUNT )                          as VS_CAT_24_CRED_COUNT,\n                sum( vs.V9_CAT_24_CRED_AMOUNT )                         as VS_CAT_24_CRED_AMOUNT,\n                sum( vs.V9_CAT_24_INTCH )                               as VS_CAT_24_INTCH,\n                0                                                       as VS_CAT_25_SALES_COUNT,\n                0                                                       as VS_CAT_25_SALES_AMOUNT,\n                0                                                       as VS_CAT_25_CRED_COUNT,\n                0                                                       as VS_CAT_25_CRED_AMOUNT,\n                sum( vs.V9_CAT_25_INTCH )                               as VS_CAT_25_INTCH,\n                sum( mc.M3_CAT_01_SALES_COUNT )                         as MC_CAT_01_SALES_COUNT,\n                sum( mc.M3_CAT_01_SALES_AMOUNT )                        as MC_CAT_01_SALES_AMOUNT,\n                sum( mc.M3_CAT_01_CRED_COUNT )                          as MC_CAT_01_CRED_COUNT,\n                sum( mc.M3_CAT_01_CRED_AMOUNT )                         as MC_CAT_01_CRED_AMOUNT,\n                sum( mc.M3_CAT_01_INTCH )                               as MC_CAT_01_INTCH,\n                sum( mc.M3_CAT_02_SALES_COUNT )                         as MC_CAT_02_SALES_COUNT,\n                sum( mc.M3_CAT_02_SALES_AMOUNT )                        as MC_CAT_02_SALES_AMOUNT,\n                sum( mc.M3_CAT_02_CRED_COUNT )                          as MC_CAT_02_CRED_COUNT,\n                sum( mc.M3_CAT_02_CRED_AMOUNT )                         as MC_CAT_02_CRED_AMOUNT,\n                sum( mc.M3_CAT_02_INTCH )                               as MC_CAT_02_INTCH,\n                sum( mc.M3_CAT_03_SALES_COUNT )                         as MC_CAT_03_SALES_COUNT,\n                sum( mc.M3_CAT_03_SALES_AMOUNT )                        as MC_CAT_03_SALES_AMOUNT,\n                sum( mc.M3_CAT_03_CRED_COUNT )                          as MC_CAT_03_CRED_COUNT,\n                sum( mc.M3_CAT_03_CRED_AMOUNT )                         as MC_CAT_03_CRED_AMOUNT,\n                sum( mc.M3_CAT_03_INTCH )                               as MC_CAT_03_INTCH,\n                sum( mc.M3_CAT_04_SALES_COUNT )                         as MC_CAT_04_SALES_COUNT,\n                sum( mc.M3_CAT_04_SALES_AMOUNT )                        as MC_CAT_04_SALES_AMOUNT,\n                sum( mc.M3_CAT_04_CRED_COUNT )                          as MC_CAT_04_CRED_COUNT,\n                sum( mc.M3_CAT_04_CRED_AMOUNT )                         as MC_CAT_04_CRED_AMOUNT,\n                sum( mc.M3_CAT_04_INTCH )                               as MC_CAT_04_INTCH,\n                sum( mc.M3_CAT_05_SALES_COUNT )                         as MC_CAT_05_SALES_COUNT,\n                sum( mc.M3_CAT_05_SALES_AMOUNT )                        as MC_CAT_05_SALES_AMOUNT,\n                sum( mc.M3_CAT_05_CRED_COUNT )                          as MC_CAT_05_CRED_COUNT,\n                sum( mc.M3_CAT_05_CRED_AMOUNT )                         as MC_CAT_05_CRED_AMOUNT,\n                sum( mc.M3_CAT_05_INTCH )                               as MC_CAT_05_INTCH,\n                sum( mc.M3_CAT_06_SALES_COUNT )                         as MC_CAT_06_SALES_COUNT,\n                sum( mc.M3_CAT_06_SALES_AMOUNT )                        as MC_CAT_06_SALES_AMOUNT,\n                sum( mc.M3_CAT_06_CRED_COUNT )                          as MC_CAT_06_CRED_COUNT,\n                sum( mc.M3_CAT_06_CRED_AMOUNT )                         as MC_CAT_06_CRED_AMOUNT,\n                sum( mc.M4_CAT_06_INTCH )                               as MC_CAT_06_INTCH,\n                sum( mc.M4_CAT_07_SALES_COUNT )                         as MC_CAT_07_SALES_COUNT,\n                sum( mc.M4_CAT_07_SALES_AMOUNT )                        as MC_CAT_07_SALES_AMOUNT,\n                sum( mc.M4_CAT_07_CRED_COUNT )                          as MC_CAT_07_CRED_COUNT,\n                sum( mc.M4_CAT_07_CRED_AMOUNT )                         as MC_CAT_07_CRED_AMOUNT,\n                sum( mc.M4_CAT_07_INTCH )                               as MC_CAT_07_INTCH,\n                sum( mc.M4_CAT_08_SALES_COUNT )                         as MC_CAT_08_SALES_COUNT,\n                sum( mc.M4_CAT_08_SALES_AMOUNT )                        as MC_CAT_08_SALES_AMOUNT,\n                sum( mc.M4_CAT_08_CRED_COUNT )                          as MC_CAT_08_CRED_COUNT,\n                sum( mc.M4_CAT_08_CRED_AMOUNT )                         as MC_CAT_08_CRED_AMOUNT,\n                sum( mc.M4_CAT_08_INTCH )                               as MC_CAT_08_INTCH,\n                sum( mc.M4_CAT_09_SALES_COUNT )                         as MC_CAT_09_SALES_COUNT,\n                sum( mc.M4_CAT_09_SALES_AMOUNT )                        as MC_CAT_09_SALES_AMOUNT,\n                sum( mc.M4_CAT_09_CRED_COUNT )                          as MC_CAT_09_CRED_COUNT,\n                sum( mc.M4_CAT_09_CRED_AMOUNT )                         as MC_CAT_09_CRED_AMOUNT,\n                sum( mc.M4_CAT_09_INTCH )                               as MC_CAT_09_INTCH,\n                sum( mc.M4_CAT_10_SALES_COUNT )                         as MC_CAT_10_SALES_COUNT,\n                sum( mc.M4_CAT_10_SALES_AMOUNT )                        as MC_CAT_10_SALES_AMOUNT,\n                sum( mc.M4_CAT_10_CRED_COUNT )                          as MC_CAT_10_CRED_COUNT,\n                sum( mc.M4_CAT_10_CRED_AMOUNT )                         as MC_CAT_10_CRED_AMOUNT,\n                sum( mc.M4_CAT_10_INTCH )                               as MC_CAT_10_INTCH,\n                sum( mc.M4_CAT_11_SALES_COUNT )                         as MC_CAT_11_SALES_COUNT,\n                sum( mc.M4_CAT_11_SALES_AMOUNT )                        as MC_CAT_11_SALES_AMOUNT,\n                sum( mc.M4_CAT_11_CRED_COUNT )                          as MC_CAT_11_CRED_COUNT,\n                sum( mc.M4_CAT_11_CRED_AMOUNT )                         as MC_CAT_11_CRED_AMOUNT,\n                sum( mc.M4_CAT_11_INTCH )                               as MC_CAT_11_INTCH,\n                sum( mc.M4_CAT_12_SALES_COUNT )                         as MC_CAT_12_SALES_COUNT,\n                sum( mc.M4_CAT_12_SALES_AMOUNT )                        as MC_CAT_12_SALES_AMOUNT,\n                sum( mc.M4_CAT_12_CRED_COUNT )                          as MC_CAT_12_CRED_COUNT,\n                sum( mc.M5_CAT_12_CRED_AMOUNT )                         as MC_CAT_12_CRED_AMOUNT,\n                sum( mc.M5_CAT_12_INTCH )                               as MC_CAT_12_INTCH\n        from    parent_org              po,\n                organization            o,\n                group_merchant          gm,\n                group_rep_merchant      grm,\n                monthly_extract_gn      gn,\n                monthly_extract_visa    vs,\n                monthly_extract_mc      mc\n        where   po.parent_org_num =  :1  and\n                o.org_num = po.org_num and\n                gm.org_num = o.org_num and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and        \n                gn.hh_active_date between  :4  and  :5  and\n                gn.hh_merchant_number = gm.merchant_number and\n                vs.hh_load_sec(+) = gn.hh_load_sec and\n                mc.hh_load_sec(+) = gn.hh_load_sec\n        group by o.org_num, o.org_group, gn.hh_active_date, \n                  vs.v1_bet_table_number, mc.m1_bet_table_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AcctICRevenueDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AcctICRevenueDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:494^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        for( int ct = 0; ct < IC_CARD_TYPES; ++ct )
        {
          switch( ct )
          {
            case IC_VISA:       // visa sales/fees
              cardType  = mesConstants.VITAL_ME_AP_VISA;
              catCount  = 25;
              break;

            case IC_MC:       // mc sales/fees
              cardType  = mesConstants.VITAL_ME_AP_MC;
              catCount  = 12;
              break;

            default:
              continue;
          }
          
          for( int cat = 1; cat <= catCount; ++cat )
          {
            fieldPrefix  = cardType + "_cat_" + catFormat.format(cat);
      
            salesCount  = resultSet.getInt( fieldPrefix + "_sales_count" );
            salesAmount = resultSet.getDouble( fieldPrefix + "_sales_amount" );
            salesInc    = resultSet.getDouble( fieldPrefix + "_sales_inc" );
            feesInc     = resultSet.getDouble( fieldPrefix + "_fees_inc" );
            
            if ( (salesInc + feesInc) != 0.0 )
            {
              ReportRows.addElement( new RowData( resultSet.getDate("active_date"), 
                                                  cardType, 
                                                  resultSet.getInt(cardType + "_bet_table"),
                                                  cat, 
                                                  loadVitalIcCatDesc( cardType, cat ),
                                                  salesCount, salesAmount,
                                                  salesInc, feesInc, 
                                                  resultSet.getDouble(fieldPrefix + "_intch") ) );
            }
          }
        }
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public String loadVitalIcCatDesc( String cardType, int cat )
  {
    String        retVal = "Undefined IC Category";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:558^7*/

//  ************************************************************
//  #sql [Ctx] { select description 
//          from   ic_category_desc
//          where  card_type = :cardType and
//                 category  = :cat
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select description  \n        from   ic_category_desc\n        where  card_type =  :1  and\n               category  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AcctICRevenueDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,cardType);
   __sJT_st.setInt(2,cat);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:564^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.MONTH, -1 );
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/