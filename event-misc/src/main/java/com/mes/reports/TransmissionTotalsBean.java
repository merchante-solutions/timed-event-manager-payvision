/*@lineinfo:filename=TransmissionTotalsBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/TransmissionTotalsBean.sqlj $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/05/02 2:18p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class TransmissionTotalsBean extends SQLJConnectionBase
{
  public    String    loadFilename            = "";
  public    boolean   submitted               = false;
  
  public    long      salesCount              = 0L;
  public    double    salesAmount             = 0.0;
  public    long      creditCount             = 0L;
  public    double    creditAmount            = 0.0;
  public    long      caCount                 = 0L;
  public    double    caAmount                = 0.0;
  public    long      batchCount              = 0L;
  public    double    batchAmount             = 0.0;
  public    long      adjCount                = 0L;
  public    double    adjAmount               = 0.0;
  
  public    long      trailerSalesCount       = 0L;
  public    double    trailerSalesAmount      = 0.0;
  public    long      trailerCreditCount      = 0L;
  public    double    trailerCreditAmount     = 0.0;
  public    long      trailerCaCount          = 0L;
  public    double    trailerCaAmount         = 0.0;
  public    long      trailerBatchCount       = 0L;
  public    double    trailerBatchAmount      = 0.0;
  public    long      trailerAdjCount         = 0L;
  public    double    trailerAdjAmount        = 0.0;
  public    long      trailerDraftACount      = 0L;
  public    double    trailerDraftAAmount     = 0.0;
  
  public TransmissionTotalsBean()
  {
  }
  
  private void loadTrailerData()
  {
    ResultSetIterator     it      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:67^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  num_deposit_sales             sales_count,
//                  deposit_sales_amt / 100       sales_amount,
//                  num_deposit_credit            credits_count,
//                  deposit_credit_amt / 100      credits_amount,
//                  num_cash_advance              cash_advance_count,
//                  cash_advance_amt / 100        cash_advance_amount,
//                  num_net_deposit               batch_count,
//                  net_deposit_amt / 100         batch_amount,
//                  num_merchant_adj              adjustment_count,
//                  merchant_adj / 100            adjustment_amount,
//                  num_draft_a                   draft_a_count,
//                  draft_a_amt / 100             draft_a_amount,
//                  load_filename
//          from    daily_detail_file_trailer
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  num_deposit_sales             sales_count,\n                deposit_sales_amt / 100       sales_amount,\n                num_deposit_credit            credits_count,\n                deposit_credit_amt / 100      credits_amount,\n                num_cash_advance              cash_advance_count,\n                cash_advance_amt / 100        cash_advance_amount,\n                num_net_deposit               batch_count,\n                net_deposit_amt / 100         batch_amount,\n                num_merchant_adj              adjustment_count,\n                merchant_adj / 100            adjustment_amount,\n                num_draft_a                   draft_a_count,\n                draft_a_amt / 100             draft_a_amount,\n                load_filename\n        from    daily_detail_file_trailer\n        where   load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.TransmissionTotalsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.TransmissionTotalsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^7*/
      
      ResultSet rs = it.getResultSet();
      
      if(rs.next())
      {
        trailerSalesCount   = rs.getLong("sales_count");
        trailerSalesAmount  = rs.getDouble("sales_amount");
        trailerCreditCount  = rs.getLong("credits_count");
        trailerCreditAmount = rs.getDouble("credits_amount");
        trailerCaCount      = rs.getLong("cash_advance_count");
        trailerCaAmount     = rs.getDouble("cash_advance_amount");
        trailerBatchCount   = rs.getLong("batch_count");
        trailerBatchAmount  = rs.getDouble("batch_amount");
        trailerAdjCount     = rs.getLong("adjustment_count");
        trailerAdjAmount    = rs.getDouble("adjustment_amount");
        trailerDraftACount  = rs.getLong("draft_a_count");
        trailerDraftAAmount = rs.getDouble("draft_a_amount");
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadTrailerData(" + loadFilename + ")", e.toString());
    }
  }
  
  private void loadDDFData()
  {
    ResultSetIterator     it      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:118^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(dt.batch_date)            transaction_count,
//                  sum(dt.transaction_amount)      transaction_amount,
//                  dtd.tran_code_short_desc        tran_type,
//                  dt.load_filename                load_filename
//          from    daily_detail_file_dt        dt,
//                  vital_ddf_tran_code_desc    dtd
//          where   dt.load_filename = :loadFilename and
//                  dt.transacction_code = dtd.tran_code(+)
//          group by dtd.tran_code_short_desc,
//                   dt.load_filename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(dt.batch_date)            transaction_count,\n                sum(dt.transaction_amount)      transaction_amount,\n                dtd.tran_code_short_desc        tran_type,\n                dt.load_filename                load_filename\n        from    daily_detail_file_dt        dt,\n                vital_ddf_tran_code_desc    dtd\n        where   dt.load_filename =  :1  and\n                dt.transacction_code = dtd.tran_code(+)\n        group by dtd.tran_code_short_desc,\n                 dt.load_filename";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.TransmissionTotalsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.TransmissionTotalsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:130^7*/
      
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {
        if(rs.getString("tran_type") != null)
        {
          if(rs.getString("tran_type").equals("PURCHASE"))
          {
            salesCount    = rs.getLong("transaction_count");
            salesAmount   = rs.getDouble("transaction_amount");
          }
          
          if(rs.getString("tran_type").equals("RETURN"))
          {
            creditCount   = rs.getLong("transaction_count");
            creditAmount  = rs.getDouble("transaction_amount");
          }
          
          if(rs.getString("tran_type").equals("CASH ADVANCE"))
          {
            caCount       = rs.getLong("transaction_count");
            caAmount      = rs.getDouble("transaction_amount");
          }
        }
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadDDFData(" + loadFilename + ")", e.toString());
    }
  }
  
  private void loadBatchData()
  {
    ResultSetIterator     it      = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:171^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(net_deposit)      net_deposit_count,
//                  sum(net_deposit *
//                      decode (debit_credit_indicator,
//                              'D', -1,
//                              1))        net_deposit_amount
//          from    daily_detail_file_bh
//          where   load_filename = :loadFilename and
//                  batch_julian_date is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(net_deposit)      net_deposit_count,\n                sum(net_deposit *\n                    decode (debit_credit_indicator,\n                            'D', -1,\n                            1))        net_deposit_amount\n        from    daily_detail_file_bh\n        where   load_filename =  :1  and\n                batch_julian_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.TransmissionTotalsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.TransmissionTotalsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:181^7*/
      
      ResultSet rs = it.getResultSet();
      
      if(rs.next())
      {
        batchCount  = rs.getLong("net_deposit_count");
        batchAmount = rs.getDouble("net_deposit_amount");
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadBatchData(" + loadFilename + ")", e.toString());
    }
  }
  
  private void loadAdjustmentData()
  {
    ResultSetIterator   it      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:205^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(adjustment_amount)        adjustment_count,
//                  sum(adjustment_amount * 
//                        decode (debit_credit_ind,
//                                'C', -1,
//                                1))               adjustment_amount
//          from    daily_detail_file_adjustment
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(adjustment_amount)        adjustment_count,\n                sum(adjustment_amount * \n                      decode (debit_credit_ind,\n                              'C', -1,\n                              1))               adjustment_amount\n        from    daily_detail_file_adjustment\n        where   load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.TransmissionTotalsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.TransmissionTotalsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:214^7*/
      
      ResultSet rs = it.getResultSet();
      
      if(rs.next())
      {
        adjCount  = rs.getLong("adjustment_count");
        adjAmount = rs.getDouble("adjustment_amount");
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadAdjustmentData(" + loadFilename + ")", e.toString());
    }
  }
  
  public void loadData()
  {
    try
    {
      connect();
      
      loadTrailerData();
      
      loadDDFData();
      
      loadBatchData();
      
      loadAdjustmentData();
    }
    catch(Exception e)
    {
      logEntry("loadData(" + loadFilename + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void setLoadFilename(String loadFilename)
  {
    this.loadFilename = loadFilename;
  }
  
  public void setSubmit(String submit)
  {
    submitted = true;
  }
}/*@lineinfo:generated-code*/