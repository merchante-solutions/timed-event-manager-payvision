/*@lineinfo:filename=MasterCardECPDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MasterCardECPDataBean.sqlj $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2008-05-15 14:46:25 -0700 (Thu, 15 May 2008) $
  Version            : $Revision: 14859 $

  Change History:
     See VSS database

  Copyright (C) 2000-2005,2006 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.ComboDateField;
import com.mes.forms.DropDownField;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;



public class MasterCardECPDataBean extends ReportSQLJBean
{
  public class DetailData
  {
    public long     MerchantNumber      = 0L;
    public String   MerchantName        = null; 
    public Date     IncomingDate        = null;
    public Date     TranDate            = null;
    public String   CardNumber          = null;
    public double   TranAmount          = 0.0;
    public String   RefNumber           = null;
    public String   CBRefNumber         = null;
    public String   ControlNumber       = null;
    public String   CardType            = null;
    
    public DetailData( ResultSet resultSet )
    throws java.sql.SQLException
    {
      MerchantNumber = resultSet.getLong("merchant_number");
      MerchantName   = resultSet.getString("merchant_name");
      IncomingDate   = resultSet.getDate("incoming_date");
      TranDate       = resultSet.getDate("tran_date");
      CardNumber     = resultSet.getString("card_number");
      TranAmount     = resultSet.getDouble("tran_amount");
      RefNumber      = resultSet.getString("ref_num");
      CBRefNumber    = resultSet.getString("cb_ref_num");
      ControlNumber  = resultSet.getString("control_number");
      CardType       = resultSet.getString("card_type");
    }
  }
  
  public class SummaryData
  {
    public String   DbaName                = null;
    public double   CBAmountCurrMonth      = 0.0;
    public double   CBAmountPrevMonth      = 0.0;
    public int      CBCountCurrMonth       = 0;
    public int      CBCountPrevMonth       = 0;
    public double   CBtoSaleRatio          = 0.0;
    public double   CreditAmountCurrMonth  = 0.0;
    public double   CreditAmountPrevMonth  = 0.0;
    public int      CreditCountCurrMonth   = 0;
    public int      CreditCountPrevMonth   = 0;
    public long     MerchantNumber         = 0L;
    public double   SaleAmountCurrMonth    = 0.0;
    public double   SaleAmountPrevMonth    = 0.0;
    public int      SaleCountCurrMonth     = 0;
    public int      SaleCountPrevMonth     = 0;
  
    public SummaryData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      DbaName = resultSet.getString("dba_name");
      MerchantNumber = resultSet.getLong("merchant_number");
      CBAmountCurrMonth = resultSet.getDouble("cb_amount_curr_month");
      CBAmountPrevMonth = resultSet.getDouble("cb_amount_prev_month");
      CBCountCurrMonth  = resultSet.getInt("cb_count_curr_month");
      CBCountPrevMonth  = resultSet.getInt("cb_count_prev_month");
      CreditAmountCurrMonth = resultSet.getDouble("credits_amount_curr_month");
      CreditAmountPrevMonth = resultSet.getDouble("credits_amount_prev_month");
      CreditCountCurrMonth = resultSet.getInt("credits_count_curr_month");
      CreditCountPrevMonth = resultSet.getInt("credits_count_prev_month");
      SaleAmountCurrMonth = resultSet.getDouble("sales_amount_curr_month");
      SaleAmountPrevMonth = resultSet.getDouble("sales_amount_prev_month");
      SaleCountCurrMonth = resultSet.getInt("sales_count_curr_month");
      SaleCountPrevMonth = resultSet.getInt("sales_count_prev_month");
      CBtoSaleRatio = resultSet.getDouble("curr_cb_to_prev_sales_ratio");
    }
  }
  
  public class CardTypeTable extends DropDownTable
  {
    public CardTypeTable()
    {
      addElement ("VS", "Visa");
      addElement ("MC", "Master Card");
    }
  }
  
  public class ReportTypeTable extends DropDownTable
  {
    public ReportTypeTable()
    {
      addElement ("Summary", "Summary");
      addElement ("Details", "Details");
    }
  }
    
  public MasterCardECPDataBean()
  {
    super(true);
  }
      
  protected void createFields( HttpServletRequest request)
  {
    super.createFields(request);
    fields.add( new DropDownField( "cardType", "Card Type", new CardTypeTable(), false) );
    fields.add( new DropDownField( "reportType", "Report Type", new ReportTypeTable(), false) );
    fields.add( new ComboDateField("beginDate", "Report Date", false, false, 2000, false, true));
    fields.add( new ButtonField("btnSubmit", "Select"));    
    fields.setGroupHtmlExtra("class=\"formFields\"");
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);

    switch( getReportType() )
    {
      case RT_SUMMARY:
        line.append("\"Merchant Number\",");
        line.append("\"DBA\",");
        line.append("\"CB Count Curr Month\",");
        line.append("\"CB $ Curr Month\",");
        line.append("\"Sale Count Curr Month\",");
        line.append("\"Sale $ Curr Month\",");
        line.append("\"Credit Count Curr Month\",");
        line.append("\"Credit $ Curr Month\",");
        line.append("\"CB Count Prev Month\",");
        line.append("\"CB $ Prev Month\",");
        line.append("\"Sale Count Prev Month\",");
        line.append("\"Sale $ Prev Month\",");
        line.append("\"Credit Count Prev Month\",");
        line.append("\"Credit $ Prev Month\",");
        line.append("\"Curr CB to Prev Sales Ratio\"");
        break;
        
      case RT_DETAILS:
        line.append("\"Merchant Number\",");
        line.append("\"Merchant Name\",");
        line.append("\"Incoming Date\",");
        line.append("\"Tran Date\",");
        line.append("\"Card Type\",");
        line.append("\"Card Number\",");
        line.append("\"Tran Amount\",");
        line.append("\"Ref Number\",");
        line.append("\"CB Ref Number\",");
        line.append("\"Control Number\"");
        break;
        
      default:
        break;
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);


    switch( getReportType() )
    {
      case RT_SUMMARY:
        SummaryData record = (SummaryData)obj;
        
        line.append( "'" + record.MerchantNumber );
        line.append( "," );
        line.append( record.DbaName );
        line.append( "," );
        line.append( record.CBCountCurrMonth );
        line.append( ",\"" );
        line.append( record.CBAmountCurrMonth );
        line.append( "\"," );
        line.append( record.SaleCountCurrMonth );
        line.append( ",\"" );
        line.append( record.SaleAmountCurrMonth );
        line.append( "\",\"" );
        line.append( record.CreditCountCurrMonth );
        line.append( "\",\"" );
        line.append( record.CreditAmountCurrMonth );      
        line.append( "\"," );
        line.append( record.CBCountPrevMonth );
        line.append( ",\"" );
        line.append( record.CBAmountPrevMonth );
        line.append( "\"," );
        line.append( record.SaleCountPrevMonth ); 
        line.append( ",\"" );
        line.append( record.SaleAmountPrevMonth ); 
        line.append( "\"," );
        line.append( record.CreditCountPrevMonth );
        line.append( ",\"" );
        line.append( record.CreditAmountPrevMonth ); 
        line.append( "\"," );
        line.append( record.CBtoSaleRatio );
        break;
        
      case RT_DETAILS:
        DetailData rec = (DetailData)obj;
        
        line.append( "'" + rec.MerchantNumber );
        line.append( "," );
        line.append( rec.MerchantName );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate(rec.IncomingDate,"MM/dd/yyyy") );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate(rec.TranDate,"MM/dd/yyyy") );
        line.append( "," );
        line.append( rec.CardType );
        line.append( "," );
        line.append( "'" + rec.CardNumber );
        line.append( "," );
        line.append( rec.TranAmount );
        line.append( "," );
        line.append( "'" + rec.RefNumber );
        line.append( "," );
        line.append( rec.CBRefNumber ); 
        line.append( "," );
        line.append( rec.ControlNumber );
        break;
        
      default:
        break;
    }        
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl(buffer,nodeId,beginDate,endDate);
    buffer.append("&reportType=");
    buffer.append(getData("reportType"));
    buffer.append("&cardType=");
    buffer.append(getData("cardType"));
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename   = new StringBuffer("");
    
    switch( getReportType() )
    {
      case RT_SUMMARY:
        filename.append("ecp_accounts");
        break;
        
      case RT_DETAILS:
        filename.append("ecm_details");
        break;
        
      default:
        break;
    }

    return ( filename.toString() );
  }
  
  public int getReportType()
  {
    int retVal = RT_INVALID;
    
    if( getData("reportType").equals("Summary") )
    {
      retVal = RT_SUMMARY;
    }
    else if( getData("reportType").equals("Details") )
    {
      retVal = RT_DETAILS;
    }
    
    return retVal;
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData()
  {
    Date      reportDate = getReportDateBegin();    
    String    cardType   = getData("cardType");
    int       reportType = getReportType();
    
    switch( getReportType() )
    {
      case RT_SUMMARY:
        loadSummaryData(cardType, reportDate);
        break;
        
      case RT_DETAILS:
        loadDetailData(cardType, reportDate);
        break;
        
      default:
        break;
    }    
  }
  
  public void loadDetailData( String cardType, Date reportDate )
  {
    ResultSetIterator    it             = null;
    ResultSet            resultSet      = null;
    
    try
    {
      ReportRows.clear();
      /*@lineinfo:generated-code*//*@lineinfo:356^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.merchant_number                        as merchant_number,
//                  cb.merchant_name                          as merchant_name,
//                  cb.incoming_date                          as incoming_date,
//                  cb.tran_date                              as tran_date,
//                  :cardType                                 as card_type,                
//                  replace(cb.card_number,'xxxxxx','000000') as card_number,
//                  cb.tran_amount                            as tran_amount,
//                  cb.reference_number                       as ref_num,
//                  cb.cb_ref_num                             as cb_ref_num,
//                  cb.cb_load_sec                            as control_number
//          from    mif                      mf, 
//                 ( 
//                   select  cbi.merchant_number       as merchant_number, 
//                           count(cbi.cb_load_sec)    as cb_count, 
//                           sum(cbi.tran_amount)      as cb_amount 
//                   from    network_chargebacks   cbi 
//                   where   cbi.bank_number in (3941,3942) and 
//                           cbi.incoming_date between trunc(:reportDate,'month') and last_day(:reportDate) and 
//                           cbi.card_type = :cardType and
//                          cbi.first_time_chargeback = 'Y'
//                   group by cbi.merchant_number 
//                 )                         cb_curr, 
//                 ( 
//                   select  cbi.merchant_number       as merchant_number, 
//                           count(cbi.cb_load_sec)    as cb_count, 
//                           sum(cbi.tran_amount)      as cb_amount 
//                   from    network_chargebacks   cbi 
//                   where   cbi.bank_number in (3941,3942) and 
//                           cbi.incoming_date between trunc((trunc(:reportDate,'month')-1),'month') and (trunc(:reportDate,'month')-1) and 
//                           cbi.card_type = :cardType and
//                           cbi.first_time_chargeback = 'Y' 
//                   group by cbi.merchant_number 
//                 )                         cb_prev, 
//                 monthly_extract_summary   sm, 
//                 monthly_extract_summary   sm_prev,
//                 network_chargebacks       cb 
//          where  mf.bank_number in (3942,3941) and 
//                 cb_curr.merchant_number = mf.merchant_number and 
//                 cb_prev.merchant_number = mf.merchant_number and 
//                 sm.merchant_number = mf.merchant_number and 
//                 sm.active_date = trunc((trunc(:reportDate,'month')-1),'month') and
//                 sm_prev.merchant_number = mf.merchant_number and 
//                 sm_prev.active_date = trunc((trunc((trunc(:reportDate,'month')-1),'month')-1),'month') and         
//                 ( 
//                   nvl(cb_curr.cb_count,0) >= 50 and
//                   nvl(cb_prev.cb_count,0) >= 50 and
//                   ( 
//                     cb_curr.cb_count/
//                     decode( decode( :cardType, 
//                                     'VS',nvl(sm.visa_sales_count,0),
//                                     'MC',nvl(sm.mc_sales_count,0),
//                                     1 ),
//                             0, 1,
//                             decode( :cardType, 
//                                     'VS',nvl(sm.visa_sales_count,0),
//                                     'MC',nvl(sm.mc_sales_count,0),
//                                     0 )
//                           )                              
//                 ) >= 0.01 and
//                 ( 
//                   cb_prev.cb_count/
//                   decode( :cardType,
//                           'VS',decode(sm_prev.visa_sales_count,null,1,0,1,sm_prev.visa_sales_count),
//                           'MC',decode(sm_prev.mc_sales_count,null,1,0,1,sm_prev.mc_sales_count),
//                           1 )  
//                         ) >= 0.01                               
//                 ) and
//                 cb.merchant_number = mf.merchant_number and
//                 cb.incoming_date between trunc(:reportDate,'month') and last_day(:reportDate) and
//                 cb.card_type = :cardType and
//                 cb.first_time_chargeback = 'Y' 
//          order by cb.merchant_number ,cb.incoming_date      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.merchant_number                        as merchant_number,\n                cb.merchant_name                          as merchant_name,\n                cb.incoming_date                          as incoming_date,\n                cb.tran_date                              as tran_date,\n                 :1                                  as card_type,                \n                replace(cb.card_number,'xxxxxx','000000') as card_number,\n                cb.tran_amount                            as tran_amount,\n                cb.reference_number                       as ref_num,\n                cb.cb_ref_num                             as cb_ref_num,\n                cb.cb_load_sec                            as control_number\n        from    mif                      mf, \n               ( \n                 select  cbi.merchant_number       as merchant_number, \n                         count(cbi.cb_load_sec)    as cb_count, \n                         sum(cbi.tran_amount)      as cb_amount \n                 from    network_chargebacks   cbi \n                 where   cbi.bank_number in (3941,3942) and \n                         cbi.incoming_date between trunc( :2 ,'month') and last_day( :3 ) and \n                         cbi.card_type =  :4  and\n                        cbi.first_time_chargeback = 'Y'\n                 group by cbi.merchant_number \n               )                         cb_curr, \n               ( \n                 select  cbi.merchant_number       as merchant_number, \n                         count(cbi.cb_load_sec)    as cb_count, \n                         sum(cbi.tran_amount)      as cb_amount \n                 from    network_chargebacks   cbi \n                 where   cbi.bank_number in (3941,3942) and \n                         cbi.incoming_date between trunc((trunc( :5 ,'month')-1),'month') and (trunc( :6 ,'month')-1) and \n                         cbi.card_type =  :7  and\n                         cbi.first_time_chargeback = 'Y' \n                 group by cbi.merchant_number \n               )                         cb_prev, \n               monthly_extract_summary   sm, \n               monthly_extract_summary   sm_prev,\n               network_chargebacks       cb \n        where  mf.bank_number in (3942,3941) and \n               cb_curr.merchant_number = mf.merchant_number and \n               cb_prev.merchant_number = mf.merchant_number and \n               sm.merchant_number = mf.merchant_number and \n               sm.active_date = trunc((trunc( :8 ,'month')-1),'month') and\n               sm_prev.merchant_number = mf.merchant_number and \n               sm_prev.active_date = trunc((trunc((trunc( :9 ,'month')-1),'month')-1),'month') and         \n               ( \n                 nvl(cb_curr.cb_count,0) >= 50 and\n                 nvl(cb_prev.cb_count,0) >= 50 and\n                 ( \n                   cb_curr.cb_count/\n                   decode( decode(  :10 , \n                                   'VS',nvl(sm.visa_sales_count,0),\n                                   'MC',nvl(sm.mc_sales_count,0),\n                                   1 ),\n                           0, 1,\n                           decode(  :11 , \n                                   'VS',nvl(sm.visa_sales_count,0),\n                                   'MC',nvl(sm.mc_sales_count,0),\n                                   0 )\n                         )                              \n               ) >= 0.01 and\n               ( \n                 cb_prev.cb_count/\n                 decode(  :12 ,\n                         'VS',decode(sm_prev.visa_sales_count,null,1,0,1,sm_prev.visa_sales_count),\n                         'MC',decode(sm_prev.mc_sales_count,null,1,0,1,sm_prev.mc_sales_count),\n                         1 )  \n                       ) >= 0.01                               \n               ) and\n               cb.merchant_number = mf.merchant_number and\n               cb.incoming_date between trunc( :13 ,'month') and last_day( :14 ) and\n               cb.card_type =  :15  and\n               cb.first_time_chargeback = 'Y' \n        order by cb.merchant_number ,cb.incoming_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MasterCardECPDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,cardType);
   __sJT_st.setDate(2,reportDate);
   __sJT_st.setDate(3,reportDate);
   __sJT_st.setString(4,cardType);
   __sJT_st.setDate(5,reportDate);
   __sJT_st.setDate(6,reportDate);
   __sJT_st.setString(7,cardType);
   __sJT_st.setDate(8,reportDate);
   __sJT_st.setDate(9,reportDate);
   __sJT_st.setString(10,cardType);
   __sJT_st.setString(11,cardType);
   __sJT_st.setString(12,cardType);
   __sJT_st.setDate(13,reportDate);
   __sJT_st.setDate(14,reportDate);
   __sJT_st.setString(15,cardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MasterCardECPDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:430^7*/
      
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new DetailData(resultSet) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadDetailData()",e.toString() );
    }
    finally
    {
      try{ it.close(); } catch (Exception e) {}
    }
  }
  
  public void loadSummaryData( String cardType, Date reportDate )
  {
    ResultSetIterator    it             = null;
    ResultSet            resultSet      = null;
    
    try
    {
      ReportRows.clear();
      /*@lineinfo:generated-code*//*@lineinfo:459^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                  as merchant_number, 
//                  replace(mf.dba_name,',',' ')        as dba_name,
//                  decode( :cardType,
//                          'VS','Visa',
//                          'MC','MasterCard',
//                    :cardType )                       as card_type,         
//                  nvl(cb_curr.cb_count,0)             as cb_count_curr_month, 
//                  nvl(cb_curr.cb_amount,0)            as cb_amount_curr_month, 
//                  nvl(sm.mc_sales_count,0)            as sales_count_curr_month, 
//                  nvl(sm.mc_sales_amount,0)           as sales_amount_curr_month, 
//                  nvl(sm.mc_credits_count,0)          as credits_count_curr_month, 
//                  nvl(sm.mc_credits_amount,0)         as credits_amount_curr_month, 
//                  nvl(cb_prev.cb_count,0)             as cb_count_prev_month, 
//                  nvl(cb_prev.cb_amount,0)            as cb_amount_prev_month, 
//                  nvl(sm_prev.mc_sales_count,0)       as sales_count_prev_month, 
//                  nvl(sm_prev.mc_sales_amount,0)      as sales_amount_prev_month, 
//                  nvl(sm_prev.mc_credits_count,0)     as credits_count_prev_month, 
//                  nvl(sm_prev.mc_credits_amount,0)    as credits_amount_prev_month, 
//                  decode( nvl(cb_curr.cb_count,0), 
//                          0, null,
//                          round( (nvl(cb_curr.cb_count,0)/decode(nvl(sm_prev.mc_sales_count,0),0,1,sm_prev.mc_sales_count)),4 )     
//                  )                                   as curr_cb_to_prev_sales_ratio 
//          from    mif                    mf, 
//                 ( 
//                   select  cbi.merchant_number       as merchant_number, 
//                           count(cbi.cb_load_sec)    as cb_count, 
//                           sum(cbi.tran_amount)      as cb_amount 
//                   from    network_chargebacks   cbi 
//                   where   cbi.bank_number in (3941,3942) and 
//                           cbi.incoming_date between trunc(:reportDate,'month') and last_day(:reportDate) and 
//                           cbi.card_type = :cardType and
//                           cbi.first_time_chargeback = 'Y' 
//                   group by cbi.merchant_number 
//                 )                       cb_curr, 
//                 ( 
//                   select  cbi.merchant_number       as merchant_number, 
//                           count(cbi.cb_load_sec)    as cb_count, 
//                           sum(cbi.tran_amount)      as cb_amount 
//                   from    network_chargebacks   cbi 
//                   where   cbi.bank_number in (3941,3942) and 
//                           cbi.incoming_date between trunc((trunc(:reportDate,'month')-1),'month') and (trunc(:reportDate,'month')-1) and 
//                           cbi.card_type = :cardType and
//                           cbi.first_time_chargeback = 'Y' 
//                   group by cbi.merchant_number 
//                 )                       cb_prev, 
//                 monthly_extract_summary sm, 
//                 monthly_extract_summary sm_prev 
//          where  mf.bank_number in (3942,3941) and
//                 cb_curr.merchant_number(+) = mf.merchant_number and 
//                 cb_prev.merchant_number(+) = mf.merchant_number and 
//                 sm.merchant_number(+) = mf.merchant_number and 
//                 sm.active_date(+) = trunc(:reportDate,'month') and 
//                 -- mastercard rules
//                 nvl(cb_curr.cb_count,0) >= 50 and
//                 sm_prev.merchant_number(+) = mf.merchant_number and 
//                 sm_prev.active_date(+) = trunc((trunc(:reportDate,'month')-1),'month') 
//          order by curr_cb_to_prev_sales_ratio desc          
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                  as merchant_number, \n                replace(mf.dba_name,',',' ')        as dba_name,\n                decode(  :1 ,\n                        'VS','Visa',\n                        'MC','MasterCard',\n                   :2  )                       as card_type,         \n                nvl(cb_curr.cb_count,0)             as cb_count_curr_month, \n                nvl(cb_curr.cb_amount,0)            as cb_amount_curr_month, \n                nvl(sm.mc_sales_count,0)            as sales_count_curr_month, \n                nvl(sm.mc_sales_amount,0)           as sales_amount_curr_month, \n                nvl(sm.mc_credits_count,0)          as credits_count_curr_month, \n                nvl(sm.mc_credits_amount,0)         as credits_amount_curr_month, \n                nvl(cb_prev.cb_count,0)             as cb_count_prev_month, \n                nvl(cb_prev.cb_amount,0)            as cb_amount_prev_month, \n                nvl(sm_prev.mc_sales_count,0)       as sales_count_prev_month, \n                nvl(sm_prev.mc_sales_amount,0)      as sales_amount_prev_month, \n                nvl(sm_prev.mc_credits_count,0)     as credits_count_prev_month, \n                nvl(sm_prev.mc_credits_amount,0)    as credits_amount_prev_month, \n                decode( nvl(cb_curr.cb_count,0), \n                        0, null,\n                        round( (nvl(cb_curr.cb_count,0)/decode(nvl(sm_prev.mc_sales_count,0),0,1,sm_prev.mc_sales_count)),4 )     \n                )                                   as curr_cb_to_prev_sales_ratio \n        from    mif                    mf, \n               ( \n                 select  cbi.merchant_number       as merchant_number, \n                         count(cbi.cb_load_sec)    as cb_count, \n                         sum(cbi.tran_amount)      as cb_amount \n                 from    network_chargebacks   cbi \n                 where   cbi.bank_number in (3941,3942) and \n                         cbi.incoming_date between trunc( :3 ,'month') and last_day( :4 ) and \n                         cbi.card_type =  :5  and\n                         cbi.first_time_chargeback = 'Y' \n                 group by cbi.merchant_number \n               )                       cb_curr, \n               ( \n                 select  cbi.merchant_number       as merchant_number, \n                         count(cbi.cb_load_sec)    as cb_count, \n                         sum(cbi.tran_amount)      as cb_amount \n                 from    network_chargebacks   cbi \n                 where   cbi.bank_number in (3941,3942) and \n                         cbi.incoming_date between trunc((trunc( :6 ,'month')-1),'month') and (trunc( :7 ,'month')-1) and \n                         cbi.card_type =  :8  and\n                         cbi.first_time_chargeback = 'Y' \n                 group by cbi.merchant_number \n               )                       cb_prev, \n               monthly_extract_summary sm, \n               monthly_extract_summary sm_prev \n        where  mf.bank_number in (3942,3941) and\n               cb_curr.merchant_number(+) = mf.merchant_number and \n               cb_prev.merchant_number(+) = mf.merchant_number and \n               sm.merchant_number(+) = mf.merchant_number and \n               sm.active_date(+) = trunc( :9 ,'month') and \n               -- mastercard rules\n               nvl(cb_curr.cb_count,0) >= 50 and\n               sm_prev.merchant_number(+) = mf.merchant_number and \n               sm_prev.active_date(+) = trunc((trunc( :10 ,'month')-1),'month') \n        order by curr_cb_to_prev_sales_ratio desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MasterCardECPDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,cardType);
   __sJT_st.setString(2,cardType);
   __sJT_st.setDate(3,reportDate);
   __sJT_st.setDate(4,reportDate);
   __sJT_st.setString(5,cardType);
   __sJT_st.setDate(6,reportDate);
   __sJT_st.setDate(7,reportDate);
   __sJT_st.setString(8,cardType);
   __sJT_st.setDate(9,reportDate);
   __sJT_st.setDate(10,reportDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MasterCardECPDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:518^7*/

      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new SummaryData(resultSet) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadSummaryData()",e.toString() );
    }
    finally
    {
      try{ it.close(); } catch (Exception e) {}
    }
  }
}/*@lineinfo:generated-code*/