/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/EpicorStagingDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000-2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.config.ConfigurationManager;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class EpicorStagingDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public long         ImportId                    = 0L;
    public Timestamp    ImportDate                  = null;
    public int          ProcessStatusCode           = -1;
    public Timestamp    ProcessDate                 = null;
    public String       ProcessMessage              = null;
    public String       VendorCode                  = null;
    public String       VoucherNumber               = null;
    public Date         VoucherDate                 = null;
    public double       VoucherDetailAmount         = 0.0;
    public String       VoucherDetailDescription    = null;

    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      ImportId                   = resultSet.getLong  ("import_id");
      ImportDate                 = resultSet.getTimestamp("import_date");
      ProcessStatusCode          = resultSet.getInt   ("status_code");
      ProcessDate                = resultSet.getTimestamp("process_date");
      ProcessMessage             = processString(resultSet.getString("process_message"));
      VendorCode                 = processString(resultSet.getString("vendor_code"));
      VoucherNumber              = processString(resultSet.getString("voucher_number"));
      VoucherDate                = resultSet.getDate  ("voucher_date");
      VoucherDetailAmount        = resultSet.getDouble("voucher_amount");
      VoucherDetailDescription   = processString(resultSet.getString("voucher_desc"));
    }
  }
  
  public EpicorStagingDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Import ID\",");
    line.append("\"Import Date\",");
    line.append("\"Process Status Code\",");
    line.append("\"Process Date\",");
    line.append("\"Process Message\",");
    line.append("\"Vendor Code\",");
    line.append("\"Voucher Number\",");
    line.append("\"Voucher Date\",");
    line.append("\"Voucher Amount\",");
    line.append("\"Voucher Description\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append(record.ImportId);
    line.append(",");                
    line.append(DateTimeFormatter.getFormattedDate(record.ImportDate,"MM/dd/yyyy hh:mm"));
    line.append(",");              
    line.append(record.ProcessStatusCode);
    line.append(",");       
    line.append(DateTimeFormatter.getFormattedDate(record.ProcessDate,"MM/dd/yyyy"));
    line.append(",\"");
    line.append(record.ProcessMessage);
    line.append("\",\"");          
    line.append(record.VendorCode);
    line.append("\",\"");              
    line.append(record.VoucherNumber);
    line.append("\",");           
    line.append(DateTimeFormatter.getFormattedDate(record.VoucherDate,"MM/dd/yyyy"));
    line.append(",");             
    line.append(record.VoucherDetailAmount);
    line.append(",\"");     
    line.append(record.VoucherDetailDescription);
    line.append("\"");
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_epicor_staging_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMMyyyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMMyyyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    Calendar                      cal               = Calendar.getInstance();
    ResultSet                     resultSet         = null;
    String                        sqlText           = null;
    Date                          voucherDate       = null;
	ResultSetIterator it = null;

    
    try
    {
        try {
            // open main events properties file
            ConfigurationManager.getInstance().loadTEMConfiguration(ConfigurationManager.TEM_PROPERTY_FILE_DEFAULT);
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }

      // empty the current contents
      ReportRows.clear();
      
      cal.setTime(beginDate);
      cal.add(Calendar.MONTH,1);
//@      cal.add(Calendar.DAY_OF_MONTH,-1);
      voucherDate = new java.sql.Date(cal.getTime().getTime());
      
      sqlText = " select  Import_ID                     as import_id,       "
              + "         Import_Date                   as import_date,     "
              + "         Process_Status_Code           as status_code,     "
              + "         Process_Date                  as process_date,    "
              + "         Process_Message               as process_message, "
              + "         Vendor_code                   as vendor_code,     "
              + "         Voucher_Number                as voucher_number,  "
              + "         Voucher_Date                  as voucher_date,    "
              + "         Voucher_Detail_Amount         as voucher_amount,  "
              + "         Voucher_Detail_Description    as voucher_desc     "
              +	" from    MES.esc_voucher_staging            " + " where   voucher_date between :1 and :2                      ";
      	{
			// declare temps
			oracle.jdbc.OraclePreparedStatement __sJT_st = null;
			sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx;
			if (__sJT_cc == null)
				sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
			sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext() == null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
			try {
				__sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc, "1com.mes.reports.EpicorStagingDataBean", sqlText);
				// set IN parameters
				__sJT_st.setDate(1, beginDate);
				__sJT_st.setDate(2, voucherDate);
				// execute query
				it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(), __sJT_st, "1com.mes.reports.EpicorStagingDataBean", null));
			}
			finally {
				__sJT_ec.oracleCloseQuery();
			}
		}
		resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
    	try {
    		it.close();
    	}
    	catch (Exception e) {
    	}

    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    // if the from date is today, then set the default to rolling previous week
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.MONTH, -1 );
      
      // since this report is for one month at a time,
      // make sure that the from date always starts with
      // the first day of the specified month.
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      // set both the begin and end dates
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
    }    
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}