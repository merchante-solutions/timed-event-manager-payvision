/*@lineinfo:filename=MerchACHDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchACHDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-08-09 14:57:56 -0700 (Mon, 09 Aug 2010) $
  Version            : $Revision: 17687 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class MerchACHDataBean extends OrgSummaryDataBean
{
  public class FxChargebackDetailRow
  {
    public String         CaseId              = null;
    public Date           IncomingDate        = null;
    public long           ReferenceNumber     = 0L;
    
    public FxChargebackDetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CaseId          = resultSet.getString("case_id");
      IncomingDate    = resultSet.getDate("incoming_date");
      ReferenceNumber = resultSet.getLong("ref_num");  
    }
  }
  
  public class DetailRow
  {
    public double         Amount              = 0.0;
    public String         Description         = null;
    public String         EntryDesc           = null;
    public long           FxCbId              = 0L;
    public long           HierarchyNode       = 0L;
    public String         OrgName             = null;
    public Date           PostDate            = null;
    public String         ReferenceNumber     = null;
    public String         AchSource           = "";
    
    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      OrgName           = resultSet.getString("org_name");
      Description       = resultSet.getString("description");
      EntryDesc         = resultSet.getString("entry_desc");
      Amount            = resultSet.getDouble("amount");
      PostDate          = resultSet.getDate("post_date");
      ReferenceNumber   = resultSet.getString("ref_num");
      AchSource         = resultSet.getString("ach_source");
      
      try
      {
        FxCbId            = resultSet.getLong("fx_cb_id");
      }
      catch( Exception e )
      {
        // record does not include fx cb info, ignore
      }
    }
  }

  // Member Variables  
  protected boolean           FindDeposit       = false;
  protected double            FindDepositAmount = 0.0;
  protected boolean           FindDataValid     = false;
  protected String            FindOp            = "=";
  protected boolean           FxCbDataPresent   = false;
  
  public MerchACHDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_SUMMARY )
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"Deposit Cnt\",");
      line.append("\"Deposit Amt\"");
    }
    else  // RT_DETAILS
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"Date\",");
      line.append("\"Type\",");
      line.append("\"Amount\",");
      line.append("\"Reference\"");
      if ( hasFxCbData() )
      {
        line.append(",\"Case ID\"");
        line.append(",\"Reference #\"");
      }
    }      
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    if ( ReportType == RT_SUMMARY )
    {
      MesOrgSummaryEntry  record    = (MesOrgSummaryEntry)obj;
    
      line.append( encodeHierarchyNode(record.getHierarchyNode()) );
      line.append( ",\"" );
      line.append( record.getOrgName() );
      line.append( "\"," );
      line.append( record.getCount() );
      line.append( "," );
      line.append( record.getAmount() );
    }
    else    // RT_DETAILS
    {
      DetailRow  record    = (DetailRow)obj;
    
      line.append( encodeHierarchyNode(record.HierarchyNode) );
      line.append( ",\"" );
      line.append( record.OrgName );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate(record.PostDate,"MM/dd/yyyy") );
      line.append( ",\"" );
      line.append( record.Description );
      line.append( "\"," );
      line.append( record.Amount );
      line.append( ",\"'" );
      line.append( record.ReferenceNumber );
      line.append( "\"" );
      if ( hasFxCbData() )
      {
        FxChargebackDetailRow cbData = loadFxCbData(record.HierarchyNode,record.FxCbId);
        if ( cbData == null )
        {
          line.append(",\"\",\"\"");
        }
        else
        {
          line.append(",\"");
          line.append(cbData.CaseId);
          line.append("\",\"");
          line.append(cbData.ReferenceNumber);
          line.append("\"");
        }
      }
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    
    if ( ReportType == RT_SUMMARY )
    {
      filename.append("_deposit_summary_");
    }
    else  // RT_DETAILS
    {
      filename.append("_deposit_details_");
    }      
    
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"MMddyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"MMddyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public double getFindDepositAmount( )
  {
    return( FindDepositAmount );
  }
  
  public char getFindDepositOp()
  {
    return( FindOp.charAt(0) );
  }
  
  public boolean hasFxCbData()
  {
    return( FxCbDataPresent );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public boolean isFindDepositData( )
  {
    return( (FindDeposit == true) && 
            (FindDataValid == true) && 
            (ReportType == RT_DETAILS) );
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      if ( FindDeposit == true )
      {
        if ( FindDataValid == true )
        {
          // ********************************************************
          // log usage of this function to see if it is being misused
          /*@lineinfo:generated-code*//*@lineinfo:264^11*/

//  ************************************************************
//  #sql [Ctx] { insert into find_ach_log
//              (
//                date_accessed,
//                accessed_by,
//                find_node,
//                begin_date,
//                end_date,
//                find_op,
//                find_amount
//              )
//              values
//              (
//                sysdate,
//                :ReportUserBean.getLoginName(),
//                :getReportHierarchyNode(),
//                :beginDate,
//                :endDate,
//                :FindOp,
//                trunc(:FindDepositAmount, 2)
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1141 = ReportUserBean.getLoginName();
 long __sJT_1142 = getReportHierarchyNode();
   String theSqlTS = "insert into find_ach_log\n            (\n              date_accessed,\n              accessed_by,\n              find_node,\n              begin_date,\n              end_date,\n              find_op,\n              find_amount\n            )\n            values\n            (\n              sysdate,\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n              trunc( :6 , 2)\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.reports.MerchACHDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1141);
   __sJT_st.setLong(2,__sJT_1142);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   __sJT_st.setString(5,FindOp);
   __sJT_st.setDouble(6,FindDepositAmount);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:286^11*/
          
          commit();
          // ********************************************************
          
          /*@lineinfo:generated-code*//*@lineinfo:291^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ORDERED USE_NL (ach) FIRST_ROWS */
//                      mf.merchant_number                        as hierarchy_node,
//                      mf.dba_name                               as org_name,
//                      ach.post_date_option                      as post_date,
//                      nvl(tc.ach_detail_tran_desc,
//                          ach.transaction_code)                 as description,
//                      ab.company_entry_description              as entry_desc,                      
//                      ( ach.amount_of_transaction *
//                        decode(tc.debit_credit_indicator,
//                               'D',-1,
//                               1) )                             as amount,
//                      nvl(to_char(ach.trace_sequence_number),
//                          ach.fx_reference_number)              as ref_num,
//                      ach.fx_cb_id                              as fx_cb_id
//              from    group_merchant            gm,
//                      mif                       mf,
//                      ach_detail                ach,
//                      ach_batch                 ab,
//                      ach_detail_tran_code      tc
//              where   gm.org_num          = :orgId and
//                      mf.merchant_number  = gm.merchant_number and 
//                      ach.merchant_number = mf.merchant_number and
//                      ach.post_date_option between :beginDate and :endDate and
//                      ach.batch_id = ab.batch_id and
//                      tc.ach_detail_trans_code(+) = ach.transaction_code and
//                      (
//                        ( :FindOp = '<' and ach.amount_of_transaction < :FindDepositAmount ) or 
//                        ( :FindOp = '=' and ach.amount_of_transaction = :FindDepositAmount ) or 
//                        ( :FindOp = '>' and ach.amount_of_transaction > :FindDepositAmount )
//                      )
//              order by mf.merchant_number, ach.post_date_option
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ORDERED USE_NL (ach) FIRST_ROWS */\n                    mf.merchant_number                        as hierarchy_node,\n                    mf.dba_name                               as org_name,\n                    ach.post_date_option                      as post_date,\n                    nvl(tc.ach_detail_tran_desc,\n                        ach.transaction_code)                 as description,\n                    ab.company_entry_description              as entry_desc,                      \n                    ( ach.amount_of_transaction *\n                      decode(tc.debit_credit_indicator,\n                             'D',-1,\n                             1) )                             as amount,\n                    nvl(to_char(ach.trace_sequence_number),\n                        ach.fx_reference_number)              as ref_num,\n                    ach.fx_cb_id                              as fx_cb_id\n            from    group_merchant            gm,\n                    mif                       mf,\n                    ach_detail                ach,\n                    ach_batch                 ab,\n                    ach_detail_tran_code      tc\n            where   gm.org_num          =  :1  and\n                    mf.merchant_number  = gm.merchant_number and \n                    ach.merchant_number = mf.merchant_number and\n                    ach.post_date_option between  :2  and  :3  and\n                    ach.batch_id = ab.batch_id and\n                    tc.ach_detail_trans_code(+) = ach.transaction_code and\n                    (\n                      (  :4  = '<' and ach.amount_of_transaction <  :5  ) or \n                      (  :6  = '=' and ach.amount_of_transaction =  :7  ) or \n                      (  :8  = '>' and ach.amount_of_transaction >  :9  )\n                    )\n            order by mf.merchant_number, ach.post_date_option";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchACHDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setString(4,FindOp);
   __sJT_st.setDouble(5,FindDepositAmount);
   __sJT_st.setString(6,FindOp);
   __sJT_st.setDouble(7,FindDepositAmount);
   __sJT_st.setString(8,FindOp);
   __sJT_st.setDouble(9,FindDepositAmount);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchACHDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:324^11*/
        }
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:329^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ORDERED USE_NL (ach) FIRST_ROWS */
//                    mf.merchant_number                        as hierarchy_node,
//                    mf.dba_name                               as org_name,
//                    ach.post_date_option                      as post_date,
//                    nvl(tc.ach_detail_tran_desc,
//                        ach.transaction_code)                 as description,
//                    ab.company_entry_description              as entry_desc,                      
//                    ( ach.amount_of_transaction *
//                      decode(tc.debit_credit_indicator,
//                             'D',-1,
//                             1) )                             as amount,
//                    nvl(to_char(ach.trace_sequence_number),
//                        ach.fx_reference_number)              as ref_num,
//                    ach.fx_cb_id                              as fx_cb_id,
//                    decode(substr(ach.load_filename, 1, 3),
//                      'ach',  'TSYS',
//                      'fee',  'TSYS',
//                      'mac',  'MES',
//                      'TSYS')                                 as ach_source
//            from    group_merchant            gm,
//                    mif                       mf,
//                    ach_detail                ach,
//                    ach_batch                 ab,
//                    ach_detail_tran_code      tc
//            where   gm.org_num          = :orgId and
//                    mf.merchant_number  = gm.merchant_number and 
//                    ach.merchant_number = mf.merchant_number and
//                    ach.post_date_option between :beginDate and :endDate and
//                    ach.batch_id = ab.batch_id and
//                    tc.ach_detail_trans_code(+) = ach.transaction_code               
//            order by mf.merchant_number, ach.post_date_option
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ORDERED USE_NL (ach) FIRST_ROWS */\n                  mf.merchant_number                        as hierarchy_node,\n                  mf.dba_name                               as org_name,\n                  ach.post_date_option                      as post_date,\n                  nvl(tc.ach_detail_tran_desc,\n                      ach.transaction_code)                 as description,\n                  ab.company_entry_description              as entry_desc,                      \n                  ( ach.amount_of_transaction *\n                    decode(tc.debit_credit_indicator,\n                           'D',-1,\n                           1) )                             as amount,\n                  nvl(to_char(ach.trace_sequence_number),\n                      ach.fx_reference_number)              as ref_num,\n                  ach.fx_cb_id                              as fx_cb_id,\n                  decode(substr(ach.load_filename, 1, 3),\n                    'ach',  'TSYS',\n                    'fee',  'TSYS',\n                    'mac',  'MES',\n                    'TSYS')                                 as ach_source\n          from    group_merchant            gm,\n                  mif                       mf,\n                  ach_detail                ach,\n                  ach_batch                 ab,\n                  ach_detail_tran_code      tc\n          where   gm.org_num          =  :1  and\n                  mf.merchant_number  = gm.merchant_number and \n                  ach.merchant_number = mf.merchant_number and\n                  ach.post_date_option between  :2  and  :3  and\n                  ach.batch_id = ab.batch_id and\n                  tc.ach_detail_trans_code(+) = ach.transaction_code               \n          order by mf.merchant_number, ach.post_date_option";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchACHDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchACHDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:362^9*/
      }        
       
      // it will be null if a find was requested
      // with invalid find data.
      if ( it != null )
      {
        resultSet = it.getResultSet();
  
        while( resultSet.next() )
        {
          if ( resultSet.getLong("fx_cb_id") != 0L )
          {
            setFxCbDataPresent(true);
          }
          ReportRows.addElement( new DetailRow( resultSet ) );
        }
        resultSet.close();
        it.close();
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public FxChargebackDetailRow loadFxCbData( long merchantId, long fxCbId )
  {
    ResultSetIterator       it          = null;
    ResultSet               resultSet   = null;
    FxChargebackDetailRow   retVal      = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:400^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.incoming_date            as incoming_date,
//                  cb.case_id                  as case_id,
//                  cb.reference_number         as ref_num
//          from    payvision_api_cb_records  cb
//          where   cb.merchant_number = :merchantId
//                  and cb.cb_id = :fxCbId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.incoming_date            as incoming_date,\n                cb.case_id                  as case_id,\n                cb.reference_number         as ref_num\n        from    payvision_api_cb_records  cb\n        where   cb.merchant_number =  :1 \n                and cb.cb_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchACHDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,fxCbId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchACHDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:408^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      retVal = new FxChargebackDetailRow(resultSet);
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadFxCbData(" + merchantId + "," + fxCbId + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
    return( retVal );
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    
    try
    {
      if ( hasAssocDistricts() )
      // returns false unless the current node is an
      // association and it has districts under it.
      {
        if ( District == DISTRICT_NONE )
        {
          /*@lineinfo:generated-code*//*@lineinfo:438^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      INDEX (gm pkgroup_merchant) 
//                      INDEX (sm pk_ach_summary)
//                     */
//                      o.org_num                                 as org_num,
//                      o.org_group                               as hierarchy_node,
//                      nvl(mf.district,:DISTRICT_UNASSIGNED)     as district,
//                      nvl(ad.district_desc,decode( mf.district,
//                                  null,'Unassigned',
//                                 ('District ' || to_char(mf.district, '0009')))) as org_name,
//                      sum(sm.debit_count + sm.credit_count)     as item_count,
//                      sum(sm.credit_amount - sm.debit_amount)   as item_amount
//              from    organization              o,
//                      group_merchant            gm,
//                      group_rep_merchant        grm,
//                      ach_summary               sm,
//                      mif                       mf,
//                      assoc_districts           ad
//              where   o.org_num           = :orgId and
//                      gm.org_num          = o.org_num and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      sm.merchant_number(+) = mf.merchant_number and
//                      sm.post_date(+) between :beginDate and :endDate and
//                      ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                      ad.district(+)      = mf.district
//              group by  o.org_num, o.org_group, mf.district, ad.district_desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    INDEX (gm pkgroup_merchant) \n                    INDEX (sm pk_ach_summary)\n                   */\n                    o.org_num                                 as org_num,\n                    o.org_group                               as hierarchy_node,\n                    nvl(mf.district, :1 )     as district,\n                    nvl(ad.district_desc,decode( mf.district,\n                                null,'Unassigned',\n                               ('District ' || to_char(mf.district, '0009')))) as org_name,\n                    sum(sm.debit_count + sm.credit_count)     as item_count,\n                    sum(sm.credit_amount - sm.debit_amount)   as item_amount\n            from    organization              o,\n                    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    ach_summary               sm,\n                    mif                       mf,\n                    assoc_districts           ad\n            where   o.org_num           =  :2  and\n                    gm.org_num          = o.org_num and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    sm.merchant_number(+) = mf.merchant_number and\n                    sm.post_date(+) between  :5  and  :6  and\n                    ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                    ad.district(+)      = mf.district\n            group by  o.org_num, o.org_group, mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchACHDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchACHDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:469^11*/
        }
        else    // a district was specified
        {
          /*@lineinfo:generated-code*//*@lineinfo:473^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                        INDEX (gm pkgroup_merchant) 
//                        INDEX (sm pk_ach_summary)
//                     */
//                      o.org_num                                 as org_num,
//                      mf.merchant_number                        as hierarchy_node,
//                      mf.dba_name                               as org_name,
//                      :District                                 as district,
//                      sum(sm.debit_count + sm.credit_count)     as item_count,
//                      sum(sm.credit_amount - sm.debit_amount)   as item_amount
//              from    group_merchant            gm,
//                      group_rep_merchant        grm,
//                      ach_summary               sm,
//                      mif                       mf,
//                      organization              o
//              where   gm.org_num          = :orgId and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      nvl(mf.district,-1) = :District and
//                      sm.merchant_number(+)  = mf.merchant_number and
//                      sm.post_date(+) between :beginDate and :endDate and
//                      o.org_group = mf.merchant_number
//              group by o.org_num, mf.merchant_number, mf.dba_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                      INDEX (gm pkgroup_merchant) \n                      INDEX (sm pk_ach_summary)\n                   */\n                    o.org_num                                 as org_num,\n                    mf.merchant_number                        as hierarchy_node,\n                    mf.dba_name                               as org_name,\n                     :1                                  as district,\n                    sum(sm.debit_count + sm.credit_count)     as item_count,\n                    sum(sm.credit_amount - sm.debit_amount)   as item_amount\n            from    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    ach_summary               sm,\n                    mif                       mf,\n                    organization              o\n            where   gm.org_num          =  :2  and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    nvl(mf.district,-1) =  :5  and\n                    sm.merchant_number(+)  = mf.merchant_number and\n                    sm.post_date(+) between  :6  and  :7  and\n                    o.org_group = mf.merchant_number\n            group by o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchACHDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchACHDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:500^11*/
        }
      }
      else // just standard child report, no districts
      {
        /*@lineinfo:generated-code*//*@lineinfo:505^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                       INDEX (gm pkgroup_merchant) 
//                       INDEX (sm pk_ach_summary)
//                    */
//                    o.org_num                                 as org_num,
//                    o.org_group                               as hierarchy_node,
//                    o.org_name                                as org_name,
//                    0                                         as district,
//                    sum(sm.debit_count + sm.credit_count)     as item_count,
//                    sum(sm.credit_amount - sm.debit_amount)   as item_amount
//            from    parent_org                po,
//                    organization              o,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    ach_summary               sm
//            where   po.parent_org_num   = :orgId and
//                    o.org_num           = po.org_num and
//                    gm.org_num(+)       = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    sm.merchant_number(+)  = gm.merchant_number and
//                    sm.post_date(+) between :beginDate and :endDate
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                     INDEX (gm pkgroup_merchant) \n                     INDEX (sm pk_ach_summary)\n                  */\n                  o.org_num                                 as org_num,\n                  o.org_group                               as hierarchy_node,\n                  o.org_name                                as org_name,\n                  0                                         as district,\n                  sum(sm.debit_count + sm.credit_count)     as item_count,\n                  sum(sm.credit_amount - sm.debit_amount)   as item_amount\n          from    parent_org                po,\n                  organization              o,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  ach_summary               sm\n          where   po.parent_org_num   =  :1  and\n                  o.org_num           = po.org_num and\n                  gm.org_num(+)       = o.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  sm.merchant_number(+)  = gm.merchant_number and\n                  sm.post_date(+) between  :4  and  :5 \n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MerchACHDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.MerchACHDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:531^9*/
      }
      processSummaryData(it.getResultSet());
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setFxCbDataPresent( boolean flag )
  {
    FxCbDataPresent = flag;
  }
  
  public void setProperties( HttpServletRequest request )
  {
    long            findNode      = 0L;
    
    super.setProperties(request);
    
    if( (FindDeposit = HttpHelper.getBoolean(request,"findDeposit",false)) == true )
    {
      FindOp            = HttpHelper.getString(request,"findOp","=");
      FindDepositAmount = HttpHelper.getDouble(request,"findDepositAmount",0.0);
      FindDataValid     = true;     // assume valid
      
      if ( FindDepositAmount == 0.0 )
      {
        addError("Must specify a deposit amount greater than $0.00.");
        FindDataValid = false;
      }
      
      // validate the find hierarchy node.  if it is not under the login
      // hierarchy node, the set the node to the current login node.
      findNode = HttpHelper.getLong(request,"findHierarchyNode",0L);
      if ( ! isOrgParentOfNode( getReportOrgIdDefault(), findNode ) )
      {
        ErrorMessage.setLength(0);
        ErrorMessage.append( "Requested node " );
        ErrorMessage.append( findNode );
        ErrorMessage.append( " is invalid because it is not a child of " );
        ErrorMessage.append( getReportHierarchyNodeDefault() );
        addError( ErrorMessage.toString() );
        FindDataValid = false;
      }
      else 
      {
        // node is valid, so set the report node
        setReportHierarchyNode(findNode);
        
        // if the node specified is not an association or a merchant
        // node, then add a message for the user and disarm the query.
        if ( !isNodeAssociation(findNode) && !isNodeMerchant(findNode) )
        {
          ErrorMessage.setLength(0);
          ErrorMessage.append( "Requested node must be a merchant or association.  " );
          ErrorMessage.append( findNode );
          ErrorMessage.append( " is a group." );
          addError( ErrorMessage.toString() );
          FindDataValid = false;
        }
      }
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/