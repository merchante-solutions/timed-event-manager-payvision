/*@lineinfo:filename=MerchDDFDepositDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchDDFDepositDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class MerchDDFDepositDataBean extends MerchACHDataBean
{
  public MerchDDFDepositDataBean( )
  {
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    boolean                       addIt             = false;
    double                        depositAmount     = 0.0;
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:62^7*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                     INDEX (gm pkgroup_merchant) 
//                  */
//                  mf.merchant_number                        as hierarchy_node,
//                  mf.dba_name                               as org_name,
//                  dt.batch_date                             as post_date,
//                  dt.batch_number                           as batch_number,
//                  decode( (sum(dt.transaction_amount *   
//                               decode(dt.debit_credit_indicator,'C',-1,1))
//                            /
//                           abs(sum(dt.transaction_amount *   
//                                   decode(dt.debit_credit_indicator,'C',-1,1)))),
//                          -1,'Debit','Credit')              as description,
//                  sum(dt.transaction_amount *   
//                      decode(dt.debit_credit_indicator,
//                             'C',-1,1))                     as amount,
//                  'Not Available'                           as ref_num
//          from    group_merchant            gm,
//                  mif                       mf,
//                  daily_detail_file_dt      dt
//          where   gm.org_num          = :orgId and
//                  mf.merchant_number  = gm.merchant_number and 
//                  dt.merchant_account_number = mf.merchant_number and
//                  dt.batch_date between :beginDate and :endDate
//          group by mf.merchant_number, mf.dba_name, dt.batch_date, 
//                   dt.batch_number                
//          order by mf.merchant_number, dt.batch_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                   INDEX (gm pkgroup_merchant) \n                */\n                mf.merchant_number                        as hierarchy_node,\n                mf.dba_name                               as org_name,\n                dt.batch_date                             as post_date,\n                dt.batch_number                           as batch_number,\n                decode( (sum(dt.transaction_amount *   \n                             decode(dt.debit_credit_indicator,'C',-1,1))\n                          /\n                         abs(sum(dt.transaction_amount *   \n                                 decode(dt.debit_credit_indicator,'C',-1,1)))),\n                        -1,'Debit','Credit')              as description,\n                sum(dt.transaction_amount *   \n                    decode(dt.debit_credit_indicator,\n                           'C',-1,1))                     as amount,\n                'Not Available'                           as ref_num\n        from    group_merchant            gm,\n                mif                       mf,\n                daily_detail_file_dt      dt\n        where   gm.org_num          =  :1  and\n                mf.merchant_number  = gm.merchant_number and \n                dt.merchant_account_number = mf.merchant_number and\n                dt.batch_date between  :2  and  :3 \n        group by mf.merchant_number, mf.dba_name, dt.batch_date, \n                 dt.batch_number                \n        order by mf.merchant_number, dt.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchDDFDepositDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchDDFDepositDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:91^7*/
      
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        if ( FindDeposit == true )
        {
          if ( FindDataValid == true )
          {
            depositAmount = resultSet.getDouble("amount");
            switch( FindOp.charAt(0) )
            {
              case '<':
                addIt = ( depositAmount < FindDepositAmount );
                break;
                
              case '=':
                addIt = ( depositAmount == FindDepositAmount );
                break;
              
              case '>':
                addIt = ( depositAmount > FindDepositAmount );
                break;
                
              default:
                addIt = false;
                break;
            }
          }
        }
        else
        {
          addIt = true;     // always add regular detail records
        }
        if ( addIt )
        {
          ReportRows.addElement( new DetailRow( resultSet ) );
        }
      }
      resultSet.close();
      it.close();
      
      // add the adjustments
      /*@lineinfo:generated-code*//*@lineinfo:135^7*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                     INDEX (gm pkgroup_merchant) 
//                  */
//                  mf.merchant_number                        as hierarchy_node,
//                  mf.dba_name                               as org_name,
//                  adj.batch_date                            as post_date,
//                  nvl(td.ach_tran_desc,
//                      'Adjustment Code ' || 
//                       adj.transacction_code)               as description,
//                  ( adj.adjustment_amount *
//                    decode(adj.debit_credit_ind,
//                           'C',1,-1) )                      as amount,
//                  adj.reference_number                      as ref_num
//          from    group_merchant                gm,
//                  mif                           mf,
//                  daily_detail_file_adjustment  adj,
//                  vital_ach_tran_desc           td
//          where   gm.org_num          = :orgId and
//                  mf.merchant_number  = gm.merchant_number and 
//                  adj.merchant_account_number = mf.merchant_number and
//                  adj.batch_date between :beginDate and :endDate and
//                  td.ACH_TRAN_CODE(+) = adj.transacction_code
//          order by mf.merchant_number, adj.batch_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                   INDEX (gm pkgroup_merchant) \n                */\n                mf.merchant_number                        as hierarchy_node,\n                mf.dba_name                               as org_name,\n                adj.batch_date                            as post_date,\n                nvl(td.ach_tran_desc,\n                    'Adjustment Code ' || \n                     adj.transacction_code)               as description,\n                ( adj.adjustment_amount *\n                  decode(adj.debit_credit_ind,\n                         'C',1,-1) )                      as amount,\n                adj.reference_number                      as ref_num\n        from    group_merchant                gm,\n                mif                           mf,\n                daily_detail_file_adjustment  adj,\n                vital_ach_tran_desc           td\n        where   gm.org_num          =  :1  and\n                mf.merchant_number  = gm.merchant_number and \n                adj.merchant_account_number = mf.merchant_number and\n                adj.batch_date between  :2  and  :3  and\n                td.ACH_TRAN_CODE(+) = adj.transacction_code\n        order by mf.merchant_number, adj.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchDDFDepositDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchDDFDepositDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:160^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        if ( FindDeposit == true )
        {
          if ( FindDataValid == true )
          {
            depositAmount = resultSet.getDouble("amount");
            switch( FindOp.charAt(0) )
            {
              case '<':
                addIt = ( depositAmount < FindDepositAmount );
                break;
                
              case '=':
                addIt = ( depositAmount == FindDepositAmount );
                break;
              
              case '>':
                addIt = ( depositAmount > FindDepositAmount );
                break;
                
              default:
                addIt = false;
                break;
            }
          }
        }
        else
        {
          addIt = true;     // always add regular detail records
        }
        if ( addIt )
        {
          ReportRows.addElement( new DetailRow( resultSet ) );
        }
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    
    try
    {
      if ( hasAssocDistricts() )
      // returns false unless the current node is an
      // association and it has districts under it.
      {
        if ( District == DISTRICT_NONE )
        {
          /*@lineinfo:generated-code*//*@lineinfo:224^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      INDEX (gm pkgroup_merchant) 
//                      INDEX (sm pk_ddf_summary) 
//                     */
//                      o.org_num                                 as org_num,
//                      o.org_group                               as hierarchy_node,
//                      nvl(mf.district,:DISTRICT_UNASSIGNED)     as district,
//                      nvl(ad.district_desc,decode( mf.district,
//                                  null,'Unassigned',
//                                 ('District ' || to_char(mf.district, '0009')))) as org_name,
//                      sum( nvl(sm.batch_count,0) )              as item_count,
//                      sum( nvl(sm.bank_amount,0) )              as item_amount
//              from    organization              o,
//                      group_merchant            gm,
//                      group_rep_merchant        grm,
//                      daily_detail_file_summary sm,
//                      mif                       mf,
//                      assoc_districts           ad
//              where   o.org_num           = :orgId and
//                      gm.org_num          = o.org_num and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      sm.merchant_number(+) = mf.merchant_number and
//                      sm.batch_date(+) between :beginDate and :endDate and
//                      ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                      ad.district(+)      = mf.district
//              group by  o.org_num, o.org_group, mf.district, ad.district_desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    INDEX (gm pkgroup_merchant) \n                    INDEX (sm pk_ddf_summary) \n                   */\n                    o.org_num                                 as org_num,\n                    o.org_group                               as hierarchy_node,\n                    nvl(mf.district, :1 )     as district,\n                    nvl(ad.district_desc,decode( mf.district,\n                                null,'Unassigned',\n                               ('District ' || to_char(mf.district, '0009')))) as org_name,\n                    sum( nvl(sm.batch_count,0) )              as item_count,\n                    sum( nvl(sm.bank_amount,0) )              as item_amount\n            from    organization              o,\n                    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    daily_detail_file_summary sm,\n                    mif                       mf,\n                    assoc_districts           ad\n            where   o.org_num           =  :2  and\n                    gm.org_num          = o.org_num and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    sm.merchant_number(+) = mf.merchant_number and\n                    sm.batch_date(+) between  :5  and  :6  and\n                    ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                    ad.district(+)      = mf.district\n            group by  o.org_num, o.org_group, mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchDDFDepositDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchDDFDepositDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^11*/
          processSummaryData(it.getResultSet());
          it.close();
          
          /*@lineinfo:generated-code*//*@lineinfo:259^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      o.org_group                               as hierarchy_node,
//                      nvl(mf.district,:DISTRICT_UNASSIGNED)     as district,
//                      nvl(ad.district_desc,decode( mf.district,
//                                  null,'Unassigned',
//                                 ('District ' || to_char(mf.district, '0009')))) as org_name,
//                      count( adj.load_filename )                as item_count,
//                      sum( adj.adjustment_amount *
//                           decode(adj.debit_credit_ind,
//                                  'C',1,-1) )                   as item_amount
//              from    organization                  o,
//                      group_merchant                gm,
//                      group_rep_merchant            grm,
//                      daily_detail_file_adjustment adj,
//                      mif                           mf,
//                      assoc_districts               ad
//              where   o.org_num           = :orgId and
//                      gm.org_num          = o.org_num and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      adj.merchant_account_number = mf.merchant_number and
//                      adj.batch_date between :beginDate and :endDate and
//                      ad.assoc_number     = (mf.bank_number || mf.dmagent) and
//                      ad.district         = mf.district
//              group by  o.org_num, o.org_group, mf.district, ad.district_desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    o.org_group                               as hierarchy_node,\n                    nvl(mf.district, :1 )     as district,\n                    nvl(ad.district_desc,decode( mf.district,\n                                null,'Unassigned',\n                               ('District ' || to_char(mf.district, '0009')))) as org_name,\n                    count( adj.load_filename )                as item_count,\n                    sum( adj.adjustment_amount *\n                         decode(adj.debit_credit_ind,\n                                'C',1,-1) )                   as item_amount\n            from    organization                  o,\n                    group_merchant                gm,\n                    group_rep_merchant            grm,\n                    daily_detail_file_adjustment adj,\n                    mif                           mf,\n                    assoc_districts               ad\n            where   o.org_num           =  :2  and\n                    gm.org_num          = o.org_num and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    adj.merchant_account_number = mf.merchant_number and\n                    adj.batch_date between  :5  and  :6  and\n                    ad.assoc_number     = (mf.bank_number || mf.dmagent) and\n                    ad.district         = mf.district\n            group by  o.org_num, o.org_group, mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchDDFDepositDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchDDFDepositDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:291^11*/
          processSummaryData(it.getResultSet(),true);
          it.close();
        }
        else    // a district was specified
        {
          /*@lineinfo:generated-code*//*@lineinfo:297^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                        INDEX (gm pkgroup_merchant) 
//                        INDEX (sm pk_ddf_summary) 
//                     */
//                      o.org_num                                 as org_num,
//                      mf.merchant_number                        as hierarchy_node,
//                      mf.dba_name                               as org_name,
//                      :District                                 as district,
//                      sum( nvl(sm.batch_count,0) )              as item_count,
//                      sum( nvl(sm.bank_amount,0) )              as item_amount
//              from    group_merchant            gm,
//                      group_rep_merchant        grm,
//                      daily_detail_file_summary sm,
//                      mif                       mf,
//                      organization              o
//              where   gm.org_num          = :orgId and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      nvl(mf.district,-1) = :District and
//                      sm.merchant_number(+)  = mf.merchant_number and
//                      sm.batch_date(+) between :beginDate and :endDate and
//                      o.org_group = mf.merchant_number
//              group by o.org_num, mf.merchant_number, mf.dba_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                      INDEX (gm pkgroup_merchant) \n                      INDEX (sm pk_ddf_summary) \n                   */\n                    o.org_num                                 as org_num,\n                    mf.merchant_number                        as hierarchy_node,\n                    mf.dba_name                               as org_name,\n                     :1                                  as district,\n                    sum( nvl(sm.batch_count,0) )              as item_count,\n                    sum( nvl(sm.bank_amount,0) )              as item_amount\n            from    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    daily_detail_file_summary sm,\n                    mif                       mf,\n                    organization              o\n            where   gm.org_num          =  :2  and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    nvl(mf.district,-1) =  :5  and\n                    sm.merchant_number(+)  = mf.merchant_number and\n                    sm.batch_date(+) between  :6  and  :7  and\n                    o.org_group = mf.merchant_number\n            group by o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchDDFDepositDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchDDFDepositDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:324^11*/
          processSummaryData(it.getResultSet());
          it.close();
          
          /*@lineinfo:generated-code*//*@lineinfo:328^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                        INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      mf.merchant_number                        as hierarchy_node,
//                      mf.dba_name                               as org_name,
//                      :District                                 as district,
//                      count( adj.load_filename )                as item_count,
//                      sum( adj.adjustment_amount *
//                           decode(adj.debit_credit_ind,
//                                  'C',1,-1) )                   as item_amount
//              from    group_merchant                gm,
//                      group_rep_merchant            grm,
//                      daily_detail_file_adjustment  adj,
//                      mif                           mf,
//                      organization                  o
//              where   gm.org_num          = :orgId and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      nvl(mf.district,-1) = :District and
//                      adj.merchant_account_number  = mf.merchant_number and
//                      adj.batch_date between :beginDate and :endDate and
//                      o.org_group = mf.merchant_number
//              group by o.org_num, mf.merchant_number, mf.dba_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                      INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    mf.merchant_number                        as hierarchy_node,\n                    mf.dba_name                               as org_name,\n                     :1                                  as district,\n                    count( adj.load_filename )                as item_count,\n                    sum( adj.adjustment_amount *\n                         decode(adj.debit_credit_ind,\n                                'C',1,-1) )                   as item_amount\n            from    group_merchant                gm,\n                    group_rep_merchant            grm,\n                    daily_detail_file_adjustment  adj,\n                    mif                           mf,\n                    organization                  o\n            where   gm.org_num          =  :2  and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    nvl(mf.district,-1) =  :5  and\n                    adj.merchant_account_number  = mf.merchant_number and\n                    adj.batch_date between  :6  and  :7  and\n                    o.org_group = mf.merchant_number\n            group by o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchDDFDepositDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchDDFDepositDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:356^11*/
          processSummaryData(it.getResultSet(),true);
          it.close();
        }
      }
      else // just standard child report, no districts
      {
        /*@lineinfo:generated-code*//*@lineinfo:363^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                       INDEX (gm pkgroup_merchant) 
//                       INDEX (sm pk_ddf_summary) 
//                    */
//                    o.org_num                                 as org_num,
//                    o.org_group                               as hierarchy_node,
//                    o.org_name                                as org_name,
//                    0                                         as district,
//                    sum( nvl(sm.batch_count,0) )              as item_count,
//                    sum( nvl(sm.bank_amount,0) )              as item_amount
//            from    parent_org                po,
//                    organization              o,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    daily_detail_file_summary sm
//            where   po.parent_org_num   = :orgId and
//                    o.org_num           = po.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    gm.org_num(+)       = o.org_num and
//                    sm.merchant_number(+)  = gm.merchant_number and
//                    sm.batch_date(+) between :beginDate and :endDate
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                     INDEX (gm pkgroup_merchant) \n                     INDEX (sm pk_ddf_summary) \n                  */\n                  o.org_num                                 as org_num,\n                  o.org_group                               as hierarchy_node,\n                  o.org_name                                as org_name,\n                  0                                         as district,\n                  sum( nvl(sm.batch_count,0) )              as item_count,\n                  sum( nvl(sm.bank_amount,0) )              as item_amount\n          from    parent_org                po,\n                  organization              o,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  daily_detail_file_summary sm\n          where   po.parent_org_num   =  :1  and\n                  o.org_num           = po.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  gm.org_num(+)       = o.org_num and\n                  sm.merchant_number(+)  = gm.merchant_number and\n                  sm.batch_date(+) between  :4  and  :5 \n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MerchDDFDepositDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.MerchDDFDepositDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:389^9*/
        
        processSummaryData(it.getResultSet());
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:394^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                       INDEX (gm pkgroup_merchant) 
//                    */
//                    o.org_num                                 as org_num,
//                    o.org_group                               as hierarchy_node,
//                    o.org_name                                as org_name,
//                    0                                         as district,
//                    count( adj.load_filename )                as item_count,
//                    sum( adj.adjustment_amount *
//                         decode(adj.debit_credit_ind,
//                                'C',1,-1) )                   as item_amount
//            from    parent_org                          po,
//                    organization                        o,
//                    group_merchant                      gm,
//                    group_rep_merchant                  grm,
//                    daily_detail_file_adjustment        adj
//            where   po.parent_org_num   = :orgId and
//                    o.org_num           = po.org_num and
//                    gm.org_num          = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    adj.merchant_account_number  = gm.merchant_number and
//                    adj.batch_date between :beginDate and :endDate
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                     INDEX (gm pkgroup_merchant) \n                  */\n                  o.org_num                                 as org_num,\n                  o.org_group                               as hierarchy_node,\n                  o.org_name                                as org_name,\n                  0                                         as district,\n                  count( adj.load_filename )                as item_count,\n                  sum( adj.adjustment_amount *\n                       decode(adj.debit_credit_ind,\n                              'C',1,-1) )                   as item_amount\n          from    parent_org                          po,\n                  organization                        o,\n                  group_merchant                      gm,\n                  group_rep_merchant                  grm,\n                  daily_detail_file_adjustment        adj\n          where   po.parent_org_num   =  :1  and\n                  o.org_num           = po.org_num and\n                  gm.org_num          = o.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  adj.merchant_account_number  = gm.merchant_number and\n                  adj.batch_date between  :4  and  :5 \n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.MerchDDFDepositDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.MerchDDFDepositDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:421^9*/
        processSummaryData(it.getResultSet(),true);
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
}/*@lineinfo:generated-code*/