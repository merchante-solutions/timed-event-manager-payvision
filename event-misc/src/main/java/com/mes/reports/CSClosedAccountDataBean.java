/*@lineinfo:filename=CSClosedAccountDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CSClosedAccountDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 7/17/03 1:34p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class CSClosedAccountDataBean extends CSNewAccountDataBean
{
  public class ClosedRowDataDetail extends RowDataDetail
  {
    public Date         LastDepositDate;
    public double       LastDepositAmount;
    public String       Status;
    public Date         StatusDate;
    public String       OwnerName;
    public double       ProjLostSales;
    public String       RepNumber;
    
    public ClosedRowDataDetail( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
    
      LastDepositAmount = resultSet.getDouble("last_amount");
      LastDepositDate   = resultSet.getDate("last_deposit");
      Status            = processString( resultSet.getString("merchant_status") );
      StatusDate        = resultSet.getDate("status_date");
      OwnerName         = processString( resultSet.getString("owner_name") );
      ProjLostSales     = resultSet.getDouble("proj_lost_sales");
      RepNumber         = processString( resultSet.getString("mif_rep_code") );
    }                          
  }    

  public CSClosedAccountDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    if ( ReportType == RT_SUMMARY )
    {
      line.setLength(0);
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"# Closed\",");
    }
    else // assume RT_DETAILS
    {
      super.encodeHeaderCSV(line);
      line.append(",");
      line.append("\"Status\",");
      line.append("\"Status Date\",");
      line.append("\"Last Dep Date\",");
      line.append("\"Last Dep Amt\",");
      line.append("\"Owner Name\",");
      line.append("\"Rep Number\",");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    if ( ReportType == RT_SUMMARY )
    {
      // super is all we need
      super.encodeLineCSV(obj,line);
    }
    else // assume RT_DETAILS
    {
      super.encodeLineCSV(obj,line);
      
      ClosedRowDataDetail        record    = (ClosedRowDataDetail)obj;
    
      line.append( ",\"" );
      line.append( record.Status );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate(record.StatusDate,"MM/dd/yyyy") );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate(record.LastDepositDate,"MM/dd/yyyy") );
      line.append( "," );
      line.append( record.LastDepositAmount );
      line.append( ",\"" );
      line.append( record.OwnerName );
      line.append( "\",\"" );
      line.append( record.RepNumber );
      line.append( "\"" );
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_cs_closed_accounts_");
    
    if ( ReportType == RT_SUMMARY )
    {
      filename.append("_summary_");
    }
    else    // RT_DETAILS
    {
      filename.append("_details_");
    }
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate, int reportType )
  {
    ResultSetIterator             it                = null;
    long                          lastNode          = 0L;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if( reportType == RT_SUMMARY )
      {
        /*@lineinfo:generated-code*//*@lineinfo:170^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ INDEX (sm pk_monthly_extract_summary) 
//                      INDEX (gm pkgroup_merchant) 
//                      INDEX (o xpkorganization) */        
//                    o.org_num                         as org_num,
//                    o.org_group                       as hierarchy_node,
//                    o.org_name                        as org_name,
//                    count( mf.merchant_number )       as closed_count
//            from    parent_org                po,
//                    organization              o,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    mif                       mf
//            where   po.parent_org_num     = :orgId and
//                    o.org_num             = po.org_num and
//                    gm.org_num            = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    mf.merchant_number = gm.merchant_number and
//                    mf.dmacctst in ('D','C') and
//                    mf.DATE_STAT_CHGD_TO_DCB between :beginDate and :endDate
//            group by o.org_name, o.org_num, o.org_group
//            order by o.org_name        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ INDEX (sm pk_monthly_extract_summary) \n                    INDEX (gm pkgroup_merchant) \n                    INDEX (o xpkorganization) */        \n                  o.org_num                         as org_num,\n                  o.org_group                       as hierarchy_node,\n                  o.org_name                        as org_name,\n                  count( mf.merchant_number )       as closed_count\n          from    parent_org                po,\n                  organization              o,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  mif                       mf\n          where   po.parent_org_num     =  :1  and\n                  o.org_num             = po.org_num and\n                  gm.org_num            = o.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  mf.merchant_number = gm.merchant_number and\n                  mf.dmacctst in ('D','C') and\n                  mf.DATE_STAT_CHGD_TO_DCB between  :4  and  :5 \n          group by o.org_name, o.org_num, o.org_group\n          order by o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CSClosedAccountDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.CSClosedAccountDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:195^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          ReportRows.addElement( new RowDataSummary( resultSet.getLong("org_num"),
                                                     resultSet.getLong("hierarchy_node"),
                                                     resultSet.getString("org_name"),
                                                     resultSet.getInt("closed_count") ) );
        }
        it.close();   // this will also close the resultSet
      }
      else // RT_DETAILS
      {
        /*@lineinfo:generated-code*//*@lineinfo:209^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ INDEX (sm pk_monthly_extract_summary)  
//                       INDEX (gm pkgroup_merchant) */
//                    po.org_num                  as parent_org,
//                    mf.association_node         as assoc_number,
//                    mf.merchant_number          as merchant_number,
//                    to_date(decode(mf.date_opened,
//                                   '000000','010100',
//                                   mf.date_opened),'mmddrr') as date_opened,                  
//                    mf.dba_name                       as dba_name,
//                    mf.ADDR1_LINE_1                   as address,
//                    mf.ADDR1_LINE_2                   as address2,
//                    mf.DMCITY                         as city,
//                    mf.DMSTATE                        as state,
//                    substr(mf.DMZIP,1,5)              as zip,
//                    mf.DDA_NUM                        as dda_number,
//                    mf.sic_code                       as sic_code,
//                    (
//                      substr(mf.PHONE_1,1,3) || '-' ||
//                      substr(mf.phone_1,3,3) || '-' ||
//                      substr(mf.phone_1,6,4)                   
//                    )                                 as phone_number,
//                    (
//                      decode(mf.DVSACCPT,'Y','VS ','') ||
//                      decode(mf.DV$ACCPT,'Y','V$ ','') ||
//                      decode(mf.DMCACCPT,'Y','MC ','') ||
//                      decode(mf.DM$ACCPT,'Y','M$ ','') ||                                        
//                      decode(mf.DAMACCPT,'Y','AM ','') ||
//                      decode(mf.DDCACCPT,'Y','DC ','') || 
//                      decode(mf.DDSACCPT,'Y','DS ','')
//                    )                                 as accept_list,
//                    (mf.VISA_DISC_RATE * 0.001)       as visa_disc_rate,
//                    (mf.VISA_PER_ITEM * 0.001)        as visa_per_item,
//                    (mf.MASTCD_DISC_RATE * 0.001)     as mc_disc_rate,
//                    (mf.mastcd_per_item * 0.001)      as mc_per_item,
//                    mf.dmacctst                       as merchant_status,
//                    mf.date_stat_chgd_to_dcb          as status_date,
//                    mf.LAST_DEPOSIT_DATE              as last_deposit,
//                    mf.LAST_DEPOSIT_AMOUNT            as last_amount,
//                    mf.REP_CODE                       as mif_rep_code,
//                    null                              as owner_name,
//                    proj_sm.proj_lost_sales           as proj_lost_sales
//            from    monthly_extract_summary   sm,
//                    mif                       mf,
//                    organization              o,
//                    parent_org                po,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    ( select  sm.merchant_number          as merchant_number,
//                              (nvl(
//                              round( decode( sum(nvl(sm.vmc_sales_amount,0)),
//                                             0, 0,
//                                             sum(nvl(sm.vmc_sales_amount,0))/
//                                             sum( decode(nvl(sm.vmc_sales_amount,0),0,0,1) ) )
//                                             , 2 )
//                                   , 0 ) *
//                              (:endDate-:beginDate)/30)   as proj_lost_sales
//                      from    group_merchant            gm,
//                              mif                       mf,
//                              monthly_extract_summary   sm
//                      where   gm.org_num = :orgId and
//                              mf.merchant_number = gm.merchant_number and
//                              not mf.date_stat_chgd_to_dcb is null and
//                              sm.merchant_number = mf.merchant_number and
//                              sm.active_date between (mf.date_stat_chgd_to_dcb-365) and mf.date_stat_chgd_to_dcb
//                      group by sm.merchant_number
//                    )                         proj_sm
//            where   po.parent_org_num       = :orgId and
//                    o.org_num               = po.org_num and
//                    gm.org_num              = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    mf.merchant_number = gm.merchant_number and
//                    mf.dmacctst in ('D','C') and        
//                    mf.DATE_STAT_CHGD_TO_DCB between :beginDate and :endDate and
//                    sm.merchant_number(+) = mf.merchant_number and
//                    sm.active_date(+) = trunc((trunc(mf.DATE_STAT_CHGD_TO_DCB,'month')-1),'month') and
//                    proj_sm.merchant_number = mf.merchant_number
//            order by proj_lost_sales desc, mf.dmagent, mf.dba_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ INDEX (sm pk_monthly_extract_summary)  \n                     INDEX (gm pkgroup_merchant) */\n                  po.org_num                  as parent_org,\n                  mf.association_node         as assoc_number,\n                  mf.merchant_number          as merchant_number,\n                  to_date(decode(mf.date_opened,\n                                 '000000','010100',\n                                 mf.date_opened),'mmddrr') as date_opened,                  \n                  mf.dba_name                       as dba_name,\n                  mf.ADDR1_LINE_1                   as address,\n                  mf.ADDR1_LINE_2                   as address2,\n                  mf.DMCITY                         as city,\n                  mf.DMSTATE                        as state,\n                  substr(mf.DMZIP,1,5)              as zip,\n                  mf.DDA_NUM                        as dda_number,\n                  mf.sic_code                       as sic_code,\n                  (\n                    substr(mf.PHONE_1,1,3) || '-' ||\n                    substr(mf.phone_1,3,3) || '-' ||\n                    substr(mf.phone_1,6,4)                   \n                  )                                 as phone_number,\n                  (\n                    decode(mf.DVSACCPT,'Y','VS ','') ||\n                    decode(mf.DV$ACCPT,'Y','V$ ','') ||\n                    decode(mf.DMCACCPT,'Y','MC ','') ||\n                    decode(mf.DM$ACCPT,'Y','M$ ','') ||                                        \n                    decode(mf.DAMACCPT,'Y','AM ','') ||\n                    decode(mf.DDCACCPT,'Y','DC ','') || \n                    decode(mf.DDSACCPT,'Y','DS ','')\n                  )                                 as accept_list,\n                  (mf.VISA_DISC_RATE * 0.001)       as visa_disc_rate,\n                  (mf.VISA_PER_ITEM * 0.001)        as visa_per_item,\n                  (mf.MASTCD_DISC_RATE * 0.001)     as mc_disc_rate,\n                  (mf.mastcd_per_item * 0.001)      as mc_per_item,\n                  mf.dmacctst                       as merchant_status,\n                  mf.date_stat_chgd_to_dcb          as status_date,\n                  mf.LAST_DEPOSIT_DATE              as last_deposit,\n                  mf.LAST_DEPOSIT_AMOUNT            as last_amount,\n                  mf.REP_CODE                       as mif_rep_code,\n                  null                              as owner_name,\n                  proj_sm.proj_lost_sales           as proj_lost_sales\n          from    monthly_extract_summary   sm,\n                  mif                       mf,\n                  organization              o,\n                  parent_org                po,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  ( select  sm.merchant_number          as merchant_number,\n                            (nvl(\n                            round( decode( sum(nvl(sm.vmc_sales_amount,0)),\n                                           0, 0,\n                                           sum(nvl(sm.vmc_sales_amount,0))/\n                                           sum( decode(nvl(sm.vmc_sales_amount,0),0,0,1) ) )\n                                           , 2 )\n                                 , 0 ) *\n                            ( :1 - :2 )/30)   as proj_lost_sales\n                    from    group_merchant            gm,\n                            mif                       mf,\n                            monthly_extract_summary   sm\n                    where   gm.org_num =  :3  and\n                            mf.merchant_number = gm.merchant_number and\n                            not mf.date_stat_chgd_to_dcb is null and\n                            sm.merchant_number = mf.merchant_number and\n                            sm.active_date between (mf.date_stat_chgd_to_dcb-365) and mf.date_stat_chgd_to_dcb\n                    group by sm.merchant_number\n                  )                         proj_sm\n          where   po.parent_org_num       =  :4  and\n                  o.org_num               = po.org_num and\n                  gm.org_num              = o.org_num and\n                  grm.user_id(+) =  :5  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :6  = -1 ) and        \n                  mf.merchant_number = gm.merchant_number and\n                  mf.dmacctst in ('D','C') and        \n                  mf.DATE_STAT_CHGD_TO_DCB between  :7  and  :8  and\n                  sm.merchant_number(+) = mf.merchant_number and\n                  sm.active_date(+) = trunc((trunc(mf.DATE_STAT_CHGD_TO_DCB,'month')-1),'month') and\n                  proj_sm.merchant_number = mf.merchant_number\n          order by proj_lost_sales desc, mf.dmagent, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.CSClosedAccountDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,endDate);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setLong(3,orgId);
   __sJT_st.setLong(4,orgId);
   __sJT_st.setLong(5,AppFilterUserId);
   __sJT_st.setLong(6,AppFilterUserId);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.CSClosedAccountDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:290^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          ReportRows.addElement( new ClosedRowDataDetail( resultSet ) );
        }
        it.close();   // this will also close the resultSet
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
}/*@lineinfo:generated-code*/