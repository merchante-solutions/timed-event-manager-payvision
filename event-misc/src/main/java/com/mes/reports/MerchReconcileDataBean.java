/*@lineinfo:filename=MerchReconcileDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchReconcileDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 11/04/03 12:49p $
  Version            : $Revision: 12 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class MerchReconcileDataBean extends OrgSummaryDataBean
{
  public static final int[]         NegIndexes = new int[] 
  {
    MesOrgSummaryEntry.TRAN_INDEX,
    MesOrgSummaryEntry.ADJ_INDEX,
  };

  public static final int[]         PosIndexes = new int[] 
  {
    MesOrgSummaryEntry.ACH_INDEX,
  };
  
  public class DetailRow extends MesOrgSummaryEntry implements Comparable
  {
    public Date     ActivityDate        = null;
    
    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      ActivityDate      = resultSet.getDate("activity_date");
    }

    public int compareTo( Object obj )
    {
      DetailRow         compareRow    = (DetailRow)obj;
      int               retVal        = 0;
      
      // sort by date, node, district
      if ( ( retVal = ActivityDate.compareTo(compareRow.ActivityDate) ) == 0 )
      {
        if ( ( retVal = (int)(HierarchyNode - compareRow.HierarchyNode) ) == 0 )
        {
          retVal = (District - compareRow.District);
        }
      }
      return( retVal );
    }
  }
  
  public MerchReconcileDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    if ( ReportType == RT_DETAILS )
    {
      line.append("\"Activity Date\",");
    }      
    line.append("\"Batch Cnt\",");
    line.append("\"Batch Amt\",");
    line.append("\"Adj Cnt\",");
    line.append("\"Adj Amt\",");
    line.append("\"Deposit Cnt\",");
    line.append("\"Deposit Amt\",");
    line.append("\"Balance\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    MesOrgSummaryEntry  record    = (MesOrgSummaryEntry)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
  
    line.append( encodeHierarchyNode(record.getHierarchyNode()) );
    line.append( ",\"" );
    line.append( record.getOrgName() );
    line.append( "\"," );
    if ( ReportType == RT_DETAILS )
    {
      DetailRow  details    = (DetailRow)obj;
      
      line.append( DateTimeFormatter.getFormattedDate( details.ActivityDate, "MM/dd/yyyy" ) );
      line.append( "," );
    }
    line.append( record.getCount( MesOrgSummaryEntry.TRAN_INDEX ) );
    line.append( "," );
    line.append( record.getAmount( MesOrgSummaryEntry.TRAN_INDEX ) );
    line.append( "," );
    line.append( record.getCount( MesOrgSummaryEntry.ADJ_INDEX ) );
    line.append( "," );
    line.append( record.getAmount( MesOrgSummaryEntry.ADJ_INDEX ) );
    line.append( "," );
    line.append( record.getCount( MesOrgSummaryEntry.ACH_INDEX ) );
    line.append( "," );
    line.append( record.getAmount( MesOrgSummaryEntry.ACH_INDEX ) );
    line.append( "," );
    line.append( record.getBalance( PosIndexes,NegIndexes ) );
  }
  
  protected void addDetailResults( ResultSet resultSet )
    throws java.sql.SQLException
  {
    DetailRow       details     = null;
    
    details = findDetailsRow( resultSet.getLong("hierarchy_node"), 
                              resultSet.getDate("activity_date"),
                              resultSet.getInt("district") );
    
    if ( details == null )
    {
      ReportRows.addElement( new DetailRow( resultSet ) );
    }
    else
    {
      details.addData( resultSet );
    }
  }
  
  private DetailRow findDetailsRow( long node, Date activityDate )
  {
    return( findDetailsRow(node,activityDate,ReportSQLJBean.DISTRICT_UNASSIGNED) );
  }
  
  private DetailRow findDetailsRow( long node, Date activityDate, int district )
  {
    DetailRow          retVal    = null;
    DetailRow          temp      = null;
    
    for ( int i = 0; i < ReportRows.size(); ++i )
    {
      temp = (DetailRow)ReportRows.elementAt(i);
      if( temp.getHierarchyNode() == node &&
          temp.getDistrict() == district &&
          temp.ActivityDate.equals(activityDate) )
      {
        retVal = temp;
        break;
      }          
    }
    return( retVal );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    
    if ( ReportType == RT_SUMMARY )
    {
      filename.append("_reconcile_summary_");
    }
    else  // RT_DETAILS
    {
      filename.append("_reconcile_details_");
    }      
    
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"MMddyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"MMddyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      for( int i = 0; i < 3; ++i )
      {
        if ( i == 0 )   // transactions
        {
          /*@lineinfo:generated-code*//*@lineinfo:239^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                         INDEX (gm pkgroup_merchant) 
//                         INDEX (sm pk_ddf_summary) 
//                      */
//                      o.org_group                     as hierarchy_node,
//                      o.org_name                      as org_name,
//                      mf.district                     as district,
//                      :orgId                          as org_num,
//                      sm.batch_date                   as activity_date,
//                      sum(sm.batch_count)             as item_count,
//                      sum(sm.bank_amount +
//                              decode( substr(nvl(mf.amex_plan,'NN'),1,1), 
//                                      'D',( nvl(sm.amex_sales_amount,0)-
//                                            nvl(sm.amex_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.discover_plan,'NN'),1,1), 
//                                      'D',( nvl(sm.disc_sales_amount,0)-
//                                            nvl(sm.disc_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.diners_plan,'NN'),1,1), 
//                                      'D',( nvl(sm.diners_sales_amount,0)-
//                                            nvl(sm.diners_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.jcb_plan,'NN'),1,1), 
//                                      'D',( nvl(sm.jcb_sales_amount,0)-
//                                            nvl(sm.jcb_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.private_label_1_plan,'NN'),1,1), 
//                                      'D',( nvl(sm.other_sales_amount,0)-
//                                            nvl(sm.other_credits_amount,0)),0 )
//                              )                       as item_amount
//              from    group_merchant            gm,
//                      mif                       mf,
//                      daily_detail_file_summary sm,
//                      organization              o
//              where   gm.org_num          = :orgId and
//                      mf.merchant_number  = gm.merchant_number and 
//                      sm.merchant_number = mf.merchant_number and
//                      sm.batch_date between :beginDate and :endDate and
//                      o.org_num = gm.org_num
//              group by o.org_group,  mf.district, o.org_name, sm.batch_date                
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                       INDEX (gm pkgroup_merchant) \n                       INDEX (sm pk_ddf_summary) \n                    */\n                    o.org_group                     as hierarchy_node,\n                    o.org_name                      as org_name,\n                    mf.district                     as district,\n                     :1                           as org_num,\n                    sm.batch_date                   as activity_date,\n                    sum(sm.batch_count)             as item_count,\n                    sum(sm.bank_amount +\n                            decode( substr(nvl(mf.amex_plan,'NN'),1,1), \n                                    'D',( nvl(sm.amex_sales_amount,0)-\n                                          nvl(sm.amex_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.discover_plan,'NN'),1,1), \n                                    'D',( nvl(sm.disc_sales_amount,0)-\n                                          nvl(sm.disc_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.diners_plan,'NN'),1,1), \n                                    'D',( nvl(sm.diners_sales_amount,0)-\n                                          nvl(sm.diners_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.jcb_plan,'NN'),1,1), \n                                    'D',( nvl(sm.jcb_sales_amount,0)-\n                                          nvl(sm.jcb_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.private_label_1_plan,'NN'),1,1), \n                                    'D',( nvl(sm.other_sales_amount,0)-\n                                          nvl(sm.other_credits_amount,0)),0 )\n                            )                       as item_amount\n            from    group_merchant            gm,\n                    mif                       mf,\n                    daily_detail_file_summary sm,\n                    organization              o\n            where   gm.org_num          =  :2  and\n                    mf.merchant_number  = gm.merchant_number and \n                    sm.merchant_number = mf.merchant_number and\n                    sm.batch_date between  :3  and  :4  and\n                    o.org_num = gm.org_num\n            group by o.org_group,  mf.district, o.org_name, sm.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:278^11*/
        } 
        else if ( i == 1 )    // adjustments
        {
          /*@lineinfo:generated-code*//*@lineinfo:282^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                         INDEX (gm pkgroup_merchant) 
//                      */
//                      o.org_group                             as hierarchy_node,
//                      o.org_name                              as org_name,
//                      mf.district                             as district,
//                      :orgId                                  as org_num,
//                      adj.batch_date                          as activity_date,
//                      count( adj.adjustment_amount )          as adj_count,
//                      sum( adj.adjustment_amount *
//                           decode(adj.debit_credit_ind,
//                                  'D',-1,1) )                 as adj_amount
//              from    group_merchant                gm,
//                      mif                           mf,
//                      daily_detail_file_adjustment  adj,
//                      organization                  o
//              where   gm.org_num          = :orgId and
//                      mf.merchant_number  = gm.merchant_number and 
//                      adj.merchant_account_number = mf.merchant_number and
//                      adj.batch_date between :beginDate and :endDate and
//                      o.org_num = gm.org_num 
//              group by o.org_group,  mf.district, o.org_name, adj.batch_date                
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                       INDEX (gm pkgroup_merchant) \n                    */\n                    o.org_group                             as hierarchy_node,\n                    o.org_name                              as org_name,\n                    mf.district                             as district,\n                     :1                                   as org_num,\n                    adj.batch_date                          as activity_date,\n                    count( adj.adjustment_amount )          as adj_count,\n                    sum( adj.adjustment_amount *\n                         decode(adj.debit_credit_ind,\n                                'D',-1,1) )                 as adj_amount\n            from    group_merchant                gm,\n                    mif                           mf,\n                    daily_detail_file_adjustment  adj,\n                    organization                  o\n            where   gm.org_num          =  :2  and\n                    mf.merchant_number  = gm.merchant_number and \n                    adj.merchant_account_number = mf.merchant_number and\n                    adj.batch_date between  :3  and  :4  and\n                    o.org_num = gm.org_num \n            group by o.org_group,  mf.district, o.org_name, adj.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:306^11*/
        }
        else if ( i == 2 )    // deposits
        {
          /*@lineinfo:generated-code*//*@lineinfo:310^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                         INDEX (gm pkgroup_merchant) 
//                         INDEX (sm pk_ach_summary) 
//                      */
//                      o.org_group                               as hierarchy_node,
//                      o.org_name                                as org_name,
//                      mf.district                               as district,
//                      :orgId                                    as org_num,
//                      sm.post_date                              as activity_date,
//                      sum(sm.debit_count + sm.credit_count)     as ach_count,
//                      sum(sm.credit_amount - sm.debit_amount)   as ach_amount
//              from    group_merchant            gm,
//                      mif                       mf,
//                      ach_summary               sm,
//                      organization              o
//              where   gm.org_num          = :orgId and
//                      mf.merchant_number  = gm.merchant_number and 
//                      sm.merchant_number = mf.merchant_number and
//                      sm.post_date between :beginDate and :endDate and
//                      ( mf.bank_number = :mesConstants.BANK_ID_MES or 
//                        substr(sm.load_filename,1,3) <> 'fee' ) and
//                      o.org_num = gm.org_num
//              group by o.org_group,  mf.district, o.org_name, sm.post_date                
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                       INDEX (gm pkgroup_merchant) \n                       INDEX (sm pk_ach_summary) \n                    */\n                    o.org_group                               as hierarchy_node,\n                    o.org_name                                as org_name,\n                    mf.district                               as district,\n                     :1                                     as org_num,\n                    sm.post_date                              as activity_date,\n                    sum(sm.debit_count + sm.credit_count)     as ach_count,\n                    sum(sm.credit_amount - sm.debit_amount)   as ach_amount\n            from    group_merchant            gm,\n                    mif                       mf,\n                    ach_summary               sm,\n                    organization              o\n            where   gm.org_num          =  :2  and\n                    mf.merchant_number  = gm.merchant_number and \n                    sm.merchant_number = mf.merchant_number and\n                    sm.post_date between  :3  and  :4  and\n                    ( mf.bank_number =  :5  or \n                      substr(sm.load_filename,1,3) <> 'fee' ) and\n                    o.org_num = gm.org_num\n            group by o.org_group,  mf.district, o.org_name, sm.post_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   __sJT_st.setInt(5,mesConstants.BANK_ID_MES);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:335^11*/
        }
        else
        {
          continue;   // skip
        }
        resultSet = it.getResultSet();
  
        while( resultSet.next() )
        {
          addDetailResults( resultSet );
        }
        resultSet.close();
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    
    try
    {
      if ( hasAssocDistricts() )
      // returns false unless the current node is an
      // association and it has districts under it.
      {
        if ( District == DISTRICT_NONE )
        {
          for( int i = 0; i < 3; ++i )
          {
            if (i == 0)         // transactions
            {
              /*@lineinfo:generated-code*//*@lineinfo:378^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                           INDEX (ddf_sm pk_ddf_summary) 
//                         */
//                          o.org_num                                         as org_num,
//                          o.org_group                                       as hierarchy_node,
//                          nvl(mf.district,:DISTRICT_UNASSIGNED)             as district,
//                          nvl(ad.district_desc,decode( mf.district,
//                                      null,'Unassigned',
//                                     ('District ' || to_char(mf.district, '0009'))))         as org_name,
//                          sum(ddf_sm.batch_count)         as item_count,
//                          sum(ddf_sm.bank_amount +
//                              decode( substr(nvl(mf.amex_plan,'NN'),1,1), 
//                                      'D',( nvl(ddf_sm.amex_sales_amount,0)-
//                                            nvl(ddf_sm.amex_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.discover_plan,'NN'),1,1), 
//                                      'D',( nvl(ddf_sm.disc_sales_amount,0)-
//                                            nvl(ddf_sm.disc_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.diners_plan,'NN'),1,1), 
//                                      'D',( nvl(ddf_sm.diners_sales_amount,0)-
//                                            nvl(ddf_sm.diners_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.jcb_plan,'NN'),1,1), 
//                                      'D',( nvl(ddf_sm.jcb_sales_amount,0)-
//                                            nvl(ddf_sm.jcb_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.private_label_1_plan,'NN'),1,1), 
//                                      'D',( nvl(ddf_sm.other_sales_amount,0)-
//                                            nvl(ddf_sm.other_credits_amount,0)),0 )
//                              )                           as item_amount
//                  from    organization                  o,
//                          group_merchant                gm,
//                          group_rep_merchant            grm,
//                          mif                           mf,
//                          assoc_districts               ad,
//                          daily_detail_file_summary     ddf_sm
//                  where   o.org_num           = :orgId and
//                          gm.org_num          = o.org_num and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                          ad.district(+)      = mf.district and
//                          ddf_sm.merchant_number(+)  = gm.merchant_number and
//                          ddf_sm.batch_date(+) between :beginDate and :endDate
//                  group by  o.org_num, o.org_group, 
//                            mf.district, ad.district_desc
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                         INDEX (ddf_sm pk_ddf_summary) \n                       */\n                        o.org_num                                         as org_num,\n                        o.org_group                                       as hierarchy_node,\n                        nvl(mf.district, :1 )             as district,\n                        nvl(ad.district_desc,decode( mf.district,\n                                    null,'Unassigned',\n                                   ('District ' || to_char(mf.district, '0009'))))         as org_name,\n                        sum(ddf_sm.batch_count)         as item_count,\n                        sum(ddf_sm.bank_amount +\n                            decode( substr(nvl(mf.amex_plan,'NN'),1,1), \n                                    'D',( nvl(ddf_sm.amex_sales_amount,0)-\n                                          nvl(ddf_sm.amex_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.discover_plan,'NN'),1,1), \n                                    'D',( nvl(ddf_sm.disc_sales_amount,0)-\n                                          nvl(ddf_sm.disc_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.diners_plan,'NN'),1,1), \n                                    'D',( nvl(ddf_sm.diners_sales_amount,0)-\n                                          nvl(ddf_sm.diners_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.jcb_plan,'NN'),1,1), \n                                    'D',( nvl(ddf_sm.jcb_sales_amount,0)-\n                                          nvl(ddf_sm.jcb_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.private_label_1_plan,'NN'),1,1), \n                                    'D',( nvl(ddf_sm.other_sales_amount,0)-\n                                          nvl(ddf_sm.other_credits_amount,0)),0 )\n                            )                           as item_amount\n                from    organization                  o,\n                        group_merchant                gm,\n                        group_rep_merchant            grm,\n                        mif                           mf,\n                        assoc_districts               ad,\n                        daily_detail_file_summary     ddf_sm\n                where   o.org_num           =  :2  and\n                        gm.org_num          = o.org_num and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                        ad.district(+)      = mf.district and\n                        ddf_sm.merchant_number(+)  = gm.merchant_number and\n                        ddf_sm.batch_date(+) between  :5  and  :6 \n                group by  o.org_num, o.org_group, \n                          mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:426^15*/
            }
            else if (i == 1)    // adjustments
            {
              /*@lineinfo:generated-code*//*@lineinfo:430^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                         */
//                          o.org_num                                         as org_num,
//                          o.org_group                                       as hierarchy_node,
//                          nvl(mf.district,:DISTRICT_UNASSIGNED)             as district,
//                          nvl(ad.district_desc,decode( mf.district,
//                                      null,'Unassigned',
//                                     ('District ' || to_char(mf.district, '0009'))))         as org_name,
//                          count( adj.adjustment_amount )                    as adj_count,
//                          sum( adj.adjustment_amount *
//                               decode(adj.debit_credit_ind,
//                                      'D',-1,1) )                           as adj_amount
//                  from    organization                  o,
//                          group_merchant                gm,
//                          group_rep_merchant            grm,
//                          mif                           mf,
//                          assoc_districts               ad,
//                          daily_detail_file_adjustment  adj
//                  where   o.org_num           = :orgId and
//                          gm.org_num          = o.org_num and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                          ad.district(+)      = mf.district and
//                          adj.merchant_account_number(+) = gm.merchant_number and
//                          adj.batch_date(+) between :beginDate and :endDate
//                  group by  o.org_num, o.org_group, 
//                            mf.district, ad.district_desc
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                       */\n                        o.org_num                                         as org_num,\n                        o.org_group                                       as hierarchy_node,\n                        nvl(mf.district, :1 )             as district,\n                        nvl(ad.district_desc,decode( mf.district,\n                                    null,'Unassigned',\n                                   ('District ' || to_char(mf.district, '0009'))))         as org_name,\n                        count( adj.adjustment_amount )                    as adj_count,\n                        sum( adj.adjustment_amount *\n                             decode(adj.debit_credit_ind,\n                                    'D',-1,1) )                           as adj_amount\n                from    organization                  o,\n                        group_merchant                gm,\n                        group_rep_merchant            grm,\n                        mif                           mf,\n                        assoc_districts               ad,\n                        daily_detail_file_adjustment  adj\n                where   o.org_num           =  :2  and\n                        gm.org_num          = o.org_num and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                        ad.district(+)      = mf.district and\n                        adj.merchant_account_number(+) = gm.merchant_number and\n                        adj.batch_date(+) between  :5  and  :6 \n                group by  o.org_num, o.org_group, \n                          mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:463^15*/
            }
            else if (i == 2)    // deposits
            {
              /*@lineinfo:generated-code*//*@lineinfo:467^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                           INDEX (ach_sm pk_ach_summary)
//                         */
//                          o.org_num                                         as org_num,
//                          o.org_group                                       as hierarchy_node,
//                          nvl(mf.district,:DISTRICT_UNASSIGNED)             as district,
//                          nvl(ad.district_desc,decode( mf.district,
//                                      null,'Unassigned',
//                                     ('District ' || to_char(mf.district, '0009'))))         as org_name,
//                          sum(ach_sm.debit_count + ach_sm.credit_count)     as ach_count,
//                          sum(ach_sm.credit_amount - ach_sm.debit_amount)   as ach_amount
//                  from    organization                  o,
//                          group_merchant                gm,
//                          group_rep_merchant            grm,
//                          mif                           mf,
//                          assoc_districts               ad,
//                          ach_summary                   ach_sm
//                  where   o.org_num           = :orgId and
//                          gm.org_num          = o.org_num and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                          ad.district(+)      = mf.district and
//                          ach_sm.merchant_number(+)  = gm.merchant_number and
//                          ach_sm.post_date(+) between :beginDate and :endDate and
//                          ( mf.bank_number = :mesConstants.BANK_ID_MES or 
//                            substr(ach_sm.load_filename,1,3) <> 'fee' )
//                  group by  o.org_num, o.org_group, 
//                            mf.district, ad.district_desc
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                         INDEX (ach_sm pk_ach_summary)\n                       */\n                        o.org_num                                         as org_num,\n                        o.org_group                                       as hierarchy_node,\n                        nvl(mf.district, :1 )             as district,\n                        nvl(ad.district_desc,decode( mf.district,\n                                    null,'Unassigned',\n                                   ('District ' || to_char(mf.district, '0009'))))         as org_name,\n                        sum(ach_sm.debit_count + ach_sm.credit_count)     as ach_count,\n                        sum(ach_sm.credit_amount - ach_sm.debit_amount)   as ach_amount\n                from    organization                  o,\n                        group_merchant                gm,\n                        group_rep_merchant            grm,\n                        mif                           mf,\n                        assoc_districts               ad,\n                        ach_summary                   ach_sm\n                where   o.org_num           =  :2  and\n                        gm.org_num          = o.org_num and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                        ad.district(+)      = mf.district and\n                        ach_sm.merchant_number(+)  = gm.merchant_number and\n                        ach_sm.post_date(+) between  :5  and  :6  and\n                        ( mf.bank_number =  :7  or \n                          substr(ach_sm.load_filename,1,3) <> 'fee' )\n                group by  o.org_num, o.org_group, \n                          mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setInt(7,mesConstants.BANK_ID_MES);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:501^15*/
            }
            else
            {           
              continue;     // skip
            }
            processSummaryData(it.getResultSet(),true);
            it.close();
          }
        }
        else    // a district was specified
        {
          for( int i = 0; i < 3; ++i )
          {
            if (i == 0)         // transactions
            {
              /*@lineinfo:generated-code*//*@lineinfo:517^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                           INDEX (ddf_sm pk_ddf_summary) 
//                         */
//                          o.org_num                                         as org_num,
//                          mf.merchant_number                                as hierarchy_node,
//                          mf.dba_name                                       as org_name,
//                          :District                                         as district,
//                          sum(ddf_sm.batch_count)                           as item_count,
//                          sum(ddf_sm.bank_amount +
//                              decode( substr(nvl(mf.amex_plan,'NN'),1,1), 
//                                      'D',( nvl(ddf_sm.amex_sales_amount,0)-
//                                            nvl(ddf_sm.amex_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.discover_plan,'NN'),1,1), 
//                                      'D',( nvl(ddf_sm.disc_sales_amount,0)-
//                                            nvl(ddf_sm.disc_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.diners_plan,'NN'),1,1), 
//                                      'D',( nvl(ddf_sm.diners_sales_amount,0)-
//                                            nvl(ddf_sm.diners_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.jcb_plan,'NN'),1,1), 
//                                      'D',( nvl(ddf_sm.jcb_sales_amount,0)-
//                                            nvl(ddf_sm.jcb_credits_amount,0)),0 ) +
//                              decode( substr(nvl(mf.private_label_1_plan,'NN'),1,1), 
//                                      'D',( nvl(ddf_sm.other_sales_amount,0)-
//                                            nvl(ddf_sm.other_credits_amount,0)),0 )
//                              )                           as item_amount
//                  from    group_merchant                gm,
//                          group_rep_merchant            grm,
//                          mif                           mf,
//                          organization                  o,
//                          daily_detail_file_summary     ddf_sm
//                  where   gm.org_num          = :orgId and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          nvl(mf.district,-1) = :District and
//                          o.org_group = mf.merchant_number and
//                          ddf_sm.merchant_number(+)  = gm.merchant_number and
//                          ddf_sm.batch_date(+) between :beginDate and :endDate
//                  group by  o.org_num, mf.merchant_number, mf.dba_name
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                         INDEX (ddf_sm pk_ddf_summary) \n                       */\n                        o.org_num                                         as org_num,\n                        mf.merchant_number                                as hierarchy_node,\n                        mf.dba_name                                       as org_name,\n                         :1                                          as district,\n                        sum(ddf_sm.batch_count)                           as item_count,\n                        sum(ddf_sm.bank_amount +\n                            decode( substr(nvl(mf.amex_plan,'NN'),1,1), \n                                    'D',( nvl(ddf_sm.amex_sales_amount,0)-\n                                          nvl(ddf_sm.amex_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.discover_plan,'NN'),1,1), \n                                    'D',( nvl(ddf_sm.disc_sales_amount,0)-\n                                          nvl(ddf_sm.disc_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.diners_plan,'NN'),1,1), \n                                    'D',( nvl(ddf_sm.diners_sales_amount,0)-\n                                          nvl(ddf_sm.diners_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.jcb_plan,'NN'),1,1), \n                                    'D',( nvl(ddf_sm.jcb_sales_amount,0)-\n                                          nvl(ddf_sm.jcb_credits_amount,0)),0 ) +\n                            decode( substr(nvl(mf.private_label_1_plan,'NN'),1,1), \n                                    'D',( nvl(ddf_sm.other_sales_amount,0)-\n                                          nvl(ddf_sm.other_credits_amount,0)),0 )\n                            )                           as item_amount\n                from    group_merchant                gm,\n                        group_rep_merchant            grm,\n                        mif                           mf,\n                        organization                  o,\n                        daily_detail_file_summary     ddf_sm\n                where   gm.org_num          =  :2  and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        nvl(mf.district,-1) =  :5  and\n                        o.org_group = mf.merchant_number and\n                        ddf_sm.merchant_number(+)  = gm.merchant_number and\n                        ddf_sm.batch_date(+) between  :6  and  :7 \n                group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:560^15*/
            }
            else if (i == 1)    // adjustments
            {
              /*@lineinfo:generated-code*//*@lineinfo:564^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                         */
//                          o.org_num                                         as org_num,
//                          mf.merchant_number                                as hierarchy_node,
//                          mf.dba_name                                       as org_name,
//                          :District                                         as district,
//                          count( adj.adjustment_amount )                    as adj_count,
//                          sum( adj.adjustment_amount *
//                               decode(adj.debit_credit_ind,
//                                      'D',-1,1) )                           as adj_amount
//                  from    group_merchant                gm,
//                          group_rep_merchant            grm,
//                          mif                           mf,
//                          organization                  o,
//                          daily_detail_file_adjustment  adj
//                  where   gm.org_num          = :orgId and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          nvl(mf.district,-1) = :District and
//                          o.org_group = mf.merchant_number and
//                          adj.merchant_account_number(+) = gm.merchant_number and
//                          adj.batch_date(+) between :beginDate and :endDate
//                  group by  o.org_num, mf.merchant_number, mf.dba_name
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                       */\n                        o.org_num                                         as org_num,\n                        mf.merchant_number                                as hierarchy_node,\n                        mf.dba_name                                       as org_name,\n                         :1                                          as district,\n                        count( adj.adjustment_amount )                    as adj_count,\n                        sum( adj.adjustment_amount *\n                             decode(adj.debit_credit_ind,\n                                    'D',-1,1) )                           as adj_amount\n                from    group_merchant                gm,\n                        group_rep_merchant            grm,\n                        mif                           mf,\n                        organization                  o,\n                        daily_detail_file_adjustment  adj\n                where   gm.org_num          =  :2  and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        nvl(mf.district,-1) =  :5  and\n                        o.org_group = mf.merchant_number and\n                        adj.merchant_account_number(+) = gm.merchant_number and\n                        adj.batch_date(+) between  :6  and  :7 \n                group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:592^15*/
            }
            else if (i == 2)    // deposits
            {
              /*@lineinfo:generated-code*//*@lineinfo:596^15*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                           INDEX (ach_sm pk_ach_summary)
//                         */
//                          o.org_num                                         as org_num,
//                          mf.merchant_number                                as hierarchy_node,
//                          mf.dba_name                                       as org_name,
//                          :District                                         as district,
//                          sum(ach_sm.debit_count + ach_sm.credit_count)     as ach_count,
//                          sum(ach_sm.credit_amount - ach_sm.debit_amount)   as ach_amount
//                  from    group_merchant                gm,
//                          group_rep_merchant            grm,
//                          mif                           mf,
//                          organization                  o,
//                          ach_summary                   ach_sm
//                  where   gm.org_num          = :orgId and
//                          grm.user_id(+) = :AppFilterUserId and
//                          grm.merchant_number(+) = gm.merchant_number and
//                          ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                          mf.merchant_number  = gm.merchant_number and
//                          nvl(mf.district,-1) = :District and
//                          o.org_group = mf.merchant_number and
//                          ach_sm.merchant_number(+)  = gm.merchant_number and
//                          ach_sm.post_date(+) between :beginDate and :endDate and
//                          ( mf.bank_number = :mesConstants.BANK_ID_MES or 
//                            substr(ach_sm.load_filename,1,3) <> 'fee' )
//                  group by  o.org_num, mf.merchant_number, mf.dba_name
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                         INDEX (ach_sm pk_ach_summary)\n                       */\n                        o.org_num                                         as org_num,\n                        mf.merchant_number                                as hierarchy_node,\n                        mf.dba_name                                       as org_name,\n                         :1                                          as district,\n                        sum(ach_sm.debit_count + ach_sm.credit_count)     as ach_count,\n                        sum(ach_sm.credit_amount - ach_sm.debit_amount)   as ach_amount\n                from    group_merchant                gm,\n                        group_rep_merchant            grm,\n                        mif                           mf,\n                        organization                  o,\n                        ach_summary                   ach_sm\n                where   gm.org_num          =  :2  and\n                        grm.user_id(+) =  :3  and\n                        grm.merchant_number(+) = gm.merchant_number and\n                        ( not grm.user_id is null or  :4  = -1 ) and        \n                        mf.merchant_number  = gm.merchant_number and\n                        nvl(mf.district,-1) =  :5  and\n                        o.org_group = mf.merchant_number and\n                        ach_sm.merchant_number(+)  = gm.merchant_number and\n                        ach_sm.post_date(+) between  :6  and  :7  and\n                        ( mf.bank_number =  :8  or \n                          substr(ach_sm.load_filename,1,3) <> 'fee' )\n                group by  o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setInt(8,mesConstants.BANK_ID_MES);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:625^15*/
            }
            else
            {           
              continue;     // skip
            }
            processSummaryData(it.getResultSet(),true);
            it.close();
          }
        }
      }
      else // just standard child report, no districts
      {
        for( int i = 0; i < 3; ++i )
        {
          if (i == 0)         // transactions
          {
            /*@lineinfo:generated-code*//*@lineinfo:642^13*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                           INDEX (ddf_sm pk_ddf_summary) 
//                        */
//                        o.org_num                       as org_num,
//                        o.org_group                     as hierarchy_node,
//                        o.org_name                      as org_name,
//                        0                               as district,
//                        sum(ddf_sm.batch_count)         as item_count,
//                        sum(ddf_sm.bank_amount +
//                            decode( substr(nvl(mf.amex_plan,'NN'),1,1), 
//                                    'D',( nvl(ddf_sm.amex_sales_amount,0)-
//                                          nvl(ddf_sm.amex_credits_amount,0)),0 ) +
//                            decode( substr(nvl(mf.discover_plan,'NN'),1,1), 
//                                    'D',( nvl(ddf_sm.disc_sales_amount,0)-
//                                          nvl(ddf_sm.disc_credits_amount,0)),0 ) +
//                            decode( substr(nvl(mf.diners_plan,'NN'),1,1), 
//                                    'D',( nvl(ddf_sm.diners_sales_amount,0)-
//                                          nvl(ddf_sm.diners_credits_amount,0)),0 ) +
//                            decode( substr(nvl(mf.jcb_plan,'NN'),1,1), 
//                                    'D',( nvl(ddf_sm.jcb_sales_amount,0)-
//                                          nvl(ddf_sm.jcb_credits_amount,0)),0 ) +
//                            decode( substr(nvl(mf.private_label_1_plan,'NN'),1,1), 
//                                    'D',( nvl(ddf_sm.other_sales_amount,0)-
//                                          nvl(ddf_sm.other_credits_amount,0)),0 )
//                            )                           as item_amount
//                from    parent_org                    po,
//                        organization                  o,
//                        group_merchant                gm,
//                        group_rep_merchant            grm,
//                        daily_detail_file_summary     ddf_sm,
//                        mif                           mf
//                where   po.parent_org_num   = :orgId and
//                        o.org_num           = po.org_num and
//                        gm.org_num(+)       = o.org_num and
//                        grm.user_id(+) = :AppFilterUserId and
//                        grm.merchant_number(+) = gm.merchant_number and
//                        ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                        ddf_sm.merchant_number(+)  = gm.merchant_number and
//                        ddf_sm.batch_date(+) between :beginDate and :endDate and
//                        mf.merchant_number(+) = ddf_sm.merchant_number
//                group by o.org_num, o.org_group, o.org_name
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                         INDEX (ddf_sm pk_ddf_summary) \n                      */\n                      o.org_num                       as org_num,\n                      o.org_group                     as hierarchy_node,\n                      o.org_name                      as org_name,\n                      0                               as district,\n                      sum(ddf_sm.batch_count)         as item_count,\n                      sum(ddf_sm.bank_amount +\n                          decode( substr(nvl(mf.amex_plan,'NN'),1,1), \n                                  'D',( nvl(ddf_sm.amex_sales_amount,0)-\n                                        nvl(ddf_sm.amex_credits_amount,0)),0 ) +\n                          decode( substr(nvl(mf.discover_plan,'NN'),1,1), \n                                  'D',( nvl(ddf_sm.disc_sales_amount,0)-\n                                        nvl(ddf_sm.disc_credits_amount,0)),0 ) +\n                          decode( substr(nvl(mf.diners_plan,'NN'),1,1), \n                                  'D',( nvl(ddf_sm.diners_sales_amount,0)-\n                                        nvl(ddf_sm.diners_credits_amount,0)),0 ) +\n                          decode( substr(nvl(mf.jcb_plan,'NN'),1,1), \n                                  'D',( nvl(ddf_sm.jcb_sales_amount,0)-\n                                        nvl(ddf_sm.jcb_credits_amount,0)),0 ) +\n                          decode( substr(nvl(mf.private_label_1_plan,'NN'),1,1), \n                                  'D',( nvl(ddf_sm.other_sales_amount,0)-\n                                        nvl(ddf_sm.other_credits_amount,0)),0 )\n                          )                           as item_amount\n              from    parent_org                    po,\n                      organization                  o,\n                      group_merchant                gm,\n                      group_rep_merchant            grm,\n                      daily_detail_file_summary     ddf_sm,\n                      mif                           mf\n              where   po.parent_org_num   =  :1  and\n                      o.org_num           = po.org_num and\n                      gm.org_num(+)       = o.org_num and\n                      grm.user_id(+) =  :2  and\n                      grm.merchant_number(+) = gm.merchant_number and\n                      ( not grm.user_id is null or  :3  = -1 ) and        \n                      ddf_sm.merchant_number(+)  = gm.merchant_number and\n                      ddf_sm.batch_date(+) between  :4  and  :5  and\n                      mf.merchant_number(+) = ddf_sm.merchant_number\n              group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:686^13*/
          }
          else if (i == 1)    // adjustments
          {
            /*@lineinfo:generated-code*//*@lineinfo:690^13*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                        */
//                        o.org_num                                         as org_num,
//                        o.org_group                                       as hierarchy_node,
//                        o.org_name                                        as org_name,
//                        0                                                 as district,
//                        count( adj.adjustment_amount )                    as adj_count,
//                        sum( adj.adjustment_amount *
//                             decode(adj.debit_credit_ind,
//                                    'D',-1,1) )                           as adj_amount
//                from    parent_org                    po,
//                        organization                  o,
//                        group_merchant                gm,
//                        group_rep_merchant            grm,
//                        daily_detail_file_adjustment  adj
//                where   po.parent_org_num   = :orgId and
//                        o.org_num           = po.org_num and
//                        gm.org_num(+)       = o.org_num and
//                        grm.user_id(+) = :AppFilterUserId and
//                        grm.merchant_number(+) = gm.merchant_number and
//                        ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                        adj.merchant_account_number(+) = gm.merchant_number and
//                        adj.batch_date(+) between :beginDate and :endDate
//                group by o.org_num, o.org_group, o.org_name
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                      */\n                      o.org_num                                         as org_num,\n                      o.org_group                                       as hierarchy_node,\n                      o.org_name                                        as org_name,\n                      0                                                 as district,\n                      count( adj.adjustment_amount )                    as adj_count,\n                      sum( adj.adjustment_amount *\n                           decode(adj.debit_credit_ind,\n                                  'D',-1,1) )                           as adj_amount\n              from    parent_org                    po,\n                      organization                  o,\n                      group_merchant                gm,\n                      group_rep_merchant            grm,\n                      daily_detail_file_adjustment  adj\n              where   po.parent_org_num   =  :1  and\n                      o.org_num           = po.org_num and\n                      gm.org_num(+)       = o.org_num and\n                      grm.user_id(+) =  :2  and\n                      grm.merchant_number(+) = gm.merchant_number and\n                      ( not grm.user_id is null or  :3  = -1 ) and        \n                      adj.merchant_account_number(+) = gm.merchant_number and\n                      adj.batch_date(+) between  :4  and  :5 \n              group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:717^13*/
          }
          else if (i == 2)    // deposits
          {
            /*@lineinfo:generated-code*//*@lineinfo:721^13*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                           INDEX (gm pkgroup_merchant) 
//                           INDEX (ach_sm pk_ach_summary)
//                        */
//                        o.org_num                                         as org_num,
//                        o.org_group                                       as hierarchy_node,
//                        o.org_name                                        as org_name,
//                        0                                                 as district,
//                        sum(ach_sm.debit_count + ach_sm.credit_count)     as ach_count,
//                        sum(ach_sm.credit_amount - ach_sm.debit_amount)   as ach_amount
//                from    parent_org                    po,
//                        organization                  o,
//                        group_merchant                gm,
//                        group_rep_merchant            grm,
//                        mif                           mf,
//                        ach_summary                   ach_sm
//                where   po.parent_org_num   = :orgId and
//                        o.org_num           = po.org_num and
//                        gm.org_num(+)       = o.org_num and
//                        grm.user_id(+) = :AppFilterUserId and
//                        grm.merchant_number(+) = gm.merchant_number and
//                        ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                        mf.merchant_number  = gm.merchant_number and
//                        ach_sm.merchant_number(+)  = gm.merchant_number and
//                        ach_sm.post_date(+) between :beginDate and :endDate and
//                        ( mf.bank_number = :mesConstants.BANK_ID_MES or 
//                          substr(ach_sm.load_filename,1,3) <> 'fee' )
//                group by o.org_num, o.org_group, o.org_name
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                         INDEX (gm pkgroup_merchant) \n                         INDEX (ach_sm pk_ach_summary)\n                      */\n                      o.org_num                                         as org_num,\n                      o.org_group                                       as hierarchy_node,\n                      o.org_name                                        as org_name,\n                      0                                                 as district,\n                      sum(ach_sm.debit_count + ach_sm.credit_count)     as ach_count,\n                      sum(ach_sm.credit_amount - ach_sm.debit_amount)   as ach_amount\n              from    parent_org                    po,\n                      organization                  o,\n                      group_merchant                gm,\n                      group_rep_merchant            grm,\n                      mif                           mf,\n                      ach_summary                   ach_sm\n              where   po.parent_org_num   =  :1  and\n                      o.org_num           = po.org_num and\n                      gm.org_num(+)       = o.org_num and\n                      grm.user_id(+) =  :2  and\n                      grm.merchant_number(+) = gm.merchant_number and\n                      ( not grm.user_id is null or  :3  = -1 ) and        \n                      mf.merchant_number  = gm.merchant_number and\n                      ach_sm.merchant_number(+)  = gm.merchant_number and\n                      ach_sm.post_date(+) between  :4  and  :5  and\n                      ( mf.bank_number =  :6  or \n                        substr(ach_sm.load_filename,1,3) <> 'fee' )\n              group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.MerchReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setInt(6,mesConstants.BANK_ID_MES);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.MerchReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:751^13*/
          }
          else
          {           
            continue;     // skip
          }
          processSummaryData(it.getResultSet(),true);
          it.close();
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.getResultSet().close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/