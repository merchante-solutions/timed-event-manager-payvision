/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/StatementPage.java $

  Description:
  
    StatementPage
    
    Contains a merchant statement page.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 6/13/01 12:05p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

public class StatementPage implements Serializable
{
  private Vector  lines         = new Vector();
  private int     pageNum       = 0;
  private int     lineCount     = 0;
  
  /*
  ** public void addLine(StatementLine newLine)
  **
  ** Adds a StatementLine to the lines vector.
  */
  public void addLine(StatementLine newLine)
  {
    lines.add(newLine);
    ++lineCount;
    
  }
  public void addLine(String lineData)
  {
    addLine(new StatementLine(lineCount,lineData));
  }

  /*
  ** public StatementLine getLine(int lineNum)
  **
  ** Gets the line corresponding with the given line number.
  **
  ** RETURNS: the line if it is found, else null.
  */
  public StatementLine getLine(int lineNum)
  {
    StatementLine getLine = null;
    if (lineNum < lines.size())
    {
      getLine = (StatementLine)lines.get(lineNum);
    }
    
    return getLine;
  }
  
  /*
  ** public String toString()
  **
  ** Concatenates line data into a String.
  **
  ** RETURNS: the contents of this page as a String.
  */
  public String toString()
  {
    StringBuffer lineData = new StringBuffer();
    for (Iterator i = lines.iterator(); i.hasNext();)
    {
      lineData.append("" + i.next() + "\r\n");
    }
    return lineData.toString();
  }
  
  /*
  ** ACCESSORS
  */
  public int getPageNum()
  {
    return pageNum;
  }
  public void setPageNum(int newPageNum)
  {
    pageNum = newPageNum;
  }
  public int getLineCount()
  {
    return lineCount;
  }
}
