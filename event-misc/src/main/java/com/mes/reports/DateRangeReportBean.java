/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/DateRangeReportBean.java $

  Description:  
  
  Extends ReportDataBean to provide support to child beans for generating 
  reports between two dates.


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 3/11/02 2:28p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.util.Calendar;
import javax.servlet.jsp.JspWriter;

public class DateRangeReportBean extends ReportDataBean
{
  protected BeanDate fromDate       = new BeanDate();
  protected BeanDate toDate         = new BeanDate();
  
  public static final int FROM_MDY  = 0;    // from month-day-year
  public static final int TO_MDY    = 1;    // to month-day-year
  public static final int FROM_MY   = 2;    // from month-year
  public static final int TO_MY     = 3;    // to month-year
  
  private String months[]           = { 
                                        "January",
                                        "February",
                                        "March",
                                        "April",
                                        "May",
                                        "June",
                                        "July",
                                        "August",
                                        "September",
                                        "October",
                                        "November",
                                        "December"
                                      };
  private int currentYear           = Calendar.getInstance().get(Calendar.YEAR);
                                      
  /*
  ** FORM CONTROL OUTPUTTERS
  */
  public void displayMonthPicker(JspWriter out, int dateType)
  {
    boolean isFromDate = (dateType == FROM_MDY || dateType == FROM_MY);
    try
    {
      String controlName  = (isFromDate ? "fromMonth" : "toMonth");
      int selItem         = (isFromDate ? getFromMonth() : getToMonth());
    
      out.println("              <select class=\"formFields\" name=\"" + controlName + "\">");
      for (int i=0; i < 12; ++i)
      {
        out.println("                <option value=\"" + i + "\" " + 
                    (i == selItem ? "selected" : "") + ">" + 
                    months[i] + "</option>");
      }
      out.println("              </select>");
    }
    catch (Exception e)
    {
      addError("displayMonthPicker: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
               "displayMonthPicker: " + e.toString());
    }
  }
  
  public void displayDayPicker(JspWriter out, int dateType)
  {
    if (dateType == FROM_MY || dateType == TO_MY)
    {
      return;
    }
    
    boolean isFromDate = (dateType == FROM_MDY || dateType == FROM_MY);
    try
    {
      String controlName  = (isFromDate ? "fromDay" : "toDay");
      int selItem         = (isFromDate ? getFromDay() : getToDay());
    
      out.println("              <select class=\"formFields\" name=\"" + controlName + "\">");
      for (int i=1; i <= 31; ++i)
      {
        out.println("                <option value=\"" + i + "\" " + 
                    (i == selItem ? "selected" : "") + ">" + i + "</option>");
      }
      out.println("              </select>");
    }
    catch (Exception e)
    {
      addError("displayDayPicker: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
               "displayDayPicker: " + e.toString());
    }
  }
  
  public void displayYearPicker(JspWriter out, int dateType)
  {
    boolean isFromDate = (dateType == FROM_MDY || dateType == FROM_MY);
    try
    {
      String controlName  = (isFromDate ? "fromYear" : "toYear");
      int selItem         = (isFromDate ? getFromYear() : getToYear());
    
      out.println("              <select class=\"formFields\" name=\"" + controlName + "\">");
      for (int i=2000; i <= currentYear; ++i)
      {
        out.println("                <option value=\"" + i + "\" " + 
                    (i == selItem ? "selected" : "") + ">" + i + "</option>");
      }
      out.println("              </select>");
    }
    catch (Exception e)
    {
      addError("displayYearPicker: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
               "displayYearPicker: " + e.toString());
    }
  }
  
  public void displayDateControls(JspWriter out, int dateType)
  {
    boolean isFromDate = (dateType == FROM_MDY || dateType == FROM_MY);
    try
    {
      out.println("          <tr>");
      out.println("            <td height=\"30\" width=\"15%\">");
      out.println("              <strong class=\"headingSub\">" + (isFromDate ? "From" : "To") + "</strong>");
      out.println("            </td>");
      out.println("            <td width=\"40%\">");
      displayMonthPicker(out,dateType);
      displayDayPicker(out,dateType);
      displayYearPicker(out,dateType);
      out.println("            </td>");
      out.println("            <td width=\"45%\">");
      out.println("              &nbsp;");
      out.println("            </td>");
      out.println("          </tr>");
    }
    catch (Exception e)
    {
      addError("displayDateControls: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
               "displayDateControls: " + e.toString());
    }
  }
  
  public void displaySubmitButton(JspWriter out)
  {
    try
    {
      out.println("          <tr>");
      out.println("            <td>");
      out.println("              <input type=\"submit\" name=\"submit\" value=\"Select\">");
      out.println("            </td>");
      out.println("          </tr>");
    }
    catch (Exception e)
    {
      addError("displaySubmitButton: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
               "displaySubmitButton: " + e.toString());
    }
    
  }
  
  public void displayTopCustomControls(JspWriter out) {}
  public void displayBottomCustomControls(JspWriter out) {}
  
  public void displayFromToDateControls(JspWriter out, boolean fullDate)
  {
    try
    {
      out.println("        <table align=\"center\" width=\"98%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
      out.println("          <tr>");
      out.println("            <td height=\"40\" colspan=\"3\">");
      out.println("              <span class=\"headingMain\">Date Range:</span>");
      out.println("            </td>");
      out.println("          </tr>");
      displayTopCustomControls(out);
      displayDateControls(out,(fullDate ? FROM_MDY : FROM_MY));
      displayDateControls(out,(fullDate ? TO_MDY : TO_MY));
      displayBottomCustomControls(out);
      out.println("        </table>");
    }
    catch (Exception e)
    {
      addError("displayFromToDateControls: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
               "displayFromToDateControls: " + e.toString());
    }
  }
  public void displayFromToDateControls(JspWriter out)
  {
    displayFromToDateControls(out,true);
  }
  
  /*
  ** ACCESSORS
  */
  public int  getFromDay  ()           { return fromDate.getDay();   }
  public int  getFromMonth()           { return fromDate.getMonth(); }
  public int  getFromYear ()           { return fromDate.getYear();  }
  public int  getToDay    ()           { return toDate.  getDay();   }
  public int  getToMonth  ()           { return toDate.  getMonth(); }
  public int  getToYear   ()           { return toDate.  getYear();  }
  public void setFromDay  (int newVal) { fromDate.setDay  (newVal);  }
  public void setFromMonth(int newVal) { fromDate.setMonth(newVal);  }
  public void setFromYear (int newVal) { fromDate.setYear (newVal);  }
  public void setToDay    (int newVal) { toDate.  setDay  (newVal);  }
  public void setToMonth  (int newVal) { toDate.  setMonth(newVal);  }
  public void setToYear   (int newVal) { toDate.  setYear (newVal);  }
  
  public void addFromDate(int field, int amount)
  {
    fromDate.add(field,amount);
  }
  public void addToDate(int field, int amount)
  {
    toDate.add(field,amount);
  }
  
  public boolean getDateSet()
  {
    return (toDate.isSet() || fromDate.isSet());
  }
}
