/*@lineinfo:filename=CommissionDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/SalesCommissionDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.Condition;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class CommissionDataBean extends ReportSQLJBean
{
  public static final int   REP_GROUP_ECOMM_TELESALES      = 1;
  public static final int   REP_GROUP_AGENT_BANK           = 2;
  public static final int   REP_GROUP_REFERRAL_TELESALES   = 3;
  
  public static final int   RT_REP_PAYMENT_SUMMARY         = 0;
  public static final int   RT_REP_PAYMENT_DETAILS         = 1;
 
  private Vector    DataRows       = new Vector();
  private Vector    ErrorMessage   = new Vector();
  
  public class PaymentSummaryData
  {
    public  double    CommissionAmount        = 0.0;
    public  int       MerchantCount           = 0;
    public  String    RepId                   = null;
    public  Date      CommissionDate          = null;
    public  double    Income                  = 0.0;
    public  double    Expense                 = 0.0;
    public  double    NetRevenue              = 0.0;
    public  double    PayRate                 = 0.0;
    public  long      HierarchyNode           = 0L;
    public  String    OrgName                 = null;
    public  int       CommissionType          = 0;
    public  double    NewAppCommission        = 0.0;
    public  double    RevenueCommission       = 0.0;
     
    public PaymentSummaryData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CommissionAmount  = resultSet.getDouble("comm_amount");
      RepId             = resultSet.getString("rep_id");
      CommissionDate    = resultSet.getDate("comm_date");      
      MerchantCount     = resultSet.getInt("merchant_count");
      Income            = resultSet.getDouble("income");
      Expense           = resultSet.getDouble("expense");
      NetRevenue        = resultSet.getDouble("net_revenue");
      PayRate           = resultSet.getDouble("pay_rate"); 
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      OrgName           = resultSet.getString("org_name");      
      CommissionType    = resultSet.getInt("comm_type");
      NewAppCommission  = resultSet.getDouble("new_app_comm");
      RevenueCommission = resultSet.getDouble("revenue_comm");
    }
  }

  public class PaymentDetailData
  {
    public  Date     AcctOpenDate          = null;
    public  Date     ActivationDate        = null;
    public  double   CommissionAmount      = 0.0;         
    public  Date     CommissionDate        = null;
    public  String   DbaName               = null;
    public  double   Expense               = 0.0;         
    public  Date     FirstCommDate         = null;
    public  double   Income                = 0.0;         
    public  Date     LastCommDate          = null;
    public  long     MerchantNumber        = 0L;
    public  double   NetRevenue            = 0.0;         
    public  double   PayRate               = 0.0;         
    public  long     RepId                 = 0L;
    public  int      VolCount              = 0;
    public  double   AcctSetupRate         = 0.0;
    public  double   AcctSetupFee          = 0.0;
    public  double   NewAppCommission      = 0.0;
    public  double   RevenueCommission     = 0.0;

    public PaymentDetailData( ResultSet resultSet )
      throws java.sql.SQLException
    {            
      MerchantNumber      = resultSet.getLong("merchant_number");
      CommissionDate      = resultSet.getDate("comm_date");
      CommissionAmount    = resultSet.getDouble("comm_amount");        
      Expense             = resultSet.getDouble("expense");
      Income              = resultSet.getDouble("income");
      NetRevenue          = resultSet.getDouble("net_revenue");
      PayRate             = resultSet.getDouble("pay_rate");
      RepId               = resultSet.getLong("rep_id");
      ActivationDate      = resultSet.getDate("activation_date");
      DbaName             = resultSet.getString("dba_name");
      FirstCommDate       = resultSet.getDate("first_comm_date");
      LastCommDate        = resultSet.getDate("last_comm_date");
      AcctOpenDate        = resultSet.getDate("date_opened");
      AcctSetupRate       = resultSet.getDouble("acct_setup_rate");
      AcctSetupFee        = resultSet.getDouble("acct_setup_fee");
      NewAppCommission    = resultSet.getDouble("new_app_comm");
      RevenueCommission   = resultSet.getDouble("revenue_comm");
    }
  }
  
  public class SalesRepHierarchy
  {
    public   Date     ActivationDate     = null;
    public   String   DbaName            = null;  
    public   Date     FirstCommDate      = null;
    public   long     HierarchyNode      = 0L;
    public   Date     LastCommDate       = null;
    public   long     MerchantNumber     = 0L;
    public   String   GroupName          = null;
    public   String   Portfolio          = null;
    public   long     OrgId              = 0L;
    
    public SalesRepHierarchy( ResultSet resultSet )
    throws java.sql.SQLException
    {
      OrgId            = resultSet.getLong("org_id");
      HierarchyNode    = resultSet.getLong("node_id");
      MerchantNumber   = resultSet.getLong("merchant_number");
      GroupName        = resultSet.getString("group_name");
      DbaName          = resultSet.getString("dba_name");
      Portfolio          = resultSet.getString("portfolio");
      FirstCommDate    = resultSet.getDate("first_comm_date");
      LastCommDate     = resultSet.getDate("last_comm_date");
      ActivationDate   = resultSet.getDate("activation_date");
    }
  }
    
  private class HierarchyValidation 
    implements Validation
 {
    private String ErrorMessage = null;

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate( String fdata )
    {
      int recCount = 0;

      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:192^9*/

//  ************************************************************
//  #sql [Ctx] { select count(org_group) 
//            from   organization 
//            where  org_group = :fdata
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(org_group)  \n          from   organization \n          where  org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fdata);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^9*/        
      }
      catch( java.sql.SQLException e )
      {
        ErrorMessage = "Invalid Hierarchy Node";
      }
  
      if ( recCount == 0 && (getAutoSubmitName().equals("btnAddHierarchy") ) )
      {
        ErrorMessage = "Invalid Hierarchy Node";          
        
        addError(ErrorMessage);
      } 

      return( ErrorMessage == null );
    }
  }
  
  private class MerchantValidation 
    implements Validation
  {
    private String ErrorMessage = null;
  
    public String getErrorText()
    {
      return( ErrorMessage );
    }
  
    public boolean validate( String fdata )
    {
      int recCount = 0;
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:230^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(mf.merchant_number) 
//            from    mif  mf
//            where   mf.merchant_number = :fdata
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mf.merchant_number)  \n          from    mif  mf\n          where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.CommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fdata);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:235^9*/        
      }
      catch( java.sql.SQLException e )
      {
        ErrorMessage = "Merchant validation failed";
      }
    
      if ( recCount == 0 && (getAutoSubmitName().equals("btnAddCommission") || getAutoSubmitName().equals("btnAddMerchant")) )
      {
        ErrorMessage = "Invalid Merchant ID";
      } 

      return( ErrorMessage == null );
    }
  }

  private class SalesRepValidation 
  implements Validation
  {
    private String ErrorMessage = null;
  
    public String getErrorText()
    {
      return( ErrorMessage );
    }
  
    public boolean validate( String fdata )
    {
      int recCount = 0;
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:266^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(sr.user_id) 
//            from    commission_sales_rep  sr
//            where   sr.user_id = :fdata
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(sr.user_id)  \n          from    commission_sales_rep  sr\n          where   sr.user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.CommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fdata);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:271^9*/        
      }
      catch( java.sql.SQLException e )
      {
        logEntry("validate(" + fdata + ")", e.toString());
        ErrorMessage = "Sales Rep validation failed";
      }
    
      if ( recCount == 0 && (getAutoSubmitName().equals("btnAddCommission") || getAutoSubmitName().equals("btnAddMerchant")) )
      {
        ErrorMessage = "Invalid RepId ID";
      } 

      return( ErrorMessage == null );
    }
  }

  private class AddCommissionCondition
    implements Condition
  {
    Field  SubmitType    = null;
  
    public AddCommissionCondition( Field submitType )
    {
      SubmitType  = submitType;
    }
  
    public boolean isMet() 
    { 
      boolean     retVal    = false;
    
      if( SubmitType != null )
      {
        String fData = SubmitType.getData();
        
        if( fData.equals("addCommission") )
        {
          retVal = true;
        }
      }

      return( retVal );
    }
  }
  
  private class AddMerchantCondition
    implements Condition
  {
    Field  SubmitType    = null;
    
    public AddMerchantCondition( Field submitType )
    {
      SubmitType  = submitType;
    }
  
    public boolean isMet() 
    { 
      boolean     retVal    = false;
    
      if( SubmitType != null )
      {
        String fData = SubmitType.getData();
        
        if( fData.equals("addMerchant") )
        {
          retVal = true;
        }
      }

      return( retVal );
    }
  }
  
  protected class SalesRepTable extends DropDownTable
  {
    public SalesRepTable()
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;

      try
      {      
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:354^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   sr.user_id        as rep_id,
//                     sr.user_id || ' - ' || sr.rep_name  as rep_name
//            from     commission_sales_rep   sr
//            order by sr.rep_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   sr.user_id        as rep_id,\n                   sr.user_id || ' - ' || sr.rep_name  as rep_name\n          from     commission_sales_rep   sr\n          order by sr.rep_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.CommissionDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.CommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:360^9*/
        resultSet = it.getResultSet();

        addElement("0","Select One");
        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      catch( Exception e )
      {
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        try{ resultSet.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }    
  }
  
  protected class CommissionTypeTable extends DropDownTable
  {
    public CommissionTypeTable()
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;

      try
      {      
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:393^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ct.commission_type,
//                    ct.commission_desc
//            from    commission_type ct
//            where   ct.commission_type > 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ct.commission_type,\n                  ct.commission_desc\n          from    commission_type ct\n          where   ct.commission_type > 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.CommissionDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.CommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:399^9*/
        resultSet = it.getResultSet();

        addElement("","select one");
        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      catch( Exception e )
      {
      }      
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        try{ resultSet.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }
  
  public CommissionDataBean()
  {
    super(true);
  }
    
  public Vector getDataRows()
  {
    return( DataRows );
  }

  protected boolean autoSubmit( )
  {
    boolean   retVal    = true;
    String    fname     = getAutoSubmitName();
    
    if ( fname.equals("btnAddCommission") )
    {
      retVal = storeCommissionData();
    }
    else if ( fname.equals("btnAddHierarchy") )
    {
      retVal = storeSalesRepHierarchy();
    }
    else if ( fname.equals("btnAddMerchant") )
    {
      retVal = storeSalesRepMerchant();
    }

    return( retVal );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    Field                field;

    super.createFields(request);

    fields.add( new DropDownField( "repId","Sales Reps", new SalesRepTable(), false) );
    fields.add( new ButtonField( "btnSelect","Select") );
    fields.add( new ButtonField( "btnAddCommission","Save") );
    fields.add( new ButtonField( "btnAddHierarchy","Save") );
    fields.add( new ButtonField( "btnAddMerchant","Save") );
    fields.add( new ButtonField( "btnCancel","Cancel") );
    fields.add( new CurrencyField( "income", "Income",12,12,false) );     
    fields.add( new CurrencyField( "expense", "Expense",12,12,false) );     
    fields.add( new DateField( "commDate","Commission Date (mm/dd/yyyy)", false ) );
    ((DateField)fields.getField("commDate")).setDateFormat("MM/dd/yyyy");
    fields.add( new DateField( "commBeginDate", "Commission Begin Date (mm/dd/yyyy)", false ) );
    ((DateField)fields.getField("commBeginDate")).setDateFormat("MM/dd/yyyy");
    fields.add( new DateField( "commEndDate", "Commission End Date (mm/dd/yyyy)", false ) );
    ((DateField)fields.getField("commEndDate")).setDateFormat("MM/dd/yyyy");
    fields.add( new HiddenField("submitType"));
    fields.add( new NumberField( "merchantId", "Merchant Number", 12, 16, true, 0) );
    fields.add( new NumberField( "payRate", "Pay Rate (%)", 6, 6, false, 2) );
    fields.add( new NumberField( "hierarchyNode", "Hierarchy Node", 10, 16, true, 0) );

    Condition condition;
    condition = new AddCommissionCondition( fields.getField("submitType") );
    fields.getField("income").setOptionalCondition( condition );
    fields.getField("expense").setOptionalCondition( condition );
    fields.getField("commDate").setOptionalCondition( condition );
    fields.getField("payRate").setOptionalCondition( condition );
    
    condition = new AddMerchantCondition( fields.getField("submitType") );
    fields.getField("commBeginDate").setOptionalCondition( condition );
    fields.getField("commEndDate").setOptionalCondition( condition );
    
    fields.getField("merchantId").addValidation( new MerchantValidation() );
    fields.getField("hierarchyNode").addValidation( new HierarchyValidation() );
    fields.getField("repId").addValidation( new SalesRepValidation() );

    fields.setHtmlExtra("class=\"formFields\"");      
  }

  public int getContractType( long nodeId )
  {
    int    retVal            = -1;
    ResultSetIterator    it           = null;
    ResultSet            resultSet    = null;
    
    try
    {
      while( nodeId != 9999999999L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:506^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  abc.contract_type  as contract_type
//            from    agent_bank_contract   abc
//            where   abc.hierarchy_node = :nodeId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  abc.contract_type  as contract_type\n          from    agent_bank_contract   abc\n          where   abc.hierarchy_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.CommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:511^9*/        
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          retVal = resultSet.getInt("contract_type");
          break;
        }
              
        // no match, get the parent node id
        nodeId = getParentNodeId(nodeId);
      }
    }
    catch( Exception e )
    {
      logEntry( "getContractType()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      try{ resultSet.close(); } catch( Exception e ) { }
    }

    return( retVal );
  }
  
  public String getBankContractDesc( long nodeId )
  {
    ResultSetIterator    it           = null;
    ResultSet            resultSet    = null;
    String    retVal     = "";
    
    try
    {
      while( nodeId != 9999999999L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:546^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  abc.contract_type         as contract_type,
//                    decode(abc.contract_type,
//                    '1', decode( sum (decode(abc.billing_element_type,'43',1,0) ),
//                                 0,'Liability',
//                                 'Partnership'),
//                    '2', 'Referral',
//                    '')                       as contract_desc
//            from    agent_bank_contract   abc
//            where   abc.hierarchy_node = :nodeId
//            group by abc.contract_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  abc.contract_type         as contract_type,\n                  decode(abc.contract_type,\n                  '1', decode( sum (decode(abc.billing_element_type,'43',1,0) ),\n                               0,'Liability',\n                               'Partnership'),\n                  '2', 'Referral',\n                  '')                       as contract_desc\n          from    agent_bank_contract   abc\n          where   abc.hierarchy_node =  :1 \n          group by abc.contract_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.CommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:558^9*/  
        
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          retVal = resultSet.getString("contract_desc");
          break;
        }        
        nodeId = getParentNodeId(nodeId);          
      }
    }
    catch( Exception e )
    {
      logEntry( "getBankContractDesc()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      try{ resultSet.close(); } catch( Exception e ) { }
    }

    return( retVal );
  }
  
  private long getParentNodeId( long nodeId )
  {
    long        retVal      = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:588^7*/

//  ************************************************************
//  #sql [Ctx] { select  orgp.org_group  
//          from    organization      orgp,
//                  organization      orgc,
//                  parent_org        po
//          where   orgc.org_group = :nodeId and                
//                  po.org_num = orgc.org_num and
//                  orgp.org_num = po.parent_org_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  orgp.org_group   \n        from    organization      orgp,\n                organization      orgc,\n                parent_org        po\n        where   orgc.org_group =  :1  and                \n                po.org_num = orgc.org_num and\n                orgp.org_num = po.parent_org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.CommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:597^7*/
    }
    catch( java.sql.SQLException e )
    { 
      // no rows found, return 0L
    }
    catch( Exception e )
    {
      logEntry( "getParentNodeId(" + nodeId + ")", e.toString() );
    }

    return( retVal );
  }
  
  public int getRepGroupId()
  {
    return( getRepGroupId(getLong("repId")) );
  }
  
  public int getRepGroupId( long repId )
  {
    int  retVal   = 0;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:622^7*/

//  ************************************************************
//  #sql [Ctx] { select   group_id 
//          from     commission_sales_rep  
//          where    user_id = :repId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select   group_id  \n        from     commission_sales_rep  \n        where    user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.CommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,repId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:627^7*/
    }
    catch(java.sql.SQLException e)
    {
    }      

    return retVal;
  }
  
  public boolean hasPartnershipContract( long nodeId )
  {
    boolean    retVal            = false;
    ResultSetIterator  it           = null;
    ResultSet          resultSet    = null;
    
    try
    {
      while( retVal == false && nodeId != 9999999999L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:646^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  abc.billing_element_type  as billing_element_type
//            from    agent_bank_contract   abc
//            where   abc.hierarchy_node = :nodeId
//                    --and abc.contract_type = '1'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  abc.billing_element_type  as billing_element_type\n          from    agent_bank_contract   abc\n          where   abc.hierarchy_node =  :1 \n                  --and abc.contract_type = '1'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.CommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:652^9*/
        
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          if( resultSet.getInt("billing_element_type") == 43 )
          {
            retVal = true;
          }
        }
              
        nodeId = getParentNodeId(nodeId);
      }
    }
    catch( Exception e )
    {
      logEntry( "hasPartnershipContract()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      try{ resultSet.close(); } catch( Exception e ) { }
    }
    return( retVal );
  }
  
  public void loadData( )
  {
    long   repId                = getLong("repId");
    long   hierarchyNode        = getLong("hierarchyNode");
    int    commType             = HttpHelper.getInt(request,"commType",0);
    int    groupId              = getRepGroupId();
    Date   commDate             = getReportDateBegin();
    Date   reportDateBegin      = getReportDateBegin();
    Date   reportDateEnd        = getReportDateEnd();
    ResultSetIterator   it      = null;
    ResultSet   resultSet       = null;

    try
    {
      ReportRows.clear();
      
      if ( getReportType() == RT_REP_PAYMENT_SUMMARY )
      {
        if( groupId == REP_GROUP_AGENT_BANK )
        {
          /*@lineinfo:generated-code*//*@lineinfo:698^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  cm.user_id                         as rep_id,
//                      cm.commission_date                 as comm_date,
//                      cm.commission_type                 as comm_type,
//                      nvl(gr.hierarchy_node,'394100000')    as hierarchy_node,
//                      nvl(gr.org_name,'Referral Merchants') as org_name,
//                      count(cm.merchant_number)          as merchant_count,
//                      sum(cm.income)                     as income,
//                      sum(cm.expense)                    as expense,
//                      sum(cm.net_revenue)                as net_revenue,
//                      sum(cm.commission_amount)          as comm_amount,
//                      sum(cm.commission_amount)          as revenue_comm,
//                      cm.pay_rate                        as pay_rate,
//                      '0'                                as new_app_comm
//              from    commissions    cm,
//                      commission_sales_rep    rep,
//                      ( select sr.hierarchy_node  as hierarchy_node,
//                               sr.org_name        as org_name,
//                               gm.merchant_number as merchant_number
//                        from   organization o,
//                               group_merchant gm,
//                               commission_sales_rep_hierarchy sr 
//                        where  sr.rep_id = :repId
//                               and o.org_group = sr.hierarchy_node
//                               and gm.org_num = o.org_num 
//                      ) gr
//              where   cm.commission_date between trunc(:reportDateBegin,'month') and last_day(:reportDateEnd) and
//                      cm.user_id = :repId and
//                      rep.user_id = cm.user_id and
//                      cm.merchant_number = gr.merchant_number(+)
//              group by cm.commission_date, cm.user_id, cm.commission_type,
//                      rep.rep_name, gr.hierarchy_node, gr.org_name, cm.pay_rate
//              order by cm.commission_date, gr.hierarchy_node
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cm.user_id                         as rep_id,\n                    cm.commission_date                 as comm_date,\n                    cm.commission_type                 as comm_type,\n                    nvl(gr.hierarchy_node,'394100000')    as hierarchy_node,\n                    nvl(gr.org_name,'Referral Merchants') as org_name,\n                    count(cm.merchant_number)          as merchant_count,\n                    sum(cm.income)                     as income,\n                    sum(cm.expense)                    as expense,\n                    sum(cm.net_revenue)                as net_revenue,\n                    sum(cm.commission_amount)          as comm_amount,\n                    sum(cm.commission_amount)          as revenue_comm,\n                    cm.pay_rate                        as pay_rate,\n                    '0'                                as new_app_comm\n            from    commissions    cm,\n                    commission_sales_rep    rep,\n                    ( select sr.hierarchy_node  as hierarchy_node,\n                             sr.org_name        as org_name,\n                             gm.merchant_number as merchant_number\n                      from   organization o,\n                             group_merchant gm,\n                             commission_sales_rep_hierarchy sr \n                      where  sr.rep_id =  :1 \n                             and o.org_group = sr.hierarchy_node\n                             and gm.org_num = o.org_num \n                    ) gr\n            where   cm.commission_date between trunc( :2 ,'month') and last_day( :3 ) and\n                    cm.user_id =  :4  and\n                    rep.user_id = cm.user_id and\n                    cm.merchant_number = gr.merchant_number(+)\n            group by cm.commission_date, cm.user_id, cm.commission_type,\n                    rep.rep_name, gr.hierarchy_node, gr.org_name, cm.pay_rate\n            order by cm.commission_date, gr.hierarchy_node";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,repId);
   __sJT_st.setDate(2,reportDateBegin);
   __sJT_st.setDate(3,reportDateEnd);
   __sJT_st.setLong(4,repId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.CommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:732^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:736^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  cm.user_id                          as rep_id,
//                      cm.commission_date                  as comm_date,
//                      cm.commission_type                  as comm_type,
//                      o.org_group                         as hierarchy_node,
//                      o.org_name                          as org_name,
//                      cm.pay_rate                         as pay_rate,
//                      sum(cm.income)                      as income,
//                      sum(cm.expense)                     as expense,
//                      sum(cm.net_revenue)                 as net_revenue,
//                      count(cm.merchant_number)           as merchant_count,
//                      sum(cm.commission_amount)           as revenue_comm,
//                      sum(nvl(cms.commission_amount,0))   as new_app_comm,
//                      sum(cm.commission_amount) + 
//                      sum(nvl(cms.commission_amount,0))   as comm_amount   
//              from    mif             mf,
//                      organization    o,
//                      commissions     cm,
//                      commissions     cms
//              where   cm.commission_date between trunc(:reportDateBegin,'month') and last_day(:reportDateEnd) and
//                      cm.user_id = :repId and
//                      cm.commission_type in (27,28) and
//                      cms.commission_date(+) = cm.commission_date and
//                      cms.user_id(+) = cm.user_id and
//                      cms.commission_type(+) = 29 and 
//                      cms.merchant_number(+) = cm.merchant_number and
//                      mf.merchant_number(+) = cm.merchant_number and
//                      o.org_group = '3941' || mf.group_2_association 
//              group by mf.group_2_association, cm.commission_date, o.org_name, 
//                      o.org_group, cm.commission_type, cm.user_id, cm.pay_rate
//              order by cm.commission_date, mf.group_2_association
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cm.user_id                          as rep_id,\n                    cm.commission_date                  as comm_date,\n                    cm.commission_type                  as comm_type,\n                    o.org_group                         as hierarchy_node,\n                    o.org_name                          as org_name,\n                    cm.pay_rate                         as pay_rate,\n                    sum(cm.income)                      as income,\n                    sum(cm.expense)                     as expense,\n                    sum(cm.net_revenue)                 as net_revenue,\n                    count(cm.merchant_number)           as merchant_count,\n                    sum(cm.commission_amount)           as revenue_comm,\n                    sum(nvl(cms.commission_amount,0))   as new_app_comm,\n                    sum(cm.commission_amount) + \n                    sum(nvl(cms.commission_amount,0))   as comm_amount   \n            from    mif             mf,\n                    organization    o,\n                    commissions     cm,\n                    commissions     cms\n            where   cm.commission_date between trunc( :1 ,'month') and last_day( :2 ) and\n                    cm.user_id =  :3  and\n                    cm.commission_type in (27,28) and\n                    cms.commission_date(+) = cm.commission_date and\n                    cms.user_id(+) = cm.user_id and\n                    cms.commission_type(+) = 29 and \n                    cms.merchant_number(+) = cm.merchant_number and\n                    mf.merchant_number(+) = cm.merchant_number and\n                    o.org_group = '3941' || mf.group_2_association \n            group by mf.group_2_association, cm.commission_date, o.org_name, \n                    o.org_group, cm.commission_type, cm.user_id, cm.pay_rate\n            order by cm.commission_date, mf.group_2_association";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,reportDateBegin);
   __sJT_st.setDate(2,reportDateEnd);
   __sJT_st.setLong(3,repId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.CommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:768^11*/
        }
        resultSet = it.getResultSet();    
        while( resultSet.next() )
        {
          ReportRows.addElement( new PaymentSummaryData( resultSet ) );
        }
        resultSet.close();
        it.close();
      }
      else if ( getReportType() == RT_REP_PAYMENT_DETAILS )
      {
        if( groupId == REP_GROUP_AGENT_BANK )
        {
          /*@lineinfo:generated-code*//*@lineinfo:782^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  cm.user_id                         as rep_id,
//                      cm.commission_date                 as comm_date,
//                      nvl(gr.hierarchy_node,'0')         as hierarchy_node,
//                      nvl(gr.org_name,'')                as org_name,
//                      cm.merchant_number                 as merchant_number,
//                      sum(cm.income)                     as income,
//                      sum(cm.expense)                    as expense,
//                      sum(cm.net_revenue)                as net_revenue,
//                      sum(cm.pay_rate)                   as pay_rate,
//                      sum(cm.commission_amount)          as comm_amount,
//                      sum(cm.commission_amount)          as revenue_comm,
//                      '0'                                acct_setup_fee,
//                      '0'                                as new_app_comm,
//                      '0.0'                              as acct_setup_rate,
//                      inf.dba_name                       as dba_name,
//                      inf.app_seq_num                    as app_seq_num,
//                      inf.date_opened                    as date_opened,
//                      inf.activation_date                as activation_date,
//                      inf.first_comm_date                as first_comm_date,
//                      inf.last_comm_date                 as last_comm_date
//              from    commissions    cm,
//                      commission_info inf,
//                      ( select sr.hierarchy_node  as hierarchy_node,
//                               sr.org_name        as org_name,
//                               gm.merchant_number as merchant_number
//                        from   organization o,
//                               group_merchant gm,
//                               commission_sales_rep_hierarchy sr 
//                        where  sr.rep_id = :repId
//                               and o.org_group = sr.hierarchy_node
//                               and gm.org_num = o.org_num 
//                      ) gr
//              where   cm.commission_date = :commDate and
//                      cm.user_id = :repId and
//                      ( :commType = 0 or cm.commission_type = :commType )and
//                      ( nvl(gr.hierarchy_node,'394100000') = :hierarchyNode and  
//                      cm.merchant_number = gr.merchant_number(+) )and
//                      inf.rep_id = cm.user_id and
//                      inf.merchant_number = cm.merchant_number
//              group by gr.hierarchy_node, gr.org_name,
//                      cm.user_id, cm.merchant_number, cm.commission_date,
//                      inf.dba_name, inf.app_seq_num, inf.date_opened,
//                      inf.activation_date, inf.first_comm_date, inf.last_comm_date 
//              order by inf.date_opened desc, inf.activation_date desc, cm.merchant_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cm.user_id                         as rep_id,\n                    cm.commission_date                 as comm_date,\n                    nvl(gr.hierarchy_node,'0')         as hierarchy_node,\n                    nvl(gr.org_name,'')                as org_name,\n                    cm.merchant_number                 as merchant_number,\n                    sum(cm.income)                     as income,\n                    sum(cm.expense)                    as expense,\n                    sum(cm.net_revenue)                as net_revenue,\n                    sum(cm.pay_rate)                   as pay_rate,\n                    sum(cm.commission_amount)          as comm_amount,\n                    sum(cm.commission_amount)          as revenue_comm,\n                    '0'                                acct_setup_fee,\n                    '0'                                as new_app_comm,\n                    '0.0'                              as acct_setup_rate,\n                    inf.dba_name                       as dba_name,\n                    inf.app_seq_num                    as app_seq_num,\n                    inf.date_opened                    as date_opened,\n                    inf.activation_date                as activation_date,\n                    inf.first_comm_date                as first_comm_date,\n                    inf.last_comm_date                 as last_comm_date\n            from    commissions    cm,\n                    commission_info inf,\n                    ( select sr.hierarchy_node  as hierarchy_node,\n                             sr.org_name        as org_name,\n                             gm.merchant_number as merchant_number\n                      from   organization o,\n                             group_merchant gm,\n                             commission_sales_rep_hierarchy sr \n                      where  sr.rep_id =  :1 \n                             and o.org_group = sr.hierarchy_node\n                             and gm.org_num = o.org_num \n                    ) gr\n            where   cm.commission_date =  :2  and\n                    cm.user_id =  :3  and\n                    (  :4  = 0 or cm.commission_type =  :5  )and\n                    ( nvl(gr.hierarchy_node,'394100000') =  :6  and  \n                    cm.merchant_number = gr.merchant_number(+) )and\n                    inf.rep_id = cm.user_id and\n                    inf.merchant_number = cm.merchant_number\n            group by gr.hierarchy_node, gr.org_name,\n                    cm.user_id, cm.merchant_number, cm.commission_date,\n                    inf.dba_name, inf.app_seq_num, inf.date_opened,\n                    inf.activation_date, inf.first_comm_date, inf.last_comm_date \n            order by inf.date_opened desc, inf.activation_date desc, cm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,repId);
   __sJT_st.setDate(2,commDate);
   __sJT_st.setLong(3,repId);
   __sJT_st.setInt(4,commType);
   __sJT_st.setInt(5,commType);
   __sJT_st.setLong(6,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.CommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:828^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:832^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  cm.user_id                         as rep_id,
//                      cm.commission_date                 as comm_date,
//                      o.org_group                        as hierarchy_node,
//                      o.org_name                         as org_name,
//                      cm.merchant_number                 as merchant_number,
//                      cm.income                          as income,
//                      cm.expense                         as expense,
//                      cm.net_revenue                     as net_revenue,
//                      cm.pay_rate                        as pay_rate,
//                      cm.commission_amount               as revenue_comm,
//                      cm.commission_amount + 
//                      nvl(cms.commission_amount,0)       as comm_amount,
//                      cm.commission_type                 as comm_type,
//                      cms.income                         as acct_setup_fee,
//                      cms.pay_rate                       as acct_setup_rate,
//                      nvl(cms.commission_amount,0)       as new_app_comm,
//                      cif.date_opened                    as date_opened,
//                      cif.dba_name                       as dba_name,
//                      cif.app_seq_num                    as app_seq_num,
//                      cif.activation_date                as activation_date,
//                      cif.first_comm_date                as first_comm_date,
//                      cif.last_comm_date                 as last_comm_date   
//              from    organization     o,
//                      group_merchant   gm,
//                      commissions      cm,
//                      commissions      cms,
//                      commission_info  cif
//              where   o.org_group = :hierarchyNode and
//                      gm.org_num = o.org_num and
//                      cm.merchant_number = gm.merchant_number and
//                      cm.commission_date = :commDate and
//                      cm.user_id = :repId and
//                      cm.commission_type in (27,28) and
//                      cms.commission_date(+) = cm.commission_date and
//                      cms.user_id(+) = cm.user_id and
//                      cms.commission_type(+) = 29 and 
//                      cms.merchant_number(+) = cm.merchant_number and
//                      cif.merchant_number = cm.merchant_number and
//                      cif.rep_id = cm.user_id 
//              order by cif.activation_date desc, cif.date_opened desc, cm.merchant_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cm.user_id                         as rep_id,\n                    cm.commission_date                 as comm_date,\n                    o.org_group                        as hierarchy_node,\n                    o.org_name                         as org_name,\n                    cm.merchant_number                 as merchant_number,\n                    cm.income                          as income,\n                    cm.expense                         as expense,\n                    cm.net_revenue                     as net_revenue,\n                    cm.pay_rate                        as pay_rate,\n                    cm.commission_amount               as revenue_comm,\n                    cm.commission_amount + \n                    nvl(cms.commission_amount,0)       as comm_amount,\n                    cm.commission_type                 as comm_type,\n                    cms.income                         as acct_setup_fee,\n                    cms.pay_rate                       as acct_setup_rate,\n                    nvl(cms.commission_amount,0)       as new_app_comm,\n                    cif.date_opened                    as date_opened,\n                    cif.dba_name                       as dba_name,\n                    cif.app_seq_num                    as app_seq_num,\n                    cif.activation_date                as activation_date,\n                    cif.first_comm_date                as first_comm_date,\n                    cif.last_comm_date                 as last_comm_date   \n            from    organization     o,\n                    group_merchant   gm,\n                    commissions      cm,\n                    commissions      cms,\n                    commission_info  cif\n            where   o.org_group =  :1  and\n                    gm.org_num = o.org_num and\n                    cm.merchant_number = gm.merchant_number and\n                    cm.commission_date =  :2  and\n                    cm.user_id =  :3  and\n                    cm.commission_type in (27,28) and\n                    cms.commission_date(+) = cm.commission_date and\n                    cms.user_id(+) = cm.user_id and\n                    cms.commission_type(+) = 29 and \n                    cms.merchant_number(+) = cm.merchant_number and\n                    cif.merchant_number = cm.merchant_number and\n                    cif.rep_id = cm.user_id \n            order by cif.activation_date desc, cif.date_opened desc, cm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setDate(2,commDate);
   __sJT_st.setLong(3,repId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.reports.CommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:874^11*/
        }
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          ReportRows.addElement( new PaymentDetailData( resultSet ) );
        }
        resultSet.close();
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      try{ resultSet.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadHierarchy()
  {
    ResultSetIterator it                 = null;
    ResultSet         resultSet          = null;
    long              repId              = getLong("repId");
    Date              reportBeginDate    = getReportDateBegin();
    
    try
    {
      ReportRows.clear();
      /*@lineinfo:generated-code*//*@lineinfo:906^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.group_2_association       as org_id,
//                  '3941' || mf.group_2_association || ' - ' || org.org_name 
//                                               as portfolio,
//                  nvl(gr.hierarchy_node,'394100000')     
//                                               as node_id,
//                  decode( nvl(gr.hierarchy_node,'394100000'),
//                          '394100000', 'Referral Merchants', 
//                          gr.hierarchy_node || ' - ' || gr.org_name) 
//                                               as group_name,
//                  cif.merchant_number          as merchant_number,
//                  cif.dba_name                 as dba_name,
//                  cif.date_opened              as date_opened,
//                  cif.activation_date          as activation_date,
//                  cif.first_comm_date          as first_comm_date,
//                  cif.last_comm_date           as last_comm_date
//          from    mif  mf,
//                  organization  org,
//                  commission_info  cif,          
//                  ( select sr.hierarchy_node  as hierarchy_node,
//                           sr.org_name        as org_name,
//                           gm.merchant_number as merchant_number
//                    from   organization o,
//                           group_merchant gm,
//                           commission_sales_rep_hierarchy sr 
//                    where  sr.rep_id = :repId
//                           and o.org_group = sr.hierarchy_node
//                           and gm.org_num = o.org_num 
//                  ) gr
//          where   cif.rep_id = :repId
//                  and :reportBeginDate between cif.first_comm_date and cif.last_comm_date
//                  and mf.merchant_number = cif.merchant_number
//                  and org.org_group = '3941' || mf.group_2_association 
//                  and cif.merchant_number = gr.merchant_number(+)    
//          order by gr.hierarchy_node, mf.group_2_association, cif.first_comm_date desc  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.group_2_association       as org_id,\n                '3941' || mf.group_2_association || ' - ' || org.org_name \n                                             as portfolio,\n                nvl(gr.hierarchy_node,'394100000')     \n                                             as node_id,\n                decode( nvl(gr.hierarchy_node,'394100000'),\n                        '394100000', 'Referral Merchants', \n                        gr.hierarchy_node || ' - ' || gr.org_name) \n                                             as group_name,\n                cif.merchant_number          as merchant_number,\n                cif.dba_name                 as dba_name,\n                cif.date_opened              as date_opened,\n                cif.activation_date          as activation_date,\n                cif.first_comm_date          as first_comm_date,\n                cif.last_comm_date           as last_comm_date\n        from    mif  mf,\n                organization  org,\n                commission_info  cif,          \n                ( select sr.hierarchy_node  as hierarchy_node,\n                         sr.org_name        as org_name,\n                         gm.merchant_number as merchant_number\n                  from   organization o,\n                         group_merchant gm,\n                         commission_sales_rep_hierarchy sr \n                  where  sr.rep_id =  :1 \n                         and o.org_group = sr.hierarchy_node\n                         and gm.org_num = o.org_num \n                ) gr\n        where   cif.rep_id =  :2 \n                and  :3  between cif.first_comm_date and cif.last_comm_date\n                and mf.merchant_number = cif.merchant_number\n                and org.org_group = '3941' || mf.group_2_association \n                and cif.merchant_number = gr.merchant_number(+)    \n        order by gr.hierarchy_node, mf.group_2_association, cif.first_comm_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,repId);
   __sJT_st.setLong(2,repId);
   __sJT_st.setDate(3,reportBeginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.CommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:942^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        ReportRows.addElement( new SalesRepHierarchy( resultSet ) );
      }
      resultSet.close();
      it.close();
      
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadHierarchy()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      try{ resultSet.close(); } catch( Exception e ) { }
    }
  }
  
  public Date getCommissionDate()
  {
    Date            retVal    = null;
 
    retVal  = ((DateField)fields.getField("commDate")).getSqlDate();
    
    return( retVal );
  }
  
  public boolean isEditMode()
  {
    return( true );
  }
  
  public boolean isSalesRepUser( long loginId )
  {                 
    int recCount = 0;
    
    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:985^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(sr.user_id) 
//          from    commission_sales_rep   sr
//          where   sr.user_id = :loginId 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(sr.user_id)  \n        from    commission_sales_rep   sr\n        where   sr.user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.CommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loginId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:990^7*/
    }
    catch(Exception e)
    {
      // ingonre
    }
    
    return( recCount > 0 );
  }
  
  public boolean storeCommissionData( )
  {
    boolean  retVal             = false;
    long     repId              = getLong("repId");
    long     merchantNumber     = getLong("merchantId");
    long     userId             = ReportUserBean.getUserId();
    Date     commDate           = ((DateField)fields.getField("commDate")).getSqlDate();
    double   income             = getDouble("income");
    double   expense            = getDouble("expense");
    double   netRevenue         = income - expense;
    double   payRate            = getDouble("payRate") * 0.01;
    double   commAmount         = netRevenue * payRate;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1015^7*/

//  ************************************************************
//  #sql [Ctx] { insert into commissions
//          (
//            user_id, 
//            merchant_number, 
//            commission_date, 
//            commission_type, 
//            transaction_type,
//            commission_amount,
//            pay_rate,
//            income,
//            expense,
//            manager_user_id, 
//            load_filename,
//            net_revenue
//          )
//          values
//          (
//            :repId,
//            :merchantNumber,
//            trunc(:commDate,'month'), 
//            '27',
//            'I',          
//            round(:commAmount,2),
//            :payRate,
//            round(:income,2),
//            round(:expense,2),
//            :userId,
//            'manual entry',
//            round(:netRevenue,2)
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into commissions\n        (\n          user_id, \n          merchant_number, \n          commission_date, \n          commission_type, \n          transaction_type,\n          commission_amount,\n          pay_rate,\n          income,\n          expense,\n          manager_user_id, \n          load_filename,\n          net_revenue\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          trunc( :3 ,'month'), \n          '27',\n          'I',          \n          round( :4 ,2),\n           :5 ,\n          round( :6 ,2),\n          round( :7 ,2),\n           :8 ,\n          'manual entry',\n          round( :9 ,2)\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,repId);
   __sJT_st.setLong(2,merchantNumber);
   __sJT_st.setDate(3,commDate);
   __sJT_st.setDouble(4,commAmount);
   __sJT_st.setDouble(5,payRate);
   __sJT_st.setDouble(6,income);
   __sJT_st.setDouble(7,expense);
   __sJT_st.setLong(8,userId);
   __sJT_st.setDouble(9,netRevenue);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1047^7*/
         
      /*@lineinfo:generated-code*//*@lineinfo:1049^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1052^7*/
            
      retVal = true;
    }
    catch( java.sql.SQLException e )
    {
      retVal = false;
      ErrorMessage.add("Internal Error: " + e.toString());
      logEntry("storeCommissionData()",e.toString());
    }

    return( retVal );
  }
  
  public boolean storeSalesRepHierarchy()
  {
    boolean  retVal             = true;
    long     repId              = getLong("repId");
    long     hierarchyNode      = getLong("hierarchyNode");
    String   nodeName           = getNodeName( hierarchyNode );
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1075^7*/

//  ************************************************************
//  #sql [Ctx] { insert into commission_sales_rep_hierarchy
//          (
//            rep_id, 
//            hierarchy_node, 
//            org_name
//          )
//          values
//          (
//            :repId,
//            :hierarchyNode,
//            :nodeName
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into commission_sales_rep_hierarchy\n        (\n          rep_id, \n          hierarchy_node, \n          org_name\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,repId);
   __sJT_st.setLong(2,hierarchyNode);
   __sJT_st.setString(3,nodeName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1089^7*/
         
      /*@lineinfo:generated-code*//*@lineinfo:1091^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1094^7*/
            
      retVal = true;
    }
    catch( java.sql.SQLException e )
    {
      retVal = false;
      ErrorMessage.add("Internal Error: " + e.toString());
      logEntry("storeSalesRepHierarchy()",e.toString());
    }

    return retVal;
  }
  
  public boolean merchantExists( long merchantId )
  {
    int      recCount = 0;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1114^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)  
//          from    commission_info    
//          where   merchant_number = :merchantId and
//                  rep_id = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)   \n        from    commission_info    \n        where   merchant_number =  :1  and\n                rep_id = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.CommissionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1120^7*/
    }
    catch( java.sql.SQLException e )
    {
    }

    return( recCount > 0 );
  }
  
  public boolean updateMerchantRecord( long repId, long merchantId, Date firstCommDate, Date lastCommDate )
  {
    boolean retVal = true;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1134^7*/

//  ************************************************************
//  #sql [Ctx] { update  commission_info comm
//          set     comm.rep_id = :repId,
//                  comm.first_comm_date = :firstCommDate,
//                  comm.last_comm_date = :lastCommDate
//          where   comm.merchant_number =:merchantId and 
//                  comm.rep_id = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  commission_info comm\n        set     comm.rep_id =  :1 ,\n                comm.first_comm_date =  :2 ,\n                comm.last_comm_date =  :3 \n        where   comm.merchant_number = :4  and \n                comm.rep_id = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,repId);
   __sJT_st.setDate(2,firstCommDate);
   __sJT_st.setDate(3,lastCommDate);
   __sJT_st.setLong(4,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1142^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1144^7*/

//  ************************************************************
//  #sql [Ctx] { commit 
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1147^7*/  
    }
    catch( java.sql.SQLException e)
    {
      retVal = false;
      logEntry( "updateMerchantRecord( " + merchantId + " )", e.toString() );
    }
    return retVal;
  }
  
  
  public boolean storeSalesRepMerchant()
  {
    boolean  retVal             = true;
    long     repId              = getLong("repId");
    long     hierarchyNode      = getLong("hierarchyNode");
    long     merchantId         = getLong("merchantId");
    long     appSeqNumber       = 0L;
    String   dbaName            = null;
    Date     dateOpened         = null;
    Date     activationDate     = null;
    Date     firstCommDate      = ((DateField)fields.getField("commBeginDate")).getSqlDate();
    Date     lastCommDate       = ((DateField)fields.getField("commEndDate")).getSqlDate();
    String   loginName          = ReportUserBean.getLoginName();    
    int      repGroupId         = 0;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    repGroupId    = getRepGroupId( repId );
    
    if( merchantExists(merchantId) )
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1181^9*/

//  ************************************************************
//  #sql [Ctx] { update  commission_info comm
//            set     comm.rep_id = :repId,
//                    comm.first_comm_date = :firstCommDate,
//                    comm.last_comm_date = :lastCommDate,
//                    comm.last_modified_by = :loginName,
//                    comm.last_modified_date = sysdate,
//                    comm.comm_group_id = :repGroupId
//            where   comm.merchant_number =:merchantId and 
//                    comm.rep_id = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  commission_info comm\n          set     comm.rep_id =  :1 ,\n                  comm.first_comm_date =  :2 ,\n                  comm.last_comm_date =  :3 ,\n                  comm.last_modified_by =  :4 ,\n                  comm.last_modified_date = sysdate,\n                  comm.comm_group_id =  :5 \n          where   comm.merchant_number = :6  and \n                  comm.rep_id = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,repId);
   __sJT_st.setDate(2,firstCommDate);
   __sJT_st.setDate(3,lastCommDate);
   __sJT_st.setString(4,loginName);
   __sJT_st.setInt(5,repGroupId);
   __sJT_st.setLong(6,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1192^9*/

        /*@lineinfo:generated-code*//*@lineinfo:1194^9*/

//  ************************************************************
//  #sql [Ctx] { commit 
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1197^9*/  
      }
      catch( java.sql.SQLException e)
      {
        retVal = false;
        ErrorMessage.add("Internal Error: " + e.toString());
        logEntry( "storeSalesRepMerchant( " + merchantId + " )", e.toString() );
      }
    }
    else
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1210^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app.app_user_id                as user_id,
//                    mr.app_seq_num                 as app_seq_num,
//                    nvl( mr.date_activated,
//                         to_date(mf.date_opened,'mmddyy')
//                       )                           as activation_date,
//                    upper(mf.dba_name)             as dba_name,
//                    to_date(mf.date_opened,'mmddyy')  as date_opened
//            from    merchant      mr,
//                    mif           mf,
//                    application   app
//            where   mr.merch_number = :merchantId
//                    and app.app_seq_num = mr.app_seq_num
//                    and mf.merchant_number = mr.merch_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app.app_user_id                as user_id,\n                  mr.app_seq_num                 as app_seq_num,\n                  nvl( mr.date_activated,\n                       to_date(mf.date_opened,'mmddyy')\n                     )                           as activation_date,\n                  upper(mf.dba_name)             as dba_name,\n                  to_date(mf.date_opened,'mmddyy')  as date_opened\n          from    merchant      mr,\n                  mif           mf,\n                  application   app\n          where   mr.merch_number =  :1 \n                  and app.app_seq_num = mr.app_seq_num\n                  and mf.merchant_number = mr.merch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.reports.CommissionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1225^9*/
        
        rs = it.getResultSet();
        
        while( rs.next() )
        {
          appSeqNumber        = rs.getLong("app_seq_num");
          dbaName             = rs.getString("dba_name");        
          dateOpened          = rs.getDate("date_opened");
          activationDate      = rs.getDate("activation_date");
        }
   
        /*@lineinfo:generated-code*//*@lineinfo:1237^9*/

//  ************************************************************
//  #sql [Ctx] { insert into commission_info
//            (
//              rep_id,
//              app_seq_num,
//              merchant_number,
//              dba_name,
//              date_opened,
//              activation_date,
//              first_comm_date,
//              last_comm_date,
//              last_modified_date,
//              last_modified_by,
//              comm_group_id
//            )
//            values
//            (
//              :repId,
//              :appSeqNumber,
//              :merchantId,
//              :dbaName,
//              :dateOpened,
//              :activationDate,
//              :firstCommDate,
//              :lastCommDate,
//              sysdate,
//              :loginName,
//              :repGroupId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into commission_info\n          (\n            rep_id,\n            app_seq_num,\n            merchant_number,\n            dba_name,\n            date_opened,\n            activation_date,\n            first_comm_date,\n            last_comm_date,\n            last_modified_date,\n            last_modified_by,\n            comm_group_id\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n            sysdate,\n             :9 ,\n             :10 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.reports.CommissionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,repId);
   __sJT_st.setLong(2,appSeqNumber);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setString(4,dbaName);
   __sJT_st.setDate(5,dateOpened);
   __sJT_st.setDate(6,activationDate);
   __sJT_st.setDate(7,firstCommDate);
   __sJT_st.setDate(8,lastCommDate);
   __sJT_st.setString(9,loginName);
   __sJT_st.setInt(10,repGroupId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1267^9*/

        /*@lineinfo:generated-code*//*@lineinfo:1269^9*/

//  ************************************************************
//  #sql [Ctx] { commit 
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1272^9*/  

        it.close();
      }
      catch( java.sql.SQLException e)
      {
        retVal = false;
        ErrorMessage.add("Internal Error: " + e.toString());
        logEntry( "storeSalesRepMerchant( " + merchantId + " )", e.toString() );
      }
      finally
      {
        try { it.close(); } catch( Exception e ) { }
        try { rs.close(); } catch( Exception e ) { }
      }      
    }
    
    return retVal;
  }
  
  public Vector getSubmitErrors()
  {
    return ErrorMessage;
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    if( isSalesRepUser(ReportUserBean.getUserId()) )
    {
      setData("repId", String.valueOf(ReportUserBean.getUserId()));
      getField("repId").addHtmlExtra("disabled");
    }
  }
 
}/*@lineinfo:generated-code*/