/*@lineinfo:filename=SabreBackOfficeDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/SabreBackOfficeDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-08-11 11:42:03 -0700 (Mon, 11 Aug 2008) $
  Version            : $Revision: 15253 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class SabreBackOfficeDataBean extends ReportSQLJBean
{
  public static final int     DT_ALL        = 0;
  public static final int     DT_VMC        = 1;
  public static final int     DT_NON_VMC    = 2;
  public static final int     DT_VMC_UATP   = 3;
  public static final int     DT_AMEX_DISC  = 4;
  
  public class RowData
  {
    public  String          AcqRefNum         = null;
    public  Date            BatchDate         = null;
    public  String          CardNumber        = null;
    public  double          CommissionAmount  = 0.0;
    public  double          CommissionRate    = 0.0;
    public  String          DbaName           = null;
    public  Date            DepositDate       = null;
    public  double          DiscountAmount    = 0.0;
    public  String          DocLocator        = null;
    public  String          IataNumber        = null;
    public  boolean         LastRecord        = false;
    public  long            MerchantId        = 0L;
    public  String          PassengerName     = null;
    public  String          Pcc               = null;
    public  String          StatementName     = null;
    public  String          TicketNumber      = null;
    public  double          TranAmount        = 0.0;
    public  Date            TranDate          = null;
    public  String          VendorCode        = null;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      AcqRefNum         = processString(resultSet.getString("acq_ref_num"));
      BatchDate         = resultSet.getDate  ("batch_date");
      CardNumber        = processString(resultSet.getString("card_number"));
      DiscountAmount    = resultSet.getDouble("disc_amount");
      CommissionRate    = resultSet.getDouble("comm_rate");
      DbaName           = processString(resultSet.getString("dba_name"));
      DepositDate       = resultSet.getDate  ("deposit_date");
      DocLocator        = processString(resultSet.getString("doc_locator"));
      IataNumber        = processString(resultSet.getString("iata"));
      MerchantId        = resultSet.getLong  ("merchant_number");
      StatementName     = processString(resultSet.getString("statement_dba"));
      TranAmount        = resultSet.getDouble("tran_amount");
      TranDate          = resultSet.getDate  ("tran_date");
      VendorCode        = processString(resultSet.getString("vendor_code"));
      TicketNumber      = processString(resultSet.getString("ticket_number"));
      Pcc               = processString(resultSet.getString("pcc"));
      PassengerName     = processString(resultSet.getString("passenger_name"));
      
      CommissionAmount  = (TranAmount - DiscountAmount);
    }
    
    public void setLast()
    {
      LastRecord = true;
    }
    
    public String getCommissionRateString( )
    {
      return( NumberFormatter.getPaddedInt( (int)(CommissionRate*100),4 ) );
    }
    
    public String getCommissionAmountString( )
    {
      // toFractionalAmount will clean up any Java floating point 
      // inaccuracy (i.e. when value sb 28.00 and is 27.9999999998)
      return( MesMath.toFractionalAmount(CommissionAmount,2,2) );
    }      
  }
  
  public class SummaryData
  {
    public  String          DbaName           = null;
    public  long            MerchantId        = 0L;
    public  double          NetAmount         = 0.0;
    
    public SummaryData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      DbaName     = processString(resultSet.getString("dba_name"));
      MerchantId  = resultSet.getLong  ("merchant_number");
      NetAmount   = resultSet.getDouble("net_amount");
    }
  }
  
  protected double          ExportGrossAmount       = 0.0;
  protected double          ExportNetAmount         = 0.0;
  protected int             DataType                = DT_ALL;
  
  public SabreBackOfficeDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    if ( ReportType != RT_DETAILS )   // RT_SUMMARY
    {
      line.append("\"Merchant Number\",");
      line.append("\"DBA Name\",");
      line.append("\"Net Amount\"");
    }      
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType != RT_DETAILS )   // RT_SUMMARY
    {
      SummaryData   record = (SummaryData)obj;
      
      line.append(record.MerchantId);
      line.append(",\"");
      line.append(record.DbaName);
      line.append("\",");
      line.append(record.NetAmount);
    }
  }
  
  protected void encodeHeaderFixed( StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_DETAILS )
    {
      ExportGrossAmount = 0.0;
      ExportNetAmount   = 0.0;
    
      // not actually using CSV, but using the hooks in the download
      // servlet to build a tab delimited file
      line.append("A\t");
      line.append( DateTimeFormatter.getFormattedDate( Calendar.getInstance().getTime(), "MM/dd/yyyy") );
      line.append("\t");
      line.append( getReportHierarchyNode() );    // download only works for merchants
      line.append("\t");
      line.append( getReportOrgName() );
      line.append("\t");
      line.append( getPcc( getReportHierarchyNode() ) );
      
      // carriage return required by the software
      // that imports this file into the back office
      // accounting system.
      line.append("\r");
    }
  }
  
  protected void encodeLineFixed( Object obj, StringBuffer line )
  {
    line.setLength(0);
    
    if ( ReportType == RT_DETAILS )
    {
      RowData record = (RowData)obj;
      
      // clear the line buffer and add 
      // the merchant specific data.
      line.append("D\t");
      line.append( record.VendorCode );
      line.append("\t");
      line.append(record.DbaName);
      line.append("\t");    
      line.append(record.Pcc);
      line.append("\t");    
      line.append(record.IataNumber);     // agency number
      line.append("\t");
      line.append(record.AcqRefNum);      // ticket number
      line.append("\t");
      line.append(DateTimeFormatter.getFormattedDate(record.TranDate,"MM/dd/yyyy"));
      line.append("\t");  
      line.append(record.TranAmount);
      line.append("\t");
      line.append(record.getCommissionRateString());
      line.append("\t");
      line.append(record.getCommissionAmountString());
      line.append("\t");
      line.append(DateTimeFormatter.getFormattedDate(record.DepositDate,"MM/dd/yyyy"));
      line.append("\tUSD\t");      // currency type
      line.append(record.CardNumber);
      line.append("\t");
      line.append(record.DocLocator);
      line.append("\t");
      line.append(record.PassengerName);
      line.append("\t");  
      line.append(record.TicketNumber);
      
      ExportGrossAmount += record.TranAmount;
      ExportNetAmount   += record.CommissionAmount;
      
      // carriage return required by the software
      // that imports this file into the back office
      // accounting system.
      line.append("\r");
      
      if( record.LastRecord == true )
      {
        line.append("\n");
        line.append("T\t");
        line.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy"));
        line.append("\t");
        line.append(DateTimeFormatter.getFormattedDate(ReportDateBegin,"MM/dd/yyyy"));
        line.append("\t");
        line.append(DateTimeFormatter.getFormattedDate(ReportDateEnd,"MM/dd/yyyy"));
        line.append("\t");
        line.append(ReportRows.size());
        line.append("\t");
        line.append(MesMath.toFractionalAmount(ExportGrossAmount,2,2));
        line.append("\t");
        line.append(MesMath.toFractionalAmount(ExportNetAmount,2,2));
        line.append("\r");
      }
    }
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    if ( ReportType == RT_DETAILS )
    {
      filename.append(NumberFormatter.getPaddedInt(DataType,2));
      filename.append(" ");
      if ( DataType == DT_ALL )
      {
        // use the current date
        filename.append( DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MMM dd yyyy") );
      }
      else    // specific data type was requested
      {        
        // useful for outputting several days from test code
        filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMM dd yyyy") );
      }        
    }
    else    // RT_SUMMARY
    {
      filename.append("back_office_summary_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
      if ( ReportDateBegin.equals(ReportDateEnd) == false )
      {
        filename.append("_to_");
        filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
      }
    }      
    return ( filename.toString() );
  }
  
  protected String getLastMEDateString( )
  {
    String      retVal    = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:303^7*/

//  ************************************************************
//  #sql [Ctx] { select  to_char(trunc((sysdate-3),'month'),'mm/dd/yyyy') 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_char(trunc((sysdate-3),'month'),'mm/dd/yyyy')  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.SabreBackOfficeDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:307^7*/
    }
    catch(Exception e)
    {
      logEntry("getLastMEDateString()",e.toString());
    }
    return( retVal );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = ( ReportType == RT_SUMMARY );  
        break;
        
      case FF_FIXED:
        retVal = ( ReportType == RT_DETAILS );
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          nodeId            = 0L;
    ResultSet                     resultSet         = null;
    RowData                       row               = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      nodeId = orgIdToHierarchyNode(orgId);
      
      if ( ReportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:348^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_number                        as merchant_number,
//                    mrs.pcc                                   as pcc,
//                    decode( dt.debit_credit_indicator,
//                            'C', mrs.refunds_vendor_code,
//                            mrs.sales_vendor_code )           as vendor_code,
//                    mf.dba_name                               as dba_name,           
//                    mrs.statement_dba_name                    as statement_dba,
//                    nvl( mrs.iata_number,
//                         substr(mf.merchant_number,-8) )      as iata,
//                    nvl(dt.reference_number,'0000000000')     as acq_ref_num,
//                    dt.transaction_date                       as tran_date,
//                    dt.batch_date                             as batch_date,
//                    ( dt.transaction_amount *
//                      decode(dt.debit_credit_indicator,'C',-1,1) )
//                                                              as tran_amount,
//                    decode( nvl(mrs.include_rates,'N'),
//                            'Y', ( decode(nvl(mrs.net_percentage, 0),
//                                    0, decode( dt.card_type,
//                                        'AM', nvl(mf.amex_discount_rate,0)*0.1,
//                                        'DS', nvl(mf.discvr_disc_rate,0)*0.1,
//                                        'DC', nvl(mf.diners_disc_rate,0)*0.1,
//                                        'JC', nvl(mf.jcb_disc_rate,0)*0.1,
//                                        'PL', nvl(mf.visa_disc_rate,0)*0.1,
//                                        'VS', nvl(mf.visa_disc_rate,0)*0.1,
//                                        'VB', nvl(mf.visa_disc_rate,0)*0.1,
//                                        'VD', nvl(mf.visa_disc_rate,0)*0.1,
//                                        'MC', nvl(mf.mastcd_disc_rate,0)*0.1,
//                                        'MB', nvl(mf.mastcd_disc_rate,0)*0.1,
//                                        'MD', nvl(mf.mastcd_disc_rate,0)*0.1,
//                                        0),
//                                    mrs.net_percentage
//                                 ) * decode(dt.debit_credit_indicator,'C',0,0.01) ),
//                            0 )                               as comm_rate,
//                    round( (                           
//                    decode( nvl(mrs.include_rates,'N'),
//                            'Y', ( decode(nvl(mrs.net_percentage, 0),
//                                    0, decode( dt.card_type,
//                                        'AM', nvl(mf.amex_discount_rate,0)*0.1,
//                                        'DS', nvl(mf.discvr_disc_rate,0)*0.1,
//                                        'DC', nvl(mf.diners_disc_rate,0)*0.1,
//                                        'JC', nvl(mf.jcb_disc_rate,0)*0.1,
//                                        'PL', nvl(mf.visa_disc_rate,0)*0.1,
//                                        'VS', nvl(mf.visa_disc_rate,0)*0.1,
//                                        'VB', nvl(mf.visa_disc_rate,0)*0.1,
//                                        'VD', nvl(mf.visa_disc_rate,0)*0.1,
//                                        'MC', nvl(mf.mastcd_disc_rate,0)*0.1,
//                                        'MB', nvl(mf.mastcd_disc_rate,0)*0.1,
//                                        'MD', nvl(mf.mastcd_disc_rate,0)*0.1,
//                                        0
//                               ),
//                                    mrs.net_percentage
//                                 ) * 0.01),
//                            0 ) * 0.01 * 
//                    ( dt.transaction_amount *
//                      decode(dt.debit_credit_indicator,'C',0,1) )
//                      ), 2 )                                  as disc_amount,
//                    (dt.batch_date+10)                        as deposit_date,
//                    dt.card_number                            as card_number,
//                    dt.acq_invoice_number                     as doc_locator,
//                    dt.passenger_name                         as passenger_name,
//                    decode( ( length(nvl(dt.purchase_id,'bad')) + 
//                              is_number(nvl(dt.purchase_id,'bad')) ),
//                            14, dt.purchase_id,
//                            null )                            as ticket_number       
//            from    organization                  o,
//                    group_merchant                gm,
//                    mif                           mf,
//                    merchant                      mr,
//                    merchant_sabre                mrs,
//                    daily_detail_file_ext_dt      dt
//            where   o.org_group = :nodeId and
//                    gm.org_num = o.org_num and
//                    mf.merchant_number = gm.merchant_number and
//                    mr.merch_number = mf.merchant_number and
//                    mrs.app_seq_num = mr.app_seq_num and
//                    --( :pcc = '-1' or mrs.pcc = :pcc ) and
//                    dt.merchant_number = mr.merch_number and        
//                    dt.batch_date between :beginDate and :endDate and
//                    ( 
//                      :DataType = 0 or -- all card types
//                      (:DataType = 1 and dt.card_type in ('VS','MC','DB')) or
//                      (:DataType = 2 and not dt.card_type in ( 'VS','MC','DB')) or
//                      (:DataType = 3 and dt.card_type in ( 'VS','MC','DB','PL')) or
//                      (:DataType = 4 and dt.card_type in ( 'AM','DS'))
//                    )
//            order by mf.merchant_number                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_number                        as merchant_number,\n                  mrs.pcc                                   as pcc,\n                  decode( dt.debit_credit_indicator,\n                          'C', mrs.refunds_vendor_code,\n                          mrs.sales_vendor_code )           as vendor_code,\n                  mf.dba_name                               as dba_name,           \n                  mrs.statement_dba_name                    as statement_dba,\n                  nvl( mrs.iata_number,\n                       substr(mf.merchant_number,-8) )      as iata,\n                  nvl(dt.reference_number,'0000000000')     as acq_ref_num,\n                  dt.transaction_date                       as tran_date,\n                  dt.batch_date                             as batch_date,\n                  ( dt.transaction_amount *\n                    decode(dt.debit_credit_indicator,'C',-1,1) )\n                                                            as tran_amount,\n                  decode( nvl(mrs.include_rates,'N'),\n                          'Y', ( decode(nvl(mrs.net_percentage, 0),\n                                  0, decode( dt.card_type,\n                                      'AM', nvl(mf.amex_discount_rate,0)*0.1,\n                                      'DS', nvl(mf.discvr_disc_rate,0)*0.1,\n                                      'DC', nvl(mf.diners_disc_rate,0)*0.1,\n                                      'JC', nvl(mf.jcb_disc_rate,0)*0.1,\n                                      'PL', nvl(mf.visa_disc_rate,0)*0.1,\n                                      'VS', nvl(mf.visa_disc_rate,0)*0.1,\n                                      'VB', nvl(mf.visa_disc_rate,0)*0.1,\n                                      'VD', nvl(mf.visa_disc_rate,0)*0.1,\n                                      'MC', nvl(mf.mastcd_disc_rate,0)*0.1,\n                                      'MB', nvl(mf.mastcd_disc_rate,0)*0.1,\n                                      'MD', nvl(mf.mastcd_disc_rate,0)*0.1,\n                                      0),\n                                  mrs.net_percentage\n                               ) * decode(dt.debit_credit_indicator,'C',0,0.01) ),\n                          0 )                               as comm_rate,\n                  round( (                           \n                  decode( nvl(mrs.include_rates,'N'),\n                          'Y', ( decode(nvl(mrs.net_percentage, 0),\n                                  0, decode( dt.card_type,\n                                      'AM', nvl(mf.amex_discount_rate,0)*0.1,\n                                      'DS', nvl(mf.discvr_disc_rate,0)*0.1,\n                                      'DC', nvl(mf.diners_disc_rate,0)*0.1,\n                                      'JC', nvl(mf.jcb_disc_rate,0)*0.1,\n                                      'PL', nvl(mf.visa_disc_rate,0)*0.1,\n                                      'VS', nvl(mf.visa_disc_rate,0)*0.1,\n                                      'VB', nvl(mf.visa_disc_rate,0)*0.1,\n                                      'VD', nvl(mf.visa_disc_rate,0)*0.1,\n                                      'MC', nvl(mf.mastcd_disc_rate,0)*0.1,\n                                      'MB', nvl(mf.mastcd_disc_rate,0)*0.1,\n                                      'MD', nvl(mf.mastcd_disc_rate,0)*0.1,\n                                      0\n                             ),\n                                  mrs.net_percentage\n                               ) * 0.01),\n                          0 ) * 0.01 * \n                  ( dt.transaction_amount *\n                    decode(dt.debit_credit_indicator,'C',0,1) )\n                    ), 2 )                                  as disc_amount,\n                  (dt.batch_date+10)                        as deposit_date,\n                  dt.card_number                            as card_number,\n                  dt.acq_invoice_number                     as doc_locator,\n                  dt.passenger_name                         as passenger_name,\n                  decode( ( length(nvl(dt.purchase_id,'bad')) + \n                            is_number(nvl(dt.purchase_id,'bad')) ),\n                          14, dt.purchase_id,\n                          null )                            as ticket_number       \n          from    organization                  o,\n                  group_merchant                gm,\n                  mif                           mf,\n                  merchant                      mr,\n                  merchant_sabre                mrs,\n                  daily_detail_file_ext_dt      dt\n          where   o.org_group =  :1  and\n                  gm.org_num = o.org_num and\n                  mf.merchant_number = gm.merchant_number and\n                  mr.merch_number = mf.merchant_number and\n                  mrs.app_seq_num = mr.app_seq_num and\n                  --( :pcc = '-1' or mrs.pcc = :pcc ) and\n                  dt.merchant_number = mr.merch_number and        \n                  dt.batch_date between  :2  and  :3  and\n                  ( \n                     :4  = 0 or -- all card types\n                    ( :5  = 1 and dt.card_type in ('VS','MC','DB')) or\n                    ( :6  = 2 and not dt.card_type in ( 'VS','MC','DB')) or\n                    ( :7  = 3 and dt.card_type in ( 'VS','MC','DB','PL')) or\n                    ( :8  = 4 and dt.card_type in ( 'AM','DS'))\n                  )\n          order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.SabreBackOfficeDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,DataType);
   __sJT_st.setInt(5,DataType);
   __sJT_st.setInt(6,DataType);
   __sJT_st.setInt(7,DataType);
   __sJT_st.setInt(8,DataType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.SabreBackOfficeDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:436^9*/
      }
      else    // assume summary
      {
        /*@lineinfo:generated-code*//*@lineinfo:440^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                        as merchant_number,
//                    mf.dba_name                               as dba_name,
//                    sum( nvl(sm.bank_amount,0) +
//                         nvl(sm.nonbank_amount,0) )           as net_amount
//            from    organization                  o,
//                    group_merchant                gm,
//                    mif                           mf,
//                    merchant                      mr,
//                    merchant_sabre                mrs,
//                    daily_detail_file_ext_summary sm
//            where   o.org_group = :nodeId and
//                    gm.org_num = o.org_num and
//                    mf.merchant_number = gm.merchant_number and
//                    mr.merch_number = mf.merchant_number and
//                    mrs.app_seq_num = mr.app_seq_num and
//                    --( :pcc = '-1' or mrs.pcc = :pcc ) and
//                    sm.merchant_number = mr.merch_number and        
//                    sm.batch_date between :beginDate and :endDate      
//            group by sm.merchant_number, mf.dba_name
//            order by mf.dba_name        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                        as merchant_number,\n                  mf.dba_name                               as dba_name,\n                  sum( nvl(sm.bank_amount,0) +\n                       nvl(sm.nonbank_amount,0) )           as net_amount\n          from    organization                  o,\n                  group_merchant                gm,\n                  mif                           mf,\n                  merchant                      mr,\n                  merchant_sabre                mrs,\n                  daily_detail_file_ext_summary sm\n          where   o.org_group =  :1  and\n                  gm.org_num = o.org_num and\n                  mf.merchant_number = gm.merchant_number and\n                  mr.merch_number = mf.merchant_number and\n                  mrs.app_seq_num = mr.app_seq_num and\n                  --( :pcc = '-1' or mrs.pcc = :pcc ) and\n                  sm.merchant_number = mr.merch_number and        \n                  sm.batch_date between  :2  and  :3       \n          group by sm.merchant_number, mf.dba_name\n          order by mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.SabreBackOfficeDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.SabreBackOfficeDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:462^9*/
      }
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        if ( ReportType == RT_DETAILS )
        {
          row = new RowData( resultSet );
          ReportRows.addElement( row );
        }
        else
        {
          ReportRows.addElement( new SummaryData( resultSet ) );
        }
      }
      it.close();   // this will also close the resultSet
      
      if ( row != null )
      {
        row.setLast();
      }        
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setDataType( int dataType )
  {
    DataType = dataType;
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         inc           = -1;
      
      if ( ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.MONDAY ) ||
           ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.TUESDAY ) )
      {
        // friday data available on monday,
        // sat,sun,mon data available on tuesday
        inc = -3;     
      }
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, inc );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      
      cal.setTime( ReportDateEnd );
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/