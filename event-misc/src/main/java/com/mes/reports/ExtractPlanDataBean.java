/*@lineinfo:filename=ExtractPlanDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ExtractPlanDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class ExtractPlanDataBean extends ReportSQLJBean
{
  public static final int           IDX_SALES           = 0;
  public static final int           IDX_CREDITS         = 1;
  public static final int           IDX_ADJUSTMENTS     = 2;
  public static final int           IDX_CHARGEBACKS     = 3;
  public static final int           IDX_COUNT           = 4;
  
  public class RowData
  {
    public Date                       ActiveDate          = null;
    public double[]                   Amounts             = new double[IDX_COUNT];
    public double[]                   AmountsYTD          = new double[IDX_COUNT];
    public double                     AvgTicket           = 0.0;
    public double                     AvgTicketYTD        = 0.0;
    public int[]                      Counts              = new int[IDX_COUNT];
    public int[]                      CountsYTD           = new int[IDX_COUNT];
    public double                     DiscountAmount      = 0.0;
    public double                     DiscountAmountYTD   = 0.0;
    public long                       HierarchyNode       = 0L;
    public long                       OrgId               = 0L;
    public String                     OrgName             = null;
    public String                     PlanTypeCode        = null;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      ActiveDate        = resultSet.getDate("active_date");
      AvgTicket         = resultSet.getDouble("avg_ticket");
      DiscountAmount    = resultSet.getDouble("discount_amount");
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      OrgId             = resultSet.getLong("org_num");
      OrgName           = resultSet.getString("org_name");
      PlanTypeCode      = resultSet.getString("plan_type");
      
      for( int i = 0; i < IDX_COUNT; ++i )
      {
        Amounts[i]  = resultSet.getDouble("amount_" + i);
        Counts[i]   = resultSet.getInt("count_" + i);
      }
      
      // using the parent class, load the YTD for this node
      loadTotalsYTD(this);
    }
    
    public int getInactiveMerchantCount()
    {
      return( MerchantCount - ActiveMerchantCount );
    }
  }
  
  public int                        ActiveMerchantCount = 0;
  public double                     AuthIncome          = 0.0;
  public double                     InterchangeIncome   = 0.0;
  public int                        MerchantCount       = 0;
  public double                     SysGenIncome        = 0.0;
  
  public ExtractPlanDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Active Date\",");
    line.append("\"Plan Type\",");
    line.append("\"Sales Count\",");
    line.append("\"Sales Amount\",");
    line.append("\"Credits Count\",");
    line.append("\"Credits Amount\",");
    line.append("\"Adj Count\",");
    line.append("\"Adj Amount\",");
    line.append("\"Chargeback Count\",");
    line.append("\"Chargeback Amount\",");
    line.append("\"Avg Ticket\",");
    line.append("\"Discount Amount\"");
    line.append("\n");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( encodeHierarchyNode(record.HierarchyNode) );
    line.append( ",\"" );
    line.append( record.OrgName );
    line.append( "\"," );
    line.append( DateTimeFormatter.getFormattedDate(record.ActiveDate,"MM/yyyy") );
    line.append( ",\"" );
    line.append( record.PlanTypeCode );
    line.append( "\"" );
    for( int i = 0; i < IDX_COUNT; ++i )
    {
      line.append(",");
      line.append( record.Counts[i] );
      line.append(",\"");
      line.append( MesMath.toCurrency(record.Amounts[i]) );
      line.append("\"");
    }
    line.append( ",\"" );
    line.append( MesMath.toCurrency(record.AvgTicket) );
    line.append( "\",\"" );
    line.append( MesMath.toCurrency(record.DiscountAmount) );
    line.append( "\n" );
  }
  
  public int getActiveMerchantCount()
  {
    return( ActiveMerchantCount );
  }
  
  public double getAuthIncome( )
  {
    return( AuthIncome );
  }
  
  public double getDiscountTotal()
  {
    double      retVal      = 0.0;
    
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      retVal += ((RowData)ReportRows.elementAt(i)).DiscountAmount;
    }
    return( retVal );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_plan_summary_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMyyyy") );
    return ( filename.toString() );
  }
  
  public double getFeesTotal( )
  {
    return( SysGenIncome + InterchangeIncome + AuthIncome );
  }
  
  public int getInactiveMerchantCount()
  {
    return( MerchantCount - ActiveMerchantCount );
  }
  
  public double getInterchangeIncome()
  {
    return( InterchangeIncome );
  }
  
  public int getMerchantCount()
  {
    return( MerchantCount );
  }
  
  public double getSysGenIncome( )
  {
    return( SysGenIncome );
  }
  
  public double getTotalDue()
  {
    return( getFeesTotal() + getDiscountTotal() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    long                          hierarchyNode     = 0L;
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      hierarchyNode = orgIdToHierarchyNode(orgId);
     
      if( SummaryType == ST_PARENT )
      { 
        /*@lineinfo:generated-code*//*@lineinfo:249^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                        ordered
//                        use_nl(sm pl)                     
//                    */
//                    o.org_num                           as org_num,
//                    :hierarchyNode                      as hierarchy_node,
//                    o.org_name                          as org_name,
//                    :beginDate                          as active_date,
//                    pl.pl_plan_type                     as plan_type,
//                    sum(pl.PL_CORRECT_DISC_AMT)         as discount_amount, 
//                    sum(pl.PL_NUMBER_OF_SALES)          as count_0,
//                    sum(pl.PL_SALES_AMOUNT)             as amount_0,
//                    sum(pl.PL_NUMBER_OF_CREDITS)        as count_1,
//                    sum(pl.PL_CREDITS_AMOUNT)           as amount_1,
//                    0                                   as count_2,
//                    0                                   as amount_2,
//                    sum(pl.PL_NUMBER_OF_CHARGEBACK)     as count_3,
//                    sum(pl.PL_CHARGEBACK_AMOUNT)        as amount_3,
//                    decode( sum(pl.PL_NUMBER_OF_SALES),
//                            0,0,
//                            round((sum(pl.PL_SALES_AMOUNT)/ 
//                                   sum(pl.PL_NUMBER_OF_SALES)),2)
//                          )                             as avg_ticket
//            from    organization                o,
//                    group_merchant              gm,
//                    group_rep_merchant          grm,
//                    monthly_extract_summary     sm,
//                    monthly_extract_pl          pl        
//            where   o.org_group = :hierarchyNode and
//                    gm.org_num = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    sm.merchant_number = gm.merchant_Number and
//                    --
//                    -- if it becomes necessary for this data to match
//                    -- the vital report exactly, then switch to using
//                    -- the assoc node at the time of the extract rather
//                    -- than using the MeS hierarchy. 
//                    --  1) comment out all reference to organization and group merchant
//                    --  2) remove the comment markers from the line below
//                    --
//                    --sm.assoc_hierarchy_node = :hierarchyNode and
//                    sm.active_date = :beginDate and
//                    pl.HH_LOAD_SEC = sm.hh_load_sec 
//            group by  o.org_num, o.org_name, pl.pl_plan_type      
//            order by pl.pl_plan_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                      ordered\n                      use_nl(sm pl)                     \n                  */\n                  o.org_num                           as org_num,\n                   :1                       as hierarchy_node,\n                  o.org_name                          as org_name,\n                   :2                           as active_date,\n                  pl.pl_plan_type                     as plan_type,\n                  sum(pl.PL_CORRECT_DISC_AMT)         as discount_amount, \n                  sum(pl.PL_NUMBER_OF_SALES)          as count_0,\n                  sum(pl.PL_SALES_AMOUNT)             as amount_0,\n                  sum(pl.PL_NUMBER_OF_CREDITS)        as count_1,\n                  sum(pl.PL_CREDITS_AMOUNT)           as amount_1,\n                  0                                   as count_2,\n                  0                                   as amount_2,\n                  sum(pl.PL_NUMBER_OF_CHARGEBACK)     as count_3,\n                  sum(pl.PL_CHARGEBACK_AMOUNT)        as amount_3,\n                  decode( sum(pl.PL_NUMBER_OF_SALES),\n                          0,0,\n                          round((sum(pl.PL_SALES_AMOUNT)/ \n                                 sum(pl.PL_NUMBER_OF_SALES)),2)\n                        )                             as avg_ticket\n          from    organization                o,\n                  group_merchant              gm,\n                  group_rep_merchant          grm,\n                  monthly_extract_summary     sm,\n                  monthly_extract_pl          pl        \n          where   o.org_group =  :3  and\n                  gm.org_num = o.org_num and\n                  grm.user_id(+) =  :4  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :5  = -1 ) and        \n                  sm.merchant_number = gm.merchant_Number and\n                  --\n                  -- if it becomes necessary for this data to match\n                  -- the vital report exactly, then switch to using\n                  -- the assoc node at the time of the extract rather\n                  -- than using the MeS hierarchy. \n                  --  1) comment out all reference to organization and group merchant\n                  --  2) remove the comment markers from the line below\n                  --\n                  --sm.assoc_hierarchy_node = :hierarchyNode and\n                  sm.active_date =  :6  and\n                  pl.HH_LOAD_SEC = sm.hh_load_sec \n          group by  o.org_num, o.org_name, pl.pl_plan_type      \n          order by pl.pl_plan_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ExtractPlanDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setLong(3,hierarchyNode);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setLong(5,AppFilterUserId);
   __sJT_st.setDate(6,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ExtractPlanDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:298^9*/
      }
      else    // SummaryType == ST_CHILD
      {
        /*@lineinfo:generated-code*//*@lineinfo:302^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                        ordered
//                        use_nl(sm pl)                     
//                    */
//                    o.org_num                           as org_num,
//                    o.org_group                         as hierarchy_node,
//                    o.org_name                          as org_name,
//                    :beginDate                          as active_date,
//                    pl.pl_plan_type                     as plan_type,
//                    sum(pl.PL_CORRECT_DISC_AMT)         as discount_amount, 
//                    sum(pl.PL_NUMBER_OF_SALES)          as count_0,
//                    sum(pl.PL_SALES_AMOUNT)             as amount_0,
//                    sum(pl.PL_NUMBER_OF_CREDITS)        as count_1,
//                    sum(pl.PL_CREDITS_AMOUNT)           as amount_1,
//                    0                                   as count_2,
//                    0                                   as amount_2,
//                    sum(pl.PL_NUMBER_OF_CHARGEBACK)     as count_3,
//                    sum(pl.PL_CHARGEBACK_AMOUNT)        as amount_3,
//                    decode( sum(pl.PL_NUMBER_OF_SALES),
//                            0,0,
//                            round((sum(pl.PL_SALES_AMOUNT)/ 
//                                   sum(pl.PL_NUMBER_OF_SALES)),2)
//                          )                             as avg_ticket
//            from    parent_org                  po,
//                    organization                o,
//                    group_merchant              gm,
//                    group_rep_merchant          grm,
//                    monthly_extract_summary     sm,
//                    monthly_extract_pl          pl        
//            where   po.parent_org_num = :orgId and
//                    o.org_num = po.org_num and
//                    gm.org_num = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    sm.merchant_number = gm.merchant_Number and
//                    --
//                    -- if it becomes necessary for this data to match
//                    -- the vital report exactly, then switch to using
//                    -- the assoc node at the time of the extract rather
//                    -- than using the MeS hierarchy. 
//                    --  1) comment out all reference to organization and group merchant
//                    --  2) remove the comment markers from the line below
//                    --
//                    --sm.assoc_hierarchy_node = :hierarchyNode and
//                    sm.active_date = :beginDate and
//                    pl.HH_LOAD_SEC = sm.hh_load_sec 
//            group by  o.org_num, o.org_group, o.org_name, pl.pl_plan_type      
//            order by o.org_name, pl.pl_plan_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                      ordered\n                      use_nl(sm pl)                     \n                  */\n                  o.org_num                           as org_num,\n                  o.org_group                         as hierarchy_node,\n                  o.org_name                          as org_name,\n                   :1                           as active_date,\n                  pl.pl_plan_type                     as plan_type,\n                  sum(pl.PL_CORRECT_DISC_AMT)         as discount_amount, \n                  sum(pl.PL_NUMBER_OF_SALES)          as count_0,\n                  sum(pl.PL_SALES_AMOUNT)             as amount_0,\n                  sum(pl.PL_NUMBER_OF_CREDITS)        as count_1,\n                  sum(pl.PL_CREDITS_AMOUNT)           as amount_1,\n                  0                                   as count_2,\n                  0                                   as amount_2,\n                  sum(pl.PL_NUMBER_OF_CHARGEBACK)     as count_3,\n                  sum(pl.PL_CHARGEBACK_AMOUNT)        as amount_3,\n                  decode( sum(pl.PL_NUMBER_OF_SALES),\n                          0,0,\n                          round((sum(pl.PL_SALES_AMOUNT)/ \n                                 sum(pl.PL_NUMBER_OF_SALES)),2)\n                        )                             as avg_ticket\n          from    parent_org                  po,\n                  organization                o,\n                  group_merchant              gm,\n                  group_rep_merchant          grm,\n                  monthly_extract_summary     sm,\n                  monthly_extract_pl          pl        \n          where   po.parent_org_num =  :2  and\n                  o.org_num = po.org_num and\n                  gm.org_num = o.org_num and\n                  grm.user_id(+) =  :3  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :4  = -1 ) and        \n                  sm.merchant_number = gm.merchant_Number and\n                  --\n                  -- if it becomes necessary for this data to match\n                  -- the vital report exactly, then switch to using\n                  -- the assoc node at the time of the extract rather\n                  -- than using the MeS hierarchy. \n                  --  1) comment out all reference to organization and group merchant\n                  --  2) remove the comment markers from the line below\n                  --\n                  --sm.assoc_hierarchy_node = :hierarchyNode and\n                  sm.active_date =  :5  and\n                  pl.HH_LOAD_SEC = sm.hh_load_sec \n          group by  o.org_num, o.org_group, o.org_name, pl.pl_plan_type      \n          order by o.org_name, pl.pl_plan_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ExtractPlanDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.ExtractPlanDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:353^9*/
      }
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
      
      // load the fee totals
      loadFees(hierarchyNode,beginDate);
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadFees( long hierarchyNode, Date activeDate )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:380^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum( decode( sm.merchant_status,
//                              'C', 0,  -- closed
//                              'D', 0,  -- deleted
//                              'F', 0,  -- fraud
//                               1 ) ),
//                  sum( decode( sm.merchant_status,
//                              'C', 0,  -- closed
//                              'D', 0,  -- deleted
//                              'F', 0,  -- fraud
//                               1 ) ),
//                  sum(sm.tot_inc_authorization),
//                  sum(sm.tot_inc_interchange),
//                  sum(sm.tot_inc_sys_generated) 
//          from    organization              o,
//                  group_merchant            gm,
//                  group_rep_merchant        grm,
//                  monthly_extract_summary   sm
//          where   o.org_group = :hierarchyNode and
//                  gm.org_num = o.org_num and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  sm.merchant_number = gm.merchant_number and
//                  sm.active_date = :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( decode( sm.merchant_status,\n                            'C', 0,  -- closed\n                            'D', 0,  -- deleted\n                            'F', 0,  -- fraud\n                             1 ) ),\n                sum( decode( sm.merchant_status,\n                            'C', 0,  -- closed\n                            'D', 0,  -- deleted\n                            'F', 0,  -- fraud\n                             1 ) ),\n                sum(sm.tot_inc_authorization),\n                sum(sm.tot_inc_interchange),\n                sum(sm.tot_inc_sys_generated)  \n        from    organization              o,\n                group_merchant            gm,\n                group_rep_merchant        grm,\n                monthly_extract_summary   sm\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and        \n                sm.merchant_number = gm.merchant_number and\n                sm.active_date =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ExtractPlanDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 5) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(5,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ActiveMerchantCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   MerchantCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   AuthIncome = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   InterchangeIncome = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   SysGenIncome = __sJT_rs.getDouble(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:410^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadFees()", e.toString() );
    }
    finally
    {
    }
  }
  
  public void loadTotalsYTD( RowData row )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
  
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:428^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ordered use_nl(sm pl) */
//                  sum(pl.PL_CORRECT_DISC_AMT)         as discount_amount, 
//                  sum(pl.PL_NUMBER_OF_SALES)          as count_0,
//                  sum(pl.PL_SALES_AMOUNT)             as amount_0,
//                  sum(pl.PL_NUMBER_OF_CREDITS)        as count_1,
//                  sum(pl.PL_CREDITS_AMOUNT)           as amount_1,
//                  0                                   as count_2,
//                  0                                   as amount_2,
//                  sum(pl.PL_NUMBER_OF_CHARGEBACK)     as count_3,
//                  sum(pl.PL_CHARGEBACK_AMOUNT)        as amount_3,
//                  decode( sum(pl.PL_NUMBER_OF_SALES),
//                          0,0,
//                          round((sum(pl.PL_SALES_AMOUNT)/ 
//                                 sum(pl.PL_NUMBER_OF_SALES)),2)
//                        )                             as avg_ticket
//          from    organization                o,
//                  group_merchant              gm,
//                  group_rep_merchant          grm,
//                  monthly_extract_summary     sm,
//                  monthly_extract_pl          pl        
//          where   o.org_group = :row.HierarchyNode and
//                  gm.org_num = o.org_num and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  sm.merchant_number = gm.merchant_Number and
//                  sm.active_date between trunc(:row.ActiveDate,'year') and :row.ActiveDate and
//                  pl.HH_LOAD_SEC = sm.hh_load_sec and
//                  pl.pl_plan_type = :row.PlanTypeCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ordered use_nl(sm pl) */\n                sum(pl.PL_CORRECT_DISC_AMT)         as discount_amount, \n                sum(pl.PL_NUMBER_OF_SALES)          as count_0,\n                sum(pl.PL_SALES_AMOUNT)             as amount_0,\n                sum(pl.PL_NUMBER_OF_CREDITS)        as count_1,\n                sum(pl.PL_CREDITS_AMOUNT)           as amount_1,\n                0                                   as count_2,\n                0                                   as amount_2,\n                sum(pl.PL_NUMBER_OF_CHARGEBACK)     as count_3,\n                sum(pl.PL_CHARGEBACK_AMOUNT)        as amount_3,\n                decode( sum(pl.PL_NUMBER_OF_SALES),\n                        0,0,\n                        round((sum(pl.PL_SALES_AMOUNT)/ \n                               sum(pl.PL_NUMBER_OF_SALES)),2)\n                      )                             as avg_ticket\n        from    organization                o,\n                group_merchant              gm,\n                group_rep_merchant          grm,\n                monthly_extract_summary     sm,\n                monthly_extract_pl          pl        \n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and        \n                sm.merchant_number = gm.merchant_Number and\n                sm.active_date between trunc( :4 ,'year') and  :5  and\n                pl.HH_LOAD_SEC = sm.hh_load_sec and\n                pl.pl_plan_type =  :6";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ExtractPlanDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,row.HierarchyNode);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,row.ActiveDate);
   __sJT_st.setDate(5,row.ActiveDate);
   __sJT_st.setString(6,row.PlanTypeCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.ExtractPlanDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:459^7*/
      // there should always be one record
      resultSet = it.getResultSet();
      resultSet.next();
      
      for( int i = 0; i < IDX_COUNT; ++i )
      {
        row.AmountsYTD[i] = resultSet.getDouble("amount_" + i);
        row.CountsYTD[i]  = resultSet.getInt("count_" + i );
      }
      row.AvgTicketYTD      = resultSet.getDouble("avg_ticket");
      row.DiscountAmountYTD = resultSet.getDouble("discount_amount");
      resultSet.close();
      it.close(); 
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadTotalYTD()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    // always set SummaryType to the specified
    SummaryType = HttpHelper.getInt(request,"summaryType",ST_PARENT);
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      Date        sqlDate       = null;
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.MONTH, -1 );
      cal.set(Calendar.DAY_OF_MONTH,1);
      sqlDate = new java.sql.Date( cal.getTime().getTime() );
      setReportDateBegin( sqlDate );
      setReportDateEnd( sqlDate );
    }    
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/