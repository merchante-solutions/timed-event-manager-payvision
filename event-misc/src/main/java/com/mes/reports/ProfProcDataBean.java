/*@lineinfo:filename=ProfProcDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ProfProcDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-05-31 15:53:42 -0700 (Thu, 31 May 2007) $
  Version            : $Revision: 13764 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

abstract class ProfProcDataBean
{
  protected DefaultContext          Ctx                   = null;
  protected BankContractDataBean    ContractDataBean      = null;
  protected Date                    ReportDateBegin       = null;
  protected Date                    ReportDateEnd         = null;
  protected long                    ReportOrgId           = 0L;
  
  public ProfProcDataBean(  )
  {
  }
  
  public void initialize( BankContractDataBean contractDataBean )
  {
    // use the database context from the
    // bank contract data bean.
    ContractDataBean  = contractDataBean;
    Ctx               = ContractDataBean.getContext();
    ReportDateBegin   = ContractDataBean.getReportDateBegin();
    ReportDateEnd     = ContractDataBean.getReportDateEnd();
    ReportOrgId       = ContractDataBean.getReportOrgId();
  }
  
  public void loadData( BankContractDataBean contractDataBean )
  {
    long            merchantId    = 0L;
    
    // setup the internal variables
    initialize( contractDataBean );
    merchantId = ContractDataBean.getReportMerchantId();
    
    switch( ContractDataBean.getReportBetGroup() )
    {
      case BankContractBean.BET_GROUP_DISC_IC:
        loadDetailDiscount( merchantId );
        break;
        
      case BankContractBean.BET_GROUP_CAPTURE_FEES:
        loadDetailCapture( merchantId );
        break;
        
      case BankContractBean.BET_GROUP_DEBIT_FEES:
        loadDetailDebit( merchantId );
        break;
        
      case BankContractBean.BET_GROUP_PLAN_FEES:
        loadDetailPlan( merchantId );
        break;
        
      case BankContractBean.BET_GROUP_AUTH_FEES:
        loadDetailAuth( merchantId );
        break;
        
      case BankContractBean.BET_GROUP_SYSTEM_FEES:
        loadDetailSysFees( merchantId );
        break;
        
      case BankContractBean.BET_GROUP_EQUIP_RENTAL:
      case BankContractBean.BET_GROUP_EQUIP_SALES:
      case BankContractBean.BET_GROUP_TAX_COLLECTED:
        loadDetailEquip( merchantId );
        break;
        
      case BankContractBean.BET_GROUP_EXCLUDED_EQUIP:
        // load the mes equipment expense (COGS) data for banks 
        // that separate processing p/l from equipment related 
        // p/l (i.e. Mountain West 3941400055)
        loadDetailMesEquip( merchantId, 
                            BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_EXCLUDED_EQUIP ), 
                            1, ContractTypes.CONTRACT_TYPE_EXPENSE );
        break;
        
      case BankContractBean.BET_GROUP_ADJUSTMENTS:
        loadDetailDCE( merchantId );
        break;
        
      //case BankContractBean.BET_GROUP_PARTNERSHIP:
      default:
        break;
    }
  }
  
  public void loadDetailMesEquipIncome( BankContractDataBean contractDataBean )
  {
    int             betGroup      = 0;
    int             betType       = 0;
    long            merchantId    = 0L;
    int             sales         = -1;
    
    // setup the internal variables
    initialize( contractDataBean );
    
    // extract the params from the BankContractDataBean
    merchantId = ContractDataBean.getReportMerchantId();
    betGroup   = ContractDataBean.getReportBetGroup();
    betType    = BankContractBean.groupIndexToBetType( betGroup );
    sales      = ( betGroup == BankContractBean.BET_GROUP_EQUIP_SALES ) ? 1 : 0;
    
    switch( ContractDataBean.getReportBetGroup() )
    {
      case BankContractBean.BET_GROUP_EQUIP_RENTAL:
      case BankContractBean.BET_GROUP_EQUIP_SALES:
        loadDetailMesEquip( merchantId, betType, sales, ContractTypes.CONTRACT_TYPE_INCOME );
        break;
    
      //case BankContractBean.BET_GROUP_DISC_IC:
      //case BankContractBean.BET_GROUP_CAPTURE_FEES:
      //case BankContractBean.BET_GROUP_DEBIT_FEES:
      //case BankContractBean.BET_GROUP_PLAN_FEES:
      //case BankContractBean.BET_GROUP_AUTH_FEES:
      //case BankContractBean.BET_GROUP_SYSTEM_FEES:
      //case BankContractBean.BET_GROUP_EXCLUDED_EQUIP:
      //case BankContractBean.BET_GROUP_ADJUSTMENTS:
      //case BankContractBean.BET_GROUP_PARTNERSHIP:
      default:
        break;
    }
  }
  
  protected abstract void loadDetailAuth( long merchantId );
  protected abstract void loadDetailCapture( long merchantId );
  protected abstract void loadDetailDCE( long merchantId );
  protected abstract void loadDetailDebit( long merchantId );
  protected abstract void loadDetailDiscount( long merchantId );
  protected abstract void loadDetailEquip( long merchantId );
  protected abstract void loadDetailPlan( long merchantId );
  protected abstract void loadDetailSysFees( long merchantId );
  
  protected void loadDetailMesEquip( long merchantId, int betType, int sales, int contractType )
  {
    loadDetailMesEquipLegacy(merchantId,betType,sales,contractType);
    loadDetailMesEquipTMG(merchantId,betType,sales,contractType);
  }
  
  protected void loadDetailMesEquipTMG( long merchantId, int betType, int sales, int contractType )
  {
    Date                            activeDate      = null;
    long                            equipNode       = 0L;
    ResultSetIterator               it              = null;
    int                             mesInventory    = 0;
    ContractTypes.PerItemRecord     record          = null;
    ResultSet                       resultSet       = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:177^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum(decode(sm.mes_inventory,'Y',1,0)),
//                  get_equip_hierarchy_node( :merchantId ) 
//          
//          from    monthly_extract_summary   sm
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and :ReportDateEnd
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum(decode(sm.mes_inventory,'Y',1,0)),\n                get_equip_hierarchy_node(  :1  ) \n         \n        from    monthly_extract_summary   sm\n        where   sm.merchant_number =  :2  and\n                sm.active_date between  :3  and  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ProfProcDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setDate(3,ReportDateBegin);
   __sJT_st.setDate(4,ReportDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mesInventory = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   equipNode = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:185^7*/        
    
      // load the expense from the inventory database
      if ( sales == 1 )
      {
        // equipment is owned by agent bank, only expense
        // is Just In Time (JIT) transfers.
        if ( mesInventory == 0 )
        {
          // just load the transfers, processing happens below
          /*@lineinfo:generated-code*//*@lineinfo:195^11*/

//  ************************************************************
//  #sql [Ctx] it = { -- determine the base cost when inventory was transferred
//              -- i.e. Just in time sale by MES to Cedars bank
//              select  sm.active_date                      as active_date,
//                      (
//                        'Transfer (' || ps.pc_code || '): ' ||
//                        nvl(pt.description,
//                          'P/N ' || pt.model_code) ||
//                        ' (SN:' || ps.serial_num ||
//                        ') deployed ' ||
//                        to_char(dp.start_ts,'mm/dd/yyyy')
//                      )                                   as equip_desc,
//                      decode( ps.pc_code,
//                              'NEW', nvl(ep.price_base,0),
//                              nvl(ep.price_used,0) 
//                            )                             as base_cost
//              from    monthly_extract_summary         sm,
//                      tmg_deployment                  dp,
//                      tmg_orders                      o,
//                      tmg_part_states                 ps,
//                      tmg_part_types                  pt,
//                      equip_price                     ep
//              where   sm.merchant_number = :merchantId and
//                      sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                      dp.merch_num = sm.merchant_number and
//                      trunc(dp.start_ts) between sm.month_begin_date and sm.month_end_date and
//                      dp.start_code in ( 'ADD','NEW' ) and
//                      o.ord_id = dp.start_ord_id and
//                      o.ord_stat_code = 'DONE' and
//                      ps.deploy_id = dp.deploy_id and
//                      ps.state_id =
//                      (
//                        select      min( psin.state_id ) 
//                        from        tmg_part_states psin 
//                        where       psin.deploy_id = dp.deploy_id
//                      ) and
//                      exists
//                      (
//                        select  x.create_ts
//                        from    tmg_part_states   x_ps,
//                                tmg_op_log        x_ol,
//                                tmg_transfers     x
//                        where   x_ps.prior_id = ps.state_id and
//                                x_ol.op_id    = x_ps.op_id and
//                                x_ol.op_code  = 'PART_XFER' and
//                                x.op_id       = x_ps.op_id
//                      ) and
//                      pt.pt_id = ps.pt_id and
//                      ep.rec_id = get_equip_price_rec_id(sm.merchant_number,pt.model_code,trunc(dp.start_ts))            
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "-- determine the base cost when inventory was transferred\n            -- i.e. Just in time sale by MES to Cedars bank\n            select  sm.active_date                      as active_date,\n                    (\n                      'Transfer (' || ps.pc_code || '): ' ||\n                      nvl(pt.description,\n                        'P/N ' || pt.model_code) ||\n                      ' (SN:' || ps.serial_num ||\n                      ') deployed ' ||\n                      to_char(dp.start_ts,'mm/dd/yyyy')\n                    )                                   as equip_desc,\n                    decode( ps.pc_code,\n                            'NEW', nvl(ep.price_base,0),\n                            nvl(ep.price_used,0) \n                          )                             as base_cost\n            from    monthly_extract_summary         sm,\n                    tmg_deployment                  dp,\n                    tmg_orders                      o,\n                    tmg_part_states                 ps,\n                    tmg_part_types                  pt,\n                    equip_price                     ep\n            where   sm.merchant_number =  :1  and\n                    sm.active_date between  :2  and  :3  and\n                    dp.merch_num = sm.merchant_number and\n                    trunc(dp.start_ts) between sm.month_begin_date and sm.month_end_date and\n                    dp.start_code in ( 'ADD','NEW' ) and\n                    o.ord_id = dp.start_ord_id and\n                    o.ord_stat_code = 'DONE' and\n                    ps.deploy_id = dp.deploy_id and\n                    ps.state_id =\n                    (\n                      select      min( psin.state_id ) \n                      from        tmg_part_states psin \n                      where       psin.deploy_id = dp.deploy_id\n                    ) and\n                    exists\n                    (\n                      select  x.create_ts\n                      from    tmg_part_states   x_ps,\n                              tmg_op_log        x_ol,\n                              tmg_transfers     x\n                      where   x_ps.prior_id = ps.state_id and\n                              x_ol.op_id    = x_ps.op_id and\n                              x_ol.op_code  = 'PART_XFER' and\n                              x.op_id       = x_ps.op_id\n                    ) and\n                    pt.pt_id = ps.pt_id and\n                    ep.rec_id = get_equip_price_rec_id(sm.merchant_number,pt.model_code,trunc(dp.start_ts))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ProfProcDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.ProfProcDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:245^11*/
        }
        else  // equipment is owned by MES
        {
          // load and process the purchases
          /*@lineinfo:generated-code*//*@lineinfo:250^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                      as active_date,
//                      (
//                        'Sales (' || ps.pc_code || '): ' || 
//                        nvl(pt.description,
//                          'P/N ' || pt.model_code) ||
//                        ' (SN:' || ps.serial_num ||
//                        ') deployed ' ||
//                        to_char(dp.start_ts,'mm/dd/yyyy')
//                      )                                   as equip_desc,
//                      decode( ps.pc_code,
//                              'NEW', nvl(ep.price_base,0),
//                              nvl(ep.price_used,0) 
//                            )                             as base_cost
//              from    monthly_extract_summary sm,
//                      tmg_deployment          dp,
//                      tmg_orders              o,
//                      tmg_part_states         ps,
//                      tmg_part_types          pt,
//                      equip_price             ep
//              where   sm.merchant_number = :merchantId and
//                      sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                      dp.merch_num = sm.merchant_number and
//                      trunc(dp.start_ts) between sm.month_begin_date and sm.month_end_date and
//                      dp.start_code in ( 'ADD','NEW' ) and
//                      o.ord_id = dp.start_ord_id and
//                      o.ord_stat_code = 'DONE' and
//                      ps.deploy_id = dp.deploy_id and
//                      ps.state_id =
//                      (
//                        select      min( psin.state_id ) 
//                        from        tmg_part_states psin 
//                        where       psin.deploy_id = dp.deploy_id
//                      ) and
//                      ps.disp_code = 'SOLD' and
//                      pt.pt_id = ps.pt_id and
//                      ep.rec_id = get_equip_price_rec_id(sm.merchant_number,pt.model_code,trunc(dp.start_ts))            
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                      as active_date,\n                    (\n                      'Sales (' || ps.pc_code || '): ' || \n                      nvl(pt.description,\n                        'P/N ' || pt.model_code) ||\n                      ' (SN:' || ps.serial_num ||\n                      ') deployed ' ||\n                      to_char(dp.start_ts,'mm/dd/yyyy')\n                    )                                   as equip_desc,\n                    decode( ps.pc_code,\n                            'NEW', nvl(ep.price_base,0),\n                            nvl(ep.price_used,0) \n                          )                             as base_cost\n            from    monthly_extract_summary sm,\n                    tmg_deployment          dp,\n                    tmg_orders              o,\n                    tmg_part_states         ps,\n                    tmg_part_types          pt,\n                    equip_price             ep\n            where   sm.merchant_number =  :1  and\n                    sm.active_date between  :2  and  :3  and\n                    dp.merch_num = sm.merchant_number and\n                    trunc(dp.start_ts) between sm.month_begin_date and sm.month_end_date and\n                    dp.start_code in ( 'ADD','NEW' ) and\n                    o.ord_id = dp.start_ord_id and\n                    o.ord_stat_code = 'DONE' and\n                    ps.deploy_id = dp.deploy_id and\n                    ps.state_id =\n                    (\n                      select      min( psin.state_id ) \n                      from        tmg_part_states psin \n                      where       psin.deploy_id = dp.deploy_id\n                    ) and\n                    ps.disp_code = 'SOLD' and\n                    pt.pt_id = ps.pt_id and\n                    ep.rec_id = get_equip_price_rec_id(sm.merchant_number,pt.model_code,trunc(dp.start_ts))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ProfProcDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.ProfProcDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:288^11*/
        }          
      }
      else    // rentals (only MES owned inventory supports rentals)
      {
        if ( mesInventory == 1 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:295^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                      as active_date,
//                      (
//                        'Rental (' || ps.pc_code || '): ' || 
//                        nvl(pt.description,
//                          'P/N ' || pt.model_code) ||
//                        ' (SN:' || ps.serial_num ||
//                        ') - ' ||
//                        to_char(sm.active_date,'Mon yyyy')
//                      )                                   as equip_desc,
//                      decode( ps.pc_code,
//                              'NEW', ep.PRICE_RENTAL,
//                              nvl(ep.price_rental_used,
//                                  ep.price_rental) 
//                            )                             as base_cost
//              from    monthly_extract_summary sm,
//                      tmg_deployment          dp,
//                      tmg_orders              o,
//                      tmg_part_states         ps,
//                      tmg_part_types          pt,
//                      equip_price             ep,
//                      mif                     mf
//              where   sm.merchant_number = :merchantId and
//                      sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                      dp.merch_num = sm.merchant_number and
//                      trunc(dp.start_ts) <= sm.month_end_date and
//                      dp.start_code in ( 'ADD','NEW' ) and
//                      o.ord_id = dp.start_ord_id and
//                      o.ord_stat_code = 'DONE' and
//                      ps.deploy_id = dp.deploy_id and
//                      ps.state_id =
//                      (
//                        select      min( psin.state_id ) 
//                        from        tmg_part_states psin 
//                        where       psin.deploy_id = dp.deploy_id
//                      ) and
//                      ps.disp_code = 'RENT' and   -- rented equipment
//                      ps.end_ts is null and       -- and still deployed
//                      pt.pt_id = ps.pt_id and
//                      mf.merchant_number = sm.merchant_number and                  
//                      --mf.activation_date > (sm.month_end_date - (365*3)) and
//                      ep.rec_id = get_equip_price_rec_id(sm.merchant_number,pt.model_code,trunc(dp.start_ts))            
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                      as active_date,\n                    (\n                      'Rental (' || ps.pc_code || '): ' || \n                      nvl(pt.description,\n                        'P/N ' || pt.model_code) ||\n                      ' (SN:' || ps.serial_num ||\n                      ') - ' ||\n                      to_char(sm.active_date,'Mon yyyy')\n                    )                                   as equip_desc,\n                    decode( ps.pc_code,\n                            'NEW', ep.PRICE_RENTAL,\n                            nvl(ep.price_rental_used,\n                                ep.price_rental) \n                          )                             as base_cost\n            from    monthly_extract_summary sm,\n                    tmg_deployment          dp,\n                    tmg_orders              o,\n                    tmg_part_states         ps,\n                    tmg_part_types          pt,\n                    equip_price             ep,\n                    mif                     mf\n            where   sm.merchant_number =  :1  and\n                    sm.active_date between  :2  and  :3  and\n                    dp.merch_num = sm.merchant_number and\n                    trunc(dp.start_ts) <= sm.month_end_date and\n                    dp.start_code in ( 'ADD','NEW' ) and\n                    o.ord_id = dp.start_ord_id and\n                    o.ord_stat_code = 'DONE' and\n                    ps.deploy_id = dp.deploy_id and\n                    ps.state_id =\n                    (\n                      select      min( psin.state_id ) \n                      from        tmg_part_states psin \n                      where       psin.deploy_id = dp.deploy_id\n                    ) and\n                    ps.disp_code = 'RENT' and   -- rented equipment\n                    ps.end_ts is null and       -- and still deployed\n                    pt.pt_id = ps.pt_id and\n                    mf.merchant_number = sm.merchant_number and                  \n                    --mf.activation_date > (sm.month_end_date - (365*3)) and\n                    ep.rec_id = get_equip_price_rec_id(sm.merchant_number,pt.model_code,trunc(dp.start_ts))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ProfProcDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.ProfProcDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:338^11*/
        }          
      }        
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1,
                                    resultSet.getDouble("base_cost") );
        record.setDescription( resultSet.getString("equip_desc") );
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                             contractType, record );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      ContractDataBean.logEntry("loadDetailMesEquip(" + merchantId + ")", e.toString());
    }      
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }
  
  protected void loadDetailMesEquipLegacy( long merchantId, int betType, int sales, int contractType )
  {
    Date                            activeDate      = null;
    long                            equipNode       = 0L;
    ResultSetIterator               it              = null;
    int                             mesInventory    = 0;
    ContractTypes.PerItemRecord     record          = null;
    ResultSet                       resultSet       = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:376^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum(decode(sm.mes_inventory,'Y',1,0)),
//                  get_equip_hierarchy_node( :merchantId ) 
//          
//          from    monthly_extract_summary   sm
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and :ReportDateEnd
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum(decode(sm.mes_inventory,'Y',1,0)),\n                get_equip_hierarchy_node(  :1  ) \n         \n        from    monthly_extract_summary   sm\n        where   sm.merchant_number =  :2  and\n                sm.active_date between  :3  and  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.ProfProcDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setDate(3,ReportDateBegin);
   __sJT_st.setDate(4,ReportDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mesInventory = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   equipNode = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:384^7*/        
    
      // load the expense from the inventory database
      if ( sales == 1 )
      {
        // equipment is owned by agent bank, only expense
        // is Just In Time (JIT) transfers.
        if ( mesInventory == 0 )
        {
          // just load the transfers, processing happens below
          /*@lineinfo:generated-code*//*@lineinfo:394^11*/

//  ************************************************************
//  #sql [Ctx] it = { -- determine the base cost when inventory was transferred
//              -- i.e. Just in time sale by MES to Cedars bank
//              select  sm.active_date                      as active_date,
//                      (
//                        'Transfer' || decode(ei.ei_class,1,':',' (Used):') ||
//                        nvl(eq.equip_descriptor,
//                          'P/N ' || ei.ei_part_number) ||
//                        ' (SN:' || ei.ei_serial_number ||
//                        ') deployed ' ||
//                        to_char(emt.action_date,'mm/dd/yyyy')
//                      )                                   as equip_desc,
//                      decode( ecd.condition,
//                              1, nvl(ep.price_base,nvl(ei.ei_unit_cost,0)),
//                              nvl(ep.price_used,nvl(ei.ei_unit_cost,0)) 
//                            )                             as base_cost
//              from    monthly_extract_summary         sm,
//                      equip_merchant_tracking         emt,
//                      equip_inventory                 ei,
//                      equip_client_deployment         ecd,
//                      equip_price                     ep,
//                      equipment                       eq
//              where   sm.merchant_number = :merchantId and
//                      sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                      emt.merchant_number = sm.merchant_number and
//                      trunc(emt.action_date) between sm.month_begin_date and sm.month_end_date and
//                      emt.action in ( 1, 4 ) and  -- acct setup deployment, additional equip deployment
//                      emt.action_desc in ( 1, 2, 4 ) and  -- buy, rent, buy refurbished
//                      emt.cancel_date is null and
//                      ei.ei_serial_number = emt.ref_num_serial_num and
//                      ecd.serial_number = ei.ei_serial_number and          
//                      ecd.to_client = ei.ei_owner and  -- to the current owner
//                      trunc(ecd.move_date) between trunc(emt.action_date)-7 and trunc(emt.action_date) and
//                      ecd.from_client = 0 and -- from MES
//                      eq.equip_model = ei.ei_part_number and
//                      ep.rec_id = get_equip_price_rec_id(emt.merchant_number,ei.ei_part_number,trunc(ecd.move_date))
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "-- determine the base cost when inventory was transferred\n            -- i.e. Just in time sale by MES to Cedars bank\n            select  sm.active_date                      as active_date,\n                    (\n                      'Transfer' || decode(ei.ei_class,1,':',' (Used):') ||\n                      nvl(eq.equip_descriptor,\n                        'P/N ' || ei.ei_part_number) ||\n                      ' (SN:' || ei.ei_serial_number ||\n                      ') deployed ' ||\n                      to_char(emt.action_date,'mm/dd/yyyy')\n                    )                                   as equip_desc,\n                    decode( ecd.condition,\n                            1, nvl(ep.price_base,nvl(ei.ei_unit_cost,0)),\n                            nvl(ep.price_used,nvl(ei.ei_unit_cost,0)) \n                          )                             as base_cost\n            from    monthly_extract_summary         sm,\n                    equip_merchant_tracking         emt,\n                    equip_inventory                 ei,\n                    equip_client_deployment         ecd,\n                    equip_price                     ep,\n                    equipment                       eq\n            where   sm.merchant_number =  :1  and\n                    sm.active_date between  :2  and  :3  and\n                    emt.merchant_number = sm.merchant_number and\n                    trunc(emt.action_date) between sm.month_begin_date and sm.month_end_date and\n                    emt.action in ( 1, 4 ) and  -- acct setup deployment, additional equip deployment\n                    emt.action_desc in ( 1, 2, 4 ) and  -- buy, rent, buy refurbished\n                    emt.cancel_date is null and\n                    ei.ei_serial_number = emt.ref_num_serial_num and\n                    ecd.serial_number = ei.ei_serial_number and          \n                    ecd.to_client = ei.ei_owner and  -- to the current owner\n                    trunc(ecd.move_date) between trunc(emt.action_date)-7 and trunc(emt.action_date) and\n                    ecd.from_client = 0 and -- from MES\n                    eq.equip_model = ei.ei_part_number and\n                    ep.rec_id = get_equip_price_rec_id(emt.merchant_number,ei.ei_part_number,trunc(ecd.move_date))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.ProfProcDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.ProfProcDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:431^11*/
        }
        else  // equipment is owned by MES
        {
          // load and process the purchases
          /*@lineinfo:generated-code*//*@lineinfo:436^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                      as active_date,
//                      (
//                        'Sales' || decode(ei.ei_class,1,':',' (Used):') ||
//                        nvl(eq.equip_descriptor,
//                          'P/N ' || ei.ei_part_number) ||
//                        ' (SN:' || ei.ei_serial_number ||
//                        ') deployed ' ||
//                        to_char(emt.action_date,'mm/dd/yyyy')
//                      )                                   as equip_desc,
//                      decode( ei.ei_class,
//                              1, nvl(ep.price_base,nvl(ei.ei_unit_cost,0)),
//                              nvl(ep.price_used,nvl(ei.ei_unit_cost,0)) 
//                             )                            as base_cost
//              from    monthly_extract_summary sm,
//                      equip_merchant_tracking emt,
//                      equip_inventory         ei,
//                      equipment               eq,
//                      equip_price             ep
//              where   sm.merchant_number = :merchantId and
//                      sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                      emt.merchant_number = sm.merchant_number and
//                      trunc(emt.action_date) between sm.month_begin_date and sm.month_end_date and
//                      emt.cancel_date is null and
//                      emt.action in (1,4) and -- acct setup deployment, additional equip deployment
//                      emt.action_desc in (1,4) and -- buy, buy refurbished
//                      ei.ei_serial_number = emt.ref_num_serial_num and
//                      eq.equip_model = ei.ei_part_number and
//                      ep.rec_id = get_equip_price_rec_id(emt.merchant_number,ei.ei_part_number,trunc(emt.action_date))
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                      as active_date,\n                    (\n                      'Sales' || decode(ei.ei_class,1,':',' (Used):') ||\n                      nvl(eq.equip_descriptor,\n                        'P/N ' || ei.ei_part_number) ||\n                      ' (SN:' || ei.ei_serial_number ||\n                      ') deployed ' ||\n                      to_char(emt.action_date,'mm/dd/yyyy')\n                    )                                   as equip_desc,\n                    decode( ei.ei_class,\n                            1, nvl(ep.price_base,nvl(ei.ei_unit_cost,0)),\n                            nvl(ep.price_used,nvl(ei.ei_unit_cost,0)) \n                           )                            as base_cost\n            from    monthly_extract_summary sm,\n                    equip_merchant_tracking emt,\n                    equip_inventory         ei,\n                    equipment               eq,\n                    equip_price             ep\n            where   sm.merchant_number =  :1  and\n                    sm.active_date between  :2  and  :3  and\n                    emt.merchant_number = sm.merchant_number and\n                    trunc(emt.action_date) between sm.month_begin_date and sm.month_end_date and\n                    emt.cancel_date is null and\n                    emt.action in (1,4) and -- acct setup deployment, additional equip deployment\n                    emt.action_desc in (1,4) and -- buy, buy refurbished\n                    ei.ei_serial_number = emt.ref_num_serial_num and\n                    eq.equip_model = ei.ei_part_number and\n                    ep.rec_id = get_equip_price_rec_id(emt.merchant_number,ei.ei_part_number,trunc(emt.action_date))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.ProfProcDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.ProfProcDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:466^11*/
        }          
      }
      else    // rentals (only MES owned inventory supports rentals)
      {
        if ( mesInventory == 1 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:473^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                      as active_date,
//                      (
//                        'Rental' || decode(ei.ei_class,1,':',' (Used):') ||
//                          nvl(eq.equip_descriptor,
//                          'P/N ' || ei.ei_part_number) ||
//                        ' (SN:' || ei.ei_serial_number ||
//                        ') - ' ||
//                        to_char(sm.active_date,'Mon yyyy')
//                      )                                   as equip_desc,
//                      decode(ei.ei_class,
//                             1, ep.PRICE_RENTAL,
//                             nvl(ep.price_rental_used,
//                                 ep.price_rental))        as base_cost
//              from    monthly_extract_summary sm,
//                      equip_merchant_tracking emt,          
//                      equip_inventory         ei,
//                      equipment               eq,
//                      equip_price             ep,
//                      mif                     mf
//              where   sm.merchant_number = :merchantId and
//                      sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                      emt.merchant_number = sm.merchant_number and
//                      emt.action in ( 1, 4 ) and -- acct setup deployment, additional equip deployment
//                      emt.action_desc = 2 and -- rental only
//                      trunc(emt.action_date) <= sm.month_end_date and
//                      emt.cancel_date is null and
//                      ei.ei_serial_number = emt.ref_num_serial_num and
//                      ei.ei_merchant_number = emt.merchant_number and
//                      ei.ei_lrb = emt.action_desc and -- rental only
//                      mf.merchant_number = sm.merchant_number and                  
//                      --mf.activation_date > (sm.month_end_date - (365*3)) and
//                      eq.equip_model = ei.ei_part_number and
//                      ep.rec_id = get_equip_price_rec_id(emt.merchant_number,ei.ei_part_number,trunc(emt.action_date))
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                      as active_date,\n                    (\n                      'Rental' || decode(ei.ei_class,1,':',' (Used):') ||\n                        nvl(eq.equip_descriptor,\n                        'P/N ' || ei.ei_part_number) ||\n                      ' (SN:' || ei.ei_serial_number ||\n                      ') - ' ||\n                      to_char(sm.active_date,'Mon yyyy')\n                    )                                   as equip_desc,\n                    decode(ei.ei_class,\n                           1, ep.PRICE_RENTAL,\n                           nvl(ep.price_rental_used,\n                               ep.price_rental))        as base_cost\n            from    monthly_extract_summary sm,\n                    equip_merchant_tracking emt,          \n                    equip_inventory         ei,\n                    equipment               eq,\n                    equip_price             ep,\n                    mif                     mf\n            where   sm.merchant_number =  :1  and\n                    sm.active_date between  :2  and  :3  and\n                    emt.merchant_number = sm.merchant_number and\n                    emt.action in ( 1, 4 ) and -- acct setup deployment, additional equip deployment\n                    emt.action_desc = 2 and -- rental only\n                    trunc(emt.action_date) <= sm.month_end_date and\n                    emt.cancel_date is null and\n                    ei.ei_serial_number = emt.ref_num_serial_num and\n                    ei.ei_merchant_number = emt.merchant_number and\n                    ei.ei_lrb = emt.action_desc and -- rental only\n                    mf.merchant_number = sm.merchant_number and                  \n                    --mf.activation_date > (sm.month_end_date - (365*3)) and\n                    eq.equip_model = ei.ei_part_number and\n                    ep.rec_id = get_equip_price_rec_id(emt.merchant_number,ei.ei_part_number,trunc(emt.action_date))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.ProfProcDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.ProfProcDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:508^11*/
        }          
      }        
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 1,
                                    resultSet.getDouble("base_cost") );
        record.setDescription( resultSet.getString("equip_desc") );
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                             contractType, record );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      ContractDataBean.logEntry("loadDetailMesEquipLegacy(" + merchantId + ")", e.toString());
    }      
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }
}/*@lineinfo:generated-code*/