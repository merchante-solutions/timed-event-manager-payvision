/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/Statement.java $

  Description:
  
    Statement
  
    Contains a merchant statement in the form of a collection of 
    statement pages.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 6/13/01 12:05p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

public class Statement implements Serializable
{
  private Vector      pages         = new Vector();
  private int         pageCount     = 0;
  
  /*
  ** public void addPage(StatementPage newPage)
  **
  ** Adds a new StatementPage into the pages hash table.
  */
  public void addPage(StatementPage newPage)
  {
    pages.add(newPage);
    ++pageCount;
  }
  
  /*
  ** public Page getPage(int pageNum)
  **
  ** Looks up the page with the given page number.
  **
  ** RETURNS: reference to the Page corresponding with pageNum if found,
  **          else null.
  */
  public StatementPage getPage(int pageNum)
  {
    return (StatementPage)pages.get(pageNum);
  }
  
  /*
  ** public String toString()
  **
  ** Generates a string containing the statement data suitable for displaying.
  **
  ** RETURNS: statement data in a String.
  */
  public String toString()
  {
    StringBuffer pageData = new StringBuffer();
    boolean isFirst = true;
    for (Iterator i = pages.iterator(); i.hasNext();)
    {
      pageData.append("" + i.next());
    }
    return pageData.toString();
  }
  
  /*
  ** ACCESSORS
  */
  public int getPageCount()
  {
    return pageCount;
  }
}