/*@lineinfo:filename=ImprinterFileDownloadBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ImprinterFileDownloadBean.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 4/18/03 12:14p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class ImprinterFileDownloadBean extends ReportSQLJBean
{
  private long appSeqNum = -1L;

  public class RowData
  {
    private final int   LINE_1              = 1;
    private final int   LINE_2              = 2;
    private final int   LINE_3              = 3;
    private final int   LINE_4              = 4;
    private final int   LINE_5              = 5;
    private final int   LINE_6              = 6;

    private final int   LINE_1_MAX_LENGTH   = 20;
    private final int   LINE_2_MAX_LENGTH   = 20;
    private final int   LINE_3_MAX_LENGTH   = 20;
    private final int   LINE_4_MAX_LENGTH   = 20;
    private final int   LINE_5_MAX_LENGTH   = 17;
    private final int   LINE_6_MAX_LENGTH   = 2;
    
    public String       line1               = null;
    public String       line2               = null;
    public String       line3               = null;
    public String       line4               = null;
    public String       line5               = null;
    public String       line6               = null;

    public RowData( ResultSet rs ) throws java.sql.SQLException
    {
      line1 = processData(rs.getString("line_1"), LINE_1_MAX_LENGTH, LINE_1);
      line2 = processData(rs.getString("line_2"), LINE_2_MAX_LENGTH, LINE_2);
      line3 = processData(rs.getString("line_3"), LINE_3_MAX_LENGTH, LINE_3);
      line4 = processData(rs.getString("line_4"), LINE_4_MAX_LENGTH, LINE_4);
      line5 = processData(rs.getString("line_5"), LINE_5_MAX_LENGTH, LINE_5);
      line6 = processData(rs.getString("line_6"), LINE_6_MAX_LENGTH, LINE_6);
    }

    private String processData(String data, int maxLength, int lineNum)
    {
      if(data == null)
      {
        return "";
      }

      String result = data.trim();

      switch(lineNum)
      {
        case LINE_1:
        case LINE_2:
        case LINE_3:
        case LINE_5:
        case LINE_6:
          if(result.length() > maxLength)
          {
            result = (result.substring(0,maxLength)).trim();
          }
        break;

        case LINE_4:
          //get characters after the first 20
          if(result.length() > maxLength)
          {
            result = (result.substring(maxLength)).trim();
          }
          else
          {
            result = "";
          }
          //makes sure characters after first 20 dont exceed 20
          if(result.length() > maxLength)
          {
            result = (result.substring(0,maxLength)).trim();
          }
        break;
      }

      return result.toUpperCase();
    }

  }
  
  public ImprinterFileDownloadBean( )
  {
  }
  
  protected void encodeLineFixed( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.line1 + "\n" );
    line.append( record.line2 + "\n" );
    line.append( record.line3 + "\n" );
    line.append( record.line4 + "\n" );
    line.append( record.line5 + "\n" );
    line.append( record.line6        );

  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append("imprinter_plate_file");
  
    return ( filename.toString() );
  }

  public String getDownloadFilenameExtension(int fileFormat)
  {
    return ".ato";
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_FIXED:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData()
  {
    loadData( ReportUserBean );
  }

  public void loadData( UserBean user )
  {
    ResultSetIterator             it                  = null;
    ResultSet                     resultSet           = null;

    try
    {
      // empty the current contents
      ReportRows.clear();

      /*@lineinfo:generated-code*//*@lineinfo:194^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.merch_number              line_1,
//                  decode(mdar.discover_merchant_num, null, decode(mpo.merchpo_card_merch_number, 0, '', mpo.merchpo_card_merch_number), mdar.discover_merchant_num)   line_2,
//                  merch.merch_business_name       line_3,
//                  merch.merch_business_name       line_4,
//                  addr.address_city               line_5,
//                  addr.countrystate_code          line_6
//  
//          from    merchant                        merch,
//                  merchpayoption                  mpo,
//                  merchant_discover_app_response  mdar,
//                  address                         addr
//  
//          where   merch.app_seq_num     = :appSeqNum             and
//                  merch.app_seq_num     = mdar.app_seq_num(+)    and
//                  (merch.app_seq_num    = mpo.app_seq_num(+)     and 
//                   14                   = mpo.cardtype_code(+))  and
//                  merch.app_seq_num     = addr.app_seq_num       and
//                  addr.addresstype_code = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.merch_number              line_1,\n                decode(mdar.discover_merchant_num, null, decode(mpo.merchpo_card_merch_number, 0, '', mpo.merchpo_card_merch_number), mdar.discover_merchant_num)   line_2,\n                merch.merch_business_name       line_3,\n                merch.merch_business_name       line_4,\n                addr.address_city               line_5,\n                addr.countrystate_code          line_6\n\n        from    merchant                        merch,\n                merchpayoption                  mpo,\n                merchant_discover_app_response  mdar,\n                address                         addr\n\n        where   merch.app_seq_num     =  :1              and\n                merch.app_seq_num     = mdar.app_seq_num(+)    and\n                (merch.app_seq_num    = mpo.app_seq_num(+)     and \n                 14                   = mpo.cardtype_code(+))  and\n                merch.app_seq_num     = addr.app_seq_num       and\n                addr.addresstype_code = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ImprinterFileDownloadBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ImprinterFileDownloadBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:214^7*/
      
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadData", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setAppSeqNum(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }

  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    setAppSeqNum(HttpHelper.getLong(request, "appSeqNum"));
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/