/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AssocChildProfSummary.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 11/02/01 2:51p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.util.Vector;

public class AssocChildProfSummary
{
  private   String          AssocName                   = "";
  private   long            AssocNumber                 = 0L;
  private   Vector          ChildDataList               = new Vector();
  private   int             ChildDataListIndex          = 0;
  

  public AssocChildProfSummary( long assocNumber, String assocName )
  {
    AssocNumber = assocNumber;
    AssocName   = assocName;
  }

  public void addChildData( ChildProfitabilitySummary childData )
  {
    ChildDataList.add( childData );
  }
  
  public ChildProfitabilitySummary first( )
  {
    ChildDataListIndex    = 0;
    return( next() );
  }
  
  public String getAssocName( )
  {
    return( AssocName );
  }
  
  public long getAssocNumber ( )
  {
    return( AssocNumber );
  }
  
  public ChildProfitabilitySummary next( )
  {
    ChildProfitabilitySummary       childSummary = null;
    
    try
    {
      childSummary = 
        (ChildProfitabilitySummary) ChildDataList.elementAt(ChildDataListIndex);
      ChildDataListIndex++;        
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( childSummary );
  }
  
  public double getGroupAmount( int contractDir, int betGroupId )
  {
    ChildProfitabilitySummary       childSummary  = first();
    double                          retVal        = 0.0;
    
    while( childSummary != null )
    {
      // treat the count as a generic all "fees" category
      if ( betGroupId == BankContractBean.BET_GROUP_COUNT )
      {
        retVal += childSummary.getFeesTotalAmount( contractDir );
      }
      else    // get just the specific groups total
      {
        retVal += childSummary.getGroupAmount( contractDir, betGroupId );
      }        
      childSummary = next();
    }
    return( retVal );
  }
  
  public double getGroupExpense( int betGroupId )
  {
    return( getGroupAmount( ContractTypes.CONTRACT_TYPE_EXPENSE, betGroupId ) );
  }
  
  public double getGroupIncome( int betGroupId )
  {
    return( getGroupAmount( ContractTypes.CONTRACT_TYPE_INCOME, betGroupId ) );
  }
  
  public void showData( java.io.PrintStream out )
  {
    ChildProfitabilitySummary     childData = first();
    
    out.println();
    out.println("************************************************");
    out.println("*  Association Profitability Data " );
    out.println("*    Assoc #          : " + AssocNumber );
    out.println("*    Assoc Name       : " + AssocName );
    out.println("*");
    
    while( childData != null )
    {
      out.println("*    Merchant Id      : " + childData.getHierarchyNode());
      out.println("*      Disc/Ic Inc    : " + childData.getDiscIcIncome());
      out.println("*      Disc/Ic Exp    : " + childData.getDiscIcExpense());
      out.println("*      Fees Inc       : " + childData.getFeesTotalIncome());
      out.println("*      Fees Exp       : " + childData.getFeesTotalExpense());
      childData = next();
    }
    out.println("************************************************");
  }
}
