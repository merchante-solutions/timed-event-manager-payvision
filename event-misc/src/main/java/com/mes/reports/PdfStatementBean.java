/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/PdfStatementBean.sqlj $

  Description:
  
  PdfStatementBean
  
  Generates a pdf document containing a merchant's monthly statement and
  writes it to a file.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 12/06/01 5:18p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.reports;

import java.io.File;
import java.io.FileOutputStream;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import com.mes.database.SQLJConnectionBase;

public class PdfStatementBean extends SQLJConnectionBase
{
  private String    merchNum;
  private long      recNum;
  private long      yearMonth;
  
  public PdfStatementBean(String merchNum, long recNum, long yearMonth)
  {
    this.merchNum   = merchNum;
    this.recNum     = recNum;
    this.yearMonth  = yearMonth;
  }
  
  
  public String buildPdf()
  {
    String pdfFilename = null;
    
    connect();
    
    StatementRecord rec = new StatementRecord();
    try
    {
      // load a statement record with the specified statement
      rec.connect();
      rec.getData(merchNum,recNum,yearMonth);

      // determine which logo gif to use
      Image logo = Image
        .getInstance("myserver/public_html/" + rec.getLogoFilename(merchNum));

      // make sure statements directory exists
      StringBuffer filename = 
        new StringBuffer("myserver/public_html/statements/" + rec.getBankNum());
      File pdfDir = new File(filename.toString());
      if (!pdfDir.exists())
      {
        pdfDir.mkdirs();
      }
      
      // set the filename
      filename.append("/" + yearMonth + "_" + rec.getMerchNum() + "_" 
        + rec.getRecNum() + ".pdf");
      File pdfFile = new File(filename.toString());
      
      // create a pdf document file if it doesn't already exist
      if (!pdfFile.exists())
      {
        FileOutputStream fos = 
          new FileOutputStream(pdfFile);
        Document doc = new Document(PageSize.A4,18,18,18,18);
        PdfWriter.getInstance(doc,fos);
        doc.open();
  
        // scan through the statement lines, adding them into the pdf document
        Statement stmt = rec.getStatement();
        for (int i = 0, pgCnt = stmt.getPageCount(); i < pgCnt; ++i)
        {
          doc.add(logo);
          StatementPage pg = stmt.getPage(i);
          for (int j = 0, lnCnt = pg.getLineCount(); j < lnCnt; ++j)
          {
            Paragraph para =
              new Paragraph(11,pg.getLine(j).toString(),new Font(Font.COURIER,9));
            para.setAlignment(Element.ALIGN_CENTER);
            doc.add(para);
          }

          doc.newPage();
        }
      
        // close the file
        doc.close();
        fos.flush();
        fos.close();
      }
      
      // set the return filename
      pdfFilename = "/statements/" + rec.getBankNum() + "/" + pdfFile.getName();
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        "::buildPdf "+ merchNum + ", " + yearMonth + " " + e.toString());
      logEntry("buildPdf" + merchNum + ", " + yearMonth,e.toString());
    }
    finally
    {
      rec.cleanUp();
      cleanUp();
    }
    
    return pdfFilename;
  }
  public static String buildPdf(String merchNum, long recNum, long yearMonth)
  {
    PdfStatementBean psb = new PdfStatementBean(merchNum,recNum,yearMonth);
    return psb.buildPdf();
  }
}
