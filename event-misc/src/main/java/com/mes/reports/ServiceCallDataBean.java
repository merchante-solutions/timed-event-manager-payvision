/*@lineinfo:filename=ServiceCallDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ServiceCallDataBean.sqlj $

  Description:  
  
  Extends the date range report bean to generate a sales activity report.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import sqlj.runtime.ResultSetIterator;

public class ServiceCallDataBean extends ReportSQLJBean
{
  public class ServiceCallSummaryRow
  {
    public String     orgName       = "";
    public long       orgNum        = 0L;
    public long       hierarchyNode = 0L;
    public int        callCount     = 0;
    
    public ServiceCallSummaryRow()
    {
    }
    
    public ServiceCallSummaryRow(ResultSet rs)
    {
      try
      {
        this.orgName        = rs.getString("org_name");
        this.orgNum         = rs.getLong("org_num");
        this.hierarchyNode  = rs.getLong("hierarchy_node");
        this.callCount      = rs.getInt("call_count");
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::constructor", e.toString());
      }
    }
    
    public String getOrgName()
    {
      return this.orgName;
    }
    public long getOrgNum()
    {
      return this.orgNum;
    }
    public long getHierarchyNode()
    {
      return this.hierarchyNode;
    }
    public int getCallCount()
    {
      return this.callCount;
    }
  }
  
  public class ServiceCallDetailRow
  {
    public String     callType      = "";
    public int        callCount     = 0;
    
    public ServiceCallDetailRow()
    {
    }
    
    public ServiceCallDetailRow(ResultSet rs)
    {
      try
      {
        this.callType = rs.getString("call_type");
        this.callCount = rs.getInt("call_count");
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::constructor", e.toString());
      }
    }
    
    public String getCallType()
    {
      return this.callType;
    }
    
    public int getCallCount()
    {
      return this.callCount;
    }
  }
  
  /**************************************************************************
  *
  * START OF MAIN CLASS (ServiceCallDataBean)
  *
  ***************************************************************************/
  public Vector     reportResults     = null;
  
  public ServiceCallDataBean()
  {
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator     it      = null;
    try
    {
      reportResults = new Vector();
      
      /*@lineinfo:generated-code*//*@lineinfo:133^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    o.org_num                   org_num,
//                    o.org_name                  org_name,
//                    o.org_group                 hierarchy_node,
//                    count(sc.merchant_number)    call_count
//          from      service_calls sc,
//                    organization o,
//                    group_merchant gm,
//                    group_rep_merchant grm
//          where     trunc(sc.call_date) between :beginDate and :endDate and
//                    nvl(sc.hidden, 'N') != 'Y' and
//                    sc.merchant_number = gm.merchant_number and
//                    gm.org_num = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    o.org_num in (select org_num 
//                                  from parent_org 
//                                  where parent_org_num = :orgId)
//          group by  o.org_group,
//                    o.org_num,
//                    o.org_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    o.org_num                   org_num,\n                  o.org_name                  org_name,\n                  o.org_group                 hierarchy_node,\n                  count(sc.merchant_number)    call_count\n        from      service_calls sc,\n                  organization o,\n                  group_merchant gm,\n                  group_rep_merchant grm\n        where     trunc(sc.call_date) between  :1  and  :2  and\n                  nvl(sc.hidden, 'N') != 'Y' and\n                  sc.merchant_number = gm.merchant_number and\n                  gm.org_num = o.org_num and\n                  grm.user_id(+) =  :3  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :4  = -1 ) and        \n                  o.org_num in (select org_num \n                                from parent_org \n                                where parent_org_num =  :5 )\n        group by  o.org_group,\n                  o.org_num,\n                  o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ServiceCallDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setLong(5,orgId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ServiceCallDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:156^7*/
      
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {
        reportResults.add(new ServiceCallSummaryRow(rs));
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }
  }
  
  public Vector getCallDetails(long detailOrgNum)
  {
    ResultSetIterator it          = null;
    Vector            callDetails = new Vector();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:180^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(sc.merchant_number)  call_count,
//                  sct.description            call_type
//          from    service_calls             sc,
//                  service_call_types        sct
//          where   trunc(sc.call_date) between :ReportDateBegin and :ReportDateEnd and
//                  nvl(sc.hidden, 'N') != 'Y' and
//                  sc.type = sct.type and
//                  sc.merchant_number in
//                  (
//                    select merchant_number
//                    from   group_merchant
//                    where  org_num = :detailOrgNum
//                  ) and
//                  (
//                    :AppFilterUserId = -1 or
//                    sc.merchant_number in
//                    (
//                      select  merchant_number
//                      from    group_rep_merchant
//                      where   user_id = :AppFilterUserId
//                    )
//                  )
//                    
//          group by  sct.description
//          order by  call_count desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(sc.merchant_number)  call_count,\n                sct.description            call_type\n        from    service_calls             sc,\n                service_call_types        sct\n        where   trunc(sc.call_date) between  :1  and  :2  and\n                nvl(sc.hidden, 'N') != 'Y' and\n                sc.type = sct.type and\n                sc.merchant_number in\n                (\n                  select merchant_number\n                  from   group_merchant\n                  where  org_num =  :3 \n                ) and\n                (\n                   :4  = -1 or\n                  sc.merchant_number in\n                  (\n                    select  merchant_number\n                    from    group_rep_merchant\n                    where   user_id =  :5 \n                  )\n                )\n                  \n        group by  sct.description\n        order by  call_count desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ServiceCallDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ReportDateBegin);
   __sJT_st.setDate(2,ReportDateEnd);
   __sJT_st.setLong(3,detailOrgNum);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setLong(5,AppFilterUserId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.ServiceCallDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:207^7*/
      
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {
        callDetails.add(new ServiceCallDetailRow(rs));
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getCallDetails(" + detailOrgNum + ")", e.toString());
    }
    
    return callDetails;
  }
  
  public Vector getReportResults()
  {
    return this.reportResults;
  }
  
  public void setProperties(HttpServletRequest request)
  {
    super.setProperties(request);
    
    Calendar cal = Calendar.getInstance();
    
    if ( usingDefaultReportDates() )
    {
      // setup the default date range
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, -30 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
}/*@lineinfo:generated-code*/