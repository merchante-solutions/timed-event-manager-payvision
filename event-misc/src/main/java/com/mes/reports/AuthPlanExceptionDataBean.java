/*@lineinfo:filename=AuthPlanExceptionDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AuthPlanExceptionDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 6/04/02 5:33p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class AuthPlanExceptionDataBean extends ReportSQLJBean
{
        
  public class AuthPlanExceptionRecord
  {
    public  Date          ActiveDate;
    public  double        AppPerItemRate;
    public  long          HierarchyNode;
    public  int           ItemCount;
    public  long          MerchantId;
    public  String        MerchantName;
    public  String        MerchantType;
    public  double        PerItemRate;
    public  String        PlanDescription;
    
    AuthPlanExceptionRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      HierarchyNode     = resultSet.getLong("assoc_number");
      MerchantId        = resultSet.getLong("merchant_number");
      MerchantName      = resultSet.getString("dba_name");
      MerchantType      = resultSet.getString("merchant_type");
      ActiveDate        = resultSet.getDate("active_date");
      PlanDescription   = mesConstants.getVitalExtractCardTypeDesc( resultSet.getString("plan_type") );
      ItemCount         = resultSet.getInt("item_count");
      PerItemRate       = resultSet.getDouble("per_item");
      AppPerItemRate    = getAppPerItem( resultSet.getLong("app_seq_num"),
                                         resultSet.getInt ("card_type_code") );
    }
    
    public String getOnlineAppPerItem()
    {
      String      retVal    = "n/a";
      
      if ( AppPerItemRate != -1.0 )
      {
        retVal = MesMath.toCurrency(AppPerItemRate);
      }
      
      return( retVal );
    }
  }        
  
  private int             ReportRowsIndex = 0;
  
  public AuthPlanExceptionDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    line.append("\"Association\",");
    line.append("\"Merchant Number\",");
    line.append("\"DBA Name\",");
    line.append("\"Active Date\",");
    line.append("\"Plan Description\",");
    line.append("\"Item Count\",");
    line.append("\"Per Item (OLA)\",");
    line.append("\"Merchant Type\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    AuthPlanExceptionRecord record = (AuthPlanExceptionRecord)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
  
    line.append( encodeHierarchyNode(record.HierarchyNode) );
    line.append( "," );
    line.append( encodeHierarchyNode(record.MerchantId) );
    line.append( ",\"" );
    line.append( record.MerchantName );
    line.append( "\"," );
    line.append( record.ActiveDate );
    line.append( ",\"" );
    line.append( record.PlanDescription );
    line.append( "\"," );
    line.append( record.ItemCount );
    line.append( "," );
    line.append( ((record.AppPerItemRate == -1.0) ? "" : Double.toString(record.AppPerItemRate)) );
    line.append( ",\"" );
    line.append( record.MerchantType );
    line.append( "\"" );
  }
  
  public double getAppPerItem( long appSeq, int cardTypeCode )
  {
    double            perItem       = -1.0;
    
    try
    {
      if ( cardTypeCode > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:137^9*/

//  ************************************************************
//  #sql [Ctx] { select nvl(tc.tranchrg_per_tran,-1) 
//            from   tranchrg               tc
//            where  tc.app_seq_num = :appSeq and
//                   tc.cardtype_code = :cardTypeCode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl(tc.tranchrg_per_tran,-1)  \n          from   tranchrg               tc\n          where  tc.app_seq_num =  :1  and\n                 tc.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AuthPlanExceptionDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeq);
   __sJT_st.setInt(2,cardTypeCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   perItem = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^9*/
      }        
    }
    catch( java.sql.SQLException e )
    {
      // ignore "cannot select null into..." exception
    }
    return( perItem );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    int                     fromMonth = 0;
    int                     fromYear  = 0;
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMMyyyy");
    StringBuffer            filename  = new StringBuffer("3941_auth_exceptions_");
    
    // setup to compare the month
    // and year from the two dates
    cal.setTime( ReportDateBegin );
    fromMonth = cal.get(Calendar.MONTH);
    fromYear  = cal.get(Calendar.YEAR);
    cal.setTime( ReportDateEnd );
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if( ( cal.get( Calendar.MONTH ) != fromMonth ) ||
        ( cal.get( Calendar.YEAR  ) != fromYear ) )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean       retVal    = false;
    
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
        
      default:
        break;
    }
    return( retVal );
  }

  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    long                        discoverNode    = 0L;
    ResultSetIterator           it              = null;
    ResultSet                   resultSet       = null;
    
    try
    {
      ReportRows.removeAllElements();
      
      discoverNode = MesDefaults.getLong(MesDefaults.DK_DISCOVER_TOP_NODE);
              
      
      /*@lineinfo:generated-code*//*@lineinfo:208^7*/

//  ************************************************************
//  #sql [Ctx] it = { select ( gn.hh_bank_number || gn.G1_ASSOCIATION_NUMBER )  as assoc_number,
//                 gn.hh_merchant_number                              as merchant_number,
//                 gn.g1_dba_name                                     as dba_name,
//                 gn.hh_active_date                                  as active_date,
//                 ap.a1_plan_type                                    as plan_type,
//                 decode(ap.a1_plan_type,
//                        :mesConstants.VITAL_ME_AP_DISC,:mesConstants.APP_CT_DISCOVER,             -- 'DS',14,
//                        :mesConstants.VITAL_ME_AP_AMEX,:mesConstants.APP_CT_AMEX,                 -- 'AM',16,
//                        :mesConstants.VITAL_ME_AP_DINERS,:mesConstants.APP_CT_DINERS_CLUB,        -- 'DC',10,
//                        :mesConstants.VITAL_ME_AP_CARTE_BLANCHE,:mesConstants.APP_CT_DINERS_CLUB, -- 'CB',10,
//                        :mesConstants.VITAL_ME_AP_JCB,:mesConstants.APP_CT_JCB,                   -- 'JC',15,
//                        0)                                          as card_type_code,
//                 ap.auth_count_total                                as item_count,
//                 ( ap.auth_income_total / ap.auth_count_total )     as per_item,               
//                 ap.auth_income_total                               as income,
//                 to_char(g.group_2)                                 as agent_bank_node,
//                 nvl(mr.app_seq_num,0)                              as app_seq_num,
//                 decode(substr(gn.g1_user_data3,3,2),
//                        'DT','Terminal',
//                        'DP','DialPay',
//                        ('Stage Only (' || substr(gn.g1_user_data3,3,2) || ')') )   as merchant_type
//          from   monthly_extract_ap     ap, 
//                 monthly_extract_gn     gn,
//                 merchant               mr,
//                 groups                 g,
//                 tranchrg               tc
//          where  gn.hh_bank_number = :mesConstants.BANK_ID_MES and -- 3941 and
//                 gn.hh_active_date between :beginDate and last_day( :endDate ) and 
//                 g.assoc_number = (gn.hh_bank_number || gn.g1_association_number) and
//                 ap.HH_LOAD_SEC = gn.HH_LOAD_SEC and
//                 not ap.a1_plan_type in ( :mesConstants.VITAL_ME_AP_VISA,
//                                          :mesConstants.VITAL_ME_AP_MC,
//                                          :mesConstants.VITAL_ME_AP_DEBIT ) and -- 'VS','MC','DB' ) and
//                 ap.auth_count_total > 0 and
//                 ap.auth_income_total = 0 and
//                 mr.merch_number(+)   = gn.hh_merchant_number and
//                 tc.app_seq_num(+) = mr.app_seq_num and
//                 tc.cardtype_code(+) = :mesConstants.APP_CT_DISCOVER and -- 14 and
//                 ( ap.a1_plan_type <> :mesConstants.VITAL_ME_AP_DISC or -- 'DS' or
//                   g.group_2 <> :discoverNode or 
//                   ( g.group_2 = :discoverNode and
//                     nvl(tc.tranchrg_per_tran,-1) <> 0 and
//                     ap.a1_plan_type = :mesConstants.VITAL_ME_AP_DISC ) ) -- 'DS' ) )
//          order by gn.g1_association_number, 
//                   gn.hh_active_date, gn.g1_dba_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select ( gn.hh_bank_number || gn.G1_ASSOCIATION_NUMBER )  as assoc_number,\n               gn.hh_merchant_number                              as merchant_number,\n               gn.g1_dba_name                                     as dba_name,\n               gn.hh_active_date                                  as active_date,\n               ap.a1_plan_type                                    as plan_type,\n               decode(ap.a1_plan_type,\n                       :1 , :2 ,             -- 'DS',14,\n                       :3 , :4 ,                 -- 'AM',16,\n                       :5 , :6 ,        -- 'DC',10,\n                       :7 , :8 , -- 'CB',10,\n                       :9 , :10 ,                   -- 'JC',15,\n                      0)                                          as card_type_code,\n               ap.auth_count_total                                as item_count,\n               ( ap.auth_income_total / ap.auth_count_total )     as per_item,               \n               ap.auth_income_total                               as income,\n               to_char(g.group_2)                                 as agent_bank_node,\n               nvl(mr.app_seq_num,0)                              as app_seq_num,\n               decode(substr(gn.g1_user_data3,3,2),\n                      'DT','Terminal',\n                      'DP','DialPay',\n                      ('Stage Only (' || substr(gn.g1_user_data3,3,2) || ')') )   as merchant_type\n        from   monthly_extract_ap     ap, \n               monthly_extract_gn     gn,\n               merchant               mr,\n               groups                 g,\n               tranchrg               tc\n        where  gn.hh_bank_number =  :11  and -- 3941 and\n               gn.hh_active_date between  :12  and last_day(  :13  ) and \n               g.assoc_number = (gn.hh_bank_number || gn.g1_association_number) and\n               ap.HH_LOAD_SEC = gn.HH_LOAD_SEC and\n               not ap.a1_plan_type in (  :14 ,\n                                         :15 ,\n                                         :16  ) and -- 'VS','MC','DB' ) and\n               ap.auth_count_total > 0 and\n               ap.auth_income_total = 0 and\n               mr.merch_number(+)   = gn.hh_merchant_number and\n               tc.app_seq_num(+) = mr.app_seq_num and\n               tc.cardtype_code(+) =  :17  and -- 14 and\n               ( ap.a1_plan_type <>  :18  or -- 'DS' or\n                 g.group_2 <>  :19  or \n                 ( g.group_2 =  :20  and\n                   nvl(tc.tranchrg_per_tran,-1) <> 0 and\n                   ap.a1_plan_type =  :21  ) ) -- 'DS' ) )\n        order by gn.g1_association_number, \n                 gn.hh_active_date, gn.g1_dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AuthPlanExceptionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mesConstants.VITAL_ME_AP_DISC);
   __sJT_st.setInt(2,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setString(3,mesConstants.VITAL_ME_AP_AMEX);
   __sJT_st.setInt(4,mesConstants.APP_CT_AMEX);
   __sJT_st.setString(5,mesConstants.VITAL_ME_AP_DINERS);
   __sJT_st.setInt(6,mesConstants.APP_CT_DINERS_CLUB);
   __sJT_st.setString(7,mesConstants.VITAL_ME_AP_CARTE_BLANCHE);
   __sJT_st.setInt(8,mesConstants.APP_CT_DINERS_CLUB);
   __sJT_st.setString(9,mesConstants.VITAL_ME_AP_JCB);
   __sJT_st.setInt(10,mesConstants.APP_CT_JCB);
   __sJT_st.setInt(11,mesConstants.BANK_ID_MES);
   __sJT_st.setDate(12,beginDate);
   __sJT_st.setDate(13,endDate);
   __sJT_st.setString(14,mesConstants.VITAL_ME_AP_VISA);
   __sJT_st.setString(15,mesConstants.VITAL_ME_AP_MC);
   __sJT_st.setString(16,mesConstants.VITAL_ME_AP_DEBIT);
   __sJT_st.setInt(17,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setString(18,mesConstants.VITAL_ME_AP_DISC);
   __sJT_st.setLong(19,discoverNode);
   __sJT_st.setLong(20,discoverNode);
   __sJT_st.setString(21,mesConstants.VITAL_ME_AP_DISC);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AuthPlanExceptionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new AuthPlanExceptionRecord( resultSet ) );
      }
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadData(" + orgId + "," + beginDate + "," + endDate + ")",
               e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    super.setProperties(request);
    
    if( usingDefaultReportDates() )
    {
      Calendar      cal = Calendar.getInstance();
      
      cal.add(Calendar.MONTH,-1);
      cal.set(Calendar.DAY_OF_MONTH,1);
      
      ReportDateBegin = new java.sql.Date( cal.getTime().getTime() );
      ReportDateEnd   = new java.sql.Date( cal.getTime().getTime() );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
    AuthPlanExceptionRecord       record;
    
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      record = (AuthPlanExceptionRecord)ReportRows.elementAt(i);
      //@ TODO
    }
  }
}/*@lineinfo:generated-code*/