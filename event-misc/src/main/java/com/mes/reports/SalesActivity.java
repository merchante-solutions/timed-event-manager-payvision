/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/SalesActivity.java $

  Description:  
  
  Extends the date range report bean to generate a sales activity report.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 8/06/03 3:29p $
  Version            : $Revision: 12 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.ops.QueueConstants;

public class SalesActivity extends DateRangeReportBean
{
  private long      userId        = 0;
  private long      hid           = -1L;
  private int       hierType      = 0;
  private int       appType       = 0;
  private int       creditStatus  = 0;
  private String    nodeName      = null;
  
  /*
  ** CONSTRUCTOR public SalesActivity()
  */
  public SalesActivity()
  {
  }
  
  public class ReportRow
  {
    private BeanDate  submitDate              = new BeanDate();
    private BeanDate  activateDate            = new BeanDate();
    
    private String    merchName;
    private String    repName;
    private String    association;
    private String    statName;
    private String    gridName;
    
    private long      merchNum;
    private long      appSeqNum;
    private long      userId;
    private long      revenueInBasisPoints;
    private long      managerId;
    private long      repId;
    
    private int       entityType;
    private int       gridType;
    private int       planType;
    private int       appStatus;
    private int       appType;
    
    private double    monthlyVMCSales;
    private double    annualSales;
    private double    averageTicket;
    private double    discRate;
    private double    centsPerTran;
    private double    centsPerTran2;
    private double    centsPerAuth;
    private double    centsPerCapture;
    private double    interchangeRate;
    private double    interchangeFee;
    private double    assocAssessFee;
    private double    commissionPaid;
    private double    amexFee;
    private double    discFee;
    private double    annualNetDiscount;
    private double    equipRentals;
    private double    equipPurchases;
    private double    miscMonthlyFees;
    private double    oneTimeFees;
    
    private boolean   isActive;
    private boolean   showRate;
    private boolean   showCentsPerTran;
    private boolean   priorStatements;
    private boolean   amexAccepted;
    private boolean   amexSplitDial;
    private boolean   discAccepted;
    private boolean   hasInternetFee;

    public ReportRow(ResultSet rs)
    {
      int prog = 0;
      try
      {
        managerId             =   rs.getLong  ("direct_child");
        entityType            =   rs.getInt   ("entity_type");
        submitDate.setSqlDate    (rs.getDate  ("app_created_date"));
        merchName             =   rs.getString("merch_business_name");        
        merchNum              =   rs.getLong  ("merch_number");               
        monthlyVMCSales       =   rs.getDouble("merch_month_visa_mc_sales");  
        averageTicket         =   rs.getDouble("merch_average_cc_tran");      
        gridType              =   rs.getInt   ("grid_type");                  
        appStatus             =   rs.getInt   ("merch_credit_status");        
        isActive              =  (rs.getDate  ("date_activated") != null);    
        planType              =   rs.getInt   ("tranchrg_discrate_type");
        discRate              =   rs.getDouble("tranchrg_disc_rate");         
        centsPerTran          =   rs.getDouble("tranchrg_pass_thru");         
        repId                 =   rs.getLong  ("app_user_id");
        repName               =   rs.getString("name");
        interchangeRate       =   rs.getDouble("interchange_rate");           
        assocAssessFee        =   rs.getDouble("assoc_assess_fee");           
        interchangeFee        =   rs.getDouble("interchange_fee");           
        appSeqNum             =   rs.getLong  ("app_seq_num");                
        appType               =   rs.getInt   ("app_type");                   
        centsPerTran2         =   rs.getDouble("per_tran");                   
        centsPerAuth          =   rs.getDouble("tranchrg_per_auth");          
        centsPerCapture       =   rs.getDouble("tranchrg_per_capture");       
        equipRentals          =   rs.getDouble("rental_total");               
        equipPurchases        =   rs.getDouble("purchase_total");
        miscMonthlyFees       =   rs.getDouble("misc_monthly_fees");
        oneTimeFees           =   rs.getDouble("one_time_fees");
        priorStatements       =  (rs.getString("merch_prior_statements").equals("Y"));
        userId                =   rs.getLong  ("app_user_id");                
        commissionPaid        =   rs.getDouble("pay_total");                  
        association           =   rs.getString("dmagent");                    
        amexAccepted          =  (rs.getLong  ("amex_asn") != 0L);            
        try { amexSplitDial   =  (rs.getString("split_dial").equals("Y")); }
        catch (Exception e)   {   amexSplitDial = false; }                    
        discAccepted          =  (rs.getLong  ("disc_asn") != 0L);            
        amexFee               =   rs.getDouble("amex_charges");               
        discFee               =   rs.getDouble("disc_charges");               
        try { hasInternetFee  =  (rs.getDouble("i_fee") > 0); }
        catch (Exception e)   {   hasInternetFee = false; }                   
        if (isActive)
        {
          activateDate.setSqlDate(rs.getDate("date_activated"));
        }
      
        // for now, just total all possible cents per tran fees into one
        centsPerTran   += centsPerTran2 + centsPerAuth + centsPerCapture;
      
        // calculate some fields
        annualSales = monthlyVMCSales * 12;
        switch (planType)
        {
          // variable rate
          case mesConstants.APP_PS_VARIABLE_RATE:
            // not sure how to handle this
            revenueInBasisPoints = 0L;
            break;
        
          // fixed rate, no cents per tran
          case mesConstants.APP_PS_FIXED_RATE:
          // fixed rate and cents per tran
          case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
          // rate (ignoring non-qual rate)
          case mesConstants.CBT_APP_BUNDLED_RATE_PLAN:
          // rate + (per item + per auth)
          case mesConstants.CBT_APP_UNBUNDLED_RATE_PLAN:
          // new transcom...
          case mesConstants.TRANS_APP_BUCKET_ONLY_PLAN:
          case mesConstants.TRANS_APP_BUCKET_WITH_PERITEM_PLAN:
          case mesConstants.TRANS_APP_DETAILED_STATEMENT_PLAN:
            revenueInBasisPoints = (long)
              ((((discRate / 100) + (centsPerTran / averageTicket)) * 10000) - 
               ((((interchangeRate + assocAssessFee) / 100) + (interchangeFee / averageTicket)) * 10000) 
               + 0.5);
            break;
               
          // cents per tran (interchange passed through so 
          // tran cost component is cancelled out)
          case mesConstants.APP_PS_INTERCHANGE:
          // per item + per auth + per capture
          case mesConstants.CBT_APP_INTERCHANGE_PLUS_PLAN:
            revenueInBasisPoints = (long)
              (((centsPerTran / averageTicket) * 10000) + 0.5);
            break;
        }

        annualNetDiscount = (averageTicket * (((double)revenueInBasisPoints) / 10000)) 
            * (annualSales / averageTicket) + 0.005;
        annualNetDiscount = (double) ((long)(annualNetDiscount * 100)) / 100;
      
        String gridNames[] = 
        {
          "",
          "RTL",
          "MOTO",
          "HTL",
          "SPMK",
          "INT",
          "EM",
          "HTL",
          "RTL",
          "MOTO",
          "RST"
        };
        gridName = "---";
        if (gridType < gridNames.length && gridType > 0)
        {
          gridName = gridNames[gridType];
        }

        statName = "";
        switch (appStatus)
        {
          default:
            statName = "???";
            break;
        
          case QueueConstants.CREDIT_NEW:
            statName = "NEW";
            break;
        
          case QueueConstants.CREDIT_APPROVE:
            statName = "APRV";
            break;
        
          case QueueConstants.CREDIT_DECLINE:
            statName = "DECL";
            break;
        
          case QueueConstants.CREDIT_PEND:
            statName = "PEND";
            break;
        
          case QueueConstants.CREDIT_CANCEL:
            statName = "CAN";
            break;
        }
      }
      catch (Exception e)
      {
        addError("ReportRow constructor (prog=" + prog + "): " + e.toString());
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
                 "ReportRow constructor (prog=" + prog + "): " + e.toString());
      }
    }
    
    // accessors
    public String getSubmitDate()
    {
      try
      {
        return submitDate.toString();
      }
      catch (Exception e) {}
      
      return "---";
    }
    public String getActivateDate()
    {
      try
      {
        return activateDate.toString();
      }
      catch (Exception e) {}
      
      return "---";
    }
    
    public Long getManagerId()
    {
      return managerId;
    }
    
    public String   getMerchName()            { return merchName;}
    public String   getRepName()              { return repName; }
    public String   getAssociation()          { return association; }
    public String   getStatName()             { return statName; }
    public String   getGridName()             { return gridName; }
    
    public long     getMerchNum()             { return merchNum; }
    public long     getAppSeqNum()            { return appSeqNum; }
    public long     getUserId()               { return userId; }
    public long     getRevenueInBasisPoints() { return revenueInBasisPoints; }
    
    public int      getEntityType()           { return entityType; }
    public int      getGridType()             { return gridType; }
    public int      getPlanType()             { return planType; }
    public int      getAppStatus()            { return appStatus; }
    public int      getAppType()              { return appType; }
    
    public double   getMonthlyVMCSales()      { return monthlyVMCSales; }
    public double   getAnnualSales()          { return annualSales; }
    public double   getAverageTicket()        { return averageTicket; }
    public double   getDiscRate()             { return discRate; }
    public double   getCentsPerTran()         { return centsPerTran; }
    public double   getCentsPerTran2()        { return centsPerTran2; }
    public double   getCentsPerAuth()         { return centsPerAuth; }
    public double   getCentsPerCapture()      { return centsPerCapture; }
    public double   getInterchangeRate()      { return interchangeRate; }
    public double   getInterchangeFee()       { return interchangeFee; }
    public double   getAssocAssessFee()       { return assocAssessFee; }
    public double   getCommissionPaid()       { return commissionPaid; }
    public double   getAmexFee()              { return amexFee; }
    public double   getDiscFee()              { return discFee; }
    public double   getAnnualNetDiscount()    { return annualNetDiscount; }
    public double   getEquipRentals()         { return equipRentals; }
    public double   getEquipPurchases()       { return equipPurchases; }
    public double   getMiscMonthlyFees()      { return miscMonthlyFees; }
    public double   getOneTimeFees()          { return oneTimeFees; }
    
    public boolean  getIsActive()             { return isActive; }
    public boolean  getShowRate()             { return showRate; }
    public boolean  getShowCentsPerTran()     { return showCentsPerTran; }
    public boolean  getPriorStatements()      { return priorStatements; }
    public boolean  getAmexAccepted()         { return amexAccepted; }
    public boolean  getAmexSplitDial()        { return amexSplitDial; }
    public boolean  getDiscAccepted()         { return discAccepted; }
    public boolean  getHasInternetFee()       { return hasInternetFee; }
  }
  
  /*
  ** METHOD public ResultSet executeQuery()
  **
  ** Query's database for sales activity data.
  **
  ** RETURNS: the query result set.
  */
  public ResultSet executeQuery()
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    
    try
    {
      qs.append("select ");
      qs.append("  h1.ancestor direct_child, ");
      qs.append("  h2.entity_type, ");
      qs.append("  a.app_seq_num, ");
      qs.append("  a.app_created_date, ");
      qs.append("  a.app_user_id, ");
      qs.append("  m.merch_business_name, ");
      qs.append("  m.merch_number, ");
      qs.append("  m.merch_month_visa_mc_sales, ");
      qs.append("  m.merch_average_cc_tran, ");
      qs.append("  i.grid_type, ");
      qs.append("  m.merch_credit_status, ");
      qs.append("  m.date_activated, ");
      qs.append("  t.tranchrg_discrate_type, ");
      qs.append("  t.tranchrg_disc_rate, ");
      qs.append("  t.tranchrg_pass_thru, ");
      qs.append("  u.name, ");
      qs.append("  i.interchange_rate, ");
      qs.append("  i.assoc_assess_fee, ");
      qs.append("  i.interchange_fee, ");
      qs.append("  a.app_type, ");
      qs.append("  (t.tranchrg_per_tran + f.charges) per_tran, ");
      qs.append("  t.tranchrg_per_auth, ");
      qs.append("  t.tranchrg_per_capture, ");
      qs.append("  ers.rental_total, ");
      qs.append("  eps.purchase_total, ");
      qs.append("  m.merch_prior_statements, ");
      qs.append("  a.app_user_id, ");
      qs.append("  rc.pay_total, ");
      qs.append("  mif.dmagent, ");
      qs.append("  aa.app_seq_num amex_asn, ");
      qs.append("  aa.split_dial, ");
      qs.append("  da.app_seq_num disc_asn, ");
      qs.append("  amex.charges amex_charges, ");
      qs.append("  disc.charges disc_charges, ");
      qs.append("  otsf.fees one_time_fees, ");
      qs.append("  mmf.fees misc_monthly_fees, ");
      qs.append("  it.charge i_fee ");
      qs.append("from ");
      qs.append("  t_hierarchy h1, ");
      qs.append("  t_hierarchy h2, ");
      qs.append("  users u, ");
      qs.append("  application a, ");
      qs.append("  merchant m, ");
      qs.append("  tranchrg t, ");
      qs.append("  rep_commissions rc, ");
      qs.append("  mif, ");
      qs.append("  amex_fee_per_tran amex, ");
      qs.append("  disc_fee_per_tran disc, ");
      qs.append("  equip_rental_summaries ers, ");
      qs.append("  equip_purchase_summaries eps, ");
      qs.append("  additional_per_tran_fees f, ");
      qs.append("  amex_accepted aa, ");
      qs.append("  disc_accepted da, ");
      qs.append("  internet_per_tran_fees it, ");
      qs.append("  one_time_setup_fees otsf, ");
      qs.append("  misc_monthly_fees mmf, ");
      qs.append("  interchange_cost i ");
      qs.append("where ");
      qs.append("  h1.entity_type = 3 ");
      //qs.append("  and h1.hier_type = ? ");
      //qs.append("  and h2.hier_type = ? ");
      qs.append("  and (h1.descendent = m.merch_number or h1.descendent = m.app_seq_num) ");
      qs.append("  and h1.ancestor = h2.descendent ");
      qs.append("  and h2.ancestor = ? ");
      qs.append("  and h2.relation = 1 ");
      qs.append("  and instr(lower(m.merch_business_name),'test') = 0 ");
      qs.append("  and m.merch_number = mif.merchant_number(+) ");
      qs.append("  and ( m.pricing_grid = i.grid_type ");
      qs.append("        or ( ( m.pricing_grid is null or m.pricing_grid = 0 )  ");
      qs.append("             and ( ( ( m.industype_code = 6 or m.industype_code = 7 or m.loctype_code = 2 ) and i.grid_type = 2 )  ");
      qs.append("                   or ( ( m.industype_code <> 6 and m.industype_code <> 7 and m.loctype_code <> 2 ) and i.grid_type = 1 ) ) ) ) ");
      qs.append("  and m.app_seq_num = a.app_seq_num ");
      qs.append("  and a.app_type in ( 0, 9, 2, 7 ) ");
      qs.append("  and a.app_seq_num = t.app_seq_num ");
      qs.append("  and t.cardtype_code = 1 ");
      qs.append("  and a.app_seq_num = ers.app_seq_num(+)  ");
      qs.append("  and a.app_seq_num = eps.app_seq_num(+) ");
      qs.append("  and a.app_seq_num = f.app_seq_num(+) ");
      qs.append("  and a.app_seq_num = rc.app_seq_num(+) ");
      qs.append("  and a.app_seq_num = aa.app_seq_num(+) ");
      qs.append("  and a.app_seq_num = da.app_seq_num(+) ");
      qs.append("  and a.app_seq_num = amex.app_seq_num(+) ");
      qs.append("  and a.app_seq_num = disc.app_seq_num(+) ");
      qs.append("  and a.app_seq_num = it.app_seq_num(+) ");
      qs.append("  and a.app_seq_num = otsf.app_seq_num(+) ");
      qs.append("  and a.app_seq_num = mmf.app_seq_num(+) ");
      qs.append("  and a.app_user_id = u.user_id (+) ");
      qs.append("  and trunc(a.app_created_date) between ? and ? ");
      if (creditStatus > 0)
      {
        qs.append("  and m.merch_credit_status = " + creditStatus + " ");
      }
      else
      {
        qs.append("  and m.merch_credit_status <> 4 ");
      }
      qs.append("order by u.name, a.app_created_date ");
      
      ps = getPreparedStatement(qs.toString());
      //ps.setInt (1,hierType);
      //ps.setInt (2,hierType);
      ps.setLong(1,hid);
      //ps.setInt (4,appType);
      ps.setDate(2,fromDate.getSqlDate());
      ps.setDate(3,toDate.getSqlDate());
      
      rs = ps.executeQuery();
    }
    catch (Exception e)
    {
      addError("executeQuery: " + e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
               "executeQuery: " + e.toString());
    }
    
    return rs;
  }
  
  public class SubReport extends Vector
  {
    String managerName;
    public SubReport(String managerName)
    {
      this.managerName = managerName;
    }
    public String getManagerName()
    {
      return managerName;
    }
  }
  
  public Hashtable getReportData()
  {
    Hashtable         reportData  = null;
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    ResultSet         rs          = null;

    int prog = 0;
    try
    {
      // if hid is not specified load the default based on the user
      if (hid == -1L)
      {
        prog = 1;
        qs.append("select ");
        qs.append("  hier_id ");
        qs.append("from ");
        qs.append("  t_hierarchy_defaults ");
        qs.append("where ");
        qs.append("  hier_type = " + hierType + " ");
        qs.append("  and user_id = " + userId);

        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();
        if (rs.next())
        {
          prog = 2;
          hid = rs.getLong("hier_id");
        }
        // if none specified for this user, 
        // get the root node of this hierarchy type
        else
        {
          prog = 3;
          qs.setLength(0);
          qs.append("select ");
          qs.append("  root_id ");
          qs.append("from ");
          qs.append("  t_hierarchy_types ");
          qs.append("where ");
          qs.append("  hier_type = " + hierType);

          ps = getPreparedStatement(qs.toString());
          rs = ps.executeQuery();
          if (rs.next())
          {
            prog = 4;
            hid = rs.getLong("root_id");
          }
          else
          {
            throw new Exception(
              "Unable to determine default hierarchy node for type " + hierType);
          }
        }
      }

      prog = 5;
      
      // get direct children of node, including itself
      qs.setLength(0);
      qs.append("select ");
      qs.append("  h.descendent, ");
      qs.append("  hn.name ");
      qs.append("from ");
      qs.append("  t_hierarchy h, ");
      qs.append("  t_hierarchy_names hn ");
      qs.append("where ");
      qs.append("  h.ancestor = " + hid + " ");
      //qs.append("  and h.hier_type = " + hierType + " ");
      qs.append("  and h.relation < 2 ");
      qs.append("  and h.entity_type <> 3 ");
      qs.append("  and h.descendent = hn.hier_id ");
      qs.append("order by ");
      qs.append("  h.relation desc");
      
      ps = getPreparedStatement(qs.toString());
      rs = ps.executeQuery();
      
      // populate the report data vector with 
      // sub reports for each direct child
      if (rs != null)
      {
        prog = 6;
        reportData = new Hashtable();
        Long dmId = -1L;
        while (rs.next())
        {
          Long managerId = rs.getLong("descendent");
          if (managerId.longValue() == hid)
          {
            managerId = dmId;
          }
          reportData.put(managerId,new SubReport(rs.getString("name")));
        }
        
        prog = 7;
        
        // get all activity and load it into the sub reports
        rs = executeQuery();
        if (rs != null)
        {
          prog = 8;
          while (rs.next())
          {
            ReportRow row = new ReportRow(rs);
            SubReport subReport = null;
            if (row.getEntityType() == 3)
            {
              subReport = (SubReport)reportData.get(dmId);
            }
            else
            {
              subReport = (SubReport)reportData.get(row.getManagerId());
            }
            subReport.add(row);
          }
        }
      }
    }
    catch (Exception e)
    {
      String desc = "getReportData (" + prog + "): " + e.toString();
      System.out.println(desc);
      addError(desc);
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), desc);
    }
    
    return reportData;
  }
  
  /*
  ** METHOD public void setProperties(HttpServletRequest request)
  **
  ** Called by the download servlet to load internal members of bean prior
  ** to a call to executeQuery().
  */
  public void setProperties(HttpServletRequest request)
  {
    try
    {
      setFromDay      (Integer.parseInt(request.getParameter("fromDay")));
      setFromMonth    (Integer.parseInt(request.getParameter("fromMonth")));
      setFromYear     (Integer.parseInt(request.getParameter("fromYear")));
      setToDay        (Integer.parseInt(request.getParameter("toDay")));
      setToMonth      (Integer.parseInt(request.getParameter("toMonth")));
      setToYear       (Integer.parseInt(request.getParameter("toYear")));
      setCreditStatus (Integer.parseInt(request.getParameter("creditStatus")));
    }
    catch (Exception e)
    {
    }
  }
  
  /*
  ** METHOD public String getDownloadFilenameBase()
  **
  ** Called by the download servlet to determine the download filename
  ** (without the extension).  Returns a file name in the format of
  ** "SalesActivity<fromDate>to<toDate>".
  **
  ** RETURNS: download filename base.
  */
  public String getDownloadFilenameBase()
  {
    return "SalesActivitity" + fromDate.toString() + "to" + toDate.toString();
  }
  
  /*
  ** ACCESSORS
  */
  public long getHid()
  {
    return hid;
  }
  public void setHid(long hid)
  {
    this.hid = hid;
  }
  
  public int getHierType()
  {
    return hierType;
  }
  public void setHierType(int hierType)
  {
    this.hierType = hierType;
  }
  
  public int getAppType()
  {
    return appType;
  }
  public void setAppType(int appType)
  {
    this.appType = appType;
  }
  
  public int getCreditStatus()
  {
    return creditStatus;
  }
  public void setCreditStatus(int creditStatus)
  {
    this.creditStatus = creditStatus;
  }
  
  public long getUserId()
  {
    return userId;
  }
  public void setUserId(long userId)
  {
    this.userId = userId;
  }
  
  public String getNodeName()
  {
    if (nodeName == null)
    {
      try
      {
        StringBuffer      qs = new StringBuffer("");
        PreparedStatement ps = null;
        ResultSet         rs = null;

        qs.append("select ");
        qs.append("  name ");
        qs.append("from ");
        qs.append("  t_hierarchy_names ");
        qs.append("where ");
        qs.append("  hier_id = " + hid + " ");
        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();
        if (rs.next())
        {
          nodeName = rs.getString("name");
        }
        else
        {
          nodeName = "unknown";
        }
      }
      catch (Exception e)
      {
        String desc = "getNodeName (hid=" + hid + ") " + e.toString();
        System.out.println(desc);
        addError(desc);
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(),desc);
      }
    }
    
    return nodeName;
  }
  
  private long parentHid = -1L;
  public long getParentHid()
  {
    if (parentHid == -1L)
    {
      try
      {
        StringBuffer      qs = new StringBuffer("");
        PreparedStatement ps = null;
        ResultSet         rs = null;

        // look for an ancestor relationship between user's
        // default (top allowed) node and the current hid
        qs.append("select ");
        qs.append("  count(*) ");
        qs.append("from ");
        qs.append("  t_hierarchy h, ");
        qs.append("  t_hierarchy_defaults hd ");
        qs.append("where ");
        qs.append("  hd.user_id = " + userId + " ");
        qs.append("  and hd.hier_id = h.ancestor ");
        qs.append("  and h.descendent = " + hid + " ");
        qs.append("  and h.relation > 0");
        
        ps = getPreparedStatement(qs.toString());
        rs = ps.executeQuery();
        boolean uplinkOk = false;
        if (rs.next())
        {
          uplinkOk = (rs.getInt(1) > 0);
        }
      
        // if user's highest accessible node is above this node
        // then look up this node's parent
        if (uplinkOk)
        {
          qs.setLength(0);
          qs.append("select ");
          qs.append("  ancestor ");
          qs.append("from ");
          qs.append("  t_hierarchy ");
          qs.append("where ");
          qs.append("  descendent = " + hid + " ");
          qs.append("  and relation = 1");
          ps = getPreparedStatement(qs.toString());
          rs = ps.executeQuery();
          if (rs.next())
          {
            parentHid = rs.getLong("ancestor");
          }
        }
      }
      catch (Exception e)
      {
        String desc = "getParentHid (hid=" + hid + ") " + e.toString();
        System.out.println(desc);
        addError(desc);
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(),desc);
      }
    }
    
    return parentHid;
  }
}
