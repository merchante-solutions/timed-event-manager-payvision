/*@lineinfo:filename=StatementRecord*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/StatementRecord.sqlj $

  Description:

    StatementRecord

    Allows storage and retrieval of statement records.

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.ListIterator;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import org.apache.log4j.Logger;
// XML libraries
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXContentHandler;
import org.dom4j.io.SAXEventRecorder;
import com.mes.database.SQLJConnectionBase;
import oracle.sql.BLOB;

public class StatementRecord extends GenericRecord
{
  static Logger log = Logger.getLogger(StatementRecord.class);

  public static final int       LINES_PER_PAGE      = 59;

  public static class XmlField
  {
    public  String      ColumnHeader  = null;
    public  String      ElementName   = null;
    public  String      HtmlAlignment = null;

    public XmlField( String elementName, String colHeader, String htmlAlign )
    {
      ElementName   = elementName;
      HtmlAlignment = htmlAlign;
      ColumnHeader  = colHeader;
    }
  }

  public static final XmlField[] DepositSummaryFields =
  {
    new XmlField( "DAY",            "Day",              "left"  ),
    new XmlField( "REF_NUMBER",     "Reference Number", "left"  ),
    new XmlField( "TRAN_TYPE",      "Tran Code",        "left"  ),
    new XmlField( "PLAN_TYPE",      "Plan Code",        "left"  ),
    new XmlField( "NUM_SALES",      "# Sales",          "right" ),
    new XmlField( "DOLLAR_SALES",   "$ Sales",          "right" ),
    new XmlField( "DOLLAR_CREDITS", "$ Credits",        "right" ),
    new XmlField( "DISC_PAID",      "Discount Paid",    "right" ),
    new XmlField( "NET_DEPOSIT",    "Net Deposit",      "right" ),
  };

  public static final XmlField[] DepositSummaryFields3941 =
  {
    new XmlField( "DAY",            "Day",              "left"  ),
    new XmlField( "REF_NUMBER",     "Reference Number", "left"  ),
    new XmlField( "TRAN_TYPE",      "Tran Code",        "left"  ),
    new XmlField( "PLAN_TYPE",      "Plan Code",        "left"  ),
    new XmlField( "NUM_SALES",      "# Sales",          "right" ),
    new XmlField( "DOLLAR_SALES",   "$ Sales",          "right" ),
    new XmlField( "DOLLAR_CREDITS", "$ Credits",        "right" ),
    new XmlField( "DISC_PAID",      "Discount Paid",    "right" ),
    new XmlField( "SETTLED",        "Net Deposit",      "right" ),
  };

  public static final XmlField[] ChargebackFields =
  {
    new XmlField( "DAY",            "Day",              "left" ),
    new XmlField( "REF_NUMBER",     "Reference Number", "left" ),
    new XmlField( "TRAN_TYPE",      "Tran Code",        "left" ),
    new XmlField( "PLAN_TYPE",      "Plan Code",        "left" ),
    new XmlField( "NUM_SALES",      "# Sales",          "right" ),
    new XmlField( "DOLLAR_SALES",   "$ Sales",          "right" ),
    new XmlField( "DOLLAR_CREDITS", "$ Credits",        "right" ),
    new XmlField( "DISC_PAID",      "Discount Paid",    "right" ),
    new XmlField( "NET_DEPOSIT",    "Net Deposit",      "right" ),
  };

  public static final XmlField[] ChargebackFields3941 =
  {
    new XmlField( "DAY",            "Day",              "left" ),
    new XmlField( "REF_NUMBER",     "Reference Number", "left" ),
    new XmlField( "TRAN_TYPE",      "Tran Code",        "left" ),
    new XmlField( "PLAN_TYPE",      "Plan Code",        "left" ),
    new XmlField( "NUM_SALES",      "# Sales",          "right" ),
    new XmlField( "DOLLAR_SALES",   "$ Sales",          "right" ),
    new XmlField( "DOLLAR_CREDITS", "$ Credits",        "right" ),
    new XmlField( "DISC_PAID",      "Discount Paid",    "right" ),
    new XmlField( "SETTLED",        "Net Deposit",      "right" ),
  };

  public static final XmlField[] AdjustmentFields =
  {
    new XmlField( "DAY",            "Day",              "left" ),
    new XmlField( "REF_NUMBER",     "Reference Number", "left" ),
    new XmlField( "TRAN_TYPE",      "Tran Code",        "left" ),
    new XmlField( "PLAN_TYPE",      "Plan Code",        "left" ),
    new XmlField( "NUM_SALES",      "# Sales",          "right"),
    new XmlField( "DOLLAR_SALES",   "$ Sales",          "right"),
    new XmlField( "DOLLAR_CREDITS", "$ Credits",        "right"),
    new XmlField( "DISC_PAID",      "Discount Paid",    "right"),
    new XmlField( "NET_DEPOSIT",    "Net Deposit",      "right"),
  };

  public static final XmlField[] AdjustmentFields3941 =
  {
    new XmlField( "DAY",            "Day",              "left" ),
    new XmlField( "REF_NUMBER",     "Reference Number", "left" ),
    new XmlField( "TRAN_TYPE",      "Tran Code",        "left" ),
    new XmlField( "PLAN_TYPE",      "Plan Code",        "left" ),
    new XmlField( "NUM_SALES",      "# Sales",          "right"),
    new XmlField( "DOLLAR_SALES",   "$ Sales",          "right"),
    new XmlField( "DOLLAR_CREDITS", "$ Credits",        "right"),
    new XmlField( "DISC_PAID",      "Discount Paid",    "right"),
    new XmlField( "SETTLED",        "Net Deposit",      "right"),
  };

  public static final XmlField[] FeeSummaryFields =
  {
    new XmlField( "NUM",          "Number",       "right" ),
    new XmlField( "AMT",          "Amount",       "right" ),
    new XmlField( "DESCRIPTION",  "Description",  "left" ),
    new XmlField( "TOTAL",        "Total",        "right" ),
  };

  public static final XmlField[] PlanSummaryFields =
  {
    new XmlField( "PLAN_CODE",        "Plan Code",      "left" ),
    new XmlField( "NUM_SALES",        "# Sales",        "right" ),
    new XmlField( "DOLLAR_SALES",     "$ Sales",        "right" ),
    new XmlField( "NUM_CREDITS",      "# Credits",      "right" ),
    new XmlField( "DOLLAR_CREDITS",   "$ Credits",      "right" ),
    new XmlField( "NET_SALES",        "Net Sales",      "right" ),
    new XmlField( "AVG_TKT",          "Average Ticket", "right" ),
    new XmlField( "DISC_PI",          "Disc P/I",       "right" ),
    new XmlField( "PERCENT",          "Disc %",         "right" ),
    new XmlField( "DISCOUNT_DUE",     "Discount Due",   "right" ),
  };

  public static final String[] XmlSectionNames =
  {
    "//PLAN_SUMMARY/PLAN_SUMMARY_TRAN",
    "//TRANSACTION_SECTION/DEPOSIT_TRAN",
    "//TRANSACTION_SECTION/ADJUSTMENT_TRAN",
    "//TRANSACTION_SECTION/CHARGEBACK_TRAN",
    "//FEES_SECTION/FEE_TRAN",
  };

  public static final String[] XmlSectionTitles =
  {
    "Plan Summary",
    "Deposits",
    "Adjustments",
    "Chargebacks",
    "Fees",
  };

  protected static String[] XmlColumnHeaders =
  {
    " PL # SALES        $ SALES # CREDITS    $ CREDITS     NET SALES  AVG TKT DISC P/I  %   DISCOUNT DUE ",
    " DAY   REF NUMBER    *  PL  # SALES       $ SALES       $ CREDITS     DISCOUNT PD           SETTLED ",
    " DAY   REF NUMBER    *  PL  # SALES       $ SALES       $ CREDITS     DISCOUNT PD           SETTLED ",
    " DAY   REF NUMBER    *  PL  # SALES       $ SALES       $ CREDITS     DISCOUNT PD           SETTLED ",
    "   NUMBER           AMOUNT    DESCRIPTION                                                     TOTAL ",
  };

  protected static final int[][] XmlStaticOffsets =
  {
    {3,11,26,36,49,63,72,79,86,99},
    {3,-7,22,25,35,49,65,81,99},
    {3,-7,22,25,35,49,65,81,99},
    {3,-7,22,25,35,49,65,81,99},
    {9,26,-31,99},
  };

  public static final XmlField[][] XmlSectionFieldsOthers =
  {
    PlanSummaryFields,
    DepositSummaryFields,
    AdjustmentFields,
    ChargebackFields,
    FeeSummaryFields,
  };

  public static final XmlField[][] XmlSectionFields3941 =
  {
    PlanSummaryFields,
    DepositSummaryFields3941,
    AdjustmentFields3941,
    ChargebackFields3941,
    FeeSummaryFields,
  };


  private byte[]            statementData;
  private Vector            cardDetails;
  Document                  StatementDocument = null;
  private int               LineCount;
  private int               PageCount;
  private StatementPage     Page;

  // public variables to deal with hacks
  public int  bankNumber  = 0;
  public XmlField[][] XmlSectionFields;

  /*
  ** CONSTRUCTORS
  */
  public StatementRecord()
  {
  }

  public int getBankNumber()
  {
    return( bankNumber );
  }

  /*
  ** public boolean submitData()
  **
  ** Stores the contents of the current record in a database.
  **
  ** RETURNS: true if submit successful, else false.
  */
  public boolean submitData()
  {
    boolean submitOk = false;

    try
    {
      connect();

      // can't auto commit since we need a lock on the record
      // to do blob writing
      setAutoCommit(false);

      // card details
      int     sViCnt = 0;   int     sMcCnt = 0;   int     sDiCnt = 0;
      double  sViAmt = 0;   double  sMcAmt = 0;   double  sDiAmt = 0;
      int     cViCnt = 0;   int     cMcCnt = 0;   int     cDiCnt = 0;
      double  cViAmt = 0;   double  cMcAmt = 0;   double  cDiAmt = 0;
      double  dViAmt = 0;   double  dMcAmt = 0;   double  dDiAmt = 0;

      int     sAxCnt = 0;   int     sJcCnt = 0;   int     sDbCnt = 0;
      double  sAxAmt = 0;   double  sJcAmt = 0;   double  sDbAmt = 0;
      int     cAxCnt = 0;   int     cJcCnt = 0;   int     cDbCnt = 0;
      double  cAxAmt = 0;   double  cJcAmt = 0;   double  cDbAmt = 0;
      double  dAxAmt = 0;   double  dJcAmt = 0;   double  dDbAmt = 0;

      int     sDsCnt = 0;
      double  sDsAmt = 0;
      int     cDsCnt = 0;
      double  cDsAmt = 0;
      double  dDsAmt = 0;

      if (cardDetails != null)
      {
        for (int i = 0; i < cardDetails.size(); ++i)
        {
          StatementCardDetails cd = (StatementCardDetails)cardDetails.get(i);
          switch (cd.getCardType())
          {
            case StatementCardDetails.CT_VISA:
              sViCnt += cd.getSalesCnt();
              sViAmt += cd.getSalesAmt();
              cViCnt += cd.getCreditCnt();
              cViAmt += cd.getCreditAmt();
              dViAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_MC:
              sMcCnt += cd.getSalesCnt();
              sMcAmt += cd.getSalesAmt();
              cMcCnt += cd.getCreditCnt();
              cMcAmt += cd.getCreditAmt();
              dMcAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_DINERS:
              sDiCnt += cd.getSalesCnt();
              sDiAmt += cd.getSalesAmt();
              cDiCnt += cd.getCreditCnt();
              cDiAmt += cd.getCreditAmt();
              dDiAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_DISCOVER:
              sDsCnt += cd.getSalesCnt();
              sDsAmt += cd.getSalesAmt();
              cDsCnt += cd.getCreditCnt();
              cDsAmt += cd.getCreditAmt();
              dDsAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_AMEX:
              sAxCnt += cd.getSalesCnt();
              sAxAmt += cd.getSalesAmt();
              cAxCnt += cd.getCreditCnt();
              cAxAmt += cd.getCreditAmt();
              dAxAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_JCB:
              sJcCnt += cd.getSalesCnt();
              sJcAmt += cd.getSalesAmt();
              cJcCnt += cd.getCreditCnt();
              cJcAmt += cd.getCreditAmt();
              dJcAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_DEBIT:
              sDbCnt += cd.getSalesCnt();
              sDbAmt += cd.getSalesAmt();
              cDbCnt += cd.getCreditCnt();
              cDbAmt += cd.getCreditAmt();
              dDbAmt += cd.getDiscAmt();
              break;
          }
        }
      }

      // insert the column data along with an empty blob
      /*@lineinfo:generated-code*//*@lineinfo:352^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_statements
//          (
//            merch_num,
//            rec_num,
//            mystery_id,
//            year_month,
//            dda,
//            transit_routing,
//            load_file_name,
//            bank_num,
//            discount,
//            fees,
//  
//            visa_sales_cnt,
//            visa_sales_amt,
//            visa_credit_cnt,
//            visa_credit_amt,
//            visa_disc_amt,
//  
//            mc_sales_cnt,
//            mc_sales_amt,
//            mc_credit_cnt,
//            mc_credit_amt,
//            mc_disc_amt,
//  
//            dinr_sales_cnt,
//            dinr_sales_amt,
//            dinr_credit_cnt,
//            dinr_credit_amt,
//            dinr_disc_amt,
//  
//            disc_sales_cnt,
//            disc_sales_amt,
//            disc_credit_cnt,
//            disc_credit_amt,
//            disc_disc_amt,
//  
//            amex_sales_cnt,
//            amex_sales_amt,
//            amex_credit_cnt,
//            amex_credit_amt,
//            amex_disc_amt,
//  
//            jcb_sales_cnt,
//            jcb_sales_amt,
//            jcb_credit_cnt,
//            jcb_credit_amt,
//            jcb_disc_amt,
//  
//            debit_sales_cnt,
//            debit_sales_amt,
//            debit_credit_cnt,
//            debit_credit_amt,
//            debit_disc_amt,
//  
//            statement_data
//          )
//          values
//          (
//            :merchNum,
//            :recNum,
//            :mystId,
//            :yearMonth,
//            :dda,
//            :trNum,
//            :loadFilename,
//            :bankNum,
//            :discount,
//            :fees,
//  
//            :sViCnt,
//            :sViAmt,
//            :cViCnt,
//            :cViAmt,
//            :dViAmt,
//  
//            :sMcCnt,
//            :sMcAmt,
//            :cMcCnt,
//            :cMcAmt,
//            :dMcAmt,
//  
//            :sDiCnt,
//            :sDiAmt,
//            :cDiCnt,
//            :cDiAmt,
//            :dDiAmt,
//  
//            :sDsCnt,
//            :sDsAmt,
//            :cDsCnt,
//            :cDsAmt,
//            :dDsAmt,
//  
//            :sAxCnt,
//            :sAxAmt,
//            :cAxCnt,
//            :cAxAmt,
//            :dAxAmt,
//  
//            :sJcCnt,
//            :sJcAmt,
//            :cJcCnt,
//            :cJcAmt,
//            :dJcAmt,
//  
//            :sDbCnt,
//            :sDbAmt,
//            :cDbCnt,
//            :cDbAmt,
//            :dDbAmt,
//  
//            empty_blob()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_statements\n        (\n          merch_num,\n          rec_num,\n          mystery_id,\n          year_month,\n          dda,\n          transit_routing,\n          load_file_name,\n          bank_num,\n          discount,\n          fees,\n\n          visa_sales_cnt,\n          visa_sales_amt,\n          visa_credit_cnt,\n          visa_credit_amt,\n          visa_disc_amt,\n\n          mc_sales_cnt,\n          mc_sales_amt,\n          mc_credit_cnt,\n          mc_credit_amt,\n          mc_disc_amt,\n\n          dinr_sales_cnt,\n          dinr_sales_amt,\n          dinr_credit_cnt,\n          dinr_credit_amt,\n          dinr_disc_amt,\n\n          disc_sales_cnt,\n          disc_sales_amt,\n          disc_credit_cnt,\n          disc_credit_amt,\n          disc_disc_amt,\n\n          amex_sales_cnt,\n          amex_sales_amt,\n          amex_credit_cnt,\n          amex_credit_amt,\n          amex_disc_amt,\n\n          jcb_sales_cnt,\n          jcb_sales_amt,\n          jcb_credit_cnt,\n          jcb_credit_amt,\n          jcb_disc_amt,\n\n          debit_sales_cnt,\n          debit_sales_amt,\n          debit_credit_cnt,\n          debit_credit_amt,\n          debit_disc_amt,\n\n          statement_data\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n\n           :21 ,\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 ,\n           :30 ,\n\n           :31 ,\n           :32 ,\n           :33 ,\n           :34 ,\n           :35 ,\n\n           :36 ,\n           :37 ,\n           :38 ,\n           :39 ,\n           :40 ,\n\n           :41 ,\n           :42 ,\n           :43 ,\n           :44 ,\n           :45 ,\n\n          empty_blob()\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.reports.StatementRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setLong(2,recNum);
   __sJT_st.setLong(3,mystId);
   __sJT_st.setLong(4,yearMonth);
   __sJT_st.setString(5,dda);
   __sJT_st.setString(6,trNum);
   __sJT_st.setString(7,loadFilename);
   __sJT_st.setInt(8,bankNum);
   __sJT_st.setDouble(9,discount);
   __sJT_st.setDouble(10,fees);
   __sJT_st.setInt(11,sViCnt);
   __sJT_st.setDouble(12,sViAmt);
   __sJT_st.setInt(13,cViCnt);
   __sJT_st.setDouble(14,cViAmt);
   __sJT_st.setDouble(15,dViAmt);
   __sJT_st.setInt(16,sMcCnt);
   __sJT_st.setDouble(17,sMcAmt);
   __sJT_st.setInt(18,cMcCnt);
   __sJT_st.setDouble(19,cMcAmt);
   __sJT_st.setDouble(20,dMcAmt);
   __sJT_st.setInt(21,sDiCnt);
   __sJT_st.setDouble(22,sDiAmt);
   __sJT_st.setInt(23,cDiCnt);
   __sJT_st.setDouble(24,cDiAmt);
   __sJT_st.setDouble(25,dDiAmt);
   __sJT_st.setInt(26,sDsCnt);
   __sJT_st.setDouble(27,sDsAmt);
   __sJT_st.setInt(28,cDsCnt);
   __sJT_st.setDouble(29,cDsAmt);
   __sJT_st.setDouble(30,dDsAmt);
   __sJT_st.setInt(31,sAxCnt);
   __sJT_st.setDouble(32,sAxAmt);
   __sJT_st.setInt(33,cAxCnt);
   __sJT_st.setDouble(34,cAxAmt);
   __sJT_st.setDouble(35,dAxAmt);
   __sJT_st.setInt(36,sJcCnt);
   __sJT_st.setDouble(37,sJcAmt);
   __sJT_st.setInt(38,cJcCnt);
   __sJT_st.setDouble(39,cJcAmt);
   __sJT_st.setDouble(40,dJcAmt);
   __sJT_st.setInt(41,sDbCnt);
   __sJT_st.setDouble(42,sDbAmt);
   __sJT_st.setInt(43,cDbCnt);
   __sJT_st.setDouble(44,cDbAmt);
   __sJT_st.setDouble(45,dDbAmt);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:468^7*/

      // get a stream handler to the blob
      BLOB b;
      /*@lineinfo:generated-code*//*@lineinfo:472^7*/

//  ************************************************************
//  #sql [Ctx] { select  statement_data 
//          from    merch_statements
//          where   merch_num  = :merchNum and
//                  year_month = :yearMonth and
//                  rec_num    = :recNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  statement_data  \n        from    merch_statements\n        where   merch_num  =  :1  and\n                year_month =  :2  and\n                rec_num    =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.StatementRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setLong(2,yearMonth);
   __sJT_st.setLong(3,recNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:479^7*/
      //deprecated
      //OutputStream bOut = b.getBinaryOutputStream();
      //Use, as of 9.0.2:
      OutputStream bOut = b.setBinaryStream(0L);

      // write the statement data to the blob
      bOut.write(statementData,0,statementData.length);
      bOut.flush();
      bOut.close();

      // commit
      /*@lineinfo:generated-code*//*@lineinfo:491^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:494^7*/

      submitOk = true;
    }
    catch (Exception e)
    {
      if (!silent)
      {
        System.out.println(this.getClass().getName() +
          "::submitData " + e.toString() + " " + loadFilename + ": " +
          merchNum + ", " + recNum);
      }
      logEntry("submitData",e.toString() +
        "::submitData " + e.toString() + " " + loadFilename + ": " +
        merchNum + ", " + recNum);
    }
    finally
    {
      cleanUp();
    }

    return submitOk;
  }
  public boolean submitData(Statement statement)
  {
    setStatement(statement);
    return submitData();
  }

  // hack to allow existing statements to be modified
  public boolean updateData(Object statement)
  {

    boolean updateOk = false;

    try
    {

      // set statement data do that given
      setStatement((Statement)statement);

      connect();

      // can't auto commit since we need a lock on the record
      // to do blob writing
      setAutoCommit(false);

      // card details
      int     sViCnt = 0;   int     sMcCnt = 0;   int     sDiCnt = 0;
      double  sViAmt = 0;   double  sMcAmt = 0;   double  sDiAmt = 0;
      int     cViCnt = 0;   int     cMcCnt = 0;   int     cDiCnt = 0;
      double  cViAmt = 0;   double  cMcAmt = 0;   double  cDiAmt = 0;
      double  dViAmt = 0;   double  dMcAmt = 0;   double  dDiAmt = 0;

      int     sAxCnt = 0;   int     sJcCnt = 0;   int     sDbCnt = 0;
      double  sAxAmt = 0;   double  sJcAmt = 0;   double  sDbAmt = 0;
      int     cAxCnt = 0;   int     cJcCnt = 0;   int     cDbCnt = 0;
      double  cAxAmt = 0;   double  cJcAmt = 0;   double  cDbAmt = 0;
      double  dAxAmt = 0;   double  dJcAmt = 0;   double  dDbAmt = 0;

      int     sDsCnt = 0;
      double  sDsAmt = 0;
      int     cDsCnt = 0;
      double  cDsAmt = 0;
      double  dDsAmt = 0;

      if (cardDetails != null)
      {
        for (int i = 0; i < cardDetails.size(); ++i)
        {
          StatementCardDetails cd = (StatementCardDetails)cardDetails.get(i);
          switch (cd.getCardType())
          {
            case StatementCardDetails.CT_VISA:
              sViCnt += cd.getSalesCnt();
              sViAmt += cd.getSalesAmt();
              cViCnt += cd.getCreditCnt();
              cViAmt += cd.getCreditAmt();
              dViAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_MC:
              sMcCnt += cd.getSalesCnt();
              sMcAmt += cd.getSalesAmt();
              cMcCnt += cd.getCreditCnt();
              cMcAmt += cd.getCreditAmt();
              dMcAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_DINERS:
              sDiCnt += cd.getSalesCnt();
              sDiAmt += cd.getSalesAmt();
              cDiCnt += cd.getCreditCnt();
              cDiAmt += cd.getCreditAmt();
              dDiAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_DISCOVER:
              sDsCnt += cd.getSalesCnt();
              sDsAmt += cd.getSalesAmt();
              cDsCnt += cd.getCreditCnt();
              cDsAmt += cd.getCreditAmt();
              dDsAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_AMEX:
              sAxCnt += cd.getSalesCnt();
              sAxAmt += cd.getSalesAmt();
              cAxCnt += cd.getCreditCnt();
              cAxAmt += cd.getCreditAmt();
              dAxAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_JCB:
              sJcCnt += cd.getSalesCnt();
              sJcAmt += cd.getSalesAmt();
              cJcCnt += cd.getCreditCnt();
              cJcAmt += cd.getCreditAmt();
              dJcAmt += cd.getDiscAmt();
              break;

            case StatementCardDetails.CT_DEBIT:
              sDbCnt += cd.getSalesCnt();
              sDbAmt += cd.getSalesAmt();
              cDbCnt += cd.getCreditCnt();
              cDbAmt += cd.getCreditAmt();
              dDbAmt += cd.getDiscAmt();
              break;
          }
        }
      }

      // delete existing record if present
      /*@lineinfo:generated-code*//*@lineinfo:627^7*/

//  ************************************************************
//  #sql [Ctx] { delete from merch_statements
//          where   merch_num = :merchNum
//                  and year_month = :yearMonth
//                  and rec_num = :recNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from merch_statements\n        where   merch_num =  :1 \n                and year_month =  :2 \n                and rec_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.reports.StatementRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setLong(2,yearMonth);
   __sJT_st.setLong(3,recNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:633^7*/

      // insert the column data along with an empty blob
      /*@lineinfo:generated-code*//*@lineinfo:636^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_statements
//          (
//            merch_num,
//            rec_num,
//            mystery_id,
//            year_month,
//            dda,
//            transit_routing,
//            load_file_name,
//            bank_num,
//            discount,
//            fees,
//  
//            visa_sales_cnt,
//            visa_sales_amt,
//            visa_credit_cnt,
//            visa_credit_amt,
//            visa_disc_amt,
//  
//            mc_sales_cnt,
//            mc_sales_amt,
//            mc_credit_cnt,
//            mc_credit_amt,
//            mc_disc_amt,
//  
//            dinr_sales_cnt,
//            dinr_sales_amt,
//            dinr_credit_cnt,
//            dinr_credit_amt,
//            dinr_disc_amt,
//  
//            disc_sales_cnt,
//            disc_sales_amt,
//            disc_credit_cnt,
//            disc_credit_amt,
//            disc_disc_amt,
//  
//            amex_sales_cnt,
//            amex_sales_amt,
//            amex_credit_cnt,
//            amex_credit_amt,
//            amex_disc_amt,
//  
//            jcb_sales_cnt,
//            jcb_sales_amt,
//            jcb_credit_cnt,
//            jcb_credit_amt,
//            jcb_disc_amt,
//  
//            debit_sales_cnt,
//            debit_sales_amt,
//            debit_credit_cnt,
//            debit_credit_amt,
//            debit_disc_amt,
//  
//            statement_data
//          )
//          values
//          (
//            :merchNum,
//            :recNum,
//            :mystId,
//            :yearMonth,
//            :dda,
//            :trNum,
//            :loadFilename,
//            :bankNum,
//            :discount,
//            :fees,
//  
//            :sViCnt,
//            :sViAmt,
//            :cViCnt,
//            :cViAmt,
//            :dViAmt,
//  
//            :sMcCnt,
//            :sMcAmt,
//            :cMcCnt,
//            :cMcAmt,
//            :dMcAmt,
//  
//            :sDiCnt,
//            :sDiAmt,
//            :cDiCnt,
//            :cDiAmt,
//            :dDiAmt,
//  
//            :sDsCnt,
//            :sDsAmt,
//            :cDsCnt,
//            :cDsAmt,
//            :dDsAmt,
//  
//            :sAxCnt,
//            :sAxAmt,
//            :cAxCnt,
//            :cAxAmt,
//            :dAxAmt,
//  
//            :sJcCnt,
//            :sJcAmt,
//            :cJcCnt,
//            :cJcAmt,
//            :dJcAmt,
//  
//            :sDbCnt,
//            :sDbAmt,
//            :cDbCnt,
//            :cDbAmt,
//            :dDbAmt,
//  
//            empty_blob()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_statements\n        (\n          merch_num,\n          rec_num,\n          mystery_id,\n          year_month,\n          dda,\n          transit_routing,\n          load_file_name,\n          bank_num,\n          discount,\n          fees,\n\n          visa_sales_cnt,\n          visa_sales_amt,\n          visa_credit_cnt,\n          visa_credit_amt,\n          visa_disc_amt,\n\n          mc_sales_cnt,\n          mc_sales_amt,\n          mc_credit_cnt,\n          mc_credit_amt,\n          mc_disc_amt,\n\n          dinr_sales_cnt,\n          dinr_sales_amt,\n          dinr_credit_cnt,\n          dinr_credit_amt,\n          dinr_disc_amt,\n\n          disc_sales_cnt,\n          disc_sales_amt,\n          disc_credit_cnt,\n          disc_credit_amt,\n          disc_disc_amt,\n\n          amex_sales_cnt,\n          amex_sales_amt,\n          amex_credit_cnt,\n          amex_credit_amt,\n          amex_disc_amt,\n\n          jcb_sales_cnt,\n          jcb_sales_amt,\n          jcb_credit_cnt,\n          jcb_credit_amt,\n          jcb_disc_amt,\n\n          debit_sales_cnt,\n          debit_sales_amt,\n          debit_credit_cnt,\n          debit_credit_amt,\n          debit_disc_amt,\n\n          statement_data\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n\n           :21 ,\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 ,\n           :30 ,\n\n           :31 ,\n           :32 ,\n           :33 ,\n           :34 ,\n           :35 ,\n\n           :36 ,\n           :37 ,\n           :38 ,\n           :39 ,\n           :40 ,\n\n           :41 ,\n           :42 ,\n           :43 ,\n           :44 ,\n           :45 ,\n\n          empty_blob()\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.reports.StatementRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setLong(2,recNum);
   __sJT_st.setLong(3,mystId);
   __sJT_st.setLong(4,yearMonth);
   __sJT_st.setString(5,dda);
   __sJT_st.setString(6,trNum);
   __sJT_st.setString(7,loadFilename);
   __sJT_st.setInt(8,bankNum);
   __sJT_st.setDouble(9,discount);
   __sJT_st.setDouble(10,fees);
   __sJT_st.setInt(11,sViCnt);
   __sJT_st.setDouble(12,sViAmt);
   __sJT_st.setInt(13,cViCnt);
   __sJT_st.setDouble(14,cViAmt);
   __sJT_st.setDouble(15,dViAmt);
   __sJT_st.setInt(16,sMcCnt);
   __sJT_st.setDouble(17,sMcAmt);
   __sJT_st.setInt(18,cMcCnt);
   __sJT_st.setDouble(19,cMcAmt);
   __sJT_st.setDouble(20,dMcAmt);
   __sJT_st.setInt(21,sDiCnt);
   __sJT_st.setDouble(22,sDiAmt);
   __sJT_st.setInt(23,cDiCnt);
   __sJT_st.setDouble(24,cDiAmt);
   __sJT_st.setDouble(25,dDiAmt);
   __sJT_st.setInt(26,sDsCnt);
   __sJT_st.setDouble(27,sDsAmt);
   __sJT_st.setInt(28,cDsCnt);
   __sJT_st.setDouble(29,cDsAmt);
   __sJT_st.setDouble(30,dDsAmt);
   __sJT_st.setInt(31,sAxCnt);
   __sJT_st.setDouble(32,sAxAmt);
   __sJT_st.setInt(33,cAxCnt);
   __sJT_st.setDouble(34,cAxAmt);
   __sJT_st.setDouble(35,dAxAmt);
   __sJT_st.setInt(36,sJcCnt);
   __sJT_st.setDouble(37,sJcAmt);
   __sJT_st.setInt(38,cJcCnt);
   __sJT_st.setDouble(39,cJcAmt);
   __sJT_st.setDouble(40,dJcAmt);
   __sJT_st.setInt(41,sDbCnt);
   __sJT_st.setDouble(42,sDbAmt);
   __sJT_st.setInt(43,cDbCnt);
   __sJT_st.setDouble(44,cDbAmt);
   __sJT_st.setDouble(45,dDbAmt);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:752^7*/

      // get a stream handler to the blob
      BLOB b;
      /*@lineinfo:generated-code*//*@lineinfo:756^7*/

//  ************************************************************
//  #sql [Ctx] { select  statement_data 
//          from    merch_statements
//          where   merch_num  = :merchNum and
//                  year_month = :yearMonth and
//                  rec_num    = :recNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  statement_data  \n        from    merch_statements\n        where   merch_num  =  :1  and\n                year_month =  :2  and\n                rec_num    =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.StatementRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setLong(2,yearMonth);
   __sJT_st.setLong(3,recNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:763^7*/
      //deprecated
      //OutputStream bOut = b.getBinaryOutputStream();
      //Use, as of 9.0.2:
      OutputStream bOut = b.setBinaryStream(0L);

      // write the statement data to the blob
      bOut.write(statementData,0,statementData.length);
      bOut.flush();
      bOut.close();

      // commit
      /*@lineinfo:generated-code*//*@lineinfo:775^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:778^7*/

      updateOk = true;
    }
    catch (Exception e)
    {
      if (!silent)
      {
        System.out.println(this.getClass().getName() +
          "::submitData " + e.toString() + " " + loadFilename + ": " +
          merchNum + ", " + recNum);
      }
      logEntry("submitData",e.toString() +
        "::submitData " + e.toString() + " " + loadFilename + ": " +
        merchNum + ", " + recNum);
    }
    finally
    {
      cleanUp();
    }

    return updateOk;
  }

  /*
  ** public void getData(String getMerchNum, String getRecNum, long getYearMonth)
  **
  ** Loads a database statement record.
  */
  public void getData(String getMerchNum, long getRecNum, long getYearMonth)
  {
    try
    {
      connect();

      // card details
      int     sViCnt = 0;   int     sMcCnt = 0;   int     sDiCnt = 0;
      double  sViAmt = 0;   double  sMcAmt = 0;   double  sDiAmt = 0;
      int     cViCnt = 0;   int     cMcCnt = 0;   int     cDiCnt = 0;
      double  cViAmt = 0;   double  cMcAmt = 0;   double  cDiAmt = 0;
      double  dViAmt = 0;   double  dMcAmt = 0;   double  dDiAmt = 0;

      int     sAxCnt = 0;   int     sJcCnt = 0;   int     sDbCnt = 0;
      double  sAxAmt = 0;   double  sJcAmt = 0;   double  sDbAmt = 0;
      int     cAxCnt = 0;   int     cJcCnt = 0;   int     cDbCnt = 0;
      double  cAxAmt = 0;   double  cJcAmt = 0;   double  cDbAmt = 0;
      double  dAxAmt = 0;   double  dJcAmt = 0;   double  dDbAmt = 0;

      int     sDsCnt = 0;
      double  sDsAmt = 0;
      int     cDsCnt = 0;
      double  cDsAmt = 0;
      double  dDsAmt = 0;

      String  xmlFlag = null;

      // load the record, including a blob reference
      Blob b;
      /*@lineinfo:generated-code*//*@lineinfo:836^7*/

//  ************************************************************
//  #sql [Ctx] { select  merch_num,
//                  bank_num,
//                  rec_num,
//                  mystery_id,
//                  year_month,
//                  dda,
//                  transit_routing,
//                  load_file_name,
//                  bank_num,
//                  discount,
//                  fees,
//                  visa_sales_cnt,
//                  visa_sales_amt,
//                  visa_credit_cnt,
//                  visa_credit_amt,
//                  visa_disc_amt,
//                  mc_sales_cnt,
//                  mc_sales_amt,
//                  mc_credit_cnt,
//                  mc_credit_amt,
//                  mc_disc_amt,
//                  dinr_sales_cnt,
//                  dinr_sales_amt,
//                  dinr_credit_cnt,
//                  dinr_credit_amt,
//                  dinr_disc_amt,
//                  disc_sales_cnt,
//                  disc_sales_amt,
//                  disc_credit_cnt,
//                  disc_credit_amt,
//                  disc_disc_amt,
//                  amex_sales_cnt,
//                  amex_sales_amt,
//                  amex_credit_cnt,
//                  amex_credit_amt,
//                  amex_disc_amt,
//                  jcb_sales_cnt,
//                  jcb_sales_amt,
//                  jcb_credit_cnt,
//                  jcb_credit_amt,
//                  jcb_disc_amt,
//                  debit_sales_cnt,
//                  debit_sales_amt,
//                  debit_credit_cnt,
//                  debit_credit_amt,
//                  debit_disc_amt,
//                  decode( substr(load_file_name,1,4),
//                          'xtmt','Y','N') as xml_statement,
//                  statement_data
//          
//          from    merch_statements
//          where   merch_num = to_number(:getMerchNum) and
//                  year_month = :getYearMonth and
//                  rec_num = :getRecNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_num,\n                bank_num,\n                rec_num,\n                mystery_id,\n                year_month,\n                dda,\n                transit_routing,\n                load_file_name,\n                bank_num,\n                discount,\n                fees,\n                visa_sales_cnt,\n                visa_sales_amt,\n                visa_credit_cnt,\n                visa_credit_amt,\n                visa_disc_amt,\n                mc_sales_cnt,\n                mc_sales_amt,\n                mc_credit_cnt,\n                mc_credit_amt,\n                mc_disc_amt,\n                dinr_sales_cnt,\n                dinr_sales_amt,\n                dinr_credit_cnt,\n                dinr_credit_amt,\n                dinr_disc_amt,\n                disc_sales_cnt,\n                disc_sales_amt,\n                disc_credit_cnt,\n                disc_credit_amt,\n                disc_disc_amt,\n                amex_sales_cnt,\n                amex_sales_amt,\n                amex_credit_cnt,\n                amex_credit_amt,\n                amex_disc_amt,\n                jcb_sales_cnt,\n                jcb_sales_amt,\n                jcb_credit_cnt,\n                jcb_credit_amt,\n                jcb_disc_amt,\n                debit_sales_cnt,\n                debit_sales_amt,\n                debit_credit_cnt,\n                debit_credit_amt,\n                debit_disc_amt,\n                decode( substr(load_file_name,1,4),\n                        'xtmt','Y','N') as xml_statement,\n                statement_data\n         \n        from    merch_statements\n        where   merch_num = to_number( :1 ) and\n                year_month =  :2  and\n                rec_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.StatementRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,getMerchNum);
   __sJT_st.setLong(2,getYearMonth);
   __sJT_st.setLong(3,getRecNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 48) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(48,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchNum = (String)__sJT_rs.getString(1);
   bankNumber = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   recNum = __sJT_rs.getLong(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mystId = __sJT_rs.getLong(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   yearMonth = __sJT_rs.getLong(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   dda = (String)__sJT_rs.getString(6);
   trNum = (String)__sJT_rs.getString(7);
   loadFilename = (String)__sJT_rs.getString(8);
   bankNum = __sJT_rs.getInt(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   discount = __sJT_rs.getDouble(10); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   fees = __sJT_rs.getDouble(11); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sViCnt = __sJT_rs.getInt(12); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sViAmt = __sJT_rs.getDouble(13); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cViCnt = __sJT_rs.getInt(14); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cViAmt = __sJT_rs.getDouble(15); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   dViAmt = __sJT_rs.getDouble(16); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sMcCnt = __sJT_rs.getInt(17); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sMcAmt = __sJT_rs.getDouble(18); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cMcCnt = __sJT_rs.getInt(19); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cMcAmt = __sJT_rs.getDouble(20); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   dMcAmt = __sJT_rs.getDouble(21); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDiCnt = __sJT_rs.getInt(22); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDiAmt = __sJT_rs.getDouble(23); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDiCnt = __sJT_rs.getInt(24); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDiAmt = __sJT_rs.getDouble(25); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   dDiAmt = __sJT_rs.getDouble(26); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDsCnt = __sJT_rs.getInt(27); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDsAmt = __sJT_rs.getDouble(28); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDsCnt = __sJT_rs.getInt(29); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDsAmt = __sJT_rs.getDouble(30); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   dDsAmt = __sJT_rs.getDouble(31); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sAxCnt = __sJT_rs.getInt(32); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sAxAmt = __sJT_rs.getDouble(33); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cAxCnt = __sJT_rs.getInt(34); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cAxAmt = __sJT_rs.getDouble(35); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   dAxAmt = __sJT_rs.getDouble(36); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sJcCnt = __sJT_rs.getInt(37); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sJcAmt = __sJT_rs.getDouble(38); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cJcCnt = __sJT_rs.getInt(39); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cJcAmt = __sJT_rs.getDouble(40); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   dJcAmt = __sJT_rs.getDouble(41); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDbCnt = __sJT_rs.getInt(42); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDbAmt = __sJT_rs.getDouble(43); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDbCnt = __sJT_rs.getInt(44); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDbAmt = __sJT_rs.getDouble(45); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   dDbAmt = __sJT_rs.getDouble(46); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   xmlFlag = (String)__sJT_rs.getString(47);
   b = (java.sql.Blob)__sJT_rs.getBlob(48);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:939^7*/

      log.debug("checking xml flag");
      if ( xmlFlag.equals("Y") )
      {
        setXmlSchema();

        log.debug("decompressing data");
        SAXEventRecorder recorder =
          (SAXEventRecorder)decompressData(b.getBinaryStream());

        SAXContentHandler saxContentHandler = new SAXContentHandler();
        recorder.replay(saxContentHandler);
        StatementDocument = saxContentHandler.getDocument();
        log.debug("done retrieving StatementDocument");
      }
      else    // assumed fixed version of data
      {

        StatementDocument = null;   // reset XML statement doc

        // use the blob ref to read the statement data into internal storage
        int dataLen = (int)b.length();
        int chunkSize = 0;
        int dataRead = 0;
        InputStream is = b.getBinaryStream();
        statementData = new byte[dataLen];
        while (chunkSize != -1)
        {
          chunkSize = is.read(statementData,dataRead,dataLen - dataRead);
          dataRead += chunkSize;
        }
        is.close();
      }

      // load the card details
      log.debug("loading card details");
      cardDetails = new Vector();
      if (dViAmt > 0)
      {
        StatementCardDetails cd =
          new StatementCardDetails(StatementCardDetails.CT_VISA);
        cd.setSalesCnt(sViCnt);
        cd.setSalesAmt(sViAmt);
        cd.setCreditCnt(cViCnt);
        cd.setCreditAmt(cViAmt);
        cd.setDiscAmt(dViAmt);
        cardDetails.add(cd);
      }
      if (dMcAmt > 0)
      {
        StatementCardDetails cd =
          new StatementCardDetails(StatementCardDetails.CT_MC);
        cd.setSalesCnt(sMcCnt);
        cd.setSalesAmt(sMcAmt);
        cd.setCreditCnt(cMcCnt);
        cd.setCreditAmt(cMcAmt);
        cd.setDiscAmt(dMcAmt);
        cardDetails.add(cd);
      }
      if (dDiAmt > 0)
      {
        StatementCardDetails cd =
          new StatementCardDetails(StatementCardDetails.CT_DINERS);
        cd.setSalesCnt(sDiCnt);
        cd.setSalesAmt(sDiAmt);
        cd.setCreditCnt(cDiCnt);
        cd.setCreditAmt(cDiAmt);
        cd.setDiscAmt(dDiAmt);
        cardDetails.add(cd);
      }
      if (dDsAmt > 0)
      {
        StatementCardDetails cd =
          new StatementCardDetails(StatementCardDetails.CT_DISCOVER);
        cd.setSalesCnt(sDsCnt);
        cd.setSalesAmt(sDsAmt);
        cd.setCreditCnt(cDsCnt);
        cd.setCreditAmt(cDsAmt);
        cd.setDiscAmt(dDsAmt);
        cardDetails.add(cd);
      }
      if (dAxAmt > 0)
      {
        StatementCardDetails cd =
          new StatementCardDetails(StatementCardDetails.CT_AMEX);
        cd.setSalesCnt(sAxCnt);
        cd.setSalesAmt(sAxAmt);
        cd.setCreditCnt(cAxCnt);
        cd.setCreditAmt(cAxAmt);
        cd.setDiscAmt(dAxAmt);
        cardDetails.add(cd);
      }
      if (dDsAmt > 0)
      {
        StatementCardDetails cd =
          new StatementCardDetails(StatementCardDetails.CT_JCB);
        cd.setSalesCnt(sJcCnt);
        cd.setSalesAmt(sJcAmt);
        cd.setCreditCnt(cJcCnt);
        cd.setCreditAmt(cJcAmt);
        cd.setDiscAmt(dJcAmt);
        cardDetails.add(cd);
      }
      if (dDsAmt > 0)
      {
        StatementCardDetails cd =
          new StatementCardDetails(StatementCardDetails.CT_DEBIT);
        cd.setSalesCnt(sDbCnt);
        cd.setSalesAmt(sDbAmt);
        cd.setCreditCnt(cDbCnt);
        cd.setCreditAmt(cDbAmt);
        cd.setDiscAmt(dDbAmt);
        cardDetails.add(cd);
      }
    }
    catch (Exception e)
    {
      log.error("StatementRecord.getData(): " + e.toString());
      if (!silent)
      {
        System.out.println(this.getClass().getName() +
          "::getData " + e.toString());
      }
      logEntry("getData",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  /*
  ** private void compressData(String dataString)
  **
  ** Given a Statement, this routine first serializes it and then compresses
  ** the data into an internal storage buffer.
  */
  private void compressData(Statement statement)
  {
    try
    {
      // serialize the statement data
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(statement);

      statementData = baos.toByteArray();

      // compress the serialized statement data
      baos.reset();
      DeflaterOutputStream dos = new DeflaterOutputStream(baos);
      dos.write(statementData,0,statementData.length);
      dos.finish();

      statementData = baos.toByteArray();
    }
    catch (Exception e)
    {
      if (!silent)
      {
        System.out.println(this.getClass().getName() +
          "::compressData " + e.toString());
        e.printStackTrace();
      }
    }
  }

  /*
  ** private String decompressData()
  **
  ** Inflates and deserializes the contents of statementData to reconstitute
  ** the Statement object stored in it.
  **
  ** RETURNS: a Statement reference.
  */
  private Statement decompressData()
  {
    Statement   statement = null;

    if (statementData != null)
    {
      statement = (Statement)decompressData( new ByteArrayInputStream(statementData) );
    }
    return( statement );
  }

  private Object decompressData( InputStream is )
  {
    Object      retVal    = null;

    try
    {
      InflaterInputStream iis = new InflaterInputStream(is);
      ObjectInputStream ois = new ObjectInputStream(iis);
      retVal = ois.readObject();
    }
    catch (Exception e)
    {
      if (!silent)
      {
        System.out.println(this.getClass().getName() +
          "::decompressData " + e.toString());
        e.printStackTrace();
      }
    }
    return( retVal );
  }

  /*
  ** public boolean dataAvailable()
  **
  ** Determines if statement data is currently stored internally.
  **
  ** RETURNS: true if data is available, else false.
  */
  public boolean dataAvailable()
  {
    return (statementData != null);
  }

  private void pad(StringBuffer buffer, int requiredLength)
  {
    for(int i = buffer.length(); i < requiredLength; ++i)
    {
      buffer.append(" ");
    }
  }

  private void addLine(Statement statement, String line )
  {
    // start a new page
    if ( Page == null || ++LineCount >= LINES_PER_PAGE )
    {
      Page = new StatementPage();
      Page.setPageNum(++PageCount);
      statement.addPage(Page);
      LineCount = 0;
    }

    if( line.length() < 100 )
    {
      StringBuffer buf = new StringBuffer(line);
      while( buf.length() < 100 )
      {
        buf.append(" ");
      }
      line = buf.toString();
    }
    Page.addLine(line.substring(0,100));
  }

  private void addLongStringLine(Statement statement, int offset, String value, int maxLength)
  {
    int           offsetAbs = Math.abs(offset);
    StringBuffer  curLine   = new StringBuffer("");
    StringBuffer  line      = new StringBuffer("");

    try
    {
      String longString = com.mes.support.HTMLDecoder.decode(value);

      StringTokenizer tokenizer = new StringTokenizer(longString, " ");

      while(tokenizer.hasMoreTokens())
      {
        String nextWord = tokenizer.nextToken();

        if( curLine.length() + nextWord.length() > maxLength )
        {
          while( line.length() < offsetAbs )
          {
            line.append(" ");
          }

          line.append(curLine);

          // pad line out to max length
          while( line.length() < maxLength )
          {
            line.append(" ");
          }

          addLine(statement, line.toString());

          line.setLength(0);
          curLine.setLength(0);
        }

        curLine.append(nextWord);
        curLine.append(" ");

        switch(nextWord.charAt(nextWord.length()-1))
        {
          case '.':
          case '!':
          case '?':
            // add another space
            curLine.append(" ");
            break;

          case '\n':
            // start new line in statement
            while( line.length() < offsetAbs )
            {
              line.append(" ");
            }

            line.append(curLine);

            // pad line out to max length
            while( line.length() < maxLength )
            {
              line.append(" ");
            }

            addLine(statement, line.toString());

            line.setLength(0);
            curLine.setLength(0);
            break;

          default:
            // do nothing
            break;
        }
      }

      if(curLine.length() > 0)
      {
        while( line.length() < offsetAbs )
        {
          line.append(" ");
        }

        line.append(curLine);

        // pad line out to max length
        while( line.length() < maxLength )
        {
          line.append(" ");
        }

        addLine(statement, line.toString());
      }
    }
    catch(Exception e)
    {
      logEntry("addLongStringLine()", e.toString());
    }
  }

  private void insertValue( StringBuffer line, int offset, String value )
  {
    int     offsetAbs = Math.abs(offset);

    while ( line.length() < offsetAbs )
    {
      line.append(" ");
    }

    if ( offset < 0 )
    {
      line.insert(offsetAbs,value);
    }
    else
    {
      line.insert(offsetAbs-value.length(),value);
    }
  }

  public void setXmlSchema()
  {
    if(bankNumber == 3941)
    {
      XmlSectionFields = XmlSectionFields3941;
    }
    else
    {
      XmlSectionFields = XmlSectionFieldsOthers;
    }
  }

  protected String getXmlData(Element root, String elementName)
  {
    String result = "";

    try
    {
      result = root.element(elementName).getText().trim();
    }
    catch(Exception e)
    {
    }

    return result;
  }

  protected Statement buildStatementFromXml( )
  {
    StringBuffer  line        = new StringBuffer();
    Statement     statement   = new Statement();

    Element       childElement  = null;
    List          elementList   = null;
    Element       el            = null;
    String        elText        = null;
    boolean       headerOutput  = false;
    ListIterator  it            = null;
    Element       root          = null;
    XmlField      xmlField      = null;

    root = getStatementRootElement();

    // reset class members
    LineCount = 0;
    PageCount = 0;
    Page      = null;

    line.setLength(0);
    pad(line,61);
    line.append("Processing Month");
    pad(line,79);
    line.append( getXmlData(root, "PROCESSING_MONTH") );
    addLine(statement,line.toString());

    line.setLength(0);
    pad(line,12);
    line.append( getXmlData(root, "REMIT_NAME") );
    addLine(statement,line.toString());

    line.setLength(0);
    pad(line,12);
    line.append( getXmlData(root, "REMIT_ADDR1") );
    pad(line,61);
    line.append("Association");
    pad(line,79);
    line.append(getXmlData(root, "ASSOCIATION_NUM"));
    addLine(statement,line.toString());

    line.setLength(0);
    pad(line,12);
    line.append( getXmlData(root, "REMIT_ADDR2") );
    addLine(statement,line.toString());

    line.setLength(0);
    pad(line,12);
    line.append( getXmlData(root, "REMIT_ADDR3") );
    pad(line,61);
    line.append("Merchant#");
    pad(line,79);
    line.append(getXmlData(root, "MERCHANT_NUM"));
    addLine(statement,line.toString());

    line.setLength(0);
    pad(line,12);
    line.append( getXmlData(root, "REMIT_ADDR4") );
    addLine(statement,line.toString());

    line.setLength(0);
    pad(line,12);
    line.append( getXmlData(root, "REMIT_ADDR5") );
    pad(line,61);

    if(bankNumber == 3867)
    {
      line.append("Routing#");
      pad(line,79);
      line.append(getXmlData(root, "ROUTING_NUM"));
    }
    addLine(statement,line.toString());
    addLine(statement," ");

    if(bankNumber == 3867)
    {
      line.setLength(0);
      pad(line,61);
      line.append("Deposit Acct#");
      pad(line,79);
      line.append(getXmlData(root, "DEPOSIT_ACCT_NUM"));
      addLine(statement,line.toString());
    }
    else
    {
      addLine(statement," ");
    }

    addLine(statement," ");
    addLine(statement," ");
    addLine(statement," ");
    addLine(statement," ");
    addLine(statement," ");

    line.setLength(0);
    pad(line,14);
    line.append(getXmlData(root, "MAILING_NAME"));
    addLine(statement,line.toString());

    line.setLength(0);
    pad(line,14);
    line.append(getXmlData(root, "MAILING_ADDR1"));
    addLine(statement,line.toString());
    line.setLength(0);

    if(! getXmlData(root, "MAILING_ADDR2").equals(""))
    {
      pad(line,14);
      line.append(getXmlData(root, "MAILING_ADDR2"));
      addLine(statement,line.toString());
      line.setLength(0);
    }

    pad(line,14);
    line.append(getXmlData(root, "MAILING_ADDR3"));
    addLine(statement,line.toString());

    line.setLength(0);
    pad(line,14);
    line.append(getXmlData(root, "MAILING_ADDR4"));
    addLine(statement,line.toString());

    line.setLength(0);
    pad(line,14);
    line.append(getXmlData(root, "MAILING_ADDR5"));
    addLine(statement,line.toString());

    line.setLength(0);
    pad(line,61);
    line.append("AMOUNT DEDUCTED:");
    pad(line,79);
    line.append(getXmlData(root, "AMOUNT_DEDUCTED"));
    addLine(statement,line.toString());

    addLine(statement," ");

    for ( int sectionIdx = 0; sectionIdx < XmlSectionNames.length; ++sectionIdx )
    {
      elementList = root.selectNodes( XmlSectionNames[sectionIdx] );

      headerOutput = false;

      if ( elementList.size() == 0 )
      {
        continue;
      }

      line.setLength(0);
      pad(line,42);
      line.append(XmlSectionTitles[sectionIdx].toUpperCase());
      addLine(statement,line.toString());

      it = elementList.listIterator();
      while( it.hasNext() )
      {
        // extract the line items
        el = (Element)it.next();

        line.setLength(0);

        for( int i = 0; i < XmlSectionFields[sectionIdx].length; ++i )
        {
          xmlField = XmlSectionFields[sectionIdx][i];

          if ( !headerOutput )
          {
            line.setLength(0);
            line.append(XmlColumnHeaders[sectionIdx]);
            addLine(statement,line.toString());
            headerOutput = true;
            line.setLength(0);    // reset the buffer
          }
          childElement = el.element(xmlField.ElementName);
          if ( childElement == null )
          {
            elText = "";
          }
          else
          {
            elText = childElement.getText().trim();
          }

          insertValue(line,XmlStaticOffsets[sectionIdx][i],elText);
        }
        addLine(statement,line.toString());
      }   // end while loop through rows

      line.setLength(0);
      switch( sectionIdx )
      {
        case 0:   // plan summary
          insertValue(line,-1,root.selectSingleNode("//PLAN_SUMMARY/PLAN_SUMMARY_TOTALS/PLAN_CODE").getText().trim());
          insertValue(line,11,root.selectSingleNode("//PLAN_SUMMARY/PLAN_SUMMARY_TOTALS/NUM_SALES").getText().trim());
          insertValue(line,26,root.selectSingleNode("//PLAN_SUMMARY/PLAN_SUMMARY_TOTALS/DOLLAR_SALES").getText().trim());
          insertValue(line,36,root.selectSingleNode("//PLAN_SUMMARY/PLAN_SUMMARY_TOTALS/NUM_CREDITS").getText().trim());
          insertValue(line,49,root.selectSingleNode("//PLAN_SUMMARY/PLAN_SUMMARY_TOTALS/DOLLAR_CREDITS").getText().trim());
          insertValue(line,63,root.selectSingleNode("//PLAN_SUMMARY/PLAN_SUMMARY_TOTALS/NET_SALES").getText().trim());
          insertValue(line,72,root.selectSingleNode("//PLAN_SUMMARY/PLAN_SUMMARY_TOTALS/AVG_TKT").getText().trim());
          insertValue(line,99,root.selectSingleNode("//PLAN_SUMMARY/PLAN_SUMMARY_TOTALS/DISCOUNT_DUE").getText().trim());
          break;

        case 1:   // deposits
          insertValue(line,-4,"DEPOSIT TOTALS");
          insertValue(line,35,root.selectSingleNode("//TRANSACTION_SECTION/DEPOSIT_TOTALS/NUM_SALES").getText().trim());
          insertValue(line,49,root.selectSingleNode("//TRANSACTION_SECTION/DEPOSIT_TOTALS/DOLLAR_SALES").getText().trim());
          insertValue(line,65,root.selectSingleNode("//TRANSACTION_SECTION/DEPOSIT_TOTALS/DOLLAR_CREDITS").getText().trim());
          insertValue(line,81,root.selectSingleNode("//TRANSACTION_SECTION/DEPOSIT_TOTALS/DISC_PAID").getText().trim());
          if(bankNumber == 3941)
          {
            insertValue(line,99,root.selectSingleNode("//TRANSACTION_SECTION/DEPOSIT_TOTALS/SETTLED").getText().trim());
          }
          else
          {
            insertValue(line,99,root.selectSingleNode("//TRANSACTION_SECTION/DEPOSIT_TOTALS/NET_DEPOSIT").getText().trim());
          }
          break;

        case 2:  // adjustments
          insertValue(line,-4,"ADJUSTMENT TOTALS");
          insertValue(line,35,root.selectSingleNode("//TRANSACTION_SECTION/ADJUSTMENT_TOTALS/NUM_SALES").getText().trim());
          insertValue(line,49,root.selectSingleNode("//TRANSACTION_SECTION/ADJUSTMENT_TOTALS/DOLLAR_SALES").getText().trim());
          insertValue(line,65,root.selectSingleNode("//TRANSACTION_SECTION/ADJUSTMENT_TOTALS/DOLLAR_CREDITS").getText().trim());
          insertValue(line,81,root.selectSingleNode("//TRANSACTION_SECTION/ADJUSTMENT_TOTALS/DISC_PAID").getText().trim());
          if(bankNumber == 3941)
          {
            insertValue(line,99,root.selectSingleNode("//TRANSACTION_SECTION/ADJUSTMENT_TOTALS/SETTLED").getText().trim());
          }
          else
          {
            insertValue(line,99,root.selectSingleNode("//TRANSACTION_SECTION/ADJUSTMENT_TOTALS/NET_DEPOSIT").getText().trim());
          }
          break;

        case 3:   // chargebacks
          insertValue(line,-4,"CHARGEBACK TOTALS");
          insertValue(line,35,root.selectSingleNode("//TRANSACTION_SECTION/CHARGEBACK_TOTALS/NUM_SALES").getText().trim());
          insertValue(line,49,root.selectSingleNode("//TRANSACTION_SECTION/CHARGEBACK_TOTALS/DOLLAR_SALES").getText().trim());
          insertValue(line,65,root.selectSingleNode("//TRANSACTION_SECTION/CHARGEBACK_TOTALS/DOLLAR_CREDITS").getText().trim());
          insertValue(line,81,root.selectSingleNode("//TRANSACTION_SECTION/CHARGEBACK_TOTALS/DISC_PAID").getText().trim());
          if(bankNumber == 3941)
          {
            insertValue(line,99,root.selectSingleNode("//TRANSACTION_SECTION/CHARGEBACK_TOTALS/SETTLED").getText().trim());
          }
          else
          {
            insertValue(line,99,root.selectSingleNode("//TRANSACTION_SECTION/CHARGEBACK_TOTALS/NET_DEPOSIT").getText().trim());
          }
          break;

        case 4:   // fees
          insertValue(line,-71,"TOTAL FEES DUE");
          insertValue(line,99,root.selectSingleNode("//FEES_SECTION/FEE_TOTALS/TOTAL_FEES_DUE").getText().trim());
          break;
      }
      addLine(statement,line.toString());
      addLine(statement," ");
    }

    log.debug("building totals box");
    // build the totals box
    if ( root.selectSingleNode("//TOTALS_BOX_SECTION/MIN_DISC_DUE") != null )
    {
      line.setLength(0);
      insertValue(line,-23,"MINIMUM DISCOUNT DUE");
      insertValue(line,54,root.selectSingleNode("//TOTALS_BOX_SECTION/MIN_DISC_DUE").getText().trim());
      addLine(statement,line.toString());
    }

    if ( root.selectSingleNode("//TOTALS_BOX_SECTION/DISC_DUE") != null )
    {
      line.setLength(0);
      insertValue(line,-23,"DISCOUNT DUE");
      insertValue(line,54,root.selectSingleNode("//TOTALS_BOX_SECTION/DISC_DUE").getText().trim());
      addLine(statement,line.toString());
    }

    if ( root.selectSingleNode("//TOTALS_BOX_SECTION/DISC_PAID") != null )
    {
      line.setLength(0);
      insertValue(line,-23,"DISCOUNT PAID");
      insertValue(line,54,root.selectSingleNode("//TOTALS_BOX_SECTION/DISC_PAID").getText().trim());
      addLine(statement,line.toString());
    }

    if ( root.selectSingleNode("//TOTALS_BOX_SECTION/FEES_DUE") != null )
    {
      line.setLength(0);
      insertValue(line,-23,"FEES DUE");
      insertValue(line,54,root.selectSingleNode("//TOTALS_BOX_SECTION/FEES_DUE").getText().trim());
      addLine(statement,line.toString());
    }

    if ( root.selectSingleNode("//TOTALS_BOX_SECTION/AMT_DEDUCTED") != null )
    {
      line.setLength(0);
      insertValue(line,-23,"AMOUNT DEDUCTED");
      insertValue(line,54,root.selectSingleNode("//TOTALS_BOX_SECTION/AMT_DEDUCTED").getText().trim());
      addLine(statement,line.toString());
    }
    addLine(statement," ");

    if ( root.selectSingleNode("//TOTALS_BOX_SECTION/AMT_DUE") != null )
    {
      line.setLength(0);
      insertValue(line,-23,"AMOUNT DUE");
      insertValue(line,54,root.selectSingleNode("//TOTALS_BOX_SECTION/AMT_DUE").getText().trim());
      addLine(statement,line.toString());
    }
    addLine(statement," ");

    log.debug("setting statement message");
    elementList = root.selectNodes( "//MSG_SECTION/MSG" );

    it = elementList.listIterator();
    while( it.hasNext() )
    {
      line.setLength(0);
      insertValue(line,-14,((Element)it.next()).getText().trim());
      addLine(statement,line.toString());
    }
    addLine(statement," ");

/*
    log.debug("setting campaign message");
    elementList = root.selectNodes( "//CAMPAIGN_SECTION/CAMP_MSG" );

    it = elementList.listIterator();
    while( it.hasNext() )
    {
      String msg = ((Element)(it.next())).getText().trim();
      line.setLength(0);
      addLongStringLine(statement, 14, msg, 74);
      addLine(statement, " ");
    }
*/

    return( statement );
  }

  /*
  ** public getElementText(String elementName)
  **
  ** Provides a safe way to retrieve the contents of an XML element tag.
  ** Returns a blank string if tag is missing.
  */
  public String getElementText(String elementName)
  {
    String result = "";

    try
    {
      result = getStatementRootElement().element(elementName).getText();
    }
    catch(Exception e)
    {
      logEntry("getElementText(" + elementName + ")", e.toString());
    }

    return result;
  }

  /*
  ** public Statement getStatement()
  **
  ** If statement data is available, a Statement object is generated from
  ** the stored statement data and returned.
  **
  ** RETURNS: the Statement.
  */
  public Statement getStatement()
  {
    Statement statement = null;
    if ( hasXmlStatement() )
    {
      log.debug("building statement from XML");
      statement = buildStatementFromXml();
    }
    else
    {
      if (dataAvailable())
      {
        statement = decompressData();
      }
    }
    return( statement );
  }

  public Document getStatementDocument()
  {
    return( StatementDocument );
  }

  public Element getStatementRootElement()
  {
    Element   retVal    = null;

    if ( StatementDocument != null )
    {
      retVal = StatementDocument.getRootElement();
    }
    return( retVal );
  }

  public boolean hasXmlStatement()
  {
    return( StatementDocument != null );
  }

  public boolean hasMbsStatement()
  {
    return( loadFilename.startsWith("mstmt") );
  }

  public String nl2br(String raw)
  {
    StringBuffer result = new StringBuffer("");

    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if( raw.charAt(i) == '\n' )
        {
          result.append("<br><br>");
        }
        else
        {
          result.append(raw.charAt(i));
        }
      }
    }
    catch(Exception e)
    {
    }

    return( result.toString() );
  }


  /*
  ** public void setStatement(Statement statement)
  **
  ** All required fields are extracted from the Statement and stored, after
  ** which the statement itself is compressed into internal storage.
  */
  public void setStatement(Statement statement)
  {
    // serialize/compress the statement
    compressData(statement);
  }

  public String toString()
  {
    return "" + decompressData();
  }

  /*
  ** ACCESSORS
  */

  public void setCardDetails(Vector details)
  {
    cardDetails = details;
  }
  public Vector getCardDetails()
  {
    return cardDetails;
  }

  public Date getStatementDate( )
  {
    Calendar  cal             = Calendar.getInstance();
    Date      statementDate   = null;
    String    yearMonth       = String.valueOf(getYearMonth());

    try
    {
      cal.set(Calendar.YEAR,  Integer.parseInt(yearMonth.substring(0,4)) );
      cal.set(Calendar.MONTH, Integer.parseInt(yearMonth.substring(4))-1 );
      cal.set(Calendar.DAY_OF_MONTH,1);
      statementDate = new java.sql.Date( cal.getTime().getTime() );
    }
    catch( Exception e )
    {
      logEntry("getStatementDate()",e.toString());
    }

    return( statementDate );
  }

  public static void main( String[] args )
  {
    StatementRecord     rec   = null;

    SQLJConnectionBase.initStandalone();

    try
    {
      rec = new StatementRecord();

      rec.connect();
      rec.getData("942000000001",1,200611);
      rec.getStatement();
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ rec.cleanUp(); } catch( Exception ee ) {}
    }
  }
}/*@lineinfo:generated-code*/