/*@lineinfo:filename=AvailableUserGroupsDropDown*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CQAppTypeDropDown.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 4/20/04 4:09p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class AvailableUserGroupsDropDown extends DropDownTable
{
  public AvailableUserGroupsDropDown( )
  {
  }
  
  public void initialize( Object[] params )
  {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    long                userNode  = 0L;
    
    try
    {
      connect();
      
      userNode = ((Long)params[0]).longValue();
      
      /*@lineinfo:generated-code*//*@lineinfo:49^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ RULE */
//                  distinct ug.group_id  value,
//                  ug.name               name
//          from    user_groups ug,
//                  user_to_group utg,
//                  users u,
//                  t_hierarchy th,
//                  mif mf
//          where   th.ancestor = :userNode and
//                  (
//                    (th.descendent = mf.association_node and mf.merchant_number = u.hierarchy_node ) or
//                    (th.descendent = u.hierarchy_node and mf.merchant_number = 941000000002)
//                  ) and
//                  u.enabled = 'Y' and
//                  (
//                    u.type_id = utg.user_id or
//                    u.user_id = utg.user_id 
//                  ) and
//                  utg.group_id = ug.group_id
//          order by ug.name 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ RULE */\n                distinct ug.group_id  value,\n                ug.name               name\n        from    user_groups ug,\n                user_to_group utg,\n                users u,\n                t_hierarchy th,\n                mif mf\n        where   th.ancestor =  :1  and\n                (\n                  (th.descendent = mf.association_node and mf.merchant_number = u.hierarchy_node ) or\n                  (th.descendent = u.hierarchy_node and mf.merchant_number = 941000000002)\n                ) and\n                u.enabled = 'Y' and\n                (\n                  u.type_id = utg.user_id or\n                  u.user_id = utg.user_id \n                ) and\n                utg.group_id = ug.group_id\n        order by ug.name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AvailableUserGroupsDropDown",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AvailableUserGroupsDropDown",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:71^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        addElement(rs);
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("initialize()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/