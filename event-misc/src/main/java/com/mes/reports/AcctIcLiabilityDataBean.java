/*@lineinfo:filename=AcctIcLiabilityDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AcctIcLiabilityDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class AcctIcLiabilityDataBean extends ReportSQLJBean
{
  public class RowData implements Comparable
  {
    public    double                CreditsAmount     = 0.0;
    public    int                   CreditsCount      = 0;
    public    double                DailyDiscount     = 0.0;
    public    double                IcDue             = 0.0;
    public    long                  NodeId            = 0L;
    public    long                  OrgId             = 0L;
    public    String                OrgName           = null;
    public    double                SalesAmount       = 0.0;
    public    int                   SalesCount        = 0;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      CreditsAmount     = resultSet.getDouble("credits_amount");
      CreditsCount      = resultSet.getInt("credits_count");
      DailyDiscount     = resultSet.getDouble("daily_discount");
      IcDue             = resultSet.getDouble("ic_due");
      NodeId            = resultSet.getLong("child_node");
      OrgId             = resultSet.getLong("child_org_num");
      OrgName           = processString(resultSet.getString("child_name"));
      SalesAmount       = resultSet.getDouble("sales_amount");
      SalesCount        = resultSet.getInt("sales_count");
    }
    
    public int compareTo( Object obj )
    {
      RowData       compareRow      = (RowData)obj;
      int           retVal          = 0;
      
      if ( ( retVal = OrgName.compareTo(compareRow.OrgName) ) == 0 )
      {
        if ( ( retVal = (int)( NodeId - compareRow.NodeId ) ) == 0 )
        {
          retVal = (int)(OrgId - compareRow.OrgId);
        }
      }
      return( retVal );
    }
    
    public double getEstLiabilityAmount()
    {
      return( IcDue - DailyDiscount );
    }
  }
  
  public AcctIcLiabilityDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Sales Count\",");
    line.append("\"Sales Amount\",");
    line.append("\"Credits Count\",");
    line.append("\"Credits Amount\",");
    line.append("\"IC Due\",");
    line.append("\"Daily Discount\",");
    line.append("\"Est Liability\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( encodeHierarchyNode(record.NodeId) );
    line.append( ",\"" );
    line.append( record.OrgName );
    line.append( "\"," );
    line.append( record.SalesCount );
    line.append( "," );
    line.append( record.SalesAmount );
    line.append( "," );
    line.append( record.CreditsCount );
    line.append( "," );
    line.append( record.CreditsAmount );
    line.append( "," );
    line.append( record.IcDue );
    line.append( "," );
    line.append( record.DailyDiscount );
    line.append( "," );
    line.append( record.getEstLiabilityAmount() );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_est_ic_liability_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    int                           i;
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    RowData                       row               = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:183^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                      index (po idx_parent_org_num)
//                      index (gm pkgroup_merchant)
//                      index (ic pk_daily_detail_ic_summary)
//                   */
//                  o.org_num                         as child_org_num,
//                  o.org_group                       as child_node,
//                  o.org_name                        as child_name,
//                  sum( ic.SALES_COUNT )             as sales_count,
//                  sum( ic.sales_amount )            as sales_amount,
//                  sum( ic.credits_count )           as credits_count,
//                  sum( ic.credits_amount )          as credits_amount,
//                  round( sum( ( ic.sales_count * nvl(icd.IC_PER_ITEM,0) ) +
//                              ( ic.sales_amount * nvl(icd.ic_rate,0) * 0.01 ) -
//                              ( ic.credits_count * nvl(icd.ic_per_item,0) ) -
//                              ( ic.credits_amount * nvl(icd.ic_rate,0) * 0.01 ) ), 2 )       
//                                                    as ic_due,
//                  round( sum( decode( nvl(mr.merch_dly_discount_flag,'N'),
//                                      'Y', ( ( ic.sales_amount * mf.VISA_DISC_RATE * 0.00001 ) +
//                                             ( ic.sales_count * mf.VISA_PER_ITEM * 0.001 ) ),
//                                      0 ) ), 
//                         2 )                        as daily_discount                    
//          from    parent_org                    po,
//                  organization                  o,        
//                  group_merchant                gm,
//                  group_rep_merchant            grm,
//                  mif                           mf,                
//                  groups                        g,
//                  daily_detail_ic_summary       ic,
//                  daily_detail_file_ic_desc     icd,
//                  merchant                      mr        
//          where   po.parent_org_num = :orgId and
//                  o.org_num = po.org_num and        
//                  gm.org_num = o.org_num and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  mf.merchant_number = gm.merchant_number and
//                  mr.merch_number(+) = mf.merchant_number and                
//                  g.assoc_number = (mf.bank_number || mf.dmagent) and
//                  ic.merchant_number = mf.merchant_number and
//                  ic.batch_date between :beginDate and :endDate and
//                  ic.ic_cat <> 0 and         
//                  icd.CARD_TYPE(+) = ic.card_type and
//                  icd.IC_CODE(+) = ic.IC_CAT and
//                  ic.batch_date between icd.valid_date_begin(+) and icd.valid_date_end(+)        
//          group by o.org_num, o.org_group, o.org_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                    index (po idx_parent_org_num)\n                    index (gm pkgroup_merchant)\n                    index (ic pk_daily_detail_ic_summary)\n                 */\n                o.org_num                         as child_org_num,\n                o.org_group                       as child_node,\n                o.org_name                        as child_name,\n                sum( ic.SALES_COUNT )             as sales_count,\n                sum( ic.sales_amount )            as sales_amount,\n                sum( ic.credits_count )           as credits_count,\n                sum( ic.credits_amount )          as credits_amount,\n                round( sum( ( ic.sales_count * nvl(icd.IC_PER_ITEM,0) ) +\n                            ( ic.sales_amount * nvl(icd.ic_rate,0) * 0.01 ) -\n                            ( ic.credits_count * nvl(icd.ic_per_item,0) ) -\n                            ( ic.credits_amount * nvl(icd.ic_rate,0) * 0.01 ) ), 2 )       \n                                                  as ic_due,\n                round( sum( decode( nvl(mr.merch_dly_discount_flag,'N'),\n                                    'Y', ( ( ic.sales_amount * mf.VISA_DISC_RATE * 0.00001 ) +\n                                           ( ic.sales_count * mf.VISA_PER_ITEM * 0.001 ) ),\n                                    0 ) ), \n                       2 )                        as daily_discount                    \n        from    parent_org                    po,\n                organization                  o,        \n                group_merchant                gm,\n                group_rep_merchant            grm,\n                mif                           mf,                \n                groups                        g,\n                daily_detail_ic_summary       ic,\n                daily_detail_file_ic_desc     icd,\n                merchant                      mr        \n        where   po.parent_org_num =  :1  and\n                o.org_num = po.org_num and        \n                gm.org_num = o.org_num and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and        \n                mf.merchant_number = gm.merchant_number and\n                mr.merch_number(+) = mf.merchant_number and                \n                g.assoc_number = (mf.bank_number || mf.dmagent) and\n                ic.merchant_number = mf.merchant_number and\n                ic.batch_date between  :4  and  :5  and\n                ic.ic_cat <> 0 and         \n                icd.CARD_TYPE(+) = ic.card_type and\n                icd.IC_CODE(+) = ic.IC_CAT and\n                ic.batch_date between icd.valid_date_begin(+) and icd.valid_date_end(+)        \n        group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AcctIcLiabilityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AcctIcLiabilityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:232^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      resultSet.close();
      it.close();
      
      // setup report rows for children that had no data 
      // in the original query.
      /*@lineinfo:generated-code*//*@lineinfo:244^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group         as child_node,
//                  o.org_name          as child_name,
//                  o.org_num           as child_org_num,
//                  0                   as sales_count,
//                  0                   as sales_amount,
//                  0                   as credits_count,
//                  0                   as credits_amount,
//                  0                   as ic_due,
//                  0                   as daily_discount
//          from    parent_org              po,
//                  organization            o
//          where   po.parent_org_num = :orgId and
//                  o.org_num = po.org_num                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group         as child_node,\n                o.org_name          as child_name,\n                o.org_num           as child_org_num,\n                0                   as sales_count,\n                0                   as sales_amount,\n                0                   as credits_count,\n                0                   as credits_amount,\n                0                   as ic_due,\n                0                   as daily_discount\n        from    parent_org              po,\n                organization            o\n        where   po.parent_org_num =  :1  and\n                o.org_num = po.org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AcctIcLiabilityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AcctIcLiabilityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:259^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        for( i = 0; i < ReportRows.size(); ++i )
        {
          row = (RowData) ReportRows.elementAt(i);
          if ( row.OrgId == resultSet.getLong("child_org_num") )
          {
            break;
          }
        }
        if ( i >= ReportRows.size() )
        {
          ReportRows.addElement( new RowData( resultSet ) );
        }
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         inc           = -1;
      
      if ( ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.MONDAY ) ||
           ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.TUESDAY ) )
      {
        // friday data available on monday,
        // sat,sun,mon data available on tuesday
        inc = -3;     
      }
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, inc );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      
      cal.setTime( ReportDateEnd );
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/