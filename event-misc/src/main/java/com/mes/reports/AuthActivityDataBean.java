/*@lineinfo:filename=AuthActivityDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AuthActivityDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-12-04 11:25:17 -0800 (Thu, 04 Dec 2008) $
  Version            : $Revision: 15585 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import com.mes.support.TridentTools;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class AuthActivityDataBean extends OrgSummaryDataBean
{
  public static final int[] DenominatorIndexes = 
  {
    MesOrgSummaryEntry.VMC_AUTH_APPR_INDEX
  };

  public static final int[] NumeratorIndexes = 
  {
    MesOrgSummaryEntry.VMC_AUTH_DECL_INDEX
  };
  
  public class DetailRow
  {
    public    String            AcquirerBin           = null;
    public    double            AuthAmount            = 0.0;
    public    double            AuthAmountSecondary   = 0.0;
    public    String            AuthCharInd           = null;
    public    String            AuthCharIndDesc       = null;
    public    String            AuthCode              = null;
    public    String            AuthFileSource        = null;
    public    String            AuthProductCode       = null;
    public    String            AuthProductDesc       = null;
    private   boolean           AuthReversed          = false;
    public    String            AVSResult             = null;
    public    String            AVSResultDesc         = null;
    public    String            CardNumber            = null;
    public    String            CardNumberEnc         = null;
    public    String            CardType              = null;
    public    String            CommCardRequestInd    = null;
    public    String            CommCardResponseDesc  = null;
    public    String            CommCardResponseInd   = null;
    public    String            CountryCode           = null;
    public    String            CountryCodeDesc       = null;
    public    String            CurrencyCode          = null;
    public    String            CVVResult             = null;
    public    String            CVV2Request           = null;
    public    String            CVV2Result            = null;
    public    String            DbaCity               = null;
    public    String            DbaName               = null;
    public    String            DbaSicCode            = null;
    public    String            DbaState              = null;
    private   String            DeveloperId           = "";
    public    String            DeviceCode            = null;
    public    String            DeviceCodeDesc        = null;
    public    String            EcommInd              = null;
    public    String            EcommIndDesc          = null;
    public    String            ExpirationDate        = null;
    public    int               FileBankNumber        = 0;
    public    String            IndustryCode          = null;
    public    String            IssuerBin             = null;
    public    String            IssuerName            = null;
    public    String            IssuerPhone           = null;
    public    String            LineType              = null;
    public    long              MerchantId            = 0L;
    public    String            PosCondCode           = null;
    public    String            PosCondCodeDesc       = null;
    public    String            PosEntryMode          = null;
    public    String            PosInfo               = null;
    private   long              RecId                 = 0L;
    public    String            RequestedACI          = null;
    public    String            RequestedACIDesc      = null;
    public    String            ResponseCode          = null;
    public    String            ResponseDesc          = null;
    public    String            ResponseText          = null;
    public    String            RetrievalRefNum       = null;
    public    int               SicCode               = 0;
    public    String            StandInAdviceCode     = null;
    public    String            StandInCodeDesc       = null;
    public    String            StationId             = null;
    public    String            SystemTraceNumber     = null;
    public    String            TerminalFormatCode    = null;
    public    String            TerminalId            = null;
    public    String            TranCode              = null;
    public    String            TranCodeDesc          = null;
    public    Date              TranDate              = null;
    public    String            TranId                = null;
    public    String            TranTimeZone          = null;
    public    Timestamp         TranTS                = null;
    public    String            TranValCode           = null;
    private   String            TridentTranId         = null;

    
    public DetailRow( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      MerchantId          = resultSet.getLong("merchant_number");
      TerminalId          = processString(resultSet.getString("terminal_id"));
      TranDate            = resultSet.getDate("tran_date");
      TranTS              = resultSet.getTimestamp("tran_ts");
      CardNumber          = processString(resultSet.getString("card_number"));
      CardNumberEnc       = resultSet.getString("card_number_enc");
      CardType            = processString(resultSet.getString("card_type"));
      AuthCode            = processString(resultSet.getString("auth_code"));
      AuthAmount          = resultSet.getDouble("auth_amount");
      AuthAmountSecondary = resultSet.getDouble("secondary_amount");
      ExpirationDate      = processString(resultSet.getString("exp_date"));
      SicCode             = resultSet.getInt("sic_code");
      AVSResult           = processString(resultSet.getString("avs_result"));
      CVVResult           = processString(resultSet.getString("cvv_result"));
      CVV2Request         = processString(resultSet.getString("cvv2_request"));
      CVV2Result          = processString(resultSet.getString("cvv2_result"));
      LineType            = processString(resultSet.getString("line_type"));
      ResponseCode        = processString(resultSet.getString("response_code"));
      TranId              = processString(resultSet.getString("tran_id"));
      TranValCode         = processString(resultSet.getString("val_code"));
      StationId           = processString(resultSet.getString("station_id"));
      PosCondCode         = processString(resultSet.getString("pos_cond_code"));
      AuthCharInd         = processString(resultSet.getString("auth_char_ind"));
      TerminalFormatCode  = processString(resultSet.getString("term_fmt_code"));
      StandInAdviceCode   = processString(resultSet.getString("stand_in_code"));
      AuthProductCode     = processString(resultSet.getString("auth_product_code"));
      AuthReversed        = (resultSet.getInt("auth_reversed") == 1);
      AuthFileSource      = processString(resultSet.getString("file_source"));
      FileBankNumber      = resultSet.getInt("file_bank_number");
      DeveloperId         = processString(resultSet.getString("developer_id"));

      // if this is an older record that did not
      // have the card type, then figure it out
      // using the card number.
      if ( CardType == null )
      {
        CardType = getCardType( CardNumber );
      }
      
      RecId = resultSet.getLong("rec_id");
      
      // enhanced fields
      try
      {
        AcquirerBin         = processString(resultSet.getString("acq_bin"));
        RequestedACI        = processString(resultSet.getString("req_aci"));
        RequestedACIDesc    = processString(resultSet.getString("req_aci_desc"));
        CountryCode         = processString(resultSet.getString("country_code"));
        CountryCodeDesc     = processString(resultSet.getString("country_code_desc"));
        DeviceCode          = processString(resultSet.getString("dev_code"));
        DeviceCodeDesc      = processString(resultSet.getString("dev_code_desc"));
        EcommInd            = processString(resultSet.getString("ecomm_ind"));
        EcommIndDesc        = processString(resultSet.getString("ecomm_ind_desc"));
        ResponseText        = processString(resultSet.getString("resp_text"));
        CommCardRequestInd  = processString(resultSet.getString("comm_card_req_ind"));
        CommCardResponseInd = processString(resultSet.getString("comm_card_resp_ind"));
        CommCardResponseDesc= processString(resultSet.getString("comm_card_resp_desc"));
        IndustryCode        = processString(resultSet.getString("industry_code"));
        RetrievalRefNum     = processString(resultSet.getString("retr_ref_num"));
        TridentTranId       = processString(resultSet.getString("trident_tran_id"));
        TranCode            = processString(resultSet.getString("tran_code"));
        TranCodeDesc        = processString(resultSet.getString("tran_code_desc"));
        PosEntryMode        = processString(resultSet.getString("pos_entry_mode"));
        IssuerBin           = processString(resultSet.getString("issuer_bin"));
        IssuerName          = processString(resultSet.getString("issuer_name"));
        IssuerPhone         = processString(resultSet.getString("issuer_phone"));
        SystemTraceNumber   = processString(resultSet.getString("system_trace_number"));
        CurrencyCode        = processString(resultSet.getString("currency_code"));
        DbaCity             = processString(resultSet.getString("dba_city"));
        DbaName             = processString(resultSet.getString("dba_name"));
        DbaState            = processString(resultSet.getString("dba_state"));
        DbaSicCode          = processString(resultSet.getString("dba_sic_code"));
        AVSResultDesc       = processString(resultSet.getString("avs_result_desc"));
        ResponseDesc        = processString(resultSet.getString("response_desc"));
        StandInCodeDesc     = processString(resultSet.getString("stand_in_desc"));
        AuthCharIndDesc     = processString(resultSet.getString("aci_desc"));
        PosCondCodeDesc     = processString(resultSet.getString("pos_cond_code_desc"));
        AuthProductDesc     = processString(resultSet.getString("auth_product_desc"));
        
        String tzString = resultSet.getString("tran_time_zone");
        
        if (tzString.startsWith("java-fill-"))
        {
          Calendar    cal       = Calendar.getInstance();
          int         hours     = (1000 * 60 * 60);   // one hour in millisecs
          int         gmtOffset = 0;
          
          cal.setTime( TranTS );
          gmtOffset = (cal.getTimeZone().getOffset( GregorianCalendar.AD,
                                                    cal.get(Calendar.YEAR), 
                                                    cal.get(Calendar.MONTH),
                                                    cal.get(Calendar.DAY_OF_MONTH), 
                                                    cal.get(Calendar.DAY_OF_WEEK), 
                                                    0) / hours);
          
          // Calendar (java) will be in pacific time so
          // adjust to file time zone if necessary
          gmtOffset += Integer.parseInt(tzString.substring(10));
         
          TranTimeZone = ("GMT" + String.valueOf(gmtOffset)); 
        }
        else
        {
          TranTimeZone = tzString;
        }
        
        loadPOSInfo();
      }
      catch( Exception e )
      {
        // ignore, data not available
      }
    }
    
    public String getAuthAmountSecondaryFormatted()
    {
      String    retVal    = "";
      if ( AuthAmountSecondary != -1 ) 
      {
        retVal = MesMath.toCurrency( AuthAmountSecondary );
      }
      return( retVal );
    }
    
    public String getHostName()
    { 
      return( getAuthHost(AuthFileSource, FileBankNumber, DeveloperId) );
    }
    
    public long getRecId()
    {
      if ( RecId == 0L )
      {
        resolveAuthRecId();
      }
      return( RecId );
    }
    
    public int getTranCodeAsInt()
    {
      int   retVal    = 0;
      
      try
      {
        retVal = Integer.parseInt(TranCode);
      }
      catch( Exception e )
      {
        // ignore parse errors, return 0
      }
      return( retVal );
    }
    
    public String getTridentTranId()
    {
      return( TridentTranId );
    }
    
    public boolean isReversed()
    {
      return( AuthReversed );
    }
    
    public void loadPOSInfo()
    {
      ResultSetIterator     it = null;
      
      try
      {
        // initialize the value
        PosInfo = "Not Available";
      
        if ( AuthFileSource.equals("tauth") )
        {
          /*@lineinfo:generated-code*//*@lineinfo:315^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(eq.equip_descriptor,mvn.equip_model)    as pos_info
//              from    trident_profile   tp,
//                      merch_vnumber     mvn,
//                      equipment         eq
//              where   tp.terminal_id = :TerminalId and
//                      mvn.vnumber = tp.catid and
//                      eq.equip_model(+) = mvn.equip_model            
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(eq.equip_descriptor,mvn.equip_model)    as pos_info\n            from    trident_profile   tp,\n                    merch_vnumber     mvn,\n                    equipment         eq\n            where   tp.terminal_id =  :1  and\n                    mvn.vnumber = tp.catid and\n                    eq.equip_model(+) = mvn.equip_model";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,TerminalId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AuthActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:324^11*/
        }
        else if ( AuthFileSource.equals("tc33") )
        {
          /*@lineinfo:generated-code*//*@lineinfo:328^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(eq.equip_descriptor,mvn.equip_model)    as pos_info
//              from    mms_stage_info    mms,
//                      merch_vnumber     mvn,
//                      equipment         eq
//              where   ( mms.merch_number ||
//                        mms.store_number || 
//                        mms.terminal_number ) = :TerminalId and
//                      mvn.vnumber = mms.vnum and
//                      eq.equip_model(+) = mvn.equip_model          
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(eq.equip_descriptor,mvn.equip_model)    as pos_info\n            from    mms_stage_info    mms,\n                    merch_vnumber     mvn,\n                    equipment         eq\n            where   ( mms.merch_number ||\n                      mms.store_number || \n                      mms.terminal_number ) =  :1  and\n                    mvn.vnumber = mms.vnum and\n                    eq.equip_model(+) = mvn.equip_model";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,TerminalId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AuthActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:339^11*/
        }
        
        if ( it != null )
        {
          ResultSet resultSet = it.getResultSet();
          if ( resultSet.next() )
          {
            PosInfo = resultSet.getString("pos_info");
          }
          resultSet.close();
          it.close();
        }
      }
      catch( Exception e )
      {
        logEntry("loadPOSInfo()",e.toString());
        PosInfo = "Failed to load POS information";
      }        
      finally
      {
        try{ it.close(); } catch( Exception e ) {}
      }
    }
    
    protected void resolveAuthRecId()
    {
      int       recCount  = 0;
      long      recId     = 0L;   // temp rec id
      
      try
      {
        if ( RecId == 0L )
        {
          System.out.println("getting match count");//@
          // count number of matching rows
          /*@lineinfo:generated-code*//*@lineinfo:375^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(auth.merchant_number) 
//              from    tc33_authorization  auth
//              where   auth.merchant_number = :MerchantId and
//                      auth.transaction_date = :TranDate and
//                      auth.card_number = :CardNumber and
//                      auth.authorization_code = :AuthCode and
//                      auth.rec_id is null
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(auth.merchant_number)  \n            from    tc33_authorization  auth\n            where   auth.merchant_number =  :1  and\n                    auth.transaction_date =  :2  and\n                    auth.card_number =  :3  and\n                    auth.authorization_code =  :4  and\n                    auth.rec_id is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.AuthActivityDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   __sJT_st.setDate(2,TranDate);
   __sJT_st.setString(3,CardNumber);
   __sJT_st.setString(4,AuthCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:384^11*/
          
          System.out.println("match count = " + recCount);//@
          
          // if there is only one, then perform the update
          if ( recCount == 1 )
          {
            System.out.println("getting sequence");//@
            /*@lineinfo:generated-code*//*@lineinfo:392^13*/

//  ************************************************************
//  #sql [Ctx] { select  tc33_auth_sequence.nextval 
//                from    dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  tc33_auth_sequence.nextval  \n              from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.AuthActivityDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:396^13*/  
            System.out.println("updating auth table");//@
            /*@lineinfo:generated-code*//*@lineinfo:398^13*/

//  ************************************************************
//  #sql [Ctx] { update tc33_authorization
//                set rec_id = :recId
//                where   merchant_number = :MerchantId and
//                        transaction_date = :TranDate and
//                        card_number = :CardNumber and
//                        authorization_code = :AuthCode
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update tc33_authorization\n              set rec_id =  :1 \n              where   merchant_number =  :2  and\n                      transaction_date =  :3  and\n                      card_number =  :4  and\n                      authorization_code =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setLong(2,MerchantId);
   __sJT_st.setDate(3,TranDate);
   __sJT_st.setString(4,CardNumber);
   __sJT_st.setString(5,AuthCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:406^13*/
            System.out.println("committing changes");//@
            /*@lineinfo:generated-code*//*@lineinfo:408^13*/

//  ************************************************************
//  #sql [Ctx] { commit
//               };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:411^13*/
            
            // if the database was updated
            // then update the class member
            System.out.println("assigning class value");//@
            RecId = recId;
          }
        }          
      }
      catch( Exception e )
      {
        logEntry( "resolveAuthRecId(" + String.valueOf(MerchantId) +
                    "," + String.valueOf(TranDate) +
                    "," + CardNumber + "," + AuthCode, 
                  e.toString() );
      }
    }
  }
  
  public class ReportTotals
  {
    public double        TotalAmount     = 0.0;
    public int           TotalCount      = 0;
    
    public ReportTotals( )
    {
    }
    
    public void add( DetailRow row )
    {
      TotalAmount += row.AuthAmount;
      TotalCount++;
    }
    
    public void clear( )
    {
      TotalAmount = 0.0;
      TotalCount  = 0;
    }
  }
  
  private long                  AuthRecId             = 0L;
  private ReportTotals          AuthTotals            = new ReportTotals();
  private boolean               FindAuth              = false;
  private String                FindAuthCode          = null;             
  private long                  FindCardNumber        = 0L;
  private String                FindCardNumberString  = null;             
  private boolean               FindDataValid         = false;
  private boolean               TridentOnly           = false;
  
  //@+  temp until index is created
  private long AuthDetailMerchantId   = 0L;
  private Date AuthDetailTranDate     = null;
  //@-
  
  public AuthActivityDataBean( )
  {
    super(true);  // enable FieldBean support
  }
  
  private long startAuthReportLog(Date beginDate, Date endDate)
  {
    long recIndex = 0L;
    // insert a record into the auth_report_log table to mark when this report
    // was started
    
    try
    {
      // first get a record index
      /*@lineinfo:generated-code*//*@lineinfo:480^7*/

//  ************************************************************
//  #sql [Ctx] { select  auth_report_log_seq.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  auth_report_log_seq.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.AuthActivityDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recIndex = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:485^7*/
    
      // insert the record
      /*@lineinfo:generated-code*//*@lineinfo:488^7*/

//  ************************************************************
//  #sql [Ctx] { insert into auth_report_log
//          (
//            record_index,
//            login_name,
//            hierarchy_node,
//            begin_date,
//            end_date,
//            date_report_started
//          )
//          values
//          (
//            :recIndex,
//            :ReportUserBean.getUserLoginId(),
//            :getReportHierarchyNode(),
//            :beginDate,
//            :endDate,
//            sysdate
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1108 = ReportUserBean.getUserLoginId();
 long __sJT_1109 = getReportHierarchyNode();
   String theSqlTS = "insert into auth_report_log\n        (\n          record_index,\n          login_name,\n          hierarchy_node,\n          begin_date,\n          end_date,\n          date_report_started\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n          sysdate\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recIndex);
   __sJT_st.setString(2,__sJT_1108);
   __sJT_st.setLong(3,__sJT_1109);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:508^7*/
    }
    catch(Exception e)
    {
      logEntry("startAuthReportLog()", e.toString());
    }
    
    return recIndex;
  }
  
  private void completeAuthReportLog(long recIndex)
  {
    // update record in database to show completed date
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:523^7*/

//  ************************************************************
//  #sql [Ctx] { update  auth_report_log
//          set     date_report_completed = sysdate
//          where   record_index = :recIndex
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  auth_report_log\n        set     date_report_completed = sysdate\n        where   record_index =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recIndex);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:528^7*/
    }
    catch(Exception e)
    {
      logEntry("completeAuthReportLog(" + recIndex + ")", e.toString());
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    Field         field;
    
    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");    
    
    if ( HttpHelper.getInt(request,"reportType",RT_SUMMARY) == RT_DETAILS )
    {
      fgroup.deleteField("portfolioId");
      fgroup.deleteField("zip2Node");
      fgroup.add( new NumberField("terminalNumber", "Terminal Number (optional)", 4, 4, true, 0 ) );
    }      
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    if ( getReportType() == RT_SUMMARY )
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"VMC Appr Cnt\",");
      line.append("\"VMC Appr Amt\",");
      line.append("\"VMC Decl Cnt\",");
      line.append("\"VMC Decl Amt\",");
      line.append("\"VMC Total Cnt\",");
      line.append("\"VMC Total Amt\",");
      line.append("\"Decl/Appr Ratio\"");
    }
    else    // RT_DETAILS
    {
      line.append("\"Terminal Id\",");
      line.append("\"Tran Date\",");
      line.append("\"Tran Time\",");
      line.append("\"POS Cond.\",");
      line.append("\"Term Fmt\",");
      line.append("\"Card Number\",");
      line.append("\"MCC\",");
      line.append("\"Card Type\",");
      line.append("\"Tran Amount\",");
      line.append("\"Exp Date\",");
      line.append("\"Response Code\",");
      line.append("\"Auth Code\",");
      line.append("\"Host\",");
      line.append("\"Line Type\",");
      line.append("\"SRC\",");
      line.append("\"Station ID\",");
      line.append("\"AVS\",");
      line.append("\"CVV/CVV2\",");
      line.append("\"PS/2000\",");
      line.append("\"Tran ID\",");
      line.append("\"Val Code\",");
      line.append("\"Product Code\"");
    }      
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    if ( getReportType() == RT_SUMMARY )
    {
      MesOrgSummaryEntry       record    = (MesOrgSummaryEntry)obj;
    
      line.append( record.getHierarchyNode() );
      line.append( ",\"" );
      line.append( record.getOrgName() );
      line.append( "\"," );
      line.append( record.getCount(0) );
      line.append( "," );
      line.append( record.getAmount(0) );
      line.append( "," );
      line.append( record.getCount(1) );
      line.append( "," );
      line.append( record.getAmount(1) );
      line.append( "," );
      line.append( record.getCount(2) );
      line.append( "," );
      line.append( record.getAmount(2) );
      line.append( "," );
      line.append( record.getCountRatio(NumeratorIndexes, DenominatorIndexes) );
    }
    else      // RT_DETAILS
    {
      DetailRow       record    = (DetailRow)obj;
      
      line.append("'");
      line.append( record.TerminalId );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate(record.TranTS,"MM/dd/yyyy") );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate(record.TranTS,"HH:mm:ss") );
      line.append( "," );
      line.append( record.PosCondCode );
      line.append( ",\"" );
      line.append( record.TerminalFormatCode );
      line.append( "\",\"" );
      line.append( record.CardNumber );
      line.append( "\"," );
      line.append( record.SicCode );
      line.append( ",\"" );
      line.append( record.CardType );
      line.append( "\"," );
      line.append( record.AuthAmount );
      line.append( ",\"" );
      line.append( record.ExpirationDate );
      line.append( "\",\"" );
      line.append( record.ResponseCode );
      line.append( "\",\"" );
      line.append( record.AuthCode );
      line.append( "\",\"" );
      line.append( record.getHostName() );
      line.append( "\",\"" );
      line.append( record.LineType );
      line.append( "\",\"" );
      line.append( record.StandInAdviceCode );
      line.append( "\",\"" );
      line.append( record.StationId );
      line.append( "\",\"" );
      line.append( record.AVSResult );
      line.append( "\",\"" );
      line.append( record.CVVResult );
      line.append("/");
      line.append( record.CVV2Result );
      line.append( "\",\"" );
      line.append( record.AuthCharInd );
      line.append( "\",\"" );
      line.append( record.TranId );
      line.append( "\",\"" );
      line.append( record.TranValCode );
      line.append( "\",\"" );
      line.append( record.AuthProductCode );
      line.append( "\"" );
    }      
  }
  
  public String getCardType( String cardNumber )
  {
    int       cardBin;
    String    retVal; 
    
    try
    {
      retVal = TridentTools.decodeVisakCardType(cardNumber);
    }
    catch( NumberFormatException e )
    {
      retVal = mesConstants.VITAL_ME_AP_ERROR;
    }
    return( retVal );
  }
  
  public String getDownloadFilenameBase()
  {
    Date                    beginDate = getReportDateBegin();
    Date                    endDate   = getReportDateEnd();
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_auth_log_");
    
    if ( TridentOnly )
    {
      filename.append("trident_only_");
    }
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(beginDate,"MMddyy") );
    if ( beginDate.equals(endDate) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(endDate,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public String getAuthHost( String authFileSource, int bankNumber, String developerInfo )
  {
    String      retVal    = "N/A";
    
    try
    {
      if( authFileSource.equals("tauth") )
      {
        if ( developerInfo != null && developerInfo.equals("vcauth") )
        {
          retVal = "Trident-V";
        }
        else
        {
          retVal = "Trident";            
        }
      }
      else if ( authFileSource.equals("tc33") || 
                authFileSource.equals("adf") )
      {
        switch( bankNumber )
        {
          case mesConstants.BANK_ID_CERTEGY:
            retVal = "Certegy";
            break;
            
          default:
            retVal = "Vital";
            break;
        }
      }
      else if ( authFileSource.equals("pti") )
      {
        retVal = "Paymentec";
      }
      else if ( authFileSource.equals("c_auth") ||
                authFileSource.equals("e_auth") ||
                authFileSource.equals("l_auth") )
      {
        retVal = "GPS";
      }
    }
    catch( Exception e )
    {
      // ignore
    }
    return( retVal );
  }
  
  public String getFindAuthCode( )
  {
    return( (FindAuthCode == null) ? "" : FindAuthCode );
  }
  
  public long getFindCardNumber()
  {
    return( FindCardNumber );
  }
  
  public ReportTotals getReportTotals( )
  {
    return(AuthTotals);
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public boolean isFindAuthData( )
  {
    return( (FindAuth == true) && 
            (FindDataValid == true) && 
            (getReportType() == RT_DETAILS) );
  }
  
  public DetailRow loadAuthDetailRecord( String tridentTranId )
  {
    long          recId     = 0L;
    DetailRow     retVal    = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:807^7*/

//  ************************************************************
//  #sql [Ctx] { select  ai.rec_id, auth.merchant_number, auth.transaction_date
//           
//          from    tc33_auth_trident_index   ai,
//                  tc33_authorization        auth
//          where   ai.trident_transaction_id = :tridentTranId and
//                  auth.rec_id = ai.rec_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ai.rec_id, auth.merchant_number, auth.transaction_date\n          \n        from    tc33_auth_trident_index   ai,\n                tc33_authorization        auth\n        where   ai.trident_transaction_id =  :1  and\n                auth.rec_id = ai.rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.AuthActivityDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tridentTranId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   AuthDetailMerchantId = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   AuthDetailTranDate = (java.sql.Date)__sJT_rs.getDate(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:815^7*/
      retVal = loadAuthDetailRecord( recId );
    }
    catch( Exception e )
    {
      // ingore, record does not exist, return null
    }
    return( retVal );
  }
  
  public DetailRow loadAuthDetailRecord( long recId )
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    DetailRow           retVal      = null;
    
    try
    {
      // store the current record id, used to lookup linked items
      AuthRecId = recId;    

      /*@lineinfo:generated-code*//*@lineinfo:836^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  auth.rec_id                                   as rec_id,
//                  auth.merchant_number                          as merchant_number,
//                  mf.dba_name                                   as dba_name,
//                  mf.dmcity                                     as dba_city,
//                  mf.dmstate                                    as dba_state,
//                  mf.sic_code                                   as dba_sic_code,
//                  auth.terminal_id                              as terminal_id, 
//                  auth.transaction_date                         as tran_date,
//                  auth.transaction_time                         as tran_ts,
//                  decode( lft.file_prefix,
//                          'tauth','java-fill-0',
//                          'tc33','GMT',
//                          'adf','GMT',
//                          'UNK' )                               as tran_time_zone,
//                  lft.file_prefix                               as file_source,                        
//                  auth.card_number                              as card_number,
//                  auth.card_number_enc                          as card_number_enc,
//                  auth.card_type                                as card_type,
//                  auth.AUTHORIZATION_CODE                       as auth_code, 
//                  auth.AUTHORIZED_AMOUNT                        as auth_amount,
//                  nvl(auth.secondary_amount,-1)                 as secondary_amount,
//                  nvl( auth.EXPIRATION_DATE_incoming,
//                       to_char(auth.expiration_date,'MMyy'))    as exp_date,
//                  auth.MERCHANT_CATEGORY_CODE                   as sic_code,
//                  auth.avs_result                               as avs_result,
//                  auth.cvv_result                               as cvv_result,
//                  auth.cvv2_request_data                        as cvv2_request,
//                  auth.cvv2_result                              as cvv2_result,
//                  auth.COMMUNICATION_LINE_TYPE                  as line_type,
//                  auth.response_code                            as response_code,
//                  auth.payment_service_transaction_id           as tran_id,
//                  auth.VALIDATION_CODE                          as val_code,
//                  auth.aquirer_station_id                       as station_id,
//                  auth.pos_condition_code                       as pos_cond_code,
//                  auth.auth_characteristics_ind                 as auth_char_ind,
//                  auth.terminal_format_code                     as term_fmt_code,
//                  auth.STND_IN_PROCESSING_ADVICE_CODE           as stand_in_code,
//                  -- expanded (trident) data
//                  pccd.pos_condition_desc                       as pos_cond_code_desc,
//                  auth.requested_aci                            as req_aci,
//                  rad.requested_aci_desc                        as req_aci_desc,
//                  auth.currency_code                            as currency_code,
//                  auth.country_code                             as country_code,
//                  ccd.country_code                              as country_code_desc,
//                  auth.device_code                              as dev_code,
//                  auth.moto_ecommerce_indicator                 as ecomm_ind,
//                  med.moto_ecommerce_desc                       as ecomm_ind_desc,
//                  auth.auth_response_text                       as resp_text,
//                  auth.comm_card_request_indicator              as comm_card_req_ind,
//                  auth.comm_card_response_indicator             as comm_card_resp_ind,
//                  ccrd.comm_card_response_desc                  as comm_card_resp_desc,
//                  auth.industry_code                            as industry_code,
//                  auth.retrieval_refrence_number                as retr_ref_num,
//                  auth.trident_transaction_id                   as trident_tran_id,
//                  auth.transaction_code_alpha                   as tran_code,
//                  mtd.message_type_desc                         as tran_code_desc,
//                  nvl(emd.pos_entry_desc,auth.pos_entry_mode_code) as pos_entry_mode,
//                  substr(auth.card_number,1,6)                  as issuer_bin,
//                  nvl(cbd.bank_name,'Not On File')              as issuer_name,
//                  nvl(cbd.phone,'Not On File')                  as issuer_phone,
//                  decode( auth.retrieval_refrence_number,
//                          null,'Not Available',
//                          substr(auth.retrieval_refrence_number,-least(6,length('12345')) )
//                        )
//                                                                as system_trace_number,
//                  auth.acquirer_bin                             as acq_bin,
//                  aac.response_message                          as avs_result_desc,
//                  arc.response_desc                             as response_desc,
//                  sicd.stand_in_desc                            as stand_in_desc,
//                  acid.aci_desc                                 as aci_desc,
//                  dcd.device_code_desc                          as dev_code_desc,
//                  auth.product_level_result_code                as auth_product_code,
//                  nvl(pcd.product_desc,'Not On File')           as auth_product_desc,
//                  nvl(auth.auth_reversed,0)                     as auth_reversed,
//                  substr(auth.load_filename,
//                         (length(lft.file_prefix)+1),4)         as file_bank_number,
//                  lower(nvl(auth.developer_id,'nodata'))        as developer_id
//          from    tc33_authorization            auth,
//                  pos_entry_mode_desc           emd,
//                  card_bin_data                 cbd,
//                  mif                           mf,
//                  mes_load_file_types           lft,
//                  tc33_authorization_avs_code   aac,
//                  tc33_authorization_resp_code  arc,
//                  tc33_auth_stand_in_code_desc  sicd,
//                  tc33_auth_comm_card_resp_desc ccrd,
//                  tc33_authorization_aci_desc   acid,
//                  tc33_auth_req_aci_desc        rad,
//                  tc33_auth_device_code_desc    dcd,
//                  tc33_auth_pos_cond_code_desc  pccd,
//                  tc33_auth_moto_ecomm_desc     med,
//                  tc33_auth_msg_type_desc       mtd,
//                  tc33_auth_prod_code_desc      pcd,
//                  country                       ccd
//          where   auth.merchant_number = :AuthDetailMerchantId and
//                  auth.transaction_date = :AuthDetailTranDate and
//                  auth.rec_id = :recId and
//                  emd.pos_entry_code(+) = 
//                    to_char(lpad( substr(auth.pos_entry_mode_code,1,length(auth.pos_entry_mode_code)-1 ), 2, '0' )) and
//                  cbd.bin(+) = substr(auth.card_number,1,6) and
//                  mf.merchant_number(+) = auth.merchant_number and
//                  lft.file_prefix = substr(auth.load_filename,1,length(lft.file_prefix)) and
//                  is_number(substr(auth.load_filename,(length(lft.file_prefix)+1),4)) != 0 and
//                  aac.avs_result_code(+) = nvl(auth.avs_result,'none') and
//                  arc.response_code(+) = nvl(auth.response_code,'none') and
//                  sicd.stand_in_code(+) = nvl(auth.stnd_in_processing_advice_code,'none') and
//                  ccrd.comm_card_response_indicator(+) = nvl(auth.comm_card_response_indicator,'none') and
//                  acid.aci(+) = nvl(auth.auth_characteristics_ind,'none') and
//                  rad.requested_aci(+) = nvl(auth.requested_aci,'none') and
//                  rad.card_type(+) = auth.card_type and
//                  dcd.device_code(+) = nvl(auth.device_code,'none') and
//                  pccd.pos_condition_code(+) = lpad(nvl(auth.pos_condition_code,'99'),2,'0') and
//                  med.moto_ecommerce_indicator(+) = nvl(auth.moto_ecommerce_indicator,'0') and
//                  mtd.message_type(+) = nvl(auth.transaction_code_alpha,'none') and
//                  pcd.product_code(+) = nvl(auth.product_level_result_code,'none') and
//                  ccd.iso_country_code(+) = auth.country_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  auth.rec_id                                   as rec_id,\n                auth.merchant_number                          as merchant_number,\n                mf.dba_name                                   as dba_name,\n                mf.dmcity                                     as dba_city,\n                mf.dmstate                                    as dba_state,\n                mf.sic_code                                   as dba_sic_code,\n                auth.terminal_id                              as terminal_id, \n                auth.transaction_date                         as tran_date,\n                auth.transaction_time                         as tran_ts,\n                decode( lft.file_prefix,\n                        'tauth','java-fill-0',\n                        'tc33','GMT',\n                        'adf','GMT',\n                        'UNK' )                               as tran_time_zone,\n                lft.file_prefix                               as file_source,                        \n                auth.card_number                              as card_number,\n                auth.card_number_enc                          as card_number_enc,\n                auth.card_type                                as card_type,\n                auth.AUTHORIZATION_CODE                       as auth_code, \n                auth.AUTHORIZED_AMOUNT                        as auth_amount,\n                nvl(auth.secondary_amount,-1)                 as secondary_amount,\n                nvl( auth.EXPIRATION_DATE_incoming,\n                     to_char(auth.expiration_date,'MMyy'))    as exp_date,\n                auth.MERCHANT_CATEGORY_CODE                   as sic_code,\n                auth.avs_result                               as avs_result,\n                auth.cvv_result                               as cvv_result,\n                auth.cvv2_request_data                        as cvv2_request,\n                auth.cvv2_result                              as cvv2_result,\n                auth.COMMUNICATION_LINE_TYPE                  as line_type,\n                auth.response_code                            as response_code,\n                auth.payment_service_transaction_id           as tran_id,\n                auth.VALIDATION_CODE                          as val_code,\n                auth.aquirer_station_id                       as station_id,\n                auth.pos_condition_code                       as pos_cond_code,\n                auth.auth_characteristics_ind                 as auth_char_ind,\n                auth.terminal_format_code                     as term_fmt_code,\n                auth.STND_IN_PROCESSING_ADVICE_CODE           as stand_in_code,\n                -- expanded (trident) data\n                pccd.pos_condition_desc                       as pos_cond_code_desc,\n                auth.requested_aci                            as req_aci,\n                rad.requested_aci_desc                        as req_aci_desc,\n                auth.currency_code                            as currency_code,\n                auth.country_code                             as country_code,\n                ccd.country_code                              as country_code_desc,\n                auth.device_code                              as dev_code,\n                auth.moto_ecommerce_indicator                 as ecomm_ind,\n                med.moto_ecommerce_desc                       as ecomm_ind_desc,\n                auth.auth_response_text                       as resp_text,\n                auth.comm_card_request_indicator              as comm_card_req_ind,\n                auth.comm_card_response_indicator             as comm_card_resp_ind,\n                ccrd.comm_card_response_desc                  as comm_card_resp_desc,\n                auth.industry_code                            as industry_code,\n                auth.retrieval_refrence_number                as retr_ref_num,\n                auth.trident_transaction_id                   as trident_tran_id,\n                auth.transaction_code_alpha                   as tran_code,\n                mtd.message_type_desc                         as tran_code_desc,\n                nvl(emd.pos_entry_desc,auth.pos_entry_mode_code) as pos_entry_mode,\n                substr(auth.card_number,1,6)                  as issuer_bin,\n                nvl(cbd.bank_name,'Not On File')              as issuer_name,\n                nvl(cbd.phone,'Not On File')                  as issuer_phone,\n                decode( auth.retrieval_refrence_number,\n                        null,'Not Available',\n                        substr(auth.retrieval_refrence_number,-least(6,length('12345')) )\n                      )\n                                                              as system_trace_number,\n                auth.acquirer_bin                             as acq_bin,\n                aac.response_message                          as avs_result_desc,\n                arc.response_desc                             as response_desc,\n                sicd.stand_in_desc                            as stand_in_desc,\n                acid.aci_desc                                 as aci_desc,\n                dcd.device_code_desc                          as dev_code_desc,\n                auth.product_level_result_code                as auth_product_code,\n                nvl(pcd.product_desc,'Not On File')           as auth_product_desc,\n                nvl(auth.auth_reversed,0)                     as auth_reversed,\n                substr(auth.load_filename,\n                       (length(lft.file_prefix)+1),4)         as file_bank_number,\n                lower(nvl(auth.developer_id,'nodata'))        as developer_id\n        from    tc33_authorization            auth,\n                pos_entry_mode_desc           emd,\n                card_bin_data                 cbd,\n                mif                           mf,\n                mes_load_file_types           lft,\n                tc33_authorization_avs_code   aac,\n                tc33_authorization_resp_code  arc,\n                tc33_auth_stand_in_code_desc  sicd,\n                tc33_auth_comm_card_resp_desc ccrd,\n                tc33_authorization_aci_desc   acid,\n                tc33_auth_req_aci_desc        rad,\n                tc33_auth_device_code_desc    dcd,\n                tc33_auth_pos_cond_code_desc  pccd,\n                tc33_auth_moto_ecomm_desc     med,\n                tc33_auth_msg_type_desc       mtd,\n                tc33_auth_prod_code_desc      pcd,\n                country                       ccd\n        where   auth.merchant_number =  :1  and\n                auth.transaction_date =  :2  and\n                auth.rec_id =  :3  and\n                emd.pos_entry_code(+) = \n                  to_char(lpad( substr(auth.pos_entry_mode_code,1,length(auth.pos_entry_mode_code)-1 ), 2, '0' )) and\n                cbd.bin(+) = substr(auth.card_number,1,6) and\n                mf.merchant_number(+) = auth.merchant_number and\n                lft.file_prefix = substr(auth.load_filename,1,length(lft.file_prefix)) and\n                is_number(substr(auth.load_filename,(length(lft.file_prefix)+1),4)) != 0 and\n                aac.avs_result_code(+) = nvl(auth.avs_result,'none') and\n                arc.response_code(+) = nvl(auth.response_code,'none') and\n                sicd.stand_in_code(+) = nvl(auth.stnd_in_processing_advice_code,'none') and\n                ccrd.comm_card_response_indicator(+) = nvl(auth.comm_card_response_indicator,'none') and\n                acid.aci(+) = nvl(auth.auth_characteristics_ind,'none') and\n                rad.requested_aci(+) = nvl(auth.requested_aci,'none') and\n                rad.card_type(+) = auth.card_type and\n                dcd.device_code(+) = nvl(auth.device_code,'none') and\n                pccd.pos_condition_code(+) = lpad(nvl(auth.pos_condition_code,'99'),2,'0') and\n                med.moto_ecommerce_indicator(+) = nvl(auth.moto_ecommerce_indicator,'0') and\n                mtd.message_type(+) = nvl(auth.transaction_code_alpha,'none') and\n                pcd.product_code(+) = nvl(auth.product_level_result_code,'none') and\n                ccd.iso_country_code(+) = auth.country_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AuthDetailMerchantId);
   __sJT_st.setDate(2,AuthDetailTranDate);
   __sJT_st.setLong(3,recId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.AuthActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:954^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        retVal = new DetailRow(resultSet);
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadAuthDetailRecord(" + String.valueOf(recId) + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( retVal );
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    int                           orgBankNum        = 0;
    ResultSet                     resultSet         = null;
    DetailRow                     row               = null;
    StringBuffer                  tempCard          = null;
    int                           tridentOnly       = (TridentOnly ? 1 : 0);
    
    try
    {
      // start logging the use of this report
      long recIndex = startAuthReportLog(beginDate, endDate);
      
      // empty the current contents
      AuthTotals.clear();
      ReportRows.clear();
    
      // set the org bank number
      orgBankNum = (int)(orgIdToHierarchyNode(orgId)/100000L);
      if ( orgBankNum > 9999 )
      {
        orgBankNum = 0;
      }
    
      if ( FindAuth == true )
      {
        if ( FindDataValid == true )
        {
          tempCard = new StringBuffer();
          
          if ( FindCardNumberString != null )
          {
            tempCard.append(FindCardNumberString);
            for( int i = 4; i < (int)Math.min(6,FindCardNumberString.length()); ++i )
            {
              if ( Character.isDigit( tempCard.charAt(i) ) )
              {
                tempCard.setCharAt(i,'x');
              }
            }
          }

          /*@lineinfo:generated-code*//*@lineinfo:1018^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ORDERED USE_NL (auth) FIRST_ROWS */
//                      auth.rec_id                                   as rec_id,
//                      auth.merchant_number                          as merchant_number,
//                      auth.terminal_id                              as terminal_id,
//                      auth.transaction_date                         as tran_date,
//                      auth.transaction_time                         as tran_ts,
//                      auth.card_number                              as card_number,
//                      auth.card_number_enc                          as card_number_enc,
//                      auth.card_type                                as card_type,
//                      auth.AUTHORIZATION_CODE                       as auth_code, 
//                      auth.AUTHORIZED_AMOUNT                        as auth_amount,
//                      nvl(auth.secondary_amount,-1)                 as secondary_amount,
//                      nvl( auth.EXPIRATION_DATE_incoming,
//                           to_char(auth.expiration_date,'MMyy'))    as exp_date,
//                      auth.MERCHANT_CATEGORY_CODE                   as sic_code,
//                      auth.avs_result                               as avs_result,
//                      auth.cvv_result                               as cvv_result,
//                      auth.cvv2_request_data                        as cvv2_request,
//                      auth.cvv2_result                              as cvv2_result,
//                      auth.COMMUNICATION_LINE_TYPE                  as line_type,
//                      auth.response_code                            as response_code,
//                      auth.payment_service_transaction_id           as tran_id,
//                      auth.VALIDATION_CODE                          as val_code,
//                      auth.aquirer_station_id                       as station_id,
//                      auth.pos_condition_code                       as pos_cond_code,
//                      auth.auth_characteristics_ind                 as auth_char_ind,
//                      auth.terminal_format_code                     as term_fmt_code,
//                      auth.STND_IN_PROCESSING_ADVICE_CODE           as stand_in_code,
//                      auth.product_level_result_code                as auth_product_code,
//                      nvl(auth.auth_reversed,0)                     as auth_reversed,
//                      lft.file_prefix                               as file_source,
//                      substr(auth.load_filename,
//                             (length(lft.file_prefix)+1),4)         as file_bank_number,
//                      lower(nvl(auth.developer_id,'nodata'))        as developer_id
//              from    group_merchant        gm,
//                      tc33_authorization    auth,
//                      mes_load_file_types   lft
//              where   gm.org_num = :orgId and
//                      auth.merchant_number = gm.merchant_number and 
//                      auth.transaction_date between :beginDate and :endDate and
//                      ( :FindCardNumberString is null or 
//                        auth.card_number like :FindCardNumberString or
//                        auth.card_number like :tempCard.toString() ) and
//                      ( :FindAuthCode is null or 
//                        auth.AUTHORIZATION_CODE = :FindAuthCode ) and
//                      lft.file_prefix = substr(auth.load_filename,1,length(lft.file_prefix)) and
//                      is_number(substr(auth.load_filename,(length(lft.file_prefix)+1),4)) != 0 and
//                      (
//                        :tridentOnly = 0 or
//                        (
//                          :tridentOnly = 1 and
//                          substr(auth.load_filename,1,5) = 'tauth'
//                        )
//                      ) 
//              order by auth.transaction_time
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1110 = tempCard.toString();
  try {
   String theSqlTS = "select  /*+ ORDERED USE_NL (auth) FIRST_ROWS */\n                    auth.rec_id                                   as rec_id,\n                    auth.merchant_number                          as merchant_number,\n                    auth.terminal_id                              as terminal_id,\n                    auth.transaction_date                         as tran_date,\n                    auth.transaction_time                         as tran_ts,\n                    auth.card_number                              as card_number,\n                    auth.card_number_enc                          as card_number_enc,\n                    auth.card_type                                as card_type,\n                    auth.AUTHORIZATION_CODE                       as auth_code, \n                    auth.AUTHORIZED_AMOUNT                        as auth_amount,\n                    nvl(auth.secondary_amount,-1)                 as secondary_amount,\n                    nvl( auth.EXPIRATION_DATE_incoming,\n                         to_char(auth.expiration_date,'MMyy'))    as exp_date,\n                    auth.MERCHANT_CATEGORY_CODE                   as sic_code,\n                    auth.avs_result                               as avs_result,\n                    auth.cvv_result                               as cvv_result,\n                    auth.cvv2_request_data                        as cvv2_request,\n                    auth.cvv2_result                              as cvv2_result,\n                    auth.COMMUNICATION_LINE_TYPE                  as line_type,\n                    auth.response_code                            as response_code,\n                    auth.payment_service_transaction_id           as tran_id,\n                    auth.VALIDATION_CODE                          as val_code,\n                    auth.aquirer_station_id                       as station_id,\n                    auth.pos_condition_code                       as pos_cond_code,\n                    auth.auth_characteristics_ind                 as auth_char_ind,\n                    auth.terminal_format_code                     as term_fmt_code,\n                    auth.STND_IN_PROCESSING_ADVICE_CODE           as stand_in_code,\n                    auth.product_level_result_code                as auth_product_code,\n                    nvl(auth.auth_reversed,0)                     as auth_reversed,\n                    lft.file_prefix                               as file_source,\n                    substr(auth.load_filename,\n                           (length(lft.file_prefix)+1),4)         as file_bank_number,\n                    lower(nvl(auth.developer_id,'nodata'))        as developer_id\n            from    group_merchant        gm,\n                    tc33_authorization    auth,\n                    mes_load_file_types   lft\n            where   gm.org_num =  :1  and\n                    auth.merchant_number = gm.merchant_number and \n                    auth.transaction_date between  :2  and  :3  and\n                    (  :4  is null or \n                      auth.card_number like  :5  or\n                      auth.card_number like  :6  ) and\n                    (  :7  is null or \n                      auth.AUTHORIZATION_CODE =  :8  ) and\n                    lft.file_prefix = substr(auth.load_filename,1,length(lft.file_prefix)) and\n                    is_number(substr(auth.load_filename,(length(lft.file_prefix)+1),4)) != 0 and\n                    (\n                       :9  = 0 or\n                      (\n                         :10  = 1 and\n                        substr(auth.load_filename,1,5) = 'tauth'\n                      )\n                    ) \n            order by auth.transaction_time";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setString(4,FindCardNumberString);
   __sJT_st.setString(5,FindCardNumberString);
   __sJT_st.setString(6,__sJT_1110);
   __sJT_st.setString(7,FindAuthCode);
   __sJT_st.setString(8,FindAuthCode);
   __sJT_st.setInt(9,tridentOnly);
   __sJT_st.setInt(10,tridentOnly);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.AuthActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1075^11*/
        }
      }
      else
      {
        int termNum = getInt("terminalNumber",-1);      

        /*@lineinfo:generated-code*//*@lineinfo:1082^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ORDERED USE_NL (auth) FIRST_ROWS */
//                    auth.rec_id                                   as rec_id,
//                    auth.merchant_number                          as merchant_number,
//                    auth.terminal_id                              as terminal_id,
//                    auth.transaction_date                         as tran_date,
//                    auth.transaction_time                         as tran_ts,
//                    auth.card_number                              as card_number,
//                    auth.card_number_enc                          as card_number_enc,
//                    auth.card_type                                as card_type,
//                    auth.AUTHORIZATION_CODE                       as auth_code, 
//                    auth.AUTHORIZED_AMOUNT                        as auth_amount,
//                    nvl(auth.secondary_amount,-1)                 as secondary_amount,
//                    nvl( auth.EXPIRATION_DATE_incoming,
//                         to_char(auth.expiration_date,'MMyy'))    as exp_date,
//                    auth.MERCHANT_CATEGORY_CODE                   as sic_code,
//                    auth.avs_result                               as avs_result,
//                    auth.cvv_result                               as cvv_result,
//                    auth.cvv2_request_data                        as cvv2_request,
//                    auth.cvv2_result                              as cvv2_result,
//                    auth.COMMUNICATION_LINE_TYPE                  as line_type,
//                    auth.response_code                            as response_code,
//                    auth.payment_service_transaction_id           as tran_id,
//                    auth.VALIDATION_CODE                          as val_code,
//                    auth.aquirer_station_id                       as station_id,
//                    auth.pos_condition_code                       as pos_cond_code,
//                    auth.auth_characteristics_ind                 as auth_char_ind,
//                    auth.terminal_format_code                     as term_fmt_code,
//                    auth.STND_IN_PROCESSING_ADVICE_CODE           as stand_in_code,
//                    lft.file_prefix                               as file_source,
//                    auth.product_level_result_code                as auth_product_code,
//                    nvl(auth.auth_reversed,0)                     as auth_reversed,
//                    substr(auth.load_filename,
//                           (length(lft.file_prefix)+1),4)         as file_bank_number,
//                    lower(nvl(auth.developer_id,'nodata'))        as developer_id
//              from  tc33_authorization    auth,
//                    mes_load_file_types   lft
//              where auth.merchant_number in 
//                    ( 
//                      select merchant_number
//                      from   group_merchant
//                      where  org_num = :orgId 
//                    ) and
//                    auth.transaction_date between :beginDate and :endDate and
//                    lft.file_prefix = substr(auth.load_filename,1,length(lft.file_prefix)) and
//                    is_number(substr(auth.load_filename,(length(lft.file_prefix)+1),4)) != 0 and
//                    (
//                      :tridentOnly = 0 or
//                      (
//                        :tridentOnly = 1 and
//                        substr(auth.load_filename,1,5) = 'tauth'
//                      )
//                    ) and 
//                    ( 
//                      :termNum = -1 or 
//                      to_number(substr(auth.terminal_id,-4)) = :termNum 
//                    )
//            order by auth.transaction_time
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ORDERED USE_NL (auth) FIRST_ROWS */\n                  auth.rec_id                                   as rec_id,\n                  auth.merchant_number                          as merchant_number,\n                  auth.terminal_id                              as terminal_id,\n                  auth.transaction_date                         as tran_date,\n                  auth.transaction_time                         as tran_ts,\n                  auth.card_number                              as card_number,\n                  auth.card_number_enc                          as card_number_enc,\n                  auth.card_type                                as card_type,\n                  auth.AUTHORIZATION_CODE                       as auth_code, \n                  auth.AUTHORIZED_AMOUNT                        as auth_amount,\n                  nvl(auth.secondary_amount,-1)                 as secondary_amount,\n                  nvl( auth.EXPIRATION_DATE_incoming,\n                       to_char(auth.expiration_date,'MMyy'))    as exp_date,\n                  auth.MERCHANT_CATEGORY_CODE                   as sic_code,\n                  auth.avs_result                               as avs_result,\n                  auth.cvv_result                               as cvv_result,\n                  auth.cvv2_request_data                        as cvv2_request,\n                  auth.cvv2_result                              as cvv2_result,\n                  auth.COMMUNICATION_LINE_TYPE                  as line_type,\n                  auth.response_code                            as response_code,\n                  auth.payment_service_transaction_id           as tran_id,\n                  auth.VALIDATION_CODE                          as val_code,\n                  auth.aquirer_station_id                       as station_id,\n                  auth.pos_condition_code                       as pos_cond_code,\n                  auth.auth_characteristics_ind                 as auth_char_ind,\n                  auth.terminal_format_code                     as term_fmt_code,\n                  auth.STND_IN_PROCESSING_ADVICE_CODE           as stand_in_code,\n                  lft.file_prefix                               as file_source,\n                  auth.product_level_result_code                as auth_product_code,\n                  nvl(auth.auth_reversed,0)                     as auth_reversed,\n                  substr(auth.load_filename,\n                         (length(lft.file_prefix)+1),4)         as file_bank_number,\n                  lower(nvl(auth.developer_id,'nodata'))        as developer_id\n            from  tc33_authorization    auth,\n                  mes_load_file_types   lft\n            where auth.merchant_number in \n                  ( \n                    select merchant_number\n                    from   group_merchant\n                    where  org_num =  :1  \n                  ) and\n                  auth.transaction_date between  :2  and  :3  and\n                  lft.file_prefix = substr(auth.load_filename,1,length(lft.file_prefix)) and\n                  is_number(substr(auth.load_filename,(length(lft.file_prefix)+1),4)) != 0 and\n                  (\n                     :4  = 0 or\n                    (\n                       :5  = 1 and\n                      substr(auth.load_filename,1,5) = 'tauth'\n                    )\n                  ) and \n                  ( \n                     :6  = -1 or \n                    to_number(substr(auth.terminal_id,-4)) =  :7  \n                  )\n          order by auth.transaction_time";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,tridentOnly);
   __sJT_st.setInt(5,tridentOnly);
   __sJT_st.setInt(6,termNum);
   __sJT_st.setInt(7,termNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.AuthActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1141^9*/
      } 
      if ( it != null )
      {
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          row = new DetailRow( resultSet );
          ReportRows.addElement(row);
          AuthTotals.add(row);
        }
        resultSet.close();
        it.close();
      }        
      completeAuthReportLog(recIndex);
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public Vector loadLinkedAuths()
  {
    ResultSetIterator   it        = null;
    ResultSet           resultSet = null;
    Vector              retVal    = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1176^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rev.rec_id,
//                  rev.transaction_date
//          from    tc33_authorization    auth,
//                  tc33_authorization    rev
//          where   auth.rec_id = :AuthRecId and
//                  rev.merchant_number = auth.merchant_number and
//                  rev.transaction_date between (auth.transaction_date-28) and (auth.transaction_date+28) and
//                  rev.rec_id != auth.rec_id and     -- not the same record
//                  rev.retrieval_refrence_number = auth.retrieval_refrence_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rev.rec_id,\n                rev.transaction_date\n        from    tc33_authorization    auth,\n                tc33_authorization    rev\n        where   auth.rec_id =  :1  and\n                rev.merchant_number = auth.merchant_number and\n                rev.transaction_date between (auth.transaction_date-28) and (auth.transaction_date+28) and\n                rev.rec_id != auth.rec_id and     -- not the same record\n                rev.retrieval_refrence_number = auth.retrieval_refrence_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,AuthRecId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.AuthActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1187^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        if ( retVal == null )
        {
          retVal = new Vector();
        }
        AuthDetailTranDate = resultSet.getDate("transaction_date");
        retVal.add( loadAuthDetailRecord( resultSet.getLong("rec_id") ) );
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("loadLinkedAuths(" + AuthRecId + ")", e.toString());
    }
    finally
    {
      try { it.close(); } catch( Exception e ){}
    }              
    return( retVal );
  }                
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    int                           tridentOnly       = (TridentOnly ? 1 : 0);
    
    try
    {
      // start logging the use of this report
      long recIndex = startAuthReportLog(beginDate, endDate);
      
      if ( hasAssocDistricts() )
      // returns false unless the current node is an
      // association and it has districts under it.
      {
        if ( District == DISTRICT_NONE )
        {
          /*@lineinfo:generated-code*//*@lineinfo:1228^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      o.org_group                               as hierarchy_node,
//                      nvl(mf.district,:DISTRICT_UNASSIGNED)     as district,
//                      nvl(ad.district_desc,decode( mf.district,
//                                  null,'Unassigned',
//                                 ('District ' || to_char(mf.district, '0009')))) as org_name,
//                      sum(sm.vmc_auth_approved_count)           as auth_appr_count,
//                      sum(sm.vmc_auth_approved_amount)          as auth_appr_amount,
//                      sum(sm.vmc_auth_decline_count)            as auth_decl_count,
//                      sum(sm.vmc_auth_decline_amount)           as auth_decl_amount,
//                      sum(sm.vmc_auth_total_count)              as auth_total_count,
//                      sum(sm.vmc_auth_total_amount)             as auth_total_amount
//              from    organization              o,
//                      group_merchant            gm,
//                      group_rep_merchant        grm,
//                      tc33_risk_summary         sm,
//                      mif                       mf,
//                      assoc_districts           ad
//              where   o.org_num           = :orgId and
//                      gm.org_num          = o.org_num and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      sm.merchant_number(+) = mf.merchant_number and
//                      sm.transaction_date(+) between :beginDate and :endDate and
//                      (
//                        :tridentOnly = 0 or
//                        (
//                          :tridentOnly = 1 and
//                          substr(sm.load_filename,1,5) = 'tauth'
//                        )
//                      ) and
//                      ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                      ad.district(+)      = mf.district
//              group by  o.org_num, o.org_group, mf.district, ad.district_desc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    o.org_group                               as hierarchy_node,\n                    nvl(mf.district, :1 )     as district,\n                    nvl(ad.district_desc,decode( mf.district,\n                                null,'Unassigned',\n                               ('District ' || to_char(mf.district, '0009')))) as org_name,\n                    sum(sm.vmc_auth_approved_count)           as auth_appr_count,\n                    sum(sm.vmc_auth_approved_amount)          as auth_appr_amount,\n                    sum(sm.vmc_auth_decline_count)            as auth_decl_count,\n                    sum(sm.vmc_auth_decline_amount)           as auth_decl_amount,\n                    sum(sm.vmc_auth_total_count)              as auth_total_count,\n                    sum(sm.vmc_auth_total_amount)             as auth_total_amount\n            from    organization              o,\n                    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    tc33_risk_summary         sm,\n                    mif                       mf,\n                    assoc_districts           ad\n            where   o.org_num           =  :2  and\n                    gm.org_num          = o.org_num and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    sm.merchant_number(+) = mf.merchant_number and\n                    sm.transaction_date(+) between  :5  and  :6  and\n                    (\n                       :7  = 0 or\n                      (\n                         :8  = 1 and\n                        substr(sm.load_filename,1,5) = 'tauth'\n                      )\n                    ) and\n                    ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                    ad.district(+)      = mf.district\n            group by  o.org_num, o.org_group, mf.district, ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setInt(7,tridentOnly);
   __sJT_st.setInt(8,tridentOnly);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.reports.AuthActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1269^11*/
        }
        else    // a district was specified
        {
          /*@lineinfo:generated-code*//*@lineinfo:1273^11*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                        INDEX (gm pkgroup_merchant) 
//                     */
//                      o.org_num                                 as org_num,
//                      mf.merchant_number                        as hierarchy_node,
//                      mf.dba_name                               as org_name,
//                      :District                                 as district,
//                      sum(sm.vmc_auth_approved_count)           as auth_appr_count,
//                      sum(sm.vmc_auth_approved_amount)          as auth_appr_amount,
//                      sum(sm.vmc_auth_decline_count)            as auth_decl_count,
//                      sum(sm.vmc_auth_decline_amount)           as auth_decl_amount,
//                      sum(sm.vmc_auth_total_count)              as auth_total_count,
//                      sum(sm.vmc_auth_total_amount)             as auth_total_amount
//              from    group_merchant            gm,
//                      group_rep_merchant        grm,
//                      tc33_risk_summary         sm,
//                      mif                       mf,
//                      organization              o
//              where   gm.org_num          = :orgId and
//                      grm.user_id(+) = :AppFilterUserId and
//                      grm.merchant_number(+) = gm.merchant_number and
//                      ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                      mf.merchant_number  = gm.merchant_number and
//                      nvl(mf.district,-1) = :District and
//                      sm.merchant_number(+)  = mf.merchant_number and
//                      sm.transaction_date(+) between :beginDate and :endDate and
//                      (
//                        :tridentOnly = 0 or
//                        (
//                          :tridentOnly = 1 and
//                          substr(sm.load_filename,1,5) = 'tauth'
//                        )
//                      ) and
//                      o.org_group = mf.merchant_number
//              group by o.org_num, mf.merchant_number, mf.dba_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                      INDEX (gm pkgroup_merchant) \n                   */\n                    o.org_num                                 as org_num,\n                    mf.merchant_number                        as hierarchy_node,\n                    mf.dba_name                               as org_name,\n                     :1                                  as district,\n                    sum(sm.vmc_auth_approved_count)           as auth_appr_count,\n                    sum(sm.vmc_auth_approved_amount)          as auth_appr_amount,\n                    sum(sm.vmc_auth_decline_count)            as auth_decl_count,\n                    sum(sm.vmc_auth_decline_amount)           as auth_decl_amount,\n                    sum(sm.vmc_auth_total_count)              as auth_total_count,\n                    sum(sm.vmc_auth_total_amount)             as auth_total_amount\n            from    group_merchant            gm,\n                    group_rep_merchant        grm,\n                    tc33_risk_summary         sm,\n                    mif                       mf,\n                    organization              o\n            where   gm.org_num          =  :2  and\n                    grm.user_id(+) =  :3  and\n                    grm.merchant_number(+) = gm.merchant_number and\n                    ( not grm.user_id is null or  :4  = -1 ) and        \n                    mf.merchant_number  = gm.merchant_number and\n                    nvl(mf.district,-1) =  :5  and\n                    sm.merchant_number(+)  = mf.merchant_number and\n                    sm.transaction_date(+) between  :6  and  :7  and\n                    (\n                       :8  = 0 or\n                      (\n                         :9  = 1 and\n                        substr(sm.load_filename,1,5) = 'tauth'\n                      )\n                    ) and\n                    o.org_group = mf.merchant_number\n            group by o.org_num, mf.merchant_number, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,District);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setInt(8,tridentOnly);
   __sJT_st.setInt(9,tridentOnly);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.AuthActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1310^11*/
        }
      }
      else // just standard child report, no districts
      {
        /*@lineinfo:generated-code*//*@lineinfo:1315^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                       INDEX (gm pkgroup_merchant) 
//                    */
//                    o.org_num                                 as org_num,
//                    o.org_group                               as hierarchy_node,
//                    o.org_name                                as org_name,
//                    0                                         as district,
//                    sum(sm.vmc_auth_approved_count)           as auth_appr_count,
//                    sum(sm.vmc_auth_approved_amount)          as auth_appr_amount,
//                    sum(sm.vmc_auth_decline_count)            as auth_decl_count,
//                    sum(sm.vmc_auth_decline_amount)           as auth_decl_amount,
//                    sum(sm.vmc_auth_total_count)              as auth_total_count,
//                    sum(sm.vmc_auth_total_amount)             as auth_total_amount
//            from    parent_org                po,
//                    organization              o,
//                    group_merchant            gm,
//                    group_rep_merchant        grm,
//                    tc33_risk_summary         sm
//            where   po.parent_org_num   = :orgId and
//                    o.org_num           = po.org_num and
//                    gm.org_num(+)       = o.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    sm.merchant_number(+)  = gm.merchant_number and
//                    sm.transaction_date(+) between :beginDate and :endDate and
//                    (
//                      :tridentOnly = 0 or
//                      (
//                        :tridentOnly = 1 and
//                        substr(sm.load_filename,1,5) = 'tauth'
//                      )
//                    )
//            group by o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                     INDEX (gm pkgroup_merchant) \n                  */\n                  o.org_num                                 as org_num,\n                  o.org_group                               as hierarchy_node,\n                  o.org_name                                as org_name,\n                  0                                         as district,\n                  sum(sm.vmc_auth_approved_count)           as auth_appr_count,\n                  sum(sm.vmc_auth_approved_amount)          as auth_appr_amount,\n                  sum(sm.vmc_auth_decline_count)            as auth_decl_count,\n                  sum(sm.vmc_auth_decline_amount)           as auth_decl_amount,\n                  sum(sm.vmc_auth_total_count)              as auth_total_count,\n                  sum(sm.vmc_auth_total_amount)             as auth_total_amount\n          from    parent_org                po,\n                  organization              o,\n                  group_merchant            gm,\n                  group_rep_merchant        grm,\n                  tc33_risk_summary         sm\n          where   po.parent_org_num   =  :1  and\n                  o.org_num           = po.org_num and\n                  gm.org_num(+)       = o.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  sm.merchant_number(+)  = gm.merchant_number and\n                  sm.transaction_date(+) between  :4  and  :5  and\n                  (\n                     :6  = 0 or\n                    (\n                       :7  = 1 and\n                      substr(sm.load_filename,1,5) = 'tauth'\n                    )\n                  )\n          group by o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.AuthActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setInt(6,tridentOnly);
   __sJT_st.setInt(7,tridentOnly);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.reports.AuthActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1351^9*/
      }
      processSummaryData(it.getResultSet());
      it.close();
      
      completeAuthReportLog(recIndex);
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    long                    findNode        = 0L;

    // load the default report properties
    super.postHandleRequest(request);
    
    setSortOrder( HttpHelper.getInt(request,"sortOrder",SortByType.SB_NONE) );
    
    //@+
    java.util.Date javaDate = HttpHelper.getDate(request,"authDate","MM-dd-yyyy");
    if ( javaDate != null )
    {
      AuthDetailTranDate = new java.sql.Date( javaDate.getTime() );
    }      
    AuthDetailMerchantId = HttpHelper.getLong(request,"authMerchantId",0L);
    //@-
    
    TridentOnly = ( HttpHelper.getBoolean(request,"tridentOnly",false) &&
                    ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_TRIDENT_FILE_SUMMARY ) );
                    
    if ( usingDefaultReportDates() )
    {
      // default to the current day to support trident auths
      setReportDateBegin( new java.sql.Date( Calendar.getInstance().getTime().getTime() ) );
      setReportDateEnd( getReportDateBegin() );
    }
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
    }
    
    if( (FindAuth = HttpHelper.getBoolean(request,"findAuth",false)) == true )
    {
      FindAuthCode = HttpHelper.getString(request,"findAuthCode",null);
      try
      {
        // look for an already masked number 444444xxxxxx0001
        StringBuffer  decodeBuf = new StringBuffer(HttpHelper.getString(request,"findCardNum",""));
        for( int i = 6; i < decodeBuf.length(); ++i )
        {
          if ( decodeBuf.charAt(i) == 'X' ||
               decodeBuf.charAt(i) == 'x' )
          {
            decodeBuf.setCharAt(i,'0');   // change to a zero
          }
        }
        FindCardNumber = Long.parseLong(decodeBuf.toString());
        decodeBuf = null;
      }
      catch( NumberFormatException e )
      {
        FindCardNumber = 0L;
      }
      FindDataValid   = true;     // assume valid
      
      if ( FindCardNumber == 0L )
      {
        FindCardNumberString = null;
        
        if ( FindAuthCode == null )
        {
          addError("Must specify a card number OR auth code to search for.");
          FindDataValid = false;
        }          
      }
      else
      {
        // build the field mask
        StringBuffer  temp = new StringBuffer(Long.toString(FindCardNumber));
      
        for( int i = 6; i < temp.length(); ++i )
        {
          if ( i < 12 ) 
          {
            temp.setCharAt(i,'x');
          }
        }
        if( temp.length() < 16 )
        {
          temp.append("%");
        }
        FindCardNumberString = temp.toString();
      }        
      
      // only check for the search node if the current user is not a merchant
      if( isNodeMerchant( getReportHierarchyNodeDefault() ) == false )
      {
        // validate the find hierarchy node.  if it is not under the login
        // hierarchy node, the set the node to the current login node.
        findNode = HttpHelper.getLong(request,"findHierarchyNode",0L);
        if ( ! isNodeParentOfNode( getReportHierarchyNodeDefault(), findNode ) )
        {
          ErrorMessage.setLength(0);
          ErrorMessage.append( "Requested node " );
          ErrorMessage.append( findNode );
          ErrorMessage.append( " is invalid because it is not a child of " );
          ErrorMessage.append( getReportHierarchyNodeDefault() );
          addError( ErrorMessage.toString() );
          FindDataValid = false;
        }
        else 
        {
          // node is valid, so set the report node
          setReportHierarchyNode(findNode);
        
          // if the node specified is not an association or a merchant
          // node, then add a message for the user and disarm the query.
          if ( !isNodeAssociation(findNode) && !isNodeMerchant(findNode) )
          {
            ErrorMessage.setLength(0);
            ErrorMessage.append( "Requested node must be a merchant or association.  " );
            ErrorMessage.append( findNode );
            ErrorMessage.append( " is a group." );
            addError( ErrorMessage.toString() );
            FindDataValid = false;
          }
        }
      }        
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/