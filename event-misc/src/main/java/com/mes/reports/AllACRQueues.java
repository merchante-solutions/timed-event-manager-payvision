/*@lineinfo:filename=AllACRQueues*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.reports;

import java.sql.ResultSet;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class AllACRQueues extends DropDownTable
{
  public AllACRQueues()
  {
    ResultSetIterator it    = null;
    ResultSet         rs    = null;

    try
    {
      connect();

      addElement("-1", "All ACR Queues");

      /*@lineinfo:generated-code*//*@lineinfo:21^7*/

//  ************************************************************
//  #sql [Ctx] it = { select type, description
//          from q_types
//          where description like 'ACR%'
//          order by type asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select type, description\n        from q_types\n        where description like 'ACR%'\n        order by type asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AllACRQueues",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AllACRQueues",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:27^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        addElement(rs);
      }
    }
    catch(Exception e)
    {
      logEntry("AllServiceCallTypes()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

  }
}/*@lineinfo:generated-code*/