/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchProfEntry.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 11/27/00 3:04p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.util.StringTokenizer;

public class MerchProfEntry
{
  protected String                  ColumnLabel;
  protected int                     ContractExpenseType;
  protected String                  ExpenseColumnName;
  protected String                  IncomeColumnName;
  protected StringTokenizer         LabelTokenizer;
  protected String                  NetColumnName;
  protected String                  Reference;
  
  public MerchProfEntry( String colLabel, String incColName, String expColName, String netColName, String href, int contractExpenseType )
  {
    ColumnLabel           = colLabel;
    ContractExpenseType   = contractExpenseType;
    ExpenseColumnName     = expColName;
    IncomeColumnName      = incColName;
    NetColumnName         = netColName;
    Reference             = href;
    
    LabelTokenizer        = null;
  }
  
  public String getColumnLabel( )
  {
    return( ColumnLabel );
  }
  
  public int getContractExpenseType( )
  {
    return( ContractExpenseType );
  }
  
  public String getExpenseColumnName( )
  {
    return( ExpenseColumnName );
  }
  
  public String getFirstColumnLabelFieldName()
  {
    // reset the tokenizer to point to the first
    // entry in the ColumnLabel, then return the
    // next token.
    LabelTokenizer = new StringTokenizer( ColumnLabel.substring(1), "," );
    return( getNextColumnLabelFieldName() );
  }
  
  public String getIncomeColumnName( )
  {
    return( IncomeColumnName );
  }
  
  public String getNetColumnName( )
  {
    return( NetColumnName );
  }
  
  public String getNextColumnLabelFieldName()
  {
    String          retVal = null;
    
    try
    {
      retVal = LabelTokenizer.nextToken();
    }
    catch( Exception e )
    {
    }
    return( retVal );
  }
  
  public String getReference( )
  {
    return( Reference );
  }
  
  public boolean isColumnLabelFromDatabase( )
  {
    boolean     retVal = false;
    
    if ( ColumnLabel != null )
    {
      if ( ColumnLabel.charAt(0) == '@' )
      {
        retVal = true;
      }
    }
    return( retVal );
  }
  
  public void setColumnLabel( String colLabel )
  {
    ColumnLabel = colLabel;
  }
  
  public void setExpenseColumnName( String expColName )
  {
    ExpenseColumnName = expColName;
  }
  
  public void setIncomeColumnName( String incColName )
  {
    IncomeColumnName = incColName;
  }
  
  public void setNetColumnName( String netColName )
  {
    NetColumnName = netColName;
  }
  
  public void setReference( String href )
  {
    Reference = href;
  }
}
