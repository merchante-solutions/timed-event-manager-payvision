/*@lineinfo:filename=PCISummaryDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/IntlChargebackRetrievalDataBean.sqlj $

  Description:


  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000-2007,2008 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesHierarchy;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class PCISummaryDataBean extends ReportSQLJBean
{

  //public    String      LinkHolder      = "drill down";

  // create class log category
  static Logger log = Logger.getLogger(PCISummaryDataBean.class);

  public static class SummaryRow
  {
    public    String      Q_None          = null;
    public    String      Q_Incomplete    = null;
    public    String      Q_Complete      = null;
    public    String      S_Yes           = null;
    public    String      S_No            = null;
    public    String      S_InProgress    = null;
    public    String      S_NA            = null;
    public    long        HierarchyNode     = 0L;
    public    String      OrgName           = null;

    public SummaryRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      OrgName           = resultSet.getString("org_name");
      Q_None            = resultSet.getString("quiz_none");
      Q_Incomplete      = resultSet.getString("quiz_incomplete");
      Q_Complete        = resultSet.getString("quiz_complete");
      S_Yes             = resultSet.getString("scan_yes");
      S_No              = resultSet.getString("scan_no");
      S_InProgress      = resultSet.getString("scan_in_prog");
      S_NA              = resultSet.getString("scan_na");
    }
  }

  public PCISummaryDataBean( )
  {
    super(true);    // use field bean
  }

  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);

    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Q None\",");
    line.append("\"Q Incomplete\",");
    line.append("\"Q Complete\",");
    line.append("\"S Yes\",");
    line.append("\"S No\",");
    line.append("\"S In Progress\",");
    line.append("\"S N//A\",");

  }

  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    // clear the line buffer and add
    // the merchant specific data.
    line.setLength(0);

    SummaryRow  row = (SummaryRow) obj;

    line.append( encodeHierarchyNode(row.HierarchyNode) );
    line.append( ",\"" );
    line.append( row.OrgName );
    line.append( "\",\"" );
    line.append( row.Q_None );
    line.append( "\"," );
    line.append( row.Q_Incomplete );
    line.append( "," );
    line.append( row.Q_Complete );
    line.append( "," );
    line.append( row.S_Yes );
    line.append( "," );
    line.append( row.S_No );
    line.append( "," );
    line.append( row.S_InProgress);
    line.append( "," );
    line.append( row.S_NA );
  }

  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;

    if( fileFormat == FF_CSV )
    {
      retVal = true;
    }
    return(retVal);
  }

  public String getDownloadFilenameBase()
  {
    Date                    beginDate = getReportDateBegin();
    Calendar                cal       = Calendar.getInstance();
    Date                    endDate   = getReportDateEnd();
    StringBuffer            filename  = new StringBuffer("");

    filename.append( getReportHierarchyNode() );
    filename.append( "_pci_");

    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( beginDate,"MMddyy" ) );
    if ( beginDate.equals(endDate) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( endDate,"MMddyy" ) );
    }
    return ( filename.toString() );

  }

  //override this to capture default hierarchy assignment
  public long getReportOrgId( )
  {
    int       hierType  = getHierType();
    long      nodeId    = getReportHierarchyNode();

    if ( nodeId == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      setReportHierarchyNode( 394100000 );
      nodeId = 394100000;
    }

    long      retVal    = 0L;

    if ( hierType == MesHierarchy.HT_BANK_PORTFOLIOS )
    {
      retVal = hierarchyNodeToOrgId(nodeId);
    }
    else
    {
      retVal = nodeId;
    }

    return( retVal );
  }


  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator     it                = null;
    ResultSet             resultSet         = null;
    try
    {

      log.debug("loadData::nodeId = "+ nodeId );

      // empty the current contents
      ReportRows.clear();

      /*@lineinfo:generated-code*//*@lineinfo:205^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num       as org_num,
//                  o.org_group     as hierarchy_node,
//                  o.org_name      as org_name,
//                  sum(decode(nvl(quiz.result,'None'),
//                    'None', 1,
//                    'Reset', 1,
//                    0))           as quiz_none,
//                  sum(decode(nvl(quiz.result,'None'),
//                    'Incomplete', 1,
//                    0))           as quiz_incomplete,
//                  sum(decode(nvl(quiz.result,'None'),
//                    'Complete', 1,
//                    0))           as quiz_complete,
//                  sum(decode(nvl(pci.status, -1),
//                    1, 1,
//                    0))           as scan_yes,
//                  sum(decode(nvl(pci.status, -1),
//                    0, 1,
//                    0))           as scan_no,
//                  sum(decode(nvl(pci.status, -1),
//                    2, 1,
//                    0))           as scan_in_prog,
//                  sum(decode(nvl(pci.status, -1),
//                    -1, 1,
//                    0))           as scan_na
//          from    parent_org    po,
//                  organization  o,
//                  group_merchant gm,
//                  mif mf,
//                  quiz_history  quiz,
//                  pci_scan_compliance pci
//          where   po.parent_org_num = :nodeId
//                  and po.org_num = o.org_num
//                  and o.org_num = gm.org_num(+)
//                  and gm.merchant_number = mf.merchant_number
//                  and mf.date_stat_chgd_to_dcb is null
//                  and gm.merchant_number = quiz.merch_number(+)
//                  and quiz.date_taken(+) between :beginDate and (:endDate+1)
//                  and gm.merchant_number = pci.merchant_number(+)
//                  and pci.last_scan_date(+) between :beginDate and (:endDate+1)
//          group by  o.org_num,
//                    o.org_group,
//                    o.org_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num       as org_num,\n                o.org_group     as hierarchy_node,\n                o.org_name      as org_name,\n                sum(decode(nvl(quiz.result,'None'),\n                  'None', 1,\n                  'Reset', 1,\n                  0))           as quiz_none,\n                sum(decode(nvl(quiz.result,'None'),\n                  'Incomplete', 1,\n                  0))           as quiz_incomplete,\n                sum(decode(nvl(quiz.result,'None'),\n                  'Complete', 1,\n                  0))           as quiz_complete,\n                sum(decode(nvl(pci.status, -1),\n                  1, 1,\n                  0))           as scan_yes,\n                sum(decode(nvl(pci.status, -1),\n                  0, 1,\n                  0))           as scan_no,\n                sum(decode(nvl(pci.status, -1),\n                  2, 1,\n                  0))           as scan_in_prog,\n                sum(decode(nvl(pci.status, -1),\n                  -1, 1,\n                  0))           as scan_na\n        from    parent_org    po,\n                organization  o,\n                group_merchant gm,\n                mif mf,\n                quiz_history  quiz,\n                pci_scan_compliance pci\n        where   po.parent_org_num =  :1 \n                and po.org_num = o.org_num\n                and o.org_num = gm.org_num(+)\n                and gm.merchant_number = mf.merchant_number\n                and mf.date_stat_chgd_to_dcb is null\n                and gm.merchant_number = quiz.merch_number(+)\n                and quiz.date_taken(+) between  :2  and ( :3 +1)\n                and gm.merchant_number = pci.merchant_number(+)\n                and pci.last_scan_date(+) between  :4  and ( :5 +1)\n        group by  o.org_num,\n                  o.org_group,\n                  o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.PCISummaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.PCISummaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:250^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.add( new SummaryRow( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
}/*@lineinfo:generated-code*/