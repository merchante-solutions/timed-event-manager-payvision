/*@lineinfo:filename=BankRebateDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/BankRebateDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 8/30/02 3:22p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import sqlj.runtime.ResultSetIterator;

public class BankRebateDataBean extends ReportSQLJBean
{
  protected static final double       CBT_CASH_ADVANCE_FEE      = 0.50;
  
  public class RebateData implements Comparable
  {
    public int            CashAdvanceCount          = 0;
    public double         CashAdvanceFeeAmount      = 0.0;
    public double         DiscountIncome            = 0.0;
    public double         FeesExpense               = 0.0;
    public double         FeesIncome                = 0.0;
    public long           HierarchyNode             = 0L;
    public double         MinDiscAmount             = 0.0;
    public long           OrgId                     = 0L;
    public String         OrgName                   = null;
    public double         Rate                      = -1.0;
    public double         SalesVolume               = 0.0;
    public double         ServiceFee                = 0.0;
    
    // year to date totals
    public int            CashAdvanceCountYTD       = 0;
    public double         CashAdvanceFeeAmountYTD   = 0.0;
    public double         DiscountIncomeYTD         = 0.0;
    public double         FeesExpenseYTD            = 0.0;
    public double         FeesIncomeYTD             = 0.0;
    public double         SalesVolumeYTD            = 0.0;
    public double         ServiceFeeYTD             = 0.0;
    
    public RebateData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      OrgId             = resultSet.getLong("org_num");
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      OrgName           = resultSet.getString("org_name");
  
      if( ( resultSet.getInt("cash_advance") == 1 ) &&
          ( resultSet.getInt("local_cash_adv") == 0 ) ) 
      {
        CashAdvanceCount      = resultSet.getInt("num_cash_advance");
        CashAdvanceFeeAmount  = resultSet.getDouble("cash_adv_fee_amount");
      }
      else
      {
        if ( resultSet.getInt("local_cash_adv") == 0 )
        {
          SalesVolume = resultSet.getDouble("sales_volume");
        }
        else
        {
          SalesVolume = ( resultSet.getDouble("sales_volume") +
                          resultSet.getDouble("cash_advance_amount") );
        }
      
        DiscountIncome  = resultSet.getDouble("disc_inc");
        FeesIncome      = resultSet.getDouble("fees_inc");
        FeesExpense     = resultSet.getDouble("fees_exp");
        ServiceFee      = resultSet.getDouble("service_fee");
      }
      
      if ( isOrgAssociation( OrgId ) || isOrgMerchant( OrgId ) )
      {
        Rate = resultSet.getDouble("rate");
        
        if ( isOrgMerchant( OrgId ) )
        {
          MinDiscAmount = resultSet.getDouble("min_disc_amount");
        }          
      }
    }
    
    public void addCashAdvance( int cashAdvCount, double feeAmount )
    {
      CashAdvanceCount += cashAdvCount;
      CashAdvanceFeeAmount += feeAmount;
    }
    
    public void addDiscountIncome( double discInc )
    {
      DiscountIncome += discInc;
    }
    
    public void addFeesExpense( double feesExp )
    {
      FeesExpense += feesExp;
    }
    
    public void addFeesIncome( double feesInc )
    {
      FeesIncome += feesInc;
    }
    
    public void addSalesVolume( double salesVolume )
    {
      SalesVolume += salesVolume;
    }
    
    public void addServiceFee( double serviceFee )
    {
      ServiceFee += serviceFee;
    }
    
    public void addCashAdvanceYTD( int cashAdvCount, double feeAmount )
    {
      CashAdvanceCountYTD += cashAdvCount;
      CashAdvanceFeeAmountYTD += feeAmount;
    }
    
    public void addDiscountIncomeYTD( double discInc )
    {
      DiscountIncomeYTD += discInc;
    }
    
    public void addFeesExpenseYTD( double feesExp )
    {
      FeesExpenseYTD += feesExp;
    }
    
    public void addFeesIncomeYTD( double feesInc )
    {
      FeesIncomeYTD += feesInc;
    }
    
    public void addSalesVolumeYTD( double salesVolume )
    {
      SalesVolumeYTD += salesVolume;
    }
    
    public void addServiceFeeYTD( double serviceFee )
    {
      ServiceFeeYTD += serviceFee;
    }
    
    public int compareTo( Object compareObj )
    {
      RebateData        compare = (RebateData) compareObj;
      int               retVal  = 0;
      
/* //@@@@@@@@@@2    
      //@ adding sort by
      if ( (retVal = (int)(HierarchyNode - compare.HierarchyNode)) == 0 )
      {
        retVal = 
      }
//@@@@@@@@@@2    */
      
      return(retVal);
    }
    
    public double getCashAdvanceIncome( )
    {
      return( CashAdvanceFeeAmount );
    }
    
    public double getRebateAmount( )
    {
      return( ( DiscountIncome + FeesIncome + getCashAdvanceIncome() ) -
              ( FeesExpense + ServiceFee ) );
    }
    
    public double getRebateAmountYTD( )
    {
      return( ( DiscountIncomeYTD + FeesIncomeYTD + CashAdvanceFeeAmountYTD ) -
              ( FeesExpenseYTD + ServiceFeeYTD ) );
    }
  }
  
  // member variables
  protected SortByType    SortBy                = null;
  
  public BankRebateDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Sales Volume\",");
    line.append("\"Discount Income\",");
    line.append("\"Fee Income\",");
    line.append("\"Fee Expense\",");
    line.append("\"Service Fee\",");
    line.append("\"# Cash Advance\",");
    line.append("\"Cash Advance Fee\",");
    line.append("\"Rebate Amount\",");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RebateData record = (RebateData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.HierarchyNode );
    line.append( ",\"" );
    line.append( record.OrgName );
    line.append( "\"," );
    line.append( record.SalesVolume );
    line.append( "," );
    line.append( record.DiscountIncome );
    line.append( "," );
    line.append( record.FeesIncome );
    line.append( "," );
    line.append( record.FeesExpense );
    line.append( "," );
    line.append( record.ServiceFee );
    line.append( "," );
    line.append( record.CashAdvanceCount );
    line.append( "," );
    line.append( record.getCashAdvanceIncome() );
    line.append( "," );
    line.append( record.getRebateAmount() );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMMyyyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_rebate_report_");
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( isSameMonthYear( ReportDateBegin, ReportDateEnd ) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          lastNode          = 0L;
    RebateData                    rebateData        = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( isOrgAssociation() == true )
      {
        long                  hierarchyNode = getReportHierarchyNode();
        
        /*@lineinfo:generated-code*//*@lineinfo:313^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      INDEX (br idx_bank_rebate_assoc_date)
//                   */
//                   o.org_num                                  as ORG_NUM,
//                   o.org_group                                as hierarchy_node,
//                   o.org_name                                 as org_name,
//                   decode(br.cash_adv_account,'Y',1,0)        as cash_advance,
//                   decode(br.local_cash_adv_account,'Y',1,0)  as local_cash_adv,
//                   br.CASH_ADV_VOL_COUNT                      as num_cash_advance,
//                   br.CASH_ADV_VOL_AMOUNT                     as cash_advance_amount,
//                   br.cash_adv_fee_amount                     as cash_adv_fee_amount,
//                   cg.cg_charge_amount                        as min_disc_amount,
//                   br.sales_vol_amount                        as sales_volume,
//                   br.discount_income                         as DISC_INC,
//                   br.fee_income                              as fees_inc,
//                   br.fee_expense                             as FEES_EXP,
//                   br.service_fee_amount                      as service_fee,
//                   nvl( vs_pl.pl_disc_rate,
//                        nvl(mc_pl.pl_disc_rate, 0 ) )         as rate
//            from   bank_rebate_summary      br,
//                   organization             o,
//                   monthly_extract_summary  sm,
//                   monthly_extract_pl       vs_pl,
//                   monthly_extract_pl       mc_pl,
//                   monthly_extract_cg       cg
//            where  br.assoc_number = :hierarchyNode and
//                   --br.active_date between :beginDate and last_day( :endDate ) and
//                   br.active_date = :beginDate and
//                   o.org_group     = br.merchant_number and
//                   sm.merchant_number = br.merchant_number and
//                   sm.active_date = br.active_date and 
//                   vs_pl.hh_load_sec(+) = sm.hh_load_sec and
//                   vs_pl.pl_plan_type(+) = 'VISA' and
//                   mc_pl.hh_load_sec(+) = sm.hh_load_sec and
//                   mc_pl.pl_plan_type(+) = 'MC' and
//                   cg.hh_load_sec(+) = sm.hh_load_sec and
//                   cg.cg_charge_record_type(+) = 'DIS'
//  --          group by br.service_fee_rate, o.org_group,
//  --                  o.org_num, br.cash_adv_account, 
//  --                  br.local_cash_adv_account, 
//  --                  o.org_name
//            order by o.org_name        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    INDEX (br idx_bank_rebate_assoc_date)\n                 */\n                 o.org_num                                  as ORG_NUM,\n                 o.org_group                                as hierarchy_node,\n                 o.org_name                                 as org_name,\n                 decode(br.cash_adv_account,'Y',1,0)        as cash_advance,\n                 decode(br.local_cash_adv_account,'Y',1,0)  as local_cash_adv,\n                 br.CASH_ADV_VOL_COUNT                      as num_cash_advance,\n                 br.CASH_ADV_VOL_AMOUNT                     as cash_advance_amount,\n                 br.cash_adv_fee_amount                     as cash_adv_fee_amount,\n                 cg.cg_charge_amount                        as min_disc_amount,\n                 br.sales_vol_amount                        as sales_volume,\n                 br.discount_income                         as DISC_INC,\n                 br.fee_income                              as fees_inc,\n                 br.fee_expense                             as FEES_EXP,\n                 br.service_fee_amount                      as service_fee,\n                 nvl( vs_pl.pl_disc_rate,\n                      nvl(mc_pl.pl_disc_rate, 0 ) )         as rate\n          from   bank_rebate_summary      br,\n                 organization             o,\n                 monthly_extract_summary  sm,\n                 monthly_extract_pl       vs_pl,\n                 monthly_extract_pl       mc_pl,\n                 monthly_extract_cg       cg\n          where  br.assoc_number =  :1  and\n                 --br.active_date between :beginDate and last_day( :endDate ) and\n                 br.active_date =  :2  and\n                 o.org_group     = br.merchant_number and\n                 sm.merchant_number = br.merchant_number and\n                 sm.active_date = br.active_date and \n                 vs_pl.hh_load_sec(+) = sm.hh_load_sec and\n                 vs_pl.pl_plan_type(+) = 'VISA' and\n                 mc_pl.hh_load_sec(+) = sm.hh_load_sec and\n                 mc_pl.pl_plan_type(+) = 'MC' and\n                 cg.hh_load_sec(+) = sm.hh_load_sec and\n                 cg.cg_charge_record_type(+) = 'DIS'\n--          group by br.service_fee_rate, o.org_group,\n--                  o.org_num, br.cash_adv_account, \n--                  br.local_cash_adv_account, \n--                  o.org_name\n          order by o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.BankRebateDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setDate(2,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.BankRebateDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:357^9*/
      }
      else    // not an association, must be a higher level group
      {
        /*@lineinfo:generated-code*//*@lineinfo:361^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      INDEX (br idx_bank_rebate_assoc_date)
//                   */
//                   o.org_num                                    as ORG_NUM,
//                   o.org_group                                  as hierarchy_node,
//                   o.org_name                                   as org_name,
//                   decode(br.cash_adv_account,'Y',1,0)          as cash_advance,
//                   decode(br.local_cash_adv_account,'Y',1,0)    as local_cash_adv,
//                   sum( br.CASH_ADV_VOL_COUNT )                 as num_cash_advance,
//                   sum( br.CASH_ADV_VOL_AMOUNT )                as cash_advance_amount,
//                   sum( br.cash_adv_fee_amount )                as cash_adv_fee_amount,
//                   sum( br.sales_vol_amount )                   as sales_volume,
//                   sum( br.discount_income )                    as DISC_INC,
//                   sum( br.fee_income )                         as fees_inc,
//                   sum( br.fee_expense )                        as FEES_EXP,
//                   sum( br.service_fee_amount )                 as service_fee,
//                   avg( asf.fee_rate )                          as rate
//            from   parent_org               po,
//                   organization             o,
//                   group_assoc              ga,
//                   bank_rebate_summary      br,
//                   assoc_service_fees       asf
//            where  po.parent_org_num = :orgId and
//                   o.org_num = po.org_num and
//                   ga.group_number = o.org_group and
//                   br.assoc_number = ga.assoc_number and
//                   --br.active_date between :beginDate and last_day( :endDate ) and
//                   br.active_date = :beginDate and
//                   asf.assoc_number(+) = br.assoc_number and
//                   br.active_date between asf.valid_date_begin(+) and asf.valid_date_end(+)
//            group by br.service_fee_rate, o.org_group,
//                    o.org_num, br.cash_adv_account, 
//                    br.local_cash_adv_account, 
//                    o.org_name
//            order by o.org_name      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    INDEX (br idx_bank_rebate_assoc_date)\n                 */\n                 o.org_num                                    as ORG_NUM,\n                 o.org_group                                  as hierarchy_node,\n                 o.org_name                                   as org_name,\n                 decode(br.cash_adv_account,'Y',1,0)          as cash_advance,\n                 decode(br.local_cash_adv_account,'Y',1,0)    as local_cash_adv,\n                 sum( br.CASH_ADV_VOL_COUNT )                 as num_cash_advance,\n                 sum( br.CASH_ADV_VOL_AMOUNT )                as cash_advance_amount,\n                 sum( br.cash_adv_fee_amount )                as cash_adv_fee_amount,\n                 sum( br.sales_vol_amount )                   as sales_volume,\n                 sum( br.discount_income )                    as DISC_INC,\n                 sum( br.fee_income )                         as fees_inc,\n                 sum( br.fee_expense )                        as FEES_EXP,\n                 sum( br.service_fee_amount )                 as service_fee,\n                 avg( asf.fee_rate )                          as rate\n          from   parent_org               po,\n                 organization             o,\n                 group_assoc              ga,\n                 bank_rebate_summary      br,\n                 assoc_service_fees       asf\n          where  po.parent_org_num =  :1  and\n                 o.org_num = po.org_num and\n                 ga.group_number = o.org_group and\n                 br.assoc_number = ga.assoc_number and\n                 --br.active_date between :beginDate and last_day( :endDate ) and\n                 br.active_date =  :2  and\n                 asf.assoc_number(+) = br.assoc_number and\n                 br.active_date between asf.valid_date_begin(+) and asf.valid_date_end(+)\n          group by br.service_fee_rate, o.org_group,\n                  o.org_num, br.cash_adv_account, \n                  br.local_cash_adv_account, \n                  o.org_name\n          order by o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.BankRebateDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.BankRebateDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:398^9*/
      }
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        if ( lastNode != resultSet.getLong("hierarchy_node") )
        {
          rebateData = new RebateData(resultSet);
          
          // load the YTD data into this record
          loadDataYTD(rebateData,beginDate);
          
          // add this record to the vector
          ReportRows.add(rebateData);
          
          // store the new last node value
          lastNode = resultSet.getLong("hierarchy_node");
        }
        else  // still processing the same node, append the additional data
        {
          if( ( resultSet.getInt("cash_advance") == 1 ) &&
              ( resultSet.getInt("local_cash_adv") == 0 ) ) 
          {
            rebateData.addCashAdvance( resultSet.getInt("num_cash_advance"),
                                       resultSet.getDouble("cash_adv_fee_amount") );
          }
          else
          {
            if ( resultSet.getInt("local_cash_adv") == 0 )
            {
              rebateData.addSalesVolume( resultSet.getDouble("sales_volume") );
            }
            else
            {
              rebateData.addSalesVolume( resultSet.getDouble("sales_volume") +
                                         resultSet.getDouble("cash_advance_amount") );
            }
            rebateData.addDiscountIncome( resultSet.getDouble("disc_inc") );
            rebateData.addFeesIncome( resultSet.getDouble("fees_inc") );
            rebateData.addFeesExpense( resultSet.getDouble("fees_exp") );
            rebateData.addServiceFee( resultSet.getDouble("service_fee") );
          }
        }
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  private void loadDataYTD( RebateData rebateData, Date activeDate  )
  {
    ResultSetIterator           it            = null;
    ResultSet                   resultSet     = null;
    
    try
    {
      if ( isNodeMerchant( rebateData.HierarchyNode ) == true )
      {
        /*@lineinfo:generated-code*//*@lineinfo:464^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(br.cash_adv_account,'Y',1,0)          as cash_advance,
//                    decode(br.local_cash_adv_account,'Y',1,0)    as local_cash_adv,
//                    sum( br.CASH_ADV_VOL_COUNT )                 as num_cash_advance,
//                    sum( br.cash_adv_vol_amount )                as cash_advance_amount,
//                    sum( br.cash_adv_fee_amount )                as cash_adv_fee_amount,
//                    sum( br.sales_vol_amount )                   as sales_volume,
//                    sum( br.discount_income )                    as DISC_INC,
//                    sum( br.fee_income )                         as fees_inc,
//                    sum( br.fee_expense )                        as FEES_EXP,
//                    sum( br.service_fee_amount )                 as service_fee
//            from    bank_rebate_summary      br
//            where   br.merchant_number = :rebateData.HierarchyNode and
//                    br.active_date between to_date('01'||to_char( :activeDate,'yyyy'),'mmyyyy') and :activeDate
//            group by decode(br.cash_adv_account,'Y',1,0),
//                     decode(br.local_cash_adv_account,'Y',1,0)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(br.cash_adv_account,'Y',1,0)          as cash_advance,\n                  decode(br.local_cash_adv_account,'Y',1,0)    as local_cash_adv,\n                  sum( br.CASH_ADV_VOL_COUNT )                 as num_cash_advance,\n                  sum( br.cash_adv_vol_amount )                as cash_advance_amount,\n                  sum( br.cash_adv_fee_amount )                as cash_adv_fee_amount,\n                  sum( br.sales_vol_amount )                   as sales_volume,\n                  sum( br.discount_income )                    as DISC_INC,\n                  sum( br.fee_income )                         as fees_inc,\n                  sum( br.fee_expense )                        as FEES_EXP,\n                  sum( br.service_fee_amount )                 as service_fee\n          from    bank_rebate_summary      br\n          where   br.merchant_number =  :1  and\n                  br.active_date between to_date('01'||to_char(  :2 ,'yyyy'),'mmyyyy') and  :3 \n          group by decode(br.cash_adv_account,'Y',1,0),\n                   decode(br.local_cash_adv_account,'Y',1,0)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.BankRebateDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,rebateData.HierarchyNode);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.BankRebateDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:481^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:485^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(br.cash_adv_account,'Y',1,0)          as cash_advance,
//                    decode(br.local_cash_adv_account,'Y',1,0)    as local_cash_adv,
//                    sum( br.CASH_ADV_VOL_COUNT )                 as num_cash_advance,
//                    sum( br.cash_adv_vol_amount )                as cash_advance_amount,
//                    sum( br.cash_adv_fee_amount )                as cash_adv_fee_amount,
//                    sum( br.sales_vol_amount )                   as sales_volume,
//                    sum( br.discount_income )                    as DISC_INC,
//                    sum( br.fee_income )                         as fees_inc,
//                    sum( br.fee_expense )                        as FEES_EXP,
//                    sum( br.service_fee_amount )                 as service_fee
//            from    group_assoc              ga,
//                    bank_rebate_summary      br
//            where   ga.group_number = :rebateData.HierarchyNode and
//                    br.assoc_number = ga.assoc_number and
//                    br.active_date between to_date('01'||to_char( :activeDate,'yyyy'),'mmyyyy') and :activeDate
//            group by decode(br.cash_adv_account,'Y',1,0),
//                     decode(br.local_cash_adv_account,'Y',1,0)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(br.cash_adv_account,'Y',1,0)          as cash_advance,\n                  decode(br.local_cash_adv_account,'Y',1,0)    as local_cash_adv,\n                  sum( br.CASH_ADV_VOL_COUNT )                 as num_cash_advance,\n                  sum( br.cash_adv_vol_amount )                as cash_advance_amount,\n                  sum( br.cash_adv_fee_amount )                as cash_adv_fee_amount,\n                  sum( br.sales_vol_amount )                   as sales_volume,\n                  sum( br.discount_income )                    as DISC_INC,\n                  sum( br.fee_income )                         as fees_inc,\n                  sum( br.fee_expense )                        as FEES_EXP,\n                  sum( br.service_fee_amount )                 as service_fee\n          from    group_assoc              ga,\n                  bank_rebate_summary      br\n          where   ga.group_number =  :1  and\n                  br.assoc_number = ga.assoc_number and\n                  br.active_date between to_date('01'||to_char(  :2 ,'yyyy'),'mmyyyy') and  :3 \n          group by decode(br.cash_adv_account,'Y',1,0),\n                   decode(br.local_cash_adv_account,'Y',1,0)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.BankRebateDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,rebateData.HierarchyNode);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.BankRebateDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:504^9*/
      }        
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        if( ( resultSet.getInt("cash_advance") == 1 ) &&
            ( resultSet.getInt("local_cash_adv") == 0 ) ) 
        {
          rebateData.addCashAdvanceYTD( resultSet.getInt("num_cash_advance"),
                                        resultSet.getDouble("cash_adv_fee_amount") );
        }
        else
        {
          if ( resultSet.getInt("local_cash_adv") == 0 )
          {
            rebateData.addSalesVolumeYTD( resultSet.getDouble("sales_volume") );
          }
          else
          {
            rebateData.addSalesVolumeYTD( resultSet.getDouble("sales_volume") +
                                          resultSet.getDouble("cash_advance_amount") );
          }            
          rebateData.addDiscountIncomeYTD( resultSet.getDouble("disc_inc") );
          rebateData.addFeesIncomeYTD( resultSet.getDouble("fees_inc") );
          rebateData.addFeesExpenseYTD( resultSet.getDouble("fees_exp") );
          rebateData.addServiceFeeYTD( resultSet.getDouble("service_fee") );
        }
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadDataYTD()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  public double loadRebateYTD( long hierarchyNode, Date activeDate )
  {
    double              retVal    = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:552^7*/

//  ************************************************************
//  #sql [Ctx] { select /*+
//                    INDEX (br idx_bank_rebate_assoc_date)
//                 */ 
//                 sum( br.rebate_amount )  
//          from   group_assoc              ga,
//                 bank_rebate_summary      br
//          where  ga.group_number = :hierarchyNode and
//                 br.assoc_number = ga.assoc_number and
//                 br.active_date between to_date('01'||to_char( :activeDate,'yyyy'),'mmyyyy') and :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select /*+\n                  INDEX (br idx_bank_rebate_assoc_date)\n               */ \n               sum( br.rebate_amount )   \n        from   group_assoc              ga,\n               bank_rebate_summary      br\n        where  ga.group_number =  :1  and\n               br.assoc_number = ga.assoc_number and\n               br.active_date between to_date('01'||to_char(  :2 ,'yyyy'),'mmyyyy') and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.BankRebateDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:563^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadRebateYTD(" + hierarchyNode + "," + activeDate + ")", e.toString() );
    }
    return( retVal );
  }
  
  public void setProperties(HttpServletRequest request)
  {
    int         sortOrder;
    
    // load the default report properties
    super.setProperties( request );
    
/********    
    sortOrder       = HttpHelper.getInt(request,"sortOrder",SortByType.SB_NONE);
    
    if ( sortOrder == SortByType.SB_NONE )
    {
      if ( ( ReportDataType == DT_STATEMENTS ) &&
           ( ReportType     == RT_DETAILS ) )
      {
        sortOrder = MesOrgSummaryEntry.SB_ENTRY_DATE;
      }
      else
      {
        sortOrder = MesOrgSummaryEntry.SB_NAME;
      }
    }
    SortBy = new SortByType( sortOrder  );
**********/    
    
    // if the from date is today, then set the default to rolling previous week
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range
      cal.setTime( ReportDateBegin );
      
      if ( cal.get( Calendar.MONTH ) == Calendar.JANUARY )
      {
        cal.set( Calendar.MONTH, Calendar.DECEMBER );
        cal.set( Calendar.YEAR,  ( cal.get( Calendar.YEAR ) - 1 ) );
      }
      else
      {
        cal.set( Calendar.MONTH, ( cal.get( Calendar.MONTH ) - 1 ) );
      }
      
      // since this report is for one month at a time,
      // make sure that the from date always starts with
      // the first day of the specified month.
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      // set both the begin and end dates
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }
    // only one date supported
    setReportDateEnd( getReportDateBegin() );
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/