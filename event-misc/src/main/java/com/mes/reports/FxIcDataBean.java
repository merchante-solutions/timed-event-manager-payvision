/*@lineinfo:filename=FxIcDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/FxIcDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000-2007,2008 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;


public class FxIcDataBean extends ReportSQLJBean
{
  public static final int         DT_FX_IC            = 0;
  public static final int         DT_INTL_SETTLEMENT  = 1;
  
  public static class DetailRow
  {
    public    String    AuthCode              = null;
    public    Date      BatchDate             = null;
    public    String    CardNumber            = null;
    public    String    CardType              = null;
    public    String    ClientRefNum          = null;
    public    String    CurrencyCode          = null;
    public    String    CurrencyCodeAlpha     = null;
    public    String    CurrencyName          = null;
    public    String    DbaName               = null;
    public    String    DebitCreditInd        = null;
    public    String    EntryMode             = null;
    public    double    FxAmount              = 0.0;
    public    int       IcCat                 = -1;
    public    String    IcDesc                = null;
    public    double    IcDue                 = 0.0;
    public    double    IcPerItem             = 0.0;
    public    double    IcRate                = 0.0;
    public    long      MerchantId            = 0L;
    public    String    PurchaseId            = null;
    public    String    ReferenceNumber       = null;
    public    double    TranAmount            = 0.0;
    public    Date      TranDate              = null;
    public    String    TridentTranId         = null;
    
  
    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AuthCode            = processString( resultSet.getString("auth_code") );
      BatchDate           = resultSet.getDate("batch_date");
      CardNumber          = processString( resultSet.getString("card_number") );
      CardType            = processString( resultSet.getString("card_type") );
      ClientRefNum        = processString( resultSet.getString("client_ref_num") );
      CurrencyName        = resultSet.getString( "currency_name" );
      CurrencyCode        = resultSet.getString( "currency_code" );
      CurrencyCodeAlpha   = resultSet.getString( "currency_code_alpha" );
      DbaName             = processString( resultSet.getString("dba_name") );
      DebitCreditInd      = resultSet.getString("debit_credit_ind");
      EntryMode           = processString( resultSet.getString("entry_mode") );
      FxAmount            = resultSet.getDouble("fx_amount");
      IcCat               = resultSet.getInt("ic_cat");
      IcDesc              = processString( resultSet.getString("ic_desc") );
      IcDue               = resultSet.getDouble("ic_due");
      IcPerItem           = resultSet.getDouble("ic_per_item");
      IcRate              = resultSet.getDouble("ic_rate");
      MerchantId          = resultSet.getLong("merchant_number");
      PurchaseId          = processString( resultSet.getString("purchase_id") );
      ReferenceNumber     = resultSet.getString("ref_num");
      TranAmount          = resultSet.getDouble("tran_amount");
      TranDate            = resultSet.getDate("tran_date");
      TridentTranId       = resultSet.getString("trident_tran_id");
      
      if( DebitCreditInd.equals("C") )
      {
        TranAmount  *= -1;
        FxAmount    *= -1;
      }
    }
  }
  
  public static class SummaryRow
  {
    public    double      CreditsAmount       = 0.0;
    public    int         CreditsCount        = 0;
    public    String      CurrencyCode        = null;
    public    String      CurrencyCodeAlpha   = null;
    public    String      CurrencyName        = null;
    public    double      FxAmount            = 0.0;
    public    long        HierarchyNode       = 0L;
    public    double      IcDue               = 0.0;
    public    double      NetAmount           = 0.0;
    public    String      OrgName             = null;
    public    double      SalesAmount         = 0.0;
    public    int         SalesCount          = 0;
    
    public SummaryRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      OrgName           = resultSet.getString("org_name");
      CreditsAmount     = resultSet.getDouble("credits_amount");
      CreditsCount      = resultSet.getInt("credits_count");
      SalesAmount       = resultSet.getDouble("sales_amount");
      SalesCount        = resultSet.getInt("sales_count");
      CurrencyName      = resultSet.getString("currency_name");
      CurrencyCode      = resultSet.getString("currency_code");
      CurrencyCodeAlpha = resultSet.getString("currency_code_alpha");
      FxAmount          = resultSet.getDouble("fx_amount");
      IcDue             = resultSet.getDouble("ic_due");
      NetAmount         = resultSet.getDouble("net_amount");
    }
  }
  
  public FxIcDataBean( )
  {
    super(true);    // use field bean
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup        fgroup    = null;
    Field             field     = null;
    
    super.createFields(request);
    fgroup = (FieldGroup)getField("searchFields");
    field = new HiddenField("reportDataType");
    field.setData( String.valueOf(DT_FX_IC) );
    fgroup.add(field);
  }    
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    int   reportDataType  = getInt("reportDataType");
    int   reportType      = getReportType();
    
    if ( reportType == RT_SUMMARY )
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");
      line.append("\"Currency Code\",");
      line.append("\"Sales Count\",");
      line.append("\"Sales Amount\",");
      line.append("\"Credits Count\",");
      line.append("\"Credits Amount\",");
      
      if ( reportDataType == DT_FX_IC )   // FX Interchange report
      {
        line.append("\"USD Amount\",");
        line.append("\"IC Due\"");
      }        
    }
    else  // RT_DETAILS
    {
      line.append("\"Merchant ID\",");
      line.append("\"Dba Name\",");
      line.append("\"Batch Date\",");
      line.append("\"Tran Date\",");
      line.append("\"Card Type\",");
      line.append("\"Card Number\",");
      line.append("\"Reference Number\",");
      line.append("\"Purchase ID\",");
      line.append("\"Trident Tran ID\",");
      line.append("\"Client Reference\",");
      line.append("\"Auth Code\",");
      line.append( "\"Entry Mode\"," );
      line.append( "\"Debit Credit Ind\"," );
      line.append( "\"Currency Code\"," );
      line.append("\"Tran Amount\",");
      
      if ( reportDataType == DT_FX_IC )   // FX Interchange report
      {
        line.append( "\"Tran Amount USD\"," );
        line.append( "\"IC Due\"" );
      }        
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    int   reportDataType  = getInt("reportDataType");
    int   reportType      = getReportType();
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    if ( reportType == RT_SUMMARY )
    {
      SummaryRow  row = (SummaryRow) obj;
      
      line.append( encodeHierarchyNode(row.HierarchyNode) );
      line.append( ",\"" );
      line.append( row.OrgName );
      line.append( "\",\"" );
      line.append( row.CurrencyCodeAlpha );
      line.append( "\"," );
      line.append( row.SalesCount );
      line.append( "," );
      line.append( row.SalesAmount );
      line.append( "," );
      line.append( row.CreditsCount );
      line.append( "," );
      line.append( row.CreditsAmount );
      
      if ( reportDataType == DT_FX_IC )   // FX Interchange report
      {
        line.append( "," );
        line.append( row.FxAmount );
        line.append( "," );
        line.append( row.IcDue );
      }
    }
    else    // RT_DETAILS
    {
      DetailRow       row = (DetailRow) obj;
      
      line.append( encodeHierarchyNode(row.MerchantId) );
      line.append( ",\"" );
      line.append( row.DbaName );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate(row.BatchDate,"MM/dd/yyyy") );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate(row.TranDate,"MM/dd/yyyy") );
      line.append( ",\"" );
      line.append( row.CardType );
      line.append( "\",\"" );
      line.append( row.CardNumber );
      line.append( "\",\"'" );
      line.append( row.ReferenceNumber );
      line.append( "\",\"" );
      line.append( row.PurchaseId );
      line.append( "\",\"" );
      line.append( row.TridentTranId );
      line.append( "\",\"" );
      line.append( row.ClientRefNum );
      line.append( "\",\"" );
      line.append( row.AuthCode );
      line.append( "\",\"" );
      line.append( row.EntryMode );
      line.append( "\",\"" );
      line.append( row.DebitCreditInd );
      line.append( "\",\"" );
      line.append( row.CurrencyCodeAlpha );
      line.append( "\"," );
      line.append( row.TranAmount );
      
      if ( reportDataType == DT_FX_IC )   // FX Interchange report
      {
        line.append( "," );
        line.append( row.FxAmount );
        line.append( "," );
        line.append( row.IcDue );
      }        
    }
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
    buffer.append( "reportDataType=" );
    buffer.append( getInt("reportDataType",DT_FX_IC) );
    super.encodeNodeUrl( buffer,nodeId,beginDate,endDate );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    
    if( fileFormat == FF_CSV )
    {
      retVal = true;
    }
    return(retVal);
  }
  
  public String getDownloadFilenameBase()
  {
    Date              beginDate       = getReportDateBegin();
    Calendar          cal             = Calendar.getInstance();  
    Date              endDate         = getReportDateEnd();
    StringBuffer      filename        = new StringBuffer("");
    int               reportDataType  = getInt("reportDataType");
    
    filename.append( getReportHierarchyNode() );
    
    if ( getReportType() == RT_SUMMARY )
    {
      if ( reportDataType == DT_INTL_SETTLEMENT )
      {
        filename.append("_intl_settlement_summary_");
      }
      else  // DT_FX_IC
      {
        filename.append("_fx_ic_summary_");
      }
    }
    else  // RT_DETAILS
    {
      if ( reportDataType == DT_INTL_SETTLEMENT )
      {
        filename.append("_intl_settlement_details_");
      }
      else  // DT_FX_IC
      {
        filename.append("_fx_ic_details_");
      }
    }      
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( beginDate,"MMddyy" ) );
    if ( beginDate.equals(endDate) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( endDate,"MMddyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public String getReportTitle()
  {
    String    reportTitle     = "No Title";
    int       reportType      = getReportType();
    
    switch( getInt("reportDataType") )
    {
      case DT_FX_IC:
        reportTitle = "Foreign Exchange Interchange";
        break;
        
      case DT_INTL_SETTLEMENT:
        reportTitle = "International Settlement";
        break;
    }
    return( reportTitle + ((reportType == RT_SUMMARY) ? " Summary" : " Details" ) );
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    int   reportType    = getReportType();
    
    switch( reportType )
    {
      case RT_SUMMARY:
        loadSummaryData(orgId,beginDate,endDate);
        break;
        
      default:    // details
        loadDetailData(orgId,beginDate,endDate);
        break;
    }        
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it          = null;
    long                nodeId      = orgIdToHierarchyNode(orgId);
    ResultSet           resultSet   = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:400^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_number                        as merchant_number,
//                  mf.dba_name                               as dba_name,
//                  dt.settlement_date                        as batch_date,
//                  dt.transaction_date                       as tran_date,
//                  dt.card_number                            as card_number, 
//                  dt.card_number_enc                        as card_number_enc,
//                  dt.card_type_id                           as card_type,
//                  dt.reference_number                       as ref_num,
//                  dt.purchase_id                            as purchase_id,
//                  dt.trident_tran_id                        as trident_tran_id,
//                  dt.client_reference_number                as client_ref_num,
//                  nvl(emd.pos_entry_desc,dt.pos_entry_mode) as entry_mode,
//                  dt.debit_credit_indicator                 as debit_credit_ind,
//                  dt.orig_amount                            as tran_amount,
//                  (nvl(dt.usd_discount_fee,0)*-1)           as ic_due,
//                  -- fx data
//                  dt.fx_amount_base                         as fx_amount,
//                  dt.orig_currency_code                     as currency_code,
//                  cc.currency_code_alpha                    as currency_code_alpha,
//                  cc.currency_name                          as currency_name,
//                  -- pending data from B+S
//                  null                                      as ic_cat,
//                  null                                      as ic_desc,
//                  null                                      as ic_rate,
//                  null                                      as ic_per_item,
//                  -- not supported
//                  null                                      as auth_code
//          from    organization                o,
//                  group_merchant              gm,
//                  payvision_api_tran_detail   dt,
//                  mif                         mf,
//                  pos_entry_mode_desc         emd,
//                  currency_codes              cc
//          where   o.org_group = :nodeId
//                  and gm.org_num = o.org_num 
//                  and dt.merchant_number = gm.merchant_number
//                  and dt.settlement_date between :beginDate and :endDate
//                  and mf.merchant_number = dt.merchant_number
//                  and emd.pos_entry_code(+) = dt.pos_entry_mode
//                  and cc.currency_code = dt.orig_currency_code
//          order by dt.orig_currency_code,dt.merchant_number, 
//                    dt.settlement_date, dt.card_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_number                        as merchant_number,\n                mf.dba_name                               as dba_name,\n                dt.settlement_date                        as batch_date,\n                dt.transaction_date                       as tran_date,\n                dt.card_number                            as card_number, \n                dt.card_number_enc                        as card_number_enc,\n                dt.card_type_id                           as card_type,\n                dt.reference_number                       as ref_num,\n                dt.purchase_id                            as purchase_id,\n                dt.trident_tran_id                        as trident_tran_id,\n                dt.client_reference_number                as client_ref_num,\n                nvl(emd.pos_entry_desc,dt.pos_entry_mode) as entry_mode,\n                dt.debit_credit_indicator                 as debit_credit_ind,\n                dt.orig_amount                            as tran_amount,\n                (nvl(dt.usd_discount_fee,0)*-1)           as ic_due,\n                -- fx data\n                dt.fx_amount_base                         as fx_amount,\n                dt.orig_currency_code                     as currency_code,\n                cc.currency_code_alpha                    as currency_code_alpha,\n                cc.currency_name                          as currency_name,\n                -- pending data from B+S\n                null                                      as ic_cat,\n                null                                      as ic_desc,\n                null                                      as ic_rate,\n                null                                      as ic_per_item,\n                -- not supported\n                null                                      as auth_code\n        from    organization                o,\n                group_merchant              gm,\n                payvision_api_tran_detail   dt,\n                mif                         mf,\n                pos_entry_mode_desc         emd,\n                currency_codes              cc\n        where   o.org_group =  :1 \n                and gm.org_num = o.org_num \n                and dt.merchant_number = gm.merchant_number\n                and dt.settlement_date between  :2  and  :3 \n                and mf.merchant_number = dt.merchant_number\n                and emd.pos_entry_code(+) = dt.pos_entry_mode\n                and cc.currency_code = dt.orig_currency_code\n        order by dt.orig_currency_code,dt.merchant_number, \n                  dt.settlement_date, dt.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.FxIcDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.FxIcDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:444^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        ReportRows.addElement( new DetailRow(resultSet) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it            = null;
    long                nodeId        = orgIdToHierarchyNode(orgId);
    ResultSet           resultSet     = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
//@      if ( SummaryType == ST_PARENT )
//@      {
//@        #sql [Ctx] it = 
//@        {
//@          select /*+ 
//@                    index (dt idx_pv_tran_detail_merch_date)
//@                    use_nl (gm dt)
//@                  */
//@                  o.org_group                           as hierarchy_node,
//@                  o.org_name                            as org_name,
//@                  dt.orig_currency_code                 as currency_code,
//@                  cc.currency_code_alpha                as currency_code_alpha,
//@                  cc.currency_name                      as currency_name,
//@                  sum(decode(dt.debit_credit_indicator,
//@                             'C',dt.orig_amount,0) )    as credits_amount,
//@                  sum(decode(dt.debit_credit_indicator,
//@                             'C',1,0) )                 as credits_count,
//@                  sum(decode(dt.debit_credit_indicator,
//@                             'D',dt.orig_amount,0) )    as sales_amount,
//@                  sum(decode(dt.debit_credit_indicator,
//@                             'D',1,0) )                 as sales_count,
//@                  sum(decode(dt.debit_credit_indicator,'C',-1,1) *
//@                      dt.fx_amount_base )               as fx_amount,
//@                  sum(nvl(dt.usd_discount_fee,0)*-1)    as ic_due
//@          from    organization                o,
//@                  group_merchant              gm,
//@                  payvision_api_tran_detail   dt,
//@                  mif                         mf,
//@                  currency_codes              cc
//@          where   o.org_group = :nodeId
//@                  and gm.org_num = o.org_num 
//@                  and dt.merchant_number = gm.merchant_number
//@                  and dt.settlement_date between :beginDate and :endDate
//@                  and mf.merchant_number = dt.merchant_number
//@                  and cc.currency_code = dt.orig_currency_code
//@          group by o.org_name, o.org_group,
//@                   dt.orig_currency_code, cc.currency_code_alpha, 
//@                   cc.currency_name
//@          order by o.org_name, o.org_group, dt.orig_currency_code
//@        };
//@      }
//@      else // ST_CHILD
      if ( true )
      {
        /*@lineinfo:generated-code*//*@lineinfo:518^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                      index (dt idx_pv_tran_detail_merch_date)
//                      use_nl (gm dt)
//                    */
//                    o.org_group                           as hierarchy_node,
//                    o.org_name                            as org_name,
//                    dt.orig_currency_code                 as currency_code,
//                    cc.currency_code_alpha                as currency_code_alpha,
//                    cc.currency_name                      as currency_name,
//                    sum(decode(dt.debit_credit_indicator,
//                               'C',dt.orig_amount,0) )    as credits_amount,
//                    sum(decode(dt.debit_credit_indicator,
//                               'C',1,0) )                 as credits_count,
//                    sum(decode(dt.debit_credit_indicator,
//                               'D',dt.orig_amount,0) )    as sales_amount,
//                    sum(decode(dt.debit_credit_indicator,
//                               'D',1,0) )                 as sales_count,
//                    sum(decode(dt.debit_credit_indicator,'C',-1,1) *
//                        dt.orig_amount )                  as net_amount,
//                    sum(decode(dt.debit_credit_indicator,'C',-1,1) *
//                        dt.fx_amount_base )               as fx_amount,
//                    sum(nvl(dt.usd_discount_fee,0)*-1)    as ic_due                             
//            from    organization                op,
//                    parent_org                  po,
//                    group_merchant              gm,
//                    payvision_api_tran_detail   dt,
//                    mif                         mf,
//                    currency_codes              cc,
//                    organization                o
//            where   op.org_group = :nodeId
//                    and po.parent_org_num = op.org_num
//                    and gm.org_num = po.org_num 
//                    and dt.merchant_number = gm.merchant_number
//                    and dt.settlement_date between :beginDate and :endDate
//                    and mf.merchant_number = dt.merchant_number
//                    and cc.currency_code = dt.orig_currency_code
//                    and o.org_num = po.org_num
//            group by o.org_name, o.org_group, 
//                     dt.orig_currency_code, cc.currency_code_alpha, 
//                     cc.currency_name
//            order by o.org_name, o.org_group, dt.orig_currency_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ \n                    index (dt idx_pv_tran_detail_merch_date)\n                    use_nl (gm dt)\n                  */\n                  o.org_group                           as hierarchy_node,\n                  o.org_name                            as org_name,\n                  dt.orig_currency_code                 as currency_code,\n                  cc.currency_code_alpha                as currency_code_alpha,\n                  cc.currency_name                      as currency_name,\n                  sum(decode(dt.debit_credit_indicator,\n                             'C',dt.orig_amount,0) )    as credits_amount,\n                  sum(decode(dt.debit_credit_indicator,\n                             'C',1,0) )                 as credits_count,\n                  sum(decode(dt.debit_credit_indicator,\n                             'D',dt.orig_amount,0) )    as sales_amount,\n                  sum(decode(dt.debit_credit_indicator,\n                             'D',1,0) )                 as sales_count,\n                  sum(decode(dt.debit_credit_indicator,'C',-1,1) *\n                      dt.orig_amount )                  as net_amount,\n                  sum(decode(dt.debit_credit_indicator,'C',-1,1) *\n                      dt.fx_amount_base )               as fx_amount,\n                  sum(nvl(dt.usd_discount_fee,0)*-1)    as ic_due                             \n          from    organization                op,\n                  parent_org                  po,\n                  group_merchant              gm,\n                  payvision_api_tran_detail   dt,\n                  mif                         mf,\n                  currency_codes              cc,\n                  organization                o\n          where   op.org_group =  :1 \n                  and po.parent_org_num = op.org_num\n                  and gm.org_num = po.org_num \n                  and dt.merchant_number = gm.merchant_number\n                  and dt.settlement_date between  :2  and  :3 \n                  and mf.merchant_number = dt.merchant_number\n                  and cc.currency_code = dt.orig_currency_code\n                  and o.org_num = po.org_num\n          group by o.org_name, o.org_group, \n                   dt.orig_currency_code, cc.currency_code_alpha, \n                   cc.currency_name\n          order by o.org_name, o.org_group, dt.orig_currency_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.FxIcDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.FxIcDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:561^9*/
      }
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new SummaryRow(resultSet) );
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
}/*@lineinfo:generated-code*/