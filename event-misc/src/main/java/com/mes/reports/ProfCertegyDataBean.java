/*@lineinfo:filename=ProfCertegyDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ProfCertegyDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 10/21/03 12:57p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

class ProfCertegyDataBean extends ProfProcDataBean
{
  public ProfCertegyDataBean(  )
  {
  }
  
  protected void loadDetailAuth( long merchantId )
  {
    int                           betType;
    StringBuffer                  desc                  = new StringBuffer("");
    ResultSetIterator             it                    = null;
    ContractTypes.PerItemRecord   record                = null;
    ResultSet                     resultSet             = null;
    
    try
    {
      // setup a generic BET type for the any summary level items.
      betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_AUTH_FEES );
      
      /*@lineinfo:generated-code*//*@lineinfo:55^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                                  as active_date,
//                  cct.CHARGE_DESCRIPTION                          as charge_desc,
//                  ( ap.auth_count_over +
//                    ap.auth_count_under )                         as item_count,
//                  ( ap.auth_inc_over +
//                    ap.auth_inc_under )                           as income,
//                  ( ap.auth_exp_over +
//                    ap.auth_exp_under )                           as expense,
//                 decode( (ap.auth_count_over + ap.auth_count_under),
//                         0, 0,                       
//                         ( (ap.auth_inc_over + ap.auth_inc_under)/
//                           (ap.auth_count_over + ap.auth_count_under) ) )
//                                                                  as inc_per_item,
//                 decode( (ap.auth_count_over + ap.auth_count_under),
//                         0, 0,                       
//                         ( (ap.auth_exp_over + ap.auth_exp_under)/
//                           (ap.auth_count_over + ap.auth_count_under) ) )
//                                                                  as exp_per_item
//          from    monthly_extract_summary       sm,
//                  monthly_extract_certegy_ap    ap,
//                  certegy_charge_types          cct  
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                  ap.LOAD_SEC = sm.HH_LOAD_SEC and
//                  cct.CHARGE_TYPE(+) = ap.charge_type
//          order by cct.charge_description, sm.active_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                                  as active_date,\n                cct.CHARGE_DESCRIPTION                          as charge_desc,\n                ( ap.auth_count_over +\n                  ap.auth_count_under )                         as item_count,\n                ( ap.auth_inc_over +\n                  ap.auth_inc_under )                           as income,\n                ( ap.auth_exp_over +\n                  ap.auth_exp_under )                           as expense,\n               decode( (ap.auth_count_over + ap.auth_count_under),\n                       0, 0,                       \n                       ( (ap.auth_inc_over + ap.auth_inc_under)/\n                         (ap.auth_count_over + ap.auth_count_under) ) )\n                                                                as inc_per_item,\n               decode( (ap.auth_count_over + ap.auth_count_under),\n                       0, 0,                       \n                       ( (ap.auth_exp_over + ap.auth_exp_under)/\n                         (ap.auth_count_over + ap.auth_count_under) ) )\n                                                                as exp_per_item\n        from    monthly_extract_summary       sm,\n                monthly_extract_certegy_ap    ap,\n                certegy_charge_types          cct  \n        where   sm.merchant_number =  :1  and\n                sm.active_date between  :2  and last_day(  :3  ) and\n                ap.LOAD_SEC = sm.HH_LOAD_SEC and\n                cct.CHARGE_TYPE(+) = ap.charge_type\n        order by cct.charge_description, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ProfCertegyDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ProfCertegyDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:83^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        // setup the description for both income and expense records
        desc.setLength(0);
        desc.append( resultSet.getString("charge_desc") );
        desc.append( " - " );
        desc.append( DateTimeFormatter.getFormattedDate( resultSet.getDate("ACTIVE_DATE"),"MMM yyyy" ) );
      
        // add the income data
        if ( resultSet.getDouble("income") != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( resultSet.getDate("active_date"), ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "item_count" ), resultSet.getDouble("inc_per_item") );
          record.setDescription( desc.toString() );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }                              
                            
        // add the expense data
        if ( resultSet.getDouble("expense") != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( resultSet.getDate("active_date"), ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "item_count" ), resultSet.getDouble("exp_per_item") );
          record.setDescription( desc.toString() );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_EXPENSE, record );
        }
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailAuth(): ", e.toString());
      ContractDataBean.addError("loadDetailAuth: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailCapture( long merchantId )
  {
    Date                            activeDate          = null;
    int                             betType;
    StringBuffer                    desc                = new StringBuffer("");
    ResultSetIterator               it                  = null;
    ContractTypes.PerItemRecord     record              = null;
    ResultSet                       resultSet           = null;
  
    try
    {
      betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_CAPTURE_FEES );
    
      /*@lineinfo:generated-code*//*@lineinfo:138^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                                  as active_date,
//                  cct.CHARGE_DESCRIPTION                          as charge_desc,
//                  scan.SCAN_BATCH_COUNT                           as batch_count,
//                  scan.scan_item_count                            as item_count,
//                  scan.SCAN_BATCH_INC                             as batch_income,
//                  scan.SCAN_BATCH_EXP                             as batch_expense,
//                  scan.SCAN_ITEM_INC                              as item_income,
//                  scan.SCAN_ITEM_EXP                              as item_expense,
//                  decode( scan.scan_batch_count,
//                          0,0,
//                          (scan.scan_batch_inc/scan.scan_batch_count) )
//                                                                  as batch_inc_per_item,
//                  decode( scan.scan_batch_count,
//                          0,0,
//                          (scan.scan_batch_exp/scan.scan_batch_count) )
//                                                                  as batch_exp_per_item,
//                  decode( scan.scan_item_count,
//                          0,0,
//                          (scan.scan_item_inc/scan.scan_item_count) )
//                                                                  as item_inc_per_item,
//                  decode( scan.scan_item_count,
//                          0,0,
//                          (scan.scan_item_exp/scan.scan_item_count) )
//                                                                  as item_exp_per_item                                                                                                                                                                                                
//          from    monthly_extract_summary       sm,
//                  monthly_extract_certegy_scan  scan,
//                  certegy_charge_types          cct  
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                  scan.LOAD_SEC = sm.HH_LOAD_SEC and
//                  cct.CHARGE_TYPE(+) = scan.charge_type
//          order by cct.charge_description, sm.active_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                                  as active_date,\n                cct.CHARGE_DESCRIPTION                          as charge_desc,\n                scan.SCAN_BATCH_COUNT                           as batch_count,\n                scan.scan_item_count                            as item_count,\n                scan.SCAN_BATCH_INC                             as batch_income,\n                scan.SCAN_BATCH_EXP                             as batch_expense,\n                scan.SCAN_ITEM_INC                              as item_income,\n                scan.SCAN_ITEM_EXP                              as item_expense,\n                decode( scan.scan_batch_count,\n                        0,0,\n                        (scan.scan_batch_inc/scan.scan_batch_count) )\n                                                                as batch_inc_per_item,\n                decode( scan.scan_batch_count,\n                        0,0,\n                        (scan.scan_batch_exp/scan.scan_batch_count) )\n                                                                as batch_exp_per_item,\n                decode( scan.scan_item_count,\n                        0,0,\n                        (scan.scan_item_inc/scan.scan_item_count) )\n                                                                as item_inc_per_item,\n                decode( scan.scan_item_count,\n                        0,0,\n                        (scan.scan_item_exp/scan.scan_item_count) )\n                                                                as item_exp_per_item                                                                                                                                                                                                \n        from    monthly_extract_summary       sm,\n                monthly_extract_certegy_scan  scan,\n                certegy_charge_types          cct  \n        where   sm.merchant_number =  :1  and\n                sm.active_date between  :2  and last_day(  :3  ) and\n                scan.LOAD_SEC = sm.HH_LOAD_SEC and\n                cct.CHARGE_TYPE(+) = scan.charge_type\n        order by cct.charge_description, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ProfCertegyDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.ProfCertegyDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:172^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        // setup the description for both income and expense records
        desc.setLength(0);
        desc.append( resultSet.getString("charge_desc") );
        
        activeDate = resultSet.getDate("active_date");
      
        // add the income data
        if ( resultSet.getDouble("batch_income") != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "batch_count" ), resultSet.getDouble("batch_inc_per_item") );
          desc.append(" (PER BATCH) ");
          desc.append( DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
          record.setDescription( desc.toString() );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
        if ( resultSet.getDouble("item_income") != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "item_count" ), resultSet.getDouble("item_inc_per_item") );
          desc.append(" (PER BATCH) ");
          desc.append( DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
          record.setDescription( desc.toString() );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }                              
                            
        // add the expense data
        if ( resultSet.getDouble("batch_expense") != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "batch_count" ), resultSet.getDouble("batch_exp_per_item") );
          desc.append(" (PER BATCH) ");
          desc.append( DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
          record.setDescription( desc.toString() );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_EXPENSE, record );
        }
        if ( resultSet.getDouble("item_expense") != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "item_count" ), resultSet.getDouble("item_exp_per_item") );
          desc.append(" (PER BATCH) ");
          desc.append( DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
          record.setDescription( desc.toString() );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_EXPENSE, record );
        }                              
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailCapture(): ", e.toString());
      ContractDataBean.addError("loadDetailCapture: " + e.toString());
    }                 
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailDCE( long merchantId )
  {
    // not supported by Certegy
  }
  
  protected void loadDetailDebit( long merchantId )
  {
    // not supported by Certegy extract
  }
  
  protected void loadDetailDiscount( long merchantId )
  {
    Date                          activeDate            = null;
    double                        adjAmount             = 0.0;
    int                           adjCount              = 0;
    double                        betIcExpense          = 0.0;
    int                           betType               = BankContractBean.BET_NONE;
    double                        calculatedDiscIncome  = 0.0;
    double                        collectedDiscIncome   = 0.0;
    StringBuffer                  desc                  = new StringBuffer("");
    double                        discExpense           = 0.0;
    double                        icTemp                = 0.0;
    ResultSetIterator             it                    = null;
    ContractTypes.PerItemRecord   record                = null;
    ResultSet                     resultSet             = null;
    
    try
    {
      betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC );
    
      // first load the discount data
      /*@lineinfo:generated-code*//*@lineinfo:267^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                          as active_date,
//                  pl.charge_type                          as charge_type,
//                  cct.CHARGE_DESCRIPTION                  as charge_desc,
//                  pl.SALES_AMOUNT                         as sales_amount,
//                  (pl.sales_count + pl.credits_count)     as item_count,
//                  pl.DISCOUNT_RATE                        as disc_rate,
//                  decode( (pl.sales_count + pl.credits_count),
//                          0, 0,
//                          ( pl.tot_inc_disc_per_item/ (pl.sales_count + pl.credits_count) ) )
//                                                          as per_item_rate_inc,
//                  ( pl.tot_inc_disc_rate + 
//                    pl.tot_inc_disc_per_item )            as disc_inc,
//                  pl.TOT_INC_DISC_RATE                    as disc_rate_inc,
//                  pl.tot_inc_disc_per_item                as disc_per_item_inc,
//                  ( pl.tot_exp_disc_rate + 
//                    pl.tot_exp_disc_per_item )            as disc_exp,
//                  pl.tot_exp_disc_rate                    as disc_rate_exp,
//                  pl.tot_exp_disc_per_item                as disc_per_item_exp,
//                  decode( (pl.sales_count + pl.credits_count),
//                          0, 0,
//                          ( pl.tot_exp_disc_per_item/ (pl.sales_count + pl.credits_count) ) )
//                                                          as per_item_rate_exp
//          from    monthly_extract_summary         sm,
//                  monthly_extract_certegy_pl      pl,
//                  certegy_charge_types            cct
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                  pl.load_sec = sm.hh_load_sec and
//                  -- ignore visa/mc assessments, expense is picked
//                  -- up in the interchange details, not discount
//                  -- see certegy_charge_types table for type defs
//                  pl.charge_type not in ('1540','1550') and   
//                  cct.charge_type = pl.charge_type
//          order by cct.charge_description, sm.active_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                          as active_date,\n                pl.charge_type                          as charge_type,\n                cct.CHARGE_DESCRIPTION                  as charge_desc,\n                pl.SALES_AMOUNT                         as sales_amount,\n                (pl.sales_count + pl.credits_count)     as item_count,\n                pl.DISCOUNT_RATE                        as disc_rate,\n                decode( (pl.sales_count + pl.credits_count),\n                        0, 0,\n                        ( pl.tot_inc_disc_per_item/ (pl.sales_count + pl.credits_count) ) )\n                                                        as per_item_rate_inc,\n                ( pl.tot_inc_disc_rate + \n                  pl.tot_inc_disc_per_item )            as disc_inc,\n                pl.TOT_INC_DISC_RATE                    as disc_rate_inc,\n                pl.tot_inc_disc_per_item                as disc_per_item_inc,\n                ( pl.tot_exp_disc_rate + \n                  pl.tot_exp_disc_per_item )            as disc_exp,\n                pl.tot_exp_disc_rate                    as disc_rate_exp,\n                pl.tot_exp_disc_per_item                as disc_per_item_exp,\n                decode( (pl.sales_count + pl.credits_count),\n                        0, 0,\n                        ( pl.tot_exp_disc_per_item/ (pl.sales_count + pl.credits_count) ) )\n                                                        as per_item_rate_exp\n        from    monthly_extract_summary         sm,\n                monthly_extract_certegy_pl      pl,\n                certegy_charge_types            cct\n        where   sm.merchant_number =  :1  and\n                sm.active_date between  :2  and last_day(  :3  ) and\n                pl.load_sec = sm.hh_load_sec and\n                -- ignore visa/mc assessments, expense is picked\n                -- up in the interchange details, not discount\n                -- see certegy_charge_types table for type defs\n                pl.charge_type not in ('1540','1550') and   \n                cct.charge_type = pl.charge_type\n        order by cct.charge_description, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ProfCertegyDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.ProfCertegyDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:303^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        
        // load the income
        if ( resultSet.getDouble("disc_inc") != 0.0 )
        {
          record = new ContractTypes.VolumeRecord( activeDate,
                                                   ContractTypes.CONTRACT_SOURCE_VITAL, 
                                                   betType, resultSet.getInt("item_count" ),
                                                   resultSet.getDouble("sales_amount"),
                                                   resultSet.getDouble("per_item_rate_inc" ),
                                                   resultSet.getDouble("disc_rate"),
                                                   resultSet.getDouble("disc_rate_inc") );
           
          record.setDescription( resultSet.getString("charge_desc") + " " +
                                 DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
    
          // load income & expense summary records
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT, ContractTypes.CONTRACT_TYPE_INCOME, record );
        
          calculatedDiscIncome  += resultSet.getDouble("disc_inc");
        }          
        
        // load the expense
        if ( resultSet.getDouble("disc_exp") != 0.0 )
        {
          record = new ContractTypes.VolumeRecord( activeDate,
                                                   ContractTypes.CONTRACT_SOURCE_VITAL, 
                                                   betType, resultSet.getInt("item_count" ),
                                                   resultSet.getDouble("sales_amount"),
                                                   resultSet.getDouble("per_item_rate_exp" ),
                                                   resultSet.getDouble("disc_rate"),
                                                   resultSet.getDouble("disc_rate_exp") );
           
          record.setDescription( resultSet.getString("charge_desc") + " " +
                                 DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
    
          // load income & expense summary records
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT, ContractTypes.CONTRACT_TYPE_EXPENSE, record );
        }          
      }   // end while( resultSet.next() )
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:350^7*/

//  ************************************************************
//  #sql [Ctx] { select sum( sm.tot_inc_discount ) 
//          from   monthly_extract_summary sm
//          where  sm.merchant_number = :merchantId and
//                 sm.active_date between :ReportDateBegin and :ReportDateEnd
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum( sm.tot_inc_discount )  \n        from   monthly_extract_summary sm\n        where  sm.merchant_number =  :1  and\n               sm.active_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ProfCertegyDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   collectedDiscIncome = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:356^7*/
      
      //
      // if these two values do not match, then the difference
      // is minimun discount charges.  Since the floating point
      // math in Java is noisy, it is necessary to mask noise in the 
      // decimal digits beyond 0.01.  Otherwise, the software will
      // occasionally display a Minimun Discount of $0.00
      //
      if ( Math.abs( collectedDiscIncome - calculatedDiscIncome ) >= 0.01 )
      {
        desc.setLength(0);
        desc.append("Minimum Discount Fees ");
        desc.append( DateTimeFormatter.getFormattedDate( ReportDateBegin, "MMM yyyy" ) );
        
        // compare the month and year.  if either value
        // does not match then display the ending date.
        if ( ! ContractDataBean.isSameMonthYear( ReportDateBegin, ReportDateEnd ) )
        {
          desc.append( " - " );
          desc.append( DateTimeFormatter.getFormattedDate( ReportDateEnd, "MMM yyyy" ) );
        }
        
        record = new ContractTypes.PerItemRecord( activeDate, 
                                                  ContractTypes.CONTRACT_SOURCE_VITAL, 
                                                  BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ),
                                                    -1, ( collectedDiscIncome - calculatedDiscIncome ) );
        record.setDescription( desc.toString() );                            
        
        ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT, ContractTypes.CONTRACT_TYPE_INCOME, record );
      }
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDiscountDetails()", e.toString());
      ContractDataBean.addError("loadDiscountDetails: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailEquip( long merchantId )
  {
    Date                            activeDate            = null;
    int                             betType;
    StringBuffer                    desc                  = new StringBuffer("");
    double                          exp                   = 0.0;
    double                          inc                   = 0.0;
    ResultSetIterator               it                    = null;
    ContractTypes.PerItemRecord     record                = null;
    ResultSet                       resultSet             = null;
    int                             sales                 = -1;
    
    try
    {
      // setup a generic BET type for the any summary level items.
      betType = BankContractBean.groupIndexToBetType( ContractDataBean.getReportBetGroup() );
      
      sales = ( ContractDataBean.getReportBetGroup() == BankContractBean.BET_GROUP_EQUIP_SALES ) ? 1 : 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:418^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                              as active_date,
//                  eq.EQUIP_COUNT                              as item_count,
//                  decode( eq.equip_count,
//                          0, eq.equip_inc,
//                          ( eq.equip_inc/eq.equip_count ) )   as inc_per_item,
//                  eq.EQUIP_INC                                as income,
//                  eq.equip_exp                                as expense,
//                  decode( eq.equip_count,
//                          0, eq.equip_exp,
//                          ( eq.equip_exp/eq.equip_count ) )   as exp_per_item,
//                  cct.charge_description                      as charge_desc
//          from    group_merchant                  gm,
//                  monthly_extract_summary         sm,
//                  monthly_extract_certegy_equip   eq,
//                  certegy_charge_types            cct
//          where   gm.org_num = :ReportOrgId and
//                  sm.merchant_number = gm.merchant_number and
//                  sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                  eq.load_sec = sm.hh_load_sec and
//                  cct.charge_type = eq.charge_type and                
//                  (
//                     ( :sales = 1 and not cct.charge_description like '%RENT%' ) or
//                     ( :sales = 0 and cct.charge_description like '%RENT%' ) 
//                   )
//          order by cct.charge_description      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                              as active_date,\n                eq.EQUIP_COUNT                              as item_count,\n                decode( eq.equip_count,\n                        0, eq.equip_inc,\n                        ( eq.equip_inc/eq.equip_count ) )   as inc_per_item,\n                eq.EQUIP_INC                                as income,\n                eq.equip_exp                                as expense,\n                decode( eq.equip_count,\n                        0, eq.equip_exp,\n                        ( eq.equip_exp/eq.equip_count ) )   as exp_per_item,\n                cct.charge_description                      as charge_desc\n        from    group_merchant                  gm,\n                monthly_extract_summary         sm,\n                monthly_extract_certegy_equip   eq,\n                certegy_charge_types            cct\n        where   gm.org_num =  :1  and\n                sm.merchant_number = gm.merchant_number and\n                sm.active_date between  :2  and  :3  and\n                eq.load_sec = sm.hh_load_sec and\n                cct.charge_type = eq.charge_type and                \n                (\n                   (  :4  = 1 and not cct.charge_description like '%RENT%' ) or\n                   (  :5  = 0 and cct.charge_description like '%RENT%' ) \n                 )\n        order by cct.charge_description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.ProfCertegyDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ReportOrgId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   __sJT_st.setInt(4,sales);
   __sJT_st.setInt(5,sales);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.ProfCertegyDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:445^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate  = resultSet.getDate("active_date");
        inc         = resultSet.getDouble("income");
        exp         = resultSet.getDouble("expense");
        
        if ( inc != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "item_count" ),
                                      resultSet.getDouble("inc_per_item") );
          desc.setLength(0);
          desc.append( resultSet.getString("charge_desc") );
          desc.append( " - " );
          desc.append( DateTimeFormatter.getFormattedDate( activeDate,"MMM yyyy" ) );
          record.setDescription( desc.toString() );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
        
        if ( exp != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate,
                                      ContractTypes.CONTRACT_SOURCE_VITAL, 
                                      betType, resultSet.getInt( "item_count" ),
                                      resultSet.getDouble("exp_per_item") );
          desc.setLength(0);
          desc.append( resultSet.getString("charge_desc") );
          desc.append( " - " );
          desc.append( DateTimeFormatter.getFormattedDate( activeDate,"MMM yyyy" ) );
          record.setDescription( desc.toString() );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_EXPENSE, record );
        }
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailEquip(): ", e.toString());
      ContractDataBean.addError("loadDetailEquip: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailPlan( long merchantId )
  {
    Date                            activeDate            = null;
    int                             betType;
    ResultSetIterator               it                    = null;
    ContractTypes.PerItemRecord     record                = null;
    ResultSet                       resultSet             = null;
    
    try
    {
      betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_PLAN_FEES );
    
      /*@lineinfo:generated-code*//*@lineinfo:507^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                          as active_date,
//                  decode( pl.card_type,
//                          '30', 'American Express Volume',
//                          '31', 'Diners Volume',
//                          '32', 'Discover Volume',
//                          '33', 'JCB Volume',
//                          nvl(cct.charge_description,('Card Type ' || pl.card_type))
//                         )                                as charge_desc,
//                  (pl.sales_count + pl.credits_count)     as item_count,
//                  decode( (pl.sales_count + pl.credits_count),
//                          0, 0,
//                          ( (pl.tot_inc_disc_rate + pl.tot_inc_disc_per_item)/(pl.sales_count + pl.credits_count) ) )
//                                                          as per_item_rate_inc,
//                  decode( (pl.sales_count + pl.credits_count),
//                          0, 0,
//                          ( (pl.tot_exp_disc_rate + pl.tot_exp_disc_per_item)/(pl.sales_count + pl.credits_count) ) )
//                                                          as per_item_rate_exp,
//                  (pl.tot_inc_disc_rate + 
//                   pl.tot_inc_disc_per_item )             as income,
//                  (pl.tot_exp_disc_rate + 
//                   pl.tot_exp_disc_per_item )             as expense                                                        
//          from    group_merchant                gm,
//                  monthly_extract_summary       sm,
//                  monthly_extract_certegy_pl    pl,
//                  certegy_charge_types          cct
//          where   gm.org_num = :ReportOrgId and
//                  sm.merchant_number = gm.merchant_number and
//                  sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                  pl.load_sec = sm.hh_load_sec and
//                  substr(pl.card_type,1,1) not in ( '4','5','6' ) and
//                  cct.charge_type(+) = pl.charge_type
//          order by sm.active_date, pl.card_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                          as active_date,\n                decode( pl.card_type,\n                        '30', 'American Express Volume',\n                        '31', 'Diners Volume',\n                        '32', 'Discover Volume',\n                        '33', 'JCB Volume',\n                        nvl(cct.charge_description,('Card Type ' || pl.card_type))\n                       )                                as charge_desc,\n                (pl.sales_count + pl.credits_count)     as item_count,\n                decode( (pl.sales_count + pl.credits_count),\n                        0, 0,\n                        ( (pl.tot_inc_disc_rate + pl.tot_inc_disc_per_item)/(pl.sales_count + pl.credits_count) ) )\n                                                        as per_item_rate_inc,\n                decode( (pl.sales_count + pl.credits_count),\n                        0, 0,\n                        ( (pl.tot_exp_disc_rate + pl.tot_exp_disc_per_item)/(pl.sales_count + pl.credits_count) ) )\n                                                        as per_item_rate_exp,\n                (pl.tot_inc_disc_rate + \n                 pl.tot_inc_disc_per_item )             as income,\n                (pl.tot_exp_disc_rate + \n                 pl.tot_exp_disc_per_item )             as expense                                                        \n        from    group_merchant                gm,\n                monthly_extract_summary       sm,\n                monthly_extract_certegy_pl    pl,\n                certegy_charge_types          cct\n        where   gm.org_num =  :1  and\n                sm.merchant_number = gm.merchant_number and\n                sm.active_date between  :2  and last_day(  :3  ) and\n                pl.load_sec = sm.hh_load_sec and\n                substr(pl.card_type,1,1) not in ( '4','5','6' ) and\n                cct.charge_type(+) = pl.charge_type\n        order by sm.active_date, pl.card_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.ProfCertegyDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ReportOrgId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.ProfCertegyDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:541^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        
        if ( resultSet.getDouble( "income" ) != 0.0 )
        { 
          record = new ContractTypes.PerItemRecord( activeDate,ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "item_count" ), resultSet.getDouble("per_item_rate_inc") );
          record.setDescription( resultSet.getString("charge_desc") + " " +
                                 DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
        
        if ( resultSet.getDouble( "expense" ) != 0.0 )
        { 
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "item_count" ), resultSet.getDouble("per_item_rate_exp") );
          record.setDescription( resultSet.getString("charge_desc") + " " +
                                 DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              ContractTypes.CONTRACT_TYPE_EXPENSE, record );
        }
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailPlan(): ", e.toString());
      ContractDataBean.addError("loadDetailPlan: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  protected void loadDetailSysFees( long merchantId )
  {
    Date                            activeDate            = null;
    double                          adjAmount             = 0.0;
    int                             adjCount              = 0;
    int                             betType;
    StringBuffer                    desc                  = new StringBuffer("");
    boolean                         includeVitalExp       = false;
    ResultSetIterator               it                    = null;
    ContractTypes.PerItemRecord     record                = null;
    ResultSet                       resultSet             = null;
    double                          totalCalculatedIncome = 0.0;
    double                          totalCollectedIncome  = 0.0;
    double                          vitalExpense          = 0.0;
    
    try
    {
      // setup a generic BET type for the any summary level items.
      betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_SYSTEM_FEES );
    
      /*@lineinfo:generated-code*//*@lineinfo:600^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.active_date                            as active_date,
//                  cct.charge_description                    as charge_desc,
//                  misc.MISC_COUNT                           as item_count,
//                  misc.MISC_INC                             as income,
//                  decode( misc.misc_count,
//                          0, misc.misc_inc,
//                          (misc.misc_inc/misc.misc_count))  as per_item_inc,                       
//                  misc.misc_exp                             as expense,
//                  decode( misc.misc_count,
//                          0, misc.misc_inc,
//                          (misc.misc_exp/misc.misc_count))  as per_item_exp              
//          from    group_merchant                gm,
//                  monthly_extract_summary       sm,
//                  monthly_extract_certegy_misc  misc,
//                  certegy_charge_types          cct
//          where   gm.org_num = :ReportOrgId and 
//                  sm.merchant_number = gm.merchant_number and
//                  sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                  misc.load_sec = sm.hh_load_sec and
//                  cct.charge_type = misc.charge_type
//          order by cct.charge_description      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.active_date                            as active_date,\n                cct.charge_description                    as charge_desc,\n                misc.MISC_COUNT                           as item_count,\n                misc.MISC_INC                             as income,\n                decode( misc.misc_count,\n                        0, misc.misc_inc,\n                        (misc.misc_inc/misc.misc_count))  as per_item_inc,                       \n                misc.misc_exp                             as expense,\n                decode( misc.misc_count,\n                        0, misc.misc_inc,\n                        (misc.misc_exp/misc.misc_count))  as per_item_exp              \n        from    group_merchant                gm,\n                monthly_extract_summary       sm,\n                monthly_extract_certegy_misc  misc,\n                certegy_charge_types          cct\n        where   gm.org_num =  :1  and \n                sm.merchant_number = gm.merchant_number and\n                sm.active_date between  :2  and last_day(  :3  ) and\n                misc.load_sec = sm.hh_load_sec and\n                cct.charge_type = misc.charge_type\n        order by cct.charge_description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.ProfCertegyDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ReportOrgId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.ProfCertegyDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:623^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        if ( resultSet.getDouble("income") != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate,
                                                    ContractTypes.CONTRACT_SOURCE_VITAL, betType, 
                                                    resultSet.getInt( "ITEM_COUNT" ), resultSet.getDouble("per_item_inc") );
          record.setDescription( resultSet.getString("charge_desc") + " " +
                                 DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                              ContractTypes.CONTRACT_TYPE_INCOME, record );
        }
        
        if ( resultSet.getDouble("expense") != 0.0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, 
                                                    resultSet.getInt( "ITEM_COUNT" ), resultSet.getDouble("per_item_exp") );
          record.setDescription( resultSet.getString("charge_desc") + " " +
                                 DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
          ContractDataBean.addContractElement( ReportOrgId, ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                                              ContractTypes.CONTRACT_TYPE_EXPENSE, record );
        }                                              
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      ContractDataBean.logEntry("loadDetailSysFees(): ", e.toString());
      ContractDataBean.addError("loadDetailSysFees: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
}/*@lineinfo:generated-code*/