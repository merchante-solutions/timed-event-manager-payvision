/*@lineinfo:filename=CBTMilitaryDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CBTMilitaryDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class CBTMilitaryDataBean extends ReportSQLJBean
{
  public static final int   DT_IC               = 0;
  public static final int   DT_AUTH             = 1;
  public static final int   DT_CAPTURE          = 2;
  public static final int   DT_EQUIP            = 3;
  public static final int   DT_TE               = 4;
  
  
  public class RowData
  {
    public    long            MerchantId          = 0L;
    public    String          DbaName             = null;
    public    double          VisaTotalAmount     = 0.0;
    public    double          VisaQualAmount      = 0.0;
    public    double          VisaMidQualAmount   = 0.0;
    public    double          VisaNonQualAmount   = 0.0;
    public    double          McTotalAmount       = 0.0;
    public    double          McQualAmount        = 0.0;
    public    double          McMidQualAmount     = 0.0;
    public    double          McNonQualAmount     = 0.0;
    public    int             AuthCount           = 0;
    public    double          AuthFees            = 0.0;
    public    int             CaptureCount        = 0;
    public    double          CaptureFees         = 0.0;
    public    int             TerminalAuthCount   = 0;
    public    double          TerminalAuthFees    = 0.0;
    public    int             VoiceAuthCount      = 0;
    public    double          VoiceAuthFees       = 0.0;
    public    int             ARUCount            = 0;
    public    double          ARUFees             = 0.0;
    public    int             VRUCount            = 0;
    public    double          VRUFees             = 0.0;
    public    int             DiscoverCount       = 0;
    public    double          DiscoverFees        = 0.0;
    public    int             AmexCount           = 0;
    public    double          AmexFees            = 0.0;
    public    double          RentalFee           = 0.0;
    public    double          TotalFees           = 0.0;
    public    double          TotalVolume         = 0.0;
    public    double          VmcSales            = 0.0;
    public    double          VmcVolume           = 0.0;
    
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      double            mcAmount      = 0.0;
      double            visaAmount    = 0.0;
      
      MerchantId  = resultSet.getLong("merchant_number");
      DbaName     = resultSet.getString("dba_name");
      
      VisaTotalAmount  = resultSet.getDouble("visa_sales");
      McTotalAmount    = resultSet.getDouble("mc_sales");
      VisaQualAmount   = VisaTotalAmount;
      McQualAmount     = McTotalAmount;
      TotalVolume      = (VisaTotalAmount + McTotalAmount);
      VmcSales         = resultSet.getDouble("vmc_sales");
      VmcVolume        = resultSet.getDouble("vmc_volume");
    }
    
    public void addAuthData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AuthCount          = resultSet.getInt("auth_count");
      AuthFees           = resultSet.getDouble("auth_fees");
      TerminalAuthCount  = resultSet.getInt("te_count");
      TerminalAuthFees   = resultSet.getDouble("te_fees");
      VoiceAuthCount     = resultSet.getInt("vo_count");
      VoiceAuthFees      = resultSet.getDouble("vo_fees");
      ARUCount           = resultSet.getInt("aru_count");
      ARUFees            = resultSet.getDouble("aru_fees");
      VRUCount           = resultSet.getInt("vru_count");
      VRUFees            = resultSet.getDouble("vru_fees");
    }
    
    public void addCaptureData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CaptureCount = resultSet.getInt("capture_count");
      CaptureFees  = resultSet.getDouble("capture_fees");
    }
    
    public void addEquipData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      RentalFee = resultSet.getDouble("rental_fee");
    }
    
    public void addIcData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      String      smsg      = null;
      
      smsg = resultSet.getString("smsg");
      
      if ( smsg.indexOf("NON QUAL") >= 0 )
      {
        if ( smsg.indexOf("VS") >= 0 )
        {
          VisaNonQualAmount += resultSet.getDouble("sales_amount");
          
          VisaQualAmount -= resultSet.getDouble("sales_amount");
        } 
        else if ( smsg.indexOf("MC") >= 0 )
        {
          McNonQualAmount += resultSet.getDouble("sales_amount");
          
          McQualAmount -= resultSet.getDouble("sales_amount");
        }
      }
      else if ( smsg.indexOf("MID QUAL") >= 0 )
      {
        if ( smsg.indexOf("VS") >= 0 )
        {
          VisaMidQualAmount += resultSet.getDouble("sales_amount");
          
          VisaQualAmount -= resultSet.getDouble("sales_amount");
        } 
        else if ( smsg.indexOf("MC") >= 0 )
        {
          McMidQualAmount += resultSet.getDouble("sales_amount");
          
          McQualAmount -= resultSet.getDouble("sales_amount");
        }
      }
    }
    
    public void addTEData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      DiscoverCount = resultSet.getInt("disc_count");
      DiscoverFees  = resultSet.getDouble("disc_fees");
      AmexCount     = resultSet.getInt("amex_count");
      AmexFees      = resultSet.getDouble("amex_fees");
    }
    
    public void showData( )
    {
      String      label     = null;
      
      System.out.println("MerchantId: " + MerchantId);
      System.out.println("DbaName   : " + DbaName);
      
      label = "Visa Qual Period: ";
      System.out.println("  " + label + MesMath.toCurrency(VisaQualAmount));
      
      label = "Visa Mid Period: ";
      System.out.println("  " + label + MesMath.toCurrency(VisaMidQualAmount));
      
      label = "Visa Non Period: ";
      System.out.println("  " + label + MesMath.toCurrency(VisaNonQualAmount));
      
      label = "MC   Period: ";
      System.out.println("  " + label + MesMath.toCurrency(McQualAmount));

      label = "Auth Period: ";
      System.out.println("  " + label + AuthCount + " " + MesMath.toCurrency(AuthFees));

      label = "TE   Period: ";
      System.out.println("  " + label + TerminalAuthCount + " " + MesMath.toCurrency(TerminalAuthFees));

      label = "VO   Period: ";
      System.out.println("  " + label + VoiceAuthCount + " " + MesMath.toCurrency(VoiceAuthFees));

      label = "ARU  Period: ";
      System.out.println("  " + label + ARUCount + " " + MesMath.toCurrency(ARUFees));

      label = "VRU  Period: ";
      System.out.println("  " + label + VRUCount + " " + MesMath.toCurrency(VRUFees));

      label = "Capt Period: ";
      System.out.println("  " + label + CaptureCount + " " + MesMath.toCurrency(CaptureFees));
      
      label = "Rent Period: ";
      System.out.println("  " + label + MesMath.toCurrency(RentalFee));
    }      
  }
  
  public CBTMilitaryDataBean( )
  {
  }
  
  protected void addData( ResultSet resultSet, int dataType )
  {
    long          merchantId      = 0L;
    RowData       row             = null;
    
    try
    {
      merchantId = resultSet.getLong("merchant_number");
      row = findRow(merchantId);    
      
      // first query is more restrictive than "add"
      // queries.  therefore if the account does
      // not exist in the first query, ignore in
      // subsequent "add" queries.
      if ( row != null )
      {
        switch( dataType )
        {
          case DT_IC:
            row.addIcData( resultSet );
            break;
          
          case DT_AUTH:
            row.addAuthData( resultSet );
            break;
          
          case DT_CAPTURE:
            row.addCaptureData( resultSet );
            break;
          
          case DT_EQUIP:
            row.addEquipData( resultSet );
            break;
          
          case DT_TE:
            row.addTEData( resultSet );
            break;
        }        
      }        
    }
    catch( Exception e )
    {
      logEntry("addData()", e.toString());
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    try
    {
      line.setLength(0);
      line.append("\"Merchant Number\",");
      line.append("\"Merchant Name\",");
      line.append("\"Current Period ");
      line.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"dd MMM yyyy") );
      line.append("...");
      line.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"dd MMM yyyy") );
      line.append("\",");
    }
    catch( Exception e )
    {
      logEntry("encodeHeaderCSV()",e.toString());
    }      
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.MerchantId );
    line.append(",");
    line.append( record.DbaName );
    line.append(",\n");
    
    line.append(",\"Visa Qualified\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.VisaQualAmount));
    line.append("\"");
    line.append("\n");
    
    line.append(",\"Visa Mid Qual\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.VisaMidQualAmount) );
    line.append("\"");
    line.append("\n");
    
    
    line.append(",\"Visa Non Qual\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.VisaNonQualAmount) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"Total Visa Sales\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.VisaQualAmount +
                                    record.VisaMidQualAmount +
                                    record.VisaNonQualAmount) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"MC Qualified\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.McQualAmount) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"MC Mid Qual\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.McMidQualAmount) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"MC Non Qual\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.McNonQualAmount) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"Total MC Sales\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.McQualAmount +
                                    record.McMidQualAmount +
                                    record.McNonQualAmount) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"# Authorizations\"");
    line.append(",\"");
    line.append( record.AuthCount );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"$ Authorization Fee\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.AuthFees) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"# Captured Items\"");
    line.append(",\"");
    line.append( record.CaptureCount );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"$ Capture Fees\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.CaptureFees) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"# Terminal Auths\"");
    line.append(",\"");
    line.append( record.TerminalAuthCount );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"$ Terminal Fees\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.TerminalAuthFees) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"# Voice Auths\"");
    line.append(",\"");
    line.append( record.VoiceAuthCount );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"$ Voice Auth Fees\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.VoiceAuthFees) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"# ARU Auths\"");
    line.append(",\"");
    line.append( record.ARUCount );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"$ ARU Auth Fees\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.ARUFees) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"# VRU Auths\"");
    line.append(",\"");
    line.append( record.VRUCount );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"$ VRU Auth Fees\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.VRUFees) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"# Discover Transactions\"");
    line.append(",\"");
    line.append( record.DiscoverCount );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"$ Discover Fees\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.DiscoverFees) );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"# Amex Transactions\"");
    line.append(",\"");
    line.append( record.AmexCount );
    line.append("\"");
    line.append("\n");
    
    line.append(",\"$ Amex Fees\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.AmexFees) );
    line.append("\"");
    line.append("\n");
    
    
    line.append(",\"Terminal Rental Fees\"");
    line.append(",\"");
    line.append( MesMath.toCurrency(record.RentalFee) );
    line.append("\"");
    line.append("\n");
  }
  
  protected RowData findRow( long merchantId )
  {
    RowData   retVal    = null;
    
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      if ( ((RowData)ReportRows.elementAt(i)).MerchantId == merchantId )
      {
        retVal = (RowData)ReportRows.elementAt(i);
      }
    }
    return( retVal );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_mil_volume_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }

  public String getDateStringPeriod( )
    throws java.sql.SQLException
  {
    Date            endDate   = null;
    StringBuffer    retVal    = new StringBuffer();
    
    retVal.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"dd MMM yyyy") );
    retVal.append("...");
    /*@lineinfo:generated-code*//*@lineinfo:506^5*/

//  ************************************************************
//  #sql [Ctx] { select  last_day(:ReportDateEnd) 
//        from    dual
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  last_day( :1 )  \n      from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,ReportDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   endDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:510^5*/
    retVal.append( DateTimeFormatter.getFormattedDate(endDate,"dd MMM yyyy") );
    return( retVal.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator     it                = null;
    long                  nodeId            = orgIdToHierarchyNode(orgId);
    ResultSet             resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();

      if ( isNodeMerchant(nodeId) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:540^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number            as merchant_number,
//                    mf.dba_name                   as dba_name,
//                    sum( sm.visa_sales_amount )   as visa_sales,
//                    sum( sm.mc_sales_amount )     as mc_sales,
//                    sum( sm.vmc_sales_amount )    as vmc_sales,
//                    sum( sm.vmc_vol_amount )      as vmc_volume
//            from    monthly_extract_summary     sm,
//                    mif                         mf
//            where   sm.merchant_number = :nodeId and
//                    sm.active_date between :beginDate and :endDate and
//                    sm.assoc_hierarchy_node like '3858555%' and
//                    mf.merchant_number = sm.merchant_number
//            group by sm.merchant_number, mf.dba_name        
//            order by sm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number            as merchant_number,\n                  mf.dba_name                   as dba_name,\n                  sum( sm.visa_sales_amount )   as visa_sales,\n                  sum( sm.mc_sales_amount )     as mc_sales,\n                  sum( sm.vmc_sales_amount )    as vmc_sales,\n                  sum( sm.vmc_vol_amount )      as vmc_volume\n          from    monthly_extract_summary     sm,\n                  mif                         mf\n          where   sm.merchant_number =  :1  and\n                  sm.active_date between  :2  and  :3  and\n                  sm.assoc_hierarchy_node like '3858555%' and\n                  mf.merchant_number = sm.merchant_number\n          group by sm.merchant_number, mf.dba_name        \n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:556^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:560^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number            as merchant_number,
//                    mf.dba_name                   as dba_name,
//                    sum( sm.visa_sales_amount )   as visa_sales,
//                    sum( sm.mc_sales_amount )     as mc_sales,
//                    sum( sm.vmc_sales_amount )    as vmc_sales,
//                    sum( sm.vmc_vol_amount )      as vmc_volume
//            from    t_hierarchy                 th,
//                    monthly_extract_summary     sm,
//                    mif                         mf
//            where   th.hier_type = 1 and
//                    th.ancestor = :nodeId and
//                    th.entity_type = 4 and
//                    th.descendent like '3858555%' and
//                    sm.assoc_hierarchy_node = th.descendent and
//                    sm.active_date between :beginDate and :endDate and
//                    mf.merchant_number = sm.merchant_number
//            group by sm.merchant_number, mf.dba_name        
//            order by sm.merchant_number      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number            as merchant_number,\n                  mf.dba_name                   as dba_name,\n                  sum( sm.visa_sales_amount )   as visa_sales,\n                  sum( sm.mc_sales_amount )     as mc_sales,\n                  sum( sm.vmc_sales_amount )    as vmc_sales,\n                  sum( sm.vmc_vol_amount )      as vmc_volume\n          from    t_hierarchy                 th,\n                  monthly_extract_summary     sm,\n                  mif                         mf\n          where   th.hier_type = 1 and\n                  th.ancestor =  :1  and\n                  th.entity_type = 4 and\n                  th.descendent like '3858555%' and\n                  sm.assoc_hierarchy_node = th.descendent and\n                  sm.active_date between  :2  and  :3  and\n                  mf.merchant_number = sm.merchant_number\n          group by sm.merchant_number, mf.dba_name        \n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:580^9*/
      }
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
      
      loadInterchangeData(nodeId,beginDate,endDate);
      loadAuthData(nodeId,beginDate,endDate);
      loadCaptureData(nodeId,beginDate,endDate);
      loadTEData(nodeId,beginDate,endDate);
      loadEquipData(nodeId,beginDate,endDate);
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void loadAuthData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      if ( isNodeMerchant(nodeId) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:615^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                      as merchant_number,
//                    sum( decode(ap.a1_media_type,
//                                'TE',ap.auth_count_total,
//                                0 ) )                       as te_count,
//                    sum( decode(ap.a1_media_type,
//                                'VO',ap.auth_count_total,
//                                0 ) )                       as vo_count,
//                    sum( decode(ap.a1_media_type,
//                                'AR',ap.auth_count_total,
//                                0 ) )                       as aru_count,
//                    sum( decode(ap.a1_media_type,
//                                'VR',ap.auth_count_total,
//                                0 ) )                       as vru_count,
//                    sum( ap.auth_count_total )              as auth_count,
//                    sum( decode(ap.a1_media_type,
//                                'TE',ap.auth_income_total,
//                                0 ) )                       as te_fees,
//                    sum( decode(ap.a1_media_type,
//                                'VO',ap.auth_income_total,
//                                0 ) )                       as vo_fees,
//                    sum( decode(ap.a1_media_type,
//                                'AR',ap.auth_income_total,
//                                0 ) )                       as aru_fees,
//                    sum( decode(ap.a1_media_type,
//                                'VR',ap.auth_income_total,
//                                0 ) )                       as vru_fees,
//                    sum( ap.auth_income_total )             as auth_fees
//            from    monthly_extract_summary     sm,
//                    monthly_extract_ap          ap
//            where   sm.merchant_number = :nodeId and
//                    sm.active_date between :beginDate and :endDate and
//                    ap.hh_load_sec(+) = sm.hh_load_sec and
//                    ap.auth_income_total > 0
//            group by sm.merchant_number
//            order by sm.merchant_number      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                      as merchant_number,\n                  sum( decode(ap.a1_media_type,\n                              'TE',ap.auth_count_total,\n                              0 ) )                       as te_count,\n                  sum( decode(ap.a1_media_type,\n                              'VO',ap.auth_count_total,\n                              0 ) )                       as vo_count,\n                  sum( decode(ap.a1_media_type,\n                              'AR',ap.auth_count_total,\n                              0 ) )                       as aru_count,\n                  sum( decode(ap.a1_media_type,\n                              'VR',ap.auth_count_total,\n                              0 ) )                       as vru_count,\n                  sum( ap.auth_count_total )              as auth_count,\n                  sum( decode(ap.a1_media_type,\n                              'TE',ap.auth_income_total,\n                              0 ) )                       as te_fees,\n                  sum( decode(ap.a1_media_type,\n                              'VO',ap.auth_income_total,\n                              0 ) )                       as vo_fees,\n                  sum( decode(ap.a1_media_type,\n                              'AR',ap.auth_income_total,\n                              0 ) )                       as aru_fees,\n                  sum( decode(ap.a1_media_type,\n                              'VR',ap.auth_income_total,\n                              0 ) )                       as vru_fees,\n                  sum( ap.auth_income_total )             as auth_fees\n          from    monthly_extract_summary     sm,\n                  monthly_extract_ap          ap\n          where   sm.merchant_number =  :1  and\n                  sm.active_date between  :2  and  :3  and\n                  ap.hh_load_sec(+) = sm.hh_load_sec and\n                  ap.auth_income_total > 0\n          group by sm.merchant_number\n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:652^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:656^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                      as merchant_number,
//                    sum( decode(ap.a1_media_type,
//                                'TE',ap.auth_count_total,
//                                0 ) )                       as te_count,
//                    sum( decode(ap.a1_media_type,
//                                'VO',ap.auth_count_total,
//                                0 ) )                       as vo_count,
//                    sum( decode(ap.a1_media_type,
//                                'AR',ap.auth_count_total,
//                                0 ) )                       as aru_count,
//                    sum( decode(ap.a1_media_type,
//                                'VR',ap.auth_count_total,
//                                0 ) )                       as vru_count,
//                    sum( ap.auth_count_total )              as auth_count,
//                    sum( decode(ap.a1_media_type,
//                                'TE',ap.auth_income_total,
//                                0 ) )                       as te_fees,
//                    sum( decode(ap.a1_media_type,
//                                'VO',ap.auth_income_total,
//                                0 ) )                       as vo_fees,
//                    sum( decode(ap.a1_media_type,
//                                'AR',ap.auth_income_total,
//                                0 ) )                       as aru_fees,
//                    sum( decode(ap.a1_media_type,
//                                'VR',ap.auth_income_total,
//                                0 ) )                       as vru_fees,
//                    sum( ap.auth_income_total )             as auth_fees
//            from    t_hierarchy                 th,
//                    monthly_extract_summary     sm,
//                    monthly_extract_ap          ap
//            where   th.hier_type = 1 and
//                    th.ancestor = :nodeId and
//                    th.entity_type = 4 and
//                    sm.assoc_hierarchy_node = th.descendent and
//                    sm.active_date between :beginDate and :endDate and
//                    ap.hh_load_sec(+) = sm.hh_load_sec and
//                    ap.auth_income_total > 0
//            group by sm.merchant_number
//            order by sm.merchant_number      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                      as merchant_number,\n                  sum( decode(ap.a1_media_type,\n                              'TE',ap.auth_count_total,\n                              0 ) )                       as te_count,\n                  sum( decode(ap.a1_media_type,\n                              'VO',ap.auth_count_total,\n                              0 ) )                       as vo_count,\n                  sum( decode(ap.a1_media_type,\n                              'AR',ap.auth_count_total,\n                              0 ) )                       as aru_count,\n                  sum( decode(ap.a1_media_type,\n                              'VR',ap.auth_count_total,\n                              0 ) )                       as vru_count,\n                  sum( ap.auth_count_total )              as auth_count,\n                  sum( decode(ap.a1_media_type,\n                              'TE',ap.auth_income_total,\n                              0 ) )                       as te_fees,\n                  sum( decode(ap.a1_media_type,\n                              'VO',ap.auth_income_total,\n                              0 ) )                       as vo_fees,\n                  sum( decode(ap.a1_media_type,\n                              'AR',ap.auth_income_total,\n                              0 ) )                       as aru_fees,\n                  sum( decode(ap.a1_media_type,\n                              'VR',ap.auth_income_total,\n                              0 ) )                       as vru_fees,\n                  sum( ap.auth_income_total )             as auth_fees\n          from    t_hierarchy                 th,\n                  monthly_extract_summary     sm,\n                  monthly_extract_ap          ap\n          where   th.hier_type = 1 and\n                  th.ancestor =  :1  and\n                  th.entity_type = 4 and\n                  sm.assoc_hierarchy_node = th.descendent and\n                  sm.active_date between  :2  and  :3  and\n                  ap.hh_load_sec(+) = sm.hh_load_sec and\n                  ap.auth_income_total > 0\n          group by sm.merchant_number\n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:697^9*/
      }        
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        addData(resultSet,DT_AUTH);
      }
    }
    catch(Exception e)
    {
      logEntry( "loadAuthData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void loadCaptureData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      if ( isNodeMerchant(nodeId) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:725^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number            as merchant_number,
//                    sum( pl.pl_number_of_sales )  as capture_count,
//                    sum( (pl.pl_disc_rate_per_item * pl.pl_number_of_sales) )
//                                                  as capture_fees
//            from    monthly_extract_summary     sm,
//                    monthly_extract_pl          pl
//            where   sm.merchant_number = :nodeId and
//                    sm.active_date between :beginDate and :endDate and
//                    pl.hh_load_sec = sm.hh_load_sec and
//                    substr(pl.pl_plan_type,1,1) in ('V','M') and
//                    pl.pl_disc_rate_per_item != 0
//            group by sm.merchant_number
//            order by sm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number            as merchant_number,\n                  sum( pl.pl_number_of_sales )  as capture_count,\n                  sum( (pl.pl_disc_rate_per_item * pl.pl_number_of_sales) )\n                                                as capture_fees\n          from    monthly_extract_summary     sm,\n                  monthly_extract_pl          pl\n          where   sm.merchant_number =  :1  and\n                  sm.active_date between  :2  and  :3  and\n                  pl.hh_load_sec = sm.hh_load_sec and\n                  substr(pl.pl_plan_type,1,1) in ('V','M') and\n                  pl.pl_disc_rate_per_item != 0\n          group by sm.merchant_number\n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:740^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:744^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ use_nl (sm pl) */
//                    sm.merchant_number            as merchant_number,
//                    sum( pl.pl_number_of_sales )  as capture_count,
//                    sum( (pl.pl_disc_rate_per_item * pl.pl_number_of_sales) )
//                                                  as capture_fees
//            from    t_hierarchy                 th,
//                    monthly_extract_summary     sm,
//                    monthly_extract_pl          pl
//            where   th.hier_type = 1 and
//                    th.ancestor = :nodeId and
//                    th.entity_type = 4 and
//                    sm.assoc_hierarchy_node = th.descendent and
//                    sm.active_date between :beginDate and :endDate and
//                    pl.hh_load_sec = sm.hh_load_sec and
//                    substr(pl.pl_plan_type,1,1) in ('V','M') and
//                    pl.pl_disc_rate_per_item != 0
//            group by sm.merchant_number
//            order by sm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ use_nl (sm pl) */\n                  sm.merchant_number            as merchant_number,\n                  sum( pl.pl_number_of_sales )  as capture_count,\n                  sum( (pl.pl_disc_rate_per_item * pl.pl_number_of_sales) )\n                                                as capture_fees\n          from    t_hierarchy                 th,\n                  monthly_extract_summary     sm,\n                  monthly_extract_pl          pl\n          where   th.hier_type = 1 and\n                  th.ancestor =  :1  and\n                  th.entity_type = 4 and\n                  sm.assoc_hierarchy_node = th.descendent and\n                  sm.active_date between  :2  and  :3  and\n                  pl.hh_load_sec = sm.hh_load_sec and\n                  substr(pl.pl_plan_type,1,1) in ('V','M') and\n                  pl.pl_disc_rate_per_item != 0\n          group by sm.merchant_number\n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:764^9*/
      }
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        addData(resultSet,DT_CAPTURE);
      }
    }
    catch(Exception e)
    {
      logEntry( "loadCaptureData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void loadEquipData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      if ( isNodeMerchant(nodeId) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:792^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number            as merchant_number,
//                    sum( sm.equip_rental_income ) as rental_fee
//            from    monthly_extract_summary     sm
//            where   sm.merchant_number = :nodeId and
//                    sm.active_date between :beginDate and :endDate
//            group by sm.merchant_number
//            order by sm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number            as merchant_number,\n                  sum( sm.equip_rental_income ) as rental_fee\n          from    monthly_extract_summary     sm\n          where   sm.merchant_number =  :1  and\n                  sm.active_date between  :2  and  :3 \n          group by sm.merchant_number\n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:801^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:805^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number            as merchant_number,
//                    sum( sm.equip_rental_income ) as rental_fee
//            from    t_hierarchy                 th,
//                    monthly_extract_summary     sm
//            where   th.hier_type = 1 and
//                    th.ancestor = :nodeId and
//                    th.entity_type = 4 and
//                    sm.assoc_hierarchy_node = th.descendent and
//                    sm.active_date between :beginDate and :endDate
//            group by sm.merchant_number
//            order by sm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number            as merchant_number,\n                  sum( sm.equip_rental_income ) as rental_fee\n          from    t_hierarchy                 th,\n                  monthly_extract_summary     sm\n          where   th.hier_type = 1 and\n                  th.ancestor =  :1  and\n                  th.entity_type = 4 and\n                  sm.assoc_hierarchy_node = th.descendent and\n                  sm.active_date between  :2  and  :3 \n          group by sm.merchant_number\n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:818^9*/
      }        
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        addData(resultSet,DT_EQUIP);
      }
    }
    catch(Exception e)
    {
      logEntry( "loadEquipData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void loadInterchangeData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      if ( isNodeMerchant(nodeId) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:846^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                    as merchant_number,
//                    st.st_statement_desc                  as smsg,
//                    sum( st.st_number_of_items_on_stmt )  as sales_count,
//                    sum( st.st_amount_of_item_on_stmt )   as sales_amount
//            from    monthly_extract_summary     sm,
//                    monthly_extract_st          st,
//                    mif                         mf
//            where   sm.merchant_number = :nodeId and
//                    sm.active_date between :beginDate and :endDate and
//                    st.hh_load_sec(+) = sm.hh_load_sec and
//                    st.st_amount_of_item_on_stmt > 0 and
//                    not exists
//                    (
//                      select  cg.cg_message_for_stmt
//                      from    monthly_extract_cg cg
//                      where   cg.hh_load_sec = sm.hh_load_sec and
//                              cg.cg_message_for_stmt = st.st_statement_desc 
//                    ) and        
//                    mf.merchant_number = sm.merchant_number
//            group by sm.merchant_number, st.st_statement_desc
//            order by sm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                    as merchant_number,\n                  st.st_statement_desc                  as smsg,\n                  sum( st.st_number_of_items_on_stmt )  as sales_count,\n                  sum( st.st_amount_of_item_on_stmt )   as sales_amount\n          from    monthly_extract_summary     sm,\n                  monthly_extract_st          st,\n                  mif                         mf\n          where   sm.merchant_number =  :1  and\n                  sm.active_date between  :2  and  :3  and\n                  st.hh_load_sec(+) = sm.hh_load_sec and\n                  st.st_amount_of_item_on_stmt > 0 and\n                  not exists\n                  (\n                    select  cg.cg_message_for_stmt\n                    from    monthly_extract_cg cg\n                    where   cg.hh_load_sec = sm.hh_load_sec and\n                            cg.cg_message_for_stmt = st.st_statement_desc \n                  ) and        \n                  mf.merchant_number = sm.merchant_number\n          group by sm.merchant_number, st.st_statement_desc\n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:869^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:873^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                    as merchant_number,
//                    st.st_statement_desc                  as smsg,
//                    sum( st.st_number_of_items_on_stmt )  as sales_count,
//                    sum( st.st_amount_of_item_on_stmt )   as sales_amount
//            from    t_hierarchy                 th,
//                    monthly_extract_summary     sm,
//                    monthly_extract_st          st,
//                    mif                         mf
//            where   th.hier_type = 1 and
//                    th.ancestor = :nodeId and
//                    th.entity_type = 4 and
//                    sm.assoc_hierarchy_node = th.descendent and
//                    sm.active_date between :beginDate and :endDate and
//                    st.hh_load_sec(+) = sm.hh_load_sec and
//                    st.st_amount_of_item_on_stmt > 0 and
//                    not exists
//                    (
//                      select  cg.cg_message_for_stmt
//                      from    monthly_extract_cg cg
//                      where   cg.hh_load_sec = sm.hh_load_sec and
//                              cg.cg_message_for_stmt = st.st_statement_desc 
//                    ) and        
//                    mf.merchant_number = sm.merchant_number
//            group by sm.merchant_number, st.st_statement_desc
//            order by sm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                    as merchant_number,\n                  st.st_statement_desc                  as smsg,\n                  sum( st.st_number_of_items_on_stmt )  as sales_count,\n                  sum( st.st_amount_of_item_on_stmt )   as sales_amount\n          from    t_hierarchy                 th,\n                  monthly_extract_summary     sm,\n                  monthly_extract_st          st,\n                  mif                         mf\n          where   th.hier_type = 1 and\n                  th.ancestor =  :1  and\n                  th.entity_type = 4 and\n                  sm.assoc_hierarchy_node = th.descendent and\n                  sm.active_date between  :2  and  :3  and\n                  st.hh_load_sec(+) = sm.hh_load_sec and\n                  st.st_amount_of_item_on_stmt > 0 and\n                  not exists\n                  (\n                    select  cg.cg_message_for_stmt\n                    from    monthly_extract_cg cg\n                    where   cg.hh_load_sec = sm.hh_load_sec and\n                            cg.cg_message_for_stmt = st.st_statement_desc \n                  ) and        \n                  mf.merchant_number = sm.merchant_number\n          group by sm.merchant_number, st.st_statement_desc\n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:900^9*/
      }
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        addData(resultSet,DT_IC);
      }
    }
    catch(Exception e)
    {
      logEntry( "loadInterchangeData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void loadTEData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      if ( isNodeMerchant(nodeId) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:928^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number            as merchant_number,
//                    sum( decode(pl.pl_plan_type,
//                                'DISC',pl.pl_correct_disc_amt,
//                                0) )              as disc_fees,
//                    sum( decode(pl.pl_plan_type,
//                                'DISC',(pl.pl_number_of_credits + pl.pl_number_of_sales),
//                                0) )              as disc_count,
//                    sum( decode(pl.pl_plan_type,
//                                'AMEX',pl.pl_correct_disc_amt,
//                                0 ) )             as amex_fees,
//                    sum( decode(pl.pl_plan_type,
//                                'AMEX',(pl.pl_number_of_credits + pl.pl_number_of_sales),
//                                0 ) )             as amex_count
//            from    monthly_extract_summary     sm,
//                    monthly_extract_pl          pl
//            where   sm.merchant_number = :nodeId and
//                    sm.active_date between :beginDate and :endDate and
//                    pl.hh_load_sec = sm.hh_load_sec and
//                    pl.pl_plan_type in ('DISC','AMEX')
//            group by sm.merchant_number
//            order by sm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number            as merchant_number,\n                  sum( decode(pl.pl_plan_type,\n                              'DISC',pl.pl_correct_disc_amt,\n                              0) )              as disc_fees,\n                  sum( decode(pl.pl_plan_type,\n                              'DISC',(pl.pl_number_of_credits + pl.pl_number_of_sales),\n                              0) )              as disc_count,\n                  sum( decode(pl.pl_plan_type,\n                              'AMEX',pl.pl_correct_disc_amt,\n                              0 ) )             as amex_fees,\n                  sum( decode(pl.pl_plan_type,\n                              'AMEX',(pl.pl_number_of_credits + pl.pl_number_of_sales),\n                              0 ) )             as amex_count\n          from    monthly_extract_summary     sm,\n                  monthly_extract_pl          pl\n          where   sm.merchant_number =  :1  and\n                  sm.active_date between  :2  and  :3  and\n                  pl.hh_load_sec = sm.hh_load_sec and\n                  pl.pl_plan_type in ('DISC','AMEX')\n          group by sm.merchant_number\n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:951^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:955^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+  
//                      use_nl(sm pl)
//                    */
//                    sm.merchant_number            as merchant_number,
//                    sum( decode(pl.pl_plan_type,
//                                'DISC',pl.pl_correct_disc_amt,
//                                0) )              as disc_fees,
//                    sum( decode(pl.pl_plan_type,
//                                'DISC',(pl.pl_number_of_credits + pl.pl_number_of_sales),
//                                0) )              as disc_count,
//                    sum( decode(pl.pl_plan_type,
//                                'AMEX',pl.pl_correct_disc_amt,
//                                0 ) )             as amex_fees,
//                    sum( decode(pl.pl_plan_type,
//                                'AMEX',(pl.pl_number_of_credits + pl.pl_number_of_sales),
//                                0 ) )             as amex_count
//            from    t_hierarchy                 th,
//                    monthly_extract_summary     sm,
//                    monthly_extract_pl          pl
//            where   th.hier_type = 1 and
//                    th.ancestor = :nodeId and
//                    th.entity_type = 4 and
//                    sm.assoc_hierarchy_node = th.descendent and
//                    sm.active_date between :beginDate and :endDate and
//                    pl.hh_load_sec = sm.hh_load_sec and
//                    pl.pl_plan_type in ('DISC','AMEX')
//            group by sm.merchant_number
//            order by sm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+  \n                    use_nl(sm pl)\n                  */\n                  sm.merchant_number            as merchant_number,\n                  sum( decode(pl.pl_plan_type,\n                              'DISC',pl.pl_correct_disc_amt,\n                              0) )              as disc_fees,\n                  sum( decode(pl.pl_plan_type,\n                              'DISC',(pl.pl_number_of_credits + pl.pl_number_of_sales),\n                              0) )              as disc_count,\n                  sum( decode(pl.pl_plan_type,\n                              'AMEX',pl.pl_correct_disc_amt,\n                              0 ) )             as amex_fees,\n                  sum( decode(pl.pl_plan_type,\n                              'AMEX',(pl.pl_number_of_credits + pl.pl_number_of_sales),\n                              0 ) )             as amex_count\n          from    t_hierarchy                 th,\n                  monthly_extract_summary     sm,\n                  monthly_extract_pl          pl\n          where   th.hier_type = 1 and\n                  th.ancestor =  :1  and\n                  th.entity_type = 4 and\n                  sm.assoc_hierarchy_node = th.descendent and\n                  sm.active_date between  :2  and  :3  and\n                  pl.hh_load_sec = sm.hh_load_sec and\n                  pl.pl_plan_type in ('DISC','AMEX')\n          group by sm.merchant_number\n          order by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.CBTMilitaryDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.CBTMilitaryDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:985^9*/
      }
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        addData(resultSet,DT_TE);
      }
    }
    catch(Exception e)
    {
      logEntry( "loadTEData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.MONTH, -1 );
      
      // since this report is for one month at a time,
      // make sure that the from date always starts with
      // the first day of the specified month.
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      // set both the begin and end dates
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
    }    
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == 394100000L )
    {
      setReportHierarchyNode( 3858000000L );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      ((RowData)ReportRows.elementAt(i)).showData();
    }
  }
  
  public static void main( String[] args )
  {
    Date                    activeDate  = null;
    Calendar                cal         = Calendar.getInstance();
    CBTMilitaryDataBean     test        = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new CBTMilitaryDataBean();
      
      test.connect();
      test.setReportHierarchyNode( 3858555000L );
      
      
      cal.setTime( DateTimeFormatter.parseDate(args[0],"MM/dd/yyyy") );
      activeDate = new java.sql.Date( cal.getTime().getTime() );
      
      test.loadData( test.getReportOrgId(), activeDate, activeDate );
      test.showData(System.out);
    }
    finally
    {
      try{ test.cleanUp(); } catch(Exception e){}
    }
  }
}/*@lineinfo:generated-code*/