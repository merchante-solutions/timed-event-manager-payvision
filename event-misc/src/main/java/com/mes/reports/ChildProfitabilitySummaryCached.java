/*@lineinfo:filename=ChildProfitabilitySummaryCached*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ChildProfitabilitySummaryCached.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 4/02/02 5:03p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import com.mes.support.MesMath;
import sqlj.runtime.ref.DefaultContext;

public class ChildProfitabilitySummaryCached 
  extends ChildProfitabilitySummary
{
  // used to store cached versions of the data  
  protected double[]        Expense       = new double[ BankContractBean.BET_GROUP_COUNT ];
  protected double[]        Income        = new double[ BankContractBean.BET_GROUP_COUNT ];


  public ChildProfitabilitySummaryCached( DefaultContext ctx, long orgId, String orgName, Date beginDate, Date endDate )
  {
    super( ctx, orgId, orgName, beginDate, endDate );
    
    // intialize the storage
    for( int i = 0; i < BankContractBean.BET_GROUP_COUNT; ++i )
    {
      Expense[i]  = 0.0;
      Income[i]   = 0.0;
    }
  }
  
  //
  // At least one embedded SQL statement must be present in the
  // SQLJ file for the -passes compiler option to work correctly.
  // The -passes compiler options allows javac errors to be passed
  // correctly to the shell and is therefore on by default.  This
  // method is never called, but allows the compiler to not choke.
  //  
  private void dummy( )
    throws java.sql.SQLException
  {
    Date      tempDate = null;
    
    /*@lineinfo:generated-code*//*@lineinfo:62^5*/

//  ************************************************************
//  #sql [Ctx] { select  sysdate 
//        from    dual
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sysdate  \n      from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ChildProfitabilitySummaryCached",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   tempDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:66^5*/
  }
  
  public double getAdjustmentsExpense( )
  {
    return( Expense[ BankContractBean.BET_GROUP_ADJUSTMENTS ] );
  }
  
  public double getAdjustmentsIncome( )
  {
    return( Income[ BankContractBean.BET_GROUP_ADJUSTMENTS ] );
  }
  
  public double getDiscIcExpense( )
  {
    return( Expense[ BankContractBean.BET_GROUP_DISC_IC ] );
  }
  
  public double getDiscIcIncome( )
  {
    return( Income[ BankContractBean.BET_GROUP_DISC_IC ] );
  }
  
  public double getFeesTotalExpense( )
  {
    double        retVal  = super.getFeesTotalExpense();
    
    for ( int i = 0; i < BankContractBean.BET_GROUP_COUNT; ++i )
    {
      if ( ( i != BankContractBean.BET_GROUP_DISC_IC ) &&
           ( i != BankContractBean.BET_GROUP_PARTNERSHIP ) &&
           ( i != BankContractBean.BET_GROUP_ADJUSTMENTS ) )
      {
        retVal += Expense[i];
      }
    }
    return( retVal );
  }
  
  public double getFeesTotalIncome( )
  {
    double        retVal  = super.getFeesTotalIncome();
    
    for ( int i = 0; i < BankContractBean.BET_GROUP_COUNT; ++i )
    {
      if ( ( i != BankContractBean.BET_GROUP_DISC_IC ) &&
           ( i != BankContractBean.BET_GROUP_PARTNERSHIP ) &&
           ( i != BankContractBean.BET_GROUP_ADJUSTMENTS ) ) 
      {
        retVal += Income[i];
      }        
    }
    return( retVal );
  }
  
  public double getGroupExpense( int cat )
  {
    return( super.getGroupExpense(cat) + Expense[cat] );
  }
  
  public double getGroupIncome( int cat )
  {
    return( super.getGroupIncome(cat) + Income[cat] );
  }
  
  public double getPartnershipExpense( )
  {
    return( super.getPartnershipExpense() + Expense[BankContractBean.BET_GROUP_PARTNERSHIP] );
  }
  
  public double getPartnershipIncome( )
  {
    return( super.getPartnershipIncome() + Income[BankContractBean.BET_GROUP_PARTNERSHIP] );
  }
  
  public void setCategoryData( int cat, double inc, double exp )
  {
    Expense[cat]  = exp;
    Income[cat]   = inc;
  }

  public void showData( java.io.PrintStream out )
  {
    out.println();
    out.println("************************************************");
    out.println("*  Cached Profitability Data " );
    out.println("*    Org #          : " + OrgId );
    out.println("*    Org Name       : " + OrgName );
    if ( isOrgMerchant() )
    {
      out.println("*    Merchant #     : " + HierarchyNode );
    }
    else
    {
      out.println("*    Hierarchy Node : " + HierarchyNode );
    }
    
    for ( int contractType = 0; contractType < ContractTypes.CONTRACT_TYPE_COUNT; ++contractType )
    {
      if ( contractType == ContractTypes.CONTRACT_TYPE_EXPENSE )
      {
        out.println("************************************************");
        out.println("*  Cached Expense Data" );
        out.println("************************************************");
        out.println(" Disc/Ic Expense : " + MesMath.toCurrency( getDiscIcExpense() ) );
        out.println(" Fees Expense    : " + MesMath.toCurrency( getFeesTotalExpense() ) );
        out.println(" Partnership Exp : " + MesMath.toCurrency( getPartnershipExpense() ) );
      }
      else // income
      {
        out.println("************************************************");
        out.println("*  Cached Income Data" );
        out.println("************************************************");
        out.println(" Disc/Ic Income  : " + MesMath.toCurrency( getDiscIcIncome() ) );
        out.println(" Fees Income     : " + MesMath.toCurrency( getFeesTotalIncome() ) );
        out.println(" Partnership Inc : " + MesMath.toCurrency( getPartnershipIncome() ) );
      }
    }
  }
}/*@lineinfo:generated-code*/