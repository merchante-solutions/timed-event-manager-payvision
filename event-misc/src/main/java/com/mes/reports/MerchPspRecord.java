package com.mes.reports;


public class MerchPspRecord { 
  
  String  sponsoredMerchantName;
  String  businessAddres1; 
  String  businessAddres3;
  String  businessCity;
  String  businessState; 
  String  businessZip;
  String  merchantName;
  long    merchantNumber;
  String  sicCode;
  String  pspName; 
  long    pspId;
  long    salesCount;
  double  salesAmount;
  long    chargebackCount;
  double  chargebackAmount;
  
  public String getSponsoredMerchantName() {
    return sponsoredMerchantName;
  }
  public void setSponsoredMerchantName(String sponsoredMerchantName) {
    this.sponsoredMerchantName = sponsoredMerchantName;
  }
  public String getBusinessAddres1() {
    return businessAddres1;
  }
  public void setBusinessAddres1(String businessAddres1) {
    this.businessAddres1 = businessAddres1;
  }
  public String getBusinessAddres3() {
    return businessAddres3;
  }
  public void setBusinessAddres3(String businessAddres3) {
    this.businessAddres3 = businessAddres3;
  }
  public String getBusinessCity() {
    return businessCity;
  }
  public void setBusinessCity(String businessCity) {
    this.businessCity = businessCity;
  }
  public String getBusinessState() {
    return businessState;
  }
  public void setBusinessState(String businessState) {
    this.businessState = businessState;
  }
  public String getBusinessZip() {
    return businessZip;
  }
  public void setBusinessZip(String businessZip) {
    this.businessZip = businessZip;
  }
  public String getMerchantName() {
    return merchantName;
  }
  public void setMerchantName(String merchantName) {
    this.merchantName = merchantName;
  }
  public long getMerchantNumber() {
    return merchantNumber;
  }
  public void setMerchantNumber(long merchantNumber) {
    this.merchantNumber = merchantNumber;
  }
  public String getSicCode() {
    return sicCode;
  }
  public void setSicCode(String sicCode) {
    this.sicCode = sicCode;
  }
  public String getPspName() {
    return pspName;
  }
  public void setPspName(String pspName) {
    this.pspName = pspName;
  }
  public long getPspId() {
    return pspId;
  }
  public void setPspId(long pspId) {
    this.pspId = pspId;
  }
  public long getSalesCount() {
    return salesCount;
  }
  public void setSalesCount(long salesCount) {
    this.salesCount = salesCount;
  }
  public double getSalesAmount() {
    return salesAmount;
  }
  public void setSalesAmount(double salesAmount) {
    this.salesAmount = salesAmount;
  }
  public long getChargebackCount() {
    return chargebackCount;
  }
  public void setChargebackCount(long chargebackCount) {
    this.chargebackCount = chargebackCount;
  }
  public double getChargebackAmount() {
    return chargebackAmount;
  }
  public void setChargebackAmount(double chargebackAmount) {
    this.chargebackAmount = chargebackAmount;
  }
  

}

