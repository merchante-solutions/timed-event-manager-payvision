/*@lineinfo:filename=DiscoverActivityDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/DiscoverActivityDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 11/07/03 11:49a $
  Version            : $Revision: 9 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.ops.QueueConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class DiscoverActivityDataBean extends ReportSQLJBean
{
  public static final int         CT_MC             = 0;
  public static final int         CT_VISA           = 1;
  public static final int         CT_AMEX           = 2;
  public static final int         CT_DINERS         = 3;
  public static final int         CT_JCB            = 4;
  public static final int         CT_DEBIT          = 5;
  public static final int         CT_CHECK          = 6;
  public static final int         CT_COUNT          = 7;
  
  public static final int         TT_SALES          = 0;
  public static final int         TT_CREDITS        = 1;
  public static final int         TT_COUNT          = 2;
  
  public static final int         FT_APPLICATION    = 0;
  public static final int         FT_TERMINAL_SETUP = 1;
  public static final int         FT_STATEMENT      = 2;
  public static final int         FT_INACTIVE       = 3;
  public static final int         FT_ANNUAL         = 4;
  public static final int         FT_MIN_DISC       = 5;
  public static final int         FT_COUNT          = 6;      // must be last
  
  private static final String[][] AmountFieldNames = 
  {
    { "mc_sales_amount"     , "mc_credits_amount"       },    // CT_MC
    { "visa_sales_amount"   , "visa_credits_amount"     },    // CT_VISA
    { "amex_sales_amount"   , "amex_credits_amount"     },    // CT_AMEX
    { "diners_sales_amount" , "diners_credits_amount"   },    // CT_DINERS
    { "jcb_sales_amount"    , "jcb_credits_amount"      },    // CT_JCB
    { "debit_sales_amount"  , "debit_credits_amount"    },    // CT_DEBIT
    { null                  , null                      },    // CT_CHECK
  };
  
  private static final String[][] CountFieldNames = 
  {
    { "mc_sales_count"      , "mc_credits_count"        },    // CT_MC
    { "visa_sales_count"    , "visa_credits_count"      },    // CT_VISA
    { "amex_sales_count"    , "amex_credits_count"      },    // CT_AMEX
    { "diners_sales_count"  , "diners_credits_count"    },    // CT_DINERS
    { "jcb_sales_count"     , "jcb_credits_count"       },    // CT_JCB
    { "debit_sales_count"   , "debit_credits_count"     },    // CT_DEBIT
    { null                  , null                      },    // CT_CHECK
  };
  
  private static final String[] ContractExpFieldNames = 
  {
    "mc_contract_expense"    ,    // CT_MC
    "visa_contract_expense"  ,    // CT_VISA
    "amex_contract_expense" ,     // CT_AMEX
    "diners_contract_expense",    // CT_DINERS
    "jcb_contract_expense"   ,    // CT_JCB
    "debit_contract_expense" ,    // CT_DEBIT
    null                     ,    // CT_CHECK
  };
  
  private static final String[] DiscountFieldNames = 
  {
    "mc_discount",    // CT_MC
    "visa_discount",  // CT_VISA
    null,             // CT_AMEX
    null,             // CT_DINERS
    null,             // CT_JCB
    null,             // CT_DEBIT
    null              // CT_CHECK
  };
  
  private static final String[][] InterchangeFieldNames = 
  {
    { "mc_ic_income",   "mc_ic_expense"     },      // CT_MC
    { "visa_ic_income", "visa_ic_expense"   },      // CT_VISA
    { null,             null                },      // CT_AMEX
    { null,             null                },      // CT_DINERS
    { null,             null                },      // CT_JCB
    { null,             null                },      // CT_DEBIT
    { null,             null                },      // CT_CHECK
  };
  
  private static final String[] IncomeFieldNames = 
  {
    "mc_income",            // CT_MC
    "visa_income",          // CT_VISA
    "amex_income",          // CT_AMEX
    "diners_income",        // CT_DINERS
    "jcb_income",           // CT_JCB
    "debit_income",         // CT_DEBIT
    null                    // CT_CHECK
  };

  private static final String[][] ChargebackFieldNames =
  {
    { "mc_cb_count",    "mc_cb_amount"    },    // CT_MC
    { "visa_cb_count",  "visa_cb_amount"  },    // CT_VISA
    { null           ,  null              },    // CT_AMEX
    { null           ,  null              },    // CT_DINERS
    { null           ,  null              },    // CT_JCB
    { null           ,  null              },    // CT_DEBIT
    { null           ,  null              },    // CT_CHECK
  };
  
  public class RowData
    implements Comparable
  {
    public Date       ActiveDate          = null;
    public Date       ApprovalDate        = null;
    public Date       CancelDate          = null;
    public double[]   ChargebackAmount    = new double[CT_COUNT];
    public int[]      ChargebackCount     = new int[CT_COUNT];
    public double[]   ContractExpense     = new double[CT_COUNT];
    public String     Converted           = null;
    public double[]   DiscountIncome      = new double[CT_COUNT];
    public String     DbaName             = null;
    public long       DiscoverMerchantId  = 0L;
    public String     EmployeeId          = null;
    public double[]   FeeIncome           = new double[FT_COUNT];
    public long       HierarchyNode       = 0L;
    public double[]   IcExpense           = new double[CT_COUNT];
    public double[]   IcIncome            = new double[CT_COUNT];
    public double[]   ProcessingIncome    = new double[CT_COUNT];
    public double[][] VolumeAmount        = new double[CT_COUNT][TT_COUNT];
    public int[][]    VolumeCount         = new int[CT_COUNT][TT_COUNT];
    
    public RowData(  ResultSet resultSet )
      throws java.sql.SQLException
    {
      ActiveDate          = resultSet.getDate("active_date");
      ApprovalDate        = resultSet.getDate("approval_date");
      CancelDate          = resultSet.getDate("cancel_date");
      Converted           = resultSet.getString("converted");
      DiscoverMerchantId  = resultSet.getLong("discover_merchant_number");
      EmployeeId          = resultSet.getString("disc_user_id");
      HierarchyNode       = resultSet.getLong("merchant_number");
      DbaName             = resultSet.getString("dba_name");
      
      // extract the fees
      FeeIncome[FT_APPLICATION]     = resultSet.getDouble("fee_inc_application");
      FeeIncome[FT_TERMINAL_SETUP]  = resultSet.getDouble("fee_inc_terminal_setup");
      FeeIncome[FT_STATEMENT]       = resultSet.getDouble("fee_inc_statement");
      FeeIncome[FT_INACTIVE]        = resultSet.getDouble("fee_inc_inactive");
      FeeIncome[FT_ANNUAL]          = resultSet.getDouble("fee_inc_annual");
      FeeIncome[FT_MIN_DISC]        = resultSet.getDouble("fee_inc_min_discount");
      
      for( int i = 0; i < CT_COUNT; ++i )
      {
        // extract the sales amounts
        if ( AmountFieldNames[i][TT_SALES] != null )
        {
          VolumeAmount[i][TT_SALES]   = resultSet.getDouble(AmountFieldNames[i][TT_SALES] );
          VolumeAmount[i][TT_CREDITS] = resultSet.getDouble(AmountFieldNames[i][TT_CREDITS] );
        }
        else
        {
          VolumeAmount[i][TT_SALES]   = 0.0;
          VolumeAmount[i][TT_CREDITS] = 0.0;
        }
        
        // extract the sales counts
        if ( CountFieldNames[i][TT_SALES] != null )
        {
          VolumeCount[i][TT_SALES]    = resultSet.getInt(CountFieldNames[i][TT_SALES] );
          VolumeCount[i][TT_CREDITS]  = resultSet.getInt(CountFieldNames[i][TT_CREDITS] );
        }            
        else
        {
          VolumeCount[i][TT_SALES]    = 0;
          VolumeCount[i][TT_CREDITS]  = 0;
        }            

        // get the discount income        
        if ( DiscountFieldNames[i] != null )
        {
          DiscountIncome[i] = resultSet.getDouble(DiscountFieldNames[i]);
        }
        
        // extract the contract expenses
        if ( ContractExpFieldNames[i] != null )
        {
          ContractExpense[i] = resultSet.getDouble(ContractExpFieldNames[i]);
        }
        
        // extract the processing income
        if ( IncomeFieldNames[i] != null )
        {
          ProcessingIncome[i] = resultSet.getDouble(IncomeFieldNames[i]);
        }
        
        // extract the interchange income/expense
        if ( InterchangeFieldNames[i][0] != null )
        {
          IcIncome[i] = resultSet.getDouble(InterchangeFieldNames[i][0]);
        }
        if ( InterchangeFieldNames[i][1] != null )
        {
          IcExpense[i] = resultSet.getDouble(InterchangeFieldNames[i][1]);
        }
        
        // extract the chargeback counts and amounts
        if ( ChargebackFieldNames[i][0] != null )
        {
          ChargebackCount[i] = resultSet.getInt(ChargebackFieldNames[i][0]);
        }
        
        if ( ChargebackFieldNames[i][1] != null )
        {
          ChargebackAmount[i] = resultSet.getDouble(ChargebackFieldNames[i][1]);
        }
        
      }
    }
    
    public int compareTo( Object obj )
    {
      RowData         compareObj      = (RowData)obj;
      int             retVal          = 0;
      
      if ( (retVal = DbaName.compareTo(compareObj.DbaName)) == 0 )
      {
        retVal = (int)(HierarchyNode - compareObj.HierarchyNode);
      }
      
      return( retVal );
    }
    
    public double getCreditsAmount( int ct )
    {
      return( VolumeAmount[ct][TT_SALES] );
    }
    
    public int getCreditsCount( int ct )
    {
      return( VolumeCount[ct][TT_CREDITS] );
    }
    
    public double getNetAmount( int ct )
    {
      return( VolumeAmount[ct][TT_SALES] - VolumeAmount[ct][TT_CREDITS] );
    }
    
    public int getNetCount( int ct )
    {
      return( VolumeCount[ct][TT_SALES] + VolumeCount[ct][TT_CREDITS] );
    }
    
    public double getSalesAmount( int ct )
    {
      return( VolumeAmount[ct][TT_SALES] );
    }
    
    public int getSalesCount( int ct )
    {
      return( VolumeCount[ct][TT_SALES] );
    }
    
    public double getTotalSalesVolume()
    {
      double        retVal = 0.0;
      
      for( int i = 0; i < CT_COUNT; ++i )
      {
        retVal += (VolumeAmount[i][TT_SALES] - VolumeAmount[i][TT_CREDITS]);  
      }
      return( retVal );
    }
  }
  
  public class ReportTotals
  {
    public double[][]             VolumeTotalAmount     = new double[CT_COUNT][TT_COUNT];
    public int[][]                VolumeTotalCount      = new int[CT_COUNT][TT_COUNT];
    
    public ReportTotals( )
    {
    }
    
    public void add( RowData row )
    {
      for( int i = 0; i < CT_COUNT; ++i )
      {
        for( int j = 0; j < TT_COUNT; ++j )
        {
          VolumeTotalAmount[i][j] += row.VolumeAmount[i][j];
          VolumeTotalCount[i][j]  += row.VolumeCount[i][j];
        }                                
      }
    }
    
    public void clear()
    {
      for ( int i = 0; i < CT_COUNT; ++i )
      {
        for ( int j = 0; j < TT_COUNT; ++j )
        {
          VolumeTotalAmount[i][j] = 0.0;
          VolumeTotalCount[i][j]  = 0;
        }          
      }        
    }
  }
  
  private ReportTotals            HeaderTotals      = new ReportTotals();
  
  public DiscoverActivityDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Bankcard Activity Report\"\n");
    line.append("\"Acquirer Name -\", \"Merchant e-Solutions Inc\"\n");
    line.append("\"Reporting Month -\",\"");
    line.append( DateTimeFormatter.getFormattedDate( ReportDateBegin, "MMM yyyy" ) );
    if ( ! isSameMonthYear(ReportDateBegin,ReportDateEnd) )
    {
      line.append( " to " );
      line.append( DateTimeFormatter.getFormattedDate( ReportDateEnd, "MMM yyyy" ) );
    }
    line.append("\"\n\n\n");
    
    line.append("\"Discover Merchant #\",");
    line.append("\"DBA\",");
    line.append("\"Approval Date\",");
    line.append("\"Employee ID #\",");
    line.append("\"MC Net Sales Volume\",");
    line.append("\"Visa Net Sales Volume\",");
    line.append("\"Amex Net Sales Volume\",");
    line.append("\"Diners Net Sales Volume\",");
    line.append("\"JCB Net Sales Volume\",");
    line.append("\"Debit Net Sales Volume\",");
    line.append("\"Check Net Sales Volume\",");
    line.append("\"Total Net Sales Volume\"");
  }
  
  protected void encodeHeaderFixed( StringBuffer line )
  {
    line.setLength(0);
    line.append( encodeFixedField( 8,  ReportRows.size() + 1 ) );
    line.append( encodeFixedField( 20, HeaderTotals.VolumeTotalAmount[CT_MC][TT_SALES], 2 ) );
    line.append( encodeFixedField( 15, HeaderTotals.VolumeTotalCount[CT_MC][TT_SALES] ) );
    line.append( encodeFixedField( new java.sql.Date(Calendar.getInstance().getTime().getTime()),"yyyyMMdd" ) );
    line.append( encodeFixedField( ReportDateBegin,"yyyyMM" ) );
    line.append( encodeFixedField( 15, "MeS" ) );
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData      record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.DiscoverMerchantId );
    line.append( ",\"" );
    line.append( record.DbaName );
    line.append( "\"," );
    line.append( DateTimeFormatter.getFormattedDate(record.ApprovalDate,"MM/dd/yyyy") );
    line.append( ",\"" );
    line.append( record.EmployeeId );
    line.append( "\"," );
    for( int i = 0; i < CT_COUNT; ++i )
    {
      line.append( MesMath.toCurrencyCompressed(record.getNetAmount(i)) );
      line.append( "," );
    }
    line.append( MesMath.toCurrencyCompressed( record.getTotalSalesVolume() ) );
  }
  
  protected void encodeLineFixed( Object obj, StringBuffer line )
  {
    RowData       record            = (RowData)obj;
    double        sharedRevenue     = 0.0;
    
    line.setLength(0);
    line.append( encodeFixedField( 15, "MeS"                              ) );
    line.append( encodeFixedField(  1, record.Converted                   ) );          
    line.append( encodeFixedField( 15, Long.toString(record.DiscoverMerchantId)) );
    line.append( encodeFixedField( 40, record.DbaName                     ) );
    line.append( encodeFixedField( record.ApprovalDate, "yyyyMMdd"        ) );
    line.append( encodeFixedField( record.CancelDate,   "yyyyMMdd"        ) );
    line.append( encodeFixedField(  5, record.EmployeeId                  ) );
    line.append( encodeFixedField( 15, "" ) );  // merchant plan type (n/a)
    line.append( encodeFixedField(  2, "" ) );  // merchant code (n/a)
    
    // add the total sales amounts and counts
    for( int i = 0; i < CT_COUNT; ++i )
    {
      line.append( encodeFixedField( 17, record.getSalesAmount(i)        , 2  ) );
    }
    for( int i = 0; i < CT_COUNT; ++i )
    {
      line.append( encodeFixedField( 12, record.getSalesCount(i) ) );
    }      
    
    // add the chargeback amounts and counts (only through JCB)
    for( int i = 0; i <= CT_JCB; ++i )
    {
      line.append( encodeFixedField( 17, record.ChargebackAmount[i], 2 ) );
    }
    for( int i = 0; i <= CT_JCB; ++i )
    {
      line.append( encodeFixedField( 12, record.ChargebackCount[i] ) );
    }
    
    // add the total credits amounts and counts
    // add the total sales amounts and counts
    for( int i = 0; i < CT_COUNT; ++i )
    {
      line.append( encodeFixedField( 17, record.getCreditsAmount(i)        , 2  ) );
    }
    for( int i = 0; i < CT_COUNT; ++i )
    {
      line.append( encodeFixedField( 12, record.getCreditsCount(i) ) );
    }
    
    // add the revenue sharing data
    for( int i = 0; i < CT_COUNT; ++i )
    {
      sharedRevenue = ((record.DiscountIncome[i] + record.IcIncome[i] + record.ProcessingIncome[i]) -
                       (record.IcExpense[i] + record.ContractExpense[i]))/2;
      line.append( encodeFixedField( 17, sharedRevenue, 2 ) );                 
    }
    
    // add the discount income
    for( int i = 0; i < CT_COUNT; ++i )
    {
      line.append( encodeFixedField( 17, record.DiscountIncome[i], 2 ) );
    }
    
    // add the processing income
    for( int i = 0; i < CT_COUNT; ++i )
    {
      line.append( encodeFixedField( 17, record.ProcessingIncome[i], 2 ) );
    }
    
    // add the misc fees
    line.append( encodeFixedField( 4, record.FeeIncome[FT_APPLICATION] ) );
    line.append( encodeFixedField( 4, record.FeeIncome[FT_TERMINAL_SETUP] ) );
    line.append( encodeFixedField( 4, record.FeeIncome[FT_STATEMENT] ) );
    line.append( encodeFixedField( 4, record.FeeIncome[FT_INACTIVE] ) );
    line.append( encodeFixedField( 4, record.FeeIncome[FT_ANNUAL] ) );
    line.append( encodeFixedField( 4, record.FeeIncome[FT_MIN_DISC] ) );
    
    // add the interchange expense
    for( int i = 0; i < CT_COUNT; ++i )
    {
      line.append( encodeFixedField( 17, record.IcExpense[i], 2 ) );
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMMyyyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_bankcard_activity_");
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( isSameMonthYear(ReportDateBegin,ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
      case FF_FIXED:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    RowData                       row               = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      HeaderTotals.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:540^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mr.merch_number                         as merchant_number,
//                  decode(nvl(des.discover_merchant_number,0),
//                         0, nvl(mf.dmdsnum, mpo.merchpo_card_merch_number),    
//                         des.discover_merchant_number)    as discover_merchant_number,
//                  nvl(mf.dba_name,mr.merch_business_name) as dba_name,
//                  des.active_date                         as active_date,
//                  trunc(at.date_completed)                as approval_date,
//                  des.cancel_date                         as cancel_date,
//                  des.converted                           as converted,
//                  nvl(des.rep_id,nvl(mr.discover_rep_id, u.third_party_user_id))
//                                                          as disc_user_id,
//                  des.mc_sales_amount                     as mc_sales_amount,
//                  des.mc_sales_count                      as mc_sales_count,
//                  des.mc_credits_amount                   as mc_credits_amount,
//                  des.mc_credits_count                    as mc_credits_count,
//                  des.visa_sales_amount                   as visa_sales_amount,
//                  des.visa_sales_count                    as visa_sales_count,
//                  des.visa_credits_amount                 as visa_credits_amount,
//                  des.visa_credits_count                  as visa_credits_count,
//                  des.amex_sales_amount                   as amex_sales_amount,
//                  des.amex_sales_count                    as amex_sales_count,
//                  des.amex_credits_amount                 as amex_credits_amount,
//                  des.amex_credits_count                  as amex_credits_count,
//                  0                                       as disc_sales_amount,
//                  0                                       as disc_sales_count,
//                  0                                       as disc_credits_amount,
//                  0                                       as disc_credits_count,
//                  des.diners_sales_amount                 as diners_sales_amount,
//                  des.diners_sales_count                  as diners_sales_count,
//                  des.diners_credits_amount               as diners_credits_amount,
//                  des.diners_credits_count                as diners_credits_count,
//                  des.jcb_sales_amount                    as jcb_sales_amount,
//                  des.jcb_sales_count                     as jcb_sales_count,
//                  des.jcb_credits_amount                  as jcb_credits_amount,
//                  des.jcb_credits_count                   as jcb_credits_count,
//                  des.debit_sales_amount                  as debit_sales_amount,
//                  des.debit_sales_count                   as debit_sales_count,
//                  des.debit_credits_amount                as debit_credits_amount,
//                  des.debit_credits_count                 as debit_credits_count,
//                  des.visa_discount                       as visa_discount,
//                  des.visa_ic_income                      as visa_ic_income,
//                  des.visa_ic_expense                     as visa_ic_expense,
//                  des.visa_income                         as visa_income,
//                  des.visa_contract_expense               as visa_contract_expense,
//                  des.mc_discount                         as mc_discount,
//                  des.mc_ic_income                        as mc_ic_income,
//                  des.mc_ic_expense                       as mc_ic_expense,
//                  des.mc_income                           as mc_income,
//                  des.mc_contract_expense                 as mc_contract_expense,
//                  des.amex_income                         as amex_income,
//                  des.amex_contract_expense               as amex_contract_expense,
//                  des.diners_income                       as diners_income,
//                  des.diners_contract_expense             as diners_contract_expense,
//                  des.jcb_income                          as jcb_income,
//                  des.jcb_contract_expense                as jcb_contract_expense,
//                  des.debit_income                        as debit_income,
//                  des.debit_contract_expense              as debit_contract_expense,
//                  des.visa_cb_count                       as visa_cb_count,
//                  des.visa_cb_amount                      as visa_cb_amount,
//                  des.mc_cb_count                         as mc_cb_count,
//                  des.mc_cb_amount                        as mc_cb_amount,
//                  des.fee_inc_application                 as fee_inc_application,
//                  des.fee_inc_terminal_setup              as fee_inc_terminal_setup,
//                  des.fee_inc_statement                   as fee_inc_statement,
//                  des.fee_inc_inactive                    as fee_inc_inactive,
//                  des.fee_inc_annual                      as fee_inc_annual,
//                  des.fee_inc_min_discount                as fee_inc_min_discount
//          from  group_merchant              gm,
//                merchant                    mr,
//                application                 app,
//                app_tracking                at,
//                mif                         mf,
//                discover_extract_summary    des,
//                merchpayoption              mpo,
//                users                       u
//          where gm.org_num            = :orgId and
//                mr.merch_number       = gm.merchant_number and
//                app.app_seq_num       = mr.app_seq_num and
//                at.app_seq_num        = mr.app_seq_num and
//                at.dept_code          = :QueueConstants.DEPARTMENT_CREDIT and -- 100 and
//                u.user_id(+)          = app.app_user_id and
//                mf.merchant_number(+) = mr.merch_number and
//                des.merchant_number(+)= mf.merchant_number and
//                des.active_date(+)    = :beginDate and
//                ( 
//                  not des.active_date is null or
//                  (
//                    at.status_code = :QueueConstants.DEPT_STATUS_CREDIT_APPROVED and -- 102 and
//                    trunc( at.date_completed, 'month' ) = :beginDate
//                  )
//                ) and
//                mpo.app_seq_num(+)    = mr.app_seq_num and
//                mpo.cardtype_code(+)  = :mesConstants.APP_CT_DISCOVER -- 14 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mr.merch_number                         as merchant_number,\n                decode(nvl(des.discover_merchant_number,0),\n                       0, nvl(mf.dmdsnum, mpo.merchpo_card_merch_number),    \n                       des.discover_merchant_number)    as discover_merchant_number,\n                nvl(mf.dba_name,mr.merch_business_name) as dba_name,\n                des.active_date                         as active_date,\n                trunc(at.date_completed)                as approval_date,\n                des.cancel_date                         as cancel_date,\n                des.converted                           as converted,\n                nvl(des.rep_id,nvl(mr.discover_rep_id, u.third_party_user_id))\n                                                        as disc_user_id,\n                des.mc_sales_amount                     as mc_sales_amount,\n                des.mc_sales_count                      as mc_sales_count,\n                des.mc_credits_amount                   as mc_credits_amount,\n                des.mc_credits_count                    as mc_credits_count,\n                des.visa_sales_amount                   as visa_sales_amount,\n                des.visa_sales_count                    as visa_sales_count,\n                des.visa_credits_amount                 as visa_credits_amount,\n                des.visa_credits_count                  as visa_credits_count,\n                des.amex_sales_amount                   as amex_sales_amount,\n                des.amex_sales_count                    as amex_sales_count,\n                des.amex_credits_amount                 as amex_credits_amount,\n                des.amex_credits_count                  as amex_credits_count,\n                0                                       as disc_sales_amount,\n                0                                       as disc_sales_count,\n                0                                       as disc_credits_amount,\n                0                                       as disc_credits_count,\n                des.diners_sales_amount                 as diners_sales_amount,\n                des.diners_sales_count                  as diners_sales_count,\n                des.diners_credits_amount               as diners_credits_amount,\n                des.diners_credits_count                as diners_credits_count,\n                des.jcb_sales_amount                    as jcb_sales_amount,\n                des.jcb_sales_count                     as jcb_sales_count,\n                des.jcb_credits_amount                  as jcb_credits_amount,\n                des.jcb_credits_count                   as jcb_credits_count,\n                des.debit_sales_amount                  as debit_sales_amount,\n                des.debit_sales_count                   as debit_sales_count,\n                des.debit_credits_amount                as debit_credits_amount,\n                des.debit_credits_count                 as debit_credits_count,\n                des.visa_discount                       as visa_discount,\n                des.visa_ic_income                      as visa_ic_income,\n                des.visa_ic_expense                     as visa_ic_expense,\n                des.visa_income                         as visa_income,\n                des.visa_contract_expense               as visa_contract_expense,\n                des.mc_discount                         as mc_discount,\n                des.mc_ic_income                        as mc_ic_income,\n                des.mc_ic_expense                       as mc_ic_expense,\n                des.mc_income                           as mc_income,\n                des.mc_contract_expense                 as mc_contract_expense,\n                des.amex_income                         as amex_income,\n                des.amex_contract_expense               as amex_contract_expense,\n                des.diners_income                       as diners_income,\n                des.diners_contract_expense             as diners_contract_expense,\n                des.jcb_income                          as jcb_income,\n                des.jcb_contract_expense                as jcb_contract_expense,\n                des.debit_income                        as debit_income,\n                des.debit_contract_expense              as debit_contract_expense,\n                des.visa_cb_count                       as visa_cb_count,\n                des.visa_cb_amount                      as visa_cb_amount,\n                des.mc_cb_count                         as mc_cb_count,\n                des.mc_cb_amount                        as mc_cb_amount,\n                des.fee_inc_application                 as fee_inc_application,\n                des.fee_inc_terminal_setup              as fee_inc_terminal_setup,\n                des.fee_inc_statement                   as fee_inc_statement,\n                des.fee_inc_inactive                    as fee_inc_inactive,\n                des.fee_inc_annual                      as fee_inc_annual,\n                des.fee_inc_min_discount                as fee_inc_min_discount\n        from  group_merchant              gm,\n              merchant                    mr,\n              application                 app,\n              app_tracking                at,\n              mif                         mf,\n              discover_extract_summary    des,\n              merchpayoption              mpo,\n              users                       u\n        where gm.org_num            =  :1  and\n              mr.merch_number       = gm.merchant_number and\n              app.app_seq_num       = mr.app_seq_num and\n              at.app_seq_num        = mr.app_seq_num and\n              at.dept_code          =  :2  and -- 100 and\n              u.user_id(+)          = app.app_user_id and\n              mf.merchant_number(+) = mr.merch_number and\n              des.merchant_number(+)= mf.merchant_number and\n              des.active_date(+)    =  :3  and\n              ( \n                not des.active_date is null or\n                (\n                  at.status_code =  :4  and -- 102 and\n                  trunc( at.date_completed, 'month' ) =  :5 \n                )\n              ) and\n              mpo.app_seq_num(+)    = mr.app_seq_num and\n              mpo.cardtype_code(+)  =  :6  -- 14";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.DiscoverActivityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_CREDIT);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setInt(4,QueueConstants.DEPT_STATUS_CREDIT_APPROVED);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setInt(6,mesConstants.APP_CT_DISCOVER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.DiscoverActivityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:635^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        row = new RowData( resultSet );
        ReportRows.addElement(row);
        HeaderTotals.add(row);
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.MONTH, -1 );
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/