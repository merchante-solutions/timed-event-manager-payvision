/*@lineinfo:filename=AmexDiscSetupExceptionDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AmexDiscSetupExceptionDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class AmexDiscSetupExceptionDataBean extends ReportSQLJBean
{
  public class MifData
  {
    public String         AmexAccepted      = "N";      
    public long           AmexSENumber      = 0L;
    public String         DiscAccepted      = "N";      
    public long           DiscAcctNumber    = 0L;
    public long           HierarchyNode     = 0L;
    public Date           OpenDate          = null;
    public long           OrgId             = 0L;
    public String         OrgName           = null;
    
    public MifData( long           orgId,
                    long           node,
                    String         orgName,
                    Date           openDate,
                    String         amexAccepted,
                    long           amexSENumber,
                    String         discAccepted,
                    long           discAcctNumber )
    {
      HierarchyNode     = node;
      OrgId             = orgId;
      OrgName           = orgName;
      AmexAccepted      = amexAccepted;
      AmexSENumber      = amexSENumber;
      DiscAccepted      = discAccepted;
      DiscAcctNumber    = discAcctNumber;
      OpenDate          = openDate;
    }
  }
  
  public AmexDiscSetupExceptionDataBean( )
  {
  }
  
  public void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Date Opened\",");
    line.append("\"Amex Accepted\",");
    line.append("\"Amex SE #\",");
    line.append("\"Disc Accepted\",");
    line.append("\"Disc Acct #\",");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    MifData     record = (MifData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.HierarchyNode );
    line.append( ",\"" );
    line.append( record.OrgName );
    line.append( "\"," );
    line.append( DateTimeFormatter.getFormattedDate( record.OpenDate, "MM/dd/yyyy" ) );
    line.append( ",\"" );
    line.append( record.AmexAccepted );
    line.append( "\"," );
    line.append( record.AmexSENumber );
    line.append( ",\"" );
    line.append( record.DiscAccepted );
    line.append( "\"," );
    line.append( record.DiscAcctNumber );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMMyyyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_amex_disc_acct_nums_");
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( isSameMonthYear( ReportDateBegin, ReportDateEnd ) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          lastNode          = 0L;
    MifData                       rebateData        = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:159^7*/

//  ************************************************************
//  #sql [Ctx] it = { select om.org_num                                   as org_num,
//                 mf.merchant_number                           as merchant_number,
//                 mf.dba_name                                  as dba_name,
//                 to_date( decode( mf.date_opened, 
//                                  '000000', '010100', 
//                                  mf.date_opened),'mmddyy')   as date_opened,
//                 mf.damaccpt                                  as amex_accepted,                                
//                 mf.DAMEXSE                                   as amex_se,
//                 mf.ddsaccpt                                  as disc_accepted,               
//                 mf.DMDSNUM                                   as disc_acct
//          from   mif                mf,
//                 group_merchant     gm,
//                 group_rep_merchant grm,
//                 orgmerchant        om
//          where  gm.org_num = :orgId and
//                 grm.user_id(+) = :AppFilterUserId and
//                 grm.merchant_number(+) = gm.merchant_number and
//                 ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                 mf.merchant_number = gm.merchant_number and
//                 om.org_merchant_num = gm.merchant_number and
//                 ( ( mf.DDSACCPT = 'Y' and mf.dmdsnum is null ) or
//                   ( mf.DAMACCPT = 'Y' and mf.damexse is null ) ) and
//                 to_date( decode( mf.date_opened, 
//                                  '000000', '010100', 
//                                  mf.date_opened),'mmddyy') between :beginDate and :endDate
//          order by mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select om.org_num                                   as org_num,\n               mf.merchant_number                           as merchant_number,\n               mf.dba_name                                  as dba_name,\n               to_date( decode( mf.date_opened, \n                                '000000', '010100', \n                                mf.date_opened),'mmddyy')   as date_opened,\n               mf.damaccpt                                  as amex_accepted,                                \n               mf.DAMEXSE                                   as amex_se,\n               mf.ddsaccpt                                  as disc_accepted,               \n               mf.DMDSNUM                                   as disc_acct\n        from   mif                mf,\n               group_merchant     gm,\n               group_rep_merchant grm,\n               orgmerchant        om\n        where  gm.org_num =  :1  and\n               grm.user_id(+) =  :2  and\n               grm.merchant_number(+) = gm.merchant_number and\n               ( not grm.user_id is null or  :3  = -1 ) and        \n               mf.merchant_number = gm.merchant_number and\n               om.org_merchant_num = gm.merchant_number and\n               ( ( mf.DDSACCPT = 'Y' and mf.dmdsnum is null ) or\n                 ( mf.DAMACCPT = 'Y' and mf.damexse is null ) ) and\n               to_date( decode( mf.date_opened, \n                                '000000', '010100', \n                                mf.date_opened),'mmddyy') between  :4  and  :5 \n        order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AmexDiscSetupExceptionDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AmexDiscSetupExceptionDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:187^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        // add this record to the vector
        ReportRows.add( new MifData(  resultSet.getLong("org_num"),
                                      resultSet.getLong("merchant_number"),
                                      resultSet.getString("dba_name"),
                                      resultSet.getDate("date_opened"),
                                      resultSet.getString("amex_accepted"),
                                      resultSet.getLong("amex_se"),
                                      resultSet.getString("disc_accepted"),
                                      resultSet.getLong("disc_acct") ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/