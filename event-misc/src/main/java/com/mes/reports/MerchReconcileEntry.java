/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchReconcileEntry.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 10/26/00 3:47p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.util.Date;

public class MerchReconcileEntry
{
  protected     double[]                Amount;
  protected     int[]                   Count;
  protected     Date                    EntryDate;
  
  public static final int               INDEX_TRANS   = 0;
  public static final int               INDEX_ADJ     = 1;
  public static final int               INDEX_ACH     = 2;

  public MerchReconcileEntry( Date date )
  {
    EntryDate = date;

    Amount = new double[3];
    Count  = new int[3];
  }

  public double getAmount( int index )
  {
    double      retVal = 0.0;
    try
    {
      retVal = Amount[index];
    }
    catch( IndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }

  public double getBalanceAmount( )
  {
    return( Amount[INDEX_ACH] - ( Amount[INDEX_ADJ] + Amount[INDEX_TRANS] ) );
  }

  public int getCount( int index )
  { 
    int       retVal = 0;

    try
    {
      retVal = Count[index];
    }
    catch( IndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }

  public Date getEntryDate( )
  {
    return( EntryDate );
  }

  public void setAmount( int index, double newAmount )
  {
    try
    {
      Amount[index] = newAmount;
    }
    catch( IndexOutOfBoundsException e )
    {
    }
  }

  public void setCount( int index, int newCount )
  {
    try
    {
      Count[index] = newCount;
    }
    catch( IndexOutOfBoundsException e )
    {
    }
  }

  public void setEntryDate( Date newDate )
  {
    EntryDate = newDate;
  }
}