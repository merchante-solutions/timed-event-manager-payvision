/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ProfIcDataBean.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-04-23 08:36:57 -0700 (Thu, 23 Apr 2009) $
  Version            : $Revision: 16022 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;

public abstract class ProfIcDataBean extends ReportSQLJBean
{
  public static final int       IC_VISA                       = 0;
  public static final int       IC_MC                         = 1;
  public static final int       IC_DINERS                     = 2;
  public static final int       IC_CARD_TYPES                 = 3;
  
  public static final int       IC_CT_DESC_SHORT              = 0;
  public static final int       IC_CT_DESC_LONG               = 1;

  public static final int       IC_INCOME                     = 0;
  public static final int       IC_EXPENSE                    = 1;
  public static final int       IC_TYPE_COUNT                 = 2;
  
  public static final int       IC_VOL_SALES                  = 0;
  public static final int       IC_VOL_CREDITS                = 1;
  public static final int       IC_VOL_COUNT                  = 2;
  
  protected static final String IcCardTypeDesc[][] = // short & long
    { 
      { "VS", "Visa" },
      { "MC", "MasterCard" },
      { "DC", "Diners" } 
    };
    
  
  public class MerchantInterchangeData
  {
    private long                      AssocNumber     = 0L;
    private InterchangeData[]         IcData          = new InterchangeData[IC_CARD_TYPES];
    private long                      MerchantId      = 0L;
    private String                    MerchantName    = "";
    
    public MerchantInterchangeData( long assocId, long merchantId )
    {
      AssocNumber           = assocId;
      MerchantId            = merchantId;
      MerchantName          = getOrgName( hierarchyNodeToOrgId( merchantId ) );
      
      for ( int i = 0; i < IC_CARD_TYPES; ++i )
      {
        IcData[i] = null;
      }
    }
    
    public void addCardData( int cardType, InterchangeData icData )
    {
      IcData[cardType] = icData;
    }
    
    public long getAssocNumber( )
    {
      return( AssocNumber );
    }
    
    public boolean isCardDataValid( int cardType )
    {
      boolean       retVal = false;
      try
      {
        if ( IcData[cardType] != null )
        {
          retVal = true;
        }
      }
      catch( ArrayIndexOutOfBoundsException e )
      {
      }
      return( retVal );
    }
  
    public double getCardCreditsAmount( int cardType )
    {
      double      retVal = 0.0;
      try
      {
        retVal = IcData[cardType].getCreditsAmountTotal();
      }
      catch( ArrayIndexOutOfBoundsException e )
      {
        logEntry("getCardCreditsAmount()", e.toString());
      }
      return( retVal );
    }
  
    public int getCardCreditsCount( int cardType )
    {
      int      retVal = 0;
      try
      {
        retVal = IcData[cardType].getCreditsCountTotal();
      }
      catch( ArrayIndexOutOfBoundsException e )
      {
        logEntry("getCardCreditsCount()", e.toString());
      }
      return( retVal );
    }
    
    public InterchangeData getCardData( int cardType )
    {
      return( IcData[cardType] );
    }
  
    public InterchangeData[] getCardData( )
    {
      return( IcData );
    }
  
    public double getCardExpense( int cardType )
    {
      double      retVal = 0.0;
      try
      {
        retVal = IcData[cardType].getExpenseTotal();
      }
      catch( Exception e )
      {
        logEntry("getCardCreditsAmount()", e.toString());
      }
      return( retVal );
    }
  
    public double getCardIncome( int cardType )
    {
      double      retVal = 0.0;
      try
      {
        retVal = IcData[cardType].getIncomeTotal();
      }
      catch( Exception e )
      {
        logEntry("getCardCreditsAmount()", e.toString());
      }
      return( retVal );
    }
  
    public double getCardSalesAmount( int cardType )
    {
      double      retVal = 0.0;
      try
      {
        retVal = IcData[cardType].getSalesAmountTotal();
      }
      catch( Exception e )
      {
        logEntry("getCardSalesAmount()", e.toString());
      }
      return( retVal );
    }
  
    public int getCardSalesCount( int cardType )
    {
      int      retVal = 0;
      try
      {
        retVal = IcData[cardType].getSalesCountTotal();
      }
      catch( Exception e )
      {
        logEntry("getCardSalesCount()", e.toString());
      }
      return( retVal );
    }
    
    public long getMerchantId( )
    {
      return( MerchantId );
    }
    
    public String getMerchantName( )
    {
      return( MerchantName );
    }
  }
  
  public class InterchangeData
  {
    private double        AssessmentIncome      = 0.0;
    private double        AssessmentExpense     = 0.0;
    private double        BulletinIncome        = 0.0;
    private double        BulletinExpense       = 0.0;
    private String[]      CategoryDesc          = null;
    private int           IcCardType            = IC_CARD_TYPES;
    private double[][]    InterchangeIncExp     = null;
    private double[][]    InterchangeVolAmount  = null;
    private int   [][]    InterchangeVolCount   = null;
    private double        IntraChangeIncome     = 0.0;
    private double        IntraChangeExpense    = 0.0;
  
    public InterchangeData( int cardType, int catCount )
    {
      CategoryDesc          = new String[catCount];
      IcCardType            = cardType;
      InterchangeIncExp     = new double[catCount][IC_TYPE_COUNT];
      InterchangeVolAmount  = new double[catCount][IC_VOL_COUNT];
      InterchangeVolCount   = new int   [catCount][IC_VOL_COUNT];
    
      // initialize the data structure
      for ( int cat = 0; cat < catCount; ++cat )
      {
        CategoryDesc[cat] = "Undefined";
        for( int i = 0; i < IC_TYPE_COUNT; ++i )
        {
          InterchangeIncExp[cat][i] = 0.0;
        }
        for( int i = 0; i < IC_VOL_COUNT; ++i )
        {
          InterchangeVolAmount[cat][i]  = 0.0;
          InterchangeVolCount[cat][i]   = 0;
        }
      }
    }
  
    public double getAssessmentExpense( )
    {
      return( AssessmentExpense );
    }
  
    public double getAssessmentIncome( )
    {
      return( AssessmentIncome );
    }
  
    public double getBulletinExpense( )
    {
      return( BulletinExpense );
    }
  
    public double getBulletinIncome( )
    {
      return( BulletinIncome );
    }
    
    public String getCardTypeDesc( )
    {
      String      retVal = "Unknown";
      
      try
      {
        retVal = IcCardTypeDesc[ IcCardType ][ IC_CT_DESC_LONG ];
      }
      catch( ArrayIndexOutOfBoundsException e )
      {
      }
      return( retVal );
    }
    
    public String getCardTypeDescShort( )
    {
      String      retVal = "UNK";
      
      try
      {
        retVal = IcCardTypeDesc[ IcCardType ][ IC_CT_DESC_SHORT ];
      }
      catch( ArrayIndexOutOfBoundsException e )
      {
      }
      return( retVal );
    }
  
    public int getCategoryCount( )
    {
      return( InterchangeIncExp.length );
    }
    
    public double[] getCategoryVolAmount( int cat )
    {
      return( InterchangeVolAmount[cat] );
    }
    
    public int[] getCategoryVolCount( int cat )
    {
      return( InterchangeVolCount[cat] );
    }
  
    public double getCategoryCreditsAmount( int cat )
    {
      double      retVal = 0.0;
      
      try
      {
        retVal = InterchangeVolAmount[cat][IC_VOL_CREDITS];
      }
      catch( Exception e )
      {
      }
      return( retVal );
    }
    
    public int getCategoryCreditsCount( int cat ) 
    {
      int           retVal = 0;
      try
      {
        retVal = InterchangeVolCount[cat][IC_VOL_CREDITS];
      }
      catch( Exception e )
      {
      }
      return( retVal );
    }
    
    public String getCategoryDesc( int cat )
    {
      return( CategoryDesc[cat] );
    }
    
    public double getCategoryExpense( int cat )
    {
      return( InterchangeIncExp[cat][IC_EXPENSE] );
    }
    
    public double getCategoryIncome( int cat )
    {
      return( InterchangeIncExp[cat][IC_INCOME] );
    }
    
    public double[] getCategoryIncomeExpense( int cat )
    {
      return( InterchangeIncExp[cat] );
    }
    
    public double getCategorySalesAmount( int cat )
    {
      double      retVal = 0.0;
      
      try
      {
        retVal = InterchangeVolAmount[cat][IC_VOL_SALES];
      }
      catch( Exception e )
      {
      }
      return( retVal );
    }
    
    public int getCategorySalesCount( int cat ) 
    {
      int           retVal = 0;
      try
      {
        retVal = InterchangeVolCount[cat][IC_VOL_SALES];
      }
      catch( Exception e )
      {
      }
      return( retVal );
    }
    
    public double getCreditsAmountTotal( )
    {
      double retVal = 0;
      
      for ( int cat = 0; cat < InterchangeVolAmount.length; ++cat )
      {
        retVal += InterchangeVolAmount[cat][IC_VOL_CREDITS];
      }
      return( retVal );
    }
    
    public int getCreditsCountTotal( )
    {
      int retVal = 0;
      
      for ( int cat = 0; cat < InterchangeVolCount.length; ++cat )
      {
        retVal += InterchangeVolCount[cat][IC_VOL_CREDITS];
      }
      return( retVal );
    }
    
    public String getDescription( )
    {
      StringBuffer    retVal = new StringBuffer( getCardTypeDesc() );
      retVal.append( " Interchange" );
      return( retVal.toString() );
    }
  
    public double getExpenseTotal( )
    {
      double        retVal    = 0.0;
      
      for ( int cat = 0; cat < InterchangeIncExp.length; ++cat )
      {
        retVal += InterchangeIncExp[cat][IC_EXPENSE];
      }
      retVal += ( getAssessmentExpense() + getBulletinExpense() + getIntraChangeExpense() );
      
      return( retVal );
    }
    
    public double getIncomeTotal( )
    {
      double        retVal    = 0.0;
      
      for ( int cat = 0; cat < InterchangeIncExp.length; ++cat )
      {
        retVal += InterchangeIncExp[cat][IC_INCOME];
      }
      retVal += ( getAssessmentIncome() + getBulletinIncome() + getIntraChangeIncome() );
      
      return( retVal );
    }
    
    public double getIntraChangeExpense( )
    {
      return( IntraChangeExpense );
    }
  
    public double getIntraChangeIncome( )
    {
      return( IntraChangeIncome );
    }
    
    public double getSalesAmountTotal( )
    {
      double retVal = 0;
      
      for ( int cat = 0; cat < InterchangeVolAmount.length; ++cat )
      {
        retVal += InterchangeVolAmount[cat][IC_VOL_SALES];
      }
      return( retVal );
    }
    
    public int getSalesCountTotal( )
    {
      int retVal = 0;
      
      for ( int cat = 0; cat < InterchangeVolCount.length; ++cat )
      {
        retVal += InterchangeVolCount[cat][IC_VOL_SALES];
      }
      return( retVal );
    }
    
    public boolean isEmpty( )
    {
      double      retVal = 0.0;
    
      try
      {
        retVal  = AssessmentIncome + 
                  AssessmentExpense +
                  BulletinIncome +
                  BulletinExpense +
                  IntraChangeIncome +
                  IntraChangeExpense;
                
        for ( int cat = 0; cat < InterchangeIncExp.length; ++cat )
        {                  
          for ( int i = 0; i < IC_TYPE_COUNT; ++i )
          {
            retVal += InterchangeIncExp[cat][i];
          }
        }
      }
      catch( ArrayIndexOutOfBoundsException e )
      {
      }
    
      return( retVal == 0.0 );
    }
  
    public boolean isCategoryEmpty( int cat )
    {
      double      retVal = 0.0;
    
      try
      {
        for ( int i = 0; i < IC_TYPE_COUNT; ++i )
        {
          retVal += InterchangeIncExp[cat][i];
        }
      }
      catch( ArrayIndexOutOfBoundsException e )
      {
      }
    
      return( retVal == 0.0 );
    }
  
    protected void setCategoryData( int cat, String catName,
                                    int salesCount, double salesAmount,
                                    int creditCount, double creditAmount,
                                    double income,   double expense )
    {
      CategoryDesc[cat]                           = catName;
      InterchangeIncExp[cat][IC_INCOME]           = income;
      InterchangeIncExp[cat][IC_EXPENSE]          = expense;
      
      InterchangeVolAmount[cat][IC_VOL_SALES]     = salesAmount;
      InterchangeVolCount [cat][IC_VOL_SALES]     = salesCount;
      InterchangeVolAmount[cat][IC_VOL_CREDITS]   = creditAmount;
      InterchangeVolCount [cat][IC_VOL_CREDITS]   = creditCount;
    }                    
  
    protected void setFixedData( double incAssessment, double expAssessment,
                                 double incBulletin,   double expBulletin,
                                 double incIntraChange,double expIntraChange )
    {
      AssessmentIncome    = incAssessment;
      AssessmentExpense   = expAssessment;
      BulletinIncome      = incBulletin;
      BulletinExpense     = expBulletin;
      IntraChangeIncome   = incIntraChange;
      IntraChangeExpense  = expIntraChange;
    }                                 
  
    public void showData( java.io.PrintStream out )
    {
      double[]        catData = null;
      out.println();
      out.println("************************************************");
      out.println("*  " + getDescription() );
      if ( isEmpty() )
      {
        out.println("*  Empty" );
      }
      out.println("************************************************");
      out.println("AssessmentIncome   : " + AssessmentIncome    );
      out.println("AssessmentExpense  : " + AssessmentExpense   );
      out.println("BulletinIncome     : " + BulletinIncome      );
      out.println("BulletinExpense    : " + BulletinExpense     );
      out.println("IntraChangeIncome  : " + IntraChangeIncome   );
      out.println("IntraChangeExpense : " + IntraChangeExpense  );
      for( int i = 0; i < InterchangeIncExp.length; ++i )
      {
        catData = InterchangeIncExp[i];
        out.println( CategoryDesc[i] + ( isCategoryEmpty(i) ? " (Empty)" : "" ) + " : " );
        out.println( "  Income  : " + catData[IC_INCOME] );
        out.println( "  Expense : " + catData[IC_EXPENSE] );
      }
    }
  }

 /***************************************************************************
  *  ProfIcDataBean Members
  ***************************************************************************/  
  private int                       MerchantIcDataIndex = 0;
  
  private static final String       IcCardDescriptions[] =
  {
    "Visa",
    "MasterCard",
    "Diners"
  };
  
  public ProfIcDataBean( )
  {
    initialize();
  }
  
  protected void addMerchantIcData( long assocId, long merchantId, int cardType, InterchangeData icData )
  {
    int                           i;
    MerchantInterchangeData       merchData = null;
    
    for ( i = 0; i < ReportRows.size(); ++i )
    {
      merchData = (MerchantInterchangeData)ReportRows.elementAt(i);
      if ( merchData.getMerchantId() == merchantId )
      {
        break;
      }
    }
    // merchant does not exist, so create and add
    if ( i >= ReportRows.size() )
    {
      merchData = new MerchantInterchangeData( assocId, merchantId );
      ReportRows.add( merchData );
    }
    merchData.addCardData( cardType, icData );
  }
  
  public MerchantInterchangeData first( )
  {
    MerchantIcDataIndex = 0;
    return( next() );
  }
  
  public String getCardDescription( int cardType )
  {
    String    retVal = "Invalid Card Type Index";
    try
    {
      retVal = IcCardDescriptions[cardType];
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      logEntry("getCardDescription(" + cardType + ")", e.toString());
    }
    return( retVal );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    int                     fromMonth = 0;
    int                     fromYear  = 0;
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMMyyyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_ic_details_");
    
    // setup to compare the month
    // and year from the two dates
    cal.setTime( ReportDateBegin );
    fromMonth = cal.get(Calendar.MONTH);
    fromYear  = cal.get(Calendar.YEAR);
    cal.setTime( ReportDateEnd );
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if( ( cal.get( Calendar.MONTH ) != fromMonth ) ||
        ( cal.get( Calendar.YEAR  ) != fromYear ) )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean       retVal    = false;
    
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
        
      default:
        break;
    }
    return( retVal );
  }
  
  public abstract void loadCashAdvanceData( long orgId, Date beginDate, Date endDate );

  public void loadData( boolean maskCashAdvanceIc )
  {
    loadData( getReportOrgId(), getReportDateBegin(), getReportDateEnd(), maskCashAdvanceIc );
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    loadData( orgId, beginDate, endDate, true );
  }
  
  public abstract void loadData( long orgId, Date beginDate, Date endDate, boolean maskCashAdvanceIc );
  
  public double loadSummaryExpense( long orgId, Date beginDate, Date endDate )
  {
    return( loadSummaryExpense( orgId, beginDate, endDate, false ) );
  }
  
  public abstract double loadSummaryExpense( long orgId, Date beginDate, Date endDate, boolean maskCashAdvanceIc );
  public abstract double loadSummaryIncome( long orgId, Date beginDate, Date endDate, boolean maskCashAdvanceIc );
  
  public MerchantInterchangeData next( )
  {
    MerchantInterchangeData       merchData = null;
    
    try
    {
      merchData = (MerchantInterchangeData)ReportRows.elementAt( MerchantIcDataIndex++ );
    }
    catch( Exception e )
    {
    }
    return( merchData );
  }
  
  public void setProperties(HttpServletRequest request)
  {
    Calendar      cal = Calendar.getInstance();
    
    super.setProperties(request);
      
    if ( usingDefaultReportDates() )
    {
      if ( cal.get( Calendar.MONTH ) == Calendar.JANUARY )
      {
        cal.set( Calendar.MONTH, cal.DECEMBER );
        cal.set( Calendar.YEAR , ( cal.get( Calendar.YEAR ) - 1 ) );
      }
      else
      {
        cal.set( Calendar.MONTH, ( cal.get( Calendar.MONTH ) - 1 ) );
      }
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      ReportDateBegin = new java.sql.Date( cal.getTime().getTime() );
      ReportDateEnd   = new java.sql.Date( cal.getTime().getTime() );
    }
  }
}  


