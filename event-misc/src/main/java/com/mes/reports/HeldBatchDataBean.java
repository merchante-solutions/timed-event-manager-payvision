/*@lineinfo:filename=HeldBatchDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/HeldBatchDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: $
  Last Modified Date : $Date: $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2000-2005,2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class HeldBatchDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public  Date            BatchDate         = null;
    public  long            BatchNumber       = 0L;
    public  String          CardType          = null;
    public  String          DbaName           = null;
    public  double          HeldAmount        = 0.0;
    public  int             HeldCount         = 0;
    public  long            MerchantId        = 0L;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      BatchDate   = resultSet.getDate  ("batch_date");
      BatchNumber = resultSet.getLong("batch_number");
      CardType    = processString(resultSet.getString("card_type"));
      DbaName     = processString(resultSet.getString("dba_name"));
      HeldAmount  = resultSet.getDouble("held_amount");
      HeldCount   = resultSet.getInt("held_count");
      MerchantId  = resultSet.getLong("merchant_number");
    }
  }
  
  public HeldBatchDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Merchant Number\",");
    line.append("\"DBA Name\",");
    line.append("\"Card Type\",");
    line.append("\"Batch Date\",");
    line.append("\"Batch Number\",");
    line.append("\"Held Count\",");
    line.append("\"Held Amount\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);
    
    RowData   record = (RowData)obj;
      
    line.append(record.MerchantId);
    line.append(",\"");
    line.append(record.DbaName);
    line.append("\",\"");
    line.append(record.CardType);
    line.append("\",");
    line.append(DateTimeFormatter.getFormattedDate(record.BatchDate,"MM/dd/yyyy"));
    line.append(",\"");
    line.append(record.BatchNumber);
    line.append("\",");
    line.append(record.HeldCount);
    line.append(",");
    line.append(record.HeldAmount);
  }
  
  public String getDownloadFilenameBase()
  {
    return ( "held_batch_summary" );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
        
      default:
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();

      for( int i = 0; i < 2; ++i )
      {      
        switch(i)
        {
          case 0:
            /*@lineinfo:generated-code*//*@lineinfo:144^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  hld.merchant_number         as merchant_number,
//                        mf.dba_name                 as dba_name,
//                        'DS'                        as card_type,
//                        hld.batch_date              as batch_date,
//                        hld.batch_number            as batch_number,
//                        count( hld.merchant_number )as held_count,
//                        sum( hld.transaction_amount *
//                             decode( hld.tran_type,
//                                     'R',-1,1) )    as held_amount
//                from    discover_settlement_hold  hld,
//                        mif                       mf
//                where   mf.merchant_number(+) = hld.merchant_number        
//                group by  hld.merchant_number,
//                          mf.dba_name, 
//                          hld.batch_date, 
//                          hld.batch_number              
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  hld.merchant_number         as merchant_number,\n                      mf.dba_name                 as dba_name,\n                      'DS'                        as card_type,\n                      hld.batch_date              as batch_date,\n                      hld.batch_number            as batch_number,\n                      count( hld.merchant_number )as held_count,\n                      sum( hld.transaction_amount *\n                           decode( hld.tran_type,\n                                   'R',-1,1) )    as held_amount\n              from    discover_settlement_hold  hld,\n                      mif                       mf\n              where   mf.merchant_number(+) = hld.merchant_number        \n              group by  hld.merchant_number,\n                        mf.dba_name, \n                        hld.batch_date, \n                        hld.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.HeldBatchDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.HeldBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:162^13*/
            break;
            
          case 1:
            /*@lineinfo:generated-code*//*@lineinfo:166^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  hld.merchant_number         as merchant_number,
//                        mf.dba_name                 as dba_name,
//                        'AM'                        as card_type,
//                        hld.batch_date              as batch_date,
//                        hld.batch_number            as batch_number,
//                        count( hld.merchant_number )as held_count,
//                        sum( hld.transaction_amount *
//                             decode( hld.debit_credit_indicator,
//                                     'C',-1,1) )    as held_amount
//                from    amex_settlement_hold    hld,
//                        mif                     mf
//                where   mf.merchant_number(+) = hld.merchant_number        
//                group by  hld.merchant_number,
//                          mf.dba_name, 
//                          hld.batch_date, 
//                          hld.batch_number            
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  hld.merchant_number         as merchant_number,\n                      mf.dba_name                 as dba_name,\n                      'AM'                        as card_type,\n                      hld.batch_date              as batch_date,\n                      hld.batch_number            as batch_number,\n                      count( hld.merchant_number )as held_count,\n                      sum( hld.transaction_amount *\n                           decode( hld.debit_credit_indicator,\n                                   'C',-1,1) )    as held_amount\n              from    amex_settlement_hold    hld,\n                      mif                     mf\n              where   mf.merchant_number(+) = hld.merchant_number        \n              group by  hld.merchant_number,\n                        mf.dba_name, \n                        hld.batch_date, \n                        hld.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.HeldBatchDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.HeldBatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:184^13*/
            break;
            
          default:
            it = null;
            break;
        }
        
        if ( it != null )
        {
          resultSet = it.getResultSet();
          while( resultSet.next() )
          {
            ReportRows.addElement( new RowData( resultSet ) );
          }
          resultSet.close();
          it.close();
        }          
      }        
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/