/*@lineinfo:filename=ChargebackWatchDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ChargebackWatchDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class ChargebackWatchDataBean extends ReportSQLJBean
{
  public class RowData implements Comparable
  {
    public String     CardNumber          = null;
    public Date       CreditBatchDate     = null;
    public long       CreditBatchNumber   = 0L;
    public long       CreditDtId          = 0L;
    public Date       CreditDate          = null;
    public long       HierarchyNode       = 0L;
    public Date       IncomingDate        = null;
    public long       OrgId               = 0L;
    public String     ReferenceNumber     = null;
    public double     TranAmount          = 0.0;
    public Date       TranDate            = null;
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      IncomingDate      = resultSet.getDate("incoming_date");
      CardNumber        = resultSet.getString("card_number");
      OrgId             = resultSet.getLong("org_num");
      ReferenceNumber   = resultSet.getString("ref_num");
      TranAmount        = resultSet.getDouble("tran_amount");
      TranDate          = resultSet.getDate("tran_date");
      CreditDate        = resultSet.getDate("credit_date");
      CreditBatchDate   = resultSet.getDate("credit_batch_date");
      CreditBatchNumber = resultSet.getLong("credit_batch_number");
      CreditDtId        = resultSet.getLong("credit_ddf_dt_id");
    }
    
    public int compareTo( Object obj )
    {
      RowData       data        = (RowData)obj;
      int           retVal      = 0;
      
      if ( ( retVal = (int)(HierarchyNode - data.HierarchyNode) ) == 0 )
      {
        if ( ( retVal = IncomingDate.compareTo(data.IncomingDate) ) == 0 )
        {
          retVal = CardNumber.compareTo(data.CardNumber);
        }
      }
      return( retVal );
    }
  }

  public ChargebackWatchDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Merchant Id\",");
    line.append("\"Incoming Date\",");
    line.append("\"Card Number\",");
    line.append("\"Reference Number\",");
    line.append("\"Tran Date\",");
    line.append("\"Tran Amount\",");
    line.append("\"Credit Batch Date\",");
    line.append("\"Credit Tran Date\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData record = (RowData) obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.HierarchyNode );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.IncomingDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( record.CardNumber );
    line.append( "," );
    line.append( record.ReferenceNumber );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.TranDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( record.TranAmount );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.CreditBatchDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.CreditDate,"MM/dd/yyyy") );
    
  }
  
  public Date getCreditDate( long merchantId, Date tranDate, Date incomingDate, String cardNumber )
  {
    Date            retVal = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:146^7*/

//  ************************************************************
//  #sql [Ctx] { select  dt.transaction_date     
//          from    daily_detail_file_dt dt
//          where   dt.merchant_account_number = :merchantId and
//                  dt.batch_date between :tranDate and :incomingDate and
//                  dt.debit_credit_indicator = 'C' and        
//                  dt.cardholder_account_number = :cardNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  dt.transaction_date      \n        from    daily_detail_file_dt dt\n        where   dt.merchant_account_number =  :1  and\n                dt.batch_date between  :2  and  :3  and\n                dt.debit_credit_indicator = 'C' and        \n                dt.cardholder_account_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ChargebackWatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,tranDate);
   __sJT_st.setDate(3,incomingDate);
   __sJT_st.setString(4,cardNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:154^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    finally
    {
    }
    return( retVal );        
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMMyyyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append("chargeback_watch_list_");
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          lastLoadSec       = -1L;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:206^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                    index (cr idx_dt_credits_merch_date) 
//                  */
//                  o.org_num                 as org_num,
//                  cb.cb_load_sec            as mes_ref_num,
//                  cb.merchant_number        as hierarchy_node,
//                  cb.incoming_date          as incoming_date,
//                  cb.card_number            as card_number,
//                  cb.reference_number       as ref_num,
//                  cb.tran_amount            as tran_amount,
//                  cb.tran_date              as tran_date,
//                  cr.tran_date              as credit_date,
//                  cr.batch_date             as credit_batch_date,
//                  cr.batch_number           as credit_batch_number,
//                  cr.ddf_dt_id              as credit_ddf_dt_id 
//          from    group_merchant              gm,
//                  group_rep_merchant          grm,
//                  network_chargeback_watch    ncw,
//                  network_chargebacks         cb,
//                  daily_detail_file_credits   cr,
//                  organization                o
//          where   gm.org_num = :orgId and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  ncw.merchant_number = gm.merchant_number and
//                  cb.merchant_number  = ncw.merchant_number and
//                  cb.incoming_date between :beginDate and :endDate and
//                  o.org_group = cb.merchant_number and
//                  cr.merchant_number(+) = cb.merchant_number and
//                  cr.batch_date(+) > cb.tran_date and
//                  cr.tran_amount(+) = cb.tran_amount and                
//                  cr.card_number(+) = cb.card_number 
//          order by cb.cb_load_sec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                  index (cr idx_dt_credits_merch_date) \n                */\n                o.org_num                 as org_num,\n                cb.cb_load_sec            as mes_ref_num,\n                cb.merchant_number        as hierarchy_node,\n                cb.incoming_date          as incoming_date,\n                cb.card_number            as card_number,\n                cb.reference_number       as ref_num,\n                cb.tran_amount            as tran_amount,\n                cb.tran_date              as tran_date,\n                cr.tran_date              as credit_date,\n                cr.batch_date             as credit_batch_date,\n                cr.batch_number           as credit_batch_number,\n                cr.ddf_dt_id              as credit_ddf_dt_id \n        from    group_merchant              gm,\n                group_rep_merchant          grm,\n                network_chargeback_watch    ncw,\n                network_chargebacks         cb,\n                daily_detail_file_credits   cr,\n                organization                o\n        where   gm.org_num =  :1  and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and        \n                ncw.merchant_number = gm.merchant_number and\n                cb.merchant_number  = ncw.merchant_number and\n                cb.incoming_date between  :4  and  :5  and\n                o.org_group = cb.merchant_number and\n                cr.merchant_number(+) = cb.merchant_number and\n                cr.batch_date(+) > cb.tran_date and\n                cr.tran_amount(+) = cb.tran_amount and                \n                cr.card_number(+) = cb.card_number \n        order by cb.cb_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ChargebackWatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.ChargebackWatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:242^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        if ( lastLoadSec == resultSet.getLong("mes_ref_num") )
        {
          // skip any duplicates.  these occur when more
          // than one credit matches the chargeback
          continue;
        }
        ReportRows.addElement( new RowData(resultSet) );
        lastLoadSec = resultSet.getLong("mes_ref_num");
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties( HttpServletRequest request )
  {
    super.setProperties(request);
    
    if ( usingDefaultReportDates() )
    {
      Calendar          cal = Calendar.getInstance();
      
      cal.setTime(ReportDateBegin);
      cal.add(Calendar.DAY_OF_MONTH,-1);
      
      ReportDateBegin = new java.sql.Date( cal.getTime().getTime() );
      ReportDateEnd   = new java.sql.Date( cal.getTime().getTime() );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/