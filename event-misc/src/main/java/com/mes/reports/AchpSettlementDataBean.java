package com.mes.reports;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class AchpSettlementDataBean extends AchpOrgSummaryDataBean
{
  static Logger log = Logger.getLogger(AchpSettlementDataBean.class);

  public AchpSettlementDataBean()
  {
    super(true);   // enable field bean support
  }

  /**
   * FIELD BEAN
   */

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    createStandardAchpFields(request);
  }

  /**
   * REPORT DATA LOADING
   */

  public class DetailRow
  {
    public long       entryId;
    public java.util.Date 
                      entryDate;
    public String     merchNum;
    public String     profileId;
    public long       tranId;
    public long       ftId;
    public String     entryType;
    public String     entryDesc;
    public long       summaryId;
    public BigDecimal amount;
    public String     testFlag;
    public String     companyName;
    public long       batchId;
    public int        batchNum;
    public java.util.Date
                      settleDate;
    public String     accountNum;
    public String     refNum;
    public String     custName;
    public String     custId;
  }

  private DetailRow createDetailRow(ResultSet rs) throws Exception
  {
    DetailRow row   = new DetailRow();
    row.entryId     = rs.getLong      ("entry_id");
    row.entryDate   = toDate(rs.getTimestamp
                                      ("entry_ts"));
    row.merchNum    = rs.getString    ("merch_num");
    row.profileId   = rs.getString    ("profile_id");
    row.tranId      = rs.getLong      ("tran_id");
    row.ftId        = rs.getLong      ("ft_id");
    row.entryType   = rs.getString    ("entry_type");
    row.entryDesc   = rs.getString    ("short_description");
    row.summaryId   = rs.getLong      ("summary_id");
    row.amount      = rs.getBigDecimal("amount").setScale(2);
    row.testFlag    = rs.getString    ("test_flag");
    row.companyName = rs.getString    ("company_name");
    row.batchId     = rs.getLong      ("batch_id");
    row.batchNum    = rs.getInt       ("batch_num");
    row.settleDate  = toDate(rs.getTimestamp
                                      ("cutoff_ts"));
    row.accountNum  = rs.getString    ("account_num");
    row.refNum      = rs.getString    ("ref_num");
    row.custName    = rs.getString    ("cust_name");
    row.custId      = rs.getString    ("cust_id");

    // adjust sign of negative entry types
    if ("CREDIT".equals(row.entryType) || 
        "SALE_RET_ADJ".equals(row.entryType))
    {
      row.amount = row.amount.multiply(new BigDecimal("-1"));
    }

    return row;
  }

  /**
   * Load settlement funding entry data from achp_sf_entries to display
   * in achp_settlement_detail.jsp.  Data is loaded into DetailRow objects
   * in ReportSQLJBean.ReportRows.
   */
  public void loadDetailData(long orgId, Date beginDate, Date endDate)
  {
    PreparedStatement ps = null;

    try
    {
      String profileId = getData("profileId");
      String pCls = 
        profileId.length() > 0 ? 
          " and p.profile_id = '" + profileId + "' " : "";

      ps = con.prepareStatement(
        " select                                        " +
        "  e.entry_id,                                  " +
        "  e.entry_ts,                                  " +
        "  e.merch_num,                                 " +
        "  e.profile_id,                                " +
        "  e.tran_id,                                   " +
        "  e.ft_id,                                     " +
        "  et.entry_type,                               " +
        "  et.short_description,                        " +
        "  e.summary_id,                                " +
        "  e.amount,                                    " +
        "  e.test_flag,                                 " +
        "  p.company_name,                              " +
        "  b.batch_id,                                  " +
        "  b.batch_num,                                 " +
        "  s.cutoff_ts,                                 " +
        "  t.account_num,                               " +
        "  t.ref_num,                                   " +
        "  t.cust_name,                                 " +
        "  t.cust_id                                    " +
        " from                                          " +
        "  achp_sf_entries      e,                      " +
        "  achp_sf_entry_types  et,                     " +
        "  achp_sf_summaries    s,                      " +
        "  achp_profiles        p,                      " +
        "  achp_transactions    t,                      " +
        "  achp_tran_batches    b,                      " +
        "  group_merchant       g                       " +
        " where                                         " +
        "  g.org_num = ?                                " +
        "  and g.merchant_number = e.merch_num          " +
        "  and e.entry_type = et.entry_type             " +
        "  and e.profile_id = p.profile_id              " +
        "  and e.tran_id = t.tran_id                    " +
        "  and t.batch_id = b.batch_id                  " +
        "  and e.summary_id = s.summary_id              " +
        "  and trunc(s.cutoff_ts) between ? and ?       " +
        " " + pCls +
        "  and t.test_flag = 'N'                        " +
        " order by                                      " +
        "  e.profile_id, e.summary_id, e.entry_ts       ");
      ps.setLong(1,orgId);
      ps.setDate(2,beginDate);
      ps.setDate(3,endDate);

      ResultSet rs = ps.executeQuery();
      while (rs.next())
      {
        ReportRows.add(createDetailRow(rs));
      }
    }
    catch (Exception e)
    {
      log.error(""+e);
      e.printStackTrace();
      logEntry(buildMethodName("loadDetailData",orgId,beginDate,endDate),""+e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
    }
  }

  public class SummaryRow
  {
    public String     merchNum;
    public String     profileId;
    public String     companyName;
    public int        saleCount;
    public BigDecimal saleAmount;
    public int        creditCount;
    public BigDecimal creditAmount;
    public int        batchCount;   // net sales and credits
    public BigDecimal batchAmount;
    public int        adjustCount;
    public BigDecimal adjustAmount;
    public BigDecimal netAmount;
  }

  public SummaryRow createSummaryRow(ResultSet rs) throws Exception
  {
    SummaryRow row    = new SummaryRow();
    row.merchNum      = rs.getString("merch_num");
    row.profileId     = rs.getString("profile_id");
    row.companyName   = rs.getString("company_name");
    row.saleCount     = rs.getInt("sale_count");
    row.saleAmount    = rs.getBigDecimal("sale_amount");
    row.creditCount   = rs.getInt("credit_count");
    row.creditAmount  = rs.getBigDecimal("credit_amount");
    row.batchCount    = row.saleCount + row.creditCount;
    row.batchAmount   = row.saleAmount.add(row.creditAmount);
    row.adjustCount   = rs.getInt("adjust_count");
    row.adjustAmount  = rs.getBigDecimal("adjust_amount");
    row.netAmount     = 
      row.saleAmount.add(row.creditAmount).add(row.adjustAmount);
    return row;
  }

  /**
   * Load settlement funding summary data from achp_sf_summaries by profile
   * to display in achp_settlement_summary.jsp.
   */
  private void loadMerchSummaryData(long orgId, Date beginDate, Date endDate)
  {
    PreparedStatement ps = null;

    try
    {
      //log.debug("orgId=" + orgId);
      ps = con.prepareStatement(
        " select                                              " +
        "  p.merch_num,                                       " +
        "  p.profile_id,                                      " +
        "  p.company_name,                                    " +
        "  sum(nvl(s.sale_count,0))           sale_count,     " +
        "  sum(nvl(s.sale_total,0))           sale_amount,    " +
        "  sum(nvl(s.credit_count,0))         credit_count,   " +
        "  sum(nvl(s.credit_total * -1,0))    credit_amount,  " +
        "  sum(nvl(s.sale_ret_adj_count,0) +                  " +
        "      nvl(s.credit_ret_adj_count,0)) adjust_count,   " +
        "  sum(nvl(s.credit_ret_adj_total,0) -                " +
        "      nvl(s.sale_ret_adj_total,0))   adjust_amount   " +
        " from                                                " +
        "  group_merchant       g,                            " +
        "  achp_sf_summaries    s,                            " +
        "  achp_profiles        p                             " +
        " where                                               " +
        "  g.org_num = ?                                      " +
        "  and g.merchant_number = p.merch_num                " +
        "  and p.profile_id = s.profile_id(+)                 " +
        "  and trunc(s.cutoff_ts(+)) between ? and ?          " +
        "  and s.test_flag(+) = 'N'                           " +
        " group by p.merch_num, p.profile_id, p.company_name  " +
        " order by p.profile_id                               ");
      ps.setLong(1,orgId);
      ps.setDate(2,beginDate);
      ps.setDate(3,endDate);

      ResultSet rs = ps.executeQuery();
      while (rs.next())
      {
        ReportRows.add(createSummaryRow(rs));
      }
    }
    catch (Exception e)
    {
      log.error(""+e);
      e.printStackTrace();
      logEntry(
        buildMethodName("loadMerchSummaryData",orgId,beginDate,endDate),""+e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
    }
  }

  /**
   * Load settlement funding summary data from achp_sf_summaries by merchant
   * to display in org_summary.jsp.
   */
  private void loadOrgSummaryData(long orgId, Date beginDate, Date endDate)
  {
    PreparedStatement ps = null;

    try
    {
      //log.debug("orgId=" + orgId);
      ps = con.prepareStatement(
        " select                                                " +
        "  o.org_num                            org_num,        " +
        "  o.org_group                          hierarchy_node, " +
        "  o.org_name                           org_name,       " +
        "  0                                    district,       " +
        "  sum(nvl(s.sale_count,0) +                            " +
        "      nvl(s.credit_count,0))           item_count,     " +
        "  sum(nvl(s.sale_total,0) -                            " +
        "      nvl(s.credit_total,0))           item_amount,    " +
        "  sum(nvl(s.sale_ret_adj_count,0) +                    " +      
        "      nvl(s.credit_ret_adj_count,0))   adj_count,      " +
        "  sum(nvl(s.credit_ret_adj_total,0) -                  " +
        "      nvl(s.sale_ret_adj_total,0))     adj_amount,     " +
        "  nvl(sum(s.net_total),0)              total           " +
        " from                                                  " +
        "  parent_org         p,                                " +
        "  organization       o,                                " +
        "  group_merchant     g,                                " +
        "  achp_sf_summaries  s                                 " +
        " where                                                 " +
        "  p.parent_org_num = ?                                 " +
        "  and o.org_num = p.org_num                            " +
        "  and g.org_num = o.org_num                            " +
        "  and g.merchant_number = s.merch_num(+)               " +
        "  and trunc(s.cutoff_ts(+)) between ? and ?            " +
        "  and s.test_flag(+) = 'N'                             " +
        " group by o.org_num, o.org_group, o.org_name           " +
        " order by o.org_num                                    ");
      ps.setLong(1,orgId);
      ps.setDate(2,beginDate);
      ps.setDate(3,endDate);
      ResultSet rs = ps.executeQuery();
      processSummaryData(rs);
    }
    catch (Exception e)
    {
      log.error(""+e);
      e.printStackTrace();
      logEntry(
        buildMethodName("loadOrgSummaryData",orgId,beginDate,endDate),""+e);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
    }
  }

  /**
   * Load summary data.  Determines if viewing at organization (org_summary.jsp)
   * or merchant summary level (achp_settlement_summary.jsp) and loads data
   * accordingly.
   */
  public void loadSummaryData(long orgId, Date beginDate, Date endDate)
  {
    if (isNodeMerchant())
    {
      loadMerchSummaryData(orgId,beginDate,endDate);
    }
    else
    {
      loadOrgSummaryData(orgId,beginDate,endDate);
    }
  }

  /**
   * CSV DOWNLOAD
   */

  public String getBeanSpecificFilenameFragment()
  {
    return "achp_settled";
  }

  /**
   * Merchant detail row
   */
  private void encodeDetailLineCSV(Object lineObj, StringBuffer line)
  {
    DetailRow row = (DetailRow)lineObj;
    encodeCSV(row.tranId,line);
    line.append(",");
    encodeCSV(row.merchNum,line);
    line.append(",");
    encodeCSV(row.profileId,line);
    line.append(",");
    encodeCSV(row.companyName,line);
    line.append(",");
    encodeCSV(row.entryDate,line);
    line.append(",");
    encodeCSV(row.entryDesc,line);
    line.append(",");
    encodeCSV(row.accountNum,line,false);
    line.append(",");
    encodeCSV(row.refNum,line);
    line.append(",");
    encodeCSV(row.custName,line);
    line.append(",");
    encodeCSV(row.custId,line);
    line.append(",");
    encodeCSV(""+row.amount,line,false);
  }

  /**
   * Merchant detail row
   */
  private void encodeDetailHeaderCSV(StringBuffer line)
  {
    line.append("\"Transaction ID\",");
    line.append("\"Merchant Number\",");
    line.append("\"Profile ID\",");
    line.append("\"Company Name\",");
    line.append("\"Date\",");
    line.append("\"Type\",");
    line.append("\"Account Number\",");
    line.append("\"Reference Number\",");
    line.append("\"Customer Name\",");
    line.append("\"Customer ID\",");
    line.append("\"Amount\"");
  }

  /**
   * Merchant summary header
   */
  private void encodeSummaryLineCSV(Object lineObj, StringBuffer line)
  {
    SummaryRow row = (SummaryRow)lineObj;
    encodeCSV(row.merchNum,line);
    line.append(",");
    encodeCSV(row.profileId,line);
    line.append(",");
    encodeCSV(row.companyName,line);
    line.append(",");
    encodeCSV(row.saleCount,line);
    line.append(",");
    encodeCSV(""+row.saleAmount,line);
    line.append(",");
    encodeCSV(row.creditCount,line);
    line.append(",");
    encodeCSV(""+row.creditAmount,line);
    line.append(",");
    encodeCSV(row.batchCount,line);
    line.append(",");
    encodeCSV(""+row.batchAmount,line);
    line.append(",");
    encodeCSV(row.adjustCount,line);
    line.append(",");
    encodeCSV(""+row.adjustAmount,line);
    line.append(",");
    encodeCSV(""+row.netAmount,line);
  }

  /**
   * Merchant summary row
   */
  private void encodeSummaryHeaderCSV(StringBuffer line)
  {
    line.append("\"Merchant Number\",");
    line.append("\"Profile Id\",");
    line.append("\"Company Name\",");
    line.append("\"Sale Cnt\",");
    line.append("\"Sale Amt\",");
    line.append("\"Credit Cnt\",");
    line.append("\"Credit Amt\",");
    line.append("\"Batch Cnt\",");
    line.append("\"Batch Amt\",");
    line.append("\"Adjustment Cnt\",");
    line.append("\"Adjustment Amt\",");
    line.append("\"Net Amt\"");
  }

  /**
   * Org summary header
   */
  private void encodeOrgSummaryHeaderCSV(StringBuffer line)
  {
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Transaction Cnt\",");
    line.append("\"Transaction Amt\",");
    line.append("\"Adjustment Cnt\",");
    line.append("\"Adjustment Amt\",");
    line.append("\"Total\"");
  }

  /**
   * Org summary row
   */
  private void encodeOrgSummaryLineCSV(Object lineObj, StringBuffer line)
  {
    MesOrgSummaryEntry row = (MesOrgSummaryEntry)lineObj;
    encodeCSV(encodeHierarchyNode(row.getHierarchyNode()),line,false);
    line.append(",");
    encodeCSV(row.getOrgName(),line);
    line.append(",");
    encodeCSV(row.getCount(),line);
    line.append(",");
    encodeCSV(row.getAmount(),line);
    line.append(",");
    encodeCSV(row.getCount(MesOrgSummaryEntry.ADJ_INDEX),line);
    line.append(",");
    encodeCSV(row.getAmount(MesOrgSummaryEntry.ADJ_INDEX),line);
    line.append(",");
    encodeCSV(row.getTotalAmount(),line);
  }

  /**
   * Generate download file header.
   *
   * Supports three download flavors, org_summary style, merchant summary 
   * and merchant detail.
   */
  protected void encodeHeaderCSV(StringBuffer line)
  {
    line.setLength(0);

    // merchant detail
    if (getReportType() == RT_DETAILS)
    {
      encodeDetailHeaderCSV(line);
    }
    else
    {
      // merchant summary
      if ((getReportMerchantId() != 0L))
      {
        encodeSummaryHeaderCSV(line);
      }
      // org summary
      else  
      {
        encodeOrgSummaryHeaderCSV(line);
      }
    }
  }

  /**
   * Generate download file row.
   */
  protected void encodeLineCSV(Object lineObj, StringBuffer line)
  {
    line.setLength(0);

    // merchant detail
    if (getReportType() == RT_DETAILS)
    {
      encodeDetailLineCSV(lineObj,line);
    }
    else
    {
      // merchant summary
      if ((getReportMerchantId() != 0L))
      {
        encodeSummaryLineCSV(lineObj,line);
      }
      // org summary
      else  
      {
        encodeOrgSummaryLineCSV(lineObj,line);
      }
    }
  }
}