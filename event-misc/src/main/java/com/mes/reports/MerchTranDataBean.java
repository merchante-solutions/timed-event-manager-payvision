/*@lineinfo:filename=MerchTranDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchTranDataBean.sqlj $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-01-15 15:06:44 -0800 (Tue, 15 Jan 2013) $
  Version            : $Revision: 20841 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.CurrencyCodeConverter;
import sqlj.runtime.ResultSetIterator;

public class MerchTranDataBean extends OrgSummaryDataBean
{
  public class DetailRow
  {
    public String     AuthCode                        = null;
    public String     AcqInvoice                      = "";
    public Date       BatchDate                       = null;
    public long       BatchNumber                     = 0L;
    public String     CardNumber                      = null;
    public String     CardNumberEnc                   = null;
    public String     CardType                        = null;
    public String     ClientRefNum                    = null;
    public String     CurrencyCode                    = null;
    public String     CurrencyCodeAlpha               = null;
    public double     DiscountAmount                  = 0.0;
    public long       DowngradeRecId                  = 0L;
    public String     EntryMode                       = null;
    public String     FilePrefix                      = null;
    public long       HierarchyNode                   = 0L;
    public long       MerchantBatchNumber             = 0;
    public double     OriginalAmount                  = 0.0;
    public String     OriginalCurrencyCode            = null;
    public String     OriginalCurrencyCodeAlpha       = null;
    public String     OrgName                         = null;
    public String     PassengerName                   = null;
    public String     PosDisplayId                    = null;
    public String     PosTerminalId                   = null;
    public String     PurchaseId                      = null;
    public String     ReferenceNumber                 = null;
    public String     TerminalNumber                  = "1";
    public String     TicketNumber                    = "";
    public double     TranAmount                      = 0.0;
    public String     TranCode                        = "";
    public Date       TranDate                        = null;
    public String     TridentTranId                   = null;
    public long       RecId                           = -1L;

    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AuthCode              = processString(resultSet.getString("auth_code"));
      BatchDate             = resultSet.getDate("batch_date");
      BatchNumber           = resultSet.getLong("batch_number");
      CardNumber            = processString(resultSet.getString("card_number"));
      CardNumberEnc         = resultSet.getString("card_number_enc");
      CardType              = processString(resultSet.getString("card_type"));
      ClientRefNum          = processString(resultSet.getString("client_ref_num"));
      EntryMode             = processString(resultSet.getString("pos_entry_mode"));
      FilePrefix            = processString(resultSet.getString("file_prefix"));
      HierarchyNode         = resultSet.getLong("hierarchy_node");
      MerchantBatchNumber   = resultSet.getLong("merchant_batch_number");
      OrgName               = processString(resultSet.getString("org_name"));
      PassengerName         = processString(resultSet.getString("passenger_name"));
      PosTerminalId         = processString(resultSet.getString("pos_tid"));
      PurchaseId            = processString(resultSet.getString("purchase_id"));
      ReferenceNumber       = processString(resultSet.getString("ref_num"));
      TerminalNumber        = processString(resultSet.getString("terminal_number"));
      TranAmount            = resultSet.getDouble("tran_amount");
      TranDate              = resultSet.getDate("tran_date");
      TridentTranId         = resultSet.getString("trident_tran_id");
      
      // result set might not contain rec_id so enclose in try/catch
      try{ RecId = resultSet.getLong("rec_id"); } catch(Exception e) {}

      // data was invalid before 2-Nov-2005
      if ( BatchDate.before( getBatchDataValidDate() ) )
      {
        MerchantBatchNumber = -1L;
      }

      try
      {
        DowngradeRecId = resultSet.getLong("downgrade_rec_id");
      }
      catch( Exception e )
      {
        // ignore
      }

      // optional fields
      try
      {
        AcqInvoice    = processString(resultSet.getString("acq_invoice"));
        TicketNumber  = processString(resultSet.getString("ticket_number"));
        TranCode      = processString(resultSet.getString("tran_code"));
      }
      catch( Exception e )
      {
        // ignore
      }

      // resolve the display terminal information
      PosDisplayId = "";
      try
      {
        if ( !PosTerminalId.equals("") )
        {
          if ( FilePrefix.equals("256m") )
          {
            /*@lineinfo:generated-code*//*@lineinfo:150^13*/

//  ************************************************************
//  #sql [Ctx] { select  tp.terminal_id 
//                from    trident_profile   tp
//                where   tp.catid = :PosTerminalId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  tp.terminal_id  \n              from    trident_profile   tp\n              where   tp.catid =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,PosTerminalId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   PosDisplayId = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:155^13*/
          }
          else if ( FilePrefix.equals("256d") )
          {
            /*@lineinfo:generated-code*//*@lineinfo:159^13*/

//  ************************************************************
//  #sql [Ctx] { select  ( mms.merch_number ||
//                          mms.store_number ||
//                          mms.terminal_number ) 
//                from    mms_stage_info    mms
//                where   mms.vnum = ('V' || substr(:PosTerminalId,2))
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ( mms.merch_number ||\n                        mms.store_number ||\n                        mms.terminal_number )  \n              from    mms_stage_info    mms\n              where   mms.vnum = ('V' || substr( :1 ,2))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,PosTerminalId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   PosDisplayId = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:166^13*/
          }
          else if ( FilePrefix.equals("256s") )
          {
            PosDisplayId = PosTerminalId;
          }
        }
      }
      catch( Exception e )
      {
        // ignore
      }
      
      // dcc support
      try
      {
        CurrencyCode              = resultSet.getString("currency_code");
        OriginalAmount            = resultSet.getDouble("original_amount");
        OriginalCurrencyCode      = resultSet.getString("original_currency_code");
        
        CurrencyCodeConverter ccc = CurrencyCodeConverter.getInstance();
        CurrencyCodeAlpha         = ccc.numericCodeToAlphaCode(CurrencyCode);
        OriginalCurrencyCodeAlpha = ccc.numericCodeToAlphaCode(OriginalCurrencyCode);
      }
      catch( Exception e )
      {
        // ignore
      }
    }

    public long getDowngradeRecId()
    {
      return( DowngradeRecId );
    }
    
    public boolean hasAssocDowngrade()
    {
      return( DowngradeRecId != 0L );
    }
    
    public boolean isMultiCurrency()
    {
      boolean     retVal      = false;
      
      if ( CurrencyCode != null && OriginalCurrencyCode != null )
      {
        retVal = !CurrencyCode.equals(OriginalCurrencyCode);
      }
      return( retVal );
    }
  }

  private static Date BatchDataValidDate    = null;   // 2-NOV-2005
  long                BatchNumber           = 0;
  TransactionFilter   Filter                = new TransactionFilter();
  boolean             FindAllAllowed        = false;
  long                FindCardNumber        = 0L;
  String              FindCardNumberString  = null;
  boolean             FindDataValid         = false;
  boolean             FindTransaction       = false;
  String              FindValue             = null;
  boolean             MultiCurrency         = false;
  boolean             ShowBatchSummary      = false;
  boolean             ShowCardSummary       = false;
  protected boolean   IncludeTridentTranId  = false;

  public MerchTranDataBean( )
  {
  }

  public static Date getBatchDataValidDate()
  {
    if ( BatchDataValidDate == null )
    {
      Calendar        cal   = Calendar.getInstance();

      // 2-November-2005
      cal.clear();    // clear all fields to reset time
      cal.set( Calendar.DAY_OF_MONTH, 2 );
      cal.set( Calendar.MONTH, Calendar.NOVEMBER );
      cal.set( Calendar.YEAR, 2005 );


      BatchDataValidDate = new java.sql.Date( cal.getTime().getTime() );
    }
    return( BatchDataValidDate );
  }

  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);

    if ( getReportType() == RT_SUMMARY )
    {
      if ( showingBatchSummary() == true )
      {
        line.append("\"Merchant Id\",");
        line.append("\"DBA Name\",");
        line.append("\"Batch Date\",");

        for( int i = 0; i < MesTransactionSummary.CARD_TYPE_COUNT; ++i )
        {
          line.append("\"");
          line.append(MesTransactionSummary.getCardTypeString(i));
          line.append("\",");
        }
        line.append("\"Total\"");
      }
      else
      {
        line.append("\"Org Id\",");
        line.append("\"Org Name\",");
        if ( (getReportMerchantId() != 0L) || ShowCardSummary )
        {
          line.append("\"Card Type\",");
          line.append("\"Sales Cnt\",");
          line.append("\"Sales Amt\",");
          line.append("\"Credits Cnt\",");
          line.append("\"Credits Amt\",");
          line.append("\"Net Amt\"");
        }
        else  // is a merchant level summary
        {
          line.append("\"Bank Cnt\",");
          line.append("\"Bank Amt\",");
          line.append("\"Other Cnt\",");
          line.append("\"Other Amt\",");
          line.append("\"Total\"");
        }
      }
    }
    else  // RT_DETAILS
    {
      if ( isSabreAccount() )
      {
        line.append("\"PCC\",");
        line.append("\"Merchant Number\",");
        line.append("\"DBA Name\",");
        line.append("\"Card Number\",");
        line.append("\"Tran Code\",");
        line.append("\"Tran Date\",");
        line.append("\"Settle Date\",");
        line.append("\"Approval\",");
        line.append("\"Reference Number\",");
        line.append("\"Ticket Number\",");
        line.append("\"Purchase ID\",");
        line.append("\"Doc Locator\",");
        line.append("\"Passenger Name\",");
        line.append("\"Tran Amount\"");
      }
      else
      {
        line.append("\"Merchant Id\",");
        line.append("\"DBA Name\",");
        line.append("\"Term Num\",");
        line.append("\"Batch Num\",");
        line.append("\"Batch Date\",");
        line.append("\"Tran Date\",");
        line.append("\"Card Type\",");
        line.append("\"Card Number\",");
        line.append("\"Reference\",");
        line.append("\"Purchase Id\",");
        line.append("\"Auth Code\",");
        line.append("\"Entry Mode\",");
        line.append("\"Tran Amount\"");
      }

      if ( hasMultiCurrency() )
      {
        line.append(",");
        line.append("\"Currency Code\",");
        line.append("\"Original Amount\",");
        line.append("\"Original Currency Code\",");
      }
      
      if ( IncludeTridentTranId )
      {
        line.append(",");
        line.append("\"Trident Tran Id\"");
        line.append(",");
        line.append("\"Client Ref Num\"");
      }
    }
  }

  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    long          merchantId        = 0L;

    // clear the line buffer and add
    // the merchant specific data.
    line.setLength(0);
    if ( getReportType() == RT_SUMMARY )
    {
      if ( showingBatchSummary() == true )
      {
        double                      amount      = 0.0;
        double                      total       = 0.0;
        MesTransactionSummary       tranSummary = (MesTransactionSummary)obj;

        line.append( encodeHierarchyNode(tranSummary.getMerchantId()) );
        line.append( ",\"" );
        line.append( tranSummary.getDbaName() );
        line.append( "\"," );
        line.append( DateTimeFormatter.getFormattedDate( tranSummary.getBatchDate(), "MM/dd/yyyy" ) );
        line.append( "," );

        for( int i = 0; i < MesTransactionSummary.CARD_TYPE_COUNT; ++i )
        {
          amount = tranSummary.getTotalAmount(i) +
                   tranSummary.getIssuerTotalAmount(i);
          line.append( amount );
          line.append( "," );
          total += amount;
        }
        line.append( total );
      }
      else
      {
        double    creditsAmount      = 0.0;
        int       creditsCount       = 0;
        double    salesAmount        = 0.0;
        int       salesCount         = 0;

        if ( ( (merchantId = getReportMerchantId()) != 0L ) || ShowCardSummary )
        {
          MesTransactionSummary  tranSummary    = (MesTransactionSummary)obj;

          for ( int i = 0; i < MesTransactionSummary.CARD_TYPE_COUNT; ++i )
          {
            salesCount    = tranSummary.getPurchaseCount(i) +
                            tranSummary.getIssuerPurchaseCount(i);
            salesAmount   = tranSummary.getPurchaseAmount(i) +
                            tranSummary.getIssuerPurchaseAmount(i);
            creditsCount  = tranSummary.getCreditCount(i) +
                            tranSummary.getIssuerCreditCount(i);
            creditsAmount = tranSummary.getCreditAmount(i) +
                            tranSummary.getIssuerCreditAmount(i);

            // only display private label if there is activity
            if ( i == MesTransactionSummary.CARD_TYPE_PL )
            {
              if ( ( salesCount + creditsCount ) == 0 )
              {
                continue;
              }
            }

            if (i > 0)
            {
              line.append("\n");
            }
            if ( merchantId == 0L )
            {
              line.append( encodeHierarchyNode(getReportHierarchyNode()) );
            }
            else
            {
              line.append( encodeHierarchyNode(merchantId) );
            }
            line.append( ",\"" );
            line.append( getReportOrgName() );
            line.append( "\"," );
            line.append( tranSummary.getCardTypeString(i) );
            line.append( "," );
            line.append( salesCount );
            line.append( "," );
            line.append( salesAmount );
            line.append( "," );
            line.append( creditsCount );
            line.append( "," );
            line.append( creditsAmount );
            line.append( "," );
            line.append( salesAmount + creditsAmount ); // credits amount is a negative number
          }
        }
        else
        {
          MesOrgSummaryEntry  record    = (MesOrgSummaryEntry)obj;

          line.append( encodeHierarchyNode(record.getHierarchyNode()) );
          line.append( ",\"" );
          line.append( record.getOrgName() );
          line.append( "\"," );
          line.append( record.getCount(MesOrgSummaryEntry.BANK_INDEX) );
          line.append( "," );
          line.append( record.getAmount(MesOrgSummaryEntry.BANK_INDEX) );
          line.append( "," );
          line.append( record.getCount(MesOrgSummaryEntry.NONBANK_INDEX) );
          line.append( "," );
          line.append( record.getAmount(MesOrgSummaryEntry.NONBANK_INDEX) );
          line.append( "," );
          line.append( record.getTotalAmount() );
        }
      }
    }
    else    // RT_DETAILS
    {
      DetailRow       row = (DetailRow) obj;

      if ( isSabreAccount() )
      {
        line.append( "\"" );
        line.append( getPcc(row.HierarchyNode) );
        line.append( "\"," );
        line.append( encodeHierarchyNode(row.HierarchyNode) );
        line.append( ",\"" );
        line.append( row.OrgName );
        line.append( "\"," );
        line.append( row.CardNumber );
        line.append( "," );
        line.append( row.TranCode );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate(row.TranDate,"MM/dd/yyyy") );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate(row.BatchDate,"MM/dd/yyyy") );
        line.append( "," );
        line.append( row.AuthCode );
        line.append( "," );
        line.append( row.ReferenceNumber );
        line.append( "," );
        line.append( row.TicketNumber );
        line.append( "," );
        line.append( row.PurchaseId );
        line.append( "," );
        line.append( row.AcqInvoice );
        line.append( ",\"" );
        line.append( row.PassengerName );
        line.append( "\"," );
        line.append( row.TranAmount );
      }
      else
      {
        line.append( encodeHierarchyNode(row.HierarchyNode) );
        line.append( ",\"" );
        line.append( row.OrgName );
        line.append( "\",\"" );
        line.append( row.TerminalNumber );
        line.append( "\"," );
        line.append( row.MerchantBatchNumber );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate(row.BatchDate,"MM/dd/yyyy") );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate(row.TranDate,"MM/dd/yyyy") );
        line.append( ",\"" );
        line.append( row.CardType );
        line.append( "\",\"" );
        line.append( row.CardNumber );
        line.append( "\",\"'" );
        line.append( row.ReferenceNumber );
        line.append( "\",\"" );
        line.append( row.PurchaseId );
        line.append( "\",\"" );
        line.append( row.AuthCode );
        line.append( "\",\"" );
        line.append( row.EntryMode );
        line.append( "\"," );
        line.append( row.TranAmount );
        
        if ( hasMultiCurrency() )
        {
          line.append( ",\"" );
          line.append( row.CurrencyCode );
          line.append( "\"," );
          line.append( row.OriginalAmount );
          line.append( ",\"" );
          line.append( row.OriginalCurrencyCode );
          line.append( "\"" );
        }
        if ( IncludeTridentTranId )
        {
          line.append( "," );
          line.append( row.TridentTranId );
          line.append( "," );
          line.append( row.ClientRefNum );
        }
      }
    }
  }

  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");

    filename.append( getReportHierarchyNode() );
    filename.append("_tran_summary_");

    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }
    return ( filename.toString() );
  }

  public TransactionFilter getFilter( )
  {
    return( Filter );
  }

  public long getFindCardNumber()
  {
    return( FindCardNumber );
  }
  
  public boolean hasMultiCurrency()
  {
    return( MultiCurrency );
  }

  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }

  public boolean isFindTransactionData( )
  {
    return( (FindTransaction == true) &&
            (FindDataValid == true) &&
            (getReportType() == RT_DETAILS) );
  }

  public boolean isSabreAccount( )
  {
    long      nodeId          = getReportHierarchyNode();
    int       recCount        = 0;
    boolean   wasStale        = false;
    
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      if ( this instanceof MerchPOSTranDataBean )
      {
        if ( isNodeMerchant(nodeId) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:617^11*/

//  ************************************************************
//  #sql [Ctx] { select  count( mf.merchant_number ) 
//              from    mif   mf
//              where   mf.merchant_number = :nodeId and
//                      mf.bank_number = 3941 and
//                      mf.group_2_association in
//                        (
//                          400059  -- sabre
//                        )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( mf.merchant_number )  \n            from    mif   mf\n            where   mf.merchant_number =  :1  and\n                    mf.bank_number = 3941 and\n                    mf.group_2_association in\n                      (\n                        400059  -- sabre\n                      )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:627^11*/
        }
        else    // assoc or group
        {
          /*@lineinfo:generated-code*//*@lineinfo:631^11*/

//  ************************************************************
//  #sql [Ctx] { select  count( th.descendent ) 
//              from    t_hierarchy       th
//              where   th.hier_type = 1 and
//                      th.ancestor = 3941400059 and
//                      th.descendent = :nodeId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( th.descendent )  \n            from    t_hierarchy       th\n            where   th.hier_type = 1 and\n                    th.ancestor = 3941400059 and\n                    th.descendent =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:638^11*/
        }
      }
    }
    catch( Exception e )
    {
      logEntry( "isSabreAccount()", e.toString() );
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }

    return( recCount > 0 );
  }

  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    long                          merchantId        = 0L;
    ResultSet                     resultSet         = null;
    StringBuffer                  temp              = null;
    String                        tempCard          = null;

    try
    {
      MultiCurrency = false;
      
      if ( FindTransaction == true )
      {
        if ( FindDataValid == true )
        {
          if ( FindCardNumberString != null )
          {
            // create a 4321xxxxxxxx1234 version for legacy card number
            // encoding in Oracle (4x4 instead of 6x4).
            temp = new StringBuffer(FindCardNumberString);
            for( int i = 4; i < (int)Math.min(6,FindCardNumberString.length()); ++i )
            {
              if ( Character.isDigit( temp.charAt(i) ) )
              {
                temp.setCharAt(i,'x');
              }
            }
            tempCard = temp.toString();
          }

          /*@lineinfo:generated-code*//*@lineinfo:688^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ORDERED USE_NL (dt pd) FIRST_ROWS */
//                      mf.merchant_number                as hierarchy_node,
//                      mf.dba_name                       as org_name,
//                      dt.reference_number               as ref_num,
//                      dt.card_type                      as card_type,
//                      dt.authorization_num              as auth_code,
//                      ( dt.transaction_amount *
//                        decode(dt.debit_credit_indicator,
//                               'C',-1,1) )              as tran_amount,
//                      dt.transaction_date               as tran_date,
//                      nvl(pd.pos_entry_desc,
//                          dt.pos_entry_mode )           as pos_entry_mode,
//                      dt.cardholder_account_number      as card_number,
//                      dt.card_number_enc                as card_number_enc,
//                      null                              as passenger_name,
//                      null                              as pos_tid,
//                      'ddf'                             as file_prefix,
//                      dt.debit_credit_indicator         as debit_credit_ind,
//                      dt.batch_number                   as batch_number,
//                      dt.batch_date                     as batch_date,
//                      ltrim(rtrim(dt.purchase_id))      as purchase_id,
//                      nvl(dt.merchant_id,1)             as terminal_number,
//                      dt.merchant_batch_number          as merchant_batch_number,
//                      dt.downgrade_rec_id               as downgrade_rec_id,
//                      dt.trident_tran_id                as trident_tran_id,
//                      dt.client_reference_number        as client_ref_num,
//                      dt.original_transaction_amount    as original_amount,
//                      dt.original_currency_code         as original_currency_code,
//                      dt.currency_code                  as currency_code
//               from   group_merchant            gm,
//                      mif                       mf,
//                      daily_detail_file_dt      dt,
//                      pos_entry_mode_desc       pd
//               where  gm.org_num                  = :orgId and
//                      mf.merchant_number          = gm.merchant_number and
//                      dt.merchant_account_number  = mf.merchant_number and
//                      dt.batch_date between :beginDate and :endDate and
//                      pd.pos_entry_code(+)        = dt.pos_entry_mode and
//                      ( not ( dt.pos_entry_mode = 'NA' and dt.net_deposit = 0 ) ) and
//                      ( dt.cardholder_account_number like :FindCardNumberString or
//                        dt.cardholder_account_number like :tempCard or
//                        dt.reference_number = :FindValue or
//                        dt.purchase_id = :FindValue )
//               order by dt.batch_date, dt.batch_number, dt.cardholder_account_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ORDERED USE_NL (dt pd) FIRST_ROWS */\n                    mf.merchant_number                as hierarchy_node,\n                    mf.dba_name                       as org_name,\n                    dt.reference_number               as ref_num,\n                    dt.card_type                      as card_type,\n                    dt.authorization_num              as auth_code,\n                    ( dt.transaction_amount *\n                      decode(dt.debit_credit_indicator,\n                             'C',-1,1) )              as tran_amount,\n                    dt.transaction_date               as tran_date,\n                    nvl(pd.pos_entry_desc,\n                        dt.pos_entry_mode )           as pos_entry_mode,\n                    dt.cardholder_account_number      as card_number,\n                    dt.card_number_enc                as card_number_enc,\n                    null                              as passenger_name,\n                    null                              as pos_tid,\n                    'ddf'                             as file_prefix,\n                    dt.debit_credit_indicator         as debit_credit_ind,\n                    dt.batch_number                   as batch_number,\n                    dt.batch_date                     as batch_date,\n                    ltrim(rtrim(dt.purchase_id))      as purchase_id,\n                    nvl(dt.merchant_id,1)             as terminal_number,\n                    dt.merchant_batch_number          as merchant_batch_number,\n                    dt.downgrade_rec_id               as downgrade_rec_id,\n                    dt.trident_tran_id                as trident_tran_id,\n                    dt.client_reference_number        as client_ref_num,\n                    dt.original_transaction_amount    as original_amount,\n                    dt.original_currency_code         as original_currency_code,\n                    dt.currency_code                  as currency_code\n             from   group_merchant            gm,\n                    mif                       mf,\n                    daily_detail_file_dt      dt,\n                    pos_entry_mode_desc       pd\n             where  gm.org_num                  =  :1  and\n                    mf.merchant_number          = gm.merchant_number and\n                    dt.merchant_account_number  = mf.merchant_number and\n                    dt.batch_date between  :2  and  :3  and\n                    pd.pos_entry_code(+)        = dt.pos_entry_mode and\n                    ( not ( dt.pos_entry_mode = 'NA' and dt.net_deposit = 0 ) ) and\n                    ( dt.cardholder_account_number like  :4  or\n                      dt.cardholder_account_number like  :5  or\n                      dt.reference_number =  :6  or\n                      dt.purchase_id =  :7  )\n             order by dt.batch_date, dt.batch_number, dt.cardholder_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setString(4,FindCardNumberString);
   __sJT_st.setString(5,tempCard);
   __sJT_st.setString(6,FindValue);
   __sJT_st.setString(7,FindValue);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:734^11*/
        }
      }
      else    // FindTransaction is false
      {
        /*@lineinfo:generated-code*//*@lineinfo:739^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ORDERED USE_NL (dt pd) FIRST_ROWS */
//                    mf.merchant_number                as hierarchy_node,
//                    mf.dba_name                       as org_name,
//                    dt.reference_number               as ref_num,
//                    dt.card_type                      as card_type,
//                    dt.authorization_num              as auth_code,
//                    ( dt.transaction_amount *
//                      decode(dt.debit_credit_indicator,
//                             'C',-1,1) )              as tran_amount,
//                    dt.transaction_date               as tran_date,
//                    nvl(pd.pos_entry_desc,
//                        dt.pos_entry_mode )           as pos_entry_mode,
//                    dt.cardholder_account_number      as card_number,
//                    dt.card_number_enc                as card_number_enc,
//                    null                              as passenger_name,
//                    null                              as pos_tid,
//                    'ddf'                             as file_prefix,
//                    dt.debit_credit_indicator         as debit_credit_ind,
//                    dt.batch_number                   as batch_number,
//                    dt.batch_date                     as batch_date,
//                    ltrim(rtrim(dt.purchase_id))      as purchase_id,
//                    nvl(dt.merchant_id,1)             as terminal_number,
//                    dt.merchant_batch_number          as merchant_batch_number,
//                    dt.downgrade_rec_id               as downgrade_rec_id,
//                    dt.trident_tran_id                as trident_tran_id,
//                    dt.client_reference_number        as client_ref_num,
//                    dt.original_transaction_amount    as original_amount,
//                    dt.original_currency_code         as original_currency_code,
//                    dt.currency_code                  as currency_code,
//                    case
//                      when nvl(dt.currency_code,'840') != nvl(dt.original_currency_code,'840') then 'Y' else 'N'
//                    end                               as multi_currency
//             from   group_merchant            gm,
//                    mif                       mf,
//                    daily_detail_file_dt      dt,
//                    pos_entry_mode_desc       pd
//             where  gm.org_num                  = :orgId and
//                    mf.merchant_number          = gm.merchant_number and
//                    dt.merchant_account_number  = mf.merchant_number and
//                    dt.batch_date between :beginDate and :endDate and
//                    pd.pos_entry_code(+)        = dt.pos_entry_mode and
//                    ( not ( dt.pos_entry_mode = 'NA' and dt.net_deposit = 0 ) ) and
//                    ( :BatchNumber = 0 or dt.batch_number = :BatchNumber ) and
//                    ( :Filter.isEnabled(TransactionFilter.FT_POS_ENTRY_MODE) = 0 or
//                      dt.pos_entry_mode = :Filter.getString(TransactionFilter.FT_POS_ENTRY_MODE) ) and
//                    ( :Filter.isEnabled(TransactionFilter.FT_CARD_TYPE) = 0 or
//                      ( :Filter.getString(TransactionFilter.FT_CARD_TYPE) = 'BANK' and
//                        nvl(decode( dt.card_type,
//                                    'VS','D',  -- visa/mc/debit always settled
//                                    'VD','D',
//                                    'VB','D',
//                                    'MC','D',
//                                    'MB','D',
//                                    'MD','D',
//                                    'DB','D',
//                                    'EB','D',
//                                    'BL','D',
//                                    'AM',substr(mf.amex_plan,1,1),
//                                    'DS',substr(mf.discover_plan,1,1),
//                                    'DC',substr(mf.diners_plan,1,1),
//                                    'JC',substr(mf.jcb_plan,1,1),
//                                    'P1',substr(mf.private_label_1_plan,1,1),
//                                    'P2',substr(mf.private_label_2_plan,1,1),
//                                    'P3',substr(mf.private_label_3_plan,1,1),
//                                    null ),'N') = 'D' ) or
//                      ( :Filter.getString(TransactionFilter.FT_CARD_TYPE) = 'BANKNOAMEX' and
//                        nvl(decode( dt.card_type,
//                                    'VS','D',  -- visa/mc/debit always settled
//                                    'VD','D',
//                                    'VB','D',
//                                    'MC','D',
//                                    'MB','D',
//                                    'MD','D',
//                                    'DB','D',
//                                    'EB','D',
//                                    'BL','D',
//                                    null ),'N') = 'D' ) or
//                      ( :Filter.getString(TransactionFilter.FT_CARD_TYPE) = 'NONBANK' and
//                        nvl(decode( dt.card_type,
//                                    'VS','D',  -- visa/mc/debit always settled
//                                    'VD','D',
//                                    'VB','D',
//                                    'MC','D',
//                                    'MB','D',
//                                    'MD','D',
//                                    'DB','D',
//                                    'EB','D',
//                                    'BL','D',
//                                    'AM',substr(mf.amex_plan,1,1),
//                                    'DS',substr(mf.discover_plan,1,1),
//                                    'DC',substr(mf.diners_plan,1,1),
//                                    'JC',substr(mf.jcb_plan,1,1),
//                                    'P1',substr(mf.private_label_1_plan,1,1),
//                                    'P2',substr(mf.private_label_2_plan,1,1),
//                                    'P3',substr(mf.private_label_3_plan,1,1),
//                                     null),'N') = 'N' ) or
//                      decode( dt.card_type,
//                              'EB','DB',      -- ebt same as debit
//                              'VD','VS',      -- all visa together
//                              'VB','VS',
//                              'MB','MC',      -- all mc together
//                              'MD','MC',
//                              'P1','PL',      -- all private label together
//                              'P2','PL',
//                              'P3','PL',
//                              dt.card_type ) = :Filter.getString(TransactionFilter.FT_CARD_TYPE) ) and
//                    ( :Filter.isEnabled(TransactionFilter.FT_TRAN_TYPE) = 0 or
//                      dt.debit_credit_indicator = :Filter.getString(TransactionFilter.FT_TRAN_TYPE) ) and
//                    ( :Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_EQUAL) = 0 or
//                      dt.transaction_amount = :Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_EQUAL) ) and
//                    ( :Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_LESS) = 0 or
//                      dt.transaction_amount < :Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_LESS) ) and
//                    ( :Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_GREATER) = 0 or
//                      dt.transaction_amount > :Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_GREATER) )
//             order by dt.batch_date, dt.batch_number, dt.cardholder_account_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1157 = Filter.isEnabled(TransactionFilter.FT_POS_ENTRY_MODE);
 String __sJT_1158 = Filter.getString(TransactionFilter.FT_POS_ENTRY_MODE);
 int __sJT_1159 = Filter.isEnabled(TransactionFilter.FT_CARD_TYPE);
 String __sJT_1160 = Filter.getString(TransactionFilter.FT_CARD_TYPE);
 String __sJT_1161 = Filter.getString(TransactionFilter.FT_CARD_TYPE);
 String __sJT_1162 = Filter.getString(TransactionFilter.FT_CARD_TYPE);
 String __sJT_1163 = Filter.getString(TransactionFilter.FT_CARD_TYPE);
 int __sJT_1164 = Filter.isEnabled(TransactionFilter.FT_TRAN_TYPE);
 String __sJT_1165 = Filter.getString(TransactionFilter.FT_TRAN_TYPE);
 int __sJT_1166 = Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_EQUAL);
 double __sJT_1167 = Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_EQUAL);
 int __sJT_1168 = Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_LESS);
 double __sJT_1169 = Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_LESS);
 int __sJT_1170 = Filter.isEnabled(TransactionFilter.FT_TRAN_AMOUNT_GREATER);
 double __sJT_1171 = Filter.getDouble(TransactionFilter.FT_TRAN_AMOUNT_GREATER);
  try {
   String theSqlTS = "select  /*+ ORDERED USE_NL (dt pd) FIRST_ROWS */\n                  mf.merchant_number                as hierarchy_node,\n                  mf.dba_name                       as org_name,\n                  dt.reference_number               as ref_num,\n                  dt.card_type                      as card_type,\n                  dt.authorization_num              as auth_code,\n                  ( dt.transaction_amount *\n                    decode(dt.debit_credit_indicator,\n                           'C',-1,1) )              as tran_amount,\n                  dt.transaction_date               as tran_date,\n                  nvl(pd.pos_entry_desc,\n                      dt.pos_entry_mode )           as pos_entry_mode,\n                  dt.cardholder_account_number      as card_number,\n                  dt.card_number_enc                as card_number_enc,\n                  null                              as passenger_name,\n                  null                              as pos_tid,\n                  'ddf'                             as file_prefix,\n                  dt.debit_credit_indicator         as debit_credit_ind,\n                  dt.batch_number                   as batch_number,\n                  dt.batch_date                     as batch_date,\n                  ltrim(rtrim(dt.purchase_id))      as purchase_id,\n                  nvl(dt.merchant_id,1)             as terminal_number,\n                  dt.merchant_batch_number          as merchant_batch_number,\n                  dt.downgrade_rec_id               as downgrade_rec_id,\n                  dt.trident_tran_id                as trident_tran_id,\n                  dt.client_reference_number        as client_ref_num,\n                  dt.original_transaction_amount    as original_amount,\n                  dt.original_currency_code         as original_currency_code,\n                  dt.currency_code                  as currency_code,\n                  case\n                    when nvl(dt.currency_code,'840') != nvl(dt.original_currency_code,'840') then 'Y' else 'N'\n                  end                               as multi_currency\n           from   group_merchant            gm,\n                  mif                       mf,\n                  daily_detail_file_dt      dt,\n                  pos_entry_mode_desc       pd\n           where  gm.org_num                  =  :1  and\n                  mf.merchant_number          = gm.merchant_number and\n                  dt.merchant_account_number  = mf.merchant_number and\n                  dt.batch_date between  :2  and  :3  and\n                  pd.pos_entry_code(+)        = dt.pos_entry_mode and\n                  ( not ( dt.pos_entry_mode = 'NA' and dt.net_deposit = 0 ) ) and\n                  (  :4  = 0 or dt.batch_number =  :5  ) and\n                  (  :6  = 0 or\n                    dt.pos_entry_mode =  :7  ) and\n                  (  :8  = 0 or\n                    (  :9  = 'BANK' and\n                      nvl(decode( dt.card_type,\n                                  'VS','D',  -- visa/mc/debit always settled\n                                  'VD','D',\n                                  'VB','D',\n                                  'MC','D',\n                                  'MB','D',\n                                  'MD','D',\n                                  'DB','D',\n                                  'EB','D',\n                                  'BL','D',\n                                  'AM',substr(mf.amex_plan,1,1),\n                                  'DS',substr(mf.discover_plan,1,1),\n                                  'DC',substr(mf.diners_plan,1,1),\n                                  'JC',substr(mf.jcb_plan,1,1),\n                                  'P1',substr(mf.private_label_1_plan,1,1),\n                                  'P2',substr(mf.private_label_2_plan,1,1),\n                                  'P3',substr(mf.private_label_3_plan,1,1),\n                                  null ),'N') = 'D' ) or\n                    (  :10  = 'BANKNOAMEX' and\n                      nvl(decode( dt.card_type,\n                                  'VS','D',  -- visa/mc/debit always settled\n                                  'VD','D',\n                                  'VB','D',\n                                  'MC','D',\n                                  'MB','D',\n                                  'MD','D',\n                                  'DB','D',\n                                  'EB','D',\n                                  'BL','D',\n                                  null ),'N') = 'D' ) or\n                    (  :11  = 'NONBANK' and\n                      nvl(decode( dt.card_type,\n                                  'VS','D',  -- visa/mc/debit always settled\n                                  'VD','D',\n                                  'VB','D',\n                                  'MC','D',\n                                  'MB','D',\n                                  'MD','D',\n                                  'DB','D',\n                                  'EB','D',\n                                  'BL','D',\n                                  'AM',substr(mf.amex_plan,1,1),\n                                  'DS',substr(mf.discover_plan,1,1),\n                                  'DC',substr(mf.diners_plan,1,1),\n                                  'JC',substr(mf.jcb_plan,1,1),\n                                  'P1',substr(mf.private_label_1_plan,1,1),\n                                  'P2',substr(mf.private_label_2_plan,1,1),\n                                  'P3',substr(mf.private_label_3_plan,1,1),\n                                   null),'N') = 'N' ) or\n                    decode( dt.card_type,\n                            'EB','DB',      -- ebt same as debit\n                            'VD','VS',      -- all visa together\n                            'VB','VS',\n                            'MB','MC',      -- all mc together\n                            'MD','MC',\n                            'P1','PL',      -- all private label together\n                            'P2','PL',\n                            'P3','PL',\n                            dt.card_type ) =  :12  ) and\n                  (  :13  = 0 or\n                    dt.debit_credit_indicator =  :14  ) and\n                  (  :15  = 0 or\n                    dt.transaction_amount =  :16  ) and\n                  (  :17  = 0 or\n                    dt.transaction_amount <  :18  ) and\n                  (  :19  = 0 or\n                    dt.transaction_amount >  :20  )\n           order by dt.batch_date, dt.batch_number, dt.cardholder_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setLong(4,BatchNumber);
   __sJT_st.setLong(5,BatchNumber);
   __sJT_st.setInt(6,__sJT_1157);
   __sJT_st.setString(7,__sJT_1158);
   __sJT_st.setInt(8,__sJT_1159);
   __sJT_st.setString(9,__sJT_1160);
   __sJT_st.setString(10,__sJT_1161);
   __sJT_st.setString(11,__sJT_1162);
   __sJT_st.setString(12,__sJT_1163);
   __sJT_st.setInt(13,__sJT_1164);
   __sJT_st.setString(14,__sJT_1165);
   __sJT_st.setInt(15,__sJT_1166);
   __sJT_st.setDouble(16,__sJT_1167);
   __sJT_st.setInt(17,__sJT_1168);
   __sJT_st.setDouble(18,__sJT_1169);
   __sJT_st.setInt(19,__sJT_1170);
   __sJT_st.setDouble(20,__sJT_1171);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:856^9*/
      }

      // find transaction does not run if params are bad
      if ( it != null )
      {
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          ReportRows.addElement( new DetailRow( resultSet ) );
          if( "Y".equals(resultSet.getString("multi_currency")) )
          {
            MultiCurrency = true;
          }
        }
        resultSet.close();
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void loadCardTypeFunding( long merchantNumber )
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      if( merchantNumber != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:895^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  substr(mf.amex_plan,1,1)              amex_plan,
//                    substr(mf.discover_plan, 1, 1)        discover_plan,
//                    substr(mf.diners_plan, 1, 1)          diners_plan,
//                    substr(mf.jcb_plan, 1, 1)             jcb_plan,
//                    substr(mf.private_label_1_plan, 1, 1) pl1_plan,
//                    substr(mf.private_label_2_plan, 1, 1) pl2_plan,
//                    substr(mf.private_label_3_plan, 1, 1) pl3_plan
//            from    mif mf
//            where   mf.merchant_number = :merchantNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  substr(mf.amex_plan,1,1)              amex_plan,\n                  substr(mf.discover_plan, 1, 1)        discover_plan,\n                  substr(mf.diners_plan, 1, 1)          diners_plan,\n                  substr(mf.jcb_plan, 1, 1)             jcb_plan,\n                  substr(mf.private_label_1_plan, 1, 1) pl1_plan,\n                  substr(mf.private_label_2_plan, 1, 1) pl2_plan,\n                  substr(mf.private_label_3_plan, 1, 1) pl3_plan\n          from    mif mf\n          where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MerchTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.MerchTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:906^9*/
        
        rs = it.getResultSet();
        
        if( rs.next() )
        {
          CtFunding = new CardTypeFunding( rs );
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("loadCardTypeFunding(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    int                           bankId            = getReportBankId();
    int                           byBatch           = 0;
    long                          dummy             = 0L;
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    MesTransactionSummary         tranSummary       = null;

    try
    {
      // load card type funding data
      loadCardTypeFunding( getReportMerchantId() );
      
      byBatch = (ShowBatchSummary ? 1 : 0);

      if ( (getReportMerchantId() != 0L) || ShowCardSummary || (byBatch != 0) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:948^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ORDERED USE_NL (gm dt mf) FIRST_ROWS */
//                    :byBatch                              as by_batch,
//                    mf.merchant_number                    as merchant_number,
//                    mf.dba_name                           as dba_name,
//                    dt.batch_date                         as batch_date,
//                    dt.batch_number                       as batch_number,
//                    decode(dt.card_type,
//                           'P1','PL',
//                           'P2','PL',
//                           'P3','PL',
//                           dt.card_type)                  as card_type,
//                    decode(dt.card_type,
//                           'AM', case when dt.batch_date >= nvl(mf.amex_conversion_date,to_date('12/31/9999','mm/dd/yyyy')) then 0 else 1 end,
//                           'DS', case when dt.batch_date >= nvl(mf.discover_map_date,to_date('12/31/9999','mm/dd/yyyy')) then 0 else 1 end,
//                           'DC', decode(substr(mf.diners_plan,1,1),'D',0,1),
//                           'JC', decode(substr(mf.jcb_plan,1,1),'D',0,1),
//                           'P1', decode(substr(mf.private_label_1_plan,1,1),'D',0,1),
//                           'P2', decode(substr(mf.private_label_2_plan,1,1),'D',0,1),
//                           'P3', decode(substr(mf.private_label_3_plan,1,1),'D',0,1),
//                           0 )                            as settled_by_issuer,
//                    decode(dt.debit_credit_indicator,'C',1,0)
//                                                          as tran_type,
//                    dt.debit_credit_indicator             as debit_credit_ind,
//                    count(dt.card_type)                   as item_count,
//                    sum(dt.transaction_amount)            as item_amount,
//                    sum(dt.transaction_amount *
//                        decode(dt.debit_credit_indicator,'C',-1,1))
//                                                          as item_amount_signed
//            from    group_merchant        gm,
//                    group_rep_merchant    grm,
//                    daily_detail_file_dt  dt,
//                    mif                   mf
//            where   gm.org_num = :orgId and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                    dt.merchant_account_number = gm.merchant_number and
//                    dt.batch_date between :beginDate and :endDate and
//                    ( not ( dt.pos_entry_mode = 'NA' and dt.net_deposit = 0 ) ) and
//                    mf.merchant_number = dt.merchant_account_number and
//                    ( :District = :DISTRICT_NONE or
//                      nvl(mf.district,:DISTRICT_UNASSIGNED) = :District )
//            group by mf.merchant_number, mf.dba_name, dt.batch_date,
//                      dt.batch_number,
//                      decode(dt.card_type,
//                             'P1','PL',
//                             'P2','PL',
//                             'P3','PL',
//                             dt.card_type),
//                      decode(dt.card_type,
//                           'AM', case when dt.batch_date >= nvl(mf.amex_conversion_date,to_date('12/31/9999','mm/dd/yyyy')) then 0 else 1 end,
//                           'DS', case when dt.batch_date >= nvl(mf.discover_map_date,to_date('12/31/9999','mm/dd/yyyy')) then 0 else 1 end,
//                           'DC', decode(substr(mf.diners_plan,1,1),'D',0,1),
//                           'JC', decode(substr(mf.jcb_plan,1,1),'D',0,1),
//                           'P1', decode(substr(mf.private_label_1_plan,1,1),'D',0,1),
//                           'P2', decode(substr(mf.private_label_2_plan,1,1),'D',0,1),
//                           'P3', decode(substr(mf.private_label_3_plan,1,1),'D',0,1),
//                           0 ),
//                      decode(dt.debit_credit_indicator,'C',1,0),
//                      dt.debit_credit_indicator
//            order by mf.merchant_number, dt.batch_date, dt.batch_number                    
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ORDERED USE_NL (gm dt mf) FIRST_ROWS */\n                   :1                               as by_batch,\n                  mf.merchant_number                    as merchant_number,\n                  mf.dba_name                           as dba_name,\n                  dt.batch_date                         as batch_date,\n                  dt.batch_number                       as batch_number,\n                  decode(dt.card_type,\n                         'P1','PL',\n                         'P2','PL',\n                         'P3','PL',\n                         dt.card_type)                  as card_type,\n                  decode(dt.card_type,\n                         'AM', case when dt.batch_date >= nvl(mf.amex_conversion_date,to_date('12/31/9999','mm/dd/yyyy')) then 0 else 1 end,\n                         'DS', case when dt.batch_date >= nvl(mf.discover_map_date,to_date('12/31/9999','mm/dd/yyyy')) then 0 else 1 end,\n                         'DC', decode(substr(mf.diners_plan,1,1),'D',0,1),\n                         'JC', decode(substr(mf.jcb_plan,1,1),'D',0,1),\n                         'P1', decode(substr(mf.private_label_1_plan,1,1),'D',0,1),\n                         'P2', decode(substr(mf.private_label_2_plan,1,1),'D',0,1),\n                         'P3', decode(substr(mf.private_label_3_plan,1,1),'D',0,1),\n                         0 )                            as settled_by_issuer,\n                  decode(dt.debit_credit_indicator,'C',1,0)\n                                                        as tran_type,\n                  dt.debit_credit_indicator             as debit_credit_ind,\n                  count(dt.card_type)                   as item_count,\n                  sum(dt.transaction_amount)            as item_amount,\n                  sum(dt.transaction_amount *\n                      decode(dt.debit_credit_indicator,'C',-1,1))\n                                                        as item_amount_signed\n          from    group_merchant        gm,\n                  group_rep_merchant    grm,\n                  daily_detail_file_dt  dt,\n                  mif                   mf\n          where   gm.org_num =  :2  and\n                  grm.user_id(+) =  :3  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :4  = -1 ) and\n                  dt.merchant_account_number = gm.merchant_number and\n                  dt.batch_date between  :5  and  :6  and\n                  ( not ( dt.pos_entry_mode = 'NA' and dt.net_deposit = 0 ) ) and\n                  mf.merchant_number = dt.merchant_account_number and\n                  (  :7  =  :8  or\n                    nvl(mf.district, :9 ) =  :10  )\n          group by mf.merchant_number, mf.dba_name, dt.batch_date,\n                    dt.batch_number,\n                    decode(dt.card_type,\n                           'P1','PL',\n                           'P2','PL',\n                           'P3','PL',\n                           dt.card_type),\n                    decode(dt.card_type,\n                         'AM', case when dt.batch_date >= nvl(mf.amex_conversion_date,to_date('12/31/9999','mm/dd/yyyy')) then 0 else 1 end,\n                         'DS', case when dt.batch_date >= nvl(mf.discover_map_date,to_date('12/31/9999','mm/dd/yyyy')) then 0 else 1 end,\n                         'DC', decode(substr(mf.diners_plan,1,1),'D',0,1),\n                         'JC', decode(substr(mf.jcb_plan,1,1),'D',0,1),\n                         'P1', decode(substr(mf.private_label_1_plan,1,1),'D',0,1),\n                         'P2', decode(substr(mf.private_label_2_plan,1,1),'D',0,1),\n                         'P3', decode(substr(mf.private_label_3_plan,1,1),'D',0,1),\n                         0 ),\n                    decode(dt.debit_credit_indicator,'C',1,0),\n                    dt.debit_credit_indicator\n          order by mf.merchant_number, dt.batch_date, dt.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.MerchTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,byBatch);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setInt(7,District);
   __sJT_st.setInt(8,DISTRICT_NONE);
   __sJT_st.setInt(9,DISTRICT_UNASSIGNED);
   __sJT_st.setInt(10,District);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.MerchTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1011^9*/
        resultSet   = it.getResultSet();
        tranSummary = new MesTransactionSummary( bankId, resultSet );

        if( byBatch == 0 )
        {
          // add this summary to the report
          ReportRows.addElement( tranSummary );
        }
        else    // process the rest of the rows in the set
        {
          while( byBatch == 1 )
          {
            // add this summary to the report
            ReportRows.addElement( tranSummary );

            try
            {
              dummy       = resultSet.getLong("batch_number");   // test if exhausted
              tranSummary = new MesTransactionSummary( bankId, resultSet, false );
            }
            catch( Exception e )
            {
              break;    // done, exit the loop
            }
          }
        }
        it.close();
      }
      else
      {
        if ( hasAssocDistricts() && !IgnoreDistricts )
        // returns false unless the current node is an
        // association and it has districts under it.
        {
          if ( District == DISTRICT_NONE )
          {
            /*@lineinfo:generated-code*//*@lineinfo:1048^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ORDERED USE_NL (gm mf sm) FIRST_ROWS */
//                        o.org_num                             as org_num,
//                        o.org_group                           as hierarchy_node,
//                        nvl(mf.district,:DISTRICT_UNASSIGNED) as district,
//                        nvl(ad.district_desc,decode( mf.district,
//                                    null,'Unassigned',
//                                   ('District ' || to_char(mf.district, '0009')))) as org_name,
//                        sum(bank_count)                       as item_count,
//                        sum(bank_amount)                      as item_amount,
//                        sum(nonbank_count +
//                            nvl(sm.other_sales_count,0) +
//                            nvl(sm.other_credits_count,0) )   as non_bank_count,
//                        sum(nonbank_amount +
//                            ( nvl(sm.other_sales_amount,0)-
//                              nvl(sm.other_credits_amount,0) )
//                            )                                 as non_bank_amount
//                from    organization              o,
//                        group_merchant            gm,
//                        group_rep_merchant        grm,
//                        daily_detail_file_summary sm,
//                        mif                       mf,
//                        assoc_districts           ad
//                where   o.org_num           = :orgId and
//                        gm.org_num          = o.org_num and
//                        grm.user_id(+) = :AppFilterUserId and
//                        grm.merchant_number(+) = gm.merchant_number and
//                        ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                        mf.merchant_number  = gm.merchant_number and
//                        sm.merchant_number(+) = mf.merchant_number and
//                        sm.batch_date(+) between :beginDate and :endDate and
//                        ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                        ad.district(+)      = mf.district
//                group by  o.org_num, o.org_group,
//                          mf.district, ad.district_desc
//                order by o.org_num, o.org_group, mf.district
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ORDERED USE_NL (gm mf sm) FIRST_ROWS */\n                      o.org_num                             as org_num,\n                      o.org_group                           as hierarchy_node,\n                      nvl(mf.district, :1 ) as district,\n                      nvl(ad.district_desc,decode( mf.district,\n                                  null,'Unassigned',\n                                 ('District ' || to_char(mf.district, '0009')))) as org_name,\n                      sum(bank_count)                       as item_count,\n                      sum(bank_amount)                      as item_amount,\n                      sum(nonbank_count +\n                          nvl(sm.other_sales_count,0) +\n                          nvl(sm.other_credits_count,0) )   as non_bank_count,\n                      sum(nonbank_amount +\n                          ( nvl(sm.other_sales_amount,0)-\n                            nvl(sm.other_credits_amount,0) )\n                          )                                 as non_bank_amount\n              from    organization              o,\n                      group_merchant            gm,\n                      group_rep_merchant        grm,\n                      daily_detail_file_summary sm,\n                      mif                       mf,\n                      assoc_districts           ad\n              where   o.org_num           =  :2  and\n                      gm.org_num          = o.org_num and\n                      grm.user_id(+) =  :3  and\n                      grm.merchant_number(+) = gm.merchant_number and\n                      ( not grm.user_id is null or  :4  = -1 ) and\n                      mf.merchant_number  = gm.merchant_number and\n                      sm.merchant_number(+) = mf.merchant_number and\n                      sm.batch_date(+) between  :5  and  :6  and\n                      ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                      ad.district(+)      = mf.district\n              group by  o.org_num, o.org_group,\n                        mf.district, ad.district_desc\n              order by o.org_num, o.org_group, mf.district";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.MerchTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.MerchTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1085^13*/
          }
          else    // a district was specified
          {
            /*@lineinfo:generated-code*//*@lineinfo:1089^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ORDERED USE_NL (gm mf sm) FIRST_ROWS */
//                        o.org_num                     as org_num,
//                        mf.merchant_number            as hierarchy_node,
//                        mf.dba_name                   as org_name,
//                        :District                     as district,
//                        sum(bank_count)               as item_count,
//                        sum(bank_amount)              as item_amount,
//                        sum(nonbank_count)            as non_bank_count,
//                        sum(nonbank_amount)           as non_bank_amount
//                from    group_merchant            gm,
//                        group_rep_merchant        grm,
//                        daily_detail_file_summary sm,
//                        mif                       mf,
//                        organization              o
//                where   gm.org_num          = :orgId and
//                        grm.user_id(+) = :AppFilterUserId and
//                        grm.merchant_number(+) = gm.merchant_number and
//                        ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                        mf.merchant_number  = gm.merchant_number and
//                        nvl(mf.district,:DISTRICT_UNASSIGNED) = :District and
//                        sm.merchant_number(+) = mf.merchant_number and
//                        sm.batch_date(+) between :beginDate and :endDate and
//                        o.org_group = mf.merchant_number
//                group by  o.org_num, mf.merchant_number, mf.dba_name
//                order by o.org_num, mf.merchant_number
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ORDERED USE_NL (gm mf sm) FIRST_ROWS */\n                      o.org_num                     as org_num,\n                      mf.merchant_number            as hierarchy_node,\n                      mf.dba_name                   as org_name,\n                       :1                      as district,\n                      sum(bank_count)               as item_count,\n                      sum(bank_amount)              as item_amount,\n                      sum(nonbank_count)            as non_bank_count,\n                      sum(nonbank_amount)           as non_bank_amount\n              from    group_merchant            gm,\n                      group_rep_merchant        grm,\n                      daily_detail_file_summary sm,\n                      mif                       mf,\n                      organization              o\n              where   gm.org_num          =  :2  and\n                      grm.user_id(+) =  :3  and\n                      grm.merchant_number(+) = gm.merchant_number and\n                      ( not grm.user_id is null or  :4  = -1 ) and\n                      mf.merchant_number  = gm.merchant_number and\n                      nvl(mf.district, :5 ) =  :6  and\n                      sm.merchant_number(+) = mf.merchant_number and\n                      sm.batch_date(+) between  :7  and  :8  and\n                      o.org_group = mf.merchant_number\n              group by  o.org_num, mf.merchant_number, mf.dba_name\n              order by o.org_num, mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.MerchTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,District);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setInt(5,DISTRICT_UNASSIGNED);
   __sJT_st.setInt(6,District);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.MerchTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1116^13*/
          }
        }
        else // just standard child report, no districts
        {
          /*@lineinfo:generated-code*//*@lineinfo:1121^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num as org_num, 
//                      o.org_group as hierarchy_node, 
//                      o.org_name as org_name, 
//                      0 as district, 
//                      sum (bank_count) as item_count, 
//                      sum (bank_amount) as item_amount, 
//                      sum (nonbank_count) as non_bank_count, 
//                      sum (nonbank_amount) as non_bank_amount 
//              from    parent_org po, 
//                      organization o, 
//                      group_merchant gm, 
//                      group_rep_merchant grm, 
//                      daily_detail_file_summary sm 
//              where   po.parent_org_num = :orgId
//                      and o.org_num = po.org_num 
//                      and gm.org_num(+) = o.org_num 
//                      and grm.user_id(+) = :AppFilterUserId
//                      and grm.merchant_number(+) = gm.merchant_number 
//                      and (not grm.user_id is null OR :AppFilterUserId = -1) 
//                      and sm.merchant_number(+) = gm.merchant_number 
//                      and sm.batch_date >= nvl(:beginDate, to_date(UID,'YY')) 
//                      and sm.batch_date <= nvl(:endDate, to_date(UID,'YY')) 
//              group by o.org_num, o.org_group, o.org_name 
//              order by o.org_num 
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num as org_num, \n                    o.org_group as hierarchy_node, \n                    o.org_name as org_name, \n                    0 as district, \n                    sum (bank_count) as item_count, \n                    sum (bank_amount) as item_amount, \n                    sum (nonbank_count) as non_bank_count, \n                    sum (nonbank_amount) as non_bank_amount \n            from    parent_org po, \n                    organization o, \n                    group_merchant gm, \n                    group_rep_merchant grm, \n                    daily_detail_file_summary sm \n            where   po.parent_org_num =  :1 \n                    and o.org_num = po.org_num \n                    and gm.org_num(+) = o.org_num \n                    and grm.user_id(+) =  :2 \n                    and grm.merchant_number(+) = gm.merchant_number \n                    and (not grm.user_id is null OR  :3  = -1) \n                    and sm.merchant_number(+) = gm.merchant_number \n                    and sm.batch_date >= nvl( :4 , to_date(UID,'YY')) \n                    and sm.batch_date <= nvl( :5 , to_date(UID,'YY')) \n            group by o.org_num, o.org_group, o.org_name \n            order by o.org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.MerchTranDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.MerchTranDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1147^11*/
        }
        processSummaryData( it.getResultSet() );
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void setProperties( HttpServletRequest request )
  {
    long            findNode      = 0L;

    super.setProperties(request);

    // load the filter for this hierarchy node
    Filter.setFilter(request);

    BatchNumber       = HttpHelper.getLong(request,"batchNumber",0);
    ShowCardSummary   = HttpHelper.getBoolean(request,"cardSummary",false);
    ShowBatchSummary  = (HttpHelper.getBoolean(request,"batchSummary",false) &&
                         (isOrgAssociation() || getReportMerchantId() != 0L) );
    FindAllAllowed    = (HttpHelper.getBoolean(request,"findAll",false) &&
                         (ReportUserBean.hasRight(MesUsers.RIGHT_QUEUE_CHARGEBACK_ADMIN)||
                          ReportUserBean.hasRight(MesUsers.RIGHT_REPORT_CB_FIND)) );
    IncludeTridentTranId = HttpHelper.getBoolean(request, "includeTridentTranId", false);

    if( (FindTransaction = HttpHelper.getBoolean(request,"findTran",false)) == true )
    {
      FindValue = HttpHelper.getString(request,"findValue",null);

      try
      {
        // look for an already masked number 444444xxxxxx0001
        StringBuffer  decodeBuf = new StringBuffer(HttpHelper.getString(request,"findCardNum",""));
        for( int i = 6; i < decodeBuf.length(); ++i )
        {
          if ( decodeBuf.charAt(i) == 'X' ||
               decodeBuf.charAt(i) == 'x' )
          {
            decodeBuf.setCharAt(i,'0');   // change to a zero
          }
        }
        FindCardNumber = Long.parseLong(decodeBuf.toString());
        decodeBuf = null;
      }
      catch( NumberFormatException e )
      {
        FindCardNumber = 0L;
      }
      FindDataValid   = true;     // assume valid

      if ( FindCardNumber == 0L )
      {
        if ( FindValue == null )    // must have one or the other
        {
          addError("Must specify a value to search for.");
          FindDataValid = false;
        }
      }
      else
      {
        if ( FindAllAllowed )   // find all requires full card number
        {
          String  findCard = Long.toString(FindCardNumber);

          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:1222^13*/

//  ************************************************************
//  #sql [Ctx] { select  ( substr(:findCard,1,6) || 'xxxxxx' ||
//                          substr(:findCard,(length(:findCard)-3),4) )
//                
//                from    dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ( substr( :1 ,1,6) || 'xxxxxx' ||\n                        substr( :2 ,(length( :3 )-3),4) )\n               \n              from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.MerchTranDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,findCard);
   __sJT_st.setString(2,findCard);
   __sJT_st.setString(3,findCard);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   FindCardNumberString = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1228^13*/
          }
          catch( Exception e )
          {
            logEntry( "setProperties(build find all card)",e.toString() );
          }
        }
        else
        {
          // build the field mask
          StringBuffer  temp = new StringBuffer(Long.toString(FindCardNumber));

          for( int i = 6; i < temp.length(); ++i )
          {
            if ( i < 12 )
            {
              temp.setCharAt(i,'x');
            }
          }
          if( temp.length() < 16 )
          {
            temp.append("%");
          }
          FindCardNumberString = temp.toString();
        }
      }


      // only check for the search node if the current user is not a merchant
      if( isNodeMerchant( getReportHierarchyNodeDefault() ) == false )
      {
        // validate the find hierarchy node.  if it is not under the login
        // hierarchy node, the set the node to the current login node.
        findNode = HttpHelper.getLong(request,"findHierarchyNode",0L);
        if ( ! isNodeParentOfNode( getReportHierarchyNodeDefault(), findNode ) )
        {
          ErrorMessage.setLength(0);
          ErrorMessage.append( "Requested node " );
          ErrorMessage.append( findNode );
          ErrorMessage.append( " is invalid because it is not a child of " );
          ErrorMessage.append( getReportHierarchyNodeDefault() );
          addError( ErrorMessage.toString() );
          FindDataValid = false;
        }
        else
        {
          // node is valid, so set the report node
          setReportHierarchyNode(findNode);

          // if the node specified is not an association or a merchant
          // node, then add a message for the user and disarm the query.
          if (  !FindAllAllowed &&
                !isNodeAssociation(findNode) &&
                !isNodeMerchant(findNode) )
          {
            ErrorMessage.setLength(0);
            ErrorMessage.append( "Requested node must be a merchant or association.  " );
            ErrorMessage.append( findNode );
            ErrorMessage.append( " is a group." );
            addError( ErrorMessage.toString() );
            FindDataValid = false;
          }
        }
      }
    }
  }

  public void showData( java.io.PrintStream out )
  {
  }

  public boolean showingBatchSummary( )
  {
    return( ShowBatchSummary );
  }

  public boolean showingCardSummary( )
  {
    return( ShowCardSummary );
  }
}/*@lineinfo:generated-code*/