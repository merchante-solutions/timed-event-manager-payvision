/*@lineinfo:filename=ChargebackQueueDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ChargebackQueueDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 3/05/03 12:13p $
  Version            : $Revision: 12 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class ChargebackQueueDataBean extends RiskBaseDataBean
{
  public class ChargebackQueueRecord extends RiskRecordBase
  {
    public String         ActionCode        = null;
    public Date           ActionDate        = null;
    public double         Amount            = 0.0;
    public int            BankNumber        = 0;
    public String         CardNumber        = null;
    public double         ClearedAmount     = 0.0;
    public Date           ClearedDate       = null;
    public int            DaysOut           = 0;
    public Date           IncomingDate      = null;
    public long           MesRefNum         = 0L;
    public String         PortfolioName     = null;
    public long           PortfolioNode     = 0L;
    public String         ReasonCode        = null;
    public String         ReasonDescription = null;
    public String         ReferenceNumber   = null;
    public Date           TranDate          = null;
    public String         UserMessage       = null;
    
    public ChargebackQueueRecord( )
    {
    }
    
    public ChargebackQueueRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      setData( resultSet );
    }                                   
    
    public void setData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      // set the base class data
      super.setData( resultSet );
      
      // set the local data
      ActionCode        = resultSet.getString("action_code");
      ActionDate        = resultSet.getDate("action_date");
      IncomingDate      = resultSet.getDate("incoming_date");
      DaysOut           = resultSet.getInt("days_out");
      ReasonCode        = resultSet.getString("reason_code");
      ReasonDescription = resultSet.getString("reason_desc");
      ClearedDate       = resultSet.getDate("date_cleared");
      ClearedAmount     = resultSet.getDouble("cleared_amount");
      BankNumber        = resultSet.getInt("bank_number");
      UserMessage       = resultSet.getString("user_message");
      MesRefNum         = resultSet.getLong("mes_ref_num");
      ReferenceNumber   = resultSet.getString("ref_num");
      CardNumber        = resultSet.getString("card_number");
      Amount            = resultSet.getDouble("tran_amount");
      PortfolioNode     = resultSet.getLong("portfolio_node");
      PortfolioName     = resultSet.getString("portfolio_name");
      TranDate          = resultSet.getDate("tran_date");
    }
  
    public void showData( java.io.PrintStream out )
    {
    }  
  }
  
  private boolean         DownloadOnly                  = false;
  
  public ChargebackQueueDataBean( )
  {
  }
  
  public void encodeHeaderCSV( StringBuffer line )
  {
    int       bankId    = getReportBankIdDefault();
    
    line.setLength(0);
    line.append("\"Bank\",");
    line.append("\"MeS Ref #\",");
    if (  bankId == 3941 || bankId == 9999 )
    {
      line.append("\"Portfolio\",");
      line.append("\"Portfolio Name\",");
    }      
    line.append("\"Association\",");
    line.append("\"Merchant Id\",");
    line.append("\"Merchant Name\",");
    line.append("\"Reference #\",");
    line.append("\"Incoming Date\",");
    line.append("\"Card Number\",");
    line.append("\"Tran Amount\",");
    line.append("\"Cleared Amount\",");
    line.append("\"Days Out\",");
    line.append("\"Cleared Date\",");
    line.append("\"Action\",");
    line.append("\"Action Date\",");
    line.append("\"Reason\",");
    line.append("\"Reason Description\",");
    line.append("\"CB Status\",");
    line.append("\"CB Group\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    int                   bankId  = getReportBankIdDefault();
    ChargebackQueueRecord record  = (ChargebackQueueRecord)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.BankNumber );
    line.append( "," );
    line.append( record.MesRefNum );
    line.append( "," );
    if (  bankId == 3941 || bankId == 9999 )
    {
      if ( record.BankNumber == 3941 )
      {
        // add the portfolio (group 2) node and name 
        line.append( record.PortfolioNode );
        line.append( ",\"" );
        line.append( record.PortfolioName );
        line.append( "\"," );
      }
      else
      {
        line.append("\"\",\"\",");    // nothing for non-MES bank numbers
      }        
    }      
    line.append( record.AssocNumber );
    line.append( "," );
    line.append( record.MerchantId );
    line.append( ",\"" );
    line.append( record.DbaName );
    line.append( "\"," );
    line.append( record.ReferenceNumber );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.IncomingDate,"MM/dd/yyyy" ) );
    line.append( ",\"" );
    line.append( record.CardNumber );
    line.append( "\"," );
    line.append( record.Amount );
    line.append( "," );
    line.append( record.ClearedAmount );
    line.append( "," );
    line.append( record.DaysOut );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.ClearedDate,"MM/dd/yyyy" ) );
    line.append( ",\"" );
    line.append( (record.ActionCode == null) ? "" : record.ActionCode );
    line.append( "\"," );
    line.append( DateTimeFormatter.getFormattedDate(record.ActionDate,"MM/dd/yyyy" ) );
    line.append( "," );
    line.append( record.ReasonCode );
    line.append( ",\"" );
    line.append( record.ReasonDescription );
    line.append( "\"," );
    if ( (record.ClearedDate == null) && 
         (record.ActionCode == null)  && 
         (record.ClearedAmount == 0.0) ) 
    {
      line.append("0,");
      
      int groupId   = 1;
      for ( int threshold = 10; threshold < 365; threshold += 5 )
      {
        switch( threshold )
        {
          case 10:
            groupId = 1;
            break;
            
          case 15:
          case 20:
            groupId = 2;
            break;
            
          case 25:
          case 30:
          case 35:
            groupId = 3;
            break;
            
          case 40:
            groupId = 4;
            break;
            
          case 45:
            groupId = 5;
            break;
            
          default:
            groupId = 6;
            break;
        }
        if ( record.DaysOut <= threshold )
        {
          line.append( groupId );
          break;    // exit the for loop
        }
      }
    }
    else
    {
      line.append("1,0");
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_cb_queue_activity_");
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    filename.append("_to_");
    filename.append( rptDate.format( ReportDateEnd ) );
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
        
      case FF_CSV_STATIC:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    loadData( RISK_STYLE_DEFAULT, getReportOrgId(), getReportDateBegin(), getReportDateEnd(), null );
  }
  
  //
  // provided as a simple entry point for the JSP that uses this bean.
  //
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    loadData( RISK_STYLE_DEFAULT, orgId, beginDate, endDate, null );
  }
  
  //  
  // required by the abstract base class to use 5 args even though 
  // this particular bean only needs three of the args.
  //  
  public void loadData( int reportStyle, long orgId, Date beginDate, Date endDate, String[] args )
  {
    int                           bankNum           = getReportBankIdDefault();
    StringBuffer                  buffer            = null;
    ResultSetIterator             it                = null;
    BufferedWriter                out               = null;
    ChargebackQueueRecord         record            = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:319^7*/

//  ************************************************************
//  #sql [Ctx] it = { select po.org_group                   as portfolio_node,
//                 po.org_name                    as portfolio_name,
//                 cb.cb_load_sec                 as mes_ref_num,
//                 cb.bank_number                 as bank_number,
//                 cb.MERCHANT_NUMBER             as merchant_number,
//                 mf.dba_name                    as dba_name,
//                 mf.sic_code                    as sic_code,
//                 cb.REFERENCE_NUMBER            as ref_num,
//                 cb.INCOMING_DATE               as incoming_date,
//                 cb.CARD_NUMBER                 as card_number,
//                 cb.TRAN_AMOUNT                 as tran_amount,
//                 cb.tran_date                   as tran_date,
//                 null                           as auth_code,
//                 decode( cba.action_code, 
//                         null, 0, 
//                         'C', (cb.tran_amount * -1), 
//                         (cb.tran_amount * 1) ) as cleared_amount,       
//                 decode( cba.action_code,
//                         null, trunc( sysdate - cb.incoming_date ),
//                         trunc( cba.action_date - cb.incoming_date ) )  as days_out,
//                 nvl( cba.adj_batch_date, 
//                      cba.action_date )         as date_cleared,               
//                 cba.action_date                as action_date,
//                 ac.ACTION_CODE                 as action_code,
//                 cb.reason_code                 as reason_code,
//                 rd.REASON_DESC                 as reason_desc,
//                 cba.user_message               as user_message,
//                 (mf.bank_number || mf.dmagent) as assoc_number
//          from   network_chargebacks            cb,
//                 network_chargeback_activity    cba,
//                 chargeback_action_codes        ac,
//                 chargeback_reason_desc         rd,
//                 mif                            mf,
//                 groups                         g,
//                 organization                   po
//          where  ( ( :bankNum = 9999 and cb.bank_number in (3858,3941,3860) ) or
//                    cb.bank_number = :bankNum ) and
//                 ( ( cb.incoming_date between :beginDate and :endDate ) or
//                   ( cba.action_date is null and cb.incoming_date > trunc(sysdate-60)) ) and
//                 cba.cb_load_sec(+) = cb.cb_load_sec and
//                 ac.SHORT_ACTION_CODE(+) = cba.action_code and
//                 rd.card_type(+) = decode( substr(cb.card_number,1,1),
//                                        '4', 'VS',
//                                        '5', 'MC',
//                                        'UK' ) and
//                 rd.item_type(+) = 'C' and -- chargeback reason                   
//                 rd.reason_code(+) = cb.reason_code and
//                 mf.merchant_number(+) = cb.merchant_number and
//                 g.assoc_number(+) = (mf.bank_number || mf.dmagent) and
//                 po.org_group(+) = g.group_2
//          order by bank_number, days_out desc,
//                   cb.merchant_number, cba.action_date      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select po.org_group                   as portfolio_node,\n               po.org_name                    as portfolio_name,\n               cb.cb_load_sec                 as mes_ref_num,\n               cb.bank_number                 as bank_number,\n               cb.MERCHANT_NUMBER             as merchant_number,\n               mf.dba_name                    as dba_name,\n               mf.sic_code                    as sic_code,\n               cb.REFERENCE_NUMBER            as ref_num,\n               cb.INCOMING_DATE               as incoming_date,\n               cb.CARD_NUMBER                 as card_number,\n               cb.TRAN_AMOUNT                 as tran_amount,\n               cb.tran_date                   as tran_date,\n               null                           as auth_code,\n               decode( cba.action_code, \n                       null, 0, \n                       'C', (cb.tran_amount * -1), \n                       (cb.tran_amount * 1) ) as cleared_amount,       \n               decode( cba.action_code,\n                       null, trunc( sysdate - cb.incoming_date ),\n                       trunc( cba.action_date - cb.incoming_date ) )  as days_out,\n               nvl( cba.adj_batch_date, \n                    cba.action_date )         as date_cleared,               \n               cba.action_date                as action_date,\n               ac.ACTION_CODE                 as action_code,\n               cb.reason_code                 as reason_code,\n               rd.REASON_DESC                 as reason_desc,\n               cba.user_message               as user_message,\n               (mf.bank_number || mf.dmagent) as assoc_number\n        from   network_chargebacks            cb,\n               network_chargeback_activity    cba,\n               chargeback_action_codes        ac,\n               chargeback_reason_desc         rd,\n               mif                            mf,\n               groups                         g,\n               organization                   po\n        where  ( (  :1  = 9999 and cb.bank_number in (3858,3941,3860) ) or\n                  cb.bank_number =  :2  ) and\n               ( ( cb.incoming_date between  :3  and  :4  ) or\n                 ( cba.action_date is null and cb.incoming_date > trunc(sysdate-60)) ) and\n               cba.cb_load_sec(+) = cb.cb_load_sec and\n               ac.SHORT_ACTION_CODE(+) = cba.action_code and\n               rd.card_type(+) = decode( substr(cb.card_number,1,1),\n                                      '4', 'VS',\n                                      '5', 'MC',\n                                      'UK' ) and\n               rd.item_type(+) = 'C' and -- chargeback reason                   \n               rd.reason_code(+) = cb.reason_code and\n               mf.merchant_number(+) = cb.merchant_number and\n               g.assoc_number(+) = (mf.bank_number || mf.dmagent) and\n               po.org_group(+) = g.group_2\n        order by bank_number, days_out desc,\n                 cb.merchant_number, cba.action_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ChargebackQueueDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNum);
   __sJT_st.setInt(2,bankNum);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ChargebackQueueDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:373^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        if ( DownloadOnly )
        {
          if ( buffer == null )
          {
            initReportFile();     // see ReportSQLJBean
            
            // setup the buffered i/o
            out = new BufferedWriter( new FileWriter( ReportFile ) );
            
            // create a memory buffer
            buffer = new StringBuffer();
            encodeHeaderCSV(buffer);
            out.write(buffer.toString());
            out.newLine();
            
            // create a new empty queue record
            record = new ChargebackQueueRecord();
          }
          
          // store the current row data into the record
          record.setData(resultSet);
          
          // reset the string buffer and encode the record
          buffer.setLength(0);
          encodeLineCSV(record,buffer);
          
          // write this row to the temp csv file
          out.write(buffer.toString());
          out.newLine();
        }
        else
        {
          ReportRows.add( new ChargebackQueueRecord( resultSet ) );
        }                                                  
      }
      it.close();   // this will also close the resultSet
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      try{ out.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    Calendar        cal = Calendar.getInstance();
    int             inc = 0;
    
    // load the default report properties
    super.setProperties( request );
    
    DownloadOnly = ( HttpHelper.getInt( request, "dl", 0 ) != 0 );
    
    // if this is a download from the menus (not download link)
    // then set the date to the last 60 days of queue activity.
    if ( DownloadOnly )
    {
      cal.add( Calendar.DAY_OF_MONTH, -60 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd  ( new java.sql.Date( Calendar.getInstance().getTime().getTime() ) );
      setReportOrgId ( HttpHelper.getLong( request, "com.mes.OrgId", getReportOrgIdDefault() ) );
    }
    else    // this is a JSP
    {
      // if the from date is today or the user selected the download link
      // from the menus, then set the default to last 60 days
      if ( usingDefaultReportDates() )
      {       
        if( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.MONDAY )
        {
          inc = -3;
        }
        else
        {
          inc = -1;
        }
        cal.add( Calendar.DAY_OF_MONTH, inc );
        
        setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
        setReportDateBegin( getReportDateBegin() );
      }    

      
      if ( usingDefaultNode() )
      {
        // The RiskBaseDataBean will set the report node
        // to 394100000 by default when the node is set
        // to the top of the tree (9999999999).  For
        // all other risk beans this is desired, but for
        // this particular bean, we return the default
        // org id to the login org id (top of tree).
        setReportOrgId( getReportOrgIdDefault() );
      }
    }      
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/