/*@lineinfo:filename=RiskMerchOtherDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskMerchOtherDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 2/03/04 4:37p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class RiskMerchOtherDataBean extends RiskMerchHistoryBase
{
  public class RowData implements Comparable
  {
    public Date           ActiveDate                = null;
    public Date           MonthBeginDate            = null;
    public Date           MonthEndDate              = null;
    
    public double         ChargebackFTAmount        = 0.0;
    public int            ChargebackFTCount         = 0;        
    public double         ChargebackTotaAmount      = 0.0;
    public int            ChargebackTotalCount      = 0;        
    public double         DuplicateAmount           = 0.0;
    public int            DuplicateCount            = 0;    
    public double         RetrievalAmount           = 0.0;
    public int            RetrievalCount            = 0;   
    public double         UnmatchedCreditsAmount    = 0.0;
    public int            UnmatchedCreditsCount     = 0;      
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      ActiveDate        = resultSet.getDate("active_date");
      MonthBeginDate    = resultSet.getDate("month_begin_date");
      MonthEndDate      = resultSet.getDate("month_end_date");
    }
    
    public void addData( ResultSet resultSet )
    {
      try
      {
        ChargebackFTAmount    = resultSet.getDouble("cb_ft_amount");
        ChargebackFTCount     = resultSet.getInt("cb_ft_count");
        ChargebackTotaAmount  = resultSet.getDouble("cb_total_amount");
        ChargebackTotalCount  = resultSet.getInt("cb_total_count");
      }
      catch(Exception e)
      {
      }
      
      try
      {
        DuplicateAmount       = resultSet.getDouble("dup_amount");
        DuplicateCount        = resultSet.getInt("dup_count");
      }
      catch(Exception e)
      {
      }
      
      try
      {
        RetrievalAmount       = resultSet.getDouble("retr_amount");
        RetrievalCount        = resultSet.getInt("retr_count");
      }
      catch(Exception e)
      {
      }
      
      try
      {
        UnmatchedCreditsAmount  = resultSet.getDouble("unmatched_amount");
        UnmatchedCreditsCount   = resultSet.getInt("unmatched_count");
      }
      catch(Exception e)
      {
      }
    }
    
    public int compareTo( Object obj )
    {
      RowData     compareObj  = (RowData)obj;
      
      return( -(ActiveDate.compareTo(compareObj.ActiveDate)) );
    }
  }
  
  public RiskMerchOtherDataBean( )
  {
  }
  
  protected void addRowData( ResultSet resultSet )
  {
    RowData         row     = null;
    RowData         temp    = null;
    
    try
    {
      for( int i = 0; i < ReportRows.size(); ++i )
      {
        temp = (RowData)ReportRows.elementAt(i);
      
        if ( temp.ActiveDate.equals( resultSet.getDate("active_date") ) )
        {
          row = temp;
          break;
        }
      }
      if ( row == null )
      {
        row = new RowData( resultSet );
        ReportRows.addElement( row );
      }
      row.addData( resultSet );
    }
    catch(Exception e)
    {
      logEntry("addRowData()",e.toString());
    }      
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    super.encodeHeaderCSV(line);
    
    line.append("\"Active Date\",");
    line.append("\"Retrieval Count\",");
    line.append("\"Retrieval Amount\",");
    line.append("\"Incoming CB Count\",");
    line.append("\"Incoming CB Amount\",");
    line.append("\"Unmatched Credits Count\",");
    line.append("\"Unmatched Credits Amount\",");
    line.append("\"Duplicates Count\",");
    line.append("\"Duplicates Amount\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( DateTimeFormatter.getFormattedDate(record.ActiveDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( record.RetrievalCount );
    line.append( "," );
    line.append( record.RetrievalAmount );
    line.append( "," );
    line.append( record.ChargebackFTCount );
    line.append( "," );
    line.append( record.ChargebackFTAmount );
    line.append( "," );
    line.append( record.UnmatchedCreditsCount );
    line.append( "," );
    line.append( record.UnmatchedCreditsAmount );
    line.append( "," );
    line.append( record.DuplicateCount );
    line.append( "," );
    line.append( record.DuplicateAmount );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_risk_other_summary_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    loadData( getReportMerchantId(), ReportDateBegin, ReportDateEnd );
  }
  
  public void loadData( long merchantId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      MerchData = null;
      
      for( int i = 0; i < 4; ++i )
      {
        it = null;
        switch( i ) 
        {
          case 0:
            /*@lineinfo:generated-code*//*@lineinfo:250^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  rt.merchant_number                          as merchant_number,
//                        mf.dba_name                                 as dba_name,
//                        mr.merch_legal_name                         as legal_name,
//                        to_date(mf.date_opened,'mmddrr')            as date_opened,
//                        mf.sic_code                                 as sic_code,
//                        trunc(rt.incoming_date,'month')             as active_date,
//                        trunc(rt.incoming_date,'month')             as month_begin_date,
//                        last_day(rt.incoming_date)                  as month_end_date,
//                        count(rt.merchant_number)                   as retr_count,
//                        sum(rt.tran_amount)                         as retr_amount
//                from    network_retrievals          rt,
//                        mif                         mf,
//                        merchant                    mr
//                where   rt.merchant_number = :merchantId and
//                        rt.incoming_date between trunc( (:ReportDateBegin - 515), 'month') and :ReportDateBegin and
//                        mf.merchant_number = rt.merchant_number and
//                        mr.merch_number(+) = mf.merchant_number
//                group by rt.merchant_number, mf.dba_name,
//                         mr.merch_legal_name,   
//                         to_date(mf.date_opened,'mmddrr'),
//                         mf.sic_code, trunc(rt.incoming_date,'month'),
//                         last_day(rt.incoming_date)
//                order by active_date desc
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rt.merchant_number                          as merchant_number,\n                      mf.dba_name                                 as dba_name,\n                      mr.merch_legal_name                         as legal_name,\n                      to_date(mf.date_opened,'mmddrr')            as date_opened,\n                      mf.sic_code                                 as sic_code,\n                      trunc(rt.incoming_date,'month')             as active_date,\n                      trunc(rt.incoming_date,'month')             as month_begin_date,\n                      last_day(rt.incoming_date)                  as month_end_date,\n                      count(rt.merchant_number)                   as retr_count,\n                      sum(rt.tran_amount)                         as retr_amount\n              from    network_retrievals          rt,\n                      mif                         mf,\n                      merchant                    mr\n              where   rt.merchant_number =  :1  and\n                      rt.incoming_date between trunc( ( :2  - 515), 'month') and  :3  and\n                      mf.merchant_number = rt.merchant_number and\n                      mr.merch_number(+) = mf.merchant_number\n              group by rt.merchant_number, mf.dba_name,\n                       mr.merch_legal_name,   \n                       to_date(mf.date_opened,'mmddrr'),\n                       mf.sic_code, trunc(rt.incoming_date,'month'),\n                       last_day(rt.incoming_date)\n              order by active_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.RiskMerchOtherDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateBegin);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.RiskMerchOtherDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:275^13*/
            break;
          
          case 1:     // incoming chargebacks
            /*@lineinfo:generated-code*//*@lineinfo:279^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.merchant_number                          as merchant_number,
//                        mf.dba_name                                 as dba_name,
//                        mr.merch_legal_name                         as legal_name,
//                        to_date(mf.date_opened,'mmddrr')            as date_opened,
//                        mf.sic_code                                 as sic_code,
//                        trunc(cb.incoming_date,'month')             as active_date,
//                        trunc(cb.incoming_date,'month')             as month_begin_date,
//                        last_day(cb.incoming_date)                  as month_end_date,
//                        sum( decode( cb.first_time_chargeback,
//                                     'Y', 1,
//                                     0 ) )                          as cb_ft_count,
//                        sum( decode( cb.first_time_chargeback,
//                                     'Y', cb.tran_amount,
//                                     0 ) )                          as cb_ft_amount,
//                        count(cb.merchant_number)                   as cb_total_count,
//                        sum(cb.tran_amount)                         as cb_total_amount
//                from    network_chargebacks         cb,
//                        mif                         mf,
//                        merchant                    mr
//                where   cb.merchant_number = :merchantId and
//                        cb.incoming_date between trunc( (:ReportDateBegin - 515), 'month') and :ReportDateBegin and
//                        mf.merchant_number = cb.merchant_number and
//                        mr.merch_number(+) = mf.merchant_number
//                group by cb.merchant_number, mf.dba_name,
//                         mr.merch_legal_name,   
//                         to_date(mf.date_opened,'mmddrr'),
//                         mf.sic_code, trunc(cb.incoming_date,'month'),
//                         last_day(cb.incoming_date)
//                order by active_date desc
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.merchant_number                          as merchant_number,\n                      mf.dba_name                                 as dba_name,\n                      mr.merch_legal_name                         as legal_name,\n                      to_date(mf.date_opened,'mmddrr')            as date_opened,\n                      mf.sic_code                                 as sic_code,\n                      trunc(cb.incoming_date,'month')             as active_date,\n                      trunc(cb.incoming_date,'month')             as month_begin_date,\n                      last_day(cb.incoming_date)                  as month_end_date,\n                      sum( decode( cb.first_time_chargeback,\n                                   'Y', 1,\n                                   0 ) )                          as cb_ft_count,\n                      sum( decode( cb.first_time_chargeback,\n                                   'Y', cb.tran_amount,\n                                   0 ) )                          as cb_ft_amount,\n                      count(cb.merchant_number)                   as cb_total_count,\n                      sum(cb.tran_amount)                         as cb_total_amount\n              from    network_chargebacks         cb,\n                      mif                         mf,\n                      merchant                    mr\n              where   cb.merchant_number =  :1  and\n                      cb.incoming_date between trunc( ( :2  - 515), 'month') and  :3  and\n                      mf.merchant_number = cb.merchant_number and\n                      mr.merch_number(+) = mf.merchant_number\n              group by cb.merchant_number, mf.dba_name,\n                       mr.merch_legal_name,   \n                       to_date(mf.date_opened,'mmddrr'),\n                       mf.sic_code, trunc(cb.incoming_date,'month'),\n                       last_day(cb.incoming_date)\n              order by active_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.RiskMerchOtherDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateBegin);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.RiskMerchOtherDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:310^13*/
            break;
            
          case 2:     // unmatched credits
            /*@lineinfo:generated-code*//*@lineinfo:314^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  cr.merchant_number                  as merchant_number,
//                        mf.dba_name                         as dba_name,
//                        mr.merch_legal_name                 as legal_name,
//                        to_date(mf.date_opened,'mmddrr')    as date_opened,
//                        mf.sic_code                         as sic_code,
//                        trunc(cr.batch_date,'month')        as active_date,
//                        trunc(cr.batch_date,'month')        as month_begin_date,
//                        last_day(cr.batch_date)             as month_end_date,
//                        count(cr.merchant_number)           as unmatched_count,
//                        sum(cr.tran_amount)                 as unmatched_amount
//                from    daily_detail_file_ext_credits   cr,
//                        mif                             mf,
//                        merchant                        mr
//                where   cr.merchant_number = :merchantId and
//                        cr.batch_date between trunc( (:ReportDateBegin - 515), 'month') and :ReportDateBegin and
//                        substr(cr.card_type,1,1) in ('V','M') and
//                        cr.purchase_batch_date is null and                 
//                        mf.merchant_number = cr.merchant_number and
//                        mr.merch_number(+) = mf.merchant_number
//                group by cr.merchant_number, mf.dba_name,
//                         mr.merch_legal_name,   
//                         to_date(mf.date_opened,'mmddrr'),
//                         mf.sic_code, trunc(cr.batch_date,'month'),
//                         last_day(cr.batch_date)
//                order by active_date desc
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cr.merchant_number                  as merchant_number,\n                      mf.dba_name                         as dba_name,\n                      mr.merch_legal_name                 as legal_name,\n                      to_date(mf.date_opened,'mmddrr')    as date_opened,\n                      mf.sic_code                         as sic_code,\n                      trunc(cr.batch_date,'month')        as active_date,\n                      trunc(cr.batch_date,'month')        as month_begin_date,\n                      last_day(cr.batch_date)             as month_end_date,\n                      count(cr.merchant_number)           as unmatched_count,\n                      sum(cr.tran_amount)                 as unmatched_amount\n              from    daily_detail_file_ext_credits   cr,\n                      mif                             mf,\n                      merchant                        mr\n              where   cr.merchant_number =  :1  and\n                      cr.batch_date between trunc( ( :2  - 515), 'month') and  :3  and\n                      substr(cr.card_type,1,1) in ('V','M') and\n                      cr.purchase_batch_date is null and                 \n                      mf.merchant_number = cr.merchant_number and\n                      mr.merch_number(+) = mf.merchant_number\n              group by cr.merchant_number, mf.dba_name,\n                       mr.merch_legal_name,   \n                       to_date(mf.date_opened,'mmddrr'),\n                       mf.sic_code, trunc(cr.batch_date,'month'),\n                       last_day(cr.batch_date)\n              order by active_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.RiskMerchOtherDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateBegin);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.RiskMerchOtherDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:341^13*/
            break;
            
          case 3:     // duplicate cards
            /*@lineinfo:generated-code*//*@lineinfo:345^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                    as merchant_number,
//                        mf.dba_name                           as dba_name,
//                        mr.merch_legal_name                   as legal_name,
//                        to_date(mf.date_opened,'mmddrr')      as date_opened,
//                        mf.sic_code                           as sic_code,
//                        dt.active_date                        as active_date,
//                        dt.active_date                        as month_begin_date,
//                        last_day(dt.active_date)              as month_end_date,
//                        sum(dt.dup_count)                     as dup_count,
//                        sum(dt.dup_amount)                    as dup_amount
//                from    mif                           mf,
//                        (
//                          select  dup.merchant_number           as merchant_number,
//                                  trunc(dup.batch_date,'month') as active_date,
//                                  dup.card_number               as card_number,
//                                  sum( dup.sales_count )        as dup_count,
//                                  sum( dup.sales_amount )       as dup_amount
//                          from    risk_duplicate_card_summary  dup
//                          where   dup.merchant_number = :merchantId and
//                                  dup.batch_date between trunc( (:ReportDateBegin - 515), 'month') and :ReportDateBegin and
//                                  substr(dup.card_number_str,1,1) in ('4','5') and
//                                  nvl(dup.sales_count,0) > 0
//                          group by  dup.merchant_number,        
//                                    trunc(dup.batch_date,'month'),
//                                    dup.card_number                                             
//                        )                             dt,
//                        merchant                      mr
//                where   mf.merchant_number = :merchantId and
//                        dt.merchant_number = mf.merchant_number and
//                        nvl(dt.dup_count,0) > 1 and
//                        mr.merch_number(+) = mf.merchant_number
//                group by mf.merchant_number, mf.dba_name,
//                         mr.merch_legal_name,   
//                         to_date(mf.date_opened,'mmddrr'),
//                         mf.sic_code, dt.active_date,
//                         last_day(dt.active_date)
//                order by active_date desc  
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                    as merchant_number,\n                      mf.dba_name                           as dba_name,\n                      mr.merch_legal_name                   as legal_name,\n                      to_date(mf.date_opened,'mmddrr')      as date_opened,\n                      mf.sic_code                           as sic_code,\n                      dt.active_date                        as active_date,\n                      dt.active_date                        as month_begin_date,\n                      last_day(dt.active_date)              as month_end_date,\n                      sum(dt.dup_count)                     as dup_count,\n                      sum(dt.dup_amount)                    as dup_amount\n              from    mif                           mf,\n                      (\n                        select  dup.merchant_number           as merchant_number,\n                                trunc(dup.batch_date,'month') as active_date,\n                                dup.card_number               as card_number,\n                                sum( dup.sales_count )        as dup_count,\n                                sum( dup.sales_amount )       as dup_amount\n                        from    risk_duplicate_card_summary  dup\n                        where   dup.merchant_number =  :1  and\n                                dup.batch_date between trunc( ( :2  - 515), 'month') and  :3  and\n                                substr(dup.card_number_str,1,1) in ('4','5') and\n                                nvl(dup.sales_count,0) > 0\n                        group by  dup.merchant_number,        \n                                  trunc(dup.batch_date,'month'),\n                                  dup.card_number                                             \n                      )                             dt,\n                      merchant                      mr\n              where   mf.merchant_number =  :4  and\n                      dt.merchant_number = mf.merchant_number and\n                      nvl(dt.dup_count,0) > 1 and\n                      mr.merch_number(+) = mf.merchant_number\n              group by mf.merchant_number, mf.dba_name,\n                       mr.merch_legal_name,   \n                       to_date(mf.date_opened,'mmddrr'),\n                       mf.sic_code, dt.active_date,\n                       last_day(dt.active_date)\n              order by active_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.RiskMerchOtherDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateBegin);
   __sJT_st.setLong(4,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.RiskMerchOtherDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:384^13*/
            break;
        }          
        
        if ( it != null )
        {
          resultSet = it.getResultSet();
    
          while( resultSet.next() )
          {
            if ( MerchData == null )
            {
              MerchData = new MerchantData( resultSet );
            }
            addRowData( resultSet );
          }
          resultSet.close();
          it.close();
        }        
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",merchantId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/