/*@lineinfo:filename=TransactionFilter*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/TransactionFilter.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 4/22/02 5:10p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;
  
public class TransactionFilter extends SQLJConnectionBase
{
  // filter ids
  static public final int      FID_CUSTOM              =   -1;
  static public final int      FID_NONE                =   0;
  
  // filter types
  static public final int      FT_NONE                 =   0;
  static public final int      FT_POS_ENTRY_MODE       =   0x0001;
  static public final int      FT_CARD_TYPE            =   0x0002;
  static public final int      FT_TRAN_TYPE            =   0x0004;
  static public final int      FT_TRAN_AMOUNT          =   0x0008;
  static public final int      FT_TRAN_AMOUNT_EQUAL    =   FT_TRAN_AMOUNT;
  static public final int      FT_TRAN_AMOUNT_LESS     =   0x0010;
  static public final int      FT_TRAN_AMOUNT_GREATER  =   0x0020;
  
  public class FilterElement
  {
    private int         Type    = 0;
    private String      Value   = null;
    
    public FilterElement( int type, String value )
    {
      Type    = type;
      Value   = value;
    }
    
    public String getDescription()
    {
      String    retVal  = getValueDesc(Type,Value);
      return( retVal );
    }
    
    public int getType()
    {
      return(Type);
    }
    
    public String getValue()
    {
      return(Value);
    }
  }
  
  public class FilterListItem
  {
    public int              FilterId        = 0;
    public String           FilterName      = null;
    
    public FilterListItem( int filterId, String name )
    {
      FilterId      = filterId;
      FilterName    = name;
    }
    
    public FilterListItem( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      FilterId      = resultSet.getInt("filter_id");
      FilterName    = resultSet.getString("filter_name");  
    }
  }

  Vector            FilterElements      = new Vector();
  int               FilterElementIndex  = 0;
  int               FilterId            = FT_NONE;
  String            FilterName          = "None";
  
  public TransactionFilter( )
  {
  }
  
  public TransactionFilter( int filterType, String filterKeys )
  {
    setFilter( FID_CUSTOM, filterType, filterKeys ); 
  }
  
  public void encodeFilterUrl( StringBuffer buffer )
  {
    FilterElement     element         = null;
    StringBuffer      filterKeys      = new StringBuffer("");
    int               filterType      = FT_NONE;
    
    for( int i = 0; i < FilterElements.size(); ++i )
    {
      element = (FilterElement)FilterElements.elementAt(i);
      filterType |= element.getType();
      if ( i > 0 )
      {
        filterKeys.append(";");
      }
      filterKeys.append( element.getValue() );
    }
    if ( filterType != FT_NONE )
    {
      buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
      buffer.append( "filterType=" );
      buffer.append( filterType );
      buffer.append( "&filterKeys=" );
      buffer.append( filterKeys.toString() );
    }      
  }
  
  public FilterElement findElement( int type )
  {
    FilterElement         retVal  = null;
    FilterElement         temp    = null;
    
    for( int i = 0; i < FilterElements.size(); ++i )
    {
      temp = (FilterElement)FilterElements.elementAt(i);
      if ( temp.getType() == type )
      {
        retVal = temp;
        break;
      }
    }
    return( retVal );
  }
  
  public FilterElement firstElement()
  {
    FilterElementIndex = 0;
    return( nextElement() );
  }
  
  public double getDouble( int type )
  {
    FilterElement   element     = findElement(type);
    double          retVal      = 0.0;
    
    try
    {
      retVal = Double.parseDouble(element.getValue());
    }
    catch( NullPointerException e )   
    {
      // no such element, return default
    }
    catch( NumberFormatException e )
    {
      // invalid number
    }
    return( retVal );
  }
  
  public String getFilterKeys( )
  {
    FilterElement   element     = null;
    StringBuffer    retVal      = new StringBuffer("");
    
    for( int i = 0; i < FilterElements.size(); ++i )
    {
      element = (FilterElement)FilterElements.elementAt(i);
      if ( i > 0 )
      {
        retVal.append(";");
      }
      retVal.append( element.getValue() );
    }
    return( retVal.toString() );
  }
  
  public int getFilterId( )
  {
    return( FilterId );
  }
  
  public String getFilterName( )
  {
    return( FilterName );
  }
  
  public int getFilterType( )
  {
    FilterElement   element     = null;
    int             retVal      = 0;
    
    for( int i = 0; i < FilterElements.size(); ++i )
    {
      element = (FilterElement)FilterElements.elementAt(i);
      retVal |= element.getType();
    }
    return( retVal );
  }
  
  public int getInt( int type )
  {
    FilterElement   element     = findElement(type);
    int             retVal      = 0;  
    
    try
    {
      retVal = Integer.parseInt(element.getValue());
    }
    catch( ClassCastException e )
    {
      // value is not an Integer, return default
    }
    catch( NullPointerException e )   
    {
      // no such element, return default
    }
    return( retVal );
  }
  
  public String getString( int type )
  {
    FilterElement   element     = findElement(type);
    String          retVal      = "";
    
    try
    {
      retVal = element.getValue();
    }
    catch( NullPointerException e )   
    {
      // no such element, return default
    }
    return( retVal );
  }
  
  public String getValueDesc( int filterType, String filterKey )
  {
    String      retVal = "ERROR";
    
    try
    {
      connect();

      switch( filterType )
      {
        case FT_POS_ENTRY_MODE:
          /*@lineinfo:generated-code*//*@lineinfo:272^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(pos_entry_desc,'ERROR')  
//              from    pos_entry_mode_desc 
//              where   pos_entry_code = :filterKey
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(pos_entry_desc,'ERROR')   \n            from    pos_entry_mode_desc \n            where   pos_entry_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.TransactionFilter",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,filterKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:277^11*/
          break;
        
        case FT_CARD_TYPE:
          retVal = MesTransactionSummary.getCardTypeString( MesTransactionSummary.getCardTypeIndex( filterKey ) );
          break;
        
        case FT_TRAN_TYPE:
          retVal = MesTransactionSummary.getTranTypeString( MesTransactionSummary.getTranTypeIndex( filterKey ) );
          break;
        
//        case FT_NONE:
        default:
          break;
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getValueDesc()",e.toString() );
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  public int isEnabled( int filterType )
  {
    return( ( (findElement(filterType) == null) ? 0 : 1 ) );
  }
  
  public void loadFilter( int filterId )
  {
    String        filterKeys    = null;
    int           filterType    = FT_NONE;
    
    try
    {
      connect();

      if ( filterId != FT_NONE )
      {
        /*@lineinfo:generated-code*//*@lineinfo:320^9*/

//  ************************************************************
//  #sql [Ctx] { select  tf.filter_type,tf.filter_keys,tf.filter_name  
//            from    tran_filters tf
//            where   tf.filter_id = :filterId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  tf.filter_type,tf.filter_keys,tf.filter_name   \n          from    tran_filters tf\n          where   tf.filter_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.TransactionFilter",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,filterId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   filterType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   filterKeys = (String)__sJT_rs.getString(2);
   FilterName = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:325^9*/
        setFilter( filterId, filterType, filterKeys );
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadFilter(" + filterId + "')", e.toString() );
    }
    finally
    {
      cleanUp();
    }
  }
  
  public Vector loadFilterList( long hierarchyNode )
  {
    ResultSetIterator           it            = null;
    Vector                      list          = new Vector();
    ResultSet                   resultSet     = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:349^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tf.filter_name        as filter_name,
//                  tf.filter_id          as filter_id,
//                  tf.hierarchy_node     as hierarchy_node
//          from    tran_filters tf
//          where   tf.hierarchy_node = :hierarchyNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tf.filter_name        as filter_name,\n                tf.filter_id          as filter_id,\n                tf.hierarchy_node     as hierarchy_node\n        from    tran_filters tf\n        where   tf.hierarchy_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.TransactionFilter",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.TransactionFilter",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:356^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        list.addElement( new FilterListItem(resultSet) );
      }
      
      resultSet.close();
      it.close();
      
      list.addElement( new FilterListItem( FID_CUSTOM,  "Custom" ) );
      list.addElement( new FilterListItem( FID_NONE,    "None" ) );
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadFilterList(" + hierarchyNode + ")", e.toString() );
    }
    finally
    {
      try { resultSet.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return(list);
  }
  
  public FilterElement nextElement( )
  {
    FilterElement       retVal      = null;
    
    try
    {
      retVal = (FilterElement)FilterElements.elementAt(FilterElementIndex);
      FilterElementIndex++;
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }
  
  public void setFilter( HttpServletRequest request )
  {
    int     filterType    = FT_NONE;
    
    filterType  = HttpHelper.getInt(request,"filterType", FT_NONE);
    FilterId    = HttpHelper.getInt(request,"filterId", FID_NONE);
    
    if ( FilterId != FID_NONE && FilterId != FID_CUSTOM )
    {
      loadFilter( FilterId );
    }
    else if ( filterType != FT_NONE )
    {
      // setup the custom filter specified
      // in the user request.
      setFilter( FID_CUSTOM, filterType, HttpHelper.getString(request,"filterKeys",null) );
    }
  }
  
  public void setFilter( int filterId, int filterType, String filterKeys )
  {
    String              filterKey = null;
    int                 key       = 0;
    StringTokenizer     tokenizer = null;
    int                 type      = 0;
    String              value     = null;
  
    try
    {
      FilterId = filterId;
      
      // empty the current filter contents
      FilterElements.removeAllElements();
      
      // setup the string tokenizer for the 
      // filter params.
      tokenizer = new StringTokenizer( filterKeys, ";" );
      
      // parse the bit map looking for
      // active filter elements
      for ( int i = 0; i < 32; ++i )
      {
        key = ( 1 << i );
      
        if ( ( filterType & key ) == key )
        {
          try
          {
            filterKey = tokenizer.nextToken();
            
            type  = key;
            value = null;
          
            switch( key )
            {
              case FT_TRAN_TYPE:
              case FT_CARD_TYPE:
              case FT_POS_ENTRY_MODE:
                value = filterKey;
                break;
            
              case FT_TRAN_AMOUNT:
                try
                {
                  StringTokenizer amountTokenizer   = new StringTokenizer( filterKey, "^" );
                  String          amountFilterType  = amountTokenizer.nextToken();
                  
                  switch( amountFilterType.charAt(0) )
                  {
                    case '<':
                      type = FT_TRAN_AMOUNT_LESS;
                      break;
                      
                    case '>':
                      type = FT_TRAN_AMOUNT_GREATER;
                      break;
                      
//                    case '=':
                    default:
                      type = FT_TRAN_AMOUNT_EQUAL;
                      break;
                  }
                  value = amountTokenizer.nextToken();
                }
                catch( NoSuchElementException e )
                {
                }
                break;
              
              default:
                break;
            }
          }
          catch( NoSuchElementException e )
          {
          }
          
          if ( value != null )
          {
            FilterElements.addElement( new FilterElement( type, value ) );
          }            
        }              
      }
    }
    catch( Exception e )
    {
    }
  }
  
  public void storeFilter( long hierarchyNode, String name )
  {
    long          filterId      = 0;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:515^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(filter_id,0) 
//          from    tran_filters
//          where   hierarchy_node  = :hierarchyNode and
//                  filter_name     = :name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(filter_id,0)  \n        from    tran_filters\n        where   hierarchy_node  =  :1  and\n                filter_name     =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.TransactionFilter",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setString(2,name);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   filterId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:521^7*/
      
      if ( filterId == 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:525^9*/

//  ************************************************************
//  #sql [Ctx] { select tran_filter_sequence.nextval  from dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select tran_filter_sequence.nextval   from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.TransactionFilter",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   filterId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:528^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:530^9*/

//  ************************************************************
//  #sql [Ctx] { insert into tran_filters
//              ( hierarchy_node, 
//                filter_name, 
//                filter_type, 
//                filter_keys, 
//                filter_id 
//              )
//            values
//              ( :hierarchyNode, 
//                :name,
//                :getFilterType(), 
//                :getFilterKeys(),
//                :filterId
//               )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1302 = getFilterType();
 String __sJT_1303 = getFilterKeys();
   String theSqlTS = "insert into tran_filters\n            ( hierarchy_node, \n              filter_name, \n              filter_type, \n              filter_keys, \n              filter_id \n            )\n          values\n            (  :1 , \n               :2 ,\n               :3 , \n               :4 ,\n               :5 \n             )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.reports.TransactionFilter",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   __sJT_st.setString(2,name);
   __sJT_st.setInt(3,__sJT_1302);
   __sJT_st.setString(4,__sJT_1303);
   __sJT_st.setLong(5,filterId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:546^9*/
      }
      else  //update
      {
        /*@lineinfo:generated-code*//*@lineinfo:550^9*/

//  ************************************************************
//  #sql [Ctx] { update tran_filters
//            set filter_type = :getFilterType(),
//                filter_keys = :getFilterKeys()
//            where filter_id = :filterId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1304 = getFilterType();
 String __sJT_1305 = getFilterKeys();
   String theSqlTS = "update tran_filters\n          set filter_type =  :1 ,\n              filter_keys =  :2 \n          where filter_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.reports.TransactionFilter",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_1304);
   __sJT_st.setString(2,__sJT_1305);
   __sJT_st.setLong(3,filterId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:556^9*/
      }        
      /*@lineinfo:generated-code*//*@lineinfo:558^7*/

//  ************************************************************
//  #sql [Ctx] { commit 
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:561^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "storeFilter(" + hierarchyNode + ",'" + name + "')", e.toString() );
    }
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/