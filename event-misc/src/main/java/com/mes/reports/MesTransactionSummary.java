/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MesTransactionSummary.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-11-04 12:37:22 -0700 (Thu, 04 Nov 2010) $
  Version            : $Revision: 18072 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

// SQL Imports
import java.sql.Date;
import java.sql.ResultSet;
import com.mes.constants.mesConstants;

public class MesTransactionSummary
{
  // read-only values
  
  // valid card types indicating single cards
  public  static final int      CARD_TYPE_VISA            = 0;
  public  static final int      CARD_TYPE_MC              = 1;
  public  static final int      CARD_TYPE_DEBT            = 2;
  public  static final int      CARD_TYPE_BML             = 3;
  public  static final int      CARD_TYPE_AMEX            = 4;
  public  static final int      CARD_TYPE_DISC            = 5;
  public  static final int      CARD_TYPE_DINR            = 6;
  public  static final int      CARD_TYPE_JCB             = 7;
  public  static final int      CARD_TYPE_PL              = 8;
  public  static final int      CARD_TYPE_COUNT           = 9;
  
  // special types for calling out "groups" of transactions
  public  static final int      CARD_TYPE_BANK            = CARD_TYPE_COUNT;
  public  static final int      CARD_TYPE_NON_BANK        = CARD_TYPE_COUNT + 1;
  public  static final int      CARD_TYPE_ALL             = CARD_TYPE_COUNT + 2;

  // transaction types  
  public  static final int      TRAN_TYPE_PURCHASE        = 0;
  public  static final int      TRAN_TYPE_CREDIT          = 1;
  public  static final int      TRAN_TYPE_COUNT           = 2;
  public  static final int      TRAN_TYPE_ALL             = TRAN_TYPE_COUNT;
  
  // settlement types types  
  public  static final int      ST_ACQUIRER               = 0;
  public  static final int      ST_ISSUER                 = 1;
  public  static final int      ST_COUNT                  = 2;
  
    // strings found in the database column
  private static final String[]       CardTypeDbStrings   = { 
                                                          "VS",     // Visa
                                                          "MC",     // Mastercard
                                                          "DB",     // Debit
                                                          "BL",     // BML
                                                          "AM",     // Amex
                                                          "DS",     // Discover
                                                          "DC",     // Diners
                                                          "JC",     // JCB
                                                          "PL",     // Private Label
                                                          "BANK",   // settled by MES
                                                          "NONBANK",// settled by issuer
                                                          "ALL",    // all cards
                                                        };
                                                        
  private static final String[]       CardTypeSqlSets   = { 
                                                          "('VS','VB','VD','MC','MB','MD','DB','EB','BL')",  // Banks cards
                                                          "('AM','DS','DC','JC','PL')",      // non-banks
                                                          };
                                                        
    // strings displayed though the UI
  private static final String[]       CardTypeLongStrings = { 
                                                          "Visa",             // Visa
                                                          "Mastercard",       // Mastercard
                                                          "Debit/EBT",        // Debit
                                                          "Bill Me Later",    // BML
                                                          "American Express", // Amex
                                                          "Discover",         // Discover
                                                          "Diners Club",      // Diners
                                                          "JCB",              // JCB
                                                          "Private Label",    // Private Label Cards
                                                          "Settled to Your Account",   // MES Settled
                                                          "Settled by Issuer",// Issuer Settled 
                                                          "All Cards",        // ALL
                                                        };
    // transaction types found in the database column
  private static final String[]       TranTypeDbStrings = { 
                                                          "D",     // Purchase
                                                          "C",     // Credit
                                                          "ALL",   // Credits and Purchases
                                                        };
                                                        
    // transaction types displayed in the UI
  private static final String[]       TranTypeLongStrings = { 
                                                          "Purchases",   // Purchase
                                                          "Credits",     // Credit
                                                          "All Trans",   // Credits and Purchases
                                                        };
         
  // offsets into the SummaryIndexes value
  public static final int   BANK_INDEX          = 0;
  public static final int   NONBANK_INDEX       = 1;
  
    // define the default index sets.                                                        
  private static final int[] BankIndexesDefault = 
  {
    CARD_TYPE_VISA,
    CARD_TYPE_MC,
    CARD_TYPE_DEBT,
    CARD_TYPE_BML,
  };
  
  private static final int[] NonBankIndexesDefault = 
  {
    CARD_TYPE_AMEX,
    CARD_TYPE_DISC,
    CARD_TYPE_DINR,
    CARD_TYPE_JCB,
    CARD_TYPE_PL,
  };
  
    // define the overloaded UBOC report
  private static final int[] BankIndexesUBOC = 
  {
    CARD_TYPE_VISA,
    CARD_TYPE_MC,
    CARD_TYPE_DEBT,
    CARD_TYPE_JCB,
  };
  
  private static final int[] NonBankIndexesUBOC =
  {
    CARD_TYPE_AMEX,
    CARD_TYPE_DISC,
    CARD_TYPE_DINR,
    CARD_TYPE_PL,
  };
  
  // this child class is used as a simple data structure
  // to hold elements in a lookup table.
  private class BankIndexSet
  {
    public int[]    BankIndexes     = null;
    public int      BankNumber      = 0;
    public int[]    NonBankIndexes  = null;
    
    public BankIndexSet( int bankNum, int[] bankIndexes, int[] nonBankIndexes )
    {
      BankNumber      = bankNum;
      BankIndexes     = bankIndexes;
      NonBankIndexes  = nonBankIndexes;
    }
  };

  // lookup map to match bank numbers to indexes for which
  // card types they settle and which are settle by the issuer.
  private final BankIndexSet[] BankSettlementIndexes =
  {
    new BankIndexSet( mesConstants.BANK_ID_UBOC, BankIndexesUBOC, NonBankIndexesUBOC ),
    // default: BankIndexesDefault, NonBankIndexesDefault
  };

  // instance members
  double            Amount[][][]      = null;
  int               Count [][][]      = null;
  Date              BatchDate         = null;  
  long              BatchNumber       = 0L;
  String            DbaName           = null;
  long              MerchantId        = 0L;
  int[][]           SummaryIndexes    = new int[][] { BankIndexesDefault, NonBankIndexesDefault };
  
  public MesTransactionSummary( int bankNumber, ResultSet resultSet )
    throws java.sql.SQLException
  {
    initialize( bankNumber );
    addData( resultSet );
  }
  
  public MesTransactionSummary( int bankNumber, ResultSet resultSet, boolean advanceFirst )
    throws java.sql.SQLException
  {
    initialize( bankNumber );
    addData( resultSet, advanceFirst );
  }
  
  public void addData( ResultSet resultSet )
    throws java.sql.SQLException
  {
    addData( resultSet, true );
  }
  
  public void addData( ResultSet resultSet, boolean advanceFirst )
    throws java.sql.SQLException
  {
    String        cardType      = null;
    boolean       hasRecords    = true;     // assume there are records
    double        signValue     = 1.0;
    int           st            = -1;
    int           tt            = -1;
    
    // must advance the ResultSet to the first row
    if ( advanceFirst == true )           
    {
      // if this fails, then no records present
      hasRecords = resultSet.next();  
    }
  
    // load the entries into the data structure
    do
    {
      if ( hasRecords == false )
      {
        // just exit the while loop, there are no records
        break;    
      }
      
      // first time into loop for this object so 
      // intialize the batch number from the results
      if ( BatchNumber == 0L )  
      {
        BatchDate   = resultSet.getDate("batch_date");
        BatchNumber = resultSet.getLong("batch_number");
      }
      else if ( (resultSet.getInt("by_batch") == 1) && 
                ( !BatchDate.equals( resultSet.getDate("batch_date") ) ||
                  (MerchantId != resultSet.getLong("merchant_number")) ||
                  (BatchNumber != resultSet.getLong("batch_number")) ) )
      {
        // results are separated by batch.  there should
        // only be one summary entry per batch, so we
        // are done processing this batch, exit loop.
        break;
      }
      
      // only load the name and merchant id if they have
      // not been loaded by a previous call to addData
      if ( DbaName == null )
      {
        DbaName     = resultSet.getString("dba_name");
        MerchantId  = resultSet.getLong("merchant_number");
      }
 
      cardType  = resultSet.getString("card_type");
      tt        = resultSet.getInt("tran_type");  // 0 = sales, 1 = credits
      st        = resultSet.getInt("settled_by_issuer"); // 0 = acq, 1 = issuer
      
      // bundle EBT transactions with debit
      if ( cardType.equals("EB") )
      {
        cardType = "DB";
      }
      
      // lump all visa together (VS, VB, VD) 
      if ( cardType.substring(0,1).equals("V") )
      {
        cardType = "VS";
      } 
      // lump all mastercard together (MC, MB, MD) 
      if ( cardType.substring(0,1).equals("M") )
      {
        cardType = "MC";
      } 
      
      for ( int ct = 0; ct < CARD_TYPE_COUNT; ++ct )
      {
        if ( cardType.equals( CardTypeDbStrings[ct] ) )
        {
          // by settlment type (st), card type (ct), tran type (tt)
          Amount[st][ct][tt] += resultSet.getDouble( "item_amount_signed" );
          Count[st][ct][tt]  += resultSet.getInt( "item_count" );
          break;
        }
      }
    }
    while ( resultSet.next() );
  }

  public Date getBatchDate( )
  {
    return( BatchDate );
  }
  
  public long getBatchNumber( )
  {
    return( BatchNumber );
  }
  
  public int getCardTypeCountBank( )
  {
    int     retVal    = 4;    // always visa, mc, debit, BML

    /*    
    for( int ct = CARD_TYPE_AMEX; ct < CARD_TYPE_COUNT; ++ct )
    {
      // number of rows in table is now defined by card acceptance flag, not whether there is actually data in the row
      if ( false && getTotalCount(ct) > 0 )
      {
        retVal++;
      }
    }
    */
    
    return( retVal );
  }
  
  public int getCardTypeCountNonBank( )
  {
    int     retVal    = 0;
    
    for( int ct = CARD_TYPE_AMEX; ct < CARD_TYPE_COUNT; ++ct )
    {
      // has volume or not included above
      if ( (getIssuerTotalCount(ct) > 0) || 
           (ct != CARD_TYPE_PL && getTotalCount(ct) == 0) )
      {
        retVal++;
      }
    }
    
    return( retVal );
  }
  
  public static String getCardTypeDbString( int cardType )
  {
    String        retVal = "ALL";   // default is all card types
    
    try
    {
      retVal = CardTypeDbStrings[ cardType ];
    }
    catch( IndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }
  
  public static String getCardTypeString( int cardType )
  {
    String        retVal = "Invalid";     // default is invalid
    
    try
    {
      retVal = CardTypeLongStrings[ cardType ];
    }
    catch( IndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }
  
  public static int getCardTypeIndex( String cardType )
  {
    int         index;
    
    for ( index = 0; index < CardTypeDbStrings.length; ++index )
    {
      if ( cardType.equals( CardTypeDbStrings[ index ] ) )
      {
        break;
      }
    }
    return( index );
  }
  
  public double getCreditAmount( int cardType )
  {
    double  retVal = 0.0;
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = Amount[ST_ACQUIRER][cardType][TRAN_TYPE_CREDIT];
    }
    return( retVal );
  }
  
  public int getCreditCount( int cardType )
  {
    int     retVal = -1;
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = Count[ST_ACQUIRER][cardType][TRAN_TYPE_CREDIT];
    }
    return( retVal );
  }
  
  public String getDbaName( )
  {
    return( DbaName );
  }
  
  public double getIssuerCreditAmount( int cardType )
  {
    double  retVal = 0.0;
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = Amount[ST_ISSUER][cardType][TRAN_TYPE_CREDIT];
    }
    return( retVal );
  }
  
  public int getIssuerCreditCount( int cardType )
  {
    int     retVal = -1;
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = Count[ST_ISSUER][cardType][TRAN_TYPE_CREDIT];
    }
    return( retVal );
  }

  public double getIssuerPurchaseAmount( int cardType )
  {
    double     retVal = 0.0;
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = Amount[ST_ISSUER][cardType][TRAN_TYPE_PURCHASE];
    }
    return( retVal );
  }
  
  public int getIssuerPurchaseCount( int cardType )
  {
    int     retVal = -1;
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = Count[ST_ISSUER][cardType][TRAN_TYPE_PURCHASE];
    }
    return( retVal );
  }

  public double getIssuerTotalAmount( int cardType )
  {
    double  retVal = 0.0;
    
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = ( Amount[ST_ISSUER][cardType][TRAN_TYPE_PURCHASE] + 
                 Amount[ST_ISSUER][cardType][TRAN_TYPE_CREDIT] );
    }
    else if ( cardType == CARD_TYPE_ALL )
    {
      for ( int i = 0; i < CARD_TYPE_COUNT; ++i )
      {
        retVal += ( Amount[ST_ISSUER][i][TRAN_TYPE_PURCHASE] + 
                    Amount[ST_ISSUER][i][TRAN_TYPE_CREDIT] );
      }
    }
    
    return( retVal );
  }
  
  public int getIssuerTotalCount( int cardType )
  {
    int     retVal = -1;
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = ( Count[ST_ISSUER][cardType][TRAN_TYPE_PURCHASE] + 
                 Count[ST_ISSUER][cardType][TRAN_TYPE_CREDIT] );
    }
    else if ( cardType == CARD_TYPE_ALL )
    {
      for ( int i = 0; i < CARD_TYPE_COUNT; ++i )
      {
        retVal += ( Count[ST_ISSUER][i][TRAN_TYPE_PURCHASE] + 
                    Count[ST_ISSUER][i][TRAN_TYPE_CREDIT] );
      }
    }
    return( retVal );
  }
  
  public long getMerchantId( )
  {
    return( MerchantId );
  }

  public double getPurchaseAmount( int cardType )
  {
    double     retVal = 0.0;
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = Amount[ST_ACQUIRER][cardType][TRAN_TYPE_PURCHASE];
    }
    return( retVal );
  }
  
  public int getPurchaseCount( int cardType )
  {
    int     retVal = -1;
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = Count[ST_ACQUIRER][cardType][TRAN_TYPE_PURCHASE];
    }
    return( retVal );
  }

  public double getTotalAmount( int cardType )
  {
    double  retVal = 0.0;
    
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = ( Amount[ST_ACQUIRER][cardType][TRAN_TYPE_PURCHASE] + 
                 Amount[ST_ACQUIRER][cardType][TRAN_TYPE_CREDIT] );
    }
    else if ( cardType == CARD_TYPE_ALL )
    {
      for ( int i = 0; i < CARD_TYPE_COUNT; ++i )
      {
        retVal += ( Amount[ST_ACQUIRER][i][TRAN_TYPE_PURCHASE] + 
                    Amount[ST_ACQUIRER][i][TRAN_TYPE_CREDIT] );
      }
    }
    
    return( retVal );
  }
  
  public int getTotalCount( int cardType )
  {
    int     retVal = -1;
    if ( cardType < CARD_TYPE_COUNT )
    {
      retVal = ( Count[ST_ACQUIRER][cardType][TRAN_TYPE_PURCHASE] + 
                 Count[ST_ACQUIRER][cardType][TRAN_TYPE_CREDIT] );
    }
    else if ( cardType == CARD_TYPE_ALL )
    {
      for ( int i = 0; i < CARD_TYPE_COUNT; ++i )
      {
        retVal += ( Count[ST_ACQUIRER][i][TRAN_TYPE_PURCHASE] + 
                    Count[ST_ACQUIRER][i][TRAN_TYPE_CREDIT] );
      }
    }
    return( retVal );
  }
  
  public static String getTranTypeDbString( int tranType )
  {
    String        retVal = "ALL";   // default is all transaction types
    
    try
    {
      retVal = TranTypeDbStrings[ tranType ];
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }
  
  public static String getTranTypeString( int tranType )
  {
    String        retVal = "All Transactions";   // default is all transaction types
    
    try
    {
      retVal = TranTypeLongStrings[ tranType ];
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }
  
  public static int getTranTypeIndex( String tranTypeString )
  {
    int     i;
    
    for ( i = 0; i < TranTypeDbStrings.length; ++i )
    {
      if ( tranTypeString.equals( TranTypeDbStrings[i] ) )
      {
        break;
      }
    }
    return( i );
  }
  
  private void initialize( int bankNumber )
  {
    Amount = new double[ ST_COUNT][ CARD_TYPE_COUNT ][ TRAN_TYPE_COUNT ];
    Count  = new int   [ ST_COUNT][ CARD_TYPE_COUNT ][ TRAN_TYPE_COUNT ];
    
    // initialize the structures
    for ( int ct = 0; ct < CARD_TYPE_COUNT; ++ct ) 
    {
      for ( int tt = 0; tt < TRAN_TYPE_COUNT; ++tt )
      {
        for( int st = 0; st < ST_COUNT; ++st )
        {
          Amount[st][ct][tt] = 0.0;
          Count[st][ct][tt]  = 0;
        }          
      }        
    }
  }
  
  public static boolean isCardTypeGroup( String dbCardType )
  {
    boolean     retVal = false;
    
    for ( int i = 0; i < CardTypeDbStrings.length; ++i )
    {
      try
      {
        if ( dbCardType.equals( CardTypeDbStrings[i] ) )
        {
          retVal = ( i >= CARD_TYPE_COUNT );
          break;
        }
      }
      catch( NullPointerException e )
      {
        break;
      }
    }
    return( retVal );
  }
}
