/*@lineinfo:filename=AllRejectDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AllRejectDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-09-12 14:42:06 -0700 (Wed, 12 Sep 2007) $
  Version            : $Revision: 14136 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class AllRejectDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public    String    RejectDate        = null;
    public    String    MerchantId        = null;
    public    String    MerchantName      = null;
    public    String    CardType          = null;
    public    String    CardNumber        = null;
    public    String    CardNumberEnc     = null;
    public    String    ReferenceNumber   = null;
    public    String    PurchaseId        = null;
    public    String    AuthNumber        = null;
    public    String    EntryMode         = null;
    public    String    Description       = null;
    public    String    AuthAmountString  = null;
    public    double    AuthAmount        = 0.0;
    public    String    AmountString      = null;
    public    double    Amount            = 0.0;
    
    public RowData( ResultSet resultSet)
      throws java.sql.SQLException
    {
      RejectDate        = processString(DateTimeFormatter.getFormattedDate(resultSet.getDate("tran_date"), "MM/dd/yyyy"));
      MerchantId        = processString(resultSet.getString("merchant_number"));
      MerchantName      = processString(resultSet.getString("merchant_name"));
      CardType          = processString(resultSet.getString("card_type"));
      CardNumber        = processString(resultSet.getString("card_number"));
      ReferenceNumber   = processString(resultSet.getString("ref_num"));
      PurchaseId        = processString(resultSet.getString("purchase_id"));
      AuthNumber        = processString(resultSet.getString("auth_code"));
      EntryMode         = processString(resultSet.getString("pos_entry_mode"));
      Description       = processString(resultSet.getString("reason_desc"));
      AuthAmount        = resultSet.getDouble("auth_amount");
      AuthAmountString  = MesMath.toCurrency(resultSet.getDouble("auth_amount"));
      Amount            = resultSet.getDouble("tran_amount");
      AmountString      = MesMath.toCurrency(resultSet.getDouble("tran_amount"));
      CardNumberEnc     = resultSet.getString("card_number_enc");
    }
  }
  
  public AllRejectDataBean()
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Reject Date\",");
    line.append("\"Merchant Number\",");
    line.append("\"Merchant Name\",");
    line.append("\"Card Type\",");
    line.append("\"Card Number\",");
    line.append("\"Reference Number\",");
    line.append("\"Purchase ID\",");
    line.append("\"Auth Code\",");
    line.append("\"Entry Mode\",");
    line.append("\"Description\",");
    line.append("\"Auth Amount\",");
    line.append("\"Amount\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.RejectDate );
    line.append( ",\"" );
    line.append( record.MerchantId );
    line.append( "\",\"" );
    line.append( record.MerchantName );
    line.append( "\",\"" );
    line.append( record.CardType );
    line.append( "\",\"" );
    line.append( record.CardNumber );
    line.append( "\",\"" );
    line.append( record.ReferenceNumber );
    line.append( "\",\"" );
    line.append( record.PurchaseId );
    line.append( "\",\"" );
    line.append( record.AuthNumber );
    line.append( "\",\"" );
    line.append( record.EntryMode );
    line.append( "\",\"" );
    line.append( record.Description );
    line.append( "\"," );
    line.append( record.AuthAmount );
    line.append( ",");
    line.append( record.Amount );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_all_rejects_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    int                           bankNumber        = 0;
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      bankNumber = getReportBankId();
      
      // default to MES if this is a top-level user
      if(bankNumber == 9999)
      {
        bankNumber = 3941;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:190^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.transaction_date                     tran_date,
//                  decode(mf.merchant_number, null, 'N/A', 
//                    mf.merchant_number)                   merchant_number,
//                  mf.dba_name                             merchant_name,
//                  dt.card_type                            card_type, 
//                  dt.cardholder_account_number            card_number,
//                  dt.card_number_enc                      as card_number_enc,
//                  dt.reference_number                     ref_num,   
//                  ltrim(rtrim(dt.purchase_id))            purchase_id,
//                  dt.authorization_num                    auth_code, 
//                  nvl(pd.pos_entry_desc,
//                      dt.pos_entry_mode )                 pos_entry_mode, 
//                  nvl(rd.reject_reason_desc,
//                      'Reason Code '||dt.reject_reason )  reason_desc,
//                  dt.auth_amt                             auth_amount, 
//                  ( dt.transaction_amount *
//                    decode(dt.debit_credit_indicator,
//                           'C',-1,1) )                    tran_amount
//          from    mif                       mf,
//                  daily_detail_file_dt      dt, 
//                  pos_entry_mode_desc       pd,
//                  reject_reason_desc        rd
//          where   dt.batch_date between :beginDate and :endDate and
//                  dt.reject_reason is not null and
//                  substr(dt.load_filename, 4, 4) = :bankNumber and
//                  dt.merchant_account_number  = mf.merchant_number(+) and
//                  dt.reject_reason = rd.reject_reason(+) and
//                  dt.pos_entry_mode = pd.pos_entry_code(+)
//          order by dt.transaction_date, mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.transaction_date                     tran_date,\n                decode(mf.merchant_number, null, 'N/A', \n                  mf.merchant_number)                   merchant_number,\n                mf.dba_name                             merchant_name,\n                dt.card_type                            card_type, \n                dt.cardholder_account_number            card_number,\n                dt.card_number_enc                      as card_number_enc,\n                dt.reference_number                     ref_num,   \n                ltrim(rtrim(dt.purchase_id))            purchase_id,\n                dt.authorization_num                    auth_code, \n                nvl(pd.pos_entry_desc,\n                    dt.pos_entry_mode )                 pos_entry_mode, \n                nvl(rd.reject_reason_desc,\n                    'Reason Code '||dt.reject_reason )  reason_desc,\n                dt.auth_amt                             auth_amount, \n                ( dt.transaction_amount *\n                  decode(dt.debit_credit_indicator,\n                         'C',-1,1) )                    tran_amount\n        from    mif                       mf,\n                daily_detail_file_dt      dt, \n                pos_entry_mode_desc       pd,\n                reject_reason_desc        rd\n        where   dt.batch_date between  :1  and  :2  and\n                dt.reject_reason is not null and\n                substr(dt.load_filename, 4, 4) =  :3  and\n                dt.merchant_account_number  = mf.merchant_number(+) and\n                dt.reject_reason = rd.reject_reason(+) and\n                dt.pos_entry_mode = pd.pos_entry_code(+)\n        order by dt.transaction_date, mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AllRejectDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AllRejectDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:221^7*/
      
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         inc           = -1;
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, inc );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      
      cal.setTime( ReportDateEnd );
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
  
}/*@lineinfo:generated-code*/