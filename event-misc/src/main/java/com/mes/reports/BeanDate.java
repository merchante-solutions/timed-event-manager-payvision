/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/BeanDate.java $

  Description:  

  Stores a date and time.  Provides methods to access the individual
  date components such as day, year, minute, hour, etc.  Also provides
  accessors that allow getting and setting as java.sql.Date.  Useful 
  for beans that need to convert date components (such as those found 
  on report page forms) to java.sql.Date and java.sql.Time types.

  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 9/20/04 5:07p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BeanDate
{
  // storage
  private Calendar calDate = Calendar.getInstance();
  private boolean dateSet = false;
  
  public BeanDate()
  {
  }
  
  public BeanDate(java.sql.Date sqlDate)
  {
    if (sqlDate != null)
    {
      setSqlDate(sqlDate);
    }
  }
  
  public java.util.Date parse(String dateStr)
  {
    SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    java.util.Date utilDate = null;
    try
    {
      utilDate = df.parse(dateStr);
    }
    catch (Exception e)
    {
      utilDate = new java.util.Date();
    }
    return utilDate;
  }
    
  // calendar components
  public int  getDay   ()           { return calDate.get(Calendar.DATE);    }
  public int  getMonth ()           { return calDate.get(Calendar.MONTH);   }
  public int  getYear  ()           { return calDate.get(Calendar.YEAR);    }
  public int  getHour  ()           { return calDate.get(Calendar.HOUR);    }
  public int  getMinute()           { return calDate.get(Calendar.MINUTE);  }
  public int  getSecond()           { return calDate.get(Calendar.SECOND);  }
  public void setDay   (int newVal) { calDate.set(Calendar.DATE,newVal);   dateSet = true; }
  public void setMonth (int newVal) { calDate.set(Calendar.MONTH,newVal);  dateSet = true; }
  public void setYear  (int newVal) { calDate.set(Calendar.YEAR,newVal);   dateSet = true; }
  public void setHour  (int newVal) { calDate.set(Calendar.HOUR,newVal);   dateSet = true; }
  public void setMinute(int newVal) { calDate.set(Calendar.MINUTE,newVal); dateSet = true; }
  public void setSecond(int newVal) { calDate.set(Calendar.SECOND,newVal); dateSet = true; }
  
  public void add(int field, int amount)
  {
    calDate.add(field,amount);
    dateSet = true;
  }

  // date String
  public String toString()
  {
    return (getMonth() + 1) + "-" + getDay() + "-" + getYear();
  }
  public void setAsString(String newDate)
  {
    calDate.setTime(parse(newDate));
    dateSet = true;
  }
  
  // java.util.Date
  public java.util.Date getUtilDate() 
  {
    return calDate.getTime();
  }
  public void setUtilDate(java.util.Date newDate)
  {
    calDate.setTime(newDate);
    dateSet = true;
  }
  
  // java.sql.Date
  public java.sql.Date getSqlDate()   
  {
    Calendar dateOnly = Calendar.getInstance();
    dateOnly.clear();
    dateOnly.set(Calendar.DATE, calDate.get(Calendar.DATE));
    dateOnly.set(Calendar.MONTH,calDate.get(Calendar.MONTH));
    dateOnly.set(Calendar.YEAR, calDate.get(Calendar.YEAR));
    return new java.sql.Date(dateOnly.getTime().getTime());
  }
  public void setSqlDate(java.sql.Date newDate)
  {
    calDate.setTime(newDate);
    dateSet = true;
  }
  
  // java.sql.Time
  public java.sql.Time getSqlTime()
  {
    Calendar timeOnly = Calendar.getInstance();
    timeOnly.clear();
    timeOnly.set(Calendar.HOUR,  calDate.get(Calendar.HOUR));
    timeOnly.set(Calendar.MINUTE,calDate.get(Calendar.MINUTE));
    timeOnly.set(Calendar.SECOND,calDate.get(Calendar.SECOND));
    return new java.sql.Time(timeOnly.getTime().getTime());
  }
  public void setSqlTime(java.sql.Time newTime)
  {
    calDate.setTime(newTime);
    dateSet = true;
  }  
  
  public boolean isSet()
  {
    return dateSet;
  }
}
