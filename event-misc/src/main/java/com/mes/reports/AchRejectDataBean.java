/*@lineinfo:filename=AchRejectDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AchRejectDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-01 11:46:29 -0700 (Tue, 01 Jul 2008) $
  Version            : $Revision: 15021 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class AchRejectDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public String       Adden1          = null;
    public String       Adden2          = null;
    public String       DbaName         = null;
    public String       Dda             = null;
    public String       Description     = null;
    public String       MerchantId      = null;
    public String       ReasonCode      = null;
    public String       ReportDate      = null;
    public int          TransactionCode = 0;
    public String       AmountString    = null;
    public double       Amount          = 0.0;
  
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      Adden1            = processString(resultSet.getString("adden_1"));
      Adden2            = processString(resultSet.getString("adden_2"));
      DbaName           = processString(resultSet.getString("dba_name"));
      Dda               = processString(resultSet.getString("dda"));
      Description       = processString(resultSet.getString("description"));
      MerchantId        = processString(resultSet.getString("merchant_number"));
      ReasonCode        = processString(resultSet.getString("reason_code"));
      ReportDate        = processString(DateTimeFormatter.getFormattedDate(resultSet.getDate("report_date"), "MM/dd/yyyy"));
      TransactionCode   = resultSet.getInt("transaction_code");
      Amount            = resultSet.getDouble("amount");
      AmountString      = MesMath.toCurrency(resultSet.getDouble("amount"));
    }
  }
  
  public AchRejectDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Reason Code\",");
    line.append("\"DDA Number\",");
    line.append("\"Tran Code\",");
    line.append("\"Acct Number\",");
    line.append("\"DBA\",");
    line.append("\"Description\",");
    line.append("\"Report Date\",");
    line.append("\"Adden_1\",");
    line.append("\"Adden_2\",");
    line.append("\"Amount\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( "\"" );
    line.append( record.ReasonCode );
    line.append( "\",\"" );
    line.append( record.Dda );
    line.append( "\"," );
    line.append( record.TransactionCode );
    line.append( ",\"" );
    line.append( record.MerchantId );
    line.append( "\",\"" );
    line.append( record.DbaName );
    line.append( "\",\"" );
    line.append( record.Description );
    line.append( "\"," );
    line.append( record.ReportDate );
    line.append( ",\"" );
    line.append( record.Adden1 );
    line.append( "\",\"" );
    line.append( record.Adden2 );
    line.append( "\"," );
    line.append( record.Amount );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_ach_rejects_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    int                           bankNumber        = 0;
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      bankNumber = getReportBankIdDefault();
      
      if(bankNumber == 9999)
      {
        // top level node, just show them all ach rejects in the date range
        /*@lineinfo:generated-code*//*@lineinfo:174^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ar.merchant_number    merchant_number,
//                    ar.reason_code        reason_code,
//                    ar.dda                dda,
//                    ar.transaction_code   transaction_code,
//                    ar.merchant_number    merchant_number,
//                    ar.merchant_name      dba_name,
//                    ar.entry_desc         description,
//                    ar.settled_date       report_date,
//                    ar.adden_1            adden_1,
//                    ar.adden_2            adden_2,
//                    ar.amount             amount
//            from    ach_rejects           ar
//            where   ar.settled_date between :beginDate and :endDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ar.merchant_number    merchant_number,\n                  ar.reason_code        reason_code,\n                  ar.dda                dda,\n                  ar.transaction_code   transaction_code,\n                  ar.merchant_number    merchant_number,\n                  ar.merchant_name      dba_name,\n                  ar.entry_desc         description,\n                  ar.settled_date       report_date,\n                  ar.adden_1            adden_1,\n                  ar.adden_2            adden_2,\n                  ar.amount             amount\n          from    ach_rejects           ar\n          where   ar.settled_date between  :1  and  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AchRejectDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AchRejectDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:189^9*/
      }
      else
      {
        // only show rejects for merchants under this node
        /*@lineinfo:generated-code*//*@lineinfo:194^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ar.merchant_number    merchant_number,
//                    ar.reason_code        reason_code,
//                    ar.dda                dda,
//                    ar.transaction_code   transaction_code,
//                    ar.merchant_number    merchant_number,
//                    ar.merchant_name      dba_name,
//                    ar.entry_desc         description,
//                    ar.settled_date       report_date,
//                    ar.adden_1            adden_1,
//                    ar.adden_2            adden_2,
//                    ar.amount             amount
//            from    group_merchant        gm,
//                    group_rep_merchant    grm,
//                    ach_rejects           ar
//            where   gm.org_num = :orgId and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    ar.merchant_number = gm.merchant_number and
//                    ar.settled_date between :beginDate and :endDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ar.merchant_number    merchant_number,\n                  ar.reason_code        reason_code,\n                  ar.dda                dda,\n                  ar.transaction_code   transaction_code,\n                  ar.merchant_number    merchant_number,\n                  ar.merchant_name      dba_name,\n                  ar.entry_desc         description,\n                  ar.settled_date       report_date,\n                  ar.adden_1            adden_1,\n                  ar.adden_2            adden_2,\n                  ar.amount             amount\n          from    group_merchant        gm,\n                  group_rep_merchant    grm,\n                  ach_rejects           ar\n          where   gm.org_num =  :1  and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  ar.merchant_number = gm.merchant_number and\n                  ar.settled_date between  :4  and  :5";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.AchRejectDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.AchRejectDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^9*/
      }
      
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         inc           = -1;
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, inc );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      
      cal.setTime( ReportDateEnd );
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/