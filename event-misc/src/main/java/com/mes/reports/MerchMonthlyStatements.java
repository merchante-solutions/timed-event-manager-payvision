/*@lineinfo:filename=MerchMonthlyStatements*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchMonthlyStatements.sqlj $

  Description:  
  
    MerchMonthlyStatements
    
    Looks up a merchant's monthly statements based on a date range and 
    merchant number.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-03-13 11:54:05 -0700 (Thu, 13 Mar 2008) $
  Version            : $Revision: 14658 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class MerchMonthlyStatements extends OrgSummaryDataBean
{

  /*
  ** public class DetailRow implements Comparable
  **
  ** This internal class represents one row in a statement summary result
  ** set.  It implements Comparable so that it may be used with a TreeSet.
  ** It contains a reference to a SortByType object that can be used to
  ** synchronize the ordering of a set of these rows.
  */
  public class DetailRow extends MesOrgSummaryEntry implements Comparable
  {
    private String          dda;
    private String          trNum;
    private long            recNum;
    private int             bankNumber;
  
    /*
    ** CONSTRUCTOR
    **
    ** public DetailRow(ResultSet rs, SortByType sbt)
    **
    ** Creates a row containing the current column values of a result set.
    ** A reference to a SortByType object is stored as well.
    */
    public DetailRow(ResultSet rs, SortByType sbt)
      throws java.sql.SQLException
    {
      super(rs,sbt);

      dda         = rs.getString("dda");
      trNum       = rs.getString("trans_routing");
      recNum      = rs.getLong  ("rec_num");
      bankNumber  = rs.getInt   ("bank_num");
    }

    /*
    ** public int compareTo(Object that)
    **
    ** Based on the current sbt sort by value a -1, 0 or 1 is returned to
    ** indicate if the relevant column in that object is less than, equal to
    ** or greater than this objects matching column.
    **
    ** RETURNS: -1, 0 or 1 depending of if that object is less than, equal to
    **          or greater than this object.
    */
    public int compareTo(Object that)
    {
      DetailRow thatRow   = (DetailRow)that;
      int       result    = 0;
    
      // handle the items specific to this child
      // class.  Note:  do not "flip" the result
      // of the base class compareTo method.
      switch ( SortOrder.getSortBy() )
      {
        case SB_MS_DDA:
          result = dda.compareTo(thatRow.dda);
          result *= ( SortOrder.isReverseSort() ? -1 : 1 );  // reverse?
          break;
          
        case SB_MS_TR_NUM:
          result = trNum.compareTo(thatRow.trNum);
          result *= ( SortOrder.isReverseSort() ? -1 : 1 );  // reverse?
          break;
          
        default:    // use the base class compare
          result = super.compareTo(that);                     // do not reverse!
          break;
      }
    
      // always sort by recNum as a backup so that fees, discount
      // and other sort fields may be equal and yet still added into
      // the set
      if (result == 0)
      {
        if (recNum < thatRow.recNum)
        {
          result = -1;
        }
        else if (recNum > thatRow.recNum)
        {
          result = 1;
        }
      }
      return( result );
    }
  
    public String getDda()
    {
      return( dda );
    }
    
    public int getBankNumber()
    {
      return( bankNumber );
    }
    
    public String getTrNum()
    {
      return( trNum );
    }
    
    public double getDiscount()
    {
      return( getAmount( MS_DISCOUNT_INDEX ) );
    }
    
    public double getFees()
    {
      return( getAmount( MS_FEES_INDEX ) );
    }
    
    public double getTotal()
    {
      return( getAmount( MS_NET_INDEX ) );
    }
    
    public long getRecNum()
    {
      return( recNum );
    }
    
    public double getVmcSales()
    {
      return( getAmount( MS_VMC_SALES_INDEX ) );
    }
    
    public double getVmcCredits()
    {
      return( getAmount( MS_VMC_CREDITS_INDEX ) );
    }
    
    public double getOtherSales()
    {
      return( getAmount( MS_OTHER_SALES_INDEX ) );
    }
    
    public double getOtherCredits()
    {
      return( getAmount( MS_OTHER_CREDITS_INDEX ) );
    }
    
    public long getYearMonth()
    {
      return( Long.parseLong( DateTimeFormatter.getFormattedDate( EntryDate, "yyyyMM" ) ) );
    }
  }
  
  public MerchMonthlyStatements()
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    if ( ReportType == RT_DETAILS )
    {
      line.append("\"Date\",");
      line.append("\"DDA Number\",");
    }      
    line.append("\"VMC Sales Amt\",");
    line.append("\"VMC Credits Amt\",");
    line.append("\"Other Sales Amt\",");
    line.append("\"Other Credits Amt\",");
    line.append("\"Discount\",");
    line.append("\"Fees\",");
    line.append("\"Total\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    MesOrgSummaryEntry  record    = (MesOrgSummaryEntry)obj;
    
    line.setLength(0);
    line.append( encodeHierarchyNode(record.getHierarchyNode()) );
    line.append( ",\"" );
    line.append( record.getOrgName() );
    line.append( "\"" );
    
    if ( ReportType == RT_DETAILS )
    {
      DetailRow   details = (DetailRow) obj;
      line.append( ",\"" );
      line.append( DateTimeFormatter.getFormattedDate( details.getEntryDate(), "MMM yyyy" ) );
      line.append( "\"," );
      line.append( details.getDda() );
    }
    for( int i = 0; i < MesOrgSummaryEntry.NUM_AMOUNTS; ++i )
    {
      line.append( "," );
      line.append( record.getAmount(i) );
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    
    if ( ReportType == RT_DETAILS )
    {
      filename.append("_stmt_details_");
    }
    else    // RT_SUMMARY
    {
      filename.append("_stmt_summary_");
    }      
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"yyyyMM" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"yyyyMM" ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadDetailData(long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator it          = null;
    ResultSet         resultSet   = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:285^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    o.org_group                             as hierarchy_node,
//                    o.org_name                              as org_name,
//                    o.org_num                               as org_num,
//                    mf.district                             as district,
//                    to_date(ms.year_month,'yyyymm')         as entry_date,
//                    ms.dda                                  as dda,
//                    ms.transit_routing                      as trans_routing,
//                    ms.bank_num                             as bank_num,
//                    sm.rec_num                              as rec_num,
//                    sm.vmc_sales_amount                     as vmc_sales_amount,
//                    sm.vmc_credits_amount                   as vmc_credits_amount,
//                    sm.other_sales_amount                   as other_sales_amount,
//                    sm.other_credits_amount                 as other_credits_amount,
//                    sm.discount                             as discount,
//                    sm.fees                                 as fees,
//                    (sm.discount + sm.fees)                 as total
//          from      organization                o,
//                    mif                         mf,
//                    merch_statements_summary    sm,
//                    merch_statements            ms
//          where     o.org_num           = :orgId and
//                    mf.merchant_number  = o.org_group and
//                    sm.merchant_number  = mf.merchant_number and
//                    sm.statement_date between :beginDate and :endDate and
//                    ms.merch_num        = sm.merchant_number and
//                    ms.rec_num          = sm.rec_num and
//                    ms.year_month       = to_number(to_char(sm.statement_date, 'yyyymm'))        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    o.org_group                             as hierarchy_node,\n                  o.org_name                              as org_name,\n                  o.org_num                               as org_num,\n                  mf.district                             as district,\n                  to_date(ms.year_month,'yyyymm')         as entry_date,\n                  ms.dda                                  as dda,\n                  ms.transit_routing                      as trans_routing,\n                  ms.bank_num                             as bank_num,\n                  sm.rec_num                              as rec_num,\n                  sm.vmc_sales_amount                     as vmc_sales_amount,\n                  sm.vmc_credits_amount                   as vmc_credits_amount,\n                  sm.other_sales_amount                   as other_sales_amount,\n                  sm.other_credits_amount                 as other_credits_amount,\n                  sm.discount                             as discount,\n                  sm.fees                                 as fees,\n                  (sm.discount + sm.fees)                 as total\n        from      organization                o,\n                  mif                         mf,\n                  merch_statements_summary    sm,\n                  merch_statements            ms\n        where     o.org_num           =  :1  and\n                  mf.merchant_number  = o.org_group and\n                  sm.merchant_number  = mf.merchant_number and\n                  sm.statement_date between  :2  and  :3  and\n                  ms.merch_num        = sm.merchant_number and\n                  ms.rec_num          = sm.rec_num and\n                  ms.year_month       = to_number(to_char(sm.statement_date, 'yyyymm'))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchMonthlyStatements",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchMonthlyStatements",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:314^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new DetailRow( resultSet, SortBy ) );
      }
      resultSet.close();
    }
    catch (Exception e)
    {
      logEntry( buildMethodName("loadDetailsData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator   it              = null;

    try
    {
      int bankNum = getReportBankId();
      
      if ( hasAssocDistricts() )
      // returns false unless the current node is an
      // association and it has districts under it.
      {
        if ( District == DISTRICT_NONE )
        {
          if ( IgnoreDistricts == true )
          {
            /*@lineinfo:generated-code*//*@lineinfo:349^13*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ index (gm pkgroup_merchant)
//                              index (ms pk_merch_statement_summary)
//                           */
//                        o.org_num                                   as org_num,
//                        o.org_group                                 as hierarchy_node,
//                        :bankNum                                    as bank_num,
//                        nvl(mf.district,:DISTRICT_UNASSIGNED)       as district,
//                        mf.dba_name                                 as org_name,
//                        sum(ms.vmc_sales_amount)                    as vmc_sales_amount,
//                        sum(ms.vmc_credits_amount)                  as vmc_credits_amount,
//                        sum(ms.other_sales_amount)                  as other_sales_amount,
//                        sum(ms.other_credits_amount)                as other_credits_amount,
//                        sum(ms.discount)                            as discount,
//                        sum(ms.fees)                                as fees,
//                        sum(ms.discount + ms.fees)                  as total
//                from    group_merchant                gm,
//                        mif                           mf,
//                        merch_statements_summary      ms,
//                        organization                  o
//                where   gm.org_num          = :orgId and
//                        mf.merchant_number  = gm.merchant_number and
//                        ms.merchant_number(+)  = gm.merchant_number and                                 
//                        ms.statement_date(+) between :beginDate and last_day(:endDate) and
//                        o.org_group = mf.merchant_number
//                group by  o.org_group, o.org_num, mf.district, mf.dba_name
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ index (gm pkgroup_merchant)\n                            index (ms pk_merch_statement_summary)\n                         */\n                      o.org_num                                   as org_num,\n                      o.org_group                                 as hierarchy_node,\n                       :1                                     as bank_num,\n                      nvl(mf.district, :2 )       as district,\n                      mf.dba_name                                 as org_name,\n                      sum(ms.vmc_sales_amount)                    as vmc_sales_amount,\n                      sum(ms.vmc_credits_amount)                  as vmc_credits_amount,\n                      sum(ms.other_sales_amount)                  as other_sales_amount,\n                      sum(ms.other_credits_amount)                as other_credits_amount,\n                      sum(ms.discount)                            as discount,\n                      sum(ms.fees)                                as fees,\n                      sum(ms.discount + ms.fees)                  as total\n              from    group_merchant                gm,\n                      mif                           mf,\n                      merch_statements_summary      ms,\n                      organization                  o\n              where   gm.org_num          =  :3  and\n                      mf.merchant_number  = gm.merchant_number and\n                      ms.merchant_number(+)  = gm.merchant_number and                                 \n                      ms.statement_date(+) between  :4  and last_day( :5 ) and\n                      o.org_group = mf.merchant_number\n              group by  o.org_group, o.org_num, mf.district, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchMonthlyStatements",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNum);
   __sJT_st.setInt(2,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(3,orgId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchMonthlyStatements",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:376^13*/
          }
          else      // group association data by district
          {
            /*@lineinfo:generated-code*//*@lineinfo:380^13*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ index (gm pkgroup_merchant)
//                              index (ms pk_merch_statement_summary)
//                           */
//                        o.org_num                                   as org_num,
//                        o.org_group                                 as hierarchy_node,
//                        :bankNum                                    as bank_num,
//                        nvl(mf.district,:DISTRICT_UNASSIGNED)       as district,
//                        nvl(ad.district_desc,decode( mf.district,
//                                      null,'Unassigned',
//                                     ('District ' || to_char(mf.district, '0009')))) as org_name,
//                        sum(ms.vmc_sales_amount)                    as vmc_sales_amount,
//                        sum(ms.vmc_credits_amount)                  as vmc_credits_amount,
//                        sum(ms.other_sales_amount)                  as other_sales_amount,
//                        sum(ms.other_credits_amount)                as other_credits_amount,
//                        sum(ms.discount)                            as discount,
//                        sum(ms.fees)                                as fees,
//                        sum(ms.discount + ms.fees)                  as total
//                from    organization                  o,
//                        group_merchant                gm,
//                        mif                           mf,
//                        merch_statements_summary      ms,
//                        assoc_districts               ad
//                where   o.org_num           = :orgId and
//                        gm.org_num          = o.org_num and
//                        mf.merchant_number  = gm.merchant_number and
//                        ms.merchant_number(+)  = gm.merchant_number and                                 
//                        ms.statement_date(+) between :beginDate and last_day(:endDate) and
//                        ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and
//                        ad.district(+)      = mf.district
//                group by  o.org_group, 
//                          o.org_num,
//                          mf.district,
//                          ad.district_desc
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ index (gm pkgroup_merchant)\n                            index (ms pk_merch_statement_summary)\n                         */\n                      o.org_num                                   as org_num,\n                      o.org_group                                 as hierarchy_node,\n                       :1                                     as bank_num,\n                      nvl(mf.district, :2 )       as district,\n                      nvl(ad.district_desc,decode( mf.district,\n                                    null,'Unassigned',\n                                   ('District ' || to_char(mf.district, '0009')))) as org_name,\n                      sum(ms.vmc_sales_amount)                    as vmc_sales_amount,\n                      sum(ms.vmc_credits_amount)                  as vmc_credits_amount,\n                      sum(ms.other_sales_amount)                  as other_sales_amount,\n                      sum(ms.other_credits_amount)                as other_credits_amount,\n                      sum(ms.discount)                            as discount,\n                      sum(ms.fees)                                as fees,\n                      sum(ms.discount + ms.fees)                  as total\n              from    organization                  o,\n                      group_merchant                gm,\n                      mif                           mf,\n                      merch_statements_summary      ms,\n                      assoc_districts               ad\n              where   o.org_num           =  :3  and\n                      gm.org_num          = o.org_num and\n                      mf.merchant_number  = gm.merchant_number and\n                      ms.merchant_number(+)  = gm.merchant_number and                                 \n                      ms.statement_date(+) between  :4  and last_day( :5 ) and\n                      ad.assoc_number(+)  = (mf.bank_number || mf.dmagent) and\n                      ad.district(+)      = mf.district\n              group by  o.org_group, \n                        o.org_num,\n                        mf.district,\n                        ad.district_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchMonthlyStatements",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNum);
   __sJT_st.setInt(2,DISTRICT_UNASSIGNED);
   __sJT_st.setLong(3,orgId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchMonthlyStatements",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:415^13*/
          }
        }
        else    // a district was specified
        {
          /*@lineinfo:generated-code*//*@lineinfo:420^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (gm pkgroup_merchant)
//                          index (ms pk_merch_statement_summary)
//                       */
//                      o.org_num                     as org_num,
//                      o.org_group                   as hierarchy_node,
//                      o.org_name                    as org_name,
//                      :bankNum                      as bank_num,
//                      :District                     as district,
//                      sum(ms.vmc_sales_amount)      as vmc_sales_amount,
//                      sum(ms.vmc_credits_amount)    as vmc_credits_amount,
//                      sum(ms.other_sales_amount)    as other_sales_amount,
//                      sum(ms.other_credits_amount)  as other_credits_amount,
//                      sum(ms.discount)              as discount,
//                      sum(ms.fees)                  as fees,
//                      sum(ms.discount + ms.fees)    as total
//              from    group_merchant              gm,
//                      mif                         mf,
//                      merch_statements_summary    ms,
//                      organization                o
//              where   gm.org_num          = :orgId and
//                      mf.merchant_number  = gm.merchant_number and
//                      nvl(mf.district,-1) = :District and
//                      ms.merchant_number(+)  = gm.merchant_number and                                 
//                      ms.statement_date(+) between :beginDate and last_day(:endDate) and
//                      o.org_group = mf.merchant_number
//              group by  o.org_num,o.org_group,o.org_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (gm pkgroup_merchant)\n                        index (ms pk_merch_statement_summary)\n                     */\n                    o.org_num                     as org_num,\n                    o.org_group                   as hierarchy_node,\n                    o.org_name                    as org_name,\n                     :1                       as bank_num,\n                     :2                      as district,\n                    sum(ms.vmc_sales_amount)      as vmc_sales_amount,\n                    sum(ms.vmc_credits_amount)    as vmc_credits_amount,\n                    sum(ms.other_sales_amount)    as other_sales_amount,\n                    sum(ms.other_credits_amount)  as other_credits_amount,\n                    sum(ms.discount)              as discount,\n                    sum(ms.fees)                  as fees,\n                    sum(ms.discount + ms.fees)    as total\n            from    group_merchant              gm,\n                    mif                         mf,\n                    merch_statements_summary    ms,\n                    organization                o\n            where   gm.org_num          =  :3  and\n                    mf.merchant_number  = gm.merchant_number and\n                    nvl(mf.district,-1) =  :4  and\n                    ms.merchant_number(+)  = gm.merchant_number and                                 \n                    ms.statement_date(+) between  :5  and last_day( :6 ) and\n                    o.org_group = mf.merchant_number\n            group by  o.org_num,o.org_group,o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchMonthlyStatements",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNum);
   __sJT_st.setInt(2,District);
   __sJT_st.setLong(3,orgId);
   __sJT_st.setInt(4,District);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchMonthlyStatements",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:448^11*/
        }
      }
      else // just standard child report, no districts
      {
        /*@lineinfo:generated-code*//*@lineinfo:453^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (gm pkgroup_merchant)
//                        index (ms pk_merch_statement_summary)
//                        ordered
//                        use_nl(gm ms)
//                     */
//                    o.org_num                     as org_num,
//                    o.org_group                   as hierarchy_node,
//                    o.org_name                    as org_name,
//                    :bankNum                      as bank_num,
//                    0                             as district,
//                    sum(ms.vmc_sales_amount)      as vmc_sales_amount,
//                    sum(ms.vmc_credits_amount)    as vmc_credits_amount,
//                    sum(ms.other_sales_amount)    as other_sales_amount,
//                    sum(ms.other_credits_amount)  as other_credits_amount,
//                    sum(ms.discount)              as discount,
//                    sum(ms.fees)                  as fees,
//                    sum(ms.discount + ms.fees)    as total
//            from    parent_org                  po,
//                    organization                o,
//                    group_merchant              gm,
//                    merch_statements_summary    ms                  
//            where   po.parent_org_num   = :orgId and
//                    o.org_num           = po.org_num and
//                    gm.org_num          = o.org_num and                                
//                    ms.merchant_number(+)  = gm.merchant_number and                                 
//                    ms.statement_date(+) between :beginDate and last_day(:endDate)
//            group by  o.org_num, o.org_group, o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (gm pkgroup_merchant)\n                      index (ms pk_merch_statement_summary)\n                      ordered\n                      use_nl(gm ms)\n                   */\n                  o.org_num                     as org_num,\n                  o.org_group                   as hierarchy_node,\n                  o.org_name                    as org_name,\n                   :1                       as bank_num,\n                  0                             as district,\n                  sum(ms.vmc_sales_amount)      as vmc_sales_amount,\n                  sum(ms.vmc_credits_amount)    as vmc_credits_amount,\n                  sum(ms.other_sales_amount)    as other_sales_amount,\n                  sum(ms.other_credits_amount)  as other_credits_amount,\n                  sum(ms.discount)              as discount,\n                  sum(ms.fees)                  as fees,\n                  sum(ms.discount + ms.fees)    as total\n          from    parent_org                  po,\n                  organization                o,\n                  group_merchant              gm,\n                  merch_statements_summary    ms                  \n          where   po.parent_org_num   =  :2  and\n                  o.org_num           = po.org_num and\n                  gm.org_num          = o.org_num and                                \n                  ms.merchant_number(+)  = gm.merchant_number and                                 \n                  ms.statement_date(+) between  :3  and last_day( :4 )\n          group by  o.org_num, o.org_group, o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchMonthlyStatements",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNum);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchMonthlyStatements",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:482^9*/
      }
      processSummaryData(it.getResultSet());
    }
    catch (Exception e)
    {
      logEntry( buildMethodName("loadSummaryData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    Calendar    cal           = Calendar.getInstance();
    
    // load the default report properties
    super.setProperties( request );
    
    // if the from date is today, then set the default to rolling previous week
    if ( usingDefaultReportDates() )
    {
      // setup the default date range
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.MONTH, -1 );
      
      // since this report is for one month at a time,
      // make sure that the from date always starts with
      // the first day of the specified month.
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      // set both the begin and end dates
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
    }
    
    // since this report is for one month at a time,
    // make sure that the from date always starts with
    // the first day of the specified month.
    cal.setTime( ReportDateBegin );
    if ( cal.get( Calendar.DAY_OF_MONTH ) != 1 )
    {
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }
    
    cal.setTime( ReportDateEnd );
    if ( cal.get( Calendar.DAY_OF_MONTH ) != 1 )
    {
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
    }
  }
}/*@lineinfo:generated-code*/