/*@lineinfo:filename=DiscoverEquipmentDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/DiscoverEquipmentDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 10/16/03 2:07p $
  Version            : $Revision: 18 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.ops.QueueConstants;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class DiscoverEquipmentDataBean extends ReportSQLJBean
{
  protected static final int   NAME_LAST         = 0;
  protected static final int   NAME_FIRST        = 1;
  protected static final int   NAME_COUNT        = 2;
  
  protected static final int   ADDR_LINE1        = 0;
  protected static final int   ADDR_LINE2        = 1;
  protected static final int   ADDR_CITY         = 2;
  protected static final int   ADDR_STATE        = 3;
  protected static final int   ADDR_ZIP          = 4;
  protected static final int   ADDR_FIELD_COUNT  = 5;

  private static final String[]   RepNameFields = 
  {
    "last_name",        // NAME_LAST
    "first_name",       // NAME_FIRST
  };
  
  private static final String[]   AddressFields = 
  {
    "address1",         // ADDR_LINE1
    "address2",         // ADDR_LINE2
    "city",             // ADDR_CITY
    "state",            // ADDR_STATE
    "zip_code",         // ADDR_ZIP
  };

  public class RowData implements Comparable
  {
    public Date       ActiveDate          = null;
    public double     AnnualSales         = 0.0;
    public double     BaseCost            = 0.0;
    public String     CardsAccepted       = null;
    public String     DataSource          = null;
    public Date       DateActivated       = null;
    public Date       DateApproved        = null;
    public Date       DateOpened          = null;
    public Date       DateShipped         = null;
    public String     DbaName             = null;
    public long       DiscoverMerchantId  = 0L;
    public String     DiscoverDisposition = null;
    public String     DiscoverStatus      = null;
    public String     EmployeeId          = null;
    public int        EquipCount          = 0;
    public String     EquipDesc           = null;
    public String     FinanceOption       = null;
    public String     NewEquipment        = null;
    public boolean    NewMerchant         = false;
    public String[]   MailingAddress      = new String[ADDR_FIELD_COUNT];
    public long       MerchantId          = 0L;
    public String     MerchantPhone       = null;
    public String     MesPartNumber       = null;
    public String     ParentName          = null;
    public long       ParentNode          = 0L;
    public long       ParentOrgId         = 0L;
    public String     PartNumber          = null;
    public String[]   RepName             = new String[NAME_COUNT];
    public String     SerialNumber        = null;
    
    public RowData(  ResultSet resultSet )
    {
      int               offset    = 0;
      String            repName   = null;
      
      try
      {
        ActiveDate          = resultSet.getDate("active_date");
        AnnualSales         = resultSet.getDouble("annual_sales");
        CardsAccepted       = resultSet.getString("cards_accepted");
        DateApproved        = resultSet.getDate("date_approved");
        DateOpened          = resultSet.getDate("date_opened");
        DateActivated       = resultSet.getDate("date_activated");
        DateShipped         = resultSet.getDate("date_shipped");
        DiscoverMerchantId  = resultSet.getLong("discover_merchant_number");
        DiscoverDisposition = resultSet.getString("discover_disposition");
        DiscoverStatus      = resultSet.getString("discover_status");
        EmployeeId          = resultSet.getString("ext_rep_id");
        FinanceOption       = resultSet.getString("finance_option");
        ParentName          = resultSet.getString("org_name");
        ParentNode          = resultSet.getLong("node");
        ParentOrgId         = resultSet.getLong("org_num");
        MerchantId          = resultSet.getLong("merchant_number");
        MerchantPhone       = resultSet.getString("merchant_phone");
        DbaName             = resultSet.getString("dba_name");
        NewMerchant         = resultSet.getString("new_merchant").equals("Y");
        NewEquipment        = resultSet.getString("new_equipment");
        PartNumber          = resultSet.getString("part_number");
        BaseCost            = resultSet.getDouble("base_cost");
        DataSource          = resultSet.getString("data_source");
        EquipCount          = 1;
        EquipDesc           = resultSet.getString("equip_desc");
        MesPartNumber       = resultSet.getString("mes_part_number");
        
        // only care about the serial numbers when 
        // storing the full details (Fixed format report)
        if ( ReportType == RT_DETAILS )
        {
          SerialNumber        = resultSet.getString("serial_number");
        }          
        
        repName             = resultSet.getString("rep_name");
        if ( repName == null )
        {
          RepName[NAME_FIRST] = null;
          RepName[NAME_LAST]  = "Unknown";
        }
        else if ( (offset = repName.indexOf(' ')) >= 0 )
        {
          RepName[NAME_FIRST] = repName.substring(0,(repName.lastIndexOf(' ')));
          RepName[NAME_LAST]  = repName.substring((repName.lastIndexOf(' ')));
        }
        else    // no space
        {
          RepName[NAME_FIRST] = null;
          RepName[NAME_LAST]  = repName;
        }
        for( int i = 0; i < AddressFields.length; ++i )
        {
          MailingAddress[i] = resultSet.getString(AddressFields[i]);
        }
        
        // discover wants all dial pay to be set to "U"
        // because they are paying their reps upgrade 
        // commission on all dial pay accounts.
        if ( DiscoverDisposition.equals("?") )
        {
          try
          {
            String edesc = EquipDesc.toLowerCase();
            if ( (edesc.indexOf("dial") >= 0) && 
                 (edesc.indexOf("pay") >= 0) )
            {
              DiscoverDisposition = "U";
            }
            else
            {
              DiscoverDisposition = decodeDisposition( resultSet.getLong("app_seq_num"),
                                                       PartNumber );     
            }
          }
          catch( NullPointerException _e )
          {
            // ignore, leave disposition as '?'
          }
        }
      }
      catch( java.sql.SQLException e )
      {
        logEntry( "RowData Constructor", e.toString() );
      }
      finally
      {
      }
    }
    
    public void addItem( )
    {
      ++EquipCount;
    }
    
    public void addItemCost( double cost )
    {
      BaseCost += cost;
    }
    
    public int compareTo( Object obj )
    {
      int           retVal      = 0;
      RowData       compareObj  = (RowData)obj;
      
      if ( (retVal = ParentName.compareTo(compareObj.ParentName)) == 0 )
      {
        if ( (retVal = DbaName.compareTo(compareObj.DbaName)) == 0 )
        {
          if ( (retVal = (int)(MerchantId - compareObj.MerchantId)) == 0 )
          {
            retVal = MesPartNumber.compareTo(compareObj.MesPartNumber);
          }
        }          
      }
      
      return( retVal );
    }
  }
  
  public DiscoverEquipmentDataBean( )
  {
  }
  
  protected String decodeDisposition( long appSeqNum, String partNum )
  {
    String            retVal      = "?";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:246^7*/

//  ************************************************************
//  #sql [Ctx] { select  decode( nvl(me.equiplendtype_code,'-'),
//                          '1','P',
//                          '2','R',
//                          '3','U',
//                          '4','P',
//                          '5','L',
//                          '?' )       
//          from    merchequipment    me
//          where   me.app_seq_num(+) = :appSeqNum and
//                  me.equip_model(+) = :partNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode( nvl(me.equiplendtype_code,'-'),\n                        '1','P',\n                        '2','R',\n                        '3','U',\n                        '4','P',\n                        '5','L',\n                        '?' )        \n        from    merchequipment    me\n        where   me.app_seq_num(+) =  :1  and\n                me.equip_model(+) =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.DiscoverEquipmentDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,partNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:258^7*/
    }
    catch( java.sql.SQLException e )
    {
      // ignore
    } 
    
    return( retVal );
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\" \",");
    line.append("\" \",");
    line.append("\"Rep ID #\",");
    line.append("\"SSE First Name\",");
    line.append("\"SSE Last Name\",");
    line.append("\"Merchant Number\",");
    line.append("\"DBA\",");
    line.append("\"Address\",");
    line.append("\"City\",");
    line.append("\"State\",");
    line.append("\"Zip Code\",");
    line.append("\"Merchant Phone Number\",");
    line.append("\"Date Approved\",");
    line.append("\"Date Opened\",");
    line.append("\"Date Activated\",");
    line.append("\"New or Existing Merchant\",");
    line.append("\"New or Existing Equipment\",");
    line.append("\"U-P-L-T\",");
    line.append("\"Qty\",");
    line.append("\"Equipment Type\",");
    line.append("\"Equip Status\",");
//    line.append("\"Eqiupment Price\",");
    line.append("\"Merchant BC Volume\",");
    line.append("\"Data Source\",");
    line.append("\"MES Merchant Number\"");
  }
  
  protected void encodeHeaderFixed( StringBuffer line )
  {
    line.setLength(0);
    line.append( encodeFixedField( 8,  ReportRows.size() + 1 ) );
    line.append( encodeFixedField( 6,  0 ) );   //@ ? should be "Total Card Services - Discover"
    line.append( encodeFixedField( new java.sql.Date(Calendar.getInstance().getTime().getTime()),"yyyyMMdd" ) );
    line.append( encodeFixedField( ReportDateBegin,"yyyyMM" ) );
    line.append( encodeFixedField( 15, "MeS" ) );
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData      record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append("\" \",");
    line.append("\" \",");
    line.append("\"");
    line.append( ((record.EmployeeId == null) ? "N/A" : record.EmployeeId) );
    line.append("\",\"");
    line.append(record.RepName[NAME_FIRST]);
    line.append("\",\"");
    line.append(record.RepName[NAME_LAST]);
    line.append("\",");
    line.append(record.DiscoverMerchantId);
    line.append(",\"");
    line.append(record.DbaName);
    line.append("\",\"");
    line.append(record.MailingAddress[ADDR_LINE1] );
    if ( record.MailingAddress[ADDR_LINE2] != null )
    {
      line.append( " " );
      line.append( record.MailingAddress[ADDR_LINE2] );
    }
    line.append("\",\"");
    line.append(record.MailingAddress[ADDR_CITY]);
    line.append("\",\"");
    line.append(record.MailingAddress[ADDR_STATE]);
    line.append("\",\"");
    line.append(record.MailingAddress[ADDR_ZIP]);
    line.append("\",\"");
    line.append(record.MerchantPhone);
    line.append("\",");
    line.append( DateTimeFormatter.getFormattedDate(record.DateApproved,"MM/dd/yyyy") );
    line.append(",");
    line.append( DateTimeFormatter.getFormattedDate(record.DateOpened,"MM/dd/yyyy") );
    line.append(",");
    line.append( DateTimeFormatter.getFormattedDate(record.DateActivated,"MM/dd/yyyy") );
    line.append(",\"");
    line.append( ((record.NewMerchant == true) ? "New" : "Existing") );
    line.append("\",\"");
    line.append( record.NewEquipment );
    line.append("\",\"");
    line.append( record.DiscoverDisposition );
    line.append("\",");
    line.append( record.EquipCount );
    line.append(",\"");
    line.append( record.EquipDesc );
    line.append("\",\"");
    line.append( record.DiscoverStatus );
    line.append("\",");
//    line.append( record.BaseCost );
//    line.append(",");
    line.append( record.AnnualSales );     // bankcard volume
    line.append(",");
    line.append( record.DataSource );
    line.append(",");
    line.append( record.MerchantId );
  }
  
  protected void encodeLineFixed( Object obj, StringBuffer line )
  {
    RowData       record            = (RowData)obj;
    
    line.setLength(0);
    line.append( encodeFixedField( 15, Long.toString(record.DiscoverMerchantId)  ) );
    line.append( encodeFixedField(  4, record.PartNumber                  ) );          
    line.append( encodeFixedField(  4, record.PartNumber                  ) );
    line.append( encodeFixedField( 14, record.CardsAccepted               ) );
    line.append( encodeFixedField( record.DateApproved, "yyyyMMdd"        ) );
    line.append( encodeFixedField( 25, record.SerialNumber                ) );
    line.append( encodeFixedField(  5, record.EmployeeId                  ) );
    line.append( encodeFixedField(  2, record.FinanceOption               ) );
    line.append( encodeFixedField(  3, 0                                  ) );    // term length
    line.append( encodeFixedField(  7, 0                                  ) );    // finance $
    line.append( encodeFixedField( 25, ""                                 ) );    // fee type desc
    line.append( encodeFixedField(  7, 0                                  ) );    // fee $
    line.append( encodeFixedField(  3, record.DiscoverStatus              ) );    // not available
    line.append( encodeFixedField( record.DateShipped, "yyyyMMdd"         ) );
    line.append( encodeFixedField(  8, ""                                 ) );    // installed date
    line.append( encodeFixedField(  8, ""                                 ) );    // cancelled date
    line.append( encodeFixedField(  4, ""                                 ) );    // cancelled reason
  }
  
  private RowData findIPSEntry( long merchantId, String partNum, String disposition )
  {
    RowData       retVal      = null;
    RowData       temp        = null;
    
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      temp = (RowData) ReportRows.elementAt(i);
      if( temp.MerchantId == merchantId &&
          temp.MesPartNumber.equals(partNum) &&
          temp.DiscoverDisposition.equals(disposition) &&
          temp.DataSource.equals("IPS") )
      {
        retVal = temp;
        break;
      }          
    }
    
    return( retVal );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_equip_activity_");
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( isSameMonthYear(ReportDateBegin,ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
      case FF_FIXED:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    java.util.Date                beforeDate      = null;
    Calendar                      cal             = Calendar.getInstance();
    String                        disposition     = null;
    RowData                       entry           = null;
    ResultSetIterator             it              = null;
    String                        lastDisposition = "";
    long                          lastMerchantId  = 0L;
    String                        lastPartNum     = "";
    long                          merchantId      = 0L;
    String                        partNum         = null;
    ResultSet                     resultSet       = null;
    RowData                       row             = null;
    
    try
    {
      // set to last day of april
      cal.set(Calendar.DAY_OF_MONTH,1);
      cal.set(Calendar.MONTH, Calendar.MAY);
      cal.set(Calendar.YEAR, 2002);
      beforeDate = cal.getTime();
      
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:473^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                    INDEX(po idx_parent_org_num) 
//                    INDEX(gm pkgroup_merchant)
//                    INDEX(o xpkorganization)
//                  */
//                  ips.install_date                                as active_date,
//                  o.org_num                                       as org_num,
//                  o.org_group                                     as node,
//                  o.org_name                                      as org_name,
//                  nvl(mr.discover_rep_id,u.THIRD_PARTY_USER_ID)   as ext_rep_id,
//                  nvl(mr.discover_rep_name,u.name)                as rep_name,
//                  mf.merchant_number                              as merchant_number,
//                  nvl(mf.dmdsnum, mpo.merchpo_card_merch_number)  as discover_merchant_number,
//                  mf.dba_name                                     as dba_name,
//                  mf.ADDR1_LINE_1                                 as address1, 
//                  mf.ADDR1_LINE_2                                 as address2,
//                  mf.DMCITY                                       as city,
//                  mf.DMSTATE                                      as state, 
//                  substr(mf.dmzip,1,5) || 
//                    '-' || substr(mf.dmzip,6,4)                   as zip_code,
//                  substr(mf.PHONE_1,1,3) || '-' ||
//                  substr(mf.phone_1,4,3) || '-' ||
//                  substr(mf.phone_1,7,4)                          as merchant_phone,            
//                  ( 
//                    decode(mf.DDSACCPT,'Y',1,0) ||
//                    decode(mf.DVSACCPT,'Y',1,0) ||
//                    decode(mf.DMCACCPT,'Y',1,0) ||
//                    decode(mf.DAMACCPT,'Y',1,0) ||
//                    decode(mf.DDCACCPT,'Y',1,0) ||
//                    decode(mf.DJCACCPT,'Y',1,0) ||
//                    '000' || -- check trunc, check verify, check guarantee
//                    decode(mf.DEBIT_PLAN,null,0,1) ||
//                    '0000'  -- other, reserved
//                  )                                               as cards_accepted,
//                  decode( at.date_completed,
//                          null, to_date(mf.date_opened,'mmddrr'),
//                          trunc(at.date_completed) )              as date_approved,
//                  to_date(mf.date_opened,'mmddrr')                as date_opened,
//                  mf.activation_date                              as date_activated,
//                  decode( substr(mf.user_data_4,15,1),
//                          'C','N','Y')                            as new_merchant,                        
//                  decode( substr(ips.disposition,1,1),
//                          'O','Existing',
//                          'P','New',
//                          'R','New',
//                          '?' )                                   as new_equipment,
//                  (decode(mr.MERCH_MONTH_VISA_MC_SALES, 
//                         null, 0, 
//                         mr.MERCH_MONTH_VISA_MC_SALES) * 12)      as annual_sales,
//                  substr(ips.DISPOSITION,1,1)                     as disposition,
//                  ips.TERMINAL_TYPE                               as mes_part_number,
//                  nvl(dpn.discover_part_number,'TEXC')            as part_number,
//                  null                                            as serial_number,
//                  nvl(dpn.description,ips.terminal_type)          as equip_desc,
//                  decode( substr(ltrim(ips.disposition),1,1),
//                          'O','U',
//                          'P','P',
//                          'R','R',
//                          'L','L',
//                          'S','N/A',
//                          '?' )                                   as discover_disposition,
//                  decode( substr(ltrim(ips.disposition),1,1),
//                          'O','E',    -- owned => existing
//                          'P','N',    -- purch => new
//                          'R','X',    -- rent => unknown
//                          'L','X',    -- lease => unknown
//                          'S','S',    -- swap => swap
//                          '?' )                                   as discover_status,
//                  dfo.finance_option_code                         as finance_option,
//                  ips.install_date                                as date_shipped,
//                  0                                               as base_cost,
//                  'IPS'                                           as data_source,
//                  mr.app_seq_num                                  as app_seq_num
//          from    parent_org                po,
//                  organization              o,
//                  group_merchant            gm,
//                  mif                       mf,
//                  ips_file                  ips,
//                  merchant                  mr,
//                  application               app,
//                  merchpayoption            mpo,
//                  app_tracking              at,
//                  users                     u,
//                  discover_part_numbers     dpn,
//                  discover_finance_options  dfo
//          where   po.parent_org_num = :orgId and
//                  o.org_num   = po.org_num and
//                  gm.org_num  = o.org_num and
//                  mf.merchant_number = gm.merchant_number and
//                  ips.merchant_number = mf.merchant_number and
//                  ips.INSTALL_DATE between :beginDate and last_day(:endDate) and
//                  mr.merch_number(+)  = mf.merchant_number and
//                  app.app_seq_num(+)  = mr.app_seq_num and
//                  at.app_seq_num(+)   = mr.app_seq_num and
//                  mpo.app_seq_num(+)  = mr.app_seq_num and
//                  mpo.cardtype_code(+)= :mesConstants.APP_CT_DISCOVER and -- 14 and
//                  at.dept_code(+)     = :QueueConstants.DEPARTMENT_CREDIT and -- 100 and
//                  at.status_code(+)   = :QueueConstants.DEPT_STATUS_CREDIT_APPROVED and -- 102 and
//                  u.user_id(+) = app.app_user_id and
//                  dpn.mes_part_number(+) = ips.terminal_type and
//                  dfo.equiplendtype_code(+) = decode( substr(ltrim(ips.disposition),1,1),
//                                                      'O',:mesConstants.APP_EQUIP_OWNED,    -- 'O',3,
//                                                      'P',:mesConstants.APP_EQUIP_PURCHASE, -- 'P',1,
//                                                      'R',:mesConstants.APP_EQUIP_RENT,     -- 'R',2,
//                                                      'L',:mesConstants.APP_EQUIP_LEASE,    -- 'L',5,
//                                                      :mesConstants.APP_EQUIP_COUNT )       -- -1 )
//          order by o.org_name, dba_name, merchant_number, mes_part_number, discover_disposition
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                  INDEX(po idx_parent_org_num) \n                  INDEX(gm pkgroup_merchant)\n                  INDEX(o xpkorganization)\n                */\n                ips.install_date                                as active_date,\n                o.org_num                                       as org_num,\n                o.org_group                                     as node,\n                o.org_name                                      as org_name,\n                nvl(mr.discover_rep_id,u.THIRD_PARTY_USER_ID)   as ext_rep_id,\n                nvl(mr.discover_rep_name,u.name)                as rep_name,\n                mf.merchant_number                              as merchant_number,\n                nvl(mf.dmdsnum, mpo.merchpo_card_merch_number)  as discover_merchant_number,\n                mf.dba_name                                     as dba_name,\n                mf.ADDR1_LINE_1                                 as address1, \n                mf.ADDR1_LINE_2                                 as address2,\n                mf.DMCITY                                       as city,\n                mf.DMSTATE                                      as state, \n                substr(mf.dmzip,1,5) || \n                  '-' || substr(mf.dmzip,6,4)                   as zip_code,\n                substr(mf.PHONE_1,1,3) || '-' ||\n                substr(mf.phone_1,4,3) || '-' ||\n                substr(mf.phone_1,7,4)                          as merchant_phone,            \n                ( \n                  decode(mf.DDSACCPT,'Y',1,0) ||\n                  decode(mf.DVSACCPT,'Y',1,0) ||\n                  decode(mf.DMCACCPT,'Y',1,0) ||\n                  decode(mf.DAMACCPT,'Y',1,0) ||\n                  decode(mf.DDCACCPT,'Y',1,0) ||\n                  decode(mf.DJCACCPT,'Y',1,0) ||\n                  '000' || -- check trunc, check verify, check guarantee\n                  decode(mf.DEBIT_PLAN,null,0,1) ||\n                  '0000'  -- other, reserved\n                )                                               as cards_accepted,\n                decode( at.date_completed,\n                        null, to_date(mf.date_opened,'mmddrr'),\n                        trunc(at.date_completed) )              as date_approved,\n                to_date(mf.date_opened,'mmddrr')                as date_opened,\n                mf.activation_date                              as date_activated,\n                decode( substr(mf.user_data_4,15,1),\n                        'C','N','Y')                            as new_merchant,                        \n                decode( substr(ips.disposition,1,1),\n                        'O','Existing',\n                        'P','New',\n                        'R','New',\n                        '?' )                                   as new_equipment,\n                (decode(mr.MERCH_MONTH_VISA_MC_SALES, \n                       null, 0, \n                       mr.MERCH_MONTH_VISA_MC_SALES) * 12)      as annual_sales,\n                substr(ips.DISPOSITION,1,1)                     as disposition,\n                ips.TERMINAL_TYPE                               as mes_part_number,\n                nvl(dpn.discover_part_number,'TEXC')            as part_number,\n                null                                            as serial_number,\n                nvl(dpn.description,ips.terminal_type)          as equip_desc,\n                decode( substr(ltrim(ips.disposition),1,1),\n                        'O','U',\n                        'P','P',\n                        'R','R',\n                        'L','L',\n                        'S','N/A',\n                        '?' )                                   as discover_disposition,\n                decode( substr(ltrim(ips.disposition),1,1),\n                        'O','E',    -- owned => existing\n                        'P','N',    -- purch => new\n                        'R','X',    -- rent => unknown\n                        'L','X',    -- lease => unknown\n                        'S','S',    -- swap => swap\n                        '?' )                                   as discover_status,\n                dfo.finance_option_code                         as finance_option,\n                ips.install_date                                as date_shipped,\n                0                                               as base_cost,\n                'IPS'                                           as data_source,\n                mr.app_seq_num                                  as app_seq_num\n        from    parent_org                po,\n                organization              o,\n                group_merchant            gm,\n                mif                       mf,\n                ips_file                  ips,\n                merchant                  mr,\n                application               app,\n                merchpayoption            mpo,\n                app_tracking              at,\n                users                     u,\n                discover_part_numbers     dpn,\n                discover_finance_options  dfo\n        where   po.parent_org_num =  :1  and\n                o.org_num   = po.org_num and\n                gm.org_num  = o.org_num and\n                mf.merchant_number = gm.merchant_number and\n                ips.merchant_number = mf.merchant_number and\n                ips.INSTALL_DATE between  :2  and last_day( :3 ) and\n                mr.merch_number(+)  = mf.merchant_number and\n                app.app_seq_num(+)  = mr.app_seq_num and\n                at.app_seq_num(+)   = mr.app_seq_num and\n                mpo.app_seq_num(+)  = mr.app_seq_num and\n                mpo.cardtype_code(+)=  :4  and -- 14 and\n                at.dept_code(+)     =  :5  and -- 100 and\n                at.status_code(+)   =  :6  and -- 102 and\n                u.user_id(+) = app.app_user_id and\n                dpn.mes_part_number(+) = ips.terminal_type and\n                dfo.equiplendtype_code(+) = decode( substr(ltrim(ips.disposition),1,1),\n                                                    'O', :7 ,    -- 'O',3,\n                                                    'P', :8 , -- 'P',1,\n                                                    'R', :9 ,     -- 'R',2,\n                                                    'L', :10 ,    -- 'L',5,\n                                                     :11  )       -- -1 )\n        order by o.org_name, dba_name, merchant_number, mes_part_number, discover_disposition";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.DiscoverEquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setInt(5,QueueConstants.DEPARTMENT_CREDIT);
   __sJT_st.setInt(6,QueueConstants.DEPT_STATUS_CREDIT_APPROVED);
   __sJT_st.setInt(7,mesConstants.APP_EQUIP_OWNED);
   __sJT_st.setInt(8,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(9,mesConstants.APP_EQUIP_RENT);
   __sJT_st.setInt(10,mesConstants.APP_EQUIP_LEASE);
   __sJT_st.setInt(11,mesConstants.APP_EQUIP_COUNT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.DiscoverEquipmentDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:582^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        merchantId  = resultSet.getLong("merchant_number");
        partNum     = resultSet.getString("mes_part_number");
        disposition = resultSet.getString("discover_disposition");
        
        if ( ReportType == RT_DETAILS )
        {
          ReportRows.addElement( new RowData( resultSet ) );
        }
        else    // merge same part number entries
        {
          if ( (lastMerchantId != merchantId) || 
              !(partNum.equals(lastPartNum))  || 
              !(disposition.equals(lastDisposition)) )
          {
            row = new RowData( resultSet );
            ReportRows.addElement(row);
          }
          else
          {
            row.addItem();
            row.addItemCost(resultSet.getDouble("base_cost"));
          }          
        
          // store the comparison values
          lastDisposition = disposition;
          lastMerchantId  = merchantId;
          lastPartNum     = partNum;
        }          
      }
      resultSet.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:619^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                    INDEX(po idx_parent_org_num) 
//                    INDEX(gm pkgroup_merchant)
//                    INDEX(o xpkorganization)
//                    index(ei idx_ei_merch_deployed_date)
//                  */
//                  trunc(ei.EI_DEPLOYED_DATE)                      as active_date,
//                  o.org_num                                       as org_num,
//                  o.org_group                                     as node,
//                  o.org_name                                      as org_name,
//                  nvl(mr.discover_rep_id,u.THIRD_PARTY_USER_ID)   as ext_rep_id,
//                  nvl(mr.discover_rep_name,u.name)                as rep_name,
//                  mf.merchant_number                              as merchant_number,
//                  nvl(mf.dmdsnum, mpo.merchpo_card_merch_number)  as discover_merchant_number,
//                  mf.dba_name                                     as dba_name,
//                  mf.ADDR1_LINE_1                                 as address1, 
//                  mf.ADDR1_LINE_2                                 as address2,
//                  mf.DMCITY                                       as city,
//                  mf.DMSTATE                                      as state, 
//                  substr(mf.dmzip,1,5) || 
//                    '-' || substr(mf.dmzip,6,4)                   as zip_code,
//                  substr(mf.PHONE_1,1,3) || '-' ||
//                  substr(mf.phone_1,4,3) || '-' ||
//                  substr(mf.phone_1,7,4)                          as merchant_phone,            
//                  ( 
//                    decode(mf.DDSACCPT,'Y',1,0) ||
//                    decode(mf.DVSACCPT,'Y',1,0) ||
//                    decode(mf.DMCACCPT,'Y',1,0) ||
//                    decode(mf.DAMACCPT,'Y',1,0) ||
//                    decode(mf.DDCACCPT,'Y',1,0) ||
//                    decode(mf.DJCACCPT,'Y',1,0) ||
//                    '000' || -- check trunc, check verify, check guarantee
//                    decode(mf.DEBIT_PLAN,null,0,1) ||
//                    '0000'  -- other, reserved
//                  )                                               as cards_accepted,
//                  decode( at.date_completed,
//                          null, to_date(mf.date_opened,'mmddrr'),
//                          trunc(at.date_completed) )              as date_approved,
//                  to_date(mf.date_opened,'mmddrr')                as date_opened,
//                  mf.activation_date                              as date_activated,
//                  decode( substr(mf.user_data_4,15,1),
//                          'C','N','Y')                            as new_merchant,
//                  decode( ei.ei_lrb,
//                          1,'New',    -- purchase
//                          2,'New',    -- rent
//                          5,'New',    -- lease
//                          4,'Ref',    -- buy refurbished
//                          '?' )                                   as new_equipment,
//                  (decode(mr.MERCH_MONTH_VISA_MC_SALES, 
//                         null, 0, 
//                         mr.MERCH_MONTH_VISA_MC_SALES) * 12)      as annual_sales,
//                  nvl(lt.equiplendtype_description,
//                      'Status Code: ' || ei.ei_lrb )              as disposition,
//                  ei.ei_part_number                               as mes_part_number,
//                  nvl(dpn.discover_part_number,'TEXC')            as part_number,
//                  ei.ei_serial_number                             as serial_number,
//                  nvl(dpn.description,'Not Available')            as equip_desc,
//                  decode( ei.ei_lrb,     
//                          :mesConstants.APP_EQUIP_PURCHASE,'P',         -- 1,'P',
//                          :mesConstants.APP_EQUIP_BUY_REFURBISHED,'P',  -- 4,'P',
//                          :mesConstants.APP_EQUIP_RENT,'R',             -- 2,'R',
//                          :mesConstants.APP_EQUIP_LEASE,'L',            -- 5,'L',
//                          '?' )                                   as discover_disposition,
//                  decode( ei.ei_lrb,     
//                          :mesConstants.APP_EQUIP_PURCHASE,
//                            decode( ei.ei_class,
//                                    1, 'N',     -- new
//                                    2, 'U',     -- used
//                                    3, 'R',     -- refurbished
//                                    'X' ),      -- unknown
//                          'X' )                                   as discover_status,
//                  dfo.finance_option_code                         as finance_option,                        
//                  ei.ei_deployed_date                             as date_shipped,
//                  ei.ei_unit_cost                                 as base_cost,
//                  'INV'                                           as data_source,
//                  mr.app_seq_num                                  as app_seq_num
//          from    parent_org                po,
//                  organization              o,
//                  group_merchant            gm,
//                  mif                       mf,
//                  equip_inventory           ei,
//                  equiplendtype             lt,
//                  merchant                  mr,
//                  application               app,
//                  merchpayoption            mpo,
//                  app_tracking              at,
//                  users                     u,
//                  discover_part_numbers     dpn,
//                  discover_finance_options  dfo
//          where   po.parent_org_num = :orgId and
//                  o.org_num   = po.org_num and
//                  gm.org_num  = o.org_num and
//                  mf.merchant_number = gm.merchant_number and
//                  nvl(ei.EI_MERCHANT_NUMBER,0) = mf.merchant_number and
//                  nvl(ei.EI_DEPLOYED_DATE,'31-DEC-9999') between :beginDate and last_day(:endDate) and
//                  mr.merch_number(+)  = mf.merchant_number and
//                  app.app_seq_num(+)  = mr.app_seq_num and
//                  at.app_seq_num(+)   = mr.app_seq_num and
//                  at.dept_code(+)     = :QueueConstants.DEPARTMENT_CREDIT and -- 100 and
//                  at.status_code(+)   = :QueueConstants.DEPT_STATUS_CREDIT_APPROVED and -- 102 and
//                  mpo.app_seq_num(+)  = mr.app_seq_num and
//                  mpo.cardtype_code(+)= :mesConstants.APP_CT_DISCOVER and -- 14 and
//                  u.user_id(+) = app.app_user_id and
//                  dpn.mes_part_number(+) = ei.EI_PART_NUMBER and
//                  dfo.equiplendtype_code(+) = ei.ei_lrb and
//                  lt.equiplendtype_code(+) = ei.ei_lrb
//          order by o.org_name, dba_name, merchant_number, mes_part_number, discover_disposition
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                  INDEX(po idx_parent_org_num) \n                  INDEX(gm pkgroup_merchant)\n                  INDEX(o xpkorganization)\n                  index(ei idx_ei_merch_deployed_date)\n                */\n                trunc(ei.EI_DEPLOYED_DATE)                      as active_date,\n                o.org_num                                       as org_num,\n                o.org_group                                     as node,\n                o.org_name                                      as org_name,\n                nvl(mr.discover_rep_id,u.THIRD_PARTY_USER_ID)   as ext_rep_id,\n                nvl(mr.discover_rep_name,u.name)                as rep_name,\n                mf.merchant_number                              as merchant_number,\n                nvl(mf.dmdsnum, mpo.merchpo_card_merch_number)  as discover_merchant_number,\n                mf.dba_name                                     as dba_name,\n                mf.ADDR1_LINE_1                                 as address1, \n                mf.ADDR1_LINE_2                                 as address2,\n                mf.DMCITY                                       as city,\n                mf.DMSTATE                                      as state, \n                substr(mf.dmzip,1,5) || \n                  '-' || substr(mf.dmzip,6,4)                   as zip_code,\n                substr(mf.PHONE_1,1,3) || '-' ||\n                substr(mf.phone_1,4,3) || '-' ||\n                substr(mf.phone_1,7,4)                          as merchant_phone,            \n                ( \n                  decode(mf.DDSACCPT,'Y',1,0) ||\n                  decode(mf.DVSACCPT,'Y',1,0) ||\n                  decode(mf.DMCACCPT,'Y',1,0) ||\n                  decode(mf.DAMACCPT,'Y',1,0) ||\n                  decode(mf.DDCACCPT,'Y',1,0) ||\n                  decode(mf.DJCACCPT,'Y',1,0) ||\n                  '000' || -- check trunc, check verify, check guarantee\n                  decode(mf.DEBIT_PLAN,null,0,1) ||\n                  '0000'  -- other, reserved\n                )                                               as cards_accepted,\n                decode( at.date_completed,\n                        null, to_date(mf.date_opened,'mmddrr'),\n                        trunc(at.date_completed) )              as date_approved,\n                to_date(mf.date_opened,'mmddrr')                as date_opened,\n                mf.activation_date                              as date_activated,\n                decode( substr(mf.user_data_4,15,1),\n                        'C','N','Y')                            as new_merchant,\n                decode( ei.ei_lrb,\n                        1,'New',    -- purchase\n                        2,'New',    -- rent\n                        5,'New',    -- lease\n                        4,'Ref',    -- buy refurbished\n                        '?' )                                   as new_equipment,\n                (decode(mr.MERCH_MONTH_VISA_MC_SALES, \n                       null, 0, \n                       mr.MERCH_MONTH_VISA_MC_SALES) * 12)      as annual_sales,\n                nvl(lt.equiplendtype_description,\n                    'Status Code: ' || ei.ei_lrb )              as disposition,\n                ei.ei_part_number                               as mes_part_number,\n                nvl(dpn.discover_part_number,'TEXC')            as part_number,\n                ei.ei_serial_number                             as serial_number,\n                nvl(dpn.description,'Not Available')            as equip_desc,\n                decode( ei.ei_lrb,     \n                         :1 ,'P',         -- 1,'P',\n                         :2 ,'P',  -- 4,'P',\n                         :3 ,'R',             -- 2,'R',\n                         :4 ,'L',            -- 5,'L',\n                        '?' )                                   as discover_disposition,\n                decode( ei.ei_lrb,     \n                         :5 ,\n                          decode( ei.ei_class,\n                                  1, 'N',     -- new\n                                  2, 'U',     -- used\n                                  3, 'R',     -- refurbished\n                                  'X' ),      -- unknown\n                        'X' )                                   as discover_status,\n                dfo.finance_option_code                         as finance_option,                        \n                ei.ei_deployed_date                             as date_shipped,\n                ei.ei_unit_cost                                 as base_cost,\n                'INV'                                           as data_source,\n                mr.app_seq_num                                  as app_seq_num\n        from    parent_org                po,\n                organization              o,\n                group_merchant            gm,\n                mif                       mf,\n                equip_inventory           ei,\n                equiplendtype             lt,\n                merchant                  mr,\n                application               app,\n                merchpayoption            mpo,\n                app_tracking              at,\n                users                     u,\n                discover_part_numbers     dpn,\n                discover_finance_options  dfo\n        where   po.parent_org_num =  :6  and\n                o.org_num   = po.org_num and\n                gm.org_num  = o.org_num and\n                mf.merchant_number = gm.merchant_number and\n                nvl(ei.EI_MERCHANT_NUMBER,0) = mf.merchant_number and\n                nvl(ei.EI_DEPLOYED_DATE,'31-DEC-9999') between  :7  and last_day( :8 ) and\n                mr.merch_number(+)  = mf.merchant_number and\n                app.app_seq_num(+)  = mr.app_seq_num and\n                at.app_seq_num(+)   = mr.app_seq_num and\n                at.dept_code(+)     =  :9  and -- 100 and\n                at.status_code(+)   =  :10  and -- 102 and\n                mpo.app_seq_num(+)  = mr.app_seq_num and\n                mpo.cardtype_code(+)=  :11  and -- 14 and\n                u.user_id(+) = app.app_user_id and\n                dpn.mes_part_number(+) = ei.EI_PART_NUMBER and\n                dfo.equiplendtype_code(+) = ei.ei_lrb and\n                lt.equiplendtype_code(+) = ei.ei_lrb\n        order by o.org_name, dba_name, merchant_number, mes_part_number, discover_disposition";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.DiscoverEquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_BUY_REFURBISHED);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_RENT);
   __sJT_st.setInt(4,mesConstants.APP_EQUIP_LEASE);
   __sJT_st.setInt(5,mesConstants.APP_EQUIP_PURCHASE);
   __sJT_st.setLong(6,orgId);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   __sJT_st.setInt(9,QueueConstants.DEPARTMENT_CREDIT);
   __sJT_st.setInt(10,QueueConstants.DEPT_STATUS_CREDIT_APPROVED);
   __sJT_st.setInt(11,mesConstants.APP_CT_DISCOVER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.DiscoverEquipmentDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:728^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        merchantId  = resultSet.getLong("merchant_number");
        partNum     = resultSet.getString("mes_part_number");
        disposition = resultSet.getString("discover_disposition");
        entry       = findIPSEntry( merchantId, partNum, disposition );
        
        // if before may 1st, then ignore the platform and keep IPS
        if ( entry != null && resultSet.getDate("date_approved").before(beforeDate) )
        {
          continue;
        }
        else if ( entry != null )   // after may 1st
        {
          ReportRows.removeElement(entry);    // remove IPS and use this
        }
        
        
        if ( ReportType == RT_DETAILS )
        {
          ReportRows.addElement( new RowData( resultSet ) );
        }
        else    // merge same part number entries
        {
          if ( (lastMerchantId != merchantId) || 
              !(partNum.equals(lastPartNum))  || 
              !(disposition.equals(lastDisposition)) )
          {
            row = new RowData( resultSet );
            ReportRows.addElement(row);
          }
          else
          {
            row.addItem();
            row.addItemCost(resultSet.getDouble("base_cost"));
          }          
        
          // store the comparison values
          lastDisposition = disposition;
          lastMerchantId  = merchantId;
          lastPartNum     = partNum;
        }          
      }
      resultSet.close();
      it.close();
      
      // dial pay merchants do not have data in the inventory database
      // so it is necessary to pull the data using user data 3 in the MIF
      /*@lineinfo:generated-code*//*@lineinfo:779^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                    INDEX(po idx_parent_org_num) 
//                    INDEX(gm pkgroup_merchant)
//                    INDEX(o xpkorganization)
//                  */
//                  to_date(mf.date_opened,'mmddrr')                as active_date,
//                  o.org_num                                       as org_num,
//                  o.org_group                                     as node,
//                  o.org_name                                      as org_name,
//                  nvl(mr.discover_rep_id,u.THIRD_PARTY_USER_ID)   as ext_rep_id,
//                  nvl(mr.discover_rep_name,u.name)                as rep_name,
//                  mf.merchant_number                              as merchant_number,
//                  nvl(mf.dmdsnum, mpo.merchpo_card_merch_number)  as discover_merchant_number,
//                  mf.dba_name                                     as dba_name,
//                  mf.ADDR1_LINE_1                                 as address1, 
//                  mf.ADDR1_LINE_2                                 as address2,
//                  mf.DMCITY                                       as city,
//                  mf.DMSTATE                                      as state, 
//                  substr(mf.dmzip,1,5) || 
//                    '-' || substr(mf.dmzip,6,4)                   as zip_code,
//                  substr(mf.PHONE_1,1,3) || '-' ||
//                  substr(mf.phone_1,4,3) || '-' ||
//                  substr(mf.phone_1,7,4)                          as merchant_phone,            
//                  ( 
//                    decode(mf.DDSACCPT,'Y',1,0) ||
//                    decode(mf.DVSACCPT,'Y',1,0) ||
//                    decode(mf.DMCACCPT,'Y',1,0) ||
//                    decode(mf.DAMACCPT,'Y',1,0) ||
//                    decode(mf.DDCACCPT,'Y',1,0) ||
//                    decode(mf.DJCACCPT,'Y',1,0) ||
//                    '000' || -- check trunc, check verify, check guarantee
//                    decode(mf.DEBIT_PLAN,null,0,1) ||
//                    '0000'  -- other, reserved
//                  )                                               as cards_accepted,
//                  decode( at.date_completed,
//                          null, to_date(mf.date_opened,'mmddrr'),
//                          trunc(at.date_completed) )              as date_approved,
//                  to_date(mf.date_opened,'mmddrr')                as date_opened,
//                  mf.activation_date                              as date_activated,
//                  decode( substr(mf.user_data_4,15,1),
//                          'C','N','Y')                            as new_merchant,                        
//                  'N/A'                                           as new_equipment,
//                  (decode(mr.MERCH_MONTH_VISA_MC_SALES, 
//                         null, 0, 
//                         mr.MERCH_MONTH_VISA_MC_SALES) * 12)      as annual_sales,
//                  'U'                                             as disposition,
//                  substr(mf.user_data_3,3,2)                      as mes_part_number,
//                  nvl(dpn.discover_part_number,'TEXC')            as part_number,
//                  null                                            as serial_number,
//                  dpn.description                                 as equip_desc,
//                  'U'                                             as discover_disposition,
//                  'X'                                             as discover_status,
//                  'P'                                             as finance_option,
//                  0                                               as base_cost,
//                  'MIF'                                           as data_source,
//                  to_date(mf.date_opened,'mmddrr')                as date_shipped,
//                  mr.app_seq_num                                  as app_seq_num
//          from    parent_org                po,
//                  organization              o,
//                  group_merchant            gm,
//                  mif                       mf,
//                  merchant                  mr,
//                  application               app,
//                  app_tracking              at,
//                  merchpayoption            mpo,
//                  users                     u,
//                  discover_part_numbers     dpn
//          where   po.parent_org_num = :orgId and
//                  o.org_num   = po.org_num and
//                  gm.org_num  = o.org_num and
//                  mf.merchant_number = gm.merchant_number and
//                  substr(mf.user_data_3,3,2) in ( 'DP', 'CS', 'VL', 'VF', 'PO', 'P2' ) and
//                  to_date(mf.date_opened,'mmddrr') between :beginDate and last_day(:endDate) and
//                  mr.merch_number(+)  = mf.merchant_number and
//                  app.app_seq_num(+)  = mr.app_seq_num and
//                  at.app_seq_num(+)   = mr.app_seq_num and
//                  at.dept_code(+)     = :QueueConstants.DEPARTMENT_CREDIT and -- 100 and
//                  at.status_code(+)   = :QueueConstants.DEPT_STATUS_CREDIT_APPROVED and -- 102 and
//                  mpo.app_seq_num(+)  = mr.app_seq_num and
//                  mpo.cardtype_code(+)= :mesConstants.APP_CT_DISCOVER and -- 14 and
//                  u.user_id(+) = app.app_user_id and
//                  dpn.mes_part_number(+) = decode( substr(mf.user_data_3,3,2),
//                                                   'PO', 'PCPS',
//                                                   'P2', 'PCPS',
//                                                   substr(mf.user_data_3,3,2) )
//          order by o.org_name, dba_name, merchant_number, mes_part_number, discover_disposition
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                  INDEX(po idx_parent_org_num) \n                  INDEX(gm pkgroup_merchant)\n                  INDEX(o xpkorganization)\n                */\n                to_date(mf.date_opened,'mmddrr')                as active_date,\n                o.org_num                                       as org_num,\n                o.org_group                                     as node,\n                o.org_name                                      as org_name,\n                nvl(mr.discover_rep_id,u.THIRD_PARTY_USER_ID)   as ext_rep_id,\n                nvl(mr.discover_rep_name,u.name)                as rep_name,\n                mf.merchant_number                              as merchant_number,\n                nvl(mf.dmdsnum, mpo.merchpo_card_merch_number)  as discover_merchant_number,\n                mf.dba_name                                     as dba_name,\n                mf.ADDR1_LINE_1                                 as address1, \n                mf.ADDR1_LINE_2                                 as address2,\n                mf.DMCITY                                       as city,\n                mf.DMSTATE                                      as state, \n                substr(mf.dmzip,1,5) || \n                  '-' || substr(mf.dmzip,6,4)                   as zip_code,\n                substr(mf.PHONE_1,1,3) || '-' ||\n                substr(mf.phone_1,4,3) || '-' ||\n                substr(mf.phone_1,7,4)                          as merchant_phone,            \n                ( \n                  decode(mf.DDSACCPT,'Y',1,0) ||\n                  decode(mf.DVSACCPT,'Y',1,0) ||\n                  decode(mf.DMCACCPT,'Y',1,0) ||\n                  decode(mf.DAMACCPT,'Y',1,0) ||\n                  decode(mf.DDCACCPT,'Y',1,0) ||\n                  decode(mf.DJCACCPT,'Y',1,0) ||\n                  '000' || -- check trunc, check verify, check guarantee\n                  decode(mf.DEBIT_PLAN,null,0,1) ||\n                  '0000'  -- other, reserved\n                )                                               as cards_accepted,\n                decode( at.date_completed,\n                        null, to_date(mf.date_opened,'mmddrr'),\n                        trunc(at.date_completed) )              as date_approved,\n                to_date(mf.date_opened,'mmddrr')                as date_opened,\n                mf.activation_date                              as date_activated,\n                decode( substr(mf.user_data_4,15,1),\n                        'C','N','Y')                            as new_merchant,                        \n                'N/A'                                           as new_equipment,\n                (decode(mr.MERCH_MONTH_VISA_MC_SALES, \n                       null, 0, \n                       mr.MERCH_MONTH_VISA_MC_SALES) * 12)      as annual_sales,\n                'U'                                             as disposition,\n                substr(mf.user_data_3,3,2)                      as mes_part_number,\n                nvl(dpn.discover_part_number,'TEXC')            as part_number,\n                null                                            as serial_number,\n                dpn.description                                 as equip_desc,\n                'U'                                             as discover_disposition,\n                'X'                                             as discover_status,\n                'P'                                             as finance_option,\n                0                                               as base_cost,\n                'MIF'                                           as data_source,\n                to_date(mf.date_opened,'mmddrr')                as date_shipped,\n                mr.app_seq_num                                  as app_seq_num\n        from    parent_org                po,\n                organization              o,\n                group_merchant            gm,\n                mif                       mf,\n                merchant                  mr,\n                application               app,\n                app_tracking              at,\n                merchpayoption            mpo,\n                users                     u,\n                discover_part_numbers     dpn\n        where   po.parent_org_num =  :1  and\n                o.org_num   = po.org_num and\n                gm.org_num  = o.org_num and\n                mf.merchant_number = gm.merchant_number and\n                substr(mf.user_data_3,3,2) in ( 'DP', 'CS', 'VL', 'VF', 'PO', 'P2' ) and\n                to_date(mf.date_opened,'mmddrr') between  :2  and last_day( :3 ) and\n                mr.merch_number(+)  = mf.merchant_number and\n                app.app_seq_num(+)  = mr.app_seq_num and\n                at.app_seq_num(+)   = mr.app_seq_num and\n                at.dept_code(+)     =  :4  and -- 100 and\n                at.status_code(+)   =  :5  and -- 102 and\n                mpo.app_seq_num(+)  = mr.app_seq_num and\n                mpo.cardtype_code(+)=  :6  and -- 14 and\n                u.user_id(+) = app.app_user_id and\n                dpn.mes_part_number(+) = decode( substr(mf.user_data_3,3,2),\n                                                 'PO', 'PCPS',\n                                                 'P2', 'PCPS',\n                                                 substr(mf.user_data_3,3,2) )\n        order by o.org_name, dba_name, merchant_number, mes_part_number, discover_disposition";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.DiscoverEquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,QueueConstants.DEPARTMENT_CREDIT);
   __sJT_st.setInt(5,QueueConstants.DEPT_STATUS_CREDIT_APPROVED);
   __sJT_st.setInt(6,mesConstants.APP_CT_DISCOVER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.DiscoverEquipmentDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:867^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        merchantId  = resultSet.getLong("merchant_number");
        partNum     = resultSet.getString("mes_part_number");
        disposition = resultSet.getString("discover_disposition");
        entry       = findIPSEntry( merchantId, partNum, disposition );
        
        // if before may 1st, then ignore the platform and keep IPS
        if ( entry != null && resultSet.getDate("date_approved").before(beforeDate) )
        {
          continue;
        }
        else if ( entry != null )   // after may 1st
        {
          ReportRows.removeElement(entry);    // remove IPS and use this
        }
        
        if ( ReportType == RT_DETAILS )
        {
          ReportRows.addElement( new RowData( resultSet ) );
        }
        else    // merge same part number entries
        {
          if ( (lastMerchantId != merchantId) || 
              !(partNum.equals(lastPartNum))  || 
              !(disposition.equals(lastDisposition)) )
          {
            row = new RowData( resultSet );
            ReportRows.addElement(row);
          }
          else
          {
            row.addItem();
            row.addItemCost(resultSet.getDouble("base_cost"));
          }          
        
          // store the comparison values
          lastDisposition = disposition;
          lastMerchantId  = merchantId;
          lastPartNum     = partNum;
        }          
      }
      resultSet.close();
      it.close(); 
      
      // load all the merchant owned equipment from the application
      /*@lineinfo:generated-code*//*@lineinfo:916^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                    INDEX(po idx_parent_org_num) 
//                    INDEX(gm pkgroup_merchant)
//                    INDEX(o xpkorganization)
//                  */
//                  to_date(mf.date_opened,'mmddrr')                as active_date,
//                  o.org_num                                       as org_num,
//                  o.org_group                                     as node,
//                  o.org_name                                      as org_name,
//                  nvl(mr.discover_rep_id,u.THIRD_PARTY_USER_ID)   as ext_rep_id,
//                  nvl(mr.discover_rep_name,u.name)                as rep_name,
//                  mf.merchant_number                              as merchant_number,
//                  nvl(mf.dmdsnum, mpo.merchpo_card_merch_number)  as discover_merchant_number,
//                  mf.dba_name                                     as dba_name,
//                  mf.ADDR1_LINE_1                                 as address1, 
//                  mf.ADDR1_LINE_2                                 as address2,
//                  mf.DMCITY                                       as city,
//                  mf.DMSTATE                                      as state, 
//                  substr(mf.dmzip,1,5) || 
//                    '-' || substr(mf.dmzip,6,4)                   as zip_code,
//                  substr(mf.PHONE_1,1,3) || '-' ||
//                  substr(mf.phone_1,4,3) || '-' ||
//                  substr(mf.phone_1,7,4)                          as merchant_phone,            
//                  ( 
//                    decode(mf.DDSACCPT,'Y',1,0) ||
//                    decode(mf.DVSACCPT,'Y',1,0) ||
//                    decode(mf.DMCACCPT,'Y',1,0) ||
//                    decode(mf.DAMACCPT,'Y',1,0) ||
//                    decode(mf.DDCACCPT,'Y',1,0) ||
//                    decode(mf.DJCACCPT,'Y',1,0) ||
//                    '000' || -- check trunc, check verify, check guarantee
//                    decode(mf.DEBIT_PLAN,null,0,1) ||
//                    '0000'  -- other, reserved
//                  )                                               as cards_accepted,
//                  decode( at.date_completed,
//                          null, to_date(mf.date_opened,'mmddrr'),
//                          trunc(at.date_completed) )              as date_approved,
//                  to_date(mf.date_opened,'mmddrr')                as date_opened,
//                  mf.activation_date                              as date_activated,
//                  decode( substr(mf.user_data_4,15,1),
//                          'C','N','Y')                            as new_merchant,                        
//                  'N/A'                                           as new_equipment,
//                  (decode(mr.MERCH_MONTH_VISA_MC_SALES, 
//                         null, 0, 
//                         mr.MERCH_MONTH_VISA_MC_SALES) * 12)      as annual_sales,
//                  'U'                                             as disposition,
//                  me.equip_model                                  as mes_part_number,
//                  nvl(dpn.discover_part_number,'TEXC')            as part_number,
//                  null                                            as serial_number,
//                  dpn.description                                 as equip_desc,
//                  'U'                                             as discover_disposition,
//                  'E'                                             as discover_status,
//                  'P'                                             as finance_option,
//                  0                                               as base_cost,
//                  'APP'                                           as data_source,
//                  to_date(mf.date_opened,'mmddrr')                as date_shipped,
//                  mr.app_seq_num                                  as app_seq_num
//          from    parent_org                po,
//                  organization              o,
//                  group_merchant            gm,
//                  mif                       mf,
//                  merchant                  mr,
//                  merchequipment            me,                
//                  application               app,
//                  app_tracking              at,
//                  merchpayoption            mpo,
//                  users                     u,
//                  discover_part_numbers     dpn
//          where   po.parent_org_num = :orgId and
//                  o.org_num   = po.org_num and
//                  gm.org_num  = o.org_num and
//                  mf.merchant_number = gm.merchant_number and
//                  to_date(mf.date_opened,'mmddrr') between :beginDate and last_day(:endDate) and
//                  mr.merch_number  = mf.merchant_number and
//                  me.app_seq_num   = mr.app_seq_num and
//                  me.equiplendtype_code = :mesConstants.APP_EQUIP_OWNED and -- 3 and
//                  app.app_seq_num(+)  = mr.app_seq_num and
//                  at.app_seq_num(+)   = mr.app_seq_num and
//                  at.dept_code(+)     = :QueueConstants.DEPARTMENT_CREDIT and -- 100 and
//                  at.status_code(+)   = :QueueConstants.DEPT_STATUS_CREDIT_APPROVED and -- 102 and
//                  mpo.app_seq_num(+)  = mr.app_seq_num and
//                  mpo.cardtype_code(+)= :mesConstants.APP_CT_DISCOVER and -- 14 and
//                  u.user_id(+) = app.app_user_id and
//                  dpn.mes_part_number(+) = me.equip_model 
//          order by o.org_name, dba_name, merchant_number, mes_part_number, discover_disposition
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                  INDEX(po idx_parent_org_num) \n                  INDEX(gm pkgroup_merchant)\n                  INDEX(o xpkorganization)\n                */\n                to_date(mf.date_opened,'mmddrr')                as active_date,\n                o.org_num                                       as org_num,\n                o.org_group                                     as node,\n                o.org_name                                      as org_name,\n                nvl(mr.discover_rep_id,u.THIRD_PARTY_USER_ID)   as ext_rep_id,\n                nvl(mr.discover_rep_name,u.name)                as rep_name,\n                mf.merchant_number                              as merchant_number,\n                nvl(mf.dmdsnum, mpo.merchpo_card_merch_number)  as discover_merchant_number,\n                mf.dba_name                                     as dba_name,\n                mf.ADDR1_LINE_1                                 as address1, \n                mf.ADDR1_LINE_2                                 as address2,\n                mf.DMCITY                                       as city,\n                mf.DMSTATE                                      as state, \n                substr(mf.dmzip,1,5) || \n                  '-' || substr(mf.dmzip,6,4)                   as zip_code,\n                substr(mf.PHONE_1,1,3) || '-' ||\n                substr(mf.phone_1,4,3) || '-' ||\n                substr(mf.phone_1,7,4)                          as merchant_phone,            \n                ( \n                  decode(mf.DDSACCPT,'Y',1,0) ||\n                  decode(mf.DVSACCPT,'Y',1,0) ||\n                  decode(mf.DMCACCPT,'Y',1,0) ||\n                  decode(mf.DAMACCPT,'Y',1,0) ||\n                  decode(mf.DDCACCPT,'Y',1,0) ||\n                  decode(mf.DJCACCPT,'Y',1,0) ||\n                  '000' || -- check trunc, check verify, check guarantee\n                  decode(mf.DEBIT_PLAN,null,0,1) ||\n                  '0000'  -- other, reserved\n                )                                               as cards_accepted,\n                decode( at.date_completed,\n                        null, to_date(mf.date_opened,'mmddrr'),\n                        trunc(at.date_completed) )              as date_approved,\n                to_date(mf.date_opened,'mmddrr')                as date_opened,\n                mf.activation_date                              as date_activated,\n                decode( substr(mf.user_data_4,15,1),\n                        'C','N','Y')                            as new_merchant,                        \n                'N/A'                                           as new_equipment,\n                (decode(mr.MERCH_MONTH_VISA_MC_SALES, \n                       null, 0, \n                       mr.MERCH_MONTH_VISA_MC_SALES) * 12)      as annual_sales,\n                'U'                                             as disposition,\n                me.equip_model                                  as mes_part_number,\n                nvl(dpn.discover_part_number,'TEXC')            as part_number,\n                null                                            as serial_number,\n                dpn.description                                 as equip_desc,\n                'U'                                             as discover_disposition,\n                'E'                                             as discover_status,\n                'P'                                             as finance_option,\n                0                                               as base_cost,\n                'APP'                                           as data_source,\n                to_date(mf.date_opened,'mmddrr')                as date_shipped,\n                mr.app_seq_num                                  as app_seq_num\n        from    parent_org                po,\n                organization              o,\n                group_merchant            gm,\n                mif                       mf,\n                merchant                  mr,\n                merchequipment            me,                \n                application               app,\n                app_tracking              at,\n                merchpayoption            mpo,\n                users                     u,\n                discover_part_numbers     dpn\n        where   po.parent_org_num =  :1  and\n                o.org_num   = po.org_num and\n                gm.org_num  = o.org_num and\n                mf.merchant_number = gm.merchant_number and\n                to_date(mf.date_opened,'mmddrr') between  :2  and last_day( :3 ) and\n                mr.merch_number  = mf.merchant_number and\n                me.app_seq_num   = mr.app_seq_num and\n                me.equiplendtype_code =  :4  and -- 3 and\n                app.app_seq_num(+)  = mr.app_seq_num and\n                at.app_seq_num(+)   = mr.app_seq_num and\n                at.dept_code(+)     =  :5  and -- 100 and\n                at.status_code(+)   =  :6  and -- 102 and\n                mpo.app_seq_num(+)  = mr.app_seq_num and\n                mpo.cardtype_code(+)=  :7  and -- 14 and\n                u.user_id(+) = app.app_user_id and\n                dpn.mes_part_number(+) = me.equip_model \n        order by o.org_name, dba_name, merchant_number, mes_part_number, discover_disposition";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.DiscoverEquipmentDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,mesConstants.APP_EQUIP_OWNED);
   __sJT_st.setInt(5,QueueConstants.DEPARTMENT_CREDIT);
   __sJT_st.setInt(6,QueueConstants.DEPT_STATUS_CREDIT_APPROVED);
   __sJT_st.setInt(7,mesConstants.APP_CT_DISCOVER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.DiscoverEquipmentDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1003^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        merchantId  = resultSet.getLong("merchant_number");
        partNum     = resultSet.getString("mes_part_number");
        disposition = resultSet.getString("discover_disposition");
        entry       = findIPSEntry( merchantId, partNum, disposition );
        
        // if before may 1st, then ignore the platform and keep IPS
        if ( entry != null && resultSet.getDate("date_approved").before(beforeDate) )
        {
          continue;
        }
        else if ( entry != null )   // after may 1st
        {
          ReportRows.removeElement(entry);    // remove IPS and use this
        }
        
        if ( ReportType == RT_DETAILS )
        {
          ReportRows.addElement( new RowData( resultSet ) );
        }
        else    // merge same part number entries
        {
          if ( (lastMerchantId != merchantId) || 
              !(partNum.equals(lastPartNum))  || 
              !(disposition.equals(lastDisposition)) )
          {
            row = new RowData( resultSet );
            ReportRows.addElement(row);
          }
          else
          {
            row.addItem();
            row.addItemCost(resultSet.getDouble("base_cost"));
          }          
        
          // store the comparison values
          lastDisposition = disposition;
          lastMerchantId  = merchantId;
          lastPartNum     = partNum;
        }          
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         mon           = 0;
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      mon = cal.get( Calendar.MONTH );
      
      if ( mon >= Calendar.JULY && mon <= Calendar.DECEMBER )
      {
        cal.set( Calendar.MONTH, Calendar.JUNE );
        cal.set( Calendar.DAY_OF_MONTH, 1 );
        
        setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
        
        cal.set( Calendar.MONTH, Calendar.NOVEMBER );
        setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
      }
      else    // set to December => May
      {
        cal.add( Calendar.YEAR, -1 );
        cal.set( Calendar.MONTH, Calendar.DECEMBER );
        cal.set( Calendar.DAY_OF_MONTH, 1 );
        
        setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
        
        cal.set( Calendar.MONTH, Calendar.MAY );
        cal.add( Calendar.YEAR, 1 );
        setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
      }           
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/