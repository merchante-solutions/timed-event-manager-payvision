/*@lineinfo:filename=ChargebackOutgoingDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ChargebackOutgoingDataBean.sqlj $

  Description:  


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ComboDateField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class ChargebackOutgoingDataBean extends ReportSQLJBean
{
  public static final int       TT_NTCR3            = 0;
  public static final int       TT_MRCH3            = 1;
  public static final int       TT_COUNT            = 2;

  public class DetailRow
  {
    public String   ActionCode              = null;
    public String   ActionDesc              = null;
    public Date     ActionDate              = null;
    public String   CardNumber              = null;
    public String   CBRefNum                = null;
    public String   DbaName                 = null;
    public Date     IncomingDate            = null;
    public long     MerchantId              = 0L;
    public String   ReferenceNumber         = null;
    public double   TranAmount              = 0.0;

    // individual batch count/amount pairs    
    private double[]  ItemAmounts       = new double[TT_COUNT];
    private int[]     ItemCounts        = new int[TT_COUNT];
    
  
    public DetailRow( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      MerchantId      = resultSet.getLong  ("merchant_number");
      DbaName         = resultSet.getString("dba_name");
      IncomingDate    = resultSet.getDate  ("incoming_date");
      ActionDate      = resultSet.getDate  ("action_date");
      ActionCode      = resultSet.getString("action_code");
      ActionDesc      = resultSet.getString("action");
      CardNumber      = resultSet.getString("card_number");
      ReferenceNumber = resultSet.getString("ref_num");
      CBRefNum        = resultSet.getString("cb_ref_num");
      TranAmount      = resultSet.getDouble("tran_amount");
      
      ItemAmounts[TT_NTCR3] = resultSet.getDouble("ntcr3_amount");
      ItemCounts [TT_NTCR3] = resultSet.getInt   ("ntcr3_count");
      ItemAmounts[TT_MRCH3] = resultSet.getDouble("mrch3_amount");
      ItemCounts [TT_MRCH3] = resultSet.getInt   ("mrch3_count");
    }
    
    public double getMRCH3Amount()
    {
      return( ItemAmounts[TT_MRCH3] );
    }
    public int getMRCH3Count()
    {
      return( ItemCounts[TT_MRCH3] );
    }
    public double getNTCR3Amount()
    {
      return( ItemAmounts[TT_NTCR3] );
    }
    public int getNTCR3Count()
    {
      return( ItemCounts[TT_NTCR3] );
    }
  }
  
  public class SummaryRow
  {
    private double[]  ItemAmounts       = new double[TT_COUNT];
    private int[]     ItemCounts        = new int[TT_COUNT];
    public String     LoadFilename      = null;
    
    
    public SummaryRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      LoadFilename            = processString(resultSet.getString("load_filename"));
      
      ItemAmounts[TT_NTCR3] = resultSet.getDouble("ntcr3_amount");
      ItemCounts [TT_NTCR3] = resultSet.getInt   ("ntcr3_count");
      ItemAmounts[TT_MRCH3] = resultSet.getDouble("mrch3_amount");
      ItemCounts [TT_MRCH3] = resultSet.getInt   ("mrch3_count");
    }
    
    public double getMRCH3Amount()
    {
      return( ItemAmounts[TT_MRCH3] );
    }
    public int getMRCH3Count()
    {
      return( ItemCounts[TT_MRCH3] );
    }
    public double getNTCR3Amount()
    {
      return( ItemAmounts[TT_NTCR3] );
    }
    public int getNTCR3Count()
    {
      return( ItemCounts[TT_NTCR3] );
    }
  }

  
  public ChargebackOutgoingDataBean( )
  {
    super(true);
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    Field         field;

    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");
    
    fgroup.deleteField("portfolioId");
    fgroup.deleteField("zip2Node");
    fgroup.add( new HiddenField("loadFilename") );
    fgroup.add( new HiddenField("batchType") );
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    if ( getReportType() == RT_SUMMARY )
    {
      line.append("\"Filename\",");
      line.append("\"NTCR3 Count\",");
      line.append("\"NTCR3 Amount\",");
      line.append("\"MRCH3 Count\",");
      line.append("\"MRCH3 Amount\"");
    }
    else // RT_DETAILS
    {
      line.append("\"Merchant Id\",");
      line.append("\"DBA Name\",");
      line.append("\"Incoming Date\",");
      line.append("\"Action Date\",");
      line.append("\"Action\",");
      line.append("\"Card Number\",");
      line.append("\"Reference Number\",");
      line.append("\"CB Ref Num\",");
      line.append("\"Amount\",");
      line.append("\"NTCR3 Amount\",");
      line.append("\"MRCH3 Amount\"");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);

    if ( getReportType() == RT_SUMMARY )
    {
      SummaryRow  record    = (SummaryRow)obj;
    
      line.append( "\"" );
      line.append( record.LoadFilename );
      line.append( "\"," );
      line.append( record.getNTCR3Count() );
      line.append( "," );
      line.append( record.getNTCR3Amount() );
      line.append( "," );
      line.append( record.getMRCH3Count() );
      line.append( "," );
      line.append( record.getMRCH3Amount() );
    }
    else      // RT_DETAILS
    {
      DetailRow       record    = (DetailRow)obj;
    
      line.append( encodeHierarchyNode(record.MerchantId) );
      line.append( ",\"" );
      line.append( record.DbaName );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate( record.IncomingDate, "MM/dd/yyyy" ) );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate( record.ActionDate, "MM/dd/yyyy" ) );
      line.append( ",\"" );
      line.append( record.ActionDesc );
      line.append( "\",\"" );
      line.append( record.CardNumber );
      line.append( "\",\"" );
      line.append( record.ReferenceNumber );
      line.append( "\",\"" );
      line.append( record.CBRefNum );
      line.append( "\"," );
      line.append( record.TranAmount );
      line.append( "," );
      line.append( record.getNTCR3Amount() );
      line.append( "," );
      line.append( record.getMRCH3Amount() );
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Date          beginDate = getReportDateBegin();
    Date          endDate   = getReportDateEnd();
    StringBuffer  filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    if ( getReportType() == RT_SUMMARY )
    {
      filename.append("_cb_file_summary_");
    }
    else    // RT_DETAILS
    {
      filename.append("_cb_file_details_");
    }      
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(beginDate,"MMddyy") );
    if ( beginDate.equals(endDate) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(endDate,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    loadData( getReportHierarchyNode(), getReportDateBegin(), getReportDateEnd() );
  }
  
  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( getReportType() == RT_SUMMARY )
      {
        loadSummaryData( nodeId, beginDate, endDate );
      }
      else    // RT_DETAILS
      {
        loadDetailData( nodeId, beginDate, endDate );
      }
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
    }
  }
  
  public void loadDetailData( long nodeId, Date beginDate, Date endDate )
  {
    int                 batchType     = getInt("batchType");
    ResultSetIterator   it            = null;
    String              loadFilename  = getData("loadFilename");
    ResultSet           resultSet     = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:331^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.merchant_number                      as merchant_number,
//                  cb.merchant_name                        as dba_name,
//                  cb.incoming_date                        as incoming_date,                
//                  cba.action_date                         as action_date, 
//                  cba.action_code                         as action_code,
//                  nvl(ac.action_code,cba.action_code)     as action,
//                  cb.card_number                          as card_number,
//                  cb.reference_number                     as ref_num,
//                  cb.tran_amount                          as tran_amount,
//                  cb.cb_ref_num                           as cb_ref_num,
//                  cb.dt_batch_date                        as dt_batch_date, 
//                  cb.dt_batch_number                      as dt_batch_number, 
//                  cb.dt_ddf_dt_id                         as dt_ddf_dt_id,
//                  decode( nvl(ac.represent,'N'),
//                          'Y', 1, 0 )                     as ntcr3_count,        
//                  decode( nvl(ac.represent,'N'),
//                          'Y', cb.tran_amount, 0 )        as ntcr3_amount,
//                  decode( nvl(ac.adj_tran_code,0),
//                          '9071', 1,
//                          '9077', 1, 
//                          0 )                             as mrch3_count,                             
//                  decode( nvl(ac.adj_tran_code,0),
//                               '9071', -cb.tran_amount,
//                               '9077', cb.tran_amount,                             
//                               0 )                        as mrch3_amount                             
//          from    organization                  o,
//                  group_merchant                gm,
//                  network_chargebacks           cb,
//                  network_chargeback_activity   cba,
//                  chargeback_action_codes       ac
//          where   o.org_group = :nodeId and
//                  gm.org_num = o.org_num and
//                  cb.merchant_number = gm.merchant_number and
//                  cb.incoming_date >= (sysdate-180) and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.load_filename = :loadFilename and
//                  ac.short_action_code(+) = cba.action_code and
//                  (
//                    :batchType = 0 or
//                    ( :batchType = 1 and nvl(ac.represent,'N') = 'Y' ) or
//                    ( :batchType = 2 and nvl(ac.adj_tran_code,0) != 0 )
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.merchant_number                      as merchant_number,\n                cb.merchant_name                        as dba_name,\n                cb.incoming_date                        as incoming_date,                \n                cba.action_date                         as action_date, \n                cba.action_code                         as action_code,\n                nvl(ac.action_code,cba.action_code)     as action,\n                cb.card_number                          as card_number,\n                cb.reference_number                     as ref_num,\n                cb.tran_amount                          as tran_amount,\n                cb.cb_ref_num                           as cb_ref_num,\n                cb.dt_batch_date                        as dt_batch_date, \n                cb.dt_batch_number                      as dt_batch_number, \n                cb.dt_ddf_dt_id                         as dt_ddf_dt_id,\n                decode( nvl(ac.represent,'N'),\n                        'Y', 1, 0 )                     as ntcr3_count,        \n                decode( nvl(ac.represent,'N'),\n                        'Y', cb.tran_amount, 0 )        as ntcr3_amount,\n                decode( nvl(ac.adj_tran_code,0),\n                        '9071', 1,\n                        '9077', 1, \n                        0 )                             as mrch3_count,                             \n                decode( nvl(ac.adj_tran_code,0),\n                             '9071', -cb.tran_amount,\n                             '9077', cb.tran_amount,                             \n                             0 )                        as mrch3_amount                             \n        from    organization                  o,\n                group_merchant                gm,\n                network_chargebacks           cb,\n                network_chargeback_activity   cba,\n                chargeback_action_codes       ac\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                cb.merchant_number = gm.merchant_number and\n                cb.incoming_date >= (sysdate-180) and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.load_filename =  :2  and\n                ac.short_action_code(+) = cba.action_code and\n                (\n                   :3  = 0 or\n                  (  :4  = 1 and nvl(ac.represent,'N') = 'Y' ) or\n                  (  :5  = 2 and nvl(ac.adj_tran_code,0) != 0 )\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ChargebackOutgoingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setInt(3,batchType);
   __sJT_st.setInt(4,batchType);
   __sJT_st.setInt(5,batchType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ChargebackOutgoingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:375^7*/
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        ReportRows.add( new DetailRow( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadDetailData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadSummaryData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:404^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cba.load_filename                       as load_filename,
//                  sum( decode( nvl(ac.represent,'N'),
//                               'Y', 1,
//                               0 ) )                      as ntcr3_count,        
//                  sum( decode( nvl(ac.represent,'N'),
//                               'Y', cb.tran_amount,
//                               0 ) )                      as ntcr3_amount,
//                  sum( decode( nvl(ac.adj_tran_code,0),
//                               '9071', 1,
//                               '9077', 1, 
//                               0 ) )                      as mrch3_count,                             
//                  sum( decode( nvl(ac.adj_tran_code,0),
//                               '9071', -cb.tran_amount,
//                               '9077', cb.tran_amount,                             
//                               0 ) )                      as mrch3_amount                             
//          from    organization                  o,
//                  group_merchant                gm,
//                  network_chargebacks           cb,
//                  network_chargeback_activity   cba,
//                  chargeback_action_codes       ac
//          where   o.org_group = :nodeId and
//                  gm.org_num = o.org_num and
//                  cb.merchant_number = gm.merchant_number and
//                  cb.incoming_date between (:beginDate-180) and (:endDate+1) and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.load_filename like 'wcb%' and
//                  to_date(substr(cba.load_filename,9,6),'mmddrr') 
//                    between :beginDate and :endDate and
//                  ac.short_action_code(+) = cba.action_code
//          group by cba.load_filename      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cba.load_filename                       as load_filename,\n                sum( decode( nvl(ac.represent,'N'),\n                             'Y', 1,\n                             0 ) )                      as ntcr3_count,        \n                sum( decode( nvl(ac.represent,'N'),\n                             'Y', cb.tran_amount,\n                             0 ) )                      as ntcr3_amount,\n                sum( decode( nvl(ac.adj_tran_code,0),\n                             '9071', 1,\n                             '9077', 1, \n                             0 ) )                      as mrch3_count,                             \n                sum( decode( nvl(ac.adj_tran_code,0),\n                             '9071', -cb.tran_amount,\n                             '9077', cb.tran_amount,                             \n                             0 ) )                      as mrch3_amount                             \n        from    organization                  o,\n                group_merchant                gm,\n                network_chargebacks           cb,\n                network_chargeback_activity   cba,\n                chargeback_action_codes       ac\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                cb.merchant_number = gm.merchant_number and\n                cb.incoming_date between ( :2 -180) and ( :3 +1) and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.load_filename like 'wcb%' and\n                to_date(substr(cba.load_filename,9,6),'mmddrr') \n                  between  :4  and  :5  and\n                ac.short_action_code(+) = cba.action_code\n        group by cba.load_filename";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ChargebackOutgoingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.ChargebackOutgoingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:436^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new SummaryRow( resultSet ) );
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadSummaryData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest( request );
  
    if ( HttpHelper.getString(request,"beginDate.month",null) == null )
    {
      Calendar cal = Calendar.getInstance();
    
      cal.add( Calendar.DAY_OF_MONTH, -1 );
    
      ((ComboDateField)getField("beginDate")).setUtilDate( cal.getTime() );
      ((ComboDateField)getField("endDate")).setUtilDate( cal.getTime() );
    }
  }
}/*@lineinfo:generated-code*/