/*@lineinfo:filename=MerchantServiceCalls*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchantServiceCalls.sqlj $

  Description:

  Extends the date range report bean to generate a sales activity report.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/26/04 4:48p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.maintenance.CallTrackingBean;
import sqlj.runtime.ResultSetIterator;

public class MerchantServiceCalls extends ReportSQLJBean
{
  public Vector     reportResults     = null;

  public MerchantServiceCalls()
  {
  }

  public void loadData()
  {
    ResultSetIterator     it                = null;
    String                callDescription   = "";

    try
    {
      reportResults     = new Vector();

      /*@lineinfo:generated-code*//*@lineinfo:56^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    sc.merchant_number    merchant_number,
//                    sc.call_date          call_date,
//                    sc.type               type,
//                    sc.other_description  other_description,
//                    sc.login_name         login_name,
//                    sc.call_back          call_back,
//                    sc.status             status,
//                    sc.notes              notes,
//                    sc.close_date         close_date,
//                    sc.close_notes        close_notes,
//                    sc.sequence           sequence,
//                    sct.description       type_description,
//                    scs.description       status_description,
//                    sc.billable           billable,
//                    sc.client_generated   client_generated,
//                    sc.display_bold       display_bold
//          from      service_calls sc,
//                    service_call_types sct,
//                    service_call_statuses scs
//          where     sc.merchant_number = :getReportHierarchyNode() and
//                    nvl(sc.hidden, 'N') != 'Y' and
//                    trunc(sc.call_Date) between :ReportDateBegin and :ReportDateEnd and
//                    sc.type = sct.type and
//                    sc.status = scs.status
//          order by  call_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1215 = getReportHierarchyNode();
  try {
   String theSqlTS = "select    sc.merchant_number    merchant_number,\n                  sc.call_date          call_date,\n                  sc.type               type,\n                  sc.other_description  other_description,\n                  sc.login_name         login_name,\n                  sc.call_back          call_back,\n                  sc.status             status,\n                  sc.notes              notes,\n                  sc.close_date         close_date,\n                  sc.close_notes        close_notes,\n                  sc.sequence           sequence,\n                  sct.description       type_description,\n                  scs.description       status_description,\n                  sc.billable           billable,\n                  sc.client_generated   client_generated,\n                  sc.display_bold       display_bold\n        from      service_calls sc,\n                  service_call_types sct,\n                  service_call_statuses scs\n        where     sc.merchant_number =  :1  and\n                  nvl(sc.hidden, 'N') != 'Y' and\n                  trunc(sc.call_Date) between  :2  and  :3  and\n                  sc.type = sct.type and\n                  sc.status = scs.status\n        order by  call_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchantServiceCalls",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1215);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchantServiceCalls",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:83^7*/

      ResultSet rs = it.getResultSet();
      CallTrackingBean  ctb = new CallTrackingBean();

      while(rs.next())
      {
        // determine the call description from the call type
        int callType = rs.getInt("type");

        if(callType == mesConstants.CALL_TYPE_OTHER)
        {
          callDescription = rs.getString("other_description");
        }
        else
        {
          callDescription = rs.getString("type_description");
        }

        
        reportResults.add(ctb.new MerchantCallDetail(
                                  rs.getString("merchant_number"),
                                  rs.getDate  ("call_date"),
                                  rs.getTime  ("call_date"),
                                  callDescription,
                                  rs.getString("login_name"),
                                  rs.getString("call_back"),
                                  rs.getString("status_description"),
                                  rs.getString("notes"),
                                  rs.getDate  ("close_date"),
                                  rs.getString("close_notes"),
                                  rs.getString("sequence"),
                                  (rs.getString("billable")!=null&&rs.getString("billable").equals("N")?false:true),
                                  (rs.getString("client_generated")!=null&&rs.getString("client_generated").equals("Y")?true:false),
                                  (rs.getString("display_bold")!=null&&rs.getString("display_bold").equals("Y")?true:false)
                                  ));
      }
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }
  }

  public Vector getReportResults()
  {
    return this.reportResults;
  }

  public void setProperties(HttpServletRequest request)
  {
    super.setProperties(request);

    Calendar cal = Calendar.getInstance();

    if ( usingDefaultReportDates() )
    {
      // setup the default date range
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, -30 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }
  }
}/*@lineinfo:generated-code*/