/*@lineinfo:filename=MerchChargebackRetrievalDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/MerchChargebackRetrievalDataBean.sqlj $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-03-22 17:05:24 -0700 (Tue, 22 Mar 2011) $
  Version            : $Revision: 18599 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class MerchChargebackRetrievalDataBean extends ReportSQLJBean
{
  public static class CardTypeTable extends DropDownTable
  {
    public CardTypeTable( )
    {
      addElement( "ALL", "All" );
      addElement( "VS", "Visa" );
      addElement( "MC", "MasterCard" );
    }
  }

  public class ChargebackRetrievalSummary
  {
    protected double      ItemAmountFirstTime     = 0.0;
    protected double      ItemAmountTotal         = 0.0;
    protected double      ItemAmountWorked        = 0.0;
    protected int         ItemCountFirstTime      = 0;
    protected int         ItemCountTotal          = 0;
    protected int         ItemCountWorked         = 0;
    protected long        NodeId                  = 0L;
    protected long        OrgId                   = 0L;
    protected String      OrgName                 = "";

    public ChargebackRetrievalSummary( ResultSet resultSet, boolean initCountAmount )
    {
      initialize( resultSet, initCountAmount );
    }

    public ChargebackRetrievalSummary( ResultSet resultSet )
      throws java.sql.SQLException
    {
      initialize( resultSet, true );
    }

    public ChargebackRetrievalSummary( long nodeId, long orgId, String orgName, int count, double amount )
    {
      OrgId             = orgId;
      OrgName           = orgName;
      ItemCountTotal    = count;
      ItemAmountTotal   = amount;
      NodeId            = nodeId;
    }

    public ChargebackRetrievalSummary( long nodeId, long orgId, String orgName )
    {
      OrgId             = orgId;
      OrgName           = orgName;
      NodeId            = nodeId;
    }

    public void addFirstTimeAmount( double amount )
    {
      ItemAmountFirstTime += amount;
    }

    public void addFirstTimeCount( int count )
    {
      ItemCountFirstTime += count;
    }

    public void addTotalAmount( double amount )
    {
      ItemAmountTotal += amount;
    }

    public void addWorkedAmount( double amount )
    {
      ItemAmountWorked += amount;
    }

    public void addTotalCount( int count )
    {
      ItemCountTotal += count;
    }

    public void addWorkedCount( int count )
    {
      ItemCountWorked += count;
    }

    public double getItemAmountTotal( )
    {
      return( ItemAmountTotal );
    }

    public double getItemAmountFirstTime( )
    {
      return( ItemAmountFirstTime );
    }

    public double getItemAmountSecondTime( )
    {
      return( ItemAmountTotal - ItemAmountFirstTime );
    }

    public double getItemAmountWorked( )
    {
      return( ItemAmountWorked );
    }

    public int getItemCountTotal ( )
    {
      return( ItemCountTotal   );
    }

    public int getItemCountFirstTime( )
    {
      return( ItemCountFirstTime );
    }

    public int getItemCountSecondTime( )
    {
      return( ItemCountTotal - ItemCountFirstTime );
    }

    public int getItemCountWorked( )
    {
      return( ItemCountWorked );
    }

    public long getNodeId( )
    {
      return( NodeId );
    }

    public long getOrgId( )
    {
      return( OrgId );
    }

    public String getOrgName( )
    {
      return( OrgName );
    }

    private void initialize( ResultSet resultSet, boolean initCountAmount )
    {
      try
      {
        NodeId          = resultSet.getLong("hierarchy_node");
        OrgId           = resultSet.getLong("org_num");
        OrgName         = resultSet.getString("org_name");

        if ( initCountAmount == true )
        {
          ItemCountTotal  = resultSet.getInt("item_count");
          ItemAmountTotal = resultSet.getDouble("item_amount");
        }
      }
      catch( java.sql.SQLException e )
      {
        logEntry( "initialize()",e.toString() );
      }
    }
  }

  public class ChargebackByReasonSummary extends ChargebackRetrievalSummary
  {
    private     String                CardType      = null;
    private     String                ReasonCode    = null;
    private     String                ReasonDesc    = null;
    private     boolean               RebutActive   = true;

    public ChargebackByReasonSummary( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super(resultSet);

      CardType   = resultSet.getString("card_type");
      ReasonCode = resultSet.getString("reason_code");
      ReasonDesc = resultSet.getString("reason_desc");
      RebutActive= resultSet.getBoolean("rebut_active");
    }

    public String getCardType( )
    {
      return( CardType );
    }

    public String getReasonCode( )
    {
      return( ReasonCode );
    }

    public String getReasonDesc( )
    {
      return( ReasonDesc );
    }

    public boolean rebutActive()
    {
      return RebutActive;
    }
  }

  abstract private class ChargebackRetrievalData implements Comparable
  {
    public double         Amount            = 0.0;
    public String         AuthCode          = null;
    public String         CardNumber        = null;
    public String         CardNumberEnc     = null;
    public String         Description       = null;
    public Date           IncomingDate      = null;
    public long           MerchantId        = 0L;
    public String         MerchantName      = null;
    public int            NoteCount         = 0;
    public String         ReferenceNumber   = null;
    public String         SaleUrl           = null;
    public String         Status            = null;
    public Date           TranDate          = null;
    public String         TridentTranId     = null;

    public ChargebackRetrievalData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      MerchantId        = resultSet.getLong("merchant_number");
      MerchantName      = processString(resultSet.getString("merchant_name"));
      IncomingDate      = resultSet.getDate("incoming_date");
      Status            = resultSet.getString("reason_code");
      Description       = resultSet.getString("reason_desc");
      Amount            = resultSet.getDouble("tran_amount");
      CardNumber        = resultSet.getString("card_number");
      TranDate          = resultSet.getDate("tran_date");
      ReferenceNumber   = processString(resultSet.getString("ref_num"));
      AuthCode          = processString(resultSet.getString("auth_code"));

      try
      {
        CardNumberEnc = resultSet.getString("card_number_enc");
      }
      catch(Exception e)
      {
        CardNumberEnc = null;
      }

      try
      {
        // column is available only in some queries
        NoteCount = resultSet.getInt("note_count");
      }
      catch( Exception e )
      {
      }

      try
      {
        TridentTranId = resultSet.getString("trident_tran_id");
      }
      catch( Exception e )
      {
      }

      try
      {
        if ( resultSet.getInt("tran_data_valid") == 1 )
        {
          StringBuffer temp = new StringBuffer();
          temp.append("/jsp/utils/find_transaction.jsp?dataType=0&reportType=1&findTran=true");
          temp.append("&findHierarchyNode=");
          temp.append(resultSet.getLong("merchant_number"));
          temp.append("&reportDateBegin=");
          temp.append(DateTimeFormatter.getFormattedDate(resultSet.getDate("dt_batch_date"),"MM/dd/yyyy"));
          temp.append("&reportDateEnd=");
          temp.append(DateTimeFormatter.getFormattedDate(resultSet.getDate("dt_batch_date"),"MM/dd/yyyy"));
          temp.append("&findCardNum=");
          temp.append(CardNumber);

          SaleUrl = temp.toString();
        }
      }
      catch( Exception e )
      {
      }
    }

    abstract public int compareTo( Object object );

    public void showData( java.io.PrintStream out )
    {
      String          amountStr = null;
      int             i;
      NumberFormat    numFormat = NumberFormat.getInstance();

      numFormat.setMinimumIntegerDigits(2);

      out.print( MerchantId );
      out.print( "  " );
      out.print( IncomingDate );
      out.print( "  " );
      out.print( TranDate );
      out.print( "  " );
      out.print( CardNumber );
      out.print( "  " );
      amountStr = MesMath.toCurrency(Amount);
      for( i = amountStr.length(); i < 12; ++i )
      {
        out.print( " " );
      }
      out.print( amountStr );
      out.print( "  " );
      out.print( ReferenceNumber );
      out.print( "  " );
      out.print( Status );
      out.print( "  " );
      out.print( Description );
      out.println();
    }
  }

  public class ChargebackRecord extends ChargebackRetrievalData
  {
    public  Date        AdjustmentDate          = null;
    public  String      AdjustmentRefNum        = null;
    public  Date        AdjustmentDateCredit    = null;
    public  String      ChallengeRequirements   = null;
    public  String      PurchaseId              = null;
    private String      actionCode              = "";

    private long        cbId                    = 0L;
    private Date        rebuttalDate            = null;
    private String      rebuttalStatus          = null;
    private String      rebuttalDocStatus       = null;
    private String      rebuttalNotes           = null;
    private int         daysOut                 = 0;
    private String      firstTimeCB             = null;
    public boolean      rebutActive             = true;

    public ChargebackRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );

      if ( resultSet.getString("adj_debit_credit_ind").equals("C") )
      {
        setCreditAdjustmentData(resultSet);
      }
      else
      {
        setDebitAdjustmentData(resultSet);
      }

      AdjustmentRefNum      = processString(resultSet.getString("adj_ref_num"));
      ChallengeRequirements = processString(resultSet.getString("challenge_requirements"));
      actionCode        = resultSet.getString("action_code");
      cbId              = resultSet.getLong("mes_ref_num");
      PurchaseId        = processString(resultSet.getString("purchase_id"));
      rebuttalDate      = resultSet.getDate("reb_date");
      rebuttalStatus    = processString(resultSet.getString("reb_status"));
      rebuttalDocStatus = processString(resultSet.getString("reb_doc"));
      rebuttalNotes     = processString(resultSet.getString("reb_notes"));
      daysOut           = resultSet.getInt("time_dif");
      firstTimeCB       = processString(resultSet.getString("first_time"));
      rebutActive       = resultSet.getBoolean("rebut_active");
    }

    public int compareTo( Object object )
    {
      ChargebackRecord  entry = (ChargebackRecord) object;
      int                     retVal;

      // sorted by merchant id, incoming date (desc), card #, ref #
      if ( ( retVal = (int)(this.MerchantId - entry.MerchantId) ) == 0 )
      {
        if ( ( retVal = entry.IncomingDate.compareTo( this.IncomingDate ) ) == 0 )
        {
          if ( ( retVal = this.CardNumber.compareTo( entry.CardNumber ) ) == 0 )
          {
            retVal = (int)(this.ReferenceNumber.compareTo(entry.ReferenceNumber));
          }
        }
      }
      return( retVal );
    }

    public java.util.Date getDueDate( )
    {
      Calendar    cal   = Calendar.getInstance();

      cal.setTime( IncomingDate );
      cal.add( Calendar.DAY_OF_MONTH, + 14 );
      return( cal.getTime() );
    }

    public boolean hasMultipleAdjustments( )
    {
      return( AdjustmentDate != null && AdjustmentDateCredit != null );
    }

    public void setCreditAdjustmentData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AdjustmentDateCredit    = resultSet.getDate("adj_date");
    }

    public String getStatusCode()
    {
      return actionCode;
    }

    public void setStatusCode(String statusCode)
    {
      this.actionCode = statusCode;
    }

    public long getCBId()
    {
      return cbId;
    }

    public void setDebitAdjustmentData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AdjustmentDate    = resultSet.getDate("adj_date");
    }

    public Date getRebuttalDate()
    {
      return rebuttalDate;
    }

    public String getRebuttalStatus()
    {
      return rebuttalStatus;
    }

    public String getRebuttalDocStatus()
    {
      return rebuttalDocStatus;
    }

    public String getRebuttalNotes()
    {
      return rebuttalNotes;
    }

    public int getDaysOut()
    {
      return daysOut;
    }

    public boolean isRebuttalOK()
    {
      return daysOut <= 40 &&
        ( actionCode.equals("MCHB")||actionCode.equals("MCHR")||
          actionCode.equals("B") || actionCode.equals("D"));
    }

    public String getFirstTime()
    {
      return firstTimeCB;
    }

  }

  public class RetrievalRecord extends ChargebackRetrievalData
  {
    public Date          ReceivedDate    = null;
    public long          RetrievalId     = 0L;
    public boolean       isAmex          = false;

    public RetrievalRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );

      ReceivedDate  = resultSet.getDate("received_date");
      RetrievalId   = resultSet.getLong("retr_load_sec");

      if("AM".equals(resultSet.getString("card_type")))
      {
        isAmex = true;
      }

    }

    public java.util.Date getDueDate( )
    {
      Calendar    cal   = Calendar.getInstance();

      cal.setTime( IncomingDate );
      cal.add( Calendar.DAY_OF_MONTH, +14 );
      return( cal.getTime() );
    }

    public int compareTo( Object object )
    {
      RetrievalRecord     entry = (RetrievalRecord) object;
      int                 retVal;

      // sorted by merchant id, status code, incoming date (desc), card #, ref #
      if ( ( retVal = (int)(this.MerchantId - entry.MerchantId) ) == 0 )
      {
        if ( ( retVal = this.Status.compareTo( entry.Status ) ) == 0 )
        {
          if ( ( retVal = entry.IncomingDate.compareTo( this.IncomingDate ) ) == 0 )
          {
            if ( ( retVal = this.CardNumber.compareTo( entry.CardNumber ) ) == 0 )
            {
              retVal = (int)(this.ReferenceNumber.compareTo(entry.ReferenceNumber));
            }
          }
        }
      }
      return( retVal );
    }
  }

  public static final int     DT_CB_ADJUSTMENTS         = 0;
  public static final int     DT_CB_PRENOTIFICATIONS    = 1;
  public static final int     DT_RETRIEVALS             = 2;
  public static final int     DT_CB_STATISTICS          = 3;
  public static final int     DT_CB_BY_REASON           = 4;
  public static final int     DT_COUNT                  = 5;

  public MerchChargebackRetrievalDataBean( )
  {
    super(true);    // use field bean
  }

  protected void createFields(HttpServletRequest request)
  {
    FieldGroup        fgroup          = null;
    Field             field           = null;

    super.createFields(request);

    fgroup = (FieldGroup)getField("searchFields");

    // remove unused fields

    // add new fields
    DropDownField dropDownField = new DropDownField("cardType","Card Type", new CardTypeTable(),false);
    dropDownField.setSelectedItem(0);
    fgroup.add(dropDownField);

    fgroup.add( new HiddenField("reportDataType",String.valueOf(DT_CB_ADJUSTMENTS)) );
    fgroup.add( new HiddenField("includeTridentTranId",String.valueOf(false)) );
    fgroup.add( new HiddenField("oneRowPerAdjustment",String.valueOf(false)) );
    fgroup.add( new HiddenField("includePurchaseId",String.valueOf(false)) );
  }

  public void encodeDetailsOrgUrl( StringBuffer buffer, long orgId )
  {
    super.encodeOrgUrl( buffer, orgId );
    buffer.append("&reportDataType=");
    buffer.append(getReportDataType());
    buffer.append("&reportType=");
    buffer.append(RT_DETAILS);
    buffer.append("&cardType=");
    buffer.append(getData("cardType"));
  }

  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);

    if ( getReportType() == RT_SUMMARY )
    {
      line.append("\"Org Id\",");
      line.append("\"Org Name\",");

      switch( getReportDataType() )
      {
        case  DT_CB_PRENOTIFICATIONS:
          line.append("\"First Time Count\",");
          line.append("\"First Time Amount\",");
          line.append("\"Second Time Count\",");
          line.append("\"Second Time Amount\",");

        case DT_CB_BY_REASON:
          line.append("\"Card Type\",");
          line.append("\"Reason Code\",");
          line.append("\"Reason Desc\",");
          break;

//        case DT_CB_ADJUSTMENTS:
//        case DT_RETRIEVALS:
        default:
          break;
      }
      line.append("\"Total Count\",");
      line.append("\"Total Amount\"");
    }
    else    // RT_DETAILS
    {
      line.append("\"Merchant Id\",");
      line.append("\"DBA Name\",");
      line.append("\"Control Number\",");
      line.append("\"Incoming Date\",");
      line.append("\"Card Number\",");
      line.append("\"Reference Number\",");
      line.append("\"Tran Date\",");
      line.append("\"Tran Amount\"");

      switch( getReportDataType() )
      {
        case DT_CB_PRENOTIFICATIONS:
        case DT_CB_ADJUSTMENTS:
          line.append(",");
          if ( includeTridentTranId() )
          {
            line.append("\"Trident Tran ID\",");
          }
          if( includePurchaseId() )
          {
            line.append("\"Purchase ID\",");
          }
          line.append("\"Auth Code\",");
          line.append("\"Adj Date\",");
          if ( isOneRowPerAdjustment() )
          {
            line.append("\"Adj Type\",");
          }
          line.append("\"Adj Ref Num\",");
          line.append("\"Reason\",");
          line.append("\"First Time\"");
          if ( getReportDataType() == DT_CB_PRENOTIFICATIONS )
          {
            line.append(",\"Due Date\",");
            line.append("\"Requested Documents\"");
          }
          break;

        case DT_RETRIEVALS:
          line.append(",\"Due Date\"");
          break;
      }
    }
  }

  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);

    if ( getReportType() == RT_SUMMARY )
    {
      ChargebackRetrievalSummary    summary   = (ChargebackRetrievalSummary)obj;

      line.append( encodeHierarchyNode(summary.NodeId) );
      line.append( ",\"" );
      line.append( summary.OrgName );
      line.append( "\"," );
      if ( getReportDataType() == DT_CB_PRENOTIFICATIONS )
      {
        line.append( summary.getItemCountFirstTime() );
        line.append( "," );
        line.append( summary.getItemAmountFirstTime() );
        line.append( "," );
        line.append( summary.getItemCountSecondTime() );
        line.append( "," );
        line.append( summary.getItemAmountSecondTime() );
        line.append( "," );
      }
      else if ( getReportDataType() == DT_CB_BY_REASON )
      {
        ChargebackByReasonSummary byReason  = (ChargebackByReasonSummary)obj;

        line.append( byReason.getCardType() );
        line.append( "," );
        line.append( byReason.getReasonCode() );
        line.append( ",\"" );
        line.append( byReason.getReasonDesc() );
        line.append( "\",");
      }
      line.append( summary.getItemCountTotal() );
      line.append( "," );
      line.append( summary.getItemAmountTotal() );
    }
    else      // RT_DETAILS
    {
      ChargebackRetrievalData     details = (ChargebackRetrievalData)obj;

      line.append( encodeHierarchyNode( details.MerchantId ) );
      line.append( ",\"" );
      line.append( details.MerchantName );
      line.append( "\"," );
      if ( details instanceof ChargebackRecord )
      {
        line.append( ((ChargebackRecord)details).getCBId() );
      }
      else if ( details instanceof RetrievalRecord )
      {
        line.append( ((RetrievalRecord)details).RetrievalId );
      }
      else
      {
        line.append("0");   // default to 0
      }
      line.append(",");
      line.append( DateTimeFormatter.getFormattedDate( details.IncomingDate, "MM/dd/yyyy" ) );
      line.append( "," );
      line.append( details.CardNumber );
      line.append( ",'" );
      line.append( details.ReferenceNumber );
      line.append( "," );
      line.append( DateTimeFormatter.getFormattedDate( details.TranDate, "MM/dd/yyyy" ) );
      line.append( "," );
      line.append( details.Amount );
      line.append( "," );

      switch( getReportDataType() )
      {
        case DT_CB_PRENOTIFICATIONS:
        case DT_CB_ADJUSTMENTS:
        {
          ChargebackRecord    cbData        = (ChargebackRecord) details;
          boolean             oneRowPerAdj  = isOneRowPerAdjustment();
          StringBuffer        secondLine    = null;

          if ( includeTridentTranId() )
          {
            line.append( cbData.TridentTranId );
            line.append( "," );
          }
          if ( includePurchaseId() )
          {
            line.append( cbData.PurchaseId );
            line.append( "," );
          }
          line.append( cbData.AuthCode );
          line.append( ",\"" );

          if ( oneRowPerAdj )
          {
            if ( cbData.hasMultipleAdjustments() )
            {
              secondLine = new StringBuffer( line.toString() );
              secondLine.append( DateTimeFormatter.getFormattedDate( cbData.AdjustmentDateCredit, "MM/dd/yyyy" ) );
              secondLine.append( "\",\"C" );  // credit adjustment
            }
            line.append( DateTimeFormatter.getFormattedDate( cbData.AdjustmentDate, "MM/dd/yyyy" ) );
            line.append( "\",\"" );  // debit adjustment
            line.append( (cbData.AdjustmentDate == null) ? "" : "D" );
          }
          else
          {
            if ( cbData.AdjustmentDate != null )
            {
              line.append( DateTimeFormatter.getFormattedDate( cbData.AdjustmentDate, "MM/dd/yyyy" ) );
              line.append( (oneRowPerAdj ? "" : "-") );
            }
            if ( cbData.AdjustmentDateCredit != null )
            {
              line.append( (cbData.AdjustmentDate == null) ? "" : " | " );
              line.append( DateTimeFormatter.getFormattedDate( cbData.AdjustmentDateCredit, "MM/dd/yyyy" ) );
              line.append( (oneRowPerAdj ? "" : "+") );
            }
          }

          for ( int lineIdx = 0; lineIdx < (secondLine == null ? 1 : 2); ++lineIdx )
          {
            StringBuffer outputLine = (lineIdx == 0 ? line : secondLine);
            outputLine.append( "\",\"" );
            outputLine.append( (cbData.AdjustmentDate == null) ? "" : cbData.AdjustmentRefNum );
            outputLine.append( "\",\"" );
            outputLine.append( cbData.Description );
            outputLine.append( "\"," );
            outputLine.append( cbData.getFirstTime() );

            if ( getReportDataType() == DT_CB_PRENOTIFICATIONS )
            {
              outputLine.append( "," );
              outputLine.append( DateTimeFormatter.getFormattedDate( cbData.getDueDate(), "MM/dd/yyyy" ) );
              outputLine.append( ",\"" );
              outputLine.append( cbData.ChallengeRequirements );
              outputLine.append( "\"" );
            }
          }
          if ( secondLine != null )
          {
            line.append("\n");
            line.append(secondLine.toString());
          }
          cbData = null;    // encourage garbage collection
          break;
        }

        case DT_RETRIEVALS:
          line.append( DateTimeFormatter.getFormattedDate( ((RetrievalRecord)obj).getDueDate(), "MM/dd/yyyy" ) );
          break;
      }
    }
  }

  public void encodeOrgUrl( StringBuffer buffer, long orgId )
  {
    super.encodeOrgUrl( buffer, orgId );
    buffer.append("&reportDataType=");
    buffer.append(getReportDataType());
    buffer.append("&reportType=");
    buffer.append(getReportType());
    buffer.append("&cardType=");
    buffer.append(getData("cardType"));
  }

  public void encodeSummaryOrgUrl( StringBuffer buffer, long orgId )
  {
    super.encodeOrgUrl( buffer, orgId );
    buffer.append("&reportDataType=");
    buffer.append(getReportDataType());
    buffer.append("&reportType=");
    buffer.append(RT_SUMMARY);
    buffer.append("&cardType=");
    buffer.append(getData("cardType"));
  }

  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");

    filename.append( getReportHierarchyNode() );
    if ( getReportType() == RT_SUMMARY )
    {
      switch( getReportDataType() )
      {
        case DT_CB_PRENOTIFICATIONS:
          filename.append("_cb_prenote");
          break;
        case DT_CB_ADJUSTMENTS:
          filename.append("_cb_adj");
          break;
        case DT_RETRIEVALS:
          filename.append("_retrieval");
          break;
      }
      filename.append("_summary_");
    }
    else    // RT_DETAILS
    {
      switch( getReportDataType() )
      {
        case DT_CB_PRENOTIFICATIONS:
          filename.append("_cb_prenote");
          break;
        case DT_CB_ADJUSTMENTS:
          filename.append("_cb_adj");
          break;
        case DT_RETRIEVALS:
          filename.append("_retrieval");
          break;
      }
      filename.append("_details_");
    }

    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( getReportDateBegin(), "MMddyyyy" ) );
    if ( getReportDateBegin().equals(getReportDateEnd()) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( getReportDateEnd(), "MMddyyyy" ) );
    }
    return ( filename.toString() );
  }

  public int getReportDataType( )
  {
    return( getInt("reportDataType") );
  }

  public String getReportTitle()
  {
    String      retVal = "No Title Available";

    switch( getReportDataType() )
    {
      case DT_CB_ADJUSTMENTS:
        retVal = "Chargeback Adjustment " + ((getReportType() == RT_SUMMARY) ? "Summary" : "Details");
        break;
      case DT_CB_PRENOTIFICATIONS:
        retVal = "Chargeback Pre-Notification " + ((getReportType() == RT_SUMMARY) ? "Summary" : "Details");
        break;
      case DT_RETRIEVALS:
        retVal = "Retrieval Request " + ((getReportType() == RT_SUMMARY) ? "Summary" : "Details");
        break;
      case DT_CB_STATISTICS:
        retVal = "Chargeback Statistics " + ((getReportType() == RT_SUMMARY) ? "Summary" : "Details");
        break;
    }
    return( retVal );
  }

  public String getSummaryAmountHeading( )
  {
    return("$ " + getSummaryHeading());
  }

  public String getSummaryCountHeading( )
  {
    return("# " + getSummaryHeading());
  }

  protected String getSummaryHeading( )
  {
    String      retVal = "";

    switch( getReportDataType() )
    {
      case DT_CB_ADJUSTMENTS:
        retVal = "Chargeback Adjustments";
        break;
      case DT_CB_PRENOTIFICATIONS:
        retVal = "Pre-Notification Chargebacks";
        break;
      case DT_RETRIEVALS:
        retVal = "Retrieval Requests";
        break;
      case DT_CB_STATISTICS:
        retVal = "Chargebacks";
        break;
    }
    return( retVal );
  }

  public boolean includeTridentTranId()
  {
    return( getBoolean("includeTridentTranId") );
  }
  
  public boolean includePurchaseId()
  {
    return( getBoolean("includePurchaseId") );
  }

  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }

  public boolean isOneRowPerAdjustment()
  {
    return( getBoolean("oneRowPerAdjustment") );
  }

  public void loadChargebackAdjustments( int reportType, long orgId, Date beginDate, Date endDate )
  {
    String                        cardType          = getData("cardType");
    ResultSetIterator             it                = null;
    long                          lastLoadSec       = -1L;
    long                          merchantId        = 0L;
    ChargebackRecord              record            = null;
    ResultSet                     resultSet         = null;

    try
    {
      // empty the current contents
      ReportRows.clear();

      if ( reportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1003^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+
//                      index (gm pkgroup_merchant)
//                      ordered
//                      use_nl(gm cb)
//                    */
//                   cb.merchant_number                   as merchant_number,
//                   cb.merchant_name                     as merchant_name,
//                   cb.cb_load_sec                       as mes_ref_num,
//                   cb.incoming_date                     as incoming_date,
//                   cb.REASON_CODE                       as reason_code,
//                   nvl(cb.first_time_chargeback,'Y')    as first_time,
//                   nvl(rd.REASON_DESC,
//                       'REASON CODE: '||cb.reason_code) as reason_desc,
//                   decode(nvl(rd.uta,'N'),'Y',0,1)      as rebut_active,
//                   cb.tran_amount                       as tran_amount,
//                   cb.card_number                       as card_number,
//                   cb.card_number_enc                   as card_number_enc,
//                   cb.tran_date                         as tran_date,
//                   cb.reference_number                  as ref_num,
//                   substr(cb.reference_number,12,11)    as adj_ref_num,
//                   null                                 as challenge_requirements,
//                   cb.auth_code                         as auth_code,
//                   nvl(ac.action_code,cba.action_code)  as action_code,
//                   cba.adj_batch_date                   as adj_date,
//                   ac.adj_tran_code                     as adj_tran_code,
//                   decode(ac.adj_tran_code,9077,'C','D')as adj_debit_credit_ind,
//                   get_q_note_count(cb.cb_load_sec,:MesQueues.QG_CHARGEBACK_QUEUES)
//                                                        as note_count,
//                   cbr.date_initiated                   as reb_date,
//                   cbr.doc_status                       as reb_doc,
//                   cbr.cbr_status                       as reb_status,
//                   cbr.notes                            as reb_notes,
//                   (trunc(sysdate) - cb.incoming_date)  as time_dif,
//                   cb.trident_tran_id                   as trident_tran_id,
//                   decode( cb.dt_batch_date,
//                           null, 0, 1 )                 as tran_data_valid,
//                   cb.dt_batch_date                     as dt_batch_date,
//                   ltrim(rtrim(nvl(cb.dt_purchase_id,''))) as purchase_id
//            from   group_merchant                 gm,
//                   group_rep_merchant             grm,
//                   network_chargebacks            cb,
//                   network_chargeback_activity    cba,
//                   chargeback_reason_desc         rd,
//                   chargeback_rebuttal            cbr,
//                   chargeback_action_codes        ac
//            where  gm.org_num = :orgId and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                   cb.merchant_number = gm.merchant_number and
//                   cb.incoming_date between (:beginDate-180) and :endDate and
//                   :cardType in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and
//                   cba.cb_load_sec = cb.cb_load_sec and
//                   cba.action_date between :beginDate and :endDate and
//                   rd.card_type(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and
//                   rd.item_type(+) = 'C' and
//                   rd.reason_code(+) = cb.reason_code and
//                   cbr.cb_load_sec(+) = cb.cb_load_sec and
//                   ac.short_action_code(+) = cba.action_code
//            order by cb.merchant_number, cb.incoming_date desc,
//                     cb.cb_load_sec, cba.adj_batch_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+\n                    index (gm pkgroup_merchant)\n                    ordered\n                    use_nl(gm cb)\n                  */\n                 cb.merchant_number                   as merchant_number,\n                 cb.merchant_name                     as merchant_name,\n                 cb.cb_load_sec                       as mes_ref_num,\n                 cb.incoming_date                     as incoming_date,\n                 cb.REASON_CODE                       as reason_code,\n                 nvl(cb.first_time_chargeback,'Y')    as first_time,\n                 nvl(rd.REASON_DESC,\n                     'REASON CODE: '||cb.reason_code) as reason_desc,\n                 decode(nvl(rd.uta,'N'),'Y',0,1)      as rebut_active,\n                 cb.tran_amount                       as tran_amount,\n                 cb.card_number                       as card_number,\n                 cb.card_number_enc                   as card_number_enc,\n                 cb.tran_date                         as tran_date,\n                 cb.reference_number                  as ref_num,\n                 substr(cb.reference_number,12,11)    as adj_ref_num,\n                 null                                 as challenge_requirements,\n                 cb.auth_code                         as auth_code,\n                 nvl(ac.action_code,cba.action_code)  as action_code,\n                 cba.adj_batch_date                   as adj_date,\n                 ac.adj_tran_code                     as adj_tran_code,\n                 decode(ac.adj_tran_code,9077,'C','D')as adj_debit_credit_ind,\n                 get_q_note_count(cb.cb_load_sec, :1 )\n                                                      as note_count,\n                 cbr.date_initiated                   as reb_date,\n                 cbr.doc_status                       as reb_doc,\n                 cbr.cbr_status                       as reb_status,\n                 cbr.notes                            as reb_notes,\n                 (trunc(sysdate) - cb.incoming_date)  as time_dif,\n                 cb.trident_tran_id                   as trident_tran_id,\n                 decode( cb.dt_batch_date,\n                         null, 0, 1 )                 as tran_data_valid,\n                 cb.dt_batch_date                     as dt_batch_date,\n                 ltrim(rtrim(nvl(cb.dt_purchase_id,''))) as purchase_id\n          from   group_merchant                 gm,\n                 group_rep_merchant             grm,\n                 network_chargebacks            cb,\n                 network_chargeback_activity    cba,\n                 chargeback_reason_desc         rd,\n                 chargeback_rebuttal            cbr,\n                 chargeback_action_codes        ac\n          where  gm.org_num =  :2  and\n                 grm.user_id(+) =  :3  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :4  = -1 ) and\n                 cb.merchant_number = gm.merchant_number and\n                 cb.incoming_date between ( :5 -180) and  :6  and\n                  :7  in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and\n                 cba.cb_load_sec = cb.cb_load_sec and\n                 cba.action_date between  :8  and  :9  and\n                 rd.card_type(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and\n                 rd.item_type(+) = 'C' and\n                 rd.reason_code(+) = cb.reason_code and\n                 cbr.cb_load_sec(+) = cb.cb_load_sec and\n                 ac.short_action_code(+) = cba.action_code\n          order by cb.merchant_number, cb.incoming_date desc,\n                   cb.cb_load_sec, cba.adj_batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.QG_CHARGEBACK_QUEUES);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setString(7,cardType);
   __sJT_st.setDate(8,beginDate);
   __sJT_st.setDate(9,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MerchChargebackRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1066^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          //
          // if the merchant was issued a chargeback (MCHB) and
          // was later issued a credit (REMC), then there will
          // be two rows in the result set for the record.  Due
          // to the sorting, the second row will always be the
          // credit to the merchant account, so store the adjustment
          // date as the credit adjustment date.
          //
          if ( lastLoadSec == resultSet.getLong("mes_ref_num") )
          {
            if ( "9077".equals(resultSet.getString("adj_tran_code")) )
            {
              record.setCreditAdjustmentData( resultSet );

              //change the action code to prevent rebuttal attempt
              record.setStatusCode(resultSet.getString("action_code"));
            }
            continue;
          }
          lastLoadSec = resultSet.getLong("mes_ref_num");

          record = new ChargebackRecord( resultSet );
          ReportRows.add( record );
        }

        it.close();   // this will also close the resultSet
      }
      else    // RT_SUMMARY
      {
        /*@lineinfo:generated-code*//*@lineinfo:1100^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (gm pkgroup_merchant) */
//                    o.org_group                               as hierarchy_node,
//                    o.org_num                                 as org_num,
//                    o.org_name                                as org_name,
//                    count( cb.tran_amount )                   as item_count,
//                    sum(cb.tran_amount)                       as item_amount
//            from    parent_org                  po,
//                    group_merchant              gm,
//                    group_rep_merchant          grm,
//                    network_chargebacks         cb,
//                    organization                o
//            where   po.parent_org_num = :orgId and
//                    gm.org_num = po.org_num and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                    cb.merchant_number = gm.merchant_number and
//                    cb.incoming_date between (:beginDate-180) and :endDate and
//                    :cardType in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and
//                    exists ( select cba.cb_load_sec
//                              from network_chargeback_activity cba
//                              where cba.cb_load_sec = cb.cb_load_sec and
//                                    cba.action_date between :beginDate and :endDate) and
//                    o.org_num = po.org_num
//            group by o.org_num, o.org_group, o.org_name
//            order by o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (gm pkgroup_merchant) */\n                  o.org_group                               as hierarchy_node,\n                  o.org_num                                 as org_num,\n                  o.org_name                                as org_name,\n                  count( cb.tran_amount )                   as item_count,\n                  sum(cb.tran_amount)                       as item_amount\n          from    parent_org                  po,\n                  group_merchant              gm,\n                  group_rep_merchant          grm,\n                  network_chargebacks         cb,\n                  organization                o\n          where   po.parent_org_num =  :1  and\n                  gm.org_num = po.org_num and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and\n                  cb.merchant_number = gm.merchant_number and\n                  cb.incoming_date between ( :4 -180) and  :5  and\n                   :6  in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and\n                  exists ( select cba.cb_load_sec\n                            from network_chargeback_activity cba\n                            where cba.cb_load_sec = cb.cb_load_sec and\n                                  cba.action_date between  :7  and  :8 ) and\n                  o.org_num = po.org_num\n          group by o.org_num, o.org_group, o.org_name\n          order by o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setString(6,cardType);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchChargebackRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1128^9*/
        resultSet = it.getResultSet();

        if ( resultSet.next() )
        {
          processSummaryItems( resultSet );
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadChargebackAdjustments",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void loadChargebackByReason( long orgId, Date beginDate, Date endDate )
  {
    String                        cardType          = getData("cardType");
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;

    try
    {
      // empty the current contents
      ReportRows.clear();

      /*@lineinfo:generated-code*//*@lineinfo:1159^7*/

//  ************************************************************
//  #sql [Ctx] it = { select org_num_to_hierarchy_node( o.org_num )   as hierarchy_node,
//                 o.org_num                                as org_num,
//                 o.org_name                               as org_name,
//                 nvl(cb.card_type,
//                     decode_card_type(cb.card_number,0))  as card_type,
//                 cb.reason_code                           as reason_code,
//                 nvl(rd.reason_desc,
//                     'No Description Available')          as reason_desc,
//                   decode(nvl(rd.uta,'N'),'Y',0,1)      as rebut_active,
//                 count( cb.reason_code )                  as item_count,
//                 sum( cb.tran_amount )                    as item_amount
//          from   network_chargebacks    cb,
//                 organization           o,
//                 group_merchant         gm,
//                 group_rep_merchant     grm,
//                 chargeback_reason_desc rd
//          where  o.org_num in
//                  ( select org_num
//                    from   parent_org
//                    where  parent_org_num = :orgId ) and
//                 gm.org_num = o.org_num and
//                 grm.user_id(+) = :AppFilterUserId and
//                 grm.merchant_number(+) = gm.merchant_number and
//                 ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                 cb.merchant_number = gm.merchant_number and
//                 cb.incoming_date between :beginDate and :endDate and
//                 cb.first_time_chargeback = 'Y' and
//                 :cardType in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and
//                 rd.reason_code(+) = cb.reason_code and
//                 rd.card_type(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and
//                 rd.item_type(+) = 'C'
//          group by o.org_num, o.org_name,
//                   nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                   cb.reason_code, rd.reason_desc
//          order by o.org_name, card_type, cb.reason_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select org_num_to_hierarchy_node( o.org_num )   as hierarchy_node,\n               o.org_num                                as org_num,\n               o.org_name                               as org_name,\n               nvl(cb.card_type,\n                   decode_card_type(cb.card_number,0))  as card_type,\n               cb.reason_code                           as reason_code,\n               nvl(rd.reason_desc,\n                   'No Description Available')          as reason_desc,\n                 decode(nvl(rd.uta,'N'),'Y',0,1)      as rebut_active,\n               count( cb.reason_code )                  as item_count,\n               sum( cb.tran_amount )                    as item_amount\n        from   network_chargebacks    cb,\n               organization           o,\n               group_merchant         gm,\n               group_rep_merchant     grm,\n               chargeback_reason_desc rd\n        where  o.org_num in\n                ( select org_num\n                  from   parent_org\n                  where  parent_org_num =  :1  ) and\n               gm.org_num = o.org_num and\n               grm.user_id(+) =  :2  and\n               grm.merchant_number(+) = gm.merchant_number and\n               ( not grm.user_id is null or  :3  = -1 ) and\n               cb.merchant_number = gm.merchant_number and\n               cb.incoming_date between  :4  and  :5  and\n               cb.first_time_chargeback = 'Y' and\n                :6  in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and\n               rd.reason_code(+) = cb.reason_code and\n               rd.card_type(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and\n               rd.item_type(+) = 'C'\n        group by o.org_num, o.org_name,\n                 nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                 cb.reason_code, rd.reason_desc\n        order by o.org_name, card_type, cb.reason_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setString(6,cardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchChargebackRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1196^7*/

      resultSet = it.getResultSet();
      while ( resultSet.next() )
      {
        ReportRows.addElement( new ChargebackByReasonSummary( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadChargebackByReason",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void loadChargebackPreNotifications( int reportType, long orgId, Date beginDate, Date endDate )
  {
    String                        cardType          = getData("cardType");
    ResultSetIterator             it                = null;
    long                          lastLoadSec       = -1L;
    long                          merchantId        = 0L;
    ChargebackRecord              record            = null;
    ResultSet                     resultSet         = null;

    try
    {
      // empty the current contents
      ReportRows.clear();

      if ( reportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1232^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                      index(gm pk_group_merchant)
//                      ordered
//                      use_nl(gm cb)
//                    */
//                   cb.merchant_number                   as merchant_number,
//                   cb.merchant_name                     as merchant_name,
//                   cb.cb_load_sec                       as mes_ref_num,
//                   cb.incoming_date                     as incoming_date,
//                   cb.REASON_CODE                       as reason_code,
//                   nvl(cb.first_time_chargeback,'Y')    as first_time,
//                   nvl(cd.REASON_DESC,
//                       'REASON CODE: '||cb.reason_code) as reason_desc,
//                   decode(nvl(cd.uta,'N'),'Y',0,1)      as rebut_active,
//                   cb.tran_amount                       as tran_amount,
//                   cb.card_number                       as card_number,
//                   cb.card_number_enc                   as card_number_enc,
//                   cb.tran_date                         as tran_date,
//                   cb.reference_number                  as ref_num,
//                   substr(cb.reference_number,12,11)    as adj_ref_num,
//                   cd.challenge_requirements            as challenge_requirements,
//                   cb.auth_code                         as auth_code,
//                   nvl(ac.action_code,cba.action_code)  as action_code,
//                   cba.adj_batch_date                   as adj_date,
//                   ac.adj_tran_code                     as adj_tran_code,
//                   decode(ac.adj_tran_code,9077,'C','D')as adj_debit_credit_ind,
//                   get_q_note_count(cb.cb_load_sec,:MesQueues.QG_CHARGEBACK_QUEUES)
//                                                        as note_count,
//                   cbr.date_initiated                   as reb_date,
//                   cbr.doc_status                       as reb_doc,
//                   cbr.cbr_status                       as reb_status,
//                   cbr.notes                            as reb_notes,
//                   (trunc(sysdate) - cb.incoming_date)  as time_dif,
//                   cb.trident_tran_id                   as trident_tran_id,
//                   decode( cb.dt_batch_date,
//                           null, 0, 1 )                 as tran_data_valid,
//                   cb.dt_batch_date                     as dt_batch_date,
//                   ltrim(rtrim(nvl(cb.dt_purchase_id,''))) as purchase_id
//            from   group_merchant                 gm,
//                   group_rep_merchant             grm,
//                   network_chargebacks            cb,
//                   network_chargeback_activity    cba,
//                   chargeback_reason_desc         cd,
//                   chargeback_rebuttal            cbr,
//                   chargeback_action_codes        ac
//            where  gm.org_num = :orgId and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                   cb.merchant_number = gm.merchant_number and
//                   cb.incoming_date between :beginDate and :endDate and
//                   :cardType in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and
//                   cd.ITEM_TYPE(+) = 'C' and
//                   cd.REASON_CODE(+) = cb.REASON_CODE and
//                   cd.CARD_TYPE(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and
//                   cba.cb_load_sec(+) = cb.cb_load_sec and
//                   cb.cb_load_sec = cbr.cb_load_sec(+) and
//                   ac.short_action_code(+) = cba.action_code
//            order by cb.merchant_number, cb.incoming_date desc,
//                     cb.cb_load_sec, cba.adj_batch_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                    index(gm pk_group_merchant)\n                    ordered\n                    use_nl(gm cb)\n                  */\n                 cb.merchant_number                   as merchant_number,\n                 cb.merchant_name                     as merchant_name,\n                 cb.cb_load_sec                       as mes_ref_num,\n                 cb.incoming_date                     as incoming_date,\n                 cb.REASON_CODE                       as reason_code,\n                 nvl(cb.first_time_chargeback,'Y')    as first_time,\n                 nvl(cd.REASON_DESC,\n                     'REASON CODE: '||cb.reason_code) as reason_desc,\n                 decode(nvl(cd.uta,'N'),'Y',0,1)      as rebut_active,\n                 cb.tran_amount                       as tran_amount,\n                 cb.card_number                       as card_number,\n                 cb.card_number_enc                   as card_number_enc,\n                 cb.tran_date                         as tran_date,\n                 cb.reference_number                  as ref_num,\n                 substr(cb.reference_number,12,11)    as adj_ref_num,\n                 cd.challenge_requirements            as challenge_requirements,\n                 cb.auth_code                         as auth_code,\n                 nvl(ac.action_code,cba.action_code)  as action_code,\n                 cba.adj_batch_date                   as adj_date,\n                 ac.adj_tran_code                     as adj_tran_code,\n                 decode(ac.adj_tran_code,9077,'C','D')as adj_debit_credit_ind,\n                 get_q_note_count(cb.cb_load_sec, :1 )\n                                                      as note_count,\n                 cbr.date_initiated                   as reb_date,\n                 cbr.doc_status                       as reb_doc,\n                 cbr.cbr_status                       as reb_status,\n                 cbr.notes                            as reb_notes,\n                 (trunc(sysdate) - cb.incoming_date)  as time_dif,\n                 cb.trident_tran_id                   as trident_tran_id,\n                 decode( cb.dt_batch_date,\n                         null, 0, 1 )                 as tran_data_valid,\n                 cb.dt_batch_date                     as dt_batch_date,\n                 ltrim(rtrim(nvl(cb.dt_purchase_id,''))) as purchase_id\n          from   group_merchant                 gm,\n                 group_rep_merchant             grm,\n                 network_chargebacks            cb,\n                 network_chargeback_activity    cba,\n                 chargeback_reason_desc         cd,\n                 chargeback_rebuttal            cbr,\n                 chargeback_action_codes        ac\n          where  gm.org_num =  :2  and\n                 grm.user_id(+) =  :3  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :4  = -1 ) and\n                 cb.merchant_number = gm.merchant_number and\n                 cb.incoming_date between  :5  and  :6  and\n                  :7  in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and\n                 cd.ITEM_TYPE(+) = 'C' and\n                 cd.REASON_CODE(+) = cb.REASON_CODE and\n                 cd.CARD_TYPE(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and\n                 cba.cb_load_sec(+) = cb.cb_load_sec and\n                 cb.cb_load_sec = cbr.cb_load_sec(+) and\n                 ac.short_action_code(+) = cba.action_code\n          order by cb.merchant_number, cb.incoming_date desc,\n                   cb.cb_load_sec, cba.adj_batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.QG_CHARGEBACK_QUEUES);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   __sJT_st.setString(7,cardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchChargebackRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1294^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          //
          // if the merchant was issued a chargeback (MCHB) and
          // was later issued a credit (REMC), then there will
          // be two rows in the result set for the record.  Due
          // to the sorting, the second row will always be the
          // credit to the merchant account, so store the adjustment
          // date as the credit adjustment date.
          //
          if ( lastLoadSec == resultSet.getLong("mes_ref_num") )
          {
            if ( "9077".equals(resultSet.getString("adj_tran_code")) )
            {
              record.setCreditAdjustmentData( resultSet );
            }
            continue;
          }
          lastLoadSec = resultSet.getLong("mes_ref_num");

          record = new ChargebackRecord( resultSet );
          ReportRows.add( record );
        }
        it.close();   // this will also close the resultSet
      }
      else    // RT_SUMMARY
      {
        /*@lineinfo:generated-code*//*@lineinfo:1324^9*/

//  ************************************************************
//  #sql [Ctx] it = { select org_num_to_hierarchy_node( o.org_num )   as hierarchy_node,
//                   o.org_num                                as org_num,
//                   o.org_name                               as org_name,
//                   cb.first_time_chargeback                 as first_time,
//                   count( cb.tran_amount )                  as item_count,
//                   sum( cb.tran_amount )                    as item_amount
//            from   parent_org           po,
//                   organization         o,
//                   group_merchant       gm,
//                   group_rep_merchant   grm,
//                   network_chargebacks  cb
//            where  po.parent_org_num = :orgId and
//                   o.org_num = po.org_num and
//                   gm.org_num = o.org_num and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                   cb.merchant_number = gm.merchant_number and
//                   cb.incoming_date between :beginDate and :endDate and
//                   :cardType in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) )
//            group by o.org_num, o.org_name, cb.first_time_chargeback
//            order by o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select org_num_to_hierarchy_node( o.org_num )   as hierarchy_node,\n                 o.org_num                                as org_num,\n                 o.org_name                               as org_name,\n                 cb.first_time_chargeback                 as first_time,\n                 count( cb.tran_amount )                  as item_count,\n                 sum( cb.tran_amount )                    as item_amount\n          from   parent_org           po,\n                 organization         o,\n                 group_merchant       gm,\n                 group_rep_merchant   grm,\n                 network_chargebacks  cb\n          where  po.parent_org_num =  :1  and\n                 o.org_num = po.org_num and\n                 gm.org_num = o.org_num and\n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and\n                 cb.merchant_number = gm.merchant_number and\n                 cb.incoming_date between  :4  and  :5  and\n                  :6  in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) )\n          group by o.org_num, o.org_name, cb.first_time_chargeback\n          order by o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setString(6,cardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchChargebackRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1348^9*/

        resultSet = it.getResultSet();
        if ( resultSet.next() )
        {
          processSummaryItems( resultSet );
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadChargebackPreNotifications",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void loadChargebackStats( int reportType, long orgId, Date beginDate, Date endDate )
  {
    String                        cardType          = getData("cardType");
    ResultSetIterator             it                = null;
    long                          merchantId        = 0L;
    ChargebackRecord              record            = null;
    ResultSet                     resultSet         = null;

    try
    {
      // empty the current contents
      ReportRows.clear();

      if ( reportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1383^9*/

//  ************************************************************
//  #sql [Ctx] it = { select cb.merchant_number                       as hierarchy_node,
//                   om.org_num                               as org_num,
//                   o.org_name                               as org_name,
//                   cb.first_time_chargeback                 as first_time,
//                   decode(cba.action_date,null,'N','Y')     as worked,
//                   count( cb.tran_amount )                  as item_count,
//                   sum( cb.tran_amount )                    as item_amount
//            from   network_chargebacks            cb,
//                   network_chargeback_activity    cba,
//                   organization                   o,
//                   orgmerchant                    om,
//                   group_merchant                 gm,
//                   group_rep_merchant             grm,
//                   mif                            mf
//            where  cb.merchant_number = gm.merchant_number and
//                   cb.incoming_date between :beginDate and :endDate and
//                   :cardType in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and
//                   gm.org_num = :orgId and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                   om.org_merchant_num = cb.merchant_number and
//                   o.org_num = om.org_num and
//                   mf.merchant_number = cb.merchant_number and
//                   cba.cb_load_sec(+) = cb.cb_load_sec and
//                   cba.action_code(+) <> 'C'
//            group by mf.bank_number, cb.merchant_number,
//                     om.org_num, o.org_name, cb.first_time_chargeback,
//                     decode(cba.action_date,null,'N','Y')
//            order by mf.bank_number, o.org_name, cb.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select cb.merchant_number                       as hierarchy_node,\n                 om.org_num                               as org_num,\n                 o.org_name                               as org_name,\n                 cb.first_time_chargeback                 as first_time,\n                 decode(cba.action_date,null,'N','Y')     as worked,\n                 count( cb.tran_amount )                  as item_count,\n                 sum( cb.tran_amount )                    as item_amount\n          from   network_chargebacks            cb,\n                 network_chargeback_activity    cba,\n                 organization                   o,\n                 orgmerchant                    om,\n                 group_merchant                 gm,\n                 group_rep_merchant             grm,\n                 mif                            mf\n          where  cb.merchant_number = gm.merchant_number and\n                 cb.incoming_date between  :1  and  :2  and\n                  :3  in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and\n                 gm.org_num =  :4  and\n                 grm.user_id(+) =  :5  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :6  = -1 ) and\n                 om.org_merchant_num = cb.merchant_number and\n                 o.org_num = om.org_num and\n                 mf.merchant_number = cb.merchant_number and\n                 cba.cb_load_sec(+) = cb.cb_load_sec and\n                 cba.action_code(+) <> 'C'\n          group by mf.bank_number, cb.merchant_number,\n                   om.org_num, o.org_name, cb.first_time_chargeback,\n                   decode(cba.action_date,null,'N','Y')\n          order by mf.bank_number, o.org_name, cb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setString(3,cardType);
   __sJT_st.setLong(4,orgId);
   __sJT_st.setLong(5,AppFilterUserId);
   __sJT_st.setLong(6,AppFilterUserId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchChargebackRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1415^9*/
      }
      else    // summary report
      {
        /*@lineinfo:generated-code*//*@lineinfo:1419^9*/

//  ************************************************************
//  #sql [Ctx] it = { select org_num_to_hierarchy_node( o.org_num )   as hierarchy_node,
//                   o.org_num                                as org_num,
//                   o.org_name                               as org_name,
//                   cb.first_time_chargeback                 as first_time,
//                   decode(cba.action_date,null,'N','Y')     as worked,
//                   count( cb.tran_amount )                  as item_count,
//                   sum( cb.tran_amount )                    as item_amount
//            from   network_chargebacks            cb,
//                   network_chargeback_activity    cba,
//                   organization                   o,
//                   group_merchant                 gm,
//                   group_rep_merchant             grm
//            where  cb.merchant_number = gm.merchant_number and
//                   cb.incoming_date between :beginDate and :endDate and
//                   :cardType in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and
//                   gm.org_num = o.org_num and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                   o.org_num in
//                    ( select org_num
//                      from   parent_org
//                      where  parent_org_num = :orgId ) and
//                   cba.cb_load_sec(+) = cb.cb_load_sec and
//                   cba.action_code(+) <> 'C'
//            group by o.org_num, o.org_name, cb.first_time_chargeback,
//                     decode(cba.action_date,null,'N','Y')
//            order by o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select org_num_to_hierarchy_node( o.org_num )   as hierarchy_node,\n                 o.org_num                                as org_num,\n                 o.org_name                               as org_name,\n                 cb.first_time_chargeback                 as first_time,\n                 decode(cba.action_date,null,'N','Y')     as worked,\n                 count( cb.tran_amount )                  as item_count,\n                 sum( cb.tran_amount )                    as item_amount\n          from   network_chargebacks            cb,\n                 network_chargeback_activity    cba,\n                 organization                   o,\n                 group_merchant                 gm,\n                 group_rep_merchant             grm\n          where  cb.merchant_number = gm.merchant_number and\n                 cb.incoming_date between  :1  and  :2  and\n                  :3  in ( 'ALL', nvl(cb.card_type,decode_card_type(cb.card_number,0)) ) and\n                 gm.org_num = o.org_num and\n                 grm.user_id(+) =  :4  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :5  = -1 ) and\n                 o.org_num in\n                  ( select org_num\n                    from   parent_org\n                    where  parent_org_num =  :6  ) and\n                 cba.cb_load_sec(+) = cb.cb_load_sec and\n                 cba.action_code(+) <> 'C'\n          group by o.org_num, o.org_name, cb.first_time_chargeback,\n                   decode(cba.action_date,null,'N','Y')\n          order by o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MerchChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setString(3,cardType);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setLong(5,AppFilterUserId);
   __sJT_st.setLong(6,orgId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.MerchChargebackRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1449^9*/
      }
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        processSummaryItems( resultSet );
      }
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadChargebackStatistics",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void loadData( )
  {
    loadData( getReportDataType(), getReportType(), getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
  }

  public void loadData( int reportDataType, int reportType )
  {
    loadData( reportDataType, reportType, getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
  }

  public void loadData( int reportDataType, int reportType, long orgId, Date beginDate, Date endDate )
  {
    switch( reportDataType )
    {
      case  DT_CB_ADJUSTMENTS:
        loadChargebackAdjustments( reportType, getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
        break;

      case  DT_CB_PRENOTIFICATIONS:
        loadChargebackPreNotifications( reportType, getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
        break;

      case DT_RETRIEVALS:
        loadRetrievals( reportType, getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
        break;

      case DT_CB_STATISTICS:
        loadChargebackStats( reportType, getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
        break;

      case DT_CB_BY_REASON:
        loadChargebackByReason( getReportOrgId(), getReportDateBegin(), getReportDateEnd() );
        break;
    }
  }

  protected void loadRetrievals( int reportType, long orgId, Date beginDate, Date endDate )
  {
    String                        cardType          = getData("cardType");
    ResultSetIterator             it                = null;
    long                          merchantId        = 0L;
    ResultSet                     resultSet         = null;

    try
    {
      // empty the current contents
      ReportRows.clear();

      if ( reportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1518^9*/

//  ************************************************************
//  #sql [Ctx] it = { select rt.merchant_number               as merchant_number,
//                   rt.merchant_name                 as merchant_name,
//                   rt.incoming_date                 as incoming_date,
//                   rt.REASON_CODE                   as reason_code,
//                   rd.REASON_DESC                   as reason_desc,
//                   rt.tran_amount                   as tran_amount,
//                   rt.card_number                   as card_number,
//                   rt.card_number_enc               as card_number_enc,
//                   rt.tran_date                     as tran_date,
//                   rt.reference_number              as ref_num,
//                   rt.auth_code                     as auth_code,
//                   rt.received_date                 as received_date,
//                   rt.retr_load_sec                 as retr_load_sec,
//                   rt.card_type                     as card_type,
//                   decode( rt.dt_batch_date,
//                           null, 0, 1 )             as tran_data_valid,
//                   rt.dt_batch_date                 as dt_batch_date
//            from   network_retrievals           rt,
//                   chargeback_reason_desc       rd
//            where  rt.merchant_number in
//                    ( select merchant_number
//                      from   group_merchant
//                      where  org_num = :orgId ) and
//                   rt.incoming_date between :beginDate and :endDate and
//                   :cardType in ( 'ALL', nvl(rt.card_type,decode_card_type(rt.card_number,0)) ) and
//                   rd.reason_code(+)    = rt.reason_code and
//                   rd.card_type(+)      = nvl(rt.card_type,decode_card_type(rt.card_number,0)) and
//                   rd.item_type(+)      = 'R'
//            order by rt.merchant_number, rt.incoming_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select rt.merchant_number               as merchant_number,\n                 rt.merchant_name                 as merchant_name,\n                 rt.incoming_date                 as incoming_date,\n                 rt.REASON_CODE                   as reason_code,\n                 rd.REASON_DESC                   as reason_desc,\n                 rt.tran_amount                   as tran_amount,\n                 rt.card_number                   as card_number,\n                 rt.card_number_enc               as card_number_enc,\n                 rt.tran_date                     as tran_date,\n                 rt.reference_number              as ref_num,\n                 rt.auth_code                     as auth_code,\n                 rt.received_date                 as received_date,\n                 rt.retr_load_sec                 as retr_load_sec,\n                 rt.card_type                     as card_type,\n                 decode( rt.dt_batch_date,\n                         null, 0, 1 )             as tran_data_valid,\n                 rt.dt_batch_date                 as dt_batch_date\n          from   network_retrievals           rt,\n                 chargeback_reason_desc       rd\n          where  rt.merchant_number in\n                  ( select merchant_number\n                    from   group_merchant\n                    where  org_num =  :1  ) and\n                 rt.incoming_date between  :2  and  :3  and\n                  :4  in ( 'ALL', nvl(rt.card_type,decode_card_type(rt.card_number,0)) ) and\n                 rd.reason_code(+)    = rt.reason_code and\n                 rd.card_type(+)      = nvl(rt.card_type,decode_card_type(rt.card_number,0)) and\n                 rd.item_type(+)      = 'R'\n          order by rt.merchant_number, rt.incoming_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.MerchChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setString(4,cardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.MerchChargebackRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1549^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          ReportRows.add( new RetrievalRecord( resultSet ) );
        }
        it.close();   // this will also close the resultSet
      }
      else    // RT_SUMMARY
      {
        /*@lineinfo:generated-code*//*@lineinfo:1560^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  org_num_to_hierarchy_node( o.org_num   )  as hierarchy_node,
//                    o.org_num                                 as org_num,
//                    o.org_name                                as org_name,
//                    count( rt.tran_amount )                   as item_count,
//                    sum( rt.tran_amount )                     as item_amount
//            from    parent_org            po,
//                    group_merchant        gm,
//                    group_rep_merchant    grm,
//                    network_retrievals    rt,
//                    organization          o
//            where   po.parent_org_num = :orgId
//                    and gm.org_num = po.org_num
//                    and grm.user_id(+) = :AppFilterUserId
//                    and grm.merchant_number(+) = gm.merchant_number
//                    and ( :AppFilterUserId = -1 or not grm.user_id is null )
//                    and rt.merchant_number = gm.merchant_number
//                    and rt.incoming_date between :beginDate and :endDate
//                    and :cardType in ( 'ALL', nvl(rt.card_type,decode_card_type(rt.card_number,0)) )
//                    and o.org_num = po.org_num
//            group by o.org_num, o.org_name
//            order by o.org_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  org_num_to_hierarchy_node( o.org_num   )  as hierarchy_node,\n                  o.org_num                                 as org_num,\n                  o.org_name                                as org_name,\n                  count( rt.tran_amount )                   as item_count,\n                  sum( rt.tran_amount )                     as item_amount\n          from    parent_org            po,\n                  group_merchant        gm,\n                  group_rep_merchant    grm,\n                  network_retrievals    rt,\n                  organization          o\n          where   po.parent_org_num =  :1 \n                  and gm.org_num = po.org_num\n                  and grm.user_id(+) =  :2 \n                  and grm.merchant_number(+) = gm.merchant_number\n                  and (  :3  = -1 or not grm.user_id is null )\n                  and rt.merchant_number = gm.merchant_number\n                  and rt.incoming_date between  :4  and  :5 \n                  and  :6  in ( 'ALL', nvl(rt.card_type,decode_card_type(rt.card_number,0)) )\n                  and o.org_num = po.org_num\n          group by o.org_num, o.org_name\n          order by o.org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.MerchChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setString(6,cardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.MerchChargebackRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1583^9*/
        resultSet = it.getResultSet();

        if ( resultSet.next() )
        {
          processSummaryItems( resultSet );
        }
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    super.postHandleRequest(request);

    // if a date was not provided setup default dates
    if ( usingDefaultReportDates() )
    {
      // setup the default date range
      Calendar cal = Calendar.getInstance();
      cal.add( Calendar.DAY_OF_MONTH, -7 );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }
  }

  protected void processSummaryItems( ResultSet resultSet )
  {
    String                        firstTimeString     = null;
    boolean                       firstTimesPresent   = false;
    long                          lastOrgId           = -1L;
    ChargebackRetrievalSummary    summaryRec          = null;

    try
    {
      try
      {
        // this will throw an exception if the column
        // does not exist in the result set.
        firstTimeString   = resultSet.getString("first_time");
        firstTimesPresent = true;
      }
      catch( java.sql.SQLException e )
      {
        firstTimesPresent = false;
      }

      do
      {
        if ( firstTimesPresent )
        {
          if ( lastOrgId != resultSet.getLong("org_num" ) )
          {
            if ( summaryRec != null )
            {
              ReportRows.add( summaryRec );
            }
            summaryRec = new ChargebackRetrievalSummary( resultSet, false );

            // store the current org id as the last processed.
            lastOrgId = resultSet.getLong("org_num");
          }

          // if these are first time chargebacks, then add them to the
          // first time count.
          firstTimeString = resultSet.getString("first_time");
          if ( ( firstTimeString != null ) &&
               ( firstTimeString.charAt(0) == 'Y' ||
                 firstTimeString.charAt(0) == 'y' ) )
          {
            summaryRec.addFirstTimeCount( resultSet.getInt("item_count") );
            summaryRec.addFirstTimeAmount( resultSet.getDouble("item_amount") );
          }

          // add this row to the summary records.
          summaryRec.addTotalCount( resultSet.getInt("item_count") );
          summaryRec.addTotalAmount( resultSet.getDouble("item_amount") );

          // if this result set had worked data as well, then
          // add the count/amount pair to the worked totals
          try
          {
            if ( (resultSet.getString("worked")).equals("Y") )
            {
              // add to the worked count for this record
              summaryRec.addWorkedCount( resultSet.getInt("item_count") );
              summaryRec.addWorkedAmount( resultSet.getDouble("item_amount") );
            }
          }
          catch( java.sql.SQLException e )
          {
            // not present, ignore
          }
        }
        else    // just one entry per row
        {
          ReportRows.add( new ChargebackRetrievalSummary( resultSet ) );
        }
      }
      while( resultSet.next() );

      // make sure to insert the last record.
      if ( summaryRec != null )
      {
        ReportRows.add( summaryRec );
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "processSummaryItems", e.toString() );
    }
    finally
    {
    }
  }

  public void resetRetrievalReceivedDate( long retrievalId )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1711^7*/

//  ************************************************************
//  #sql [Ctx] { update  network_retrievals
//          set     received_date = null
//          where   retr_load_sec = :retrievalId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  network_retrievals\n        set     received_date = null\n        where   retr_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.reports.MerchChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,retrievalId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1716^7*/
      /*@lineinfo:generated-code*//*@lineinfo:1717^7*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1717^27*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "resetRetrievalReceivedDate(" + retrievalId + ")", e.toString() );
    }
  }

  public void showData( java.io.PrintStream out )
  {
  }

  public void storeRetrievalReceivedDate( long retrievalId, java.util.Date newDate )
  {
    try
    {
      if ( newDate != null )
      {
        // convert the java.util.Date to a java.sql.Date
        Date      recvDate = new Date( newDate.getTime() );

        /*@lineinfo:generated-code*//*@lineinfo:1738^9*/

//  ************************************************************
//  #sql [Ctx] { update  network_retrievals
//            set     received_date = :recvDate
//            where   retr_load_sec = :retrievalId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  network_retrievals\n          set     received_date =  :1 \n          where   retr_load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.reports.MerchChargebackRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,recvDate);
   __sJT_st.setLong(2,retrievalId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1743^9*/
        /*@lineinfo:generated-code*//*@lineinfo:1744^9*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1744^29*/
      }
      else
      {
        addError("Received date was missing or invalid.  Date was not changed." );
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "storeRetrievalReceivedDate(" + retrievalId + "," + newDate + ")", e.toString() );
    }
  }
}/*@lineinfo:generated-code*/