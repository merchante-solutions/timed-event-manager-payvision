/*@lineinfo:filename=SVBPortfolioDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/SVBPortfolioDataBean.sqlj $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-09 10:58:27 -0700 (Wed, 09 Apr 2008) $
  Version            : $Revision: 14728 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class SVBPortfolioDataBean extends ReportSQLJBean
{
  public class RowData
  {
    public String   cif               = "";
    public String   merchantNumber    = "";
    public String   clientName        = "";
    public String   openDate          = "";
    public String   closeDate         = "";
    public String   dda               = "";
    //public String   cdAmount          = "";
    public double   cdAmount          = 0.0;
    public double   cdPercent         = 0.0;
    
    public Date     ActiveDate        = null;
    public double   ChargebackAmount  = 0.0;
    public int      ChargebackCount   = 0;
    public double   VmcSalesAmount    = 0.0;
    public int      VmcSalesCount     = 0;

    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      cif = resultSet.getString("cif");
      merchantNumber = resultSet.getString("merchant_number");
      clientName = resultSet.getString("client_name");
      openDate = DateTimeFormatter.getFormattedDate(resultSet.getDate("date_opened"), "MM/dd/yy");
      closeDate = DateTimeFormatter.getFormattedDate(resultSet.getDate("date_closed"), "MM/dd/yy");
      dda = resultSet.getString("dda");
      cdAmount  = resultSet.getDouble("cd_amount");
      cdPercent = resultSet.getDouble("cd_percent");
      
      ActiveDate        = resultSet.getDate("last_billing_month");
      ChargebackAmount  = resultSet.getDouble("cb_amount");
      ChargebackCount   = resultSet.getInt("cb_count");
      VmcSalesAmount    = resultSet.getDouble("vmc_sales_amount");
      VmcSalesCount     = resultSet.getInt("vmc_sales_count");
    }
  }

  public SVBPortfolioDataBean( )
  {
  }

  protected void encodeHeaderFixed( StringBuffer line )
  {
    // do nothing, no header in a fixed record file
    line.setLength(0);
  }

  protected void encodeLineFixed( Object obj, StringBuffer line)
  {
    RowData   record    = (RowData)obj;

    // clear line buffer
    line.setLength(0);

    // add row data
    line.append(encodeFixedField(10, record.cif));
    line.append(encodeFixedField(32, record.clientName));
    line.append(encodeFixedField(12, record.merchantNumber));
    line.append(encodeFixedField(8, record.openDate));
    line.append(encodeFixedField(8, record.closeDate));
    line.append(encodeFixedField(17, record.dda));
    line.append(encodeFixedField(10, record.cdAmount, 0));
    line.append(".00");
    line.append(encodeFixedField(10, record.cdPercent, 0));
  }

  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);

    // put in all header columns
    line.append("\"CIF #\",");
    line.append("\"CLIENT NAME\",");
    line.append("\"MERCHANT NUMBER\",");
    line.append("\"DATE OPENED\",");
    line.append("\"DATE CLOSED\",");
    line.append("\"RELATED DDA#\",");
    line.append("\"CD AMOUNT\",");
    line.append("\"CD PERCENT\",");
    line.append("\"BILLING MONTH\",");
    line.append("\"VMC SALES COUNT\",");
    line.append("\"VMC SALES AMOUNT\",");
    line.append("\"CHARGEBACK COUNT\",");
    line.append("\"CHARGEBACK AMOUNT\"");
  }

  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;

    // clear the line buffer and add
    // the merchant specific data.
    line.setLength(0);
    line.append("\"" + record.cif + "\",");
    line.append("\"" + record.clientName + "\",");
    line.append(record.merchantNumber + ",");
    line.append(record.openDate + ",");
    line.append(record.closeDate + ",");
    line.append(record.dda + ",");
    line.append(record.cdAmount);
    line.append(",");
    line.append(record.cdPercent);
    line.append(",");
    line.append(DateTimeFormatter.getFormattedDate(record.ActiveDate,"MM/dd/yyyy"));
    line.append(",");
    line.append(record.VmcSalesCount);
    line.append(",");
    line.append(record.VmcSalesAmount);
    line.append(",");
    line.append(record.ChargebackCount);
    line.append(",");
    line.append(record.ChargebackAmount);
  }

  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");

    filename.append("client_data_");

    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );

    return ( filename.toString() );
  }

  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
      case FF_FIXED:
        retVal = true;
        break;
        
      default:
        break;
    }
    return(retVal);
  }

  public void loadData( )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;

    int                           svbBankNum        = 3941;
    int                           svbGroupNum       = 400010;

    try
    {
      // empty the current contents
      ReportRows.clear();
      /*@lineinfo:generated-code*//*@lineinfo:204^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  substr(mf.user_acount_1, 1, 10)             as cif,
//                  mf.dba_name                                 as client_name,
//                  mf.merchant_number                          as merchant_number,
//                  to_date(mf.date_opened, 'MMDDYY')           as date_opened,
//                  mf.date_stat_chgd_to_dcb                    as date_closed,
//                  mf.dda_num                                  as dda,
//                  to_number(substr(mf.user_acount_1, 11, 6))  as cd_amount,
//                  mr.svb_cd_collateral                        as cd_percent,
//                  sm.active_date                              as last_billing_month,
//                  nvl(sm.vmc_sales_count,0)                   as vmc_sales_count,
//                  nvl(sm.vmc_sales_amount,0)                  as vmc_sales_amount,
//                  nvl(sm.incoming_chargeback_count,0)         as cb_count,
//                  nvl(sm.incoming_chargeback_amount,0)        as cb_amount
//          from    mif                       mf,
//                  monthly_extract_summary   sm,
//                  merchant                  mr
//          where   mf.bank_number = :svbBankNum and
//                  mf.group_2_association = :svbGroupNum and
//                  sm.merchant_number(+) = mf.merchant_number and
//                  sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                  mr.merch_number(+) = mf.merchant_number
//          order by substr(mf.user_acount_1, 1, 10) asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  substr(mf.user_acount_1, 1, 10)             as cif,\n                mf.dba_name                                 as client_name,\n                mf.merchant_number                          as merchant_number,\n                to_date(mf.date_opened, 'MMDDYY')           as date_opened,\n                mf.date_stat_chgd_to_dcb                    as date_closed,\n                mf.dda_num                                  as dda,\n                to_number(substr(mf.user_acount_1, 11, 6))  as cd_amount,\n                mr.svb_cd_collateral                        as cd_percent,\n                sm.active_date                              as last_billing_month,\n                nvl(sm.vmc_sales_count,0)                   as vmc_sales_count,\n                nvl(sm.vmc_sales_amount,0)                  as vmc_sales_amount,\n                nvl(sm.incoming_chargeback_count,0)         as cb_count,\n                nvl(sm.incoming_chargeback_amount,0)        as cb_amount\n        from    mif                       mf,\n                monthly_extract_summary   sm,\n                merchant                  mr\n        where   mf.bank_number =  :1  and\n                mf.group_2_association =  :2  and\n                sm.merchant_number(+) = mf.merchant_number and\n                sm.active_date between  :3  and  :4  and\n                mr.merch_number(+) = mf.merchant_number\n        order by substr(mf.user_acount_1, 1, 10) asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.SVBPortfolioDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,svbBankNum);
   __sJT_st.setInt(2,svbGroupNum);
   __sJT_st.setDate(3,ReportDateBegin);
   __sJT_st.setDate(4,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.SVBPortfolioDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:228^7*/

      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadData()", e.toString() );
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    if( usingDefaultReportDates() )
    {
      // set report begin date to last month
      Calendar    cal           = Calendar.getInstance();
    
      // setup the default date range
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.MONTH, -1 );
    
      // since this report is for one month at a time,
      // make sure that the from date always starts with
      // the first day of the specified month.
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      // set both the begin and end dates
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
    }
  }

  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/