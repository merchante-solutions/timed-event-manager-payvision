/*@lineinfo:filename=ChargebackAgingDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ChargebackAgingDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-09-12 14:42:06 -0700 (Wed, 12 Sep 2007) $
  Version            : $Revision: 14136 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class ChargebackAgingDataBean extends ReportSQLJBean
{
  public static class DetailData
  {
    public String     CardNumber          = null;
    public String     CardNumberEnc       = null;
    public String     CardType            = null;
    public String     CBRefNum            = null;
    public int        DaysOut             = 0;
    public String     DbaName             = null;
    public Date       IncomingDate        = null;
    public long       MerchantId          = 0L;
    public long       MesRefNum           = 0L;
    public String     ReasonCode          = null;
    public String     ReasonDesc          = null;
    public String     ReferenceNumber     = null;
    public double     TranAmount          = 0.0;
    
    public DetailData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      CardNumber        = resultSet.getString("card_number");
      CardType          = resultSet.getString("card_type");
      CBRefNum          = resultSet.getString("cb_ref_num");
      DaysOut           = resultSet.getInt("days_out");
      DbaName           = resultSet.getString("dba_name");
      IncomingDate      = resultSet.getDate("incoming_date");
      MerchantId        = resultSet.getLong("merchant_number");
      MesRefNum         = resultSet.getInt("mes_ref_num");
      ReasonCode        = resultSet.getString("reason_code");
      ReasonDesc        = resultSet.getString("reason_desc");
      ReferenceNumber   = resultSet.getString("ref_num");
      TranAmount        = resultSet.getDouble("tran_amount");
      CardNumberEnc     = resultSet.getString("card_number_enc");
    }
  }
  
  public static class SummaryData
    implements Comparable
  {
    public  int             AgeGroup          = 0;
    public  String          AgeGroupDesc      = null;
    public  long            HierarchyNode     = 0L;
    public  double          ItemAmount        = 0.0;
    public  int             ItemCount         = 0;
    public  String          OrgName           = null;
    public  long            OrgId             = 0L;
  
    public SummaryData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      OrgId         = resultSet.getLong("org_num");
      HierarchyNode = resultSet.getLong("hierarchy_node");
      OrgName       = resultSet.getString("org_name");
      AgeGroup      = resultSet.getInt("age_group");
      AgeGroupDesc  = resultSet.getString("age_group_desc");
      ItemCount     = resultSet.getInt("item_count");
      ItemAmount    = resultSet.getDouble("item_amount");
    }      
    
    public int compareTo( Object obj )
    {
      SummaryData     compareObj    = (SummaryData)obj;
      int             retVal        = 0;
      
      if ( (retVal = OrgName.compareTo(compareObj.OrgName)) == 0 )
      {
        if ( (retVal = (int)(HierarchyNode - compareObj.HierarchyNode)) == 0 )
        {
          if ( (retVal = (AgeGroup - compareObj.AgeGroup)) == 0 )
          {
            retVal = (ItemCount - compareObj.ItemCount);
          }
        }
      }
      return( retVal );
    }
  }
  
  private int             ReportAgeGroup          = 0;
  
  public ChargebackAgingDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    if ( ReportType == RT_DETAILS )
    {
      line.append("\"Merchant ID\",");
      line.append("\"DBA Name\",");
      line.append("\"Incoming Date\",");
      line.append("\"Card Type\",");
      line.append("\"Card Number\",");
      line.append("\"Tran Amount\",");
      line.append("\"Reference\",");
      line.append("\"MES CB Ref\",");
      line.append("\"Assoc CB Ref\",");
      line.append("\"Reason\",");
      line.append("\"Days Out\"");
    }
    else    // assume summary
    {
      line.append("\"Node ID\",");
      line.append("\"Org Name\",");
      line.append("\"Age Group\",");
      line.append("\"Item Count\",");
      line.append("\"Item Amount\"");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    if ( ReportType == RT_DETAILS )
    {
      DetailData      record    = (DetailData)obj;
    
      line.append( encodeHierarchyNode(record.MerchantId) );
      line.append( ",\"" );
      line.append( record.DbaName );
      line.append( "\"," );
      line.append( DateTimeFormatter.getFormattedDate( record.IncomingDate, "MM/dd/yyyy" ) );
      line.append( ",\"" );
      line.append( record.CardType );
      line.append( "\",\"" );
      line.append( record.CardNumber );
      line.append( "\"," );
      line.append( record.TranAmount );
      line.append( ",\"'" );
      line.append( record.ReferenceNumber );
      line.append( "\"," );
      line.append( record.MesRefNum );
      line.append( "," );
      line.append( record.CBRefNum );
      line.append( ",\"" );
      line.append( record.ReasonDesc );
      line.append( " (" );
      line.append( record.ReasonCode );
      line.append( ")\"," );
      line.append( record.DaysOut );
    }
    else    // assume summary
    {
      SummaryData       record    = (SummaryData)obj;
      
      line.append( encodeHierarchyNode(record.HierarchyNode) );
      line.append( ",\"" );
      line.append( record.OrgName );
      line.append( "\",\"" );
      line.append( record.AgeGroupDesc );
      line.append( "\"," );
      line.append( record.ItemCount );
      line.append( "," );
      line.append( record.ItemAmount );
    }
  }
  
  public String getAgeGroupDesc( int ageGroup )
  {
    int         bankNumber  = 0;
    String      retVal      = "";
    
    try
    {
      bankNumber = getOrgBankId(getReportOrgId());
      
      /*@lineinfo:generated-code*//*@lineinfo:217^7*/

//  ************************************************************
//  #sql [Ctx] { select  ag.description 
//          from    chargeback_age_groups   ag
//          where   ag.bank_number = :bankNumber and
//                  ag.age_group = :ageGroup
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ag.description  \n        from    chargeback_age_groups   ag\n        where   ag.bank_number =  :1  and\n                ag.age_group =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ChargebackAgingDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,ageGroup);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:223^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMddyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_cb_aging_");
    
    if ( ReportType == RT_DETAILS )
    {
      filename.append("details_");
    }
    else
    {
      filename.append("summary_");
    }
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    int                           bankNumber        = 0;
    ResultSetIterator             it                = null;
    long                          merchantId        = 0;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      // set the bank number if the 
      // current node is a bank node
      if ( isOrgBank( orgId ) )
      {
        bankNumber = getOrgBankId( orgId );
      }
      
      if ( ReportType == RT_DETAILS )
      {
        merchantId = getReportMerchantId();
      
        /*@lineinfo:generated-code*//*@lineinfo:294^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec                              as mes_ref_num,
//                    cb.incoming_date                            as incoming_date,
//                    cb.merchant_number                          as merchant_number,
//                    nvl(mf.dba_name,cb.merchant_name)           as dba_name,
//                    decode( substr(cb.card_number,1,1),
//                                   '4', 'VS',
//                                   '5', 'MC',
//                                   'UK' )                       as card_type,
//                    cb.card_number                              as card_number,
//                    cb.card_number_enc                          as card_number_enc,
//                    cb.reference_number                         as ref_num,
//                    cb.tran_amount                              as tran_amount,
//                    cb.cb_ref_num                               as cb_ref_num,
//                    cb.reason_code                              as reason_code,
//                    rd.REASON_DESC                              as reason_desc,
//                    round((trunc(sysdate)-cb.incoming_date),0)  as days_out                
//            from    network_chargebacks         cb,
//                    chargeback_age_groups       ag,
//                    mif                         mf,
//                    chargeback_reason_desc      rd
//            where   (
//                      (cb.merchant_number = :merchantId) or
//                      ((cb.merchant_number = 0) and (cb.bank_number = :bankNumber))
//                    ) and 
//                    cb.incoming_date between (:beginDate-180) and :beginDate and
//                    not exists
//                    (
//                      select  cba.cb_load_sec
//                      from    network_chargeback_activity   cba
//                      where   cba.cb_load_sec = cb.cb_load_sec and
//                              cba.action_date <= :beginDate
//                    ) and
//                    ag.bank_number = cb.bank_number and
//                    ( :ReportAgeGroup = 0 or ag.age_group = :ReportAgeGroup ) and
//                    (:beginDate-cb.incoming_date) between ag.lower_range and ag.upper_range and
//                    mf.merchant_number(+) = cb.merchant_number and
//                    rd.card_type(+) = decode( substr(cb.card_number,1,1),
//                                              '4', 'VS',
//                                              '5', 'MC',
//                                              'UK' ) and
//                    rd.item_type(+) = 'C' and -- chargeback reason                   
//                    rd.reason_code(+) = cb.reason_code
//            order by cb.incoming_date, cb.merchant_number, cb.card_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec                              as mes_ref_num,\n                  cb.incoming_date                            as incoming_date,\n                  cb.merchant_number                          as merchant_number,\n                  nvl(mf.dba_name,cb.merchant_name)           as dba_name,\n                  decode( substr(cb.card_number,1,1),\n                                 '4', 'VS',\n                                 '5', 'MC',\n                                 'UK' )                       as card_type,\n                  cb.card_number                              as card_number,\n                  cb.card_number_enc                          as card_number_enc,\n                  cb.reference_number                         as ref_num,\n                  cb.tran_amount                              as tran_amount,\n                  cb.cb_ref_num                               as cb_ref_num,\n                  cb.reason_code                              as reason_code,\n                  rd.REASON_DESC                              as reason_desc,\n                  round((trunc(sysdate)-cb.incoming_date),0)  as days_out                \n          from    network_chargebacks         cb,\n                  chargeback_age_groups       ag,\n                  mif                         mf,\n                  chargeback_reason_desc      rd\n          where   (\n                    (cb.merchant_number =  :1 ) or\n                    ((cb.merchant_number = 0) and (cb.bank_number =  :2 ))\n                  ) and \n                  cb.incoming_date between ( :3 -180) and  :4  and\n                  not exists\n                  (\n                    select  cba.cb_load_sec\n                    from    network_chargeback_activity   cba\n                    where   cba.cb_load_sec = cb.cb_load_sec and\n                            cba.action_date <=  :5 \n                  ) and\n                  ag.bank_number = cb.bank_number and\n                  (  :6  = 0 or ag.age_group =  :7  ) and\n                  ( :8 -cb.incoming_date) between ag.lower_range and ag.upper_range and\n                  mf.merchant_number(+) = cb.merchant_number and\n                  rd.card_type(+) = decode( substr(cb.card_number,1,1),\n                                            '4', 'VS',\n                                            '5', 'MC',\n                                            'UK' ) and\n                  rd.item_type(+) = 'C' and -- chargeback reason                   \n                  rd.reason_code(+) = cb.reason_code\n          order by cb.incoming_date, cb.merchant_number, cb.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ChargebackAgingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setInt(6,ReportAgeGroup);
   __sJT_st.setInt(7,ReportAgeGroup);
   __sJT_st.setDate(8,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.ChargebackAgingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:339^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          ReportRows.addElement( new DetailData( resultSet ) );
        }
        resultSet.close();
        it.close();
      }        
      else  // RT_SUMMARY
      {
        if ( SummaryType == ST_PARENT )
        {
          if ( bankNumber != 0 )
          {
            String  orgName = getOrgName( orgId );
            long    orgNode = orgIdToHierarchyNode( orgId );
            
            // bank level node, pull by bank number to pick up
            // all chargebacks including those with a 0 merchant #
            /*@lineinfo:generated-code*//*@lineinfo:360^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  :orgId                          as org_num,
//                        :orgNode                        as hierarchy_node,
//                        :orgName                        as org_name,
//                        ag.age_group                    as age_group,
//                        ag.description                  as age_group_desc,
//                        count( cb.cb_load_sec )         as item_count,
//                        sum( cb.tran_amount )           as item_amount
//                from    network_chargebacks         cb,
//                        chargeback_age_groups       ag
//                where   cb.bank_number = :bankNumber and
//                        cb.incoming_date between (:beginDate-180) and :beginDate and                    
//                        not exists
//                        (
//                          select  cba.cb_load_sec
//                          from    network_chargeback_activity   cba
//                          where   cba.cb_load_sec = cb.cb_load_sec and
//                                  cba.action_date <= :beginDate
//                        ) and
//                        ag.bank_number = cb.bank_number and
//                        (:beginDate-cb.incoming_date) between ag.lower_range and ag.upper_range                               
//                group by ag.age_group, ag.description
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                           as org_num,\n                       :2                         as hierarchy_node,\n                       :3                         as org_name,\n                      ag.age_group                    as age_group,\n                      ag.description                  as age_group_desc,\n                      count( cb.cb_load_sec )         as item_count,\n                      sum( cb.tran_amount )           as item_amount\n              from    network_chargebacks         cb,\n                      chargeback_age_groups       ag\n              where   cb.bank_number =  :4  and\n                      cb.incoming_date between ( :5 -180) and  :6  and                    \n                      not exists\n                      (\n                        select  cba.cb_load_sec\n                        from    network_chargeback_activity   cba\n                        where   cba.cb_load_sec = cb.cb_load_sec and\n                                cba.action_date <=  :7 \n                      ) and\n                      ag.bank_number = cb.bank_number and\n                      ( :8 -cb.incoming_date) between ag.lower_range and ag.upper_range                               \n              group by ag.age_group, ag.description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ChargebackAgingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgNode);
   __sJT_st.setString(3,orgName);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.ChargebackAgingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:383^13*/
          }
          else    // not a bank node, only show cbs with merchant #s
          {
            /*@lineinfo:generated-code*//*@lineinfo:387^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                       as org_num,
//                        o.org_group                     as hierarchy_node,
//                        o.org_name                      as org_name,
//                        ag.age_group                    as age_group,
//                        ag.description                  as age_group_desc,
//                        count( cb.cb_load_sec )         as item_count,
//                        sum( cb.tran_amount )           as item_amount
//                from    organization                o,
//                        group_merchant              gm,
//                        network_chargebacks         cb,
//                        chargeback_age_groups       ag
//                where   o.org_num = :orgId and
//                        gm.org_num = o.org_num and
//                        cb.merchant_number = gm.merchant_number and
//                        cb.incoming_date between (:beginDate-180) and :beginDate and                    
//                        not exists
//                        (
//                          select  cba.cb_load_sec
//                          from    network_chargeback_activity   cba
//                          where   cba.cb_load_sec = cb.cb_load_sec and
//                                  cba.action_date <= :beginDate
//                        ) and
//                        ag.bank_number = cb.bank_number and
//                        (:beginDate-cb.incoming_date) between ag.lower_range and ag.upper_range                               
//                group by o.org_num, o.org_group, o.org_name, ag.age_group, ag.description
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                       as org_num,\n                      o.org_group                     as hierarchy_node,\n                      o.org_name                      as org_name,\n                      ag.age_group                    as age_group,\n                      ag.description                  as age_group_desc,\n                      count( cb.cb_load_sec )         as item_count,\n                      sum( cb.tran_amount )           as item_amount\n              from    organization                o,\n                      group_merchant              gm,\n                      network_chargebacks         cb,\n                      chargeback_age_groups       ag\n              where   o.org_num =  :1  and\n                      gm.org_num = o.org_num and\n                      cb.merchant_number = gm.merchant_number and\n                      cb.incoming_date between ( :2 -180) and  :3  and                    \n                      not exists\n                      (\n                        select  cba.cb_load_sec\n                        from    network_chargeback_activity   cba\n                        where   cba.cb_load_sec = cb.cb_load_sec and\n                                cba.action_date <=  :4 \n                      ) and\n                      ag.bank_number = cb.bank_number and\n                      ( :5 -cb.incoming_date) between ag.lower_range and ag.upper_range                               \n              group by o.org_num, o.org_group, o.org_name, ag.age_group, ag.description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ChargebackAgingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.ChargebackAgingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:414^13*/
          }
        }
        else    // standard child report
        {
          if ( bankNumber != 0 )
          {
            long    orgNode = orgIdToHierarchyNode( orgId );
          
            // if the current node is a bank node, then show all
            // of the unassigned chargebacks belonging to this bank #
            /*@lineinfo:generated-code*//*@lineinfo:425^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  :orgId                          as org_num,
//                        :orgNode                        as hierarchy_node,
//                        'Unassigned Chargebacks'        as org_name,
//                        ag.age_group                    as age_group,
//                        ag.description                  as age_group_desc,
//                        count( cb.cb_load_sec )         as item_count,
//                        sum( cb.tran_amount )           as item_amount
//                from    network_chargebacks           cb,
//                        chargeback_age_groups         ag
//                where   cb.merchant_number = 0 and
//                        cb.bank_number = :bankNumber and
//                        cb.incoming_date between (:beginDate-180) and :beginDate and                    
//                        not exists
//                        (
//                          select  cba.cb_load_sec
//                          from    network_chargeback_activity   cba
//                          where   cba.cb_load_sec = cb.cb_load_sec and
//                                  cba.action_date <= :beginDate
//                        ) and
//                        ag.bank_number = cb.bank_number and
//                        (:beginDate-cb.incoming_date) between ag.lower_range and ag.upper_range
//                group by ag.age_group, ag.description
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1                           as org_num,\n                       :2                         as hierarchy_node,\n                      'Unassigned Chargebacks'        as org_name,\n                      ag.age_group                    as age_group,\n                      ag.description                  as age_group_desc,\n                      count( cb.cb_load_sec )         as item_count,\n                      sum( cb.tran_amount )           as item_amount\n              from    network_chargebacks           cb,\n                      chargeback_age_groups         ag\n              where   cb.merchant_number = 0 and\n                      cb.bank_number =  :3  and\n                      cb.incoming_date between ( :4 -180) and  :5  and                    \n                      not exists\n                      (\n                        select  cba.cb_load_sec\n                        from    network_chargeback_activity   cba\n                        where   cba.cb_load_sec = cb.cb_load_sec and\n                                cba.action_date <=  :6 \n                      ) and\n                      ag.bank_number = cb.bank_number and\n                      ( :7 -cb.incoming_date) between ag.lower_range and ag.upper_range\n              group by ag.age_group, ag.description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.ChargebackAgingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,orgNode);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.ChargebackAgingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:449^13*/          
            resultSet = it.getResultSet();
            
            while( resultSet.next() )
            {
              ReportRows.addElement( new SummaryData( resultSet ) );
            }
            resultSet.close();
            it.close();
          }
        
          /*@lineinfo:generated-code*//*@lineinfo:460^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_num                       as org_num,
//                      o.org_group                     as hierarchy_node,
//                      o.org_name                      as org_name,
//                      ag.age_group                    as age_group,
//                      ag.description                  as age_group_desc,
//                      count( cb.cb_load_sec )         as item_count,
//                      sum( cb.tran_amount )           as item_amount
//              from    parent_org                  po,
//                      organization                o,
//                      group_merchant              gm,
//                      network_chargebacks         cb,
//                      chargeback_age_groups       ag
//              where   po.parent_org_num = :orgId and
//                      o.org_num = po.org_num and
//                      gm.org_num = o.org_num and
//                      cb.merchant_number = gm.merchant_number and
//                      cb.incoming_date between (:beginDate-180) and :beginDate and                    
//                      not exists
//                      (
//                        select  cba.cb_load_sec
//                        from    network_chargeback_activity   cba
//                        where   cba.cb_load_sec = cb.cb_load_sec and
//                                cba.action_date <= :beginDate
//                      ) and
//                      ag.bank_number = cb.bank_number and
//                      (:beginDate-cb.incoming_date) between ag.lower_range and ag.upper_range                               
//              group by o.org_num, o.org_group, o.org_name, ag.age_group, ag.description
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_num                       as org_num,\n                    o.org_group                     as hierarchy_node,\n                    o.org_name                      as org_name,\n                    ag.age_group                    as age_group,\n                    ag.description                  as age_group_desc,\n                    count( cb.cb_load_sec )         as item_count,\n                    sum( cb.tran_amount )           as item_amount\n            from    parent_org                  po,\n                    organization                o,\n                    group_merchant              gm,\n                    network_chargebacks         cb,\n                    chargeback_age_groups       ag\n            where   po.parent_org_num =  :1  and\n                    o.org_num = po.org_num and\n                    gm.org_num = o.org_num and\n                    cb.merchant_number = gm.merchant_number and\n                    cb.incoming_date between ( :2 -180) and  :3  and                    \n                    not exists\n                    (\n                      select  cba.cb_load_sec\n                      from    network_chargeback_activity   cba\n                      where   cba.cb_load_sec = cb.cb_load_sec and\n                              cba.action_date <=  :4 \n                    ) and\n                    ag.bank_number = cb.bank_number and\n                    ( :5 -cb.incoming_date) between ag.lower_range and ag.upper_range                               \n            group by o.org_num, o.org_group, o.org_name, ag.age_group, ag.description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.ChargebackAgingDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.ChargebackAgingDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:489^11*/
        }          
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          ReportRows.addElement( new SummaryData( resultSet ) );
        }
        resultSet.close();
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // load the default report properties
    super.setProperties( request );
    
    ReportAgeGroup = HttpHelper.getInt(request,"ageGroup",0);
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      int         inc           = -1;
      
      if ( cal.get( Calendar.DAY_OF_WEEK ) == Calendar.MONDAY )
      {
        // friday data available on monday,
        inc = -3;     
      }
      
      // setup the default date range to be last day
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.DAY_OF_MONTH, inc );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/