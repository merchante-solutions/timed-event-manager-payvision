/*@lineinfo:filename=AASWXMLDownload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/AASWXMLDownload.sqlj $

  Description:  
  
    Send queued XML messages to verisign.


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 1/27/03 5:43p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.PrintWriter;
import java.sql.ResultSet;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class AASWXMLDownload extends SQLJConnectionBase
{
  public String     errorString = "Unknown Error";
  public Document   doc         = null;
  
  private boolean debug         = false;
  
  public AASWXMLDownload()
  {
  }
  
  public boolean buildXML( String beginDate, 
                           String endDate,
                           String franchise)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean result = false;
    
    try
    {
      connect();
      
      // run query
      /*@lineinfo:generated-code*//*@lineinfo:63^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.merch_business_name                 name,
//                  'AASW'                                isv,
//                  to_char(acc.date_completed,
//                          'MM/DD/YYYY')                 activation_date,
//                  'Yes'                                 desires_ach,
//                  'Yes'                                 accepts_credit_cards,
//                  -- here we go with card acceptance
//                  decode(visa.cardtype_code, 
//                    null, 'N', 'Y')                     accepts_visa,
//                  decode(master.cardtype_code, 
//                    null, 'N', 'Y')                     accepts_mc,
//                  decode(diners.cardtype_code, 
//                    null, 'N', 'Y')                     accepts_diners,
//                  decode(discover.cardtype_code, 
//                    null, 'N', 'Y')                     accepts_discover,
//                  decode(jcb.cardtype_code, 
//                    null, 'N', 'Y')                     accepts_jcb,
//                  decode(amex.cardtype_code, 
//                    null, 'N', 'Y')                     accepts_amex,
//                  '433239'                              acquirer_bin,
//                  '000000'                              agent_number,
//                  '000000'                              chain_number,
//                  m.merch_number                        merchant_number,
//                  to_char(vn.store_number, '0009')      store_number,        
//                  to_char(vn.terminal_number, '0009')   terminal_number,
//                  m.sic_code                            merchant_category,
//                  '840'                                 country_code,
//                  '840'                                 currency_code,
//                  'D'                                   industry_code,
//                  tz.gmt_offset                         time_zone,
//                  a.address_city                        merchant_city,
//                  a.address_zip                         merchant_zip,
//                  a.countrystate_code                   merchant_state,
//                  a.address_phone                       merchant_phone,
//                  decode(trunc(m.merch_mail_phone_sales/50),
//                          0, '54', '56')                transaction_code,
//                  to_char(vn.location_number, '00009')  location_number,
//                  'No'                                  enforce_avs,
//                  'No'                                  enforce_card_verification,
//                  0                                     min_cc_transaction,
//                  99999                                 max_cc_transaction,
//                  mc.merchcont_prim_first_name||' '||
//                  mc.merchcont_prim_last_name           contact_name,
//                  a.address_line1                       contact_address,
//                  a.address_city                        contact_city,
//                  a.countrystate_code                   contact_state,
//                  a.address_zip                         contact_zip,
//                  'US'                                  contact_country,
//                  m.merch_email_address                 contact_email,
//                  ''                                    contact_email2,
//                  mc.merchcont_prim_phone               contact_phone,
//                  ''                                    contact_phone2,
//                  mb.merchbank_name                     ach_bank_name,
//                  mb.merchbank_transit_route_num        ach_bank_number,
//                  mb.merchbank_acct_num                 ach_account_number
//          from    application app,
//                  merchant m,
//                  v_numbers vn,
//                  time_zones tz,
//                  address a,
//                  app_account_complete  acc,
//                  merchcontact mc,
//                  merchbank mb,
//                  merchpayoption visa,
//                  merchpayoption master,
//                  merchpayoption diners,
//                  merchpayoption discover,
//                  merchpayoption jcb,
//                  merchpayoption amex
//          where   app.app_type in (9, 12) and
//                  app.app_seq_num = m.app_seq_num and
//                  m.franchise_code = :franchise and
//                  m.app_seq_num = acc.app_seq_num and
//                  trunc(acc.date_completed) between 
//                    to_date(:beginDate, 'MM/DD/YYYY') and 
//                    to_date(:endDate, 'MM/DD/YYYY') and
//                  m.app_seq_num = vn.app_seq_num and
//                  vn.model_index = 1 and
//                  vn.time_zone = tz.code and
//                  m.app_seq_num = a.app_seq_num and
//                  a.addresstype_code = 1 and
//                  m.app_seq_num = mc.app_seq_num and
//                  m.app_seq_num = mb.app_seq_num and
//                  mb.merchbank_acct_srnum = 1 and
//                  m.app_seq_num = visa.app_seq_num(+) and
//                  visa.cardtype_code(+) = 1 and
//                  m.app_seq_num = master.app_seq_num(+) and
//                  master.cardtype_code(+) = 4 and
//                  m.app_seq_num = diners.app_seq_num(+) and
//                  diners.cardtype_code(+) = 10 and
//                  m.app_seq_num = discover.app_seq_num(+) and
//                  discover.cardtype_code(+) = 14 and
//                  m.app_seq_num = jcb.app_seq_num(+) and
//                  jcb.cardtype_code(+) = 15 and
//                  m.app_seq_num = amex.app_seq_num(+) and
//                  amex.cardtype_code(+) = 16
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.merch_business_name                 name,\n                'AASW'                                isv,\n                to_char(acc.date_completed,\n                        'MM/DD/YYYY')                 activation_date,\n                'Yes'                                 desires_ach,\n                'Yes'                                 accepts_credit_cards,\n                -- here we go with card acceptance\n                decode(visa.cardtype_code, \n                  null, 'N', 'Y')                     accepts_visa,\n                decode(master.cardtype_code, \n                  null, 'N', 'Y')                     accepts_mc,\n                decode(diners.cardtype_code, \n                  null, 'N', 'Y')                     accepts_diners,\n                decode(discover.cardtype_code, \n                  null, 'N', 'Y')                     accepts_discover,\n                decode(jcb.cardtype_code, \n                  null, 'N', 'Y')                     accepts_jcb,\n                decode(amex.cardtype_code, \n                  null, 'N', 'Y')                     accepts_amex,\n                '433239'                              acquirer_bin,\n                '000000'                              agent_number,\n                '000000'                              chain_number,\n                m.merch_number                        merchant_number,\n                to_char(vn.store_number, '0009')      store_number,        \n                to_char(vn.terminal_number, '0009')   terminal_number,\n                m.sic_code                            merchant_category,\n                '840'                                 country_code,\n                '840'                                 currency_code,\n                'D'                                   industry_code,\n                tz.gmt_offset                         time_zone,\n                a.address_city                        merchant_city,\n                a.address_zip                         merchant_zip,\n                a.countrystate_code                   merchant_state,\n                a.address_phone                       merchant_phone,\n                decode(trunc(m.merch_mail_phone_sales/50),\n                        0, '54', '56')                transaction_code,\n                to_char(vn.location_number, '00009')  location_number,\n                'No'                                  enforce_avs,\n                'No'                                  enforce_card_verification,\n                0                                     min_cc_transaction,\n                99999                                 max_cc_transaction,\n                mc.merchcont_prim_first_name||' '||\n                mc.merchcont_prim_last_name           contact_name,\n                a.address_line1                       contact_address,\n                a.address_city                        contact_city,\n                a.countrystate_code                   contact_state,\n                a.address_zip                         contact_zip,\n                'US'                                  contact_country,\n                m.merch_email_address                 contact_email,\n                ''                                    contact_email2,\n                mc.merchcont_prim_phone               contact_phone,\n                ''                                    contact_phone2,\n                mb.merchbank_name                     ach_bank_name,\n                mb.merchbank_transit_route_num        ach_bank_number,\n                mb.merchbank_acct_num                 ach_account_number\n        from    application app,\n                merchant m,\n                v_numbers vn,\n                time_zones tz,\n                address a,\n                app_account_complete  acc,\n                merchcontact mc,\n                merchbank mb,\n                merchpayoption visa,\n                merchpayoption master,\n                merchpayoption diners,\n                merchpayoption discover,\n                merchpayoption jcb,\n                merchpayoption amex\n        where   app.app_type in (9, 12) and\n                app.app_seq_num = m.app_seq_num and\n                m.franchise_code =  :1  and\n                m.app_seq_num = acc.app_seq_num and\n                trunc(acc.date_completed) between \n                  to_date( :2 , 'MM/DD/YYYY') and \n                  to_date( :3 , 'MM/DD/YYYY') and\n                m.app_seq_num = vn.app_seq_num and\n                vn.model_index = 1 and\n                vn.time_zone = tz.code and\n                m.app_seq_num = a.app_seq_num and\n                a.addresstype_code = 1 and\n                m.app_seq_num = mc.app_seq_num and\n                m.app_seq_num = mb.app_seq_num and\n                mb.merchbank_acct_srnum = 1 and\n                m.app_seq_num = visa.app_seq_num(+) and\n                visa.cardtype_code(+) = 1 and\n                m.app_seq_num = master.app_seq_num(+) and\n                master.cardtype_code(+) = 4 and\n                m.app_seq_num = diners.app_seq_num(+) and\n                diners.cardtype_code(+) = 10 and\n                m.app_seq_num = discover.app_seq_num(+) and\n                discover.cardtype_code(+) = 14 and\n                m.app_seq_num = jcb.app_seq_num(+) and\n                jcb.cardtype_code(+) = 15 and\n                m.app_seq_num = amex.app_seq_num(+) and\n                amex.cardtype_code(+) = 16";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.AASWXMLDownload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,franchise);
   __sJT_st.setString(2,beginDate);
   __sJT_st.setString(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.AASWXMLDownload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^7*/
      
      rs = it.getResultSet();
      
      // build XML Document from result set
      Element body = new Element("AASWDoc");
      
      while(rs.next())
      {
        body.addContent(buildMerchant(rs));
      }
      
      rs.close();
      it.close();
      
      doc = new Document(body);
      
      // output to console if debug
      if(debug)
      {
        XMLOutputter fmt = new XMLOutputter();
      
        fmt.output(doc, new PrintWriter(System.out, true));
      }
      
      result = true;
    }
    catch(Exception e)
    {
      errorString = e.toString();
      logEntry("buildXML(" + beginDate + ", " + endDate + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  private Element buildMerchant(ResultSet rs)
    throws Exception 
  {
    Element merchant = new Element("Merchant");
    
    merchant.addContent(new Element("Name").setText(rs.getString("name")));
    merchant.addContent(new Element("ISV").setText(rs.getString("isv")));
    merchant.addContent(new Element("ActivationDate").setText(rs.getString("activation_date")));
    merchant.addContent(new Element("DesiresACH").setText(rs.getString("desires_ach")));
    merchant.addContent(new Element("AcceptsCreditCards").setText(rs.getString("accepts_credit_cards")));
    merchant.addContent(buildCardAcceptance(rs));
    merchant.addContent(new Element("BankIdentificationNumber").setText(rs.getString("acquirer_bin")));
    merchant.addContent(new Element("AgentNumber").setText(rs.getString("agent_number")));
    merchant.addContent(new Element("ChainNumber").setText(rs.getString("chain_number")));
    merchant.addContent(new Element("MerchantNumber").setText(rs.getString("merchant_number")));
    merchant.addContent(new Element("StoreNumber").setText(rs.getString("store_number")));
    merchant.addContent(new Element("TerminalNumber").setText(rs.getString("terminal_number")));
    merchant.addContent(new Element("MerchantCategory").setText(rs.getString("merchant_category")));
    merchant.addContent(new Element("CountryCode").setText(rs.getString("country_code")));
    merchant.addContent(new Element("CurrencyCode").setText(rs.getString("currency_code")));
    merchant.addContent(new Element("IndustryCode").setText(rs.getString("industry_code")));
    merchant.addContent(new Element("TimeZone").setText(rs.getString("time_zone")));
    merchant.addContent(new Element("MerchantCity").setText(rs.getString("merchant_city")));
    merchant.addContent(new Element("MerchantZip").setText(rs.getString("merchant_zip")));
    merchant.addContent(new Element("MerchantState").setText(rs.getString("merchant_state")));
    merchant.addContent(new Element("MerchantPhone").setText(rs.getString("merchant_phone")));
    merchant.addContent(new Element("TransactionCode").setText(rs.getString("transaction_code")));
    merchant.addContent(new Element("MerchantLocationNumber").setText(rs.getString("location_number")));
    merchant.addContent(new Element("EnforceAVS").setText(rs.getString("enforce_avs")));
    merchant.addContent(new Element("EnforceCardVerification").setText(rs.getString("enforce_card_verification")));
    merchant.addContent(new Element("MinCCTransactionAmount").setText(rs.getString("min_cc_transaction")));
    merchant.addContent(new Element("MaxCCTransactionAmount").setText(rs.getString("max_cc_transaction")));
    merchant.addContent(new Element("ContactName").setText(rs.getString("contact_name")));
    merchant.addContent(new Element("ContactAddress").setText(rs.getString("contact_address")));
    merchant.addContent(new Element("ContactCity").setText(rs.getString("contact_city")));
    merchant.addContent(new Element("ContactState").setText(rs.getString("contact_state")));
    merchant.addContent(new Element("ContactZip").setText(rs.getString("contact_zip")));
    merchant.addContent(new Element("ContactCountry").setText(rs.getString("contact_country")));
    merchant.addContent(new Element("ContactEmail").setText(rs.getString("contact_email")));
    merchant.addContent(new Element("ContactEmail2").setText(rs.getString("contact_email2")));
    merchant.addContent(new Element("ContactPhone").setText(rs.getString("contact_phone")));
    merchant.addContent(new Element("ContactPhone2").setText(rs.getString("contact_phone2")));
    merchant.addContent(new Element("ACHBankName").setText(rs.getString("ach_bank_name")));
    merchant.addContent(new Element("ACHBankNumber").setText(rs.getString("ach_bank_number")));
    merchant.addContent(new Element("ACHAccountNumber").setText(rs.getString("ach_account_number")));
    
    return merchant;
  }
  
  private Element buildCardAcceptance(ResultSet rs)
    throws Exception 
  {
    Element cards = new Element("AcceptedCardTypes");
    
    if(rs.getString("accepts_visa").equals("Y"))
    {
      cards.addContent(new Element("AcceptedCard").setText("Visa"));
    }
    
    if(rs.getString("accepts_mc").equals("Y"))
    {
      cards.addContent(new Element("AcceptedCard").setText("MasterCard"));
    }
    
    if(rs.getString("accepts_amex").equals("Y"))
    {
      cards.addContent(new Element("AcceptedCard").setText("American Express"));
    }
    
    if(rs.getString("accepts_discover").equals("Y"))
    {
      cards.addContent(new Element("AcceptedCard").setText("Discover"));
    }
    
    if(rs.getString("accepts_diners").equals("Y"))
    {
      cards.addContent(new Element("AcceptedCard").setText("Diners"));
    }
    
    if(rs.getString("accepts_jcb").equals("Y"))
    {
      cards.addContent(new Element("AcceptedCard").setText("JCB"));
    }
    
    return cards;
  }
  
  public void setErrorString(String error)
  {
    errorString = error;
  }
  
  public void buildErrorXML()
  {
    try
    {
      Element body = new Element("AASWDoc");
      
      body.addContent(new Element("ERROR").setText(errorString));
      
      doc = new Document(body);
      
      // output to console if in debug mode
      if(debug)
      {
        XMLOutputter fmt = new XMLOutputter();
      
        PrintWriter system = new PrintWriter(System.out, true);
      
        fmt.output(doc, system);
      
        system.flush();
      }
    }
    catch(Exception e)
    {
      logEntry("buildErrorXML()", e.toString());
    }
  }
  
  public void setDebug(boolean flag)
  {
    debug = flag;
  }
}/*@lineinfo:generated-code*/