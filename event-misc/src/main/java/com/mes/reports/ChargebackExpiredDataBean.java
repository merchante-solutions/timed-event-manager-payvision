/*@lineinfo:filename=ChargebackExpiredDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ChargebackExpiredDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/25/03 5:22p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class ChargebackExpiredDataBean extends ReportSQLJBean
{
  public class RowData implements Comparable
  {
    public String     CardNumber          = null;
    public int        DaysAllowed         = 0;
    public int        DaysBetween         = 0;
    public String     DbaName             = null;
    public long       HierarchyNode       = 0L;
    public Date       IncomingDate        = null;
    public long       OrgId               = 0L;
    public String     ReasonDesc          = null;
    public String     ReferenceNumber     = null;
    public Date       RetrievalDate       = null;
    public double     TranAmount          = 0.0;
    public Date       TranDate            = null;
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      IncomingDate      = resultSet.getDate("incoming_date");
      CardNumber        = resultSet.getString("card_number");
      ReferenceNumber   = resultSet.getString("ref_num");
      TranAmount        = resultSet.getDouble("tran_amount");
      TranDate          = resultSet.getDate("tran_date");
      DaysAllowed       = resultSet.getInt("days_allowed");
      DaysBetween       = resultSet.getInt("days_between");
      DbaName           = resultSet.getString("dba_name");
      ReasonDesc        = resultSet.getString("reason_desc");
      RetrievalDate     = resultSet.getDate("rt_incoming_date");
    }
    
    public int compareTo( Object obj )
    {
      RowData       data        = (RowData)obj;
      int           retVal      = 0;
      
      if ( ( retVal = (int)(HierarchyNode - data.HierarchyNode) ) == 0 )
      {
        if ( ( retVal = CardNumber.compareTo(data.CardNumber) ) == 0 )
        {
          retVal = IncomingDate.compareTo(data.IncomingDate);
        }
      }
      return( retVal );
    }

    public String getFormattedCompareDate( )
    {
      String    retVal      = null;

      if ( RetrievalDate == null )
      {
        retVal = DateTimeFormatter.getFormattedDate(TranDate,"MM/dd/yyyy");
      }
      else
      {
        retVal = DateTimeFormatter.getFormattedDate(RetrievalDate,"MM/dd/yyyy");
        retVal = retVal.concat(" (R)");
      }
      return( retVal );
    }
  }

  public ChargebackExpiredDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Merchant Id\",");
    line.append("\"DBA Name\",");
    line.append("\"Incoming Date\",");
    line.append("\"Tran Date\",");
    line.append("\"Retr Date\",");
    line.append("\"Date Used\",");
    line.append("\"Days Between\",");
    line.append("\"Days Allowed\",");
    line.append("\"Card Number\",");
    line.append("\"Tran Amount\",");
    line.append("\"Reference Number\",");
    line.append("\"Reason Description\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData record = (RowData) obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append( record.HierarchyNode );
    line.append( ",\"" );
    line.append( record.DbaName );
    line.append( "\"," );
    line.append( DateTimeFormatter.getFormattedDate(record.IncomingDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.TranDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.RetrievalDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( ((record.RetrievalDate == null) ? "T" : "R") );
    line.append( "," );
    line.append( record.DaysBetween );
    line.append( "," );
    line.append( record.DaysAllowed );
    line.append( "," );
    line.append( record.CardNumber );
    line.append( "," );
    line.append( record.TranAmount );
    line.append( "," );
    line.append( record.ReferenceNumber );
    line.append( ",\"" );
    line.append( record.ReasonDesc );
    line.append( "\"" );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    SimpleDateFormat        rptDate   = new SimpleDateFormat("MMMyyyy");
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append("expired_chargebacks_");
    
    // build the first date into the filename
    filename.append( rptDate.format( ReportDateBegin ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( rptDate.format( ReportDateEnd ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:210^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.merchant_number                          as hierarchy_node,
//                  mf.dba_name                                 as dba_name,
//                  cb.incoming_date                            as incoming_date,
//                  rt.incoming_date                            as rt_incoming_date,
//                  cb.tran_date                                as tran_date,
//                  (rd.reason_desc || ' (' || 
//                   cb.reason_code || ')')                     as reason_desc,
//                  trunc(cb.incoming_date - 
//                        nvl(rt.incoming_date,cb.tran_date))   as days_between,
//                  nvl(rd.time_frame,120)                      as days_allowed,
//                  cb.card_number                              as card_number,
//                  cb.tran_amount                              as tran_amount,
//                  cb.reference_number                         as ref_num
//          from    organization                o,
//                  group_merchant              gm,
//                  group_rep_merchant          grm,
//                  network_chargebacks         cb,
//                  network_retrievals          rt,
//                  chargeback_reason_desc      rd,
//                  mif                         mf
//          where   o.org_num   = :orgId and
//                  gm.org_num  = o.org_num and
//                  grm.user_id(+) = :AppFilterUserId and
//                  grm.merchant_number(+) = gm.merchant_number and
//                  ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                  cb.merchant_number = gm.merchant_number and
//                  cb.incoming_date between :beginDate and :endDate and
//                  rd.reason_code(+) = cb.reason_code and
//                  rd.card_type(+)   = decode(substr(cb.card_number,1,1),'4','VS','MC') and
//                  rd.item_type(+)   = 'C' and
//                  trunc(cb.incoming_date - nvl(rt.incoming_date,cb.tran_date)) > nvl(rd.time_frame,120) and
//  --                trunc(cb.incoming_date - cb.tran_date) > nvl(rd.time_frame,120) and
//                  mf.merchant_number(+) = cb.merchant_number and
//                  rt.merchant_number(+) = cb.merchant_number and
//                  rt.incoming_date(+)  <= cb.incoming_date and
//                  rt.card_number(+)     = cb.card_number and
//                  rt.reference_number(+)= cb.reference_number
//  --        order by cb.merchant_number, cb.card_number, cb.incoming_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.merchant_number                          as hierarchy_node,\n                mf.dba_name                                 as dba_name,\n                cb.incoming_date                            as incoming_date,\n                rt.incoming_date                            as rt_incoming_date,\n                cb.tran_date                                as tran_date,\n                (rd.reason_desc || ' (' || \n                 cb.reason_code || ')')                     as reason_desc,\n                trunc(cb.incoming_date - \n                      nvl(rt.incoming_date,cb.tran_date))   as days_between,\n                nvl(rd.time_frame,120)                      as days_allowed,\n                cb.card_number                              as card_number,\n                cb.tran_amount                              as tran_amount,\n                cb.reference_number                         as ref_num\n        from    organization                o,\n                group_merchant              gm,\n                group_rep_merchant          grm,\n                network_chargebacks         cb,\n                network_retrievals          rt,\n                chargeback_reason_desc      rd,\n                mif                         mf\n        where   o.org_num   =  :1  and\n                gm.org_num  = o.org_num and\n                grm.user_id(+) =  :2  and\n                grm.merchant_number(+) = gm.merchant_number and\n                ( not grm.user_id is null or  :3  = -1 ) and        \n                cb.merchant_number = gm.merchant_number and\n                cb.incoming_date between  :4  and  :5  and\n                rd.reason_code(+) = cb.reason_code and\n                rd.card_type(+)   = decode(substr(cb.card_number,1,1),'4','VS','MC') and\n                rd.item_type(+)   = 'C' and\n                trunc(cb.incoming_date - nvl(rt.incoming_date,cb.tran_date)) > nvl(rd.time_frame,120) and\n--                trunc(cb.incoming_date - cb.tran_date) > nvl(rd.time_frame,120) and\n                mf.merchant_number(+) = cb.merchant_number and\n                rt.merchant_number(+) = cb.merchant_number and\n                rt.incoming_date(+)  <= cb.incoming_date and\n                rt.card_number(+)     = cb.card_number and\n                rt.reference_number(+)= cb.reference_number\n--        order by cb.merchant_number, cb.card_number, cb.incoming_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ChargebackExpiredDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.ChargebackExpiredDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:250^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData(resultSet) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties( HttpServletRequest request )
  {
    super.setProperties(request);
    
    if ( usingDefaultReportDates() )
    {
      Calendar          cal = Calendar.getInstance();
      
      cal.setTime(ReportDateBegin);
      cal.add(Calendar.DAY_OF_MONTH,-1);
      
      ReportDateBegin = new java.sql.Date( cal.getTime().getTime() );
      ReportDateEnd   = new java.sql.Date( cal.getTime().getTime() );
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/