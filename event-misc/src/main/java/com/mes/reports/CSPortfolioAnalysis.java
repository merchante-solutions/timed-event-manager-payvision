/*@lineinfo:filename=CSPortfolioAnalysis*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/CSPortfolioAnalysis.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-02-06 11:17:09 -0800 (Tue, 06 Feb 2007) $
  Version            : $Revision: 13414 $

  Change History:
     See VSS database

  Copyright (C) 2000-2005,2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.ComboDateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HierarchyNodeField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.tools.HierarchyTree;

public class CSPortfolioAnalysis extends ReportSQLJBean
{
  public class RowData
  {
    public  int         AccountsOpened            = 0;
    public  int         AccountsClosed            = 0;
    public  int         AccountsTotal             = 0;
    public  int         AccountsNet               = 0;
    public  double      SalesAmount               = 0.0;
    public  double      SalesAmountPrev           = 0.0;
    public  int         SalesCount                = 0;
    public  int         SalesCountPrev            = 0;
    public  double      MesProfitLoss             = 0.0;
    public  double      PortfolioProfitLoss       = 0.0;
    public  double      SalesAttritionAmount      = 0.0;
   
    public  Date        ActiveDate                = null; 
    public  String      PortfolioName             = null;
    public  long        PortfolioNode             = 0L;
    
    public RowData( Date activeDate, long nodeId, String nodeName )
    {
      ActiveDate    = activeDate;  
      PortfolioNode = nodeId;
      PortfolioName = nodeName;
    }
    
    public void addData( CSActivityDataBean.RowData row )
    {
      AccountsOpened            += row.MerchantCounts[CSActivityDataBean.MC_OPEN];
      AccountsClosed            += row.MerchantCounts[CSActivityDataBean.MC_CLOSED];
      AccountsTotal             += row.MerchantCounts[CSActivityDataBean.MC_TOTAL];
      AccountsNet               = (AccountsOpened - AccountsClosed);
      SalesAmount               += row.SalesAmount;
      SalesAttritionAmount      += row.SalesAmountClosed;
    }
    
    public void addData( BankContractDataBean.SummaryData childSummary, boolean mesData )
    {
      double  adjustmentsExp    = childSummary.getAdjustmentsExpense();
      double  adjustmentsInc    = childSummary.getAdjustmentsIncome();
      double  discIcExpense     = childSummary.getDiscIcExpense();
      double  discIcIncome      = childSummary.getDiscIcIncome();
      double  excludedEquipExp  = childSummary.getExcludedEquipExpense();
      double  excludedFeesExp   = childSummary.getExcludedFeesExpense();
      double  feesExpense       = childSummary.getFeesTotalExpense();
      double  feesIncome        = childSummary.getFeesTotalIncome();
      double  partnershipExp    = childSummary.getPartnershipExpense();
      double  partnershipInc    = childSummary.getPartnershipIncome();
      double  referralExp       = childSummary.getReferralContractExpense();
      double  salesTax          = childSummary.getEquipSalesTax();
      
      // calculated fields
      double  netProcPL         = 0.0;
      double  netProfitLoss     = 0.0;
      
      netProcPL     = ( ( discIcIncome + feesIncome + partnershipInc + adjustmentsInc ) - 
                        ( discIcExpense + feesExpense + partnershipExp + adjustmentsExp ) );
      
      netProfitLoss = ( netProcPL - ( excludedEquipExp + excludedFeesExp ) );
      
      // 3941 excludes sales tax from the amount returned by 
      // SummaryData.getFeesTotalIncome() to allow it to appear
      // in a separate column on this report.  It needs to be 
      // added back in manually to the net P/L.
      if ( getReportBankId() == mesConstants.BANK_ID_MES )
      {
        netProfitLoss += salesTax;
      }
    
      if ( mesData )
      {
        MesProfitLoss       += netProfitLoss;
      }
      else
      {
        PortfolioProfitLoss += netProfitLoss;
        SalesCount          += (childSummary.VmcSalesCount + childSummary.CashAdvanceCount);
      }
    }

    public double getAverageTicket()
    {
      return( SalesAmount/((SalesCount == 0) ? 1 : SalesCount) );
    }
    
    public double getProfitPerMerchant()
    {
      return( PortfolioProfitLoss/(double)AccountsTotal );
    }
    
    public String getSalesAmountChangePercentString()
    {
      String    retVal    = "";
      
      if ( SalesAmountPrev != 0.0 && SalesAmount != 0.0 )
      {
        retVal = NumberFormatter.getPercentString( ((SalesAmount - SalesAmountPrev)/SalesAmount),3 );
      }
      return( retVal );
    }
    
    public String getSalesCountChangePercentString()
    {
      String    retVal    = "";
      
      if ( SalesCountPrev != 0.0 && SalesCount != 0.0 )
      {
        retVal = NumberFormatter.getPercentString( ((double)(SalesCount - SalesCountPrev)/(double)SalesCount),3 );
      }
      return( retVal );
    }
    
    public void setPrevMonthData( RowData prevMonth )
    {
      if ( prevMonth != null )
      {
        SalesAmountPrev = prevMonth.SalesAmount;
        SalesCountPrev  = prevMonth.SalesCount;
      }
    }
  }
  
  protected HttpServletRequest      ReportRequest   = null;
  
  public CSPortfolioAnalysis( )
  {
    super(true);    // enable field bean support
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup        fgroup;
    Field             field;
    
    super.createFields(request);
    
    fgroup = (FieldGroup)getField("searchFields");
    
    // delete and re-create to maintain the order
    // and remove the day field from the date selection
    fgroup.deleteField("beginDate");
    fgroup.deleteField("endDate");
    fgroup.deleteField("portfolioId");
    fgroup.deleteField("zip2Node");
    
    field = new ComboDateField("beginDate", "Report Begin Date", false, false, 2000, false);
    ((ComboDateField)field).setExpansionFlag(false);
    fgroup.add(field);
    
    field = new ComboDateField("endDate", "Report End Date", false, false, 2000, false);
    ((ComboDateField)field).setExpansionFlag(false);
    fgroup.add(field);
    
    long userNodeId = getUser().getHierarchyNode();
    if ( userNodeId == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      field = new DropDownField("portfolioId","Portfolio",new PortfolioTable(userNodeId),true);
      field.setData( String.valueOf(userNodeId) );
      fgroup.add(field);
    }
    
    if ( !isNodeMerchant(userNodeId) )
    {
      field = new HierarchyNodeField( Ctx, userNodeId, "zip2Node","Node ID", true );
      fgroup.add(field);
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Billing Month\",");
    line.append("\"Open Accounts\",");
    line.append("\"Closed Accounts\",");
    line.append("\"Total Accounts\",");
    line.append("\"Net Increase in Accts\",");
    line.append("\"Sales Volume\",");
    if ( ReportUserBean.getHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      line.append("\"MeS Profit/Loss\",");
    }      
    line.append("\"Bank Profit/Loss\",");
    line.append("\"% Change Profit from Prior Year\",");
    line.append("\"Transaction Volume\",");
    line.append("\"% Change Sales Vol Prev Month\",");
    line.append("\"% Change Trans Vol Prev Month\",");
    line.append("\"Profit Per Merchant\",");
    line.append("\"Average Ticket (Sales Vol/#Sales)\",");
    line.append("\"Sales Volume Attrition (Projected Lost Sales)\",");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    line.setLength(0);

    RowData   record = (RowData)obj;
    
    line.append(DateTimeFormatter.getFormattedDate(record.ActiveDate,"MMM-yyyy"));
    line.append(",");
    line.append(record.AccountsOpened);
    line.append(",");
    line.append(record.AccountsClosed);
    line.append(",");
    line.append(record.AccountsTotal);
    line.append(",");
    line.append(record.AccountsNet);
    line.append(",");
    line.append(record.SalesAmount);
    line.append(",");
    if ( ReportUserBean.getHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      line.append(record.MesProfitLoss);
      line.append(",");
    }      
    line.append(record.PortfolioProfitLoss);
    line.append(",,");
    line.append(record.SalesCount);
    line.append(",");
    line.append(record.getSalesAmountChangePercentString());
    line.append(",");
    line.append(record.getSalesCountChangePercentString());
    line.append(",");
    line.append(MesMath.round(record.getProfitPerMerchant(),2));
    line.append(",");
    line.append(MesMath.round(record.getAverageTicket(),2));
    line.append(",");
    line.append(record.SalesAttritionAmount);
  }
  
  public String getDownloadFilenameBase()
  {
    Date              beginDate   = getReportDateBegin();
    Date              endDate     = getReportDateEnd();
    StringBuffer      filename    = new StringBuffer();
    
    filename.append(getReportHierarchyNode());
    filename.append("_portfolio_analysis_");
    filename.append( DateTimeFormatter.getFormattedDate(beginDate,"MMMyyyy") );
    if ( !beginDate.equals(endDate) )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(endDate,"MMMyyyy") );
    }
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
        
      default:
        break;
    }
    return(retVal);
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    Date                  activeDate        = null;
    CSActivityDataBean    activityDataBean  = null;
    Calendar              cal               = Calendar.getInstance();
    BankContractDataBean  contractDataBean  = null;
    RowData               lastRow           = null;
    long                  nodeId            = orgIdToHierarchyNode(orgId);
    RowData               row               = null;
    Vector                rows              = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( nodeId != HierarchyTree.DEFAULT_HIERARCHY_NODE &&
           nodeId != 394100000L )
      {
        // create and initialize the required report beans
        activityDataBean = new CSActivityDataBean();
        activityDataBean.connect(true);
        activityDataBean.initialize();
        activityDataBean.setReportUserBean( ReportUserBean );
        activityDataBean.setProperties( ReportRequest );
        
        contractDataBean = new BankContractDataBean();
        contractDataBean.connect(true);
        contractDataBean.initialize();
        contractDataBean.setReportUserBean( ReportUserBean );
        contractDataBean.setProperties( ReportRequest );
      
        activeDate = endDate;   // set the report active date
      
        while( true )
        {
          // add a result row
          row = new RowData( activeDate, nodeId, getNodeName(nodeId) );
          ReportRows.add(row);
          
          // load the client services data
          activityDataBean.setReportHierarchyNodeDefault(nodeId);
          activityDataBean.setReportHierarchyNode(nodeId);
          activityDataBean.setReportDateBegin(activeDate);
      
          // set the end date to the end of the month
          cal.setTime(activeDate);
          cal.add(Calendar.MONTH,1);
          cal.add(Calendar.DAY_OF_MONTH,-1);
          activityDataBean.setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
      
          activityDataBean.loadData();
          rows = activityDataBean.getReportRows();
      
          for( int i = 0; i < rows.size(); ++i )
          {
            row.addData( (CSActivityDataBean.RowData) rows.elementAt(i) );
          }
      
          // load the inc/exp data from the MES perspective
          contractDataBean.setReportHierarchyNodeDefault(HierarchyTree.DEFAULT_HIERARCHY_NODE);
          contractDataBean.setReportHierarchyNode(nodeId);
          contractDataBean.setReportDateBegin(activeDate);
          contractDataBean.setReportDateEnd(activeDate);
          contractDataBean.loadData();
          rows = contractDataBean.getReportRows();
      
          for( int i = 0; i < rows.size(); ++i )
          {
            row.addData( (BankContractDataBean.SummaryData) rows.elementAt(i), true );
          }
          
          // load the inc/exp from the bank perspective
          contractDataBean.setReportHierarchyNodeDefault(nodeId);
          contractDataBean.loadData();
          rows = contractDataBean.getReportRows();
          for( int i = 0; i < rows.size(); ++i )
          {
            row.addData( (BankContractDataBean.SummaryData) rows.elementAt(i), false );
          }
          
          // since we are loading in descending order
          // we have to post-populate the previous month data
          if ( lastRow != null )
          {
            lastRow.setPrevMonthData(row);
          }
          lastRow = row;
          
          // going backwards from end to begin
          if ( activeDate.equals(beginDate) )
          {
            break;    // done with the loop
          }
          
          // otherwise setup for the next iteration
          cal.setTime(activeDate);
          cal.add(Calendar.MONTH,-1);
          activeDate = new java.sql.Date(cal.getTime().getTime());
        }
        activityDataBean.cleanUp();
        contractDataBean.cleanUp();
      }        
    }
    catch( Exception e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest( request );
    
    String  fieldName = null;
    Date    tempDate  = null;
    for ( int i = 0; i < 2; ++i )
    {
      switch(i)
      {
        case 0:
          fieldName = "beginDate";
          tempDate  = getReportDateBegin();
          break;
        
        case 1:
          fieldName = "endDate";
          tempDate  = getReportDateEnd();
          break;
          
        default:
          continue;
      }          
    
      Calendar cal = Calendar.getInstance();
      if ( tempDate != null )
      {
        cal.setTime(tempDate);
      }
    
      // if a begin date was not specified in the request, set the 
      // default to the previous month.
      if ( HttpHelper.getString(request,(fieldName + ".month"),null) == null )
      {
        cal.add( Calendar.MONTH, -1 );
      }
    
      // always set to the first of the month
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      switch(i)
      {
        case 0:
          setReportDateBegin( new java.sql.Date(cal.getTime().getTime()) );
          break;
          
        case 1:
          setReportDateEnd( new java.sql.Date(cal.getTime().getTime()) );
          break;
      }
    }
    
    ReportRequest = request;  // store reference the the request
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/