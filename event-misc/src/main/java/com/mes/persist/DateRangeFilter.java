package com.mes.persist;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DateRangeFilter extends AbstractFilter
{
  private DateHelper dh;

  public DateRangeFilter(String colName, DateHelper dh)
  {
    this.colName = colName;
    this.dh = dh;
  }
  public DateRangeFilter(String colName, java.sql.Date fromDate, 
    java.sql.Date toDate)
  {
    this(colName,new DateHelper(fromDate,toDate));
  }
  public DateRangeFilter(String colName, java.util.Date fromDate, 
    java.util.Date toDate)
  {
    this(colName,new DateHelper(fromDate,toDate));
  }
  public DateRangeFilter(String colName, java.util.Date fromDate)
  {
    this(colName, new DateHelper(fromDate));
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    return "trunc(" + td.getColumnDef(colName).getQualifiedName(qualifier)
      + ") between ? and ?";
  }

  public String getAuditLog()
  {
    return "trunc(" + td.getColumnDef(colName).getQualifiedName(qualifier)
      + ") between " + dh.getSqlFrom() + " and " + dh.getSqlTo();
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    ps.setDate(mark++,dh.getSqlFrom());
    ps.setDate(mark++,dh.getSqlTo());
    return mark;
  }
}