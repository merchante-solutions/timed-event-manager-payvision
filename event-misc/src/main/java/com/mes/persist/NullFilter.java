package com.mes.persist;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class NullFilter extends AbstractFilter
{
  private boolean isNull;

  public NullFilter(String colName, boolean isNull)
  {
    this.colName = colName;
    this.isNull = isNull;
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    return td.getColumnDef(colName).getQualifiedName(qualifier)
      + " is " + (isNull ? "null" : "not null");
  }

  public String getAuditLog()
  {
    return " " + td.getColumnDef(colName).getQualifiedName(qualifier) 
      + " is " + (isNull ? "null" : "not null");
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    return mark;
  }
}