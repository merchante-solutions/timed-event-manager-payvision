package com.mes.persist;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DateFilter extends AbstractFilter
{
  public static final String DFT_ON     = "on";
  public static final String DFT_BEFORE = "before";
  public static final String DFT_AFTER  = "after";

  private String dfType;
  private Date dfDate;

  public DateFilter(String colName, Date dfDate, String dfType)
  {
    if (!DFT_ON.equals(dfType) &&
        !DFT_BEFORE.equals(dfType) &&
        !DFT_AFTER.equals(dfType))
    {
      throw new RuntimeException("Invalid date filter type: " + dfType);
    }
    this.colName = colName;
    this.dfDate = dfDate;
    this.dfType = dfType;
  }
  public DateFilter(String colName, Date dfDate)
  {
    this(colName,dfDate,DFT_ON);
  }
  public DateFilter(String colName, java.util.Date utilDate, String dfType)
  {
    this(colName,DateHelper.toSql(utilDate),dfType);
  }
  public DateFilter(String colName, java.util.Date utilDate)
  {
    this(colName,DateHelper.toSql(utilDate));
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    StringBuffer buf = new StringBuffer();
    buf.append("trunc( ");
    buf.append(td.getColumnDef(colName).getQualifiedName(qualifier));
    buf.append(" )");
    if (DFT_ON.equals(dfType))
    {
      buf.append(" = trunc( ? )");
    }
    else if (DFT_BEFORE.equals(dfType)) 
    {
      buf.append(" < ?");
    }
    else
    {
      buf.append(" > ?");
    }
    return buf.toString();
  }

  public String getAuditLog()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("trunc( ");
    buf.append(td.getColumnDef(colName).getQualifiedName(qualifier));
    buf.append(" )");
    if (DFT_ON.equals(dfType))
    {
      buf.append(" = trunc( " + dfDate + " )");
    }
    else if (DFT_BEFORE.equals(dfType)) 
    {
      buf.append(" < " + dfDate);
    }
    else
    {
      buf.append(" > " + dfDate);
    }
    return buf.toString();
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    ps.setDate(mark++,dfDate);
    return mark;
  }
}