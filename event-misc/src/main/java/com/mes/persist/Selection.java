package com.mes.persist;

import java.util.List;

public interface Selection extends Auditable
{
  public boolean hasBean();
  public boolean hasIndexes();
  public boolean hasFilters();
  public boolean isJoin();

  public void addIndex(String idxName);
  public void addSubIndex(String idxName, String subName, Object subBean);
  public void addFilter(Filter filter, String refName);
  public void addFilter(Filter filter);
  //public void addSubFilter(Filter filter, String subName, Object subBean);
  public void addOrder(String colName, boolean descending);
  public void addSubtableOrder(String tblName, String colName, boolean descending);

  public void addJoinIndex(String idxName, Object idxBean);
  public void addJoinFilter(Filter filter, String tblName);
  public void addJoinOrder(String tblName, String colName, boolean descending);
  public void addMapValueOrder(String tblName, String colName, boolean descending);

  public void clearOrders();

  public void addSubSel(Selection subSel, String colName);
  public List getSubSels();
  public String getSubSelColName(Selection subSel);
  public int getSubSelNum(Selection subSel);

  public Class getBeanClass();
  public Object getBean();
  public TableDef getTableDef();
  public JoinDef getJoinDef();
  public TableDef getMapTableDef();
  public Record getRecordForBean(Object bean);
  public List getIndexes();
  public Index[] getIndexArray();
  public List getFilters();
  public Filter[] getFilterArray();
  public List getOrders();
  public Order[] getOrderArray();
  public List getMapValueOrders();
  public Order[] getMapValueOrderArray();

  public Index getIndex(String idxName);

  public void setPageSize(int pageSize);
  public void setPageNum(int pageNum);

  public int getPageSize();
  public int getPageNum();
  public int getStartRowNum();
  public int getEndRowNum();
}