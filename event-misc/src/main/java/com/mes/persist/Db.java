/**
 * A base class for package database managers.
 *
 * Provides child classes with ability to select, persist and delete Persistent
 * items.
 * 
 * Also provides audit logging functionality and a mechanism for generating 
 * unique id's within a db's table set.  
 */
package com.mes.persist;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public abstract class Db
{
  public static final String  DBA_INSERT  = "insert";
  public static final String  DBA_UPDATE  = "udpate";
  public static final String  DBA_DELETE  = "delete";
  public static final String  DBA_SELECT  = "select";

  private static Map dbSources = new Hashtable();

  public static void registerDbSource(Class dbClass, SchemaFactory factory, 
    URL schemaUrl, File schemaFile)
  {
    if (!Db.class.isAssignableFrom(dbClass))
    {
      throw new ClassCastException("Db class " + dbClass.getName() + " must be of type " 
        + Db.class);
    }
    dbSources.put(dbClass,new DbSource(factory,schemaUrl,schemaFile));
  }
  public static void registerDbSource(Class dbClass, SchemaFactory factory,
    URL schemaUrl)
  {
    registerDbSource(dbClass,factory,schemaUrl,null);
  }
  public static void registerDbSource(Class dbClass, SchemaFactory factory,
    File schemaFile)
  {
    registerDbSource(dbClass,factory,null,schemaFile);
  }

  public static Schema getSchema(Class dbClass)
  {
    if (dbClass == null)
    {
      throw new NullPointerException("Null db class");
    }
    DbSource source = (DbSource)dbSources.get(dbClass);
    if (source == null)
    {
      throw new NullPointerException("No db source registered for class " 
        + dbClass);
    }
    return source.getSchema();
  }
  
  public Date getCurDate()
  {
    return Calendar.getInstance().getTime();
  }

  public static String formatDlDate(Date date)
  {
    return formatDlDate(date,"MM/dd/yy h:mm a");
  }
  public static String formatDlDate(Date date, String fmt)
  {
    SimpleDateFormat sdf = new SimpleDateFormat(fmt);
    return sdf.format(date);
  }

  public abstract long getNewId();

  public abstract User validateUser(Object user, String caller);
  
  public abstract void logAuditData(Auditable auditItem, Object userObj, String dbAction);
  public abstract void logAuditData(Auditable auditItem, Object userObj, String dbAction,
    String logData);
  
  public abstract void persist(Object bean, Object user);
  
  public abstract Selection getSelection(Class beanClass);
  public abstract Selection getSelection(Object bean);
  public abstract Selection getSelection(Object bean, boolean autoIdx);
  public abstract Selection getSelection(String joinName);
  public abstract Selection getSelection(Record record);

  public abstract Object select(Selection selection);
  public abstract List selectAll(Selection selection);
  public abstract Map selectJoinMap(Selection selection);
  public abstract int selectCount(Selection selection);

  public abstract void startSelection(Selection selection);
  public abstract Object getNext();
  public abstract void finishSelection();

  public abstract void startManualStatement(PreparedStatement ps);
  public abstract ResultSet getDlRs();
  public abstract void finishManualStatement();

  public abstract void delete(Selection selection, Object user);

  public abstract boolean inTransaction();
  public abstract long startTransaction();
  public abstract void commitTransaction();
  public abstract void rollbackTransaction();
  public abstract void finishTransaction();

  public abstract Connection getConnection();

  public abstract void doConnect();
  public abstract void doDisconnect();
  public abstract boolean isConnected();
}