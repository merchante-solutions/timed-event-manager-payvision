package com.mes.persist.mes;

import org.apache.log4j.Logger;
import com.mes.persist.Record;
import com.mes.persist.TableDef;

public class MesUpdate extends MesExecutable
{
  static Logger log = Logger.getLogger(MesUpdate.class);

  public MesUpdate(Record record)
  {
    super(record,ET_UPDATE);
  }

  protected String createStatement()
  {
    TableDef td = record.getTableDef();
    return "UPDATE\n" + td.getQualifiedName() + "\n"
         + "SET\n" + td.getUpdateSetList() + "\n"
         + "WHERE\n" + record.getPrimaryKey().getWhereClause();
  }

  protected void setMarks()
  {
    setIndexMarks(record.getPrimaryKey(),setColumnMarks());
  }
}