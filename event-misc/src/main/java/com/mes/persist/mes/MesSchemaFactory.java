package com.mes.persist.mes;

import java.io.File;
import java.net.URL;
import com.mes.persist.Schema;
import com.mes.persist.SchemaFactory;

public class MesSchemaFactory implements SchemaFactory
{
  public Schema createSchema(URL schemaUrl)
  {
    return (new MesXmlSchemaParser()).parseXml(schemaUrl);
  }

  public Schema createSchema(File schemaFile)
  {
    return (new MesXmlSchemaParser()).parseXml(schemaFile);
  }
}
