package com.mes.persist.mes;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.apache.log4j.Logger;
import com.mes.persist.Db;
import com.mes.persist.Selection;
import com.mes.persist.SubtableDef;

public class MesSubtableList implements List
{
  static Logger log = Logger.getLogger(MesSubtableList.class);

  private Db          db;
  private SubtableDef subTd;
  private Object      parent;
  private List        dataList;

  public MesSubtableList(Db db, SubtableDef subTd, Object parent)
  {
    this.db = db;
    this.subTd = subTd;
    this.parent = parent;
  }

  private Object getKeySubBean()
  {
    // get sub bean key value from parent key column def
    Method parentKeyGetter = subTd.getParentKeyGetterMethod();
    Object value = null;
    try
    {
      value = parentKeyGetter.invoke(parent,null);
    }
    catch (Exception e)
    {
      throw new RuntimeException("Failed to load sub bean key value from parent "
        +"bean method " + parentKeyGetter.getName() + ": " + e);
    }

    // instantiate sub bean key object for use as a select key
    Class subClass = subTd.getBeanClass();
    Object subBeanKey = null;
    try
    {
      subBeanKey = subClass.newInstance();
    }
    catch (Exception e)
    {
      throw new RuntimeException("Error instantiating " + subClass.getName()
        + " sub bean key: " + e);
    }

    // load sub bean key value into the sub bean key object
    Method setter = subTd.getJoinToCd().getSetterMethod();
    try
    {
      setter.invoke(subBeanKey,new Object[] { value });
    }
    catch (Exception e)
    {
      throw new RuntimeException("Failed to invoke setter in sub bean class " 
        + subBeanKey.getClass().getName() + " for column " 
        + subTd.getJoinToCd().getName() + ": " + e);
    }
    
    return subBeanKey;
  }

  private Selection getDataSelection()
  {
    Selection sel = db.getSelection(getKeySubBean(),false);
    sel.addIndex(subTd.getSubIndexName());
    return sel;
  }
    

  private List getDataList()
  {
    if (dataList == null)
    {
      dataList = db.selectAll(getDataSelection());
    }
    return dataList;
  }

  public void add(int index, Object element)
  {
    getDataList().add(index,element);
  }

  public boolean add(Object o)
  {
    return getDataList().add(o);
  }

  public boolean addAll(Collection c)
  {
    return getDataList().addAll(c);
  }

  public boolean addAll(int index, Collection c)
  {
    return getDataList().addAll(index,c);
  }

  public void clear()
  {
    getDataList().clear();
  }

  public boolean contains(Object o)
  {
    return getDataList().contains(o);
  }

  public boolean containsAll(Collection c)
  {
    return getDataList().containsAll(c);
  }

  public boolean equals(Object o)
  {
    return getDataList().equals(o);
  }

  public Object get(int index)
  {
    return getDataList().get(index);
  }

  public int hashCode()
  {
    return getDataList().hashCode();
  }

  public int indexOf(Object o)
  {
    return getDataList().indexOf(o);
  }

  public boolean isEmpty()
  {
    return getDataList().isEmpty();
  }

  public Iterator iterator()
  {
    return getDataList().iterator();
  }

  public int lastIndexOf(Object o)
  {
    return getDataList().lastIndexOf(o);
  }

  public ListIterator listIterator()
  {
    return getDataList().listIterator();
  }

  public ListIterator listIterator(int index)
  {
    return getDataList().listIterator(index);
  }

  public Object remove(int index)
  {
    return getDataList().remove(index);
  }

  public boolean remove(Object o)
  {
    return getDataList().remove(o);
  }

  public boolean removeAll(Collection c)
  {
    return getDataList().removeAll(c);
  }

  public boolean retainAll(Collection c)
  {
    return getDataList().retainAll(c);
  }

  public Object set(int index, Object element)
  {
    return getDataList().set(index,element);
  }

  public int size()
  {
    if (dataList != null)
    {
      return dataList.size();
    }
    return db.selectCount(getDataSelection());
  }

  public List subList(int fromIndex, int toIndex)
  {
    return getDataList().subList(fromIndex,toIndex);
  }

  public Object[] toArray()
  {
    return getDataList().toArray();
  }

  public Object[] toArray(Object[] a)
  {
    return getDataList().toArray(a);
  }
}