package com.mes.persist.mes;

import java.lang.reflect.Method;
import java.util.List;
import com.mes.persist.ColumnDef;
import com.mes.persist.ColumnDefSet;
import com.mes.persist.DataType;
import com.mes.persist.IndexDef;
import com.mes.persist.JoinDef;
import com.mes.persist.SubqueryDef;
import com.mes.persist.SubtableDef;
import com.mes.persist.TableDef;

public class MesSubtableDef implements SubtableDef
{
  private Method      getter;
  private Method      setter;
  private TableDef    parentTd;
  private TableDef    td;
  private SubtableDef joinSubTd;
  private String      colName;
  private String      subIdxName;
  private String      joinType;

  private boolean     requiredFlag;

  private String      subQualifier;

  private ColumnDef   joinFromCd;
  private ColumnDef   joinToCd;

  public MesSubtableDef(String subQualifier, Method getter, Method setter, 
    TableDef parentTd, String colName, TableDef td, String subIdxName, 
    String joinType, Object tableType, SubtableDef joinSubTd)
  {
    this.subQualifier = subQualifier;
    this.setter       = setter;
    this.getter       = getter;
    this.parentTd     = parentTd;
    this.colName      = colName;
    this.td           = td;
    this.subIdxName   = subIdxName;
    this.joinType     = joinType;
    this.joinSubTd    = joinSubTd;


    if (tableType.equals(REQUIRED))
    {
      requiredFlag = true;
    }
    else if (tableType.equals(OPTIONAL))
    {
      requiredFlag = false;
    }
    else
    {
      throw new RuntimeException("Invalid subtable type '" + tableType + "'");
    }

    // get parent column def that joins to this subtable
    if (joinSubTd != null)
    {
      joinFromCd = joinSubTd.getColumnDef(colName);
    }
    else
    {
      joinFromCd = parentTd.getColumnDef(colName);
    }

    // get subtable column def to join with parent
    List joinToCdList = td.getIndexDef(subIdxName).getColumnDefSet().getColumnDefs();
    if (joinToCdList.size() != 1)
    {
      throw new RuntimeException("Subtables may only use single column indexes, " 
        +"index " + subIdxName + " has more than one column");
    }
    joinToCd = (ColumnDef)joinToCdList.get(0);
  }

  public boolean isRequired()
  {
    return requiredFlag;
  }

  public Method getGetter()
  {
    return getter;
  }

  public Method getSetter()
  {
    return setter;
  }

  public TableDef getParentTd()
  {
    return parentTd;
  }

  public SubtableDef getJoinSubTd()
  {
    return joinSubTd;
  }

  public TableDef getTd()
  {
    return td;
  }

  public ColumnDef getJoinToCd()
  {
    return joinToCd;
  }

  public String getSubIndexName()
  {
    return subIdxName;
  }

  public Method getParentKeyGetterMethod()
  {
    return joinFromCd.getGetterMethod();
  }

  public Object getJoinType()
  {
    return joinType;
  }

  public void setQualifier(String qualifier)
  {
    throw new RuntimeException("Cannot set qualifier in subtable");
  }

  public String getQualifier()
  {
    return subQualifier + td.getQualifier();
  }

  public String getQualifiedName()
  {
    if (subQualifier == null)
    {
      throw new NullPointerException("Subtable alias is not set");
    }
    return getName() + " " + subQualifier;
  }

  public String getName()
  {
    return td.getName();
  }

  public Class getBeanClass()
  {
    return td.getBeanClass();
  }

  public void addColumnDef(ColumnDef cDef)
  {
    throw new RuntimeException("Cannot add column def to subtable");
  }
    
  public void addColumnDef(String colName,DataType dataType)
  {
    throw new RuntimeException("Cannot add column def to subtable");
  }

  public ColumnDef getColumnDef(String name)
  {
    return td.getColumnDef(name);
  }

  public ColumnDefSet getColumnDefSet()
  {
    return td.getColumnDefSet();
  }
  
  public IndexDef addIndexDef(String name, boolean isUnique)
  {
    throw new RuntimeException("Cannot add index def to subtable");
  }

  public IndexDef getIndexDef(String name)
  {
    throw new RuntimeException("Cannot add index def to subtable");
  }

  public List getIndexDefs()
  {
    return td.getIndexDefs();
  }

  public IndexDef getPrimaryKeyDef()
  {
    return td.getPrimaryKeyDef();
  }

  public JoinDef addJoinDef(String joinName, String colName, TableDef joinTd,
    String joinColName, Object joinType)
  {
    throw new RuntimeException("Cannot add join def to subtable");
  }

  public JoinDef getJoinDef(String joinName)
  {
    return td.getJoinDef(joinName);
  }

  public List getJoinDefs()
  {
    return td.getJoinDefs();
  }

  public void addSubtableDef(String colName, TableDef td, String subIdxName, 
    Object joinType, Object tableType, String accName, String subTblName)
  {
    throw new RuntimeException("Cannot add subtable def to subtable");
  }
  public void addSubtableDef(String colName, TableDef td, String subIdxName, 
    Object joinType, Object tableType)
  {
    throw new RuntimeException("Cannot add subtable def to subtable");
  }

  public SubtableDef getSubtableDef(String subName)
  {
    return td.getSubtableDef(subName);
  }

  public List getSubtableDefs()
  {
    return td.getSubtableDefs();
  }

  public String getInsertColumnList()
  {
    return td.getInsertColumnList();
  }
  public String getInsertValueMarks()
  {
    return td.getInsertValueMarks();
  }
  public String getUpdateSetList()
  {
    return td.getUpdateSetList();
  }

  public String getQualifier(String prefix)
  {
    return (prefix != null && prefix.length() > 0 ? prefix : "") 
      + getQualifier();
  }

  public String getSubQualifier()
  {
    return subQualifier;
  }

  public String getSelectClause()
  {
    throw new RuntimeException("Invalid subtable entry point");
  }
  public String getSelectClause(String prefix)
  {
    //return td.getSelectClause(prefix + "_s");
    return td.getSelectClause(prefix + subQualifier);
  }

  public String getFromClause()
  {
    throw new RuntimeException("Invalid subtable entry point");
  }
  public String getFromClause(String prefix)
  {
    //return td.getFromClause(prefix + "_s");
    return td.getFromClause(prefix + subQualifier);
  }

  public String getWhereClause()
  {
    throw new RuntimeException("Invalid subtable entry point");
  }
  public String getWhereClause(String prefix)
  {
    return getWhereClause(prefix,false);
  }
  public String getWhereClause(String prefix, boolean parentOptional)
  {
    StringBuffer clause = new StringBuffer();

    // subtable link expression: ' j_tbl.col = s_tbl.col [ (+) ] '
    if (joinSubTd != null)
    {
      clause.append(joinFromCd.getQualifiedName(prefix 
        + joinSubTd.getQualifier()));
    }
    else
    {
      clause.append(joinFromCd.getQualifiedName(prefix));
    }
    clause.append(" = ");
    clause.append(joinToCd.getQualifiedName(prefix + getQualifier()));
    clause.append((isRequired() && !parentOptional ? "" : "(+)"));

    // subtable's own where clause (if this subtable def is not required 
    // or parent is optional, then pass in true for the parent optional parm
    String whereClause = td.getWhereClause(prefix + subQualifier,
      parentOptional || !isRequired());
    if (whereClause != null && whereClause.length() > 0)
    {
      clause.append(" and\n" + whereClause);
    }

    return clause.toString();
  }

  public String getAliasedColumnName(String colName)
  {
    throw new RuntimeException("Cannot create subtable alias name without"
      + " prefix in subtable");
  }
  public String getAliasedColumnName(String colName, String prefix)
  {
    return td.getAliasedColumnName(colName,prefix + "s");
  }

  public String getQualifiedColumnName(String colName)
  {
    throw new RuntimeException("Cannot create subtable qualified name without"
      + " prefix in subtable");
  }
  public String getQualifiedColumnName(String colName, String prefix)
  {
    return td.getQualifiedColumnName(colName,prefix + "s");
  }

  public List getSubqueryDefs()
  {
    return td.getSubqueryDefs();
  }

  public void addSubqueryDef(SubqueryDef sqDef)
  {
    td.addSubqueryDef(sqDef);
  }
}
