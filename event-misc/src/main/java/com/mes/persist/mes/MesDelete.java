package com.mes.persist.mes;

import org.apache.log4j.Logger;
import com.mes.persist.JoinDef;
import com.mes.persist.Selection;
import com.mes.persist.TableDef;

public class MesDelete extends MesExecutable
{
  static Logger log = Logger.getLogger(MesDelete.class);

  public MesDelete(Selection sel)
  {
    super(sel,ET_UPDATE);
  }

  private String getWhereSubclause()
  {
    StringBuffer buf = new StringBuffer();

    // indexes and subtable where subclause
    buf.append(getIndexSubclause(sel.getIndexArray()));

    // filters where subclause
    String filterSub = getFilterSubclause(sel.getFilterArray());
    if (filterSub.length() > 0)
    {
      if (buf.length() > 0)
      {
        buf.append(" and\n");
      }
      buf.append(filterSub);
    }

    return buf.toString();
  }

  private String getJoinStatementStr()
  {
    StringBuffer buf = new StringBuffer();

    // delete from jtrg_table_name 
    // where join_id in 
    JoinDef jd = sel.getJoinDef();
    buf.append("DELETE FROM\n" + jd.getName() + "\n");
    buf.append("WHERE\n" + jd.getTargetJoinIdName() + " IN\n");

    //    ( select jtrg.join_id 
    //      from   jsrc, ..., jtrg
    //      where  jtrg.join_id .. = jsrc.join_id and
    buf.append("( " + jd.getDeleteSubquery() + " and\n");

    //      [ index & filter subclauses ] )
    buf.append(getWhereSubclause() + " )");

    return buf.toString();
  }

  protected String createStatement()
  {
    if (sel.isJoin())
    {
      return getJoinStatementStr();
    }

    StringBuffer buf = new StringBuffer();
    TableDef td = sel.getTableDef();

    // delete from tbl_name
    buf.append("DELETE FROM\n" + td.getQualifiedName());

    // where [ index & filter subclauses ]
    String where = getWhereSubclause();
    buf.append((where.length() > 0 ? "\nWHERE\n" : "") + where);
      
    return buf.toString();
  }

  protected void setMarks()
  {
    setWhereMarks();
  }

  protected boolean updateSupported()
  {
    return true;
  }

  protected boolean querySupported()
  {
    return false;
  }
}