package com.mes.persist.mes;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.persist.Column;
import com.mes.persist.ColumnDef;
import com.mes.persist.DataType;

public class MesColumn implements Column
{
  static Logger log = Logger.getLogger(Column.class);

  private ColumnDef cDef;
  private Object    value;
  private boolean   nullFlag = true;

  private boolean   persistFlag;
  private Object    persistValue;
  private boolean   persistNullFlag = true;;

  public MesColumn(ColumnDef cDef)
  {
    this.cDef = cDef;
  }

  /**
   * Returns the column's column def.
   */
  public ColumnDef getColumnDef()
  {
    return cDef;
  }

  /**
   * Returns true if this column equals that column.
   */
  public boolean equals(Object o)
  {
    if (!(o instanceof MesColumn))
    {
      return false;
    }
    MesColumn thatCol = (MesColumn)o;
    if (!getName().equals(thatCol.getName()))
    {
      return false;
    }
    if (isNull() != thatCol.isNull() || 
        (!isNull() && !getValue().equals(thatCol.getValue())))
    {
      return false;
    }
    return true;
  }

  /**
   * Returns column's name.
   */
  public String getName()
  {
    return cDef.getName();
  }

  public String getQualifiedName(String qualifier)
  {
    return cDef.getQualifiedName(qualifier);
  }

  public String getColumnAlias(String qualifier)
  {
    return cDef.getColumnAlias(qualifier);
  }

  /**
   * Returns the column's data type.
   */
  public DataType getDataType()
  {
    return cDef.getDataType();
  }

  /**
   * Returns true if the column null flag is on (see set(...) and setNull()
   * methods).
   */
  public boolean isNull()
  {
    return nullFlag;
  }

  /**
   * Internal set method used once the data type has been determined from
   * the class of the value object.  Makes sure the value is not null and
   * the determined data type matches the column's designated data type.
   * Turns off the null flag.
   */
  private void set(Object value, DataType setType)
  {
    DataType myType = cDef.getDataType();
    
    // make sure the data type matches the column's data type
    if (!setType.equals(myType))
    {
      throw new RuntimeException("Attempt to set invalid type "
        + setType + " in column with type " + myType);
    }

    // don't allow null setting, setNull() is used for that
    if (value == null)
    {
      throw new NullPointerException("Cannot set null value, use setNull()"
        + " instead");
    }

    this.value = value;
    nullFlag = false;
    persistFlag = false;
  }

  /**
   * External setter.  Takes a value object and determines a corresponding
   * data type.  Throws an exception if the value object isn't an instance
   * of a supported type.
   */
  public void set(Object value)
  {
    DataType dt = null;

    if (value instanceof String)
    {
      dt = DataType.STRING;
    }
    else if (value instanceof Long)
    {
      dt = DataType.LONG;
    }
    else if (value instanceof Integer)
    {
      dt = DataType.INT;
    }
    else if (value instanceof Float)
    {
      dt = DataType.FLOAT;
    }
    else if (value instanceof Double)
    {
      dt = DataType.DOUBLE;
    }
    else if (value instanceof java.sql.Time)
    {
      dt = DataType.TIME;
    }
    else if (value instanceof java.sql.Date)
    {
      dt = DataType.DATE;
    }
    else if (value instanceof java.util.Date)
    {
      dt = DataType.TIMESTAMP;
    }
    else
    {
      throw new RuntimeException("Unsupported value type: "
        + value.getClass().getName());
    }

    set(value,dt);
  }
  
  public void set(long value)
  {
    set(Long.valueOf(value));
  }
  
  public void set(int value)
  {
    set(Integer.valueOf(value));
  }
  
  public void set(double value)
  {
    set(Double.valueOf(value));
  }
  
  public void set(float value)
  {
    set(Float.valueOf(value));
  }


  /**
   * Used to set a null value, turns on the null value flag.
   */
  public void setNull()
  {
    nullFlag = true;
  }

  /**
   * Getters
   */
   
  /**
   * Used by Db to fetch the raw value object to set in prepared statements.
   */
  public Object getValue()
  {
    return (nullFlag ? null : value);
  }

  /**
   * Returns a string representing the value of the column data in a human
   * readable form.  The null flag is used for null values, it causes null
   * to be returned.  Dates/times are convertd to formatted date time strings.
   * Throws NullPointerException if null value is passed in and null flag is
   * false.
   */
  private String getValueString(Object value, boolean nullFlag)
  {
    DataType myType = cDef.getDataType();
    if (nullFlag)
    {
      return null;
    }
    else if (value == null)
    {
      throw new NullPointerException(
        "Cannot generate value string, value is null");
    }
    else if (myType.isTimestamp())
    {
      SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ssa");
      return df.format((Date)value);
    }
    else if (myType.isDate())
    {
      SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
      return df.format((Date)value);
    }
    else if (myType.isTime())
    {
      SimpleDateFormat df = new SimpleDateFormat("hh:mm:ssa");
      return df.format((Date)value);
    }

    return value.toString();
  }
  
  public String getString()
  {
    return getValueString(value,nullFlag);
  }

  private Number valueAsNumber()
  {
    if (nullFlag)
    {
      throw new NullPointerException("Column " + getName()+ " has null value");
    }

    // hopefully the value is a number already
    if (value instanceof Number)
    {
      return (Number)value;
    }

    // try parsing the string value of the object as a Long
    try
    {
      return Long.valueOf(value.toString());
    }
    catch (NumberFormatException nfe)
    {
      log.debug("Column " + getName()+ " with value " + value
        + " unparsable as Long");
    }

    // try parsing the string value of the object as a Double
    try
    {
      return Double.valueOf(value.toString());
    }
    catch (NumberFormatException nfe) { }
    {
      log.debug("Column " + getName()+ " with value " + value
        + " unparsable as Double");
    }

    // try parsing out a date time in millisecs
    try
    {
      return ((Date) value).getTime();
    }
    catch (ClassCastException cfe)
    {
      log.debug("Column " + getName()+ " with value " + value
        + " cannot be cast as date");
    }

    // give up
    throw new ClassCastException("Unable to derive numeric data from value of"
      + " column " + getName());
  }

  public long getLong()
  {
    return valueAsNumber().longValue();
  }

  public int getInt()
  {
    return valueAsNumber().intValue();
  }

  public double getDouble()
  {
    return valueAsNumber().doubleValue();
  }

  public float getFloat()
  {
    return valueAsNumber().floatValue();
  }

  public java.sql.Timestamp getTimestamp()
  {
    DataType myType = cDef.getDataType();
    if (myType.isTime() || myType.isDate() || myType.isTimestamp())
    {
      if (!nullFlag)
      {
        if (value instanceof java.sql.Timestamp)
        {
          return (Timestamp)value;
        }
        if (value instanceof java.util.Date)
        {
          return new java.sql.Timestamp(((java.util.Date)value).getTime());
        }
      }
      else
      {
        return null;
      }
    }
    throw new ClassCastException("Column " + getName()+ " value is not a date");
  }

  public java.sql.Time getTime()
  {
    DataType myType = cDef.getDataType();
    if (myType.isTime() || myType.isDate() || myType.isTimestamp())
    {
      if (!nullFlag)
      {
        if (value instanceof java.sql.Time)
        {
          return (java.sql.Time)value;
        }
        if (value instanceof java.util.Date)
        {
          return new java.sql.Time(((java.util.Date)value).getTime());
        }
      }
      else
      {
        return null;
      }
    }
    throw new ClassCastException("Column " + getName()+ " value is not a date");
  }

  public java.sql.Date getSqlDate()
  {
    DataType myType = cDef.getDataType();
    if (myType.isTime() || myType.isDate() || myType.isTimestamp())
    {
      if (!nullFlag)
      {
        if (value instanceof java.sql.Date)
        {
          return (java.sql.Date)value;
        }
        if (value instanceof java.util.Date)
        {
          return new java.sql.Date(((java.util.Date)value).getTime());
        }
      }
      else
      {
        return null;
      }
    }
    throw new ClassCastException("Column " + getName()+ " value is not a date");
  }

  public java.util.Date getDate()
  {
    DataType myType = cDef.getDataType();
    if (myType.isDate())
    {
      return getSqlDate();
    }
    else if (myType.isTime())
    {
      return getTime();
    }
    return getTimestamp();
  }

  /**
   * Stores copy of the current column value in persistValue.  This allows
   * comparisons between the persisted value and the current value of the
   * column.
   */
  public void setPersisted()
  {
    persistValue = value;
    persistFlag = true;
    persistNullFlag = nullFlag;
  }
  
  /**
   * Returns whatever is stored in persistValue.
   */
  public Object getPersistValue()
  {
    return persistValue;
  }
  
  /**
   * Returns true if column has been marked as persisted.
   */
  public boolean isPersisted()
  {
    return persistFlag;
  }
  
  /**
   * Returns true if column persisted value is marked as null.
   */
  public boolean isPersistNull()
  {
    return persistNullFlag;
  }

  /**
   * Throws exception if given column does not share same data type as this.
   */
  private void validateColumnCompatability(Column col)
  {
    if (!col.getDataType().equals(cDef.getDataType()))
    {
      throw new RuntimeException("Incompatable columns, this " + cDef
        + " vs. " + col.getColumnDef());
    }
  }
        
  /**
   * Copy persisted info from another column into this one.
   */
  public void copyDbState(Column col)
  {
    if (col != null)
    {
      validateColumnCompatability(col);
      persistValue    = col.getPersistValue();
      persistFlag     = col.isPersisted();
      persistNullFlag = col.isPersistNull();
    }
  }
  
  /**
   * Copy all state from another column.
   */
  public void copy(Column col)
  {
    if (col != null)
    {
      copyDbState(col);
      value     = col.getValue();
      nullFlag  = col.isNull();
    }
  }

  /**
   * Returns a name-value descriptor of the column contents along with the
   * persisted value if it differs from the current value.  Intended to be
   * used for generating audit log data.
   */
  public String getAuditLog()
  {
    String valStr = getValueString(value,nullFlag);
    valStr = (valStr == null ? "null" : valStr);
    if (persistFlag)
    {
      String perStr = getValueString(persistValue,persistNullFlag);
      perStr = (perStr == null ? "null" : perStr);
      if (!valStr.equals(perStr))
      {
        // return current persisted value and new value
        return getName()+ ": " + perStr + " > " + valStr;
      }
    }
    // return value, either new or same as previously persisted
    return getName()+ ": " + valStr;
  }
  
  /**
   * Returns a descriptor of the column contents representing the name and
   * current value of the column.
   */
  public String toString()
  {
    return getName() + ": " + getValueString(value,nullFlag);
  }

  public boolean isReadOnly()
  {
    return cDef.isReadOnly();
  }
}

