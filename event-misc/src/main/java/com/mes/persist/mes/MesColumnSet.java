package com.mes.persist.mes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.mes.persist.Column;
import com.mes.persist.ColumnDef;
import com.mes.persist.ColumnDefSet;
import com.mes.persist.ColumnSet;
import com.mes.persist.SubqueryDef;

public class MesColumnSet implements ColumnSet
{
  private ColumnDefSet cDefSet;
  private Map colMap = new HashMap();
  private List cols = new ArrayList();
  
  /**
   * Used by records to create a new set of columns from a column def set.
   */
  public MesColumnSet(ColumnDefSet cDefSet)
  {
    this.cDefSet = cDefSet;
    
    for (Iterator i = cDefSet.getColumnDefs().iterator(); i.hasNext();)
    {
      Column col = new MesColumn((ColumnDef)i.next());
      cols.add(col);
      colMap.put(col.getName(),col);
    }
  }

  /**
   * Used by indexes to create a column subset of an existing record column
   * set.
   */
  public MesColumnSet(ColumnDefSet cDefSet, ColumnSet cSet)
  {
    this.cDefSet = cDefSet;
    
    for (Iterator i = cDefSet.getColumnDefs().iterator(); i.hasNext();)
    {
      Column col = cSet.getColumn(((ColumnDef)i.next()).getName());
      cols.add(col);
      colMap.put(col.getName(),col);
    }
  }

  /**
   * Returns true if each column in this column set equals each corresponding
   * column in that column set.
   */
  public boolean equals(Object o)
  {
    if (!(o instanceof MesColumnSet))
    {
      return false;
    }
    for (Iterator i = ((MesColumnSet)o).getColumns().iterator(); i.hasNext(); )
    {
      Column thatCol = (Column)i.next();
      Column thisCol = getColumn(thatCol.getName());
      if (thisCol == null || !thisCol.equals(thatCol))
      {
        return false;
      }
    }
    return true;
  }
  
  public ColumnDefSet getColumnDefSet()
  {
    return cDefSet;
  }

  public List getColumns()
  {
    return new ArrayList(cols);
  }
  
  public Column getColumn(String name)
  {
    return (Column)colMap.get(name);
  }

  public void addSubqueryColumns(SubqueryDef sqDef)
  {
    for (Iterator i = sqDef.getColumnDefs().iterator(); i.hasNext();)
    {
      Column col = new MesColumn((ColumnDef)i.next());
      cols.add(col);
      colMap.put(col.getName(),col);
    }
  }

  /**
   * Scan columns, if all columns are null return true to indicate the
   * column set is empty.
   */
  public boolean isEmpty()
  {
    for (Iterator i = cols.iterator(); i.hasNext();)
    {
      Column col = (Column)i.next();
      if (!col.isNull())
      {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Generates a string representation of the contents of the data object by
   * iterating through each field and fetching it's toString().
   */
  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    boolean isFirst = true;
    for (Iterator i = cols.iterator(); i.hasNext();)
    {
      Column col = (Column)i.next();
      sb.append((!isFirst ? ", " : "") + col);
      isFirst = false;
    }
    sb.append((isFirst ? "(no columns)" : ""));
    return sb.toString();
  }
}
