package com.mes.persist.mes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import org.apache.log4j.Logger;
import com.mes.persist.Column;
import com.mes.persist.ColumnDef;
import com.mes.persist.Executable;
import com.mes.persist.Filter;
import com.mes.persist.Index;
import com.mes.persist.Indexable;
import com.mes.persist.Record;
import com.mes.persist.Selection;
import com.mes.persist.TableDef;

public abstract class MesExecutable implements Executable
{
  static Logger log = Logger.getLogger(MesExecutable.class);

  public static final String ET_UPDATE = "update";
  public static final String ET_QUERY = "query";

  public static final String ID_TAG = "@@@";

  protected Record record;
  protected Selection sel;
  protected ResultSet rs;
  protected String statement;
  protected PreparedStatement ps;
  protected boolean countFlag;

  private Object execType;

  public MesExecutable(Record record, Object execType)
  {
    this.record = record;
    setExecType(execType);
  }
  public MesExecutable(Selection sel, Object execType)
  {
    this.sel = sel;
    setExecType(execType);
  }

  private void setExecType(Object execType)
  {
    if (!execType.equals(ET_UPDATE) && !execType.equals(ET_QUERY))
    {
      throw new RuntimeException("Invalid executable type '" + execType + "'");
    }
    this.execType = execType;
  }

  protected void invalidOp()
  {
    throw new RuntimeException("Invalid operation for " + this.getClass().getName());
  }

  protected abstract String createStatement();

  protected abstract void setMarks();

  protected boolean updateSupported()
  {
    return execType.equals(ET_UPDATE);
  }

  protected boolean querySupported()
  {
    return execType.equals(ET_QUERY);
  }

  public String getStatement()
  {
    if (statement == null)
    {
      statement = createStatement();
    }
    return statement;
  }

  private PreparedStatement prepareStatement(Connection con) throws SQLException
  {
    ps = con.prepareStatement(getStatement());
    setMarks();
    return ps;
  }

  public int executeUpdate(Connection con)
  {
    if (!updateSupported()) invalidOp();

    try
    {
      return prepareStatement(con).executeUpdate();
    }
    catch (Exception e)
    {
      throw new RuntimeException(e);
    }
  }

  public ResultSet executeQuery(Connection con)
  {
    if (!querySupported()) invalidOp();

    try
    {
      rs = prepareStatement(con).executeQuery();
      return rs;
    }
    catch (Exception e)
    {
      throw new RuntimeException(e);
    }
  }

  public int executeCountQuery(Connection con)
  {
    if (!querySupported()) invalidOp();

    try
    {
      countFlag = true;
      executeQuery(con);
      if (rs.next()) return rs.getInt("r_count");
      return 0;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    finally
    {
      countFlag = false;
      close();
    }
  }

  public boolean next()
  {
    if (!querySupported())
    {
      invalidOp();
    }
    return false;
  }

  public Object getColumnValue(ColumnDef cd, String qualifier)
  {
    invalidOp();
    return null;
  }
 
  public void loadRecord(Record record, String qualifier)
  {
    invalidOp();
  }

  public void close()
  {
    try { rs.close(); } catch (Exception e) { }
    try { ps.close(); } catch (Exception e) { }
  }

  /*****************************************************************************
   *
   *  Shared statement string generation methods
   *
   ****************************************************************************/

  /**
   * Given a table def or some such and an array of other indexes this will
   * return a single array with the table def as the first element followed
   * by the elements in the other indexes array.
   */
  protected Indexable[] mergeIndexables(Indexable index, Indexable[] indexes)
  {
    Indexable[] merged = new Indexable[indexes ==  null ? 1 : indexes.length + 1];
    merged[0] = index;
    if (indexes != null)
    {
      for (int i = 0; i < indexes.length; ++i)
      {
        merged[i + 1] = indexes[i];
      }
    }
    return merged;
  }

  protected String getIndexSubclause(Indexable[] idxs, String prefix)
  {
    StringBuffer buf = new StringBuffer();
    if (idxs != null)
    {
      for (int i = 0; i < idxs.length; ++i)
      {
        // hack to allow null indexes
        if (idxs[i] == null) continue;

        if (buf.length() > 0)
        {
          buf.append(" and\n");
        }
        buf.append(idxs[i].getWhereClause(prefix));

      }
    }
    return buf.toString();
  }
  protected String getIndexSubclause(Indexable[] idxs)
  {
    return getIndexSubclause(idxs,null);
  }

  /**
   * For now filters are not indexables, so we have a separate where clause
   * generation method for them...
   */
  protected String getFilterSubclause(Filter[] filters, String prefix)
  {
    StringBuffer buf = new StringBuffer();
    if (filters != null)
    {
      for (int i = 0; i < filters.length; ++i)
      {
        if (buf.length() > 0)
        {
          buf.append(" and\n");
        }
        buf.append(filters[i].getWhereClause(prefix)); 
      }
    }
    return buf.toString();
  }
  protected String getFilterSubclause(Filter[] filters)
  {
    return getFilterSubclause(filters,null);
  }

  /**
   * Generate subquery for the where clause that joins subquery tables in.
   */
  protected String getSubquerySubclause(Selection sel)
  {
    StringBuffer buf = new StringBuffer();
    for (Iterator i = sel.getSubSels().iterator(); i.hasNext();)
    {
      Selection subSel = (Selection)i.next();
      String colName = sel.getSubSelColName(subSel);
      buf.append(buf.length() > 0 ? " and\n" : "");
      buf.append(ID_TAG + sel.getTableDef().getQualifiedColumnName(colName));
      buf.append(" = ");
      buf.append("ss" + sel.getSubSelNum(subSel) + ".");
      buf.append(ID_TAG + subSel.getTableDef().getAliasedColumnName(colName));
    }
    return buf.toString();
  }

  /**
   * Generates all selection subclauses for a select statement where clause,
   * including subtables.
   */
  protected String getWhereSubclause(Selection sel, String prefix)
  {
    StringBuffer buf = new StringBuffer();

    // indexable subclause (subtables from td + indexes)
    TableDef td = sel.getTableDef();
    Index[] indexes = sel.getIndexArray();
    Indexable[] indexables = mergeIndexables(td,indexes);
    buf.append(getIndexSubclause(indexables,prefix));

    // filters subclause
    String filterSub = getFilterSubclause(sel.getFilterArray(),prefix);
    if (filterSub.length() > 0)
    {
      buf.append((buf.length() > 0 ? " and\n" : "") + filterSub);
    }

    // sub sel subquery joins
    String subqBuf = getSubquerySubclause(sel);
    if (subqBuf.length() > 0)
    {
      buf.append((buf.length() > 0 ? " and\n" : "") + subqBuf);
    }
      
    return buf.toString();        
  }
  protected String getWhereSubclause(Selection sel)
  {
    return getWhereSubclause(sel,null);
  }

  /*****************************************************************************
   *
   *  Shared mark setting methods
   *
   ****************************************************************************/

  /**
   * Sets column values of the record, returns next mark number.  Starts
   * at mark 1.
   */
  protected int setColumnMarks()
  {
    int mark = 1;
    for (Iterator i = record.getColumns().iterator(); i.hasNext();)
    {
      Column column = (Column)i.next();
      if (!column.isReadOnly())
      {
        try
        {
          if (!column.isNull())
          {
            ps.setObject(mark++,column.getValue(),column.getDataType().sqlType());
          }
          else
          {
            ps.setNull(mark++,column.getDataType().sqlType());
          }
        }
        catch (Exception e)
        {
          throw new RuntimeException("Failed to set value mark with column " 
            + column.getName() + ": " + e);
        }
      }
    }
    return mark;
  }

  /**
   * Loads values into the prepared statement marks for the given index
   * starting at the indicated mark.  Returns next available mark number.
   * Null values are skipped as they do not have corresponding marks in where
   * clauses.
   */
  protected int setIndexMarks(Index index, int mark)
  {
    for (Iterator i = index.getColumns().iterator(); i.hasNext();)
    {
      Column column = (Column)i.next();
      try
      {
        if (!column.isNull())
        {
          ps.setObject(mark++,column.getValue(),column.getDataType().sqlType());
        }
      }
      catch (Exception e)
      {
        throw new RuntimeException("Failed to set index mark with column " 
          + column.getName() + ": " + e);
      }
    }
    return mark;
  }


  /**
   * Sets values in a prepared statement associatied with an array of indexes.
   * The index value of the mark to start setting at is given and incremented
   * as values are set.  Returns the incremented mark value.
   */
  protected int setIndexMarks(Index[] indexes, int mark)
  {
    if (indexes != null)
    {
      for (int i = 0; i < indexes.length; ++i)
      {
        if (indexes[i] != null)
        {
          mark = setIndexMarks(indexes[i],mark);
        }
      }
    }
    return mark;
  }

  /**
   * Sets values in a prepared statement associated with an array of filters.
   * The index of the mark to start setting at is given, and incremented as
   * values are set.  Returns the incremented mark value.
   */
  protected int setFilterMarks(Filter[] filters, int mark) throws SQLException
  {
    if (filters != null)
    {
      for (int i = 0; i < filters.length; ++i)
      {
        mark = filters[i].setMarks(ps,mark);
      }
    }
    return mark;
  }

  /**
   * Sets the values in a prepared statement's where clause.
   */
  protected int setWhereMarks(Selection sel, int mark)
  {
    try
    {
      mark = setIndexMarks(sel.getIndexArray(),mark);
      return setFilterMarks(sel.getFilterArray(),mark);
    }
    catch (Exception e)
    {
      log.error("Error setting where marks: " + e);
      throw new RuntimeException(e);
    }
  }
  protected int setWhereMarks()
  {
    return setWhereMarks(sel,1);
  }
}