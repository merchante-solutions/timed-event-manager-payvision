package com.mes.persist.mes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.mes.persist.DataType;
import com.mes.persist.Schema;
import com.mes.persist.TableDef;

public class MesSchema implements Schema
{
  private int qualifierCount = 1;
  private String idSequence;
  private String logTable;
  List defs = new ArrayList();
  Map defMap = new HashMap();
  
  public MesSchema(String idSequence, String logTable)
  {
    this.idSequence = idSequence;
    this.logTable = logTable;

    // add log table def
    TableDef logTd = new MesTableDef(logTable,MesLog.class);
    logTd.addColumnDef("log_id",DataType.LONG);
    logTd.addColumnDef("item_id",DataType.LONG);
    logTd.addColumnDef("user_name",DataType.STRING);
    logTd.addColumnDef("log_date",DataType.TIMESTAMP);
    logTd.addColumnDef("table_name",DataType.STRING);
    logTd.addColumnDef("action",DataType.STRING);
    logTd.addColumnDef("log_data",DataType.STRING);
    addTableDef(logTd);
  }
  
  /**
   * Returns name of id sequence used to generate unique id values for this
   * schema.
   */
  public String getIdSequence()
  {
    return idSequence;
  }
  
  /**
   * Returns name of db table to log audit info in for operations performed
   * on tables in this schema.
   */
  public String getLogTable()
  {
    return logTable;
  }
  
  /**
   * Adds a table def to the schema.  Maps the table's associated bean class 
   * and the table's name to the table def for later retrieval by either.
   * Throws an exception if either name or class is already present in the
   * def map.  Also makes sure each table def has a unique alias associated
   * with it and stores a mapping of the table alias to table def as well.
   */
  public void addTableDef(TableDef td)
  {
    if (defMap.get(td.getBeanClass()) != null)
    {
      throw new RuntimeException("Class " + td.getBeanClass().getName()
        + " already exists in schema");
    }
    if (defMap.get(td.getName()) != null)
    {
      throw new RuntimeException("Table name or alias " + td.getName()
        + " already exists in schema");
    }
    defs.add(td);
    defMap.put(td.getBeanClass(),td);
    defMap.put(td.getName(),td);

    // make sure the table has a unique alias
    String Alias = td.getQualifier();
    while (Alias == null || defMap.get(Alias) != null)
    {
      Alias = "t" + qualifierCount++;
      td.setQualifier(Alias);
    }
    defMap.put(Alias,td);
  }
  
  /**
   * Returns a list of all table definitions in this schema.
   */
  public List getTableDefs()
  {
    return defs;
  }
  
  /**
   * Retrieves a table def associated with the given key object.  The key
   * may be either the table name (String) or the table's associated bean
   * class.
   */
  public TableDef getTableDef(Object defKey)
  {
    return (TableDef)defMap.get(defKey);
  }
}