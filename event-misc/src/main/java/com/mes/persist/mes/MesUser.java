package com.mes.persist.mes;

import com.mes.persist.User;
import com.mes.user.UserBean;

public class MesUser implements User
{
  private UserBean userBean;
  
  public MesUser(UserBean userBean)
  {
    this.userBean = userBean;
  }
  
  public String getName()
  {
    return userBean.getLoginName();
  }
  
  public boolean isValid()
  {
    return userBean.isValid();
  }
}