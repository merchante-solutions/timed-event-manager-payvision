package com.mes.persist.mes;

import java.sql.Timestamp;

public class MesLog
{
  private long      logId;
  private long      itemId;
  private String    userName;
  private Timestamp logDate;
  private String    tableName;
  private String    action;
  private String    logData;

  public void setLogId(long logId)
  {
    this.logId = logId;
  }

  public long getLogId()
  {
    return logId;
  }

  public void setItemId(long itemId)
  {
    this.itemId = itemId;
  }

  public long getItemId()
  { 
    return itemId;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }

  public String getUserName()
  {
    return userName;
  }

  public void setLogDate(Timestamp logDate)
  {
    this.logDate = logDate;
  }

  public Timestamp getLogDate()
  {
    return logDate;
  }

  public void setTableName(String tableName)
  {
    this.tableName = tableName;
  }

  public String getTableName()
  {
    return tableName;
  }

  public void setAction(String action)
  {
    this.action = action;
  }

  public String getAction()
  {
    return action;
  }

  public void setLogData(String logData)
  {
    this.logData = logData;
  }

  public String getLogData()
  {
    return logData;
  }

  public String toString()
  {
    return "MesLog [ id: " + logId + ", item id: " + itemId 
      + ", table: " + tableName + ", action: " + action 
      + ", data: " + logData + " ]";
  }
}