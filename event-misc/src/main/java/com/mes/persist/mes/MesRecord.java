package com.mes.persist.mes;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.mes.persist.Column;
import com.mes.persist.ColumnDefSet;
import com.mes.persist.ColumnSet;
import com.mes.persist.Index;
import com.mes.persist.IndexDef;
import com.mes.persist.JoinDef;
import com.mes.persist.Record;
import com.mes.persist.SubqueryDef;
import com.mes.persist.TableDef;

public class MesRecord implements Record
{
  private TableDef td;
  private MesColumnSet cSet;
  
  private Map subRecMap = new HashMap();

  private boolean persisted;
  
  public MesRecord(TableDef td)
  {
    this.td = td;
    cSet = new MesColumnSet(td.getColumnDefSet());
    for (Iterator i = td.getSubqueryDefs().iterator(); i.hasNext();)
    {
      SubqueryDef sqDef = (SubqueryDef)i.next();
      cSet.addSubqueryColumns(sqDef);
    }
  }
  
  /**
   * Returns true if this record's column set equals that record's column set.
   */
  public boolean equals(Object o)
  {
    if (o instanceof MesRecord)
    {
      MesRecord thatRec = (MesRecord)o;
      return getName().equals(thatRec.getName()) && cSet.equals(thatRec.cSet);
    }
    return false;
  }

  /**
   * Returns the record's table def.
   */
  public TableDef getTableDef()
  {
    return td;
  }

  /**
   * Returns contained column set's column def set.
   */ 
  public ColumnDefSet getColumnDefSet()
  {
    return cSet.getColumnDefSet();
  }

  public List getColumnDefs()
  {
    return getColumnDefSet().getColumnDefs();
  }

  /**
   * Returns the table def's table name.
   */
  public String getName()
  {
    return td.getName();
  }

  /**
   * Returns the table def's table name (for audit purposes).
   */
  public String getAuditName()
  {
    return getName();
  }

  private Index createIndex(IndexDef id, String qualifier)
  {
    return new MesIndex(id,this,qualifier);
  }
  
  /**
   * Returns primary key index, may be null.
   */
  public Index getPrimaryKey(String qualifier)
  {
    IndexDef id = td.getPrimaryKeyDef();
    return id == null ? null : createIndex(id,qualifier);
  }
  public Index getPrimaryKey()
  {
    return getPrimaryKey(td.getQualifier());
  }

  public Object getKeyValue(int colNum)
  {
    Index key = getPrimaryKey();
    if (key == null)
    {
      throw new NullPointerException("No primary key defined for record");
    }
    List columns = key.getColumns();
    if (colNum < 0 || colNum >= columns.size())
    {
      throw new NullPointerException("Key column num " + colNum + " out of range");
    }
    return ((Column)columns.get(colNum)).getValue();
  }
  public Object getKeyValue()
  {
    return getKeyValue(0);
  }
  
  /**
   * Returns first loaded key index found.  Currently keys are indexes that
   * are flagged as unique.  Check primary key first, if that is not loaded
   * scans the rest of the indexes.
   */
  public Index getLoadedKey(String qualifier)
  {
    Index key = getPrimaryKey(qualifier);
    if (key != null && key.isLoaded())
    {
      return key;
    }

    for (Iterator i = td.getIndexDefs().iterator(); i.hasNext();)
    {
      IndexDef id = (IndexDef)i.next();
      key = createIndex(id,qualifier);
      if (key.isKey() && key.isLoaded())
      {
        return key;
      }
    }

    return null;
  }
  public Index getLoadedKey()
  {
    return getLoadedKey(td.getQualifier());
  }
  
  /**
   * Fetches an index by name.
   */
  public Index getIndex(String name, String qualifier)
  {
    IndexDef idxDef = td.getIndexDef(name);
    String idxQual = null;
    if (td instanceof JoinDef)
    {
      idxQual = ((JoinDef)td).getJoinQualifier(idxDef.getTableDef());
    }
    else
    {
      idxQual = qualifier;
    }
    if (idxDef != null && idxQual != null)
    {
      return createIndex(idxDef,idxQual);
    }
    return null;
  }
  public Index getIndex(String name)
  {
    return getIndex(name,td.getQualifier());
  }
  
  public ColumnSet getColumnSet()
  {
    return cSet;
  }
  
  public List getColumns()
  {
    return cSet.getColumns();
  }
  
  public Column getColumn(String name)
  {
    return cSet.getColumn(name);
  }
  
  /**
   * The persisted state should be set by the database management immediately 
   * after a database persist or fetch.
   */
  public boolean isPersisted()
  {
    return persisted;
  }
  
  /**
   * Turns on the persisted flag and sets persisted in each contained field.
   */
  public void setPersisted()
  {
    for (Iterator i = cSet.getColumns().iterator(); i.hasNext();)
    {
      ((Column)i.next()).setPersisted();
    }
    persisted = true;
  }
  
  /**
   * Turns off the persisted flag.
   */
  public void unsetPersisted()
  {
    persisted = false;
  }
  
  /**
   * Copies the db state from another record into this one.  Used by Db during 
   * persists to enable accurate audit log data.  Throws an exception if a
   * field in this record is not present or of wrong type in the other record.
   */
  public void copyDbState(Record record)
  {
    for (Iterator i = cSet.getColumns().iterator(); i.hasNext();)
    {
      Column col = (Column)i.next();
      Column otherCol = record.getColumn(col.getName());
      if (otherCol == null)
      {
        throw new NullPointerException("No column named " + col.getName()
          + " exists");
      }
      col.copyDbState(otherCol);
    }
  }
  
  /**
   * Loads the contents of another record into this record.  Throws an 
   * exception if a field in this record is not present or of wrong type 
   * in the other record.  Used by Db to load record data.
   */
  public void copy(Record record)
  {
    for (Iterator i = cSet.getColumns().iterator(); i.hasNext();)
    {
      Column col = (Column)i.next();
      Column otherCol = record.getColumn(col.getName());
      if (otherCol == null)
      {
        throw new NullPointerException("No column named " + col.getName()
          + " exists");
      }
      col.copy(otherCol);
    }
  }
  
  /**
   * Generate a string descriptor of the contents of the data object suitable
   * for audit log records.  Calls each fields getAuditLog() method.
   */
  public String getAuditLog()
  {
    StringBuffer sb = new StringBuffer();
    boolean isFirst = true;
    for (Iterator i = cSet.getColumns().iterator(); i.hasNext();)
    {
      Column col = (Column)i.next();
      sb.append((!isFirst ? ", " : "") + col.getAuditLog());
      isFirst = false;
    }
    sb.append((isFirst ? "(no columns)" : ""));
    return sb.toString();
  }
   
  /**
   * Add a subrecord corresponding with a sub table def of this record's 
   * table def.
   */
  public void putSubrecord(TableDef subTd, Record subRec)
  {
    subRecMap.put(subTd,subRec);
  }

  public Record getSubrecord(TableDef subTd)
  {
    return (Record)subRecMap.get(subTd);
  }

  /**
   * Determine if record is empty.
   */
  public boolean isEmpty()
  {
    return cSet.isEmpty();
  }

  /**
   * Generates a string representation of the contents of the data object by
   * iterating through each field and fetching it's toString().
   */
  public String toString()
  {
    return "Record " + td.getName() + " [ " + cSet.toString() + " ]";
  }
}