/**
 * A base class for package database managers.
 *
 * Provides child classes with ability to select, persist and delete Persistent
 * items.
 * 
 * Also provides audit logging functionality and a mechanism for generating 
 * unique id's within a db's table set.  
 */

package com.mes.persist.mes;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.persist.Auditable;
import com.mes.persist.Column;
import com.mes.persist.Db;
import com.mes.persist.Executable;
import com.mes.persist.Index;
import com.mes.persist.JoinDef;
import com.mes.persist.Record;
import com.mes.persist.Schema;
import com.mes.persist.Selection;
import com.mes.persist.SubtableDef;
import com.mes.persist.TableDef;
import com.mes.persist.User;
import com.mes.user.UserBean;

public class MesDb extends Db
{
  static Logger log = Logger.getLogger(MesDb.class);

  public static final int ST_STANDARD         = 1;
  public static final int ST_JOIN_MAP_KEYS    = 2;
  public static final int ST_JOIN_MAP_VALUES  = 3;

  private SQLJConnectionBase  dbConnector = new SQLJConnectionBase();        
  private Connection          con;
  private Schema              schema;
  
  public MesDb()
  {
    this.schema = getSchema(this.getClass());
  }

  public MesDb(Schema schema)
  {
    this.schema = schema;
  }

  /**************************************************************************
   *
   * MES DB operations
   *
   *************************************************************************/

  private void connect()
  {
    dbConnector.connect();
    con = dbConnector.getConnection();
  }

  private void cleanUp()
  {
    dbConnector.cleanUp();
    //con = dbConnector.getConnection();
    con = null;
  }

  public void doConnect()
  {
    connect();
  }

  public void doDisconnect()
  {
    cleanUp();
  }

  public Connection getConnection()
  {
    doConnect();
    return con;
  }

  private boolean transactionFlag;
  private boolean autoCommitFlag;

  public boolean inTransaction()
  {
    return transactionFlag;
  }

  public boolean isConnected()
  {
    return con != null;
  }

  protected void autoCommitOn()
  {
    dbConnector.setAutoCommit(true);
  }

  protected void autoCommitOff()
  {
    dbConnector.setAutoCommit(false);
  }

  protected void storeAutoCommitState()
  {
    autoCommitFlag = dbConnector.getAutoCommit();
  }

  protected void restoreAutoCommitState()
  {
    dbConnector.setAutoCommit(autoCommitFlag);
  }

  protected void doCommit()
  {
    dbConnector.commit();
  }

  protected void doRollback()
  {
    dbConnector.rollback();
  }

  private void validateInTransaction()
  {
    if (!inTransaction())
      throw new RuntimeException("Invalid operation, not in transaction");
  }
  private void validateNotInTransaction()
  {
    if (inTransaction())
      throw new RuntimeException("Invalid operation, transaction in progress");
  }

  public long startTransaction()
  {
    if (con != null)
      throw new RuntimeException("Cannot start transaction with open connection");
    validateNotInTransaction();
    storeAutoCommitState();
    autoCommitOff();
    doConnect(); 
    transactionFlag = true;
    return getNewId();
  }

  public void commitTransaction()
  {
    validateInTransaction();
    doCommit();
  }

  public void rollbackTransaction()
  {
    validateInTransaction();
    doRollback();
  }

  public void finishTransaction()
  {
    validateInTransaction();
    doDisconnect();
    restoreAutoCommitState();
    transactionFlag = false;
  }

  /**************************************************************************
   *
   * Utility methods
   *
   *************************************************************************/

  /**
   * Fetches a new unique id for this db.
   */
  public long getNewId()
  {
    PreparedStatement ps = null;
    ResultSet         rs = null;
    
    String idSequence = schema.getIdSequence();
    
    long newId = -1L;

    try
    {
      connect();
      
      String qs = "select " + idSequence + ".nextval from dual";
      ps = con.prepareStatement(qs);
      rs = ps.executeQuery();
      if (!rs.next())
      {
        throw new RuntimeException(
          "Unable to retrieve next value from id sequence " + idSequence);
      }
      newId = rs.getLong(1);
    }
    catch (Exception e)
    {
      log.error("Error retrieving next value from id sequence " + idSequence
        + ": " + e);
      throw new RuntimeException(e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }

    return newId;    
  }
  
  /**
   * Makes sure a valid user object is present for audit logging.  Throws
   * an exception if user is null or not valid.  Returns a valid User
   * object based on the user object passed in.
   */
  public User validateUser(Object userObj, String caller)
  {
    // check for null
    if (userObj == null)
    {
      throw new NullPointerException("Null user in " + caller);
    }

    // check type    
    User user = null;
    if (userObj instanceof User)
    {
      user = (User)userObj;
    }
    else if (userObj instanceof UserBean)
    {
      user = new MesUser((UserBean)userObj);
    }
    else
    {
      throw new RuntimeException("Invalid user type " 
        + userObj.getClass().getName() + " in " + caller);
    }
    
    // check validity
    if (!user.isValid())
    {
      throw new RuntimeException("Invalid user in " + caller);
    }
    
    return user;
  }
  
  /**************************************************************************
   *
   * Audit logging
   *
   *************************************************************************/

  /**
   * Fetches the key id from the given auditable item.  Audit logs will
   * attempt to log a unique numeric item id, but if the record does not have
   * such a key the return value is -1L.
   */
  private long getAuditId(Auditable auditItem)
  {
    long keyId = -1L;

    Index key = auditItem.getPrimaryKey();
    if (key != null)
    {
      if (key.getColumns().size() != 1)
      {
        throw new RuntimeException("Unable to record audit log action for"
          + " audit item " + auditItem + ", only single key column is allowed.");
      }
      try
      {
        Column col = (Column)key.getColumns().get(0);
        if (col.getDataType().isLong())
        {
          keyId = col.getLong();
        }
      }
      catch (Exception e) { }
    }

    return keyId;
  }

  /**
   * Creates an audit log entry in the log table named by schema.getLogTable().
   */
  protected void logAuditData(String tableName, String userName, String dbAction, 
    String logData, long itemId)
  {
    // don't log logging, it's bad for the environment
    // update: now filtering out any table named *_log
    if (tableName.toLowerCase().endsWith("_log")) return;
    //if (tableName.equals(schema.getLogTable())) return;

    // HACK: don't know actual max length of log data column, 
    // so this is truncating at an arbitrary length!
    if (logData.length() > 300) logData = logData.substring(0,300);

    // create a log record
    MesLog log = new MesLog();
    log.setLogId(getNewId());
    log.setItemId(itemId);
    log.setUserName(userName);
    log.setLogDate(new Timestamp(Calendar.getInstance().getTime().getTime()));
    log.setTableName(tableName);
    log.setAction(dbAction);
    log.setLogData(logData);

    // insert log record
    insertRecord(generateRecordFromBean(log),null);
  }

  public void logAuditData(Auditable auditItem, Object userObj, String dbAction,
    String logData)
  {
    String auditName = auditItem.getAuditName();
    String userName = validateUser(userObj,"logAuditData()").getName();
    long auditId = getAuditId(auditItem);
    logAuditData(auditName,userName,dbAction,logData,auditId);
  }

  public void logAuditData(Auditable auditItem, Object userObj, String dbAction)
  {
    logAuditData(auditItem,userObj,dbAction,auditItem.getAuditLog());
  }

  /**************************************************************************
   *
   * bean <-> record methods
   *
   *************************************************************************/

  /**
   * Given a bean and a record with data, this will load the record data into
   * the bean.  This now takes into account presence of subtables and will
   * generate subtable lists as needed.
   */
  private Object loadBeanWithRecord(Object bean, Record record)
  {
    // try to match column names to setter methods 
    // and invoke any matching methods with the column value
    for (Iterator i = record.getColumns().iterator(); i.hasNext();)
    {
      // retrieve adjusted name and value from column
      Column column = (Column)i.next();
      Method method = column.getColumnDef().getSetterMethod();
      
      // set the column value in the bean
      try
      {
        Object value = column.getValue();
        if (value != null)
        {
          method.invoke(bean,new Object[] { column.getValue() });
        }
      }
      catch (java.lang.reflect.InvocationTargetException ite)
      {
        throw new RuntimeException("Exception thrown in setter in bean class " 
          + bean.getClass().getName() + " for column " + column.getName() 
          + ": " + ite.getTargetException());
      }
      catch (Exception e)
      {
        throw new RuntimeException("Failed to invoke setter in bean class " 
          + bean.getClass().getName() + " for column " + column.getName() 
          + ": " + e);
      }
    }

    // load subtable info
    TableDef td = record.getTableDef();
    for (Iterator i = td.getSubtableDefs().iterator(); i.hasNext();)
    {
      SubtableDef subTd = (SubtableDef)i.next();
      Object subValue = null;
      if (subTd.getJoinType().equals(JoinDef.ONE_TO_MANY))
      {
        subValue = new MesSubtableList(this,subTd,bean);
      }
      else if (subTd.getJoinType().equals(JoinDef.ONE_TO_ONE))
      {
        Record subRecord = record.getSubrecord(subTd);
        if (subRecord != null)
        {
          subValue = generateBeanFromRecord(subRecord);
        }
      }

      try
      {
        subTd.getSetter().invoke(bean,new Object[] { subValue });
      }
      catch (Exception e)
      {
        throw new RuntimeException("Failed to invoke setter in bean class " 
          + bean.getClass().getName() + " for sub table " + subTd.getName()
          + ": " + e);
      }
    }
    
    return bean;
  }
  
  /**
   * Given a record this method will instantiate a bean object of the
   * class associated with the record's table def.  It then Uses reflection 
   * to load record data into the bean.  Returns the loaded bean.
   */
  private Object generateBeanFromRecord(Record record)
  {
    // fetch the bean class from the record table def
    Class beanClass = record.getTableDef().getBeanClass();
    
    // instantiate a bean object
    Object bean = null;
    try
    {
      bean = beanClass.newInstance();
    }
    catch (Exception e)
    {
      throw new RuntimeException("Error instantiating " + beanClass.getName()
        + " bean: " + e);
    }
    
    return loadBeanWithRecord(bean,record);
  }
  
  /**
   * Given a list of records, generates a list of beans.
   */
  private List generateBeansFromRecords(List records)
  {
    // generate beans from records, return list of beans
    List beans = new ArrayList();
    for (Iterator i = records.iterator(); i.hasNext();)
    {
      beans.add(generateBeanFromRecord((Record)i.next()));
    }
    
    return beans;
  }
  
  private Map generateBeanMapFromRecords(List keyRecords, Map joinMap, 
    boolean isOneToMany)
  {
    Map beanMap = new HashMap();
    for (Iterator i = keyRecords.iterator(); i.hasNext();)
    {
      Record keyRecord = (Record)i.next();
      if (isOneToMany)
      {
        List joinRecords = (List)joinMap.get(keyRecord.getKeyValue());
        if (joinRecords == null)
        {
          joinRecords = new ArrayList();
        }
        beanMap.put(generateBeanFromRecord(keyRecord),
                      generateBeansFromRecords(joinRecords));
      }
      else
      {
        Record joinRecord = (Record)joinMap.get(keyRecord.getKeyValue());
        beanMap.put(generateBeanFromRecord(keyRecord),
                      generateBeanFromRecord(joinRecord));
      }
    }

    return beanMap;
  }

  /**
   * Creates a record based on a bean class.  Fetches a table def from the
   * schema using the bean's class, creates a record with the table def and
   * returns it.
   */
  private Record createRecordFromClass(Class beanClass)
  {
    // retrieve a table def using the bean's class
    TableDef td = schema.getTableDef(beanClass);
    if (td == null)
    {
      throw new NullPointerException("No table def found for class "
        + beanClass.getName());
    }

    return new MesRecord(td);
  }
  
  /**
   * Takes a bean and generates a record containing it's data. Uses reflection 
   * to retrieve the bean data and place it in the record.  Returns the 
   * generated record.
   */
  private Record generateRecordFromBean(Object bean)
  {
    // create a record to hold the bean data
    Record record = createRecordFromClass(bean.getClass());
    
    // load the bean data into the record
    for (Iterator i = record.getColumns().iterator(); i.hasNext();)
    {
      // get the bean getter method to invoke
      Column column = (Column)i.next();

      // skip read only columns (subquery values, etc.)
      if (column.isReadOnly()) continue;

      // set the column value to the value returned by the bean getter method
      try
      {
        Method method = column.getColumnDef().getGetterMethod();
        Object value = method.invoke(bean,null);
        if (value == null)
        {
          column.setNull();
        }
        else
        {
          column.set(value);
        }
      }
      catch (Exception e)
      {
        throw new RuntimeException("Failed to set value in column "
          + column.getName() + ": " + e);
      }
    }
    
    return record;
  }

  /**************************************************************************
   *
   * Record operations
   *
   *************************************************************************/

  /**
   * Generates a record for the given bean def (table or join) from the current 
   * row of the executable.
   */
  private Record generateRecord(TableDef td, Executable ex, String qualifier)
    throws SQLException
  {
    Record record = createRecordFromClass(td.getBeanClass());
    ex.loadRecord(record,qualifier);
    if (record.isEmpty())
    {
      return null;
    }
    for (Iterator i = td.getSubtableDefs().iterator(); i.hasNext();)
    {
      SubtableDef subTd = (SubtableDef)i.next();
      if (subTd.getJoinType().equals(JoinDef.ONE_TO_ONE))
      {
        Record subRecord =
          generateRecord(subTd,ex,qualifier + subTd.getQualifier());
        if (subRecord != null)
        {
          record.putSubrecord(subTd,subRecord);
        }
      }
    }
    return record;
  }
  private Record generateRecord(TableDef td, Executable ex) throws SQLException
  {
    return generateRecord(td,ex,td.getQualifier());
  }
  private Record generateRecord(JoinDef jd, Executable ex) throws SQLException
  {
    return generateRecord(jd.getFirstTableDef(),ex,jd.getJoinQualifier(1));
  }

  /**
   * Generates a list of records loaded from the database based on a selection
   * object.
   */
  private List selectAllRecords(Selection sel)
  {
    Executable ex = null;

    try
    {
      connect();
      ex = new MesSelect(sel);
      log.debug("select:\n" + ex.getStatement());
      List records = new ArrayList();
      for (ex.executeQuery(con); ex.next();)
      {
        records.add(generateRecord(sel.getTableDef(),ex));
      }
      return records;
    }
    catch (Exception e)
    {
      log.error("error selecting from " + sel.getTableDef().getName() + ": " + e);
      e.printStackTrace();
    }
    finally
    {
      try { ex.close(); } catch (Exception e) { }
      cleanUp();
    }

    return null;
  }

  /**
   * Selects list of records to be used in a join key -> value list map.
   */
  private List selectJoinMapKeyRecords(Selection sel)
  {
    Executable ex = null;

    try
    {
      connect();
      ex = new MesSelect(sel,MesSelect.ST_JOIN_MAP_KEYS);
      log.debug("select join keys:\n" + ex.getStatement());
      List records = new ArrayList();
      for (ex.executeQuery(con); ex.next();)
      {
        records.add(generateRecord(sel.getJoinDef(),ex));
      }
      return records;
    }
    catch (Exception e)
    {
      log.error("error selecting from " + sel.getJoinDef().getName() + ": " + e);
      e.printStackTrace();
      throw new RuntimeException(e.toString());
    }
    finally
    {
      try { ex.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  /**
   * Selects records using join selection.  A map is generated with keys that
   * can be linked to the join source records.  The values of the map are either
   * lists of join target records (one-to-many joins) or single target join
   * records (one-to-one joins).
   *
   *  Map:  key value -> join records list
   *                  - or -
   *        key value -> join record
   *
   * TODO: joins are currently broken for subtable loading
   */
  private Map selectJoinMapValueRecords(Selection sel)
  {
    JoinDef jd = sel.getJoinDef();
    Executable ex = new MesSelect(sel,MesSelect.ST_JOIN_MAP_VALUES);

    try
    {
      connect();
      Map joinMap = new HashMap();
      log.debug("select join values:\n" + ex.getStatement());
      for (ex.executeQuery(con); ex.next();)
      {
        Record record = createRecordFromClass(jd.getBeanClass());
        Object key = 
          ex.getColumnValue(jd.getKeyColumnDef(),jd.getJoinQualifier(1));

        if (jd.isOneToMany())
        {
          List records = (List)joinMap.get(key);
          if (records == null)
          {
            records = new ArrayList();
            joinMap.put(key,records);
          }
          ex.loadRecord(record,jd.getQualifier());
          records.add(record);
        }
        else
        {
          if (joinMap.containsKey(key))
          {
            throw new RuntimeException("Found multiple values for key " + key
              + " in one-to-one join results for " + jd);
          }
          ex.loadRecord(record,jd.getQualifier());
          joinMap.put(key,record);
        }
      }
      
      return joinMap;
    }
    catch (Exception e)
    {
      log.error("Error performing join select " + jd + ": " + e);
      e.printStackTrace();
      throw new RuntimeException(e.toString());
    }
    finally
    {
      try { ex.close(); } catch (Exception e) { }
      cleanUp();
    }
  }
    
  /**
   * Inserts a record into the database.
   */
  private void insertRecord(Record record, User user)
  {
    Executable ex = null;
    
    try
    {
      connect();

      // insert the record
      ex = new MesInsert(record);
      log.debug("insert:\n" + ex.getStatement());
      ex.executeUpdate(con);

      // create audit data
      if (user != null)
      {
        logAuditData(record,user,DBA_INSERT);
      }

      // mark record as persisted
      record.setPersisted();
    }
    catch (Exception e)
    {
      String msg = "Failed to insert record " + record + ": " + e;
      log.error(msg);
      throw new RuntimeException(msg);
    }
    finally
    {
      try { ex.close(); } catch (Exception e) { }
      cleanUp();
    }
  }
  
  /** 
   * Updates a record in the database.
   */
  private void updateRecord(Record record, User user)
  {
    Executable ex = null;
    
    try
    {
      connect();

      // execute update
      ex = new MesUpdate(record);
      log.debug("update:\n" + ex.getStatement());
      ex.executeUpdate(con);

      // log updates before marking record as persisted
      if (user != null)
      {
        logAuditData(record,user,DBA_UPDATE);
      }

      // mark record as persisted
      record.setPersisted();
    }
    catch (Exception e)
    {
      String msg = "Failed to update record " + record + ": " + e;
      log.error(msg);
      throw new RuntimeException(msg);
    }
    finally
    {
      try { ex.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  /**
   * Selects a record from the database.  The record must contain primary
   * key info.
   */
  private Record selectRecord(Record record)
  {
    Selection sel = getSelection(record);
    List records = selectAllRecords(sel);
    return records.size() > 0 ? (Record)records.get(0) : null;
  }

  /**
   * Inserts or updates a record in the database.
   */
  private void persistRecord(Record record, Object userObj)
  {
    User user = validateUser(userObj,"persistRecord (" + record + ")");

    // determine if record already has a row in it's db table
    Record dbRecord = selectRecord(record);
    if (dbRecord == null)
    {
      // record is new to db so insert a new row in 
      // the record's db table
      insertRecord(record,user);
    }
    else if (!record.equals(dbRecord))
    {
      // copy db's version of record state to record for audit 
      // log generation, then update the record's db data
      record.copyDbState(dbRecord);
      updateRecord(record,user);
    }
    else
    {
      //log.debug("Duplicate record found in db, update not performed: " + record);
    }
  }
  
  private int deleteAllRecords(Selection sel, Object userObj)
  {
    User user = validateUser(userObj,"deleteAllRecords (" + sel.getTableDef() + ")");

    Executable ex = null;
    int rowCount = 0;

    try
    {
      connect();

      ex = new MesDelete(sel);
      log.debug("delete:\n" + ex.getStatement());
      rowCount = ex.executeUpdate(con);
      
      if (rowCount > 0)
      {
        logAuditData(sel,user,DBA_DELETE);
      }

      return rowCount;
    }
    catch (Exception e)
    {
      log.error("error deleting from " + sel.getTableDef().getName() + ": " + e);
      e.printStackTrace();
      throw new RuntimeException(e.toString());
    }
    finally
    {
      try { ex.close(); } catch (Exception e) { }
      cleanUp();
    }
  }
  
  /**************************************************************************
   *
   * Bean operations
   *
   *************************************************************************/

  /**
   * Generate selection objects for this db.
   */
  public Selection getSelection(Class beanClass)
  {
    return new MesSelection(schema,beanClass);
  }
  public Selection getSelection(Object bean, boolean autoIdx)
  {
    return new MesSelection(schema,bean,autoIdx);
  }
  public Selection getSelection(Object bean)
  {
    return getSelection(bean,true);
  }
  public Selection getSelection(String joinName)
  {
    return new MesSelection(schema,joinName);
  }
  public Selection getSelection(Record record)
  {
    return new MesSelection(schema,record);
  }

  /**
   * Store the bean in the database.
   */
  public void persist(Object bean, Object userObj)
  {
    persistRecord(generateRecordFromBean(bean),userObj);
  }

  /**
   * Selects all beans from database using the given select statement
   */
  public List selectAll(Selection sel)
  {
    return generateBeansFromRecords(selectAllRecords(sel));
  }

  /**
   * Loads a bean from the database using the select statement.  This returns
   * the first found bean in the database.  It is possible to call this
   * method with a selection that generates multiple rows.
   */
  public Object select(Selection sel)
  {
    List records = selectAllRecords(sel);
    if (records.size() > 0)
    {
      return generateBeanFromRecord((Record)records.get(0));
    }
    return null;
  }

  /**
   * Generates a map using a join selection where the map keys are source table
   * objects and map values are target table objects.
   */
  public Map selectJoinMap(Selection sel)
  {
    return generateBeanMapFromRecords(selectJoinMapKeyRecords(sel),
      selectJoinMapValueRecords(sel),sel.getJoinDef().isOneToMany());
  }

  /**
   * Count number of rows a selection object would generate.
   */
  public int selectCount(Selection sel)
  {
    try
    {
      connect();
      Executable ex = new MesSelect(sel);
      int count = ex.executeCountQuery(con);
      log.debug("select count:\n" + ex.getStatement());
      return count;
    }
    catch (Exception e)
    {
      log.error("error executing count query for " + sel.getTableDef().getName() 
        + ": " + e);
      e.printStackTrace();
      throw new RuntimeException(e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /**
   * Deletes all records indicated by the selection object.
   */
  public void delete(Selection sel, Object userObj)
  {
    deleteAllRecords(sel,userObj);
  }

  /**
   * Externally managed selection
   */

  private Selection externSel;
  private Executable externEx;

  public void startSelection(Selection sel)
  {
    try
    {
      doConnect();
      externEx = new MesSelect(sel);
      externEx.executeQuery(con);
      externSel = sel;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      String errorMsg = "error starting selection from " 
        + sel.getTableDef().getName() + ": " + e;
      log.error(errorMsg);
      throw new RuntimeException(errorMsg);
    }
  }

  public Object getNext()
  {
    try
    {
      if (externEx.next())
      {
        return generateBeanFromRecord(
                generateRecord(externSel.getTableDef(),externEx));
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      String errorMsg = "error getting next bean: " + e;
      log.error(errorMsg);
      throw new RuntimeException(errorMsg);
    }
    return null;
  }

  public void finishSelection()
  {
    try { externEx.close(); } catch (Exception e) { }
    try { doDisconnect(); } catch (Exception e) { }
  }


  /**
   * Externally managed prepared statement
   */

  private PreparedStatement externPs;
  private ResultSet         externRs;

  public void startManualStatement(PreparedStatement ps)
  {
    try
    {
      externPs = ps;
      externRs = externPs.executeQuery();
    }
    catch (Exception e)
    {
      log.error("Error starting manual statement: " + e);
      e.printStackTrace();
      try { externRs.close(); } catch (Exception ie) { }
      try { externPs.close(); } catch (Exception ie) { }
      doDisconnect();
    }
  }

  public ResultSet getDlRs()
  {
    return externRs;
  }
  
  public void finishManualStatement()
  {
      try { externRs.close(); } catch (Exception e) { }
      try { externPs.close(); } catch (Exception e) { }
      try { doDisconnect(); } catch (Exception e) { }
  }

}