package com.mes.persist.mes;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import com.mes.persist.ColumnDef;
import com.mes.persist.DataType;
import com.mes.persist.IndexDef;
import com.mes.persist.JoinDef;
import com.mes.persist.Schema;
import com.mes.persist.SubqueryDef;
import com.mes.persist.SubtableDef;
import com.mes.persist.TableDef;

public class MesXmlSchemaParser
{
  static Logger log = Logger.getLogger(MesXmlSchemaParser.class);

  public MesXmlSchemaParser()
  {
  }

  private DataType getDataTypeFromName(String dtName)
  {
    if (dtName.equals("string"))
    {
      return DataType.STRING;
    }
    else if (dtName.equals("long"))
    {
      return DataType.LONG;
    }
    else if (dtName.equals("timestamp"))
    {
      return DataType.TIMESTAMP;
    }
    else if (dtName.equals("int"))
    {
      return DataType.INT;
    }
    else if (dtName.equals("double"))
    {
      return DataType.DOUBLE;
    }
    else if (dtName.equals("float"))
    {
      return DataType.FLOAT;
    }
    else if (dtName.equals("date"))
    {
      return DataType.DATE;
    }
    else if (dtName.equals("time"))
    {
      return DataType.TIME;
    }
    else
    {
      throw new RuntimeException("Invalid data type name " + dtName);
    }
  }

  private void generateTableColumns(Element te, TableDef td)
  {
    for (Iterator i = te.getChildren("column").iterator(); i.hasNext();)
    {
      Element ce = (Element)i.next();
      String name = ce.getAttribute("name").getValue();
      DataType dt = getDataTypeFromName(ce.getAttribute("type").getValue());
      td.addColumnDef(name,dt);
    }
  }

  private void generateSubqueries(Element te, Schema schema, TableDef td)
    throws Exception
  {
    for (Iterator i = te.getChildren("subquery").iterator(); i.hasNext();)
    {
      Element sqEl = (Element)i.next();

      String name = sqEl.getAttribute("name").getValue();
      String subName = sqEl.getAttribute("sub-name").getValue();
      TableDef subTd = schema.getTableDef(subName);
      if (subTd == null)
      {
        throw new NullPointerException("Subquery table " + subName + " not found"
          + " for subquery " + name + " in table " + td.getName());
      }

      String joinColName = sqEl.getAttribute("join-column").getValue();
      ColumnDef parentCd = td.getColumnDef(joinColName);
      if (parentCd == null)
      {
        throw new NullPointerException("Table " + td.getName() + " does not"
          + " contain column " + joinColName + " for subquery " + name);
      }
      String subIdxName = sqEl.getAttribute("sub-index").getValue();
      IndexDef subIdxDef = subTd.getIndexDef(subIdxName);
      if (subIdxDef == null)
      {
        throw new NullPointerException("Subquery table " + name + " does not"
          + " contain index " + subIdxName + " in table " + td.getName());
      }
      if (subIdxDef.getColumnDefSet().getColumnDefs().size() != 1)
      {
        throw new RuntimeException("Index " + subIdxDef.getName() + " must have"
          +" single column in subquery " + name);
      }
      ColumnDef subCd = (ColumnDef)subIdxDef.getColumnDefSet().getColumnDefs().get(0);

      Object sqType 
        = sqEl.getAttribute("subquery-type").getValue().equals("unique")
        ? SubqueryDef.ONE_TO_ONE
        : SubqueryDef.AGGREGATE;

      boolean requiredFlag = sqEl.getAttribute("required").getValue().equals("true");

      SubqueryDef sqDef = 
        new MesSubqueryDef(name,td,parentCd,subTd,subCd,sqType,requiredFlag);

      for (Iterator j = sqEl.getChildren("subquery-column").iterator(); j.hasNext();)
      {
        Element sqcEl = (Element)j.next();
        String colName = sqcEl.getAttribute("name").getValue();
        DataType dt = getDataTypeFromName(sqcEl.getAttribute("type").getValue());
        String wrapperExpr = null;
        if (sqcEl.getAttribute("wrapper-expression") != null)
        {
          wrapperExpr = sqcEl.getAttribute("wrapper-expression").getValue();
        }
        String sqExpr = null;
        if (sqcEl.getAttribute("expression") != null)
        {
          sqExpr = sqcEl.getAttribute("expression").getValue();
        }
        sqDef.addColumnDef(colName,dt,wrapperExpr,sqExpr);
      }

      td.addSubqueryDef(sqDef);
    }
  }

  private void generateSubtables(Element te, Schema schema, TableDef td)
    throws Exception
  {
    for (Iterator i = te.getChildren("subtable").iterator(); i.hasNext();)
    {
      Element subEl = (Element)i.next();

      String subName = subEl.getAttribute("sub-name").getValue();
      TableDef subTd = schema.getTableDef(subName);
      if (subTd == null)
      {
        throw new NullPointerException("Subtable " + subName + " not found"
          + " for subtable def in table " + td.getName());
      }

      String subColumn = subEl.getAttribute("sub-column").getValue();
      String subIndex = subEl.getAttribute("sub-index").getValue();

      Object subType 
        = subEl.getAttribute("sub-type").getValue().equals("unique")
        ? JoinDef.ONE_TO_ONE
        : JoinDef.ONE_TO_MANY;

      String subReq
        = subEl.getAttribute("required").getValue().equals("false")
        ? SubtableDef.OPTIONAL
        : SubtableDef.REQUIRED;

      String accName = null;
      if (subEl.getAttribute("acc-name") != null)
      {
        accName = subEl.getAttribute("acc-name").getValue();
      }

      String subColumnTable = null;
      if (subEl.getAttribute("sub-column-table") != null)
      {
        subColumnTable = subEl.getAttribute("sub-column-table").getValue();
      }

      td.addSubtableDef(subColumn,subTd,subIndex,subType,subReq,accName,subColumnTable);

      /*
      List filters = subEl.getChildren("filter");
      if (!filters.isEmpty())
      {
        if (accName == null)
        {
          throw new NullPointerException("Subtable " + subName + " must have "
            + "explicit accessor name in order to define filters");
        }

        SubtableDef sd = td.getSubtableDef(accName);
        for (Iterator j = filters.iterator(); j.hasNext();)
        {
          Element filterEl = (Element)j.next();
          String filterCol = filterEl.getAttribute("filter-column").getValue();
          String filterVal = filterEl.getAttribute("filter-value").getValue();
          sd.addFilter(filterCol,filterVal);
          log.debug("added filter " + filterCol + " = " + filterVal);
        }
      }
      */
    }
  }

  private void generateTableIndexes(Element te, TableDef td)
  {
    for (Iterator i = te.getChildren("index").iterator(); i.hasNext();)
    {
      Element ce = (Element)i.next();
      String name = ce.getAttribute("name").getValue();
      boolean isKey = ce.getAttribute("type").getValue().equals("unique");
      IndexDef id = td.addIndexDef(name,isKey);
      String keyColName = ce.getAttribute("key-column").getValue();
      if (!keyColName.equals("@is-compound"))
      {
        id.addColumnDef(keyColName);
      }
      else
      {
        for (Iterator j = ce.getChildren("index-column").iterator(); j.hasNext();)
        {
          Element icEl = (Element)j.next();
          id.addColumnDef(icEl.getAttribute("column").getValue());
        }
      }
    }
  }

  private void generateTables(Element root, Schema schema) throws Exception
  {
    for (Iterator i = root.getChildren("table").iterator(); i.hasNext();)
    {
      Element el = (Element)i.next();
      String name = el.getAttribute("name").getValue();
      Class beanClass = Class.forName(el.getAttribute("class").getValue());
      TableDef td = new MesTableDef(name,beanClass);
      generateTableColumns(el,td);
      generateSubqueries(el,schema,td);
      generateSubtables(el,schema,td);
      generateTableIndexes(el,td);
      schema.addTableDef(td);
    }
  }

  private void generateJoins(Element root, Schema schema) throws Exception
  {
    for (Iterator i = root.getChildren("join").iterator(); i.hasNext();)
    {
      Element el = (Element)i.next();

      String name = el.getAttribute("name").getValue();

      String fromTable = el.getAttribute("from-table").getValue();
      TableDef fromTd = schema.getTableDef(fromTable);
      if (fromTd == null)
      {
        throw new NullPointerException("From table named " + fromTable
          + " not found for join " + name);
      }

      String toTable = el.getAttribute("to-table").getValue();
      TableDef toTd = schema.getTableDef(toTable);
      if (toTd == null)
      {
        throw new NullPointerException("To table named " + toTable
          + " not found for join " + name);
      }

      String fromColumn = el.getAttribute("from-column").getValue();
      String toColumn = el.getAttribute("to-column").getValue();
      Object joinType 
        = el.getAttribute("type").getValue().equals("one-to-one")
        ? JoinDef.ONE_TO_ONE
        : JoinDef.ONE_TO_MANY;

      fromTd.addJoinDef(name,fromColumn,toTd,toColumn,joinType);
    }
  }

  private void generateCompoundJoins(Element root, Schema schema) throws Exception
  {
    for (Iterator i = root.getChildren("compound-join").iterator(); i.hasNext();)
    {
      Element el = (Element)i.next();

      JoinDef jd = null;

      // extract attrs from compound-join element
      String joinName = el.getAttribute("name").getValue();
      String joinTable = el.getAttribute("table").getValue();
      TableDef joinTd = schema.getTableDef(joinTable);
      if (joinTd == null)
      {
        throw new NullPointerException("Join table " + joinTable
          + " not found for compound join " + joinName);
      }
      String joinCol = el.getAttribute("join-col").getValue();
      Object joinType 
        = el.getAttribute("type").getValue().equals("one-to-one")
        ? JoinDef.ONE_TO_ONE
        : JoinDef.ONE_TO_MANY;

      // process any inner link elements
      for (Iterator j = el.getChildren("inner-link").iterator(); j.hasNext();)
      {
        Element innerEl = (Element)j.next();

        // extract inner link attrs to create join
        String innerTable = innerEl.getAttribute("table").getValue();
        TableDef innerTd = schema.getTableDef(innerTable);
        if (innerTd == null)
        {
          throw new NullPointerException("Inner table " + innerTable
            + " not found for compound join " + joinName);
        }
        String linkCol = innerEl.getAttribute("link-col").getValue();

        if (jd == null)
        {
          jd = joinTd.addJoinDef(joinName,joinCol,innerTd,linkCol,joinType);
        }
        else
        {
          jd.addJoin(joinCol,innerTd,linkCol,joinType);
        }

        // update join col and type for next join
        joinCol = innerEl.getAttribute("join-col").getValue();
        joinType
          = innerEl.getAttribute("type").getValue().equals("one-to-one")
          ? JoinDef.ONE_TO_ONE
          : JoinDef.ONE_TO_MANY;
      }

      // process end link element
      Element endEl = el.getChild("end-link");

      // extract end link attrs to create join
      String endTable = endEl.getAttribute("table").getValue();
      TableDef endTd = schema.getTableDef(endTable);
      if (endTd == null)
      {
        throw new NullPointerException("End table " + endTable
          + " not found for compound join " + joinName);
      }
      String linkCol = endEl.getAttribute("link-col").getValue();

      if (jd == null)
      {
        jd = joinTd.addJoinDef(joinName,joinCol,endTd,linkCol,joinType);
      }
      else
      {
        jd.addJoin(joinCol,endTd,linkCol,joinType);
      }
    }
  }

  private Schema generateSchema(Document doc)
  {
    Schema schema = null;

    try
    {
      Element root = doc.getRootElement();
      String idName = root.getAttribute("id-gen-name").getValue();
      String logTable = root.getAttribute("log-table-name").getValue();
      schema = new MesSchema(idName,logTable);
      generateTables(root,schema);
      generateJoins(root,schema);
      generateCompoundJoins(root,schema);
      log.debug("Schema for " + root.getAttribute("name").getValue() + " is valid");
    }
    catch (Exception e)
    {
      log.error("Error in generateSchema(): " + e);
      e.printStackTrace();
    }

    return schema;
  }
  
  private Document generateDocument(Object source)
  {
    SAXBuilder builder 
      = new SAXBuilder("org.apache.xerces.parsers.SAXParser",true);
    Document doc = null;
    try
    {
      if (source instanceof File)
      {
        doc = builder.build((File)source);
      }
      else if (source instanceof URL)
      {
        doc = builder.build((URL)source);
      }
      else if (source instanceof InputStream)
      {
        doc = builder.build((InputStream)source);
      }
      else
      {
        String errorText = "Invalid xml input source: " 
          + source.getClass().getName();
        log.error(errorText);
        throw new RuntimeException(errorText);
      }
    }
    catch (Exception e)
    {
      String errorText = "Error generating JDOM document: " + e;
      log.error(errorText);
      throw new RuntimeException(errorText);
    }
    return doc;
  }

  public Schema parseXml(Object xmlSource)
  {
    return generateSchema(generateDocument(xmlSource));
  }
}