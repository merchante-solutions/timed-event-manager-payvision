package com.mes.persist.mes;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.persist.ColumnDef;
import com.mes.persist.ColumnDefSet;
import com.mes.persist.DataType;
import com.mes.persist.IndexDef;
import com.mes.persist.JoinDef;
import com.mes.persist.SubqueryDef;
import com.mes.persist.SubtableDef;
import com.mes.persist.TableDef;

public class MesTableDef implements TableDef
{
  static Logger log = Logger.getLogger(MesTableDef.class);
  
  private String name;
  private String qualifier;
  private Class beanClass;
  
  private MesColumnDefSet cDefSet = new MesColumnDefSet();

  private Map setterMap = new HashMap();
  private Map getterMap = new HashMap();
  
  private List iDefs = new ArrayList();
  private IndexDef primaryKeyDef = null;

  private List sdList = new ArrayList();
  private Map sdMap = new HashMap();

  private Map jDefs = new HashMap();
  
  public MesTableDef(String name, Class beanClass)
  {
    this.name = name;
    this.beanClass = beanClass;
    processClass(beanClass);
  }
  
  /**
   * Processes the class that is being associated with this table def.  Makes
   * sure the class can be instantiated with no args and loads all viable 
   * class setter and getter methods into a map.  
   */
  private void processClass(Class beanClass)
  {
    // do a test instantiation
    try
    {
      Object bean = beanClass.newInstance();
    }
    catch (Exception e)
    {
      throw new RuntimeException("Invalid bean class, newInstance() failed: "
        + e);
    }

    // find all setter methods out of the class
    // (public method starting with "set" taking a single parameter)
    Method[] methods = beanClass.getMethods();
    for (int i = 0; i < methods.length; ++i)
    {
      // setters
      String name = methods[i].getName();
      if (name.startsWith("set") && methods[i].getParameterTypes().length == 1)
      {
        String mapName = name.substring(3).toLowerCase();
        setterMap.put(mapName,methods[i]);
      }
      // getters
      else if (name.startsWith("get") && methods[i].getParameterTypes().length == 0)
      {
        String mapName = name.substring(3).toLowerCase();
        getterMap.put(mapName,methods[i]);
      }
    }
  }
  
  /**
   * Returns the name of the database table associated with this table def.
   */
  public String getName()
  {
    return name;
  }

  public void setQualifier(String qualifier)
  {
    this.qualifier = qualifier;
  }
  
  public String getQualifier()
  {
    return qualifier;
  }

  public String getQualifiedName()
  {
    if (qualifier == null)
    {
      throw new NullPointerException("Table qualifier is not set");
    }
    return name + " " + qualifier;
  }

  /**
   * Returns the bean class associated with this table def.
   */
  public Class getBeanClass()
  {
    return beanClass;
  }
  
  /**
   * Returns a column name adjusted to match a bean accessor.  Currently this
   * means lower case and only letters or digits.  May need to do more strict
   * adjustment (follow java identifier rules more strictly).
   */
  private String getAdjustedName(String name)
  {
    StringBuffer adjustBuf = new StringBuffer();
    for (int i = 0; i < name.length(); ++i)
    {
      char ch = name.charAt(i);
      if (Character.isLetterOrDigit(ch))
      {
        adjustBuf.append(ch);
      }
    }
    return (""+adjustBuf).toLowerCase();
  }
  
  /**
   * Adds a column def to the table.  The column must have a corresponding
   * getter/setter methods in the setterMap which is registered with the 
   * column def.
   */
  public void addColumnDef(ColumnDef cDef)
  {
    String adjName = getAdjustedName(cDef.getName());
    
    // register bean class setter with column
    Method setMethod = (Method)setterMap.get(adjName);
    if (setMethod == null)
    {
      throw new NullPointerException("No corresponding class setter method"
        + " found for column def " + cDef.getName());
    }
    cDef.setSetterMethod(setMethod);
    
    // register bean class getter with column
    Method getMethod = (Method)getterMap.get(adjName);
    if (getMethod == null)
    {
      throw new NullPointerException("No corresponding class getter method"
        + " found for column def " + cDef.getName());
    }
    cDef.setGetterMethod(getMethod);
    
    cDefSet.addColumnDef(cDef);    
  }

  /**
   * Creates and adds a column def with the given name and data type and with 
   * this table name as the qualifier.
   */
  public void addColumnDef(String colName, DataType dataType)
  {
    addColumnDef(new MesColumnDef(colName,dataType));
  }
  
  /**
   * Retrieves the column def with the given name.
   */
  public ColumnDef getColumnDef(String name)
  {
    return cDefSet.getColumnDef(name);
  }
  
  /**
   * Returns a list containing this table def's column defs.
   */
  public ColumnDefSet getColumnDefSet()
  {
    return cDefSet;
  }
  
  /**
   * Creates an index def with the given name and uniqueness flag.  The def
   * is stored internally.  If this is the first unique index to be defined
   * it is also designated the primary key for the table. 
   *
   * Returns the created def.
   */
  public IndexDef addIndexDef(String name, boolean isUnique)
  {
    IndexDef iDef = new MesIndexDef(name,this,isUnique);
    if (primaryKeyDef == null && iDef.isUnique())
    {
      primaryKeyDef = iDef;
    }
    iDefs.add(iDef);
    return iDef;
  }

  public IndexDef getIndexDef(String name)
  {
    for (Iterator i = iDefs.iterator(); i.hasNext();)
    {
      IndexDef id = (IndexDef)i.next();
      if (id.getName().equals(name))
      { 
        return id;
      }
    }
    return null;
  }
  
  /**
   * Returns list of index defs defined for this table.
   */  
  public List getIndexDefs()
  {
    return iDefs;
  }
  
  /**
   * Returns the table's primary index def.
   */
  public IndexDef getPrimaryKeyDef()
  {
    return primaryKeyDef;
  }

  private String determineSubtableAccessor(TableDef subTd, Object joinType,
    String subAccName)
  {
    StringBuffer accBuf = new StringBuffer();
    if (subAccName != null && subAccName.length() > 0)
    {
      accBuf.append(subAccName.toLowerCase());
    }
    else
    {
      accBuf.append(subTd.getBeanClass().getName().toLowerCase());
      int idx = accBuf.lastIndexOf(".");
      if (idx != -1)
      {
        accBuf.delete(0,idx + 1);
      }
      if (joinType.equals(JoinDef.ONE_TO_MANY))
      {
        accBuf.append(accBuf.toString().endsWith("s") ? "es" : "s");
      }
    }
    String accName = accBuf.toString();

    // make sure accessor isn't already in use by another subtable
    if (sdMap.get(accName) != null)
    {
      throw new RuntimeException("Attempt to define subtable with"
        + " accessor " + subAccName + " that is already in use by"
        + " another subtable");
    }

    /*
    Method getter = (Method)getterMap.get(accName);
    for (Iterator i = sdList.iterator(); i.hasNext();)
    {
      SubtableDef sd = (SubtableDef)i.next();
      if (sd.getGetter().equals(getter))
      {
        throw new RuntimeException("Attempt to define subtable with"
          + " accessor " + subAccName + " that is already in use by"
          + " another subtable");
      }
    }
    */

    return accName;
  }

  /**
   * Adds a table def as a subtable to this table def.
   */
  public void addSubtableDef(String colName, TableDef subTd, String subIdxName, 
    Object joinType, Object tableType, String subAccName, String subTblName)
  {
    // validate join type
    if (!joinType.equals(JoinDef.ONE_TO_MANY) &&
        !joinType.equals(JoinDef.ONE_TO_ONE))
    {
      throw new ClassCastException("Invalid subtable join type (" +
        joinType.getClass().getName() + ": " + joinType + ")");
    }

    // if joining to another subtable, make sure this is
    // not a one-to-many and find the other subtable def 
    // and make sure it's not one-to-many also
    SubtableDef joinSubTd = null;
    if (subTblName != null)
    {
      // to avoid issues with SubtableList...
      if (!joinType.equals(JoinDef.ONE_TO_ONE))
      {
        throw new RuntimeException("Invalid join type " + joinType + " for"
          + " subtable " + subTd.getName() + ", subtable to"
          + " subtable joins must be one-to-one");
      }
      joinSubTd = (SubtableDef)sdMap.get(subTblName);
      if (joinSubTd == null)
      {
        throw new NullPointerException("Subtable " + subTblName + " not found"
          + " in table def " + name + " for subtable " + subTd.getName());
      }
      // to avoid issues with SubtableList...
      if (!joinSubTd.getJoinType().equals(JoinDef.ONE_TO_ONE))
      {
        throw new NullPointerException("Subtable " + subTd.getName() + " cannot"
          + " join to subtable " + subTblName + " in table " + name + " because"
          + " subtable " + subTblName + " is not one-to-one");
      }
    }
      
    // determine how accessors are named, may be explicitly set by schema
    // or implicitly named with bean class name...
    String accName = determineSubtableAccessor(subTd,joinType,subAccName);
    Method getter = (Method)getterMap.get(accName);
    if (getter == null)
    {
      throw new NullPointerException("No getter method for " + accName +
        " in bean class " + beanClass.getName());
    }
    Method setter = (Method)setterMap.get(accName);
    if (setter == null)
    {
      throw new NullPointerException("No setter method for " + accName +
        " in bean class " + beanClass.getName());
    }

    // validate col name exists either in parent table 
    // or the subtable this is joining to
    if (joinSubTd != null)
    {
      if (joinSubTd.getColumnDef(colName) == null)
      {
        throw new NullPointerException("No column named " + colName + " found in"
          + " target subtable " + joinSubTd.getName() + " for subtable "
          + subTd.getName() + " in table " + name);
      }
    }
    else
    {
      if (getColumnDef(colName) == null)
      {
        throw new NullPointerException("No column named " + colName + " found in"
          + " table " + name + " for subtable " + subTd.getName());
      }
    }
    /* Not sure what this colMethod check is for...hmm

    Method colMethod = (Method)setterMap.get(getAdjustedName(colName));
    if (colMethod == null)
    {
      throw new NullPointerException("No setter method found for " + colName
        + " in bean class " + beanClass.getName());
    }

    // validate column name
    if (cDefSet.getColumnDef(colName) == null)
    {
      throw new NullPointerException("Column name " + colName + " not found"
        + " in table def " + name);
    }
    */

    // validate sub idx name (needs to exist in subtable and be single column)
    IndexDef iDef = subTd.getIndexDef(subIdxName);
    if (iDef == null)
    {
      throw new NullPointerException("Index " + subIdxName + " not found in "
        + " subtable " + subTd.getName());
    }
    if (iDef.getColumnDefSet().getColumnDefs().size() != 1)
    {
      throw new RuntimeException("Index " + subIdxName + " must have single"
        + " column in subtable " + subTd.getName());
    }

    // generate subtable qualifier
    int cnt = 1;
    String subKey = null;
    String subQual = null;
    do
    {
      subKey = "s" + cnt + subTd.getQualifier();
      subQual = "s" + cnt;
      ++cnt;
    }
    while (sdMap.get(subKey) != null);

    SubtableDef sd = new MesSubtableDef(subQual,getter,setter,this,colName,
      subTd,subIdxName,(String)joinType,tableType,joinSubTd);
    sdList.add(sd);
    sdMap.put(subTd.getName(),sd);
    sdMap.put(subKey,sd);
    sdMap.put(accName,sd);
  }
  public void addSubtableDef(String colName, TableDef subTd, String subIdxName, 
    Object joinType, Object tableType)
  {
    addSubtableDef(colName,subTd,subIdxName,joinType,tableType,null,null);
  }

  public SubtableDef getSubtableDef(String key)
  {
    return (SubtableDef)sdMap.get(key);
  }

  public List getSubtableDefs()
  {
    return sdList;
  }

  /**
   * Create a join between this table and another using the specified columns.
   * Returns the created join def.
   */
  public JoinDef addJoinDef(String joinName, String leftName, TableDef rightTd,
    String rightName, Object joinType)
  {
    JoinDef jd = new MesJoinDef(joinName,this,leftName,rightTd,rightName,joinType);
    jDefs.put(joinName,jd);
    return jd;
  }

  /**
   * Gets the join def with the given name.
   */
  public JoinDef getJoinDef(String joinName)
  {
    return (JoinDef)jDefs.get(joinName);
  }

  /**
   * Returns a list of all join defs defined for this table.
   */
  public List getJoinDefs()
  {
    return new ArrayList(jDefs.values());
  }

  /**
   * Clause getters.
   */

  public String getQualifier(String prefix)
  {
    return (prefix == null ? "" : prefix) + qualifier;
  }

  public String getAliasedColumnName(String colName, String prefix)
  {
    prefix = (prefix == null ? "" : prefix) + qualifier;
    ColumnDef cd = cDefSet.getColumnDef(colName);
    if (cd != null)
    {
      return cd.getColumnAlias(prefix);
    }
    
    String alias = null;
    for (Iterator i = sdList.iterator(); i.hasNext() && alias == null;)
    {
      SubtableDef std = (SubtableDef)i.next();
      alias = std.getAliasedColumnName(colName,prefix);
    }

    return alias;
  }  
  public String getAliasedColumnName(String colName)
  {
    return getAliasedColumnName(colName,null);
  }

  public String getQualifiedColumnName(String colName, String prefix)
  {
    prefix = (prefix == null ? "" : prefix) + qualifier;
    ColumnDef cd = cDefSet.getColumnDef(colName);
    if (cd != null)
    {
      return cd.getQualifiedName(prefix);
    }
    
    String alias = null;
    for (Iterator i = sdList.iterator(); i.hasNext() && alias == null;)
    {
      SubtableDef std = (SubtableDef)i.next();
      alias = std.getQualifiedColumnName(colName,prefix);
    }

    return alias;
  }
  public String getQualifiedColumnName(String colName)
  {
    return getQualifiedColumnName(colName,null);
  }

  /**
   * Returns select clause for table.  Subtable select clauses are included.
   */
  public String getSelectClause()
  {
    return getSelectClause(null);
  }
  public String getSelectClause(String prefix)
  {
    StringBuffer clause = new StringBuffer();
    String qualifier = getQualifier(prefix);
    clause.append(cDefSet.getSelectClause(qualifier));
    for (Iterator i = sdList.iterator(); i.hasNext();)
    {
      SubtableDef td = (SubtableDef)i.next();
      if (td.getJoinType().equals(JoinDef.ONE_TO_ONE))
      {
        clause.append(",\n" + td.getSelectClause(qualifier));
      }
    }
    return clause.toString();
  }

  public String getFromClause()
  {
    return getFromClause(null);
  }
  public String getFromClause(String prefix)
  {
    StringBuffer clause = new StringBuffer();
    String qualifier = getQualifier(prefix);
    clause.append(name + " " + qualifier);
    for (Iterator i = sdList.iterator(); i.hasNext();)
    {
      SubtableDef td = (SubtableDef)i.next();
      if (td.getJoinType().equals(JoinDef.ONE_TO_ONE))
      {
        clause.append(",\n" + td.getFromClause(qualifier));
      }
    }
    for (Iterator i = subqueryDefs.iterator(); i.hasNext();)
    {
      SubqueryDef sqDef = (SubqueryDef)i.next();
      clause.append(",\n" + sqDef.getFromClause(qualifier));
    }
    return clause.toString();
  }

  public String getWhereClause()
  {
    return getWhereClause(null);
  }
  public String getWhereClause(String prefix)
  {
    return getWhereClause(prefix,false);
  }
  public String getWhereClause(String prefix, boolean parentOptional)
  {
    StringBuffer clause = new StringBuffer();
    String qualifier = getQualifier(prefix);
    for (Iterator i = sdList.iterator(); i.hasNext();)
    {
      SubtableDef td = (SubtableDef)i.next();
      if (td.getJoinType().equals(JoinDef.ONE_TO_ONE))
      {
        clause.append(clause.length() > 0 ? " and\n" : "");
        clause.append(td.getWhereClause(qualifier,parentOptional));
      }
    }
    for (Iterator i = subqueryDefs.iterator(); i.hasNext();)
    {
      SubqueryDef sqDef = (SubqueryDef)i.next();
      clause.append(clause.length() > 0 ? " and\n" : "");
      clause.append(sqDef.getWhereClause(qualifier,parentOptional));
    }
    return clause.toString();
  }

  public String getInsertColumnList()
  {
    return cDefSet.getInsertColumnList(qualifier);
  }

  public String getInsertValueMarks()
  {
    return cDefSet.getInsertValueMarks();
  }

  public String getUpdateSetList()
  {
    return cDefSet.getUpdateSetList(qualifier);
  }

  public String toString()
  {
    return "Table " + name;
  }

  private List subqueryDefs = new ArrayList();

  public void addSubqueryDef(SubqueryDef sqDef)
  {
    subqueryDefs.add(sqDef);
    for (Iterator i = sqDef.getColumnDefs().iterator(); i.hasNext();)
    {
      addColumnDef((ColumnDef)i.next());
    }
  }

  public List getSubqueryDefs()
  {
    return subqueryDefs;
  }
}
