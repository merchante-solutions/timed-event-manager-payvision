package com.mes.persist.mes;

import com.mes.persist.ColumnDef;
import com.mes.persist.ColumnDefSet;
import com.mes.persist.IndexDef;
import com.mes.persist.TableDef;

public class MesIndexDef implements IndexDef
{
  private String name;
  private boolean uniqueFlag;
  private TableDef tDef;
  private MesColumnDefSet cDefSet = new MesColumnDefSet();
  
  public MesIndexDef(String name, TableDef tDef, boolean uniqueFlag)
  {
    this.name = name;
    this.tDef = tDef;
    this.uniqueFlag = uniqueFlag;
  }

  public void addColumnDef(String colName)
  {
    ColumnDef cDef = tDef.getColumnDef(colName);
    if (cDef == null)
    {
      throw new NullPointerException("ColumnDef named " + colName + " does not"
        + " exist in index for table " + tDef.getName());
    }
    cDefSet.addColumnDef(cDef);
  }
  
  public ColumnDefSet getColumnDefSet()
  {
    return cDefSet;
  }
  
  public String getName()
  {
    return name;
  }

  public TableDef getTableDef()
  {
    return tDef;
  }
  
  public boolean isUnique()
  {
    return uniqueFlag;
  }


}
