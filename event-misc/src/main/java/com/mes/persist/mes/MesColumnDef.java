package com.mes.persist.mes;

import java.lang.reflect.Method;
import com.mes.persist.ColumnDef;
import com.mes.persist.DataType;

public class MesColumnDef implements ColumnDef
{
  private String name;
  private DataType dt;
  private String wrapperExpr;
  private Method setterMethod;
  private Method getterMethod;
  
  public MesColumnDef(String name, DataType dt)
  {
    this.name = name;
    this.dt = dt;
  }
  
  public MesColumnDef(String name, DataType dt, String wrapperExpr)
  {
    this.name = name;
    this.dt = dt;
    this.wrapperExpr = wrapperExpr;
  }

  /**
   * Returns the name of the database column this def is associated with.
   */
  public String getName()
  {
    return name;
  }

  /**
   * Returns the data type definition of the column.
   */
  public DataType getDataType()
  {
    return dt;
  }

  public String getWrapperExpr()
  {
    return wrapperExpr;
  }
  
  /**
   * Stores a setter method.
   */
  public void setSetterMethod(Method setterMethod)
  {
    this.setterMethod = setterMethod;
  }
  
  /**
   * Returns the column's setter method.
   */
  public Method getSetterMethod()
  {
    return setterMethod;
  }
  
  /**
   * Stores a getter method.
   */
  public void setGetterMethod(Method getterMethod)
  {
    this.getterMethod = getterMethod;
  }
  
  /**
   * Returns the column's getter method.
   */
  public Method getGetterMethod()
  {
    return getterMethod;
  }

  public String getColumnAlias(String qualifier)
  {
    return qualifier + "_" + name;
  }

  public String getSubQualifier(String qualifier)
  {
    return qualifier;
  }

  public String getQualifiedName(String alias)
  {
    return alias + "." + name;
  }

  public String getSelectClause(String qualifier)
  {
    String qualName = getQualifiedName(qualifier);
    String alias = getColumnAlias(qualifier);
    if (wrapperExpr == null)
    {
      return qualName + " " + alias;
    }

    StringBuffer qualExpr = new StringBuffer(wrapperExpr);
    int nameLen = name.length();
    int qualLen = qualName.length();
    for (int i = 0; i < qualExpr.length() - nameLen; ++i)
    {
      if (qualExpr.substring(i,i + nameLen).equals(name))
      {
        qualExpr.replace(i,i + nameLen,qualName);
        i += qualLen;
      }
    }

    return qualExpr.toString() + " " + alias;
  }

  public String getSubquerySelectClause()
  {
    return name;
  }

  public String toString()
  {
    return "ColumnDef [ " + name + " " + dt 
      + (wrapperExpr != null ? " " + wrapperExpr : "") + " ]";
  }

  public boolean isReadOnly()
  {
    return false;
  }
}