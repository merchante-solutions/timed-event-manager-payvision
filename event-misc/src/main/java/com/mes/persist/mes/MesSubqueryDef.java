package com.mes.persist.mes;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.persist.ColumnDef;
import com.mes.persist.DataType;
import com.mes.persist.SubqueryDef;
import com.mes.persist.TableDef;

public class MesSubqueryDef implements SubqueryDef
{
  static Logger log = Logger.getLogger(MesSubqueryDef.class);

  public static final String ID_TERMINATOR = "#";

  private String name;

  private TableDef parentTd;
  private ColumnDef parentCd;

  private TableDef subTd;
  private ColumnDef subCd;

  private Object sqType;

  private boolean requiredFlag;

  private MesColumnDefSet cdSet = new MesColumnDefSet();

  public MesSubqueryDef(String name, TableDef parentTd, ColumnDef parentCd, 
    TableDef subTd, ColumnDef subCd, Object sqType, boolean requiredFlag)
  {
    this.name = name;
    this.parentTd = parentTd;
    this.parentCd = parentCd;
    this.subTd = subTd;
    this.subCd = subCd;
    this.sqType = sqType;
    this.requiredFlag = requiredFlag;
  }

  public String getName()
  {
    return name;
  }

  public TableDef getParentTd()
  {
    return parentTd;
  }

  public ColumnDef getParentCd()
  {
    return parentCd;
  }

  public TableDef getSubTd()
  {
    return subTd;
  }

  public ColumnDef getSubCd()
  {
    return subCd;
  }

  public Object getType()
  {
    return sqType;
  }

  public boolean isRequired()
  {
    return requiredFlag;
  }


  public void addColumnDef(String name, DataType dt, String wrapperExpr, 
    String sqExpr)
  {
    MesSubqueryColumnDef cd = 
      new MesSubqueryColumnDef(this,name,dt,wrapperExpr,sqExpr);
    cdSet.addColumnDef(cd);
  }
  public List getColumnDefs()
  {
    return cdSet.getColumnDefs();
  }

  public String getSelectClause(String qualifier)
  {
    return cdSet.getSelectClause(qualifier + name);
  }

  /**
   *  ( select  [ subquery column select clauses ], 
   *            [ subquery-table index select clause ] 
   *    from    [ subquery-table ]
   *    [ group by clause ] ) [parent table qualifier]_[ subquery alias ]
   */
  public String getFromClause(String qualifier)
  {
    StringBuffer clause = new StringBuffer();
    clause.append("( SELECT ");

    // subquery columns/expressions
    for (Iterator i = cdSet.getColumnDefs().iterator(); i.hasNext();)
    {
      ColumnDef cd = (ColumnDef)i.next();
      //SubqueryColumnDef sqcDef = (SubqueryColumnDef)i.next();
      clause.append(cd.getSubquerySelectClause() + ", ");
    }

    // index select columns
    clause.append(subCd.getSubquerySelectClause());

    // from clause
    clause.append(" FROM " + subTd.getName());

    // group by index column if aggregate
    if (sqType.equals(AGGREGATE))
    {
      clause.append(" GROUP BY " + subCd.getSubquerySelectClause());
    }

    // subquery from alias
    clause.append(" ) " + qualifier + name + ID_TERMINATOR);
    return clause.toString();
  }

  public String getWhereClause(String qualifier)
  {
    return getWhereClause(qualifier,false);
  }
  public String getWhereClause(String qualifier, boolean parentOptional)
  {
    // subquery link expression: ' p_tbl.col = p_tblsubname#.col [ (+) ] '
    return parentCd.getQualifiedName(qualifier) + " = "
      + qualifier + name + ID_TERMINATOR + "." + subCd.getName()
      + (!isRequired() || parentOptional ? "(+)" : "");
  }
}