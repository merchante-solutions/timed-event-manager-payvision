package com.mes.persist.mes;

import org.apache.log4j.Logger;
import com.mes.persist.Db;
import com.mes.persist.Transaction;
import com.mes.persist.User;

public abstract class MesTransaction implements Transaction
{
  static Logger log = Logger.getLogger(MesTransaction.class);

  protected long    tranId = -1L;
  protected boolean runFlag;
  protected Db      db;
  protected User    user;

  public MesTransaction(Db db, User user)
  {
    this.db = db;
    this.user = user;
  }

  public abstract int execute();

  /**
   * Does a transaction run.  Uses an MesDb object for connectivity and transaction
   * handling, does child defined execute(), commits or rolls back transaction
   * based on result of execute() call.
   */
  public final boolean run()
  {
    boolean runOk = false;

    try
    {
      runFlag = true;
      tranId = db.startTransaction();

      int result = execute();
      if (result == COMMIT)
      {
        db.commitTransaction();
        runOk = true;
      }
      else if (result == ROLLBACK)
      {
        db.rollbackTransaction();
      }
      else
      {
        throw new Exception("Invalid execution result: " + result);
      }
    }
    catch (Exception e)
    {
      log.error("Error in transaction: " + e);
      e.printStackTrace();
      try { db.rollbackTransaction(); } catch (Exception rte) { }
    }
    finally
    {
      try
      {
        db.finishTransaction();
      }
      catch (Exception fte)
      {
        log.warn("Transaction finish error: " + fte);
      }
      runFlag = false;
    }

    return runOk;
  }

  public long getTranId()
  {
    return tranId;
  }
}
