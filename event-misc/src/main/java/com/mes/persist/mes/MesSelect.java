package com.mes.persist.mes;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import com.mes.persist.Column;
import com.mes.persist.ColumnDef;
import com.mes.persist.DataType;
import com.mes.persist.JoinDef;
import com.mes.persist.Order;
import com.mes.persist.Record;
import com.mes.persist.Selection;

public class MesSelect extends MesExecutable
{
  static Logger log = Logger.getLogger(MesSelect.class);

  public static final int ST_STANDARD         = 1;
  public static final int ST_JOIN_MAP_KEYS    = 2;
  public static final int ST_JOIN_MAP_VALUES  = 3;

  private Map idMap;
  private int selType;

  public MesSelect(Selection sel, int selType)
  {
    super(sel,ET_QUERY);
    this.selType = selType;
  }
  public MesSelect(Selection sel)
  {
    this(sel,ST_STANDARD);
  }

  /*****************************************************************************
   *
   *  Id mapping
   *
   ****************************************************************************/

  public class LongestFirstComparator implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      int len1 = (""+o1).length();
      int len2 = (""+o2).length();
      if (len2 < len1) return -1;
      else if (len2 == len1) return 0;
      else return 1;
    }
  }

  /**
   * Replace id's in statement with shorter ones.  Map replaced id's to there
   * replacements.
   */
  private String replaceIds(String stmt)
  {
    Pattern pAnyId = Pattern.compile(ID_TAG + "([a-zA-Z0-9]+)[,.\n]+|" 
                                   + ID_TAG + "([a-zA-Z0-9_]+" 
                                   + MesSubqueryDef.ID_TERMINATOR + ")");
    Matcher mAnyId = pAnyId.matcher(stmt);
    List idList = new ArrayList();
    while (mAnyId.find())
    {
      String id = mAnyId.group(1);
      if (id == null) id = mAnyId.group(2);
      if (!idList.contains(id)) idList.add(id);
    }
    // sort longest to shortest
    Collections.sort(idList,new LongestFirstComparator());
    int idCount = idList.size();
    idMap = new HashMap();
    for (Iterator i = idList.iterator(); i.hasNext();)
    {
      String longId = ""+i.next();
      Pattern p = Pattern.compile(ID_TAG + longId);
      Matcher m = p.matcher(stmt);
      String shortId = "t" + idCount--;
      stmt = m.replaceAll(shortId);
      idMap.put(longId,shortId);
    }
    return stmt;
  }

  /**
   * Lookup the id in the id map.  If found, mapped id is returned, otherwise
   * the original id is returned.
   */
  private String translateId(String id)
  {
    if (idMap.get(id) == null) return id;
    return ""+idMap.get(id);
  }

  /*****************************************************************************
   *
   *  Statement string generation
   *
   ****************************************************************************/

  private String getSelectClause(Selection sel, int selType)
  {
    StringBuffer stmt = new StringBuffer("SELECT\n");
    if (selType == ST_STANDARD)
    {
      stmt.append(sel.getTableDef().getSelectClause(ID_TAG));
    }
    else if (selType == ST_JOIN_MAP_KEYS)
    {
      stmt.append(((JoinDef)sel.getTableDef()).getSelectMapKeyClause(ID_TAG));
    }
    else if (selType == ST_JOIN_MAP_VALUES)
    {
      stmt.append(((JoinDef)sel.getTableDef()).getSelectMapValueClause(ID_TAG));
    }
    else
    {
      throw new RuntimeException("Invalid selection type (" + selType + ")");
    }
    stmt.append("\n");
    return stmt.toString();
  }

  /**
   * Generates SQL from clause for the selection object.
   */
  private String getFromClause(Selection sel)
  {
    StringBuffer clause = new StringBuffer("FROM\n");
    clause.append(sel.getTableDef().getFromClause(ID_TAG));

    // sub selection subqueries
    for (Iterator i = sel.getSubSels().iterator(); i.hasNext();)
    {
      Selection subSel = (Selection)i.next();
      clause.append(",\n( ");
      clause.append(getSelectInnerStatementStr(subSel,ST_STANDARD));
      clause.append(" ) ss" + sel.getSubSelNum(subSel));
    }

    return clause.toString();
  }

  /**
   * Generate SQL where clause for a selection.  Generates subclauses for any
   * subtables, indexes and filters in the selection statement prefixed 
   * by ' where '.  For use with selections (will link in subtables)
   */
  private String getWhereClause(Selection sel)
  {
    String clause = getWhereSubclause(sel,ID_TAG);
    if (clause.length() > 0)
    {
      return "\nWHERE\n" + clause;
    }
    return "";
  }

  /**
   * Generate SQL order by clause for a selection.  Generates subclause
   * looking something like: 
   *
   *   ' order by qual_col_name [ desc ] [, qual_col_name2 ] ... '
   */
  private String getOrderClause(Selection sel, int selType)
  {
    StringBuffer buf = new StringBuffer();
    Order[] orders = null;
    boolean isJoin = (selType != ST_STANDARD);
    if (selType == ST_JOIN_MAP_VALUES)
    {
      orders = sel.getMapValueOrderArray();
    }
    else
    {
      orders = sel.getOrderArray();
    }
    if (orders != null)
    {
      for (int i = 0; i < orders.length; ++i)
      {
        if (buf.length() > 0) buf.append(",\n");
        buf.append(ID_TAG + orders[i].getOrderAlias());
        //buf.append((isJoin ? "" : ID_TAG) + orders[i].getOrderAlias());
        if (orders[i].isDescending()) buf.append(" DESC");
      }
      buf.insert(0,"\nORDER BY\n");
    }
    return buf.toString();
  }

  private String getSelectInnerStatementStr(Selection sel, int selType)
  {
    return  getSelectClause(sel,selType) +
            getFromClause(sel) +
            getWhereClause(sel) +
            getOrderClause(sel,selType);
  }

  private String getSelectPageStatementStr(Selection sel,int selType)
  {
    String innerStmt = getSelectInnerStatementStr(sel,selType);
    String stmt = "SELECT * FROM ( SELECT rownum r_num, r.* FROM (\n" 
      + innerStmt + "\n) r ) WHERE r_num between "
      + sel.getStartRowNum() + " and " + sel.getEndRowNum();
    return stmt;
  }

  /**
   * Generate select statement to count rows a normal select would return.
   */
  private String getSelectCountStatementStr(Selection sel, int selType)
  {
    String innerStmt = getSelectInnerStatementStr(sel,selType);
    String stmt = " SELECT count(*) r_count FROM (\n" + innerStmt + " )";
    return stmt;
  }

  protected String createStatement()
  {
    String stmt = null;
    if (countFlag)
    {
      stmt = getSelectCountStatementStr(sel, selType);
    }
    else if (sel.getPageSize() > 0)
    {
      stmt = getSelectPageStatementStr(sel, selType);
    }
    else
    {
      stmt = getSelectInnerStatementStr(sel, selType);
    }
    log.debug("raw select:\n" + stmt);
    stmt = replaceIds(stmt);
    log.debug("id'd select:\n" + stmt);
    return stmt;
  }

  protected void setMarks()
  {
    int mark = 1;
    for (Iterator i = sel.getSubSels().iterator(); i.hasNext();)
    {
      Selection subSel = (Selection)i.next();
      mark = setWhereMarks(subSel,mark);
    }
    setWhereMarks(sel,mark);
  }


  protected boolean updateSupported()
  {
    return false;
  }

  protected boolean querySupported()
  {
    return true;
  }

  /*****************************************************************************
   *
   *  Access to result data
   *
   ****************************************************************************/

  public boolean next()
  {
    try
    {
      return rs.next();
    }
    catch (Exception e)
    {
      log.error("Error in executable next: " + e);
      throw new RuntimeException(e);
    }
  }

  public Object getColumnValue(ColumnDef cd, String qualifier)
  {
    DataType dt = cd.getDataType();
    String alias = null;
    String tranQualifier = null;
    if (cd instanceof MesSubqueryColumnDef)
    {
      qualifier = cd.getSubQualifier(qualifier);
      tranQualifier = translateId(qualifier);
      alias = tranQualifier + "_" + cd.getName();
    }
    else
    {
      tranQualifier = translateId(qualifier);
      alias = cd.getColumnAlias(tranQualifier);
    }
    Object value = null;

    try
    {

      // fetch the column value for the column using 
      // the correct getter type based on the column's data type
      if (dt.isString())
      {
        value = rs.getString(alias);
      }
      else if (dt.isInt())
      {
        value = rs.getInt(alias);
      }
      else if (dt.isLong())
      {
        value = rs.getLong(alias);
      }
      else if (dt.isDouble())
      {
        value = rs.getDouble(alias);
      }
      else if (dt.isFloat())
      {
        value = rs.getFloat(alias);
      }
      else if (dt.isTime())
      {
        value = rs.getTime(alias);
      }
      else if (dt.isDate())
      {
        value = rs.getDate(alias);
      }
      else if (dt.isTimestamp())
      {
        value = rs.getTimestamp(alias);
      }
      else
      {
        throw new RuntimeException("Unknown data type in column def " + cd);
      }
    }
    catch (SQLException sqle)
    {
      log.error("Unable to get value for column " + cd + " with data type "
        + dt + " and alias " + alias);
      log.error("qualifier = " + qualifier);
      log.error("tranQualifier = " + tranQualifier);
      throw new RuntimeException(sqle);
    }

    return value;
  }

  public void loadRecord(Record record, String qualifier)
  {
    for (Iterator i = record.getColumnSet().getColumns().iterator(); i.hasNext();)
    {
      Column column = (Column)i.next();
      Object value = getColumnValue(column.getColumnDef(),qualifier);
      try
      {
        if (rs.wasNull())
        {
          column.setNull();
        }
        else
        {
          column.set(value);
        }
      }
      catch (Exception e)
      {
        log.error("Error checking for null value in result set: " + e);
        throw new RuntimeException(e);
      }
    }
    record.setPersisted();
  }
}