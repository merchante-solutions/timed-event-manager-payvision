package com.mes.persist.mes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.persist.ColumnDef;
import com.mes.persist.ColumnDefSet;
import com.mes.persist.DataType;
import com.mes.persist.Index;
import com.mes.persist.IndexDef;
import com.mes.persist.JoinDef;
import com.mes.persist.Record;
import com.mes.persist.SubqueryDef;
import com.mes.persist.SubtableDef;
import com.mes.persist.TableDef;

public class MesJoinDef implements JoinDef
{
  static Logger log = Logger.getLogger(MesJoinDef.class);

  private String joinName;
  private List joins = new ArrayList();
  private TableDef sourceTd;
  private TableDef targetTd;
  private ColumnDef keyCd;

  public class Join
  {
    TableDef leftTd, rightTd;
    ColumnDef leftCd, rightCd;
    Object joinType;

    public Join(TableDef leftTd, String leftName, TableDef rightTd, 
      String rightName, Object joinType)
    {
      if (!ONE_TO_ONE.equals(joinType) && !ONE_TO_MANY.equals(joinType))
      {
        throw new RuntimeException("Invalid join type (" + joinType + ")");
      }
      this.leftTd = leftTd;
      this.rightTd = rightTd;
      this.joinType = joinType;

      leftCd = leftTd.getColumnDef(leftName);
      leftCd.getName();

      rightCd = rightTd.getColumnDef(rightName);
      rightCd.getName();
    }

/*
    public String getRightFromClause(int joinNum)
    {
      return rightTd.getName() + " " + getJoinQualifier(joinNum + 1);
    }
*/
    public String getRightFromClause(String qualifier, int joinNum)
    {
      if (rightTd == targetTd)
      {
        return rightTd.getFromClause(qualifier);
      }
      return rightTd.getName() + " " + qualifier + getJoinQualifier(joinNum + 1);
    }

    public String getLeftFromClause(String qualifier, int joinNum)
    {
      return leftTd.getName() + " " + qualifier + getJoinQualifier(joinNum);
    }

    public String toString()
    {
      return leftTd.getName() + ":" + leftCd.getName() + "-"
        + rightTd.getName() + ":" + rightCd.getName();
    }

/*
    public String getWhereClause(int joinNum)
    {
      return leftCd.getQualifiedName(getJoinQualifier(joinNum))
        + " = " + rightCd.getQualifiedName(getJoinQualifier(joinNum + 1));
    }
*/
    public String getWhereClause(String qualifier, int joinNum, 
      boolean parentOptional)
    {
      StringBuffer clause = new StringBuffer();

      // qualified left hand of where clause
      clause.append(leftCd.getQualifiedName(qualifier 
        + getJoinQualifier(joinNum)) + " = ");

      // qualified right hand of where clause (left joined to right)
      String compQualifier = null;
      if (rightTd == targetTd)
      {
        compQualifier = rightTd.getQualifier(qualifier);
      }
      else
      {
        compQualifier = qualifier + getJoinQualifier(joinNum + 1);
      }
      clause.append(rightCd.getQualifiedName(compQualifier));
      clause.append(parentOptional ? "(+)" : "");

      // additional right sub clauses if right is target td
      if (rightTd == targetTd)
      {
        String targWhere = rightTd.getWhereClause(qualifier,parentOptional);
        if (targWhere != null && targWhere.length() > 0)
        {
          clause.append(" and " + targWhere);
        }
      }

      return clause.toString();
    }
  }

  public MesJoinDef(String joinName, TableDef leftTd, String leftName,
    TableDef rightTd, String rightName, Object joinType)
  {
    this.joinName = joinName;
    addJoin(leftTd,leftName,rightTd,rightName,joinType);
  }

  public boolean isOneToMany()
  {
    for (Iterator i = joins.iterator(); i.hasNext();)
    {
      Join join = (Join)i.next();
      if (join.joinType.equals(ONE_TO_MANY))
      {
        return true;
      }
    }
    return false;
  }

  public String getJoinName()
  {
    return joinName;
  }

  /**
   * Returns the table def from the joined table defs matching the given table name.
   */
  public TableDef getJoinTableDef(String tblName)
  {
    if (tblName.equals(sourceTd.getName()))
    {
      return sourceTd;
    }
    for (int i = 0; i < joins.size(); ++i)
    {
      Join join = (Join)joins.get(i);
      if (join.rightTd.getName().equals(tblName))
      {
        return join.rightTd;
      }
    }
    return null;
  }

  public String getJoinQualifier(int joinNum)
  {
    // allow values one greater than the joins list size
    // (last is needed to provide qualifier for right hand
    // table in last join)
    if (joinNum < 1 || joinNum > (joins.size() + 1))
    {
      throw new RuntimeException("Join number " + joinNum + " out of range");
    }
    if (joinNum == joins.size() + 1)
    {
      return targetTd.getQualifier();
    }
    return "j" + joinNum;
  }

  /**
   * Scans the joined table defs and looks for a match with the given table def.
   * If found the qualifier corresponding with the table def is given.  This is
   * useful for records wanting to generate an index for a joined table.
   */
  public String getJoinQualifier(TableDef td)
  {
    if (td == targetTd)
    {
      return targetTd.getQualifier();
    }
    if (td.getName().equals(sourceTd.getName()))
    {
      return getJoinQualifier(1);
    }
    for (int i = 0; i < joins.size(); ++i)
    {
      Join join = (Join)joins.get(i);
      if (join.rightTd.getName().equals(td.getName()))
      {
        return getJoinQualifier(i + 2);
      }
    }
    return null;
  }

  private void addJoin(TableDef leftTd, String leftName, TableDef rightTd,
    String rightName, Object joinType)
  {
    joins.add(new Join(leftTd,leftName,rightTd,rightName,joinType));
    if (sourceTd == null)
    {
      sourceTd = leftTd;
      IndexDef pkd = sourceTd.getPrimaryKeyDef();
      if (pkd != null)
      {
        // assuming the primary key is a single column def...
        keyCd = (ColumnDef)pkd.getColumnDefSet().getColumnDefs().get(0);
      }
    }
    targetTd = rightTd;
  }

  private TableDef getTargetTd()
  {
    return targetTd;
  }

  public void addJoin(String leftName, TableDef rightTd, String rightName,
    Object joinType)
  {
    addJoin(targetTd,leftName,rightTd,rightName,joinType);
  }

  public void setQualifier(String joinQualifier)
  {
    throw new RuntimeException("under construction");
  }

  public String getQualifier()
  {
    return getJoinQualifier(joins.size() + 1);
  }
  public String getQualifier(String prefix)
  {
    return (prefix == null ? "" : prefix) + getQualifier();
  }


  public String getQualifiedName()
  {
    return getName() + " " + getQualifier();
  }

  public String getQualifiedKeyName()
  {
    return keyCd.getQualifiedName(getJoinQualifier(1));
  }
  public String getAliasedKeyName()
  {
    return keyCd.getColumnAlias(getJoinQualifier(1));
  }

  public ColumnDef getKeyColumnDef()
  {
    return keyCd;
  }

  /**
   * Join mapping handlers
   *
   * TODO: fix this to use new targetTd usage...
   */

  public String getMapKeyCdSelectClause(String qualifier)
  {
    return keyCd.getSelectClause(qualifier + getJoinQualifier(1));
  }
  public String getMapKeyCdSelectClause()
  {
    return getMapKeyCdSelectClause("");
  }

/*
  public String getSelectClause()
  {
    ColumnDefSet lastCds = targetTd.getColumnDefSet();
    return getMapKeyCdSelectClause() + ", "
      + lastCds.getSelectClause(getQualifier());
  }
*/
  public String getSelectClause()
  {
    return getMapKeyCdSelectClause() + ",\n" + targetTd.getSelectClause();
  }
  public String getSelectClause(String qualifier)
  {
    return getMapKeyCdSelectClause(qualifier) + ",\n" + targetTd.getSelectClause(qualifier);
  }

  public String getSelectMapKeyClause(String qualifier)
  {
    ColumnDefSet keyCds = sourceTd.getColumnDefSet();
    return keyCds.getSelectClause(qualifier + getJoinQualifier(1));
  }
  public String getSelectMapKeyClause()
  {
    return getSelectMapKeyClause("");
  }

  // TODO: joins are currently broken for subtable loading
  public String getSelectMapValueClause(String qualifier)
  {
    return getSelectClause(qualifier);
  }
  public String getSelectMapValueClause()
  {
    return getSelectMapValueClause("");
  }

  public String getFromClause(String qualifier)
  {
    StringBuffer buf = new StringBuffer();
    Join firstJoin = (Join)joins.get(0);
    buf.append(firstJoin.getLeftFromClause(qualifier,1));
    int joinCount = 1;
    for (Iterator i = joins.iterator(); i.hasNext();)
    {
      buf.append(",\n");
      Join join = (Join)i.next();
      buf.append(join.getRightFromClause(qualifier,joinCount++));
    }
    return buf.toString();
  }
  public String getFromClause()
  {
    return getFromClause("");
  }

  public String getWhereClause(String qualifier)
  {
    return getWhereClause(qualifier,false);
  }
  public String getWhereClause(String qualifier, boolean parentOptional)
  {
    StringBuffer buf = new StringBuffer();
    int joinCount = 1;
    for (Iterator i = joins.iterator(); i.hasNext();)
    {
      Join join = (Join)i.next();
      if (buf.length() > 0) buf.append(" and\n");
      buf.append(join.getWhereClause(qualifier,joinCount++,parentOptional));
    }
    return buf.toString();
  }
  public String getWhereClause()
  {
    return getWhereClause("");
  }

  /**
   * Generates a subquery suitable for use in a delete statement.
   */
  public String getDeleteSubquery()
  {
    StringBuffer buf = new StringBuffer();

    int joinSize = joins.size();
    Join trgJoin = (Join)joins.get(joinSize - 1);
    String joinIdName
      = trgJoin.rightCd.getQualifiedName(targetTd.getQualifier(null));
    //  = trgJoin.rightCd.getQualifiedName(getJoinQualifier(joinSize + 1));

    // select jtrg.join_id
    buf.append("select " + joinIdName);

    // from   jtrg, jsrc, ...
    buf.append(" from " + getFromClause());

    // where  jtrg.join_id = ... jsrc.join_id
    buf.append(" where " + getWhereClause());

    return buf.toString();
  }

  public String getTargetJoinIdName()
  {
    Join trgJoin = (Join)joins.get(joins.size() - 1);
    return trgJoin.rightCd.getName();
  }

  /**
   * returns 'delete from jtrg_name 
   *          where join_id in ( select jtrg.join_id
   *                             from   jtrg, jsrc
   *                             where  jtrg.join_id .. = jsrc.join_id
   *                                    [ and jsrc.key_id = ? ] ) '
   */
  public String getDeleteStatement(Index index)
  {
    StringBuffer buf = new StringBuffer();

    // 'delete from jtrg_table_name'
    int joinNum = joins.size();
    Join trgJoin = (Join)joins.get(joinNum - 1);
    buf.append("delete from " + trgJoin.rightTd.getName());

    // 'where join_id in ( '
    buf.append(" where " + trgJoin.rightCd.getName() + " in ( ");

    // subquery
    buf.append(getDeleteSubquery());

    // add index where clause if present
    if (index != null)
    {
      buf.append(" and " + index.getWhereClause());
    }

    // close subquery
    buf.append(" )");

    return buf.toString();
  }

  public TableDef getFirstTableDef()
  {
    return sourceTd;
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    for (Iterator i = joins.iterator(); i.hasNext();)
    {
      if (buf.length() > 0) buf.append(" ");
      buf.append(i.next().toString());
    }
    return "JoinDef [ " + buf + " ]";
  }

  private void matchRecordToTable(TableDef td, Record record)
  {
    if (!record.getName().equals(td.getName()))
    {
      throw new RuntimeException("Record (" + record.getName()
        + ") does not match join table def (" + td.getName() + ")");
    }

  }
  public Index getPrimaryKey(Record record)
  {
    matchRecordToTable(sourceTd,record);
    return record.getPrimaryKey(getJoinQualifier(1));
  }

  public Index getLoadedKey(Record record)
  {
    matchRecordToTable(sourceTd,record);
    return record.getLoadedKey(getJoinQualifier(1));
  }

  public Index getIndex(Record record, String indexName)
  {
    matchRecordToTable(sourceTd,record);
    return record.getIndex(indexName,getJoinQualifier(1));
  }


  // wrapper methods...

  public String getName()
  {
    return targetTd.getName();
  }

  public Class getBeanClass()
  {
    return targetTd.getBeanClass();
  }

  public void addColumnDef(ColumnDef cDef)
  {
    throw new RuntimeException("Cannot add column def to join");
  }
    
  public void addColumnDef(String colName,DataType dataType)
  {
    throw new RuntimeException("Cannot add column def to join");
  }

  public ColumnDef getColumnDef(String name)
  {
    return targetTd.getColumnDef(name);
  }

  public ColumnDefSet getColumnDefSet()
  {
    return targetTd.getColumnDefSet();
  }
  
  public IndexDef addIndexDef(String name, boolean isUnique)
  {
    throw new RuntimeException("Cannot add index def to join");
  }

  public IndexDef getIndexDef(String name)
  {
    IndexDef idxDef = null;
    idxDef = sourceTd.getIndexDef(name);
    if (idxDef != null) return idxDef;
    for (Iterator joinIter = joins.iterator(); joinIter.hasNext() && idxDef == null;)
    {
      Join join = (Join)joinIter.next();
      idxDef = join.leftTd.getIndexDef(name);
    }
    return idxDef;
  }

  public List getIndexDefs()
  {
    return targetTd.getIndexDefs();
  }

  public IndexDef getPrimaryKeyDef()
  {
    return targetTd.getPrimaryKeyDef();
  }


  public JoinDef addJoinDef(String joinName, String colName, TableDef joinTd,
    String joinColName, Object joinType)
  {
    throw new RuntimeException("Cannot add join def to join");
  }

  public JoinDef getJoinDef(String joinName)
  {
    return targetTd.getJoinDef(joinName);
  }

  public List getJoinDefs()
  {
    return targetTd.getJoinDefs();
  }

  public void addSubtableDef(String colName, TableDef td, String subIdxName, 
    Object joinType, Object tableType)
  {
    throw new RuntimeException("Cannot add subtable def to join");
  }
  public void addSubtableDef(String colName, TableDef td, String subIdxName, 
    Object joinType, Object tableType, String subAccName, String subTblName)
  {
    throw new RuntimeException("Cannot add subtable def to join");
  }


  public SubtableDef getSubtableDef(String subName)
  {
    return targetTd.getSubtableDef(subName);
  }

  public List getSubtableDefs()
  {
    return targetTd.getSubtableDefs();
  }

  public String getInsertColumnList()
  {
    return targetTd.getInsertColumnList();
  }
  public String getInsertValueMarks()
  {
    return targetTd.getInsertValueMarks();
  }
  public String getUpdateSetList()
  {
    return targetTd.getUpdateSetList();
  }


  public String getAliasedColumnName(String colName, String prefix)
  {
    throw new RuntimeException("Cannot create alias name with"
      + " prefix in join def");
  }
  public String getAliasedColumnName(String colName)
  {
    if (colName.equals(keyCd.getName()))
    {
      return getAliasedKeyName();
    }
    return targetTd
            .getColumnDefSet()
              .getColumnDef(colName)
                .getColumnAlias(getQualifier());
  }

  public String getQualifiedColumnName(String colName, String prefix)
  {
    throw new RuntimeException("Cannot create qualified name with"
      + " prefix in join def");
  }
  public String getQualifiedColumnName(String colName)
  {
    if (colName.equals(keyCd.getName()))
    {
      return getQualifiedKeyName();
    }
    return targetTd
            .getColumnDefSet()
              .getColumnDef(colName)
                .getQualifiedName(getQualifier());
  }

  public List getSubqueryDefs()
  {
    return targetTd.getSubqueryDefs();
  }

  public void addSubqueryDef(SubqueryDef sqDef)
  {
    targetTd.addSubqueryDef(sqDef);
  }
}
