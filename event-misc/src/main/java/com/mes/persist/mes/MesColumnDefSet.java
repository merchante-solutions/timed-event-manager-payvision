package com.mes.persist.mes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.persist.ColumnDef;
import com.mes.persist.ColumnDefSet;

public class MesColumnDefSet implements ColumnDefSet
{
  static Logger log = Logger.getLogger(MesColumnDefSet.class);
  
  private List cDefs = new ArrayList();
  private Map cDefMap = new HashMap();

  public MesColumnDefSet()
  {
  }
  public MesColumnDefSet(ColumnDefSet defSet)
  {
    addColumnDefs(defSet);
  }
  
  public List getColumnDefs()
  {
    return new ArrayList(cDefs);
  }
  
  public ColumnDef getColumnDef(String name)
  {
    return (ColumnDef)cDefMap.get(name);
  }
  
  /*
   * Add column defs...
   */
  protected void addColumnDef(ColumnDef cDef)
  {
    if (cDefMap.get(cDef.getName()) != null)
    {
      throw new RuntimeException("Duplicate column def encountered: " + cDef);
    }
    
    cDefs.add(cDef);
    cDefMap.put(cDef.getName(),cDef);
  }

  protected void addColumnDefs(List cDefList)
  {
    for (Iterator i = cDefList.iterator(); i.hasNext();)
    {
      addColumnDef((ColumnDef)i.next());
    }
  }

  protected void addColumnDefs(ColumnDefSet cDefSet)
  {
    addColumnDefs(cDefSet.getColumnDefs());
  }

  public String getSelectClause(String qualifier)
  {
    StringBuffer buf = new StringBuffer();
    for (Iterator i = cDefs.iterator(); i.hasNext();)
    {
      ColumnDef cd = (ColumnDef)i.next();
      buf.append(cd.getSelectClause(qualifier));
      if (i.hasNext()) buf.append(",\n");
    }
    return buf.toString();
  }

  public String getInsertColumnList(String qualifier)
  {
    StringBuffer buf = new StringBuffer();
    for (Iterator i = cDefs.iterator(); i.hasNext();)
    {
      ColumnDef cd = (ColumnDef)i.next();
      if (!cd.isReadOnly())
      {
        buf.append((buf.length() > 0 ? ",\n" : "") + cd.getQualifiedName(qualifier));
      }
    }
    return buf.toString();
  }

  public String getInsertValueMarks()
  {
    StringBuffer buf = new StringBuffer();
    for (Iterator i = cDefs.iterator(); i.hasNext();)
    {
      ColumnDef cd = (ColumnDef)i.next();
      if (!cd.isReadOnly())
      {
        buf.append(buf.length() > 0 ? ", ?" : "?");
      }
    }
    return buf.toString();
  }

  public String getUpdateSetList(String qualifier)
  {
    StringBuffer buf = new StringBuffer();
    for (Iterator i = cDefs.iterator(); i.hasNext();)
    {
      ColumnDef cd = (ColumnDef)i.next();
      if (!cd.isReadOnly())
      {
        if (buf.length() > 0) buf.append(",\n");
        buf.append(cd.getQualifiedName(qualifier) + " = ?");
      }
    }
    return buf.toString();
  }
}
