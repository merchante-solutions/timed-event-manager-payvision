package com.mes.persist.mes;

import java.util.Iterator;
import java.util.List;
import com.mes.persist.Column;
import com.mes.persist.ColumnSet;
import com.mes.persist.Index;
import com.mes.persist.IndexDef;
import com.mes.persist.Record;

public class MesIndex implements Index
{
  private Record record;
  private IndexDef iDef;
  private ColumnSet cs;
  private String qualifier;
  
  public MesIndex(IndexDef iDef, Record record, String qualifier)
  {
    this.iDef = iDef;
    this.record = record;
    this.qualifier = qualifier;

    cs = new MesColumnSet(iDef.getColumnDefSet(),record.getColumnSet());
  }
  public MesIndex(IndexDef iDef, Record record)
  {
    this(iDef,record,record.getTableDef().getQualifier());
  }
  
  public IndexDef getIndexDef()
  {
    return iDef;
  }
  
  public String getName()
  {
    return iDef.getName();
  }
  
  public ColumnSet getColumnSet()
  {
    return cs;
  }
  
  public List getColumns()
  {
    return cs.getColumns();
  }

  public String getQualifier(String prefix)
  {
    return (prefix == null ? "" : prefix) + qualifier;
  }
  public String getQualifier()
  {
    return getQualifier(null);
  }

  /**
   * Generate an SQL where clause fragment for the this index.  Returns a
   * string consisting of qualified column names with either an is null
   * operator or a prepared statement marker.  Each sub clause is separated
   * with an ' and '.
   */
  public String getWhereClause()
  {
    return getWhereClause(null);
  }
  public String getWhereClause(String prefix)
  {
    return getWhereClause(prefix,false);
  }
  public String getWhereClause(String prefix, boolean parentOptional)
  {
    StringBuffer buf = new StringBuffer();
    String qualifier = getQualifier(prefix);
    for (Iterator i = cs.getColumns().iterator(); i.hasNext();)
    {
      if (buf.length() > 0)
      {
        buf.append(" and\n");
      }
      Column column = (Column)i.next();
      buf.append(column.getQualifiedName(qualifier));
      if (column.isNull())
      {
        buf.append(" is null");
      }
      else
      {
        buf.append(" = ?");
      }
    }
    return buf.toString();
  }
  
  /**
   * Generate a string descriptor of the contents of the data object suitable
   * for audit log records.  Calls each fields getAuditLog() method.
   */
  public String getAuditLog()
  {
    StringBuffer sb = new StringBuffer("Idx: ");
    boolean isFirst = true;
    for (Iterator i = cs.getColumns().iterator(); i.hasNext();)
    {
      Column col = (Column)i.next();
      sb.append((!isFirst ? ", " : "") + col.getAuditLog());
      isFirst = false;
    }
    sb.append((isFirst ? "(no columns)" : ""));
    return sb.toString();
  }
   
  /**
   * Scans the columns in the index column set.  If any are non-null the index
   * is considered loaded.  Returns true if found to be loaded.
   */
  public boolean isLoaded()
  {
    for (Iterator i = cs.getColumns().iterator(); i.hasNext();)
    {
      Column column = (Column)i.next();
      if (!column.isNull())
      {
        return true;
      }
    }
    return false;
  }
  
  public boolean isUnique()
  {
    return iDef.isUnique();
  }
  
  public boolean isKey()
  {
    return isUnique();
  }
  
  public String toString()
  {
    return "Index " + getName() + " [ " + cs.toString() + " ]";
  }
}