package com.mes.persist.mes;

import com.mes.persist.Db;
import com.mes.persist.DbFactory;
import com.mes.persist.Schema;

public class MesDbFactory implements DbFactory
{
  private Schema schema;
  
  public MesDbFactory(Schema schema)
  {
    this.schema = schema;
  }
  
  public Db getDb()
  {
    return new MesDb(schema);
  }
}
