package com.mes.persist.mes;

import org.apache.log4j.Logger;
import com.mes.persist.Record;
import com.mes.persist.TableDef;

public class MesInsert extends MesExecutable
{
  static Logger log = Logger.getLogger(MesInsert.class);

  public MesInsert(Record record)
  {
    super(record,ET_UPDATE);
  }

  protected String createStatement()
  {
    TableDef td = record.getTableDef();
    return "INSERT INTO\n" + td.getQualifiedName() + "\n"
         + "( " + td.getInsertColumnList() + " )\n"
         + "VALUES\n( " + td.getInsertValueMarks() + " )";
  }

  protected void setMarks()
  {
    setColumnMarks();
  }
}