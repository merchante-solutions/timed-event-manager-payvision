package com.mes.persist.mes;

import com.mes.persist.Order;
import com.mes.persist.TableDef;

public class MesOrder implements Order
{
  private String colName;
  private String qualifier;
  private boolean descFlag;
  private TableDef td;

  public MesOrder(String colName, TableDef td, String qualifier, boolean descFlag)
  {
    this.qualifier = qualifier;
    this.colName = colName;
    this.td = td;
    this.descFlag = descFlag;
  }

  public MesOrder(String colName, TableDef td, boolean descFlag)
  {
    this(colName,td,td.getQualifier(),descFlag);
  }

  public String getColName()
  {
    return colName;
  }

  public String getQualifier()
  {
    return qualifier;
  }

  public String getOrderAlias()
  {
    return td.getColumnDef(colName).getColumnAlias(qualifier);
  }

  public boolean isDescending()
  {
    return descFlag;
  }
}

