package com.mes.persist.mes;

import org.apache.log4j.Logger;
import com.mes.persist.DataType;
import com.mes.persist.SubqueryDef;

public class MesSubqueryColumnDef extends MesColumnDef
{
  static Logger log = Logger.getLogger(MesSubqueryColumnDef.class);

  private String sqExpr;
  private SubqueryDef sqDef;
  
  public MesSubqueryColumnDef(SubqueryDef sqDef, String name, DataType dt, 
    String wrapperExpr, String sqExpr)
  {
    super(name,dt,wrapperExpr);
    this.sqDef = sqDef;
    this.sqExpr = sqExpr;
  }

  public String getSubQualifier(String qualifier)
  {
    return qualifier + sqDef.getName() + MesSubqueryDef.ID_TERMINATOR;
  }

  public String getColumnAlias(String qualifier)
  {
    return super.getColumnAlias(qualifier + sqDef.getName() + MesSubqueryDef.ID_TERMINATOR);
  }
  public String getQualifiedName(String qualifier)
  {
    return super.getQualifiedName(qualifier + sqDef.getName() + MesSubqueryDef.ID_TERMINATOR);
  }

  public String getSubquerySelectClause()
  {
    if (sqExpr != null)
    {
      return sqExpr + " " + getName();
    }
    return super.getSubquerySelectClause();
  }

  public String toString()
  {
    String name = getName();
    DataType dt = getDataType();
    String wrapperExpr = getWrapperExpr();
    return "MesSubqueryColumnDef [ " + name + " " + dt 
      + (wrapperExpr != null ? " " + wrapperExpr : "")
      + (sqExpr != null ? " " + sqExpr : "") + " ]";
  }

  public boolean isReadOnly()
  {
    return true;
  }
}