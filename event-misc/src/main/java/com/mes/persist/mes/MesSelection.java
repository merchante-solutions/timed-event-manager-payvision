package com.mes.persist.mes;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.persist.Column;
import com.mes.persist.CompoundFilter;
import com.mes.persist.Filter;
import com.mes.persist.FilterWrapper;
import com.mes.persist.Index;
import com.mes.persist.IndexDef;
import com.mes.persist.JoinDef;
import com.mes.persist.Order;
import com.mes.persist.Record;
import com.mes.persist.Schema;
import com.mes.persist.Selection;
import com.mes.persist.TableDef;

public class MesSelection implements Selection
{
  static Logger log = Logger.getLogger(MesSelection.class);

  private Schema schema;
  private Class beanClass;
  private Object bean;
  private TableDef td;
  private TableDef mapTd;
  private boolean joinFlag;
  private List indexes = new ArrayList();
  private List filters = new ArrayList();
  private List orders = new ArrayList();
  private List mapValueOrders = new ArrayList();
  private int pageSize;
  private int pageNum = 1;
  private boolean countFlag;

  /**
   * Internal table reference class used to locate a table along with
   * an accompanying qualifier.
   */
  class TableRef
  {
    public TableDef td;
    public String qualifier;

    public TableRef (TableDef td, String qualifier)
    {
      this.td = td;
      this.qualifier = qualifier;
    }

    public String toString()
    {
      return "TableRef [ td: " + td.getName() + ", qualifier: " 
                + qualifier + " ]";
    }
  }

  public MesSelection(Schema schema, Class beanClass)
  {
    this.schema = schema;
    this.beanClass = beanClass;
    td = schema.getTableDef(beanClass);
    if (td == null)
    {
      throw new NullPointerException("Bean class " + beanClass.getName()
        + " not found in schema");
    }
  }

  public MesSelection(Schema schema, Object bean, boolean autoIdx)
  {
    this.schema = schema;
    this.beanClass = bean.getClass();
    this.bean = bean;
    td = schema.getTableDef(beanClass);
    if (td == null)
    {
      throw new NullPointerException("Bean class " + beanClass.getName()
        + " not found in schema");
    }

    // look for a primary if auto index is requested
    if (autoIdx)
    {
      // automatically add primary key if it exists
      IndexDef keyDef = td.getPrimaryKeyDef();
      if (keyDef != null)
      {
        addIndex(keyDef.getName());
      }
    }
  }

  public MesSelection(Schema schema, String joinName)
  {
    this.schema = schema;

    JoinDef jd = null;
    for (Iterator i = schema.getTableDefs().iterator(); i.hasNext() && jd == null;)
    {
      TableDef tblDef = (TableDef)i.next();
      jd = tblDef.getJoinDef(joinName);
    }
    if (jd == null)
    {
      throw new NullPointerException("Invalid join name " + joinName);
    }
    beanClass = jd.getBeanClass();
    td = jd;
    mapTd = jd.getFirstTableDef();
    joinFlag = true;
  }

  /**
   * This constructor allows a select to be generated from a record.  This is for
   * the somewhat special cases where the db wants to determine if a record already
   * exists in a table in order to know if an update or insert is required to 
   * persist a record or if a record exists prior to a delete attempt...
   */
  public MesSelection(Schema schema, Record record)
  {
    td = record.getTableDef();
    beanClass = td.getBeanClass();

    // record needs a key (unique) index, which is manually added here...
    Index keyIdx = record.getPrimaryKey();
    if (keyIdx == null)
    {
      throw new RuntimeException("Unable to perform select, record has no"
        + " primary key: " + record);
    }
    indexes.add(keyIdx);
  }

  public boolean hasBean()
  {
    return bean != null;
  }

  public boolean hasIndexes()
  {
    return !indexes.isEmpty();
  }

  public boolean hasFilters()
  {
    return !filters.isEmpty();
  }

  public boolean isJoin()
  {
    return joinFlag;
  }

  public Record getRecordForBean(Object bean)
  {
    // get the bean's table def, generate a record with it
    TableDef beanTd = schema.getTableDef(bean.getClass());
    if (beanTd == null)
    {
      throw new NullPointerException("No table def found for class "
        + beanClass.getName());
    }
    Record record = new MesRecord(beanTd);

    // load the bean data into the record
    for (Iterator i = record.getColumns().iterator(); i.hasNext();)
    {
      // get the bean getter method to invoke
      Column column = (Column)i.next();
      Method method = column.getColumnDef().getGetterMethod();
      
      // set the column value to the value returned by the bean getter method
      try
      {
        Object value = method.invoke(bean,null);
        if (value == null)
        {
          column.setNull();
        }
        else
        {
          column.set(value);
        }
      }
      catch (Exception e)
      {
        throw new RuntimeException("Failed to set value in column "
          + column.getName() + ": " + e);
      }
    }
    
    return record;
  }

  private boolean hasIndex(String idxName)
  {
    for (Iterator i = indexes.iterator(); i.hasNext();)
    {
      Index idx = (Index)i.next();
      if (idx.getName().equals(idxName))
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Recursive routine to traverse subtables within a table.  Locates a subtable
   * with name (accessor based or table) in a table def.  A subtable qualifier is 
   * built appropriate to the subtable.  Returns a TableRef object containing 
   * the subtable def and the qualifier.  Returns null if no subtable is found.
   */
  private TableRef getTableRef(TableDef curTd, String refName, String prefix)
  {
    if (curTd.getName().equals(refName))
    {
      return new TableRef(curTd,prefix);
    }
    for (Iterator i = curTd.getSubtableDefs().iterator(); i.hasNext();)
    {
      TableDef innerTd = (TableDef)i.next();
      TableRef tr = getTableRef(innerTd,refName,innerTd.getQualifier(prefix));
      if (tr != null)
      {
        return tr;
      }
    }
    return null;
  }

  // entry point
  private TableRef getTableRef(String refName)
  {
    return getTableRef(td,refName,td.getQualifier());
  }

  private TableRef getTableRefByCol(TableDef curTd, String colName, String prefix)
  {
    if (curTd.getColumnDef(colName) != null)
    {
      return new TableRef(curTd,prefix);
    }
    for (Iterator i = curTd.getSubtableDefs().iterator(); i.hasNext();)
    {
      TableDef innerTd = (TableDef)i.next();
      TableRef tr = getTableRefByCol(innerTd,colName,innerTd.getQualifier(prefix));
      if (tr != null)
      {
        return tr;
      }
    }
    return null;
  }

  // entry point
  private TableRef getTableRefByCol(String colName)
  {
    TableRef tr = getTableRefByCol(td,colName,td.getQualifier());
    return tr;
  }

  /**
   * Generates an index with the given name loaded with the selection bean data.
   */
  public void addIndex(String idxName)
  {
    // is this a join selection?
    if (joinFlag)
    {
      throw new RuntimeException("Cannot add join indexes without index bean");
    }

    // ignore attempts to add indexes that are already present
    if (hasIndex(idxName))
    {
      return;
    }

    // create the index using the default selection bean data 
    Index idx = getRecordForBean(bean).getIndex(idxName);
    if (idx == null)
    {
      throw new NullPointerException("Unable to generate index " + idxName
        + " for table " + td.getName());
    }
    indexes.add(idx);
  }

  /**
   * Generates an index with the given name loaded with the given bean.
   */
  public void addSubIndex(String idxName, String subName, Object idxBean)
  {
    // is this a join selection?
    if (joinFlag)
    {
      throw new RuntimeException(
        "Subtable indexes not supported with join selects");
    }

    // ignore attempts to add indexes that are already present
    if (hasIndex(idxName))
    {
      return;
    }

    // create the index using the default selection bean data 
    TableRef tr = getTableRef(subName);
    if (tr == null)
    {
      throw new RuntimeException("Subtable " + subName + " not found in "
        + "table " + td.getName());
    }
    Index idx = getRecordForBean(idxBean).getIndex(idxName,tr.qualifier);
    if (idx == null)
    {
      throw new NullPointerException("Unable to generate index " + idxName
        + " for subtable " + tr.td.getName() + " in table " + td.getName());
    }
    indexes.add(idx);
  }

  /**
   * Index adder for joins.  This takes a bean object in addition to idx name.
   * The bean is used to determine the join table def and join qualifier as well
   * as a container for the index column data.
   */
  public void addJoinIndex(String idxName, Object idxBean)
  {
    JoinDef jd = (JoinDef)td;
    if (!joinFlag)
    {
      throw new RuntimeException("Index bean may only be used with joins");
    }

    // ignore attempts to add indexes that are already present
    if (hasIndex(idxName))
    {
      return;
    }

    Record record = getRecordForBean(idxBean);
    String joinQual = jd.getJoinQualifier(record.getTableDef());
    if (joinQual == null)
    {
      throw new NullPointerException("Index " + idxName + " is not valid with"
        + " join " + td.getName());
    }

    Index idx = record.getIndex(idxName,joinQual);
    if (idx == null)
    {
      throw new NullPointerException("Unable to generate index " + idxName
        + " for join " + td.getName());
    }
    indexes.add(idx);
  }

  /**
   * Given a filter and an optional table name, need to set the table def and
   * qualifier string in the filter.  If ref name is given, look for table def
   * and qualifier with it.  If not, look for table def and qualifier based on
   * the column name in the filter.  If the filter is compound, recurse through
   * contained filters and link them as well.
   */
  private void linkFilter(Filter filter, String refName)
  {
    if (filter instanceof CompoundFilter)
    {
      for (Iterator i = ((CompoundFilter)filter).getFilters().iterator(); 
            i.hasNext();)
      {
        linkFilter((Filter)i.next(),null);
      }
    }
    else
    {
      TableRef tr = null;
      if (refName != null)
      {
        tr = getTableRef(refName);
      }
      else
      {
        tr = getTableRefByCol(filter.getColName());
      }
      filter.setTableDef(tr.td);
      filter.setQualifier(tr.qualifier);
    }
  }

  public void addFilter(Filter filter, String refName)
  {
    if (joinFlag)
    {
      throw new RuntimeException("Cannot add non-join filter to join selection,"
        + " use addJoinFilter() instead");
    }

    linkFilter(filter,refName);
    filters.add(filter);
  }
  public void addFilter(Filter filter)
  {
    addFilter(filter,null);
  }

  public void addJoinFilter(Filter filter, String tblName)
  {
    if (!joinFlag)
    {
      throw new RuntimeException("Cannot add join filter to non-join selection,"
        + " use addFilter() instead");
    }
    JoinDef jd = (JoinDef)td;
    TableDef joinTd = jd.getJoinTableDef(tblName);
    if (joinTd == null)
    {
      throw new NullPointerException("Invalid table name " + tblName
        + ", not present in join def " + jd.getJoinName());
    }
    String joinQual = jd.getJoinQualifier(joinTd);
    filters.add(new FilterWrapper(filter,joinTd,joinQual));
  }

  public void addOrder(String colName, boolean descFlag)
  {
    if (td.getColumnDef(colName) == null)
    {
      throw new NullPointerException("Column " + colName + " not found in table " 
        + td.getName());
    }
    orders.add(new MesOrder(colName,td,descFlag));
  }

  public void addSubtableOrder(String tblName, String colName, boolean descFlag)
  {
    TableRef tr = getTableRef(tblName);
    if (tr == null)
    {
      throw new NullPointerException("Cannot find subtable " + tblName);
    }
    TableDef foundTd = tr.td;
    if (foundTd.getColumnDef(colName) == null)
    {
      throw new NullPointerException("Column " + colName + " not found in subtable "
        + foundTd.getName());
    }
    orders.add(new MesOrder(colName,foundTd,tr.qualifier,descFlag));
  }

  public void addJoinOrder(String tblName, String colName, boolean descFlag)
  {
    if (!joinFlag)
    {
      throw new RuntimeException("Cannot add join order to non-join selection,"
        + " use addOrder() instead");
    }
    JoinDef jd = (JoinDef)td;
    TableDef joinTd = jd.getJoinTableDef(tblName);
    if (joinTd == null)
    {
      throw new NullPointerException("Invalid table name " + tblName
        + ", not present in join def " + jd.getJoinName());
    }
    if (joinTd.getColumnDef(colName) == null)
    {
      throw new NullPointerException("Column " + colName + " not found in table " 
        + joinTd.getName());
    }
    String joinQual = jd.getJoinQualifier(joinTd);
    orders.add(new MesOrder(colName,joinTd,joinQual,descFlag));
  }

  public void addMapValueOrder(String tblName, String colName, boolean descFlag)
  {
    if (!joinFlag)
    {
      throw new RuntimeException("Cannot add join order to non-join selection,"
        + " use addOrder() instead");
    }
    JoinDef jd = (JoinDef)td;
    TableDef joinTd = jd.getJoinTableDef(tblName);
    if (joinTd == null)
    {
      throw new NullPointerException("Invalid table name " + tblName
        + ", not present in join def " + jd.getJoinName());
    }
    if (joinTd.getColumnDef(colName) == null)
    {
      throw new NullPointerException("Column " + colName + " not found in table " 
        + joinTd.getName());
    }
    String joinQual = jd.getJoinQualifier(joinTd);
    mapValueOrders.add(new MesOrder(colName,joinTd,joinQual,descFlag));
  }

  public void clearOrders()
  {
    orders = new ArrayList();
    mapValueOrders = new ArrayList();
  }

  public Class getBeanClass()
  {
    return beanClass;
  }

  public Object getBean()
  {
    return bean;
  }

  /**
   * Returns the table def corresponding with the select bean/class.  If a join
   * has been specified, the join def is actually returned.
   */
  public TableDef getTableDef()
  {
    return td;
  }

  public JoinDef getJoinDef()
  {
    return (JoinDef)td;
  }

  public TableDef getMapTableDef()
  {
    if (!joinFlag)
    {
      throw new RuntimeException("Map table def is not available in non-join"
        + " selections");
    }
    return mapTd;
  }

  public List getIndexes()
  {
    return indexes;
  }

  public Index[] getIndexArray()
  {
    if (indexes.isEmpty())
    {
      return null;
    }
    return (Index[])indexes.toArray(new Index[] {});
  }

  public List getFilters()
  {
    return filters;
  }

  public Filter[] getFilterArray()
  {
    List allFilters = getFilters();
    if (allFilters.isEmpty())
    {
      return null;
    }
    return (Filter[])allFilters.toArray(new Filter[] {});
  }

  public List getOrders()
  {
    return orders;
  }

  public Order[] getOrderArray()
  {
    if (orders.isEmpty())
    {
      return null;
    }
    return (Order[])orders.toArray(new Order[] {});
  }

  public List getMapValueOrders()
  {
    return mapValueOrders;
  }

  public Order[] getMapValueOrderArray()
  {
    if (mapValueOrders.isEmpty())
    {
      return null;
    }
    return (Order[])mapValueOrders.toArray(new Order[] {});
  }

  public Index getIndex(String idxName)
  {
    for (Iterator i = indexes.iterator(); i.hasNext();)
    {
      Index idx = (Index)i.next();
      if (idx.getName().equals(idxName)) return idx;
    }
    return null;
  }

  /**
   * Audit name is the name of the table relevant to audit log info.  In non-joins
   * this will just be the name of the table def.  In joins this will be the name
   * of the target table def.
   */
  public String getAuditName()
  {
    return td.getName();
  }

  /**
   * Generates details suitable for auditing.  Currently selections will generate
   * the where subclauses of the indexes and filters they contain.  This is suitable
   * for audit logging having to do with multiple records (only deletes right now).
   *
   * TODO: may want to make this detect when it is referencing a unique record and
   *       generate record-style audit log data when appropriate...
   */
  public String getAuditLog()
  {
    StringBuffer logData = new StringBuffer();

    // indexes
    for (Iterator i = indexes.iterator(); i.hasNext();)
    {
      Index idx = (Index)i.next();
      if (logData.length() > 0) logData.append(", ");
      logData.append(idx.getAuditLog());
    }

    // filters
    for (Iterator i = filters.iterator(); i.hasNext();)
    {
      Filter fil = (Filter)i.next();
      if (logData.length() > 0) logData.append(", ");
      logData.append(fil.getAuditLog());
    }

    return logData.toString();
  }

  /**
   * Auditables need to return a primary key for audit data generation.
   * This is currently going to return null for join selections...
   */
  public Index getPrimaryKey()
  {
    return getIndex(td.getPrimaryKeyDef().getName());
  }

  public void setPageSize(int pageSize)
  {
    this.pageSize = pageSize;
  }
  public int getPageSize()
  {
    return pageSize;
  }

  public void setPageNum(int pageNum)
  {
    this.pageNum = pageNum;
  }
  public int getPageNum()
  {
    return pageNum;
  }

  public int getStartRowNum()
  {
    return ((pageNum - 1) * pageSize) + 1;
  }

  public int getEndRowNum()
  {
    return pageNum * pageSize;
  }

  private List subSels = new ArrayList();
  private Map subSelNameMap = new HashMap();
  private Map subSelNumMap = new HashMap();

  public void addSubSel(Selection subSel, String colName)
  {
    subSels.add(subSel);
    subSelNameMap.put(subSel,colName);
    subSelNumMap.put(subSel, subSels.size());
  }

  public List getSubSels()
  {
    return subSels;
  }

  public String getSubSelColName(Selection subSel)
  {
    return (String)subSelNameMap.get(subSel);
  }

  public int getSubSelNum(Selection subSel)
  {
    Integer i = (Integer)subSelNumMap.get(subSel);
    return i.intValue();
  }
}