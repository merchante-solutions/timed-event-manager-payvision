/**
 * A servlet that initializes db objects (maps db classes to their corresponding
 * schema URL).  This servlet models the Log4jInit servlet, and it's only purpose
 * is to run the init() method once during web application server startup.  Each
 * init parameter is assumed to contain a Db class name with the URL to the Db's
 * schema as the value.  Each Db-schema pair is registered via Db static methods.
 */

package com.mes.persist;

import java.io.File;
import java.net.URL;
import java.util.Enumeration;
import javax.servlet.http.HttpServlet;
import org.apache.log4j.Logger;

public class DbInitServlet extends HttpServlet
{
  static Logger log = Logger.getLogger(DbInitServlet.class);
  
  public static final String IP_SCHEMA_FACTORY = "schema-factory";

  private File getFileForPath(String path)
  {
    String sep = path.startsWith(File.separator) ? "" : File.separator;
    String filePath = getServletContext().getRealPath("/");
    return filePath != null ? new File(filePath + sep + path) : null;
  }

  public void init()
  {
    try
    {
      Class sfClass = Class.forName(getInitParameter(IP_SCHEMA_FACTORY));
      SchemaFactory schemaFactory = (SchemaFactory)sfClass.newInstance();
      log.info("schema-factory set: " + schemaFactory.getClass().getName());
      for (Enumeration e = getInitParameterNames(); e.hasMoreElements();)
      {
        String className = (String)e.nextElement();
        if (className.equals(IP_SCHEMA_FACTORY)) continue;
        String schemaPath = getInitParameter(className);
        Class dbClass = Class.forName(className);
        URL schemaUrl = getServletContext().getResource(schemaPath);
        File schemaFile = getFileForPath(schemaPath);
        Db.registerDbSource(dbClass,schemaFactory,schemaUrl,schemaFile);
        log.info("db source registered: " + className + " -> " + schemaPath);
      }
      log.info("persist db sources successfully initialized");
    }
    catch (Exception e)
    {
      log.error("Error in init(): " + e);
      e.printStackTrace();
    }
  }
}



