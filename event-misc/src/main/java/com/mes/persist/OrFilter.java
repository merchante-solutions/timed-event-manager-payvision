package com.mes.persist;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class OrFilter extends AbstractFilter implements CompoundFilter
{
  static Logger log = Logger.getLogger(OrFilter.class);

  private List filters;

  public OrFilter()
  {
    filters = new ArrayList();
  }
  public OrFilter(List filters)
  {
    this.filters = filters;
  }

  public List getFilters()
  {
    return filters;
  }

  public void addFilter(Filter filter)
  {
    filters.add(filter);
  }
  
  public String getWhereClause(TableDef td, String qualifier)
  {
    StringBuffer clause = new StringBuffer();
    clause.append("(");
    for (Iterator i = filters.iterator(); i.hasNext();)
    {
      Filter filter = (Filter)i.next();
      clause.append(filter.getWhereClause() 
                    + (i.hasNext() ? "or\n " : ""));
    }
    clause.append(")");
    return clause.toString();
  }

  public String getWhereClause(String prefix)
  {
    StringBuffer clause = new StringBuffer();
    clause.append("(");
    for (Iterator i = filters.iterator(); i.hasNext();)
    {
      Filter filter = (Filter)i.next();
      clause.append(filter.getWhereClause(prefix) 
                    + (i.hasNext() ? "or\n " : ""));
    }
    clause.append(")");
    return clause.toString();
  }

  public String getAuditLog()
  {
    StringBuffer clause = new StringBuffer();
    clause.append("(");
    for (Iterator i = filters.iterator(); i.hasNext();)
    {
      Filter filter = (Filter)i.next();
      clause.append(filter.getAuditLog() + (i.hasNext() ? "or " : ""));
    }
    clause.append(")");
    return clause.toString();
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    for (Iterator i = filters.iterator(); i.hasNext();)
    {
      Filter filter = (Filter)i.next();
      mark = filter.setMarks(ps,mark);
    }
    return mark;
  }
}