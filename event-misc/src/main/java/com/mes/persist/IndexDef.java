package com.mes.persist;

public interface IndexDef
{
  public void addColumnDef(String name);
  public ColumnDefSet getColumnDefSet();
  public String getName();
  public TableDef getTableDef();
  public boolean isUnique();
}
