package com.mes.persist;

import java.io.File;
import java.net.URL;

public interface SchemaFactory
{
  public Schema createSchema(URL schemaUrl);
  public Schema createSchema(File schemaFile);
}
