package com.mes.persist;

import java.util.List;

public interface SubqueryDef
{
  public static String ONE_TO_ONE = "one-to-one";
  public static String AGGREGATE = "aggregate";

  public static String OPTIONAL = "optional";
  public static String REQUIRED = "required";

  public String getName();

  public TableDef getParentTd();
  public ColumnDef getParentCd();

  public TableDef getSubTd();
  public ColumnDef getSubCd();

  public Object getType();

  public boolean isRequired();

  public void addColumnDef(String name, DataType dt, String wrapperExpr, 
    String sqExpr);
  public List getColumnDefs();

  public String getSelectClause(String qualifier);
  public String getFromClause(String qualifier);
  public String getWhereClause(String qualifier);
  public String getWhereClause(String qualifier, boolean parentOptional);
}