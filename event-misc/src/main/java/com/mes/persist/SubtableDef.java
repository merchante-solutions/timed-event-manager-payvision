package com.mes.persist;

import java.lang.reflect.Method;

public interface SubtableDef extends TableDef
{
  public static final String REQUIRED = "required";
  public static final String OPTIONAL = "optional";

  public String       getName();
  public Method       getGetter();
  public Method       getSetter();

  public String       getSubQualifier();

  public String       getQualifier(String prefix);

  public TableDef     getTd();
  public TableDef     getParentTd();
  public SubtableDef  getJoinSubTd();

  public ColumnDef    getJoinToCd();
  public String       getSubIndexName();
  public Method       getParentKeyGetterMethod();

  public Object       getJoinType();

  public boolean      isRequired();
}
