package com.mes.persist;

public interface Indexable
{
  public String getWhereClause();
  public String getWhereClause(String qualifier);
  public String getWhereClause(String qualifier, boolean parentOptional);
}