package com.mes.persist;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.log4j.Logger;

public class StringFilter extends AbstractFilter
{
  static Logger log = Logger.getLogger(StringFilter.class);

  private String filterVal;

  private boolean isEqual;

  private boolean ignoreCase = true;
  private boolean startsWith;
  private boolean endsWith;

  public StringFilter(String colName, String filterVal, boolean isEqual)
  {
    this.colName = colName;
    this.filterVal = filterVal;
    this.isEqual = isEqual;
  }
  public StringFilter(String colName, String filterVal)
  {
    this(colName,filterVal,true);
  }

  public void setIgnoreCase(boolean ignoreCase)
  {
    this.ignoreCase = ignoreCase;
  }

  public void setStartsWith(boolean startsWith)
  {
    this.startsWith = startsWith;
  }
  public void setEndsWith(boolean endsWith)
  {
    this.endsWith = endsWith;
  }
  public void setIsSubstring(boolean flag)
  {
    startsWith = flag;
    endsWith = flag;
  }
  public boolean isSubstring()
  {
    return startsWith || endsWith;
  }

  private String getQualifiedCol(TableDef td, String qualifier)
  {
    StringBuffer col = new StringBuffer();
    col.append(ignoreCase ? "lower(" : "");
    col.append(td.getColumnDef(colName).getQualifiedName(qualifier));
    col.append(ignoreCase ? ")" : "");
    return ""+col;
  }

  private String getOperator()
  {
    return isSubstring() ? (isEqual ? "like" : "not like")
                         : (isEqual ? "=" : "<>");
  }

  private String getValueExpression(boolean forAudit)
  {
    StringBuffer val = new StringBuffer();
    val.append(endsWith ? "'%'||" : "");
    val.append(forAudit ?
      (ignoreCase ? filterVal.toLowerCase() : filterVal) : "?");
    val.append(startsWith ? "||'%' " : " ");
    return ""+val;
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    return  " " + getQualifiedCol(td,qualifier) + 
            " " + getOperator() + 
            " " + getValueExpression(false);
  }

  public String getAuditLog()
  {
    return  " " + getQualifiedCol(td,qualifier) + 
            " " + getOperator() + 
            " " + getValueExpression(true);
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    ps.setString(mark++,ignoreCase ? filterVal.toLowerCase() : filterVal);
    return mark;
  }
}