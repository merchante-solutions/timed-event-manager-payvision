package com.mes.persist;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class InFilter extends AbstractFilter
{
  static Logger log = Logger.getLogger(InFilter.class);

  public static final int VT_UNDEFINED = 0;
  public static final int VT_STRING = 1;
  public static final int VT_OTHER = 2;

  private boolean notFlag;
  private List values = new ArrayList();
  private int valueType = VT_UNDEFINED;

  public InFilter(String colName, boolean notFlag)
  {
    this.colName = colName;
    this.notFlag = notFlag;
  }
  public InFilter(String colName, Collection vals, boolean notFlag)
  {
    this(colName,notFlag);
    addValues(vals);
  }
  public InFilter(String colName)
  {
    this(colName,false);
  }
  public InFilter(String colName, Collection vals)
  {
    this(colName,vals,false);
  }
      

  public void addValue(Object value)
  {
    if (valueType == VT_UNDEFINED)
    {
      valueType = value instanceof String ? VT_STRING : VT_OTHER;
    }
    else if (valueType != VT_STRING && value instanceof String)
    {
      throw new RuntimeException("InFilter error, cannot add String value"
        + " to filter, non-String values already exist");
    }
    else if (valueType == VT_STRING && !(value instanceof String))
    {
      throw new RuntimeException("InFilter error, cannot add non-String value"
        + " to filter, String values already exist");
    }
    values.add(""+value);
  }
  public void addValue(int value)
  {
    addValue(Integer.valueOf(value));
  }
  public void addValue(long value)
  {
    addValue(Long.valueOf(value));
  }
  public void addValue(float value)
  {
    addValue(Float.valueOf(value));
  }
  public void addValue(double value)
  {
    addValue(Double.valueOf(value));
  }
  public void addValues(Collection vals)
  {
    for (Iterator i = vals.iterator(); i.hasNext();) addValue(i.next());
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    StringBuffer clause = new StringBuffer();
    clause.append(" " + td.getColumnDef(colName).getQualifiedName(qualifier));
    clause.append(" " + (notFlag ? "not" : "") + " in (");
    for (Iterator i = values.iterator(); i.hasNext();)
    {
      clause.append(" ");
      if (valueType == VT_STRING) clause.append("'");
      clause.append(""+i.next());
      if (valueType == VT_STRING) clause.append("'");
      if (i.hasNext()) clause.append(",");
    }
    clause.append(" )");
    return clause.toString();
  }

  public String getAuditLog()
  {
    StringBuffer clause = new StringBuffer();
    clause.append(" " + td.getColumnDef(colName).getQualifiedName(qualifier));
    clause.append(" " + (notFlag ? "not" : "") + " in (");
    for (Iterator i = values.iterator(); i.hasNext();)
    {
      clause.append(" ");
      if (valueType == VT_STRING) clause.append("'");
      clause.append(""+i.next());
      if (valueType == VT_STRING) clause.append("'");
      if (i.hasNext()) clause.append(",");
    }
    clause.append(" )");
    return clause.toString();
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    return mark;
  }

  public void reset()
  {
    valueType = VT_UNDEFINED;
    values = new ArrayList();
  }
}