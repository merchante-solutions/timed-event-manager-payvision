package com.mes.persist;

import java.sql.Connection;
import java.sql.ResultSet;

public interface Executable
{
  public int executeUpdate(Connection con);
  public ResultSet executeQuery(Connection con);
  public int executeCountQuery(Connection con);
  public boolean next();
  public Object getColumnValue(ColumnDef cd, String qualifier);
  public void loadRecord(Record record, String qualifier);
  public void close();
  public String getStatement();
}