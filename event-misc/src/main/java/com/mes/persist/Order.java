package com.mes.persist;

public interface Order
{
  public String getColName();
  public String getQualifier();
  public String getOrderAlias();
  public boolean isDescending();
}