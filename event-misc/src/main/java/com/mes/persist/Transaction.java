package com.mes.persist;

public interface Transaction
{
  public static final int COMMIT    = 1;
  public static final int ROLLBACK  = 2;

  public int execute();
  public boolean run();
  public long getTranId();
}
