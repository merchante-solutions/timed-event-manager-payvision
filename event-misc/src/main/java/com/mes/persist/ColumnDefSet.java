package com.mes.persist;

import java.util.List;

public interface ColumnDefSet
{
  public ColumnDef getColumnDef(String name);

  public List getColumnDefs();

  public String getSelectClause(String qualifier);
  public String getInsertColumnList(String qualifier);
  public String getInsertValueMarks();
  public String getUpdateSetList(String qualifier);
}
