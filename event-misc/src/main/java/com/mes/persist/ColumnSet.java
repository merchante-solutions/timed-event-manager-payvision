package com.mes.persist;

import java.util.List;

public interface ColumnSet
{
  public ColumnDefSet getColumnDefSet();
  
  public List getColumns();
  public Column getColumn(String name);
  public boolean isEmpty();
}
