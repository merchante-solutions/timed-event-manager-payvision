package com.mes.persist;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface Filter
{
  public String getColName();

  public void setTableDef(TableDef td);
  public void setQualifier(String qualifier);

  public String getWhereClause();
  public String getWhereClause(String prefix);
  public String getWhereClause(TableDef td, String qualifier);

  public int setMarks(PreparedStatement ps, int mark) throws SQLException;

  public String getAuditLog();

  public void reset();
}