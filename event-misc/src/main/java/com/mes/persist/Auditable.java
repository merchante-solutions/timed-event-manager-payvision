package com.mes.persist;

public interface Auditable
{
  public Index getPrimaryKey();
  public String getAuditLog();
  public String getAuditName();
}
