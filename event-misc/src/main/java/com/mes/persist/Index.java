package com.mes.persist;

import java.util.List;

public interface Index extends Indexable
{
  public IndexDef getIndexDef();
  
  public String getName();
  
  public ColumnSet getColumnSet();
  public List getColumns();
  
  public boolean isLoaded();
  public boolean isUnique();
  public boolean isKey();

  public String getAuditLog();

  public String getQualifier(String prefix);
  public String getQualifier();
}