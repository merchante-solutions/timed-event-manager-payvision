package com.mes.persist;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

public class CodeFilter extends AbstractFilter implements CompoundFilter
{
  static Logger log = Logger.getLogger(CodeFilter.class);

  private List filters = new ArrayList();
  private Filter theFilter;

  public CodeFilter(String colName, String codeStr)
  {
    createInternalFilter(colName,codeStr);
    this.colName = colName;
    theFilter = (Filter)filters.get(0);

  }

  private void createInternalFilter(String colName, String codeStr)
  {
    List intFilters = new ArrayList();
    Pattern pattern = Pattern.compile(" *([|+]?) *([!]?) *([a-zA-Z0-9_-]+) *");
    Matcher matcher = pattern.matcher(codeStr);
    String lastOp = null;
    while (matcher.find())
    {
      String op = matcher.group(1);
      boolean isEqual = matcher.group(2).length() == 0;
      String code = matcher.group(3);
      Filter f =  new StringFilter(colName,code,isEqual);
      intFilters.add(f);
      lastOp = op;
    }
    if (intFilters.size() > 1)
    {
      boolean isAnd = lastOp != null && !lastOp.equals("|");
      if (isAnd)
      {
        addFilter(new AndFilter(intFilters));
      }
      else
      {
        addFilter(new OrFilter(intFilters));
      }
    }
    else if (intFilters.size() == 1)
    {
      addFilter((Filter)intFilters.get(0));
    }
    else
    {
      throw new RuntimeException("Invalid code expression");
    }
  }

  public List getFilters()
  {
    return filters;
  }

  public void addFilter(Filter filter)
  {
    filters.add(filter);
  }

  public String getWhereClause()
  {
    return theFilter.getWhereClause();
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    return theFilter.getWhereClause(td,qualifier);
  }

  public String getWhereClause(String prefix)
  {
    return theFilter.getWhereClause(prefix);
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    return theFilter.setMarks(ps,mark);
  }

  public String getAuditLog()
  {
    return theFilter.getAuditLog();
  }
}






