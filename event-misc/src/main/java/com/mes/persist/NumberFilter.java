package com.mes.persist;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class NumberFilter extends AbstractFilter
{
  private int intVal;
  private long longVal;
  private float floatVal;
  private double doubleVal;
  private boolean isEqual;
  private DataType dt;

  private NumberFilter(String colName, boolean isEqual)
  {
    this.colName = colName;
    this.isEqual = isEqual;
  }

  public NumberFilter(String colName, long longVal, boolean isEqual)
  {
    this(colName,isEqual);
    this.longVal = longVal;
    dt = DataType.LONG;
  }
  public NumberFilter(String colName, long longVal)
  {
    this(colName,longVal,true);
  }

  public NumberFilter(String colName, int intVal, boolean isEqual)
  {
    this(colName,isEqual);
    this.intVal = intVal;
    dt = DataType.INT;
  }
  public NumberFilter(String colName, int intVal)
  {
    this(colName,intVal,true);
  }

  public NumberFilter(String colName, float floatVal, boolean isEqual)
  {
    this(colName,isEqual);
    this.floatVal = floatVal;
    dt = DataType.FLOAT;
  }
  public NumberFilter(String colName, float floatVal)
  {
    this(colName,floatVal,true);
  }

  public NumberFilter(String colName, double doubleVal, boolean isEqual)
  {
    this(colName,isEqual);
    this.doubleVal = doubleVal;
    dt = DataType.DOUBLE;
  }
  public NumberFilter(String colName, double doubleVal)
  {
    this(colName,doubleVal,true);
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    return " " + td.getColumnDef(colName).getQualifiedName(qualifier)
      + " " + (isEqual ? "=" : "<>") + " ? ";
  }

  public String getAuditLog()
  {
    return " " + td.getColumnDef(colName).getQualifiedName(qualifier) 
      + " " + (isEqual ? "=" : "<>") + " " + longVal;
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    if (dt.isLong())
    {
      ps.setLong(mark++,longVal);
    }
    else if (dt.isInt())
    {
      ps.setInt(mark++,intVal);
    }
    else if (dt.isFloat())
    {
      ps.setFloat(mark++,floatVal);
    }
    else if (dt.isDouble())
    {
      ps.setDouble(mark++,doubleVal);
    }
    else
    {
      throw new RuntimeException("Invalid data type of filter val: " + dt);
    }
    return mark;
  }
}