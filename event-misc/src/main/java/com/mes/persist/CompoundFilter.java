package com.mes.persist;

import java.util.List;

public interface CompoundFilter extends Filter
{
  public List getFilters();
  public void addFilter(Filter f);
}