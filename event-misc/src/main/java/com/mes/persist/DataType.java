package com.mes.persist;

import java.util.Hashtable;
import java.util.Map;

public class DataType
{
  public static final int DT_STRING     = 0;
  public static final int DT_INT        = 1;
  public static final int DT_LONG       = 2;
  public static final int DT_DOUBLE     = 3;
  public static final int DT_FLOAT      = 4;
  public static final int DT_TIMESTAMP  = 5;
  public static final int DT_DATE       = 6;
  public static final int DT_TIME       = 7;

  private static String[] names =
  {
    "STRING",
    "INT",
    "LONG",
    "DOUBLE",
    "FLOAT",
    "TIMESTAMP",
    "DATE",
    "TIME"
  };

  private static int[] sqlTypes =
  {
    java.sql.Types.VARCHAR,
    java.sql.Types.NUMERIC,
    java.sql.Types.NUMERIC,
    java.sql.Types.NUMERIC,
    java.sql.Types.NUMERIC,
    java.sql.Types.TIMESTAMP,
    java.sql.Types.DATE,
    java.sql.Types.TIME
  };
  

  // not used right now...
  private static Map[] sqlTypeMaps = new Map[8];
  static
  {
    sqlTypeMaps[DT_STRING] = new Hashtable();
    sqlTypeMaps[DT_STRING].put("VARCHAR",String.class);

    sqlTypeMaps[DT_INT] = new Hashtable();
    sqlTypeMaps[DT_INT].put("NUMERIC",Integer.class);
  }

  private int type;

  private DataType(int type)
  {
    this.type = type;
  }

  public boolean equals(Object that)
  {
    if (that instanceof DataType)
    {
      return ((DataType)that).type == type;
    }
    return false;
  }

  public String toString()
  {
    return names[type];
  }
  
  public int sqlType()
  {
    return sqlTypes[type];
  }
  
  public boolean isString()
  {
    return this == STRING;
  }
  public boolean isInt()
  {
    return this == INT;
  }
  public boolean isLong()
  {
    return this == LONG;
  }
  public boolean isDouble()
  {
    return this == DOUBLE;
  }
  public boolean isFloat()
  {
    return this == FLOAT;
  }
  public boolean isTimestamp()
  {
    return this == TIMESTAMP;
  }
  public boolean isDate()
  {
    return this == DATE;
  }
  public boolean isTime()
  {
    return this == TIME;
  }
  public boolean isNumeric()
  {
    return isInt() || isLong() || isDouble() || isFloat();
  }

  public static DataType  STRING    = new DataType(DT_STRING);
  public static DataType  INT       = new DataType(DT_INT);
  public static DataType  LONG      = new DataType(DT_LONG);
  public static DataType  DOUBLE    = new DataType(DT_DOUBLE);
  public static DataType  FLOAT     = new DataType(DT_FLOAT);
  public static DataType  TIMESTAMP = new DataType(DT_TIMESTAMP);
  public static DataType  DATE      = new DataType(DT_DATE);
  public static DataType  TIME      = new DataType(DT_TIME);
}
