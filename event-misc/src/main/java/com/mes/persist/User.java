package com.mes.persist;

public interface User
{
  public String getName();
  
  public boolean isValid();
}