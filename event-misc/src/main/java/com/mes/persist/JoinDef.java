package com.mes.persist;

public interface JoinDef extends TableDef
{
  public static String ONE_TO_ONE = "one-to-one";
  public static String ONE_TO_MANY = "one-to-many";
  public static String COMPOSITE = "composite";

  public String getJoinName();

  public TableDef getJoinTableDef(String tblName);

  public String getJoinQualifier(int joinNum);
  public String getJoinQualifier(TableDef td);

  public void addJoin(String leftName, TableDef rightTd, String rightName, 
    Object joinType);

  public String getSelectMapKeyClause();
  public String getSelectMapKeyClause(String qualifier);
  public String getSelectMapValueClause();
  public String getSelectMapValueClause(String qualifier);
  public String getDeleteSubquery();
  public String getDeleteStatement(Index index);

  public String getQualifiedKeyName();
  public String getAliasedKeyName();
  public ColumnDef getKeyColumnDef();

  public String getTargetJoinIdName();

  public TableDef getFirstTableDef();
  public Index getPrimaryKey(Record record);
  public Index getLoadedKey(Record record);
  public Index getIndex(Record record, String indexName);

  public boolean isOneToMany();
}
