package com.mes.persist;

import java.lang.reflect.Method;

public interface ColumnDef
{
  public String getName();
  
  public DataType getDataType();

  public String getWrapperExpr();
  
  public void setSetterMethod(Method method);
  public Method getSetterMethod();
  
  public void setGetterMethod(Method method);
  public Method getGetterMethod();

  public String getColumnAlias(String qualifier);
  public String getQualifiedName(String qualifier);
  public String getSubQualifier(String qualifier);

  public String getSelectClause(String qualifer);
  public String getSubquerySelectClause();

  public boolean isReadOnly();
}