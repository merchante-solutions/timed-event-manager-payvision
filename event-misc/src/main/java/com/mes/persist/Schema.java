package com.mes.persist;

import java.util.List;

public interface Schema
{
  public String getIdSequence();
  
  public String getLogTable();
  
  public void addTableDef(TableDef tDef);
  public List getTableDefs();
  public TableDef getTableDef(Object defKey);
}