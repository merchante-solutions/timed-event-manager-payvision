package com.mes.persist;

import org.apache.log4j.Logger;

public abstract class AbstractFilter implements Filter
{
  static Logger log = Logger.getLogger(AbstractFilter.class);

  protected String colName;
  protected TableDef td;
  protected String qualifier;

  public String getColName()
  {
    return colName;
  }

  public void setTableDef(TableDef td)
  {
    this.td = td;
  }

  public void setQualifier(String qualifier)
  {
    this.qualifier = qualifier;
  }

  public String getWhereClause()
  {
    return getWhereClause(td,qualifier);
  }

  public String getWhereClause(String prefix)
  {
    String preQual = prefix == null ? qualifier : prefix + qualifier;
    return getWhereClause(td,preQual);
  }

  public void reset()
  {
  }
}