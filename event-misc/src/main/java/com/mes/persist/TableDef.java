package com.mes.persist;

import java.util.List;

public interface TableDef extends Indexable
{
  public String getName();

  public void setQualifier(String sq);
  public String getQualifier();
  public String getQualifier(String prefix);
  public String getQualifiedName();

  public void addColumnDef(ColumnDef cDef);
  public void addColumnDef(String colName,DataType dataType);
  public ColumnDef getColumnDef(String name);
  public ColumnDefSet getColumnDefSet();
  public String getAliasedColumnName(String colName);
  public String getAliasedColumnName(String colName, String prefix);
  public String getQualifiedColumnName(String colName);
  public String getQualifiedColumnName(String colName, String prefix);
  
  public IndexDef addIndexDef(String name, boolean isUnique);
  public IndexDef getIndexDef(String name);
  public List getIndexDefs();
  public IndexDef getPrimaryKeyDef();

  public JoinDef addJoinDef(String joinName, String colName, TableDef joinTd,
    String joinColName, Object joinType);
  public JoinDef getJoinDef(String joinName);
  public List getJoinDefs();

  public void addSubtableDef(String colName, TableDef subTd, String subIdxName, 
    Object joinType, Object tableType, String subAccName, String subTblName);
  public void addSubtableDef(String colName, TableDef subTd, String subIdxName, 
    Object joinType, Object tableType);
  public SubtableDef getSubtableDef(String subalias);
  public List getSubtableDefs();

  public String getInsertColumnList();
  public String getInsertValueMarks();
  public String getUpdateSetList();

  public String getSelectClause();
  public String getSelectClause(String qualifier);
  public String getFromClause();
  public String getFromClause(String qualifier);

  public Class getBeanClass();

  public void addSubqueryDef(SubqueryDef sqDef);
  public List getSubqueryDefs();
}