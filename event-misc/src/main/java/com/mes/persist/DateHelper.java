package com.mes.persist;

public class DateHelper
{
  public static java.sql.Date toSql(java.util.Date utilDate)
  {
    return new java.sql.Date(utilDate.getTime());
  }

  private java.util.Date  utilFrom;
  private java.util.Date  utilTo;
  private java.sql.Date   sqlFrom;
  private java.sql.Date   sqlTo;

  public DateHelper(java.util.Date utilFrom, java.util.Date utilTo)
  {
    this.utilFrom = utilFrom;
    this.utilTo = utilTo;
    utilToSql();
  }
  public DateHelper(java.util.Date utilFrom)
  {
    this(utilFrom,null);
  }
  public DateHelper(java.sql.Date sqlFrom, java.sql.Date sqlTo)
  {
    utilFrom = sqlFrom;
    utilTo = sqlTo;
    utilToSql();
  }
  public DateHelper(java.sql.Date sqlFrom)
  {
    this(sqlFrom,null);
  }

  private void utilToSql()
  {
    sqlFrom = null;
    sqlTo = null;
    if (utilFrom != null)
    {
      sqlFrom = toSql(utilFrom);
      if (utilTo != null)
      {
        sqlTo = toSql(utilTo);
      }
      else
      {
        sqlTo = sqlFrom;
      }
    }
  }

  public java.sql.Date getSqlFrom()
  {
    return sqlFrom;
  }
  public java.sql.Date getSqlTo()
  {
    return sqlTo;
  }

  public java.util.Date getUtilFrom()
  {
    return utilFrom;
  }
  public java.util.Date getUtilTo()
  {
    return utilTo;
  }
}

