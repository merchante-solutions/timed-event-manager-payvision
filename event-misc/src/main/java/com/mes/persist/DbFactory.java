package com.mes.persist;

public interface DbFactory
{
  public Db getDb();
}