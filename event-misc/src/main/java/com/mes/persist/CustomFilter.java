package com.mes.persist;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.log4j.Logger;

public class CustomFilter extends AbstractFilter
{
  static Logger log = Logger.getLogger(CustomFilter.class);

  private String customClause;

  public CustomFilter(String colName, String customClause)
  {
    this.colName = colName;
    this.customClause = customClause;
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    StringBuffer clause = new StringBuffer();
    clause.append(" " + td.getColumnDef(colName).getQualifiedName(qualifier));
    clause.append(" " + customClause);
    return clause.toString();
  }

  public String getAuditLog()
  {
    StringBuffer clause = new StringBuffer();
    clause.append(" " + td.getColumnDef(colName).getQualifiedName(qualifier));
    clause.append(" " + customClause);
    return clause.toString();
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    return mark;
  }
}