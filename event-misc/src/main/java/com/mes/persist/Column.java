package com.mes.persist;

public interface Column
{
  public ColumnDef getColumnDef();
  
  public String getName();
  public String getColumnAlias(String qualifier);
  public String getQualifiedName(String qualifier);
  
  public DataType getDataType();

  public void set(Object value);
  
  public void set(long value);
  public void set(int value);
  public void set(double value);
  public void set(float value);
  
  public void setNull();

  public Object             getValue();
  
  public String             getString();
  public long               getLong();
  public int                getInt();
  public double             getDouble();
  public float              getFloat();
  public java.sql.Timestamp getTimestamp();
  public java.sql.Time      getTime();
  public java.sql.Date      getSqlDate();
  public java.util.Date     getDate();

  public boolean isNull();

  public void setPersisted();
  public boolean isPersisted();
  public Object getPersistValue();
  public boolean isPersistNull();
  
  public String getAuditLog();
  
  public void copyDbState(Column column);
  public void copy(Column column);

  public boolean isReadOnly();
}

