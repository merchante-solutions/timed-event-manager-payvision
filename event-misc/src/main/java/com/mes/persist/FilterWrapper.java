package com.mes.persist;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class FilterWrapper implements Filter
{
  private Filter filter;

  public FilterWrapper(Filter filter, TableDef td, String qualifier)
  {
    this.filter = filter;
    filter.setTableDef(td);
    filter.setQualifier(qualifier);
  }

  public String getColName()
  {
    return filter.getColName();
  }

  public void setTableDef(TableDef td)
  {
    filter.setTableDef(td);
  }

  public void setQualifier(String qualifier)
  {
    filter.setQualifier(qualifier);
  }

  public String getWhereClause()
  {
    return filter.getWhereClause();
  }

  public String getWhereClause(TableDef td, String qualifier)
  {
    return filter.getWhereClause(td,qualifier);
  }

  public String getWhereClause(String prefix)
  {
    return filter.getWhereClause(prefix);
  }

  public int setMarks(PreparedStatement ps, int mark) throws SQLException
  {
    return filter.setMarks(ps,mark);
  }

  public String getAuditLog()
  {
    return filter.getAuditLog();
  }

  public void reset()
  {
    filter.reset();
  }
}