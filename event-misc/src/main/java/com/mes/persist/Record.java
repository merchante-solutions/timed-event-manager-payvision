package com.mes.persist;

import java.util.List;

public interface Record extends Auditable
{
  public TableDef getTableDef();
  public String getName();

  public ColumnSet getColumnSet();
  public List getColumns();
  public Column getColumn(String name);

  public ColumnDefSet getColumnDefSet();
  public List getColumnDefs();

  public Index getPrimaryKey(String qualifier);
  public Index getLoadedKey();
  public Index getLoadedKey(String qualifier);
  public Index getIndex(String name);
  public Index getIndex(String name, String qualifier);

  public Object getKeyValue();
  public Object getKeyValue(int colNum);
  
  public boolean isPersisted();
  public void setPersisted();
  public void unsetPersisted();
  
  public void copyDbState(Record record);
  public void copy(Record record);
  
  public void putSubrecord(TableDef subTd, Record subrec);
  public Record getSubrecord(TableDef subTd);

  public boolean isEmpty();
}