package com.mes.persist;

import java.io.File;
import java.net.URL;
import java.util.Calendar;
import org.apache.log4j.Logger;

public class DbSource
{
  static Logger log = Logger.getLogger(DbSource.class);

  private SchemaFactory schemaFactory;
  private URL schemaUrl;
  private File schemaFile;
  private Schema schema;
  private long lastLoaded;

  /**
   * If a schema file is provided the source will attempt to reload
   * the schema when a new version of the schema file is detected.
   * If no file is available it will load the schema once only.
   */

  public DbSource(SchemaFactory schemaFactory, URL schemaUrl, File schemaFile)
  {
    if (schemaFactory == null)
    {
      throw new NullPointerException("Null schema factory not allowed");
    }
    this.schemaFactory = schemaFactory;
    if (schemaUrl == null && schemaFile == null)
    {
      throw new NullPointerException(
        "Null schema source (URL and file cannot both be null)");
    }
    this.schemaUrl = schemaUrl;
    this.schemaFile = schemaFile;
  }
  public DbSource(SchemaFactory schemaFactory, URL schemaUrl)
  {
    this(schemaFactory,schemaUrl,null);
  }
  public DbSource(SchemaFactory schemaFactory, File schemaFile)
  {
    this(schemaFactory,null,schemaFile);
  }

  public Schema getSchema()
  {
    boolean schemaLoaded  = schema != null;
    boolean newFileFound  = schemaFile != null && 
                            lastLoaded < schemaFile.lastModified();

    if (!schemaLoaded || newFileFound)
    {
      if (schemaUrl != null)
      {
        schema = schemaFactory.createSchema(schemaUrl);
      }
      else
      {
        schema = schemaFactory.createSchema(schemaFile);
      }
      lastLoaded = Calendar.getInstance().getTime().getTime();
    }
    return schema;
  }
}