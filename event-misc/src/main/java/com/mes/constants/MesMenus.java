/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/constants/MesMenus.java $

  Description:
    Base class for report data beans

    This class should maintain any data that is common to all of the
    possible report data beans.  The inheritors of this class should
    maintain any page-specific report data.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-12-23 10:38:35 -0800 (Wed, 23 Dec 2009) $
  Version            : $Revision: 16846 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.constants;

public class MesMenus
{
  // menu ids ( see mes.menu_types )
  public static final int     MENU_ID_MAIN                      = 0;
  public static final int     MENU_ID_MERCHANT                  = 1;
  public static final int     MENU_ID_SUMMARY                   = 2;
  public static final int     MENU_ID_BANK                      = 3;
  public static final int     MENU_ID_RISK                      = 4;
  public static final int     MENU_ID_CHARGEBACK                = 5;
  public static final int     MENU_ID_ACCOUNTING                = 6;
  public static final int     MENU_ID_MANAGEMENT                = 7;
  public static final int     MENU_ID_SALES                     = 8;
  public static final int     MENU_ID_CALL_TRACKING             = 9;
  public static final int     MENU_ID_EQUIP_MAIN                = 10;
  public static final int     MENU_ID_EQUIP_ORDER               = 11;
  public static final int     MENU_ID_EQUIP_ADD                 = 12;
  public static final int     MENU_ID_DISC_SALES_SUPPORT        = 13;
  public static final int     MENU_ID_CLIENT_SERVICES           = 14;
  public static final int     MENU_ID_EQUIPMENT_SWAP            = 15;
  public static final int     MENU_ID_ACCOUNT_SERVICING         = 16;
  public static final int     MENU_ID_CHANGE_REQUESTS           = 17;
  public static final int     MENU_ID_PROSPECTS                 = 18;
  public static final int     MENU_ID_REPAIR                    = 19;
  public static final int     MENU_ID_DEPLOYMENT                = 20;
  public static final int     MENU_ID_PINPAD_EXCHANGE           = 21;
  public static final int     MENU_ID_ACTIVATION_QUEUE          = 22;
  public static final int     MENU_ID_DISCOVER_BULLETINS        = 23;
  public static final int     MENU_ID_CBT_SALES_MATERIALS       = 24;
  public static final int     MENU_ID_CBT_TRAINING              = 25;
  public static final int     MENU_ID_CBT_POS_PARTNER           = 26;

  public static final int     MENU_ID_CBT_CREDIT_QUEUES         = 29;

  public static final int     MENU_ID_TOOLS_FOR_USERIDS         = 30;
  public static final int     MENU_ID_ACTIVATION_QUEUES         = 31;
  public static final int     MENU_ID_ACR_QUEUES                = 32;
  public static final int     MENU_ID_ACH_REJECT_QUEUES         = 33;
  public static final int     MENU_ID_SUMMIT_SALES_MATERIALS    = 34;
  public static final int     MENU_ID_INTERNAL_REPORTS          = 35;
  public static final int     MENU_ID_MMS_SETUP                 = 36;
  public static final int     MENU_ID_NEW_ACTIVATION_QUEUES     = 37;
  public static final int     MENU_ID_PROGRAMMING_QUEUES        = 38;
  public static final int     MENU_ID_RISK_QUEUES               = 39;
  public static final int     MENU_ID_COLLECTIONS_QUEUES        = 40;
  public static final int     MENU_ID_LEGAL_QUEUES              = 41;

  public static final int     MENU_ID_CBT_CREDIT_QUEUES_LV      = 42;
  public static final int     MENU_ID_CBT_CREDIT_QUEUES_HV      = 43;
  public static final int     MENU_ID_CBT_DOCUMENT_QUEUES       = 44;
  public static final int     MENU_ID_RETRIEVAL_QUEUES          = 45;

  public static final int     MENU_ID_CALLTAG_MANAGEMENT        = 46;
  public static final int     MENU_ID_CALLTAG_QUEUES            = 47;

  public static final int     MENU_ID_CHECKREPORTS              = 48;
  public static final int     MENU_ID_CHARGEBACK_QUEUES         = 49;

  public static final int     MENU_ID_VC_PROGRAMMING_QUEUES     = 50;
  public static final int     MENU_ID_TM_PROGRAMMING_QUEUES     = 51;

  public static final int     MENU_ID_CBT_REVIEW_QUEUES         = 53;

  public static final int     MENU_ID_BBT_QA_QUEUES             = 54;
  public static final int     MENU_ID_BBT_PRE_QA_QUEUES         = 55;

  public static final int     MENU_ID_CHARGEBACK_COLLECTION     = 56;

  public static final int     MENU_ID_CLIENT_REVIEW_QUEUES      = 57;

  public static final int     MENU_ID_CBT_APP_REVIEW_QUEUES     = 59;
  public static final int     MENU_ID_CBT_DATA_ASSIGN_QUEUES    = 60;
  public static final int     MENU_ID_CBT_RISKY_SIC_QUEUES      = 61;
  public static final int     MENU_ID_CBT_PULL_CREDIT_QUEUES    = 62;
  public static final int     MENU_ID_CBT_LOW_SCORE_QUEUES      = 63;
  public static final int     MENU_ID_CBT_HIGH_VOLUME_QUEUES    = 64;
  public static final int     MENU_ID_CBT_LARGE_TICKET_QUEUES   = 65;
  public static final int     MENU_ID_CBT_DOC_QUEUES            = 66;
  public static final int     MENU_ID_ACR_MGMT_QUEUES           = 67;
  public static final int     MENU_ID_BBT_ACR_QUEUES            = 68;

  public static final int     MENU_ID_CBT_TIER_QUEUES           = 70;
  public static final int     MENU_ID_CBT_TIER_SCORE_QUEUES     = 71;
  public static final int     MENU_ID_CBT_TIER_MCC_QUEUES       = 72;
  public static final int     MENU_ID_CBT_TIER_ASSIGN_QUEUES    = 73;
  public static final int     MENU_ID_CBT_TIER_34_QUEUES        = 74;
  public static final int     MENU_ID_CBT_TIER_34_HIGH_VOL_QUEUES = 75;
  public static final int     MENU_ID_CBT_TIER_HIGH_VOL_QUEUES  = 76;
  public static final int     MENU_ID_CBT_TIER_LOW_SCORE_QUEUES = 77;
  public static final int     MENU_ID_CBT_TIER_LARGE_TKT_QUEUES = 78;
  public static final int     MENU_ID_CBT_TIER_DOC_QUEUES       = 79;
  public static final int     MENU_ID_CBT_TIER_FINANCIAL_QUEUES = 81;

  public static final int     MENU_ID_PRODUCT_DETERMINATION     = 82;

  public static final int     MENU_ID_PCI_MERCHANT_OPS          = 83;

  public static final int     MENU_ID_EDIT_PKG_REJECTS          = 84;

  public static final int     MENU_ID_DEBIT_EXCEPTION_QUEUES    = 80;

  public static final int     MENU_ID_TPG_MAIN                  = 200;
  public static final int     MENU_ID_TPG_BATCH_MAINT           = 204;
  public static final int     MENU_ID_TPG_SETTINGS              = 205;
  public static final int     MENU_ID_TPG_HELP                  = 206;
  public static final int     MENU_ID_TPG_TRANSACTIONS          = 207;

  public static final int     MENU_ID_MINI_VT_MAIN              = 300;
  public static final int     MENU_ID_MINI_VT_GENERAL_SETTINGS  = 310;
  public static final int     MENU_ID_MINI_VT_BATCH_MANAGEMENT  = 320;
  public static final int     MENU_ID_MINI_VT_HELP              = 330;

  public static final int     MENU_ID_VOICE_CAPTURE_MAIN        = 400;

  // menu destination codes
  public static final int     MENU_DEST_INVALID                 = -1;
  public static final int     MENU_DEST_SUBMIT_APP              = 1;
  public static final int     MENU_DEST_ACTIVITY_REPORTS        = 2;
  public static final int     MENU_DEST_BBT_APP_PAGE0           = 3;


  // menu item types
  public static final int     ITEM_TYPE_UNSPECIFIED             = -1;
  public static final int     ITEM_TYPE_DEPOSITS                = 1;
  public static final int     ITEM_TYPE_TRANSACTIONS            = 2;
  public static final int     ITEM_TYPE_STATEMENTS              = 3;
  public static final int     ITEM_TYPE_RETRIEVALS              = 4;
  public static final int     ITEM_TYPE_CB_ADJUSTMENTS          = 5;
  public static final int     ITEM_TYPE_CB_PRENOTIFICATIONS     = 6;
  public static final int     ITEM_TYPE_SETTLEMENT              = 7;
  public static final int     ITEM_TYPE_DAILY_DEPOSIT           = 8;
  public static final int     ITEM_TYPE_BATCH_SUMMARY           = 9;
  public static final int     ITEM_TYPE_ADJUSTMENTS             = 10;
  public static final int     ITEM_TYPE_REJECTS                 = 11;
  public static final int     ITEM_TYPE_INTERCHANGE_SUMMARY     = 12;
  public static final int     ITEM_TYPE_INTERCHANGE_DOWNGRADES  = 13;
}
