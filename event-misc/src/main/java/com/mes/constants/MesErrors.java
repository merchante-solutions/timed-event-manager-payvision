/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/constants/MesErrors.java $

  Description:  
    Utility class for getting a database connection from an established 
    connection pool.


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 8/25/00 10:40a $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.constants;

public class MesErrors
{
  // error modules
  public static final int   ER_MOD_DATABASE           = 0x01;
  
  // error module segments
  public static final int   ER_SEG_ADMIN              = 0x01;
  
  // error codes
  public static final int   ER_NONE                   = 0;
  
  // database errors
  public static final int   ER_DB_GEN_FAIL            = ( ((ER_MOD_DATABASE & 0xff) << 24) | ((ER_SEG_ADMIN & 0xff) << 16) | 0x0001 );
  public static final int   ER_DB_EXCEPTION           = ( ((ER_MOD_DATABASE & 0xff) << 24) | ((ER_SEG_ADMIN & 0xff) << 16) | 0x0002 );
  public static final int   ER_DB_USER_ALREADY_EXISTS = ( ((ER_MOD_DATABASE & 0xff) << 24) | ((ER_SEG_ADMIN & 0xff) << 16) | 0x0003 );
  public static final int   ER_DB_USER_CREATE_FAILED  = ( ((ER_MOD_DATABASE & 0xff) << 24) | ((ER_SEG_ADMIN & 0xff) << 16) | 0x0004 );
  public static final int   ER_DB_USER_DOES_NOT_EXIST = ( ((ER_MOD_DATABASE & 0xff) << 24) | ((ER_SEG_ADMIN & 0xff) << 16) | 0x0005 );
  public static final int   ER_DB_USER_UPDATE_FAILED  = ( ((ER_MOD_DATABASE & 0xff) << 24) | ((ER_SEG_ADMIN & 0xff) << 16) | 0x0006 );
  public static final int   ER_DB_USER_DELETE_FAILED  = ( ((ER_MOD_DATABASE & 0xff) << 24) | ((ER_SEG_ADMIN & 0xff) << 16) | 0x0007 );
  public static final int   ER_DB_INSERT_FAILED       = ( ((ER_MOD_DATABASE & 0xff) << 24) | ((ER_SEG_ADMIN & 0xff) << 16) | 0x0008 );
  public static final int   ER_DB_UPDATE_FAILED       = ( ((ER_MOD_DATABASE & 0xff) << 24) | ((ER_SEG_ADMIN & 0xff) << 16) | 0x0009 );
  
  static public boolean isError( int errorCode )
  {
    return( ( errorCode == ER_NONE ) ? false : true );
  }
};
