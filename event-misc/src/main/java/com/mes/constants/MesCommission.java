/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/constants/MesCommission.java $

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 5/04/04 5:10p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.constants;

public class MesCommission
{
  public static class ChargeRecord
  {
    public    int         CommType        = mesConstants.COMM_TYPE_NONE;
    public    String      Description     = null;
    public    int         ExpenseType     = mesConstants.COMM_TYPE_NONE;
    public    String      ProductCode     = null;
    public    String      SearchMask      = null;
    
    public ChargeRecord(  String searchMask, 
                          String productCode, 
                          int commType, 
                          int expType, 
                          String desc )
    {
      CommType      = commType;
      Description   = desc;
      ExpenseType   = expType;
      ProductCode   = productCode;
      SearchMask    = searchMask;
    }
  }
  
  public static final ChargeRecord[]    MonthlyChargeRecords = 
  {
    new ChargeRecord( "%MONTH%VERISIGN%", 
                      "VL",
                      mesConstants.COMM_TYPE_ECOMMERCE_MONTHLY_FEES, 
                      mesConstants.COMM_TYPE_VERISIGN_LINK_COST, 
                      "Verisign Payflow Link Monthly Fees" ),
                      
    new ChargeRecord( "%MONTH%VERISIGN%", 
                      "VF",
                      mesConstants.COMM_TYPE_ECOMMERCE_MONTHLY_FEES, 
                      mesConstants.COMM_TYPE_VERISIGN_PRO_COST,
                      "Verisign Payflow Pro Monthly Fees" ),
                      
    new ChargeRecord( "%MONTH%VERISIGN%", 
                      "VP",
                      mesConstants.COMM_TYPE_ECOMMERCE_MONTHLY_FEES, 
                      mesConstants.COMM_TYPE_VERISIGN_PRO_COST,
                      "Verisign Payflow Pro Monthly Fees" ),
                      
    new ChargeRecord( "%CYBER%MONTH%",
                      null,
                      mesConstants.COMM_TYPE_ECOMMERCE_MONTHLY_FEES,
                      mesConstants.COMM_TYPE_CYBERCASH_COST,
                      "Cybercash Monthly Fees"),
                      
    new ChargeRecord( "%STATEMENT%",
                      null,
                      mesConstants.COMM_TYPE_STATEMENT_FEES_MONTHLY,
                      mesConstants.COMM_TYPE_NONE,
                      null ),
                      
    new ChargeRecord( "%MONTH%SERVICE%",
                      null,
                      mesConstants.COMM_TYPE_STATEMENT_FEES_MONTHLY,
                      mesConstants.COMM_TYPE_NONE,
                      null ),
  };
  
  public static final ChargeRecord[]    QuarterlyChargeRecords = 
  {
    new ChargeRecord( "%VERISIGN%SETUP%", 
                      "VL",
                      mesConstants.COMM_TYPE_ECOMMERCE_SETUP_FEES, 
                      mesConstants.COMM_TYPE_VERISIGN_LINK_COST_SETUP, 
                      "Verisign Payflow Link Setup Fees" ),
                      
    new ChargeRecord( "%VERISIGN%SETUP%", 
                      "VF",
                      mesConstants.COMM_TYPE_ECOMMERCE_SETUP_FEES, 
                      mesConstants.COMM_TYPE_VERISIGN_PRO_COST_SETUP,
                      "Verisign Payflow Pro Setup Fees" ),
                      
    new ChargeRecord( "%VERISIGN%SETUP%", 
                      "VP",
                      mesConstants.COMM_TYPE_ECOMMERCE_SETUP_FEES, 
                      mesConstants.COMM_TYPE_VERISIGN_PRO_COST_SETUP,
                      "Verisign Payflow Pro Setup Fees" ),
  
    new ChargeRecord( "%MONTH%VERISIGN%", 
                      "VL",
                      mesConstants.COMM_TYPE_ECOMMERCE_MONTHLY_FEES, 
                      mesConstants.COMM_TYPE_VERISIGN_LINK_COST, 
                      "Verisign Payflow Link Monthly Fees" ),
                      
    new ChargeRecord( "%MONTH%VERISIGN%", 
                      "VF",
                      mesConstants.COMM_TYPE_ECOMMERCE_MONTHLY_FEES, 
                      mesConstants.COMM_TYPE_VERISIGN_PRO_COST,
                      "Verisign Payflow Pro Monthly Fees" ),
                      
    new ChargeRecord( "%MONTH%VERISIGN%", 
                      "VP",
                      mesConstants.COMM_TYPE_ECOMMERCE_MONTHLY_FEES, 
                      mesConstants.COMM_TYPE_VERISIGN_PRO_COST,
                      "Verisign Payflow Pro Monthly Fees" ),
                      
    new ChargeRecord( "%CYBER%SETUP%",
                      null,
                      mesConstants.COMM_TYPE_ECOMMERCE_SETUP_FEES,
                      mesConstants.COMM_TYPE_CYBERCASH_COST_SETUP,
                      "Cybercash Setup Fees"),
                      
    new ChargeRecord( "%CYBER%MONTH%",
                      null,
                      mesConstants.COMM_TYPE_ECOMMERCE_MONTHLY_FEES,
                      mesConstants.COMM_TYPE_CYBERCASH_COST,
                      "Cybercash Monthly Fees"),
                      
    new ChargeRecord( "%STATEMENT%",
                      null,
                      mesConstants.COMM_TYPE_STATEMENT_FEES_MONTHLY,
                      mesConstants.COMM_TYPE_NONE,
                      null ),
                      
    new ChargeRecord( "%EQUIP%SUP%",
                      null,
                      mesConstants.COMM_TYPE_EQUIP_SERVICE_FEE_MONTHLY,
                      mesConstants.COMM_TYPE_NONE,
                      null ),
                      
    new ChargeRecord( "%APPL%",
                      null,
                      mesConstants.COMM_TYPE_APP_FEES,
                      mesConstants.COMM_TYPE_NONE,
                      null ),
                      
    new ChargeRecord( "%INTERNET%SETUP%",
                      null,
                      mesConstants.COMM_TYPE_ECOMMERCE_SETUP_FEES,
                      mesConstants.COMM_TYPE_NONE,
                      null ),
                      
    new ChargeRecord( "%INTERNET%MONTH%",
                      null,
                      mesConstants.COMM_TYPE_ECOMMERCE_MONTHLY_FEES,
                      mesConstants.COMM_TYPE_NONE,
                      null ),
  };
};
