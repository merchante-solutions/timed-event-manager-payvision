package com.mes.logging;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

public class SensitiveDataFilteringLayout extends PatternLayout {
  private static final String MASK_CN = "$1xxxxxx$3";
  private static final Pattern PATTERN_CN = Pattern.compile("([0-9]{6})([0-9]{6,10})([0-9]{4})");
  private static final String MASK_PK = "$1xxxxxx$3";
  private static final Pattern PATTERN_PK = Pattern.compile("(profileKey:\\s*)([a-zA-Z0-9]{28})([a-zA-Z0-9]{4})");
  
  @Override
  public String format(LoggingEvent event) {
    boolean reformat = false;
    
    if (event.getMessage() instanceof String) {
      String message = event.getRenderedMessage();

      Matcher matcher_cn = PATTERN_CN.matcher(message);      
      if (matcher_cn.find()) {
        message = matcher_cn.replaceAll(MASK_CN);
        reformat = true;
      }
      Matcher matcher_pk = PATTERN_PK.matcher(message);
      if (matcher_pk.find()) {
        message = matcher_pk.replaceAll(MASK_PK);
        reformat = true;
      }

      if (reformat)
      {
        Throwable throwable = event.getThrowableInformation() != null ?
                              event.getThrowableInformation().getThrowable() : null;
        LoggingEvent maskedEvent = new LoggingEvent(
                    event.fqnOfCategoryClass,
                    Logger.getLogger(event.getLoggerName()), 
                    event.timeStamp, 
                    event.getLevel(), 
                    message, 
                    throwable);

        return super.format(maskedEvent);
      } 
    }

    return super.format(event);
  }
}