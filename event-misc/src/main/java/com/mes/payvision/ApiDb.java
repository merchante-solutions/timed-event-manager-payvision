package com.mes.payvision;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;

public class ApiDb extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(ApiDb.class);

  private void saveResponse(ApiResponse response) throws Exception
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      ps = con.prepareStatement(
        " insert into payvision_api_responses ( " +
        "  request_id,                          " +
        "  request_ts,                          " +
        "  request_type,                        " +
        "  endpoint_addr,                       " +
        "  user_name,                           " +
        "  cr_member_id,                        " +
        "  cr_member_guid,                      " +
        "  member_id,                           " +
        "  start_ts,                            " +
        "  end_ts,                              " +
        "  result_code,                         " +
        "  result_message )                     " +
        " values (                              " +
        "  ?, ?, ?, ?, ?, ?,                    " +
        "  ?, ?, ?, ?, ?, ?  )                  ");

      ps.setLong      (1, response.getRequestId());
      ps.setTimestamp (2, response.getRequestTs());
      ps.setString    (3, response.getRequestType());
      ps.setString    (4, response.getHostAddr() + response.getEndpoint());
      ps.setString    (5, response.getUserName());
      ps.setInt       (6, response.getCrMemberId());
      ps.setString    (7, response.getCrMemberGuid());
      ps.setInt       (8, response.getMemberId());
      ps.setTimestamp (9, response.getStartTs());
      ps.setTimestamp (10,response.getEndTs());
      ps.setString    (11,""+response.getCode());
      ps.setString    (12,response.getMessage());
      ps.executeUpdate();
    }
    catch (Exception e)
    {
      throw new Exception("Error saving response record: " + e,e);
    }
    finally
    {
      try { ps.close(); } catch (Exception ie) { }
      cleanUp();
    }
  }

  public void saveChargebackRecords(ChargebackResponse response)
  {
    PreparedStatement ps1 = null;
    PreparedStatement ps2 = null;
    PreparedStatement ps3 = null;
    ResultSet rs = null;

    try
    {
      connect();

      saveResponse(response);

      ps1 = con.prepareStatement(
        " insert into payvision_api_cb_records (  " +
        "  request_id,                            " +
        "  member_id,                             " +
        "  cb_id,                                 " +
        "  case_id,                               " +
        "  cb_type_id,                            " +
        "  amount,                                " +
        "  currency_code,                         " +
        "  proc_date,                             " +
        "  reason_code,                           " +
        "  card_id,                               " +
        "  card_guid,                             " +
        "  tran_date,                             " +
        "  tran_id,                               " +
        "  merch_acct_id )                        " +
        " values (                                " +
        "  ?, ?, ?, ?, ?, ?, ?,                   " +
        "  ?, ?, ?, ?, ?, ?, ?  )                 ");

      ps2 = con.prepareStatement(
        " insert into payvision_api_cb_entries (  " +
        "  request_id,                            " +
        "  member_id,                             " +
        "  cb_id,                                 " +
        "  key,                                   " +
        "  value )                                " +
        " values (                                " +
        "  ?, ?, ?, ?, ? )                        ");

      ps3 = con.prepareStatement(
        " select * from payvision_api_cb_records  " +
        " where cb_id = ?                         ");

      for (Iterator i = response.getRecords().iterator(); i.hasNext();)
      {
        ChargebackRecord record = (ChargebackRecord)i.next();

        ps3.setLong(1,record.getCbId());
        rs = ps3.executeQuery();
        if (rs.next())
        {
          log.debug("skipping duplicate cb id: " + record.getCbId());
          continue;
        }

        ps1.setLong       (1, record.getRequestId());
        ps1.setInt        (2, record.getMemberId());
        ps1.setLong       (3, record.getCbId());
        ps1.setString     (4, record.getCaseId());
        ps1.setInt        (5, record.getCbTypeId());
        ps1.setBigDecimal (6, record.getAmount());
        ps1.setInt        (7, record.getCurrencyCode());
        ps1.setTimestamp  (8, record.getProcTs());
        ps1.setString     (9, record.getReasonCode());
        ps1.setLong       (10,record.getCardId());
        ps1.setString     (11,record.getCardGuid());
        ps1.setTimestamp  (12,record.getTranTs());
        ps1.setLong       (13,record.getTranId());
        ps1.setLong       (14,record.getMerchAcctId());
        ps1.executeUpdate();
        for (Iterator j = record.getEntryItems().iterator(); j.hasNext();)
        {
          ChargebackEntryItem item = (ChargebackEntryItem)j.next();
          ps2.setLong   (1,item.getRequestId());
          ps2.setInt    (2,item.getMemberId());
          ps2.setLong   (3,item.getCbId());
          ps2.setString (4,item.getKey());
          ps2.setString (5,item.getValue());
          ps2.executeUpdate();
        }
      }
    }
    catch (Exception e)
    {
      log.error("Error saving chargebacks: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch (Exception ie) { }
      try { ps1.close(); } catch (Exception ie) { }
      try { ps2.close(); } catch (Exception ie) { }
      try { ps3.close(); } catch (Exception ie) { }
      cleanUp();
    }

  }

  public List loadChargeBackRecords(Date fromDate, Date toDate)
  {
    try
    {
      return new ArrayList();
    }
    catch (Exception e)
    {
    }
    finally
    {
    }

    return null;
  }
}