package com.mes.startup;

import org.apache.log4j.Logger;
import com.mes.aus.AusDb;

public final class AusCleanupDbEvent extends EventBase
{
  static Logger log = Logger.getLogger(AusCleanupDbEvent.class);

  public boolean execute()
  {
    try
    {
      (new AusDb(true)).cleanupDb();
      return true;
    }
    catch (Exception e)
    {
      log.error("AUS Database Cleanup Error: " + e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }

    return false;
  }
}