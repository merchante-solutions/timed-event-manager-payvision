/*@lineinfo:filename=AmexConversion*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/ChargebackUpload.sqlj $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2010-07-22 15:16:29 -0700 (Thu, 22 Jul 2010) $
  Version            : $Revision: 17600 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;

public class AmexConversion extends EventBase
{
  public boolean execute()
  {
    Vector  accts = new Vector();
    
    ResultSetIterator it = null;
    ResultSet         rs = null;
    try
    {
      connect();
      
      // see if there are items to process
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:47^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    amex_merchants_2
//          where   process_sequence = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    amex_merchants_2\n        where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.AmexConversion",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:53^7*/
      
      if( recCount > 0 )
      {
        int processSequence = -1;
        
        // get new process sequence
        /*@lineinfo:generated-code*//*@lineinfo:60^9*/

//  ************************************************************
//  #sql [Ctx] { select  amex_esa_setup_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  amex_esa_setup_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.AmexConversion",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   processSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:65^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:67^9*/

//  ************************************************************
//  #sql [Ctx] { update  amex_merchants_2
//            set     process_sequence = :processSequence
//            where   process_sequence = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  amex_merchants_2\n          set     process_sequence =  :1 \n          where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.AmexConversion",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:72^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:74^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  am2.merchant_number as merchant_number,
//                    am2.discount_rate   as discount_rate,
//                    am2.new_se_number   as se_number
//            from    amex_merchants_2 am2,
//                    mif mf
//            where   am2.process_sequence = :processSequence
//                    and am2.merchant_number = mf.merchant_number
//                    and nvl(mf.mes_amex_processing,'N') = 'N'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  am2.merchant_number as merchant_number,\n                  am2.discount_rate   as discount_rate,\n                  am2.new_se_number   as se_number\n          from    amex_merchants_2 am2,\n                  mif mf\n          where   am2.process_sequence =  :1 \n                  and am2.merchant_number = mf.merchant_number\n                  and nvl(mf.mes_amex_processing,'N') = 'N'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.AmexConversion",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.AmexConversion",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^9*/
        
        rs = it.getResultSet();
      
        while(rs.next())
        {
          accts.add( new AmexMerchant(rs) );
        }
      
        rs.close();
        it.close();
      
        for( int i = 0; i < accts.size(); ++i )
        {
          AmexMerchant am = (AmexMerchant)accts.elementAt(i);
        
          /*@lineinfo:generated-code*//*@lineinfo:100^11*/

//  ************************************************************
//  #sql [Ctx] { call convert_amex_merchant( :am.merchantNumber, :am.discountRate, :am.seNumber )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN convert_amex_merchant(  :1 ,  :2 ,  :3  )\n          \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.AmexConversion",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,am.merchantNumber);
   __sJT_st.setDouble(2,am.discountRate);
   __sJT_st.setLong(3,am.seNumber);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:103^11*/
        }
      
        commit();
      
        /*@lineinfo:generated-code*//*@lineinfo:108^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  lpad(to_char(am2.merchant_number), 12, ' ')         merchant_number,
//                    rpad(mf.dba_name, 25, ' ')                          dba_name,
//                    am2.new_se_number                                   se_number,
//                    ltrim(rtrim(to_char(am2.discount_rate, '$99.00')))  discount_rate
//            from    amex_merchants_2 am2,
//                    mif mf
//            where   am2.process_sequence = :processSequence
//                    and am2.merchant_number = mf.merchant_number
//                    and mf.mes_amex_processing = 'Y'
//            order by am2.merchant_number                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  lpad(to_char(am2.merchant_number), 12, ' ')         merchant_number,\n                  rpad(mf.dba_name, 25, ' ')                          dba_name,\n                  am2.new_se_number                                   se_number,\n                  ltrim(rtrim(to_char(am2.discount_rate, '$99.00')))  discount_rate\n          from    amex_merchants_2 am2,\n                  mif mf\n          where   am2.process_sequence =  :1 \n                  and am2.merchant_number = mf.merchant_number\n                  and mf.mes_amex_processing = 'Y'\n          order by am2.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.AmexConversion",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.AmexConversion",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^9*/
        
        rs = it.getResultSet();
        
        // build email
        StringBuffer msgBody = new StringBuffer("");
        
        msgBody.append("Merchant #   SE #       DBA Name                  Rate\n");
        msgBody.append("------------ ---------- ------------------------- -----\n");
        
        while( rs.next() )
        {
          msgBody.append(rs.getString("merchant_number"));
          msgBody.append(" ");
          msgBody.append(rs.getString("se_number"));
          msgBody.append(" ");
          msgBody.append(rs.getString("dba_name"));
          msgBody.append(" ");
          msgBody.append(rs.getString("discount_rate"));
          msgBody.append("\n");
        }
        
        MailMessage msg = new MailMessage();
        msg.setAddresses(MesEmails.MSG_ADDRS_AMEX_CONVERSIONS);
        msg.setSubject("Merchants converted to MES Amex Funding (" + com.mes.support.DateTimeFormatter.getCurDateTimeString() + ")");
        msg.setText(msgBody.toString());
        msg.send();
      }
      
      return( true );
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( true );
  }
  
  public class AmexMerchant
  {
    public long   merchantNumber = 0L;
    public double discountRate = 0.0;
    public long   seNumber = 0L;
    
    public AmexMerchant( ResultSet rs )
      throws Exception
    {
      merchantNumber  = rs.getLong("merchant_number");
      discountRate    = rs.getDouble("discount_rate");
      seNumber        = rs.getLong("se_number");
    }
  }
  
  public static void main(String[] args)
  {
    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
      
      AmexConversion ac = new AmexConversion();
    
      ac.execute();
    }
    catch(Exception e)
    {
      System.out.println(e.toString());
    }
  }
}/*@lineinfo:generated-code*/