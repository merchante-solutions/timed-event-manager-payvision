/*@lineinfo:filename=MonthlySalesTaxUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.Date;
import java.util.Calendar;
import com.mes.api.avalara.salesData.SalesDataBean;
import com.mes.config.MesDefaults;
import com.mes.support.DateTimeFormatter;

public class MonthlySalesTaxUpload extends EventBase {

  private Date manualActiveDate = null;

  public boolean execute() {
    Date activeDate = null;
    Date monthEndDate = null;
    Calendar cal = Calendar.getInstance();

    try {
      if (manualActiveDate == null) {
        //default is previous month of current date
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        activeDate = new java.sql.Date(cal.getTime().getTime());
      } else {
        // use user provided date and roll back to previous month
        cal.setTime(manualActiveDate);
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        activeDate = new java.sql.Date(cal.getTime().getTime());
      }

      // MBS uses calendar month as the billing month
      cal.setTime(activeDate);
      cal.add(Calendar.MONTH, 1);
      cal.set(Calendar.DAY_OF_MONTH, 1);
      cal.add(Calendar.DAY_OF_MONTH, -1);
      monthEndDate = new java.sql.Date(cal.getTime().getTime());
      log.info("Montlhy Sales Tax Upload for the period  " + activeDate.toString() + " to " + monthEndDate.toString());
      loadSalesTaxDataInAvalara(activeDate, monthEndDate);
    } catch (Exception e) {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    } 
    return (true);
  }

  public Date getManualActiveDate() {
    return manualActiveDate;
  }

  public void setManualActiveDate(Date manualActiveDate) {
    this.manualActiveDate = manualActiveDate;
  }

  private void loadSalesTaxDataInAvalara(Date activeDate, Date monthEndDate) {
    log.info("Calling loadData() of SalesDataBean");    
    SalesDataBean sb = new SalesDataBean();
    sb.loadData(activeDate, monthEndDate);    
  }

  public static void main(String[] args) {

    try {
      if (args.length > 0 && args[0].equals("testproperties")) {
        EventBase.printKeyListStatus(new String[]{
                MesDefaults.GCF_ORDER_FILE_FTP_ARC_HOST,
                MesDefaults.GCF_ORDER_FILE_FTP_ARC_PORT,
                MesDefaults.GCF_ORDER_FILE_FTP_ARC_USER,
                MesDefaults.GCF_ORDER_FILE_FTP_ARC_PASSWORD,
                MesDefaults.GCF_ORDER_FILE_INTERNAL_USERS,
                MesDefaults.DK_GCF_FTPS_IP_ADDRESS,
                MesDefaults.DK_GCF_FTPS_PORT,
                MesDefaults.DK_GCF_USER,
                MesDefaults.DK_GCF_PASSWORD,
                MesDefaults.DK_GCF_IP_ADDRESS,

        });
      }

    loadTEMConfiguration();
    MonthlySalesTaxUpload mst = new MonthlySalesTaxUpload();
    // If MM/dd/yyyy is 01/01/2015, it will execute for month of december.
    //sample arguments "execute 01/01/2015"
    if (args.length > 0 && "execute".equals(args[0])) {
      mst.setManualActiveDate((args.length > 1) ? new Date(DateTimeFormatter
          .parseDate(args[1], "MM/dd/yyyy").getTime()) : null);
      mst.execute();
    } else {
      System.out.println("Invalid Command");
    }

    } catch (Exception e) {
      log.error(e.toString());
    }
  }

}/*@lineinfo:generated-code*/