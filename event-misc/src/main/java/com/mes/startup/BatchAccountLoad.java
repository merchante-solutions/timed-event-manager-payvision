/*@lineinfo:filename=BatchAccountLoad*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/BatchAccountLoad.sqlj $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/19/04 9:37a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.flatfile.MASTape;
import com.mes.net.FTPClient;
import com.mes.net.FTPTransferType;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class BatchAccountLoad extends EventBase
{
  protected Vector  bankFiles = new Vector();
  /*
  ** METHOD execute()
  **
  ** creates tape files for all necessary bank numbers
  */
  public boolean execute()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;
    
    try
    {
      connect();
      
      // get list of banks for which to create files
      /*@lineinfo:generated-code*//*@lineinfo:57^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  class_name
//          from    mas_banks
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  class_name\n        from    mas_banks";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.BatchAccountLoad",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.BatchAccountLoad",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:61^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        bankFiles.add(rs.getString("class_name"));
      }
      
      rs.close();
      it.close();
      
      for(int i=0; i < bankFiles.size(); ++i)
      {
        processTapeFile((String)(bankFiles.elementAt(i)));
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      
      cleanUp();
    }
    
    return result;
  }
  
  protected void processTapeFile(String className)
  {
    try
    {
      // instantiate specified class
      MASTape mt = (MASTape)Class.forName(className).newInstance();
      
      StringBuffer newFileName = new StringBuffer("");
      
      // build file
      if(mt.buildMASTape())
      {
        String masFile = mt.getMasFile();
        
        // generate new file name for processing and archiving
        newFileName.append("bal");
        newFileName.append(mt.getBankNumber());
        newFileName.append("_");
        newFileName.append(DateTimeFormatter.getCurDateString("MMddyy"));
      
        // send file to sna server for processing
        String ftpHost      = MesDefaults.getString(MesDefaults.DK_SNA_FTP_ADDRESS);
        String ftpUser      = MesDefaults.getString(MesDefaults.DK_SNA_FTP_USER);
        String ftpPassword  = MesDefaults.getString(MesDefaults.DK_SNA_FTP_PASSWORD);
      
        FTPClient ftp = new FTPClient(ftpHost, 21);
        ftp.login(ftpUser, ftpPassword);
        ftp.setType(FTPTransferType.ASCII);
      
        ftp.put(masFile.getBytes(), newFileName.toString() + ".dat");
      
        // send flag file to sna server
        ftp.put((new String("Flag File")).getBytes(), newFileName.toString() + ".flg");
      
        // save file to disk for archiving purposes
        try
        {
          BufferedWriter out = new BufferedWriter(new FileWriter("./tapefile/" + newFileName.toString() + ".dat"));
          out.write(masFile);
          out.close();
        }
        catch(Exception be)
        {
          logEntry("processTapeFile()", be.toString());
        }
      }
      
      // notify
      StringBuffer message  = new StringBuffer("");
      StringBuffer subject  = new StringBuffer("");
      
      if(mt.getFileSuccess())
      {
        // success
        subject.append("MAS Upload Success");
        
        message.append("MAS file ");
        message.append(newFileName);
        message.append(".dat processed successfully.");
      }
      else
      {
        // failure
        subject.append("MAS Upload Failure");
        
        message.append("MAS file ");
        message.append(newFileName);
        message.append(".dat failed to process.\n\n");
        
        message.append("Errors:\n");
        
        Vector errors = mt.getErrorList();
        
        for(int i=0; i < errors.size(); ++i)
        {
          message.append("  ");
          message.append((String)(errors.elementAt(i)));
          message.append("\n");
        }
      }
      
      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_MAS_TAPE_UPLOAD);
      msg.setSubject(subject.toString());
      msg.setText(message.toString());
      msg.send();
    }
    catch(Exception e) 
    {
      logEntry("processTapeFile(" + className + ")", e.toString());
    }
  }
}/*@lineinfo:generated-code*/