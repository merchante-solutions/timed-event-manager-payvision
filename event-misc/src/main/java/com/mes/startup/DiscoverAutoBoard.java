/*@lineinfo:filename=DiscoverAutoBoard*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/DiscoverAutoBoard.sqlj $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-11-12 15:27:32 -0800 (Thu, 12 Nov 2015) $
  Version            : $Revision: 23997 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;

public class DiscoverAutoBoard extends TsysFileBase
{
  static Logger log = Logger.getLogger(DiscoverAutoBoard.class);
  
  public boolean testMode = false;
  
  private static final String newLine = System.getProperty("line.separator").toString();
  private static final String delimiter = "~";
  
  private ArrayList<String> recordTypes = new ArrayList<String>(
      Arrays.asList("01","04","05","06","07","08","10","11","12","20","21","22"));
  
  public DiscoverAutoBoard( )
  {
    PropertiesFilename = "discover-registration.properties";
    EmailGroupSuccess = MesEmails.MSG_ADDRS_OUTGOING_DISCOVER_PROFILE_NOTIFY;
    EmailGroupFailure = MesEmails.MSG_ADDRS_OUTGOING_DISCOVER_PROFILE_FAILURE;
  }
  
  private boolean buildDiscoverFile( int procSeq )
  {
    String            workFilename  = null;
  
    BufferedWriter    out           = null;
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    
    FlatFileRecord    ffd           = null;
    
    boolean           result        = true;
    
    try
    {
      connect();
      
      workFilename = buildFilename( "disc-reg3941" );
      log.debug("building file: " + workFilename);
      out = new BufferedWriter( new FileWriter( workFilename, false ) );
      
      // build header
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_PROFILE_HEADER_03152);

      log.debug("building header record");
      /*@lineinfo:generated-code*//*@lineinfo:90^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(systimestamp, 'YYYY-MM-dd-hh24.mi.ss.ff') as time_stamp
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(systimestamp, 'YYYY-MM-dd-hh24.mi.ss.ff') as time_stamp\n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.DiscoverAutoBoard",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.DiscoverAutoBoard",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^7*/
      
      rs = it.getResultSet();
      
      if( rs.next() )
      {
        ffd.resetAllFields();
        
        ffd.setAllFieldData( rs );
        
        out.write( ffd.spew( "~" ) );
        out.newLine();
      }
      
      rs.close();
      it.close();
      
      // build detail records
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_PROFILE_DETAIL_03152);
      log.debug("getting detail records");
      /*@lineinfo:generated-code*//*@lineinfo:114^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dab.action                    as record_type,
//                  to_char(mb.ds_acquirer_id)    as acquirer_id,
//                  '01'                          as merchant_type,
//                  mf.dmdsnum                    as discover_merchant_number,
//                  mf.dba_name                   as dba_name,
//                  decode(mf.bank_number,
//                    3858, mf.addr1_line_1,
//                    mf.dmaddr)                  as dba_address_1,
//                  decode(mf.bank_number,
//                    3858, mf.addr1_line_2,
//                    mf.address_line_3)          as dba_address_2,
//                  decode(mf.bank_number,
//                    3858, mf.city1_line_4,
//                    mf.dmcity)                  as dba_city,
//                  decode(mf.bank_number,
//                    3858, mf.state1_line_4,
//                    mf.dmstate)                 as dba_state,
//                  decode(mf.bank_number,
//                    3858, substr(mf.zip1_line_4,1,5),
//                    substr(mf.dmzip,1,5))       as dba_zip,
//                  decode(mf.dmstate,
//                    'PR', 'PRI',
//                    'USA')                      as dba_country,
//                  mf.phone_1                    as dba_phone,
//                  --mc.merchcont_prim_first_name  as contact_first_name,
//                  --mc.merchcont_prim_last_name   as contact_last_name,
//                  ltrim(rtrim(
//                    substr(mf.owner_name, 1, instr(mf.owner_name, ' '))
//                    ))                          as principal_first_name,
//                  ltrim(rtrim(
//                    substr(mf.owner_name, instr(mf.owner_name, ' '), length(mf.owner_name))
//                    ))                          as principal_last_name,
//                  nvl(bo.busowner_title,
//                    'OWNER')                    as principal_title,
//                  ba.address_line1              as principal_address_1,
//                  ba.address_line2              as principal_address_2,
//                  decode(ba.address_line1,
//                    null, null,
//                    ba.address_city)            as principal_city,
//                  decode(ba.address_line1,
//                    null, null,
//                    ba.countrystate_code)       as principal_state,
//                  substr(decode(ba.address_line1,
//                    null, null,
//                    ba.address_zip),1,5)        as principal_zip,
//                  decode(ba.address_line1,
//                    null, null,
//                    decode(ba.countrystate_code, 'PR', 'PRI',
//                    'USA'))                     as principal_country,
//                  lpad(nvl(dukpt_decrypt_wrapper(bo.busowner_ssn_enc), bo.busowner_ssn),9,'0')
//                                                as principal_ssn,
//                  lpad(mf.federal_tax_id,9,'0') as tax_id_number,
//                  mf.fdr_corp_name              as legal_name,
//                  mf.addr1_line_1               as business_address_1,
//                  mf.addr1_line_2               as business_address_2,
//                  mf.city1_line_4               as business_city,
//                  mf.state1_line_4              as business_state,
//                  substr(mf.zip1_line_4,1,5)    as business_zip,
//                  decode(mf.state1_line_4,
//                    'PR', 'PRI',
//                    'USA')                      as business_country,
//                  mf.sic_code                   as mcc,
//                  decode(mr.bustype_code,
//                    2, 'C',
//                    3, 'P',
//                    4, 'C',
//                    5, 'N',
//                    6, 'N',
//                    7, 'G',
//                    8, 'L',
//                    'S')                        as business_type_desc,
//                  to_char(dab.date_created, 
//                    'yyyy-mm-dd')               as merchant_status_date,
//                  decode(mf.dmacctst,
//                    null, '0001',
//                    'Z',  '0001',
//                    '1009')                     as merchant_status,
//                  null                          as merchant_website_address,
//                  mr.merch_email_address        as merchant_dba_email_address,
//                  --mf.old_discover_mid           as old_discover_mid,
//                  '05'                          as pos_device_download_indicator,
//                  2                             as network_indicator
//          from    discover_map_auto_board   dab,
//                  mif                       mf,
//                  mbs_banks                 mb,
//                  merchant                  mr,
//                  merchcontact              mc,
//                  businessowner             bo,
//                  address                   ba
//          where   dab.process_sequence = :procSeq
//                  and dab.merchant_number = mf.merchant_number
//                  and mf.bank_number = mb.bank_number
//                  and mf.merchant_number = mr.merch_number
//                  and mr.app_seq_num = mc.app_seq_num(+)
//                  and mr.app_seq_num = bo.app_seq_num(+)
//                  and bo.busowner_num(+) = 1
//                  and mr.app_seq_num = ba.app_seq_num(+)
//                  and ba.addresstype_code(+) = 4        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dab.action                    as record_type,\n                to_char(mb.ds_acquirer_id)    as acquirer_id,\n                '01'                          as merchant_type,\n                mf.dmdsnum                    as discover_merchant_number,\n                mf.dba_name                   as dba_name,\n                decode(mf.bank_number,\n                  3858, mf.addr1_line_1,\n                  mf.dmaddr)                  as dba_address_1,\n                decode(mf.bank_number,\n                  3858, mf.addr1_line_2,\n                  mf.address_line_3)          as dba_address_2,\n                decode(mf.bank_number,\n                  3858, mf.city1_line_4,\n                  mf.dmcity)                  as dba_city,\n                decode(mf.bank_number,\n                  3858, mf.state1_line_4,\n                  mf.dmstate)                 as dba_state,\n                decode(mf.bank_number,\n                  3858, substr(mf.zip1_line_4,1,5),\n                  substr(mf.dmzip,1,5))       as dba_zip,\n                decode(mf.dmstate,\n                  'PR', 'PRI',\n                  'USA')                      as dba_country,\n                mf.phone_1                    as dba_phone,\n                --mc.merchcont_prim_first_name  as contact_first_name,\n                --mc.merchcont_prim_last_name   as contact_last_name,\n                ltrim(rtrim(\n                  substr(mf.owner_name, 1, instr(mf.owner_name, ' '))\n                  ))                          as principal_first_name,\n                ltrim(rtrim(\n                  substr(mf.owner_name, instr(mf.owner_name, ' '), length(mf.owner_name))\n                  ))                          as principal_last_name,\n                nvl(bo.busowner_title,\n                  'OWNER')                    as principal_title,\n                ba.address_line1              as principal_address_1,\n                ba.address_line2              as principal_address_2,\n                decode(ba.address_line1,\n                  null, null,\n                  ba.address_city)            as principal_city,\n                decode(ba.address_line1,\n                  null, null,\n                  ba.countrystate_code)       as principal_state,\n                substr(decode(ba.address_line1,\n                  null, null,\n                  ba.address_zip),1,5)        as principal_zip,\n                decode(ba.address_line1,\n                  null, null,\n                  decode(ba.countrystate_code, 'PR', 'PRI',\n                  'USA'))                     as principal_country,\n                lpad(nvl(dukpt_decrypt_wrapper(bo.busowner_ssn_enc), bo.busowner_ssn),9,'0')\n                                              as principal_ssn,\n                lpad(mf.federal_tax_id,9,'0') as tax_id_number,\n                mf.fdr_corp_name              as legal_name,\n                mf.addr1_line_1               as business_address_1,\n                mf.addr1_line_2               as business_address_2,\n                mf.city1_line_4               as business_city,\n                mf.state1_line_4              as business_state,\n                substr(mf.zip1_line_4,1,5)    as business_zip,\n                decode(mf.state1_line_4,\n                  'PR', 'PRI',\n                  'USA')                      as business_country,\n                mf.sic_code                   as mcc,\n                decode(mr.bustype_code,\n                  2, 'C',\n                  3, 'P',\n                  4, 'C',\n                  5, 'N',\n                  6, 'N',\n                  7, 'G',\n                  8, 'L',\n                  'S')                        as business_type_desc,\n                to_char(dab.date_created, \n                  'yyyy-mm-dd')               as merchant_status_date,\n                decode(mf.dmacctst,\n                  null, '0001',\n                  'Z',  '0001',\n                  '1009')                     as merchant_status,\n                null                          as merchant_website_address,\n                mr.merch_email_address        as merchant_dba_email_address,\n                --mf.old_discover_mid           as old_discover_mid,\n                '05'                          as pos_device_download_indicator,\n                2                             as network_indicator\n        from    discover_map_auto_board   dab,\n                mif                       mf,\n                mbs_banks                 mb,\n                merchant                  mr,\n                merchcontact              mc,\n                businessowner             bo,\n                address                   ba\n        where   dab.process_sequence =  :1 \n                and dab.merchant_number = mf.merchant_number\n                and mf.bank_number = mb.bank_number\n                and mf.merchant_number = mr.merch_number\n                and mr.app_seq_num = mc.app_seq_num(+)\n                and mr.app_seq_num = bo.app_seq_num(+)\n                and bo.busowner_num(+) = 1\n                and mr.app_seq_num = ba.app_seq_num(+)\n                and ba.addresstype_code(+) = 4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.DiscoverAutoBoard",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSeq);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.DiscoverAutoBoard",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:214^7*/
      
      rs = it.getResultSet();
      
      int recordCount = 0;
      while( rs.next() )
      {
        ++recordCount;
        ffd.resetAllFields();
        
        ffd.setAllFieldData( rs );
        ffd.setFieldData("record_sequence_number", recordCount);
        
        out.write( ffd.spew( "~" ) );
        out.newLine();
        
      }
      
      rs.close();
      it.close();
      
      // trailer record
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_PROFILE_TRAILER_03152);

      log.debug("building trailer record");        
      /*@lineinfo:generated-code*//*@lineinfo:239^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  :recordCount  as record_count
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1   as record_count\n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.DiscoverAutoBoard",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,recordCount);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.DiscoverAutoBoard",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^7*/
      
      rs = it.getResultSet();
      
      if( rs.next() )
      {
        ffd.resetAllFields();
        
        ffd.setAllFieldData( rs );
        
        out.write( ffd.spew("~") );
        out.newLine();
      }
      
      rs.close();
      it.close();
      
      out.flush();
      out.close();
      
      // send file (if not empty)
      if( recordCount > 0 && testMode != true )
      {
        log.debug("transmitting file to Discover");
        
        boolean success = 
          sendDataFile( workFilename, 
                        MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PROFILE_HOST),
                        MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PROFILE_USER),
                        MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PROFILE_PW),
                        MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PROFILE_PATH),
                        false,
                        true );
        
        if( success == true )
        {
          log.debug("archiving file");
          archiveDailyFile(workFilename);
        }
        else
        {
          log.debug("*** transmission failed ***");
          result = false;
        }
        
        sendEmail(success, workFilename);
      }
    }
    catch(Exception e)
    {
      logEntry("buildDiscoverFile(" + procSeq + ")", e.toString());
      result = false;
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }
   
  private boolean transmitBoardingFile()
  {
    boolean           result        = true;
    
    try
    {
      connect();
      
      // mark items to process if present
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:317^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    discover_map_auto_board
//          where   process_sequence = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    discover_map_auto_board\n        where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.DiscoverAutoBoard",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:323^7*/
      
      if( recCount > 0 )
      {
        // mark items
        int procSeq = 0;
        /*@lineinfo:generated-code*//*@lineinfo:329^9*/

//  ************************************************************
//  #sql [Ctx] { select  wf_auto_board_seq.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  wf_auto_board_seq.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.DiscoverAutoBoard",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSeq = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:334^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:336^9*/

//  ************************************************************
//  #sql [Ctx] { update  discover_map_auto_board
//            set     process_sequence = :procSeq
//            where   process_sequence = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  discover_map_auto_board\n          set     process_sequence =  :1 \n          where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.DiscoverAutoBoard",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSeq);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:341^9*/
        
        result = buildDiscoverFile( procSeq );
      }
    }
    catch(Exception e)
    {
      logEntry("transmitBoardingFile()", e.toString());
      result = false;
    }
    finally
    {
      cleanUp();
    }
    
    return( result );
  }
  
  private boolean retrieveBoardingResponses()
  {
    log.debug("retrieving profile response file(s)");
    
    try
    {
      String host = MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PROFILE_HOST);
      String user = MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_USER);
      String pw   = MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_PW);
      String path = MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_PATH);
      
      Sftp sftp = null;
      
      sftp = getSftp( host, user, pw, path, false);
      
      SimpleDateFormat sfm = new SimpleDateFormat("yyyyDDD");
	  String fileMaskDate = sfm.format(new java.util.Date()); 
	  String fileMask = null;
	 
	  fileMask = MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_RESPONSE_FILE_MASK) + ".J" +fileMaskDate + ".*";
	  
	  EnumerationIterator enumi = new EnumerationIterator();
      Iterator it = enumi.iterator(sftp.getNameListing(fileMask));
      
      String fileName = null;
      
      while( it.hasNext() )
      {
        fileName = (String)it.next();
        
        log.debug("found file: " + fileName);
        
        log.debug("retrieving file");
        // retrieve this file
        byte[] respFile = ftpGet(fileName, null, host, user, pw, path, false);
        
        // generate filename for this file
        String outFilename = buildFilename( "disc_resp3941" );
        
        log.debug("writing response file: " + outFilename);
        
        FileWriter out = null;
        try
        {
          out = new FileWriter(outFilename);
        
          String data = new String(respFile);
          
          out.write( data );
          
          out.flush();
          out.close();
          
          // send file in email
          MailMessage msg = new MailMessage();
          
          msg.setAddresses( EmailGroupSuccess );
          msg.setSubject("Discover Profile Response File - " + outFilename);
          msg.setText("Response file retrieved from Discover\n\n" + outFilename);
          
          msg.addStringAsTextFile(outFilename, data);
          
          String reviewedData = getReviewedData(outFilename);
          SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyy");
          String dateStr = dateFormat.format(new java.util.Date());
          try {
             msg.addStringAsTextFile("disc_for_review_"+dateStr+".dat", reviewedData);
          } catch (Exception e) {
            logEntry("Error in retrieveBoardingResponses() - creation of disc_for_review_"+dateStr+".dat file", e.toString());
          }
          
          msg.send();
          
          // archive file
          archiveDailyFile(outFilename);
        }
        catch(Exception fe)
        {
          logEntry("retrieveBoardingResponses() - writing file", fe.toString());
        }
        finally
        {
          try{ out.flush(); out.close(); } catch(Exception se) {}
        }
      }
    }
    catch(Exception e)
    {
      logEntry("retrieveBoardingRepsonse()", e.toString());
    }
    
    return( true );
  }
  
  private boolean isRecordTypeValid(String recordType) {
    
    for( int i = 0; i < recordTypes.size(); ++i ) {
      String validRecordType = (String)recordTypes.get(i);
      
      if (recordType.equals(validRecordType)) {
        return true;
      }
    }

    return false;
  }
  
  public String getReviewedData(String outFilename) {
    StringBuffer reviewedData = new StringBuffer("");
    String  line = null;
//    int acquirerId = 0, isoId = 0; 
    String recordType, dbaName = "";
    StringBuffer errors = new StringBuffer("");
    long discoverMerchantNumber = 0L, mesMerchantNumber = 0L;
    try {
      connect(true);
      //append the header
      reviewedData.append("Discover Merchant Number").append(delimiter)
                  .append("MES Merchant Number").append(delimiter)
                  .append("DBA Name").append(delimiter)
                  .append("Error List").append(newLine);
    
      BufferedReader in = new BufferedReader(new FileReader(outFilename));
      while ((line = in.readLine()) != null) {
        line = line.trim();
        if (line.length() > 2) {
          recordType = line.substring(0, 2);
          if (isRecordTypeValid(recordType)) {
            errors  = new StringBuffer("");
//             acquirerId = Integer.parseInt(line.substring(3, 14));
//             isoId = Integer.parseInt(line.substring(15, 26));
             discoverMerchantNumber = Long.parseLong(line.substring(27, 42));
             mesMerchantNumber = getMerchantNumberFromDmdsnum(discoverMerchantNumber);
             line = line.substring(43);
             if (recordType.equals("01") ||
                 recordType.equals("04") ||
                 recordType.equals("05") ||
                 recordType.equals("06") ||
                 recordType.equals("07") ||
                 recordType.equals("08") ||
                 recordType.equals("10") || recordType.equals("11") || recordType.equals("12")) {  
               int idx = line.indexOf("~");
               if (idx !=-1) {
                 dbaName = line.substring(0, idx);
                 int numberOfErrors = Integer.parseInt(line.substring(idx+1, idx+4));
                 line = line.substring(idx+5);
                 idx = 0;
                 for (int j = 0; j < numberOfErrors; j++, idx += 5) {
                   errors.append(getErrorDescription(line.substring(idx, idx+4)));
                   if (j < numberOfErrors-1) {
                     errors.append("; ");
                   }
                 }
               } else {
                 dbaName = line;
               }
             } else { //recordType.equals("20") || recordType.equals("21") || recordType.equals("22")
               dbaName = "<CMNF>"+line;
             }
             //create a detail record here
             reviewedData.append(discoverMerchantNumber).append(delimiter)
                         .append(mesMerchantNumber).append(delimiter)
                         .append(dbaName).append(delimiter)
                         .append(errors.toString()).append(newLine);
          }
        }
      }
      in.close();
    } catch (Exception e) {
      logEntry("Error in getReviewedData()", e.toString());
    } finally {
      cleanUp();
    }
    return reviewedData.toString();
  }
  
  private long getMerchantNumberFromDmdsnum(long discoverMerchantNumber) {
    long merchNumber  = 0L;
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    try {
//      // get an automated timeout exempt connection
//      connect(true);
      /*@lineinfo:generated-code*//*@lineinfo:542^7*/

//  ************************************************************
//  #sql [Ctx] it = { select merchant_number as merchantNumber
//            from  mif
//            where dmdsnum = :discoverMerchantNumber
//                  and nvl(dmacctst,'A') not in ( 'D','C','B' )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select merchant_number as merchantNumber\n          from  mif\n          where dmdsnum =  :1 \n                and nvl(dmacctst,'A') not in ( 'D','C','B' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.DiscoverAutoBoard",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,discoverMerchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.DiscoverAutoBoard",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:548^7*/
      rs = it.getResultSet();
      if( rs.next() )
        merchNumber = rs.getLong("merchantNumber");
      {
      }
      rs.close();
      it.close();

    }
    catch (Exception e) {
       logEntry("Error in getMerchantNumberFromDmdsnum() - discoverMerchantNumber: "+discoverMerchantNumber, e.toString());
    }
    finally {
//      cleanUp();
    }

    return merchNumber;
  }
  
  private String getErrorDescription(String errorCode) {
    String errorDescription = "";
    try {
      // get an automated timeout exempt connection
//      connect(true);
      /*@lineinfo:generated-code*//*@lineinfo:573^7*/

//  ************************************************************
//  #sql [Ctx] { select description
//            
//            from  reject_type
//            where id = :errorCode and type = 'DS'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select description\n           \n          from  reject_type\n          where id =  :1  and type = 'DS'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.DiscoverAutoBoard",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,errorCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   errorDescription = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:579^7*/
    }
    catch (Exception e) {
       logEntry("Error in getErrorDescription() - errorCode: "+errorCode, e.toString());
    }
    finally {
//      cleanUp();
    }
    return ("<"+errorCode+">"+(errorDescription==null?"":errorDescription));
  }
  
  public boolean execute( )
  {
    boolean   result  = false;
    String    action  = getEventArg(0);
    
    if( action != null )
    {
      if( ("SEND").equals(action) )
      {
        result = transmitBoardingFile();
      }
      
      if( ("RECEIVE").equals(action) )
      {
        result = retrieveBoardingResponses();
      }
    }
    
    return( result );
  }
  
  private void sendEmail(boolean success, String workFilename)
  {
    StringBuffer subject  = new StringBuffer("");
    StringBuffer body     = new StringBuffer("");
    
    try
    {
      subject.append( "Outgoing Discover MAP Profile Registration - " );
      
      body.append("Filename: ");
      body.append(workFilename);
      body.append("\n\n");
      
      if( success )
      {
        subject.append("SUCCESS");
      }
      else
      {
        subject.append("FAILURE");
      }
      
      MailMessage msg = new MailMessage();
 
      if(success)
      {
        msg.setAddresses( EmailGroupSuccess );
      }
      else
      {
        msg.setAddresses( EmailGroupFailure );
      }
      
      msg.setSubject(subject.toString());
      msg.setText(body.toString());
      msg.send();
    }
    catch( Exception e )
    {
      logEntry("sendEmail()", e.toString());
    }
  }
  
  public static void main(String[] args)
  {
    DiscoverAutoBoard worker = null;
    
    try
    {
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_DISCOVER_MAP_PROFILE_HOST,
                    MesDefaults.DK_DISCOVER_MAP_PROFILE_USER,
                    MesDefaults.DK_DISCOVER_MAP_PROFILE_PW,
                    MesDefaults.DK_DISCOVER_MAP_PROFILE_PATH
            });
        }


       com.mes.database.SQLJConnectionBase.initStandalonePool("DEBUG");
      
      worker = new DiscoverAutoBoard();
      worker.testMode = true;
      
      if( args.length == 0 )
      {
        worker.setEventArgs("SEND");
      
        worker.execute();
      }
      else
      {
        // rebuild file in test mode
        worker.buildDiscoverFile(Integer.parseInt(args[0]));
      }
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
    
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/