/*@lineinfo:filename=MerchPsp*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.net.MailMessage;
import com.mes.reports.MerchPspRecord;
import com.mes.support.CSVFileMemory;
import com.mes.support.DateTimeFormatter;
import com.mes.support.StringUtilities;
import sqlj.runtime.ScrollableResultSetIterator;

public class MerchPsp extends EventBase {

  static Logger log = Logger.getLogger(MerchPsp.class);
  ArrayList merchPspRows = new ArrayList();
  static String fromEmailAddress = null;
  static String toEmailAddress   = null;
  static String ccEmailAddress   = null;
  static String ccProjectManagerEmailAddress = null;
  static String ccComplianceManagerEmailAddress = null;
  static String FILE_NAME = null;

  CSVFileMemory csvMerchPspFile = null;
  
  private Date manualActiveDate = null;
  private String cardType = null;
  private int bankNumber = -1;
  
  final static int MERCHANT_NUMBER         = 1;
  final static int SPONSORED_MERCHANT_NAME = 2;
  final static int BUSINESS_ADDRESS1 = 3;
  final static int BUSINESS_ADDRESS3 = 4;
  final static int BUSINESS_CITY   = 5;
  final static int BUSINESS_STATE  = 6;
  final static int BUSINESS_ZIP  = 7;
  final static int MERCHANT_NAME = 8;
  final static int MCC_CODE    = 9;
  final static int PSP_NAME    = 10;
  final static int PSP_ID      = 11;
  final static int SALES_COUNT = 12;
  final static int SALES_AMOUNT = 13;
  final static int CHARGEBACK_COUNT  = 14;
  final static int CHARGEBACK_AMOUNT = 15;
  
  
  public boolean execute() {
    Date fromDate = null;
    Date toDate = null;

    Calendar cal = Calendar.getInstance();

    try {
    
      if (cardType == null) {
        cardType = getEventArg(0);
      }
      
      if (bankNumber == -1) {
        bankNumber = Integer.parseInt(getEventArg(1));
      }
      
      if (manualActiveDate == null) {
        //default is the previous month
        cal.add(Calendar.MONTH, -1);
      } else {
        // use user provided date
        cal.setTime(manualActiveDate);
      }
      
      cal.set(Calendar.DAY_OF_MONTH, 1);
      cal.set(Calendar.HOUR_OF_DAY, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MILLISECOND, 0);
      toDate = new java.sql.Date(cal.getTime().getTime());

      // MBS uses calendar month as the billing month
      cal.setTime(toDate);
      if (cardType.trim().equalsIgnoreCase("MC")) {
        cal.add(Calendar.MONTH, -2);
      }
      fromDate = new java.sql.Date(cal.getTime().getTime());     

      log.info("Calling loadData() of MerchPsp");
      loadData(fromDate, toDate, cardType, bankNumber);
    } catch (Exception e) {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    } 
    return (true);
  }

  private void setFileName(String cardType, int bankNumber, Date endDate) {
    String fileNameSuffix = cardType.trim().equalsIgnoreCase("VS") ? "Visa" : "MasterCard"; 
    Calendar cal = Calendar.getInstance();
    //
    cal.setTime(endDate);
    cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
    //
    String dateStr =  Integer.toString(cal.get(Calendar.YEAR))
        +StringUtilities.rightJustify(Integer.toString(cal.get(Calendar.MONTH)+1), 2, '0')
        +StringUtilities.rightJustify(Integer.toString(cal.get(Calendar.DAY_OF_MONTH)), 2, '0');
    
    if (cardType.trim().equalsIgnoreCase("VS") && (bankNumber == mesConstants.BANK_ID_MES_WF)) {
      FILE_NAME = "1186_AltE_"+dateStr+"_MESPF_thinfile.csv";  //.xlsx
    } else {
      FILE_NAME = "MerchPsp_"+fileNameSuffix+"_"+dateStr+".csv";
    }
  }
  
  public void loadData(Date beginDate, Date endDate, String cardType, int bankNumber) {
    try {  
      log.info("Loading MerchantPsp Data for the period " + beginDate + " to " + endDate);
      connect();
      setFileName(cardType, bankNumber, endDate);
      loadMerchPspProperties(cardType, bankNumber);
      getMerchantPspData(beginDate, endDate, cardType, bankNumber);    

      if (cardType.trim().equalsIgnoreCase("VS") && (bankNumber == mesConstants.BANK_ID_MES_WF)) {
        transferMerchPspFileToWellsFargo();
      } else { //send file by email
        if (toEmailAddress != null) {          
          sendEmail(cardType);      
        }
      }
      log.info("Done!");
    } catch (Exception e) {
      logEntry("loadData()", e.toString());
    } finally {
      cleanUp();
    }

  }

  private void createMerchPspRecord(ResultSet resultSet, Date endDate) throws SQLException {
   
    while (resultSet.next()) {
      MerchPspRecord merchPspRecord = new MerchPspRecord();
      
      merchPspRecord.setMerchantNumber(resultSet.getLong(MERCHANT_NUMBER)); 
      merchPspRecord.setSponsoredMerchantName(resultSet.getString(SPONSORED_MERCHANT_NAME));
      merchPspRecord.setBusinessAddres1(resultSet.getString(BUSINESS_ADDRESS1)); 
      merchPspRecord.setBusinessAddres3(resultSet.getString(BUSINESS_ADDRESS3));
      merchPspRecord.setBusinessCity(resultSet.getString(BUSINESS_CITY));
      merchPspRecord.setBusinessState(resultSet.getString(BUSINESS_STATE)); 
      merchPspRecord.setBusinessZip(resultSet.getString(BUSINESS_ZIP));
      merchPspRecord.setMerchantName(resultSet.getString(MERCHANT_NAME));
      merchPspRecord.setSicCode(resultSet.getString(MCC_CODE));
      merchPspRecord.setPspName(resultSet.getString(PSP_NAME));
      merchPspRecord.setPspId(resultSet.getLong(PSP_ID)); 
      merchPspRecord.setSalesCount(resultSet.getLong(SALES_COUNT)); 
      merchPspRecord.setSalesAmount(resultSet.getDouble(SALES_AMOUNT));
      merchPspRecord.setChargebackCount(resultSet.getLong(CHARGEBACK_COUNT)); 
      merchPspRecord.setChargebackAmount(resultSet.getDouble(CHARGEBACK_AMOUNT));
      merchPspRows.add(merchPspRecord);
      System.out.println("merchantNumber: "+resultSet.getLong(MERCHANT_NUMBER)); 
    }    
    logInfo("MerchPsp record collection size = " + merchPspRows.size());
    System.out.println("MerchPsp record collection size = " + merchPspRows.size());
  }


  private void getMerchantPspData(Date beginDate, Date endDate, String cardType, int bankNumber) {
    ScrollableResultSetIterator   it                = null;  
    ResultSet                     resultSet         = null;
    String                        dynaSelectColumns = "";
   
    try {
      
      if (cardType.trim().equalsIgnoreCase("VS")) {
        dynaSelectColumns = " sum(nvl(mes.visa_sales_count,0))  as \"Sales Count\", "+
                            " sum(nvl(mes.visa_sales_amount,0)) as \"Sales Amount\", ";
      } else { //MC card type
        dynaSelectColumns = " sum(nvl(mes.mc_sales_count,0))    as \"Sales Count\", "+
                            " sum(nvl(mes.mc_sales_amount,0))   as \"Sales Amount\", ";
      }
   
      /*@lineinfo:generated-code*//*@lineinfo:189^7*/

//  ************************************************************
//  #sql [Ctx] it = { select mf.merchant_number                as "Merchant Number",  
//                   m.merch_business_name             as "Sponsored Merchant Name",
//                   mf.dmaddr                         as "Addres1", 
//                   mf.address_line_3                 as "Addres3", 
//                   mf.dmcity                         as "City", 
//                   mf.dmstate                        as "State", 
//                   mf.dmzip                          as "Zip",
//                   mf.dba_name                       as "Merchant Name",
//                   mf.sic_code                       as "Mcc Code",
//                   o.org_name                        as "Psp Name", 
//                   o.org_group                       as "Psp Id",
//                   :dynaSelectColumns                 
//                   sum(nvl(mes.chargeback_vol_count,0))   as "Chargeback Count", 
//                   sum(nvl(mes.chargeback_vol_amount,0))  as "Chargeback Amount" 
//            from   mif             mf,
//                   merchant        m,
//                   Monthly_extract_summary mes,
//                   organization            o,
//                   group_merchant          gm,
//                   psp_report_node         prn
//            where  mf.merchant_number = m.merch_number
//                   and mf.merchant_number = mes.merchant_number
//                   and o.org_group = prn.hierarchy_node
//                   and substr(to_char(o.org_group), 1, 4) = :bankNumber
//                   and gm.org_num  = o.org_num
//                   and mf.merchant_number = gm.merchant_number
//                   and mes.active_date between :beginDate and :endDate 
//            group  by m.merch_business_name, mf.dmaddr, mf.address_line_3, mf.dmcity, mf.dmstate, 
//                      mf.dmzip, mf.dba_name, mf.merchant_number, mf.sic_code, o.org_name,  o.org_group
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select mf.merchant_number                as \"Merchant Number\",  \n                 m.merch_business_name             as \"Sponsored Merchant Name\",\n                 mf.dmaddr                         as \"Addres1\", \n                 mf.address_line_3                 as \"Addres3\", \n                 mf.dmcity                         as \"City\", \n                 mf.dmstate                        as \"State\", \n                 mf.dmzip                          as \"Zip\",\n                 mf.dba_name                       as \"Merchant Name\",\n                 mf.sic_code                       as \"Mcc Code\",\n                 o.org_name                        as \"Psp Name\", \n                 o.org_group                       as \"Psp Id\",\n                  ");
   __sjT_sb.append(dynaSelectColumns);
   __sjT_sb.append("                  \n                 sum(nvl(mes.chargeback_vol_count,0))   as \"Chargeback Count\", \n                 sum(nvl(mes.chargeback_vol_amount,0))  as \"Chargeback Amount\" \n          from   mif             mf,\n                 merchant        m,\n                 Monthly_extract_summary mes,\n                 organization            o,\n                 group_merchant          gm,\n                 psp_report_node         prn\n          where  mf.merchant_number = m.merch_number\n                 and mf.merchant_number = mes.merchant_number\n                 and o.org_group = prn.hierarchy_node\n                 and substr(to_char(o.org_group), 1, 4) =  ? \n                 and gm.org_num  = o.org_num\n                 and mf.merchant_number = gm.merchant_number\n                 and mes.active_date between  ?  and  ?  \n          group  by m.merch_business_name, mf.dmaddr, mf.address_line_3, mf.dmcity, mf.dmstate, \n                    mf.dmzip, mf.dba_name, mf.merchant_number, mf.sic_code, o.org_name,  o.org_group");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "0com.mes.startup.MerchPsp:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS,1004,1007);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ScrollableResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:220^9*/
      resultSet = it.getResultSet();
      
      if (resultSet != null) {          
        createMerchPspRecord(resultSet, endDate);
        resultSet.beforeFirst(); 
        csvMerchPspFile = new CSVFileMemory(resultSet, true); // copy to csv file
        resultSet.close();  
      }
    
    } catch(Exception e) {
      logEntry("Exception in getMerchantPspData() : ", e.toString());
    } finally {
      try{
        logInfo("Closing SQL connection.");        
        it.close();       
      } catch(Exception e) {
        logEntry("Exception in finally block of getMerchantPspData() : ", e.toString());
      }
    }
  }

  private void loadMerchPspProperties(String cardType, int bankNumber) throws Exception {
    fromEmailAddress = MesDefaults.getString(MesDefaults.MERCH_PSP_FROM_EMAIL_ADDRESS);
    
    if (cardType.trim().equalsIgnoreCase("VS")) {
      if (bankNumber != mesConstants.BANK_ID_MES_WF) {
        toEmailAddress = MesDefaults.getString(MesDefaults.MERCH_PSP_TO_VISA_EMAIL_ADDRESS);
        ccEmailAddress = MesDefaults.getString(MesDefaults.MERCH_PSP_CC_SYNOVUS_EMAIL_ADDRESS);
      } else { //WF 
        toEmailAddress = MesDefaults.getString(MesDefaults.MERCH_PSP_TO_VISA_EMAIL_ADDRESS_FOR_WF);
      }
    } else { //MC
      toEmailAddress = MesDefaults.getString(MesDefaults.MERCH_PSP_TO_MASTER_CARD_EMAIL_ADDRESS);
      if (bankNumber == mesConstants.BANK_ID_MES_WF) {
        ccEmailAddress = MesDefaults.getString(MesDefaults.MERCH_PSP_CC_WELLS_FARGO_EMAIL_ADDRESS);
      } else { //Synovus
        ccEmailAddress = MesDefaults.getString(MesDefaults.MERCH_PSP_CC_SYNOVUS_EMAIL_ADDRESS);
      }
    }
    
    ccProjectManagerEmailAddress = MesDefaults.getString(MesDefaults.MERCH_PSP_CC_PROJECT_MANAGER_EMAIL_ADDRESS);
    ccComplianceManagerEmailAddress = MesDefaults.getString(MesDefaults.MERCH_PSP_CC_COMPLIANCE_MANAGER_EMAIL_ADDRESS);

  }

  private void sendEmail(String cardType) {
    try {
      // send email
      log.info("Sending notification email");
      sendNotificationEmail(true, cardType, "Merchant PSP report is generated successfully");
    } catch (Exception e) {
      logEntry("sendEmail()", e.toString());
      sendNotificationEmail(false, cardType, "Failed to generate Merchant PSP report : " + e.getMessage());
      e.printStackTrace();
    }
  }
  
  public CSVFileMemory createCSVFile(ResultSet anotherSet, boolean withHeader) {
    CSVFileMemory file = new CSVFileMemory(anotherSet, withHeader);
    return file;
  }

  private void sendNotificationEmail(boolean wasSentSuccessfully, String cardType, String body) {
    StringBuffer subject = new StringBuffer(); 
    if (cardType.trim().equalsIgnoreCase("VS")) {
      subject.append("Visa");
    } else { //MC
      subject.append("MasterCard");
    }
    subject.append(" PSP Report");

    try {
      MailMessage msg = new MailMessage();
      msg.setFrom(fromEmailAddress);
      if (wasSentSuccessfully) {
        msg.addTo(toEmailAddress);
        
        if (ccEmailAddress != null) {
          msg.addCC(ccEmailAddress);
        }
  
        if (ccProjectManagerEmailAddress != null) {
          msg.addCC(ccProjectManagerEmailAddress);
        }
        if (ccComplianceManagerEmailAddress != null) {
          msg.addCC(ccComplianceManagerEmailAddress);
        }
      } else {
        msg.addTo(ccProjectManagerEmailAddress);
        msg.addTo(ccComplianceManagerEmailAddress);
      }
      
      if (csvMerchPspFile != null) {
        msg.addFile(FILE_NAME, csvMerchPspFile.toString().getBytes());
      }
      msg.setSubject(subject.toString());
      msg.setText(body);

      msg.send();
    } catch (Exception e) {
      logEntry("sendNotificationEmail(): ", e.getMessage());

    }
  }

  private boolean transferMerchPspFileToWellsFargo( ) {
    String            errorString   = null;
    Sftp              sftp          = null;
    boolean           result        = false;
    
    for (int attemptCount = 0; attemptCount < 3; ++attemptCount) {
      try {
        log.debug("generating sftp");
        sftp = getSftp(
            MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_HOST),     
            MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_USER),     
            MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_PASSWORD), 
            MesDefaults.getString(MesDefaults.MERCH_PSP_TO_WF_OUTGOING_PATH),  
            false);
              
        log.debug("uploading file: " + FILE_NAME);
        sftp.upload(csvMerchPspFile.toString().getBytes(), FILE_NAME);            
        
        try { sftp.disconnect(); } catch(Exception ee) {} finally{ sftp = null; }
        
        result = true;
        break;
       } catch (Exception ftpe) {
         logEntry("transferMerchPspFileToWellsFargo()", ftpe.toString());
         errorString = ftpe.toString();
         // close the connection and release the reference
         try{ sftp.disconnect(); } catch(Exception ee){} finally{ sftp = null; }
  
         // delay 1 second before retry attempt
         try{ Thread.sleep(1000); } catch( Exception ee ) {}
       }
     }

    
    try {
      StringBuffer subject = new StringBuffer();
      if (result == true) {
        subject.append("Merchant PSP report Upload Success: ").append(FILE_NAME).append(" (").append(csvMerchPspFile.rowCount()).append(" records)");
      } else {
        subject.append("Merchant PSP report Upload failed: ").append(errorString);
      }
    
      MailMessage msg = new MailMessage();
      msg.setFrom(fromEmailAddress);

      msg.addTo(toEmailAddress);
      
      if (ccProjectManagerEmailAddress != null) {
        msg.addCC(ccProjectManagerEmailAddress);
      }
      
      if (ccComplianceManagerEmailAddress != null) {
        msg.addCC(ccComplianceManagerEmailAddress);
      }
      
//      if (csvMerchPspFile != null) {
//        msg.addFile(FILE_NAME, csvMerchPspFile.toString().getBytes());
//      }
      msg.setSubject(subject.toString());
      msg.setText(subject.toString());
      msg.send();
    }
    catch (Exception e) {
    }
    
    return result;
  }
  
  public Date getManualActiveDate() {
    return manualActiveDate;
  }

  public void setManualActiveDate(Date manualActiveDate) {
    this.manualActiveDate = manualActiveDate;
  }
  
  public String getCardType() {
    return cardType;
  }

  public void setCardType(String cardType) {
    this.cardType = cardType;
  }

  public int getBankNumber() {
    return bankNumber;
  }

  public void setBankNumber(int bankNumber) {
    this.bankNumber = bankNumber;
  }
  
  public static void main(String[] args) {  
    // com.mes.support.HttpHelper.setTestServer(true);
    //  arg[0] = "execute"
    //  arg[1] = "VS"
    //  arg[2] = mesConstants.BANK_ID_MES_WF
    //  arg[3] = "07/31/2014"


      try {
      if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.MERCH_PSP_FROM_EMAIL_ADDRESS,
                      MesDefaults.MERCH_PSP_TO_VISA_EMAIL_ADDRESS,
                      MesDefaults.MERCH_PSP_CC_SYNOVUS_EMAIL_ADDRESS,
                      MesDefaults.MERCH_PSP_TO_VISA_EMAIL_ADDRESS_FOR_WF,
                      MesDefaults.MERCH_PSP_TO_MASTER_CARD_EMAIL_ADDRESS,
                      MesDefaults.MERCH_PSP_CC_WELLS_FARGO_EMAIL_ADDRESS,
                      MesDefaults.MERCH_PSP_CC_SYNOVUS_EMAIL_ADDRESS,
                      MesDefaults.MERCH_PSP_CC_PROJECT_MANAGER_EMAIL_ADDRESS,
                      MesDefaults.MERCH_PSP_CC_COMPLIANCE_MANAGER_EMAIL_ADDRESS,
                      MesDefaults.DK_WF_OUTGOING_HOST,
                      MesDefaults.DK_WF_OUTGOING_USER,
                      MesDefaults.DK_WF_OUTGOING_PASSWORD,
                      MesDefaults.MERCH_PSP_TO_WF_OUTGOING_PATH
              });
      }
          } catch (Exception e) {
              log.error(e.toString());
          }

     MerchPsp merchPsp = new MerchPsp();
     if (args.length > 2 && "execute".equals(args[0])) {
       merchPsp.setCardType(args[1]);
       merchPsp.setBankNumber(Integer.parseInt(args[2]));
       merchPsp.setManualActiveDate((args.length > 3) ? new Date(DateTimeFormatter.parseDate(args[3], "MM/dd/yyyy").getTime()) : null);
       System.out.println("Calling Execute()");      
       merchPsp.execute();
       System.exit(0);
     } else {
       System.out.println("Invalid Command");
       System.exit(1);
     }

   }

}/*@lineinfo:generated-code*/