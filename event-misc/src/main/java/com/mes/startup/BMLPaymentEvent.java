/*@lineinfo:filename=BMLPaymentEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/BMLPaymentEvent.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class BMLPaymentEvent extends EventBase
{
  static Logger log = Logger.getLogger(BMLPaymentEvent.class);
  
  public BMLPaymentEvent( )
  {
  }
  
  public boolean execute()
  {
    ProcessTable.ProcessTableEntry  entry         = null;
    boolean                         retVal        = false;
    ProcessTable                    pt            = null;
  
    try
    {
      connect(true);
      
      pt = new ProcessTable("bml_payment_process","bml_pmt_proc_sequence",ProcessTable.PT_ALL);
      
      if ( pt.hasPendingEntries() )
      {
        pt.preparePendingEntries();
        
        Vector entries = pt.getEntryVector();
        for( int i = 0; i < entries.size(); ++i )
        {
          entry = (ProcessTable.ProcessTableEntry)entries.elementAt(i);
         
          switch( entry.getProcessType() )
          {
            case 0:
              entry.recordTimestampBegin();
              payItems( entry.getLoadFilename() );
              entry.recordTimestampEnd();
              break;
          }
        }
      }
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("execute()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
 
  protected void payItems( String loadFilename )
  {
    double                    amount            = 0.0;
    int                       batchNumber       = 0;
    ResultSetIterator         it                = null;
    long                      loadFileId        = 0L;
    long                      merchantId        = 0L;
    ResultSet                 resultSet         = null;
    
    try
    {
      loadFileId = loadFilenameToLoadFileId(loadFilename);
      
      /*@lineinfo:generated-code*//*@lineinfo:105^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_number                    as merchant_number,
//                  dt.merchant_batch_number              as batch_number,
//                  sum( decode(dt.debit_credit_indicator,'C',-1,1) *
//                       dt.transaction_amount )          as ach_amount
//          from    daily_detail_file_ext_dt      dt
//          where   dt.load_filename = :loadFilename
//                  and dt.card_type = 'BL'
//          group by  dt.merchant_number, 
//                    dt.merchant_batch_number
//          order by  dt.merchant_number, dt.merchant_batch_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_number                    as merchant_number,\n                dt.merchant_batch_number              as batch_number,\n                sum( decode(dt.debit_credit_indicator,'C',-1,1) *\n                     dt.transaction_amount )          as ach_amount\n        from    daily_detail_file_ext_dt      dt\n        where   dt.load_filename =  :1 \n                and dt.card_type = 'BL'\n        group by  dt.merchant_number, \n                  dt.merchant_batch_number\n        order by  dt.merchant_number, dt.merchant_batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.BMLPaymentEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.BMLPaymentEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:117^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        merchantId  = resultSet.getLong("merchant_number");
        batchNumber = resultSet.getInt("batch_number");
        amount      = resultSet.getDouble("ach_amount");
      }
      resultSet.close();
      it.close();
      
      if ( amount != 0.0 )
      {
        String debitCreditInd = ((amount < 0.0) ? "D" : "C");
        String entryDesc      = ((amount < 0.0) ? "BML DEP DB" : "BML DEP CR");
        
        // queue the payment in the bankserv queue
        /*@lineinfo:generated-code*//*@lineinfo:135^9*/

//  ************************************************************
//  #sql [Ctx] { insert into bankserv_ach_detail 
//            ( 
//              merchant_number, 
//              amount, 
//              target_account_number,
//              target_routing_number,
//              entry_description, 
//              credit_debit_ind, 
//              created_by,
//              source_type,
//              source_id,
//              date_authorized,
//              authorized_by
//            ) 
//            select  mf.merchant_number,
//                    abs(:amount),
//                    mf.dda_num,
//                    mf.transit_routng_num,
//                    :entryDesc,
//                    :debitCreditInd,
//                    'system',
//                    :mesConstants.BANKSERV_SOURCE_BML,
//                    :batchNumber,
//                    sysdate,
//                    'system'
//            from    mif     mf
//            where   mf.merchant_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into bankserv_ach_detail \n          ( \n            merchant_number, \n            amount, \n            target_account_number,\n            target_routing_number,\n            entry_description, \n            credit_debit_ind, \n            created_by,\n            source_type,\n            source_id,\n            date_authorized,\n            authorized_by\n          ) \n          select  mf.merchant_number,\n                  abs( :1 ),\n                  mf.dda_num,\n                  mf.transit_routng_num,\n                   :2 ,\n                   :3 ,\n                  'system',\n                   :4 ,\n                   :5 ,\n                  sysdate,\n                  'system'\n          from    mif     mf\n          where   mf.merchant_number =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.BMLPaymentEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,amount);
   __sJT_st.setString(2,entryDesc);
   __sJT_st.setString(3,debitCreditInd);
   __sJT_st.setInt(4,mesConstants.BANKSERV_SOURCE_BML);
   __sJT_st.setInt(5,batchNumber);
   __sJT_st.setLong(6,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:164^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("payItems()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }
  
  public static void main( String[] args )
  {
    BMLPaymentEvent      test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new BMLPaymentEvent();
      test.execute();
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/