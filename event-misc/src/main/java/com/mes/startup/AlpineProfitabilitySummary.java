/*@lineinfo:filename=AlpineProfitabilitySummary*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/VirtualAppSubmitter.sqlj $

  Description:

    VPSISOServiceCalls

    Class to generate and send service calls to VPS via email

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-07-14 14:49:08 -0700 (Tue, 14 Jul 2009) $
  Version            : $Revision: 16303 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.CSVFileMemory;
import sqlj.runtime.ResultSetIterator;

public class AlpineProfitabilitySummary extends EventBase
{
  static Logger log = Logger.getLogger(AlpineProfitabilitySummary.class);
  
  public AlpineProfitabilitySummary()
  {
  }
  
  public boolean execute()
  {
    boolean           result  = false;
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:57^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number    as merch,
//                  mf.dba_name           as name,
//                  mf.dda_num            as dda,
//                  gn1.group_number      as region_node,
//                  gn1.group_name        as region_name,
//                  gn.group_number       as branch_node,
//                  gn.group_name         as branch_name,
//                  mf.dmagent            as assoc,
//                  mf.dmacctst           as status,
//                  mf.federal_tax_id     as tax_id,
//                  ltrim(rtrim(to_char(last_month.sales,'$999,999,999.99')))     as sales,
//                  ltrim(rtrim(to_char(last_month.discover_sales,'$999,999,999.99'))) as discover_sales,
//                  ltrim(rtrim(to_char(last_month.income,'$999,999,999.99')))     as income,
//                  ltrim(rtrim(to_char(last_month.expense,'$999,999,999.99')))    as expense,
//                  ltrim(rtrim(to_char(last_month.income-last_month.expense,'$999,999,999.99'))) as profit,
//                  ltrim(rtrim(decode(last_month.sales, 0, '0.00', to_char(round( ((last_month.income-last_month.expense) / last_month.sales)*100, 2), '999,990.99'))||'%')) as return,
//                  ltrim(rtrim(to_char(ytd.sales,'$999,999,999.99'))) as ytd_sales,
//                  ltrim(rtrim(to_char(ytd.discover_sales,'$999,999,999.99'))) as ytd_discover_sales,
//                  ltrim(rtrim(to_char(ytd.income-ytd.expense,'$999,999,999.99'))) as ytd_profit,
//                  ltrim(rtrim(decode(ytd.sales, 0, '0.00', to_char(round( ((ytd.income-ytd.expense) / ytd.sales)*100, 2), '999,990.99'))||'%')) as ytd_return
//          from    t_hierarchy  th,
//                  mif mf,
//                  group_names gn,
//                  group_names gn1,
//                  (
//                    select  sm.merchant_number  merchant_number,
//                            nvl(sm.cash_advance_vol_amount,0)+sm.vmc_sales_amount sales,
//                            nvl(sm.disc_sales_amount,0) discover_sales,
//                            sm.tot_inc_discount 
//                              -sm.equip_sales_tax_collected +
//                              sm.tot_inc_interchange +
//                              sm.tot_inc_authorization +
//                              sm.tot_inc_capture +
//                              sm.tot_inc_debit +
//                              sm.tot_inc_ind_plans +
//                              sm.tot_inc_sys_generated +
//                              nvl(sm.disc_ic_dce_adj_amount,0) +
//                              nvl(sm.disc_ic_dce_adj_amount,0) + 
//                              nvl(sm.fee_dce_adj_amount,0) as income,
//                            decode(sm.interchange_expense, 0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),sm.interchange_expense) +
//                              sm.vmc_assessment_expense +
//                              sm.equip_transfer_cost +
//                              nvl(mcli.tot_ndr,0) +
//                              nvl(mcli.tot_authorization,0) +
//                              nvl(mcli.tot_capture,0) +
//                              nvl(mcli.tot_debit,0) +
//                              nvl(mcli.tot_ind_plans,0) +
//                              nvl(mcli.tot_sys_generated,0) +
//                              nvl(mcli.tot_equip_rental,0) +
//                              nvl(mcli.tot_equip_sales,0) as expense
//                    from    t_hierarchy th,
//                            mif mf,
//                            monthly_extract_summary sm,
//                            monthly_extract_contract mcli
//                    where   th.hier_type = 1
//                            and th.ancestor in ( 3941300396, 3858916001)
//                            and th.descendent = mf.association_node
//                            and mf.merchant_number = sm.merchant_number
//                            and sm.active_date = trunc(trunc(sysdate,'mm')-1, 'mm')
//                            and sm.hh_load_sec = mcli.hh_load_sec(+)
//                            and sm.merchant_number = mcli.merchant_number(+)
//                            and mcli.contract_type(+) = 1
//                  ) last_month,
//                  (
//                    select  sm.merchant_number  merchant_number,
//                            sum(nvl(sm.cash_advance_vol_amount,0)+sm.vmc_sales_amount) sales,
//                            sum(sm.disc_sales_amount) discover_sales,
//                            sum(sm.tot_inc_discount
//                              -sm.equip_sales_tax_collected +
//                              sm.tot_inc_interchange +
//                              sm.tot_inc_authorization +
//                              sm.tot_inc_capture +
//                              sm.tot_inc_debit +
//                              sm.tot_inc_ind_plans +
//                              sm.tot_inc_sys_generated +
//                              nvl(sm.disc_ic_dce_adj_amount,0) +
//                              nvl(sm.disc_ic_dce_adj_amount,0) + 
//                              nvl(sm.fee_dce_adj_amount,0)) as income,
//                            sum(decode(sm.interchange_expense, 0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),sm.interchange_expense) +
//                              sm.vmc_assessment_expense +
//                              sm.equip_transfer_cost +
//                              nvl(mcli.tot_ndr,0) +
//                              nvl(mcli.tot_authorization,0) +
//                              nvl(mcli.tot_capture,0) +
//                              nvl(mcli.tot_debit,0) +
//                              nvl(mcli.tot_ind_plans,0) +
//                              nvl(mcli.tot_sys_generated,0) +
//                              nvl(mcli.tot_equip_rental,0) +
//                              nvl(mcli.tot_equip_sales,0)) as expense
//                    from    t_hierarchy th,
//                            mif mf,
//                            monthly_extract_summary sm,
//                            monthly_extract_contract mcli
//                    where   th.hier_type = 1
//                            and th.ancestor in ( 3941300396, 3858916001)
//                            and th.descendent = mf.association_node
//                            and mf.merchant_number = sm.merchant_number
//                            and sm.active_date between trunc(sysdate-20, 'yy') and trunc(trunc(sysdate,'mm')-1, 'mm')
//                            and sm.hh_load_sec = mcli.hh_load_sec(+)
//                            and sm.merchant_number = mcli.merchant_number(+)
//                            and mcli.contract_type(+) = 1
//                    group by sm.merchant_number
//                  ) ytd
//          where   th.hier_type = 1
//                  and th.ancestor in ( 3941300396, 3858916001)
//                  and th.descendent = mf.association_node
//                  and (mf.bank_number||mf.group_5_association) = gn.group_number(+)
//                  and (mf.bank_number||mf.group_4_association) = gn1.group_number(+)
//                  and mf.merchant_number = last_month.merchant_number
//                  and mf.merchant_number = ytd.merchant_number
//          order by mf.group_4_association, mf.group_5_association, mf.dba_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number    as merch,\n                mf.dba_name           as name,\n                mf.dda_num            as dda,\n                gn1.group_number      as region_node,\n                gn1.group_name        as region_name,\n                gn.group_number       as branch_node,\n                gn.group_name         as branch_name,\n                mf.dmagent            as assoc,\n                mf.dmacctst           as status,\n                mf.federal_tax_id     as tax_id,\n                ltrim(rtrim(to_char(last_month.sales,'$999,999,999.99')))     as sales,\n                ltrim(rtrim(to_char(last_month.discover_sales,'$999,999,999.99'))) as discover_sales,\n                ltrim(rtrim(to_char(last_month.income,'$999,999,999.99')))     as income,\n                ltrim(rtrim(to_char(last_month.expense,'$999,999,999.99')))    as expense,\n                ltrim(rtrim(to_char(last_month.income-last_month.expense,'$999,999,999.99'))) as profit,\n                ltrim(rtrim(decode(last_month.sales, 0, '0.00', to_char(round( ((last_month.income-last_month.expense) / last_month.sales)*100, 2), '999,990.99'))||'%')) as return,\n                ltrim(rtrim(to_char(ytd.sales,'$999,999,999.99'))) as ytd_sales,\n                ltrim(rtrim(to_char(ytd.discover_sales,'$999,999,999.99'))) as ytd_discover_sales,\n                ltrim(rtrim(to_char(ytd.income-ytd.expense,'$999,999,999.99'))) as ytd_profit,\n                ltrim(rtrim(decode(ytd.sales, 0, '0.00', to_char(round( ((ytd.income-ytd.expense) / ytd.sales)*100, 2), '999,990.99'))||'%')) as ytd_return\n        from    t_hierarchy  th,\n                mif mf,\n                group_names gn,\n                group_names gn1,\n                (\n                  select  sm.merchant_number  merchant_number,\n                          nvl(sm.cash_advance_vol_amount,0)+sm.vmc_sales_amount sales,\n                          nvl(sm.disc_sales_amount,0) discover_sales,\n                          sm.tot_inc_discount \n                            -sm.equip_sales_tax_collected +\n                            sm.tot_inc_interchange +\n                            sm.tot_inc_authorization +\n                            sm.tot_inc_capture +\n                            sm.tot_inc_debit +\n                            sm.tot_inc_ind_plans +\n                            sm.tot_inc_sys_generated +\n                            nvl(sm.disc_ic_dce_adj_amount,0) +\n                            nvl(sm.disc_ic_dce_adj_amount,0) + \n                            nvl(sm.fee_dce_adj_amount,0) as income,\n                          decode(sm.interchange_expense, 0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),sm.interchange_expense) +\n                            sm.vmc_assessment_expense +\n                            sm.equip_transfer_cost +\n                            nvl(mcli.tot_ndr,0) +\n                            nvl(mcli.tot_authorization,0) +\n                            nvl(mcli.tot_capture,0) +\n                            nvl(mcli.tot_debit,0) +\n                            nvl(mcli.tot_ind_plans,0) +\n                            nvl(mcli.tot_sys_generated,0) +\n                            nvl(mcli.tot_equip_rental,0) +\n                            nvl(mcli.tot_equip_sales,0) as expense\n                  from    t_hierarchy th,\n                          mif mf,\n                          monthly_extract_summary sm,\n                          monthly_extract_contract mcli\n                  where   th.hier_type = 1\n                          and th.ancestor in ( 3941300396, 3858916001)\n                          and th.descendent = mf.association_node\n                          and mf.merchant_number = sm.merchant_number\n                          and sm.active_date = trunc(trunc(sysdate,'mm')-1, 'mm')\n                          and sm.hh_load_sec = mcli.hh_load_sec(+)\n                          and sm.merchant_number = mcli.merchant_number(+)\n                          and mcli.contract_type(+) = 1\n                ) last_month,\n                (\n                  select  sm.merchant_number  merchant_number,\n                          sum(nvl(sm.cash_advance_vol_amount,0)+sm.vmc_sales_amount) sales,\n                          sum(sm.disc_sales_amount) discover_sales,\n                          sum(sm.tot_inc_discount\n                            -sm.equip_sales_tax_collected +\n                            sm.tot_inc_interchange +\n                            sm.tot_inc_authorization +\n                            sm.tot_inc_capture +\n                            sm.tot_inc_debit +\n                            sm.tot_inc_ind_plans +\n                            sm.tot_inc_sys_generated +\n                            nvl(sm.disc_ic_dce_adj_amount,0) +\n                            nvl(sm.disc_ic_dce_adj_amount,0) + \n                            nvl(sm.fee_dce_adj_amount,0)) as income,\n                          sum(decode(sm.interchange_expense, 0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),sm.interchange_expense) +\n                            sm.vmc_assessment_expense +\n                            sm.equip_transfer_cost +\n                            nvl(mcli.tot_ndr,0) +\n                            nvl(mcli.tot_authorization,0) +\n                            nvl(mcli.tot_capture,0) +\n                            nvl(mcli.tot_debit,0) +\n                            nvl(mcli.tot_ind_plans,0) +\n                            nvl(mcli.tot_sys_generated,0) +\n                            nvl(mcli.tot_equip_rental,0) +\n                            nvl(mcli.tot_equip_sales,0)) as expense\n                  from    t_hierarchy th,\n                          mif mf,\n                          monthly_extract_summary sm,\n                          monthly_extract_contract mcli\n                  where   th.hier_type = 1\n                          and th.ancestor in ( 3941300396, 3858916001)\n                          and th.descendent = mf.association_node\n                          and mf.merchant_number = sm.merchant_number\n                          and sm.active_date between trunc(sysdate-20, 'yy') and trunc(trunc(sysdate,'mm')-1, 'mm')\n                          and sm.hh_load_sec = mcli.hh_load_sec(+)\n                          and sm.merchant_number = mcli.merchant_number(+)\n                          and mcli.contract_type(+) = 1\n                  group by sm.merchant_number\n                ) ytd\n        where   th.hier_type = 1\n                and th.ancestor in ( 3941300396, 3858916001)\n                and th.descendent = mf.association_node\n                and (mf.bank_number||mf.group_5_association) = gn.group_number(+)\n                and (mf.bank_number||mf.group_4_association) = gn1.group_number(+)\n                and mf.merchant_number = last_month.merchant_number\n                and mf.merchant_number = ytd.merchant_number\n        order by mf.group_4_association, mf.group_5_association, mf.dba_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.AlpineProfitabilitySummary",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.AlpineProfitabilitySummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:170^7*/
      
      rs = it.getResultSet();
      
      log.debug("creating alpine profitability summary");
      
      CSVFileMemory csv = new CSVFileMemory(rs, true);
      
      log.debug("csv file created");
      
      rs.close();
      it.close();
      
      String curMonthStr = "";
      String curMonthExt = "";
      
      /*@lineinfo:generated-code*//*@lineinfo:186^7*/

//  ************************************************************
//  #sql [Ctx] { select  to_char(trunc(trunc(sysdate,'mm')-1,'mm'), 'Mon YYYY'),
//                  to_char(trunc(trunc(sysdate,'mm')-1,'mm'), '_MM_YYYY')
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_char(trunc(trunc(sysdate,'mm')-1,'mm'), 'Mon YYYY'),\n                to_char(trunc(trunc(sysdate,'mm')-1,'mm'), '_MM_YYYY')\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.AlpineProfitabilitySummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   curMonthStr = (String)__sJT_rs.getString(1);
   curMonthExt = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:193^7*/
      
      // send file 
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(MesEmails.MSG_ADDRS_ALPINE_PROF);
      msg.setSubject("Alpine Bank Profitability Report (" + curMonthStr + ")");
      msg.setText("Attached is the Alpine Bank profitability report for last month (" + curMonthStr + ").");
      msg.addStringAsTextFile("alpine_profitability"+curMonthExt+".csv", csv.toString());
      
      log.debug("sending email");
      msg.send();
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }
  
  public static void main( String[] args )
  {
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      AlpineProfitabilitySummary worker = new AlpineProfitabilitySummary();
      
      worker.execute();
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/