/*@lineinfo:filename=RetrievalImageUploadEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/startup/RetrievalImageUploadEvent.sqlj $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2012-02-23 15:44:40 -0800 (Thu, 23 Feb 2012) $
  Version            : $Revision: 19864 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.database.OracleConnectionPool;
import com.mes.net.MailMessage;
import com.mes.support.WildCardFilter;

public class RetrievalImageUploadEvent extends EventBase
{
  protected void archiveImageFiles( String[] tifFiles )
  {
    try
    {
      byte[]  zipData         = new byte[4096];
      String  zipFilename     = generateFilename("verifi9999",".zip");
      String  flagFilename    = zipFilename.substring(0,zipFilename.lastIndexOf('.')) + ".flg";
      
      // create the archive .zip file
      ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFilename));
      zos.setMethod(ZipOutputStream.DEFLATED);

      // zip all the TIFs into a single zip file
      for (int i = 0; i < tifFiles.length; ++i)
      {
        int               bytesRead       = 0;
        String            tifFilename     = tifFiles[i];
        
        FileInputStream   zipInFile       = new FileInputStream(tifFilename);
        ZipEntry          dataZipEntry    = new ZipEntry(tifFilename);
        
        dataZipEntry.setMethod(ZipEntry.DEFLATED);
        zos.putNextEntry(dataZipEntry);
        
        while(bytesRead != -1)
        {
          bytesRead = zipInFile.read(zipData);

          if(bytesRead != -1)
          {
            // write these bytes to the zip file
            zos.write(zipData, 0, bytesRead);
          }
        }
        zos.closeEntry();
        zipInFile.close();
      }
      // close the zip file
      zos.close();

      // post the zip file to the archive server      
      String archiveHost = MesDefaults.getString(MesDefaults.DK_ARCHIVE_HOST);
      String archiveUser = MesDefaults.getString(MesDefaults.DK_ARCHIVE_USER);
      String archivePass = MesDefaults.getString(MesDefaults.DK_ARCHIVE_PASS);

      ftpPut( zipFilename,
              archiveHost,
              archiveUser,
              archivePass,
              ARCHIVE_PATH_DAILY,
              true);

      ftpPut( flagFilename,
              zipFilename.getBytes(),
              archiveHost,
              archiveUser,
              archivePass,
              ARCHIVE_PATH_DAILY,
              true);

      new File(flagFilename).delete();
      new File(zipFilename).delete();
      
      for (int i = 0; i < tifFiles.length; ++i)
      {
        new File(tifFiles[i]).delete();
      }
    }
    catch( Exception e )
    {
      logEntry("archiveImageFiles()",e.toString());
    }
    finally
    {
    }      
  }
  
  public boolean execute()
  {
    FTPClient           cablecar        = null;
    StringBuffer        msgText         = null;
    boolean             retVal          = false;
    String[]            tifFiles        = null;
    FTPClient           wa1fax01        = null;
    
    try
    {
      cablecar = new FTPClient(MesDefaults.getString(MesDefaults.DK_VERIFI_IMAGE_HOST), 21);
      cablecar.login(MesDefaults.getString(MesDefaults.DK_VERIFI_IMAGE_USER), 
                     MesDefaults.getString(MesDefaults.DK_VERIFI_IMAGE_PASSWORD));
      cablecar.setConnectMode(FTPConnectMode.ACTIVE);
      cablecar.setType(FTPTransferType.BINARY);
      cablecar.chdir(MesDefaults.getString(MesDefaults.DK_VERIFI_IMAGE_PATH));
      
      wa1fax01 = new FTPClient(MesDefaults.getString(MesDefaults.DK_SPOKANE_IMAGE_HOST), 21);
      wa1fax01.login(MesDefaults.getString(MesDefaults.DK_SPOKANE_IMAGE_USER),
                     MesDefaults.getString(MesDefaults.DK_SPOKANE_IMAGE_PASSWORD));
      wa1fax01.setConnectMode(FTPConnectMode.ACTIVE);
      wa1fax01.setType(FTPTransferType.BINARY);
      wa1fax01.chdir(MesDefaults.getString(MesDefaults.DK_SPOKANE_IMAGE_PATH));
      
      try
      {
        tifFiles = cablecar.dir("r*.tif");
      
        // retrieve all the files to process
        for (int i = 0; i < tifFiles.length; ++i)
        {
          String tifFilename = tifFiles[i];
          cablecar.get(tifFilename, tifFilename);
          wa1fax01.put(tifFilename, tifFilename);
          cablecar.delete(tifFilename);
          
          if ( msgText == null )
          {
            msgText = new StringBuffer();
            msgText.append("The following files were moved to the processing server:\n\n");
          }            
          msgText.append(tifFilename + "\n");
        }
      }
      catch(com.enterprisedt.net.ftp.FTPException fe)
      {
        log.error("No retrieval image files to process");
      }
      
      if ( msgText != null )
      {
        MailMessage msg = new MailMessage();
        msg.setAddresses(MesEmails.MSG_ADDRS_AUTO_IMAGE_ATTACH);
        msg.setSubject("Retrieval Image Upload Notification");
        msg.setText(msgText.toString());
        msg.send();
      }
      
      if ( tifFiles != null && tifFiles.length > 0 )
      {
        archiveImageFiles(tifFiles);
      }
      
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ cablecar.quit(); } catch( Exception ee ) {}
      try{ wa1fax01.quit(); } catch( Exception ee ) {}
    }
    return( retVal );
  }
  
  public static void main( String[] args )
  {
    RetrievalImageUploadEvent     event     = null;
    
    try
    {
      if (args.length > 0 && args[0].equals("testproperties")) {
        EventBase.printKeyListStatus(new String[]{
                MesDefaults.DK_ARCHIVE_HOST,
                MesDefaults.DK_ARCHIVE_USER,
                MesDefaults.DK_ARCHIVE_PASS,
                MesDefaults.DK_VERIFI_IMAGE_HOST,
                MesDefaults.DK_VERIFI_IMAGE_USER,
                MesDefaults.DK_VERIFI_IMAGE_PASSWORD,
                MesDefaults.DK_VERIFI_IMAGE_PATH,
                MesDefaults.DK_SPOKANE_IMAGE_HOST,
                MesDefaults.DK_SPOKANE_IMAGE_USER,
                MesDefaults.DK_SPOKANE_IMAGE_PASSWORD,
                MesDefaults.DK_SPOKANE_IMAGE_PATH
        });
      }

      event = new RetrievalImageUploadEvent();
      event.connect(true);
      
      if ( "archiveImageFiles".equals(args[0]) )
      {
        WildCardFilter filter = new WildCardFilter("*.tif");
      
        File    dir       = new File("./");
        File[]  fileList  = dir.listFiles(filter);
      
        if(fileList != null)
        {
          Vector tifFiles = new Vector();
          for( int i = 0; i < fileList.length; ++i )
          {
            if ( fileList[i].getName().toLowerCase().indexOf(".tif") > 0 )
            {
              tifFiles.addElement(fileList[i].getName());
            }
          }
          String[] filenames = new String[tifFiles.size()];
          for( int i = 0; i < tifFiles.size(); ++i )
          {
            filenames[i] = (String)tifFiles.elementAt(i);
          }
          event.archiveImageFiles(filenames);
        }
        else
        {
          System.out.println("No files found");
        }
      }
      else if ( "execute".equals(args[0]) )
      {
        event.execute();
      }
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ event.cleanUp(); } catch( Exception e ){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);    
  }
}/*@lineinfo:generated-code*/