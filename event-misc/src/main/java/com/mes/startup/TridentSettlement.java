/*@lineinfo:filename=TridentSettlement*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TridentSettlement.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-09-19 13:54:03 -0700 (Wed, 19 Sep 2012) $
  Version            : $Revision: 20570 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

public class TridentSettlement
{
  public TridentSettlement( )
  {
// unused code deleted August 2012
  }
}/*@lineinfo:generated-code*/