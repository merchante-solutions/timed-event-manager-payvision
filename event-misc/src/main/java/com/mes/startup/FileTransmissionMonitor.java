/*@lineinfo:filename=FileTransmissionMonitor*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: /com/mes/startup/FileTransmissionMonitor.sqlj

  Description:  
    Timed event that checks to verify that files have been completely loaded
    and summarized

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;

public class FileTransmissionMonitor extends EventBase
{
  static Logger log = Logger.getLogger(FileTransmissionMonitor.class);

  private long  procSequence  = 0L;
  
  public FileTransmissionMonitor()
  {
  }
  
  public boolean execute()
  {
    boolean             result        = false;
    ResultSetIterator   it            = null;
    ResultSet           rs            = null;
    
    Vector              files         = new Vector();
    
    try
    {
      connect();
      
      int fileCount = 0;
      // look to see if there are files to check on
      /*@lineinfo:generated-code*//*@lineinfo:54^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(ftm.load_filename)
//          
//          from    file_transmission_monitor ftm,
//                  file_transmission_types ftt
//          where   ftm.process_sequence = 0 and
//                  ftm.filename_base = ftt.filename_base(+)
//                  and trunc((sysdate - ftm.date_loaded) * 24) >= nvl(ftt.hour_delay, 0) 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ftm.load_filename)\n         \n        from    file_transmission_monitor ftm,\n                file_transmission_types ftt\n        where   ftm.process_sequence = 0 and\n                ftm.filename_base = ftt.filename_base(+)\n                and trunc((sysdate - ftm.date_loaded) * 24) >= nvl(ftt.hour_delay, 0)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.FileTransmissionMonitor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   fileCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:63^7*/
      
      if(fileCount > 0)
      {
        // get next process sequence
        /*@lineinfo:generated-code*//*@lineinfo:68^9*/

//  ************************************************************
//  #sql [Ctx] { select  ft_monitor_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ft_monitor_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.FileTransmissionMonitor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:73^9*/
        
        // mark affected items with process sequence
        /*@lineinfo:generated-code*//*@lineinfo:76^9*/

//  ************************************************************
//  #sql [Ctx] { update  file_transmission_monitor
//            set     process_sequence = :procSequence
//            where   process_sequence is null and
//                    load_filename in
//                    (
//                      select  distinct ftm.load_filename
//                      from    file_transmission_monitor ftm,
//                              file_transmission_types ftt
//                      where   ftm.process_sequence is null and
//                              ftm.filename_base = ftt.filename_base(+)
//                              and trunc((sysdate - ftm.date_loaded) * 24) >= nvl(ftt.hour_delay, 0) 
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  file_transmission_monitor\n          set     process_sequence =  :1 \n          where   process_sequence is null and\n                  load_filename in\n                  (\n                    select  distinct ftm.load_filename\n                    from    file_transmission_monitor ftm,\n                            file_transmission_types ftt\n                    where   ftm.process_sequence is null and\n                            ftm.filename_base = ftt.filename_base(+)\n                            and trunc((sysdate - ftm.date_loaded) * 24) >= nvl(ftt.hour_delay, 0) \n                  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.FileTransmissionMonitor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^9*/
        
        // now we're ready to actually get the files and their 
        /*@lineinfo:generated-code*//*@lineinfo:93^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  load_filename,
//                    filename_base
//            from    file_transmission_monitor
//            where   process_sequence = :procSequence
//            order by date_loaded
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  load_filename,\n                  filename_base\n          from    file_transmission_monitor\n          where   process_sequence =  :1 \n          order by date_loaded";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.FileTransmissionMonitor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.FileTransmissionMonitor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:100^9*/
        
        rs = it.getResultSet();
       
        while(rs.next())
        {
          files.add(new FileMonitor(rs.getString("load_filename"), rs.getString("filename_base")));
        }
        
        rs.close();
        it.close();
        
        for(int i=0; i<files.size(); ++i)
        {
          FileMonitor procFile = (FileMonitor)(files.elementAt(i));
          
          // process this file
          //checkFile(procFile);
        }
      }
      
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  private void checkFile(FileMonitor procFile)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    Vector            tables  = new Vector();
    boolean           success = true;
    
    try
    {
      // get list of tables to check for this filename base
      /*@lineinfo:generated-code*//*@lineinfo:146^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  check_table
//          from    file_transmission_types
//          where   filename_base = :procFile.filenameBase
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  check_table\n        from    file_transmission_types\n        where   filename_base =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.FileTransmissionMonitor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,procFile.filenameBase);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.FileTransmissionMonitor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:151^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        tables.add(rs.getString("check_table"));
      }
      
      rs.close();
      it.close();
      
      // now check to see that there are records in the check table for this
      // load filename
      for(int i=0; i < tables.size(); ++i)
      {
        String tableName = (String)(tables.elementAt(i));
        int recCount = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:170^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(load_filename)
//            
//            from    :tableName
//            where   load_filename = :procFile.loadFilename
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  count(load_filename)\n           \n          from     ");
   __sjT_sb.append(tableName);
   __sjT_sb.append(" \n          where   load_filename =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "5com.mes.startup.FileTransmissionMonitor:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,procFile.loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:176^9*/
        
        if(recCount > 0)
        {
          // success
        }
        else
        {
          // failure
          success = false;
          
          /*@lineinfo:generated-code*//*@lineinfo:187^11*/

//  ************************************************************
//  #sql [Ctx] { update  file_transmission_monitor
//              set     result = result||' F('||:tableName||') ',
//                      date_checked = sysdate
//              where   process_sequence = :procSequence and
//                      load_filename = :procFile.loadFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  file_transmission_monitor\n            set     result = result||' F('|| :1 ||') ',\n                    date_checked = sysdate\n            where   process_sequence =  :2  and\n                    load_filename =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.FileTransmissionMonitor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,tableName);
   __sJT_st.setLong(2,procSequence);
   __sJT_st.setString(3,procFile.loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:194^11*/
          
          // send email
          MailMessage msg = new MailMessage();
          
          msg.setAddresses(MesEmails.MSG_ADDRS_FILETRANSMISSION_MONITOR);
          msg.setSubject("FTM Failure: " + procFile.loadFilename + ", " + tableName);
          msg.setText(procFile.loadFilename + " was not found in table " + tableName);
          
          msg.send();
        }
        
        commit();
      }
      
      if(success)
      {
        /*@lineinfo:generated-code*//*@lineinfo:211^9*/

//  ************************************************************
//  #sql [Ctx] { update  file_transmission_monitor
//            set     result = 'SUCCESS',
//                    date_checked = sysdate
//            where   process_sequence = :procSequence and
//                    load_filename = :procFile.loadFilename
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  file_transmission_monitor\n          set     result = 'SUCCESS',\n                  date_checked = sysdate\n          where   process_sequence =  :1  and\n                  load_filename =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.FileTransmissionMonitor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,procSequence);
   __sJT_st.setString(2,procFile.loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:218^9*/
        
        commit();
      }
    }
    catch(Exception e)
    {
      logEntry("checkFile(" + procFile.loadFilename + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  public class FileMonitor
  {
    public String loadFilename;
    public String filenameBase;
    
    public FileMonitor(String _loadFilename, String _filenameBase)
    {
      loadFilename = _loadFilename;
      filenameBase = _filenameBase;
    }
  }
  
  public static void main(String args[])
  {
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      FileTransmissionMonitor ftm = new FileTransmissionMonitor();
      ftm.execute();
    }
    catch(Exception e)
    {
      log.error("FileTransmissionMonitor::main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/