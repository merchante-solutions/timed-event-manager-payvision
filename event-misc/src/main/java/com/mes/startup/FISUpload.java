/*@lineinfo:filename=FISUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/ChargebackUpload.sqlj $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2010-07-22 15:16:29 -0700 (Thu, 22 Jul 2010) $
  Version            : $Revision: 17600 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.flatfile.FlatFileRecord;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class FISUpload extends TsysFileBase
{
  static Logger log = Logger.getLogger(FISUpload.class);
  
  public boolean TestMode = false;
  
  public FISUpload( )
  {
    PropertiesFilename = "fis.properties";
    EmailGroupSuccess = MesEmails.MSG_ADDRS_OUTGOING_FIS_NOTIFY;   
    EmailGroupFailure = MesEmails.MSG_ADDRS_OUTGOING_FIS_FAILURE;  
  }
  
  public boolean execute( )
  {
    String            workFilename  = null;
    long              pSeq          = 0L;
    BufferedWriter    out           = null;
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    
    FlatFileRecord  ffd           = null;
    
    
    boolean result = false;
    
    try
    {
      connect();
      
      String dateString = null;
      /*@lineinfo:generated-code*//*@lineinfo:70^7*/

//  ************************************************************
//  #sql [Ctx] { select  to_char(sysdate,'mmddrr')
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_char(sysdate,'mmddrr')\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.FISUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dateString = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:75^7*/
      String indexString = NumberFormatter.getPaddedInt( getFileId( "fis3941", dateString), 3 );
      
      workFilename = "DQEM3552_"+dateString+"_"+indexString+".dat";
      //workFilename = "fis3942_"+dateString+"_"+indexString+".dat";
      
      // build outgoing file
      out = new BufferedWriter( new FileWriter( workFilename, false ) );
        
      // detail records...
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_FIS_DETAIL);

      /*@lineinfo:generated-code*//*@lineinfo:87^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number                as merchant_number,
//                  to_char(sysdate, 'mmddyyyyddd')   as transaction_date,
//                  to_char(sysdate, 'hh24miss')      as transaction_time,
//                  mf.dba_name                       as dba_name,
//                  nvl(mf.fdr_corp_name,mf.dba_name) as legal_name,
//                  lpad(mf.federal_tax_id,9,'0')     as fed_tax_id,
//                  to_char(mif_date_opened(mf.date_opened), 'mmddyyyyddd') as open_date
//          from    mif mf
//          where   ( mf.bank_number = 3942 or (mf.bank_number = 3941 and mf.group_2_association = '400078') )
//                  and mf.date_stat_chgd_to_dcb is null
//          order by mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number                as merchant_number,\n                to_char(sysdate, 'mmddyyyyddd')   as transaction_date,\n                to_char(sysdate, 'hh24miss')      as transaction_time,\n                mf.dba_name                       as dba_name,\n                nvl(mf.fdr_corp_name,mf.dba_name) as legal_name,\n                lpad(mf.federal_tax_id,9,'0')     as fed_tax_id,\n                to_char(mif_date_opened(mf.date_opened), 'mmddyyyyddd') as open_date\n        from    mif mf\n        where   ( mf.bank_number = 3942 or (mf.bank_number = 3941 and mf.group_2_association = '400078') )\n                and mf.date_stat_chgd_to_dcb is null\n        order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.FISUpload",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.FISUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:100^7*/
        
      rs = it.getResultSet();
      
      while(rs.next())
      {
        ffd.resetAllFields();
        
        ffd.setAllFieldData( rs );
        
        out.write( ffd.spew() );
        out.write("\r\n");
      }
      
      rs.close();
      it.close();
        
      // close outgoing file
      out.close();
        
      // send file
      if( TestMode == false && sendDataFile( workFilename, 
                    MesDefaults.getString(MesDefaults.DK_FIS_OUTGOING_HOST),    
                    MesDefaults.getString(MesDefaults.DK_FIS_OUTGOING_USER),     
                    MesDefaults.getString(MesDefaults.DK_FIS_OUTGOING_PASSWORD),
                    MesDefaults.getString(MesDefaults.DK_FIS_OUTGOING_PATH),
                    false,
                    true) == true )
      {
        archiveDailyFile(workFilename);
      }
        
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }
  
  public static void main(String[] args)
  {
    try
    {
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_FIS_OUTGOING_HOST,
                    MesDefaults.DK_FIS_OUTGOING_USER,
                    MesDefaults.DK_FIS_OUTGOING_PASSWORD,
                    MesDefaults.DK_FIS_OUTGOING_PATH
            });
        }

      com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
      FISUpload worker = new FISUpload();
      
      //worker.TestMode = true;
      
      worker.execute();
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/