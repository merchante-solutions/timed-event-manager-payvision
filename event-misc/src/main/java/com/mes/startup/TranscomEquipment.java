/*@lineinfo:filename=TranscomEquipment*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TranscomEquipment.sqlj $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 8/13/04 1:55p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class TranscomEquipment extends TranscomFileProcess
{
  private Vector    apps      = new Vector();
  private int       sequence  = 0;
  
  public TranscomEquipment()
  {
    super(FILE_TYPE_EQUIPMENT);
  }
  
  public TranscomEquipment(String connectionString)
  {
    super(FILE_TYPE_EQUIPMENT, connectionString);
  }
  
  protected void markFilesProcessed()
  {
    try
    {
      super.markFilesProcessed();
      
      /*@lineinfo:generated-code*//*@lineinfo:57^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_process_equipment
//          set     process_date = sysdate
//          where   process_job = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_process_equipment\n        set     process_date = sysdate\n        where   process_job =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.TranscomEquipment",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:62^7*/
    }
    catch(Exception e)
    {
      logEntry("markFilesProcessed()", e.toString());
    }
  }
  
  private void markAppsToProcess()
  {
    ResultSetIterator   it      = null;
    
    try
    {
      // mark new app seq nums with the current process sequence
      /*@lineinfo:generated-code*//*@lineinfo:77^7*/

//  ************************************************************
//  #sql [Ctx] { update  transcom_equipment_update
//          set     process_job = :procSequence
//          where   process_job = -1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  transcom_equipment_update\n        set     process_job =  :1 \n        where   process_job = -1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.TranscomEquipment",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:82^7*/
      
      // get all app seq nums we need to process
      /*@lineinfo:generated-code*//*@lineinfo:85^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num
//          from    transcom_equipment_update
//          where   process_job = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num\n        from    transcom_equipment_update\n        where   process_job =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.TranscomEquipment",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.TranscomEquipment",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^7*/
      
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {
        apps.add(rs.getLong("app_seq_num"));
      }
      
      it.close();
    }
    catch(Exception e)
    {
      setError("markAppsToProcess()" + e.toString());
      logEntry("markAppsToProcess()", e.toString());
    }
  }
  
  private String processStringField(String field)
  {
    String result = "";
    
    if(field != null && !field.equals(""))
    {
      result = field;
    }
    
    return result;
  }
  
  private void getEquipmentData(long appSeqNum)
  {
    ResultSetIterator     it          = null;
    ResultSet             rs          = null;
    Vector                rows        = new Vector();
    int                   itemCount   = 0;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:131^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.app_seq_num                   app_seq_num,
//                  m.merc_cntrl_number             control_number,
//                  me.merchequip_equip_quantity    equip_count,
//                  me.equiplendtype_code           lend_type,
//                  me.merchequip_amount            equip_amount,
//                  ea.equip_buy_min                sale_cost,
//                  ea.equip_rent_min               rental_cost,
//                  me.equip_model                  eq1type,
//                  ea.equip_description            eqdesc
//          from    merchant m,
//                  merchequipment me,
//                  equipment_application ea
//          where   m.app_seq_num = :appSeqNum and
//                  m.app_seq_num = me.app_seq_num and
//                  me.equiplendtype_code in (1, 2) and
//                  me.equip_model = ea.equip_model(+) and
//                  ea.app_type(+) = 28      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.app_seq_num                   app_seq_num,\n                m.merc_cntrl_number             control_number,\n                me.merchequip_equip_quantity    equip_count,\n                me.equiplendtype_code           lend_type,\n                me.merchequip_amount            equip_amount,\n                ea.equip_buy_min                sale_cost,\n                ea.equip_rent_min               rental_cost,\n                me.equip_model                  eq1type,\n                ea.equip_description            eqdesc\n        from    merchant m,\n                merchequipment me,\n                equipment_application ea\n        where   m.app_seq_num =  :1  and\n                m.app_seq_num = me.app_seq_num and\n                me.equiplendtype_code in (1, 2) and\n                me.equip_model = ea.equip_model(+) and\n                ea.app_type(+) = 28";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.TranscomEquipment",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.TranscomEquipment",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:150^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        itemCount   = rs.getInt("equip_count");
        
        EquipData   ed = new EquipData(rs);
        
        // add items to vector
        for(int i=0; i<itemCount; ++i)
        {
          rows.add(ed);
        }
      }
      
      rs.close();
      it.close();
      
      for(int i=0; i < rows.size(); ++i)
      {
        EquipData ed = (EquipData)(rows.elementAt(i));
        
        // insert a row into trancom_process_equipment for each row in rows
        /*@lineinfo:generated-code*//*@lineinfo:175^9*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_process_equipment
//            (
//              process_job, 
//              job_sequence, 
//              app_seq_num, 
//              control_number, 
//              type, 
//              eq_sale_pr, 
//              field_sale_pr, 
//              rental_payment, 
//              rental_cost, 
//              eq1type, 
//              eqdesc, 
//              fourpay
//            )
//            values
//            (
//              :procSequence,
//              :this.sequence++,
//              :ed.appSeqNum,
//              :ed.controlNum,
//              :ed.type,
//              :ed.salePrice,
//              :ed.saleCost,
//              :ed.rentPayment,
//              :ed.rentCost,
//              :ed.eq1Type,
//              :ed.eqDesc,
//              'N'
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_4835 = this.sequence++;
   String theSqlTS = "insert into transcom_process_equipment\n          (\n            process_job, \n            job_sequence, \n            app_seq_num, \n            control_number, \n            type, \n            eq_sale_pr, \n            field_sale_pr, \n            rental_payment, \n            rental_cost, \n            eq1type, \n            eqdesc, \n            fourpay\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n            'N'\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.TranscomEquipment",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   __sJT_st.setInt(2,__sJT_4835);
   __sJT_st.setLong(3,ed.appSeqNum);
   __sJT_st.setLong(4,ed.controlNum);
   __sJT_st.setString(5,ed.type);
   __sJT_st.setDouble(6,ed.salePrice);
   __sJT_st.setDouble(7,ed.saleCost);
   __sJT_st.setDouble(8,ed.rentPayment);
   __sJT_st.setDouble(9,ed.rentCost);
   __sJT_st.setString(10,ed.eq1Type);
   __sJT_st.setString(11,ed.eqDesc);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:207^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("getEquipmentData(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  private void collectEquipmentData()
  {
    for(int i=0; i < apps.size(); ++i)
    {
      long appSeqNum = ((Long)apps.elementAt(i)).longValue();
      
      getEquipmentData(appSeqNum);
    }
  }
  
  private void setTranscomProcess()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:236^7*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_process
//          (
//            file_type,
//            process_sequence,
//            load_filename
//          )
//          values
//          (
//            :FILE_TYPE_EQUIPMENT,
//            :procSequence,
//            'equipment'
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into transcom_process\n        (\n          file_type,\n          process_sequence,\n          load_filename\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          'equipment'\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.TranscomEquipment",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,FILE_TYPE_EQUIPMENT);
   __sJT_st.setInt(2,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:250^7*/
    }
    catch(Exception e)
    {
      setError("setTranscomProcess()" + e.toString());
      logEntry("setTranscomProcess()", e.toString());
    }
  }
  
  protected void loadCSVFile(Vector unused)
  {
    ResultSetIterator     it      = null;
    
    try
    {
      csvFile.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:267^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  control_number,
//                  type,
//                  eq_sale_pr,
//                  field_sale_pr,
//                  rental_payment,
//                  rental_cost,
//                  lease_term,
//                  serial_no,
//                  vnum,
//                  eq1type, 
//                  eqdesc, 
//                  eq_ship_date, 
//                  fff, 
//                  mo_lease_pay, 
//                  lease_rating, 
//                  app_code, 
//                  fourpay
//          from    transcom_process_equipment
//          where   process_job = :procSequence
//          order by job_sequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  control_number,\n                type,\n                eq_sale_pr,\n                field_sale_pr,\n                rental_payment,\n                rental_cost,\n                lease_term,\n                serial_no,\n                vnum,\n                eq1type, \n                eqdesc, \n                eq_ship_date, \n                fff, \n                mo_lease_pay, \n                lease_rating, \n                app_code, \n                fourpay\n        from    transcom_process_equipment\n        where   process_job =  :1 \n        order by job_sequence";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.TranscomEquipment",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.TranscomEquipment",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:289^7*/
      
      ResultSet rs = it.getResultSet();
      
      // set the header
      csvFile.setHeader(rs);
      
      // add rows
      csvFile.addRows(rs, "MM/dd/yyyy");
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadCSVFile()", e.toString());
      setError("loadCSVFile(): " + e.toString());
    }
  }
  
  protected void preProcess()
  {
    try
    {
      // get all the necessary data from the transcom_equipment_update table
      markAppsToProcess();
      
      // collect the equipment info into the transcom_process_equipment table
      if(! getError())
      {
        collectEquipmentData();
      }
      
      // add an entry to the transcom_process table so the load will occur
      if(! getError())
      {
        setTranscomProcess();
      }
    }
    catch(Exception e)
    {
      setError("preProcess(): " + e.toString());
      logEntry("preProcess()", e.toString());
    }
  }
  
  public class EquipData
  {
    public long           appSeqNum   = 0L;
    public long           controlNum  = 0L;
    public String         type        = "";
    public double         salePrice   = 0.0D;
    public double         saleCost    = 0.0D;
    public double         rentPayment = 0.0D;
    public double         rentCost    = 0.0D;
    public String         eq1Type     = "";
    public String         eqDesc      = "";
    
    public EquipData(ResultSet rs)
    {
      try
      {
        appSeqNum   = rs.getLong("app_seq_num");
        controlNum  = rs.getLong("control_number");
        eq1Type     = rs.getString("eq1type");
        eqDesc      = rs.getString("eqdesc");
        
        switch(rs.getInt("lend_type"))
        {
          case mesConstants.APP_EQUIP_PURCHASE:
            type = "P";
            salePrice = rs.getDouble("equip_amount");
            saleCost  = rs.getDouble("sale_cost");
            break;
          case mesConstants.APP_EQUIP_RENT:
            type = "R";
            rentPayment = rs.getDouble("equip_amount");
            rentCost  = rs.getDouble("rental_cost");
            break;
        }
      }
      catch(Exception e)
      {
        logEntry("EquipData constructor", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/