/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-vs-ammf/src/main/com/mes/util/jaxb/VisaAmmfOutgoingProcessor.java $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-04-08 15:28:05 -0700 (Wed, 08 Apr 2015) $
  Version            : $Revision: 23558 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.startup;

import java.io.File;
import java.util.Iterator;
import java.util.Vector;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.utils.jaxb.AmmfParser;

public class VisaAmmfIncomingProcessor extends EventBase
{
  public boolean execute()
  {
    Vector      dataFilenames   = new Vector();
    String      dataFilename    = null;
    String      flagFilename    = null;
    boolean     retVal          = false;
    Sftp        sftp            = null;
    AmmfParser  parser          = null;
    
    try
    {
      //get the Sftp connection
      sftp = getSftp( MesDefaults.getString(MesDefaults.DK_VISA_HOST),
                      MesDefaults.getString(MesDefaults.DK_VISA_USER),
                      MesDefaults.getString(MesDefaults.DK_VISA_PASSWORD),
                      MesDefaults.getString(MesDefaults.DK_VISA_INCOMING_PATH),
                      false     // !binary
                    );
      
      log.debug("Attempting to retrieve AMMF files for loading");
      EnumerationIterator enumi = new EnumerationIterator();
      Iterator it = enumi.iterator(sftp.getNameListing("AMMF" + ".*\\.flg"));
      while( it.hasNext() )
      {
        flagFilename = (String)it.next();
        dataFilename = flagFilename.substring(0,flagFilename.lastIndexOf(".")) + ".dat";
        
        log.debug("retrieving " + dataFilename);
        sftp.download(dataFilename, dataFilename);
        sftp.deleteFile(flagFilename);
        sftp.deleteFile(dataFilename);
        
        dataFilenames.addElement(dataFilename);
      }
      
      for( int i = 0; i < dataFilenames.size(); ++i )
      {
        dataFilename = (String)dataFilenames.elementAt(i);
        
        log.debug("parse " + dataFilename);
        parser = new AmmfParser();
        parser.parse(dataFilename);
        (new File(dataFilename)).delete();
      }
      retVal = true;
    }
    catch(Exception e)
    {
      e.printStackTrace();
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ sftp.disconnect(); } catch( Exception ee ) {}
    }
    return( retVal );
  }
  
  public static void main( String[] args )
  {
    VisaAmmfIncomingProcessor   proc    = null;

    try
    {
      proc = new VisaAmmfIncomingProcessor();
      proc.connect(true);
      proc.execute();
    }
    finally
    {
      try{ proc.cleanUp(); } catch(Exception e){}
      log.debug("Command line execution of VisaAmmfIncomingProcessor complete");
      Runtime.getRuntime().exit(0);
    }
  }
}
