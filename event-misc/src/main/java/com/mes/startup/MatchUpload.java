/*@lineinfo:filename=MatchUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/MatchUpload.sqlj $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-11-12 14:53:50 -0800 (Thu, 12 Nov 2015) $
  Version            : $Revision: 23993 $

  Change History:
     See SVN database

  Copyright (C) 2000-2005,2006 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.sql.ResultSet;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.ops.AddQueueBean;
import com.mes.ops.QueueConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;

public class MatchUpload extends EventBase
{
  static Logger log = Logger.getLogger(MatchUpload.class);

  public static final String MATCH_REQUEST_STATUS_NEW             = "N";  // new
  public static final String MATCH_REQUEST_STATUS_TEST            = "T";  // used for testing only
  public static final String MATCH_REQUEST_STATUS_RESUBMIT        = "S";  // re-Submit (functions like new request status)
  public static final String MATCH_REQUEST_STATUS_PENDING         = "P";  // in process
  public static final String MATCH_REQUEST_STATUS_LOADERROR       = "L";  // load error
  public static final String MATCH_REQUEST_STATUS_RESPPARSEERROR  = "R";  // error parsing response
  public static final String MATCH_REQUEST_STATUS_COMPLETE        = "C";  // complete
  
  public static class MatchBank
  {
    public int          BankNumber    = 0;
    public int          McIcaNumber   = 0;
    
    public MatchBank( int bankNumber, int icaNum )
    {
      BankNumber  = bankNumber;
      McIcaNumber = icaNum;
    }
  }
   
  private final static MatchBank [] ActiveBanks = 
  {
    new MatchBank( 3941, 5185),
//    new MatchBank( 3858, 6739)
  };
  
  public static class MatchFileFilter implements FilenameFilter
  {
    public boolean accept(File dir, String name)
    {
      return( name.startsWith("TT") );
    }
  }
  
  
  protected int               FileBankNumber          = 0;
  protected int               FileDetailRecordCount   = 0;
  protected int               FileDetailSeqNum        = 0;
  protected int               FileIcaNumber           = 0;
  protected String            FileTransmissionId      = null;
  protected boolean           TestMode                = false;
  protected String            WorkFilename            = null;
  
  public MatchUpload( )
  {
    PropertiesFilename = "match-upload.properties";
  }
  
  public void buildMatchHeader( BufferedWriter out, ResultSet resultSet )
  { 
    StringBuffer        buffer      = new StringBuffer();
    String              dateString  = null;
    FlatFileRecord      ffd         = null;
      
    try
    {
      ffd = new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_MATCH_HEADER );
      ffd.setAllFieldData( resultSet );
      
      // build the file transmission id
      dateString = DateTimeFormatter.getFormattedDate( Calendar.getInstance().getTime(), "yyyyMMdd" );
      buffer.append( NumberFormatter.getPaddedInt( FileIcaNumber , 7 ) );
      buffer.append( dateString );
      buffer.append( NumberFormatter.getPaddedInt( extractFileId(WorkFilename), 2 ) );
      FileTransmissionId = buffer.toString();
      ffd.setFieldData("file_transmission_id",FileTransmissionId);
      
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildMatchHeader()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  public void buildMatchDetail( BufferedWriter out, ResultSet resultSet )
  {                               
    FlatFileRecord      ffd         = null;
    int                 flatFileId  = 0;
      
    try
    {
      // build the general record
      ffd = new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_MATCH_GENERAL );
      ffd.setAllFieldData( resultSet );
      ffd.setFieldData("seq_num",++FileDetailSeqNum);
      out.write( ffd.spew() );
      out.newLine();
      
      // build the principal record
      ffd.setDefType( MesFlatFiles.DEF_TYPE_MATCH_PRINCIPAL );
      ffd.setAllFieldData( resultSet );
      ffd.setFieldData("seq_num",++FileDetailSeqNum);
      out.write( ffd.spew() );
      out.newLine();
      
      FileDetailRecordCount++;
      
      if ( !TestMode )
      {
        long seqNum = resultSet.getLong("match_seq_num");
      
        // update the status in the database for this record
        /*@lineinfo:generated-code*//*@lineinfo:164^9*/

//  ************************************************************
//  #sql [Ctx] { update  match_requests mrq
//            set     process_start_date = sysdate,
//                    request_status     = :MATCH_REQUEST_STATUS_PENDING,
//                    output_filename    = :WorkFilename
//            where   match_seq_num = :seqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  match_requests mrq\n          set     process_start_date = sysdate,\n                  request_status     =  :1 ,\n                  output_filename    =  :2 \n          where   match_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.MatchUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,MATCH_REQUEST_STATUS_PENDING);
   __sJT_st.setString(2,WorkFilename);
   __sJT_st.setLong(3,seqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:171^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("buildMatchDetail()", e.toString());
    }
    finally
    {
    }
  }
  
  public void buildMatchTrailer( BufferedWriter out )
  { 
    FlatFileRecord      ffd         = null;
      
    try
    {
      ffd = new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_MATCH_TRAILER );
      ffd.setFieldData("file_transmission_id",FileTransmissionId);
      ffd.setFieldData("total_inquiry_record_count", FileDetailRecordCount);
      ffd.setFieldData("total_record_count", FileDetailRecordCount);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildMatchTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      
      processOutgoing();    // send new match requests
      processIncoming();    // pick up and process match responses
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( true );
  }
  
  protected long getAppSeqNum( int matchSeqNum )
  {
    long    retVal      = 0L;
  
    try 
    {
      int recCount;
      
      /*@lineinfo:generated-code*//*@lineinfo:242^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(mrq.app_seq_num)
//          
//          from    match_requests    mrq
//          where   to_number(substr(mrq.match_seq_num, length(mrq.match_seq_num)-4, 5)) = :matchSeqNum
//                  and process_start_date > sysdate-2000
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mrq.app_seq_num)\n         \n        from    match_requests    mrq\n        where   to_number(substr(mrq.match_seq_num, length(mrq.match_seq_num)-4, 5)) =  :1 \n                and process_start_date > sysdate-2000";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.MatchUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,matchSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:249^7*/
      
      if( recCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:253^9*/

//  ************************************************************
//  #sql [Ctx] { select  distinct mrq.app_seq_num
//            
//            from    match_requests    mrq
//            where   to_number(substr(mrq.match_seq_num, length(mrq.match_seq_num)-4, 5)) = :matchSeqNum
//                    and process_start_date > sysdate-2000
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct mrq.app_seq_num\n           \n          from    match_requests    mrq\n          where   to_number(substr(mrq.match_seq_num, length(mrq.match_seq_num)-4, 5)) =  :1 \n                  and process_start_date > sysdate-2000";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.MatchUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,matchSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:260^9*/
      }
    }
    catch( Exception e ) 
    {
      logEntry("getAppSeqNum(" + String.valueOf(matchSeqNum) + ")", e.toString());
    }
    return( retVal );
  }
  
  protected boolean isAppDeclined( long appSeqNum )
  {
    int     recCount    = 0;
  
    try 
    {
      /*@lineinfo:generated-code*//*@lineinfo:276^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//          from    app_queue   aq
//          where   aq.app_seq_num = :appSeqNum and
//                  app_queue_type = :com.mes.ops.QueueConstants.QUEUE_CREDIT and -- 1 and
//                  app_queue_stage = :com.mes.ops.QueueConstants.Q_CREDIT_DECLINE -- 11
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n        from    app_queue   aq\n        where   aq.app_seq_num =  :1  and\n                app_queue_type =  :2  and -- 1 and\n                app_queue_stage =  :3  -- 11";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.MatchUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,com.mes.ops.QueueConstants.QUEUE_CREDIT);
   __sJT_st.setInt(3,com.mes.ops.QueueConstants.Q_CREDIT_DECLINE);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:283^7*/
    }
    catch( Exception e ) 
    {
      logEntry("isAppDeclined(" + String.valueOf(appSeqNum) + ")", e.toString());
    }
    return( recCount > 0 );
  }
  
  protected void processIncoming( )
  {
    try 
    {
      // establish the path to the incoming files
      String path = MesDefaults.getString(MesDefaults.DK_MATCH_INCOMING_PATH);
      
      // append a final file seperator if necessary
//      char lastChar = path.charAt( path.length()-1 );
//      if ( lastChar != '/' && lastChar != '\\' )
//      {
//        path = (path + "/");
//      }

      File dir = new File( path );
      
      MatchFileFilter filter = new MatchFileFilter();
      
      // open directory and list contents
      String[] files = dir.list(filter);
      
      if( files != null )
      {
        for( int i=0; i < files.length; ++i )
        {
          log.debug("found file: " + files[i]);
          String filename = files[i];
          processResponseFile(path + "/" + filename);
        }
      }
      else
      {
        log.debug("no files to process");
      }
    }
    catch(Exception e) 
    {
      logEntry("processIncoming()", e.toString());
    }
    finally
    {
    }
  }
  
  protected void processOutgoing( )
  {
    ResultSetIterator   it              = null;
    BufferedWriter      out             = null;
    String              path            = null;
    ResultSet           resultSet       = null;
    
    try
    {
      // establish the path for the outgoing file
      path = MesDefaults.getString(MesDefaults.DK_MATCH_OUTGOING_PATH);
      
      // append a final file seperator if necessary
      char lastChar = path.charAt( path.length()-1 );
      if ( lastChar != '/' && lastChar != '\\' )
      {
        path = (path + "/");
      }
    
      for( int i = 0; i < ActiveBanks.length; ++i )
      {
        // set bank specific values
        FileIcaNumber         = ActiveBanks[i].McIcaNumber;
        FileBankNumber        = ActiveBanks[i].BankNumber;
        
        // resetn output variables
        FileDetailRecordCount = 0;
        FileDetailSeqNum      = 0;
        out                   = null;
        
        /*@lineinfo:generated-code*//*@lineinfo:366^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mrq.match_seq_num                       as match_seq_num,
//                    :FileIcaNumber                          as acq_customer_number,
//                    -- only take the last 5 digits of sequence number
//                    substr(mrq.match_seq_num,greatest(-5,-length(mrq.match_seq_num)))            
//                                                            as tran_ref_num,
//                    mrq.app_seq_num                         as app_seq_num,
//                    mrq.process_start_date                  as start_date,
//                    mrq.process_end_date                    as end_date,
//                    mrq.request_status                      as status,
//                    mrq.request_action                      as action,
//                    mrq.response_action                     as response_action,
//                    mrq.imp_response                        as imp_response,
//                    mrq.imp_status                          as imp_status,
//                    mrq.request_reason_code                 as request_reason,
//                    mrq.request_comments                    as comments,
//                    mrq.error_codes                         as error_codes,
//                    mrq.mastercard_ref_number               as mc_ref_num,
//                    mrq.visa_bin                            as visa_bin,
//                    mrq.orig_mastercard_refnum              as orig_mc_ref_num,
//                    mr.merch_number                         as merchant_number,
//                    mr.merch_application_type               as app_type,
//                    mr.merch_cat_code                       as sic_code,
//                    mr.merch_date_opened                    as date_opened,
//                    mr.merch_statetax_id                    as state_tax_id,
//                    mr.merch_federal_tax_id                 as fed_tax_id,
//                    mr.merch_web_url                        as web_url,
//                    mr.merch_legal_name                     as legal_name,
//                    mr.merch_business_name                  as dba_name,
//                    nvl(addr_m.address_line1,
//                        addr_b.address_line1)               as business_addr_line1,
//                    nvl(addr_m.address_line2,
//                        addr_b.address_line2)               as business_addr_line2,
//                    nvl(addr_m.address_city,
//                        addr_b.address_city)                as business_city,
//                    nvl(addr_m.countrystate_code,
//                        addr_b.countrystate_code)           as business_state,
//                    nvl(addr_m.address_zip,
//                        addr_b.address_zip)                 as business_zip,
//                    decode( nvl(addr_m.country_code,nvl(addr_b.country_code,'USA')),
//                            'US','USA',
//                            nvl(addr_m.country_code,nvl(addr_b.country_code,'USA'))
//                          )                                 as business_country,
//                    nvl(addr_m.address_phone,
//                        addr_b.address_phone)               as business_phone,
//                    mb.merchbank_acct_num                   as dda,
//                    bo.busowner_num                         as id_principal,
//                    bo.busowner_last_name                   as owner_last_name,
//                    bo.busowner_first_name                  as owner_first_name,
//                    bo.busowner_name_initial                as owner_initial,
//                    nvl(addr_o.address_line1,
//                        addr_m.address_line1)               as owner_addr_line1,
//                    nvl(addr_o.address_line2,
//                        addr_m.address_line2)               as owner_addr_line2,
//                    nvl(addr_o.address_city,
//                        addr_m.address_city)                as owner_city,
//                    nvl(addr_o.countrystate_code,
//                        addr_m.countrystate_code)           as owner_state,
//                    nvl(addr_o.address_zip,
//                        addr_m.address_zip)                 as owner_zip,
//                    decode(nvl(addr_o.country_code,nvl(addr_m.country_code,'USA')),
//                           'US','USA',
//                           nvl(addr_o.country_code,nvl(addr_m.country_code,'USA'))
//                          )                                 as owner_country,
//                    decode(nvl(addr_o.address_phone,addr_m.address_phone),
//                           '0',null,
//                           nvl(addr_o.address_phone,addr_m.address_phone)
//                          )                                 as owner_phone,                      
//                    nvl(dukpt_decrypt_wrapper(bo.busowner_ssn_enc), bo.busowner_ssn)
//                                                            as owner_ssn,
//                    bo.busowner_drvlicnum                   as owner_dl,
//                    mc.merchcont_prim_first_name            as contact_first_name,
//                    mc.merchcont_prim_last_name             as contact_last_name,
//                    mc.merchcont_prim_name_initial          as contact_initial,
//                    mc.merchcont_prim_phone                 as contact_phone
//            from    match_requests          mrq,
//                    merchant                mr,
//                    address                 addr_b,
//                    address                 addr_m,
//                    address                 addr_o,
//                    businessowner           bo,
//                    merchbank               mb,
//                    merchcontact            mc
//            where   mrq.request_status in
//                    (
//                      :MATCH_REQUEST_STATUS_NEW,      -- 'N',
//                      :MATCH_REQUEST_STATUS_RESUBMIT  -- 'S'
//                    ) and
//                    mr.app_seq_num = mrq.app_seq_num and
//                    addr_b.app_seq_num(+) = mr.app_seq_num and 
//                    addr_b.addresstype_code(+) = :mesConstants.ADDR_TYPE_BUSINESS and -- 1 and
//                    addr_m.app_seq_num(+) = mr.app_seq_num and 
//                    addr_m.addresstype_code(+) = :mesConstants.ADDR_TYPE_MAILING and -- 2 and
//                    addr_o.app_seq_num(+) = mr.app_seq_num and 
//                    addr_o.addresstype_code(+) = :mesConstants.ADDR_TYPE_OWNER1 and -- 4 and
//                    bo.app_seq_num(+) = mr.app_seq_num and
//                    length(bo.busowner_last_name) > 0 and
//                    mb.app_seq_num(+) = mr.app_seq_num and
//                    mc.app_seq_num(+) = mr.app_seq_num 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mrq.match_seq_num                       as match_seq_num,\n                   :1                           as acq_customer_number,\n                  -- only take the last 5 digits of sequence number\n                  substr(mrq.match_seq_num,greatest(-5,-length(mrq.match_seq_num)))            \n                                                          as tran_ref_num,\n                  mrq.app_seq_num                         as app_seq_num,\n                  mrq.process_start_date                  as start_date,\n                  mrq.process_end_date                    as end_date,\n                  mrq.request_status                      as status,\n                  mrq.request_action                      as action,\n                  mrq.response_action                     as response_action,\n                  mrq.imp_response                        as imp_response,\n                  mrq.imp_status                          as imp_status,\n                  mrq.request_reason_code                 as request_reason,\n                  mrq.request_comments                    as comments,\n                  mrq.error_codes                         as error_codes,\n                  mrq.mastercard_ref_number               as mc_ref_num,\n                  mrq.visa_bin                            as visa_bin,\n                  mrq.orig_mastercard_refnum              as orig_mc_ref_num,\n                  mr.merch_number                         as merchant_number,\n                  mr.merch_application_type               as app_type,\n                  mr.merch_cat_code                       as sic_code,\n                  mr.merch_date_opened                    as date_opened,\n                  mr.merch_statetax_id                    as state_tax_id,\n                  mr.merch_federal_tax_id                 as fed_tax_id,\n                  mr.merch_web_url                        as web_url,\n                  mr.merch_legal_name                     as legal_name,\n                  mr.merch_business_name                  as dba_name,\n                  nvl(addr_m.address_line1,\n                      addr_b.address_line1)               as business_addr_line1,\n                  nvl(addr_m.address_line2,\n                      addr_b.address_line2)               as business_addr_line2,\n                  nvl(addr_m.address_city,\n                      addr_b.address_city)                as business_city,\n                  nvl(addr_m.countrystate_code,\n                      addr_b.countrystate_code)           as business_state,\n                  nvl(addr_m.address_zip,\n                      addr_b.address_zip)                 as business_zip,\n                  decode( nvl(addr_m.country_code,nvl(addr_b.country_code,'USA')),\n                          'US','USA',\n                          nvl(addr_m.country_code,nvl(addr_b.country_code,'USA'))\n                        )                                 as business_country,\n                  nvl(addr_m.address_phone,\n                      addr_b.address_phone)               as business_phone,\n                  mb.merchbank_acct_num                   as dda,\n                  bo.busowner_num                         as id_principal,\n                  bo.busowner_last_name                   as owner_last_name,\n                  bo.busowner_first_name                  as owner_first_name,\n                  bo.busowner_name_initial                as owner_initial,\n                  nvl(addr_o.address_line1,\n                      addr_m.address_line1)               as owner_addr_line1,\n                  nvl(addr_o.address_line2,\n                      addr_m.address_line2)               as owner_addr_line2,\n                  nvl(addr_o.address_city,\n                      addr_m.address_city)                as owner_city,\n                  nvl(addr_o.countrystate_code,\n                      addr_m.countrystate_code)           as owner_state,\n                  nvl(addr_o.address_zip,\n                      addr_m.address_zip)                 as owner_zip,\n                  decode(nvl(addr_o.country_code,nvl(addr_m.country_code,'USA')),\n                         'US','USA',\n                         nvl(addr_o.country_code,nvl(addr_m.country_code,'USA'))\n                        )                                 as owner_country,\n                  decode(nvl(addr_o.address_phone,addr_m.address_phone),\n                         '0',null,\n                         nvl(addr_o.address_phone,addr_m.address_phone)\n                        )                                 as owner_phone,                      \n                  nvl(dukpt_decrypt_wrapper(bo.busowner_ssn_enc), bo.busowner_ssn)\n                                                          as owner_ssn,\n                  bo.busowner_drvlicnum                   as owner_dl,\n                  mc.merchcont_prim_first_name            as contact_first_name,\n                  mc.merchcont_prim_last_name             as contact_last_name,\n                  mc.merchcont_prim_name_initial          as contact_initial,\n                  mc.merchcont_prim_phone                 as contact_phone\n          from    match_requests          mrq,\n                  merchant                mr,\n                  address                 addr_b,\n                  address                 addr_m,\n                  address                 addr_o,\n                  businessowner           bo,\n                  merchbank               mb,\n                  merchcontact            mc\n          where   mrq.request_status in\n                  (\n                     :2 ,      -- 'N',\n                     :3   -- 'S'\n                  ) and\n                  mr.app_seq_num = mrq.app_seq_num and\n                  addr_b.app_seq_num(+) = mr.app_seq_num and \n                  addr_b.addresstype_code(+) =  :4  and -- 1 and\n                  addr_m.app_seq_num(+) = mr.app_seq_num and \n                  addr_m.addresstype_code(+) =  :5  and -- 2 and\n                  addr_o.app_seq_num(+) = mr.app_seq_num and \n                  addr_o.addresstype_code(+) =  :6  and -- 4 and\n                  bo.app_seq_num(+) = mr.app_seq_num and\n                  length(bo.busowner_last_name) > 0 and\n                  mb.app_seq_num(+) = mr.app_seq_num and\n                  mc.app_seq_num(+) = mr.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.MatchUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,FileIcaNumber);
   __sJT_st.setString(2,MATCH_REQUEST_STATUS_NEW);
   __sJT_st.setString(3,MATCH_REQUEST_STATUS_RESUBMIT);
   __sJT_st.setInt(4,mesConstants.ADDR_TYPE_BUSINESS);
   __sJT_st.setInt(5,mesConstants.ADDR_TYPE_MAILING);
   __sJT_st.setInt(6,mesConstants.ADDR_TYPE_OWNER1);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.MatchUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:466^9*/
        resultSet = it.getResultSet();
      
        while ( resultSet.next() )
        {
          if ( out == null )    // first record
          {
            WorkFilename = generateFilename("match" + String.valueOf(FileBankNumber));
            
            log.debug("WorkFilename: " + WorkFilename);
            
            out = new BufferedWriter( new FileWriter( (path + WorkFilename), false ) );
            
            buildMatchHeader(out,resultSet);
          }
          
          // build the detail record for this row
          buildMatchDetail(out,resultSet);
        }
        
        if ( out != null ) 
        {
          buildMatchTrailer(out);
          out.close();
        }          
      }        
    }
    catch( Exception e )
    {
      logEntry("processOutgoing()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception ee) {}
    }
  }
  
  protected void processResponseFile( String filename )
  {
    char            actionCode        = '\0';
    long            appSeqNum         = 0L;
    String          errorCodes        = null;
    FlatFileRecord  ffd               = new FlatFileRecord(Ctx);
    BufferedReader  file              = null;
    String          generalRecord     = null;
    int             genId             = 0;
    String          principalRecord   = null;
    int             prinId            = 0;
    int             lastSeqNum        = 0;
    String          line              = null;
    int             lineCount         = 0;
    String          mcRefNum          = null;
    String          lastMcRefNum      = "";
    String          mcRefNumOrig      = null;
    int             queueId           = 0;
    char            recordType        = '\0';
    int             seqNum            = 0;

    try
    {
      log.debug("Processing response file " + filename);
      file = new BufferedReader( new FileReader( filename ) );
      
      // loop until the file is empty or we find the first detail record
      while ( (line = file.readLine()) != null )
      {
        ++lineCount;
        if ( line.startsWith("653") )
        {
          // extract required fields
          actionCode  = line.charAt(3);
          recordType  = line.charAt(17);
        
          switch( actionCode )
          {
            case 'E':     // Error in the add or inquiry record
              genId   = MesFlatFiles.DEF_TYPE_MATCH_RESP_GEN_E;   // 308
              prinId  = MesFlatFiles.DEF_TYPE_MATCH_RESP_PRIN_E;  // 309
              queueId = QueueConstants.Q_MATCH_ERROR;
              break;
            
            case 'N':     // no merchant possible match
              genId   = MesFlatFiles.DEF_TYPE_MATCH_RESP_GEN_N;   // 306
              prinId  = MesFlatFiles.DEF_TYPE_MATCH_RESP_PRIN_N;  // 307
              break;
            
            case 'P':     // Possible match
            case 'R':     // Retroactive possible match
              genId   = MesFlatFiles.DEF_TYPE_MATCH_RESP_GEN_PR;  // 310
              prinId  = MesFlatFiles.DEF_TYPE_MATCH_RESP_PRIN_PR; // 311
              queueId = QueueConstants.Q_MATCH_PEND;
              break;
            
            case 'M':     // Possible merchant match
            case 'H':     // Previously reported possible merchant match
              genId   = MesFlatFiles.DEF_TYPE_MATCH_RESP_GEN_MH;  // 312
              prinId  = MesFlatFiles.DEF_TYPE_MATCH_RESP_PRIN_MH; // 313
              break;
            
            case 'Q':     // Possible inquiry match
            case 'Y':     // Previously reported possible inquiry match             
              genId   = MesFlatFiles.DEF_TYPE_MATCH_RESP_GEN_QY;  // 314
              prinId  = MesFlatFiles.DEF_TYPE_MATCH_RESP_PRIN_QY; // 315
              break;
          
  //          case 'A':     // Merchant was added
  //          case 'C':     // merchant record corrected
  //          case 'D':     // Merchant record deleted
            default:  
              break;
          }
      
          // store the record for insertion raw into the database
          // this allows the legacy code to work correctly (*s/b re-written)
          switch( recordType )
          {
            case '1':   // general record
              ffd.setDefType(genId);
              
              generalRecord = line;
              
              // MC does not fill their record to length, so do it for them
              ffd.suck(StringUtilities.leftJustify(line,ffd.getRecordLength(),' '));
              
              // extract necessary fields
              seqNum        = ffd.getFieldAsInt("tran_ref_num");
              errorCodes    = ffd.getFieldRawData("error_codes");
              mcRefNum      = ffd.getFieldRawData("mc_ref_num");
              mcRefNumOrig  = ffd.getFieldRawData("mc_ref_num_orig");
              
              // for action code 'M', look for Original MasterCard Reference Number
              // to match with previous MasterCard Reference Number
              if ( seqNum == 0 && mcRefNumOrig.equals(lastMcRefNum) )
              {
                seqNum = lastSeqNum;
                mcRefNum = mcRefNumOrig;
              }

              // responses often have multiple gen/prin record sets.
              // only execute this code for the first general row 
              // for this match request.
              if ( lastSeqNum != seqNum )
              {
                // get the application associated with this match request
                appSeqNum = getAppSeqNum(seqNum);
                
                if( TestMode == false )
                {
                  log.debug("Updating match request " + seqNum);
                  log.debug("===================================================");
                  
                  // update the requests table
                  /*@lineinfo:generated-code*//*@lineinfo:618^19*/

//  ************************************************************
//  #sql [Ctx] { update  match_requests
//                      set     request_status          = 'C',
//                              process_end_date        = sysdate,
//                              response_action         = :String.valueOf(actionCode),
//                              error_codes             = :errorCodes,
//                              mastercard_ref_number   = :mcRefNum,
//                              orig_mastercard_refnum  = :mcRefNumOrig
//                      where   to_number(substr(match_seq_num, length(match_seq_num)-4, 5)) = :seqNum and
//                              request_status = :MATCH_REQUEST_STATUS_PENDING -- 'P'
//                     };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4327 = String.valueOf(actionCode);
   String theSqlTS = "update  match_requests\n                    set     request_status          = 'C',\n                            process_end_date        = sysdate,\n                            response_action         =  :1 ,\n                            error_codes             =  :2 ,\n                            mastercard_ref_number   =  :3 ,\n                            orig_mastercard_refnum  =  :4 \n                    where   to_number(substr(match_seq_num, length(match_seq_num)-4, 5)) =  :5  and\n                            request_status =  :6  -- 'P'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.MatchUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4327);
   __sJT_st.setString(2,errorCodes);
   __sJT_st.setString(3,mcRefNum);
   __sJT_st.setString(4,mcRefNumOrig);
   __sJT_st.setInt(5,seqNum);
   __sJT_st.setString(6,MATCH_REQUEST_STATUS_PENDING);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:629^19*/
              
                  // delete any previous responses
                  /*@lineinfo:generated-code*//*@lineinfo:632^19*/

//  ************************************************************
//  #sql [Ctx] { delete
//                      from    match_requests_responsedata mrd
//                      where   mrd.match_seq_num = :seqNum
//                     };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n                    from    match_requests_responsedata mrd\n                    where   mrd.match_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.MatchUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,seqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:637^19*/
                  updateQueue( actionCode, appSeqNum, queueId );
                }
              }
              lastSeqNum = seqNum;    // store the match sequence number
              lastMcRefNum = mcRefNum;
              break;
    
            case '2':   // principal record
              principalRecord = line;
              
              if( TestMode == true )   // in test mode
              {
                log.debug("===================================================");
                log.debug("seqNum    : " + seqNum);
                log.debug("errors    : " + errorCodes);
                log.debug("mcRefNum  : " + mcRefNum);
                log.debug("mcRefNumO : " + mcRefNumOrig);
                log.debug("General   : " + generalRecord);
                log.debug("Principal : " + principalRecord);
              }
              else if ( seqNum != 0 ) // production mode, require request id
              {
                long respSeqNum   = 0L;
                /*@lineinfo:generated-code*//*@lineinfo:661^17*/

//  ************************************************************
//  #sql [Ctx] { select  match_response_data_sequence.nextval
//                    
//                    from    dual
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  match_response_data_sequence.nextval\n                   \n                  from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.MatchUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   respSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:666^17*/
                
                log.debug("Storing response data for " + seqNum + "   line# " + lineCount);
                // add record
                /*@lineinfo:generated-code*//*@lineinfo:670^17*/

//  ************************************************************
//  #sql [Ctx] { insert into match_requests_responsedata
//                    (
//                      match_seq_num,
//                      mrd_seq_num,
//                      general_record,
//                      principal_record,
//                      isacknowledged,
//                      is_imr,
//                      is_from_impblacklist
//                    )
//                    values
//                    (
//                      :seqNum,
//                      :respSeqNum,
//                      :generalRecord,
//                      :principalRecord,
//                      0,
//                      0,
//                      0
//                    )
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into match_requests_responsedata\n                  (\n                    match_seq_num,\n                    mrd_seq_num,\n                    general_record,\n                    principal_record,\n                    isacknowledged,\n                    is_imr,\n                    is_from_impblacklist\n                  )\n                  values\n                  (\n                     :1 ,\n                     :2 ,\n                     :3 ,\n                     :4 ,\n                    0,\n                    0,\n                    0\n                  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.MatchUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,seqNum);
   __sJT_st.setLong(2,respSeqNum);
   __sJT_st.setString(3,generalRecord);
   __sJT_st.setString(4,principalRecord);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:692^17*/
              }
              break;
          }
        }
      }        
      file.close();
      
      if ( TestMode == false )
      {
        File newFile = new File(filename);
        String curdt = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(), "MMddyyHHmmss");
        
        String newName = MesDefaults.getString(MesDefaults.DK_MATCH_INCOMING_PATH) + "/arc/" + newFile.getName() + "." + curdt;
        newFile.renameTo(new File(newName));
      }        
      
      // send an email indicating that the file was processed
      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_MATCH_INCOMING);
      
      msg.setSubject("MATCH Processed file: " + filename);
      msg.setText("MATCH Processed file: " + filename);
      
      msg.send();
    }
    catch( Exception e )
    {
      logEntry("processResponseFile(" + filename + ")",e.toString());
    }
    finally
    {
    }
  }
  
  protected void setTestMode( boolean testMode )
  {
    TestMode = testMode;
    log.debug("Mode is " + (TestMode ? "TEST" : "PROD") );
  }
  
  private void updateQueue( char actionCode, long appSeqNum, int queueId )
  {
    AddQueueBean    queueBean         = null;
    
    try
    {
      // setup the queue bean
      queueBean = new AddQueueBean();
      queueBean.connect();
    
      // base on the action, update the MES queues
      switch( actionCode )
      {
        case 'N':     // no merchant possible match, remove from queue
          log.debug("NO match, deleting from app_queue");
          /*@lineinfo:generated-code*//*@lineinfo:748^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    app_queue   aq
//              where   aq.app_seq_num = :appSeqNum and
//                      app_queue_type = :QueueConstants.QUEUE_MATCH and -- 20 and
//                      app_queue_stage in
//                      (
//                        :QueueConstants.Q_MATCH_PEND, -- 200,
//                        :QueueConstants.Q_MATCH_ERROR -- 201
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    app_queue   aq\n            where   aq.app_seq_num =  :1  and\n                    app_queue_type =  :2  and -- 20 and\n                    app_queue_stage in\n                    (\n                       :3 , -- 200,\n                       :4  -- 201\n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.MatchUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,QueueConstants.QUEUE_MATCH);
   __sJT_st.setInt(3,QueueConstants.Q_MATCH_PEND);
   __sJT_st.setInt(4,QueueConstants.Q_MATCH_ERROR);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:759^11*/
          break;

        case 'E':     // Error in the add or inquiry record
        case 'P':     // Possible match
        case 'R':     // Retroactive possible match
          log.debug("Error, possible match, or retroactive possible match");
          if ( isAppDeclined(appSeqNum) == false )
          {
            log.debug("  APP WAS DECLINED, ADDING TO MATCH QUEUE");
            queueBean.insertQueue(appSeqNum,
                                  QueueConstants.QUEUE_MATCH,
                                  queueId,
                                  QueueConstants.Q_STATUS_NONE,
                                  "system",-1,"system");
          }
          break;

//        case 'M':     // Possible merchant match
//        case 'H':     // Previously reported possible merchant match
//        case 'Q':     // Possible inquiry match
//        case 'Y':     // Previously reported possible inquiry match             
//        case 'A':     // Merchant was added
//        case 'C':     // merchant record corrected
//        case 'D':     // Merchant record deleted
        default:  
          log.debug("POSSIBLE MATCH, doing nothing");
          break;
      }
    }
    catch( Exception e )
    {
      logEntry( "updateQueue(" + String.valueOf(actionCode) + "," + 
                                 String.valueOf(appSeqNum) + "," +
                                 String.valueOf(queueId) + ")", e.toString() );
    }
    finally
    {
      try{ queueBean.cleanUp(); }catch(Exception e){}
    }
  }
  
  public static void main( String[] args )
  {
    MatchUpload        test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      test = new MatchUpload();
      
      if ( args.length > 0 )
      {
        test.connect();
        test.setTestMode(true);
        
        if ( args[0].equals("extractFileId") )
        {
          log.debug( "fileId = " + test.extractFileId(args[1]) );
        }
        else if ( args[0].equals("procOut") )
        {
          if ( args.length > 1 && args[1].equals("prod") )
          {
            test.setTestMode(false);
          }
          test.processOutgoing();
        }
        else if ( args[0].equals("procResp") )
        {
          if ( args.length > 2 && args[2].equals("prod") )
          {
            test.setTestMode(false);
          }
          test.processResponseFile(args[1]);
        }
        else if ( args[0].equals("procIn") )
        {
          test.processIncoming();
        }
        test.cleanUp();
      }
      else
      {
        test.execute();
      }
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/