/*@lineinfo:filename=DiscoverEntitlement*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/DiscoverEntitlement.sqlj $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-11-12 15:27:32 -0800 (Thu, 12 Nov 2015) $
  Version            : $Revision: 23997 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.support.PropertiesFile;
import sqlj.runtime.ResultSetIterator;

public class DiscoverEntitlement extends EventBase
{
  static Logger log = Logger.getLogger(DiscoverEntitlement.class);

  private int               FileBankNumber    = mesConstants.BANK_ID_MES;
  private int               FileRecordCount   = 0;
  private boolean           Verbose           = false;
  
  public final static int     PT_MERCHANT_PROFILE = 0;
  public final static int     PT_PCC_PRENOTIFY    = 1;
  public final static int     PT_COUNT            = 2;
  
  public DiscoverEntitlement( )
  {
  }
  
  private void buildHeader( BufferedWriter out )
  {
    FlatFileRecord        ffd     = null;
    
    try
    {
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_HDR);
      ffd.setFieldData("file_timestamp",Calendar.getInstance().getTime());
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildHeader()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  private void buildDetails( BufferedWriter out )
  {
    StringBuffer          controlNum    = new StringBuffer();
    int                   controlNumLen = 0;
    FlatFileRecord        ffd           = null;
    ResultSetIterator     it            = null;
    double                ratio         = 0.0;
    ResultSet             rs            = null;
    int                   seqNum        = 0;
    
    try
    {
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_ENTITLEMENT_DT);
    
      switch ( FileBankNumber )
      {
        case mesConstants.BANK_ID_UBOC:
        case mesConstants.BANK_ID_NBSC:
        case mesConstants.BANK_ID_BBT:
          /*@lineinfo:generated-code*//*@lineinfo:107^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group                           as portfolio_node,
//                      o.org_name                            as portfolio_name,
//                      '01'                                  as rec_type,
//                      mf.merchant_number                    as merchant_number,                    
//                      mf.dba_name                           as dba_name,
//                      mf.dmaddr                             as dba_addr1,
//                      mf.dmcity                             as dba_city,
//                      mf.dmstate                            as dba_state,
//                      mf.dmzip                              as dba_zip,
//                      mf.phone_1                            as dba_phone,
//                      --(bo.busowner_first_name || ' ' || 
//                      -- bo.busowner_last_name)               as owner_name,
//                      --bo.busowner_title                     as owner_title,
//                      --ad.address_line1                      as owner_addr1,
//                      --ad.address_city                       as owner_city,
//                      --ad.countrystate_code                  as owner_state,
//                      --ad.address_zip                        as owner_zip,
//                      --nvl(dukpt_decrypt_wrapper(bo.busowner_ssn_enc), bo.busowner_ssn) as owner_ssn,
//                      mf.federal_tax_id                     as fed_tax_id, 
//                      mf.fdr_corp_name                      as corp_name,
//                      nvl(mf.addr1_line_1,' ')              as corp_addr1,
//                      nvl(mf.addr1_line_2,' ')              as corp_addr2,
//                      nvl(mf.city1_line_4,' ')              as corp_city,
//                      nvl(mf.state1_line_4,' ')             as corp_state,
//                      decode( mf.zip1_line_4,
//                              null, ' ',
//                            (substr(mf.zip1_line_4,1,5) ||
//                             decode(substr(mf.zip1_line_4,6),
//                                    null,'',
//                                    '0000','',
//                                    '-' || 
//                                    substr(mf.zip1_line_4,6))) )    as corp_zip,
//                      mf.sic_code                           as sic_code,
//                      vmc_keyed_item_count(sm.merchant_number,sm.month_begin_date,sm.month_end_date)
//                                                            as keyed_count,
//                      (nvl(sm.visa_sales_count,0) + nvl(sm.mc_sales_count,0))
//                                                            as sales_count,                                                          
//                      least( 999,trunc(nvl(sm.vmc_avg_ticket,0)) 
//                           )                                as avg_ticket,
//                      trunc(nvl(smy.avg_monthly_sales,0)*12)as annual_sales,
//                      --decode( mr.bustype_code, 
//                      --        1,'S',
//                      --        2,'C',
//                      --        3,'P',
//                      --        4,'C',
//                      --        5,'P',
//                      --        6,'C',
//                      --        7,'C',
//                      --        8,'C',
//                      --        ' ')                          as business_type,
//                      mf.transit_routng_num                 as transit_routing_number,
//                      mf.dda_num                            as dda_number,
//                      0                                     as discover_disc_rate,
//                      trunc( months_between(sysdate,to_date(mf.date_opened,'mmddrr'))/12 )
//                                                            as years_open
//              from    mif                       mf,        
//                      organization              o,
//                      monthly_extract_summary   sm,
//                      (
//                        select  sm.merchant_number                as merchant_number,
//                                ( sum(nvl(sm.vmc_sales_amount,0))/
//                                  count(sm.merchant_number) )     as avg_monthly_sales
//                        from    t_hierarchy             th,
//                                monthly_extract_summary sm
//                        where   th.hier_type = 1 and
//                                th.ancestor = (:FileBankNumber || '00000') and
//                                th.entity_type = 4 and
//                                sm.assoc_hierarchy_node = th.descendent and
//                                sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month')
//                        group by sm.merchant_number                        
//                      )                         smy
//              where   mf.bank_number = :FileBankNumber and
//                      not nvl(mf.dmacctst,' ') in ( 'D','C','B' ) and
//                      o.org_group = (mf.bank_number || mf.group_2_association) and
//                      nvl( (
//                              select  sum(nvl(sm.disc_sales_count,0) + nvl(sm.disc_credits_count,0))
//                              from    daily_detail_file_ext_summary   sm
//                              where   sm.merchant_number = mf.merchant_number and
//                                      sm.batch_date between (sysdate-60) and sysdate
//                                 ),0 ) = 0 and
//                      nvl( (
//                              select  sum(nvl(sm.vmc_sales_count,0) + nvl(sm.vmc_credits_count,0))
//                              from    monthly_extract_summary   sm
//                              where   sm.merchant_number = mf.merchant_number and
//                                      sm.active_date between trunc((sysdate-60),'month') and sysdate
//                                 ),0 ) > 0 and
//                      sm.merchant_number(+) = mf.merchant_number and
//                      sm.active_date(+) = trunc( trunc(sysdate-3,'month')-1,'month' ) and
//                      smy.merchant_number(+) = mf.merchant_number          
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group                           as portfolio_node,\n                    o.org_name                            as portfolio_name,\n                    '01'                                  as rec_type,\n                    mf.merchant_number                    as merchant_number,                    \n                    mf.dba_name                           as dba_name,\n                    mf.dmaddr                             as dba_addr1,\n                    mf.dmcity                             as dba_city,\n                    mf.dmstate                            as dba_state,\n                    mf.dmzip                              as dba_zip,\n                    mf.phone_1                            as dba_phone,\n                    --(bo.busowner_first_name || ' ' || \n                    -- bo.busowner_last_name)               as owner_name,\n                    --bo.busowner_title                     as owner_title,\n                    --ad.address_line1                      as owner_addr1,\n                    --ad.address_city                       as owner_city,\n                    --ad.countrystate_code                  as owner_state,\n                    --ad.address_zip                        as owner_zip,\n                    --nvl(dukpt_decrypt_wrapper(bo.busowner_ssn_enc), bo.busowner_ssn) as owner_ssn,\n                    mf.federal_tax_id                     as fed_tax_id, \n                    mf.fdr_corp_name                      as corp_name,\n                    nvl(mf.addr1_line_1,' ')              as corp_addr1,\n                    nvl(mf.addr1_line_2,' ')              as corp_addr2,\n                    nvl(mf.city1_line_4,' ')              as corp_city,\n                    nvl(mf.state1_line_4,' ')             as corp_state,\n                    decode( mf.zip1_line_4,\n                            null, ' ',\n                          (substr(mf.zip1_line_4,1,5) ||\n                           decode(substr(mf.zip1_line_4,6),\n                                  null,'',\n                                  '0000','',\n                                  '-' || \n                                  substr(mf.zip1_line_4,6))) )    as corp_zip,\n                    mf.sic_code                           as sic_code,\n                    vmc_keyed_item_count(sm.merchant_number,sm.month_begin_date,sm.month_end_date)\n                                                          as keyed_count,\n                    (nvl(sm.visa_sales_count,0) + nvl(sm.mc_sales_count,0))\n                                                          as sales_count,                                                          \n                    least( 999,trunc(nvl(sm.vmc_avg_ticket,0)) \n                         )                                as avg_ticket,\n                    trunc(nvl(smy.avg_monthly_sales,0)*12)as annual_sales,\n                    --decode( mr.bustype_code, \n                    --        1,'S',\n                    --        2,'C',\n                    --        3,'P',\n                    --        4,'C',\n                    --        5,'P',\n                    --        6,'C',\n                    --        7,'C',\n                    --        8,'C',\n                    --        ' ')                          as business_type,\n                    mf.transit_routng_num                 as transit_routing_number,\n                    mf.dda_num                            as dda_number,\n                    0                                     as discover_disc_rate,\n                    trunc( months_between(sysdate,to_date(mf.date_opened,'mmddrr'))/12 )\n                                                          as years_open\n            from    mif                       mf,        \n                    organization              o,\n                    monthly_extract_summary   sm,\n                    (\n                      select  sm.merchant_number                as merchant_number,\n                              ( sum(nvl(sm.vmc_sales_amount,0))/\n                                count(sm.merchant_number) )     as avg_monthly_sales\n                      from    t_hierarchy             th,\n                              monthly_extract_summary sm\n                      where   th.hier_type = 1 and\n                              th.ancestor = ( :1  || '00000') and\n                              th.entity_type = 4 and\n                              sm.assoc_hierarchy_node = th.descendent and\n                              sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month')\n                      group by sm.merchant_number                        \n                    )                         smy\n            where   mf.bank_number =  :2  and\n                    not nvl(mf.dmacctst,' ') in ( 'D','C','B' ) and\n                    o.org_group = (mf.bank_number || mf.group_2_association) and\n                    nvl( (\n                            select  sum(nvl(sm.disc_sales_count,0) + nvl(sm.disc_credits_count,0))\n                            from    daily_detail_file_ext_summary   sm\n                            where   sm.merchant_number = mf.merchant_number and\n                                    sm.batch_date between (sysdate-60) and sysdate\n                               ),0 ) = 0 and\n                    nvl( (\n                            select  sum(nvl(sm.vmc_sales_count,0) + nvl(sm.vmc_credits_count,0))\n                            from    monthly_extract_summary   sm\n                            where   sm.merchant_number = mf.merchant_number and\n                                    sm.active_date between trunc((sysdate-60),'month') and sysdate\n                               ),0 ) > 0 and\n                    sm.merchant_number(+) = mf.merchant_number and\n                    sm.active_date(+) = trunc( trunc(sysdate-3,'month')-1,'month' ) and\n                    smy.merchant_number(+) = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.DiscoverEntitlement",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setInt(2,FileBankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.DiscoverEntitlement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:198^11*/
          break;
          
        case mesConstants.BANK_ID_CBT:
          /*@lineinfo:generated-code*//*@lineinfo:202^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group                           as portfolio_node,
//                      o.org_name                            as portfolio_name,
//                      '01'                                  as rec_type,
//                      mf.dba_name                           as dba_name,
//                      mf.dmaddr                             as dba_addr1,
//                      mf.dmcity                             as dba_city,
//                      mf.dmstate                            as dba_state,
//                      mf.dmzip                              as dba_zip,
//                      mf.phone_1                            as dba_phone,
//                      (bo.busowner_first_name || ' ' || 
//                       bo.busowner_last_name)               as owner_name,
//                      bo.busowner_title                     as owner_title,
//                      ad.address_line1                      as owner_addr1,
//                      ad.address_city                       as owner_city,
//                      ad.countrystate_code                  as owner_state,
//                      ad.address_zip                        as owner_zip,
//                      mf.federal_tax_id                     as fed_tax_id,
//                      mf.fdr_corp_name                      as corp_name,
//                      adc.address_line1                     as corp_addr1,
//                      adc.address_line2                     as corp_addr2,        
//                      adc.address_city                      as corp_city,
//                      adc.countrystate_code                 as corp_state,
//                      adc.address_zip                       as corp_zip,
//                      mf.sic_code                           as sic_code,
//                      decode( nvl(mr.merch_mail_phone_sales,0),
//                              0, 'N',
//                              decode( ((mr.merch_mail_phone_sales - 50)/abs(mr.merch_mail_phone_sales)),
//                                      1, 'Y',
//                                      'N'
//                                    )
//                            )                               as moto_account,
//                      least( 999,
//                             trunc(nvl(sm.vmc_avg_ticket,
//                                       nvl(mr.merch_average_cc_tran,0)))
//                           )                                as avg_ticket,
//                      trunc(nvl(mr.merch_month_visa_mc_sales,0)*12)
//                                                            as annual_sales,
//                      decode( mr.bustype_code, 
//                              1,'S',
//                              2,'C',
//                              3,'P',
//                              4,'C',
//                              5,'P',
//                              6,'C',
//                              7,'C',
//                              8,'C',
//                              ' ')                          as business_type,
//                      nvl(dukpt_decrypt_wrapper(bo.busowner_ssn_enc), bo.busowner_ssn)
//                                                            as owner_ssn,
//                      mf.transit_routng_num                 as transit_routing_number,
//                      mf.dda_num                            as dda_number,
//                      0                                     as discover_disc_rate,
//                      least(99,nvl(mr.merch_years_in_business,0))  
//                                                            as years_open,
//                      mf.merchant_number                    as merchant_number
//              from    t_hierarchy               th,
//                      mif                       mf,
//                      merchant                  mr,
//                      merchpayoption            mpo,
//                      businessowner             bo,
//                      address                   ad,
//                      address                   adc,        
//                      organization              o,
//                      monthly_extract_summary   sm
//              where   th.hier_type = 1 and
//                      --th.ancestor = 3858979999 and    -- agent banks
//                      th.ancestor = 3858949999 and      -- locals
//                      th.entity_type = 4 and
//                      mf.association_node = th.descendent and
//                      not nvl(mf.dmacctst,' ') in ( 'D','C','B' ) and
//                      not exists    -- omit 3858907661 accounts
//                      (
//                        select  thi.ancestor
//                        from    t_hierarchy   thi
//                        where   thi.hier_type = 1 and
//                                thi.ancestor = 3858907661 and
//                                thi.entity_type = 4 and
//                                thi.descendent = mf.association_node
//                      ) and
//                      --mr.merch_number(+) = mf.merchant_number and -- OLA optional
//                      mr.merch_number = mf.merchant_number and -- OLA required
//                      mpo.app_seq_num(+) = mr.app_seq_num and
//                      mpo.cardtype_code(+) = 14 and
//                      nvl(mpo.merchpo_card_merch_number,0) = 0 and
//                      bo.app_seq_num(+) = mr.app_seq_num and
//                      bo.busowner_num(+) = 1 and          -- primary owner
//                      ad.app_seq_num(+) = mr.app_seq_num and
//                      ad.addresstype_code(+) = 4 and      -- primary owner address
//                      adc.app_seq_num(+) = mr.app_seq_num and
//                      adc.addresstype_code(+) = 2 and     -- mailing address        
//                      o.org_group = (mf.bank_number || mf.group_2_association) and
//                      not o.org_group in ( 3941400059 ) and         -- omit some groups (i.e. sabre)
//                      sm.merchant_number(+) = mf.merchant_number and
//                      sm.active_date(+) = trunc( trunc(sysdate-3,'month')-1,'month' )      
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group                           as portfolio_node,\n                    o.org_name                            as portfolio_name,\n                    '01'                                  as rec_type,\n                    mf.dba_name                           as dba_name,\n                    mf.dmaddr                             as dba_addr1,\n                    mf.dmcity                             as dba_city,\n                    mf.dmstate                            as dba_state,\n                    mf.dmzip                              as dba_zip,\n                    mf.phone_1                            as dba_phone,\n                    (bo.busowner_first_name || ' ' || \n                     bo.busowner_last_name)               as owner_name,\n                    bo.busowner_title                     as owner_title,\n                    ad.address_line1                      as owner_addr1,\n                    ad.address_city                       as owner_city,\n                    ad.countrystate_code                  as owner_state,\n                    ad.address_zip                        as owner_zip,\n                    mf.federal_tax_id                     as fed_tax_id,\n                    mf.fdr_corp_name                      as corp_name,\n                    adc.address_line1                     as corp_addr1,\n                    adc.address_line2                     as corp_addr2,        \n                    adc.address_city                      as corp_city,\n                    adc.countrystate_code                 as corp_state,\n                    adc.address_zip                       as corp_zip,\n                    mf.sic_code                           as sic_code,\n                    decode( nvl(mr.merch_mail_phone_sales,0),\n                            0, 'N',\n                            decode( ((mr.merch_mail_phone_sales - 50)/abs(mr.merch_mail_phone_sales)),\n                                    1, 'Y',\n                                    'N'\n                                  )\n                          )                               as moto_account,\n                    least( 999,\n                           trunc(nvl(sm.vmc_avg_ticket,\n                                     nvl(mr.merch_average_cc_tran,0)))\n                         )                                as avg_ticket,\n                    trunc(nvl(mr.merch_month_visa_mc_sales,0)*12)\n                                                          as annual_sales,\n                    decode( mr.bustype_code, \n                            1,'S',\n                            2,'C',\n                            3,'P',\n                            4,'C',\n                            5,'P',\n                            6,'C',\n                            7,'C',\n                            8,'C',\n                            ' ')                          as business_type,\n                    nvl(dukpt_decrypt_wrapper(bo.busowner_ssn_enc), bo.busowner_ssn)\n                                                          as owner_ssn,\n                    mf.transit_routng_num                 as transit_routing_number,\n                    mf.dda_num                            as dda_number,\n                    0                                     as discover_disc_rate,\n                    least(99,nvl(mr.merch_years_in_business,0))  \n                                                          as years_open,\n                    mf.merchant_number                    as merchant_number\n            from    t_hierarchy               th,\n                    mif                       mf,\n                    merchant                  mr,\n                    merchpayoption            mpo,\n                    businessowner             bo,\n                    address                   ad,\n                    address                   adc,        \n                    organization              o,\n                    monthly_extract_summary   sm\n            where   th.hier_type = 1 and\n                    --th.ancestor = 3858979999 and    -- agent banks\n                    th.ancestor = 3858949999 and      -- locals\n                    th.entity_type = 4 and\n                    mf.association_node = th.descendent and\n                    not nvl(mf.dmacctst,' ') in ( 'D','C','B' ) and\n                    not exists    -- omit 3858907661 accounts\n                    (\n                      select  thi.ancestor\n                      from    t_hierarchy   thi\n                      where   thi.hier_type = 1 and\n                              thi.ancestor = 3858907661 and\n                              thi.entity_type = 4 and\n                              thi.descendent = mf.association_node\n                    ) and\n                    --mr.merch_number(+) = mf.merchant_number and -- OLA optional\n                    mr.merch_number = mf.merchant_number and -- OLA required\n                    mpo.app_seq_num(+) = mr.app_seq_num and\n                    mpo.cardtype_code(+) = 14 and\n                    nvl(mpo.merchpo_card_merch_number,0) = 0 and\n                    bo.app_seq_num(+) = mr.app_seq_num and\n                    bo.busowner_num(+) = 1 and          -- primary owner\n                    ad.app_seq_num(+) = mr.app_seq_num and\n                    ad.addresstype_code(+) = 4 and      -- primary owner address\n                    adc.app_seq_num(+) = mr.app_seq_num and\n                    adc.addresstype_code(+) = 2 and     -- mailing address        \n                    o.org_group = (mf.bank_number || mf.group_2_association) and\n                    not o.org_group in ( 3941400059 ) and         -- omit some groups (i.e. sabre)\n                    sm.merchant_number(+) = mf.merchant_number and\n                    sm.active_date(+) = trunc( trunc(sysdate-3,'month')-1,'month' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.DiscoverEntitlement",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.DiscoverEntitlement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:298^11*/
          break;
          
        default:
          /*@lineinfo:generated-code*//*@lineinfo:302^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group                           as portfolio_node,
//                      o.org_name                            as portfolio_name,
//                      '01'                                  as rec_type,
//                      mf.dba_name                           as dba_name,
//                      mf.dmaddr                             as dba_addr1,
//                      mf.dmcity                             as dba_city,
//                      mf.dmstate                            as dba_state,
//                      mf.dmzip                              as dba_zip,
//                      mf.phone_1                            as dba_phone,
//                      (bo.busowner_first_name || ' ' || 
//                       bo.busowner_last_name)               as owner_name,
//                      bo.busowner_title                     as owner_title,
//                      ad.address_line1                      as owner_addr1,
//                      ad.address_city                       as owner_city,
//                      ad.countrystate_code                  as owner_state,
//                      ad.address_zip                        as owner_zip,
//                      mf.federal_tax_id                     as fed_tax_id,                   
//                      mf.fdr_corp_name                      as corp_name,
//                      adc.address_line1                     as corp_addr1,
//                      adc.address_line2                     as corp_addr2,        
//                      adc.address_city                      as corp_city,
//                      adc.countrystate_code                 as corp_state,
//                      adc.address_zip                       as corp_zip,
//                      mf.sic_code                           as sic_code,
//                      decode( nvl(mr.merch_mail_phone_sales,0),
//                              0, 'N',
//                              decode( ((mr.merch_mail_phone_sales - 50)/abs(mr.merch_mail_phone_sales)),
//                                      1, 'Y',
//                                      'N'
//                                    )
//                            )                               as moto_account,
//                      least( 999,
//                             trunc(nvl(sm.vmc_avg_ticket,
//                                       nvl(mr.merch_average_cc_tran,0)))
//                           )                                as avg_ticket,
//                      trunc(nvl(mr.merch_month_visa_mc_sales,0)*12)
//                                                            as annual_sales,
//                      decode( mr.bustype_code, 
//                              1,'S',
//                              2,'C',
//                              3,'P',
//                              4,'C',
//                              5,'P',
//                              6,'C',
//                              7,'C',
//                              8,'C',
//                              ' ')                          as business_type,
//                      nvl(dukpt_decrypt_wrapper(bo.busowner_ssn_enc), bo.busowner_ssn)
//                                                            as owner_ssn,
//                      mf.transit_routng_num                 as transit_routing_number,
//                      mf.dda_num                            as dda_number,
//                      0                                     as discover_disc_rate,
//                      least(99,mr.merch_years_in_business)  as years_open,
//                      mf.merchant_number                    as merchant_number
//              from    t_hierarchy               th,
//                      mif                       mf,
//                      merchant                  mr,
//                      merchpayoption            mpo,
//                      businessowner             bo,
//                      address                   ad,
//                      address                   adc,        
//                      organization              o,
//                      monthly_extract_summary   sm
//              where   th.hier_type = 1 and
//                      th.ancestor in ( 3941500001, 3941500003 ) and
//                      th.entity_type = 4 and
//                      mf.association_node = th.descendent and
//                      not nvl(mf.dmacctst,' ') in ( 'D','C','B' ) and
//                      not exists    -- omit sales rep accounts
//                      (
//                        select  sr.rep_merchant_number
//                        from    sales_rep   sr
//                        where   sr.rep_merchant_number = mf.merchant_number
//                      ) and
//                      mr.merch_number = mf.merchant_number and
//                      mpo.app_seq_num(+) = mr.app_seq_num and
//                      mpo.cardtype_code(+) = 14 and
//                      nvl(mpo.merchpo_card_merch_number,0) = 0 and
//                      bo.app_seq_num(+) = mr.app_seq_num and
//                      bo.busowner_num(+) = 1 and          -- primary owner
//                      ad.app_seq_num(+) = mr.app_seq_num and
//                      ad.addresstype_code(+) = 4 and      -- primary owner address
//                      adc.app_seq_num(+) = mr.app_seq_num and
//                      adc.addresstype_code(+) = 2 and     -- mailing address        
//                      o.org_group = (mf.bank_number || mf.group_2_association) and
//                      not o.org_group in ( 3941400059 ) and         -- omit some groups (i.e. sabre)
//                      sm.merchant_number(+) = mf.merchant_number and
//                      sm.active_date(+) = trunc( trunc(sysdate-3,'month')-1,'month' )      
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group                           as portfolio_node,\n                    o.org_name                            as portfolio_name,\n                    '01'                                  as rec_type,\n                    mf.dba_name                           as dba_name,\n                    mf.dmaddr                             as dba_addr1,\n                    mf.dmcity                             as dba_city,\n                    mf.dmstate                            as dba_state,\n                    mf.dmzip                              as dba_zip,\n                    mf.phone_1                            as dba_phone,\n                    (bo.busowner_first_name || ' ' || \n                     bo.busowner_last_name)               as owner_name,\n                    bo.busowner_title                     as owner_title,\n                    ad.address_line1                      as owner_addr1,\n                    ad.address_city                       as owner_city,\n                    ad.countrystate_code                  as owner_state,\n                    ad.address_zip                        as owner_zip,\n                    mf.federal_tax_id                     as fed_tax_id,                   \n                    mf.fdr_corp_name                      as corp_name,\n                    adc.address_line1                     as corp_addr1,\n                    adc.address_line2                     as corp_addr2,        \n                    adc.address_city                      as corp_city,\n                    adc.countrystate_code                 as corp_state,\n                    adc.address_zip                       as corp_zip,\n                    mf.sic_code                           as sic_code,\n                    decode( nvl(mr.merch_mail_phone_sales,0),\n                            0, 'N',\n                            decode( ((mr.merch_mail_phone_sales - 50)/abs(mr.merch_mail_phone_sales)),\n                                    1, 'Y',\n                                    'N'\n                                  )\n                          )                               as moto_account,\n                    least( 999,\n                           trunc(nvl(sm.vmc_avg_ticket,\n                                     nvl(mr.merch_average_cc_tran,0)))\n                         )                                as avg_ticket,\n                    trunc(nvl(mr.merch_month_visa_mc_sales,0)*12)\n                                                          as annual_sales,\n                    decode( mr.bustype_code, \n                            1,'S',\n                            2,'C',\n                            3,'P',\n                            4,'C',\n                            5,'P',\n                            6,'C',\n                            7,'C',\n                            8,'C',\n                            ' ')                          as business_type,\n                    nvl(dukpt_decrypt_wrapper(bo.busowner_ssn_enc), bo.busowner_ssn)\n                                                          as owner_ssn,\n                    mf.transit_routng_num                 as transit_routing_number,\n                    mf.dda_num                            as dda_number,\n                    0                                     as discover_disc_rate,\n                    least(99,mr.merch_years_in_business)  as years_open,\n                    mf.merchant_number                    as merchant_number\n            from    t_hierarchy               th,\n                    mif                       mf,\n                    merchant                  mr,\n                    merchpayoption            mpo,\n                    businessowner             bo,\n                    address                   ad,\n                    address                   adc,        \n                    organization              o,\n                    monthly_extract_summary   sm\n            where   th.hier_type = 1 and\n                    th.ancestor in ( 3941500001, 3941500003 ) and\n                    th.entity_type = 4 and\n                    mf.association_node = th.descendent and\n                    not nvl(mf.dmacctst,' ') in ( 'D','C','B' ) and\n                    not exists    -- omit sales rep accounts\n                    (\n                      select  sr.rep_merchant_number\n                      from    sales_rep   sr\n                      where   sr.rep_merchant_number = mf.merchant_number\n                    ) and\n                    mr.merch_number = mf.merchant_number and\n                    mpo.app_seq_num(+) = mr.app_seq_num and\n                    mpo.cardtype_code(+) = 14 and\n                    nvl(mpo.merchpo_card_merch_number,0) = 0 and\n                    bo.app_seq_num(+) = mr.app_seq_num and\n                    bo.busowner_num(+) = 1 and          -- primary owner\n                    ad.app_seq_num(+) = mr.app_seq_num and\n                    ad.addresstype_code(+) = 4 and      -- primary owner address\n                    adc.app_seq_num(+) = mr.app_seq_num and\n                    adc.addresstype_code(+) = 2 and     -- mailing address        \n                    o.org_group = (mf.bank_number || mf.group_2_association) and\n                    not o.org_group in ( 3941400059 ) and         -- omit some groups (i.e. sabre)\n                    sm.merchant_number(+) = mf.merchant_number and\n                    sm.active_date(+) = trunc( trunc(sysdate-3,'month')-1,'month' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.DiscoverEntitlement",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.DiscoverEntitlement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:392^11*/
          break;
      }
      rs = it.getResultSet();
      
      // setup the base control number
      controlNum.append("R");
      controlNum.append("    ");    //@ need real partner code
      controlNum.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"yyMMdd"));
      controlNumLen = controlNum.length();
      
      while(rs.next())
      {
        ++FileRecordCount;
        ++seqNum;
        
        controlNum.setLength(controlNumLen);
        controlNum.append(NumberFormatter.getPaddedInt(seqNum,4));
        
        ffd.setAllFieldData(rs);
        ffd.setFieldData("control_number",controlNum.toString());
        
        switch( FileBankNumber )
        {
          case mesConstants.BANK_ID_UBOC:
          case mesConstants.BANK_ID_NBSC:
          case mesConstants.BANK_ID_CBT:
          case mesConstants.BANK_ID_BBT:
            try
            { 
              ratio = rs.getDouble("keyed_count")/
                      rs.getDouble("sales_count"); 
            }
            catch(Exception e)
            {
              ratio = 0.0;
            }
            if ( ratio > 0.50 )
            {
              ffd.setFieldData("moto_account","Y");
            }
            else
            {
              ffd.setFieldData("moto_account","N");
            }
            break;
            
          default:
            break;
        }
        
        out.write( ffd.spew() );
        out.newLine();
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("buildDetails()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      try { ffd.cleanUp(); } catch(Exception ee) {}
    }
  }
  
  public void buildFooter( BufferedWriter out )
  {
    FlatFileRecord      ffd     = null;
      
    try
    {
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_DISCOVER_FTR);
      
      // set record count and total
      ffd.setFieldData("rec_count", FileRecordCount);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildFooter()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    String              dateString  = null;
    BufferedWriter      out         = null;
    StringBuffer        tfilename   = null;
    
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      
      // setup a temp filename
      // DISC-ENT-MES-yyyymmdd.dat
      dateString = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"yyyyMMdd");
      tfilename = new StringBuffer();
      tfilename.append("disc_ent");
      tfilename.append(FileBankNumber);
      tfilename.append("_");
      tfilename.append(dateString);
      tfilename.append("_");
      tfilename.append( NumberFormatter.getPaddedInt( getFileId(dateString), 3 ) );
      
      // open the data file
      out = new BufferedWriter( new FileWriter( tfilename.toString() + ".dat", true ) );
      
      if (Verbose)
      { 
        log.debug("Created file '" + tfilename + ".dat'");
      }        
      
      // build the file using the FlatFileDef
      FileRecordCount = 0;
      buildHeader(out);
      buildDetails(out);
      buildFooter(out);
      out.close();
      
      // ftp the file to the outgoing directory
//@      sendFile(tfilename.toString());
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }      
    finally
    {
      cleanUp();
    }
    return( true );
  }
  
  public synchronized int getFileId( String dateString )
  {
    int               fileId    = 0;
    PropertiesFile    propsFile = null;
    
    try
    {
      propsFile = new PropertiesFile("disc_ent.properties");
    }
    catch(Exception e)
    {
    }
    
    if ( dateString.equals( propsFile.getString("file.lastdate") ) )
    {
      fileId = Integer.parseInt( propsFile.getString("file.lastcount"));
    }
    fileId++;
    
    propsFile.setProperty("file.lastdate",dateString);
    propsFile.setProperty("file.lastcount",Integer.toString(fileId));
    propsFile.store("disc_ent.properties");
    
    return( fileId );
  }
  
  protected void sendFile( String baseFilename )
  {
    String        dataFilename    = baseFilename + ".txt";
    String        flagFilename    = baseFilename + ".flg";
    String        progress        = "";
    boolean       success         = false;
    StringBuffer  status          = new StringBuffer();
    
    try
    {    
      connect();
      
      try
      {
        // log in to the proper host
        progress = "instantiating FTP";
        FTPClient ftp = new FTPClient(MesDefaults.getString(MesDefaults.DK_SABRE_HOST), 21);
      
        // set timeout
        ftp.setTimeout(5000);   // 5 seconds
  
        progress = "logging in to ftp server";
        ftp.login( MesDefaults.getString(MesDefaults.DK_SABRE_USER),
                   MesDefaults.getString(MesDefaults.DK_SABRE_PASSWORD));
      
        progress = "setting command mode to ACTIVE";
        ftp.setConnectMode(FTPConnectMode.ACTIVE);
      
        progress = "setting transfer type to BINARY";
        ftp.setType(FTPTransferType.BINARY);
      
        progress = "changing directory";
        ftp.chdir(MesDefaults.getString(MesDefaults.DK_SABRE_OUTGOING_PATH));
      
        // sending file
        progress = "transmitting file";
        ftp.put( dataFilename , dataFilename );
        
        // create a flag file
        BufferedWriter out = new BufferedWriter( new FileWriter( flagFilename, false) );
        out.write( dataFilename );
        out.close();
        
        // send the flag file
        ftp.put( flagFilename , flagFilename );
        
        // close
        progress = "closing connection";
        ftp.quit();
      
        progress = "archiving files";
        ftp = new FTPClient(MesDefaults.getString(MesDefaults.DISC_ENT_ARCHIVE_HOST), 21);
        
        ftp.login(MesDefaults.getString(MesDefaults.DISC_ENT_ARCHIVE_USER), MesDefaults.getString(MesDefaults.DISC_ENT_ARCHIVE_PASSWORD));
        ftp.setType(FTPTransferType.ASCII);
        ftp.chdir(MesDefaults.getString(MesDefaults.DISC_ENT_ARCHIVE_PATH));
        
        ftp.put( dataFilename, dataFilename);
        ftp.quit();
        
        new File(dataFilename).delete();
        new File(flagFilename).delete();
        
        status.append("[");
        status.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
        status.append("] Successfully processed Sabre merchant profile updates as ");
        status.append(dataFilename);
        
        success = true;
      }
      catch(Exception ftpe)
      {
        logEntry("sendFile(" + baseFilename + ")", progress + ": " + ftpe.toString());
        status.append(ftpe.toString());
      }
      
      // send a status email
      sendStatusEmail(success, dataFilename, status.toString() );
    }      
    catch( Exception e )
    {
      logEntry("sendFile(" + baseFilename + ")", e.toString());
    }
    finally
    {
    }
  }    
  
  private void sendStatusEmail(boolean success, String fileName, String message)
  {
    StringBuffer  subject         = new StringBuffer("");
    StringBuffer  body            = new StringBuffer("");
    
    try
    {
      if(success)
      {
        subject.append("Sabre Success");
      }
      else
      {
        subject.append("Sabre Error");
        
        // make sure the filename is in 
        // the message text for error research
        body.append("Filename: ");
        body.append(fileName);
        body.append("\n\n");
      }
      body.append(message);
      
      MailMessage msg = new MailMessage();
      
      if(success)
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_SABRE_NOTIFY);
      }
      else
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_SABRE_FAILURE);
      }
      
      msg.setSubject(subject.toString());
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "sendStatusEmail()", e.toString());
      logEntry("sendStatusEmail()", e.toString());
    }
  }
  
  public void setBankNumber( String bankNum )
  {
    try
    {
      FileBankNumber = Integer.parseInt(bankNum);
    }      
    catch(Exception e)
    {
    }
  }
  
  public static void main( String[] args )
  {
    DiscoverEntitlement        test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      test = new DiscoverEntitlement();
      test.Verbose = true;
      test.setBankNumber(args[0]);
      test.execute();
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/