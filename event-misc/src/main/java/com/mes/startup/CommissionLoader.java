/*@lineinfo:filename=CommissionLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/CommissionLoader.sqlj $

  Description:  
    Summarization methods for merchant profitability.


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;


public class CommissionLoader extends ExtractLoader
{
  static Logger log = Logger.getLogger(CommissionLoader.class);
  
  public static final int PT_ACTIVATION                     = 0;
  public static final int PT_EXTRACT                        = 1;
  
  public static final int BANK_CONTRACT_TYPE_LIABILITY      = 1;
  public static final int BANK_CONTRACT_TYPE_REFERRAL       = 2;
  public static final int BANK_CONTRACT_TYPE_RESELLER       = 3;
  public static final int BANK_CONTRACT_TYPE_THIRD_PARTY    = 4;

  public static final int REP_GROUP_ECOMMERCE_TELESALES     = 1;
  public static final int REP_GROUP_AGENT_BANK              = 2;
  public static final int REP_GROUP_REFERRAL_TELESALES      = 3;
  public static final int REP_GROUP_ECOMMERCE_MANAGER       = 4;
  public static final int REP_GROUP_ECOMMERCE_FIELDSALES    = 5;
  public static final int REP_GROUP_MERCHANT_SERVICES       = 6;
  public static final int REP_GROUP_CBT_SALES_REP           = 7;
  public static final int REP_GROUP_CBT_SALES_MANAGER       = 8;

  public static final int TYPE_MONTHLY_COMMISSION           = 27;
  public static final int TYPE_MONTHLY_BONUS_COMMISSION     = 28;
  public static final int TYPE_ACCOUNT_SETUP_COMMISSION     = 29;
  public static final int TYPE_YEAR_END_BONUS_COMMISSION    = 30;
  public static final int TYPE_OVERRIDE_COMMISSION          = 31;

  public static final int NEW_ACCOUNT_SETUP_FEE             = 12;
  public static final int NEW_ACCOUNT_REFERRAL_FEE          = 70;

  private int         GroupId                    = -1;
  private Vector      GroupIds                   = new Vector();
  private long        RepId                      = -1L;
  private long        MerchantNumber             = -1L;
  private Date        ActiveDate                 = null;
  private String      LoadFilename               = "passall";
  private long        TestSequenceId             = 0L;
  private boolean     TestMode                   = false;  
  private Vector      DataRows                   = new Vector();
  
  private class CommissionData
  {
    public long      MerchantNumber       = 0L;
    public String    DbaName              = null;
    public long      RepId                = 0L;
    public int       GroupId              = 0;
    public double    PayRate              = 0.0;
    public double    Income               = 0.0;
    public double    Expense              = 0.0;
    public double    CommisionAmount      = 0.0;
    public Date      CommissionDate       = null;
    public int       VolumCount           = 0;
    public double    VolumAmount          = 0.0;
    public double    NetRevenue           = 0.0;
    public double    ReferralFees         = 0.0;
    public String    LoadFilename         = null;
    public int       CommissionType       = 0;
    public long      HierarchyNode        = 0L;
    public double    PreContractRevenue   = 0.0;

    public CommissionData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      MerchantNumber      = resultSet.getLong("merchant_number");
      DbaName             = resultSet.getString("dba_name");
      RepId               = resultSet.getLong("rep_id");
      GroupId             = resultSet.getInt("group_id");
      PayRate             = resultSet.getDouble("pay_rate");
      CommissionDate      = resultSet.getDate("commission_date");   
      LoadFilename        = resultSet.getString("load_filename");
      CommissionType      = resultSet.getInt("commission_type");
      
      Income    = getMerchantIncome(resultSet) + getAdjustmentFees(resultSet);
      Expense   = getIcExpense(resultSet);
      Expense  += getVmcAssessment(resultSet);
      Expense  += getReferralFees(resultSet);
      Expense  += getEquipSalesExpense(resultSet);
      Expense  += getEquipRentalExpense(resultSet);
      Expense  += getAuthFees(resultSet);
      Expense  += getCaptureFees(resultSet);
      Expense  += getDebitFees(resultSet);
      Expense  += getPlanFees(resultSet);
      Expense  += getSystemFees(resultSet);
      PreContractRevenue = Income - Expense;      
      if( processString(resultSet.getString("liability_contract")).equals("Y") )
      {
        Income   = getLiabilityContractIncome(resultSet);
        Expense  = getEquipSalesExpense(resultSet);
      }
      NetRevenue      = Income - Expense;
      CommisionAmount = NetRevenue * PayRate * 0.01;
    }
    
    public CommissionData( long merchantNumber, long repId, double payRate, String loadFilename,
                           Date commissionDate, int commissionType, double income, double expense, 
                           double netRevenue, double preContractRevenue, double commisionAmount )
    {
      MerchantNumber     = merchantNumber;
      RepId              = repId;
      PayRate            = payRate;
      LoadFilename       = loadFilename;
      CommissionDate     = commissionDate;
      CommissionType     = commissionType;
      Income             = income;
      Expense            = expense;
      NetRevenue         = netRevenue;
      CommisionAmount    = commisionAmount;
      PreContractRevenue = preContractRevenue;
    }
    
    public CommissionData( long merchantNumber, long repId, double payRate, String loadFilename,
                           Date commissionDate, int commissionType, double income, double expense, 
                           double netRevenue, double commisionAmount )
    {
      this( merchantNumber, repId, payRate, loadFilename,
            commissionDate, commissionType, income, expense, 
            netRevenue, 0.0, commisionAmount );
    }
   
    protected double getAuthFees( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("exp_auth") );
    }

    protected double getCaptureFees( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("exp_capture") );
    }
    
    protected double getDebitFees( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("exp_debit") );
    }
  
    protected double getReferralFees( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("auth_referral") +
              resultSet.getDouble("capture_referral") +
              resultSet.getDouble("debit_referral") +
              resultSet.getDouble("equip_rental_referral") +
              resultSet.getDouble("equip_sales_referral") +
              resultSet.getDouble("ind_plans_referral") +
              resultSet.getDouble("ndr_referral") +
              resultSet.getDouble("sys_gen_referral") );
    }
    
    protected double getMerchantIncome( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("merchant_income") );
    }

    protected double getAdjustmentFees( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("disc_ic_adj") + 
              resultSet.getDouble("fee_adj") );
    }
    
    protected double getEquipmentIncome( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("equip_rental_inc") +
              resultSet.getDouble("equip_sales_inc") +
              resultSet.getDouble("equip_sales_tax") );
    }
  
    protected double getIcExpense( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("ic_exp") );
    }
    
    protected double getGlobalSupportFee( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("mc_intl_expense") );
    }

    protected double getEquipRentalExpense( ResultSet resultSet )
      throws java.sql.SQLException
    {
      double retVal = 0.0;
      
      if ( resultSet.getDouble("partnership") == 0.0 )
      {
        retVal = resultSet.getDouble("equip_rental_exp");
      }
      return retVal;
    }
    
    protected double getEquipSalesExpense( ResultSet resultSet )
      throws java.sql.SQLException
    {
      double retVal = 0.0;
      
      if ( resultSet.getDouble("partnership") == 0.0 )
      {
        retVal = resultSet.getDouble("equip_sales_exp");
      }
      return retVal;
    }
    
    protected double getPlanFees( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("exp_ind_plans") );
    }
    
    protected double getSystemFees( ResultSet resultSet )
    throws java.sql.SQLException
    {
      return( resultSet.getDouble("sys_gen_exp") );
    }

    protected double getVmcAssessment( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("vmc_assessment_expense") );
    }
    
    protected double getLiabilityContractIncome( ResultSet resultSet )
      throws java.sql.SQLException
    {
      double income = 0.0;
      
      if( processString(resultSet.getString("cash_advance_referral")).equals("Y") )
      {
        income  = resultSet.getDouble("disc_ic_inc") +
                  resultSet.getDouble("capture_inc") + 
                  resultSet.getDouble("debit_inc") +
                  resultSet.getDouble("ind_plans_inc") +
                  resultSet.getDouble("auth_inc") +
                  resultSet.getDouble("sys_gen_inc") +
                  resultSet.getDouble("equip_rental_inc") + 
                  resultSet.getDouble("equip_sales_inc") +
                  resultSet.getDouble("equip_sales_tax") +
                  resultSet.getDouble("disc_ic_adj") +
                  resultSet.getDouble("fee_adj");
      }

      income   += resultSet.getDouble("ndr_liability") +          // liability contract
                  resultSet.getDouble("capture_liability") +
                  resultSet.getDouble("debit_liability") +
                  resultSet.getDouble("ind_plans_liability") +
                  resultSet.getDouble("auth_liability") +
                  resultSet.getDouble("sys_gen_liability") +
                  resultSet.getDouble("equip_rental_liability") +
                  resultSet.getDouble("equip_sales_liability") +
                  resultSet.getDouble("mes_only_disc_ic") +       // mes only fees
                  resultSet.getDouble("mes_only_capture") +
                  resultSet.getDouble("mes_only_debit") +
                  resultSet.getDouble("mes_only_plan") +
                  resultSet.getDouble("mes_only_auth") +
                  resultSet.getDouble("mes_only_sys_gen") +
                  resultSet.getDouble("excluded_fees")+           // excluded fees
                  resultSet.getDouble("partnership");             // partntership fees
      
      if ( resultSet.getDouble("partnership") == 0.0 )
      {
        // if this merchant uses MES owned inventory 
        // then the base cost is rental/sales base cost        
        if( processString(resultSet.getString("mes_inventory")).equals("Y") )
        {
          // add values for income/expense report
          income  += resultSet.getDouble("equip_rental_base_cost") +
                     resultSet.getDouble("equip_sales_base_cost");
        }
        else  // bank owns equipment, only cost is transfers made to the bank
        {
          // add values for income
          income += resultSet.getDouble("equip_transfer_cost");
        }              
      }
      
      return income;      
    }
  }
  
  public static class MerchantData
  {
    public Date    ActivationDate      = null;
    public long    AppSeqNumber        = 0L;
    public int     CommType            = -1;
    public String  DbaName             = null;
    public Date    DateOpened          = null;
    public Date    FirstCommDate       = null;
    public Date    LastCommDate        = null;
    public long    MerchantNumber      = 0L;
    public int     RepGroupId          = -1;
    public long    SalesRepId          = 0L;
    
    public MerchantData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ActivationDate        = resultSet.getDate("activation_date");
      AppSeqNumber          = resultSet.getLong("app_seq_num");
      CommType              = resultSet.getInt("comm_type");
      DbaName               = resultSet.getString("dba_name");
      DateOpened            = resultSet.getDate("date_opened");
      FirstCommDate         = resultSet.getDate("first_comm_date");
      LastCommDate          = resultSet.getDate("last_comm_date");
      MerchantNumber        = resultSet.getLong("merchant_number");
      SalesRepId            = resultSet.getLong("rep_id");
      RepGroupId            = resultSet.getInt("group_id");
    }
    
    public MerchantData( int groupId, long repId, long merchantNumber, String dbaName, long appSeqNumber,
                         Date dateOpened, Date dateActivated, Date commBegin, Date commEnd, int commType )
    {
      RepGroupId       = groupId;
      SalesRepId       = repId;
      MerchantNumber   = merchantNumber;
      DbaName          = dbaName;
      AppSeqNumber     = appSeqNumber;
      DateOpened       = dateOpened;
      ActivationDate   = dateActivated;
      FirstCommDate    = commBegin;
      LastCommDate     = commEnd;
      CommType         = commType;
    }
  }

  public CommissionLoader()
  {
  }
    
  public boolean inTestMode()
  {
    return( TestMode );
  }
    
  public boolean isTranscomAccount( long merchantId )
  {
    int  retVal   = 0;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:383^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(mf.merchant_number)  
//          from    mif   mf
//          where   mf.group_1_association = '500004'
//                  and mf.merchant_number =:merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mf.merchant_number)   \n        from    mif   mf\n        where   mf.group_1_association = '500004'\n                and mf.merchant_number = :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.CommissionLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:389^7*/
    }
    catch(java.sql.SQLException e)
    {
    }      

    return( retVal > 0 );
  }
  
  public int getGroupId()
  {
    return GroupId;
  }
  
  public int getGroupId( long repId )
  {
    int  retVal   = -1;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:409^7*/

//  ************************************************************
//  #sql [Ctx] { select   group_id 
//          from     commission_sales_rep
//          where    user_id = :repId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select   group_id  \n        from     commission_sales_rep\n        where    user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.CommissionLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,repId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:414^7*/
    }
    catch(java.sql.SQLException e)
    {
    }      

    return retVal;
  }
  
  protected void recordTimestampBegin( long recId )
  {
    Timestamp     beginTime   = null;
    
    if( !inTestMode() )
    {
      try
      {
        beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
        /*@lineinfo:generated-code*//*@lineinfo:433^9*/

//  ************************************************************
//  #sql [Ctx] { update  commission_process
//            set     process_begin_date = :beginTime
//            where   rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  commission_process\n          set     process_begin_date =  :1 \n          where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:438^9*/
      }
      catch(Exception e)
      {
        logEntry("recordTimestampBegin()", e.toString());
      }      
    }       
  }
  
  protected void recordTimestampEnd( long recId )
  {
    Timestamp     beginTime   = null;
    long          elapsed     = 0L;
    Timestamp     endTime     = null;
    
    if( !inTestMode() )
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:457^9*/

//  ************************************************************
//  #sql [Ctx] { select  cp.process_begin_date 
//            from    commission_process cp
//            where   cp.rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cp.process_begin_date  \n          from    commission_process cp\n          where   cp.rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.CommissionLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:462^9*/
        endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
        elapsed = (endTime.getTime() - beginTime.getTime());
      
        /*@lineinfo:generated-code*//*@lineinfo:466^9*/

//  ************************************************************
//  #sql [Ctx] { update  commission_process
//            set     process_end_date = :endTime,
//                    process_elapsed = :DateTimeFormatter.getFormattedTimestamp(elapsed)
//            where   rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4046 = DateTimeFormatter.getFormattedTimestamp(elapsed);
   String theSqlTS = "update  commission_process\n          set     process_end_date =  :1 ,\n                  process_elapsed =  :2 \n          where   rec_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_4046);
   __sJT_st.setLong(3,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:472^9*/
      }
      catch(Exception e)
      {
        logEntry("recordTimestampEnd()", e.toString());
      }      
    }
  }
  
  private void setActiveDate( Date activeDate )
  {
    ActiveDate = activeDate;
  }
  
  private void setGroupId( int groupId )
  {
    GroupId = groupId;
  }
  
  private void setLoadFilename( String loadFilename )
  {
    LoadFilename = loadFilename;
  }
  
  private void setMerchantNumber( long merchantId )
  {
    MerchantNumber = merchantId;
  }
  
  private void setRepId( long repId )
  {
    RepId = repId;
  }
  
  public void setTestMode( boolean mode )
  {
    TestMode = mode;
  }
  
  public void setTestSequenceId( long sequenceId )
  {
    TestSequenceId = sequenceId;
  }
        
  public void loadAgentBank()
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:523^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   cm.rep_id                                 as rep_id,
//                   cm.comm_group_id                          as group_id,
//                   cm.activation_date                        as activation,
//                   nvl(gr.hierarchy_node,0)                  as hierarchy_node,
//                   sm.assoc_hierarchy_node                   as sm_assoc,
//                   gr.hierarchy_node                         as hierarchy_node,
//                   cm.merchant_number                        as merchant_number,
//                   cm.dba_name                               as dba_name,
//                   sm.active_date                            as commission_date,
//                   sm.load_filename                          as load_filename,
//                   nvl(sm.liability_contract,'N')            as liability_contract,
//                   nvl(sm.referral_contract,'N')             as referral_contract,
//                   nvl(sm.reseller_contract,'N')             as reseller_contract,
//                   nvl(sm.cash_advance_referral,'N')         as cash_advance_referral,
//                   nvl(sm.mes_inventory,'N')                 as mes_inventory,
//                   rate.commission_type                      as commission_type,
//                   rate.pay_rate                             as pay_rate,
//                   nvl(sm.vital_merchant_income,0)           as merchant_income,
//                   decode( sm.interchange_expense,
//                           0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),
//                           sm.interchange_expense )          as ic_exp,
//                   nvl(sm.vmc_assessment_expense,0)          as vmc_assessment_expense,
//                   nvl(mec.tot_ndr,0)                        as ndr_liability,
//                   nvl(mec.tot_authorization,0)              as auth_liability,
//                   nvl(mec.tot_capture,0)                    as capture_liability,
//                   nvl(mec.tot_debit,0)                      as debit_liability,
//                   nvl(mec.tot_ind_plans,0)                  as ind_plans_liability,
//                   nvl(mec.tot_sys_generated,0)              as sys_gen_liability,
//                   nvl(mec.tot_equip_rental,0)               as equip_rental_liability,
//                   nvl(mec.tot_equip_sales,0)                as equip_sales_liability,
//                   nvl(mxc.tot_ndr,0)                        as ndr_referral,
//                   nvl(mxc.tot_authorization,0)              as auth_referral,
//                   nvl(mxc.tot_capture,0)                    as capture_referral,
//                   nvl(mxc.tot_debit,0)                      as debit_referral,
//                   nvl(mxc.tot_ind_plans,0)                  as ind_plans_referral,
//                   nvl(mxc.tot_sys_generated,0)              as sys_gen_referral,
//                   nvl(mxc.tot_equip_rental,0)               as equip_rental_referral,
//                   nvl(mxc.tot_equip_sales,0)                as equip_sales_referral,
//                   sm.tot_inc_discount + 
//                   sm.tot_inc_interchange                    as disc_ic_inc,
//                   nvl(sm.tot_inc_authorization,0)           as auth_inc,
//                   nvl(sm.tot_inc_capture,0)                 as capture_inc,
//                   nvl(sm.tot_inc_debit,0)                   as debit_inc,
//                   nvl(sm.tot_inc_ind_plans,0)               as ind_plans_inc,
//                   sm.tot_inc_sys_generated -
//                   ( sm.equip_sales_income +
//                     sm.equip_rental_income +
//                     nvl(sm.equip_sales_tax_collected,0) )   as sys_gen_inc,
//                   nvl(sm.equip_sales_income,0)              as equip_sales_inc,
//                   nvl(sm.equip_rental_income,0)             as equip_rental_inc,
//                   nvl(sm.equip_sales_tax_collected,0)       as equip_sales_tax,
//                   nvl(sm.disc_ic_dce_adj_amount,0)          as disc_ic_adj,
//                   nvl(sm.fee_dce_adj_amount,0)              as fee_adj,
//                   nvl(sm.tot_exp_capture,0)                 as exp_capture,
//                   nvl(sm.tot_exp_debit,0)                   as exp_debit,
//                   nvl(sm.tot_exp_ind_plans,0)               as exp_ind_plans,
//                   nvl(sm.tot_exp_authorization,0)           as exp_auth,
//                   nvl(sm.equip_sales_expense,0)             as equip_sales_exp,
//                   nvl(sm.equip_rental_expense,0)            as equip_rental_exp,
//                   sm.tot_exp_sys_generated -
//                   ( nvl(sm.equip_sales_expense,0) + 
//                     nvl(sm.equip_rental_expense,0) )        as sys_gen_exp,
//                   nvl(sm.mes_only_inc_disc_ic,0)            as mes_only_disc_ic,
//                   nvl(sm.mes_only_inc_authorization,0)      as mes_only_auth,
//                   nvl(sm.mes_only_inc_capture,0)            as mes_only_capture,
//                   nvl(sm.mes_only_inc_debit,0)              as mes_only_debit,
//                   nvl(sm.mes_only_inc_ind_plans,0)          as mes_only_plan,
//                   nvl(sm.mes_only_inc_sys_generated,0)      as mes_only_sys_gen,
//                   nvl(sm.equip_rental_base_cost,0)          as equip_rental_base_cost,
//                   nvl(sm.equip_sales_base_cost,0)           as equip_sales_base_cost,
//                   nvl(sm.equip_transfer_cost,0)             as equip_transfer_cost,
//                   nvl(sm.tot_partnership,0)                 as partnership,
//                   nvl(sm.excluded_fees_amount,0)            as excluded_fees
//          from     commission_info              cm,
//                   monthly_extract_summary      sm,
//                   monthly_extract_contract     mec,
//                   monthly_extract_contract     mxc,
//                   commission_sales_rep         rep,
//                   commission_rates             rate,
//                   ( select sr.rep_id          as rep_id,
//                            sr.hierarchy_node  as hierarchy_node,
//                            sr.org_name        as org_name,
//                            gm.merchant_number as merchant_number
//                     from   organization o,
//                            group_merchant gm,
//                            commission_sales_rep_hierarchy sr
//                     where  (:RepId = -1 or sr.rep_id = :RepId)
//                            and o.org_group = sr.hierarchy_node
//                            and gm.org_num = o.org_num
//                   ) gr
//          where    rep.group_id = :REP_GROUP_AGENT_BANK and  -- 2
//                   (:GroupId = -1 or :GroupId = :REP_GROUP_AGENT_BANK) and
//                   (:RepId = -1 or rep.user_id = :RepId ) and
//                   cm.rep_id = rep.user_id and
//                   :ActiveDate between cm.first_comm_date and cm.last_comm_date and
//                   (:MerchantNumber = -1 or cm.merchant_number = :MerchantNumber) and
//                   sm.active_date = :ActiveDate and
//                   ('passall' = :LoadFilename or sm.load_filename = :LoadFilename) and
//                   gr.merchant_number(+) = cm.merchant_number and
//                   gr.rep_id(+) = cm.rep_id and
//                   sm.merchant_number = cm.merchant_number and
//                   mec.hh_load_sec(+) = sm.hh_load_sec and
//                   mec.contract_type(+) = 1 and
//                   mec.merchant_number(+) = sm.merchant_number and
//                   mxc.hh_load_sec(+) = sm.hh_load_sec and
//                   mxc.contract_type(+) = 2 and
//                   mxc.merchant_number(+) = sm.merchant_number and
//                   rate.user_id = cm.rep_id and
//                   :ActiveDate between rate.valid_date_begin and rate.valid_date_end and
//                   rate.commission_type = :TYPE_MONTHLY_COMMISSION and
//                   rate.contract_type = decode( nvl(gr.hierarchy_node,'0'),
//                                                0, 2,
//                                                decode( decode(sm.liability_contract,'Y',1,0),1,1,0,2) )
//                   and not exists
//                   (
//                     select merchant_number
//                     from   commissions
//                     where  commission_date = :ActiveDate
//                            and user_id = cm.rep_id
//                            and merchant_number = cm.merchant_number
//                   )
//          order by gr.hierarchy_node,cm.rep_id, cm.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   cm.rep_id                                 as rep_id,\n                 cm.comm_group_id                          as group_id,\n                 cm.activation_date                        as activation,\n                 nvl(gr.hierarchy_node,0)                  as hierarchy_node,\n                 sm.assoc_hierarchy_node                   as sm_assoc,\n                 gr.hierarchy_node                         as hierarchy_node,\n                 cm.merchant_number                        as merchant_number,\n                 cm.dba_name                               as dba_name,\n                 sm.active_date                            as commission_date,\n                 sm.load_filename                          as load_filename,\n                 nvl(sm.liability_contract,'N')            as liability_contract,\n                 nvl(sm.referral_contract,'N')             as referral_contract,\n                 nvl(sm.reseller_contract,'N')             as reseller_contract,\n                 nvl(sm.cash_advance_referral,'N')         as cash_advance_referral,\n                 nvl(sm.mes_inventory,'N')                 as mes_inventory,\n                 rate.commission_type                      as commission_type,\n                 rate.pay_rate                             as pay_rate,\n                 nvl(sm.vital_merchant_income,0)           as merchant_income,\n                 decode( sm.interchange_expense,\n                         0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),\n                         sm.interchange_expense )          as ic_exp,\n                 nvl(sm.vmc_assessment_expense,0)          as vmc_assessment_expense,\n                 nvl(mec.tot_ndr,0)                        as ndr_liability,\n                 nvl(mec.tot_authorization,0)              as auth_liability,\n                 nvl(mec.tot_capture,0)                    as capture_liability,\n                 nvl(mec.tot_debit,0)                      as debit_liability,\n                 nvl(mec.tot_ind_plans,0)                  as ind_plans_liability,\n                 nvl(mec.tot_sys_generated,0)              as sys_gen_liability,\n                 nvl(mec.tot_equip_rental,0)               as equip_rental_liability,\n                 nvl(mec.tot_equip_sales,0)                as equip_sales_liability,\n                 nvl(mxc.tot_ndr,0)                        as ndr_referral,\n                 nvl(mxc.tot_authorization,0)              as auth_referral,\n                 nvl(mxc.tot_capture,0)                    as capture_referral,\n                 nvl(mxc.tot_debit,0)                      as debit_referral,\n                 nvl(mxc.tot_ind_plans,0)                  as ind_plans_referral,\n                 nvl(mxc.tot_sys_generated,0)              as sys_gen_referral,\n                 nvl(mxc.tot_equip_rental,0)               as equip_rental_referral,\n                 nvl(mxc.tot_equip_sales,0)                as equip_sales_referral,\n                 sm.tot_inc_discount + \n                 sm.tot_inc_interchange                    as disc_ic_inc,\n                 nvl(sm.tot_inc_authorization,0)           as auth_inc,\n                 nvl(sm.tot_inc_capture,0)                 as capture_inc,\n                 nvl(sm.tot_inc_debit,0)                   as debit_inc,\n                 nvl(sm.tot_inc_ind_plans,0)               as ind_plans_inc,\n                 sm.tot_inc_sys_generated -\n                 ( sm.equip_sales_income +\n                   sm.equip_rental_income +\n                   nvl(sm.equip_sales_tax_collected,0) )   as sys_gen_inc,\n                 nvl(sm.equip_sales_income,0)              as equip_sales_inc,\n                 nvl(sm.equip_rental_income,0)             as equip_rental_inc,\n                 nvl(sm.equip_sales_tax_collected,0)       as equip_sales_tax,\n                 nvl(sm.disc_ic_dce_adj_amount,0)          as disc_ic_adj,\n                 nvl(sm.fee_dce_adj_amount,0)              as fee_adj,\n                 nvl(sm.tot_exp_capture,0)                 as exp_capture,\n                 nvl(sm.tot_exp_debit,0)                   as exp_debit,\n                 nvl(sm.tot_exp_ind_plans,0)               as exp_ind_plans,\n                 nvl(sm.tot_exp_authorization,0)           as exp_auth,\n                 nvl(sm.equip_sales_expense,0)             as equip_sales_exp,\n                 nvl(sm.equip_rental_expense,0)            as equip_rental_exp,\n                 sm.tot_exp_sys_generated -\n                 ( nvl(sm.equip_sales_expense,0) + \n                   nvl(sm.equip_rental_expense,0) )        as sys_gen_exp,\n                 nvl(sm.mes_only_inc_disc_ic,0)            as mes_only_disc_ic,\n                 nvl(sm.mes_only_inc_authorization,0)      as mes_only_auth,\n                 nvl(sm.mes_only_inc_capture,0)            as mes_only_capture,\n                 nvl(sm.mes_only_inc_debit,0)              as mes_only_debit,\n                 nvl(sm.mes_only_inc_ind_plans,0)          as mes_only_plan,\n                 nvl(sm.mes_only_inc_sys_generated,0)      as mes_only_sys_gen,\n                 nvl(sm.equip_rental_base_cost,0)          as equip_rental_base_cost,\n                 nvl(sm.equip_sales_base_cost,0)           as equip_sales_base_cost,\n                 nvl(sm.equip_transfer_cost,0)             as equip_transfer_cost,\n                 nvl(sm.tot_partnership,0)                 as partnership,\n                 nvl(sm.excluded_fees_amount,0)            as excluded_fees\n        from     commission_info              cm,\n                 monthly_extract_summary      sm,\n                 monthly_extract_contract     mec,\n                 monthly_extract_contract     mxc,\n                 commission_sales_rep         rep,\n                 commission_rates             rate,\n                 ( select sr.rep_id          as rep_id,\n                          sr.hierarchy_node  as hierarchy_node,\n                          sr.org_name        as org_name,\n                          gm.merchant_number as merchant_number\n                   from   organization o,\n                          group_merchant gm,\n                          commission_sales_rep_hierarchy sr\n                   where  ( :1  = -1 or sr.rep_id =  :2 )\n                          and o.org_group = sr.hierarchy_node\n                          and gm.org_num = o.org_num\n                 ) gr\n        where    rep.group_id =  :3  and  -- 2\n                 ( :4  = -1 or  :5  =  :6 ) and\n                 ( :7  = -1 or rep.user_id =  :8  ) and\n                 cm.rep_id = rep.user_id and\n                  :9  between cm.first_comm_date and cm.last_comm_date and\n                 ( :10  = -1 or cm.merchant_number =  :11 ) and\n                 sm.active_date =  :12  and\n                 ('passall' =  :13  or sm.load_filename =  :14 ) and\n                 gr.merchant_number(+) = cm.merchant_number and\n                 gr.rep_id(+) = cm.rep_id and\n                 sm.merchant_number = cm.merchant_number and\n                 mec.hh_load_sec(+) = sm.hh_load_sec and\n                 mec.contract_type(+) = 1 and\n                 mec.merchant_number(+) = sm.merchant_number and\n                 mxc.hh_load_sec(+) = sm.hh_load_sec and\n                 mxc.contract_type(+) = 2 and\n                 mxc.merchant_number(+) = sm.merchant_number and\n                 rate.user_id = cm.rep_id and\n                  :15  between rate.valid_date_begin and rate.valid_date_end and\n                 rate.commission_type =  :16  and\n                 rate.contract_type = decode( nvl(gr.hierarchy_node,'0'),\n                                              0, 2,\n                                              decode( decode(sm.liability_contract,'Y',1,0),1,1,0,2) )\n                 and not exists\n                 (\n                   select merchant_number\n                   from   commissions\n                   where  commission_date =  :17 \n                          and user_id = cm.rep_id\n                          and merchant_number = cm.merchant_number\n                 )\n        order by gr.hierarchy_node,cm.rep_id, cm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,RepId);
   __sJT_st.setLong(2,RepId);
   __sJT_st.setInt(3,REP_GROUP_AGENT_BANK);
   __sJT_st.setInt(4,GroupId);
   __sJT_st.setInt(5,GroupId);
   __sJT_st.setInt(6,REP_GROUP_AGENT_BANK);
   __sJT_st.setLong(7,RepId);
   __sJT_st.setLong(8,RepId);
   __sJT_st.setDate(9,ActiveDate);
   __sJT_st.setLong(10,MerchantNumber);
   __sJT_st.setLong(11,MerchantNumber);
   __sJT_st.setDate(12,ActiveDate);
   __sJT_st.setString(13,LoadFilename);
   __sJT_st.setString(14,LoadFilename);
   __sJT_st.setDate(15,ActiveDate);
   __sJT_st.setInt(16,TYPE_MONTHLY_COMMISSION);
   __sJT_st.setDate(17,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.CommissionLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:647^7*/
      
      resultSet = it.getResultSet();
      while( resultSet.next() )
      {
        DataRows.add( new CommissionData(resultSet) );
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadAgentBank()", e.toString() );
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
      try { resultSet.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadMonthEndBonus()
  {
    double      income              = 0.0;
    double      expense             = 0.0;
    double      netRevenue          = 0.0;
    double      payRate             = 0.0;
    double      commAmount          = 0.0;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    
    DataRows.clear();

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:680^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rep.user_id                      as user_id,
//                  rep.group_id                     as group_id,
//                  :TYPE_MONTHLY_BONUS_COMMISSION   as comm_type,
//                  rate.pay_rate                    as pay_rate,
//                  rate.revenue_goal                as revenue_goal,
//                  nvl(sum(cm.income),0)            as tot_income,
//                  nvl(sum(cm.expense),0)           as tot_expense,
//                  nvl(sum(cm.net_revenue),0)       as net_revenue
//          from    commissions            cm,
//                  commission_rates       rate,
//                  (
//                    select sr.group_id as group_id,
//                           sr.user_id as user_id
//                    from   users u,
//                           commission_sales_rep sr
//                    where  sr.group_id in( :REP_GROUP_ECOMMERCE_TELESALES,    -- 1
//                                           :REP_GROUP_REFERRAL_TELESALES,     -- 3
//                                           :REP_GROUP_ECOMMERCE_FIELDSALES )  -- 5
//                           and ( :GroupId = -1 or sr.group_id = :GroupId )
//                           and ( :RepId = -1 or sr.user_id = :RepId )
//                           and u.login_name = sr.login_name
//                           and nvl(u.enabled,'N') = 'Y'
//                    group by sr.group_id, sr.user_id 
//                  ) rep
//           where  :ActiveDate between rate.valid_date_begin and rate.valid_date_end
//                  and rate.user_id = rep.user_id
//                  and rate.commission_type = :TYPE_MONTHLY_BONUS_COMMISSION
//                  and cm.commission_date = :ActiveDate
//                  and cm.commission_type = :TYPE_MONTHLY_COMMISSION
//                  and cm.user_id = rep.user_id
//          group by rep.user_id, rep.group_id, rate.pay_rate, rate.revenue_goal
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rep.user_id                      as user_id,\n                rep.group_id                     as group_id,\n                 :1    as comm_type,\n                rate.pay_rate                    as pay_rate,\n                rate.revenue_goal                as revenue_goal,\n                nvl(sum(cm.income),0)            as tot_income,\n                nvl(sum(cm.expense),0)           as tot_expense,\n                nvl(sum(cm.net_revenue),0)       as net_revenue\n        from    commissions            cm,\n                commission_rates       rate,\n                (\n                  select sr.group_id as group_id,\n                         sr.user_id as user_id\n                  from   users u,\n                         commission_sales_rep sr\n                  where  sr.group_id in(  :2 ,    -- 1\n                                          :3 ,     -- 3\n                                          :4  )  -- 5\n                         and (  :5  = -1 or sr.group_id =  :6  )\n                         and (  :7  = -1 or sr.user_id =  :8  )\n                         and u.login_name = sr.login_name\n                         and nvl(u.enabled,'N') = 'Y'\n                  group by sr.group_id, sr.user_id \n                ) rep\n         where   :9  between rate.valid_date_begin and rate.valid_date_end\n                and rate.user_id = rep.user_id\n                and rate.commission_type =  :10 \n                and cm.commission_date =  :11 \n                and cm.commission_type =  :12 \n                and cm.user_id = rep.user_id\n        group by rep.user_id, rep.group_id, rate.pay_rate, rate.revenue_goal";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,TYPE_MONTHLY_BONUS_COMMISSION);
   __sJT_st.setInt(2,REP_GROUP_ECOMMERCE_TELESALES);
   __sJT_st.setInt(3,REP_GROUP_REFERRAL_TELESALES);
   __sJT_st.setInt(4,REP_GROUP_ECOMMERCE_FIELDSALES);
   __sJT_st.setInt(5,GroupId);
   __sJT_st.setInt(6,GroupId);
   __sJT_st.setLong(7,RepId);
   __sJT_st.setLong(8,RepId);
   __sJT_st.setDate(9,ActiveDate);
   __sJT_st.setInt(10,TYPE_MONTHLY_BONUS_COMMISSION);
   __sJT_st.setDate(11,ActiveDate);
   __sJT_st.setInt(12,TYPE_MONTHLY_COMMISSION);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.CommissionLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:713^7*/        
      rs = it.getResultSet();
      while( rs.next() )
      {
        income   = rs.getDouble("net_revenue");
        expense  = rs.getDouble("revenue_goal");
        payRate  = rs.getDouble("pay_rate");
        netRevenue   = income - expense;
        netRevenue   = ( netRevenue < 0 ? 0 : netRevenue );
        commAmount   = netRevenue * payRate * 0.01;
        DataRows.add( new CommissionData( 0,
                                          rs.getLong("user_id"),
                                          payRate,
                                          null,
                                          ActiveDate,
                                          TYPE_MONTHLY_BONUS_COMMISSION,
                                          income,
                                          expense,
                                          netRevenue,
                                          commAmount ) );
      }
      
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:737^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rep.user_id                      as user_id,
//                  rep.group_id                     as group_id,
//                  :TYPE_MONTHLY_BONUS_COMMISSION   as comm_type,
//                  rate.pay_rate                    as pay_rate,
//                  rate.revenue_goal                as revenue_goal,
//                  sum(cm.income)            as tot_income,
//                  sum(cm.expense)           as tot_expense,
//                  sum(cm.net_revenue)       as net_revenue,
//                  sum(cm.pre_cntr_net_revenue)       as pre_cntr_net_revenue
//          from    commissions            cm,
//                  commission_rates       rate,
//                  (
//                    select sr.group_id as group_id,
//                           sr.user_id as user_id
//                    from   commission_sales_rep sr
//                    where  sr.group_id in (:REP_GROUP_CBT_SALES_REP,:REP_GROUP_CBT_SALES_MANAGER)
//                           and ( :GroupId = -1 or sr.group_id = :GroupId )
//                           and ( :RepId = -1 or sr.user_id = :RepId )
//                           and nvl(sr.enabled,'N') = 'Y'
//                    group by sr.group_id, sr.user_id 
//                  ) rep
//           where  :ActiveDate between rate.valid_date_begin and rate.valid_date_end
//                  and rate.user_id = rep.user_id
//                  and rate.commission_type = :TYPE_MONTHLY_BONUS_COMMISSION
//                  and cm.commission_date = :ActiveDate
//                  and cm.commission_type = :TYPE_MONTHLY_COMMISSION
//                  and cm.user_id = rep.user_id
//          group by rep.user_id, rep.group_id, rate.pay_rate, rate.revenue_goal
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rep.user_id                      as user_id,\n                rep.group_id                     as group_id,\n                 :1    as comm_type,\n                rate.pay_rate                    as pay_rate,\n                rate.revenue_goal                as revenue_goal,\n                sum(cm.income)            as tot_income,\n                sum(cm.expense)           as tot_expense,\n                sum(cm.net_revenue)       as net_revenue,\n                sum(cm.pre_cntr_net_revenue)       as pre_cntr_net_revenue\n        from    commissions            cm,\n                commission_rates       rate,\n                (\n                  select sr.group_id as group_id,\n                         sr.user_id as user_id\n                  from   commission_sales_rep sr\n                  where  sr.group_id in ( :2 , :3 )\n                         and (  :4  = -1 or sr.group_id =  :5  )\n                         and (  :6  = -1 or sr.user_id =  :7  )\n                         and nvl(sr.enabled,'N') = 'Y'\n                  group by sr.group_id, sr.user_id \n                ) rep\n         where   :8  between rate.valid_date_begin and rate.valid_date_end\n                and rate.user_id = rep.user_id\n                and rate.commission_type =  :9 \n                and cm.commission_date =  :10 \n                and cm.commission_type =  :11 \n                and cm.user_id = rep.user_id\n        group by rep.user_id, rep.group_id, rate.pay_rate, rate.revenue_goal";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,TYPE_MONTHLY_BONUS_COMMISSION);
   __sJT_st.setInt(2,REP_GROUP_CBT_SALES_REP);
   __sJT_st.setInt(3,REP_GROUP_CBT_SALES_MANAGER);
   __sJT_st.setInt(4,GroupId);
   __sJT_st.setInt(5,GroupId);
   __sJT_st.setLong(6,RepId);
   __sJT_st.setLong(7,RepId);
   __sJT_st.setDate(8,ActiveDate);
   __sJT_st.setInt(9,TYPE_MONTHLY_BONUS_COMMISSION);
   __sJT_st.setDate(10,ActiveDate);
   __sJT_st.setInt(11,TYPE_MONTHLY_COMMISSION);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.CommissionLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:767^7*/        
      rs = it.getResultSet();
      int count = 0;
      while( rs.next() )
      {
        ++count;
        netRevenue = 0;
        payRate = rs.getDouble("pay_rate");
        if( rs.getDouble("pre_cntr_net_revenue") >= rs.getDouble("revenue_goal") )
        {
          netRevenue = rs.getDouble("net_revenue");
        }
        
        DataRows.add( new CommissionData( 0,
                                          rs.getLong("user_id"),
                                          payRate,
                                          null,
                                          ActiveDate,
                                          TYPE_MONTHLY_BONUS_COMMISSION,
                                          netRevenue,
                                          0.0,
                                          netRevenue,
                                          rs.getDouble("pre_cntr_net_revenue"),
                                          netRevenue * payRate * 0.01 ) );

      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadMonthEndBonus()", e.toString() );
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadData()
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      DataRows.clear();
      if( GroupId == -1 || 
          GroupId == REP_GROUP_ECOMMERCE_TELESALES ||
          GroupId == REP_GROUP_REFERRAL_TELESALES || 
          GroupId == REP_GROUP_ECOMMERCE_FIELDSALES ||
          GroupId == REP_GROUP_ECOMMERCE_MANAGER ||
          GroupId == REP_GROUP_CBT_SALES_REP ||
          GroupId == REP_GROUP_CBT_SALES_MANAGER )
      {
        /*@lineinfo:generated-code*//*@lineinfo:820^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cif.comm_group_id                         as group_id,
//                     csr.user_id                               as rep_id,
//                     cmr.commission_type                       as commission_type,
//                     cmr.pay_rate                              as pay_rate,
//                     cif.merchant_number                       as merchant_number,
//                     cif.dba_name                              as dba_name,
//                     cif.date_opened                           as date_opened,
//                     cif.activation_date                       as activation_date,
//                     decode( trunc(cif.activation_date,'month'), :ActiveDate, 1, 0 )
//                                                               as account_activated,
//                     decode( trunc(cif.date_opened,'month'), :ActiveDate, 1, 0 )
//                                                               as account_opened,
//                     sm.active_date                            as commission_date,
//                     sm.load_filename                          as load_filename,
//                     sm.vital_merchant_income                  as merchant_income,
//                     decode( sm.interchange_expense,
//                             0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),
//                             sm.interchange_expense )          as ic_exp,
//                     nvl(sm.vmc_assessment_expense,0)          as vmc_assessment_expense,
//                     nvl(sm.liability_contract,'N')            as liability_contract,
//                     nvl(sm.referral_contract,'N')             as referral_contract,
//                     nvl(sm.reseller_contract,'N')             as reseller_contract,
//                     nvl(sm.cash_advance_referral,'N')         as cash_advance_referral,
//                     nvl(sm.mes_inventory,'N')                 as mes_inventory,
//                     nvl(sm.tot_authorization_referral,0)      as auth_referral,
//                     nvl(sm.tot_capture_referral ,0)           as capture_referral,
//                     nvl(sm.tot_debit_referral ,0)             as debit_referral,
//                     nvl(sm.tot_equip_rental_referral ,0)      as equip_rental_referral,
//                     nvl(sm.tot_equip_sales_referral, 0)       as equip_sales_referral,
//                     nvl(sm.tot_ind_plans_referral, 0)         as ind_plans_referral,
//                     nvl(sm.tot_ndr_referral,0)                as ndr_referral,
//                     nvl(sm.tot_sys_generated_referral, 0)     as sys_gen_referral,
//                     nvl(sm.tot_authorization_liability,0)     as auth_liability,
//                     nvl(sm.tot_capture_liability,0)           as capture_liability,
//                     nvl(sm.tot_debit_liability,0)             as debit_liability,
//                     nvl(sm.tot_equip_rental_liability,0)      as equip_rental_liability,
//                     nvl(sm.tot_equip_sales_liability,0)       as equip_sales_liability,
//                     nvl(sm.tot_ind_plans_liability,0)         as ind_plans_liability,
//                     nvl(sm.tot_ndr_liability,0)               as ndr_liability,
//                     nvl(sm.tot_sys_generated_liability,0)     as sys_gen_liability,
//                     sm.tot_inc_discount + 
//                     sm.tot_inc_interchange                    as disc_ic_inc,
//                     nvl(sm.tot_inc_authorization,0)           as auth_inc,
//                     nvl(sm.tot_inc_capture,0)                 as capture_inc,
//                     nvl(sm.tot_inc_debit,0)                   as debit_inc,
//                     nvl(sm.tot_inc_ind_plans,0)               as ind_plans_inc,
//                     sm.tot_inc_sys_generated -
//                     ( sm.equip_sales_income +
//                       sm.equip_rental_income +
//                       nvl(sm.equip_sales_tax_collected,0) )   as sys_gen_inc,
//                     nvl(sm.equip_sales_income,0)              as equip_sales_inc,
//                     nvl(sm.equip_rental_income,0)             as equip_rental_inc,
//                     nvl(sm.equip_sales_tax_collected,0)       as equip_sales_tax,
//                     nvl(sm.disc_ic_dce_adj_amount,0)          as disc_ic_adj,
//                     nvl(sm.fee_dce_adj_amount,0)              as fee_adj,
//                     nvl(sm.tot_exp_capture,0)                 as exp_capture,
//                     nvl(sm.tot_exp_debit,0)                   as exp_debit,
//                     nvl(sm.tot_exp_ind_plans,0)               as exp_ind_plans,
//                     nvl(sm.tot_exp_authorization,0)           as exp_auth,
//                     nvl(sm.equip_sales_expense,0)             as equip_sales_exp,
//                     nvl(sm.equip_rental_expense,0)            as equip_rental_exp,
//                     sm.tot_exp_sys_generated -
//                     ( nvl(sm.equip_sales_expense,0) + 
//                       nvl(sm.equip_rental_expense,0) )        as sys_gen_exp,
//                     nvl(sm.mes_only_inc_disc_ic,0)            as mes_only_disc_ic,
//                     nvl(sm.mes_only_inc_authorization,0)      as mes_only_auth,
//                     nvl(sm.mes_only_inc_capture,0)            as mes_only_capture,
//                     nvl(sm.mes_only_inc_debit,0)              as mes_only_debit,
//                     nvl(sm.mes_only_inc_ind_plans,0)          as mes_only_plan,
//                     nvl(sm.mes_only_inc_sys_generated,0)      as mes_only_sys_gen,
//                     nvl(sm.equip_rental_base_cost,0)          as equip_rental_base_cost,
//                     nvl(sm.equip_sales_base_cost,0)           as equip_sales_base_cost,
//                     nvl(sm.equip_transfer_cost,0)             as equip_transfer_cost,
//                     nvl(sm.tot_partnership,0)                 as partnership,
//                     nvl(sm.excluded_fees_amount,0)            as excluded_fees
//             from    commission_info cif,
//                     commission_rates cmr,
//                     monthly_extract_summary sm,
//                     ( select sr.user_id, sr.rep_name
//                       from   commission_sales_rep sr
//                       where  sr.group_id in( :REP_GROUP_ECOMMERCE_TELESALES,    -- 1
//                                              :REP_GROUP_REFERRAL_TELESALES,     -- 3
//                                              :REP_GROUP_ECOMMERCE_FIELDSALES,   -- 5
//                                              :REP_GROUP_ECOMMERCE_MANAGER,      -- 4
//                                              :REP_GROUP_CBT_SALES_REP,          -- 7
//                                              :REP_GROUP_CBT_SALES_MANAGER )     -- 8
//                              and nvl(sr.enabled,'N') = 'Y'
//                              and (:GroupId = -1 or sr.group_id = :GroupId)
//                              and (:RepId = -1 or sr.user_id = :RepId)
//                       group by sr.user_id, sr.rep_name
//                     ) csr
//             where   cif.rep_id = csr.user_id
//                     and :ActiveDate between cif.first_comm_date and cif.last_comm_date
//                     and sm.active_date = :ActiveDate
//                     and ('passall' = :LoadFilename or sm.load_filename = :LoadFilename)
//                     and sm.merchant_number = cif.merchant_number
//                     and cif.activation_date between cmr.valid_date_begin and cmr.valid_date_end
//                     and cmr.user_id = cif.rep_id
//                     and cmr.commission_type = cif.comm_type
//                     and not exists
//                     (
//                       select merchant_number
//                       from   commissions
//                       where  commission_date = :ActiveDate
//                              and user_id = csr.user_id
//                              and merchant_number = cif.merchant_number
//                              and commission_type = cmr.commission_type
//                     )
//  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cif.comm_group_id                         as group_id,\n                   csr.user_id                               as rep_id,\n                   cmr.commission_type                       as commission_type,\n                   cmr.pay_rate                              as pay_rate,\n                   cif.merchant_number                       as merchant_number,\n                   cif.dba_name                              as dba_name,\n                   cif.date_opened                           as date_opened,\n                   cif.activation_date                       as activation_date,\n                   decode( trunc(cif.activation_date,'month'),  :1 , 1, 0 )\n                                                             as account_activated,\n                   decode( trunc(cif.date_opened,'month'),  :2 , 1, 0 )\n                                                             as account_opened,\n                   sm.active_date                            as commission_date,\n                   sm.load_filename                          as load_filename,\n                   sm.vital_merchant_income                  as merchant_income,\n                   decode( sm.interchange_expense,\n                           0, (sm.bet_interchange_expense-sm.vmc_assessment_expense),\n                           sm.interchange_expense )          as ic_exp,\n                   nvl(sm.vmc_assessment_expense,0)          as vmc_assessment_expense,\n                   nvl(sm.liability_contract,'N')            as liability_contract,\n                   nvl(sm.referral_contract,'N')             as referral_contract,\n                   nvl(sm.reseller_contract,'N')             as reseller_contract,\n                   nvl(sm.cash_advance_referral,'N')         as cash_advance_referral,\n                   nvl(sm.mes_inventory,'N')                 as mes_inventory,\n                   nvl(sm.tot_authorization_referral,0)      as auth_referral,\n                   nvl(sm.tot_capture_referral ,0)           as capture_referral,\n                   nvl(sm.tot_debit_referral ,0)             as debit_referral,\n                   nvl(sm.tot_equip_rental_referral ,0)      as equip_rental_referral,\n                   nvl(sm.tot_equip_sales_referral, 0)       as equip_sales_referral,\n                   nvl(sm.tot_ind_plans_referral, 0)         as ind_plans_referral,\n                   nvl(sm.tot_ndr_referral,0)                as ndr_referral,\n                   nvl(sm.tot_sys_generated_referral, 0)     as sys_gen_referral,\n                   nvl(sm.tot_authorization_liability,0)     as auth_liability,\n                   nvl(sm.tot_capture_liability,0)           as capture_liability,\n                   nvl(sm.tot_debit_liability,0)             as debit_liability,\n                   nvl(sm.tot_equip_rental_liability,0)      as equip_rental_liability,\n                   nvl(sm.tot_equip_sales_liability,0)       as equip_sales_liability,\n                   nvl(sm.tot_ind_plans_liability,0)         as ind_plans_liability,\n                   nvl(sm.tot_ndr_liability,0)               as ndr_liability,\n                   nvl(sm.tot_sys_generated_liability,0)     as sys_gen_liability,\n                   sm.tot_inc_discount + \n                   sm.tot_inc_interchange                    as disc_ic_inc,\n                   nvl(sm.tot_inc_authorization,0)           as auth_inc,\n                   nvl(sm.tot_inc_capture,0)                 as capture_inc,\n                   nvl(sm.tot_inc_debit,0)                   as debit_inc,\n                   nvl(sm.tot_inc_ind_plans,0)               as ind_plans_inc,\n                   sm.tot_inc_sys_generated -\n                   ( sm.equip_sales_income +\n                     sm.equip_rental_income +\n                     nvl(sm.equip_sales_tax_collected,0) )   as sys_gen_inc,\n                   nvl(sm.equip_sales_income,0)              as equip_sales_inc,\n                   nvl(sm.equip_rental_income,0)             as equip_rental_inc,\n                   nvl(sm.equip_sales_tax_collected,0)       as equip_sales_tax,\n                   nvl(sm.disc_ic_dce_adj_amount,0)          as disc_ic_adj,\n                   nvl(sm.fee_dce_adj_amount,0)              as fee_adj,\n                   nvl(sm.tot_exp_capture,0)                 as exp_capture,\n                   nvl(sm.tot_exp_debit,0)                   as exp_debit,\n                   nvl(sm.tot_exp_ind_plans,0)               as exp_ind_plans,\n                   nvl(sm.tot_exp_authorization,0)           as exp_auth,\n                   nvl(sm.equip_sales_expense,0)             as equip_sales_exp,\n                   nvl(sm.equip_rental_expense,0)            as equip_rental_exp,\n                   sm.tot_exp_sys_generated -\n                   ( nvl(sm.equip_sales_expense,0) + \n                     nvl(sm.equip_rental_expense,0) )        as sys_gen_exp,\n                   nvl(sm.mes_only_inc_disc_ic,0)            as mes_only_disc_ic,\n                   nvl(sm.mes_only_inc_authorization,0)      as mes_only_auth,\n                   nvl(sm.mes_only_inc_capture,0)            as mes_only_capture,\n                   nvl(sm.mes_only_inc_debit,0)              as mes_only_debit,\n                   nvl(sm.mes_only_inc_ind_plans,0)          as mes_only_plan,\n                   nvl(sm.mes_only_inc_sys_generated,0)      as mes_only_sys_gen,\n                   nvl(sm.equip_rental_base_cost,0)          as equip_rental_base_cost,\n                   nvl(sm.equip_sales_base_cost,0)           as equip_sales_base_cost,\n                   nvl(sm.equip_transfer_cost,0)             as equip_transfer_cost,\n                   nvl(sm.tot_partnership,0)                 as partnership,\n                   nvl(sm.excluded_fees_amount,0)            as excluded_fees\n           from    commission_info cif,\n                   commission_rates cmr,\n                   monthly_extract_summary sm,\n                   ( select sr.user_id, sr.rep_name\n                     from   commission_sales_rep sr\n                     where  sr.group_id in(  :3 ,    -- 1\n                                             :4 ,     -- 3\n                                             :5 ,   -- 5\n                                             :6 ,      -- 4\n                                             :7 ,          -- 7\n                                             :8  )     -- 8\n                            and nvl(sr.enabled,'N') = 'Y'\n                            and ( :9  = -1 or sr.group_id =  :10 )\n                            and ( :11  = -1 or sr.user_id =  :12 )\n                     group by sr.user_id, sr.rep_name\n                   ) csr\n           where   cif.rep_id = csr.user_id\n                   and  :13  between cif.first_comm_date and cif.last_comm_date\n                   and sm.active_date =  :14 \n                   and ('passall' =  :15  or sm.load_filename =  :16 )\n                   and sm.merchant_number = cif.merchant_number\n                   and cif.activation_date between cmr.valid_date_begin and cmr.valid_date_end\n                   and cmr.user_id = cif.rep_id\n                   and cmr.commission_type = cif.comm_type\n                   and not exists\n                   (\n                     select merchant_number\n                     from   commissions\n                     where  commission_date =  :17 \n                            and user_id = csr.user_id\n                            and merchant_number = cif.merchant_number\n                            and commission_type = cmr.commission_type\n                   )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ActiveDate);
   __sJT_st.setDate(2,ActiveDate);
   __sJT_st.setInt(3,REP_GROUP_ECOMMERCE_TELESALES);
   __sJT_st.setInt(4,REP_GROUP_REFERRAL_TELESALES);
   __sJT_st.setInt(5,REP_GROUP_ECOMMERCE_FIELDSALES);
   __sJT_st.setInt(6,REP_GROUP_ECOMMERCE_MANAGER);
   __sJT_st.setInt(7,REP_GROUP_CBT_SALES_REP);
   __sJT_st.setInt(8,REP_GROUP_CBT_SALES_MANAGER);
   __sJT_st.setInt(9,GroupId);
   __sJT_st.setInt(10,GroupId);
   __sJT_st.setLong(11,RepId);
   __sJT_st.setLong(12,RepId);
   __sJT_st.setDate(13,ActiveDate);
   __sJT_st.setDate(14,ActiveDate);
   __sJT_st.setString(15,LoadFilename);
   __sJT_st.setString(16,LoadFilename);
   __sJT_st.setDate(17,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.CommissionLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:931^9*/
        resultSet = it.getResultSet();
        while( resultSet.next() )
        {
          DataRows.add( new CommissionData(resultSet) );
        }
        it.close();
      }
      
      
      if( GroupId == -1 || GroupId == REP_GROUP_AGENT_BANK )
      {
        loadAgentBank();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadData()", e.toString() );
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  
  public void showCommData()
  { 
    CommissionData   cd  = null; 
    
    try
    {
      for( int i = 0; i < DataRows.size() && i < 20; i++ )
      { 
        cd = (CommissionData)DataRows.elementAt(i);
        
        log.debug("sales rep       " + cd.RepId);
        log.debug("merchant number " + cd.MerchantNumber + " " + cd.DbaName);
        log.debug("comm amount     " + cd.CommisionAmount);  
        log.debug("income          " + cd.Income);
        log.debug("expense         " + cd.Expense);
        log.debug("net revenue     " + cd.NetRevenue);  
        log.debug("pay rate        " + cd.PayRate);  
        log.debug("--------------------------------------------------");
      }
    }
    catch( Exception e )
    {
    }
  }
  
  public void showMerchantData()
  { 
    MerchantData   md  = null;
    
    try
    {
      for( int i = 0; i < DataRows.size(); i++ )
      {
        md = (MerchantData)DataRows.elementAt(i);
        
        log.debug("repId " + md.SalesRepId + "  mid " + md.MerchantNumber + "  " + md.FirstCommDate + "  " + md.LastCommDate);
      }
    }
    catch( Exception e )
    {
    }
  }
  
  public void processMonthEndBonus()
  {
    long           recId    = -1L;
    CommissionData cd       = null;
    
    loadMonthEndBonus();
    
    try
    {
      for( int i = 0; i < DataRows.size(); i++ )
      { 
        cd = (CommissionData)DataRows.elementAt(i);
        
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1014^11*/

//  ************************************************************
//  #sql [Ctx] { select  rec_id 
//              from    commissions
//              where   user_id = :cd.RepId and
//                      commission_date = :ActiveDate and
//                      commission_type = :TYPE_MONTHLY_BONUS_COMMISSION
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rec_id  \n            from    commissions\n            where   user_id =  :1  and\n                    commission_date =  :2  and\n                    commission_type =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.CommissionLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,cd.RepId);
   __sJT_st.setDate(2,ActiveDate);
   __sJT_st.setInt(3,TYPE_MONTHLY_BONUS_COMMISSION);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1021^11*/
          
          if( recId != -1L )
          {
            /*@lineinfo:generated-code*//*@lineinfo:1025^13*/

//  ************************************************************
//  #sql [Ctx] { delete  
//                from    commissions
//                where   rec_id = :recId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  \n              from    commissions\n              where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1030^13*/
          }
          
          /*@lineinfo:generated-code*//*@lineinfo:1033^11*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1033^31*/
        }
        catch(java.sql.SQLException e)
        {
        }
      }
      
      storeCommission();
    }
    catch(Exception e)
    {
      logEntry( "processMonthEndBonus()", e.toString() );
    }
  }
  
  public void processYearEndBonus()
  {
    double      netRevenue          = 0.0;
    double      commAmount          = 0.0;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    
    if( !DateTimeFormatter.getFormattedDate(ActiveDate,"MM").equals("12") )
    {
      return;
    }
    
    DataRows.clear();

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1064^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cif.rep_id                        as user_id, 
//                  cif.comm_group_id                 as group_id,
//                  :TYPE_YEAR_END_BONUS_COMMISSION   as comm_type,
//                  rt.pay_rate                       as pay_rate,
//                  rt.revenue_goal                   as revenue_goal,
//                  count(cm.rec_id)                  as merch_count,
//                  sum(cm.net_revenue)               as net_revenue
//          from    commissions cm,
//                  commission_rates rt,
//                  commission_info  cif
//          where   :ActiveDate between rt.valid_date_begin and rt.valid_date_end 
//                  and rt.commission_type = :TYPE_YEAR_END_BONUS_COMMISSION
//                  and cif.rep_id = rt.user_id
//                  and cif.activation_date between rt.valid_date_begin and rt.valid_date_end
//                  and cm.user_id = cif.rep_id
//                  and cm.merchant_number = cif.merchant_number
//          group by cif.rep_id, cif.comm_group_id, rt.pay_rate, rt.revenue_goal
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cif.rep_id                        as user_id, \n                cif.comm_group_id                 as group_id,\n                 :1    as comm_type,\n                rt.pay_rate                       as pay_rate,\n                rt.revenue_goal                   as revenue_goal,\n                count(cm.rec_id)                  as merch_count,\n                sum(cm.net_revenue)               as net_revenue\n        from    commissions cm,\n                commission_rates rt,\n                commission_info  cif\n        where    :2  between rt.valid_date_begin and rt.valid_date_end \n                and rt.commission_type =  :3 \n                and cif.rep_id = rt.user_id\n                and cif.activation_date between rt.valid_date_begin and rt.valid_date_end\n                and cm.user_id = cif.rep_id\n                and cm.merchant_number = cif.merchant_number\n        group by cif.rep_id, cif.comm_group_id, rt.pay_rate, rt.revenue_goal";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,TYPE_YEAR_END_BONUS_COMMISSION);
   __sJT_st.setDate(2,ActiveDate);
   __sJT_st.setInt(3,TYPE_YEAR_END_BONUS_COMMISSION);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.startup.CommissionLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1083^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        netRevenue   = rs.getDouble("net_revenue") - rs.getDouble("revenue_goal");
        netRevenue   = ( netRevenue < 0 ? 0 : netRevenue );
        commAmount   = netRevenue * rs.getDouble("pay_rate") * 0.01;
        DataRows.add( new CommissionData( 0,
                                          rs.getLong("user_id"),
                                          rs.getDouble("pay_rate"),
                                          null,
                                          ActiveDate,
                                          TYPE_YEAR_END_BONUS_COMMISSION,
                                          rs.getDouble("net_revenue"),
                                          rs.getDouble("revenue_goal"),
                                          netRevenue,
                                          commAmount ) );
      }
      
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "processYearEndBonus()", e.toString() );
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
    
    storeCommission();
  }
  
  public void storeCommission()
  {
    int               count    = 0;
    CommissionData    cd       = null;
    
    try
    {
      for( int k = 0; k < DataRows.size(); k++ )
      { 
        cd = (CommissionData)DataRows.elementAt(k);
        count ++;
        /*@lineinfo:generated-code*//*@lineinfo:1128^9*/

//  ************************************************************
//  #sql [Ctx] { insert into commissions
//            (
//              user_id,
//              merchant_number,
//              commission_date,
//              commission_type,
//              transaction_type,
//              commission_amount,
//              pay_rate,
//              vol_count,
//              vol_amount,
//              income,
//              expense,
//              load_filename,
//              net_revenue,
//              pre_cntr_net_revenue
//            )
//            values
//            (
//              :cd.RepId,
//              :cd.MerchantNumber,
//              :cd.CommissionDate,
//              :cd.CommissionType,
//              'I',
//              :cd.CommisionAmount,
//              :cd.PayRate * 0.01,
//              '0',
//              '0.0',
//              :cd.Income,
//              :cd.Expense,
//              :cd.LoadFilename,
//              :cd.NetRevenue,
//              :cd.PreContractRevenue
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4047 = cd.PayRate * 0.01;
   String theSqlTS = "insert into commissions\n          (\n            user_id,\n            merchant_number,\n            commission_date,\n            commission_type,\n            transaction_type,\n            commission_amount,\n            pay_rate,\n            vol_count,\n            vol_amount,\n            income,\n            expense,\n            load_filename,\n            net_revenue,\n            pre_cntr_net_revenue\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n            'I',\n             :5 ,\n             :6 ,\n            '0',\n            '0.0',\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,cd.RepId);
   __sJT_st.setLong(2,cd.MerchantNumber);
   __sJT_st.setDate(3,cd.CommissionDate);
   __sJT_st.setInt(4,cd.CommissionType);
   __sJT_st.setDouble(5,cd.CommisionAmount);
   __sJT_st.setDouble(6,__sJT_4047);
   __sJT_st.setDouble(7,cd.Income);
   __sJT_st.setDouble(8,cd.Expense);
   __sJT_st.setString(9,cd.LoadFilename);
   __sJT_st.setDouble(10,cd.NetRevenue);
   __sJT_st.setDouble(11,cd.PreContractRevenue);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1164^9*/
        
        if( count == 200 )
        {
          count = 0;
          /*@lineinfo:generated-code*//*@lineinfo:1169^11*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1169^31*/  
        }        
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1173^7*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1173^27*/
    }
    catch( java.sql.SQLException e)
    {
      logEntry( "storeCommission()", e.toString() );
    }
  }
  
  public void storeMerchantData()
  {
    MerchantData    md       = null;
    
    for( int i = 0; i < DataRows.size(); i++ )
    {
      md = (MerchantData)DataRows.elementAt(i);
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1190^9*/

//  ************************************************************
//  #sql [Ctx] { insert into commission_info
//            (
//              rep_id,
//              app_seq_num,
//              merchant_number,
//              dba_name,
//              date_opened,
//              activation_date,
//              first_comm_date,
//              last_comm_date,
//              last_modified_date,
//              last_modified_by,
//              comm_group_id,
//              comm_type
//            )
//            values
//            (
//              :md.SalesRepId,
//              :md.AppSeqNumber,
//              :md.MerchantNumber,
//              :md.DbaName,
//              :md.DateOpened,
//              :md.ActivationDate,
//              :md.FirstCommDate,
//              :md.LastCommDate,
//              sysdate,
//              'ttran',
//              :md.RepGroupId,
//              :md.CommType
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into commission_info\n          (\n            rep_id,\n            app_seq_num,\n            merchant_number,\n            dba_name,\n            date_opened,\n            activation_date,\n            first_comm_date,\n            last_comm_date,\n            last_modified_date,\n            last_modified_by,\n            comm_group_id,\n            comm_type\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n            sysdate,\n            'ttran',\n             :9 ,\n             :10 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,md.SalesRepId);
   __sJT_st.setLong(2,md.AppSeqNumber);
   __sJT_st.setLong(3,md.MerchantNumber);
   __sJT_st.setString(4,md.DbaName);
   __sJT_st.setDate(5,md.DateOpened);
   __sJT_st.setDate(6,md.ActivationDate);
   __sJT_st.setDate(7,md.FirstCommDate);
   __sJT_st.setDate(8,md.LastCommDate);
   __sJT_st.setInt(9,md.RepGroupId);
   __sJT_st.setInt(10,md.CommType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1222^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:1224^9*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1224^29*/
      }
      catch( java.sql.SQLException e)
      {
        logEntry( "storeMerchantData()repId" + md.SalesRepId + "mId" + md.MerchantNumber, e.toString() );
      }
    }
  }

  
  public void processCommission()
  {
    loadData();
    storeCommission();
    processMonthEndBonus();
    processYearEndBonus();
  }
    
  public void updateMerchantInfo( long merchantId )
  {
    int       repId              = 0;
    Date      activationDate     = null;
    ResultSetIterator   it       = null;
    ResultSet           rs       = null;

    if( !isTranscomAccount(merchantId) )
    {
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:1255^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cif.rep_id   as rep_id,
//                    m.date_activated  as activation_date
//            from    merchant  m,
//                    commission_info   cif
//            where   m.merch_number = :merchantId
//                    and cif.merchant_number = m.merch_number
//                    and decode(cif.activation_date,null,'1','0') = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cif.rep_id   as rep_id,\n                  m.date_activated  as activation_date\n          from    merchant  m,\n                  commission_info   cif\n          where   m.merch_number =  :1 \n                  and cif.merchant_number = m.merch_number\n                  and decode(cif.activation_date,null,'1','0') = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.startup.CommissionLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1264^9*/

        rs = it.getResultSet();
        
        while( rs.next() )
        {
          repId               = rs.getInt("rep_id");
          activationDate      = rs.getDate("activation_date");

          /*@lineinfo:generated-code*//*@lineinfo:1273^11*/

//  ************************************************************
//  #sql [Ctx] { update  commission_info
//              set     activation_date = :activationDate,
//                      last_modified_by = 'system',
//                      last_modified_date = sysdate
//              where   rep_id = :repId
//                      and merchant_number = :merchantId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  commission_info\n            set     activation_date =  :1 ,\n                    last_modified_by = 'system',\n                    last_modified_date = sysdate\n            where   rep_id =  :2 \n                    and merchant_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activationDate);
   __sJT_st.setInt(2,repId);
   __sJT_st.setLong(3,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1281^11*/

          /*@lineinfo:generated-code*//*@lineinfo:1283^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1286^11*/
        }
      }
      catch( java.sql.SQLException e )
      {
        logEntry( "updateMerchantInfo( " + merchantId + " )", e.toString() );
      }
      finally
      {
        try { it.close(); } catch( Exception e ) { }
        try { rs.close(); } catch( Exception e ) { }
      }      
    }
  }
  
  public boolean execute()
  {
    int               itemCount         = 0;
    int               processType       = -1;
    long              recId             = 0L;
    long              sequenceId        = 0L;
    long              merchantId        = 0L;
    ResultSet         rs                = null;
    ResultSetIterator it                = null;

    try
    {
      connect(true);

      /*@lineinfo:generated-code*//*@lineinfo:1315^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(load_filename)  
//          from    commission_process
//          where   process_sequence is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(load_filename)   \n        from    commission_process\n        where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.CommissionLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1320^7*/
      
      if ( itemCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1324^9*/

//  ************************************************************
//  #sql [Ctx] { select  commission_process_sequence.nextval 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  commission_process_sequence.nextval  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.CommissionLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequenceId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1328^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:1330^9*/

//  ************************************************************
//  #sql [Ctx] { update  commission_process
//            set     process_sequence = :sequenceId
//            where   process_sequence is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  commission_process\n          set     process_sequence =  :1 \n          where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,sequenceId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1335^9*/
      }        

      if( inTestMode() )
      {
        log.debug("-------- In test mode --------");        
        sequenceId = TestSequenceId;
      }

      if( sequenceId != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1346^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rec_id                        as rec_id,
//                    nvl(merchant_number,0)        as merchant_number,
//                    load_filename                 as load_filename,
//                    process_type                  as process_type
//            from    commission_process 
//            where   process_sequence = :sequenceId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rec_id                        as rec_id,\n                  nvl(merchant_number,0)        as merchant_number,\n                  load_filename                 as load_filename,\n                  process_type                  as process_type\n          from    commission_process \n          where   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.CommissionLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,sequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.startup.CommissionLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1354^9*/
        rs = it.getResultSet();
      
        while( rs.next() )
        {
          merchantId    = rs.getLong("merchant_number");
          LoadFilename  = rs.getString("load_filename");
          processType   = rs.getInt("process_type");
          recId         = rs.getLong("rec_id");
          
          recordTimestampBegin( recId );

          switch( processType )
          {
            case PT_ACTIVATION:
              updateMerchantInfo( merchantId );
              break;
            
            case PT_EXTRACT:
              /*@lineinfo:generated-code*//*@lineinfo:1373^15*/

//  ************************************************************
//  #sql [Ctx] { select   distinct sm.active_date 
//                  from     monthly_extract_summary sm
//                  where    sm.bank_number = substr(:LoadFilename,8,4) 
//                           and sm.active_date >= sysdate - 360  
//                           and sm.load_filename = :LoadFilename
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select   distinct sm.active_date  \n                from     monthly_extract_summary sm\n                where    sm.bank_number = substr( :1 ,8,4) \n                         and sm.active_date >= sysdate - 360  \n                         and sm.load_filename =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.CommissionLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,LoadFilename);
   __sJT_st.setString(2,LoadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ActiveDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1380^15*/ 
              processCommission();
              break;
              
            default:
              continue;
          }

          recordTimestampEnd( recId );
        }        
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return( true );
  }

  public static void main( String[] args )
  {
    CommissionLoader      test   = null;
    
    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone();
      
      test = new CommissionLoader();
      test.connect();  
      
      if ( (args.length == 2) && args[0].equals("process") )
      {
        test.setTestMode(true);
        test.setTestSequenceId(Long.parseLong(args[1]));
        test.execute();
      }
      else if( (args.length == 4) && args[0].equals("processcomm") )
      {
        // args[1] : group Id, -1 for all
        // args[2] : rep Id, -1 for all
        // args[2] : active date mm/dd/yyyy
        
        test.setGroupId( Integer.parseInt(args[1]) );
        test.setRepId( Long.parseLong(args[2]) );        
        test.setActiveDate( DateTimeFormatter.parseSQLDate( args[3].toString(), "MM/dd/yyyy") );
        test.processCommission();
      }
      else if ( (args.length == 3) && args[0].equals("processbonus") )
      {
        // args[1] : group Id, -1 for all
        // args[2] : rep Id, -1 for all
        // args[2] : active date mm/dd/yyyy

        test.setGroupId( Integer.parseInt(args[1]) );
        test.setRepId( Long.parseLong(args[2]) );        
        test.setActiveDate( DateTimeFormatter.parseSQLDate( args[3].toString(), "MM/dd/yyyy") );
        test.processMonthEndBonus();
        test.processYearEndBonus();
      }
      else
      {
        test.execute();
      }
      test.cleanUp();
    }
    catch ( Exception e )
    {
    }
  }
}/*@lineinfo:generated-code*/