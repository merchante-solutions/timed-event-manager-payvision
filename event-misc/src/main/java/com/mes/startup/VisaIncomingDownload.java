/*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/startup/VisaIncomingDownload.java $

  Description:  
  

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-26 22:01:59 -0800 (Wed, 26 Jan 2011) $
  Version            : $Revision: 18342 $

  Change History:
     See VSS database

  Copyright (C) 2009-2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.util.Iterator;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;

public class VisaIncomingDownload 
  extends EventBase
{
  static Logger log = Logger.getLogger(VisaIncomingDownload.class);

  public VisaIncomingDownload()
  {
  }
  
  public boolean execute()
  {
    String    dataFilename  = null;
    String    flagFilename  = null;
    boolean   retVal        = false;
    Sftp      sftp          = null;
    
    
    try
    {
      //get the Sftp connection
      sftp = getSftp( MesDefaults.getString(MesDefaults.DK_VISA_HOST),
                      MesDefaults.getString(MesDefaults.DK_VISA_USER),
                      MesDefaults.getString(MesDefaults.DK_VISA_PASSWORD),
                      MesDefaults.getString(MesDefaults.DK_VISA_INCOMING_PATH),
                      true    // binary
                    );
      
      String filePrefix = (getEventArgCount() == 0 ? "" : getEventArg(0));
      EnumerationIterator enumi = new EnumerationIterator();
      Iterator it = enumi.iterator(sftp.getNameListing(filePrefix + ".*\\.flg"));
      
      while( it.hasNext() )
      {
        flagFilename = (String)it.next();
        dataFilename = flagFilename.substring(0,flagFilename.lastIndexOf(".")) + ".dat";
        
        sftp.download(dataFilename, dataFilename);
        sftp.deleteFile(flagFilename);
        sftp.deleteFile(dataFilename);
      }
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ sftp.disconnect(); } catch( Exception ee ) {}
      
    }
    return( retVal );
  }
  
  public static void main( String[] args )
  {
    VisaIncomingDownload  visa    = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      visa = new VisaIncomingDownload();
      if ( args.length > 0 ) 
      {
        visa.setEventArgs(args[0]);
      }
      visa.execute();
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ visa.cleanUp(); } catch( Exception ee ){}
    }
  }
}
