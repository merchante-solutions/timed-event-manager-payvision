package com.mes.startup;

public class TestEvent extends EventBase
{
 /* static { LoggingConfigurator.requireConfiguration(); }
  static Logger log = Logger.getLogger(TestEvent.class);
  
  public class FileTransferResult {
  
    public String host;
    public String user;
    public String path;
    public String defaultPath;
    public boolean connectOk;
    public boolean pathOk;
    public boolean sendOk;
    public boolean recvOk;
    public boolean cleanupOk;
    
    public String toString() {

      StringBuffer buf = new StringBuffer();

      boolean overallOk = connectOk && sendOk && recvOk && cleanupOk;
      buf.append("Result:       " + (overallOk ? "OK" : "ERROR") + "\n");
      buf.append("Host:         " + host + "\n");
      buf.append("User:         " + user + "\n");
      buf.append("Path:         " + path + "\n");
      buf.append("Start Loc:    " + defaultPath + "\n");
      buf.append("Connect:      " + (connectOk ? "OK" : "ERROR") + "\n");
      buf.append("Path Exists:  " + (pathOk ? "OK" : "ERROR") + "\n");
      buf.append("Send File:    " + (sendOk ? "OK" : "ERROR") + "\n");
      buf.append("Recv File:    " + (recvOk ? "OK" : "ERROR") + "\n");
      buf.append("Cleanup:      " + (cleanupOk ? "OK" : "ERROR"));
      return ""+buf;
    }
  }
  
  public class FileTransferTest {
  
    public String host;
    public String user;
    public String password;
    public String path;
  
    public FileTransferTest(String host, String user, String password, String path) {
      this.host = host;
      this.user = user;
      this.password = password;
      this.path = path;
    }
    
    public FileTransferResult run() {
  
      FileTransferResult result = new FileTransferResult();
      result.host = host;
      result.user = user;
      result.path = path;
    
      try {
      
        SshParameters sshParms = 
          new SshParameters(host,user,password);
        log.debug("sshParms: " + sshParms);

        Sftp sftp = new Sftp(sshParms);
        log.debug("connecting to " + host + " as user " + user + " with password " + password);
        sftp.connect();
        result.connectOk = true;
        log.debug("connected.");
        log.debug("getting starting directory...");
        result.defaultPath = sftp.getDir();
        log.debug("started in " + result.defaultPath);
      
        // move to desired path
        log.debug("changing to path: " + path);
        sftp.setDir(path);
        result.pathOk = true;
      
        // upload test file

        String testContents = "test";
        String remoteFilename = "filetransfer.test";

        log.debug("uploading test file...");
        byte[] testData = testContents.getBytes();
        sftp.upload(testData,remoteFilename);
        result.sendOk = true;
      
        // download test file
        log.debug("downloading test file...");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        sftp.download(baos,remoteFilename);
        log.debug("received contents of test file: '" + baos +  "'");
        result.recvOk = baos.toString().equals(testContents);
      
        // delete the test file
        log.debug("deleting test file from remote server...");
        sftp.deleteFile(remoteFilename);
        result.cleanupOk = true;
      
        sftp.disconnect();
      
        log.debug("test complete.");
      }
      catch (Exception e) {
        log.error(e);
        e.printStackTrace();
      }
    
      return result;
    }
  }
  
  private FileTransferTest[] xferTests = null;
  
  private void initializeXferTests() throws Exception {
  
    xferTests = new FileTransferTest[] {
  
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_SNA_FTP_ADDRESS),
        MesDefaults.getString(MesDefaults.DK_SNA_FTP_USER),
        MesDefaults.getString(MesDefaults.DK_SNA_FTP_PASSWORD),
        "."),
        
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_HOST),
        MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_USER),
        MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_PASSWORD),
        "."),
        
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_UATP_OUTGOING_HOST),
        MesDefaults.getString(MesDefaults.DK_UATP_OUTGOING_USER),
        MesDefaults.getString(MesDefaults.DK_UATP_OUTGOING_PASSWORD),
        MesDefaults.getString(MesDefaults.DK_UATP_OUTGOING_PATH)),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_ARCHIVE_HOST),
        MesDefaults.getString(MesDefaults.DK_ARCHIVE_USER),
        MesDefaults.getString(MesDefaults.DK_ARCHIVE_PASS),
        "."),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_AMEX_OUTGOING_HOST),
        MesDefaults.getString(MesDefaults.DK_AMEX_OUTGOING_USER),
        MesDefaults.getString(MesDefaults.DK_AMEX_OUTGOING_PASSWORD),
        MesDefaults.getString(MesDefaults.DK_AMEX_OUTGOING_PATH)),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_DISC_ME_OUTGOING_HOST),
        MesDefaults.getString(MesDefaults.DK_DISC_ME_OUTGOING_USER),
        MesDefaults.getString(MesDefaults.DK_DISC_ME_OUTGOING_PASSWORD),
        MesDefaults.getString(MesDefaults.DK_DISC_ME_OUTGOING_PATH)),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_TRIDENT_OUTGOING_HOST),
        MesDefaults.getString(MesDefaults.DK_TRIDENT_OUTGOING_USER),
        MesDefaults.getString(MesDefaults.DK_TRIDENT_OUTGOING_PASSWORD),
        "."),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_DISCOVER_OUTGOING_HOST),
        MesDefaults.getString(MesDefaults.DK_DISCOVER_OUTGOING_USER),
        MesDefaults.getString(MesDefaults.DK_DISCOVER_OUTGOING_PASSWORD),
        MesDefaults.getString(MesDefaults.DK_DISCOVER_OUTGOING_PATH)),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_MMS_MES_HOST),
        MesDefaults.getString(MesDefaults.DK_MMS_MES_USER),
        MesDefaults.getString(MesDefaults.DK_MMS_MES_PASSWORD),
        MesDefaults.getString(MesDefaults.DK_MMS_MES_OUTGOING_PATH)),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_TDBT_ADJ_HOST_DEFAULT),
        MesDefaults.getString(MesDefaults.DK_TDBT_ADJ_USER),
        MesDefaults.getString(MesDefaults.DK_TDBT_ADJ_PASSWORD),
        "."),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_DRAP_HOST),
        MesDefaults.getString(MesDefaults.DK_DRAP_USER),
        MesDefaults.getString(MesDefaults.DK_DRAP_PASSWORD),
        MesDefaults.getString(MesDefaults.DK_DRAP_OUTGOING_PATH)),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_STERLING_ACH_HOST),
        MesDefaults.getString(MesDefaults.DK_STERLING_ACH_USER),
        MesDefaults.getString(MesDefaults.DK_STERLING_ACH_PASS),
        MesDefaults.getString(MesDefaults.DK_STERLING_ACH_PATH)),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_VISA_HOST),
        MesDefaults.getString(MesDefaults.DK_VISA_USER),
        MesDefaults.getString(MesDefaults.DK_VISA_PASSWORD),
        "."),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_MC_HOST),
        MesDefaults.getString(MesDefaults.DK_MC_USER),
        MesDefaults.getString(MesDefaults.DK_MC_PASSWORD),
        "."),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_MC_EP_HOST),
        MesDefaults.getString(MesDefaults.DK_MC_EP_USER),
        MesDefaults.getString(MesDefaults.DK_MC_EP_PASSWORD),
        MesDefaults.getString(MesDefaults.DK_MC_EP_OUTGOING_PATH)),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_VISA_EP_HOST),
        MesDefaults.getString(MesDefaults.DK_VISA_EP_USER),
        MesDefaults.getString(MesDefaults.DK_VISA_EP_PASSWORD),
        MesDefaults.getString(MesDefaults.DK_VISA_EP_OUTGOING_PATH)),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_GRS_OUTGOING_HOST),
        MesDefaults.getString(MesDefaults.DK_GRS_OUTGOING_USER),
        MesDefaults.getString(MesDefaults.DK_GRS_OUTGOING_PASSWORD),
        "."),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_FIS_OUTGOING_HOST),
        MesDefaults.getString(MesDefaults.DK_FIS_OUTGOING_USER),
        MesDefaults.getString(MesDefaults.DK_FIS_OUTGOING_PASSWORD),
        "."),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_VERIFI_IMAGE_HOST),
        MesDefaults.getString(MesDefaults.DK_VERIFI_IMAGE_USER),
        MesDefaults.getString(MesDefaults.DK_VERIFI_IMAGE_PASSWORD),
        MesDefaults.getString(MesDefaults.DK_VERIFI_IMAGE_PATH)),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_HOST),
        MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_USER),
        MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_PASSWORD),
        MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_AB_PATH)),
      
      new FileTransferTest(
        MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_HOST),
        MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_USER),
        MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_PASSWORD),
        MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_ACH_PATH))
    };
  }
  
  private List doFileTransferTests() throws Exception {
    
    initializeXferTests();
    List resultList = new ArrayList();
    for (int i = 0; i < xferTests.length; ++i) {
      try {
        resultList.add(xferTests[i].run());
      }
      catch (Exception e) {
        log.error(e);
      }
    }
    
    return resultList;
  }
  
  public boolean execute() {

    try {
    
      List resultList = new ArrayList();
      
      resultList.addAll(doFileTransferTests());
      
      for (Iterator i = resultList.iterator(); i.hasNext();) {
        log.info("\n" + i.next());
      }
      
      return true;
    }
    catch (Exception e) {
      log.error("Error: " + e);
    }
    
    return false;
  }
  
  public static void main(String[] args) {
  
    try {
      SQLJConnectionBase.initStandalone(false);
      (new TestEvent()).execute();
      System.exit(0);
    }
    catch (Exception e) {
      log.error(e);
      System.exit(1);
    }
  }*/
}