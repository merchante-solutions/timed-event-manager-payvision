/*@lineinfo:filename=BMLTransactionProcessor*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TridentBatchExtractor.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-09-19 13:54:03 -0700 (Wed, 19 Sep 2012) $
  Version            : $Revision: 20570 $

  Change History:
     See VSS database

  Copyright (C) 2000-2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import com.mes.tools.ChargebackTools;
import sqlj.runtime.ResultSetIterator;

public class BMLTransactionProcessor extends EventBase
{
  static Logger log = Logger.getLogger(BMLTransactionProcessor.class);

  public static final int   BML_STATUS_NO_MERCHANT  = -1;
  public static final int   BML_STATUS_UNLINKED     = 0;
  public static final int   BML_STATUS_DDF          = 1;
  public static final int   BML_STATUS_ACH          = 2;
  public static final int   BML_STATUS_BILLING      = 3;

  public static final int   BML_TRANS_SALES         = 253;
  public static final int   BML_TRANS_RETURNS       = 255;
  public static final int   BML_TRANS_CB            = 256;
  public static final int   BML_TRANS_REPR          = 280;

  public BMLTransactionProcessor( )
  {
    super();
  }

  private void linkBMLMerchants()
    throws Exception
  {
    ResultSetIterator it              = null;
    ResultSet         rs              = null;
    Vector            recs            = new Vector();
    long              merchantNumber  = -1L;

    try
    {
      log.debug("Linking BML Merchant Numbers");

      log.debug("getting records to link");
      /*@lineinfo:generated-code*//*@lineinfo:73^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rec_id,
//                  bml_merchant_number
//          from    bml_rec_detail
//          where   transaction_status = :BML_STATUS_UNLINKED
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rec_id,\n                bml_merchant_number\n        from    bml_rec_detail\n        where   transaction_status =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,BML_STATUS_UNLINKED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.BMLTransactionProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:79^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        recs.add( new LinkRecord(rs.getLong("rec_id"), rs.getLong("bml_merchant_number")) );
      }

      rs.close();
      it.close();

      // process items in vector
      log.debug("linking records");
      for(int i=0; i<recs.size(); ++i)
      {
        LinkRecord rec = (LinkRecord)(recs.elementAt(i));

        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:99^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct tp.merchant_number as merchant_number
//              from    trident_profile_api tpa,
//                      trident_profile tp
//              where   (
//                        tpa.bml_merch_id_1 = :rec.bmlMerchantNumber
//                        or tpa.bml_merch_id_2 = :rec.bmlMerchantNumber
//                        or tpa.bml_merch_id_3 = :rec.bmlMerchantNumber
//                        or tpa.bml_merch_id_4 = :rec.bmlMerchantNumber
//                        or tpa.bml_merch_id_5 = :rec.bmlMerchantNumber
//                      )
//                      and tpa.terminal_id = tp.terminal_id
//              order by tp.merchant_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct tp.merchant_number as merchant_number\n            from    trident_profile_api tpa,\n                    trident_profile tp\n            where   (\n                      tpa.bml_merch_id_1 =  :1 \n                      or tpa.bml_merch_id_2 =  :2 \n                      or tpa.bml_merch_id_3 =  :3 \n                      or tpa.bml_merch_id_4 =  :4 \n                      or tpa.bml_merch_id_5 =  :5 \n                    )\n                    and tpa.terminal_id = tp.terminal_id\n            order by tp.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,rec.bmlMerchantNumber);
   __sJT_st.setLong(2,rec.bmlMerchantNumber);
   __sJT_st.setLong(3,rec.bmlMerchantNumber);
   __sJT_st.setLong(4,rec.bmlMerchantNumber);
   __sJT_st.setLong(5,rec.bmlMerchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.BMLTransactionProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^11*/
        }
        catch(Exception e)
        {
          merchantNumber = -1L;
        }

        rs = it.getResultSet();

        if(rs.next())
        {
          merchantNumber = rs.getLong("merchant_number");
        }

        rs.close();
        it.close();

        // update merchant number and status
        /*@lineinfo:generated-code*//*@lineinfo:131^9*/

//  ************************************************************
//  #sql [Ctx] { update  bml_rec_detail
//            set     merchant_number = :merchantNumber,
//                    transaction_status = decode(:merchantNumber,
//                                                -1, -1,
//                                                :BML_STATUS_DDF)
//            where   rec_id = :rec.recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  bml_rec_detail\n          set     merchant_number =  :1 ,\n                  transaction_status = decode( :2 ,\n                                              -1, -1,\n                                               :3 )\n          where   rec_id =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setLong(2,merchantNumber);
   __sJT_st.setInt(3,BML_STATUS_DDF);
   __sJT_st.setLong(4,rec.recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:139^9*/
      }

      log.debug("done linking bml merchant records");
    }
    catch(Exception e)
    {
      logEntry("linkBMLMerchants()", e.toString());
      throw( e );
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  private void createDDFEntries()
    throws Exception
  {
    log.debug("in createDDFEntries");

    ResultSetIterator it              = null;
    ResultSet         rs              = null;
    ArrayList         ddfRecs         = new ArrayList();
    ArrayList         sumPros         = new ArrayList();

    log.debug("pulling ddf records to load");
    
    // process ddf records for reporting purposes
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:171^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rd.rec_id               rec_id,
//                  rd.load_file_id         load_file_id,
//                  rd.merchant_number      merchant_number,
//                  rd.card_number          card_number,
//                  rd.load_filename        load_filename,
//                  rd.transaction_id       transaction_id,
//                  rd.transaction_date     transaction_date,
//                  rd.transaction_amount   transaction_amount,
//                  rd.transaction_code     transaction_code,
//                  rd.batch_date           batch_date,
//                  bac.bml_order_id        bml_order_id,
//                  baa.auth_code           auth_code,
//                  baa.promo_code          promo_code,
//                  bac.tran_id             trident_tran_id
//          from    bml_rec_detail rd,
//                  bml_api_auths baa,
//                  bml_api_captures bac
//          where   rd.transaction_status = :BML_STATUS_DDF
//                  and rd.transaction_code in ((:BML_TRANS_SALES), :BML_TRANS_RETURNS)
//                  and rd.transaction_id = bac.recon_id(+)
//                  and bac.auth_tran_id = baa.tran_id(+)
//          order by rd.rec_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rd.rec_id               rec_id,\n                rd.load_file_id         load_file_id,\n                rd.merchant_number      merchant_number,\n                rd.card_number          card_number,\n                rd.load_filename        load_filename,\n                rd.transaction_id       transaction_id,\n                rd.transaction_date     transaction_date,\n                rd.transaction_amount   transaction_amount,\n                rd.transaction_code     transaction_code,\n                rd.batch_date           batch_date,\n                bac.bml_order_id        bml_order_id,\n                baa.auth_code           auth_code,\n                baa.promo_code          promo_code,\n                bac.tran_id             trident_tran_id\n        from    bml_rec_detail rd,\n                bml_api_auths baa,\n                bml_api_captures bac\n        where   rd.transaction_status =  :1 \n                and rd.transaction_code in (( :2 ),  :3 )\n                and rd.transaction_id = bac.recon_id(+)\n                and bac.auth_tran_id = baa.tran_id(+)\n        order by rd.rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,BML_STATUS_DDF);
   __sJT_st.setInt(2,BML_TRANS_SALES);
   __sJT_st.setInt(3,BML_TRANS_RETURNS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.BMLTransactionProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:195^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        ddfRecs.add( new DDFRecord( rs ));
      }
      
      rs.close();
      it.close();

      // process items in vector
      DDFRecord rec;

      for(int i=0; i<ddfRecs.size(); ++i)
      {
        rec = (DDFRecord)(ddfRecs.get(i));

        //only insert sales and returns
        String decodeTransType = (rec.transType == BML_TRANS_SALES? "D" : "C");

        /*@lineinfo:generated-code*//*@lineinfo:217^9*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_dt
//            (
//              ddf_dt_id,
//              reference_number,
//              batch_number,
//              batch_date,
//              merchant_account_number,
//              cardholder_account_number,
//              transaction_date,
//              transaction_amount,
//              card_type,
//              load_filename,
//              pos_entry_mode,
//              net_deposit,
//              debit_credit_indicator,
//              purchase_id,
//              authorization_num,
//              trident_tran_id
//            )
//            values
//            (
//              :rec.recId,
//              :rec.transId,
//              :rec.loadFileId,
//              :rec.batchDate,
//              :rec.merchantNumber,
//              :rec.cardNum,
//              :rec.transDate,
//              :rec.transAmount,
//              'BL',
//              :rec.loadFilename,
//              '01', --keyed
//              -1,
//              :decodeTransType,
//              :rec.orderId,
//              :rec.authCode,
//              :rec.tridentTranId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into daily_detail_file_dt\n          (\n            ddf_dt_id,\n            reference_number,\n            batch_number,\n            batch_date,\n            merchant_account_number,\n            cardholder_account_number,\n            transaction_date,\n            transaction_amount,\n            card_type,\n            load_filename,\n            pos_entry_mode,\n            net_deposit,\n            debit_credit_indicator,\n            purchase_id,\n            authorization_num,\n            trident_tran_id\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n            'BL',\n             :9 ,\n            '01', --keyed\n            -1,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,rec.recId);
   __sJT_st.setString(2,rec.transId);
   __sJT_st.setLong(3,rec.loadFileId);
   __sJT_st.setDate(4,rec.batchDate);
   __sJT_st.setLong(5,rec.merchantNumber);
   __sJT_st.setString(6,rec.cardNum);
   __sJT_st.setDate(7,rec.transDate);
   __sJT_st.setDouble(8,rec.transAmount);
   __sJT_st.setString(9,rec.loadFilename);
   __sJT_st.setString(10,decodeTransType);
   __sJT_st.setString(11,rec.orderId);
   __sJT_st.setString(12,rec.authCode);
   __sJT_st.setString(13,rec.tridentTranId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:257^9*/

        //add in the unique load_filenames to submit to summarizer_process
        if( !sumPros.contains(rec.loadFilename) )
        {
          sumPros.add(rec.loadFilename);
        }

        // update merchant number and status regardless of
        // daily detail insert
        /*@lineinfo:generated-code*//*@lineinfo:267^9*/

//  ************************************************************
//  #sql [Ctx] { update  bml_rec_detail
//            set     transaction_status = :BML_STATUS_ACH
//            where   rec_id = :rec.recId
//                    and transaction_status = :BML_STATUS_DDF
//                    and transaction_code in (:BML_TRANS_SALES, :BML_TRANS_RETURNS)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  bml_rec_detail\n          set     transaction_status =  :1 \n          where   rec_id =  :2 \n                  and transaction_status =  :3 \n                  and transaction_code in ( :4 ,  :5 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,BML_STATUS_ACH);
   __sJT_st.setLong(2,rec.recId);
   __sJT_st.setInt(3,BML_STATUS_DDF);
   __sJT_st.setInt(4,BML_TRANS_SALES);
   __sJT_st.setInt(5,BML_TRANS_RETURNS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:274^9*/
      }
      
      //insert appropriate load_filenames into summarizer_process
      String name = "";
      log.debug("adding filenames to summarizer_process");
      for(Iterator iter = sumPros.iterator(); iter.hasNext();)
      {
        name = (String)iter.next();
        log.debug("inserting "+name+" into summarizer_process");

        /*@lineinfo:generated-code*//*@lineinfo:285^9*/

//  ************************************************************
//  #sql [Ctx] { insert into summarizer_process
//            (file_type, load_filename)
//            values
//            (1, :name)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into summarizer_process\n          (file_type, load_filename)\n          values\n          (1,  :1 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,name);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:291^9*/
      }
      
      log.debug("done inserting DDF records");
    }
    catch(Exception e)
    {
      logEntry("createDDFEntries()", e.toString());
      throw( e );
    }
  }
  
  private void createChargebackEntries()
    throws Exception
  {
    ResultSetIterator it              = null;
    ResultSet         rs              = null;
    ArrayList         cbRecs          = new ArrayList();

    log.debug("pulling cb records to load");
    
    boolean autoCommit = getAutoCommit();
    
    setAutoCommit(false);
    
    // process ddf records for reporting purposes
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:319^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(brd.chargeback_case_id, brd.rec_id) case_number,
//                  brd.merchant_number                     merchant_number,
//                  mf.bank_number                          bank_number,
//                  brd.batch_date                          incoming_date,
//                  brd.rec_id                              reference_number,
//                  brd.card_number                         card_number,
//                  brd.transaction_date                    tran_date,
//                  brd.transaction_amount                  tran_amount,
//                  mf.dba_name                             merchant_name,
//                  brd.chargeback_reason_code              reason_code,
//                  brd.load_filename                       load_filename,
//                  decode( (brd.transaction_amount/abs(brd.transaction_amount)),
//                    -1, 'C',
//                    'D')                                  debit_credit_ind,
//                  brd.card_number_enc                     card_number_enc
//          from    bml_rec_detail brd,
//                  mif mf
//          where   brd.transaction_status = :BML_STATUS_DDF
//                  and brd.transaction_code in (:BML_TRANS_CB, :BML_TRANS_REPR)
//                  and brd.merchant_number = mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(brd.chargeback_case_id, brd.rec_id) case_number,\n                brd.merchant_number                     merchant_number,\n                mf.bank_number                          bank_number,\n                brd.batch_date                          incoming_date,\n                brd.rec_id                              reference_number,\n                brd.card_number                         card_number,\n                brd.transaction_date                    tran_date,\n                brd.transaction_amount                  tran_amount,\n                mf.dba_name                             merchant_name,\n                brd.chargeback_reason_code              reason_code,\n                brd.load_filename                       load_filename,\n                decode( (brd.transaction_amount/abs(brd.transaction_amount)),\n                  -1, 'C',\n                  'D')                                  debit_credit_ind,\n                brd.card_number_enc                     card_number_enc\n        from    bml_rec_detail brd,\n                mif mf\n        where   brd.transaction_status =  :1 \n                and brd.transaction_code in ( :2 ,  :3 )\n                and brd.merchant_number = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,BML_STATUS_DDF);
   __sJT_st.setInt(2,BML_TRANS_CB);
   __sJT_st.setInt(3,BML_TRANS_REPR);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.BMLTransactionProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:341^7*/
      rs = it.getResultSet();

      while(rs.next())
      {
        cbRecs.add( new CBRecord( rs ));
      }
      
      rs.close();
      it.close();

      // process items in vector
      CBRecord rec;

      long  newLoadSec = 0L;
      
      for(int i=0; i < cbRecs.size(); ++i)
      {
        rec = (CBRecord)(cbRecs.get(i));
        
        // get a cb_load_sec
        newLoadSec = ChargebackTools.getNewLoadSec();
        
        // add to network_chargebacks
        /*@lineinfo:generated-code*//*@lineinfo:365^9*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargebacks
//            (
//              cb_load_sec,
//              cb_ref_num,
//              merchant_number,
//              bank_number,
//              incoming_date,
//              reference_number,
//              card_number,
//              card_type,
//              tran_date,
//              tran_amount,
//              merchant_name,
//              reason_code,
//              load_filename,
//              first_time_chargeback,
//              debit_credit_ind,
//              tran_date_missing,
//              card_number_enc
//            )
//            values
//            (
//              :newLoadSec,
//              :rec.caseNumber,
//              :rec.merchantNumber,
//              :rec.bankNumber,
//              :rec.incomingDate,
//              :rec.referenceNumber,
//              :rec.cardNumber,
//              'BL',
//              :rec.tranDate,
//              :rec.tranAmount,
//              :rec.merchantName,
//              :rec.reasonCode,
//              :rec.loadFilename,
//              'Y',
//              :rec.debitCreditInd,
//              'N',
//              :rec.cardNumberEnc
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_chargebacks\n          (\n            cb_load_sec,\n            cb_ref_num,\n            merchant_number,\n            bank_number,\n            incoming_date,\n            reference_number,\n            card_number,\n            card_type,\n            tran_date,\n            tran_amount,\n            merchant_name,\n            reason_code,\n            load_filename,\n            first_time_chargeback,\n            debit_credit_ind,\n            tran_date_missing,\n            card_number_enc\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n            'BL',\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n            'Y',\n             :13 ,\n            'N',\n             :14 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,newLoadSec);
   __sJT_st.setString(2,rec.caseNumber);
   __sJT_st.setLong(3,rec.merchantNumber);
   __sJT_st.setInt(4,rec.bankNumber);
   __sJT_st.setDate(5,rec.incomingDate);
   __sJT_st.setString(6,rec.referenceNumber);
   __sJT_st.setString(7,rec.cardNumber);
   __sJT_st.setDate(8,rec.tranDate);
   __sJT_st.setDouble(9,rec.tranAmount);
   __sJT_st.setString(10,rec.merchantName);
   __sJT_st.setString(11,rec.reasonCode);
   __sJT_st.setString(12,rec.loadFilename);
   __sJT_st.setString(13,rec.debitCreditInd);
   __sJT_st.setString(14,rec.cardNumberEnc);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:407^9*/
        
        // network_chargeback_activity
        log.debug("adding to network_chargeback_activity");
        /*@lineinfo:generated-code*//*@lineinfo:411^9*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_activity
//            (
//              cb_load_sec,
//              file_load_sec,
//              action_code,
//              action_date,
//              user_message,
//              action_source,
//              user_login
//            )
//            values
//            (
//              :newLoadSec,
//              1,
//              'D',
//              trunc(sysdate),
//              'Auto Action - BML',
//              9,
//              'system'
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_chargeback_activity\n          (\n            cb_load_sec,\n            file_load_sec,\n            action_code,\n            action_date,\n            user_message,\n            action_source,\n            user_login\n          )\n          values\n          (\n             :1 ,\n            1,\n            'D',\n            trunc(sysdate),\n            'Auto Action - BML',\n            9,\n            'system'\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,newLoadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:433^9*/
        
        // chargeback queue
        log.debug("adding to chargeback queue");
        /*@lineinfo:generated-code*//*@lineinfo:437^9*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//            (
//              id,
//              type,
//              item_type,
//              owner,
//              date_created,
//              source,
//              affiliate,
//              last_changed,
//              last_user
//            )
//            values
//            (
//              :newLoadSec,
//              :MesQueues.Q_CHARGEBACKS_MCHB,
//              18,
//              1,
//              :rec.incomingDate,
//              'bml',
//              :rec.bankNumber,
//              sysdate,
//              'system'
//            )          
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_data\n          (\n            id,\n            type,\n            item_type,\n            owner,\n            date_created,\n            source,\n            affiliate,\n            last_changed,\n            last_user\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n            18,\n            1,\n             :3 ,\n            'bml',\n             :4 ,\n            sysdate,\n            'system'\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,newLoadSec);
   __sJT_st.setInt(2,MesQueues.Q_CHARGEBACKS_MCHB);
   __sJT_st.setDate(3,rec.incomingDate);
   __sJT_st.setInt(4,rec.bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:463^9*/
      }
      
      // update records to show new status
      log.debug("updating tran status");
      /*@lineinfo:generated-code*//*@lineinfo:468^7*/

//  ************************************************************
//  #sql [Ctx] { update  bml_rec_detail
//          set     transaction_status = :BML_STATUS_ACH
//          where   transaction_status = :BML_STATUS_DDF
//                  and transaction_code in (:BML_TRANS_CB, :BML_TRANS_REPR)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  bml_rec_detail\n        set     transaction_status =  :1 \n        where   transaction_status =  :2 \n                and transaction_code in ( :3 ,  :4 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.BMLTransactionProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,BML_STATUS_ACH);
   __sJT_st.setInt(2,BML_STATUS_DDF);
   __sJT_st.setInt(3,BML_TRANS_CB);
   __sJT_st.setInt(4,BML_TRANS_REPR);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:474^7*/
    }
    catch(Exception e)
    {
      rollback();
      logEntry("createChargebackEntries()", e.toString());
      throw( e );
    }
    finally
    {
      setAutoCommit(autoCommit);
    }
  }

  public void processTransactions(int status)
    throws Exception
  {
    int recCount  = 0;

    try
    {
      connect();
      setAutoCommit(false);

      /*@lineinfo:generated-code*//*@lineinfo:498^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    bml_rec_detail
//          where   transaction_status = :status
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    bml_rec_detail\n        where   transaction_status =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.BMLTransactionProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,status);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:504^7*/

      log.debug("after count");

      if( recCount > 0 )
      {
        switch(status)
        {
          case BML_STATUS_UNLINKED:
            linkBMLMerchants();
            break;

          case BML_STATUS_DDF:
            createDDFEntries();
            createChargebackEntries();
            break;

          case BML_STATUS_ACH:
            break;

          case BML_STATUS_BILLING:
            break;

          default:
            break;
        }
      }

      commit();
    }
    catch(Exception e)
    {
      logEntry("processTransactions(" + status + ")", e.toString());
      rollback();
      throw( e );
    }
    finally
    {
      cleanUp();
    }
  }

  public boolean execute()
  {
    boolean             result        = false;

    try
    {
      processTransactions( Integer.parseInt(getEventArg(0)) );

      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }

    return( result );
  }

  public static void main( String[] args )
  {
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");

      BMLTransactionProcessor grunt = new BMLTransactionProcessor();

      grunt.processTransactions(Integer.parseInt(args[0]));
    }
    catch(Exception e)
    {
      log.error("main(): " + e.toString());
    }
  }

  public class LinkRecord
  {
    public long recId = 0L;
    public long bmlMerchantNumber = 0L;

    public LinkRecord(long recId, long bmlMerchantNumber)
    {
      this.recId = recId;
      this.bmlMerchantNumber = bmlMerchantNumber;
    }
  }

  public class DDFRecord
  {
    public long recId                   = 0L;
    public long loadFileId              = 0L;
    public long merchantNumber          = 0L;
    public String cardNum               = "";
    public String loadFilename          = "";
    public int transType                 = 0;
    public java.sql.Date transDate      = null;
    public java.sql.Date batchDate      = null;
    public double transAmount           = 0.0d;
    public String transId               = "";
    public String orderId               = "";
    public String authCode              = "";
    public String promoCode             = "";
    public String tridentTranId         = "";

    public DDFRecord( ResultSet rs )
    {
      try
      {
        recId           = rs.getLong("rec_id");
        loadFileId      = rs.getLong("load_file_id");
        merchantNumber  = rs.getLong("merchant_number");
        cardNum         = rs.getString("card_number");
        loadFilename    = rs.getString("load_filename");
        transDate       = rs.getDate("transaction_date");
        batchDate       = rs.getDate("batch_date");
        transAmount     = rs.getDouble("transaction_amount");
        transType       = rs.getInt("transaction_code");
        transId         = rs.getString("transaction_id");
        orderId         = rs.getString("bml_order_id");
        authCode        = rs.getString("auth_code");
        promoCode       = rs.getString("promo_code");
        tridentTranId   = rs.getString("trident_tran_id");
      }
      catch(Exception e)
      {
        logEntry("DDFRecord()", e.toString());
      }
    }
  }
  
  public class CBRecord
  {
    public String   caseNumber;
    public long     merchantNumber;
    public int      bankNumber;
    public Date     incomingDate;
    public String   referenceNumber;
    public String   cardNumber;
    public Date     tranDate;
    public double   tranAmount;
    public String   merchantName;
    public String   reasonCode;
    public String   loadFilename;
    public String   debitCreditInd;
    public String   cardNumberEnc;
    
    public CBRecord( ResultSet rs )
    {
      try
      {
        caseNumber = rs.getString("case_number");
        merchantNumber = rs.getLong("merchant_number");
        bankNumber    = rs.getInt("bank_number");
        incomingDate = rs.getDate("incoming_date");
        referenceNumber = rs.getString("reference_number");
        cardNumber = rs.getString("card_number");
        tranDate = rs.getDate("tran_date");
        tranAmount = rs.getDouble("tran_amount");
        merchantName = rs.getString("merchant_name");
        reasonCode = rs.getString("reason_code");
        loadFilename = rs.getString("load_filename");
        debitCreditInd = rs.getString("debit_credit_ind");
        cardNumberEnc = rs.getString("card_number_enc");
      }
      catch(Exception e)
      {
        logEntry("CBRecord()", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/