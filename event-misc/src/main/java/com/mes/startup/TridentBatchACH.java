/*@lineinfo:filename=TridentBatchACH*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TridentBatchACH.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-09-19 13:54:03 -0700 (Wed, 19 Sep 2012) $
  Version            : $Revision: 20570 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.ach.AchDb;
import com.mes.ach.AchEntryData;
import com.mes.ach.AchStatementRecord;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class TridentBatchACH extends EventBase
{
  static Logger log = Logger.getLogger(TridentBatchACH.class);
  
  public static final int   TASK_CREATE_ACH_STATEMENT = 0;
  public static final int   TASK_CREATE_ACH_FILE      = 1;
  
  // ACH data sources
  public static final int   DS_VISAK                  = 0;
  public static final int   DS_PG                     = 1;
  
  public TridentBatchACH( )
  {
    super();
  }
  
  protected boolean nodeEnabled( long merchantNumber, String descriptor )
  {
    boolean             result  = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:63^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ato.origin_node       as origin_node,
//                  upper(ato.ach_enabled)    as enabled
//          from    mif mf,
//                  t_hierarchy th,
//                  ach_trident_origin ato
//          where   mf.merchant_number = :merchantNumber
//                  and mf.association_node = th.descendent
//                  and th.ancestor = ato.origin_node
//                  and ato.origin_descriptor = :descriptor
//          order by th.relation
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ato.origin_node       as origin_node,\n                upper(ato.ach_enabled)    as enabled\n        from    mif mf,\n                t_hierarchy th,\n                ach_trident_origin ato\n        where   mf.merchant_number =  :1 \n                and mf.association_node = th.descendent\n                and th.ancestor = ato.origin_node\n                and ato.origin_descriptor =  :2 \n        order by th.relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TridentBatchACH",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setString(2,descriptor);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.TridentBatchACH",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:75^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        if( "Y".equals(rs.getString("enabled")) )
        {
          result = true;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("nodeEnabled(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    
    return( result );
  }
  
  protected void extractAchTridentStatementRecords(int dataSource, long processSequence, String testString)
    throws Exception
  {
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    Vector            statements    = new Vector();
  
    try
    {
      // retrieve records
      log.debug("pulling details for ach_trident_statement");
      
      if ( dataSource == DS_VISAK )   // visa-k batches
      {
        /*@lineinfo:generated-code*//*@lineinfo:114^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index (tct idx_trident_cap_totals_pseq)
//                      index (tc idx_trident_cap_recid_rsp_code) 
//                      index (tp pk_trident_profile)
//                      ordered
//                      use_nl(tct tc)          
//                    */
//                    tc.rec_id                                   as rec_id,
//                    tp.merchant_number                          as merchant_number,
//                    (trunc(sysdate) + nvl(mf.suspended_days,0)) as post_date,
//                    :AchEntryData.ED_MERCH_DEP                as entry_desc,
//                    sum(decode(tct.card_type,
//                               'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_count,
//                               'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_count,
//                               'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_count,
//                               'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_count,
//                               'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_count,
//                               'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tct.sales_count,
//                               'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tct.sales_count,
//                                0 )
//                       )       sales_count,
//                    sum(decode(tct.card_type,
//                               'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_amount,
//                               'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_amount,
//                               'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_amount,
//                               'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_amount,
//                               'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_amount,
//                               'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tct.sales_amount,
//                               'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tct.sales_amount,
//                                0 )
//                       )       sales_amount,
//                    sum(decode(tct.card_type,
//                               'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_count,
//                               'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_count,
//                               'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_count,
//                               'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_count,
//                               'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_count,
//                               'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tct.credits_count,
//                               'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tct.credits_count,
//                                0 )
//                       )       credits_count,
//                    sum(decode(tct.card_type,
//                               'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_amount,
//                               'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_amount,
//                               'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_amount,
//                               'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_amount,
//                               'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_amount,
//                               'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tct.credits_amount,
//                               'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tct.credits_amount,
//                                0 )
//                       )       credits_amount,
//                    sum(decode(tct.card_type,
//                               'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,
//                               'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,
//                               'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,
//                               'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,
//                               'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,
//                               'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tct.net_amount,
//                               'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tct.net_amount,
//                                0 )
//                       )       net_amount
//            from    trident_capture_totals  tct,
//                    trident_capture         tc,
//                    trident_profile         tp,
//                    mif                     mf
//            where   tct.process_sequence = :processSequence
//                    and tct.rec_id = tc.rec_id
//                    and nvl(tc.currency_code,'840') = '840'
//                    and tc.response_code = 0
//                    and tc.profile_id = tp.terminal_id
//                    and tp.merchant_number = mf.merchant_number
//                    and mf.bank_number = 3942
//            group by tc.rec_id, tp.merchant_number, (trunc(sysdate)+nvl(mf.suspended_days,0))
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    index (tct idx_trident_cap_totals_pseq)\n                    index (tc idx_trident_cap_recid_rsp_code) \n                    index (tp pk_trident_profile)\n                    ordered\n                    use_nl(tct tc)          \n                  */\n                  tc.rec_id                                   as rec_id,\n                  tp.merchant_number                          as merchant_number,\n                  (trunc(sysdate) + nvl(mf.suspended_days,0)) as post_date,\n                   :1                 as entry_desc,\n                  sum(decode(tct.card_type,\n                             'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_count,\n                             'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_count,\n                             'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_count,\n                             'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_count,\n                             'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_count,\n                             'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tct.sales_count,\n                             'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tct.sales_count,\n                              0 )\n                     )       sales_count,\n                  sum(decode(tct.card_type,\n                             'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_amount,\n                             'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_amount,\n                             'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_amount,\n                             'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_amount,\n                             'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tct.sales_amount,\n                             'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tct.sales_amount,\n                             'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tct.sales_amount,\n                              0 )\n                     )       sales_amount,\n                  sum(decode(tct.card_type,\n                             'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_count,\n                             'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_count,\n                             'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_count,\n                             'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_count,\n                             'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_count,\n                             'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tct.credits_count,\n                             'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tct.credits_count,\n                              0 )\n                     )       credits_count,\n                  sum(decode(tct.card_type,\n                             'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_amount,\n                             'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_amount,\n                             'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_amount,\n                             'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_amount,\n                             'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tct.credits_amount,\n                             'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tct.credits_amount,\n                             'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tct.credits_amount,\n                              0 )\n                     )       credits_amount,\n                  sum(decode(tct.card_type,\n                             'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,\n                             'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,\n                             'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,\n                             'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,\n                             'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,\n                             'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tct.net_amount,\n                             'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tct.net_amount,\n                              0 )\n                     )       net_amount\n          from    trident_capture_totals  tct,\n                  trident_capture         tc,\n                  trident_profile         tp,\n                  mif                     mf\n          where   tct.process_sequence =  :2 \n                  and tct.rec_id = tc.rec_id\n                  and nvl(tc.currency_code,'840') = '840'\n                  and tc.response_code = 0\n                  and tc.profile_id = tp.terminal_id\n                  and tp.merchant_number = mf.merchant_number\n                  and mf.bank_number = 3942\n          group by tc.rec_id, tp.merchant_number, (trunc(sysdate)+nvl(mf.suspended_days,0))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TridentBatchACH",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,AchEntryData.ED_MERCH_DEP);
   __sJT_st.setLong(2,processSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.TridentBatchACH",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:189^9*/
      }
      else if ( dataSource == DS_PG )
      {
        /*@lineinfo:generated-code*//*@lineinfo:193^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ordered */
//                    tapi.batch_id                               as rec_id,
//                    tapi.merchant_number                        as merchant_number,
//                    (trunc(sysdate) + nvl(mf.suspended_days,0)) as post_date,
//                    :AchEntryData.ED_MERCH_DEP                as entry_desc,
//                    sum(decode(tapi.card_type,
//                               'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0),
//                               'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),
//                               'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0),
//                               'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),
//                               'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0),
//                               'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0),
//                               'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0),
//                               'BL', 1,
//                                0 ) *
//                        decode(tapi.debit_credit_indicator,'D',1,0)
//                       )       sales_count,
//                    sum(decode(tapi.card_type,
//                               'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'BL', tapi.transaction_amount,
//                                0 ) *
//                        decode(tapi.debit_credit_indicator,'D',1,0)
//                       )       sales_amount,
//                    sum(decode(tapi.card_type,
//                               'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0),
//                               'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),
//                               'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0),
//                               'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),
//                               'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0),
//                               'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0),
//                               'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0),
//                               'BL', 1,
//                                0 ) *
//                        decode(tapi.debit_credit_indicator,'C',1,0)
//                       )       credits_count,
//                    sum(decode(tapi.card_type,
//                               'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'BL', tapi.transaction_amount,
//                                0 ) *
//                        decode(tapi.debit_credit_indicator,'C',1,0)
//                       )       credits_amount,
//                    sum(decode(tapi.card_type,
//                               'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,
//                               'BL', tapi.transaction_amount,
//                                0 ) * 
//                        decode(tapi.debit_credit_indicator,'C',-1,1)
//                       )       net_amount
//            from    trident_capture_api     tapi,
//                    mif                     mf
//            where   tapi.ach_sequence = :processSequence
//                    and nvl(tapi.currency_code,'840') = '840'
//                    and tapi.test_flag = 'N'
//                    and mf.merchant_number = tapi.merchant_number
//                    and mf.bank_number = 3942
//            group by tapi.batch_id, tapi.merchant_number, (trunc(sysdate)+nvl(mf.suspended_days,0))
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ordered */\n                  tapi.batch_id                               as rec_id,\n                  tapi.merchant_number                        as merchant_number,\n                  (trunc(sysdate) + nvl(mf.suspended_days,0)) as post_date,\n                   :1                 as entry_desc,\n                  sum(decode(tapi.card_type,\n                             'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0),\n                             'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),\n                             'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0),\n                             'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),\n                             'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0),\n                             'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0),\n                             'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0),\n                             'BL', 1,\n                              0 ) *\n                      decode(tapi.debit_credit_indicator,'D',1,0)\n                     )       sales_count,\n                  sum(decode(tapi.card_type,\n                             'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'BL', tapi.transaction_amount,\n                              0 ) *\n                      decode(tapi.debit_credit_indicator,'D',1,0)\n                     )       sales_amount,\n                  sum(decode(tapi.card_type,\n                             'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0),\n                             'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),\n                             'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0),\n                             'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0),\n                             'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0),\n                             'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0),\n                             'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0),\n                             'BL', 1,\n                              0 ) *\n                      decode(tapi.debit_credit_indicator,'C',1,0)\n                     )       credits_count,\n                  sum(decode(tapi.card_type,\n                             'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'BL', tapi.transaction_amount,\n                              0 ) *\n                      decode(tapi.debit_credit_indicator,'C',1,0)\n                     )       credits_amount,\n                  sum(decode(tapi.card_type,\n                             'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tapi.transaction_amount,\n                             'BL', tapi.transaction_amount,\n                              0 ) * \n                      decode(tapi.debit_credit_indicator,'C',-1,1)\n                     )       net_amount\n          from    trident_capture_api     tapi,\n                  mif                     mf\n          where   tapi.ach_sequence =  :2 \n                  and nvl(tapi.currency_code,'840') = '840'\n                  and tapi.test_flag = 'N'\n                  and mf.merchant_number = tapi.merchant_number\n                  and mf.bank_number = 3942\n          group by tapi.batch_id, tapi.merchant_number, (trunc(sysdate)+nvl(mf.suspended_days,0))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.TridentBatchACH",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,AchEntryData.ED_MERCH_DEP);
   __sJT_st.setLong(2,processSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.TridentBatchACH",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:268^9*/
      }
      
      if ( it != null )
      {
        rs = it.getResultSet();
      
        while( rs.next() )
        {
          statements.add(new AchStatementRecord(rs));
        }
        rs.close();
        it.close();
      
        String tableName = null;
        if( "PROD".equals(testString) )
        {
          tableName = "ach_trident_statement";
        }
        else
        {
          tableName = "ach_trident_statement_test";
        }
        for(int i=0; i<statements.size(); ++i)
        {
          AchStatementRecord rec = (AchStatementRecord)(statements.elementAt(i));
        
          if((rec.getSalesCount() > 0 || rec.getCreditsCount() > 0) && nodeEnabled( rec.getMerchantId(), AchEntryData.ED_MERCH_DEP ) )
          {
            // add record to database
            log.debug("Storing statement record: " + rec.getMerchantId() + ", " + rec.getPostDate() + ", " + rec.getNetAmount());
            if( AchDb.storeAchStatementRecord(rec,tableName) == false )
            {
              // this statement record was not stored successfully, need to un-mark in process table
              if ( dataSource == DS_VISAK )
              {
                /*@lineinfo:generated-code*//*@lineinfo:304^17*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture_totals
//                    set     process_sequence = 0
//                    where   rec_id = :rec.getRecId()
//                            and process_sequence = :processSequence
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4877 = rec.getRecId();
   String theSqlTS = "update  trident_capture_totals\n                  set     process_sequence = 0\n                  where   rec_id =  :1 \n                          and process_sequence =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.TridentBatchACH",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4877);
   __sJT_st.setLong(2,processSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:310^17*/
                commit();
              }
              else if ( dataSource == DS_PG )
              {
                /*@lineinfo:generated-code*//*@lineinfo:315^17*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture_api   tapi
//                    set     ach_sequence = 0
//                    where   tapi.batch_id = :rec.getRecId()
//                            and tapi.ach_sequence = :processSequence
//                            and tapi.mesdb_timestamp >= (sysdate-30)
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4878 = rec.getRecId();
   String theSqlTS = "update  trident_capture_api   tapi\n                  set     ach_sequence = 0\n                  where   tapi.batch_id =  :1 \n                          and tapi.ach_sequence =  :2 \n                          and tapi.mesdb_timestamp >= (sysdate-30)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.TridentBatchACH",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4878);
   __sJT_st.setLong(2,processSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:322^17*/
                commit();
              }
            }
          }
        }
      }   // end if ( it != null )
    }
    catch( Exception e )
    {
      logEntry("extractAchTridentStatementRecords(" + dataSource + "," + processSequence + ")",e.toString());
      throw(e);
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void processACHStatement(String testString)
    throws Exception
  {
    long      processSequence     = 0L;
    int       recCount            = 0;
    
    try
    {
      // check for records to process
      /*@lineinfo:generated-code*//*@lineinfo:351^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1) 
//          from    trident_capture_totals    tct
//          where   tct.process_sequence = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)  \n        from    trident_capture_totals    tct\n        where   tct.process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.TridentBatchACH",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:356^7*/
      
      if( recCount > 0 )
      {
        log.debug("starting processACHStatement");
        /*@lineinfo:generated-code*//*@lineinfo:361^9*/

//  ************************************************************
//  #sql [Ctx] { select  ach_trident_statement_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ach_trident_statement_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.TridentBatchACH",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   processSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:366^9*/
        
        log.debug("marking trident_capture_totals records with new sequence");
        /*@lineinfo:generated-code*//*@lineinfo:369^9*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture_totals
//            set     process_sequence = :processSequence
//            where   process_sequence = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_capture_totals\n          set     process_sequence =  :1 \n          where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.TridentBatchACH",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,processSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:374^9*/
        
        extractAchTridentStatementRecords(DS_VISAK,processSequence,testString);
      }        
    }
    catch(Exception e)
    {
      logEntry("processACHStatement()", e.toString());
      throw( e );
    }
    finally
    {
    }
  }
  
  private void processACHStatementPG(String testString)
    throws Exception
  {
    int       recCount    = 0;
    long      seqNum      = 0L;
    
    try
    {
      // check for records to process
      /*@lineinfo:generated-code*//*@lineinfo:398^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1) 
//          from    trident_capture_api    tapi
//          where   tapi.ach_sequence = 0
//                  and tapi.mesdb_timestamp >= (sysdate-30)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)  \n        from    trident_capture_api    tapi\n        where   tapi.ach_sequence = 0\n                and tapi.mesdb_timestamp >= (sysdate-30)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.TridentBatchACH",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:404^7*/
      
      if( recCount > 0 )
      {
        log.debug("starting processACHStatementPG");
        /*@lineinfo:generated-code*//*@lineinfo:409^9*/

//  ************************************************************
//  #sql [Ctx] { select  ach_trident_statement_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ach_trident_statement_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.TridentBatchACH",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:414^9*/
        
        log.debug("marking trident_capture_api records with new sequence");
        /*@lineinfo:generated-code*//*@lineinfo:417^9*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture_api   tapi
//            set     ach_sequence = :seqNum
//            where   ach_sequence = 0
//                    and tapi.mesdb_timestamp >= (sysdate-30)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_capture_api   tapi\n          set     ach_sequence =  :1 \n          where   ach_sequence = 0\n                  and tapi.mesdb_timestamp >= (sysdate-30)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.TridentBatchACH",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,seqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:423^9*/
        
        extractAchTridentStatementRecords(DS_PG,seqNum,testString);
      }        
    }
    catch(Exception e)
    {
      logEntry("processACHStatementPG()", e.toString());
      throw( e );
    }
    finally
    {
    }
  }
  
  private void process(int taskType, String testString)
  {
    try
    {
      connect();
      
      switch(taskType)
      {
        case TASK_CREATE_ACH_STATEMENT:
          processACHStatement(testString);    // extract visak
          processACHStatementPG(testString);  // extract payment gateway
          break;
          
        case TASK_CREATE_ACH_FILE:
          break;
      }
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("process(" + taskType + ")", e.toString());
      rollback();
    }
    finally
    {
      cleanUp();
    }
  }
  
  public boolean execute()
  {
    boolean result  = false;
    
    try
    {
      process( Integer.parseInt(getEventArg(0)), getEventArg(1) );
    
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    
    return( result );
  }
  
  public static void main( String[] args )
  {
    TridentBatchACH grunt = null;
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      grunt = new TridentBatchACH();
      
      // args[0] is process_sequence of records in trident_capture_totals
      grunt.connect();
      grunt.extractAchTridentStatementRecords(DS_VISAK,Long.parseLong(args[0]),"TEST");
      //grunt.process(Integer.parseInt(args[0]), "TEST");
    }
    catch(Exception e)
    {
      log.error("main(): " + e.toString());
    }
    finally
    {
      grunt.cleanUp();
    }
  }
  
}/*@lineinfo:generated-code*/