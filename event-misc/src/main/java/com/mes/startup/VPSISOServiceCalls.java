/*@lineinfo:filename=VPSISOServiceCalls*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/VirtualAppSubmitter.sqlj $

  Description:

    VPSISOServiceCalls

    Class to generate and send service calls to VPS via email

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-07-20 10:07:58 -0700 (Mon, 20 Jul 2009) $
  Version            : $Revision: 16324 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.CSVFileMemory;
import sqlj.runtime.ResultSetIterator;

public class VPSISOServiceCalls extends EventBase
{
  static Logger log = Logger.getLogger(VPSISOServiceCalls.class);
  
  public VPSISOServiceCalls()
  {
  }
  
  private boolean generateAndSend(String callDate)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    boolean           result  = false;
    
    try
    {
      connect();
      
      // generate list of calls
      /*@lineinfo:generated-code*//*@lineinfo:58^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(call_date, 'mm/dd/yy hh24:mi:ss') call_date,
//                  scs.description               case_status,
//                  sccn.case_number              case_id,
//                  sct.description               subject,
//                  substr(
//                    translate(sc.notes, ',"', ' '),
//                    1, 2000)                    note,
//                  sc.merchant_number            merchant_number,
//                  m.dba_name                    merchant_name, 
//                  vxpi.vendor_id                vendor_id,
//                  mc.merchcont_prim_first_name  contactfirst_name,
//                  mc.merchcont_prim_last_name   contactlast_name
//          from    service_calls sc,
//                  service_call_statuses scs,
//                  service_calls_case_numbers sccn,
//                  service_call_types sct,
//                  vapp_xml_post_iso vxpi,
//                  mif m,
//                  merchant mr,
//                  merchcontact mc,
//                  t_hierarchy th
//          where   th.ancestor = 3941500005 and
//                  th.descendent = m.association_node and
//                  m.merchant_number = sc.merchant_number and
//                  trunc(sc.call_date) = to_date(:callDate, 'mm/dd/yyyy') and
//                  sc.sequence = sccn.sequence(+) and
//                  sc.type = sct.type(+) and
//                  sc.status = scs.status(+) and
//                  m.merchant_number = mr.merch_number and
//                  mr.app_seq_num = vxpi.app_seq_num(+) and
//                  mr.app_seq_num = mc.app_seq_num(+)
//          order by sc.call_date asc        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(call_date, 'mm/dd/yy hh24:mi:ss') call_date,\n                scs.description               case_status,\n                sccn.case_number              case_id,\n                sct.description               subject,\n                substr(\n                  translate(sc.notes, ',\"', ' '),\n                  1, 2000)                    note,\n                sc.merchant_number            merchant_number,\n                m.dba_name                    merchant_name, \n                vxpi.vendor_id                vendor_id,\n                mc.merchcont_prim_first_name  contactfirst_name,\n                mc.merchcont_prim_last_name   contactlast_name\n        from    service_calls sc,\n                service_call_statuses scs,\n                service_calls_case_numbers sccn,\n                service_call_types sct,\n                vapp_xml_post_iso vxpi,\n                mif m,\n                merchant mr,\n                merchcontact mc,\n                t_hierarchy th\n        where   th.ancestor = 3941500005 and\n                th.descendent = m.association_node and\n                m.merchant_number = sc.merchant_number and\n                trunc(sc.call_date) = to_date( :1 , 'mm/dd/yyyy') and\n                sc.sequence = sccn.sequence(+) and\n                sc.type = sct.type(+) and\n                sc.status = scs.status(+) and\n                m.merchant_number = mr.merch_number and\n                mr.app_seq_num = vxpi.app_seq_num(+) and\n                mr.app_seq_num = mc.app_seq_num(+)\n        order by sc.call_date asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.VPSISOServiceCalls",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,callDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.VPSISOServiceCalls",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^7*/
      
      rs = it.getResultSet();
      
      // create csv file in memory
      log.debug("creating call file for: " + callDate);
      
      CSVFileMemory csv = new CSVFileMemory(rs, true);
      
      rs.close();
      it.close();
      
      // send file to recipients
      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_VPS_ISO_SERVICE_CALLS);
      msg.setSubject("MeS Service Calls for " + callDate);
      msg.setText("Attached are the service calls taken by Merchant e-Solutions for VPS ISO merchants on " + callDate + ".");
      msg.addStringAsTextFile("mes_calls.csv", csv.toString());
      
      log.debug("sending email");
      msg.send();
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("generateAndSend(" + callDate + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  public boolean execute()
  {
    boolean result = false;
    
    try
    {
      connect();
      
      String yesterday = "";
      
      // get yesterday's date and run the process for that date
      /*@lineinfo:generated-code*//*@lineinfo:141^7*/

//  ************************************************************
//  #sql [Ctx] { select  to_char(sysdate-1, 'mm/dd/yyyy')
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_char(sysdate-1, 'mm/dd/yyyy')\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.VPSISOServiceCalls",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   yesterday = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:146^7*/
      
      result = generateAndSend(yesterday);
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public static void main (String[] args)
  {
    try
    {
      String callDate = args[0];
      
      SQLJConnectionBase.initStandalone("DEBUG");
      
      VPSISOServiceCalls worker = new VPSISOServiceCalls();
      
      worker.generateAndSend(callDate);
    }
    catch(Exception e)
    {
      log.error("main: " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/