/*@lineinfo:filename=CertegyBilling*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.io.FileOutputStream;
import java.io.OutputStream;

public class CertegyBilling extends EventBase
{
  
  public boolean execute()
  {
    FileOutputStream  out = null;
    OutputStream      os  = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:25^7*/

//  ************************************************************
//  #sql [Ctx] { update  test
//          set     john = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  test\n        set     john = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.CertegyBilling",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:29^7*/
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }
    
    return true;
  }
}/*@lineinfo:generated-code*/