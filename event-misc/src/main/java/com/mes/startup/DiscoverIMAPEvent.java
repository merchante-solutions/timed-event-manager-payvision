/*@lineinfo:filename=DiscoverIMAPEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/DiscoverIMAPEvent.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.File;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.ops.EasiDownloadError;
import com.mes.ops.EasiDownloadResponse;
import com.mes.ops.EasiFile;
import com.mes.ops.EasiFileManager;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import sqlj.runtime.ResultSetIterator;

public class DiscoverIMAPEvent extends EventBase
{
    public static final String CABLECAR_HOST = "com.mes.cablecar.host";
    public static final String CABLECAR_USER = "com.mes.cablecar.user";
    public static final String CABLECAR_PASSWORD = "com.mes.cablecar.password";
    public static final String RAPINCOMING = "com.mes.rapincoming";
    public static final String RAPOUTGOING = "com.mes.rapoutgoing";
  static  Logger  log = Logger.getLogger(DiscoverIMAPEvent.class);

  private static final int      ZERO                = 0;
  private static final int      BLANK               = 1;
  private static final int      RIGHT_JUST          = 0;
  private static final int      LEFT_JUST           = 1;

  private static final int      NO_FORMATTING       = 1;
  private static final int      NUM_LET_SPC         = 2;
  private static final int      LET_SPC             = 3;
  private static final int      NUMS_ONLY           = 4;
  private static final int      MMDDYY              = 5;
  private static final int      YYMMDD              = 6;
  private static final int      MMYY                = 7;
  private static final int      IMPLIED2            = 8;
  private static final int      IMPLIED3            = 9;
  private static final int      NUMS_PAD_SPACE      = 10;
  private static final int      SSN_FORMAT          = 11;

  private static final String   PARTNER_MES         = "9966";
  private static final String   PARTNER_DISCOVER    = "9968";
  private static final String   PARTNER_BBT         = "9849";
  
  public static final int       PROCESS_OUTGOING    = 0;
  public static final int       PROCESS_INCOMING    = 1;

  private ResultSetIterator batch_it            = null;
  private int               idx                 = 0;
  private String[]          fileDataLine        = null;
  private int               formatCode          = NO_FORMATTING;  //temp solution
  private String            dbName              = null;

  private String            dateString          = null;
  private String            timeString          = null;
  private String            headerDateStr       = null;
  private String            headerTimeStr       = null;
  private String            cntrlNumStr         = null;
  
  private boolean           filesDeleted        = false;
  private String            errorDescription    = "";
  private Vector            errorVec            = new Vector();

  private EasiFile          currentFile         = null;
  private EasiFileManager   fileManager         = new EasiFileManager();
  
  private boolean           TestMode            = false;



  /*
  ** CONSTRUCTOR public NSIBatchUpdate()
  **
  ** Default constructor.  Turns off autocommit.
  */
  public DiscoverIMAPEvent()
  {
    // disable auto commit
    super(false);
  }

  /*
  ** CONSTRUCTOR public NSIBatchUpdate(String connectionString)
  **
  ** Accepts a connection string.  Turns off autocommit.
  */
  public DiscoverIMAPEvent(String connectionString)
  {
    // disable auto commit
    super(connectionString, false);
  }
  
  private void addError(String errDesc)
  {
    errorVec.add(errDesc);

    //clear out error description
    this.errorDescription = "";
  }
  
  private void addField(ResultSet rs, String dbName, int fieldLength, String defaultData, int formatCode) throws Exception
  {
    String fieldData = null;
    if (dbName != null && rs != null)
    {
      fieldData = rs.getString(dbName);
      fieldData = isBlank(fieldData) ? defaultData : fieldData.trim();
    }
    else if (defaultData != null)
    {
      fieldData = defaultData.trim();
    }
    fieldData = specialFormatting(fieldData, fieldLength, formatCode);
    fileDataLine[idx] = fieldData.toUpperCase();
    idx++;
  }
  
  private void addHeaders() throws Exception
  {
    createHeader();
  }
  
  private void addTrailers() throws Exception
  {
    for(int x=0; x<fileManager.getFileIdentifierListSize(); x++)
    {
      //files should already exist so dont get new one if its not found.. false
      currentFile = fileManager.getEasiFile(fileManager.getFileIdentifierFromList(x), false);

      if(currentFile != null)
      {
        if(currentFile.getLinesWritten() > 0)
        {
          createTrailer();
          fileManager.saveEasiFile(currentFile);
        }
        else //no lines were written to file, so delete it
        {
          currentFile.deleteFile();
        }
      }
    }
  }

  private void createHeader() throws Exception
  {
    fileDataLine = new String[5];
    idx          = 0;   //reset the array index

    // 1 record type
    addField(null, null, 2, "00",   NO_FORMATTING); 

    // 2 easi partner number
    addField(null, null, 4, currentFile.getFileIdentifier(), NO_FORMATTING); 

    // 3 version number
    addField(null, null, 2, "01", NO_FORMATTING);

    // 4 date timestamp
    addField(null, null, 24, (headerDateStr + "." + headerTimeStr + ".0000"), NO_FORMATTING);

    // 5 filler
    addField(null, null, 532, "", NO_FORMATTING);

    currentFile.writeLineToFile(fileDataLine, false);
  }
  
  private void createTrailer() throws Exception
  {
    fileDataLine = new String[4];
    idx          = 0;   //reset the array index

    // 1 record type
    addField(null, null, 2, "99",   NO_FORMATTING); 

    // 2 easi partner number
    addField(null, null, 4, currentFile.getFileIdentifier(), NO_FORMATTING); 

    // 3 number of records in the file
    addField(null, null, 5, Integer.toString(currentFile.getLinesWritten()), NUMS_ONLY);

    // 4 filler
    addField(null, null, 553, "", NO_FORMATTING);

    currentFile.writeLineToFile(fileDataLine, false);
  }
  
  /*
  ** METHOD public boolean execute()
  **
  ** EventBase entry point.
  ** creates tape file, generates a tape file status email message and sends it.
  ** RETURNS: true if no errors occurred....
  */
  public boolean execute()
  {
    int         processType   = -1;
    boolean     retVal        = false;

    try
    {
      connect();

      processType = Integer.parseInt( getEventArgs() );
      
      switch( processType )
      {
        case PROCESS_OUTGOING:
          processOutgoingRequests();
          break;
          
        case PROCESS_INCOMING:
          processIncomingRequests();
          break;
          
        default:
          throw new Exception("Invalid process type " + processType);
      }
    }
    catch(Exception e)
    {
      errorDescription += "ERROR in execute:" + e.toString() + ")\n";
      addError(errorDescription);
      retVal = false;
    }
    finally
    {
      cleanUp();
    }
    return (retVal);
  }
  
  /*
  ** METHOD private ResultSet getBatchRecords() throws Exception
  **
  ** Timestamps all unprocessed accounts and returns a result set 
  ** containing all available NSI batch elements.
  **
  ** RETURNS: ResultSet containing the batch data.
  */
  private ResultSet getBatchRecords() throws Exception
  {
    // get a timestamp value
    java.sql.Timestamp today = null;
    /*@lineinfo:generated-code*//*@lineinfo:282^5*/

//  ************************************************************
//  #sql [Ctx] { select    sysdate 
//        from      dual
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    sysdate  \n      from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.DiscoverIMAPEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   today = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:286^5*/
    log.debug("Processing Timestamp: " + DateTimeFormatter.getFormattedDate(today,"MM/dd/yyyy HH:mm:ss"));
    
    // timestamp all unprocessed accounts
    /*@lineinfo:generated-code*//*@lineinfo:290^5*/

//  ************************************************************
//  #sql [Ctx] { update    MERCHANT_DISCOVER_APP_COMPLETE acc
//        set       acc.date_processed = :today
//        where     acc.date_processed is null
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update    MERCHANT_DISCOVER_APP_COMPLETE acc\n      set       acc.date_processed =  :1 \n      where     acc.date_processed is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.DiscoverIMAPEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:295^5*/
  
    // gather todays freshly stamped records
    /*@lineinfo:generated-code*//*@lineinfo:298^5*/

//  ************************************************************
//  #sql [Ctx] batch_it = { select    vtf.*
//        from      MERCHANT_DISCOVER_APP vtf,
//                  MERCHANT_DISCOVER_APP_COMPLETE acc
//        where     vtf.app_seq_num = acc.app_seq_num and 
//                  acc.date_processed = :today
//        order by  acc.date_completed asc
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    vtf.*\n      from      MERCHANT_DISCOVER_APP vtf,\n                MERCHANT_DISCOVER_APP_COMPLETE acc\n      where     vtf.app_seq_num = acc.app_seq_num and \n                acc.date_processed =  :1 \n      order by  acc.date_completed asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.DiscoverIMAPEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   // execute query
   batch_it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.DiscoverIMAPEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:306^5*/
    
    return batch_it.getResultSet();
  }
  
  private int getIntTime(String str)
  {
    int tempInt = getRandomNum(); 

    try
    {
      tempInt = Integer.parseInt(str);  
    }
    catch(Exception e)
    {
    }

    return tempInt;
  }

  private int getRandomNum()
  {
    int result = 123;
    
    double tempRand = java.lang.Math.random();
    
    tempRand = tempRand * 100;

    int tempInt = java.lang.Math.round((float)tempRand);
    
    switch(tempInt)
    {
      case 0:
        result = 111;
      break;
      case 1:
        result = 222;
      break;
      case 2:
        result = 333;
      break;
      case 3:
        result = 444;
      break;
      case 4:
        result = 555;
      break;
      case 5:
        result = 6661;
      break;
      case 6:
        result = 777;
      break;
      case 7:
        result = 888;
      break;
      case 8:
        result = 22;
      break;
      case 9:
        result = 0;
      break;
    }
    return result;
  }
  
  /*
  ** METHOD private String getRecordData(ResultSet rs) throws Exception
  **
  ** Builds an NSI batch record from the current row in the given ResultSet.
  **
  ** RETURNS: the batch record in a String.
  */
  private boolean getRecordData(ResultSet rs) throws Exception
  {
    boolean errorOccurred   = false;
    fileDataLine            = new String[43];
    idx                     = 0;   //reset the array index

    // 1 record type
    addField(null, null, 2, "01", NUM_LET_SPC);
    
    // 2 version no.
    addField(null, null, 2, "01", NUM_LET_SPC);
    
    // 3 control no.
    addField(null, null, 15, ("R" + currentFile.getFileIdentifier() + specialFormatting(dateString, 6, YYMMDD) + specialFormatting(Integer.toString(getIntTime(cntrlNumStr) + currentFile.getLinesWritten()), 4, NUMS_ONLY)), NUM_LET_SPC);
    
    // 4 hq control no.
    addField(null, null, 15, "", NUM_LET_SPC);

    // 5 dba name
    addField(rs, "dba_name", 30, "", NUM_LET_SPC); 

    // 6 dba address
    addField(rs, "dba_address", 30, "", NUM_LET_SPC); 

    // 7 dba city
    addField(rs, "dba_city", 13, "", NUM_LET_SPC); 

    // 8 dba state
    addField(rs, "dba_state", 2, "", NUM_LET_SPC); 

    // 9 dba zip
    addField(rs, "dba_zip", 5, "", NUMS_ONLY); 

    // 10 dba phone
    addField(rs, "dba_phone", 10, "", NUMS_ONLY); 

    // 11 business principal 
    addField(rs, "principal_name", 45, "", NUM_LET_SPC); 

    // 12 principal title
    addField(rs, "principal_title", 15, "", NUM_LET_SPC); 

    // 13 principal address
    addField(rs, "principal_address", 30, "", NUM_LET_SPC); 

    // 14 principal city
    addField(rs, "principal_city", 13, "", NUM_LET_SPC); 

    // 15 principal state
    addField(rs, "principal_state", 2, "", NUM_LET_SPC); 

    // 16 principal zip
    addField(rs, "principal_zip", 5, "", NUMS_ONLY); 

    // 17 federal tax id
    addField(rs, "federal_tax_id", 9, "", SSN_FORMAT); 

    // 18 corporate name 
    addField(rs, "corporate_name", 30, "", NUM_LET_SPC); 

    // 19 principal address
    addField(rs, "corporate_address", 30, "", NUM_LET_SPC); 

    // 20 principal address line 2
    addField(rs, "corporate_address2", 30, "", NUM_LET_SPC); 
    
    // 21 principal city
    addField(rs, "corporate_city", 13, "", NUM_LET_SPC); 

    // 22 principal state
    addField(rs, "corporate_state", 2, "", NUM_LET_SPC); 

    // 23 principal zip
    addField(rs, "corporate_zip", 5, "", NUMS_ONLY); 

    // 24 merchant category code
    addField(rs, "merchant_cat_code", 4, "", NUMS_ONLY); 

     // 25 moto merchant flag
    addField(rs, "moto_flag", 1, "", NUM_LET_SPC); 

     // 26 non profit flag
    addField(rs, "non_profit_flag", 1, "", NUM_LET_SPC); 

    // 27 average sale
    addField(rs, "average_sale", 3, "", NUMS_ONLY); 
 
    // 28 annual sales
    addField(rs, "annual_sales", 8, "", NUMS_ONLY); 

    // 29 business type
    addField(rs, "business_type", 1, "", NUM_LET_SPC); 

    // 30 principal ssn#
    addField(rs, "principal_ssn", 9, "", SSN_FORMAT); 

    // 31 transit routing num
    addField(rs, "transit_routing_num", 9, "", NUMS_ONLY); 

    // 32 bank account dda
    addField(rs, "account_dda", 17, "", NUMS_PAD_SPACE); 

    // 33 discount rate
    addField(rs, "discover_disc_rate", 5, "", IMPLIED3); 

    // 34 membership fee
    addField(rs, "membership_fee", 3, "", NUMS_ONLY); 

    // 35 Years in business
    addField(rs, "years_in_business", 2, "", NUMS_ONLY); 

    // 36 franchise code
    addField(rs, "franchise_code", 4, "", NUM_LET_SPC); 

    // 37 easi partner number
    addField(null, null, 4, currentFile.getFileIdentifier(), NUM_LET_SPC); 

    //38,39 easi partner intermediate level 1 & 2
    if((currentFile.getFileIdentifier()).equals(PARTNER_DISCOVER))
    {
      addField(null, null, 15, ("PTF=" + (isBlank(rs.getString("DISCOVER_PER_ITEM_RATE")) ? "" : rs.getString("DISCOVER_PER_ITEM_RATE"))), NO_FORMATTING); 
      addField(null, null, 15, "Paymt Solutions", NUM_LET_SPC); 
    }
    else
    {
      addField(null, null, 15, "", NUM_LET_SPC); 
      addField(rs, "INTERMEDIATE_LEVEL2", 15, "", NUM_LET_SPC); 
    }

    //40 easi partner sales rep Id
    addField(rs, "sales_rep_id", 15, "", NUM_LET_SPC); 

    //41 acquirer merchant number
    addField(rs, "acquirer_merchant_num", 20, "", NUM_LET_SPC); 

    //42 website address
    addField(rs, "dba_url", 45, "", NO_FORMATTING); 

    //43 email address
    addField(rs, "dba_email", 30, "", NO_FORMATTING); 

    return !errorOccurred;
  }
  
  protected boolean inTestMode()
  {
    return(TestMode);
  }
  
  public boolean isBlank(String test)
  {
    boolean pass = false;

    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }

    return pass;
  }
  
  /*
  ** METHOD private boolean loadBatchData()
  **
  ** Timestamps today's batch and fills a StringBuffer with the data formatted
  ** according to the NSI batch specs.
  **
  ** RETURNS: true if successful, else false if an error occured (exception
  **          was thrown
  */
  private boolean loadFileData()
  {
    try
    {
      // get batch data
      ResultSet rs = getBatchRecords();
      
      while (rs.next())
      {
        //first check to see if filemanger exists and if so try to get it
        //if it doesnt exist, make a new instance of it and return that
        //each partner_num gets their own file
        String fileIdentifier = rs.getString("partner_num");
        
        //fileIdentifier cannot be blank
        //so if this isnt the case, log the error and continue to the next record
        if(isBlank(fileIdentifier))
        {
          errorDescription += "ERROR: Partner number was: " + (isBlank(fileIdentifier) ? "BLANK" : fileIdentifier) + " for Merchant# " + rs.getString("ACQUIRER_MERCHANT_NUM") + " (" + rs.getString("dba_name") + ")\n";
          addError(errorDescription);
          continue;
        }
        
        //get the current file before executing getRecordData because current file info is used in that method
        currentFile = fileManager.getEasiFile(fileIdentifier);

        if(!currentFile.isHeaderWrittenToFile())
        {
          addHeaders();
          currentFile.setHeaderWrittenToFile();
        }

          
        if(getRecordData(rs)) //if record has no errors (true) then we can submit to file
        {
          //then we write to it
          currentFile.writeLineToFile(fileDataLine); //will throw exception
        }
        else
        {
          errorDescription += "ERROR: getRecordData(rs) returned false for Merchant# " + rs.getString("ACQUIRER_MERCHANT_NUM") + " (" + rs.getString("dba_name") + ")\n";
          addError(errorDescription);
        }

        //put back in hashmap
        fileManager.saveEasiFile(currentFile);
      }
      
      //add the trailers.. we can assume that enteries were actually made into the file by this time
      //or the files will not exist anyway.. if no files exists nothing will happen
      addTrailers();

      //close the files.. close the writer for each file.. if it exists..if no files exists nothing will happen
      fileManager.closeFiles();

      // commit the timestamping      
      con.commit();
    }
    catch (Exception e)
    {
      // rollback the timestamping
      try 
      {
        log.error("Fatal Error Occurred in loadFileData -- Rolling back.");
        con.rollback(); 
      }
      catch (Exception re) 
      {
        log.error(this.getClass().getName() + "::loadFileData(con.rollback()): " + re);
      }

      //on error...delete the files if they were created.. if no files exists nothing will happen
      fileManager.deleteFiles();
      filesDeleted = true;
      errorDescription += "MAJOR ERROR: All Files Were Deleted!\n error:" + e.toString();
      addError(errorDescription);
      
      log.error(this.getClass().getName() + "::loadFileData(): " + e);
    }

    return !filesDeleted;
  }
  
  protected boolean processIncomingRequests()
  {
    boolean   retVal    = false;
    Vector    procFiles = new Vector();
    
    try
    {
      // get flag files from cablecar to see what response files we have to deal with
      String ftpHost = EventProps.getString(CABLECAR_HOST);
      String ftpUser = EventProps.getString(CABLECAR_USER);
      String ftpPswd = EventProps.getString(CABLECAR_PASSWORD);
      
      String inbox = EventProps.getString(RAPINCOMING);
      
      FTPClient ftp = new FTPClient(ftpHost, 21);
      
      ftp.login(ftpUser, ftpPswd);
      
      ftp.setConnectMode(FTPConnectMode.ACTIVE);
      
      ftp.setType(FTPTransferType.ASCII);
      
      ftp.chdir(inbox);
      
      // get list of all flag files
      try
      {
        String[] flagFiles = ftp.dir("*.flg");
      
        // retrieve all the files to process
        for (int i=0; i<flagFiles.length; ++i)
        {
          String flagFilename = flagFiles[i];
        
          String procFilename = flagFilename.substring(0, flagFilename.lastIndexOf("."));
        
          procFiles.add(procFilename);
        
          //retrieve process file
          ftp.get(procFilename, procFilename);
          ftp.get(flagFilename, flagFilename);
        
          // delete files from host
          ftp.delete(procFilename);
          ftp.delete(flagFilename);
        }
      }
      catch(com.enterprisedt.net.ftp.FTPException fe)
      {
        log.error("No Discover response or error files to process");
        // most likely no flag files to process, just ignore
      }
      
      try { ftp.quit(); } catch(Exception e) {}
      
      // now process the files existing in the rap/ folder
      for(int i=0; i<procFiles.size(); ++i)
      {
        String procFilename = (String)(procFiles.elementAt(i));
        
        // process file (error or response)
        readFile(procFilename);
      }
    }
    catch( Exception e )
    {
      logEntry("processIncomingRequests()", e.toString());
    }
    return( retVal );
  }
  
  private void readFile(String filename)
  {
    try
    {
      String filetype = filename.substring(0, 3);
      
      File procFile = new File(filename);
      
      if(filetype.equals("ERR"))
      {
        if((new EasiDownloadError()).getData(procFile))
        {
          // send file to kirin to be archived
          archiveDailyFile(filename);
        }
      }
      else if(filetype.equals("RSP"))
      {
        if((new EasiDownloadResponse()).getData(procFile))
        {
          // send file to kirin to be archived
          archiveDailyFile(filename);
        }
      }
      else
      {
        logEntry("readFile(" + filename + ")", "Invalid file type");
      }
    }
    catch(Exception e)
    {
      logEntry("readFile(" + filename + ")", e.toString());
    }
  }
  
  
  protected boolean processOutgoingRequests()
  {
    boolean retVal   = false;

    try
    {
      //sets date and time
      setDateTime();

      if ( (retVal = loadFileData()) == true )
      {
        sendFiles();
      }
    }
    catch(Exception e)
    {
      logEntry("processOutgoingRequests()",e.toString());
      errorDescription += "ERROR in execute:" + e.toString() + ")\n";
      addError(errorDescription);
      retVal = false;
    }
    finally
    {
      sendFileStatusMail();
    }
    return (retVal);
  }
  

  private void sendFiles()
  {
    EasiFile  easiFile            = null;
    String    fileId              = null;
    String    outgoingFilename    = null;
    
    for( int i = 0; i < fileManager.getFileIdentifierListSize(); ++i )
    {
      fileId = fileManager.getFileIdentifierFromList(i);
      easiFile = fileManager.getEasiFile(fileId,false);
      outgoingFilename = easiFile.getFileName();
    
      if ( inTestMode() )
      {
        log.debug("'" + outgoingFilename + "' was created but not sent");
      }
      else
      {
        try
        {
          sendDataFile( outgoingFilename,
                        EventProps.getString(CABLECAR_HOST),
                        EventProps.getString(CABLECAR_USER),
                        EventProps.getString(CABLECAR_PASSWORD),
                        EventProps.getString(RAPOUTGOING),
                        false,
                        true );   // send flag file
                    
          archiveDailyFile(outgoingFilename);
        }
        catch( Exception e )
        {
          logEntry("sendFiles(" + outgoingFilename + ")", e.toString());
        }
      }
    }
  }

  private void sendFileStatusMail()
  {
    String message = "";
    String subject = "Discover Upload Ran On " + this.dateString + ", At " + this.timeString;

    try
    {
      if(filesDeleted)
      {
        message = "MAJOR ERROR: ALL FILES WERE DELETED AND DATABASE UPDATE WAS ROLLED BACK!\n";
      }
      else
      {
        message = "Files Created: " + "\n";
      
        for(int x=0; x<fileManager.getFileIdentifierListSize(); x++)
        {
          currentFile = fileManager.getEasiFile(fileManager.getFileIdentifierFromList(x) ,false);
        
          if(currentFile != null)
          {
            if(!currentFile.isFileDeleted())
            {
              message += "Partner # " + currentFile.getFileIdentifier() + " File Name: " + currentFile.getFileName()  + ", Number of Records: " + currentFile.getLinesWritten() + "\n";
            }
            else
            {
              message += "Partner # " + currentFile.getFileIdentifier() + " File Name: " + currentFile.getFileName()  + ", WAS CREATED BUT THEN DELETED, IT HAD NO RECORDS\n";
            }
          }
        }

        if(errorVec.size() > 0)
        {
      
          message += "\n\nErrors\n\n";
      
          for(int p=0; p<errorVec.size(); p++)
          {
            message += (String)errorVec.elementAt(p) + "\n\n";
          }
    
        }
      }
      
      MailMessage msg = new MailMessage(true);
      msg.setAddresses(MesEmails.MSG_ADDRS_RAP_RESPONSE_ADMIN);
      msg.setSubject(subject);
      msg.setText(message);
      msg.send();
    }
    catch(Exception e)
    {
      log.error("sendFileStatusMail " + e.toString());
    }  
  }
  
  private void setDateTime()
  {
    try
    {
      // get current date and time
      Calendar   cal  = Calendar.getInstance();

      DateFormat df   = new SimpleDateFormat("MM/dd/yy");
      DateFormat tf   = new SimpleDateFormat("hh:mm:ss");
      dateString      = df.format(cal.getTime());
      timeString      = tf.format(cal.getTime());

      DateFormat hdf  = new SimpleDateFormat("yyyy-MM-dd");
      DateFormat htf  = new SimpleDateFormat("hh.mm.ss");
      headerDateStr   = hdf.format(cal.getTime());
      headerTimeStr   = htf.format(cal.getTime());
      
      DateFormat cnf  = new SimpleDateFormat("HHmm");
      cntrlNumStr     = cnf.format(cal.getTime());
    }
    catch(Exception e)
    {
    }
  }
  
  protected void setTestMode( boolean newMode )
  {
    TestMode = newMode;
  }
  
  private String specialFormatting(String data, int fieldLength, int formatCode) throws Exception
  {
    StringBuffer      dataString  = null;
    SimpleDateFormat  df          = null;
    DateFormat        fmt         = null;
    DecimalFormat     nf          = null;
    String            appendage   = " ";
    int               strLength   = 0;
    int               padLength   = 0;
    double            tempDouble  = 0.0;

    if (data == null)
    {
      data = "";
    }

    switch(formatCode)
    {
      case NO_FORMATTING:
        dataString = new StringBuffer(data);   
      break;
      
      case NUM_LET_SPC:    //only numbers letters and whitespaces.. removes punctuation
        dataString = new StringBuffer();   
        try
        {
          for(int i=0; i<data.length(); ++i)
          {
            if(Character.isLetterOrDigit(data.charAt(i)) || Character.isWhitespace(data.charAt(i)))
            {
              dataString.append(data.charAt(i));
            }
          }
        }
        catch(Exception e)
        {}
      break;
      
      case LET_SPC: //only letters and whitespaces
        dataString = new StringBuffer();   
        try
        {
          for(int i=0; i<data.length(); ++i)
          {
            if(Character.isLetter(data.charAt(i)) || Character.isWhitespace(data.charAt(i)))
            {
              dataString.append(data.charAt(i));
            }
          }
        }
        catch(Exception e)
        {}
      break;

      case NUMS_ONLY: //only numbers
        
        dataString = new StringBuffer();   

        try
        {
          for(int i=0; i<data.length(); ++i)
          {
            if(Character.isDigit(data.charAt(i)))
            {
              dataString.append(data.charAt(i));
            }
          }
        }
        catch(Exception e)
        {}
        appendage = "0";
      break;
      
      case NUMS_PAD_SPACE:
        
        dataString = new StringBuffer();   
        
        try
        {
          for(int i=0; i<data.length(); ++i)
          {
            if(Character.isDigit(data.charAt(i)))
            {
              dataString.append(data.charAt(i));
            }
          }
        }
        catch(Exception e)
        {}
        appendage = " ";
      break;

      case SSN_FORMAT:
        
        dataString = new StringBuffer();   
        
        try
        {
          //0 gets knocked off the front sometimes..
           if(data.length() == 8)
          {
            data = "0" + data;
          }
          else if(data.length() == 7)
          {
            data = "00" + data;
          }
          else if(data.length() == 6)
          {
            data = "000" + data;
          }

          for(int i=0; i<data.length(); ++i)
          {
            if(Character.isDigit(data.charAt(i)))
            {
              dataString.append(data.charAt(i));
            }
          }

          if(data.length() != 9) //then its an invalid ssn so blank it out
          {
            data = "";
          }

        }
        catch(Exception e)
        {}
        appendage = " ";

      break;


      case MMDDYY:
        if(isBlank(data))
        {
          dataString = new StringBuffer();   
          break;
        }

        df            = new SimpleDateFormat("MMddyy");
        fmt           = DateFormat.getDateInstance(DateFormat.SHORT);
        dataString    = new StringBuffer(df.format(fmt.parse(data)));
      break;

      case YYMMDD:
        if(isBlank(data))
        {
          dataString = new StringBuffer();   
          break;
        }
        
        df            = new SimpleDateFormat("yyMMdd");
        fmt           = DateFormat.getDateInstance(DateFormat.SHORT);
        dataString    = new StringBuffer(df.format(fmt.parse(data)));
      break;
      
      case MMYY:
        if(isBlank(data))
        {
          dataString = new StringBuffer();   
          break;
        }
        df            = new SimpleDateFormat("MMyy");
        fmt           = DateFormat.getDateInstance(DateFormat.SHORT);
        dataString    = new StringBuffer(df.format(fmt.parse(data)));
      break;

      case IMPLIED2: //implied 2 spaces
        if(isBlank(data))
        {
          dataString = new StringBuffer();   
          appendage = "0";
          break;
        }
        
        try
        {
          tempDouble = Double.parseDouble(data);
        }
        catch(Exception e)
        {
          dataString = new StringBuffer();   
          break;
        }

        if(fieldLength == 5)
        {
          nf = new DecimalFormat("000.00");
        }
        else if(fieldLength == 6)
        {
          nf = new DecimalFormat("0000.00");
        }
        else if(fieldLength == 7)
        {
          nf = new DecimalFormat("00000.00");
        }
        
        dataString = new StringBuffer(nf.format(tempDouble));
        if(fieldLength == 5)
        {
          dataString.deleteCharAt(3);
        }
        else if(fieldLength == 6)
        {
          dataString.deleteCharAt(4);
        }
        else if(fieldLength == 7)
        {
          dataString.deleteCharAt(5);
        }
        appendage = "0";
      break;
      
      case IMPLIED3: //implied 3 spaces
        if(isBlank(data))
        {
          dataString = new StringBuffer();   
          appendage = "0";
          break;
        }

        try
        {
          tempDouble = Double.parseDouble(data);
        }
        catch(Exception e)
        {
          dataString = new StringBuffer();   
          break;
        }

        if(fieldLength == 5)
        {
          nf = new DecimalFormat("00.000");
        }
        else if(fieldLength == 6)
        {
          nf = new DecimalFormat("000.000");
        }
        dataString = new StringBuffer(nf.format(tempDouble));
        if(fieldLength == 5)
        {
          dataString.deleteCharAt(2);
        }
        else if(fieldLength == 6)
        {
          dataString.deleteCharAt(3);
        }
        appendage = "0";
      break;
      
      default:
      break;
    }

    strLength = dataString.length();
    padLength = fieldLength - strLength;

    if(padLength > 0 && strLength == 0)
    {
      while(padLength != 0)
      {
        dataString.append(appendage);
        padLength--;
      }
    }
    else if(padLength > 0)
    {
      while(padLength != 0)
      {
        if(appendage.equals("0"))
        {
          dataString.insert(0,appendage);
        }
        else if(appendage.equals(" "))
        {
          dataString.append(appendage);
        }
        padLength--;
      }
    }
    else if(padLength < 0)
    {
      dataString.delete(fieldLength, strLength);
    }

    return dataString.toString();
  }
  
  public static void main( String[] args )
  {
    DiscoverIMAPEvent        test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new DiscoverIMAPEvent();
      test.setTestMode(true);
      
      PropertiesFile props = null;
      
      try
      {
        props = new PropertiesFile("event.properties");
      }
      catch(Exception e) {}
      test.setEventProperties(props);
      if ( args.length > 0 )
      {
        test.setEventArgs(args[0]);
      }
      else    // default is process the outgoing requests
      {
        test.setEventArgs(String.valueOf(PROCESS_OUTGOING)); 
      }
      test.execute();
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/