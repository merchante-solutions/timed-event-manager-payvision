/*@lineinfo:filename=REDUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/ChargebackUpload.sqlj $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2010-07-22 15:16:29 -0700 (Thu, 22 Jul 2010) $
  Version            : $Revision: 17600 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import com.mes.support.CSVFileMemory;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class REDUpload extends EventBase
{
  static Logger log = Logger.getLogger(REDUpload.class);
  
  public boolean TestMode = false;
  
  public REDUpload( )
  {
  }
  
  public boolean execute( )
  {
    String            workFilename  = null;
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    CSVFileMemory     csv           = null;
    String            errorString   = null;
    
    boolean result = false;
    
    try
    {
      connect();

      String dateString = null;
      /*@lineinfo:generated-code*//*@lineinfo:63^7*/

//  ************************************************************
//  #sql [Ctx] { select  to_char(sysdate,'mmddrr')
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_char(sysdate,'mmddrr')\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.REDUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dateString = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:68^7*/
      String indexString = NumberFormatter.getPaddedInt( getFileId( "RED_MeS", dateString), 3 );
      workFilename = "RED_MeS_"+dateString+"_"+indexString+".csv";
    
      log.debug("Running RED chargeback query (" + workFilename + ")");
      /*@lineinfo:generated-code*//*@lineinfo:73^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                    ordered
//                  */
//                  cb.merchant_number                      as "Merchant Id",
//                  cb.merchant_name                        as "DBA Name",
//                  cb.cb_load_sec                          as "Control Number",
//                  cb.incoming_date                        as "Incoming Date",
//                  mes.dukpt_decrypt_wrapper(cb.card_number_enc) as "Card Number",
//                  cb.reference_number                     as "Reference Number",
//                  cb.tran_date                            as "Tran Date",
//                  cb.tran_amount                          as "Tran Amount",
//                  cb.auth_code                            as "Auth Code",
//                  cba.adj_batch_date                      as "Adj Date",
//                  case 
//                     when cb.card_type in ('VS','MC') then substr(cb.reference_number,12,11) 
//                     else cb.reference_number
//                  end                                     as "Adj Ref Num",
//                  nvl(rd.REASON_DESC,
//                      'REASON CODE: '||cb.reason_code)    as "Reason",
//                  nvl(cb.first_time_chargeback,'Y')       as "First Time",
//                  cb.reason_code                          as "Reason Code",
//                  cb.cb_ref_num                           as "CB Ref Num"
//          from    retail_decisions_merchants     rdm,
//                  network_chargebacks            cb,
//                  network_chargeback_activity    cba,
//                  chargeback_reason_desc         rd
//          where   rdm.enabled = 'Y'
//                  and rdm.merchant_number = cb.merchant_number  
//                  and cb.incoming_date = trunc(sysdate-1)
//                  and cba.cb_load_sec(+) = cb.cb_load_sec and
//                  cba.action_date(+) = cb.incoming_date--trunc(sysdate)-1
//                  and rd.card_type(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and
//                  rd.item_type(+) = 'C' and
//                  rd.reason_code(+) = cb.reason_code
//          order by  cb.merchant_number, 
//                    cb.incoming_date desc,
//                    cb.cb_load_sec, 
//                    cba.adj_batch_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                  ordered\n                */\n                cb.merchant_number                      as \"Merchant Id\",\n                cb.merchant_name                        as \"DBA Name\",\n                cb.cb_load_sec                          as \"Control Number\",\n                cb.incoming_date                        as \"Incoming Date\",\n                mes.dukpt_decrypt_wrapper(cb.card_number_enc) as \"Card Number\",\n                cb.reference_number                     as \"Reference Number\",\n                cb.tran_date                            as \"Tran Date\",\n                cb.tran_amount                          as \"Tran Amount\",\n                cb.auth_code                            as \"Auth Code\",\n                cba.adj_batch_date                      as \"Adj Date\",\n                case \n                   when cb.card_type in ('VS','MC') then substr(cb.reference_number,12,11) \n                   else cb.reference_number\n                end                                     as \"Adj Ref Num\",\n                nvl(rd.REASON_DESC,\n                    'REASON CODE: '||cb.reason_code)    as \"Reason\",\n                nvl(cb.first_time_chargeback,'Y')       as \"First Time\",\n                cb.reason_code                          as \"Reason Code\",\n                cb.cb_ref_num                           as \"CB Ref Num\"\n        from    retail_decisions_merchants     rdm,\n                network_chargebacks            cb,\n                network_chargeback_activity    cba,\n                chargeback_reason_desc         rd\n        where   rdm.enabled = 'Y'\n                and rdm.merchant_number = cb.merchant_number  \n                and cb.incoming_date = trunc(sysdate-1)\n                and cba.cb_load_sec(+) = cb.cb_load_sec and\n                cba.action_date(+) = cb.incoming_date--trunc(sysdate)-1\n                and rd.card_type(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and\n                rd.item_type(+) = 'C' and\n                rd.reason_code(+) = cb.reason_code\n        order by  cb.merchant_number, \n                  cb.incoming_date desc,\n                  cb.cb_load_sec, \n                  cba.adj_batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.REDUpload",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.REDUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^7*/      
      
      rs = it.getResultSet();
      
      log.debug("creating RED Chargeback file");
      csv = new CSVFileMemory(rs, true);
      
      rs.close();
      it.close();
      
      log.debug("csv file created");
      //System.out.println( csv.toString() );
      
      log.debug("sending file via sftp");
      Sftp  sftp = null;
      
      for( int attemptCount = 0; attemptCount < 3; ++attemptCount )
      {
        try
        {
          log.debug("generating sftp");
          sftp = getSftp(
            MesDefaults.getString(MesDefaults.DK_RED_IP),
            MesDefaults.getString(MesDefaults.DK_RED_USER),
            MesDefaults.getString(MesDefaults.DK_RED_PASSWORD),
            "",
            false);
            
          log.debug("uploading file: " + workFilename);
          sftp.upload(csv.toString().getBytes(), workFilename);            
        
          try{ sftp.disconnect(); } catch(Exception ee) {} finally{ sftp = null; }
        
          result = true;
          break;
        }
        catch(Exception ftpe)
        {
          // close the connection and release the reference
          try{ sftp.disconnect(); } catch(Exception ee){} finally{ sftp = null; }

          // delay 1 second before retry attempt
          try{ Thread.sleep(1000); } catch( Exception ee ) {}
        }
      }
      
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
      errorString = e.toString();
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    try
    {
      String subject = "";
      if( result == true )
      {
        subject = "RED Chargeback Upload Success: " + workFilename + " (" + csv.rowCount() + " records)";
      }
      else
      {
        subject = "RED Chargeback Upload failed: " + errorString;
      }
    
      MailMessage msg = new MailMessage();
    
      msg.setAddresses(MesEmails.MSG_ADDRS_RED_UPLOAD);
      msg.setSubject(subject);
      msg.setText(subject);
      msg.send();
    }
    catch(Exception e) 
    {
    }
    
    return( result );
  }
  
  public static void main(String[] args)
  {
    try
    {
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_RED_IP,
                    MesDefaults.DK_RED_USER,
                    MesDefaults.DK_RED_PASSWORD
            });
        }


      com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
      REDUpload worker = new REDUpload();
      
      worker.execute();
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/