/*@lineinfo:filename=GroupHierarchyRefresh*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/GroupHierarchyRefresh.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-07-20 10:07:58 -0700 (Mon, 20 Jul 2009) $
  Version            : $Revision: 16324 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import com.mes.tools.GroupList;
import com.mes.tools.HierarchyTools;
import sqlj.runtime.ResultSetIterator;

public class GroupHierarchyRefresh extends EventBase
{
  static Logger log = Logger.getLogger(GroupHierarchyRefresh.class);

  private static final int    NODE_TYPE_ASSN                  = 4;
  private static final int    NODE_TYPE_GROUP                 = 5;
  private static final int    HIER_TYPE_TSYS                  = 1;
  private static final long   BANK_PORTFOLIOS_HIERARCHY_NODE  = 9999999999L;
  private static final long   BANK_PORTFOLIO_ASSOC_NUMBER     = 100000L;
  
  public static final int     PASSWORD_SCHEME_PHONE           = 1;
  public static final int     PASSWORD_SCHEME_MERCHANT_NUMBER = 2;
  
  private GroupHierarchy      curAssoc      = null;
  private boolean             assocExists   = false;
  
  public GroupHierarchyRefresh()
  {
  }
  
  private void logHierarchy(String source, GroupHierarchy culprit, String error)
  {
    boolean wasStale    = false;
    
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:73^7*/

//  ************************************************************
//  #sql [Ctx] { insert into java_log_hierarchy
//          (
//            log_date, 
//            source, 
//            error, 
//            bank_number, 
//            association,
//            group1, 
//            group2, 
//            group3, 
//            group4, 
//            group5, 
//            group6        
//          )
//          values
//          (
//            sysdate,
//            :source,
//            :error,
//            :culprit.bankNumber,
//            :culprit.association,
//            :culprit.group1,
//            :culprit.group2,
//            :culprit.group3,
//            :culprit.group4,
//            :culprit.group5,
//            :culprit.group6
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into java_log_hierarchy\n        (\n          log_date, \n          source, \n          error, \n          bank_number, \n          association,\n          group1, \n          group2, \n          group3, \n          group4, \n          group5, \n          group6        \n        )\n        values\n        (\n          sysdate,\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.GroupHierarchyRefresh",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,source);
   __sJT_st.setString(2,error);
   __sJT_st.setInt(3,culprit.bankNumber);
   __sJT_st.setLong(4,culprit.association);
   __sJT_st.setLong(5,culprit.group1);
   __sJT_st.setLong(6,culprit.group2);
   __sJT_st.setLong(7,culprit.group3);
   __sJT_st.setLong(8,culprit.group4);
   __sJT_st.setLong(9,culprit.group5);
   __sJT_st.setLong(10,culprit.group6);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:103^7*/
      
      // send email
      MailMessage msg = new MailMessage();
      StringBuffer body = new StringBuffer("");
      
      body.append("Source:   ");
      body.append(source);
      body.append("\n");
      body.append("Error:    ");
      body.append(error);
      body.append("\n");
      body.append("Bank:     " + culprit.bankNumber + "\n");
      body.append("Assoc:    " + culprit.association + "\n");
      body.append("Group 1:  " + culprit.group1 + "\n");
      body.append("Group 2:  " + culprit.group2 + "\n");
      body.append("Group 3:  " + culprit.group3 + "\n");
      body.append("Group 4:  " + culprit.group4 + "\n");
      body.append("Group 5:  " + culprit.group5 + "\n");
      body.append("Group 6:  " + culprit.group6 + "\n");
      
      msg.setAddresses(MesEmails.MSG_ADDRS_HIERARCHY_UPDATE);
      msg.setSubject("Error building GROUP hierarchy");
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      log.error("logHierarchy(): " + e.toString());
      com.mes.support.SyncLog.LogEntry("GroupHierarchy::logHierarchy()", e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
  }
  
  private void updateGroupHierarchy(long association, GroupList groups)
  {
    try
    {
      // update groups starting from the last valid group and moving up
      long lastParent = 0L;
      long parent     = 0L;
      long child      = 0L;
      int  nodeType   = 0;
      
      for(int i = groups.getGroupSize() - 1; i >= 0; --i)
      {
        // first linkage should be between association and last value
        if(i == groups.getGroupSize() - 1)
        {
          nodeType = NODE_TYPE_ASSN;
          parent = groups.getGroup(i);
          child = association;
        }
        else
        {
          nodeType = NODE_TYPE_GROUP;
          parent = groups.getGroup(i);
          child = groups.getGroup(i + 1);
        }
        
        HierarchyTools.insertNode(child, nodeType, "TSYS Node", parent, HIER_TYPE_TSYS);
        
        lastParent = parent;
      }
      
      // link the last parent to the bank node
      long  bankNode = association / 1000000L;
      bankNode *= 100000L;
      
      HierarchyTools.insertNode(lastParent, 
                                NODE_TYPE_GROUP, 
                                "TSYS Node",
                                bankNode,
                                HIER_TYPE_TSYS);
    }
    catch(Exception e)
    {
      log.error("updateGroupHierarchy(): " + e.toString());
      logHierarchy("updateGroupHierarchy(" + association + ")", curAssoc, e.toString());
      logEntry("updateGroupHierarchy(" + association + ")", e.toString());
    }
  }
  
  private void updateHierarchy(GroupHierarchy gh)
  {
    GroupList         groups      = new GroupList();
    ResultSetIterator it          = null;
    ResultSet         rs          = null;
    long              assoc       = 0L;
    String            progress    = "started";
    
    try
    {
      // store current item for reference by members
      curAssoc = gh;
      
      // update the groups table
      assoc = gh.association;
      
      // add groups in order
      if(gh.group1 > 0)
      {
        groups.add(gh.group1);
      }
      if(gh.group2 > 0)
      {
        groups.add(gh.group2);
      }
      if(gh.group3 > 0)
      {
        groups.add(gh.group3);
      }
      if(gh.group4 > 0)
      {
        groups.add(gh.group4);
      }
      if(gh.group5 > 0)
      {
        groups.add(gh.group5);
      }
      if(gh.group6 > 0)
      {
        groups.add(gh.group6);
      }
      
      updateGroupHierarchy(assoc, groups);
    }
    catch(Exception e)
    {
      log.error("updateHierarchy(): " + e.toString());
      logHierarchy("updateHierarchy()", curAssoc, e.toString());
      logEntry("updateHierarchy(" + gh.association + ")", e.toString());
    }
  }

  
  public boolean execute()
  {
    boolean           result        = false;
    Vector            associations  = new Vector();
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    
    try
    {
      connect();
      
      // get a process sequence
      long procSequence = 0L;
      
      /*@lineinfo:generated-code*//*@lineinfo:259^7*/

//  ************************************************************
//  #sql [Ctx] { select  hierarchy_refresh_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  hierarchy_refresh_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.GroupHierarchyRefresh",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:264^7*/
      
      // mark items that need processing
      /*@lineinfo:generated-code*//*@lineinfo:267^7*/

//  ************************************************************
//  #sql [Ctx] { update  groups_process
//          set     process_sequence = :procSequence
//          where   process_sequence is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  groups_process\n        set     process_sequence =  :1 \n        where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.GroupHierarchyRefresh",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:272^7*/
      
      commit();
      
      // build vector of items
      /*@lineinfo:generated-code*//*@lineinfo:277^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  substr(to_char(g.assoc_number), 1, 4) bank_number,
//                  g.assoc_number                        association,
//                  g.group_1                             group_1,
//                  g.group_2                             group_2,
//                  g.group_3                             group_3,
//                  g.group_4                             group_4,
//                  g.group_5                             group_5,
//                  g.group_6                             group_6
//          from    groups                                g,
//                  groups_process                        gp
//          where   gp.process_sequence = :procSequence and
//                  gp.association = g.assoc_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  substr(to_char(g.assoc_number), 1, 4) bank_number,\n                g.assoc_number                        association,\n                g.group_1                             group_1,\n                g.group_2                             group_2,\n                g.group_3                             group_3,\n                g.group_4                             group_4,\n                g.group_5                             group_5,\n                g.group_6                             group_6\n        from    groups                                g,\n                groups_process                        gp\n        where   gp.process_sequence =  :1  and\n                gp.association = g.assoc_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.GroupHierarchyRefresh",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.GroupHierarchyRefresh",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:291^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        associations.add(new GroupHierarchy(rs));
      }
      
      rs.close();
      it.close();
      
      
      // walk through vector and build hierarchies as necessary
      for(int i=0; i < associations.size(); ++i)
      {
        GroupHierarchy gh = (GroupHierarchy)associations.elementAt(i);
        
        updateHierarchy(gh);
        
        // update process
        /*@lineinfo:generated-code*//*@lineinfo:312^9*/

//  ************************************************************
//  #sql [Ctx] { update  groups_process
//            set     date_completed = sysdate
//            where   process_sequence = :procSequence and
//                    association = :gh.association
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  groups_process\n          set     date_completed = sysdate\n          where   process_sequence =  :1  and\n                  association =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.GroupHierarchyRefresh",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,procSequence);
   __sJT_st.setLong(2,gh.association);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:318^9*/
        
        commit();
      }
    }
    catch(Exception e)
    {
      log.error("GroupHierarchy::execute(): " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  public class GroupHierarchy
  {
    int   bankNumber        = 0;
    long  association       = 0;
    long  group1            = 0;
    long  group2            = 0;
    long  group3            = 0;
    long  group4            = 0;
    long  group5            = 0;
    long  group6            = 0;
    
    public GroupHierarchy(ResultSet rs)
    {
      try
      {
        bankNumber      = rs.getInt ("bank_number");
        association     = rs.getLong("association");
        group1          = rs.getLong("group_1");
        group2          = rs.getLong("group_2");
        group3          = rs.getLong("group_3");
        group4          = rs.getLong("group_4");
        group5          = rs.getLong("group_5");
        group6          = rs.getLong("group_6");
      }
      catch(Exception e)
      {
        log.error("GroupHierarchy::constructor(): " + e.toString());
        logHierarchy("GroupHierarchy::GroupHierarchy()", this, e.toString());
        com.mes.support.SyncLog.LogEntry("GroupHierarchy::GroupHierarchy()", e.toString());
      }
    }
    
  }
}/*@lineinfo:generated-code*/