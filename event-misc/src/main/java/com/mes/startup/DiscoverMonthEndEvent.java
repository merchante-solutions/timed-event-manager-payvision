/*@lineinfo:filename=DiscoverMonthEndEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/DiscoverMonthEndEvent.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.File;
import java.sql.Date;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.reports.DiscoverActivityDataBean;
import com.mes.reports.DiscoverEquipmentDataBean;
import com.mes.reports.ReportSQLJBean;

public class DiscoverMonthEndEvent extends EventBase
{
  public static final int       RT_ACTIVITY       = 0;
  public static final int       RT_EQUIPMENT      = 1;
  public static final int       RT_COUNT          = 2;
  
  protected boolean     TestMode      = false;
  
  public DiscoverMonthEndEvent()
  {
    PropertiesFilename = "discover-me.properties";
  }
  
  public boolean execute()
  {
    Date                beginDate     = null;
    Date                endDate       = null;
    String              fileType      = null;
    long                nodeId        = 0L;
    ReportSQLJBean      reportBean    = null;
    boolean             result        = false;
    String              tempFilename  = "discover_tempfile";
    String              workFilename  = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:63^7*/

//  ************************************************************
//  #sql [Ctx] { select  trunc((trunc(sysdate,'month')-1),'month'),
//                  (trunc(sysdate,'month')-1)   
//                         
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc((trunc(sysdate,'month')-1),'month'),\n                (trunc(sysdate,'month')-1)   \n                        \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.DiscoverMonthEndEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginDate = (java.sql.Date)__sJT_rs.getDate(1);
   endDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:69^7*/
      
      nodeId = MesDefaults.getLong(MesDefaults.DK_DISCOVER_TOP_NODE);
      
      for ( int i = 0; i < RT_COUNT; ++ i )
      {
        switch(i)
        {
          case RT_ACTIVITY:
            reportBean  = new DiscoverActivityDataBean();
            fileType = "disc_bc3941";
            break;
            
          case RT_EQUIPMENT:
            reportBean = new DiscoverEquipmentDataBean();
            fileType = "disc_pos3941";
            break;
        }
        workFilename = buildFilename(fileType);
      
        // connect to the database
        reportBean.connect(true);
      
        // initialize the ReportSQLJBean object
        reportBean.initialize();
        reportBean.setReportHierarchyNodeDefault(nodeId);
        reportBean.setReportHierarchyNode(nodeId);
        reportBean.setReportDateBegin(beginDate);
        reportBean.setReportDateEnd(endDate);
        
        // load the data from the database
        reportBean.loadData();
        reportBean.encodeData( mesConstants.FF_FIXED, ".", tempFilename );
        
        // cleanup the database connection
        reportBean.cleanUp();
        
        // rename the tempfile to the correct filename
        File f = new File(tempFilename + reportBean.getDownloadFilenameExtension(mesConstants.FF_FIXED));
        f.renameTo( new File(workFilename) );
        
        if ( !inTestMode() )
        {
          sendDataFile( workFilename,
                        MesDefaults.getString(MesDefaults.DK_DISC_ME_OUTGOING_HOST),
                        MesDefaults.getString(MesDefaults.DK_DISC_ME_OUTGOING_USER),
                        MesDefaults.getString(MesDefaults.DK_DISC_ME_OUTGOING_PASSWORD),
                        MesDefaults.getString(MesDefaults.DK_DISC_ME_OUTGOING_PATH),
                        false,
                        true );
                      
          archiveMonthlyFile( workFilename );                      
        }          
      }
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( result );
  }
  
  public boolean inTestMode()
  {
    return( TestMode );
  }
  
  public void setTestMode( boolean testMode )
  {
    TestMode = testMode;
  }
  
  public static void main( String[] args )
  {
    DiscoverMonthEndEvent      test   = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new DiscoverMonthEndEvent();
      if ( args.length <= 0 || !args[0].equals("prod") )
      {
        test.setTestMode(true);
      }        
      test.execute();
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/