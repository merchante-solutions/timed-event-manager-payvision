/*@lineinfo:filename=CBTSalesTaxReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/CBTSalesTaxReport.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-07-20 10:07:58 -0700 (Mon, 20 Jul 2009) $
  Version            : $Revision: 16324 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import com.mes.support.CSVFileMemory;
import sqlj.runtime.ResultSetIterator;

public class CBTSalesTaxReport extends EventBase
{
  public boolean execute()
  {
    boolean             result  = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    connect();
    try
    {
      // run query
      /*@lineinfo:generated-code*//*@lineinfo:48^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.merchant_number                 merchant_number,
//                  m.dba_name                        merchant_name,
//                  m.dmacctst                        account_status,
//                  substr(m.zip1_line_4, 1, 5)       merchant_zip,
//                  m.city1_line_4                    merchant_city,
//                  m.state1_line_4                   merchant_state,
//                  st.county                         merchant_county,
//                  trunc(round(cg.cg_charge_amount / sales_tax_cbt_rate(substr(m.zip1_line_4, 1, 5), m.city1_line_4), 1)) total_rental_amount,
//                  sales_tax_cbt_rate(
//                    substr(m.zip1_line_4, 1, 5), 
//                    m.city1_line_4)                 sales_tax_rate,
//                  st.state_tax                      state_tax_rate,
//                  st.county_tax                     county_tax_rate,
//                  st.county_local_tax               county_local_tax_rate,
//                  st.city_tax                       city_tax_rate,
//                  st.city_local_tax                 city_local_tax_rate,
//                  cg.cg_charge_amount               total_tax 
//          from    mif     m,
//                  monthly_extract_summary sm,
//                  monthly_extract_cg      cg,
//                  salestax_flat           st
//          where   sm.active_date = add_months(trunc(sysdate, 'MM'), -1) and
//                  sm.merchant_number = m.merchant_number and
//                  m.bank_number = 3858 and
//                  m.date_stat_chgd_to_dcb is null and
//                  sm.hh_load_sec = cg.hh_load_sec and
//                  cg.cg_charge_record_type = 'POS' and
//                  cg.cg_message_for_stmt = 'RENTAL SALES TAX' and
//                  st.id = sales_tax_id(substr(m.zip1_line_4, 1, 5), m.city1_line_4)
//          group by  m.merchant_number,
//                    m.dba_name,
//                    m.dmacctst,
//                    substr(m.zip1_line_4, 1, 5),
//                    m.city1_line_4,
//                    m.state1_line_4,
//                    st.county,
//                    sales_tax_cbt_rate(substr(m.zip1_line_4, 1, 5), m.city1_line_4),
//                    st.state_tax,
//                    st.county_tax,
//                    st.county_local_tax,
//                    st.city_tax,
//                    st.city_local_tax,
//                    cg.cg_charge_amount
//          order by m.merchant_number asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.merchant_number                 merchant_number,\n                m.dba_name                        merchant_name,\n                m.dmacctst                        account_status,\n                substr(m.zip1_line_4, 1, 5)       merchant_zip,\n                m.city1_line_4                    merchant_city,\n                m.state1_line_4                   merchant_state,\n                st.county                         merchant_county,\n                trunc(round(cg.cg_charge_amount / sales_tax_cbt_rate(substr(m.zip1_line_4, 1, 5), m.city1_line_4), 1)) total_rental_amount,\n                sales_tax_cbt_rate(\n                  substr(m.zip1_line_4, 1, 5), \n                  m.city1_line_4)                 sales_tax_rate,\n                st.state_tax                      state_tax_rate,\n                st.county_tax                     county_tax_rate,\n                st.county_local_tax               county_local_tax_rate,\n                st.city_tax                       city_tax_rate,\n                st.city_local_tax                 city_local_tax_rate,\n                cg.cg_charge_amount               total_tax \n        from    mif     m,\n                monthly_extract_summary sm,\n                monthly_extract_cg      cg,\n                salestax_flat           st\n        where   sm.active_date = add_months(trunc(sysdate, 'MM'), -1) and\n                sm.merchant_number = m.merchant_number and\n                m.bank_number = 3858 and\n                m.date_stat_chgd_to_dcb is null and\n                sm.hh_load_sec = cg.hh_load_sec and\n                cg.cg_charge_record_type = 'POS' and\n                cg.cg_message_for_stmt = 'RENTAL SALES TAX' and\n                st.id = sales_tax_id(substr(m.zip1_line_4, 1, 5), m.city1_line_4)\n        group by  m.merchant_number,\n                  m.dba_name,\n                  m.dmacctst,\n                  substr(m.zip1_line_4, 1, 5),\n                  m.city1_line_4,\n                  m.state1_line_4,\n                  st.county,\n                  sales_tax_cbt_rate(substr(m.zip1_line_4, 1, 5), m.city1_line_4),\n                  st.state_tax,\n                  st.county_tax,\n                  st.county_local_tax,\n                  st.city_tax,\n                  st.city_local_tax,\n                  cg.cg_charge_amount\n        order by m.merchant_number asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.CBTSalesTaxReport",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.CBTSalesTaxReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^7*/
      
      rs = it.getResultSet();
      
      // create csv file in memory
      CSVFileMemory csv = new CSVFileMemory(rs, true);
      
      // send file to recipients
      String        subject = "CB&T Sales Tax Report";
      StringBuffer  body    = new StringBuffer();
      body.append("Attached is the previous month's sales tax report for CB&T.\n\n");
      body.append("Thanks!\n\n");
      body.append("John Firman\n");
      body.append("Merchant e-Solutions\n");
      body.append("jfirman@merchante-solutions.com\n");
      
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(MesEmails.MSG_ADDRS_CBT_SALES_TAX_REPORT);
      msg.setSubject(subject);
      msg.setText(body.toString());
      msg.addStringAsTextFile("edwin_seagul_cbt_tax_report.csv", csv.toString());
      
      msg.send();
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
}/*@lineinfo:generated-code*/