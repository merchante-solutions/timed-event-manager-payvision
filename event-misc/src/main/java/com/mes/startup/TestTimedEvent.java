/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TestTimedEvent.java $

  Description:  
  

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-07-20 10:07:58 -0700 (Mon, 20 Jul 2009) $
  Version            : $Revision: 16324 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import org.apache.log4j.Logger;

public class TestTimedEvent extends EventBase
{
  static Logger log = Logger.getLogger(TestTimedEvent.class);

  public TestTimedEvent()
  {
  }
  
  public boolean execute()
  {
    int       bankNumber    = 0;
    
    try
    {
      bankNumber = Integer.parseInt( getEventArgs() );
      log.debug("---Starting execution of TestTimedEvent");
      log.debug("  eventArgs  : " + getEventArgs());
      log.debug("  bankNumber : " + bankNumber);
      
      // sleep for 45 seconds
      Thread.sleep(45000);
      
      log.debug("---TestTimedEvent is complete");
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    return true;
  }
}
