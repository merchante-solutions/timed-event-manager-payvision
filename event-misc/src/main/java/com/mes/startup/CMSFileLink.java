/*@lineinfo:filename=CMSFileLink*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/CMSFileLink.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-07-20 10:07:58 -0700 (Mon, 20 Jul 2009) $
  Version            : $Revision: 16324 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class CMSFileLink extends EventBase
{
  public CMSFileLink( )
  {
  }
  
  public boolean execute()
  {
    try
    {
      connect(true);
      
      linkTransactions();
    }
    catch( Exception e )
    {
      logEntry("execute()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( true );
  }
  
  protected void linkTransactions()
  {
    ResultSetIterator               it          = null;
    ResultSetIterator               it2         = null;
    int                             recCount    = 0;
    ResultSet                       resultSet   = null;
    ResultSet                       resultSet2  = null;
    int                             rowCount    = 0;
    int                             updateCount = 0;
    
    try
    {
      for( int queryId = 0; queryId < 2; ++queryId )
      {
        switch( queryId )
        {
          case 0:     // visa & mc items, get data from DDF
            /*@lineinfo:generated-code*//*@lineinfo:72^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.batch_date                       as batch_date,
//                        dt.batch_number                     as batch_number,
//                        dt.ddf_dt_id                        as ddf_dt_id,
//                        dt.cardholder_account_number        as card_number,
//                        dt.card_type                        as card_type,  
//                        decode( substr(dt.card_type,1,1),
//                                'V',0,'M',0,1 )             as payment_record,
//                        ( decode(dt.debit_credit_indicator,'C',-1,1) *
//                          dt.transaction_amount )           as tran_amount,
//                        nvl(dt.authorization_num,0)         as auth_code,
//                        dt.transaction_date                 as tran_date
//                from    daily_detail_file_dt dt
//                where   dt.merchant_account_number = 941000020471 and
//                        --dt.batch_date > (sysdate-60) and
//                        ( dt.purchase_id is null or
//                          substr(dt.purchase_id,1,1) = 'V' )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.batch_date                       as batch_date,\n                      dt.batch_number                     as batch_number,\n                      dt.ddf_dt_id                        as ddf_dt_id,\n                      dt.cardholder_account_number        as card_number,\n                      dt.card_type                        as card_type,  \n                      decode( substr(dt.card_type,1,1),\n                              'V',0,'M',0,1 )             as payment_record,\n                      ( decode(dt.debit_credit_indicator,'C',-1,1) *\n                        dt.transaction_amount )           as tran_amount,\n                      nvl(dt.authorization_num,0)         as auth_code,\n                      dt.transaction_date                 as tran_date\n              from    daily_detail_file_dt dt\n              where   dt.merchant_account_number = 941000020471 and\n                      --dt.batch_date > (sysdate-60) and\n                      ( dt.purchase_id is null or\n                        substr(dt.purchase_id,1,1) = 'V' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.CMSFileLink",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.CMSFileLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^13*/
            break;
            
          case 1:     // non-bank (amex & discover), get data from front end
            /*@lineinfo:generated-code*//*@lineinfo:94^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.batch_date                       as batch_date,
//                        dt.batch_number                     as batch_number,
//                        dt.row_sequence                     as ddf_dt_id,
//                        dt.card_number                      as card_number,
//                        dt.card_type                        as card_type,
//                        1                                   as payment_record,
//                        ( decode(dt.debit_credit_indicator,'C',-1,1) *
//                          dt.transaction_amount )           as tran_amount,
//                        nvl(dt.auth_code,0)                 as auth_code,
//                        dt.transaction_date                 as tran_date
//                from    daily_detail_file_ext_dt dt
//                where   dt.merchant_number = 941000020471 and
//                        not substr(dt.card_type,1,1) in ('V','M') and
//                        -- last day of non-bank in DDF for this account
//                        dt.batch_date > '20-MAR-2003' and  
//                        ( dt.purchase_id is null or
//                          substr(dt.purchase_id,1,1) = 'V' )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.batch_date                       as batch_date,\n                      dt.batch_number                     as batch_number,\n                      dt.row_sequence                     as ddf_dt_id,\n                      dt.card_number                      as card_number,\n                      dt.card_type                        as card_type,\n                      1                                   as payment_record,\n                      ( decode(dt.debit_credit_indicator,'C',-1,1) *\n                        dt.transaction_amount )           as tran_amount,\n                      nvl(dt.auth_code,0)                 as auth_code,\n                      dt.transaction_date                 as tran_date\n              from    daily_detail_file_ext_dt dt\n              where   dt.merchant_number = 941000020471 and\n                      not substr(dt.card_type,1,1) in ('V','M') and\n                      -- last day of non-bank in DDF for this account\n                      dt.batch_date > '20-MAR-2003' and  \n                      ( dt.purchase_id is null or\n                        substr(dt.purchase_id,1,1) = 'V' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.CMSFileLink",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.CMSFileLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^13*/
            break;
        }
        
        // this code allows for debug
        if ( it == null ) 
        {
          continue;
        }
        
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          ++recCount;
        
          /*@lineinfo:generated-code*//*@lineinfo:129^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(cms.rec_id)     
//              from    daily_detail_file_cms_dt cms
//              where   cms.dt_batch_date   = :resultSet.getDate("batch_date") and
//                      cms.dt_batch_number = :resultSet.getLong("batch_number") and
//                      cms.dt_ddf_dt_id    = :resultSet.getLong("ddf_dt_id")
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_3800 = resultSet.getDate("batch_date");
 long __sJT_3801 = resultSet.getLong("batch_number");
 long __sJT_3802 = resultSet.getLong("ddf_dt_id");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cms.rec_id)      \n            from    daily_detail_file_cms_dt cms\n            where   cms.dt_batch_date   =  :1  and\n                    cms.dt_batch_number =  :2  and\n                    cms.dt_ddf_dt_id    =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.CMSFileLink",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,__sJT_3800);
   __sJT_st.setLong(2,__sJT_3801);
   __sJT_st.setLong(3,__sJT_3802);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^11*/
        
          if ( rowCount == 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:140^13*/

//  ************************************************************
//  #sql [Ctx] it2 = { select  rec_id                      as rec_id, 
//                        itinerary_number            as it_num
//                from    daily_detail_file_cms_dt cms
//                where   cms.merchant_number = 941000020471 and
//    --                    cms.transaction_date = :(resultSet.getDate("tran_date")) and
//                        cms.transaction_amount = :resultSet.getDouble("tran_amount") and
//                        cms.transaction_type = 'I' and
//                        nvl(cms.auth_code,0) = :resultSet.getString("auth_code") and
//                        cms.dt_batch_date is null and
//                        cms.card_number = :resultSet.getString("card_number")
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_3803 = resultSet.getDouble("tran_amount");
 String __sJT_3804 = resultSet.getString("auth_code");
 String __sJT_3805 = resultSet.getString("card_number");
  try {
   String theSqlTS = "select  rec_id                      as rec_id, \n                      itinerary_number            as it_num\n              from    daily_detail_file_cms_dt cms\n              where   cms.merchant_number = 941000020471 and\n  --                    cms.transaction_date = :(resultSet.getDate(\"tran_date\")) and\n                      cms.transaction_amount =  :1  and\n                      cms.transaction_type = 'I' and\n                      nvl(cms.auth_code,0) =  :2  and\n                      cms.dt_batch_date is null and\n                      cms.card_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.CMSFileLink",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_3803);
   __sJT_st.setString(2,__sJT_3804);
   __sJT_st.setString(3,__sJT_3805);
   // execute query
   it2 = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.CMSFileLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:152^13*/
          }          
          else
          {
            /*@lineinfo:generated-code*//*@lineinfo:156^13*/

//  ************************************************************
//  #sql [Ctx] it2 = { select  rec_id                      as rec_id, 
//                        itinerary_number            as it_num
//                from    daily_detail_file_cms_dt cms
//                where   cms.dt_batch_date   = :resultSet.getDate("batch_date") and
//                        cms.dt_batch_number = :resultSet.getLong("batch_number") and
//                        cms.dt_ddf_dt_id    = :resultSet.getLong("ddf_dt_id")
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_3806 = resultSet.getDate("batch_date");
 long __sJT_3807 = resultSet.getLong("batch_number");
 long __sJT_3808 = resultSet.getLong("ddf_dt_id");
  try {
   String theSqlTS = "select  rec_id                      as rec_id, \n                      itinerary_number            as it_num\n              from    daily_detail_file_cms_dt cms\n              where   cms.dt_batch_date   =  :1  and\n                      cms.dt_batch_number =  :2  and\n                      cms.dt_ddf_dt_id    =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.CMSFileLink",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_3806);
   __sJT_st.setLong(2,__sJT_3807);
   __sJT_st.setLong(3,__sJT_3808);
   // execute query
   it2 = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.CMSFileLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:164^13*/
          }
          resultSet2 = it2.getResultSet();
          if( resultSet2.next() )
          {
            ++updateCount;
            
            // update the appropriate transaction table
            if ( queryId == 0 )     // bank card and legacy non-bank
            {
              /*@lineinfo:generated-code*//*@lineinfo:174^15*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_dt
//                  set     purchase_id   = :resultSet2.getString("it_num")
//                  where   batch_date    = :resultSet.getDate("batch_date") and
//                          batch_number  = :resultSet.getLong("batch_number") and
//                          ddf_dt_id     = :resultSet.getLong("ddf_dt_id")
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3809 = resultSet2.getString("it_num");
 java.sql.Date __sJT_3810 = resultSet.getDate("batch_date");
 long __sJT_3811 = resultSet.getLong("batch_number");
 long __sJT_3812 = resultSet.getLong("ddf_dt_id");
   String theSqlTS = "update  daily_detail_file_dt\n                set     purchase_id   =  :1 \n                where   batch_date    =  :2  and\n                        batch_number  =  :3  and\n                        ddf_dt_id     =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.CMSFileLink",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3809);
   __sJT_st.setDate(2,__sJT_3810);
   __sJT_st.setLong(3,__sJT_3811);
   __sJT_st.setLong(4,__sJT_3812);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:181^15*/
            }
            else                    // non-bank
            {
              /*@lineinfo:generated-code*//*@lineinfo:185^15*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_ext_dt
//                  set     purchase_id   = :resultSet2.getString("it_num")
//                  where   merchant_number = 941000020471 and
//                          batch_date    = :resultSet.getDate("batch_date") and
//                          batch_number  = :resultSet.getLong("batch_number") and
//                          row_sequence  = :resultSet.getLong("ddf_dt_id")
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3813 = resultSet2.getString("it_num");
 java.sql.Date __sJT_3814 = resultSet.getDate("batch_date");
 long __sJT_3815 = resultSet.getLong("batch_number");
 long __sJT_3816 = resultSet.getLong("ddf_dt_id");
   String theSqlTS = "update  daily_detail_file_ext_dt\n                set     purchase_id   =  :1 \n                where   merchant_number = 941000020471 and\n                        batch_date    =  :2  and\n                        batch_number  =  :3  and\n                        row_sequence  =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.CMSFileLink",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3813);
   __sJT_st.setDate(2,__sJT_3814);
   __sJT_st.setLong(3,__sJT_3815);
   __sJT_st.setLong(4,__sJT_3816);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:193^15*/
            }
            
            // if necessary, add the payment record
            if ( resultSet.getInt("payment_record") == 1 )
            {
              // check for an existing payment record for this transaction
              /*@lineinfo:generated-code*//*@lineinfo:200^15*/

//  ************************************************************
//  #sql [Ctx] { select  count(cms.transaction_type) 
//                  from    daily_detail_file_cms_dt    cms
//                  where   cms.transaction_type = 'P' and
//                          cms.itinerary_number = :resultSet2.getString("it_num") and
//                          cms.transaction_date = :resultSet.getDate("tran_date") and
//                          cms.transaction_amount = :resultSet.getDouble("tran_amount") and
//                          nvl(cms.card_number,'na') in ( 'na', :resultSet.getString("card_number") )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3817 = resultSet2.getString("it_num");
 java.sql.Date __sJT_3818 = resultSet.getDate("tran_date");
 double __sJT_3819 = resultSet.getDouble("tran_amount");
 String __sJT_3820 = resultSet.getString("card_number");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cms.transaction_type)  \n                from    daily_detail_file_cms_dt    cms\n                where   cms.transaction_type = 'P' and\n                        cms.itinerary_number =  :1  and\n                        cms.transaction_date =  :2  and\n                        cms.transaction_amount =  :3  and\n                        nvl(cms.card_number,'na') in ( 'na',  :4  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.CMSFileLink",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3817);
   __sJT_st.setDate(2,__sJT_3818);
   __sJT_st.setDouble(3,__sJT_3819);
   __sJT_st.setString(4,__sJT_3820);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:209^15*/
              
              if ( rowCount == 0 )
              {
                /*@lineinfo:generated-code*//*@lineinfo:213^17*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_cms_dt
//                    ( 
//                      merchant_number,
//                      batch_date,
//                      transaction_date,
//                      transaction_type,
//                      transaction_amount,
//                      card_number,
//                      card_type,
//                      auth_code,
//                      itinerary_number,
//                      dt_ddf_dt_id,
//                      dt_batch_date,
//                      dt_batch_number,
//                      load_filename
//                    )
//                    values
//                    ( 
//                      941000020471,
//                      :resultSet.getDate("batch_date"),
//                      :resultSet.getDate("tran_date"),
//                      'P',
//                      :resultSet.getDouble("tran_amount"),
//                      :resultSet.getString("card_number"),
//                      :resultSet.getString("card_type"),
//                      :resultSet.getString("auth_code"),
//                      :resultSet2.getString("it_num"),
//                      :resultSet.getLong("ddf_dt_id"),
//                      :resultSet.getDate("batch_date"),
//                      :resultSet.getLong("batch_number"),
//                      'com.mes.startup.CMSFileLink'
//                    )
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_3821 = resultSet.getDate("batch_date");
 java.sql.Date __sJT_3822 = resultSet.getDate("tran_date");
 double __sJT_3823 = resultSet.getDouble("tran_amount");
 String __sJT_3824 = resultSet.getString("card_number");
 String __sJT_3825 = resultSet.getString("card_type");
 String __sJT_3826 = resultSet.getString("auth_code");
 String __sJT_3827 = resultSet2.getString("it_num");
 long __sJT_3828 = resultSet.getLong("ddf_dt_id");
 java.sql.Date __sJT_3829 = resultSet.getDate("batch_date");
 long __sJT_3830 = resultSet.getLong("batch_number");
   String theSqlTS = "insert into daily_detail_file_cms_dt\n                  ( \n                    merchant_number,\n                    batch_date,\n                    transaction_date,\n                    transaction_type,\n                    transaction_amount,\n                    card_number,\n                    card_type,\n                    auth_code,\n                    itinerary_number,\n                    dt_ddf_dt_id,\n                    dt_batch_date,\n                    dt_batch_number,\n                    load_filename\n                  )\n                  values\n                  ( \n                    941000020471,\n                     :1 ,\n                     :2 ,\n                    'P',\n                     :3 ,\n                     :4 ,\n                     :5 ,\n                     :6 ,\n                     :7 ,\n                     :8 ,\n                     :9 ,\n                     :10 ,\n                    'com.mes.startup.CMSFileLink'\n                  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.CMSFileLink",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_3821);
   __sJT_st.setDate(2,__sJT_3822);
   __sJT_st.setDouble(3,__sJT_3823);
   __sJT_st.setString(4,__sJT_3824);
   __sJT_st.setString(5,__sJT_3825);
   __sJT_st.setString(6,__sJT_3826);
   __sJT_st.setString(7,__sJT_3827);
   __sJT_st.setLong(8,__sJT_3828);
   __sJT_st.setDate(9,__sJT_3829);
   __sJT_st.setLong(10,__sJT_3830);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:247^17*/
              }
            }
            
            // update the CMS table with the transaction link data
            /*@lineinfo:generated-code*//*@lineinfo:252^13*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_cms_dt
//                set     dt_batch_date    = :resultSet.getDate("batch_date"),
//                        dt_batch_number  = :resultSet.getLong("batch_number"),
//                        dt_ddf_dt_id     = :resultSet.getLong("ddf_dt_id")
//                where   rec_id           = :resultSet2.getLong("rec_id")
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_3831 = resultSet.getDate("batch_date");
 long __sJT_3832 = resultSet.getLong("batch_number");
 long __sJT_3833 = resultSet.getLong("ddf_dt_id");
 long __sJT_3834 = resultSet2.getLong("rec_id");
   String theSqlTS = "update  daily_detail_file_cms_dt\n              set     dt_batch_date    =  :1 ,\n                      dt_batch_number  =  :2 ,\n                      dt_ddf_dt_id     =  :3 \n              where   rec_id           =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.CMSFileLink",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_3831);
   __sJT_st.setLong(2,__sJT_3832);
   __sJT_st.setLong(3,__sJT_3833);
   __sJT_st.setLong(4,__sJT_3834);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:259^13*/
          }
          resultSet2.close();
          it2.close();
        
          //System.out.print("Processed (" + queryId + ")" + recCount + " Updated " + updateCount + "\r");
        }
        resultSet.close();
        it.close();
        it = null;    // reset it reference
      
        /*@lineinfo:generated-code*//*@lineinfo:270^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:273^9*/
      }     // end for loop through card type sources (DDF, front end)
    }
    catch( Exception e )
    {
      logEntry("linkTransactions",e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
      try{it2.close();}catch(Exception e){}
    }
  }
}/*@lineinfo:generated-code*/