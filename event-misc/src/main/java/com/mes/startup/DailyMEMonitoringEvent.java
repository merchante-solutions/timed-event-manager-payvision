/*@lineinfo:filename=DailyMEMonitoringEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/PMMonitoringEvent.sqlj $

  Description:

  Last Modified By   : $Author: swaxman $
  Last Modified Date : $Date: 2014-08-08 $
  Version            : $Revision: 17600 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

 **************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;
import javax.mail.Message.RecipientType;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.support.PropertiesFile;
import com.mes.support.SMTPMail;
import sqlj.runtime.ResultSetIterator;

public class DailyMEMonitoringEvent extends EventBase
{
	private static String DEFAULT_EMAIL_HOST;
	private static Logger log = Logger.getLogger(DailyMEMonitoringEvent.class);

	public boolean TestMode = false;

	private Vector queryResults = new Vector();
	private SftpFilesCheck sftpobject = null;
	private String runType = "0";
	private String senderEmailAddress = "DevOpsTeam@merchante-solutions.com";
	private String devOpsNotices = "";
	private String devOpsAlerts =  "";
	private Date firstOfMonth;
	
	public boolean allPass;
	public boolean zeroDiscountResult;
	public boolean dualDiscountResult;
	public boolean distinctDualDiscountResult;
	public boolean backfillDualDiscountResult;
	public boolean processTypeCountResult;
	public boolean discountRateZeroResult;
	public boolean ddfCountResult;
	public boolean missingSummarizationResult;
	public boolean hangingSummarizationsResult;
	
	public DailyMEMonitoringEvent() {
	  try {
	  devOpsNotices = MesDefaults.getString(MesDefaults.DAILY_ME_MONITORING_DEVOPS_NOTICES_EMAIL);
	  devOpsAlerts  = MesDefaults.getString(MesDefaults.DAILY_ME_MONITORING_DEVOPS_ALERTS_EMAIL);
	  } catch (Exception e) {
	    logEntry("DailyMEMonitoringEvent() constructor - set devOps notices' email alerts' email", e.toString());
	  }
	}
	
	public void getZeroDiscountResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:78^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '27'
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '27'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:81^4*/
			zeroDiscountResult = false;
			String[] cur = new String[2];
			cur[0] = ""; 
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:86^4*/

//  ************************************************************
//  #sql [Ctx] it = { select mds.* 
//  					from   mbs_daily_summary mds 
//  					where  mds.activity_date  between :firstOfMonth and last_day(:firstOfMonth)
//  					       and mds.item_type = 101 
//  					       and mds.rate = 0 
//  					       and exists 
//  					       ( select pr.merchant_number 
//  					         from   mbs_pricing pr 
//  					         where  pr.merchant_number = mds.merchant_number 
//  					                and pr.item_type = mds.item_type 
//  					                and pr.item_subclass = mds.item_subclass 
//  					                and pr.rate != 0 
//  					                and mds.activity_date between pr.valid_date_begin and pr.valid_date_end 
//  					       )
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select mds.* \n\t\t\t\t\tfrom   mbs_daily_summary mds \n\t\t\t\t\twhere  mds.activity_date  between  :1  and last_day( :2 )\n\t\t\t\t\t       and mds.item_type = 101 \n\t\t\t\t\t       and mds.rate = 0 \n\t\t\t\t\t       and exists \n\t\t\t\t\t       ( select pr.merchant_number \n\t\t\t\t\t         from   mbs_pricing pr \n\t\t\t\t\t         where  pr.merchant_number = mds.merchant_number \n\t\t\t\t\t                and pr.item_type = mds.item_type \n\t\t\t\t\t                and pr.item_subclass = mds.item_subclass \n\t\t\t\t\t                and pr.rate != 0 \n\t\t\t\t\t                and mds.activity_date between pr.valid_date_begin and pr.valid_date_end \n\t\t\t\t\t       )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,firstOfMonth);
   __sJT_st.setDate(2,firstOfMonth);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.DailyMEMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:102^5*/

			rs = it.getResultSet();

			if(!rs.next()) {
				cur[1] = "<span style='background-color:green'>No results returned.</span>";
				zeroDiscountResult = true;
			} else {
				zeroDiscountResult = false;
				cur[1] += rs.getString("rec_id");
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:118^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  					creation_date,
//  					text,
//  					achnowledgement,
//  					pass,
//  					error_type
//  				)
//  				values
//  				(
//  					sysdate,
//  					:cur[1],
//  					'N',
//  					:zeroDiscountResult ? "Y" : "N",
//  					27
//  				)
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4071 = cur[1];
 String __sJT_4072 = zeroDiscountResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\tcreation_date,\n\t\t\t\t\ttext,\n\t\t\t\t\tachnowledgement,\n\t\t\t\t\tpass,\n\t\t\t\t\terror_type\n\t\t\t\t)\n\t\t\t\tvalues\n\t\t\t\t(\n\t\t\t\t\tsysdate,\n\t\t\t\t\t :1 ,\n\t\t\t\t\t'N',\n\t\t\t\t\t :2 ,\n\t\t\t\t\t27\n\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4071);
   __sJT_st.setString(2,__sJT_4072);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:135^4*/
			if(!zeroDiscountResult) {
				sendAlertEmail(!zeroDiscountResult, "One or More Merchants with 0 discount rate.");
			}
		} catch (Exception e) {
			logEntry("getZeroDiscountRateResult()", e.toString());
		}
	}
	
	public void getDualDiscountResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:146^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '28'
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '28'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:149^4*/
			dualDiscountResult = false;
			String[] cur = new String[2];
			cur[0] = ""; 
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:154^4*/

//  ************************************************************
//  #sql [Ctx] it = { select sysdate, mds.merchant_number, mds.item_subclass, count(distinct mds.rate)
//  					from mbs_daily_summary mds, mif mif
//  					where mds.activity_date between :firstOfMonth and last_day(:firstOfMonth)
//  					and mif.merchant_number = mds.merchant_number
//  					and mif.bank_number in (3941, 3942, 3943, 3858, 3003)
//  					and item_type = 101
//  					group by sysdate, mds.merchant_number, mds.item_subclass
//  					having count(distinct rate) > 1
//  					order by mds.merchant_number
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sysdate, mds.merchant_number, mds.item_subclass, count(distinct mds.rate)\n\t\t\t\t\tfrom mbs_daily_summary mds, mif mif\n\t\t\t\t\twhere mds.activity_date between  :1  and last_day( :2 )\n\t\t\t\t\tand mif.merchant_number = mds.merchant_number\n\t\t\t\t\tand mif.bank_number in (3941, 3942, 3943, 3858, 3003)\n\t\t\t\t\tand item_type = 101\n\t\t\t\t\tgroup by sysdate, mds.merchant_number, mds.item_subclass\n\t\t\t\t\thaving count(distinct rate) > 1\n\t\t\t\t\torder by mds.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,firstOfMonth);
   __sJT_st.setDate(2,firstOfMonth);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.DailyMEMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^5*/

			rs = it.getResultSet();

			if(!rs.next()) {
				cur[1] = "<span style='background-color:green'>No results returned.</span>";
				dualDiscountResult = true;
			} else {
				dualDiscountResult = false;
				cur[1] += rs.getString("merchant_number");
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:181^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  					creation_date,
//  					text,
//  					achnowledgement,
//  					pass,
//  					error_type
//  				)
//  				values
//  				(
//  					sysdate,
//  					:cur[1],
//  					'N',
//  					:dualDiscountResult ? "Y" : "N",
//  					28
//  				)
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4073 = cur[1];
 String __sJT_4074 = dualDiscountResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\tcreation_date,\n\t\t\t\t\ttext,\n\t\t\t\t\tachnowledgement,\n\t\t\t\t\tpass,\n\t\t\t\t\terror_type\n\t\t\t\t)\n\t\t\t\tvalues\n\t\t\t\t(\n\t\t\t\t\tsysdate,\n\t\t\t\t\t :1 ,\n\t\t\t\t\t'N',\n\t\t\t\t\t :2 ,\n\t\t\t\t\t28\n\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4073);
   __sJT_st.setString(2,__sJT_4074);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:198^4*/
			if(!dualDiscountResult) {
				sendAlertEmail(!dualDiscountResult, "One or More Merchants with Dual Discount Rates.");
			}
		} catch (Exception e) {
			logEntry("getDualDiscountResult()", e.toString());
		}
	}

	public void getDistinctDualDiscountResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:209^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '29'
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '29'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:212^6*/
					distinctDualDiscountResult = false;
					String[] cur = new String[2];
					cur[0] = ""; 
					cur[1] = "";
					/*@lineinfo:generated-code*//*@lineinfo:217^6*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct mds.merchant_number   
//  							from mbs_daily_summary mds, mif mif
//  							where mds.activity_date between :firstOfMonth and last_day(:firstOfMonth)
//  							and mif.merchant_number = mds.merchant_number
//  							and mif.bank_number in (3941, 3942, 3943, 3858, 3003)
//  							and item_type = 101
//  							group by sysdate, mds.merchant_number, mds.item_subclass
//  							having count(distinct rate) > 1
//  						 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct mds.merchant_number   \n\t\t\t\t\t\t\tfrom mbs_daily_summary mds, mif mif\n\t\t\t\t\t\t\twhere mds.activity_date between  :1  and last_day( :2 )\n\t\t\t\t\t\t\tand mif.merchant_number = mds.merchant_number\n\t\t\t\t\t\t\tand mif.bank_number in (3941, 3942, 3943, 3858, 3003)\n\t\t\t\t\t\t\tand item_type = 101\n\t\t\t\t\t\t\tgroup by sysdate, mds.merchant_number, mds.item_subclass\n\t\t\t\t\t\t\thaving count(distinct rate) > 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,firstOfMonth);
   __sJT_st.setDate(2,firstOfMonth);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.DailyMEMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:227^7*/

					rs = it.getResultSet();

					if(!rs.next()) {
						cur[1] = "<span style='background-color:green'>No results returned.</span>";
						distinctDualDiscountResult = true;
					} else {
						distinctDualDiscountResult = false;
						cur[1] += rs.getString("merchant_number");
					}
					queryResults.add(cur);

					rs.close();
					it.close();

					/*@lineinfo:generated-code*//*@lineinfo:243^6*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  						(
//  								creation_date,
//  								text,
//  								achnowledgement,
//  								pass,
//  								error_type
//  								)
//  								values
//  								(
//  										sysdate,
//  										:cur[1],
//  										'N',
//  										:distinctDualDiscountResult ? "Y" : "N",
//  										29
//  										)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4075 = cur[1];
 String __sJT_4076 = distinctDualDiscountResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\t\t\ttext,\n\t\t\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\t\t\tpass,\n\t\t\t\t\t\t\t\terror_type\n\t\t\t\t\t\t\t\t)\n\t\t\t\t\t\t\t\tvalues\n\t\t\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t\t\t29\n\t\t\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4075);
   __sJT_st.setString(2,__sJT_4076);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:260^6*/
					if(!distinctDualDiscountResult) {
						sendAlertEmail(!distinctDualDiscountResult, "One or More Distinct Merchants with Dual Discount Rates.");
					}
		} catch (Exception e) {
			logEntry("getDistinctDualDiscountResult()", e.toString());
		}
	}
	
	public void getBackfillDualDiscountResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:271^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '30'
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '30'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:274^6*/
					backfillDualDiscountResult = false;
					String[] cur = new String[2];
					cur[0] = ""; 
					cur[1] = "";
					/*@lineinfo:generated-code*//*@lineinfo:279^6*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct mds.merchant_number   
//  							from mbs_daily_summary mds, mif mif
//  							where mds.activity_date between :firstOfMonth and last_day(:firstOfMonth)
//  							and mif.merchant_number = mds.merchant_number
//  							and mif.bank_number in (3941, 3942, 3943, 3858, 3003)
//  							and item_type = 101
//  							group by sysdate, mds.merchant_number, mds.item_subclass
//  							having count(distinct rate) > 1
//  						 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct mds.merchant_number   \n\t\t\t\t\t\t\tfrom mbs_daily_summary mds, mif mif\n\t\t\t\t\t\t\twhere mds.activity_date between  :1  and last_day( :2 )\n\t\t\t\t\t\t\tand mif.merchant_number = mds.merchant_number\n\t\t\t\t\t\t\tand mif.bank_number in (3941, 3942, 3943, 3858, 3003)\n\t\t\t\t\t\t\tand item_type = 101\n\t\t\t\t\t\t\tgroup by sysdate, mds.merchant_number, mds.item_subclass\n\t\t\t\t\t\t\thaving count(distinct rate) > 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,firstOfMonth);
   __sJT_st.setDate(2,firstOfMonth);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.startup.DailyMEMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:289^7*/

					rs = it.getResultSet();

					if(!rs.next()) {
						cur[1] = "<span style='background-color:green'>No results returned.</span>";
						backfillDualDiscountResult = true;
					} else {
						backfillDualDiscountResult = false;
						cur[1] += rs.getString("merchant_number");
					}
					queryResults.add(cur);

					rs.close();
					it.close();

					/*@lineinfo:generated-code*//*@lineinfo:305^6*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  						(
//  								creation_date,
//  								text,
//  								achnowledgement,
//  								pass,
//  								error_type
//  								)
//  								values
//  								(
//  										sysdate,
//  										:cur[1],
//  										'N',
//  										:backfillDualDiscountResult ? "Y" : "N",
//  										30
//  										)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4077 = cur[1];
 String __sJT_4078 = backfillDualDiscountResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\t\t\ttext,\n\t\t\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\t\t\tpass,\n\t\t\t\t\t\t\t\terror_type\n\t\t\t\t\t\t\t\t)\n\t\t\t\t\t\t\t\tvalues\n\t\t\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t\t\t30\n\t\t\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4077);
   __sJT_st.setString(2,__sJT_4078);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:322^6*/
					if(!backfillDualDiscountResult) {
						sendAlertEmail(!backfillDualDiscountResult, "One or More Merchants found with Dual Discount Rates from backfill process.");
					}
		} catch (Exception e) {
			logEntry("getBackfillDualDiscountResult()", e.toString());
		}
	}
	
	public void getProcessTypeCountResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:333^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '31'
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '31'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:336^6*/
					processTypeCountResult = true;
					String errorText = "";
					String[] cur = new String[2];
					cur[0] = ""; 
					cur[1] = "<span style='background-color:green'>Query Not Run.</span>";
					Calendar now = Calendar.getInstance();
					if(now.get(Calendar.DAY_OF_MONTH) != 1) { 
						/*@lineinfo:generated-code*//*@lineinfo:344^7*/

//  ************************************************************
//  #sql [Ctx] it = { select trunc(process_begin_date) dat, process_type, count(*) as count
//  								from mbs_process mbs
//  								where  process_begin_date >= :firstOfMonth
//  								and (process_type != 4 or (process_type = 4 and hierarchy_node is null))
//  								and process_type  in (1,2,4,5,6)
//  								group by trunc(process_begin_date), process_type
//  								order by 1 desc, 2
//  							 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select trunc(process_begin_date) dat, process_type, count(*) as count\n\t\t\t\t\t\t\t\tfrom mbs_process mbs\n\t\t\t\t\t\t\t\twhere  process_begin_date >=  :1 \n\t\t\t\t\t\t\t\tand (process_type != 4 or (process_type = 4 and hierarchy_node is null))\n\t\t\t\t\t\t\t\tand process_type  in (1,2,4,5,6)\n\t\t\t\t\t\t\t\tgroup by trunc(process_begin_date), process_type\n\t\t\t\t\t\t\t\torder by 1 desc, 2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,firstOfMonth);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.startup.DailyMEMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:353^8*/

						rs = it.getResultSet();

						if(!rs.next()) {
							cur[1] = "<span style='background-color:red'>No results returned.</span>";
							processTypeCountResult = false;
						} else {
							boolean saturday;
							boolean sunday;
							Calendar date = Calendar.getInstance();
							int count;
							int procType ;
							SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy");
							cur[1] = "";
							do
							{
								if(now.get(Calendar.DAY_OF_MONTH) != rs.getDate("dat").toLocalDate().getDayOfMonth()) {
									date.setTime(rs.getDate("dat"));
									saturday = date.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY;
									sunday = date.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
									count = rs.getInt("count");
									procType = rs.getInt("process_type");
									switch(procType) {
									case 1:
										if(count < 35 || count > 36) {
											processTypeCountResult = false;
											errorText = df.format(date.getTime()) + ": " + procType + "," + count + "\n";
										}
										break;
									case 2:
										if(count < 3 || count > 4) {
											processTypeCountResult = false;
											errorText = df.format(date.getTime()) + ": " + procType + "," + count + "\n";
										}
										break;
									case 4:
										if(count < 11 || count > 14) {
											processTypeCountResult = false;
											errorText = df.format(date.getTime()) + ": " + procType + "," + count + "\n";
										}
										break;
									case 5:
										if(saturday) {
											if(count != 2) {
												processTypeCountResult = false; 
												errorText = df.format(date.getTime()) + ": " + procType + "," + count + "\n";
											}
										}
										else if(sunday) {
											if(count < 9 || count > 10) {
												processTypeCountResult = false;
												errorText = df.format(date.getTime()) + ": " + procType + "," + count + "\n";
											}
										}
										else {
											if(count < 11 || count > 14) {
												processTypeCountResult = false;
												errorText = df.format(date.getTime()) + ": " + procType + "," + count + "\n";
											}
										}
										break;
									case 6:
										if(sunday) {
											if(count < 2 || count > 3) {
												processTypeCountResult = false;
												errorText = df.format(date.getTime()) + ": " + procType + "," + count + "\n";
											}
										}
										else {
											if(count != 1) {
												processTypeCountResult = false;
												errorText = df.format(date.getTime()) + ": " + procType + "," + count + "\n";
											}
										}
									}
									cur[1] += df.format(date.getTime()) + "," + procType + "," + count + ";";
								}
							} while(rs.next());
						}
						queryResults.add(cur);

						rs.close();
						it.close();
					}

						/*@lineinfo:generated-code*//*@lineinfo:439^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  							(
//  									creation_date,
//  									text,
//  									achnowledgement,
//  									pass,
//  									error_type
//  									)
//  									values
//  									(
//  											sysdate,
//  											:cur[1],
//  											'N',
//  											:processTypeCountResult ? "Y" : "N",
//  											31
//  											)
//  						 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4079 = cur[1];
 String __sJT_4080 = processTypeCountResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\t\t\t\ttext,\n\t\t\t\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\t\t\t\tpass,\n\t\t\t\t\t\t\t\t\terror_type\n\t\t\t\t\t\t\t\t\t)\n\t\t\t\t\t\t\t\t\tvalues\n\t\t\t\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t\t\t\t31\n\t\t\t\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4079);
   __sJT_st.setString(2,__sJT_4080);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:456^7*/
						if(!processTypeCountResult) {
							sendAlertEmail(!processTypeCountResult, "One or More File Counts for one or more process types are out of bounds.\n" + errorText);
						}
		} catch (Exception e) {
			logEntry("getProcessTypeCountResult()", e.toString());
		}
	}
	
	public void getDiscountRateZeroResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:467^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '32'
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '32'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:470^6*/
					discountRateZeroResult = true;
					String[] cur = new String[2];
					cur[0] = ""; 
					cur[1] = "";
					/*@lineinfo:generated-code*//*@lineinfo:475^6*/

//  ************************************************************
//  #sql [Ctx] it = { select activity_date, count(*) count
//  							from mbs_daily_summary mds
//  							where mds.activity_date >= :firstOfMonth
//  							and mds.me_load_file_id = 0
//  							and mds.item_type = 101
//  							and mds.rate = 0
//  							group by activity_date
//  							order by 1 desc
//  						 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select activity_date, count(*) count\n\t\t\t\t\t\t\tfrom mbs_daily_summary mds\n\t\t\t\t\t\t\twhere mds.activity_date >=  :1 \n\t\t\t\t\t\t\tand mds.me_load_file_id = 0\n\t\t\t\t\t\t\tand mds.item_type = 101\n\t\t\t\t\t\t\tand mds.rate = 0\n\t\t\t\t\t\t\tgroup by activity_date\n\t\t\t\t\t\t\torder by 1 desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,firstOfMonth);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.startup.DailyMEMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:485^7*/

					rs = it.getResultSet();

					if(!rs.next()) {
						cur[1] = "<span style='background-color:red'>No results returned.</span>";
						discountRateZeroResult = false;
					} else {
						int count;
						do
						{
							count = rs.getInt("count");
							if(count < 1400 || count > 4100) {
								discountRateZeroResult = false;
							}
							cur[1] += rs.getDate("activity_date") + "," + count + ";";
						} while(rs.next());
					}
					queryResults.add(cur);

					rs.close();
					it.close();

					/*@lineinfo:generated-code*//*@lineinfo:508^6*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  						(
//  								creation_date,
//  								text,
//  								achnowledgement,
//  								pass,
//  								error_type
//  								)
//  								values
//  								(
//  										sysdate,
//  										:cur[1],
//  										'N',
//  										:discountRateZeroResult ? "Y" : "N",
//  										32
//  										)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4081 = cur[1];
 String __sJT_4082 = discountRateZeroResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\t\t\ttext,\n\t\t\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\t\t\tpass,\n\t\t\t\t\t\t\t\terror_type\n\t\t\t\t\t\t\t\t)\n\t\t\t\t\t\t\t\tvalues\n\t\t\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t\t\t32\n\t\t\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4081);
   __sJT_st.setString(2,__sJT_4082);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:525^6*/
					if(!discountRateZeroResult) {
						sendAlertEmail(!discountRateZeroResult, "Discount Rate Zero Count Out of Bounds.");
					}
		} catch (Exception e) {
			logEntry("getDiscountRateZeroResult()", e.toString());
		}
	}
	
	public void getDDFCountResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:536^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '33'
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '33'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:539^6*/
					ddfCountResult = true;
					String[] cur = new String[2];
					cur[0] = ""; 
					cur[1] = "";
					/*@lineinfo:generated-code*//*@lineinfo:544^6*/

//  ************************************************************
//  #sql [Ctx] it = { select ddf.batch_date, count(*) count
//  							from daily_detail_file_dt ddf
//  							where ddf.batch_date between  :firstOfMonth and last_day(:firstOfMonth)
//  							and discount_rate = 0
//  							group by batch_date
//  							order by 1 desc
//  						 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select ddf.batch_date, count(*) count\n\t\t\t\t\t\t\tfrom daily_detail_file_dt ddf\n\t\t\t\t\t\t\twhere ddf.batch_date between   :1  and last_day( :2 )\n\t\t\t\t\t\t\tand discount_rate = 0\n\t\t\t\t\t\t\tgroup by batch_date\n\t\t\t\t\t\t\torder by 1 desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,firstOfMonth);
   __sJT_st.setDate(2,firstOfMonth);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.startup.DailyMEMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:552^7*/

					rs = it.getResultSet();

					if(!rs.next()) {
						cur[1] = "<span style='background-color:red'>No results returned.</span>";
						ddfCountResult = false;
					} else {
						int count;
						do
						{
							count = rs.getInt("count");
							if(count < 25000 || count > 92000) {
								ddfCountResult = false;
							}
							cur[1] += rs.getDate("batch_date") + "," + count + ";";
						} while(rs.next());
					}
					queryResults.add(cur);

					rs.close();
					it.close();

					/*@lineinfo:generated-code*//*@lineinfo:575^6*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  						(
//  								creation_date,
//  								text,
//  								achnowledgement,
//  								pass,
//  								error_type
//  								)
//  								values
//  								(
//  										sysdate,
//  										:cur[1],
//  										'N',
//  										:ddfCountResult ? "Y" : "N",
//  										33
//  										)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4083 = cur[1];
 String __sJT_4084 = ddfCountResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\t\t\ttext,\n\t\t\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\t\t\tpass,\n\t\t\t\t\t\t\t\terror_type\n\t\t\t\t\t\t\t\t)\n\t\t\t\t\t\t\t\tvalues\n\t\t\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t\t\t33\n\t\t\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4083);
   __sJT_st.setString(2,__sJT_4084);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:592^6*/
					if(!ddfCountResult) {
						sendAlertEmail(!ddfCountResult, "Daily detail file dt discount rate zero count out of bounds.");
					}
		} catch (Exception e) {
			logEntry("getDDFCountResult()", e.toString());
		}
	}
	
	public void getMissingSummarizationResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:603^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '34'
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '34'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:606^6*/
					missingSummarizationResult = true;
					String[] cur = new String[2];
					cur[0] = ""; 
					cur[1] = "";
					/*@lineinfo:generated-code*//*@lineinfo:611^6*/

//  ************************************************************
//  #sql [Ctx] it = { select mds.data_source_id, mds.data_source
//  							from mbs_daily_summary mds, mif mf
//  							where mds.activity_date between :firstOfMonth and last_day(:firstOfMonth)
//  							and mds.merchant_number = mf.merchant_number
//  							and mf.bank_number in (3003,3858,3941,3942,3943)
//  							and (mds.data_source like 'cdf%' or mds.data_source like 'mddf%' or mds.data_source like '256s%')  
//  							group by mds.data_source_id, mds.data_source
//  							minus
//  							select ddf.load_file_id, ddf.load_filename
//  							from daily_detail_file_dt ddf
//  							where ddf.bank_number in (3003,3858,3941,3942,3943)
//  							and ddf.batch_date between :firstOfMonth and last_day(:firstOfMonth)
//  							group by ddf.load_file_id, ddf.load_filename
//  						 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select mds.data_source_id, mds.data_source\n\t\t\t\t\t\t\tfrom mbs_daily_summary mds, mif mf\n\t\t\t\t\t\t\twhere mds.activity_date between  :1  and last_day( :2 )\n\t\t\t\t\t\t\tand mds.merchant_number = mf.merchant_number\n\t\t\t\t\t\t\tand mf.bank_number in (3003,3858,3941,3942,3943)\n\t\t\t\t\t\t\tand (mds.data_source like 'cdf%' or mds.data_source like 'mddf%' or mds.data_source like '256s%')  \n\t\t\t\t\t\t\tgroup by mds.data_source_id, mds.data_source\n\t\t\t\t\t\t\tminus\n\t\t\t\t\t\t\tselect ddf.load_file_id, ddf.load_filename\n\t\t\t\t\t\t\tfrom daily_detail_file_dt ddf\n\t\t\t\t\t\t\twhere ddf.bank_number in (3003,3858,3941,3942,3943)\n\t\t\t\t\t\t\tand ddf.batch_date between  :3  and last_day( :4 )\n\t\t\t\t\t\t\tgroup by ddf.load_file_id, ddf.load_filename";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,firstOfMonth);
   __sJT_st.setDate(2,firstOfMonth);
   __sJT_st.setDate(3,firstOfMonth);
   __sJT_st.setDate(4,firstOfMonth);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.startup.DailyMEMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:626^7*/

					rs = it.getResultSet();

					if(!rs.next()) {
						cur[1] = "<span style='background-color:green'>No results returned.</span>";
						missingSummarizationResult &= true;
					} else {
						missingSummarizationResult = false;
						cur[1] += rs.getString("data_source");
					}
					queryResults.add(cur);

					rs.close();
					it.close();

					/*@lineinfo:generated-code*//*@lineinfo:642^6*/

//  ************************************************************
//  #sql [Ctx] it = { select ddf.load_file_id, ddf.load_filename
//  							from daily_detail_file_dt ddf
//  							where ddf.bank_number in (3003,3858,3941,3942,3943)
//  							and ddf.batch_date between :firstOfMonth and last_day(:firstOfMonth)
//  							group by ddf.load_file_id, ddf.load_filename
//  							minus
//  							select mds.data_source_id, mds.data_source
//  							from mbs_daily_summary mds, mif mf
//  							where mds.activity_date between :firstOfMonth and last_day(:firstOfMonth)
//  							and mds.merchant_number = mf.merchant_number
//  							and mf.bank_number in (3003,3858,3941,3942,3943)
//  							and (mds.data_source like 'cdf%' or mds.data_source like 'mddf%' or mds.data_source like '256s%')  
//  							group by mds.data_source_id, mds.data_source
//  						 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select ddf.load_file_id, ddf.load_filename\n\t\t\t\t\t\t\tfrom daily_detail_file_dt ddf\n\t\t\t\t\t\t\twhere ddf.bank_number in (3003,3858,3941,3942,3943)\n\t\t\t\t\t\t\tand ddf.batch_date between  :1  and last_day( :2 )\n\t\t\t\t\t\t\tgroup by ddf.load_file_id, ddf.load_filename\n\t\t\t\t\t\t\tminus\n\t\t\t\t\t\t\tselect mds.data_source_id, mds.data_source\n\t\t\t\t\t\t\tfrom mbs_daily_summary mds, mif mf\n\t\t\t\t\t\t\twhere mds.activity_date between  :3  and last_day( :4 )\n\t\t\t\t\t\t\tand mds.merchant_number = mf.merchant_number\n\t\t\t\t\t\t\tand mf.bank_number in (3003,3858,3941,3942,3943)\n\t\t\t\t\t\t\tand (mds.data_source like 'cdf%' or mds.data_source like 'mddf%' or mds.data_source like '256s%')  \n\t\t\t\t\t\t\tgroup by mds.data_source_id, mds.data_source";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,firstOfMonth);
   __sJT_st.setDate(2,firstOfMonth);
   __sJT_st.setDate(3,firstOfMonth);
   __sJT_st.setDate(4,firstOfMonth);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.startup.DailyMEMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:657^7*/

					rs = it.getResultSet();

					if(!rs.next()) {
						cur[1] += "<span style='background-color:green'>No results returned.</span>";
						missingSummarizationResult &= true;
					} else {
						missingSummarizationResult = false;
						cur[1] += rs.getString("load_filename");
					}
					queryResults.add(cur);

					rs.close();
					it.close();
					
					/*@lineinfo:generated-code*//*@lineinfo:673^6*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  						(
//  								creation_date,
//  								text,
//  								achnowledgement,
//  								pass,
//  								error_type
//  								)
//  								values
//  								(
//  										sysdate,
//  										:cur[1],
//  										'N',
//  										:missingSummarizationResult ? "Y" : "N",
//  										34
//  										)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4085 = cur[1];
 String __sJT_4086 = missingSummarizationResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\t\t\ttext,\n\t\t\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\t\t\tpass,\n\t\t\t\t\t\t\t\terror_type\n\t\t\t\t\t\t\t\t)\n\t\t\t\t\t\t\t\tvalues\n\t\t\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t\t\t34\n\t\t\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4085);
   __sJT_st.setString(2,__sJT_4086);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:690^6*/
					if(!missingSummarizationResult) {
						sendAlertEmail(!missingSummarizationResult, "One or more files not summarized in both dail_detail_file_dt and mbs_daily_summary.");
					}
		} catch (Exception e) {
			logEntry("getMissingSummarizationResult()", e.toString());
		}
	}
	
	public void getHangingSummarizationsResult(ResultSetIterator it, ResultSet rs) {
		try {
			/*@lineinfo:generated-code*//*@lineinfo:701^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '35'
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type = '35'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:704^6*/
					hangingSummarizationsResult = false;
					String[] cur = new String[2];
					cur[0] = ""; 
					cur[1] = "";
					String fileName = "%_" + new SimpleDateFormat("MM").format(Calendar.getInstance().getTime()) + "%_%";
					/*@lineinfo:generated-code*//*@lineinfo:710^6*/

//  ************************************************************
//  #sql [Ctx] it = { select *
//  							from mbs_process
//  							where ((process_begin_date >= :firstOfMonth and process_begin_date > sysdate - 1/48 and process_end_date is null) -- Summarization started but did not end 
//  							or process_begin_date is null  --- Summarization did not start at all
//  							or process_sequence = 0)
//  							and process_sequence != 999999212
//  							and load_filename like :fileName
//  						 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select *\n\t\t\t\t\t\t\tfrom mbs_process\n\t\t\t\t\t\t\twhere ((process_begin_date >=  :1  and process_begin_date > sysdate - 1/48 and process_end_date is null) -- Summarization started but did not end \n\t\t\t\t\t\t\tor process_begin_date is null  --- Summarization did not start at all\n\t\t\t\t\t\t\tor process_sequence = 0)\n\t\t\t\t\t\t\tand process_sequence != 999999212\n\t\t\t\t\t\t\tand load_filename like  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,firstOfMonth);
   __sJT_st.setString(2,fileName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.startup.DailyMEMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:719^7*/

					rs = it.getResultSet();

					if(!rs.next()) {
						cur[1] = "<span style='background-color:green'>No results returned.</span>";
						hangingSummarizationsResult = true;
					} else {
						hangingSummarizationsResult = false;
						cur[1] += rs.getString("load_filename");
					}
					queryResults.add(cur);

					rs.close();
					it.close();

					/*@lineinfo:generated-code*//*@lineinfo:735^6*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  						(
//  								creation_date,
//  								text,
//  								achnowledgement,
//  								pass,
//  								error_type
//  								)
//  								values
//  								(
//  										sysdate,
//  										:cur[1],
//  										'N',
//  										:hangingSummarizationsResult ? "Y" : "N",
//  										35
//  										)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4087 = cur[1];
 String __sJT_4088 = hangingSummarizationsResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\t\t\ttext,\n\t\t\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\t\t\tpass,\n\t\t\t\t\t\t\t\terror_type\n\t\t\t\t\t\t\t\t)\n\t\t\t\t\t\t\t\tvalues\n\t\t\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t\t\t35\n\t\t\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.startup.DailyMEMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4087);
   __sJT_st.setString(2,__sJT_4088);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:752^6*/
					if(!hangingSummarizationsResult) {
						sendAlertEmail(!hangingSummarizationsResult, "One or more hanging summarizations.");
					}
		} catch (Exception e) {
			logEntry("getHangingSummarizationsResult()", e.toString());
		}
	}
			
	/*
	 ** METHOD public void getData()
	 **
	 */
	public synchronized void getData()
	{
		ResultSetIterator it        = null;
		ResultSet         rs        = null;
		boolean           restrict  = false;
		Calendar temp = Calendar.getInstance();
//		temp.set(Calendar.DAY_OF_MONTH, 1);
		temp.add(Calendar.DAY_OF_MONTH, -1);
		firstOfMonth = new Date(temp.getTimeInMillis());

		try
		{
			loadProps();
			queryResults.clear();
			allPass = false;
			connect();

			getZeroDiscountResult(it, rs);
			getDualDiscountResult(it, rs);
			getDistinctDualDiscountResult(it, rs);
			getBackfillDualDiscountResult(it, rs);
			getProcessTypeCountResult(it, rs);
			getDiscountRateZeroResult(it, rs);
			getDDFCountResult(it, rs);
			getMissingSummarizationResult(it, rs);
			getHangingSummarizationsResult(it, rs);
			allPass();
			sendAlertEmail(!allPass, "");

		}
		catch(Exception e)
		{
			logEntry("getData()", e.toString());
		}
		finally
		{
			try { rs.close(); } catch(Exception e) {}
			try { it.close(); } catch(Exception e) {}
			cleanUp();
		}
	}

	/**
	 * METHOD allPass
	 * 
	 * Sets the allPass variable
	 */
	public void allPass() {
		allPass = zeroDiscountResult && dualDiscountResult && processTypeCountResult && discountRateZeroResult 
				&& ddfCountResult && missingSummarizationResult && hangingSummarizationsResult;
	}

	public void sendAlertEmail(boolean alert, String emailContext) {
		String  body             = "";
		try {
			// create a mailer object with the host parameters in MesDefaults
			String emailHost = MesDefaults.getString(MesDefaults.DK_SMTP_HOST);
			SMTPMail mailer = new SMTPMail(emailHost);
			mailer.setFrom(senderEmailAddress);

			if (alert){ 
				mailer.addRecipient(RecipientType.TO, devOpsAlerts);
				mailer.setSubject("Daily Month-End Alert");
			}
			else {
				mailer.addRecipient(RecipientType.TO, devOpsNotices);
				mailer.setSubject("Daily Month-End Notification");                           
			}

			if (!alert) {
				body += "\n";
				body += "------------------------------------------------------------" + "\n";
				body += "No Current Issues with Daily Month-End Checks.\n";
				body += "------------------------------------------------------------" + "\n";
				mailer.setText(body);
				mailer.send();
			}
			else {
				body += "\n";
				body += "One or more errors were found by the Daily Month-End Checks.\n";
				body += emailContext;
				body += "\n";
				mailer.setText(body);
				mailer.send();
			}
		} catch(Exception e) {
			logEntry("sendEmail()", e.toString());
		}
	}
	
  private void loadProps()
  {
    log.debug("in loadProps... ");
    try
    {
      PropertiesFile props = new PropertiesFile("monitoring.properties");
      
      DEFAULT_EMAIL_HOST = props.getString("DEFAULT_EMAIL_HOST");
      senderEmailAddress = props.getString("SENDER_EMAIL_ADDRESS", senderEmailAddress);
      devOpsNotices = props.getString("DEV_OPS_NOTICES_MONTHLY", devOpsNotices);
      devOpsAlerts = props.getString("DEV_OPS_ALERTS_MONTHLY", devOpsAlerts);
    }
    catch(Exception e)
    {
      logEntry("loadProps()", e.toString());
      log.error(e.getMessage());
    }
  }
  
	public boolean execute( )
	{
		boolean result = true;
		getData();
		return( result );
	}

	public static void main(String[] args)
	{
		try
		{
			if (args.length > 0 && args[0].equals("testproperties")) {
				EventBase.printKeyListStatus(new String[]{
						MesDefaults.DAILY_ME_MONITORING_DEVOPS_NOTICES_EMAIL,
						MesDefaults.DAILY_ME_MONITORING_DEVOPS_ALERTS_EMAIL,
						MesDefaults.DK_SMTP_HOST
				});
			}

			//			com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
			DailyMEMonitoringEvent worker = new DailyMEMonitoringEvent();
			worker.execute();
		}
		catch(Exception e)
		{
			System.out.println("main(): " + e.toString());
			log.error(e.getMessage());
		}
	}
}/*@lineinfo:generated-code*/