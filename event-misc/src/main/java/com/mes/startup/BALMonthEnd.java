/*@lineinfo:filename=BALMonthEnd*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/BALMonthEnd.sqlj $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/19/04 9:37a $
  Version            : $Revision: 1 $
  
  This class runs only on the last business day of the month to ensure
  all accounts get uploaded correctly

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class BALMonthEnd extends BatchAccountLoad
{
  protected boolean lastBusinessDayOfMonth()
  {
    boolean result = false;
    try
    {
      connect();
      
      String isLastBusDay = "";
      
      /*@lineinfo:generated-code*//*@lineinfo:44^7*/

//  ************************************************************
//  #sql [Ctx] { select  is_last_business_day(sysdate)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  is_last_business_day(sysdate)\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.BALMonthEnd",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   isLastBusDay = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:49^7*/
      
      result = (isLastBusDay.equals("Y"));
    }
    catch(Exception e)
    {
      logEntry("lastBusinessDayOfMonth()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return result;
  }
  
  public boolean execute()
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    boolean           result  = false;
    
    try
    {
      connect();
      
      if(lastBusinessDayOfMonth())
      {
        /*@lineinfo:generated-code*//*@lineinfo:76^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  class_name
//            from    mas_banks
//            where   nvl(special_month_end, 'N') = 'Y'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  class_name\n          from    mas_banks\n          where   nvl(special_month_end, 'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.BALMonthEnd",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.BALMonthEnd",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:81^9*/
    
        rs = it.getResultSet();
    
        while(rs.next())
        {
          bankFiles.add(rs.getString("class_name"));
        }
    
        rs.close();
        it.close();
    
        for(int i=0; i<bankFiles.size(); ++i)
        {
          processTapeFile((String)(bankFiles.elementAt(i)));
        }
        
        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
}/*@lineinfo:generated-code*/