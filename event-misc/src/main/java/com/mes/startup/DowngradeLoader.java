/*@lineinfo:filename=DowngradeLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/DowngradeLoader.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class DowngradeLoader extends EventBase
{
  static Logger log = Logger.getLogger(DowngradeLoader.class);
  
  public boolean execute()
  {
    ProcessTable.ProcessTableEntry  entry         = null;
    boolean                         retVal        = false;
    ProcessTable                    pt            = null;

    try
    {
      connect();
      
      pt = new ProcessTable("downgrade_process","downgrade_process_sequence",ProcessTable.PT_ALL);
      
      if ( pt.hasPendingEntries() )
      {
        pt.preparePendingEntries();
        
        Vector entries = pt.getEntryVector();
        for( int i = 0; i < entries.size(); ++i )
        {
          entry = (ProcessTable.ProcessTableEntry)entries.elementAt(i);
          
          switch( entry.getProcessType() )
          {
            case 0:   // link downgrades to transactions
              entry.recordTimestampBegin();
              linkDowngradesToTransactions( entry.getLoadFilename() );
              entry.recordTimestampEnd();
              break;
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected void linkDowngradesToTransactions( String loadFilename )
  {
    double                amount          = 0.0;
    int                   bankNumber      = getFileBankNumber( loadFilename );
    Date                  batchDate       = null;
    long                  batchNumber     = 0L;
    String                cardNumber      = null;
    long                  ddfDtId         = 0L;
    ResultSetIterator     it              = null;
    long                  loadFileId      = 0L;
    long                  loadSec         = 0L;
    long                  merchantId      = 0L;
    long                  nodeId          = (bankNumber * 100000);
    String                refNum          = null;
    ResultSet             resultSet       = null;
    Date                  tranDate        = null;
    long                  tsBegin         = System.currentTimeMillis();
    
    int                   recCount        = 0;
    long                  queryTimeBegin  = 0L;
    long                  queryTimeMax    = 0L;
    long                  queryTimeCurrent= 0L;
    long                  queryTimeTotal  = 0L;
    
    try
    {
      loadFileId = loadFilenameToLoadFileId(loadFilename);
      
      if ( loadFilename.startsWith("vs_") )
      {
        log.debug("Linking Visa Downgrades for file '" + loadFilename + "' (" + loadFileId + ")");
        /*@lineinfo:generated-code*//*@lineinfo:115^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  vd.load_sec           as load_sec,
//                    vd.source_batch_date  as source_date,
//                    vd.account_number     as card_number,
//                    vd.acquirer_ref_num   as ref_num,
//                    vd.source_amount      as tran_amount
//            from    visa_downgrades   vd
//            where   vd.load_file_id = :loadFileId and
//                    vd.dt_batch_date is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vd.load_sec           as load_sec,\n                  vd.source_batch_date  as source_date,\n                  vd.account_number     as card_number,\n                  vd.acquirer_ref_num   as ref_num,\n                  vd.source_amount      as tran_amount\n          from    visa_downgrades   vd\n          where   vd.load_file_id =  :1  and\n                  vd.dt_batch_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.DowngradeLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.DowngradeLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          loadSec     = resultSet.getLong("load_sec");
          batchDate   = resultSet.getDate("source_date");
          amount      = resultSet.getDouble("tran_amount");
          cardNumber  = resultSet.getString("card_number");
          refNum      = resultSet.getString("ref_num");
          
          try
          {
            queryTimeBegin = System.currentTimeMillis();
            /*@lineinfo:generated-code*//*@lineinfo:139^13*/

//  ************************************************************
//  #sql [Ctx] { select  /*+ 
//                            ordered 
//                            use_nl(gm dt) 
//                            index(dt idx_ddf_dt_bdate_merch_acct)
//                        */
//                        merchant_account_number, 
//                        batch_date, batch_number, ddf_dt_id 
//                
//                from    organization          o,
//                        group_merchant        gm,
//                        daily_detail_file_dt  dt
//                where   o.org_group = :nodeId 
//                        and gm.org_num = o.org_num 
//                        and dt.merchant_account_number = gm.merchant_number
//                        and dt.batch_date between :batchDate and 
//                                                  (:batchDate + 3) 
//                        and dt.cardholder_account_number = :cardNumber
//                        and dt.reference_number = :refNum 
//                        and dt.transaction_amount = :amount
//                        and not exists
//                        (
//                          select  vd.load_sec
//                          from    visa_downgrades vd
//                          where   vd.dt_batch_date = dt.batch_date and
//                                  vd.dt_batch_number = dt.batch_number and
//                                  vd.dt_ddf_dt_id = dt.ddf_dt_id
//                        )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  /*+ \n                          ordered \n                          use_nl(gm dt) \n                          index(dt idx_ddf_dt_bdate_merch_acct)\n                      */\n                      merchant_account_number, \n                      batch_date, batch_number, ddf_dt_id \n               \n              from    organization          o,\n                      group_merchant        gm,\n                      daily_detail_file_dt  dt\n              where   o.org_group =  :1  \n                      and gm.org_num = o.org_num \n                      and dt.merchant_account_number = gm.merchant_number\n                      and dt.batch_date between  :2  and \n                                                ( :3  + 3) \n                      and dt.cardholder_account_number =  :4 \n                      and dt.reference_number =  :5  \n                      and dt.transaction_amount =  :6 \n                      and not exists\n                      (\n                        select  vd.load_sec\n                        from    visa_downgrades vd\n                        where   vd.dt_batch_date = dt.batch_date and\n                                vd.dt_batch_number = dt.batch_number and\n                                vd.dt_ddf_dt_id = dt.ddf_dt_id\n                      )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.DowngradeLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,batchDate);
   __sJT_st.setDate(3,batchDate);
   __sJT_st.setString(4,cardNumber);
   __sJT_st.setString(5,refNum);
   __sJT_st.setDouble(6,amount);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   batchDate = (java.sql.Date)__sJT_rs.getDate(2);
   batchNumber = __sJT_rs.getLong(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   ddfDtId = __sJT_rs.getLong(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:169^13*/
            queryTimeCurrent = (System.currentTimeMillis() - queryTimeBegin);
            if ( queryTimeCurrent > queryTimeMax )
            {
              queryTimeMax = queryTimeCurrent;
            }
            queryTimeTotal += queryTimeCurrent;
            //System.out.print("Processed Count: " + ++recCount + "  Max Query Time: " + queryTimeMax + "  Avg Query Time: " + (queryTimeTotal/recCount) + "                 \r");//@
            
            /*@lineinfo:generated-code*//*@lineinfo:178^13*/

//  ************************************************************
//  #sql [Ctx] { update  visa_downgrades
//                set     card_acceptor_id = :merchantId,
//                        dt_batch_date = :batchDate,
//                        dt_batch_number = :batchNumber,
//                        dt_ddf_dt_id = :ddfDtId
//                where   load_sec = :loadSec                      
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  visa_downgrades\n              set     card_acceptor_id =  :1 ,\n                      dt_batch_date =  :2 ,\n                      dt_batch_number =  :3 ,\n                      dt_ddf_dt_id =  :4 \n              where   load_sec =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.DowngradeLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,batchDate);
   __sJT_st.setLong(3,batchNumber);
   __sJT_st.setLong(4,ddfDtId);
   __sJT_st.setLong(5,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:186^13*/
            
            /*@lineinfo:generated-code*//*@lineinfo:188^13*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_dt
//                set     downgrade_rec_id = :loadSec
//                where   batch_date = :batchDate 
//                        and batch_number = :batchNumber
//                        and ddf_dt_id = :ddfDtId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  daily_detail_file_dt\n              set     downgrade_rec_id =  :1 \n              where   batch_date =  :2  \n                      and batch_number =  :3 \n                      and ddf_dt_id =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.DowngradeLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setDate(2,batchDate);
   __sJT_st.setLong(3,batchNumber);
   __sJT_st.setLong(4,ddfDtId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:195^13*/
          }
          catch( Exception ee )
          {
            // ignore, did not find a match
          }            
        }
        resultSet.close();
      }
      else if ( loadFilename.startsWith("mc_") )
      {
        log.debug("Linking MasterCard Downgrades");
        /*@lineinfo:generated-code*//*@lineinfo:207^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  md.load_sec           as load_sec,
//                    md.merchant_number    as merchant_number,
//                    md.transaction_date   as tran_date,
//                    md.business_date      as source_date,
//                    md.card_number        as card_number,
//                    md.reference_number   as ref_num,
//                    md.transaction_amount as tran_amount
//            from    mc_downgrades   md
//            where   md.load_file_id = :loadFileId 
//                    and not md.business_date is null
//                    and md.dt_batch_date is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  md.load_sec           as load_sec,\n                  md.merchant_number    as merchant_number,\n                  md.transaction_date   as tran_date,\n                  md.business_date      as source_date,\n                  md.card_number        as card_number,\n                  md.reference_number   as ref_num,\n                  md.transaction_amount as tran_amount\n          from    mc_downgrades   md\n          where   md.load_file_id =  :1  \n                  and not md.business_date is null\n                  and md.dt_batch_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.DowngradeLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.DowngradeLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:220^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          loadSec     = resultSet.getLong("load_sec");
          batchDate   = resultSet.getDate("source_date");
          tranDate    = resultSet.getDate("tran_date");
          amount      = resultSet.getDouble("tran_amount");
          cardNumber  = resultSet.getString("card_number");
          refNum      = resultSet.getString("ref_num");
          merchantId  = resultSet.getLong("merchant_number");
          
          try
          {
            queryTimeBegin = System.currentTimeMillis();
            /*@lineinfo:generated-code*//*@lineinfo:236^13*/

//  ************************************************************
//  #sql [Ctx] { select  /*+ 
//                            ordered 
//                            use_nl(mf dt) 
//                        */
//                        merchant_account_number, 
//                        batch_date, batch_number, ddf_dt_id 
//                
//                from    daily_detail_file_dt  dt
//                where   dt.merchant_account_number = :merchantId
//                        and dt.batch_date between :tranDate and 
//                                                  (:batchDate + 3) 
//                        and dt.cardholder_account_number = :cardNumber
//                        and dt.reference_number = :refNum 
//                        and dt.transaction_amount = :amount
//                        and not exists
//                        (
//                          select  md.load_sec
//                          from    mc_downgrades md
//                          where   md.dt_batch_date = dt.batch_date and
//                                  md.dt_batch_number = dt.batch_number and
//                                  md.dt_ddf_dt_id = dt.ddf_dt_id
//                        )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  /*+ \n                          ordered \n                          use_nl(mf dt) \n                      */\n                      merchant_account_number, \n                      batch_date, batch_number, ddf_dt_id \n               \n              from    daily_detail_file_dt  dt\n              where   dt.merchant_account_number =  :1 \n                      and dt.batch_date between  :2  and \n                                                ( :3  + 3) \n                      and dt.cardholder_account_number =  :4 \n                      and dt.reference_number =  :5  \n                      and dt.transaction_amount =  :6 \n                      and not exists\n                      (\n                        select  md.load_sec\n                        from    mc_downgrades md\n                        where   md.dt_batch_date = dt.batch_date and\n                                md.dt_batch_number = dt.batch_number and\n                                md.dt_ddf_dt_id = dt.ddf_dt_id\n                      )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.DowngradeLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,tranDate);
   __sJT_st.setDate(3,batchDate);
   __sJT_st.setString(4,cardNumber);
   __sJT_st.setString(5,refNum);
   __sJT_st.setDouble(6,amount);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   batchDate = (java.sql.Date)__sJT_rs.getDate(2);
   batchNumber = __sJT_rs.getLong(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   ddfDtId = __sJT_rs.getLong(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:261^13*/
            queryTimeCurrent = (System.currentTimeMillis() - queryTimeBegin);
            if ( queryTimeCurrent > queryTimeMax )
            {
              queryTimeMax = queryTimeCurrent;
            }
            queryTimeTotal += queryTimeCurrent;
            //System.out.print("Processed Count: " + ++recCount + "  Max Query Time: " + queryTimeMax + "  Avg Query Time: " + (queryTimeTotal/recCount) + "                 \r");//@
            
            /*@lineinfo:generated-code*//*@lineinfo:270^13*/

//  ************************************************************
//  #sql [Ctx] { update  mc_downgrades
//                set     dt_batch_date = :batchDate,
//                        dt_batch_number = :batchNumber,
//                        dt_ddf_dt_id = :ddfDtId
//                where   load_sec = :loadSec                      
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mc_downgrades\n              set     dt_batch_date =  :1 ,\n                      dt_batch_number =  :2 ,\n                      dt_ddf_dt_id =  :3 \n              where   load_sec =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.DowngradeLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,batchDate);
   __sJT_st.setLong(2,batchNumber);
   __sJT_st.setLong(3,ddfDtId);
   __sJT_st.setLong(4,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:277^13*/
            
            /*@lineinfo:generated-code*//*@lineinfo:279^13*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_dt
//                set     downgrade_rec_id = :loadSec
//                where   batch_date = :batchDate 
//                        and batch_number = :batchNumber
//                        and ddf_dt_id = :ddfDtId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  daily_detail_file_dt\n              set     downgrade_rec_id =  :1 \n              where   batch_date =  :2  \n                      and batch_number =  :3 \n                      and ddf_dt_id =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.DowngradeLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setDate(2,batchDate);
   __sJT_st.setLong(3,batchNumber);
   __sJT_st.setLong(4,ddfDtId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:286^13*/
          }
          catch( Exception ee )
          {
            // ignore, did not find a match
            log.error(ee.toString());
          }            
        }
        resultSet.close();
      }
      
      long elapsed = (System.currentTimeMillis() - tsBegin);
      log.debug("Elapsed: " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    catch( Exception e )
    {
      // ignore?
      logEntry("linkDowngradesToTransactions( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e){}
    }
  }
  
  public static void main( String[] args )
  {
    DowngradeLoader    loader   = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      loader = new DowngradeLoader();
      loader.connect(true);
      
      if ( args.length > 0 && args[0].equals("linkDowngrades") )
      {
        log.debug("linkDowngradesToTransactions( " + args[1] + " )");
        loader.linkDowngradesToTransactions(args[1]);
      }
      else
      {
        log.debug("execute()");
        loader.execute();
      }
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
      try{ loader.cleanUp(); }catch( Exception e ){}
    }
  }
}/*@lineinfo:generated-code*/