/*@lineinfo:filename=ExtractorHandler*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.tools.FileUtils;
import com.mes.utils.DataExtractor;
import com.mes.utils.IPMExtractor;
import com.mes.utils.VisaRejectExtractor;

public class ExtractorHandler extends EventBase
{
  static Logger log = Logger.getLogger(ExtractorHandler.class);

  private   List Extractors;
  protected List mcFilenames   = new ArrayList();
  protected List vsFilenames   = new ArrayList();

  public ExtractorHandler()
  {
  }

  public ExtractorHandler(String[] incFiles)
  {
    if(incFiles!=null)
    {
      for(int i = 0; i < incFiles.length; i++)
      {
        String filename = incFiles[i];


        if( "dat".equalsIgnoreCase(FileUtils.getExtension(filename)) ||
            "ctf".equalsIgnoreCase(FileUtils.getExtension(filename)) )
        {
          vsFilenames.add(filename);
        }
        else if( "ipm".equalsIgnoreCase(FileUtils.getExtension(filename)) )
        {
          mcFilenames.add(filename);
        }
      }
    }
  }

  public boolean execute()
  {

    boolean result  = true;

    //load all Extractors
    loadExtractors();

    //get the iterator
    Iterator extractors = Extractors.iterator();

    //implementation
    DataExtractor de;

    while(extractors.hasNext())
    {
      de = (DataExtractor)extractors.next();
      try
      {
        //process the data
        de.extract();
      }
      catch(Exception e)
      {
        //might want more sophistication here
        log.debug(e.getMessage());
        logEntry(de.getClass().getName()+"(" + de.getDataSource() + ")", e.toString());
        result = false;
      }
    }

    return result;
  }


  //hardcoded for MC IPM extraction right now...
  //probably stay this way, since we won't really need plug n play
  //but maybe move it to a /tools util to follow suit
  public void loadExtractors()
  {
    Extractors = new ArrayList();

    //this might go into a parent class process for chargebacks
    //but for now we'll add it directly
    if(mcFilenames.size()>0)
    {
      Extractors.add(new IPMExtractor(mcFilenames));
    }

    if(vsFilenames.size()>0)
    {
      Extractors.add(new VisaRejectExtractor(vsFilenames));
    }

  }


  public static void main( String[] args )
  {
    ExtractorHandler        test          = null;

    try
    {
      SQLJConnectionBase.initStandalone();
      test = new ExtractorHandler(args);

      test.execute();
    }
    finally
    {
    }
  }


}/*@lineinfo:generated-code*/