/*@lineinfo:filename=MasterCardECPEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/MasterCardECPEvent.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-07-20 10:07:58 -0700 (Mon, 20 Jul 2009) $
  Version            : $Revision: 16324 $

  Change History:
     See VSS database

  Copyright (C) 2000-2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.FileOutputStream;
import java.sql.Date;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import masthead.util.XOMSerializer;
import nu.xom.Attribute;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Serializer;
import sqlj.runtime.ResultSetIterator;

public class MasterCardECPEvent extends EventBase
{
  static Logger log = Logger.getLogger(MasterCardECPEvent.class);

  private   String          TestFilename      = null;
  
  private void buildChargebackDetailElements(Element reportEl, long merchantId, Date activeDate )
  {
    Element               cbEl          = null;
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;
    Element               sectionEl     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:55^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  replace(cb.card_number,'xxxxxx','000000') as card_number,
//                  to_char(cb.incoming_date,'yyyy-mm-dd')    as incoming_date,
//                  to_char(cb.tran_amount,'FM9999999999.00') as tran_amount,
//                  to_char(trunc(:activeDate,'month'),'yyyy-mm') as cb_month
//          from    network_chargebacks     cb
//          where   cb.merchant_number = :merchantId and
//                  cb.incoming_date between trunc(:activeDate,'month') and last_day(:activeDate) and
//                  cb.card_type = 'MC' and
//                  nvl(cb.first_time_chargeback,'Y') = 'Y' and
//                  not exists  -- ignore REPR, REMC and FPAY
//                  (
//                    select  cba.cb_load_sec
//                    from    network_chargeback_activity   cba
//                    where   cba.cb_load_sec = cb.cb_load_sec and
//                            cba.action_code in ('S','C','Z')
//                  )                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  replace(cb.card_number,'xxxxxx','000000') as card_number,\n                to_char(cb.incoming_date,'yyyy-mm-dd')    as incoming_date,\n                to_char(cb.tran_amount,'FM9999999999.00') as tran_amount,\n                to_char(trunc( :1 ,'month'),'yyyy-mm') as cb_month\n        from    network_chargebacks     cb\n        where   cb.merchant_number =  :2  and\n                cb.incoming_date between trunc( :3 ,'month') and last_day( :4 ) and\n                cb.card_type = 'MC' and\n                nvl(cb.first_time_chargeback,'Y') = 'Y' and\n                not exists  -- ignore REPR, REMC and FPAY\n                (\n                  select  cba.cb_load_sec\n                  from    network_chargeback_activity   cba\n                  where   cba.cb_load_sec = cb.cb_load_sec and\n                          cba.action_code in ('S','C','Z')\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.MasterCardECPEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setDate(4,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.MasterCardECPEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:73^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        if ( sectionEl == null )
        {
          sectionEl = new Element("chargebacks");
          sectionEl.addAttribute( new Attribute("cbMonth",resultSet.getString("cb_month")) );
          reportEl.appendChild(sectionEl);
        }
        cbEl = new Element("chargeback");
        cbEl.appendChild( createElement("cbDate",resultSet.getString("incoming_date")) );
        cbEl.appendChild( createElement("cbAmount",resultSet.getString("tran_amount")) );
        cbEl.appendChild( createElement("cbAcctNumber",resultSet.getString("card_number")) );
        sectionEl.appendChild(cbEl);
      }
      
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("buildChargebackDetailElements(" + merchantId + "," + activeDate + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  private void buildXmlDocument( String filename, ResultSet resultSet )
    throws java.sql.SQLException
  {
    Date                  activeDate  = null;
    Element               el          = null;
    long                  merchantId  = 0L;
    Element               merchantEl  = null;
    Element               reportEl    = null;
    Element               root        = null;
    String                value       = null;

    try
    {  
      // emit enclosing elt
      root = new Element("ecpreports");
      root.setNamespaceURI( "http://www.mastercard.com/ECPReport" );
    
      while( resultSet.next() )
      {
        merchantId  = resultSet.getLong("merchant_number");
        activeDate  = resultSet.getDate("active_date");
      
        if ( resultSet.getString("new_report").equals("Y") )
        {
          reportEl = new Element("newReport");
          reportEl.addAttribute( new Attribute("merchantID",resultSet.getString("mc_merchant_number")) );
          reportEl.addAttribute( new Attribute("acquirerID",resultSet.getString("acq_id")) );
      
          merchantEl = new Element("merchant");
          value = resultSet.getString("dba_name");
          merchantEl.appendChild( createElement("merchantName",value) );
          merchantEl.appendChild( createElement("doingBusAs",value) );
          merchantEl.appendChild( createElement("ccBillDesc",value) );
      
          // add the address block
          el = new Element("address");
          el.appendChild( createElement("line1",resultSet.getString("addr_line1")) );
          value = resultSet.getString("addr_line2");
          if ( value != null && !value.equals("") )
          {
            el.appendChild( createElement("line2", value ) );
          }        
          el.appendChild( createElement("city",resultSet.getString("addr_city")) );
          el.appendChild( createElement("state",resultSet.getString("addr_state")) );
          el.appendChild( createElement("country",resultSet.getString("country")) );
          el.appendChild( createElement("postalCode",resultSet.getString("addr_zip")) );
          merchantEl.appendChild(el);
      
          merchantEl.appendChild( createElement("busPhone",resultSet.getString("phone_number")) );
          merchantEl.appendChild( createElement("catCode",resultSet.getString("sic_code")) );
          merchantEl.appendChild( createElement("contractOpen",resultSet.getString("date_opened")) );
      
          value = resultSet.getString("date_closed");
          if( value != null )
          {
            merchantEl.appendChild( createElement("contractClose",value) );
          }
      
          // add the principal complex element
          el = new Element("principal");
          el.appendChild( createElement("lastName",resultSet.getString("prin_last_name")) );
          el.appendChild( createElement("firstName",resultSet.getString("prin_first_name")) );
          el.appendChild( createElement("country",resultSet.getString("prin_country")) );
          merchantEl.appendChild(el);
          reportEl.appendChild(merchantEl);
          
          // add previous two months
          el = new Element("monthlySummaryData");
          el.appendChild( createElement("month",resultSet.getString("cm_active_date")) );
          el.appendChild( createElement("totSalesNum",resultSet.getString("cm_sales_count")) );
          el.appendChild( createElement("totSalesAmt",resultSet.getString("cm_sales_amount")) );
          el.appendChild( createElement("totCBNum",resultSet.getString("cm_cb_count")) );
          el.appendChild( createElement("totCBAmt",resultSet.getString("cm_cb_amount")) );
          el.appendChild( createElement("totCreditNum",resultSet.getString("cm_credits_count")) );
          el.appendChild( createElement("totCreditAmt",resultSet.getString("cm_credits_amount")) );
          reportEl.appendChild(el);
      
          el = new Element("monthlySummaryData");
          el.appendChild( createElement("month",resultSet.getString("lm_active_date")) );
          el.appendChild( createElement("totSalesNum",resultSet.getString("lm_sales_count")) );
          el.appendChild( createElement("totSalesAmt",resultSet.getString("lm_sales_amount")) );
          el.appendChild( createElement("totCBNum",resultSet.getString("lm_cb_count")) );
          el.appendChild( createElement("totCBAmt",resultSet.getString("lm_cb_amount")) );
          el.appendChild( createElement("totCreditNum",resultSet.getString("lm_credits_count")) );
          el.appendChild( createElement("totCreditAmt",resultSet.getString("lm_credits_amount")) );
          reportEl.appendChild(el);
        }
        else
        {
          reportEl = new Element("existingReport");
          reportEl.addAttribute( new Attribute("merchantID",resultSet.getString("mc_merchant_number")) );
          reportEl.addAttribute( new Attribute("acquirerID",resultSet.getString("acq_id")) );
      
          value = resultSet.getString("date_closed");
          if( value != null )
          {
            reportEl.appendChild( createElement("contractClose",value) );
          }
      
          // add previous two months
          el = new Element("monthlySummaryData");
          el.appendChild( createElement("month",resultSet.getString("cm_date")) );
          el.appendChild( createElement("totSalesNum",resultSet.getString("cm_sales_count")) );
          el.appendChild( createElement("totSalesAmt",resultSet.getString("cm_sales_amount")) );
          el.appendChild( createElement("totCBNum",resultSet.getString("cm_cb_count")) );
          el.appendChild( createElement("totCBAmt",resultSet.getString("cm_cb_amount")) );
          el.appendChild( createElement("totCreditNum",resultSet.getString("cm_credits_count")) );
          el.appendChild( createElement("totCreditAmt",resultSet.getString("cm_credits_amount")) );
          reportEl.appendChild(el);
        }
      
        buildChargebackDetailElements(reportEl,merchantId,activeDate);
        root.appendChild( reportEl );
      }      
    
      Document              doc     = new Document(root);
      FileOutputStream      xmlOut  = new FileOutputStream(filename);
  
      Serializer serializer = new XOMSerializer(xmlOut, "ISO-8859-1");
      serializer.setIndent(2);    // enable std indentation
      //serializer.setMaxLength(90);
      serializer.write(doc);
      xmlOut.close();
    }
    catch( Exception e )
    {
      logEntry("buildXmlDocument(" + merchantId + "," + activeDate +")",e.toString());
    }
  }  
  
  private Element createElement( String elName, String elValue )
  {
    Element el = new Element(elName);
    el.appendChild(elValue);
    return(el);
  }
  
  public boolean execute()
  {
    ResultSetIterator it            = null;
    Date              reportDate    = null;
    ResultSet         resultSet     = null;
    boolean           retVal        = false;
    String            workFilename  = null;

    try
    {
      connect();
      
      // select the last day of the previous month as report date
      // ex:  on 1/10/2007, select 12/31/2006 as report date
      /*@lineinfo:generated-code*//*@lineinfo:254^7*/

//  ************************************************************
//  #sql [Ctx] { select  (trunc(sysdate,'month')-1) 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  (trunc(sysdate,'month')-1)  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.MasterCardECPEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   reportDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:258^7*/
      
      if ( TestFilename != null )
      {
        workFilename = TestFilename;
      }
      else
      {
        workFilename = generateFilename("mc_ecp3941");
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:269^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number            as merchant_number,
//                  substr(mf.merchant_number,-11)as mc_merchant_number,
//                  trunc(:reportDate,'month')    as active_date,
//                  'Y'                           as new_report,
//                  '5185'                        as acq_id,
//                  replace(mf.dba_name,',',' ')  as dba_name,
//                  mf.dmaddr                     as addr_line1,
//                  mf.address_line_3             as addr_line2,
//                  mf.dmcity                     as addr_city,
//                  mf.dmstate                    as addr_state,
//                  mf.dmzip                      as addr_zip,
//                  'USA'                         as country,
//                  mf.phone_1                    as phone_number,
//                  mf.sic_code                   as sic_code,
//                  to_char( mif_date_opened(mf.date_opened),
//                           'yyyy-mm-dd' )       as date_opened,
//                  decode( mf.date_stat_chgd_to_dcb,
//                          null,null,
//                          to_char(mf.date_stat_chgd_to_dcb,'yyyy-mm-dd')
//                        )                       as date_closed,
//                  upper(bo.busowner_first_name) as prin_first_name,
//                  upper(bo.busowner_last_name)  as prin_last_name,
//                  'USA'                         as prin_country,
//                  to_char( trunc(:reportDate,'month'),
//                           'yyyy-mm' )       as cm_active_date,
//                  nvl(cb_curr.cb_count,0)       as cm_cb_count,
//                  nvl(cb_curr.cb_amount,0)      as cm_cb_amount,
//                  nvl(sm.mc_sales_count,0)      as cm_sales_count,
//                  nvl(sm.mc_sales_amount,0)     as cm_sales_amount,
//                  nvl(sm.mc_credits_count,0)    as cm_credits_count,
//                  nvl(sm.mc_credits_amount,0)   as cm_credits_amount,
//                  to_char( trunc((trunc(:reportDate,'month')-1),'month'),
//                           'yyyy-mm' )       as lm_active_date,
//                  nvl(cb_prev.cb_count,0)       as lm_cb_count,
//                  nvl(cb_prev.cb_amount,0)      as lm_cb_amount,
//                  nvl(sml.mc_sales_count,0)     as lm_sales_count,
//                  nvl(sml.mc_sales_amount,0)    as lm_sales_amount,
//                  nvl(sml.mc_credits_count,0)   as lm_credits_count,
//                  nvl(sml.mc_credits_amount,0)  as lm_credits_amount,
//                  round((cb_curr.cb_count/sml.mc_sales_count)*100,2)  
//                                                as curr_cb_to_prev_sales_ratio
//          from    mif                   mf,
//                  (
//                    select  cbi.merchant_number       as merchant_number,
//                            count(cbi.cb_load_sec)    as cb_count,
//                            sum(cbi.tran_amount)      as cb_amount
//                    from    network_chargebacks   cbi
//                    where   cbi.bank_number in (3941,3942) and
//                            cbi.incoming_date between trunc(:reportDate,'month') and last_day(:reportDate) and
//                            nvl(cbi.card_type,decode_card_type(cbi.card_number,null)) = 'MC' and
//                            nvl(cbi.first_time_chargeback,'Y') = 'Y' and
//                            not exists
//                            (
//                              select cba.cb_load_sec
//                              from    network_chargeback_activity cba
//                              where   cba.cb_load_sec = cbi.cb_load_sec and
//                                      cba.action_code in ( 'S','C','Z')
//                            )
//                    group by cbi.merchant_number
//                  )                     cb_curr,
//                  (
//                    select  cbi.merchant_number       as merchant_number,
//                            count(cbi.cb_load_sec)    as cb_count,
//                            sum(cbi.tran_amount)      as cb_amount
//                    from    network_chargebacks   cbi
//                    where   cbi.bank_number in (3941,3942) and
//                            cbi.incoming_date between trunc((trunc(:reportDate,'month')-1),'month') and (trunc(:reportDate,'month')-1) and
//                            nvl(cbi.card_type,decode_card_type(cbi.card_number,null)) = 'MC' and
//                            nvl(cbi.first_time_chargeback,'Y') = 'Y' and
//                            not exists
//                            (
//                              select cba.cb_load_sec
//                              from    network_chargeback_activity cba
//                              where   cba.cb_load_sec = cbi.cb_load_sec and
//                                      cba.action_code in ( 'S','C','Z')
//                            )
//                    group by cbi.merchant_number
//                  )                     cb_prev,
//                  monthly_extract_summary     sm,
//                  monthly_extract_summary     sml,
//                  merchant                    mr,
//                  businessowner               bo
//          where   mf.bank_number in (3941,3942) and
//                  cb_curr.merchant_number(+) = mf.merchant_number and
//                  cb_prev.merchant_number(+) = mf.merchant_number and
//                  sm.merchant_number(+) = mf.merchant_number and
//                  sm.active_date(+) = trunc(:reportDate,'month') and
//                  sml.merchant_number(+) = mf.merchant_number and
//                  sml.active_date(+) = trunc((trunc(:reportDate,'month')-1),'month') and
//                  nvl(sml.mc_sales_count,0) != 0 and
//                  nvl(cb_curr.cb_count,0) > 50 and
//                  (cb_curr.cb_count/sml.mc_sales_count) >= 0.005 and
//                  mr.merch_number(+) = mf.merchant_number and
//                  bo.app_seq_num(+) = mr.app_seq_num and
//                  bo.busowner_num(+)= 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number            as merchant_number,\n                substr(mf.merchant_number,-11)as mc_merchant_number,\n                trunc( :1 ,'month')    as active_date,\n                'Y'                           as new_report,\n                '5185'                        as acq_id,\n                replace(mf.dba_name,',',' ')  as dba_name,\n                mf.dmaddr                     as addr_line1,\n                mf.address_line_3             as addr_line2,\n                mf.dmcity                     as addr_city,\n                mf.dmstate                    as addr_state,\n                mf.dmzip                      as addr_zip,\n                'USA'                         as country,\n                mf.phone_1                    as phone_number,\n                mf.sic_code                   as sic_code,\n                to_char( mif_date_opened(mf.date_opened),\n                         'yyyy-mm-dd' )       as date_opened,\n                decode( mf.date_stat_chgd_to_dcb,\n                        null,null,\n                        to_char(mf.date_stat_chgd_to_dcb,'yyyy-mm-dd')\n                      )                       as date_closed,\n                upper(bo.busowner_first_name) as prin_first_name,\n                upper(bo.busowner_last_name)  as prin_last_name,\n                'USA'                         as prin_country,\n                to_char( trunc( :2 ,'month'),\n                         'yyyy-mm' )       as cm_active_date,\n                nvl(cb_curr.cb_count,0)       as cm_cb_count,\n                nvl(cb_curr.cb_amount,0)      as cm_cb_amount,\n                nvl(sm.mc_sales_count,0)      as cm_sales_count,\n                nvl(sm.mc_sales_amount,0)     as cm_sales_amount,\n                nvl(sm.mc_credits_count,0)    as cm_credits_count,\n                nvl(sm.mc_credits_amount,0)   as cm_credits_amount,\n                to_char( trunc((trunc( :3 ,'month')-1),'month'),\n                         'yyyy-mm' )       as lm_active_date,\n                nvl(cb_prev.cb_count,0)       as lm_cb_count,\n                nvl(cb_prev.cb_amount,0)      as lm_cb_amount,\n                nvl(sml.mc_sales_count,0)     as lm_sales_count,\n                nvl(sml.mc_sales_amount,0)    as lm_sales_amount,\n                nvl(sml.mc_credits_count,0)   as lm_credits_count,\n                nvl(sml.mc_credits_amount,0)  as lm_credits_amount,\n                round((cb_curr.cb_count/sml.mc_sales_count)*100,2)  \n                                              as curr_cb_to_prev_sales_ratio\n        from    mif                   mf,\n                (\n                  select  cbi.merchant_number       as merchant_number,\n                          count(cbi.cb_load_sec)    as cb_count,\n                          sum(cbi.tran_amount)      as cb_amount\n                  from    network_chargebacks   cbi\n                  where   cbi.bank_number in (3941,3942) and\n                          cbi.incoming_date between trunc( :4 ,'month') and last_day( :5 ) and\n                          nvl(cbi.card_type,decode_card_type(cbi.card_number,null)) = 'MC' and\n                          nvl(cbi.first_time_chargeback,'Y') = 'Y' and\n                          not exists\n                          (\n                            select cba.cb_load_sec\n                            from    network_chargeback_activity cba\n                            where   cba.cb_load_sec = cbi.cb_load_sec and\n                                    cba.action_code in ( 'S','C','Z')\n                          )\n                  group by cbi.merchant_number\n                )                     cb_curr,\n                (\n                  select  cbi.merchant_number       as merchant_number,\n                          count(cbi.cb_load_sec)    as cb_count,\n                          sum(cbi.tran_amount)      as cb_amount\n                  from    network_chargebacks   cbi\n                  where   cbi.bank_number in (3941,3942) and\n                          cbi.incoming_date between trunc((trunc( :6 ,'month')-1),'month') and (trunc( :7 ,'month')-1) and\n                          nvl(cbi.card_type,decode_card_type(cbi.card_number,null)) = 'MC' and\n                          nvl(cbi.first_time_chargeback,'Y') = 'Y' and\n                          not exists\n                          (\n                            select cba.cb_load_sec\n                            from    network_chargeback_activity cba\n                            where   cba.cb_load_sec = cbi.cb_load_sec and\n                                    cba.action_code in ( 'S','C','Z')\n                          )\n                  group by cbi.merchant_number\n                )                     cb_prev,\n                monthly_extract_summary     sm,\n                monthly_extract_summary     sml,\n                merchant                    mr,\n                businessowner               bo\n        where   mf.bank_number in (3941,3942) and\n                cb_curr.merchant_number(+) = mf.merchant_number and\n                cb_prev.merchant_number(+) = mf.merchant_number and\n                sm.merchant_number(+) = mf.merchant_number and\n                sm.active_date(+) = trunc( :8 ,'month') and\n                sml.merchant_number(+) = mf.merchant_number and\n                sml.active_date(+) = trunc((trunc( :9 ,'month')-1),'month') and\n                nvl(sml.mc_sales_count,0) != 0 and\n                nvl(cb_curr.cb_count,0) > 50 and\n                (cb_curr.cb_count/sml.mc_sales_count) >= 0.005 and\n                mr.merch_number(+) = mf.merchant_number and\n                bo.app_seq_num(+) = mr.app_seq_num and\n                bo.busowner_num(+)= 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.MasterCardECPEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,reportDate);
   __sJT_st.setDate(2,reportDate);
   __sJT_st.setDate(3,reportDate);
   __sJT_st.setDate(4,reportDate);
   __sJT_st.setDate(5,reportDate);
   __sJT_st.setDate(6,reportDate);
   __sJT_st.setDate(7,reportDate);
   __sJT_st.setDate(8,reportDate);
   __sJT_st.setDate(9,reportDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.MasterCardECPEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:366^7*/
      resultSet = it.getResultSet();
      
      buildXmlDocument( workFilename, resultSet );

      resultSet.close();
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return( retVal );
  }
  
  protected void setTestFilename( String filename )
  {
    TestFilename = filename;
  }
  
  public static void main( String[] args )
  {
    MasterCardECPEvent    test    = null;
    
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      test = new MasterCardECPEvent();
      test.connect(true);
      test.setTestFilename(args[0]);
      test.execute();
    }
    catch( Exception e )
    {
      log.error( e.toString() );
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception ee ){}
    }
  }
}/*@lineinfo:generated-code*/