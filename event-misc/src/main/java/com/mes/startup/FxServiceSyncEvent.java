/*@lineinfo:filename=FxServiceSyncEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/FxServiceSyncEvent.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiTranBase;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.PropertiesFile;
import com.tfxusa.reports.ArrayOfTFXR_HistRate;
import com.tfxusa.reports.ArrayOfTFXR_Payment;
import com.tfxusa.reports.TFXR_HistRate;
import com.tfxusa.reports.TFXR_HistRateReq;
import com.tfxusa.reports.TFXR_Merchant_ReportReq;
import com.tfxusa.reports.TFXR_Payment;
import com.tfxusa.reports.TFXUSAReportingLocator;
import com.tfxusa.reports.TFXUSAReportingSoapStub;
import com.tfxusa.secure.ArrayOfFXRate;
import com.tfxusa.secure.FXCloseBatch;
import com.tfxusa.secure.FXCloseBatchResponse;
import com.tfxusa.secure.FXRate;
import com.tfxusa.secure.FXTransPost;
import com.tfxusa.secure.FXTransPostResponse;
import com.tfxusa.secure.TFXUSAServiceLocator;
import com.tfxusa.secure.TFXUSAServiceSoapStub;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class FxServiceSyncEvent extends EventBase
{
  static Logger log = Logger.getLogger(FxServiceSyncEvent.class);

  public static final int       ET_TABLE_REFRESH      = 0;
  public static final int       ET_TRAN_SYNC          = 1;
  public static final int       ET_DEPOSIT_UPDATE     = 2;
  public static final int       ET_BASE_RATE_REFRESH  = 3;
  public static final int       ET_IC_POST            = 4;
  public static final int       ET_IC_BY_BATCH        = 5;

  public static class FxClientData
  {
    private   String    ClientId          = null;
    private   long      MerchantId        = 0L;
    
    public FxClientData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ClientId    = resultSet.getString("client_id");
      MerchantId  = resultSet.getLong("merchant_number");
    }
    
    public String getClientId()
    {
      return( ClientId );
    }
    
    public long getMerchantId()
    {
      return( MerchantId );
    }
  }
  
  public class FxTransactionData
  {
    protected     double      AuthAmount                = 0.0;
    protected     String      CardNumber                = null;
    protected     boolean     CreditTran                = false;
    protected     String      ExternalTranId            = null;
    protected     double      FxAmount                  = 0.0;
    protected     String      FxBatchNumber             = null;
    protected     int         FxClientId                = 0;
    protected     String      FxCurrencyCode            = null;
    protected     int         FxMerchantId              = 0;
    protected     int         FxProductType             = 0;
    protected     int         FxRateId                  = 0;
    protected     String      FxTranId                  = null;
    protected     String      GatewayTranType           = null;
    protected     long        MerchantId                = 0L;
    protected     String      MotoEcommIndicator        = null;
    protected     String      PurchaseId                = null;
    protected     long        RecId                     = 0L;
    protected     double      TranAmount                = 0.0;
    protected     Date        TranDate                  = null;
    protected     String      TridentTranId             = null;
  
    public FxTransactionData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AuthAmount      = resultSet.getDouble("auth_amount");
      CardNumber      = resultSet.getString("card_number");
      CreditTran      = resultSet.getString("is_credit").equals("Y");
      ExternalTranId  = resultSet.getString("external_tran_id");
      FxAmount        = resultSet.getDouble("fx_amount");
      FxBatchNumber   = resultSet.getString("fx_batch_number");
      FxClientId      = resultSet.getInt("fx_client_id");
      FxCurrencyCode  = resultSet.getString("currency_code");
      FxMerchantId    = resultSet.getInt("fx_merchant_id");
      FxProductType   = resultSet.getInt("fx_product_type");
      FxRateId        = resultSet.getInt("fx_rate_id");
      FxTranId        = resultSet.getString("fx_ref_num");
      GatewayTranType = resultSet.getString("api_tran_type");
      MerchantId      = resultSet.getLong("merchant_number");
      MotoEcommIndicator = resultSet.getString("moto_ecomm_ind");
      PurchaseId      = resultSet.getString("purchase_id");
      RecId           = resultSet.getLong("rec_id");
      TranAmount      = resultSet.getDouble("tran_amount");
      TranDate        = resultSet.getDate("tran_date");
      TridentTranId   = resultSet.getString("trident_tran_id");
    }
    
    public double getAuthAmount()
    {
      return( AuthAmount );
    }
    
    public String getCardNumber()
    {
      return( CardNumber );
    }
    
    public String getExternalTranId()
    {
      return( ExternalTranId );
    }
    
    public double getFxAmount()
    {
      return( FxAmount );
    }
    
    public String getFxBatchNumber()
    {
      return( ((FxBatchNumber == null) ? "0" : FxBatchNumber) );
    }
    
    public int getFxClientId()
    {
      return( FxClientId );
    }
    
    public String getFxCurrencyCode()
    {
      return( FxCurrencyCode );
    }
    
    public int getFxMerchantId()
    {
      return( FxMerchantId );
    }
    
    public int getFxProductType()
    {
      return( FxProductType );
    }
    
    public int getFxRateId()
    {
      return( FxRateId );
    }
    
    public String getFxTranId()
    {
      return( FxTranId );
    }
    
    public String getGatewayTranType()
    {
      return( GatewayTranType );
    }
    
    public long getMerchantId()
    {
      return( MerchantId );
    }
    
    public String getMotoEcommIndicator()
    {
      return( MotoEcommIndicator );
    }
    
    public String getPurchaseId()
    {
      return( PurchaseId );
    }
    
    public long getRecId()
    {
      return( RecId );
    }
    
    public double getTranAmount()
    {
      return( TranAmount );
    }
    
    public Date getTranDate()
    {
      return( TranDate );
    }
    
    public String getTridentTranId()
    {
      return( TridentTranId );
    }
    
    public boolean isCreditTransaction()
    {
      return( CreditTran );
    }              
    
    public boolean isPartialCapture()
    {
      return( getGatewayTranType().equals(TridentApiTranBase.TT_DEBIT) &&
              getAuthAmount() > getTranAmount() );
    }
    
    public boolean isRecurringPayment()
    {
      String  recurring   = ( TridentApiConstants.FV_MOTO_RECURRING_MOTO +
                              TridentApiConstants.FV_MOTO_INSTALLMENT_PAYMENT );
      
      return( getGatewayTranType().equals(TridentApiTranBase.TT_DEBIT) &&
              recurring.indexOf( getMotoEcommIndicator() ) >= 0 );
    }
    
    public void setFxTranId( String newValue )
    {
      FxTranId = newValue;
    }
  }
  
  // member variables
  private   Date            TestDateBegin     = null;
  private   Date            TestDateEnd       = null;
  
  public FxServiceSyncEvent( )
  {
    PropertiesFilename = "fx-service-sync.properties";
  }
  
  public boolean execute()
  {
    boolean     retVal      = false;
    try
    {
      connect(true);
      
      int eventType = Integer.parseInt( getEventArg(0) );
      
      switch( eventType )
      {
        case ET_TABLE_REFRESH:
          retVal = rateTablesRefresh();
          break;
          
        case ET_TRAN_SYNC:
          retVal = fxTransactionsPost();   // post pending transactions to the fx service
          break;
          
        case ET_DEPOSIT_UPDATE:
          retVal = fxDepositsUpdate();
          break;
          
        case ET_BASE_RATE_REFRESH:
          retVal = fxBaseRatesRefresh();
          break;
          
        case ET_IC_POST:
          retVal = postIcFees();
          break;
          
        case ET_IC_BY_BATCH:
          retVal = sendIcFeesByBatch();
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected String extractCbId( String batchId )
  {
    StringBuffer        retVal    = new StringBuffer();
    String              token     = null;
    StringTokenizer     tokens    = new StringTokenizer(batchId,"/");
    
    tokens.nextToken();
    token = tokens.nextToken();
    
    if ( token != null )
    {
      token = token.trim();
    
      for( int i = 0; i < token.length(); ++i )
      {
        char ch = token.charAt(i);
        switch( ch )
        {
          case '0':
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8':
          case '9':
            retVal.append(ch);
            break;
          
          default:
            i = token.length();   // force loop exit
            break; 
        }
      }
    }
    return( retVal.toString() );
  }
  
  public boolean fxBaseRatesRefresh()
  {
    Calendar                calBegin          = Calendar.getInstance();
    Calendar                calEnd            = Calendar.getInstance();
    Timestamp               expDate           = null;
    TFXR_HistRate           rate              = null;
    TFXR_HistRateReq        rateRequest       = null;
    ArrayOfTFXR_HistRate    rateArray         = null;
    TFXR_HistRate[]         rates             = null;
    boolean                 retVal            = false;
    TFXUSAReportingSoapStub service           = null;
    int                     toccataMerchantId = getToccataReportingMerchantId();
    
    String[]  currencies =
    {
      "EUR",    // Euro
      "GBP",    // Pound Sterling
    };
    
    try
    {
      setAutoCommit(false);
      
      // setup the date range to be the previous day
      if ( TestDateBegin != null )
      {
        calBegin.setTime(TestDateBegin);
      }
      else
      {
        calBegin.add(Calendar.DAY_OF_MONTH,-1);
      }
      calBegin.set(Calendar.HOUR,0);
      calBegin.set(Calendar.MINUTE,0);
      calBegin.set(Calendar.SECOND,0);
      
      if ( TestDateEnd != null )
      {
        calEnd.setTime(TestDateEnd);
      }
      else
      {
        calEnd.add(Calendar.DAY_OF_MONTH,-1);
      }
      calEnd.set(Calendar.HOUR,23);
      calEnd.set(Calendar.MINUTE,59);
      calEnd.set(Calendar.SECOND,59);
        
      // prepare the service
      service = initReportingService();
      
      rateRequest = new TFXR_HistRateReq( getToccataReportingAccessKey(),
                                          calBegin,calEnd,"USD","USD" );
      
      for( int currencyIdx = 0; currencyIdx < currencies.length; ++currencyIdx )
      {
        rateRequest.setFromISO( currencies[currencyIdx] );
        rateArray = service.TFX_GetHistBaseRate_1_00(rateRequest);
        rates = rateArray.getTFXR_HistRate();
        
        for( int i = 0; i < rates.length; ++i )
        {
          rate = rates[i];
          expDate = new Timestamp( rate.getRateDate().getTime().getTime() );
          
          /*@lineinfo:generated-code*//*@lineinfo:418^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    fx_rate_tables
//              where   merchant_number = 394100000
//                      and rate_id = :rate.getRateID()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_4209 = rate.getRateID();
  try {
   String theSqlTS = "delete\n            from    fx_rate_tables\n            where   merchant_number = 394100000\n                    and rate_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_4209);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:424^11*/
      
          /*@lineinfo:generated-code*//*@lineinfo:426^11*/

//  ************************************************************
//  #sql [Ctx] { insert into fx_rate_tables
//              (
//                merchant_number,
//                merchant_id,
//                client_id,
//                rate_id,
//                merchant_currency_code,
//                consumer_currency_code,
//                consumer_currency_country,
//                consumer_currency_desc,
//                expiration_date,
//                rate
//              )
//              values
//              (
//                394100000,
//                :toccataMerchantId,
//                0,
//                :rate.getRateID(),
//                :rate.getToISO(),
//                :rate.getFromISO(),
//                null,   -- consumer_currency_country
//                null,   -- consumer_currency_desc
//                :expDate,
//                :rate.getRate()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_4210 = rate.getRateID();
 String __sJT_4211 = rate.getToISO();
 String __sJT_4212 = rate.getFromISO();
 java.math.BigDecimal __sJT_4213 = rate.getRate();
   String theSqlTS = "insert into fx_rate_tables\n            (\n              merchant_number,\n              merchant_id,\n              client_id,\n              rate_id,\n              merchant_currency_code,\n              consumer_currency_code,\n              consumer_currency_country,\n              consumer_currency_desc,\n              expiration_date,\n              rate\n            )\n            values\n            (\n              394100000,\n               :1 ,\n              0,\n               :2 ,\n               :3 ,\n               :4 ,\n              null,   -- consumer_currency_country\n              null,   -- consumer_currency_desc\n               :5 ,\n               :6 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,toccataMerchantId);
   __sJT_st.setInt(2,__sJT_4210);
   __sJT_st.setString(3,__sJT_4211);
   __sJT_st.setString(4,__sJT_4212);
   __sJT_st.setTimestamp(5,expDate);
   __sJT_st.setBigDecimal(6,__sJT_4213);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:454^11*/
        }        
      }        
      
      /*@lineinfo:generated-code*//*@lineinfo:458^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:461^7*/

      retVal = true;
    }
    catch( Exception e )
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:467^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:467^34*/ } catch( Exception ee ) {}
      logEntry("fxBaseRatesRefresh()",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:469^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:469^32*/ } catch( Exception ee ) {}
    }
    finally
    {
      setAutoCommit(true);
    }
    return( retVal );
  }
  
  public boolean fxDepositsUpdate()
  {
    long                      batchId       = 0L;
    Calendar                  calBegin      = Calendar.getInstance();
    Calendar                  calEnd        = Calendar.getInstance();
    FxClientData              clientData    = null;
    Vector                    clientDataSet = new Vector();
    String                    dbaName       = null;
    String                    ddaNum        = null;
    TFXR_Payment              deposit       = null;
    ArrayOfTFXR_Payment       depositArray  = null;
    TFXR_Payment[]            deposits      = null;
    ResultSetIterator         it            = null;
    Timestamp                 fileTimestamp = null;
    String                    loadFilename  = null;
    Date                      postDate      = null;
    int                       recCount      = 0;
    int                       recNum        = 1;
    TFXR_Merchant_ReportReq   request       = null;
    ResultSet                 resultSet     = null;
    boolean                   retVal        = false;
    TFXUSAReportingSoapStub   service       = null;
    int                       tranCode      = 0;
    int                       tranCodeBase  = 0;
    String                    trNum         = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:506^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct 
//                  tp.merchant_number          as merchant_number,
//                  tpa.fx_client_id            as client_id
//          from    trident_profile_api   tpa,
//                  trident_profile       tp
//          where   nvl(tpa.fx_client_id,0) != 0 and
//                  tp.terminal_id = tpa.terminal_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct \n                tp.merchant_number          as merchant_number,\n                tpa.fx_client_id            as client_id\n        from    trident_profile_api   tpa,\n                trident_profile       tp\n        where   nvl(tpa.fx_client_id,0) != 0 and\n                tp.terminal_id = tpa.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.FxServiceSyncEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:515^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        clientDataSet.addElement( new FxClientData(resultSet) );
      }
      resultSet.close();
      it.close();
      
      // disable auto-commit
      setAutoCommit(false);
      
      // setup the date range to be the previous day
      if ( TestDateBegin != null )
      {
        calBegin.setTime(TestDateBegin);
      }
      else
      {
        calBegin.add(Calendar.DAY_OF_MONTH,-1);
      }
      calBegin.set(Calendar.HOUR,0);
      calBegin.set(Calendar.MINUTE,0);
      calBegin.set(Calendar.SECOND,0);
      
      if ( TestDateEnd != null )
      {
        calEnd.setTime(TestDateEnd);
      }
      else
      {
        calEnd.add(Calendar.DAY_OF_MONTH,-1);
      }
      calEnd.set(Calendar.HOUR,23);
      calEnd.set(Calendar.MINUTE,59);
      calEnd.set(Calendar.SECOND,59);
      
      // prepare the service
      service = initReportingService();
      
      for( int clientIdx = 0; clientIdx < clientDataSet.size(); ++clientIdx )
      {
        clientData = (FxClientData)clientDataSet.elementAt(clientIdx);
        
        // create a request object
        request = new TFXR_Merchant_ReportReq( getToccataReportingAccessKey(),
                                               getToccataReportingMerchantId(),
                                               Integer.parseInt(clientData.getClientId()),
                                               calBegin,
                                               calEnd
                                             );

        // request the payments for the date range    
        log.debug("Searching for deposits for " + clientData.getMerchantId() + " (" + clientData.getClientId() + ") between " + DateTimeFormatter.getFormattedDate(calBegin.getTime(),"MM/dd/yyyy") + " and " + DateTimeFormatter.getFormattedDate(calEnd.getTime(),"MM/dd/yyyy"));//@
        depositArray = service.TFX_GetPayments_1_00(request);
        deposits = depositArray.getTFXR_Payment();
        
        // process the deposits
        for( int i = 0; i < deposits.length; ++i )
        {
          if ( fileTimestamp == null )
          {
            fileTimestamp = new Timestamp( Calendar.getInstance().getTime().getTime() );
            loadFilename  = generateFilename("fx_ach3941");
          }
          deposit   = deposits[i];
          
          if ( deposit.getPaidAmt().doubleValue() == 0.0 )
          {
            continue;
          }
          
          String  tfxBatchId  = deposit.getBatchID();
          String  cbId        = null;
          if ( tfxBatchId.startsWith("ChgBk") )
          {
            cbId = extractCbId(tfxBatchId);
            tranCodeBase = 800;
          }
          else
          {
            tranCodeBase = 900;
          }
          
          try
          {
            batchId = Long.parseLong(deposit.getBatchID());
          }
          catch( Exception invalidBatchIdException )
          {
            batchId = 0L;
          }
          
          postDate  = new java.sql.Date( deposit.getDatePaid().getTime().getTime() );
          tranCode  = tranCodeBase + (deposit.getPaidAmt().doubleValue() < 0.0 ? 27 : 22);
          
          /*@lineinfo:generated-code*//*@lineinfo:612^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1) 
//              from    ach_detail    ach
//              where   ach.merchant_number = :clientData.getMerchantId()
//                      and ach.post_date_option = :postDate
//                      and ach.fx_reference_number = :deposit.getACHRef() 
//                      and ach.amount_of_transaction = abs( :deposit.getPaidAmt().doubleValue() )
//                      and ach.batch_id = decode(:batchId,0,ach.batch_id,:batchId)
//                      and nvl(ach.fx_cb_id,-1) = nvl(:cbId,-1)
//                      and ach.transaction_code = :tranCode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4214 = clientData.getMerchantId();
 String __sJT_4215 = deposit.getACHRef();
 double __sJT_4216 = deposit.getPaidAmt().doubleValue();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)  \n            from    ach_detail    ach\n            where   ach.merchant_number =  :1 \n                    and ach.post_date_option =  :2 \n                    and ach.fx_reference_number =  :3  \n                    and ach.amount_of_transaction = abs(  :4  )\n                    and ach.batch_id = decode( :5 ,0,ach.batch_id, :6 )\n                    and nvl(ach.fx_cb_id,-1) = nvl( :7 ,-1)\n                    and ach.transaction_code =  :8";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.FxServiceSyncEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4214);
   __sJT_st.setDate(2,postDate);
   __sJT_st.setString(3,__sJT_4215);
   __sJT_st.setDouble(4,__sJT_4216);
   __sJT_st.setLong(5,batchId);
   __sJT_st.setLong(6,batchId);
   __sJT_st.setString(7,cbId);
   __sJT_st.setInt(8,tranCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:623^11*/
          
          if ( recCount == 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:627^13*/

//  ************************************************************
//  #sql [Ctx] { select  mf.dda_num,
//                        mf.transit_routng_num,
//                        mf.dba_name,
//                        ach_fx_detail_sequence.nextval 
//                
//                from    mif     mf
//                where   mf.merchant_number = :clientData.getMerchantId()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4217 = clientData.getMerchantId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mf.dda_num,\n                      mf.transit_routng_num,\n                      mf.dba_name,\n                      ach_fx_detail_sequence.nextval \n               \n              from    mif     mf\n              where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.FxServiceSyncEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4217);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ddaNum = (String)__sJT_rs.getString(1);
   trNum = (String)__sJT_rs.getString(2);
   dbaName = (String)__sJT_rs.getString(3);
   recNum = __sJT_rs.getInt(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:636^13*/
            
            // batch did not have a valid number
            // generated a new value
            if ( batchId == 0L )
            {
              batchId = getAchBatchId();
            }
            
            log.debug("  Inserting " + batchId + "," + recNum + "," + DateTimeFormatter.getFormattedDate(postDate,"MM/dd/yyyy") );//@
            /*@lineinfo:generated-code*//*@lineinfo:646^13*/

//  ************************************************************
//  #sql [Ctx] { insert into ach_detail
//                (
//                  merchant_number,
//                  record_type_code,
//                  transaction_code, 
//                  receiving_dfi_id_ttttaaaac,
//                  dfi_account_number_dda_number,
//                  amount_of_transaction,
//                  internal_account_number,
//                  individual_name,
//                  addenda_record_indicator,
//                  file_creation_date,
//                  batch_number,
//                  post_date_option,
//                  batch_detail_id,
//                  batch_id,
//                  fx_reference_number,
//                  fx_cb_id,
//                  load_filename
//                )
//                values
//                (
//                  :clientData.getMerchantId(),
//                  6,
//                  :tranCode,
//                  :trNum,
//                  :ddaNum,
//                  abs( :deposit.getPaidAmt().doubleValue() ),
//                  :clientData.getMerchantId(),
//                  :dbaName,
//                  0,
//                  :fileTimestamp,
//                  1,
//                  :postDate,
//                  :recNum,
//                  :batchId,
//                  :deposit.getACHRef(),
//                  :cbId,
//                  :loadFilename
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4218 = clientData.getMerchantId();
 double __sJT_4219 = deposit.getPaidAmt().doubleValue();
 long __sJT_4220 = clientData.getMerchantId();
 String __sJT_4221 = deposit.getACHRef();
   String theSqlTS = "insert into ach_detail\n              (\n                merchant_number,\n                record_type_code,\n                transaction_code, \n                receiving_dfi_id_ttttaaaac,\n                dfi_account_number_dda_number,\n                amount_of_transaction,\n                internal_account_number,\n                individual_name,\n                addenda_record_indicator,\n                file_creation_date,\n                batch_number,\n                post_date_option,\n                batch_detail_id,\n                batch_id,\n                fx_reference_number,\n                fx_cb_id,\n                load_filename\n              )\n              values\n              (\n                 :1 ,\n                6,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                abs(  :5  ),\n                 :6 ,\n                 :7 ,\n                0,\n                 :8 ,\n                1,\n                 :9 ,\n                 :10 ,\n                 :11 ,\n                 :12 ,\n                 :13 ,\n                 :14 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4218);
   __sJT_st.setInt(2,tranCode);
   __sJT_st.setString(3,trNum);
   __sJT_st.setString(4,ddaNum);
   __sJT_st.setDouble(5,__sJT_4219);
   __sJT_st.setLong(6,__sJT_4220);
   __sJT_st.setString(7,dbaName);
   __sJT_st.setTimestamp(8,fileTimestamp);
   __sJT_st.setDate(9,postDate);
   __sJT_st.setInt(10,recNum);
   __sJT_st.setLong(11,batchId);
   __sJT_st.setString(12,__sJT_4221);
   __sJT_st.setString(13,cbId);
   __sJT_st.setString(14,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:688^13*/
            
            // if the entry is a chargeback, add the adjustment
            // date and adjustment amount to the chargebacks table
            if ( cbId != null )
            {
              /*@lineinfo:generated-code*//*@lineinfo:694^15*/

//  ************************************************************
//  #sql [Ctx] { update  payvision_api_cb_records
//                  set     adjustment_amount = abs( :deposit.getPaidAmt().doubleValue() ),
//                          adjustment_date = :postDate
//                  where   cb_id = :cbId
//                          and merchant_number = :clientData.getMerchantId()
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4222 = deposit.getPaidAmt().doubleValue();
 long __sJT_4223 = clientData.getMerchantId();
   String theSqlTS = "update  payvision_api_cb_records\n                set     adjustment_amount = abs(  :1  ),\n                        adjustment_date =  :2 \n                where   cb_id =  :3 \n                        and merchant_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_4222);
   __sJT_st.setDate(2,postDate);
   __sJT_st.setString(3,cbId);
   __sJT_st.setLong(4,__sJT_4223);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:701^15*/
            }
            
            ++recNum;
          }            
        }
      }
      
      // store the summary data
      /*@lineinfo:generated-code*//*@lineinfo:710^7*/

//  ************************************************************
//  #sql [Ctx] { call load_ach_summary(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_ach_summary( :1 )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:713^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:715^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:718^7*/
      
      retVal = true;
    }
    catch( Exception e )
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:724^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:724^34*/ } catch( Exception ee ) {}
      logEntry("fxDepositsUpdate()",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:726^12*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:726^32*/ } catch( Exception ee ) {}
    }
    finally
    {
      setAutoCommit(true);
    }
    return( retVal );
  }    
  
  protected void fxTransactionPost( FxTransactionData fxData )
  {
    FXTransPost                 fxTran          = null;
    int                         originalTranId  = 0;
    FXTransPostResponse         response        = null;
    TFXUSAServiceSoapStub       service         = null;
    int                         updateRefNum    = 0;
  
    try
    {
      setAutoCommit(false);   // disable auto-commit
    
      // get a pointer to the service
      service = initService();
      
      if( service != null )
      {    
        // create a new Toccata transaction to post
        fxTran  = new FXTransPost();
    
        // setup Toccata specific fields
        BigDecimal  tranAmount      = new BigDecimal( fxData.getTranAmount() );
        BigDecimal  zero            = new BigDecimal( 0.0 );
        int         toccataTranType = 0;
        String      tranType        = fxData.getGatewayTranType();
        String      tridentTranId   = fxData.getTridentTranId();
        String      externalTranId  = fxData.getExternalTranId();
    
        if( tranType.equals(TridentApiTranBase.TT_PRE_AUTH) )
        {
          toccataTranType = TridentApiConstants.TTT_AUTH;
          updateRefNum    = 1;
        }
        else if( tranType.equals(TridentApiTranBase.TT_REFUND) ||
                 tranType.equals(TridentApiTranBase.TT_CREDIT) )
        {
          if ( fxData.isCreditTransaction() )
          {
            // credit with no previous sale
            toccataTranType = TridentApiConstants.TTT_CREDIT;
          }
          else
          {
            // refund of a previous sale transaction
            toccataTranType = TridentApiConstants.TTT_REFUND;
          }          
        }
        else if( tranType.equals(TridentApiTranBase.TT_VOID) )
        {
          toccataTranType = TridentApiConstants.TTT_VOID;
        }
        else  // assume TT_DEBIT
        {
          if ( fxData.isRecurringPayment() )
          {
            toccataTranType = TridentApiConstants.TTT_RECURRING_CAPTURE;
          }
          else if ( fxData.isPartialCapture() )
          {
            toccataTranType = TridentApiConstants.TTT_PARTIAL_CAPTURE_CLOSE;
          }
          else
          {
            toccataTranType = TridentApiConstants.TTT_CAPTURE;
          }          
          updateRefNum    = 1;
        }
    
        try
        {
          originalTranId = Integer.parseInt(fxData.getFxTranId());
        }
        catch( Exception e )  // null or blank
        {
          originalTranId = 0;
        }
    
        // setup the Toccata transaction 
        fxTran.setTWS_UserID( getToccataUserId() );
        fxTran.setTWS_AccessKey( getToccataAccessKey() );
        fxTran.setRelatedTransID( originalTranId );
        fxTran.setTransType( toccataTranType );
        fxTran.setProductType( fxData.getFxProductType() );
        fxTran.setProcessorID( getToccataProcessorId() );
        fxTran.setProcTransID( externalTranId );
        fxTran.setMerchantID( fxData.getFxMerchantId() );
        fxTran.setClientID( fxData.getFxClientId() );
        fxTran.setReference( fxData.getFxBatchNumber() );
        fxTran.setMerchantTransID( tridentTranId );
        fxTran.setRateID( fxData.getFxRateId() );
        fxTran.setTransDate( Calendar.getInstance() );
        fxTran.setMerchCurrency( TridentApiConstants.FV_CURRENCY_CODE_USD_ALPHA );
        fxTran.setMerchAmount( new BigDecimal(fxData.getFxAmount()) );
        fxTran.setAuthCurrency( fxData.getFxCurrencyCode() );
        fxTran.setAuthAmt( tranAmount );
        fxTran.setInterChgPlusCurrency( TridentApiConstants.FV_CURRENCY_CODE_USD_ALPHA );
        fxTran.setInterChgPlusFee( zero );
        fxTran.setBankTransFeeCurrency( TridentApiConstants.FV_CURRENCY_CODE_USD_ALPHA );
        fxTran.setBankTransFee( zero );
        fxTran.setVATCurrency( "" );
        fxTran.setVATAmount( zero );
        fxTran.setVATCountry( "" );
        fxTran.setThirdPartyCurrency( "" );
        fxTran.setThirdPartyAmount( zero );
        fxTran.setThirdPartyID( "" );
    
        // post the transaction to the FX service
        log.debug("Posting '" + tridentTranId  + "' to the FX service.  Transaction Type " + toccataTranType);//@
        response = service.TFX_TransPost_v1_10(fxTran);
    
        if ( response.getErrorCode() == 0 )
        {
          log.debug("Post Successful.  FX Trans ID '" + response.getTransID()  + "'");//@
          /*@lineinfo:generated-code*//*@lineinfo:848^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture_api
//              set     fx_post_status = 'C',    -- post complete
//                      fx_post_ts = sysdate,
//                      fx_reference_number = decode( :updateRefNum, 
//                                                    1, :response.getTransID(),
//                                                    fx_reference_number )
//              where   rec_id = :fxData.getRecId()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_4224 = response.getTransID();
 long __sJT_4225 = fxData.getRecId();
   String theSqlTS = "update  trident_capture_api\n            set     fx_post_status = 'C',    -- post complete\n                    fx_post_ts = sysdate,\n                    fx_reference_number = decode(  :1 , \n                                                  1,  :2 ,\n                                                  fx_reference_number )\n            where   rec_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,updateRefNum);
   __sJT_st.setInt(2,__sJT_4224);
   __sJT_st.setLong(3,__sJT_4225);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:857^11*/
          /*@lineinfo:generated-code*//*@lineinfo:858^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:861^11*/
        }
        else  // failed
        {
          log.error("FX Post Failed.  '" + response.getErrorCode() + " - " + response.getMessage() + "'");
          logEntry("fxTransactionPost()", "FX Post Failed.  '" + response.getErrorCode() + " - " + response.getMessage() + "'");
        }
      }
    }
    catch( java.lang.NullPointerException ne )
    {
      try { rollback(); } catch(Exception se) {}
    }
    catch( Exception e )
    {
      logEntry( "fxTransactionPost()", e.toString() );
      try{ /*@lineinfo:generated-code*//*@lineinfo:877^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:877^33*/ } catch( Exception ee ) {}
    }
    finally
    {
      setAutoCommit(true);
    }
  }
  
  protected boolean fxTransactionsPost()
  {
    FxTransactionData       fxTranData    = null;
    ResultSetIterator       it            = null;
    ResultSet               resultSet     = null;
    boolean                 retVal        = false;
    Vector                  transactions  = new Vector();
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:897^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tapi.rec_id                     as rec_id,
//                  tapi.merchant_number            as merchant_number,
//                  tapi.card_number                as card_number,
//                  tapi.transaction_amount         as tran_amount,
//                  tapi.last_modified_date         as tran_date,
//                  tapi.auth_amount                as auth_amount,
//                  nvl(tapi.batch_number,0)        as fx_batch_number,
//                  tapi.fx_amount_base             as fx_amount,
//                  nvl( tapi.purchase_id,
//                       tapi.reference_number )    as purchase_id,
//                  tpa.fx_client_id                as fx_client_id,
//                  fx.consumer_currency_code       as currency_code,
//                  fx.merchant_id                  as fx_merchant_id,
//                  tpa.fx_product_type             as fx_product_type,
//                  tapi.fx_rate_id                 as fx_rate_id,
//                  nvl(tapi.fx_reference_number,0) as fx_ref_num,
//                  tapi.moto_ecommerce_ind         as moto_ecomm_ind,
//                  tapi.transaction_type           as api_tran_type,
//                  tapi.transaction_amount         as tran_amount,
//                  nvl(tapi.external_tran_id,
//                      tapi.reference_number )     as external_tran_id,
//                  decode( tapi.transaction_type,
//                          'C', decode( tapi.external_tran_id, null, 'Y', 'N' ),
//                          'N' )                   as is_credit,
//                  tapi.trident_tran_id            as trident_tran_id
//          from    trident_capture_api   tapi,
//                  trident_profile_api   tpa,
//                  fx_rate_tables        fx
//          where   tapi.fx_post_status = 'P'     -- pending posts
//                  and tpa.terminal_id = tapi.terminal_id
//                  and fx.merchant_number = tapi.merchant_number
//                  and fx.rate_id = tapi.fx_rate_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tapi.rec_id                     as rec_id,\n                tapi.merchant_number            as merchant_number,\n                tapi.card_number                as card_number,\n                tapi.transaction_amount         as tran_amount,\n                tapi.last_modified_date         as tran_date,\n                tapi.auth_amount                as auth_amount,\n                nvl(tapi.batch_number,0)        as fx_batch_number,\n                tapi.fx_amount_base             as fx_amount,\n                nvl( tapi.purchase_id,\n                     tapi.reference_number )    as purchase_id,\n                tpa.fx_client_id                as fx_client_id,\n                fx.consumer_currency_code       as currency_code,\n                fx.merchant_id                  as fx_merchant_id,\n                tpa.fx_product_type             as fx_product_type,\n                tapi.fx_rate_id                 as fx_rate_id,\n                nvl(tapi.fx_reference_number,0) as fx_ref_num,\n                tapi.moto_ecommerce_ind         as moto_ecomm_ind,\n                tapi.transaction_type           as api_tran_type,\n                tapi.transaction_amount         as tran_amount,\n                nvl(tapi.external_tran_id,\n                    tapi.reference_number )     as external_tran_id,\n                decode( tapi.transaction_type,\n                        'C', decode( tapi.external_tran_id, null, 'Y', 'N' ),\n                        'N' )                   as is_credit,\n                tapi.trident_tran_id            as trident_tran_id\n        from    trident_capture_api   tapi,\n                trident_profile_api   tpa,\n                fx_rate_tables        fx\n        where   tapi.fx_post_status = 'P'     -- pending posts\n                and tpa.terminal_id = tapi.terminal_id\n                and fx.merchant_number = tapi.merchant_number\n                and fx.rate_id = tapi.fx_rate_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.startup.FxServiceSyncEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:931^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        // add this transaction to the list to be posted
        transactions.addElement( new FxTransactionData(resultSet) );
      }
      resultSet.close();
      it.close();
      
      for( int i = 0; i < transactions.size(); ++i )
      {
        fxTranData = (FxTransactionData)transactions.elementAt(i);
        
        // TFX requires the lowest FX tran id to be passed with recurring
        // transactions to allow them to cover for the longer rates
        if ( fxTranData.isRecurringPayment() )
        {
          String  cardNumber      = fxTranData.getCardNumber();
          int     daysOut         = MesDefaults.getInt(MesDefaults.DK_FX_RATE_RECURRING_EXTENSION);
          long    merchantId      = fxTranData.getMerchantId();
          String  originalTranId  = null;
          int     rateId          = fxTranData.getFxRateId();
          Date    tranDate        = fxTranData.getTranDate();
          
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:959^13*/

//  ************************************************************
//  #sql [Ctx] { select  min( tapi.fx_reference_number ) 
//                
//                from    trident_capture_api   tapi
//                where   tapi.merchant_number = :merchantId
//                        and tapi.batch_date >= (:tranDate - :daysOut)
//                        and tapi.card_number = :cardNumber
//                        and tapi.fx_rate_id = :rateId
//                        and not tapi.fx_reference_number is null
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  min( tapi.fx_reference_number ) \n               \n              from    trident_capture_api   tapi\n              where   tapi.merchant_number =  :1 \n                      and tapi.batch_date >= ( :2  -  :3 )\n                      and tapi.card_number =  :4 \n                      and tapi.fx_rate_id =  :5 \n                      and not tapi.fx_reference_number is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.FxServiceSyncEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,tranDate);
   __sJT_st.setInt(3,daysOut);
   __sJT_st.setString(4,cardNumber);
   __sJT_st.setInt(5,rateId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   originalTranId = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:969^13*/
          }
          catch( java.sql.SQLException sqe )
          {
            // ignore, assume no record found
          }
          fxTranData.setFxTranId( originalTranId );
        }
        fxTransactionPost( fxTranData );
      }
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry( "fxTransactionsPost()", e.toString() );
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected long getAchBatchId( )
  {
    SQLJConnectionBase      ftpdb       = null;
    DefaultContext          ftpCtx      = null;
    long                    retVal      = 0L;
  
    try
    {
      // This uses the mes user to connect to ftpdb
      // as this uses an mes schema sequence to generate these ids
      // not to be confused with mesload.charge_back_pk table ids
      ftpdb = new SQLJConnectionBase(MesDefaults.getString(MesDefaults.MES_FTP_DB_NAME));
      ftpdb.connect();                // connect to ftp db
      ftpCtx = ftpdb.getContext();    // store a ref to the context
      
      /*@lineinfo:generated-code*//*@lineinfo:1007^7*/

//  ************************************************************
//  #sql [ftpCtx] { select  seq_ach_batch_id.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = ftpCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  seq_ach_batch_id.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.FxServiceSyncEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1011^7*/
    }
    catch( Exception e )
    {
      logEntry("getAchBatchId()",e.toString());
    }      
    finally
    {
      try{ ftpdb.cleanUp(); } catch (Exception e) {}
    }
    return( retVal );
  }
  
  protected String getToccataAccessKey( )
  {
    PropertiesFile  propsFile     = null;
    
    try
    {
      propsFile   = new PropertiesFile( PropertiesFilename );
    } catch(Exception e) {}
    return( propsFile.getString("toccata.access.key","WelcomeTemp") );
  }
  
  protected int getToccataProcessorId( )
  {
    PropertiesFile  propsFile     = null;
    
    try
    {
      propsFile   = new PropertiesFile( PropertiesFilename );
    } catch(Exception e) {}
    return( propsFile.getInt("toccata.processor.id",0) );
  }
  
  protected String getToccataReportingAccessKey( )
  {
    PropertiesFile  propsFile     = null;
    
    try
    {
      propsFile   = new PropertiesFile( PropertiesFilename );
    } catch(Exception e) {}
    return( propsFile.getString("toccata.reporting.access.key","MESTemp|WelcomeTemp") );
  }
  
  protected int getToccataReportingMerchantId( )
  {
    PropertiesFile  propsFile     = null;
    
    try
    { 
      propsFile   = new PropertiesFile( PropertiesFilename );
    } catch(Exception e) {}
    return( propsFile.getInt("toccata.user.id",1026) );
  }
  
  protected String getToccataUserId( )
  {
    PropertiesFile  propsFile     = null;
    
    try
    {
      propsFile   = new PropertiesFile( PropertiesFilename );
    } catch(Exception e) {}
    return( propsFile.getString("toccata.user.id","MESTemp") );
  }
  
  protected TFXUSAReportingSoapStub initReportingService( )
  {
    TFXUSAReportingLocator    sl            = null;
    TFXUSAReportingSoapStub   service       = null;
  
    try
    {
      sl      =  new TFXUSAReportingLocator(); 
      service = (TFXUSAReportingSoapStub)sl.getTFXUSAReportingSoap(); 
    }
    catch( Exception e )
    {
      logEntry("initReportingService()",e.toString());
    }
    return( service );
  }
  
  protected TFXUSAServiceSoapStub initService( )
  {
    TFXUSAServiceLocator    sl      = null;
    TFXUSAServiceSoapStub   service = null;
  
    try
    {
      sl      =  new TFXUSAServiceLocator(); 
      service = (TFXUSAServiceSoapStub)sl.getTFXUSAServiceSoap(); 
    }
    catch( Exception e )
    {
      logEntry("initService()",e.toString());
    }
    return( service );
  }
  
  protected boolean postIcFees( )
  {
    StringBuffer        batchDescriptor   = new StringBuffer("Interchange_");
    int                 batchDescriptorLen= 0;
    Date                icDate            = null;
    Vector              icToPost          = new Vector();
    ResultSetIterator   it                = null;
    ResultSet           resultSet         = null;
    boolean             retVal            = false;
    
    try
    {
      if ( TestDateBegin != null )
      {
        icDate = TestDateBegin;
      }
      else    // production mode, date is previous month
      {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH,-1);
        cal.set(Calendar.DAY_OF_MONTH,1);
        icDate = new java.sql.Date( cal.getTime().getTime() );
      }
      
      // create the batch descriptor
      batchDescriptor.append( DateTimeFormatter.getFormattedDate(icDate,"MMMyyyy") );
      batchDescriptorLen = batchDescriptor.length();
      
      // select the interchange to post
      /*@lineinfo:generated-code*//*@lineinfo:1142^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  fx.fx_client_id             as fx_client_id,
//                  min(dt.settlement_date)     as min_date,
//                  max(dt.settlement_date)     as max_date,
//                  sum( dt.usd_discount_fee )  as ic_amount
//          from    (
//                    select  distinct tp.merchant_number,
//                            tpa.fx_client_id
//                    from    trident_profile_api         tpa,
//                            trident_profile             tp
//                    where   nvl(tpa.fx_client_id,0) != 0
//                            and tp.terminal_id = tpa.terminal_id
//                  )                           fx,
//                  payvision_api_tran_detail   dt
//          where   dt.merchant_number = fx.merchant_number
//                  and dt.settlement_date between trunc(:icDate,'month') and last_day(:icDate)
//                  and dt.orig_currency_code != '840'  -- USD is settled directly
//          group by fx.fx_client_id
//          order by fx.fx_client_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  fx.fx_client_id             as fx_client_id,\n                min(dt.settlement_date)     as min_date,\n                max(dt.settlement_date)     as max_date,\n                sum( dt.usd_discount_fee )  as ic_amount\n        from    (\n                  select  distinct tp.merchant_number,\n                          tpa.fx_client_id\n                  from    trident_profile_api         tpa,\n                          trident_profile             tp\n                  where   nvl(tpa.fx_client_id,0) != 0\n                          and tp.terminal_id = tpa.terminal_id\n                )                           fx,\n                payvision_api_tran_detail   dt\n        where   dt.merchant_number = fx.merchant_number\n                and dt.settlement_date between trunc( :1 ,'month') and last_day( :2 )\n                and dt.orig_currency_code != '840'  -- USD is settled directly\n        group by fx.fx_client_id\n        order by fx.fx_client_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,icDate);
   __sJT_st.setDate(2,icDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.startup.FxServiceSyncEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1162^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        // post the min/max dates to the end of the batch descriptor
        // this assists TFX in reconciling the interchange payments
        batchDescriptor.setLength(batchDescriptorLen);
        batchDescriptor.append(" (");
        batchDescriptor.append(DateTimeFormatter.getFormattedDate(resultSet.getDate("min_date"),"MM/dd/yyyy"));
        batchDescriptor.append("-");
        batchDescriptor.append(DateTimeFormatter.getFormattedDate(resultSet.getDate("max_date"),"MM/dd/yyyy"));
        batchDescriptor.append(")");
        
        icToPost.add( new FXCloseBatch( MesDefaults.getString(MesDefaults.DK_TOCCATA_USER_ID),
                                        MesDefaults.getString(MesDefaults.DK_TOCCATA_ACCESS_KEY),
                                        resultSet.getInt("fx_client_id"),
                                        batchDescriptor.toString(),
                                        new BigDecimal(resultSet.getDouble("ic_amount")) )
                    );
      }
      resultSet.close();
      it.close();
      
      for( int i = 0; i < icToPost.size(); ++i )
      {
        FXCloseBatch icMsg = (FXCloseBatch)icToPost.elementAt(i);
        
        TFXUSAServiceLocator   sl      =  new TFXUSAServiceLocator(); 
        TFXUSAServiceSoapStub  service = (TFXUSAServiceSoapStub)sl.getTFXUSAServiceSoap(); 
        
        log.debug("Client ID: " + icMsg.getClientID() + "  IC Due: " + MesMath.toCurrency(icMsg.getBatchTotal().doubleValue()) );//@
        FXCloseBatchResponse response = service.TFX_TransBatchClose_v1_10( icMsg );
        
        if ( response.getErrorCode() != 0 )
        {
          StringBuffer errorMessage = new StringBuffer("FX batch close failed.  ");
          errorMessage.append("FX Client ID: ");
          errorMessage.append(icMsg.getClientID());
          errorMessage.append("  Batch #: ");
          errorMessage.append(icMsg.getBatchID());
          errorMessage.append("  Error Msg: ");
          errorMessage.append(response.getMessage());
          logEntry( "postIcFees()", errorMessage.toString() );
        }
      }
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("postIcFees()",e.toString());
    }
    return( retVal );
  }
  
  protected boolean rateTablesRefresh()
  {
    FxClientData              clientData      = null;
    Vector                    clientDataSet   = new Vector();
    Timestamp                 expirationDate  = null;
    ResultSetIterator         it              = null;
    FXRate[]                  ratesArray      = null;
    ArrayOfFXRate             ratesTable      = null;
    ResultSet                 resultSet       = null;
    boolean                   retVal          = false;
    TFXUSAServiceSoapStub     service         = null;
  
    try
    {
      setAutoCommit(false);
      
      service = initService();
      
      if( service != null )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1237^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct 
//                    tp.merchant_number          as merchant_number,
//                    tpa.fx_client_id            as client_id
//            from    trident_profile_api   tpa,
//                    trident_profile       tp
//            where   nvl(tpa.fx_client_id,0) != 0 and
//                    tp.terminal_id = tpa.terminal_id and
//                    (
//                      not exists  -- no entries for this client
//                      ( 
//                        select  trt.rate_id
//                        from    fx_rate_tables trt
//                        where   trt.merchant_number = tp.merchant_number 
//                                and trt.client_id = tpa.fx_client_id
//                      ) or
//                      exists      -- expired entries
//                      (
//                        select  trt.rate_id
//                        from    fx_rate_tables trt
//                        where   trt.merchant_number = tp.merchant_number and
//                                trt.expiration_date < sysdate
//                      )
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct \n                  tp.merchant_number          as merchant_number,\n                  tpa.fx_client_id            as client_id\n          from    trident_profile_api   tpa,\n                  trident_profile       tp\n          where   nvl(tpa.fx_client_id,0) != 0 and\n                  tp.terminal_id = tpa.terminal_id and\n                  (\n                    not exists  -- no entries for this client\n                    ( \n                      select  trt.rate_id\n                      from    fx_rate_tables trt\n                      where   trt.merchant_number = tp.merchant_number \n                              and trt.client_id = tpa.fx_client_id\n                    ) or\n                    exists      -- expired entries\n                    (\n                      select  trt.rate_id\n                      from    fx_rate_tables trt\n                      where   trt.merchant_number = tp.merchant_number and\n                              trt.expiration_date < sysdate\n                    )\n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.startup.FxServiceSyncEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1262^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          clientDataSet.addElement( new FxClientData(resultSet) );
        }
        resultSet.close();
        it.close();
      
        for( int clientIdx = 0; clientIdx < clientDataSet.size(); ++clientIdx )
        {
          clientData = (FxClientData)clientDataSet.elementAt(clientIdx);
          ratesTable = service.TFX_GetAllRatesByClient_1_00_Post(getToccataUserId(),getToccataAccessKey(),clientData.getClientId());
          ratesArray = ratesTable.getFXRate();
      
          for( int rateIdx = 0; rateIdx < ratesArray.length; ++rateIdx )
          {
            if ( ratesArray[rateIdx] == null ) continue;
        
            log.debug("Deleting " + ratesArray[rateIdx].getRateID() + " for MID " + clientData.getMerchantId());
            /*@lineinfo:generated-code*//*@lineinfo:1283^13*/

//  ************************************************************
//  #sql [Ctx] { delete
//                from    fx_rate_tables
//                where   merchant_number = :clientData.getMerchantId()
//                        and rate_id = :ratesArray[rateIdx].getRateID()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4226 = clientData.getMerchantId();
 int __sJT_4227 = ratesArray[rateIdx].getRateID();
  try {
   String theSqlTS = "delete\n              from    fx_rate_tables\n              where   merchant_number =  :1 \n                      and rate_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4226);
   __sJT_st.setInt(2,__sJT_4227);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1289^13*/
          
            expirationDate = new java.sql.Timestamp(ratesArray[rateIdx].getExpiration().getTime().getTime());
          
            log.debug("Inserting " + ratesArray[rateIdx].getRateID() + " for MID " + clientData.getMerchantId());
            /*@lineinfo:generated-code*//*@lineinfo:1294^13*/

//  ************************************************************
//  #sql [Ctx] { insert into fx_rate_tables
//                (
//                  merchant_number,
//                  merchant_id,
//                  client_id,
//                  rate_id,
//                  merchant_currency_code,
//                  consumer_currency_code,
//                  consumer_currency_country,
//                  consumer_currency_desc,
//                  expiration_date,
//                  rate
//                )
//                values
//                (
//                  :clientData.getMerchantId(),
//                  :ratesArray[rateIdx].getMerchantID(),
//                  :ratesArray[rateIdx].getClientID(),
//                  :ratesArray[rateIdx].getRateID(),
//                  :ratesArray[rateIdx].getMerchant_ISO(),
//                  :ratesArray[rateIdx].getConsumer_ISO(),
//                  :ratesArray[rateIdx].getConsumerCurrCtry(),
//                  :ratesArray[rateIdx].getConsumerCurrency(),
//                  :expirationDate,
//                  :ratesArray[rateIdx].getRate()
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4228 = clientData.getMerchantId();
 int __sJT_4229 = ratesArray[rateIdx].getMerchantID();
 int __sJT_4230 = ratesArray[rateIdx].getClientID();
 int __sJT_4231 = ratesArray[rateIdx].getRateID();
 String __sJT_4232 = ratesArray[rateIdx].getMerchant_ISO();
 String __sJT_4233 = ratesArray[rateIdx].getConsumer_ISO();
 String __sJT_4234 = ratesArray[rateIdx].getConsumerCurrCtry();
 String __sJT_4235 = ratesArray[rateIdx].getConsumerCurrency();
 java.math.BigDecimal __sJT_4236 = ratesArray[rateIdx].getRate();
   String theSqlTS = "insert into fx_rate_tables\n              (\n                merchant_number,\n                merchant_id,\n                client_id,\n                rate_id,\n                merchant_currency_code,\n                consumer_currency_code,\n                consumer_currency_country,\n                consumer_currency_desc,\n                expiration_date,\n                rate\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                 :6 ,\n                 :7 ,\n                 :8 ,\n                 :9 ,\n                 :10 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4228);
   __sJT_st.setInt(2,__sJT_4229);
   __sJT_st.setInt(3,__sJT_4230);
   __sJT_st.setInt(4,__sJT_4231);
   __sJT_st.setString(5,__sJT_4232);
   __sJT_st.setString(6,__sJT_4233);
   __sJT_st.setString(7,__sJT_4234);
   __sJT_st.setString(8,__sJT_4235);
   __sJT_st.setTimestamp(9,expirationDate);
   __sJT_st.setBigDecimal(10,__sJT_4236);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1322^13*/
          }
        
          /*@lineinfo:generated-code*//*@lineinfo:1325^11*/

//  ************************************************************
//  #sql [Ctx] { commit
//             };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1328^11*/
        }        
      
        /*@lineinfo:generated-code*//*@lineinfo:1331^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1334^9*/
      
        retVal = true;
      }
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:1341^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1341^34*/ }catch( Exception ee ) {}
      logEntry("rateTablesRefresh()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      setAutoCommit(true);
    }
    return( retVal );
  }    
  
  protected boolean sendIcFeesByBatch( )
  {
    Date                icDate            = null;
    ResultSetIterator   it                = null;
    ResultSetMetaData   metaData          = null;
    BufferedWriter      out               = null;
    ResultSet           resultSet         = null;
    boolean             retVal            = false;
    
    try
    {
      if ( TestDateBegin != null )
      {
        icDate = TestDateBegin;
      }
      else    // production mode, date is previous month
      {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH,-1);
        cal.set(Calendar.DAY_OF_MONTH,1);
        icDate = new java.sql.Date( cal.getTime().getTime() );
      }
      
      // be sure that the batch id is assigned to all rows in the payvision api data
      /*@lineinfo:generated-code*//*@lineinfo:1376^7*/

//  ************************************************************
//  #sql [Ctx] { begin
//            loop
//              update  payvision_api_tran_detail dt
//              set     batch_number = (select tapi.batch_number from trident_capture_api tapi where tapi.trident_tran_id = dt.trident_tran_id and tapi.mesdb_timestamp >= '01-aug-2011')
//              where   rowid in 
//                      (
//                        select  dt.rowid as row_id
//                        from    (
//                                  select  distinct tp.merchant_number,
//                                          tpa.fx_client_id
//                                  from    trident_profile_api         tpa,
//                                          trident_profile             tp
//                                  where   nvl(tpa.fx_client_id,0) != 0
//                                          and tp.terminal_id = tpa.terminal_id
//                                )                           fx,
//                                payvision_api_tran_detail   dt
//                        where   dt.merchant_number = fx.merchant_number
//                                and dt.settlement_date between trunc(:icDate,'month') and last_day(:icDate)
//                                and dt.orig_currency_code != '840'  -- USD is settled directly
//                                and dt.batch_number is null
//                                and rownum <= 1000
//                      );
//  
//              exit when sql%notfound; -- this will be true after last records have been updated
//              commit;
//            end loop;
//          end;
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "begin\n          loop\n            update  payvision_api_tran_detail dt\n            set     batch_number = (select tapi.batch_number from trident_capture_api tapi where tapi.trident_tran_id = dt.trident_tran_id and tapi.mesdb_timestamp >= '01-aug-2011')\n            where   rowid in \n                    (\n                      select  dt.rowid as row_id\n                      from    (\n                                select  distinct tp.merchant_number,\n                                        tpa.fx_client_id\n                                from    trident_profile_api         tpa,\n                                        trident_profile             tp\n                                where   nvl(tpa.fx_client_id,0) != 0\n                                        and tp.terminal_id = tpa.terminal_id\n                              )                           fx,\n                              payvision_api_tran_detail   dt\n                      where   dt.merchant_number = fx.merchant_number\n                              and dt.settlement_date between trunc( :1 ,'month') and last_day( :2 )\n                              and dt.orig_currency_code != '840'  -- USD is settled directly\n                              and dt.batch_number is null\n                              and rownum <= 1000\n                    );\n\n            exit when sql%notfound; -- this will be true after last records have been updated\n            commit;\n          end loop;\n        end;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,icDate);
   __sJT_st.setDate(2,icDate);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1405^7*/
      
      String dataFilename = "tfx_ic_" + DateTimeFormatter.getFormattedDate(icDate,"MMMyyyy") + ".txt";
      
      // select the interchange to post by batch number
      /*@lineinfo:generated-code*//*@lineinfo:1410^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  fx.fx_client_id             as fx_client_id,
//                  dt.batch_number             as batch_id,
//                  to_char(min(dt.settlement_date),'mm/dd/yyyy')
//                                              as min_date,
//                  to_char(max(dt.settlement_date),'mm/dd/yyyy')
//                                              as max_date,
//                  sum( dt.usd_discount_fee )  as ic_amount
//          from    (
//                    select  distinct tp.merchant_number,
//                            tpa.fx_client_id
//                    from    trident_profile_api         tpa,
//                            trident_profile             tp
//                    where   nvl(tpa.fx_client_id,0) != 0
//                            and tp.terminal_id = tpa.terminal_id
//                  )                           fx,
//                  payvision_api_tran_detail   dt
//          where   dt.merchant_number = fx.merchant_number
//                  and dt.settlement_date between trunc(:icDate,'month') and last_day(:icDate)
//                  and dt.orig_currency_code != '840'  -- USD is settled directly
//          group by fx.fx_client_id,dt.batch_number
//          order by fx.fx_client_id,dt.batch_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  fx.fx_client_id             as fx_client_id,\n                dt.batch_number             as batch_id,\n                to_char(min(dt.settlement_date),'mm/dd/yyyy')\n                                            as min_date,\n                to_char(max(dt.settlement_date),'mm/dd/yyyy')\n                                            as max_date,\n                sum( dt.usd_discount_fee )  as ic_amount\n        from    (\n                  select  distinct tp.merchant_number,\n                          tpa.fx_client_id\n                  from    trident_profile_api         tpa,\n                          trident_profile             tp\n                  where   nvl(tpa.fx_client_id,0) != 0\n                          and tp.terminal_id = tpa.terminal_id\n                )                           fx,\n                payvision_api_tran_detail   dt\n        where   dt.merchant_number = fx.merchant_number\n                and dt.settlement_date between trunc( :1 ,'month') and last_day( :2 )\n                and dt.orig_currency_code != '840'  -- USD is settled directly\n        group by fx.fx_client_id,dt.batch_number\n        order by fx.fx_client_id,dt.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.FxServiceSyncEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,icDate);
   __sJT_st.setDate(2,icDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.startup.FxServiceSyncEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1433^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        if ( out == null )
        {
          out = new BufferedWriter( new FileWriter(dataFilename) );
          metaData  = resultSet.getMetaData();
          
          for (int i = 1; i <= metaData.getColumnCount(); ++i)
          {
            if ( i > 1 ) { out.write("\t"); }
            out.write( metaData.getColumnName(i).toLowerCase() );
          }
          out.newLine();
        }
        
        for (int i = 1; i <= metaData.getColumnCount(); ++i)
        {
          if ( i > 1 ) { out.write("\t"); }
          out.write( resultSet.getString(i) );
        }
        out.newLine();
      }
      resultSet.close();
      it.close();
      out.close();
      
      // send file 
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(MesEmails.MSG_ADDRS_FX_IC_NOTICE);
      msg.setSubject("FX Interchange by Batch (" + DateTimeFormatter.getFormattedDate(icDate,"MMM yyyy") + ")");
      msg.setText("Attached is the FX interchange that will post on the 10th.");
      msg.addFile(dataFilename);
      msg.send();
      
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("sendIcFeesByBatch()",e.toString());
    }
    finally
    {
      try{ it.close();  } catch( Exception ee ) {}
      try{ out.close(); } catch( Exception ee ) {}
    }
    return( retVal );
  }
  
  protected void setTestDates( Date beginDate, Date endDate )
  {
    TestDateBegin = beginDate;
    TestDateEnd   = endDate;
  }
  
  public static void main( String[] args )
  {
    FxServiceSyncEvent      bc          = null;
    Date                    beginDate   = null;
    Date                    endDate     = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      if ( args.length > 1 )
      {
        beginDate = new java.sql.Date( DateTimeFormatter.parseDate(args[1],"MM/dd/yyyy").getTime() );
      }
      if ( args.length > 2 )
      {
        endDate = new java.sql.Date( DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy").getTime() );
      }
      
      bc = new FxServiceSyncEvent();
      bc.setEventArgs(args[0]);
      bc.setTestDates( beginDate,endDate );
      bc.execute();
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/