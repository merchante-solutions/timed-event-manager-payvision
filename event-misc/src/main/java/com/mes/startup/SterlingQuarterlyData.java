/*@lineinfo:filename=SterlingQuarterlyData*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/SterlingQuarterlyData.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: rmehta $
  Last Modified Date : $Date: 2015-05-07 08:48:22 -0700 (Thu, 07 May 2015) $
  Version            : $Revision: 23618 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class SterlingQuarterlyData extends EventBase
{
  static Logger log = Logger.getLogger(SterlingQuarterlyData.class);

  public final String   INTEGER_FORMAT  = "###,###,##0;(###,###,##0)";
  public final String   CURRENCY_FORMAT = "$###,###,###,##0.00;($###,###,###,##0.00)";
  
  public boolean execute()
  {
    boolean   result = false;
    long      approvedApps;
    long      accountsOnFile;
    
    String[]  months        = new String[3];
    long[]    debitCounts   = new long[3];
    double[]  debitAmounts  = new double[3];
    long[]    creditCounts  = new long[3];
    double[]  creditAmounts = new double[3];
    
    long[]    totalMonthCount = new long[3];
    double[]  totalMonthAmount = new double[3];
    
    long      totalCount   = 0L;
    double    totalAmount  = 0.0;
    
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    int       bankNumber  = 3942;
    String    quarter     = "";
    String    progress    = "";
    
    Calendar  activeCal   = Calendar.getInstance();
    Calendar  beginCal    = Calendar.getInstance();
    Calendar  endCal      = Calendar.getInstance();
    
    java.sql.Date activeDate;
    java.sql.Date beginDate;
    java.sql.Date endDate;
    
    try
    {
      connect();
      
      // determine dates of previous quarter based on current date
      Calendar now = Calendar.getInstance();
      
      activeCal.clear();
      beginCal.clear();
      endCal.clear();
      
      switch(now.get(Calendar.MONTH))
      {
        // these evaluate to 4th quarter of previous year
        case Calendar.JANUARY:
        case Calendar.FEBRUARY:
        case Calendar.MARCH:
          log.debug("4th quarter of last year (" + (now.get(Calendar.YEAR)-1) + ")");
          quarter = "Q4 " + (now.get(Calendar.YEAR) - 1);
          // set activeCal to December 1st
          activeCal.set(now.get(Calendar.YEAR)-1, Calendar.DECEMBER, 1);
          
          // set beginCal to October 1st
          beginCal.set(now.get(Calendar.YEAR)-1, Calendar.OCTOBER, 1);
          
          // set endCal to December 31st
          endCal.set(now.get(Calendar.YEAR)-1, Calendar.DECEMBER, 31);
          break;
          
        // these evaluate to 1st quarter of current year
        case Calendar.APRIL:
        case Calendar.MAY:
        case Calendar.JUNE:
          log.debug("1st quarter of this year");
          quarter = "Q1 " + now.get(Calendar.YEAR);
          // set activeCal to March 1st
          activeCal.set(now.get(Calendar.YEAR), Calendar.MARCH, 1);
          
          // set beginCal to January 1st
          beginCal.set(now.get(Calendar.YEAR), Calendar.JANUARY, 1);
          
          // set endCal to March 31st
          endCal.set(now.get(Calendar.YEAR), Calendar.MARCH, 31);
          break;
          
        // these evaluate to 2nd quarter of current year
        case Calendar.JULY:
        case Calendar.AUGUST:
        case Calendar.SEPTEMBER:
          log.debug("2nd quarter of this year");
          quarter = "Q2 " + now.get(Calendar.YEAR);
          // set activeCal to June 1st
          activeCal.set(now.get(Calendar.YEAR), Calendar.JUNE, 1);
          
          // set beginCal to April 1st
          beginCal.set(now.get(Calendar.YEAR), Calendar.APRIL, 1);
          
          // set endCal to June 30th
          endCal.set(now.get(Calendar.YEAR), Calendar.JUNE, 30);
          break;
          
        // these evaluate to 3rd quarter of curent year
        case Calendar.OCTOBER:
        case Calendar.NOVEMBER:
        case Calendar.DECEMBER:
          log.debug("3rd quarter of this year");
          quarter = "Q3 " + now.get(Calendar.YEAR);
          // set activeCal to September 1st
          activeCal.set(now.get(Calendar.YEAR), Calendar.SEPTEMBER, 1);
          
          // set beginCal to July 1st
          beginCal.set(now.get(Calendar.YEAR), Calendar.JULY, 1);
          
          // set endCal to September 30th
          endCal.set(now.get(Calendar.YEAR), Calendar.SEPTEMBER, 30);
          break;
      }
      
      // now get Dates from Calendars
      activeDate  = new java.sql.Date(activeCal.getTime().getTime());
      beginDate   = new java.sql.Date(beginCal.getTime().getTime());
      endDate     = new java.sql.Date(endCal.getTime().getTime());
      
      log.debug("running query");
      // get visa sales count and volume
      /*@lineinfo:generated-code*//*@lineinfo:166^7*/

//  ************************************************************
//  #sql [Ctx] it = { select 'Credit'                                           as type,    
//      		     to_char(gn.hh_active_date, 'Mon YYYY')                 as activity_month,
//      		     to_char(gn.hh_active_date, 'mm')                       as month,     
//      		     sum( pl.pl_sales_amount - pl.pl_credits_amount )       as vol_amount,
//      		     sum( pl.pl_number_of_credits + pl.pl_number_of_sales ) as vol_count    		     
//      		from   monthly_extract_gn gn,
//      		     monthly_extract_pl pl
//      		where  gn.hh_bank_number = 3942 and
//      		     gn.hh_active_date between :beginDate and last_day(:endDate) and
//      		     pl.hh_load_sec = gn.hh_load_sec and
//      		     pl.pl_plan_type like 'V%' and
//      		     pl.pl_plan_type != 'VS$' and
//      		     pl.pl_plan_type != 'VSDB'
//      		group by gn.hh_active_date 
//      		union
//      		select 'Debit'                                              as type,
//      		     to_char(gn.hh_active_date, 'Mon YYYY')                 as activity_month,
//      		     to_char(gn.hh_active_date, 'mm')                       as month,     
//      		     sum( pl.pl_sales_amount - pl.pl_credits_amount )       as vol_amount,
//      		     sum( pl.pl_number_of_credits + pl.pl_number_of_sales ) as vol_count    		     
//      		from   monthly_extract_gn gn,
//      		     monthly_extract_pl pl
//      		where  gn.hh_bank_number = 3942 and
//      		     gn.hh_active_date between :beginDate and last_day(:endDate) and
//      		     pl.hh_load_sec = gn.hh_load_sec and     
//      		     pl.pl_plan_type = 'VSDB'
//      		group by gn.hh_active_date
//      		order by 3, 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select 'Credit'                                           as type,    \n    \t\t     to_char(gn.hh_active_date, 'Mon YYYY')                 as activity_month,\n    \t\t     to_char(gn.hh_active_date, 'mm')                       as month,     \n    \t\t     sum( pl.pl_sales_amount - pl.pl_credits_amount )       as vol_amount,\n    \t\t     sum( pl.pl_number_of_credits + pl.pl_number_of_sales ) as vol_count    \t\t     \n    \t\tfrom   monthly_extract_gn gn,\n    \t\t     monthly_extract_pl pl\n    \t\twhere  gn.hh_bank_number = 3942 and\n    \t\t     gn.hh_active_date between  :1  and last_day( :2 ) and\n    \t\t     pl.hh_load_sec = gn.hh_load_sec and\n    \t\t     pl.pl_plan_type like 'V%' and\n    \t\t     pl.pl_plan_type != 'VS$' and\n    \t\t     pl.pl_plan_type != 'VSDB'\n    \t\tgroup by gn.hh_active_date \n    \t\tunion\n    \t\tselect 'Debit'                                              as type,\n    \t\t     to_char(gn.hh_active_date, 'Mon YYYY')                 as activity_month,\n    \t\t     to_char(gn.hh_active_date, 'mm')                       as month,     \n    \t\t     sum( pl.pl_sales_amount - pl.pl_credits_amount )       as vol_amount,\n    \t\t     sum( pl.pl_number_of_credits + pl.pl_number_of_sales ) as vol_count    \t\t     \n    \t\tfrom   monthly_extract_gn gn,\n    \t\t     monthly_extract_pl pl\n    \t\twhere  gn.hh_bank_number = 3942 and\n    \t\t     gn.hh_active_date between  :3  and last_day( :4 ) and\n    \t\t     pl.hh_load_sec = gn.hh_load_sec and     \n    \t\t     pl.pl_plan_type = 'VSDB'\n    \t\tgroup by gn.hh_active_date\n    \t\torder by 3, 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.SterlingQuarterlyData",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.SterlingQuarterlyData",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:196^7*/
      
      rs = it.getResultSet();
      
      log.debug("processing resultset");
      int i=0;
      int month = 0;
      int prevMonth = 0;
      String type = "";
      
      while(rs.next())
      {
    	  month = rs.getInt("month");
    	  if(prevMonth != 0 && month != prevMonth) {
    		  i++;
    	  }
    	  
          months[i] = rs.getString("activity_month");
          type = rs.getString("type");
          
          if( type.equals("Credit") ) {        	  
        	  creditCounts[i] = rs.getLong("vol_count");
        	  creditAmounts[i] = rs.getDouble("vol_amount");
        	  
        	  totalMonthCount[i] += creditCounts[i];
        	  totalMonthAmount[i] += creditAmounts[i];
        	  
        	  totalCount += creditCounts[i];
              totalAmount += creditAmounts[i];
        	  
          } else if( type.equals("Debit") ) {        	  
        	  debitCounts[i]   = rs.getLong("vol_count");
        	  debitAmounts[i]  = rs.getDouble("vol_amount");
        	  
        	  totalMonthCount[i] += debitCounts[i];
        	  totalMonthAmount[i] += debitAmounts[i];
        	  
        	  totalCount += debitCounts[i];
              totalAmount += debitAmounts[i];
          }
                    
          prevMonth = month;
      }
      
      rs.close();
      it.close();
      
      log.debug("building e-mail");
      // now generate email
      StringBuffer body = new StringBuffer();
      
      body.append("Total Gross Merchant Sales Volume Reported \n\n");
        
      body.append("Month : " + months[0] + "\n");
      body.append("Sales Volume - Credit : " + NumberFormatter.getDoubleString(creditAmounts[0], CURRENCY_FORMAT) + "\n");
      body.append("Sales Volume - Debit : " + NumberFormatter.getDoubleString(debitAmounts[0], CURRENCY_FORMAT) + "\n");
      body.append("Transaction Count - Credit : " + NumberFormatter.getLongString(creditCounts[0], INTEGER_FORMAT) + "\n");
      body.append("Transaction Count - Debit : " + NumberFormatter.getLongString(debitCounts[0], INTEGER_FORMAT) + "\n");
      body.append("Total Sales Volume : " + NumberFormatter.getDoubleString(totalMonthAmount[0], CURRENCY_FORMAT) + "\n");
      body.append("Total Transaction Count : " + NumberFormatter.getLongString(totalMonthCount[0], INTEGER_FORMAT) + "\n");
      
      body.append("\n");
      
      body.append("Month : " + months[1] + "\n");
      body.append("Sales Volume - Credit : " + NumberFormatter.getDoubleString(creditAmounts[1], CURRENCY_FORMAT) + "\n");
      body.append("Sales Volume - Debit : " + NumberFormatter.getDoubleString(debitAmounts[1], CURRENCY_FORMAT) + "\n");
      body.append("Transaction Count - Credit : " + NumberFormatter.getLongString(creditCounts[1], INTEGER_FORMAT) + "\n");
      body.append("Transaction Count - Debit : " + NumberFormatter.getLongString(debitCounts[1], INTEGER_FORMAT) + "\n");
      body.append("Total Sales Volume : " + NumberFormatter.getDoubleString(totalMonthAmount[1], CURRENCY_FORMAT) + "\n");
      body.append("Total Transaction Count : " + NumberFormatter.getLongString(totalMonthCount[1], INTEGER_FORMAT) + "\n");
      
      body.append("\n");
      
      body.append("Month : " + months[2] + "\n");
      body.append("Sales Volume - Credit : " + NumberFormatter.getDoubleString(creditAmounts[2], CURRENCY_FORMAT) + "\n");
      body.append("Sales Volume - Debit : " + NumberFormatter.getDoubleString(debitAmounts[2], CURRENCY_FORMAT) + "\n"); 
      body.append("Transaction Count - Credit : " + NumberFormatter.getLongString(creditCounts[2], INTEGER_FORMAT) + "\n");
      body.append("Transaction Count - Debit : " + NumberFormatter.getLongString(debitCounts[2], INTEGER_FORMAT) + "\n");
      body.append("Total Sales Volume : " + NumberFormatter.getDoubleString(totalMonthAmount[2], CURRENCY_FORMAT) + "\n");
      body.append("Total Transaction Count : " + NumberFormatter.getLongString(totalMonthCount[2], INTEGER_FORMAT) + "\n");
      
      body.append("\n");
      
	  body.append("Total Quarter Sales Volume - Credit : " + NumberFormatter.getDoubleString(creditAmounts[0] + creditAmounts[1] + creditAmounts[2], CURRENCY_FORMAT) + "\n");
      body.append("Total Quarter Sales Volume - Debit : " + NumberFormatter.getDoubleString(debitAmounts[0] + debitAmounts[1] + debitAmounts[2], CURRENCY_FORMAT) + "\n");    
	  body.append("Total Quarter Transaction Count - Credit : " + NumberFormatter.getLongString(creditCounts[0] + creditCounts[1] + creditCounts[2], INTEGER_FORMAT) + "\n");
	  body.append("Total Quarter Transaction Count - Debit : " + NumberFormatter.getLongString(debitCounts[0] + debitCounts[1] + debitCounts[2], INTEGER_FORMAT) + "\n");
	  body.append("Total Quarter Sales Volume : " + NumberFormatter.getDoubleString(totalMonthAmount[0] + totalMonthAmount[1] + totalMonthAmount[2], CURRENCY_FORMAT) + "\n");
	  body.append("Total Quarter Transaction Count : " + NumberFormatter.getLongString(totalMonthCount[0] + totalMonthCount[1] + totalMonthCount[2], INTEGER_FORMAT) + "\n");
	        
	  
      // send email
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(MesEmails.MSG_ADDRS_STERLING_QUARTERLY_DATA);
      msg.setSubject("VISA " + quarter + " REPORT for Umpqua Bank");
      msg.setText(body.toString());      
      msg.send();
      
      result = true;
      log.debug("done");
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      SterlingQuarterlyData test = new SterlingQuarterlyData();
      
      test.execute();
    }
    catch(Exception e)
    {
      log.error("main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/