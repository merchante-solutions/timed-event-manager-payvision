/*@lineinfo:filename=PCIExceptionQGenerator*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;

public class PCIExceptionQGenerator extends EventBase
{

  // create class log category
  static Logger log = Logger.getLogger(PCIExceptionQGenerator.class);

  private int qType        = MesQueues.Q_PCI_EXCEPTION_NEW;
  private int qItemType    = MesQueues.Q_ITEM_TYPE_PCI_EXCEPTIONS;

  private HashMap exceptionMap    = new HashMap();

  //generic list for method use
  private ArrayList pciExceptions = null;

  //resulting list of new Exceptions to process
  private ArrayList newExceptions = null;


  public boolean execute()
  {

    boolean result = true;

    //gather daily information from quiz tables
    //and populate q_data so that internal staff can handle exceptions
    try
    {
      //call to ensure that all merchants have a level assigned
      calibrateMerchantLevels();

      //this cycles the activity list and generates a list of
      //applicable exceptions
      compileNewExceptions();

      //inserts the exception data into q_data, type = 350, item_type = 43
      insertQData();

      //check all existing merchants that have gone past there one year date
      //and flag them as non-compliant
      checkCompliance();

    }
    catch(Exception e)
    {
      result = false;
    }

    return result;

  }

  private void compileNewExceptions()
  throws Exception
  {
    log.debug("compiling new exceptions");

    try
    {
      connect();

      setAutoCommit(false);

      int type = 1;

      //only 4 types of exceptions currently...
      while (type < 5)
      {
        //Meat of the process here:
        //runs existing exceptions against found exceptions
        //and only adds those not found
        processFoundExceptions(type, runNewExceptions(type));

        type++;
      }

      commit();

    }
    catch (Exception e)
    {
      logEntry("compileNewExceptions()",e.toString());
      //e.printStackTrace();
      rollback();
      throw e;
    }
    finally
    {
      cleanUp();
    }

  }

  private ArrayList runNewExceptions(int type)
  throws Exception
  {

    log.debug("runNewExceptions... type = " + type);

    ResultSetIterator it  = null;
    ResultSet rs          = null;

    String startDate = "01-JUN-10";//"11-NOV-08";

    switch(type)
    {
      case 1:
        //SAQ with Incomplete status of over 60 days
        //need to determine all quizzes that fit this description
        //and filter out the ones that have already been processed
        /*@lineinfo:generated-code*//*@lineinfo:125^9*/

//  ************************************************************
//  #sql [Ctx] it = { select
//              tracking_number
//            from
//              quiz_history
//            where
//              trunc(sysdate) - trunc(date_taken) > 60
//            and
//                tracking_number is not null
//            and
//              result = 'Incomplete'
//            and tracking_number not in
//            (
//              select
//              tracking_number
//              from quiz_exception
//              where type = :type
//            )
//            order by date_taken asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n            tracking_number\n          from\n            quiz_history\n          where\n            trunc(sysdate) - trunc(date_taken) > 60\n          and\n              tracking_number is not null\n          and\n            result = 'Incomplete'\n          and tracking_number not in\n          (\n            select\n            tracking_number\n            from quiz_exception\n            where type =  :1 \n          )\n          order by date_taken asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.PCIExceptionQGenerator",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:145^9*/

        rs = it.getResultSet();

        break;

      case 2:
        //SAQ with N/A selected on any question
        //should only be a daily check
        /*@lineinfo:generated-code*//*@lineinfo:154^9*/

//  ************************************************************
//  #sql [Ctx] it = { select
//              distinct q.tracking_number as tracking_number
//            from
//              quiz_history q,
//              quiz_results qr
//            where
//              qr.answer = 'NA'
//            and
//              qr.history_id = q.id
//            and
//                q.tracking_number is not null
//            and
//              q.date_taken > :startDate
//            and
//              q.result not in ('Reset', 'None')
//            and q.tracking_number not in
//            (
//              select
//              tracking_number
//              from quiz_exception
//              where type = :type
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n            distinct q.tracking_number as tracking_number\n          from\n            quiz_history q,\n            quiz_results qr\n          where\n            qr.answer = 'NA'\n          and\n            qr.history_id = q.id\n          and\n              q.tracking_number is not null\n          and\n            q.date_taken >  :1 \n          and\n            q.result not in ('Reset', 'None')\n          and q.tracking_number not in\n          (\n            select\n            tracking_number\n            from quiz_exception\n            where type =  :2 \n          )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,startDate);
   __sJT_st.setInt(2,type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.PCIExceptionQGenerator",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:178^9*/

        rs = it.getResultSet();

        break;

      case 3:
        //SAQ with Special selected on any question
        //same as 2 above
        /*@lineinfo:generated-code*//*@lineinfo:187^9*/

//  ************************************************************
//  #sql [Ctx] it = { select
//              distinct q.tracking_number as tracking_number
//            from
//              quiz_history q,
//              quiz_results qr
//            where
//              qr.answer = 'CCU'
//            and
//              qr.history_id = q.id
//            and
//                q.tracking_number is not null
//            and
//              q.date_taken > :startDate
//            and
//              q.result not in ('Reset', 'None')
//            and
//              q.tracking_number not in
//              (
//                select
//                tracking_number
//                from quiz_exception
//                where type = :type
//              )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n            distinct q.tracking_number as tracking_number\n          from\n            quiz_history q,\n            quiz_results qr\n          where\n            qr.answer = 'CCU'\n          and\n            qr.history_id = q.id\n          and\n              q.tracking_number is not null\n          and\n            q.date_taken >  :1 \n          and\n            q.result not in ('Reset', 'None')\n          and\n            q.tracking_number not in\n            (\n              select\n              tracking_number\n              from quiz_exception\n              where type =  :2 \n            )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,startDate);
   __sJT_st.setInt(2,type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.PCIExceptionQGenerator",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:212^9*/

        rs = it.getResultSet();

        break;

      case 4:
        //SAQ with any action plan compliance date violation
        //either missing on a 'N' or past due
        /*@lineinfo:generated-code*//*@lineinfo:221^9*/

//  ************************************************************
//  #sql [Ctx] it = { select
//              pcd.tracking_number,
//              pcd.compliant,
//              pcd.compliancedate,
//              sysdate,
//              q.result
//            from
//              pci_compliance_data pcd,
//              quiz_history q
//            where
//            (
//                pcd.compliant = 'N' and
//                 (  pcd.COMPLIANCEDATE is null or
//                    pcd.COMPLIANCEDATE < sysdate
//                 )
//            )
//            and
//                pcd.tracking_number = q.tracking_number
//            and
//                q.tracking_number is not null
//            and
//                q.result not in ('Reset', 'None')
//            and
//                q.tracking_number not in
//                (
//                  select
//                  tracking_number
//                  from quiz_exception
//                  where type = :type
//                )
//            order by submitdate asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n            pcd.tracking_number,\n            pcd.compliant,\n            pcd.compliancedate,\n            sysdate,\n            q.result\n          from\n            pci_compliance_data pcd,\n            quiz_history q\n          where\n          (\n              pcd.compliant = 'N' and\n               (  pcd.COMPLIANCEDATE is null or\n                  pcd.COMPLIANCEDATE < sysdate\n               )\n          )\n          and\n              pcd.tracking_number = q.tracking_number\n          and\n              q.tracking_number is not null\n          and\n              q.result not in ('Reset', 'None')\n          and\n              q.tracking_number not in\n              (\n                select\n                tracking_number\n                from quiz_exception\n                where type =  :1 \n              )\n          order by submitdate asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.PCIExceptionQGenerator",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:254^9*/

        rs = it.getResultSet();

        break;

      default:
        throw new Exception("Exception Type not valid.");

    }

    ArrayList trackNums = new ArrayList();

    if (rs != null)
    {
      while(rs.next())
      {
        trackNums.add(rs.getString("tracking_number"));
      }

      rs.close();
    }

    it.close();

    return trackNums;
  }

  private void processFoundExceptions(int type, ArrayList nums)
  throws Exception
  {

    log.debug("processFoundExceptions...");

    PCIException ex;
    String trackNum;
    boolean found;

    if(nums.size()>0)
    {
      int count = 0;

      for(Iterator it = nums.iterator();it.hasNext();)
      {
        trackNum  = (String)it.next();
        createAndSaveException(trackNum, type);
        count++;
      }
/*
      String _type="";
      switch(type)
      {
        case 1:
          _type = "Incomplete status of over 60 days";
          break;
        case 2:
          _type = "N/A selected on any question";
          break;
        case 3:
          _type = "Special selected on any question";
          break;
        case 4:
          _type = "Any action plan compliance date violation";
          break;
        default:
      };
      log.debug("Exception type: "+ _type +":: count: "+ count );
*/
    }
  }

  private void createAndSaveException(String trackNum, int type)
  throws Exception
  {

    log.debug("running createAndSaveException...");


    if(newExceptions == null)
    {
      newExceptions = new ArrayList();
    }

    long newId = 0L;

    //generate new exception id

    /*@lineinfo:generated-code*//*@lineinfo:341^5*/

//  ************************************************************
//  #sql [Ctx] { select quiz_exception_q_sequence.nextVal
//        
//        from dual
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select quiz_exception_q_sequence.nextVal\n       \n      from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   newId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:346^5*/


    //add the exception to list
    newExceptions.add(new PCIException(newId, trackNum, type));


    //persist to db
    /*@lineinfo:generated-code*//*@lineinfo:354^5*/

//  ************************************************************
//  #sql [Ctx] { insert into quiz_exception
//        (
//          id,
//          tracking_number,
//          type,
//          status,
//          date_submitted
//        )
//        values
//        (
//          :newId,
//          :trackNum,
//          :type,
//          0,
//          sysdate
//        )
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into quiz_exception\n      (\n        id,\n        tracking_number,\n        type,\n        status,\n        date_submitted\n      )\n      values\n      (\n         :1 ,\n         :2 ,\n         :3 ,\n        0,\n        sysdate\n      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,newId);
   __sJT_st.setString(2,trackNum);
   __sJT_st.setInt(3,type);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:372^5*/



  }

  private void insertQData()
  {
    log.debug("inserting q data");

    String source     = "AUTO";
    String userOwner  = "SYSTEM";

    PCIException ex;

    if(newExceptions != null)
    {

      try
      {
        connect();
        setAutoCommit(false);

        for(Iterator i = newExceptions.iterator(); i.hasNext();)
        {
          ex = (PCIException)i.next();

          log.debug(ex.toString());

          //put the new exception in the queues
          /*@lineinfo:generated-code*//*@lineinfo:402^11*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//              (
//                id,
//                type,
//                item_type,
//                owner,
//                date_created,
//                source,
//                last_user
//              )
//              values
//              (
//                :ex.id,
//                :this.qType,
//                :this.qItemType,
//                1,
//                sysdate,
//                :source,
//                :userOwner
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_data\n            (\n              id,\n              type,\n              item_type,\n              owner,\n              date_created,\n              source,\n              last_user\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n              1,\n              sysdate,\n               :4 ,\n               :5 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ex.id);
   __sJT_st.setInt(2,this.qType);
   __sJT_st.setInt(3,this.qItemType);
   __sJT_st.setString(4,source);
   __sJT_st.setString(5,userOwner);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:424^11*/


          //change the status of the Exception to 'queued' = 1
          /*@lineinfo:generated-code*//*@lineinfo:428^11*/

//  ************************************************************
//  #sql [Ctx] { update quiz_exception
//              set status = 1
//              where id = :ex.id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update quiz_exception\n            set status = 1\n            where id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ex.id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:433^11*/

        }//for

        commit();

      }
      catch(Exception e)
      {
        log.debug(e.getMessage());
        logEntry("insertQData()",e.getMessage());
        rollback();
      }
      finally
      {
        cleanUp();
      }

    }

  }


  public class PCIException
  {
    public long id;
    public int type;
    public String trackNum;

    public PCIException(){};

    public PCIException(long id, String trackNum, int type)
    {
      this.id = id;
      this.trackNum = trackNum;
      this.type = type;
    }

    public PCIException (String trackNum, int type)
    {
      this.id = -1;
      this.trackNum = trackNum;
      this.type = type;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append("id = ").append(id).append(" -- ");
      sb.append("trackNum = ").append(trackNum).append(" -- ");
      sb.append("type = ").append(type);
      return sb.toString();
    }

  }


  //convenience method for manual trigger of
  //exception processing
  public void processQDataOnly()
  {

    log.debug("running processQDataOnly()...");

    ResultSetIterator it  = null;
    ResultSet rs          = null;
    newExceptions         = new ArrayList();

    try
    {

      connect();

      //generate list of exceptions
      /*@lineinfo:generated-code*//*@lineinfo:507^7*/

//  ************************************************************
//  #sql [Ctx] it = { select
//            id,
//            tracking_number,
//            type
//          from
//            quiz_exception
//          where
//            status = 0
//          order by
//            id asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n          id,\n          tracking_number,\n          type\n        from\n          quiz_exception\n        where\n          status = 0\n        order by\n          id asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.PCIExceptionQGenerator",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:519^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        newExceptions.add(new PCIException(rs.getLong("id"),
                                            rs.getString("tracking_number"),
                                            rs.getInt("type")));
      }
    }
    catch(Exception e)
    {
        logEntry("processQDataOnly()",e.getMessage());
    }
    finally
    {
     cleanUp();
    }

    log.debug("OK to process exceptions, count --->" + newExceptions.size());

    //insert the q_data
    insertQData();

  }

  //select merchant list from quiz history
  public void calibrateMerchantLevels()
  {
    log.debug("running calibrateMerchantLevels()...");

    ResultSetIterator it  = null;
    ResultSet rs          = null;
    ArrayList mids        = new ArrayList();

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:559^7*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct merch_number
//          from quiz_history
//          where merchant_level is null
//          and merch_number != -1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct merch_number\n        from quiz_history\n        where merchant_level is null\n        and merch_number != -1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.startup.PCIExceptionQGenerator",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:565^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        mids.add(rs.getString("merch_number"));
      }

      rs.close();
      it.close();

      String merchNum;
      int merchLevel = -1;
      long count;
      long counter = 0;

      log.debug("merchant count..."+mids.size());
      for(Iterator iter = mids.iterator(); iter.hasNext();)
      {
        //log.debug("calibrating counter..." + counter++);
        merchNum = (String)iter.next();

        //Set Merchant Level
        /*@lineinfo:generated-code*//*@lineinfo:589^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(sm.vmc_sales_count),0)
//          
//          from    monthly_extract_summary sm
//          where   sm.merchant_number = :merchNum and
//                  sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(sm.vmc_sales_count),0)\n         \n        from    monthly_extract_summary sm\n        where   sm.merchant_number =  :1  and\n                sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:596^9*/

        if(count > 6000000L)
        {
          merchLevel = 1;
        }
        else if(6000000L >= count && count >= 1000000L)
        {
          merchLevel = 2;
        }
        else
        {
          //check only ecommerce transactions
          /*@lineinfo:generated-code*//*@lineinfo:609^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(sm.vmc_sales_count),0)
//              
//              from    monthly_extract_summary   sm,
//                      merch_pos mp,
//                      pos_category pc,
//                      merchant m
//              where   sm.merchant_number = :merchNum and
//                      sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month') and
//                      sm.merchant_number = m.merch_number and
//                      m.app_seq_num = mp.app_seq_num and
//                      mp.pos_code = pc.pos_code and
//                      pc.pos_type not in (1, 4) --everything else could be considered e-commerce
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(sm.vmc_sales_count),0)\n             \n            from    monthly_extract_summary   sm,\n                    merch_pos mp,\n                    pos_category pc,\n                    merchant m\n            where   sm.merchant_number =  :1  and\n                    sm.active_date between trunc(sysdate-365,'month') and trunc(sysdate,'month') and\n                    sm.merchant_number = m.merch_number and\n                    m.app_seq_num = mp.app_seq_num and\n                    mp.pos_code = pc.pos_code and\n                    pc.pos_type not in (1, 4) --everything else could be considered e-commerce";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:623^11*/

          if(count < 20000)
          {
           merchLevel = 4;
          }
          else
          {
           merchLevel = 3;
          }
        }

        /*@lineinfo:generated-code*//*@lineinfo:635^9*/

//  ************************************************************
//  #sql [Ctx] { update quiz_history set merchant_level = :merchLevel
//            where merch_number = :merchNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update quiz_history set merchant_level =  :1 \n          where merch_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,merchLevel);
   __sJT_st.setString(2,merchNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:639^9*/
      }

    }
    catch(Exception e)
    {
        logEntry("calibrateMerchantLevels()",e.getMessage());
    }
    finally
    {
      cleanUp();
    }
  }

  private void checkCompliance()
  {
    connect();

    try
    {
      log.debug("running checkCompliance()...");

      ResultSetIterator it  = null;
      ResultSet rs          = null;
      List mids             = new ArrayList();

      /*@lineinfo:generated-code*//*@lineinfo:665^7*/

//  ************************************************************
//  #sql [Ctx] it = { select
//            distinct qh.merch_number,
//            qh.tracking_number,
//            mf.dba_name,
//            nvl(pcd.m_email,nvl(mer.merch_email_address,'not found')) as email,
//            qh.date_taken as mark_date,
//            to_char(qh.date_taken,'mm/dd/yyyy') as date_taken,
//            m.diff
//          from
//            quiz_history qh,
//            (select
//              merch_number as mid,
//              round(sysdate - max(date_taken),0) as diff
//             from quiz_history
//             group by merch_number) m,
//            merchant mer,
//            mif mf,
//            pci_compliance_data pcd
//          where
//            qh.merch_number = m.mid
//            and m.diff between 365 and 372
//            and mer.merch_number(+) = qh.merch_number
//            and mer.risk_pci_compliant = 'Y'
//            and qh.merch_number = mf.merchant_number(+)
//            and mf.date_stat_chgd_to_dcb is null
//            and qh.tracking_number = pcd.tracking_number(+)
//          order by qh.merch_number, qh.date_taken desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n          distinct qh.merch_number,\n          qh.tracking_number,\n          mf.dba_name,\n          nvl(pcd.m_email,nvl(mer.merch_email_address,'not found')) as email,\n          qh.date_taken as mark_date,\n          to_char(qh.date_taken,'mm/dd/yyyy') as date_taken,\n          m.diff\n        from\n          quiz_history qh,\n          (select\n            merch_number as mid,\n            round(sysdate - max(date_taken),0) as diff\n           from quiz_history\n           group by merch_number) m,\n          merchant mer,\n          mif mf,\n          pci_compliance_data pcd\n        where\n          qh.merch_number = m.mid\n          and m.diff between 365 and 372\n          and mer.merch_number(+) = qh.merch_number\n          and mer.risk_pci_compliant = 'Y'\n          and qh.merch_number = mf.merchant_number(+)\n          and mf.date_stat_chgd_to_dcb is null\n          and qh.tracking_number = pcd.tracking_number(+)\n        order by qh.merch_number, qh.date_taken desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.startup.PCIExceptionQGenerator",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:694^7*/

      rs = it.getResultSet();

      long currentMID = 0;
      long lastMID    = 0;
      Emid _emid;

      if (rs != null)
      {
        while(rs.next())
        {
          currentMID = rs.getLong("merch_number");
          if(currentMID != lastMID)
          {
            _emid = new Emid(currentMID,
                     rs.getString("tracking_number"),
                     rs.getString("dba_name"),
                     rs.getString("email"),
                     rs.getString("date_taken"));

            mids.add( _emid );

           lastMID = currentMID;
          }
        }

        rs.close();
      }

      it.close();

      Emid mid;
      List yesMids = new Vector();
      List noMids  = new Vector();

      for(Iterator itr = mids.iterator(); itr.hasNext();)
      {
        mid = (Emid)itr.next();
        /*@lineinfo:generated-code*//*@lineinfo:733^9*/

//  ************************************************************
//  #sql [Ctx] { update merchant
//            set risk_pci_compliant = 'N'
//            where merch_number = :mid.mid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update merchant\n          set risk_pci_compliant = 'N'\n          where merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.PCIExceptionQGenerator",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mid.mid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:738^9*/

        //email each merchant, copy pci dept
        if( emailNotification( mid ) )
        {
          yesMids.add(mid);
        }
        else
        {
          noMids.add(mid);
        }

      }

      doInternalNotification(yesMids, noMids);

    }
    catch(Exception e)
    {
      logEntry("checkCompliance()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public final static String PCI_EMAIL = "PCI-help@merchante-solutions.com";

  private int count = 0;

  private boolean emailNotification(Emid mid)
  {
    boolean sent = false;
    /*
    //Disabled via DBRQ-4482
    MailMessage msg;

    //quick test to ensure it's a valid email format
    if( mid.validEmail() )
    {
      try
      {
        //count is for testing only... remove if on prod
        //if(count == 0)
        //{
          //email the merchant about compliance change
          msg = new MailMessage();
          msg.setFrom( PCI_EMAIL );
          msg.setSubject("PCI Renewal Notification");
          msg.setText( mid.getMIDMessage() );
          msg.addTo( mid.email );
          //msg.addTo( PCI_EMAIL );
          //msg.addTo( "rsorensen@merchante-solutions.com" );
          msg.send();
          //count++;
        //}
        sent = true;
      }
      catch(Exception e)
      {
        logEntry("emailNotification()::"+mid.mid,e.getMessage());
      }

    }
    */

    return sent;
  }

  private void doInternalNotification(List ok, List notOk)
  throws Exception
  {
    //send one email, defining who was successfully
    //notified and who wasn't
    Emid mid;

    StringBuffer sb = new StringBuffer();
    sb.append("PCI Notifications:\n\n").append("SUCCESS:\n");
    sb.append("Type\tMerchant ID\tDate Taken\tEmail\t\t\tDBA Name\n");
    sb.append("------------------------------------------------------------------------------------\n");
    for (Iterator it = ok.iterator();it.hasNext();)
    {
      mid = (Emid)it.next();
      sb.append(mid.getDeets());
    }

    sb.append("\n\n").append("FAILURE:\n");
    sb.append("Type\tMerchant ID\tDate Taken\tEmail\t\t\tDBA Name\n");
    sb.append("------------------------------------------------------------------------------------\n");
    for (Iterator it = notOk.iterator();it.hasNext();)
    {
      mid = (Emid)it.next();
      sb.append(mid.getDeets());
    }

    //log.debug(sb.toString());

    MailMessage msg = new MailMessage();
    msg.setFrom( PCI_EMAIL );
    msg.setSubject("PCI EMAIL REMINDER NOTICE SENT");
    msg.setText( sb.toString() );
    msg.addTo( PCI_EMAIL );
    //msg.addTo( "rsorensen@merchante-solutions.com" );
    msg.send();

  }

  public void processComplianceOnly()
  {
    checkCompliance();
  }

  public class Emid
  {
    public long   mid;
    public String trackNum;
    public String name;
    public String email;
    public String dateTaken;

    public Emid(long _mid, String tn, String _name, String _email, String dt)
    {
      mid        = _mid;
      trackNum   = tn;
      name       = _name;
      email      = _email;
      dateTaken  = dt;

      if(trackNum==null) trackNum = "0";
    }

    //for internal mailing
    public String getDeets()
    {
      StringBuffer sb = new StringBuffer();
      if (trackNum.length() < 5) {
        sb.append("--\t");
      }
      else {
        sb.append(trackNum.substring(3,4)).append("\t");
      }
      sb.append(mid).append("\t");
      sb.append(dateTaken).append("\t");
      sb.append(email).append("\t");
      sb.append(name).append("\n");
      return sb.toString();
    }

    public String getMIDMessage()
    {
      StringBuffer sb = new StringBuffer();
      sb.append("Dear Merchant,\n\n");
      sb.append("It appears to be that time again to complete the Security Questionnaire for your business. Our records indicate the last completion date was ");
      sb.append(dateTaken);
      sb.append(". Reference Number ").append(trackNum).append("\n\n");
      sb.append("\t- Go to the Merchant e-Solutions web site (www.merchante-solutions.com) and click on Login.\n");
      sb.append("\t- For your User ID, enter your Merchant ID number\n");
      sb.append("\t- For your Activation Code, enter your 10-digit business phone number or enter your Password if you�ve already created one previously.\n");
      sb.append("\t- On the Main Menu, select PCI Compliance Questionnaire.\n\n");
      sb.append("Select and complete the questionnaire most applicable to your business. Please note a new CVT Version is available for IP based Virtual Terminals.\n\n");
      sb.append("Sincerely,\n\n");
      sb.append("Stephen R. Prince\nMerchant Services\nOperations Manager");
      return sb.toString();
    }

    public boolean validEmail()
    {
      return (email.indexOf("@") > -1 && email.indexOf(".") > -1);
    }
  }

  public static void main( String[] args )
  {
    PCIExceptionQGenerator     test      = null;

    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");

      test = new PCIExceptionQGenerator();

      //test.execute();

      //Specific testing scenarios
      //test.processQDataOnly();
      test.processComplianceOnly();

    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      log.debug("done");
    }
  }


}/*@lineinfo:generated-code*/