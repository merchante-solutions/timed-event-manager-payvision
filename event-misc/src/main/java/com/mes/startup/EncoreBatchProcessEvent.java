/*@lineinfo:filename=EncoreBatchProcessEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/EncoreBatchProcessEvent.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;
import com.jscape.inet.sftp.Sftp;
import com.mes.api.ApiRequestFactory;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiRequest;
import com.mes.api.TridentApiResponse;
import com.mes.api.TridentApiTranBase;
import com.mes.config.MesDefaults;
import com.mes.constants.MesFlatFiles;
import com.mes.flatfile.FlatFileRecord;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import com.mes.support.StringUtilities;
import com.mes.support.ThreadPool;
import com.mes.support.TridentTools;
import com.mes.support.WildCardFilter;

public class EncoreBatchProcessEvent
  extends EventBase
{
  public static final String    ENCORE_ARC_DIR    = "./arc/encore";
  
  public class ApiTranThread 
    implements Runnable
  {
    private   PtBpTran        BpTran        = null;
    
    public ApiTranThread( PtBpTran bpTran )
    {
      BpTran        = bpTran;
    }
    
    protected boolean isCardTypeAccepted( String cardType )
    {
      return( cardType != null && "VI,MC,AX,DI,DD,JC".indexOf(cardType) >= 0 );
    }
    
    public void run()
    {
      TridentApiResponse      response      = null;
      
      if ( BpTran.getTranType() != null && isCardTypeAccepted(BpTran.getCardType()) )
      {
        TridentApiRequest apiRequest = ApiRequestFactory.createRequest(BpTran.getCredentials(),BpTran.getTranType());
        apiRequest.setArg(TridentApiConstants.FN_MOTO_IND       ,BpTran.getMotoEcommInd() );
        apiRequest.setArg(TridentApiConstants.FN_CARD_NUMBER    ,BpTran.getCardNumber()   );
        apiRequest.setArg(TridentApiConstants.FN_EXP_DATE       ,BpTran.getExpDate()      );
        apiRequest.setArg(TridentApiConstants.FN_AMOUNT         ,BpTran.getAmount()       );
        apiRequest.setArg(TridentApiConstants.FN_INV_NUM        ,BpTran.getInvoiceNumber());
        apiRequest.setArg(TridentApiConstants.FN_AVS_STREET     ,BpTran.getAvsStreet()    );
        apiRequest.setArg(TridentApiConstants.FN_AVS_ZIP        ,BpTran.getAvsZip()       );
        apiRequest.setArg(TridentApiConstants.FN_DBA_NAME       ,BpTran.getDbaName()      );
        apiRequest.setArg(TridentApiConstants.FN_PHONE          ,BpTran.getPhoneNumber()  );
        apiRequest.setArg(TridentApiConstants.FN_CURRENCY_CODE  ,BpTran.getCurrencyCode() );
        apiRequest.setArg(TridentApiConstants.FN_CLIENT_REF_NUM ,BpTran.getClientRefNum() );
        apiRequest.setPostUrl(ApiUrl);
        BpTran.setApiResponse( apiRequest.post() );
      }        
    }
  }
  
  public class PtBpTran
  {
    private   TridentApiResponse        ApiResponse       = null;
    private   double                    Amount            = 0.0;
    private   boolean                   AvsAddr           = false;
    private   String                    AvsStreet         = null;
    private   String                    AvsZip            = null;
    private   String                    CardNumber        = null;
    private   String                    CardType          = null;
    private   String                    ClientRefNum      = null;
    private   String[]                  Credentials       = null;
    private   String                    CurrencyCode      = null;
    private   String                    DbaName           = null;
    private   FlatFileRecord            DetailRecord      = null;
    private   String                    ExpDate           = null;
    private   String                    InvoiceNumber     = null;
    private   String                    MotoEcommInd      = null;
    private   String                    PhoneNumber       = null;
    private   String                    TranType          = null;
    
    public PtBpTran()
    {
    }
    
    public void add( String rawData )
    {
      FlatFileRecord      ffd           = null;
      
      switch( rawData.charAt(0) )
      {
        case 'S':   // detail record
          ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_EBP_DTL);
          ffd.suck(rawData);
          CardType      = ffd.getFieldData("method_of_payment").trim();
          CardNumber    = ffd.getFieldData("card_number").trim();
          ExpDate       = ffd.getFieldData("exp_date");
          Amount        = ffd.getFieldAsDouble("tran_amount");
          CurrencyCode  = ffd.getFieldData("currency_code");
          InvoiceNumber = ffd.getFieldData("invoice_number").trim();
          ClientRefNum  = ffd.getFieldData("merchant_data").trim();
          MotoEcommInd  = ffd.getFieldData("moto_ecommerce_ind").trim();
          TranType      = decodeTranType(ffd.getFieldData("action_code").trim());
          Credentials   = loadCredentials("90" + ffd.getFieldData("division").trim());
          
          // truncate the card number for the response file
          ffd.setFieldData("card_number",TridentTools.encodeCardNumber(CardNumber));
          DetailRecord = ffd;
          break;
      
        case 'M':   // soft descriptor (aka dynamic dba)
          ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_EBP_DDBA);
          ffd.suck(rawData);
          DbaName     = ffd.getFieldData("dba_name").trim();
          PhoneNumber = ffd.getFieldData("cust_serv_phone").trim().replaceAll("-","");
          break;
        
        case 'A':   // address data
          if ( AvsAddr )    // only process billing address data
          {
            ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_EBP_ADDR);
            ffd.suck(rawData);
          
            String atype = ffd.getFieldData("addr_type");
            if ( "2".equals(atype) )
            {
              AvsStreet   = ffd.getFieldData("addr_data").trim();
            }
            else if ( "3".equals(atype) )
            {
              String addrData = ffd.getFieldData("addr_data");
              AvsZip   = addrData.substring(addrData.length()-10,addrData.length()-5);
            }
          }
          break;
      }
    }
    
    protected String decodeTranType( String actionCode )
    {
      String    retVal    = null;
      
           if ( "B".equals(actionCode) )  { retVal = TridentApiTranBase.TT_DEBIT;       }
      else if ( "F".equals(actionCode) )  { retVal = TridentApiTranBase.TT_CARD_VERIFY; }
      else if ( "A".equals(actionCode) )  { retVal = TridentApiTranBase.TT_PRE_AUTH;    }
      else if ( "R".equals(actionCode) )  { retVal = TridentApiTranBase.TT_CREDIT;      }
      
      return( retVal );
    }
    
    public String encodeResponse()
    {
      StringBuffer    retVal      = new StringBuffer();
      
      if ( ApiResponse != null )
      {
        // set response values
        DetailRecord.setFieldData("response_code"  , ApiResponse.getParameter(TridentApiConstants.FN_ERROR_CODE)  );
        DetailRecord.setFieldData("auth_code"      , ApiResponse.getParameter(TridentApiConstants.FN_AUTH_CODE)   );
        DetailRecord.setFieldData("avs_result"     , ApiResponse.getParameter(TridentApiConstants.FN_AVS_RESULT)  );
        DetailRecord.setFieldData("cvv2_result"    , ApiResponse.getParameter(TridentApiConstants.FN_CVV2_RESULT) );
        DetailRecord.setFieldData("response_date"  , Calendar.getInstance().getTime()                             );
        DetailRecord.setFieldData("deposit_flag"   , TridentApiConstants.ER_NONE.equals(ApiResponse.getParameter(TridentApiConstants.FN_ERROR_CODE)) ? "Y" : "N");
      }
      else  // no api response, indicate invalid request
      {
        DetailRecord.setFieldData("response_code"  , "102"    );  // bad request
        DetailRecord.setFieldData("auth_code"      , "000000" );
        DetailRecord.setFieldData("response_date"  , Calendar.getInstance().getTime());
        DetailRecord.setFieldData("deposit_flag"   , "N"      );
      }
      retVal.append(DetailRecord.spew());
      retVal.append(System.getProperty("line.separator"));
      
      return( retVal.toString() );
    }
    
    public double   getAmount()         { return(Amount);           }
    public String   getAvsStreet()      { return(AvsStreet);        }
    public String   getAvsZip()         { return(AvsZip);           }
    public String   getCardNumber()     { return(CardNumber);       }
    public String   getCardType()       { return(CardType);         }
    public String   getClientRefNum()   { return(ClientRefNum);     }
    public String[] getCredentials()    { return(Credentials);      }
    public String   getCurrencyCode()   { return(CurrencyCode);     }
    public String   getDbaName()        { return(DbaName);          }
    public String   getExpDate()        { return(ExpDate);          }
    public String   getInvoiceNumber()  { return(InvoiceNumber);    }
    public String   getMotoEcommInd()   { return(MotoEcommInd);     }
    public String   getPhoneNumber()    { return(PhoneNumber);      }
    public String   getTranType()       { return(TranType);         }
    
    public void setApiResponse( TridentApiResponse resp )
    {
      ApiResponse = resp;
    }
    
    public void setAvsAddrFlag( char flag )
    {
      AvsAddr = (flag == 'B');    // billing address
    }
  }
  
  private   String    ApiUrl          = "https://test.merchante-solutions.com/mes-api/tridentApi";
  private   boolean   RunImmediately  = false;

  public EncoreBatchProcessEvent( )
  {
    PropertiesFilename = "encore-bp.properties";
  }
  
  protected void archiveResponseFile( String respFilename )
  {
    try
    {
      File  respFile  = new File(respFilename);
      File  arcDir    = new File(ENCORE_ARC_DIR);
    
      if ( !arcDir.exists() )
      {
        arcDir.mkdirs();
      }
      if ( !respFile.renameTo(new File(arcDir, respFile.getName())) )
      {
        logEntry("archiveResponseFile(" + respFilename + ")","Failed to archive Encore response file '" + respFilename + "'");
      }
    }
    catch( Exception e )
    {
      logEntry("archiveResponseFile(" + respFilename + ")",e.toString());
    }
  }
  
  public boolean execute()
  {
    Vector            filesToProcess    = new Vector();
    String            loadFilename      = null;
    String            remoteFilename    = null;
    int               recCount          = 0;
    String            respFilename      = null;
    boolean           retVal            = false;
    Sftp              sftp              = null;
    
    try
    {
      connect(true);
      
      if ( getEventArgCount() > 1 )
      {
        RunImmediately = "true".equals(getEventArg(1));
      }
      
      int lastDate  = getLastDate();
      int date      = Integer.parseInt(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"yyyyMMddHH"));
      int minute    = Integer.parseInt(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"mm"));
      
      if ( RunImmediately || (minute >= 15 && minute <= 20 && lastDate != date) )
      {
        setApiUrl( getEventArg(0) );
      
        //get the Sftp connection
        sftp = getSftp( MesDefaults.getString(MesDefaults.DK_ENCORE_BP_HOST),         // host
                        MesDefaults.getString(MesDefaults.DK_ENCORE_BP_USER),         // user
                        MesDefaults.getString(MesDefaults.DK_ENCORE_BP_PW),           // password
                        MesDefaults.getString(MesDefaults.DK_ENCORE_BP_DOWNLOAD_PATH),// path
                        true                                                          // binary
                      );

        String fileMask = "EM.*\\.TXT\\.pgp";
        EnumerationIterator enumi = new EnumerationIterator();
        Iterator it = enumi.iterator(sftp.getNameListing(fileMask));
      
        while( it.hasNext() )
        {
          remoteFilename  = (String)it.next();

          log.debug("Downloading '" + remoteFilename + "'");
          sftp.download(remoteFilename,remoteFilename);
          sftp.deleteFile(remoteFilename);
          filesToProcess.add(remoteFilename);
        }        
        sftp.disconnect();
      
        for( int i = 0; i < filesToProcess.size(); ++i )
        {
          remoteFilename  = (String)filesToProcess.elementAt(i);
          respFilename    = remoteFilename.replaceAll(".TXT.pgp",".rsp.txt");
          loadFilename    = generateFilename("em_bp9999",Calendar.getInstance().getTime(),Integer.parseInt(remoteFilename.substring(2,remoteFilename.indexOf("."))),".dat");
      
          log.debug("Decrypting '" + remoteFilename + "'");
          String[] command = { "/bin/ksh", "-c", (getGpgCommand() + " --no-tty -o " + loadFilename + " -d " + remoteFilename + " 2>> ep_bp.log") };
          Process process = Runtime.getRuntime().exec(command);
          process.waitFor();
        
          /*@lineinfo:generated-code*//*@lineinfo:328^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    trident_api_encore_log
//              where   encore_filename = :remoteFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    trident_api_encore_log\n            where   encore_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.EncoreBatchProcessEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,remoteFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:334^11*/
        
          if ( recCount == 0 )
          {
            // store the filename to prevent duplication
            /*@lineinfo:generated-code*//*@lineinfo:339^13*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_api_encore_log
//                (
//                  encore_filename,
//                  load_filename,
//                  process_date
//                )
//                values
//                (
//                  :remoteFilename,
//                  :loadFilename,
//                  sysdate
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into trident_api_encore_log\n              (\n                encore_filename,\n                load_filename,\n                process_date\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                sysdate\n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.EncoreBatchProcessEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,remoteFilename);
   __sJT_st.setString(2,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:353^13*/
            log.debug("Processing '" + loadFilename + "'");
            processFile(loadFilename,respFilename);
            
            // if the processed file was successfully
            // archived, then delete the incoming file
            // and the response file from the filesystem
            if ( archiveDailyFile(loadFilename) )
            {
              new File(remoteFilename).delete();
            }
          }
          else
          {
            logEntry("execute()","Duplicate file skipped - " + remoteFilename);
            new File(loadFilename).delete();
          }
        }
      
        // connect to the SFTP server to upload response file(s)
        sftp = getSftp( MesDefaults.getString(MesDefaults.DK_ENCORE_BP_HOST),         // host
                        MesDefaults.getString(MesDefaults.DK_ENCORE_BP_USER),         // user
                        MesDefaults.getString(MesDefaults.DK_ENCORE_BP_PW),           // password
                        MesDefaults.getString(MesDefaults.DK_ENCORE_BP_UPLOAD_PATH),  // path
                        false                                                         // !binary
                      );
      
        for( int i = 0; i < filesToProcess.size(); ++i )
        {
          remoteFilename  = (String)filesToProcess.elementAt(i);
          respFilename    = remoteFilename.replaceAll(".TXT.pgp",".rsp.txt");
      
          if ( new File(respFilename).exists() )
          {
            log.debug("Uploading '" + respFilename + "'");
            sftp.upload(respFilename,respFilename);
            archiveResponseFile(respFilename);
          }
        }        
        sftp.disconnect();
        
        // remove old response files
        skimResponseArchive();
        
        storeLastDate(date);
      }
      
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected String getGpgCommand()
  {
    PropertiesFile      propsFile     = null;
    
    try
    {
      propsFile   = new PropertiesFile( PropertiesFilename );
    } catch(Exception e) {}
    return( propsFile.getString("gpg.cmd","gpg") );
  }
  
  protected int getLastDate()
  {
    PropertiesFile      propsFile     = null;
    
    try
    {
      propsFile   = new PropertiesFile( PropertiesFilename );
    } catch(Exception e) {}
    return( propsFile.getInt("last.date",2012010100) );
  }
  
  protected String[] loadCredentials( String catid )
  {
    String      profileId     = null;
    String      profileKey    = null;
    String[]    retVal        = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:445^7*/

//  ************************************************************
//  #sql [Ctx] { select  terminal_id, merchant_key
//          
//          from    trident_profile   tp
//          where   tp.catid = :catid
//                  and tp.api_enabled = 'Y'
//                  and exists
//                  (
//                    select  mf.merchant_number
//                    from    mif     mf
//                    where   mf.association_node = 3943600380
//                            and mf.merchant_number = tp.merchant_number
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  terminal_id, merchant_key\n         \n        from    trident_profile   tp\n        where   tp.catid =  :1 \n                and tp.api_enabled = 'Y'\n                and exists\n                (\n                  select  mf.merchant_number\n                  from    mif     mf\n                  where   mf.association_node = 3943600380\n                          and mf.merchant_number = tp.merchant_number\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.EncoreBatchProcessEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,catid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   profileId = (String)__sJT_rs.getString(1);
   profileKey = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:459^7*/
      retVal = new String[] { profileId, profileKey };
    }
    catch( Exception e )
    {
      retVal = new String[] { "bad-catid-" + catid, "invalid-profile-key" };
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  public void processFile( String inputFilename, String responseFilename )
  {
    PtBpTran            bpTran              = null;
    String              header              = null;
    BufferedReader      in                  = null;
    String              line                = null;
    BufferedWriter      out                 = null;
    Vector              trailers            = new Vector(3);
    Vector              tranList            = new Vector();
    
    try
    {
      in = new BufferedReader( new FileReader(inputFilename) );
      
      while( (line = in.readLine()) != null )
      {
        line = StringUtilities.leftJustify(line,96,' ');  // pad to 96 bytes
        char recType = line.charAt(0);
        if ( line.startsWith("PID") ) // header/trailer
        {
          if ( line.indexOf("START") >= 0 )
          {
            header = line;
          }
          else
          {
            trailers.add(line);
          }
          continue;
        }
        else if ( recType == 'B' || recType == 'T' )  // batch and file totals
        {
          trailers.add(line);
          continue;
        }
        else if ( recType == 'M' )    // merchant descriptor
        {
          bpTran = new PtBpTran();
          tranList.addElement(bpTran);
        }
        else if ( recType == 'A' )    // address data
        {
          if ( !Character.isDigit(line.charAt(1)) )
          {
            bpTran.setAvsAddrFlag(line.charAt(1));
          }
        }
        else if ( recType == 0x1a )
        {
          continue;   // skip
        }
        bpTran.add(line);
      }
      
      ThreadPool pool = new ThreadPool(7);
      
      for( int i = 0; i < tranList.size(); ++i )
      {
        Thread thread = pool.getThread( new ApiTranThread((PtBpTran)tranList.elementAt(i)) );
        thread.start();
      }
      pool.waitForAllThreads();
      
      out = new BufferedWriter( new FileWriter(responseFilename) );
      out.write(header);
      out.newLine();
      
      for( int i = 0; i < tranList.size(); ++i )
      { 
        bpTran = (PtBpTran)tranList.elementAt(i);
        out.write(bpTran.encodeResponse());
      }
      for( int i = 0; i < trailers.size(); ++i )
      {
        out.write((String)trailers.elementAt(i));
        out.newLine();
      }
      out.close();
    }
    catch( Exception e )
    {
      logEntry("processFile(" + inputFilename + ")", e.toString());
    }
    finally
    {
      try{ in.close();  } catch( Exception ee ) {}
      try{ out.close(); } catch( Exception ee ) {}
    }
  }
  
  public void setApiUrl ( String host )
  {
    if ( "prod".equals(host) )
    {
      ApiUrl  = "https://api.merchante-solutions.com/mes-api/tridentApi";
    }
    else if ( "cert".equals(host) )
    {
      ApiUrl  = "https://cert.merchante-solutions.com/mes-api/tridentApi";
    }
    else if ( "test".equals(host) )
    {
      ApiUrl  = "https://test.merchante-solutions.com/mes-api/tridentApi";
    }
    else
    {
      ApiUrl  = host;
    }
  }
  
  protected void skimResponseArchive()
  {
    Calendar      cal       = Calendar.getInstance();
    long          expTs     = 0L;
    
    try
    {
      cal.set(Calendar.HOUR,0);
      cal.set(Calendar.MINUTE,0);
      cal.set(Calendar.SECOND,0);
      cal.set(Calendar.MILLISECOND,0);
      cal.add(Calendar.DAY_OF_MONTH,-15);
      expTs = cal.getTime().getTime();
      
      WildCardFilter filter = new WildCardFilter("*.rsp.txt");
      
      File    dir       = new File(ENCORE_ARC_DIR);
      File[]  fileList  = dir.listFiles(filter);
      
      if( fileList != null )
      {
        for( int i = 0; i < fileList.length; ++i )
        {
          if ( fileList[i].lastModified() < expTs )
          {
            fileList[i].delete();
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry("skimResponseArchive()",e.toString());
    }
    finally
    {
    }
  }
  
  protected void storeLastDate( int date )
  {
    PropertiesFile      propsFile     = null;
    
    try
    {
      propsFile   = new PropertiesFile( PropertiesFilename );
      propsFile.load( PropertiesFilename );
    } catch(Exception e) {}
  
    // store the new filename and risk timestamp
    propsFile.setProperty("last.date",String.valueOf(date));
    propsFile.store( PropertiesFilename );
  }

  public static void main(String[] args)
  {
    EncoreBatchProcessEvent         t     = null;

    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone("ERROR");
      
      t = new EncoreBatchProcessEvent();

      if ( "processFile".equals(args[0]) )
      {
        if ( args.length > 2 ) 
        {
          t.setApiUrl(args[2]);
        }
        t.processFile(args[1],args[1] + ".rsp.txt");
      }
      else if ( "execute".equals(args[0]) )
      {
        t.setEventArgs( (args.length > 1) ? args[1] : "test" );
        t.execute();
      }
      else
      {
        System.out.println("Unrecognized command: " + args[0]);
      }
    }
    catch(Exception e)
    {
      System.out.println(e.toString());
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/