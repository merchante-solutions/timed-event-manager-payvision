/*@lineinfo:filename=BBTEmailer*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/BBTEmailer.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;

public class BBTEmailer extends EventBase
{
  private   int     procSequence      = 0;
  
  public BBTEmailer()
  {
    super();
  }
  
  private void getProcSequence()
  {
    try
    {
      // get the processSequence
      /*@lineinfo:generated-code*//*@lineinfo:50^7*/

//  ************************************************************
//  #sql [Ctx] { select  summarizer_process_sequence.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  summarizer_process_sequence.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.BBTEmailer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:54^7*/
      
    }
    catch(Exception e)
    {
      logEntry("getProcSequence()", e.toString());
    }
  }
  
  public boolean execute()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;
    StringBuffer        body    = new StringBuffer("");
    
    try
    {
      connect();
      
      getProcSequence();
      
      // mark rows with sequence so we can track them later
      int emailDelay = MesDefaults.getInt(MesDefaults.DK_BBT_ENROLL_EMAIL_DELAY);
      
      /*@lineinfo:generated-code*//*@lineinfo:79^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_enrollment_bbt
//          set     process_sequence = :procSequence
//          where   process_sequence is null and
//                  date_email_sent is null and
//                  trunc(date_mif_received) <= (trunc(sysdate - :emailDelay))
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_enrollment_bbt\n        set     process_sequence =  :1 \n        where   process_sequence is null and\n                date_email_sent is null and\n                trunc(date_mif_received) <= (trunc(sysdate -  :2 ))";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.BBTEmailer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   __sJT_st.setInt(2,emailDelay);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:86^7*/
      
      commit();
      
      // get all marked rows and send email
      /*@lineinfo:generated-code*//*@lineinfo:91^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  meb.merchant_number     merchant_number,
//                  meb.email_address       email_address,
//                  me.confirmation_code    confirmation_code
//          from    merchant_enrollment_bbt meb,
//                  merchant_enrollment     me
//          where   meb.process_sequence = :procSequence and
//                  meb.merchant_number = me.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  meb.merchant_number     merchant_number,\n                meb.email_address       email_address,\n                me.confirmation_code    confirmation_code\n        from    merchant_enrollment_bbt meb,\n                merchant_enrollment     me\n        where   meb.process_sequence =  :1  and\n                meb.merchant_number = me.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.BBTEmailer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.BBTEmailer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:100^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        // send email to each merchant in the list
        MailMessage msg = new MailMessage();
        body.setLength(0);
        body.append("Welcome and thank you for choosing BB&T Merchant Connection.  ");
        body.append("To complete registration, simply go online to http://www.bbandt.com/merchantservices/merchantconnection.html.\n\n");
        body.append("You will need your confirmation code, which is: ");
        body.append(rs.getString("confirmation_code"));
        body.append(", your 15-digit merchant ID number which can be found in your \"Welcome Package\", and a valid email address.\n\n");
        body.append("Once you have selected your password, please record in a secure location.");
        
        msg.setAddresses(MesEmails.MSG_ADDRS_BBT_PRE_ENROLLMENT);
        msg.addTo(rs.getString("email_address"));
        msg.setSubject("Welcome to BB&T Merchant Connection");
        msg.setText(body.toString());
        msg.send();
      }
      
      rs.close();
      it.close();
      
      // update database to show email sent
      /*@lineinfo:generated-code*//*@lineinfo:127^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_enrollment_bbt
//          set     date_email_sent = sysdate
//          where   process_sequence = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_enrollment_bbt\n        set     date_email_sent = sysdate\n        where   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.BBTEmailer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^7*/
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
}/*@lineinfo:generated-code*/