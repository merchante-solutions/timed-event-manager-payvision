/*@lineinfo:filename=BankTransitRoutingProcessor*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;


import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;


public class BankTransitRoutingProcessor extends EventBase {

  static Logger log = Logger.getLogger(BankTransitRoutingProcessor.class);

  public boolean execute() {

    boolean result = false;


    try {
      
      connect();
      
      String host = MesDefaults.getString(MesDefaults.DK_UATP_OUTGOING_HOST);
      String user = MesDefaults.getString(MesDefaults.DK_UATP_OUTGOING_USER);
      String pwd = MesDefaults.getString(MesDefaults.DK_UATP_OUTGOING_PASSWORD);
      String vitalUploadPath = MesDefaults.getString(MesDefaults.DK_VITAL_UPLOAD_PATH);
      String fedDirPath = MesDefaults.getString(MesDefaults.DK_FED_URL);
      
      
      // build file name of the type aba3941_mmddyy_xxx.dat
      String arcFileName = buildFilename("aba3941");
      log.debug("build file name : "  + arcFileName);
      
      // save to File System
      downloadFile(fedDirPath, arcFileName);
      log.debug("file downloaded");
      
      // FTP to Vital directory along with flag file
      if (sendDataFile(arcFileName, host, user, pwd, vitalUploadPath, false, true)) {
        log.debug("file transfered : " + arcFileName);
        result = true;
        sendEmail(result, arcFileName);
        return result;
      }else{        
        result = false;
        sendEmail(result, arcFileName);
      }
    } catch (Exception e) {
      logEntry("execute() : ", e.toString());
    }finally{
      //release connection object
      cleanUp();
    }
    return false;
  }

 


  private void sendEmail(boolean result, String filename) {   
    StringBuffer msgBody = new StringBuffer();
    StringBuffer  subject         = new StringBuffer("");
    try {
      MailMessage msg = new MailMessage();
    
    
    if(result){
      //success email
      subject.append("Bank transit routing number outgoing file Success -" + filename);
      
      msgBody.append("[");
      msgBody.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
      msgBody.append("] Successfully processed bank transit routing number outgoing file: ");
      msgBody.append(filename);
      msgBody.append("\n\n");
      
      msg.setAddresses(MesEmails.MSG_ADDRS_FILEPROC_SUCCESS);
      
    }else{
      //failure email
      subject.append("Bank transit routing number outgoing file failed -" + filename);
      msg.setAddresses(MesEmails.MSG_ADDRS_FILEPROC_ERROR);
      
      msgBody.append("[");
      msgBody.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
      msgBody.append("] Successfully downloaded bank transit routing number but failed to transfer outgoing file: ");
      msgBody.append(filename);
      msgBody.append("\n\n");
    }
    msg.setSubject(subject.toString());
    msg.setText(msgBody.toString());
    msg.send();
    log.debug("email notification sent");
    
    } catch (Exception e) {    
      e.printStackTrace();
    logEntry("sendEmail() : ", e.getMessage());
    }
    
  }




  public static void main(String args[]) {
        try {
            if (args.length > 0 && args[0].equals("testproperties")) {
                EventBase.printKeyListStatus(new String[]{
                        MesDefaults.DK_UATP_OUTGOING_HOST,
                        MesDefaults.DK_UATP_OUTGOING_USER,
                        MesDefaults.DK_UATP_OUTGOING_PASSWORD,
                        MesDefaults.DK_VITAL_UPLOAD_PATH,
                        MesDefaults.DK_FED_URL
                });
            }
        } catch (Exception e) {
            System.err.println("main(): " + e.toString());
            log.error(e.getMessage());
        }


    BankTransitRoutingProcessor proc = new BankTransitRoutingProcessor();
    proc.initStandalone("DEBUG");
    if(proc.execute()){
      System.out.println("Main() - Success");
    }else{
      System.out.println("main() - Failure");
    }
  }

}/*@lineinfo:generated-code*/