/*@lineinfo:filename=CieloFileLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/startup/CieloFileLoader.sqlj $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2013-06-26 18:55:51 -0700 (Wed, 26 Jun 2013) $
  Version            : $Revision: 21248 $

  Change History:
     See SVN database

  Copyright (C) 2000-2012,2013 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.constants.MesFlatFiles;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.flatfile.FlatFileRecordField;
import com.mes.settlement.McIccTLV;
import com.mes.settlement.SettlementTools;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import com.mes.support.StringUtilities;
import com.mes.support.ThreadPool;
import com.mes.support.TridentTools;
import com.mes.tools.AsciiToEbcdicConverter;
import masthead.util.ByteUtil;
import sqlj.runtime.ResultSetIterator;

public class CieloFileLoader extends EventBase
{
  public static final int     PT_LOAD_BASE_I        = 0;


  private static final HashMap  BooleanFields =
    new HashMap()
    {
      {
        put("emv_indicator"           ,"emv_indicator"            );
        put("pin_usage_indicator"     ,"pin_usage_indicator"      );
        put("mobile_indicator"        ,"mobile_indicator"         );
        put("term_solution_indicator" ,"term_solution_indicator"  );
        put("term_campaign_indicator" ,"term_campaign_indicator"  );
      }
    };

  private static final HashMap  ExcludedFieldNames = 
    new HashMap()
    {
      {
        put("filler"                  ,"filler"                 );
        put("reserved"                ,"reserved"               );
        put("transaction_key"         ,"transaction_key"        );
      }
    };

  private static final String[][] MifFields = 
  {
    // mif                            mif_cielo
    { "merchant_number"               ,"merchant_number"                },
    { "*needed?*"                     ,"retail_name"                    },
    { "dba_name"                      ,"dba_name"                       },
    { "fdr_corp_name"                 ,"legal_name"                     },
    { "*ignore*"                      ,"routine_type"                   },
    { "*ignore*"                      ,"movto_date"                     },
    { "*ignore*"                      ,"version_number"                 },
    { "*ignore*"                      ,"activity_type"                  },
    { "*ignore*"                      ,"principal"                      },
    { "*ignore*"                      ,"chain"                          },
    { "*ignore*"                      ,"associate"                      },
    { "sic_code"                      ,"*substr(mfc.sic_code,2)"        },
    { "*ignore*"                      ,"invoice_category"               },
    { "dmacctst"                      ,"merchant_status"                },
    { "date_opened"                   ,"*to_char(date_opened,'mmddyy')" },
    { "date_stat_chgd_to_dcb"         ,"date_closed"                    },
    { "*add*"                         ,"closure_reason"                 },
    { "*add*"                         ,"activity_status"                },
    { "*add*"                         ,"affiliation_code"               },
    { "*app submitted date - ola*"    ,"affiliation_date"               },
    { "date_last_activity"            ,"date_last_activity"             },
    { "*needed?*"                     ,"date_visit"                     },
    { "*add*"                         ,"marketing_rep_code"             },
    { "*needed?*"                     ,"date_marketing_visit"           },
    { "user_flag_1"                   ,"user_code_2"                    },
    { "*add*"                         ,"billing_cycle_code"             },
    { "dmaddr"                        ,"address_1"                      },
    { "*needed?*"                     ,"address_2"                      },
    { "*add or alter*"                ,"phone_ddd"                      },
    { "*add or alter*"                ,"phone_number"                   },
    { "*add or alter*"                ,"fax_ddd"                        },
    { "*add or alter*"                ,"fax_number"                     },
    { "*add*"                         ,"secured_indicator"              },
    { "*add*"                         ,"ro_indicator"                   },
    { "*add*"                         ,"debit_balance_indicator"        },
    { "*ignore*"                      ,"force_monitor_flag"             },
    { "ecommerce_flag"                ,"ecommerce_indicator"            },
    { "*add*"                         ,"moto_indicator"                 },
    { "*add*"                         ,"suspend_supplies_indicator"     },
    { "*add*"                         ,"cashback_indicator"             },
    { "user_flag_2"                   ,"user_code_3"                    },
    { "*add*"                         ,"vol_indicator"                  },
    { "transit_routng_num"            ,"transit_number"                 },
    { "dda_num"                       ,"dda_number"                     },
    { "dmcity"                        ,"city"                           },
    { "dmstate"                       ,"state"                          },
    { "dmzip"                         ,"zip_code"                       },
    { "*add or app*"                  ,"business_type"                  },
    { "*add*"                         ,"state_reg_number"               },
    { "*add*"                         ,"cpf_number"                     },
    { "*add*"                         ,"service_rep_code"               },
    { "*add or app*"                  ,"owner_1_name"                   },
    { "*add or app*"                  ,"owner_1_cpf"                    },
    { "*add or app*"                  ,"owner_1_dob"                    },
    { "*add or app*"                  ,"owner_2_name"                   },
    { "*add or app*"                  ,"owner_2_cpf"                    },
    { "*add or app*"                  ,"owner_2_dob"                    },
    { "*add or app*"                  ,"owner_3_name"                   },
    { "*add or app*"                  ,"owner_3_cpf"                    },
    { "*add or app*"                  ,"owner_3_dob"                    },
    { "*add or app*"                  ,"billing_attention"              },
    { "*add*"                         ,"auto_payment_indicator"         },
    { "*add*"                         ,"auto_payment_frequency"         },
    { "*add*"                         ,"auto_anticipate_1"              },
    { "*add*"                         ,"auto_anticipate_2"              },
    { "*add*"                         ,"auto_anticipate_3"              },
    { "*add*"                         ,"activation_status"              },
    { "*add*"                         ,"activation_status_date"         },
    { "*add*"                         ,"date_first_sale_after_f0"       },
    { "*add*"                         ,"date_stat_chgd_to_f0"           },
    { "*add*"                         ,"active_indicator"               },
    { "*needed?*"                     ,"action_flag_1"                  },
    { "*needed?*"                     ,"action_flag_2"                  },
    { "*needed?*"                     ,"action_flag_3"                  },
    { "*needed?*"                     ,"action_flag_4"                  },
    { "*needed?*"                     ,"action_flag_5"                  },
    { "*needed?*"                     ,"action_flag_6"                  },
    { "*needed?*"                     ,"action_flag_7"                  },
    { "*needed?*"                     ,"action_flag_8"                  },
    { "*needed?*"                     ,"action_flag_9"                  },
    { "date_of_1st_deposit"           ,"date_first_sale"                },
    { "*add*"                         ,"date_status_changed"            },
    { "*add*"                         ,"date_purge_letter"              },
    { "addr1_line_1"                  ,"billing_address_1"              },
    { "addr1_line_2"                  ,"billing_address_2"              },
    { "zip1_line_4"                   ,"billing_zip"                    },
    { "city1_line_4"                  ,"billing_city"                   },
    { "state1_line_4"                 ,"billing_state"                  },
    { "*add*"                         ,"account_mgmt_indicator"         },
    { "*ignore*"                      ,"auto_capture_indicator"         },
    { "*add*"                         ,"account_payment_indicator"      },
    { "*ignore*"                      ,"collection_reason_code"         },
    { "*add*"                         ,"visa_vale_legal_action_ind"     },
    { "*add*"                         ,"visa_vale_special_ind"          },
    { "*add*"                         ,"date_visa_vale_activited"       },
    { "*add*"                         ,"visa_vale_meal_indicator"       },
    { "*add*"                         ,"visa_vale_food_indicator"       },
    { "*add*"                         ,"visa_vale_auto_indicator"       },
    { "*add*"                         ,"visa_vale_auto_period"          },
    { "*add*"                         ,"visa_vale_auto_day_1"           },
    { "*add*"                         ,"visa_vale_auto_day_2"           },
    { "*add*"                         ,"visa_vale_auto_day_3"           },
    { "*add*"                         ,"visa_vale_bank_id"              },
    { "*add*"                         ,"visa_vale_agency_id"            },
    { "*add*"                         ,"visa_vale_account_number"       },
    { "*add*"                         ,"visa_vale_change_date"          },
    { "*ignore*"                      ,"visa_fleet_indicator"           },
    { "*add*"                         ,"recurring_indicator"            },
    { "*add*"                         ,"bank_payment_indicator"         },
    { "*add*"                         ,"mobile_payment_indicator"       },
    { "*add*"                         ,"v_mania_indicator"              },
    { "*add*"                         ,"v_mania_campaign_indicator"     },
    { "*add*"                         ,"capture_via_auth_indicator"     },
    { "*add*"                         ,"capture_via_auth_date"          },
    { "*add*"                         ,"visa_dist_indicator"            },
    { "*add*"                         ,"payment_lock_indicator"         },
    { "*add*"                         ,"auto_anticipate"                },
    { "*add*"                         ,"anticipation_frequency"         },
    { "*add*"                         ,"anticipation_type"              },
    { "*add*"                         ,"market_segment"                 },
    { "*add*"                         ,"service_package"                },
    { "*add*"                         ,"signature_file_indicator"       },
    { "*add*"                         ,"extr_electronic_indicator"      },
    { "*add*"                         ,"exch_rate_diff_indicator"       },
    { "addr2_line_1"                  ,"supply_address_1"               },
    { "addr2_line_2"                  ,"supply_address_2"               },
    { "*add*"                         ,"supply_phone_ddd"               },
    { "city2_line_4"                  ,"supply_city"                    },
    { "zip2_line_4"                   ,"supply_cep_number"              },
    { "state2_line_4"                 ,"supply_uf_code"                 },
    { "*add*"                         ,"supply_phone"                   },
    { "*add*"                         ,"supply_contact"                 },
    { "*add*"                         ,"mark_code"                      },
  };
  
  public class BinaryLineReader extends BufferedInputStream
  {
    public BinaryLineReader(InputStream in) 
    {
      super(in);
    }

    public BinaryLineReader(InputStream in, int size)
    {
      super(in,size);
    }
    
    public byte[] readLine()
      throws java.io.IOException
    {
      Vector  buffer        = new Vector();
      byte[]  bytes         = new byte[1];
      int     bytesRead     = 0;
      byte[]  retVal        = null;
      
      while( (bytesRead = in.read(bytes,0,1)) > 0 )
      {
        if ( (char)bytes[0] == '\n' ) { break; }
        buffer.addElement(bytes[0]);
      }
      
      if ( buffer.size() > 0 )
      {
        retVal = new byte[buffer.size()];
        
        for( int i = 0; i < buffer.size(); ++i )
        {
          Byte theByte = (Byte)buffer.elementAt(i);
          retVal[i] = theByte.byteValue(); 
        }
      }
      return( retVal );
    }
  }

  public class MifCieloProductsWorker
    extends SQLJConnectionBase
    implements Runnable
  {
    protected   String    LoadFilename  = null;
    protected   String    RawData       = null;
    
    public MifCieloProductsWorker( String rawData, String loadFilename  )
      throws Exception
    {
      RawData       = rawData;
      LoadFilename  = loadFilename;
    }
    
    public void run()
    {
      FlatFileRecord    ffd         = null;
      int               recCount    = 0;
      
      try
      {
        connect(true);
        
        ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_MIF_CIELO_PRODUCTS);
        ffd.suck(RawData);
        
        /*@lineinfo:generated-code*//*@lineinfo:295^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    mif_cielo_products
//            where   load_file_id        = load_filename_to_load_file_id(:LoadFilename)
//                    and merchant_number = :ffd.getFieldData("merchant_number").trim()
//                    and seq_num         = :ffd.getFieldData("seq_num").trim()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3836 = ffd.getFieldData("merchant_number").trim();
 String __sJT_3837 = ffd.getFieldData("seq_num").trim();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    mif_cielo_products\n          where   load_file_id        = load_filename_to_load_file_id( :1 )\n                  and merchant_number =  :2 \n                  and seq_num         =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,LoadFilename);
   __sJT_st.setString(2,__sJT_3836);
   __sJT_st.setString(3,__sJT_3837);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:303^9*/
        
        if ( recCount == 0 )
        {
          String dateTimeString  = ffd.getFieldData("file_date") + ffd.getFieldData("file_time");
          
          /*@lineinfo:generated-code*//*@lineinfo:309^11*/

//  ************************************************************
//  #sql [Ctx] { insert into mif_cielo_products
//              (
//                rec_type,
//                seq_num,
//                archive_type,
//                file_ts,
//                version,
//                movement_type,
//                product,
//                merchant_number,
//                max_installments,
//                days_pre_dated,
//                tip_op_pre_dated,
//                suspended_days,  
//                card_present_indicator,
//                moto_indicator,
//                ecommerce_indicator,
//                visa_vale_qual_date,
//                commission_type,
//                discount_rate,
//                item_discount_value,
//                lease_tariff,
//                guaranteed_tax_percent,
//                installment_count_min,
//                installment_count_max,
//                valid_start_date,
//                sale_type,
//                installment_indicator,
//                reg_total,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :ffd.getFieldData("rec_type").trim(),
//                :ffd.getFieldData("seq_num").trim(),
//                :ffd.getFieldData("archive_type").trim(),
//                to_date(:dateTimeString,'yyyymmddhh24miss'),
//                :ffd.getFieldData("version").trim(),
//                :ffd.getFieldData("movement_type").trim(),
//                :ffd.getFieldData("product").trim(),
//                :ffd.getFieldData("merchant_number").trim(),
//                :ffd.getFieldData("max_installments").trim(),
//                :ffd.getFieldData("days_pre_dated").trim(),
//                :ffd.getFieldData("tip_op_pre_dated").trim(),
//                :ffd.getFieldData("suspended_days").trim(),
//                :ffd.getFieldData("card_present_indicator").trim(),
//                :ffd.getFieldData("moto_indicator").trim(),
//                :ffd.getFieldData("ecommerce_indicator").trim(),
//                :ffd.getFieldAsSqlDate("visa_vale_qual_date"),
//                :ffd.getFieldData("commission_type").trim(),
//                replace(:ffd.getFieldData("discount_rate").trim(),',','.'),
//                replace(:ffd.getFieldData("item_discount_value").trim(),',','.'),
//                replace(:ffd.getFieldData("lease_tariff").trim(),',','.'),
//                replace(:ffd.getFieldData("guaranteed_tax_percent").trim(),',','.'),
//                :ffd.getFieldData("installment_count_min").trim(),
//                :ffd.getFieldData("installment_count_max").trim(),
//                :ffd.getFieldAsSqlDate("valid_start_date"),
//                :ffd.getFieldData("sale_type").trim(),
//                :ffd.getFieldData("installment_indicator").trim(),
//                :ffd.getFieldData("reg_total").trim(),
//                :LoadFilename,
//                load_filename_to_load_file_id(:LoadFilename)
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3838 = ffd.getFieldData("rec_type").trim();
 String __sJT_3839 = ffd.getFieldData("seq_num").trim();
 String __sJT_3840 = ffd.getFieldData("archive_type").trim();
 String __sJT_3841 = ffd.getFieldData("version").trim();
 String __sJT_3842 = ffd.getFieldData("movement_type").trim();
 String __sJT_3843 = ffd.getFieldData("product").trim();
 String __sJT_3844 = ffd.getFieldData("merchant_number").trim();
 String __sJT_3845 = ffd.getFieldData("max_installments").trim();
 String __sJT_3846 = ffd.getFieldData("days_pre_dated").trim();
 String __sJT_3847 = ffd.getFieldData("tip_op_pre_dated").trim();
 String __sJT_3848 = ffd.getFieldData("suspended_days").trim();
 String __sJT_3849 = ffd.getFieldData("card_present_indicator").trim();
 String __sJT_3850 = ffd.getFieldData("moto_indicator").trim();
 String __sJT_3851 = ffd.getFieldData("ecommerce_indicator").trim();
 java.sql.Date __sJT_3852 = ffd.getFieldAsSqlDate("visa_vale_qual_date");
 String __sJT_3853 = ffd.getFieldData("commission_type").trim();
 String __sJT_3854 = ffd.getFieldData("discount_rate").trim();
 String __sJT_3855 = ffd.getFieldData("item_discount_value").trim();
 String __sJT_3856 = ffd.getFieldData("lease_tariff").trim();
 String __sJT_3857 = ffd.getFieldData("guaranteed_tax_percent").trim();
 String __sJT_3858 = ffd.getFieldData("installment_count_min").trim();
 String __sJT_3859 = ffd.getFieldData("installment_count_max").trim();
 java.sql.Date __sJT_3860 = ffd.getFieldAsSqlDate("valid_start_date");
 String __sJT_3861 = ffd.getFieldData("sale_type").trim();
 String __sJT_3862 = ffd.getFieldData("installment_indicator").trim();
 String __sJT_3863 = ffd.getFieldData("reg_total").trim();
   String theSqlTS = "insert into mif_cielo_products\n            (\n              rec_type,\n              seq_num,\n              archive_type,\n              file_ts,\n              version,\n              movement_type,\n              product,\n              merchant_number,\n              max_installments,\n              days_pre_dated,\n              tip_op_pre_dated,\n              suspended_days,  \n              card_present_indicator,\n              moto_indicator,\n              ecommerce_indicator,\n              visa_vale_qual_date,\n              commission_type,\n              discount_rate,\n              item_discount_value,\n              lease_tariff,\n              guaranteed_tax_percent,\n              installment_count_min,\n              installment_count_max,\n              valid_start_date,\n              sale_type,\n              installment_indicator,\n              reg_total,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n              to_date( :4 ,'yyyymmddhh24miss'),\n               :5 ,\n               :6 ,\n               :7 ,\n               :8 ,\n               :9 ,\n               :10 ,\n               :11 ,\n               :12 ,\n               :13 ,\n               :14 ,\n               :15 ,\n               :16 ,\n               :17 ,\n              replace( :18 ,',','.'),\n              replace( :19 ,',','.'),\n              replace( :20 ,',','.'),\n              replace( :21 ,',','.'),\n               :22 ,\n               :23 ,\n               :24 ,\n               :25 ,\n               :26 ,\n               :27 ,\n               :28 ,\n              load_filename_to_load_file_id( :29 )\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3838);
   __sJT_st.setString(2,__sJT_3839);
   __sJT_st.setString(3,__sJT_3840);
   __sJT_st.setString(4,dateTimeString);
   __sJT_st.setString(5,__sJT_3841);
   __sJT_st.setString(6,__sJT_3842);
   __sJT_st.setString(7,__sJT_3843);
   __sJT_st.setString(8,__sJT_3844);
   __sJT_st.setString(9,__sJT_3845);
   __sJT_st.setString(10,__sJT_3846);
   __sJT_st.setString(11,__sJT_3847);
   __sJT_st.setString(12,__sJT_3848);
   __sJT_st.setString(13,__sJT_3849);
   __sJT_st.setString(14,__sJT_3850);
   __sJT_st.setString(15,__sJT_3851);
   __sJT_st.setDate(16,__sJT_3852);
   __sJT_st.setString(17,__sJT_3853);
   __sJT_st.setString(18,__sJT_3854);
   __sJT_st.setString(19,__sJT_3855);
   __sJT_st.setString(20,__sJT_3856);
   __sJT_st.setString(21,__sJT_3857);
   __sJT_st.setString(22,__sJT_3858);
   __sJT_st.setString(23,__sJT_3859);
   __sJT_st.setDate(24,__sJT_3860);
   __sJT_st.setString(25,__sJT_3861);
   __sJT_st.setString(26,__sJT_3862);
   __sJT_st.setString(27,__sJT_3863);
   __sJT_st.setString(28,LoadFilename);
   __sJT_st.setString(29,LoadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:375^11*/
        }
      }
      catch( Exception e )
      {
        logEntry("run()",e.toString());
        ffd.showData();
      }
      finally
      {
        cleanUp();
      }
    }
  }
  
  public class DtRecordBuilder
    extends SQLJConnectionBase
    implements Runnable
  {
    private   String        LoadFilename          = null;
    private   String        TransactionKey        = null;
    
    public DtRecordBuilder( String transactionKey, String loadFilename )
    {
      LoadFilename      = loadFilename;
      TransactionKey    = transactionKey;
    }
    
    private String encodeFieldValue( FlatFileRecordField fld, FlatFileRecord ffd )
    {
      String    name      = fld.getName();
      String    retVal    = null;
      
      switch( fld.getType() )
      {
        case 1:   // number
          if ( fld.getDecimalPlaces() == 0 )
          {
            retVal = String.valueOf( ffd.getFieldAsLong(name) );
          }
          else
          {
            retVal = String.valueOf( ffd.getFieldAsDouble(name) );
          }
          break;
      
        case 2:   // date
          retVal = "'" + DateTimeFormatter.getFormattedDate(ffd.getFieldAsSqlDate(name),"dd-MMM-yyyy") + "'";
          break;
      
        default:
          retVal = ffd.getFieldData(name).trim().replaceAll("'","''");
          if ( BooleanFields.get(name) != null )
          {
            retVal = ("S".equals(retVal) ? "Y" : retVal); 
          }
          retVal = "'" + retVal + "'";
          break;
      }
      return( retVal );
    }
    
    public void run()
    {
      String              authCurrencyCode  = null;
      Date                batchDate         = null;
      String              batchRecId        = null;
      int                 blockType         = -1;
      String              cardNumberEnc     = null;
      String              dateTimeString    = null;
      String              extData           = null;
      String              extraCols         = "";
      String              extraVals         = "";
      FlatFileRecord      ffdBase           = null;
      int                 ffdComplBase      = 919;
      FlatFileRecord      ffd               = null;
      int                 ffdId             = 0;
      Vector              ffds              = new Vector();
      String              iccData           = "";
      ResultSetIterator   it                = null;
      long                loadFileId        = 0L;
      String              rawData           = null;
      ResultSet           resultSet         = null;
    
      try
      {
        connect(true);
        
        /*@lineinfo:generated-code*//*@lineinfo:463^9*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:LoadFilename),
//                    get_file_date(:LoadFilename)
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 ),\n                  get_file_date( :2 )\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,LoadFilename);
   __sJT_st.setString(2,LoadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   batchDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:469^9*/
        
        char      ct          = LoadFilename.charAt(5);
        int       defType     = -1;
        
        /*@lineinfo:generated-code*//*@lineinfo:474^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rw.block_type,rw.raw_data,rw.card_number_enc
//            from    daily_detail_file_cielo_raw   rw
//            where   rw.transaction_key = :TransactionKey
//            order by rw.block_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rw.block_type,rw.raw_data,rw.card_number_enc\n          from    daily_detail_file_cielo_raw   rw\n          where   rw.transaction_key =  :1 \n          order by rw.block_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,TransactionKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.CieloFileLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:480^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          rawData   = resultSet.getString("raw_data");
          blockType = resultSet.getInt("block_type");
          
          // the first row needs to be block type 0 (base transaction)
          if ( ffdBase == null )
          {
            if ( blockType == 0 )
            {
                   if ( ct == 'm' ) { defType = MesFlatFiles.DEF_TYPE_DDF_CIELO_MC; }
              else if ( ct == 'v' ) { defType = MesFlatFiles.DEF_TYPE_DDF_CIELO_VS; }
              else                  { defType = -1;                                 }
              
              ffdBase = new FlatFileRecord(Ctx,defType);
              ffdBase.suck(rawData);
              
              dateTimeString  = ffdBase.getFieldData("auth_date") + ffdBase.getFieldData("auth_time");
              batchRecId      = TransactionKey.substring(TransactionKey.length()-9);
              cardNumberEnc   = resultSet.getString("card_number_enc");
              
              FlatFileRecordField fld = ffdBase.getFirstField();
              do
              {
                String name   = fld.getName();
          
                if ( ExcludedFieldNames.get(name) != null ) { continue; } // skip 
                if ( name.startsWith("x_") )                { continue; } // skip
          
                extraCols += ("," + name);
                extraVals += ("," + encodeFieldValue(fld,ffdBase));
              } while( (fld = ffdBase.getNextField()) != null );
            }
            else    // block 0 required here
            {
              throw new Exception("Invalid transaction - " + TransactionKey);
            }
          }
          else    // compl blocks
          {
            switch( ct )
            {
              case 'v':
                defType = MesFlatFiles.DEF_TYPE_DDF_CIELO_VS_COMPL_BASE;
                break;
                
              default:    // multi-bank
                blockType = Integer.parseInt(rawData.substring(75,78));
                if ( blockType == 13 || blockType == 14 )
                {
                  // MasterCard binary blocks are converted to ascii hex
                  // equivalent which makes the ext_data block larger
                  defType = MesFlatFiles.DEF_TYPE_DDF_CIELO_MC_COMPL_BASE_BIN;
                }
                else  // use standard multi-bank complementary base record
                {
                  defType = MesFlatFiles.DEF_TYPE_DDF_CIELO_MC_COMPL_BASE; 
                }
                break;
            }
            ffd = new FlatFileRecord(Ctx,defType);
            ffd.suck(rawData);
            ffds.addElement(ffd);
          }
        }
        resultSet.close();
        it.close();
        
        // load the extra data from the base transaction record
        int  extraDefType   = 0;
        if ( ffdBase.getFieldData("tran_type").charAt(0) == 'A' )   // refund
        {
               if ( ct == 'm' ) { extraDefType = MesFlatFiles.DEF_TYPE_DDF_CIELO_MC_REFUND; }
          else if ( ct == 'v' ) { extraDefType = MesFlatFiles.DEF_TYPE_DDF_CIELO_VS_REFUND; }
        }
        else if ( "C03".equals(ffdBase.getFieldData("tran_type")) ) // cashback
        {
          extraDefType = MesFlatFiles.DEF_TYPE_DDF_CIELO_VS_CASHBACK;
        }
        else if ( false ) // electron agro??
        {
        }
        else
        {
          int  clearingMethod = 0;
          try{ clearingMethod = ffdBase.getFieldAsInt("clearing_method"); } catch( Exception ee ){}
        
          switch( clearingMethod )
          {
            //case 0:   // credit/debit, no installments
            //  break;
          
            case 1:   // installment (issuer/emissor)
            case 2:   // installment (acquirer/loja)
                   if ( ct == 'm' ) { extraDefType = MesFlatFiles.DEF_TYPE_DDF_CIELO_MC_INSTALLMENT; }
              else if ( ct == 'v' ) { extraDefType = MesFlatFiles.DEF_TYPE_DDF_CIELO_VS_INSTALLMENT; }
              break; 
            
            case 3:   // pre-dated electron
              if ( "41".equals(ffdBase.getFieldData("cielo_product")) )
              {
                extraDefType = MesFlatFiles.DEF_TYPE_DDF_CIELO_VS_ELECTRON_PD;  
              }
              break;
          }
          
        }
        
        if ( extraDefType > 0 ) 
        {
          ffd = new FlatFileRecord(Ctx,extraDefType);
          ffd.suck(ffdBase.getFieldData("x_extra_data"));
        
          FlatFileRecordField fld = ffd.getFirstField();
          do
          {
            String name   = fld.getName();
          
            if ( "filler".equals(name) ) { continue; }    // skip filler
            
            extraCols += ("," + name);
            extraVals += ("," + encodeFieldValue(fld,ffd));
          } while( (fld = ffd.getNextField()) != null );
        }
        
        // scan through the compl ffds and build the SQL statement
        for( int i = 0; i < ffds.size(); ++i )
        {
          ffd       = (FlatFileRecord)ffds.elementAt(i);
          blockType = ffd.getFieldAsInt("block_type");
          extData   = ffd.getFieldData("ext_data");
          
          if ( blockType == 8 )   // block type 8 has two variations
          {
            if ( "001".equals(extData.substring(0,3)) ) { ffdId = MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_008A; }
            else                                        { ffdId = MesFlatFiles.DEF_TYPE_DDF_CIELO_COMPL_BLOCK_008B; }
          }
          else
          {
            ffdId = (ffdComplBase + blockType);   // flat file defs 920-935
          }
          ffd = new FlatFileRecord(Ctx,ffdId);
          ffd.suck(extData);
          
          FlatFileRecordField fld = ffd.getFirstField();
          do
          {
            String name   = fld.getName();
        
            if ( ExcludedFieldNames.get(name) != null ) { continue; } // skip 
            if ( name.startsWith("x_") )                { continue; } // skip
            if ( name.startsWith("mc_icc_segment") )
            {
              iccData += ffd.getFieldData(name).trim();
              continue;
            }
            else if ( name.equals("ica_number") )    // DE94 - only use last 6
            {
              String icaNumber = ffd.getFieldData(name).trim();
              ffd.setFieldData(name,icaNumber.substring((icaNumber.length() < 6 ? 0 : icaNumber.length()-6)));
            }
            else if ( name.equals("auth_currency_code") )
            {
              authCurrencyCode = ffd.getFieldData("auth_currency_code");
            }
            extraCols += ("," + name);
            extraVals += ("," + encodeFieldValue(fld,ffd));
          } while( (fld = ffd.getNextField()) != null );
        }
        if ( !"".equals(iccData) && !StringUtilities.isAllZeroes(iccData) )
        {
          extraCols +=  ",mc_icc_data";
          extraVals += (",'" + scrubMcIccData(iccData) + "'");
        }
        //@System.out.println("========================"); //@
        //@System.out.println(extraCols);                  //@
        //@System.out.println("========================"); //@
        //@System.out.println(extraVals);                  //@
        //@System.out.println("========================"); //@
        //@System.out.println(RawData);                    //@
        //@System.out.println("========================"); //@
        
        long batchId = getBatchId(ffdBase.getFieldData("catid"),batchDate,ffdBase.getFieldData("currency_code"),authCurrencyCode);
        /*@lineinfo:generated-code*//*@lineinfo:666^9*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_cielo_dt
//            (
//              transaction_key,
//              batch_id,
//              batch_record_id,
//              batch_date,
//              debit_credit_indicator,
//              auth_time,
//              card_number_enc,
//              load_filename,
//              load_file_id
//              :extraCols
//            )
//            values
//            (
//              :TransactionKey,
//              :batchId,
//              :batchRecId,
//              :batchDate,
//              decode(substr(:ffdBase.getFieldData("tran_type").trim(),1,1),'A','C','D'),
//              to_date(:dateTimeString,'yymmddhh24miss'),
//              :cardNumberEnc,
//              :LoadFilename,
//              :loadFileId
//              :extraVals
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3864 = ffdBase.getFieldData("tran_type").trim();
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("insert into daily_detail_file_cielo_dt\n          (\n            transaction_key,\n            batch_id,\n            batch_record_id,\n            batch_date,\n            debit_credit_indicator,\n            auth_time,\n            card_number_enc,\n            load_filename,\n            load_file_id\n             ");
   __sjT_sb.append(extraCols);
   __sjT_sb.append(" \n          )\n          values\n          (\n             ? ,\n             ? ,\n             ? ,\n             ? ,\n            decode(substr( ? ,1,1),'A','C','D'),\n            to_date( ? ,'yymmddhh24miss'),\n             ? ,\n             ? ,\n             ? \n             ");
   __sjT_sb.append(extraVals);
   __sjT_sb.append(" \n          )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "4com.mes.startup.CieloFileLoader:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,TransactionKey);
   __sJT_st.setLong(2,batchId);
   __sJT_st.setString(3,batchRecId);
   __sJT_st.setDate(4,batchDate);
   __sJT_st.setString(5,__sJT_3864);
   __sJT_st.setString(6,dateTimeString);
   __sJT_st.setString(7,cardNumberEnc);
   __sJT_st.setString(8,LoadFilename);
   __sJT_st.setLong(9,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:694^9*/
      }
      catch( Exception e )
      {
        logEntry("run()",e.toString());
        ffdBase.showData();
        System.out.println(TransactionKey);
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ) {}
        cleanUp();
      }
    }
    
    protected String scrubMcIccData( String iccDataRaw )
      throws Exception
    {
      List          tlvs    = SettlementTools.parseMcIccTags(iccDataRaw,true);
      Iterator      it      = tlvs.iterator();
      StringBuffer  retVal  = new StringBuffer();
      
      while( it.hasNext() )
      {
        McIccTLV  tlv   = (McIccTLV)it.next();
        retVal.append(tlv.getTag() + tlv.getLen() + tlv.getVal());
      }
      return( AsciiToEbcdicConverter.ebcdicToAscii(retVal.toString()) );
    }
  }
  
  public class DdfWorker
    extends SQLJConnectionBase
    implements Runnable
  {
    protected   String    LoadFilename          = null;
    protected   String    RawData               = null;
    protected   long      SeqNum                = 0L;
    
    public DdfWorker( long seqNum, String rawData, String loadFilename )
      throws Exception
    {
      LoadFilename        = loadFilename;
      SeqNum              = seqNum;
      
      StringBuffer buffer = new StringBuffer(rawData);
      for( int i = 0; i < (LoadFilename.charAt(5) == 'v' ? 350 : 444); ++i )
      {
        if ( buffer.charAt(i) == '\0' )    // replace null
        {
          buffer.setCharAt(i,' ');         // with space
        }
      } 
      RawData = buffer.toString();
    }
    
    public void run()
    {
      Date              batchDate       = null;
      FlatFileRecord    ffd             = null;
      long              loadFileId      = 0L;
      
      try
      {
        connect(true);
        
        /*@lineinfo:generated-code*//*@lineinfo:760^9*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:LoadFilename),
//                    get_file_date(:LoadFilename)
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 ),\n                  get_file_date( :2 )\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,LoadFilename);
   __sJT_st.setString(2,LoadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   batchDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:766^9*/
        
        char      ct          = LoadFilename.charAt(5);
        int       defType     = -1;
        
             if ( ct == 'm' ) { defType = MesFlatFiles.DEF_TYPE_DDF_CIELO_MC; }
        else if ( ct == 'v' ) { defType = MesFlatFiles.DEF_TYPE_DDF_CIELO_VS; }
        else                  { defType = -1;                                 }
        
        ffd = new FlatFileRecord(Ctx,defType);
        ffd.suck(RawData);
        
        String transactionKey = null;
        
        if ( ct == 'v' )  // visa
        {
          transactionKey  =   String.valueOf(loadFileId)
                            + StringUtilities.rightJustify(String.valueOf(SeqNum),9,'0');
        }
        else    // mc
        {
          transactionKey  =   String.valueOf(loadFileId)
                            + ffd.getFieldData("transaction_key");
        }
        
        String cardNumber = TridentTools.encodeCardNumber(ffd.getFieldData("card_number").trim()) + "   ";
        // store the raw data in the raw table
        if ( ct == 'v' )
        {
          RawData = (RawData.substring(0,85) + cardNumber + RawData.substring(104));
        }
        else if ( ct == 'm' )
        {
          RawData = (RawData.substring(0,111) + cardNumber + RawData.substring(130)).replace('\0','0');
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:802^9*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_cielo_raw
//            (
//              rec_type,
//              merchant_number,
//              catid,
//              store_number,
//              batch_date,
//              transaction_key,
//              file_seq_num,
//              ro_number,
//              tran_seq_num,
//              block_type,
//              raw_data,
//              card_number_enc,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :ffd.getFieldData("rec_type").trim(),
//              :ffd.getFieldData("merchant_number").trim(),
//              :ffd.getFieldData("catid").trim(),
//              :ffd.getFieldData("store_number").trim(),
//              :batchDate,
//              :transactionKey,
//              :ffd.getFieldData("file_seq_num").trim(),
//              :ffd.getFieldData("batch_number").trim(),
//              :ffd.getFieldData("tran_seq_num").trim(),
//              0,
//              :RawData,
//              dukpt_encrypt_wrapper(:ffd.getFieldData("card_number").trim()),
//              :LoadFilename,
//              :loadFileId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3865 = ffd.getFieldData("rec_type").trim();
 String __sJT_3866 = ffd.getFieldData("merchant_number").trim();
 String __sJT_3867 = ffd.getFieldData("catid").trim();
 String __sJT_3868 = ffd.getFieldData("store_number").trim();
 String __sJT_3869 = ffd.getFieldData("file_seq_num").trim();
 String __sJT_3870 = ffd.getFieldData("batch_number").trim();
 String __sJT_3871 = ffd.getFieldData("tran_seq_num").trim();
 String __sJT_3872 = ffd.getFieldData("card_number").trim();
   String theSqlTS = "insert into daily_detail_file_cielo_raw\n          (\n            rec_type,\n            merchant_number,\n            catid,\n            store_number,\n            batch_date,\n            transaction_key,\n            file_seq_num,\n            ro_number,\n            tran_seq_num,\n            block_type,\n            raw_data,\n            card_number_enc,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n            0,\n             :10 ,\n            dukpt_encrypt_wrapper( :11 ),\n             :12 ,\n             :13 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3865);
   __sJT_st.setString(2,__sJT_3866);
   __sJT_st.setString(3,__sJT_3867);
   __sJT_st.setString(4,__sJT_3868);
   __sJT_st.setDate(5,batchDate);
   __sJT_st.setString(6,transactionKey);
   __sJT_st.setString(7,__sJT_3869);
   __sJT_st.setString(8,__sJT_3870);
   __sJT_st.setString(9,__sJT_3871);
   __sJT_st.setString(10,RawData);
   __sJT_st.setString(11,__sJT_3872);
   __sJT_st.setString(12,LoadFilename);
   __sJT_st.setLong(13,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:838^9*/
      }
      catch( Exception e )
      {
        logEntry("run()",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }
  
  public class DdfExtDataWorker
    extends SQLJConnectionBase
    implements Runnable
  {
    protected   String    LoadFilename  = null;
    protected   byte[]    RawBytes      = new byte[250];
    protected   long      SeqNum        = 0L;
    
    public DdfExtDataWorker( long seqNum, byte[] rawBytes, String loadFilename )
      throws Exception
    {
      LoadFilename  = loadFilename;
      SeqNum        = seqNum;
      
      System.arraycopy(rawBytes,0,RawBytes,0,rawBytes.length);
      for( int i = rawBytes.length; i < 250; ++i )
      {
        RawBytes[i] = '\0'; // null fill
      }
    }
    
    public void run()
    {
      Date              batchDate       = null;
      FlatFileRecord    ffd             = null;
      String            fileSeqNum      = null;
      long              loadFileId      = 0L;
      String            rawData         = null;
      String            transactionKey  = null;
      
      try
      {
        connect(true);
        
        /*@lineinfo:generated-code*//*@lineinfo:885^9*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:LoadFilename),
//                    get_file_date(:LoadFilename)
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 ),\n                  get_file_date( :2 )\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,LoadFilename);
   __sJT_st.setString(2,LoadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   batchDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:891^9*/
        
        char      ct              = LoadFilename.charAt(5);
        int       defType         = -1;
        int       blockType       = -1;
        String    iccData         = null;
        int       blockTypeOffset = (ct == 'v' ? 46 : 75);
        String    blockTypeStr    = "";
        
        // build a string for the block type using the raw bytes, then convert to integer
        for ( int i = 0; i < 3; ++i ) { blockTypeStr += (char)RawBytes[blockTypeOffset+i]; }
        blockType = Integer.parseInt(blockTypeStr);
        
        // blocks 13 and 14 contain binary MasterCard ICC data
        if ( blockType == 13 || blockType == 14 )
        {
          int     iccDataLength = ((blockType == 13) ? 147 : 108);
          byte[]  iccDataBytes  = new byte[iccDataLength];
          for( int i = 0, offset = ((blockType == 13) ? 81 : 78); i < iccDataLength; ++i ) 
          {
            iccDataBytes[i]     = RawBytes[offset+i];
            RawBytes[offset+i]  = ' ';
          }
          iccData = ByteUtil.hexString(iccDataBytes).replaceAll("0x","").replaceAll(" ","");
        }
        rawData = new String(RawBytes);
        
        // replace the binary data in the string with the hex representation
        if ( blockType == 13 || blockType == 14 )
        {
          int iccDataOffset   = (blockType == 13 ?  81 :  78);
          int iccDataLength   = (blockType == 13 ? 147 : 108);
          int addlDataOffset  = (iccDataOffset+iccDataLength-1);  // zero based
          rawData = rawData.substring(0,iccDataOffset)
                    + iccData 
                    + (addlDataOffset < 227 ? rawData.substring(addlDataOffset) : "");
        }
        
             if ( ct == 'm' ) { defType = MesFlatFiles.DEF_TYPE_DDF_CIELO_MC_COMPL_BASE; }
        else if ( ct == 'v' ) { defType = MesFlatFiles.DEF_TYPE_DDF_CIELO_VS_COMPL_BASE; }
        else                  { defType = -1;                                            }
        
        ffd = new FlatFileRecord(Ctx,defType);
        ffd.suck(rawData);
        
        if ( ct == 'v' )  // visa
        {
          fileSeqNum      =   ffd.getFieldData("file_seq_num");                            
          transactionKey  =   String.valueOf(loadFileId)
                            + StringUtilities.rightJustify(String.valueOf(SeqNum),9,'0');
        }
        else    // multi-bank
        {
          fileSeqNum      =   "000";
          transactionKey  =   String.valueOf(loadFileId)
                            + ffd.getFieldData("transaction_key");
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:949^9*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_cielo_raw
//            (
//              rec_type,
//              merchant_number,
//              batch_date,
//              catid,
//              store_number,
//              transaction_key,
//              file_seq_num,
//              ro_number,
//              tran_seq_num,
//              block_type,
//              raw_data,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :ffd.getFieldData("rec_type").trim(),
//              :ffd.getFieldData("merchant_number").trim(),
//              :batchDate,
//              :ffd.getFieldData("catid").trim(),
//              :ffd.getFieldData("store_number").trim(),
//              :transactionKey,
//              :fileSeqNum,
//              :ffd.getFieldData("batch_number").trim(),
//              :ffd.getFieldData("tran_seq_num").trim(),
//              decode(:blockType,
//                      8,to_number(substr(:rawData,decode(:String.valueOf(ct),'v',50,79),3) || lpad(to_char(:blockType),3,'0')),
//                      :blockType),
//              :rawData,
//              :LoadFilename,
//              load_filename_to_load_file_id(:LoadFilename)
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3873 = ffd.getFieldData("rec_type").trim();
 String __sJT_3874 = ffd.getFieldData("merchant_number").trim();
 String __sJT_3875 = ffd.getFieldData("catid").trim();
 String __sJT_3876 = ffd.getFieldData("store_number").trim();
 String __sJT_3877 = ffd.getFieldData("batch_number").trim();
 String __sJT_3878 = ffd.getFieldData("tran_seq_num").trim();
 String __sJT_3879 = String.valueOf(ct);
   String theSqlTS = "insert into daily_detail_file_cielo_raw\n          (\n            rec_type,\n            merchant_number,\n            batch_date,\n            catid,\n            store_number,\n            transaction_key,\n            file_seq_num,\n            ro_number,\n            tran_seq_num,\n            block_type,\n            raw_data,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n            decode( :10 ,\n                    8,to_number(substr( :11 ,decode( :12 ,'v',50,79),3) || lpad(to_char( :13 ),3,'0')),\n                     :14 ),\n             :15 ,\n             :16 ,\n            load_filename_to_load_file_id( :17 )\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3873);
   __sJT_st.setString(2,__sJT_3874);
   __sJT_st.setDate(3,batchDate);
   __sJT_st.setString(4,__sJT_3875);
   __sJT_st.setString(5,__sJT_3876);
   __sJT_st.setString(6,transactionKey);
   __sJT_st.setString(7,fileSeqNum);
   __sJT_st.setString(8,__sJT_3877);
   __sJT_st.setString(9,__sJT_3878);
   __sJT_st.setInt(10,blockType);
   __sJT_st.setString(11,rawData);
   __sJT_st.setString(12,__sJT_3879);
   __sJT_st.setInt(13,blockType);
   __sJT_st.setInt(14,blockType);
   __sJT_st.setString(15,rawData);
   __sJT_st.setString(16,LoadFilename);
   __sJT_st.setString(17,LoadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:985^9*/
      }
      catch( Exception e )
      {
        logEntry("run()",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }
  
  public class MifWorker
    extends SQLJConnectionBase
    implements Runnable
  {
    protected   String    LoadFilename  = null;
    protected   String    RawData       = null;
    
    public MifWorker( String rawData, String loadFilename )
      throws Exception
    {
      RawData       = rawData;
      LoadFilename  = loadFilename;
    }
    
    public void run()
    {
      int               defId = -1;
      FlatFileRecord    ffd   = null;
      
      try
      {
        connect(true);
        
        if ( LoadFilename.startsWith("cmif_test") )
        {
          defId = MesFlatFiles.DEF_TYPE_MIF_CIELO_TEST;
        }
        else
        {
          defId = MesFlatFiles.DEF_TYPE_MIF_CIELO;
        }
        
        ffd = new FlatFileRecord(Ctx,defId);
        ffd.suck(RawData);
        ffd.scrubFiller('*');
        
        String dateTimeString = ffd.getFieldData("file_date") + ffd.getFieldData("file_time");
        
        /*@lineinfo:generated-code*//*@lineinfo:1036^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    mif_cielo
//            where   merchant_number = :ffd.getFieldData("merchant_number").trim()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3880 = ffd.getFieldData("merchant_number").trim();
  try {
   String theSqlTS = "delete\n          from    mif_cielo\n          where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3880);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1041^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:1043^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mif_cielo
//            (
//              rec_type,
//              seq_num,
//              archive_type,
//              file_ts,
//              routine_type,
//              movto_date,
//              version_number,
//              activity_type,
//              principal,
//              sic_code,
//              invoice_category,
//              merchant_status,
//              date_opened,
//              date_closed,
//              closure_reason,
//              activity_status,
//              affiliation_code,
//              affiliation_date,
//              date_last_activity,
//              date_visit,
//              marketing_rep_code,
//              date_marketing_visit,
//              user_code_2,
//              merchant_number,
//              billing_cycle_code,
//              retail_name,
//              address_1,
//              address_2,
//              phone_ddd,
//              phone_number,
//              fax_ddd,
//              fax_number,
//              secured_indicator,
//              ro_indicator,
//              debit_balance_indicator,
//              force_monitor_flag,
//              ecommerce_indicator,
//              moto_indicator,
//              suspend_supplies_indicator,
//              cashback_indicator,
//              user_code_3,
//              vol_indicator,
//              transit_number,
//              dda_number,
//              city,
//              state,
//              zip_code,
//              business_type,
//              legal_name,
//              dba_name,
//              state_reg_number,
//              cpf_number,
//              service_rep_code,
//              chain,
//              associate,
//              owner_1_name,
//              owner_1_cpf,
//              owner_1_dob,
//              owner_2_name,
//              owner_2_cpf,
//              owner_2_dob,
//              owner_3_name,
//              owner_3_cpf,
//              owner_3_dob,
//              billing_attention,
//              auto_payment_indicator,
//              auto_payment_frequency,
//              auto_anticipate_1,
//              auto_anticipate_2,
//              auto_anticipate_3,
//              activation_status,
//              activation_status_date,
//              date_first_sale_after_f0,
//              date_stat_chgd_to_f0,
//              active_indicator,
//              action_flag_1,
//              action_flag_2,
//              action_flag_3,
//              action_flag_4,
//              action_flag_5,
//              action_flag_6,
//              action_flag_7,
//              action_flag_8,
//              action_flag_9,
//              date_first_sale,
//              date_status_changed,
//              date_purge_letter,
//              billing_address_1,
//              billing_address_2,
//              billing_zip,
//              billing_city,
//              billing_state,
//              account_mgmt_indicator,
//              auto_capture_indicator,
//              account_payment_indicator,
//              collection_reason_code,
//              visa_vale_legal_action_ind,
//              visa_vale_special_ind,
//              date_visa_vale_activited,
//              visa_vale_meal_indicator,
//              visa_vale_food_indicator,
//              visa_vale_auto_indicator,
//              visa_vale_auto_period,
//              visa_vale_auto_day_1,
//              visa_vale_auto_day_2,
//              visa_vale_auto_day_3,
//              visa_vale_bank_id,
//              visa_vale_agency_id,
//              visa_vale_account_number,
//              visa_vale_change_date,
//              visa_fleet_indicator,
//              recurring_indicator,
//              bank_payment_indicator,
//              mobile_payment_indicator,
//              v_mania_indicator,
//              v_mania_campaign_indicator,
//              capture_via_auth_indicator,
//              capture_via_auth_date,
//              visa_dist_indicator,
//              payment_lock_indicator,
//              auto_anticipate,
//              anticipation_frequency,
//              anticipation_type,
//              market_segment,
//              service_package,
//              signature_file_indicator,
//              extr_electronic_indicator,
//              exch_rate_diff_indicator,
//              supply_address_1,
//              supply_address_2,
//              supply_phone_ddd,
//              supply_city,
//              supply_cep_number,
//              supply_uf_code,
//              supply_phone,
//              supply_contact,
//              mark_code,
//              merchant_number_primary,
//              rec_count_total,
//              rec_count_included,
//              rec_count_altered,
//              rec_count_excluded,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :ffd.getFieldData("rec_type").trim(),
//              :ffd.getFieldData("seq_num").trim(),
//              :ffd.getFieldData("archive_type").trim(),
//              to_date(:dateTimeString,'yyyymmddhh24miss'),
//              :ffd.getFieldData("routine_type").trim(),
//              :ffd.getFieldAsSqlDate("movto_date"),
//              :ffd.getFieldData("version_number").trim(),
//              :ffd.getFieldData("activity_type").trim(),
//              :ffd.getFieldData("principal").trim(),
//              :ffd.getFieldData("sic_code").trim(),
//              :ffd.getFieldData("invoice_category").trim(),
//              :ffd.getFieldData("merchant_status").trim(),
//              :ffd.getFieldAsSqlDate("date_opened"),
//              :ffd.getFieldAsSqlDate("date_closed"),
//              :ffd.getFieldData("closure_reason").trim(),
//              :ffd.getFieldData("activity_status").trim(),
//              :ffd.getFieldData("affiliation_code").trim(),
//              :ffd.getFieldAsSqlDate("affiliation_date"),
//              :ffd.getFieldAsSqlDate("date_last_activity"),
//              :ffd.getFieldAsSqlDate("date_visit"),
//              :ffd.getFieldData("marketing_rep_code").trim(),
//              :ffd.getFieldAsSqlDate("date_marketing_visit"),
//              :ffd.getFieldData("user_code_2").trim(),
//              :ffd.getFieldData("merchant_number").trim(),
//              :ffd.getFieldData("billing_cycle_code").trim(),
//              :ffd.getFieldData("retail_name").trim(),
//              :ffd.getFieldData("address_1").trim(),
//              :ffd.getFieldData("address_2").trim(),
//              :ffd.getFieldData("phone_ddd").trim(),
//              :ffd.getFieldData("phone_number").trim(),
//              :ffd.getFieldData("fax_ddd").trim(),
//              :ffd.getFieldData("fax_number").trim(),
//              :ffd.getFieldData("secured_indicator").trim(),
//              :ffd.getFieldData("ro_indicator").trim(),
//              :ffd.getFieldData("debit_balance_indicator").trim(),
//              :ffd.getFieldData("force_monitor_flag").trim(),
//              :ffd.getFieldData("ecommerce_indicator").trim(),
//              :ffd.getFieldData("moto_indicator").trim(),
//              :ffd.getFieldData("suspend_supplies_indicator").trim(),
//              :ffd.getFieldData("cashback_indicator").trim(),
//              :ffd.getFieldData("user_code_3").trim(),
//              :ffd.getFieldData("vol_indicator").trim(),
//              :ffd.getFieldData("transit_number").trim(),
//              :ffd.getFieldData("dda_number").trim(),
//              :ffd.getFieldData("city").trim(),
//              :ffd.getFieldData("state").trim(),
//              :ffd.getFieldData("zip_code").trim(),
//              :ffd.getFieldData("business_type").trim(),
//              :ffd.getFieldData("legal_name").trim(),
//              :ffd.getFieldData("dba_name").trim(),
//              :ffd.getFieldData("state_reg_number").trim(),
//              :ffd.getFieldData("cpf_number").trim(),
//              :ffd.getFieldData("service_rep_code").trim(),
//              :ffd.getFieldData("chain").trim(),
//              :ffd.getFieldData("associate").trim(),
//              :ffd.getFieldData("owner_1_name").trim(),
//              :ffd.getFieldData("owner_1_cpf").trim(),
//              :ffd.getFieldAsSqlDate("owner_1_dob"),
//              :ffd.getFieldData("owner_2_name").trim(),
//              :ffd.getFieldData("owner_2_cpf").trim(),
//              :ffd.getFieldAsSqlDate("owner_2_dob"),
//              :ffd.getFieldData("owner_3_name").trim(),
//              :ffd.getFieldData("owner_3_cpf").trim(),
//              :ffd.getFieldAsSqlDate("owner_3_dob"),
//              :ffd.getFieldData("billing_attention").trim(),
//              :ffd.getFieldData("auto_payment_indicator").trim(),
//              :ffd.getFieldData("auto_payment_frequency").trim(),
//              :ffd.getFieldData("auto_anticipate_1").trim(),
//              :ffd.getFieldData("auto_anticipate_2").trim(),
//              :ffd.getFieldData("auto_anticipate_3").trim(),
//              :ffd.getFieldData("activation_status").trim(),
//              :ffd.getFieldAsSqlDate("activation_status_date"),
//              :ffd.getFieldAsSqlDate("date_first_sale_after_f0"),
//              :ffd.getFieldAsSqlDate("date_stat_chgd_to_f0"),
//              :ffd.getFieldData("active_indicator").trim(),
//              :ffd.getFieldData("action_flag_1").trim(),
//              :ffd.getFieldData("action_flag_2").trim(),
//              :ffd.getFieldData("action_flag_3").trim(),
//              :ffd.getFieldData("action_flag_4").trim(),
//              :ffd.getFieldData("action_flag_5").trim(),
//              :ffd.getFieldData("action_flag_6").trim(),
//              :ffd.getFieldData("action_flag_7").trim(),
//              :ffd.getFieldData("action_flag_8").trim(),
//              :ffd.getFieldData("action_flag_9").trim(),
//              :ffd.getFieldAsSqlDate("date_first_sale"),
//              :ffd.getFieldAsSqlDate("date_status_changed"),
//              :ffd.getFieldAsSqlDate("date_purge_letter"),
//              :ffd.getFieldData("billing_address_1").trim(),
//              :ffd.getFieldData("billing_address_2").trim(),
//              :ffd.getFieldData("billing_zip").trim(),
//              :ffd.getFieldData("billing_city").trim(),
//              :ffd.getFieldData("billing_state").trim(),
//              :ffd.getFieldData("account_mgmt_indicator").trim(),
//              :ffd.getFieldData("auto_capture_indicator").trim(),
//              :ffd.getFieldData("account_payment_indicator").trim(),
//              :ffd.getFieldData("collection_reason_code").trim(),
//              :ffd.getFieldData("visa_vale_legal_action_ind").trim(),
//              :ffd.getFieldData("visa_vale_special_ind").trim(),
//              :ffd.getFieldAsSqlDate("date_visa_vale_activited"),
//              :ffd.getFieldData("visa_vale_meal_indicator").trim(),
//              :ffd.getFieldData("visa_vale_food_indicator").trim(),
//              :ffd.getFieldData("visa_vale_auto_indicator").trim(),
//              :ffd.getFieldData("visa_vale_auto_period").trim(),
//              :ffd.getFieldData("visa_vale_auto_day_1").trim(),
//              :ffd.getFieldData("visa_vale_auto_day_2").trim(),
//              :ffd.getFieldData("visa_vale_auto_day_3").trim(),
//              :ffd.getFieldData("visa_vale_bank_id").trim(),
//              :ffd.getFieldData("visa_vale_agency_id").trim(),
//              :ffd.getFieldData("visa_vale_account_number").trim(),
//              :ffd.getFieldAsSqlDate("visa_vale_change_date"),
//              :ffd.getFieldData("visa_fleet_indicator").trim(),
//              :ffd.getFieldData("recurring_indicator").trim(),
//              :ffd.getFieldData("bank_payment_indicator").trim(),
//              :ffd.getFieldData("mobile_payment_indicator").trim(),
//              :ffd.getFieldData("v_mania_indicator").trim(),
//              :ffd.getFieldData("v_mania_campaign_indicator").trim(),
//              :ffd.getFieldData("capture_via_auth_indicator").trim(),
//              :ffd.getFieldAsSqlDate("capture_via_auth_date"),
//              :ffd.getFieldData("visa_dist_indicator").trim(),
//              :ffd.getFieldData("payment_lock_indicator").trim(),
//              :ffd.getFieldData("auto_anticipate").trim(),
//              :ffd.getFieldData("anticipation_frequency").trim(),
//              :ffd.getFieldData("anticipation_type").trim(),
//              :ffd.getFieldData("market_segment").trim(),
//              :ffd.getFieldData("service_package").trim(),
//              :ffd.getFieldData("signature_file_indicator").trim(),
//              :ffd.getFieldData("extr_electronic_indicator").trim(),
//              :ffd.getFieldData("exch_rate_diff_indicator").trim(),
//              :ffd.getFieldData("supply_address_1").trim(),
//              :ffd.getFieldData("supply_address_2").trim(),
//              :ffd.getFieldData("supply_phone_ddd").trim(),
//              :ffd.getFieldData("supply_city").trim(),
//              :ffd.getFieldData("supply_cep_number").trim(),
//              :ffd.getFieldData("supply_uf_code").trim(),
//              :ffd.getFieldData("supply_phone").trim(),
//              :ffd.getFieldData("supply_contact").trim(),
//              :ffd.getFieldData("mark_code").trim(),
//              :ffd.getFieldData("merchant_number_primary").trim(),
//              :ffd.getFieldData("rec_count_total").trim(),
//              :ffd.getFieldData("rec_count_included").trim(),
//              :ffd.getFieldData("rec_count_altered").trim(),
//              :ffd.getFieldData("rec_count_excluded").trim(),
//              :LoadFilename,
//              load_filename_to_load_file_id(:LoadFilename)
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3881 = ffd.getFieldData("rec_type").trim();
 String __sJT_3882 = ffd.getFieldData("seq_num").trim();
 String __sJT_3883 = ffd.getFieldData("archive_type").trim();
 String __sJT_3884 = ffd.getFieldData("routine_type").trim();
 java.sql.Date __sJT_3885 = ffd.getFieldAsSqlDate("movto_date");
 String __sJT_3886 = ffd.getFieldData("version_number").trim();
 String __sJT_3887 = ffd.getFieldData("activity_type").trim();
 String __sJT_3888 = ffd.getFieldData("principal").trim();
 String __sJT_3889 = ffd.getFieldData("sic_code").trim();
 String __sJT_3890 = ffd.getFieldData("invoice_category").trim();
 String __sJT_3891 = ffd.getFieldData("merchant_status").trim();
 java.sql.Date __sJT_3892 = ffd.getFieldAsSqlDate("date_opened");
 java.sql.Date __sJT_3893 = ffd.getFieldAsSqlDate("date_closed");
 String __sJT_3894 = ffd.getFieldData("closure_reason").trim();
 String __sJT_3895 = ffd.getFieldData("activity_status").trim();
 String __sJT_3896 = ffd.getFieldData("affiliation_code").trim();
 java.sql.Date __sJT_3897 = ffd.getFieldAsSqlDate("affiliation_date");
 java.sql.Date __sJT_3898 = ffd.getFieldAsSqlDate("date_last_activity");
 java.sql.Date __sJT_3899 = ffd.getFieldAsSqlDate("date_visit");
 String __sJT_3900 = ffd.getFieldData("marketing_rep_code").trim();
 java.sql.Date __sJT_3901 = ffd.getFieldAsSqlDate("date_marketing_visit");
 String __sJT_3902 = ffd.getFieldData("user_code_2").trim();
 String __sJT_3903 = ffd.getFieldData("merchant_number").trim();
 String __sJT_3904 = ffd.getFieldData("billing_cycle_code").trim();
 String __sJT_3905 = ffd.getFieldData("retail_name").trim();
 String __sJT_3906 = ffd.getFieldData("address_1").trim();
 String __sJT_3907 = ffd.getFieldData("address_2").trim();
 String __sJT_3908 = ffd.getFieldData("phone_ddd").trim();
 String __sJT_3909 = ffd.getFieldData("phone_number").trim();
 String __sJT_3910 = ffd.getFieldData("fax_ddd").trim();
 String __sJT_3911 = ffd.getFieldData("fax_number").trim();
 String __sJT_3912 = ffd.getFieldData("secured_indicator").trim();
 String __sJT_3913 = ffd.getFieldData("ro_indicator").trim();
 String __sJT_3914 = ffd.getFieldData("debit_balance_indicator").trim();
 String __sJT_3915 = ffd.getFieldData("force_monitor_flag").trim();
 String __sJT_3916 = ffd.getFieldData("ecommerce_indicator").trim();
 String __sJT_3917 = ffd.getFieldData("moto_indicator").trim();
 String __sJT_3918 = ffd.getFieldData("suspend_supplies_indicator").trim();
 String __sJT_3919 = ffd.getFieldData("cashback_indicator").trim();
 String __sJT_3920 = ffd.getFieldData("user_code_3").trim();
 String __sJT_3921 = ffd.getFieldData("vol_indicator").trim();
 String __sJT_3922 = ffd.getFieldData("transit_number").trim();
 String __sJT_3923 = ffd.getFieldData("dda_number").trim();
 String __sJT_3924 = ffd.getFieldData("city").trim();
 String __sJT_3925 = ffd.getFieldData("state").trim();
 String __sJT_3926 = ffd.getFieldData("zip_code").trim();
 String __sJT_3927 = ffd.getFieldData("business_type").trim();
 String __sJT_3928 = ffd.getFieldData("legal_name").trim();
 String __sJT_3929 = ffd.getFieldData("dba_name").trim();
 String __sJT_3930 = ffd.getFieldData("state_reg_number").trim();
 String __sJT_3931 = ffd.getFieldData("cpf_number").trim();
 String __sJT_3932 = ffd.getFieldData("service_rep_code").trim();
 String __sJT_3933 = ffd.getFieldData("chain").trim();
 String __sJT_3934 = ffd.getFieldData("associate").trim();
 String __sJT_3935 = ffd.getFieldData("owner_1_name").trim();
 String __sJT_3936 = ffd.getFieldData("owner_1_cpf").trim();
 java.sql.Date __sJT_3937 = ffd.getFieldAsSqlDate("owner_1_dob");
 String __sJT_3938 = ffd.getFieldData("owner_2_name").trim();
 String __sJT_3939 = ffd.getFieldData("owner_2_cpf").trim();
 java.sql.Date __sJT_3940 = ffd.getFieldAsSqlDate("owner_2_dob");
 String __sJT_3941 = ffd.getFieldData("owner_3_name").trim();
 String __sJT_3942 = ffd.getFieldData("owner_3_cpf").trim();
 java.sql.Date __sJT_3943 = ffd.getFieldAsSqlDate("owner_3_dob");
 String __sJT_3944 = ffd.getFieldData("billing_attention").trim();
 String __sJT_3945 = ffd.getFieldData("auto_payment_indicator").trim();
 String __sJT_3946 = ffd.getFieldData("auto_payment_frequency").trim();
 String __sJT_3947 = ffd.getFieldData("auto_anticipate_1").trim();
 String __sJT_3948 = ffd.getFieldData("auto_anticipate_2").trim();
 String __sJT_3949 = ffd.getFieldData("auto_anticipate_3").trim();
 String __sJT_3950 = ffd.getFieldData("activation_status").trim();
 java.sql.Date __sJT_3951 = ffd.getFieldAsSqlDate("activation_status_date");
 java.sql.Date __sJT_3952 = ffd.getFieldAsSqlDate("date_first_sale_after_f0");
 java.sql.Date __sJT_3953 = ffd.getFieldAsSqlDate("date_stat_chgd_to_f0");
 String __sJT_3954 = ffd.getFieldData("active_indicator").trim();
 String __sJT_3955 = ffd.getFieldData("action_flag_1").trim();
 String __sJT_3956 = ffd.getFieldData("action_flag_2").trim();
 String __sJT_3957 = ffd.getFieldData("action_flag_3").trim();
 String __sJT_3958 = ffd.getFieldData("action_flag_4").trim();
 String __sJT_3959 = ffd.getFieldData("action_flag_5").trim();
 String __sJT_3960 = ffd.getFieldData("action_flag_6").trim();
 String __sJT_3961 = ffd.getFieldData("action_flag_7").trim();
 String __sJT_3962 = ffd.getFieldData("action_flag_8").trim();
 String __sJT_3963 = ffd.getFieldData("action_flag_9").trim();
 java.sql.Date __sJT_3964 = ffd.getFieldAsSqlDate("date_first_sale");
 java.sql.Date __sJT_3965 = ffd.getFieldAsSqlDate("date_status_changed");
 java.sql.Date __sJT_3966 = ffd.getFieldAsSqlDate("date_purge_letter");
 String __sJT_3967 = ffd.getFieldData("billing_address_1").trim();
 String __sJT_3968 = ffd.getFieldData("billing_address_2").trim();
 String __sJT_3969 = ffd.getFieldData("billing_zip").trim();
 String __sJT_3970 = ffd.getFieldData("billing_city").trim();
 String __sJT_3971 = ffd.getFieldData("billing_state").trim();
 String __sJT_3972 = ffd.getFieldData("account_mgmt_indicator").trim();
 String __sJT_3973 = ffd.getFieldData("auto_capture_indicator").trim();
 String __sJT_3974 = ffd.getFieldData("account_payment_indicator").trim();
 String __sJT_3975 = ffd.getFieldData("collection_reason_code").trim();
 String __sJT_3976 = ffd.getFieldData("visa_vale_legal_action_ind").trim();
 String __sJT_3977 = ffd.getFieldData("visa_vale_special_ind").trim();
 java.sql.Date __sJT_3978 = ffd.getFieldAsSqlDate("date_visa_vale_activited");
 String __sJT_3979 = ffd.getFieldData("visa_vale_meal_indicator").trim();
 String __sJT_3980 = ffd.getFieldData("visa_vale_food_indicator").trim();
 String __sJT_3981 = ffd.getFieldData("visa_vale_auto_indicator").trim();
 String __sJT_3982 = ffd.getFieldData("visa_vale_auto_period").trim();
 String __sJT_3983 = ffd.getFieldData("visa_vale_auto_day_1").trim();
 String __sJT_3984 = ffd.getFieldData("visa_vale_auto_day_2").trim();
 String __sJT_3985 = ffd.getFieldData("visa_vale_auto_day_3").trim();
 String __sJT_3986 = ffd.getFieldData("visa_vale_bank_id").trim();
 String __sJT_3987 = ffd.getFieldData("visa_vale_agency_id").trim();
 String __sJT_3988 = ffd.getFieldData("visa_vale_account_number").trim();
 java.sql.Date __sJT_3989 = ffd.getFieldAsSqlDate("visa_vale_change_date");
 String __sJT_3990 = ffd.getFieldData("visa_fleet_indicator").trim();
 String __sJT_3991 = ffd.getFieldData("recurring_indicator").trim();
 String __sJT_3992 = ffd.getFieldData("bank_payment_indicator").trim();
 String __sJT_3993 = ffd.getFieldData("mobile_payment_indicator").trim();
 String __sJT_3994 = ffd.getFieldData("v_mania_indicator").trim();
 String __sJT_3995 = ffd.getFieldData("v_mania_campaign_indicator").trim();
 String __sJT_3996 = ffd.getFieldData("capture_via_auth_indicator").trim();
 java.sql.Date __sJT_3997 = ffd.getFieldAsSqlDate("capture_via_auth_date");
 String __sJT_3998 = ffd.getFieldData("visa_dist_indicator").trim();
 String __sJT_3999 = ffd.getFieldData("payment_lock_indicator").trim();
 String __sJT_4000 = ffd.getFieldData("auto_anticipate").trim();
 String __sJT_4001 = ffd.getFieldData("anticipation_frequency").trim();
 String __sJT_4002 = ffd.getFieldData("anticipation_type").trim();
 String __sJT_4003 = ffd.getFieldData("market_segment").trim();
 String __sJT_4004 = ffd.getFieldData("service_package").trim();
 String __sJT_4005 = ffd.getFieldData("signature_file_indicator").trim();
 String __sJT_4006 = ffd.getFieldData("extr_electronic_indicator").trim();
 String __sJT_4007 = ffd.getFieldData("exch_rate_diff_indicator").trim();
 String __sJT_4008 = ffd.getFieldData("supply_address_1").trim();
 String __sJT_4009 = ffd.getFieldData("supply_address_2").trim();
 String __sJT_4010 = ffd.getFieldData("supply_phone_ddd").trim();
 String __sJT_4011 = ffd.getFieldData("supply_city").trim();
 String __sJT_4012 = ffd.getFieldData("supply_cep_number").trim();
 String __sJT_4013 = ffd.getFieldData("supply_uf_code").trim();
 String __sJT_4014 = ffd.getFieldData("supply_phone").trim();
 String __sJT_4015 = ffd.getFieldData("supply_contact").trim();
 String __sJT_4016 = ffd.getFieldData("mark_code").trim();
 String __sJT_4017 = ffd.getFieldData("merchant_number_primary").trim();
 String __sJT_4018 = ffd.getFieldData("rec_count_total").trim();
 String __sJT_4019 = ffd.getFieldData("rec_count_included").trim();
 String __sJT_4020 = ffd.getFieldData("rec_count_altered").trim();
 String __sJT_4021 = ffd.getFieldData("rec_count_excluded").trim();
   String theSqlTS = "insert into mif_cielo\n          (\n            rec_type,\n            seq_num,\n            archive_type,\n            file_ts,\n            routine_type,\n            movto_date,\n            version_number,\n            activity_type,\n            principal,\n            sic_code,\n            invoice_category,\n            merchant_status,\n            date_opened,\n            date_closed,\n            closure_reason,\n            activity_status,\n            affiliation_code,\n            affiliation_date,\n            date_last_activity,\n            date_visit,\n            marketing_rep_code,\n            date_marketing_visit,\n            user_code_2,\n            merchant_number,\n            billing_cycle_code,\n            retail_name,\n            address_1,\n            address_2,\n            phone_ddd,\n            phone_number,\n            fax_ddd,\n            fax_number,\n            secured_indicator,\n            ro_indicator,\n            debit_balance_indicator,\n            force_monitor_flag,\n            ecommerce_indicator,\n            moto_indicator,\n            suspend_supplies_indicator,\n            cashback_indicator,\n            user_code_3,\n            vol_indicator,\n            transit_number,\n            dda_number,\n            city,\n            state,\n            zip_code,\n            business_type,\n            legal_name,\n            dba_name,\n            state_reg_number,\n            cpf_number,\n            service_rep_code,\n            chain,\n            associate,\n            owner_1_name,\n            owner_1_cpf,\n            owner_1_dob,\n            owner_2_name,\n            owner_2_cpf,\n            owner_2_dob,\n            owner_3_name,\n            owner_3_cpf,\n            owner_3_dob,\n            billing_attention,\n            auto_payment_indicator,\n            auto_payment_frequency,\n            auto_anticipate_1,\n            auto_anticipate_2,\n            auto_anticipate_3,\n            activation_status,\n            activation_status_date,\n            date_first_sale_after_f0,\n            date_stat_chgd_to_f0,\n            active_indicator,\n            action_flag_1,\n            action_flag_2,\n            action_flag_3,\n            action_flag_4,\n            action_flag_5,\n            action_flag_6,\n            action_flag_7,\n            action_flag_8,\n            action_flag_9,\n            date_first_sale,\n            date_status_changed,\n            date_purge_letter,\n            billing_address_1,\n            billing_address_2,\n            billing_zip,\n            billing_city,\n            billing_state,\n            account_mgmt_indicator,\n            auto_capture_indicator,\n            account_payment_indicator,\n            collection_reason_code,\n            visa_vale_legal_action_ind,\n            visa_vale_special_ind,\n            date_visa_vale_activited,\n            visa_vale_meal_indicator,\n            visa_vale_food_indicator,\n            visa_vale_auto_indicator,\n            visa_vale_auto_period,\n            visa_vale_auto_day_1,\n            visa_vale_auto_day_2,\n            visa_vale_auto_day_3,\n            visa_vale_bank_id,\n            visa_vale_agency_id,\n            visa_vale_account_number,\n            visa_vale_change_date,\n            visa_fleet_indicator,\n            recurring_indicator,\n            bank_payment_indicator,\n            mobile_payment_indicator,\n            v_mania_indicator,\n            v_mania_campaign_indicator,\n            capture_via_auth_indicator,\n            capture_via_auth_date,\n            visa_dist_indicator,\n            payment_lock_indicator,\n            auto_anticipate,\n            anticipation_frequency,\n            anticipation_type,\n            market_segment,\n            service_package,\n            signature_file_indicator,\n            extr_electronic_indicator,\n            exch_rate_diff_indicator,\n            supply_address_1,\n            supply_address_2,\n            supply_phone_ddd,\n            supply_city,\n            supply_cep_number,\n            supply_uf_code,\n            supply_phone,\n            supply_contact,\n            mark_code,\n            merchant_number_primary,\n            rec_count_total,\n            rec_count_included,\n            rec_count_altered,\n            rec_count_excluded,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n            to_date( :4 ,'yyyymmddhh24miss'),\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 ,\n             :34 ,\n             :35 ,\n             :36 ,\n             :37 ,\n             :38 ,\n             :39 ,\n             :40 ,\n             :41 ,\n             :42 ,\n             :43 ,\n             :44 ,\n             :45 ,\n             :46 ,\n             :47 ,\n             :48 ,\n             :49 ,\n             :50 ,\n             :51 ,\n             :52 ,\n             :53 ,\n             :54 ,\n             :55 ,\n             :56 ,\n             :57 ,\n             :58 ,\n             :59 ,\n             :60 ,\n             :61 ,\n             :62 ,\n             :63 ,\n             :64 ,\n             :65 ,\n             :66 ,\n             :67 ,\n             :68 ,\n             :69 ,\n             :70 ,\n             :71 ,\n             :72 ,\n             :73 ,\n             :74 ,\n             :75 ,\n             :76 ,\n             :77 ,\n             :78 ,\n             :79 ,\n             :80 ,\n             :81 ,\n             :82 ,\n             :83 ,\n             :84 ,\n             :85 ,\n             :86 ,\n             :87 ,\n             :88 ,\n             :89 ,\n             :90 ,\n             :91 ,\n             :92 ,\n             :93 ,\n             :94 ,\n             :95 ,\n             :96 ,\n             :97 ,\n             :98 ,\n             :99 ,\n             :100 ,\n             :101 ,\n             :102 ,\n             :103 ,\n             :104 ,\n             :105 ,\n             :106 ,\n             :107 ,\n             :108 ,\n             :109 ,\n             :110 ,\n             :111 ,\n             :112 ,\n             :113 ,\n             :114 ,\n             :115 ,\n             :116 ,\n             :117 ,\n             :118 ,\n             :119 ,\n             :120 ,\n             :121 ,\n             :122 ,\n             :123 ,\n             :124 ,\n             :125 ,\n             :126 ,\n             :127 ,\n             :128 ,\n             :129 ,\n             :130 ,\n             :131 ,\n             :132 ,\n             :133 ,\n             :134 ,\n             :135 ,\n             :136 ,\n             :137 ,\n             :138 ,\n             :139 ,\n             :140 ,\n             :141 ,\n             :142 ,\n             :143 ,\n            load_filename_to_load_file_id( :144 )\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3881);
   __sJT_st.setString(2,__sJT_3882);
   __sJT_st.setString(3,__sJT_3883);
   __sJT_st.setString(4,dateTimeString);
   __sJT_st.setString(5,__sJT_3884);
   __sJT_st.setDate(6,__sJT_3885);
   __sJT_st.setString(7,__sJT_3886);
   __sJT_st.setString(8,__sJT_3887);
   __sJT_st.setString(9,__sJT_3888);
   __sJT_st.setString(10,__sJT_3889);
   __sJT_st.setString(11,__sJT_3890);
   __sJT_st.setString(12,__sJT_3891);
   __sJT_st.setDate(13,__sJT_3892);
   __sJT_st.setDate(14,__sJT_3893);
   __sJT_st.setString(15,__sJT_3894);
   __sJT_st.setString(16,__sJT_3895);
   __sJT_st.setString(17,__sJT_3896);
   __sJT_st.setDate(18,__sJT_3897);
   __sJT_st.setDate(19,__sJT_3898);
   __sJT_st.setDate(20,__sJT_3899);
   __sJT_st.setString(21,__sJT_3900);
   __sJT_st.setDate(22,__sJT_3901);
   __sJT_st.setString(23,__sJT_3902);
   __sJT_st.setString(24,__sJT_3903);
   __sJT_st.setString(25,__sJT_3904);
   __sJT_st.setString(26,__sJT_3905);
   __sJT_st.setString(27,__sJT_3906);
   __sJT_st.setString(28,__sJT_3907);
   __sJT_st.setString(29,__sJT_3908);
   __sJT_st.setString(30,__sJT_3909);
   __sJT_st.setString(31,__sJT_3910);
   __sJT_st.setString(32,__sJT_3911);
   __sJT_st.setString(33,__sJT_3912);
   __sJT_st.setString(34,__sJT_3913);
   __sJT_st.setString(35,__sJT_3914);
   __sJT_st.setString(36,__sJT_3915);
   __sJT_st.setString(37,__sJT_3916);
   __sJT_st.setString(38,__sJT_3917);
   __sJT_st.setString(39,__sJT_3918);
   __sJT_st.setString(40,__sJT_3919);
   __sJT_st.setString(41,__sJT_3920);
   __sJT_st.setString(42,__sJT_3921);
   __sJT_st.setString(43,__sJT_3922);
   __sJT_st.setString(44,__sJT_3923);
   __sJT_st.setString(45,__sJT_3924);
   __sJT_st.setString(46,__sJT_3925);
   __sJT_st.setString(47,__sJT_3926);
   __sJT_st.setString(48,__sJT_3927);
   __sJT_st.setString(49,__sJT_3928);
   __sJT_st.setString(50,__sJT_3929);
   __sJT_st.setString(51,__sJT_3930);
   __sJT_st.setString(52,__sJT_3931);
   __sJT_st.setString(53,__sJT_3932);
   __sJT_st.setString(54,__sJT_3933);
   __sJT_st.setString(55,__sJT_3934);
   __sJT_st.setString(56,__sJT_3935);
   __sJT_st.setString(57,__sJT_3936);
   __sJT_st.setDate(58,__sJT_3937);
   __sJT_st.setString(59,__sJT_3938);
   __sJT_st.setString(60,__sJT_3939);
   __sJT_st.setDate(61,__sJT_3940);
   __sJT_st.setString(62,__sJT_3941);
   __sJT_st.setString(63,__sJT_3942);
   __sJT_st.setDate(64,__sJT_3943);
   __sJT_st.setString(65,__sJT_3944);
   __sJT_st.setString(66,__sJT_3945);
   __sJT_st.setString(67,__sJT_3946);
   __sJT_st.setString(68,__sJT_3947);
   __sJT_st.setString(69,__sJT_3948);
   __sJT_st.setString(70,__sJT_3949);
   __sJT_st.setString(71,__sJT_3950);
   __sJT_st.setDate(72,__sJT_3951);
   __sJT_st.setDate(73,__sJT_3952);
   __sJT_st.setDate(74,__sJT_3953);
   __sJT_st.setString(75,__sJT_3954);
   __sJT_st.setString(76,__sJT_3955);
   __sJT_st.setString(77,__sJT_3956);
   __sJT_st.setString(78,__sJT_3957);
   __sJT_st.setString(79,__sJT_3958);
   __sJT_st.setString(80,__sJT_3959);
   __sJT_st.setString(81,__sJT_3960);
   __sJT_st.setString(82,__sJT_3961);
   __sJT_st.setString(83,__sJT_3962);
   __sJT_st.setString(84,__sJT_3963);
   __sJT_st.setDate(85,__sJT_3964);
   __sJT_st.setDate(86,__sJT_3965);
   __sJT_st.setDate(87,__sJT_3966);
   __sJT_st.setString(88,__sJT_3967);
   __sJT_st.setString(89,__sJT_3968);
   __sJT_st.setString(90,__sJT_3969);
   __sJT_st.setString(91,__sJT_3970);
   __sJT_st.setString(92,__sJT_3971);
   __sJT_st.setString(93,__sJT_3972);
   __sJT_st.setString(94,__sJT_3973);
   __sJT_st.setString(95,__sJT_3974);
   __sJT_st.setString(96,__sJT_3975);
   __sJT_st.setString(97,__sJT_3976);
   __sJT_st.setString(98,__sJT_3977);
   __sJT_st.setDate(99,__sJT_3978);
   __sJT_st.setString(100,__sJT_3979);
   __sJT_st.setString(101,__sJT_3980);
   __sJT_st.setString(102,__sJT_3981);
   __sJT_st.setString(103,__sJT_3982);
   __sJT_st.setString(104,__sJT_3983);
   __sJT_st.setString(105,__sJT_3984);
   __sJT_st.setString(106,__sJT_3985);
   __sJT_st.setString(107,__sJT_3986);
   __sJT_st.setString(108,__sJT_3987);
   __sJT_st.setString(109,__sJT_3988);
   __sJT_st.setDate(110,__sJT_3989);
   __sJT_st.setString(111,__sJT_3990);
   __sJT_st.setString(112,__sJT_3991);
   __sJT_st.setString(113,__sJT_3992);
   __sJT_st.setString(114,__sJT_3993);
   __sJT_st.setString(115,__sJT_3994);
   __sJT_st.setString(116,__sJT_3995);
   __sJT_st.setString(117,__sJT_3996);
   __sJT_st.setDate(118,__sJT_3997);
   __sJT_st.setString(119,__sJT_3998);
   __sJT_st.setString(120,__sJT_3999);
   __sJT_st.setString(121,__sJT_4000);
   __sJT_st.setString(122,__sJT_4001);
   __sJT_st.setString(123,__sJT_4002);
   __sJT_st.setString(124,__sJT_4003);
   __sJT_st.setString(125,__sJT_4004);
   __sJT_st.setString(126,__sJT_4005);
   __sJT_st.setString(127,__sJT_4006);
   __sJT_st.setString(128,__sJT_4007);
   __sJT_st.setString(129,__sJT_4008);
   __sJT_st.setString(130,__sJT_4009);
   __sJT_st.setString(131,__sJT_4010);
   __sJT_st.setString(132,__sJT_4011);
   __sJT_st.setString(133,__sJT_4012);
   __sJT_st.setString(134,__sJT_4013);
   __sJT_st.setString(135,__sJT_4014);
   __sJT_st.setString(136,__sJT_4015);
   __sJT_st.setString(137,__sJT_4016);
   __sJT_st.setString(138,__sJT_4017);
   __sJT_st.setString(139,__sJT_4018);
   __sJT_st.setString(140,__sJT_4019);
   __sJT_st.setString(141,__sJT_4020);
   __sJT_st.setString(142,__sJT_4021);
   __sJT_st.setString(143,LoadFilename);
   __sJT_st.setString(144,LoadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1339^9*/
      }
      catch( Exception e )
      {
        logEntry("run()",e.toString());
        ffd.showData();
      }
      finally
      {
        cleanUp();
      }
    }
  }
  
  public class HierarchyWorker
    extends SQLJConnectionBase
    implements Runnable
  {
    protected   String    LoadFilename  = null;
    protected   String    RawData       = null;
    
    public HierarchyWorker( String rawData, String loadFilename )
      throws Exception
    {
      RawData       = rawData;
      LoadFilename  = loadFilename;
    }
    
    public void run()
    {
      FlatFileRecord    ffd   = null;
      try
      {
        connect(true);
        
        ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_MIF_CIELO_HIERARCHY);
        ffd.suck(RawData);
        
        // for chain merchants, Cielo puts the primary merchant 
        // number into group 5 and group 6 hierarchy nodes.  
        // replace with unique 6-digit group 5 and group 6 values
        // none chain merchants will be assigned non-chain
        // group 5 and group 6 hierarchy nodes in the table trigger
        if ( ffd.getFieldData("group5_id").trim().length() > 6 
             && ffd.getFieldData("chain").trim().length() != 0 )
        {
          fixGroupIds(ffd);
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:1388^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    mif_cielo_hierarchy
//            where   merchant_number = :ffd.getFieldData("merchant_number").trim()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4022 = ffd.getFieldData("merchant_number").trim();
  try {
   String theSqlTS = "delete\n          from    mif_cielo_hierarchy\n          where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4022);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1393^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:1395^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mif_cielo_hierarchy
//            (
//              merchant_number,
//              merchant_number_primary,
//              dba_name,
//              merchant_type,
//              payment_type,
//              company,
//              group1_name,
//              group2_id,
//              group2_name,
//              group3_id,
//              group3_name,
//              group4_id,
//              group4_name,
//              group5_id,
//              group5_name,
//              group6_id,
//              group6_name,
//              principal,
//              chain,
//              association_name,
//              associate,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :ffd.getFieldData("merchant_number").trim(),
//              :ffd.getFieldData("merchant_number_primary").trim(),
//              :ffd.getFieldData("dba_name").trim(),
//              :ffd.getFieldData("merchant_type").trim(),
//              :ffd.getFieldData("payment_type").trim(),
//              :ffd.getFieldData("company").trim(),
//              :ffd.getFieldData("group1_name").trim(),
//              :ffd.getFieldData("group2_id").trim(),
//              :ffd.getFieldData("group2_name").trim(),
//              :ffd.getFieldData("group3_id").trim(),
//              :ffd.getFieldData("group3_name").trim(),
//              :ffd.getFieldData("group4_id").trim(),
//              :ffd.getFieldData("group4_name").trim(),
//              :ffd.getFieldData("group5_id").trim(),
//              :ffd.getFieldData("group5_name").trim(),
//              :ffd.getFieldData("group6_id").trim(),
//              :ffd.getFieldData("group6_name").trim(),
//              :ffd.getFieldData("principal").trim(),
//              :ffd.getFieldData("chain").trim(),
//              :ffd.getFieldData("chain_name").trim(),
//              :ffd.getFieldData("associate").trim(),
//              :LoadFilename,
//              load_filename_to_load_file_id(:LoadFilename)
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4023 = ffd.getFieldData("merchant_number").trim();
 String __sJT_4024 = ffd.getFieldData("merchant_number_primary").trim();
 String __sJT_4025 = ffd.getFieldData("dba_name").trim();
 String __sJT_4026 = ffd.getFieldData("merchant_type").trim();
 String __sJT_4027 = ffd.getFieldData("payment_type").trim();
 String __sJT_4028 = ffd.getFieldData("company").trim();
 String __sJT_4029 = ffd.getFieldData("group1_name").trim();
 String __sJT_4030 = ffd.getFieldData("group2_id").trim();
 String __sJT_4031 = ffd.getFieldData("group2_name").trim();
 String __sJT_4032 = ffd.getFieldData("group3_id").trim();
 String __sJT_4033 = ffd.getFieldData("group3_name").trim();
 String __sJT_4034 = ffd.getFieldData("group4_id").trim();
 String __sJT_4035 = ffd.getFieldData("group4_name").trim();
 String __sJT_4036 = ffd.getFieldData("group5_id").trim();
 String __sJT_4037 = ffd.getFieldData("group5_name").trim();
 String __sJT_4038 = ffd.getFieldData("group6_id").trim();
 String __sJT_4039 = ffd.getFieldData("group6_name").trim();
 String __sJT_4040 = ffd.getFieldData("principal").trim();
 String __sJT_4041 = ffd.getFieldData("chain").trim();
 String __sJT_4042 = ffd.getFieldData("chain_name").trim();
 String __sJT_4043 = ffd.getFieldData("associate").trim();
   String theSqlTS = "insert into mif_cielo_hierarchy\n          (\n            merchant_number,\n            merchant_number_primary,\n            dba_name,\n            merchant_type,\n            payment_type,\n            company,\n            group1_name,\n            group2_id,\n            group2_name,\n            group3_id,\n            group3_name,\n            group4_id,\n            group4_name,\n            group5_id,\n            group5_name,\n            group6_id,\n            group6_name,\n            principal,\n            chain,\n            association_name,\n            associate,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n            load_filename_to_load_file_id( :23 )\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4023);
   __sJT_st.setString(2,__sJT_4024);
   __sJT_st.setString(3,__sJT_4025);
   __sJT_st.setString(4,__sJT_4026);
   __sJT_st.setString(5,__sJT_4027);
   __sJT_st.setString(6,__sJT_4028);
   __sJT_st.setString(7,__sJT_4029);
   __sJT_st.setString(8,__sJT_4030);
   __sJT_st.setString(9,__sJT_4031);
   __sJT_st.setString(10,__sJT_4032);
   __sJT_st.setString(11,__sJT_4033);
   __sJT_st.setString(12,__sJT_4034);
   __sJT_st.setString(13,__sJT_4035);
   __sJT_st.setString(14,__sJT_4036);
   __sJT_st.setString(15,__sJT_4037);
   __sJT_st.setString(16,__sJT_4038);
   __sJT_st.setString(17,__sJT_4039);
   __sJT_st.setString(18,__sJT_4040);
   __sJT_st.setString(19,__sJT_4041);
   __sJT_st.setString(20,__sJT_4042);
   __sJT_st.setString(21,__sJT_4043);
   __sJT_st.setString(22,LoadFilename);
   __sJT_st.setString(23,LoadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1449^9*/
      }
      catch( Exception e )
      {
        logEntry("run()",e.toString());
        ffd.showData();
      }
      finally
      {
        cleanUp();
      }
    }
  }
  
  public class UpdateMifWorker
    extends SQLJConnectionBase
    implements Runnable
  {
    protected   long      MerchantId    = 0L;
    
    public UpdateMifWorker( long merchantId )
      throws Exception
    {
      MerchantId = merchantId;
    }
    
    private String encodeField( ResultSet resultSet, String fname )
      throws Exception
    {
      int                   idx           = 1;
      ResultSetMetaData     metaData      = resultSet.getMetaData();
      String                retVal        = null;
      
      //@System.out.println("fname = " + fname);//@
      for( idx = 1; idx < metaData.getColumnCount(); ++idx )
      {
        if ( fname.compareToIgnoreCase(metaData.getColumnName(idx)) == 0 )
        {
          break;
        }
      }
      switch ( metaData.getColumnType(idx) )
      {
        case java.sql.Types.REAL:          // float  ( 32-bit floating point )
        case java.sql.Types.FLOAT:         // double ( 64-bit floating point )
        case java.sql.Types.DOUBLE:        // ""
        case java.sql.Types.NUMERIC:
        case java.sql.Types.DECIMAL:
        case java.sql.Types.SMALLINT:      // short  ( 16-bit signed )
        case java.sql.Types.INTEGER:       // int    ( 32-bit signed )
        case java.sql.Types.BIGINT:        // long   ( 32-bit signed )
          retVal = resultSet.getString(fname);
          break;
          
        case java.sql.Types.DATE:          // java.sql.Date - date data
        case java.sql.Types.TIMESTAMP:     // java.sql.Timestamp - date and time data
        case java.sql.Types.TIME:          // java.sql.Time - time data
          retVal = "'" + DateTimeFormatter.getFormattedDate(resultSet.getDate(fname),"dd-MMM-yyyy") + "'";
          break;
          
        case java.sql.Types.CHAR:          // String - fixed length string
        case java.sql.Types.VARCHAR:       // String - variable length string
        case java.sql.Types.LONGVARCHAR:   // String - variable length string - large (BLOB data)
        default:
          String value = resultSet.getString(fname);
          if ( value == null )
          {
            retVal = "null";
          }
          else
          {
            retVal  = "'" + value.trim().replaceAll("'","''") + "'";
          }
          break;
      }
      return( retVal );
    }
    
    public void run()
    {
      ResultSetIterator   it            = null;
      int                 recCount      = 0;
      ResultSet           resultSet     = null;
      String              updateSql     = "";
      
      try
      {
        connect(true);
        
        String  fname       = "";
        String  insertSql   = "insert into mif ( bank_number";
        String  selectSql   = "select 8008";
        
        /*@lineinfo:generated-code*//*@lineinfo:1542^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    mif   mf
//            where   mf.merchant_number = :MerchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    mif   mf\n          where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1548^9*/
        
        if ( recCount == 0 )
        {
          for( int i = 0; i < MifFields.length; ++i )
          {
            if ( MifFields[i][0].charAt(0) == '*' ) continue;
          
            fname = ((MifFields[i][1].charAt(0) == '*') ? MifFields[i][1].substring(1) : "mfc." + MifFields[i][1]);
          
            insertSql += "," + MifFields[i][0];
            selectSql += "," + fname;
          }
          insertSql +=    ",group_1_association   "
                        + ",group_2_association   "
                        + ",group_3_association   "
                        + ",group_4_association   "
                        + ",group_5_association   "
                        + ",group_6_association   " 
                        + ",association_node      "
                        + ",dmagent  )            ";
          selectSql +=    " ,mfh.group1_id                                      "
                        + " ,mfh.group2_id                                      "
                        + " ,mfh.group3_id                                      "
                        + " ,mfh.group4_id                                      "
                        + " ,mfh.group5_id                                      "
                        + " ,mfh.group6_id                                      "
                        + " ,('8008' || mfh.association)                        "
                        + " ,mfh.association                                    "
                        + " from mif_cielo              mfc,                    "
                        + "      mif_cielo_hierarchy    mfh                     "
                        + " where mfc.merchant_number = ?                       "
                        + "       and mfh.merchant_number = to_char(mfc.merchant_number) ";
                      
          //@System.out.println(insertSql+selectSql);                      
                      
          PreparedStatement ps = getPreparedStatement(insertSql + selectSql);
          ps.setLong(1,MerchantId);
          ps.executeUpdate();
          ps.close();
        }
        else    // update
        {
          /*@lineinfo:generated-code*//*@lineinfo:1591^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  mfc.*
//              from    mif_cielo   mfc
//              where   mfc.merchant_number = :MerchantId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mfc.*\n            from    mif_cielo   mfc\n            where   mfc.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.startup.CieloFileLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1596^11*/
          resultSet = it.getResultSet();
          
          if ( resultSet.next() )
          {
            for( int i = 0; i < MifFields.length; ++i )
            {
              if ( MifFields[i][0].charAt(0) == '*' ) continue;
              if ( MifFields[i][1].charAt(0) == '*' ) continue; //@ skip?
              
              fname = MifFields[i][1];
              if ( fname.equals("merchant_number") ) continue;
              
              updateSql += (updateSql.length() > 0 ? "," : "") + MifFields[i][0] + " = " + encodeField(resultSet,fname);
            }
            
            //@System.out.println(updateSql);
            
            /*@lineinfo:generated-code*//*@lineinfo:1614^13*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//                set     :updateSql
//                where   merchant_number = :MerchantId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update  mif\n              set      ");
   __sjT_sb.append(updateSql);
   __sjT_sb.append(" \n              where   merchant_number =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "15com.mes.startup.CieloFileLoader:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1619^13*/
            
            /*@lineinfo:generated-code*//*@lineinfo:1621^13*/

//  ************************************************************
//  #sql [Ctx] { update  organization  o
//                set     o.org_name = (select dba_name from mif_cielo where merchant_number = :MerchantId)
//                where   o.org_group = :MerchantId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  organization  o\n              set     o.org_name = (select dba_name from mif_cielo where merchant_number =  :1 )\n              where   o.org_group =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   __sJT_st.setLong(2,MerchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1626^13*/              
          }
          resultSet.close();
          it.close();
        }
      }
      catch( Exception e )
      {
        logEntry("run(" + MerchantId + ")",e.toString());
        System.out.println(updateSql);//@
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ) {}
        cleanUp();
      }
    }
  }
  
  private boolean       ArchiveFile           = true;
  private HashMap       BatchIds              = new HashMap();
  private String        DefaultCurrencyCode   = "986";
  private HashMap       GroupIds              = new HashMap();
  private long          Group5Id              = 225000L;
  private long          Group6Id              = 125000L;
  
  public CieloFileLoader()
  {
    PropertiesFile    propsFile   = new PropertiesFile("cielo-file-loader.properties");
    
    ArchiveFile = propsFile.getBoolean("archive.file",true);
  }
  
  protected synchronized boolean addCieloHeader( String incomingFilename, String loadFilename, String line )
  {
    int     recCount  = 0;
    int     offset    = (loadFilename.startsWith("cddf_v") ? 36 : 38);
    String  fileId    = line.substring(0,offset).trim();
    String  complData = line.substring(offset,offset+1);
    boolean complFlag = " ".equals(complData);
    boolean retVal    = false;
  
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1670^7*/

//  ************************************************************
//  #sql [Ctx] { select count(1)
//          
//          from    daily_detail_file_cielo_fh
//          where   file_id = :fileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(1)\n         \n        from    daily_detail_file_cielo_fh\n        where   file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1676^7*/
      
      if ( recCount == 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1680^9*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_cielo_fh ( file_id ) values ( :fileId )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into daily_detail_file_cielo_fh ( file_id ) values (  :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1683^9*/  
      }
      
      if ( complFlag )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1688^9*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_cielo_fh
//            set     incoming_ts_compl       = sysdate,
//                    incoming_filename_compl = :incomingFilename,
//                    load_filename_compl     = :loadFilename,
//                    load_file_id_compl      = load_filename_to_load_file_id(:loadFilename),
//                    load_complete           = decode(incoming_filename_tran,null,'N','Y')
//            where   file_id = :fileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  daily_detail_file_cielo_fh\n          set     incoming_ts_compl       = sysdate,\n                  incoming_filename_compl =  :1 ,\n                  load_filename_compl     =  :2 ,\n                  load_file_id_compl      = load_filename_to_load_file_id( :3 ),\n                  load_complete           = decode(incoming_filename_tran,null,'N','Y')\n          where   file_id =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,incomingFilename);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setString(3,loadFilename);
   __sJT_st.setString(4,fileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1697^9*/
      }
      else    // transaction data
      {
        /*@lineinfo:generated-code*//*@lineinfo:1701^9*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_cielo_fh
//            set     complimentary_data      = decode(:complData,'S','Y',:complData),
//                    incoming_ts_tran        = sysdate,
//                    incoming_filename_tran  = :incomingFilename,
//                    load_filename           = :loadFilename,
//                    load_file_id            = load_filename_to_load_file_id(:loadFilename),
//                    load_complete           = decode(:complData,'N','Y',decode(incoming_filename_compl,null,'N','Y'))
//            where   file_id = :fileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  daily_detail_file_cielo_fh\n          set     complimentary_data      = decode( :1 ,'S','Y', :2 ),\n                  incoming_ts_tran        = sysdate,\n                  incoming_filename_tran  =  :3 ,\n                  load_filename           =  :4 ,\n                  load_file_id            = load_filename_to_load_file_id( :5 ),\n                  load_complete           = decode( :6 ,'N','Y',decode(incoming_filename_compl,null,'N','Y'))\n          where   file_id =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,complData);
   __sJT_st.setString(2,complData);
   __sJT_st.setString(3,incomingFilename);
   __sJT_st.setString(4,loadFilename);
   __sJT_st.setString(5,loadFilename);
   __sJT_st.setString(6,complData);
   __sJT_st.setString(7,fileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1711^9*/
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1714^7*/

//  ************************************************************
//  #sql [Ctx] { select count(1)
//          
//          from    daily_detail_file_cielo_fh
//          where   file_id = :fileId
//                  and load_complete = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(1)\n         \n        from    daily_detail_file_cielo_fh\n        where   file_id =  :1 \n                and load_complete = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1721^7*/
      retVal = (recCount > 0);
    }
    catch( Exception e )
    {
      logEntry("addCieloHeader(" + loadFilename + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  protected void addMbsBatches( String loadFilename )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1738^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_batches
//          (                      
//            bank_number,         
//            merchant_number,     
//            terminal_id,         
//            merchant_batch_date, 
//            batch_number,        
//            batch_id,            
//            trident_batch_id,    
//            mbs_batch_type,      
//            mesdb_timestamp,     
//            detail_timestamp_min,
//            detail_timestamp_max,
//            test_flag,           
//            response_code,       
//            sales_count,         
//            sales_amount,        
//            credits_count,       
//            credits_amount,      
//            total_count,         
//            net_amount,          
//            currency_code,       
//            load_file_id         
//          )             
//          select  8008                            as bank_number,
//                  dt.merchant_number              as merchant_number,
//                  dt.catid                        as catid,
//                  dt.batch_date                   as merchant_batch_date,
//                  --//@dt.batch_number                 as batch_number,
//                  1                               as batch_number,--//@
//                  dt.batch_id                     as batch_id,
//                  ( lpad(dt.merchant_number,12,'0') || lpad(dt.catid,8,'0')
//                    || '.' || to_char(dt.batch_date,'mmdd') 
//                    || '.' || '001'   --//@ batch number?
//                    || '-' || dt.batch_id)        as trident_batch_id,
//                  101                             as mbs_batch_type,                                          
//                  sysdate                         as mesdb_timestamp,
//                  min(dt.auth_time)               as detail_timestamp_min,
//                  max(dt.auth_time)               as detail_timestamp_max,
//                  'N'                             as test_flag,
//                  0                               as response_code,
//                  sum(decode(debit_credit_indicator,'D',1,0))
//                                                  as sales_count,
//                  sum(decode(debit_credit_indicator,'D',1,0)  
//                      * transaction_amount)       as sales_amount,
//                  sum(decode(debit_credit_indicator,'C',1,0))
//                                                  as credits_count,
//                  sum(decode(debit_credit_indicator,'C',1,0)  
//                      * transaction_amount)       as credits_amount,
//                  count(1)                        as total_count,
//                  sum(decode(debit_credit_indicator,'C',-1,1) 
//                      * transaction_amount)       as net_amount,
//                  dt.currency_code                as currency_code, 
//                  -1                              as load_file_id
//          from    daily_detail_file_cielo_dt    dt
//          where   dt.load_file_id = load_filename_to_load_file_id(:loadFilename)
//          group by dt.merchant_number,dt.catid,dt.batch_date,dt.batch_number,dt.batch_id,dt.currency_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_batches\n        (                      \n          bank_number,         \n          merchant_number,     \n          terminal_id,         \n          merchant_batch_date, \n          batch_number,        \n          batch_id,            \n          trident_batch_id,    \n          mbs_batch_type,      \n          mesdb_timestamp,     \n          detail_timestamp_min,\n          detail_timestamp_max,\n          test_flag,           \n          response_code,       \n          sales_count,         \n          sales_amount,        \n          credits_count,       \n          credits_amount,      \n          total_count,         \n          net_amount,          \n          currency_code,       \n          load_file_id         \n        )             \n        select  8008                            as bank_number,\n                dt.merchant_number              as merchant_number,\n                dt.catid                        as catid,\n                dt.batch_date                   as merchant_batch_date,\n                --//@dt.batch_number                 as batch_number,\n                1                               as batch_number,--//@\n                dt.batch_id                     as batch_id,\n                ( lpad(dt.merchant_number,12,'0') || lpad(dt.catid,8,'0')\n                  || '.' || to_char(dt.batch_date,'mmdd') \n                  || '.' || '001'   --//@ batch number:1\n                  || '-' || dt.batch_id)        as trident_batch_id,\n                101                             as mbs_batch_type,                                          \n                sysdate                         as mesdb_timestamp,\n                min(dt.auth_time)               as detail_timestamp_min,\n                max(dt.auth_time)               as detail_timestamp_max,\n                'N'                             as test_flag,\n                0                               as response_code,\n                sum(decode(debit_credit_indicator,'D',1,0))\n                                                as sales_count,\n                sum(decode(debit_credit_indicator,'D',1,0)  \n                    * transaction_amount)       as sales_amount,\n                sum(decode(debit_credit_indicator,'C',1,0))\n                                                as credits_count,\n                sum(decode(debit_credit_indicator,'C',1,0)  \n                    * transaction_amount)       as credits_amount,\n                count(1)                        as total_count,\n                sum(decode(debit_credit_indicator,'C',-1,1) \n                    * transaction_amount)       as net_amount,\n                dt.currency_code                as currency_code, \n                -1                              as load_file_id\n        from    daily_detail_file_cielo_dt    dt\n        where   dt.load_file_id = load_filename_to_load_file_id( :2 )\n        group by dt.merchant_number,dt.catid,dt.batch_date,dt.batch_number,dt.batch_id,dt.currency_code";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1797^7*/
    }
    catch( Exception e )
    {
      logEntry("addMbsBatches(" + loadFilename + ")",e.toString());
    }
    finally
    {
    }
  }
  
  public void addMidNames()
  {
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;
    int                   updateCount = 0;
    
    try
    {
      connect(true);
      
      /*@lineinfo:generated-code*//*@lineinfo:1818^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number
//          from    mif     mf
//          where   mf.bank_number = 8008
//                  and mf.dba_name = 'NAME - ' || mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number\n        from    mif     mf\n        where   mf.bank_number = 8008\n                and mf.dba_name = 'NAME - ' || mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.startup.CieloFileLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.startup.CieloFileLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1824^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        String  dbaName     = null;
        long    merchantId  = resultSet.getLong("merchant_number");
      
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1834^11*/

//  ************************************************************
//  #sql [Ctx] { select  trim(substr(raw_data,50,25))
//              
//              from    daily_detail_file_cielo_raw
//              where   merchant_number = :merchantId
//                      and block_type = 4
//                      and trim(substr(raw_data,50,25)) is not null
//                      and rownum <= 1
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trim(substr(raw_data,50,25))\n             \n            from    daily_detail_file_cielo_raw\n            where   merchant_number =  :1 \n                    and block_type = 4\n                    and trim(substr(raw_data,50,25)) is not null\n                    and rownum <= 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dbaName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1843^11*/
          
          if ( dbaName.startsWith("PARC=") )
          {
            dbaName = dbaName.substring(8);
          }
        }
        catch( java.sql.SQLException sqle )
        {
          continue;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:1855^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//            set     dba_name = :dbaName
//            where   merchant_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif\n          set     dba_name =  :1 \n          where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,dbaName);
   __sJT_st.setLong(2,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1860^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:1862^9*/

//  ************************************************************
//  #sql [Ctx] { update  organization  o
//            set     o.org_name = :dbaName
//            where   o.org_group = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  organization  o\n          set     o.org_name =  :1 \n          where   o.org_group =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,dbaName);
   __sJT_st.setLong(2,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1867^9*/
        System.out.print("Added " + merchantId + " - " + dbaName + "                                    \r");
      }
      resultSet.close();
      it.close();
      
      System.out.println();
    }
    catch(Exception e)
    {
      logEntry("addMidNames()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void addOrgNames()
  {
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;
    int                   updateCount = 0;
    
    try
    {
      connect(true);
      
      /*@lineinfo:generated-code*//*@lineinfo:1895^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group,
//                  decode(th.entity_type,4,'7',substr(o.org_group,5,1))   
//                                            as node_level,
//                  substr(o.org_group,5)     as sub_node
//          from    organization    o,
//                  t_hierarchy     th
//          where   o.org_group like '8008%'
//                  and o.org_name = 'Not Named'
//                  and th.hier_type = 1
//                  and th.ancestor = o.org_group
//                  and th.relation = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group,\n                decode(th.entity_type,4,'7',substr(o.org_group,5,1))   \n                                          as node_level,\n                substr(o.org_group,5)     as sub_node\n        from    organization    o,\n                t_hierarchy     th\n        where   o.org_group like '8008%'\n                and o.org_name = 'Not Named'\n                and th.hier_type = 1\n                and th.ancestor = o.org_group\n                and th.relation = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.startup.CieloFileLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"27com.mes.startup.CieloFileLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1908^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        int     level     = resultSet.getInt("node_level");
        String  subNode   = resultSet.getString("sub_node");
        String  orgName   = "";  
        long    orgGroup  = resultSet.getLong("org_group");
        String  nameCol   = null;
        String  nodeCol   = null;
        
        try
        {
          switch( level )
          {
            case 1  : nameCol = "group6_name"       ; nodeCol = "group6_id"   ; break;
            case 2  : nameCol = "group5_name"       ; nodeCol = "group5_id"   ; break;
            case 3  : nameCol = "group4_name"       ; nodeCol = "group4_id"   ; break;
            case 4  : nameCol = "group3_name"       ; nodeCol = "group3_id"   ; break; 
            case 5  : nameCol = "group2_name"       ; nodeCol = "group2_id"   ; break;
            case 6  : nameCol = "group1_name"       ; nodeCol = "group1_id"   ; break;
            case 7  : nameCol = "association_name"  ; nodeCol = "association" ; break;
            default : continue;
          }
          
          /*@lineinfo:generated-code*//*@lineinfo:1934^11*/

//  ************************************************************
//  #sql [Ctx] { select  distinct
//                      :nameCol
//                                          
//              from    mif_cielo_hierarchy mfh
//              where   :nodeCol = :subNode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  distinct\n                     ");
   __sjT_sb.append(nameCol);
   __sjT_sb.append(" \n                                         \n            from    mif_cielo_hierarchy mfh\n            where    ");
   __sjT_sb.append(nodeCol);
   __sjT_sb.append("  =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "28com.mes.startup.CieloFileLoader:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,subNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1941^11*/
        }
        catch( java.sql.SQLException sqle )
        {
          orgName = "Non-Chain";
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:1948^9*/

//  ************************************************************
//  #sql [Ctx] { update  group_names 
//            set     group_name   = :orgName
//            where   group_number = :orgGroup
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  group_names \n          set     group_name   =  :1 \n          where   group_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,orgName);
   __sJT_st.setLong(2,orgGroup);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1953^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:1955^9*/

//  ************************************************************
//  #sql [Ctx] { update  organization  o
//            set     o.org_name = :orgName
//            where   o.org_group = :orgGroup
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  organization  o\n          set     o.org_name =  :1 \n          where   o.org_group =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,orgName);
   __sJT_st.setLong(2,orgGroup);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1960^9*/
        System.out.print("Added " + orgGroup + " - " + orgName + "                                    \r");
      }
      resultSet.close();
      it.close();
      
      System.out.println();
    }
    catch(Exception e)
    {
      logEntry("addOrgNames()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void assign3xxxxxLevel()
  {
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;
    int                   updateCount = 0;
    
    try
    {
      connect(true);
      
      /*@lineinfo:generated-code*//*@lineinfo:1988^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (mfh IDX_MIF_CIELO_HIER_GROUP5_ID) */
//                  distinct group5_id
//          from    mif_cielo_hierarchy   mfh
//          where   length(group5_id) > 6
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (mfh IDX_MIF_CIELO_HIER_GROUP5_ID) */\n                distinct group5_id\n        from    mif_cielo_hierarchy   mfh\n        where   length(group5_id) > 6";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.startup.CieloFileLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.startup.CieloFileLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1994^7*/
      resultSet = it.getResultSet();
      
      long  group5Id  = 225000L;
      long  group6Id  = 125000L;
      int   recCount  = 0;
      
//@      #sql [Ctx]
//@      {
//@        select  max(group5_id)+1
//@        into    :group5Id
//@        from    mif_cielo_hierarchy
//@        where   length(group5_id) = 6      
//@      };
//@      group6Id = group5Id - 100000L;
      
      while( resultSet.next() )
      {
        do 
        {
          ++group5Id; ++group6Id;   // increment the ids until we find a non-existent one
          
          Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
          /*@lineinfo:generated-code*//*@lineinfo:2017^11*/

//  ************************************************************
//  #sql [Ctx] { select  /*+ index (mfh IDX_MIF_CIELO_HIER_GROUP5_ID) */
//                      count(1)
//                      
//              from    mif_cielo_hierarchy   mfh
//              where   mfh.group5_id = to_char(:group5Id)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  /*+ index (mfh IDX_MIF_CIELO_HIER_GROUP5_ID) */\n                    count(1)\n                     \n            from    mif_cielo_hierarchy   mfh\n            where   mfh.group5_id = to_char( :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,group5Id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2024^11*/
          Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
          long elapsed = (endTime.getTime() - beginTime.getTime());
          
          System.out.print("Scanning " + group5Id + " (" + elapsed + ")                \r");
        }
        while( recCount > 0 );
        
        /*@lineinfo:generated-code*//*@lineinfo:2032^9*/

//  ************************************************************
//  #sql [Ctx] { update  /*+ index (mfh idx_mif_cielo_hier_group5_id) */
//                    mif_cielo_hierarchy   mfh
//            set     group5_id = to_char(:group5Id),
//                    group6_id = to_char(:group6Id)
//            where   group5_id = :resultSet.getString("group5_id")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4044 = resultSet.getString("group5_id");
   String theSqlTS = "update  /*+ index (mfh idx_mif_cielo_hier_group5_id) */\n                  mif_cielo_hierarchy   mfh\n          set     group5_id = to_char( :1 ),\n                  group6_id = to_char( :2 )\n          where   group5_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"33com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,group5Id);
   __sJT_st.setLong(2,group6Id);
   __sJT_st.setString(3,__sJT_4044);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2039^9*/
        System.out.print("Updated " + ++updateCount + "                \r");
      }
      resultSet.close();
      it.close();
      
      System.out.println();
    }
    catch(Exception e)
    {
      logEntry("assign3xxxxxLevel()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  protected void buildDTRecords( String loadFilename )
  {
    ResultSetIterator       it              = null;
    ThreadPool              pool            = null;
    int                     recCount        = 0;
    ResultSet               resultSet       = null;
    
    try
    {
      connect(true);
      
      if ( loadFilename.charAt(5) == 'v' )    // visa 
      {
        /*@lineinfo:generated-code*//*@lineinfo:2070^9*/

//  ************************************************************
//  #sql [Ctx] { select  substr(fh.file_id,2,5) 
//            
//            from    daily_detail_file_cielo_fh    fh
//            where   fh.load_file_id = load_filename_to_load_file_id(:loadFilename)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  substr(fh.file_id,2,5) \n           \n          from    daily_detail_file_cielo_fh    fh\n          where   fh.load_file_id = load_filename_to_load_file_id( :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   DefaultCurrencyCode = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2076^9*/
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:2079^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (rw idx_ddf_cielo_raw_lfid) */
//                  distinct rw.transaction_key
//          from    daily_detail_file_cielo_raw   rw
//          where   rw.load_file_id = load_filename_to_load_file_id(:loadFilename)
//          order by rw.transaction_key
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (rw idx_ddf_cielo_raw_lfid) */\n                distinct rw.transaction_key\n        from    daily_detail_file_cielo_raw   rw\n        where   rw.load_file_id = load_filename_to_load_file_id( :1 )\n        order by rw.transaction_key";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"35com.mes.startup.CieloFileLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2086^7*/
      resultSet = it.getResultSet();
      
      pool = new ThreadPool(100);
      while( resultSet.next() )
      {
        Thread thread = pool.getThread( new DtRecordBuilder(resultSet.getString("transaction_key"),loadFilename) );
        thread.start();
        System.out.print("Building " + ++recCount + "            \r");
      }
      resultSet.close();
      it.close();
      
      pool.waitForAllThreads();
      System.out.println();
      
    }
    catch(Exception e)
    {
      logEntry("buildDTRecords(" + loadFilename + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    
  }
  
  public boolean execute( )
  {
    boolean           retVal            = false;
    Sftp              sftp              = null;
  
    try
    {
      connect(true);
      
      switch ( Integer.parseInt(getEventArg(0)) )
      {
        case PT_LOAD_BASE_I:
          loadBaseIFiles(getEventArg(1));
          break;
      }
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  private synchronized void fixGroupIds( FlatFileRecord ffd )
    throws Exception
  {
    String      delimiter   = "/";
    String      groupId     =   ffd.getFieldData("group4_id").trim() 
                              + delimiter 
                              + ffd.getFieldData("group5_id").trim();
    String      groupIdList = (String)GroupIds.get(groupId);
    
    if ( groupIdList == null )
    {
      int   recCount  = 0;
    
      do 
      {
        ++Group5Id; ++Group6Id;   // increment the ids until we find a non-existent one
    
        /*@lineinfo:generated-code*//*@lineinfo:2160^9*/

//  ************************************************************
//  #sql [Ctx] { select  /*+ index (mfh idx_mif_cielo_hier_group5_id) */
//                    count(1)
//                    
//            from    mif_cielo_hierarchy   mfh
//            where   mfh.group5_id = :Group5Id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  /*+ index (mfh idx_mif_cielo_hier_group5_id) */\n                  count(1)\n                   \n          from    mif_cielo_hierarchy   mfh\n          where   mfh.group5_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,Group5Id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2167^9*/
      }
      while( recCount > 0 );
      GroupIds.put(groupId,String.valueOf(Group5Id) + delimiter + String.valueOf(Group6Id));
      
      ffd.setFieldData("group5_id",String.valueOf(Group5Id));
      ffd.setFieldData("group6_id",String.valueOf(Group6Id));
    }
    else
    {
      String[] groupIds = groupIdList.split(delimiter); 
      ffd.setFieldData("group5_id",groupIds[0]);
      ffd.setFieldData("group6_id",groupIds[1]);
    }
  }
  
  private synchronized long getBatchId( String catId, Date batchDate, String currencyCode, String authCurrencyCode )
    throws java.sql.SQLException
  {
    long    batchId     = 0L;
    String  cc          = DefaultCurrencyCode;
    String  midKey      = (catId + "/" + DateTimeFormatter.getFormattedDate(batchDate,"yyMMdd"));
    String  batchIdStr  = (String)BatchIds.get(midKey);
    
    // if there is no currency code on the base transaction (eg Visa)
    // then try to use the authorization currency code.  if the 
    // authorization currency code is not available, then use the
    // default currency code from the file header.
    if ( currencyCode == null || "".equals(currencyCode.trim()) )
    {
      if ( authCurrencyCode != null && !"".equals(authCurrencyCode.trim()) )
      {
        cc = authCurrencyCode;
      }
    }
    else  // have a currency code from the transactions (mc)
    {
      cc = currencyCode;
    }
    midKey += ("/" + cc);
    
    if ( batchIdStr == null )
    {
      /*@lineinfo:generated-code*//*@lineinfo:2210^7*/

//  ************************************************************
//  #sql [Ctx] { select  trident_capture_test_sequence.nextval 
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trident_capture_test_sequence.nextval \n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   batchId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2215^7*/
      BatchIds.put(midKey,String.valueOf(batchId));
    }
    else
    {
      batchId = Long.parseLong(batchIdStr);
    }
    return( batchId );
  }
  
  private String getFilePrefix( String header )
  {
    String      retVal      = null;
    
    if ( "CREDITO".equals(header.substring(26,36).trim()) )
    {
      retVal = ((header.charAt(36) == ' ') ? "cddf_vsext" : "cddf_vs");
    }
    else if ( "ELECTRON".equals(header.substring(26,36).trim()) )
    {
      retVal = ((header.charAt(36) == ' ') ? "cddf_vdext" : "cddf_vd");
    }
    else if ( "MB-CREDITO".equals(header.substring(28,38).trim()) )
    {
      retVal = ((header.charAt(38) == ' ') ? "cddf_mcext" : "cddf_mc");
    }
    else if ( "MB-DEBITO".equals(header.substring(28,38).trim()) )
    {
      retVal = ((header.charAt(38) == ' ') ? "cddf_mdext" : "cddf_md");
    }
    return( retVal );
  }
  
  private boolean isExtData( String header )
  {
    boolean       retVal            = false;
    String        visaFileId        = header.substring(26,36).trim();
    String        multiBankFileId   = header.substring(28,38).trim();
    
    // check visa file tyeps first, then multi-bank file types
         if (     "CREDITO".equals(visaFileId) 
              || "ELECTRON".equals(visaFileId) )
    {
      retVal = (header.charAt(36) == ' ');
    }
    else if (    "MB-CREDITO".equals(multiBankFileId) 
             ||  "MB-DEBITO".equals(multiBankFileId) )
    {             
      retVal = (header.charAt(38) == ' ');
    }
    return( retVal );
  }
  
  public void loadMifProducts( String inputFilename )
  {
    BufferedReader  in            = null;
    String          line          = null;
    String          loadFilename  = inputFilename;
    ThreadPool      pool          = null;
    int             recCount      = 0;
  
    try
    {
      loadFilename = loadFilename.replace('\\','/');
      if ( loadFilename.indexOf("/") >= 0 )
      {
        loadFilename = loadFilename.substring(loadFilename.lastIndexOf("/")+1);
      }
      /*@lineinfo:generated-code*//*@lineinfo:2283^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1 ) \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2283^61*/
      
      in = new BufferedReader( new FileReader(inputFilename) );
    
      pool = new ThreadPool(100);
      while( (line = in.readLine()) != null )
      {
        if( "1".equals(line.substring(0,1)) )
        {
          if ( "TOT".equals(line.substring(10,13)) )
          {
            line = line.substring(0,59) + " ;" + line.substring(59);  //@ hack to fix offset problem
          }
          
          Thread thread = pool.getThread( new MifCieloProductsWorker(line,loadFilename) );
          thread.start();
          System.out.print("Loading " + ++recCount + "            \r");
        }
      }
      pool.waitForAllThreads();
      System.out.println();
    }
    catch( Exception e )
    {
      logEntry("loadMifProducts(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ in.close(); }catch( Exception ee ){}
    }
  }
  
  protected void loadBaseIFiles( String fileType )
  {
    BufferedReader    in                = null;
    String            incomingFilename  = null;
    String            line              = null;
    boolean           loadFile          = true;
    String            loadFilename      = null;
    Sftp              sftp              = null;

    try
    {
      //get the Sftp connection
      sftp = getSftp( MesDefaults.getString(MesDefaults.DK_VISA_HOST),      // host
                      MesDefaults.getString(MesDefaults.DK_VISA_USER),      // user
                      MesDefaults.getString(MesDefaults.DK_VISA_PASSWORD),  // password
                      "./CieloIN",      // path
                      true              // binary
                    );
                    
      String fileMask = fileType + ".*";
      EnumerationIterator enumi = new EnumerationIterator();
      Iterator it = enumi.iterator(sftp.getNameListing(fileMask));
      
      while( it.hasNext() )
      {
        incomingFilename  = (String)it.next();

        System.out.println("Downloading '" + incomingFilename + "'");//@
        sftp.download(incomingFilename,incomingFilename); // download file
        sftp.deleteFile(incomingFilename);                // remove from server
        
        in = new BufferedReader( new FileReader(incomingFilename) );
    
        while( (line = in.readLine()) != null )
        {
          if( "".equals(line.trim()) ) continue;    // skip blank lines
        
          line = StringUtilities.leftJustify(line,50,' '); // pad to min 50 characters
        
          if( "00".equals(line.substring(0,2)) )    // header
          {
            String          filePrefix  = getFilePrefix(line);
            int             dateOffset  = filePrefix.startsWith("cddf_v") ? 12 : 14;
            java.util.Date  fileDate    = DateTimeFormatter.parseDate(line.substring(dateOffset,dateOffset+14),"yyyyMMddHHmmss");
            loadFilename =  filePrefix + "8008"
                            + "_" + DateTimeFormatter.getFormattedDate(fileDate,"MMddyy") 
                            + "_" + DateTimeFormatter.getFormattedDate(fileDate,"HHmmss")
                            + ".dat";
            /*@lineinfo:generated-code*//*@lineinfo:2363^13*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1 ) \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2363^67*/
            
            //@+ hack to just archive files we have loaded
            int recCount = 0;
            /*@lineinfo:generated-code*//*@lineinfo:2367^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//                
//                from    daily_detail_file_cielo_fh
//                where   (incoming_filename_tran = :incomingFilename
//                         or incoming_filename_compl = :incomingFilename)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n               \n              from    daily_detail_file_cielo_fh\n              where   (incoming_filename_tran =  :1 \n                       or incoming_filename_compl =  :2 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"40com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,incomingFilename);
   __sJT_st.setString(2,incomingFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2374^13*/
            loadFile = (recCount == 0);
            //@- hack to just archive files we have loaded
            break;
          }
        }
        in.close();
        
        System.out.println("Renaming '" + incomingFilename + "' to '" + loadFilename + "'");//@
        File f = new File(incomingFilename);
        f.renameTo( new File(loadFilename) );
        
        //@+ hack to just archive files we have loaded
        if ( loadFile )
        {
          loadBaseIFile(loadFilename);
        }
        //@- hack to just archive files we have loaded
        //@loadBaseIFile(loadFilename);
        
        if ( ArchiveFile )
        {
          System.out.println("Archiving '" + loadFilename + "'");//@
          archiveDailyFile(loadFilename);
        }
        
        boolean buildDtRecs = addCieloHeader(incomingFilename,loadFilename,line);
        
        if ( buildDtRecs ) 
        {
          // use the base filename to build the DT records
          buildDTRecords(loadFilename.replaceAll("ext",""));
        }
      }
    }
    catch( Exception e )
    {
      logEntry("loadBaseIFiles(" + incomingFilename + ")",e.toString());
    }
    finally
    {
      try{ in.close(); } catch( Exception ee ){}
      try{ sftp.disconnect(); } catch( Exception ee ) {}
    }
  }
  
  protected void loadBaseIFile( String loadFilename )
  {
    String          baseFilename      = null;
  
    try
    {
      // compl data file, insure that the base file id is established
      baseFilename  = loadFilename.replaceAll("ext","");
      /*@lineinfo:generated-code*//*@lineinfo:2428^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:baseFilename)  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1 ) \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"41com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,baseFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2428^61*/
    
      System.out.println("loadFilename: " + loadFilename);//@
      System.out.println("baseFilename: " + baseFilename);//@
      
      if ( loadFilename.indexOf("ext") >= 0 )   // complimentary data
      {
        if ( baseFilename.startsWith("cddf_v") )
        {
          loadDdfExtVS(loadFilename,baseFilename);
        }
        else    // mastercard has binary data, requires special handling
        {          
          loadDdfExtMC(loadFilename,baseFilename);
        }
      }
      else    // base transaction data
      {
        loadDdf(loadFilename,baseFilename);
      }
    }
    catch( Exception e )
    {
      logEntry( "loadBaseIFile(" + loadFilename + ")",e.toString() );
    }
    finally
    {
    }
  }
  
  public void loadDdf( String inputFilename, String loadFilename )
  {
    BufferedReader  in                    = null;
    String          line                  = null;
    ThreadPool      pool                  = null;
    int             recCount              = 0;
    long            seqNum                = 0L;
  
    try
    {
      in = new BufferedReader( new FileReader(inputFilename) );
    
      pool = new ThreadPool(100);
      while( (line = in.readLine()) != null )
      {
        if( "".equals(line.trim()) ) continue;    // skip blank lines
        
        if( "01".equals(line.substring(0,2)) )    // detail
        {
          //@if ( line.charAt(138) != '1' ) continue;//@ installments (issuer)
          //@if ( line.charAt(138) != '2' ) continue;//@ installments (acquirer)
          //@if ( !line.substring(49,51).equals("41") || line.charAt(138) != '3' ) continue;//@ electron (pre-dated)
          //@if ( !line.startsWith("01A01") ) continue;//@ refund
          Thread thread = pool.getThread( new DdfWorker(++seqNum,line,loadFilename) );
          thread.start();
          System.out.print("Loading " + ++recCount + "            \r");
          //@if ( recCount == 185 ) break;//@
          //@break;
        }
      }
      pool.waitForAllThreads();
      System.out.println();
    }
    catch( Exception e )
    {
      logEntry("loadDdf(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ in.close(); }catch( Exception ee ){}
    }
  }

  public void loadDdfExtVS( String inputFilename, String loadFilename )
  {
    byte[]                bytes             = new byte[250];
    int                   bytesRead         = 0;
    BufferedInputStream   in                = null;
    String                lastTranKey       = "";
    String                line              = null;
    byte[]                lineSep           = new byte[1];
    ThreadPool            pool              = null;
    int                   recCount          = 0;
    long                  seqNum            = 0L;
    String                tranKey           = null;
  
    try
    {
      in = new BufferedInputStream( new FileInputStream(inputFilename) );
      
      while( (bytesRead = in.read(bytes, 0, 1)) > 0 )
      {
        if ( (char)bytes[0] == '\n' ) { break; }
      }
    
      pool = new ThreadPool(100);
      while( (bytesRead = in.read(bytes, 0, 250)) >= 0 )
      {
        if( bytesRead == 0 ) continue;    // skip blank lines
        
        in.read(lineSep,0,1);
        if ( lineSep[0] == '\r' )
        {
          in.read(lineSep,0,1);   // read the new line character
        }
        for( int i = 0; i < 250; ++i )
        {
          if ( bytes[i] == 0x00 )
          {
            bytes[i] = 0x20;      // replace nulls with spaces
          }
        }
        line = new String(bytes);
        
        if( line.startsWith("01") )   // 01 detail record
        {
          tranKey = (line.substring(2,46) + line.substring(199,202));
          
          if ( !tranKey.equals(lastTranKey) )
          {
            ++seqNum;
            lastTranKey = tranKey;
          }
          Thread thread = pool.getThread( new DdfExtDataWorker(seqNum,bytes,loadFilename) );
          thread.start();
          System.out.print("Loading " + ++recCount + "            \r");
          //@if ( recCount == 3 ) break;   //@
        }
      }
      pool.waitForAllThreads();
      System.out.println();
    }
    catch( Exception e )
    {
      logEntry("loadDdfExt(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ in.close(); }catch( Exception ee ){}
    }
  }
  
  public void loadDdfExtMC( String inputFilename, String loadFilename )
  {
    byte[]                bytes             = null;
    BinaryLineReader      in                = null;
    String                line              = null;
    byte[]                nextBytes         = null;
    String                nextLine          = null;
    ThreadPool            pool              = null;
    int                   recCount          = 0;
  
    try
    {
      in = new BinaryLineReader( new FileInputStream(inputFilename) );
      
      pool = new ThreadPool(100);
      while( (bytes = in.readLine()) != null )
      {
        line = new String(bytes);
        if ( line.length() < 2 ) { continue; }
        
        if( "01".equals(line.substring(0,2)) )    // detail
        {
          int blockType = Integer.parseInt(line.substring(75,78));
          if ( blockType == 13 || blockType == 14 )   // binary block types
          {
            while( (nextBytes = in.readLine()) != null )
            {
              nextLine = new String(nextBytes);
              
              //*************************************************************
              // HACK WARNING
              // test to see if the next group of bytes looks like a new record.
              // if it starts with "01" (dtl) or "99" (trl) and has a valid
              // 9-digit number following, assume it is the next record
              //*************************************************************
              String  recType   = "";

              //@long tranKey     = Long.parseLong(line.substring(2,11));
              long nextTranKey = 0L;
              try{ nextTranKey = Long.parseLong(nextLine.substring(2,11)); } catch( Exception ee ) {}
              try{ recType     = nextLine.substring(0,2);                  } catch( Exception ee ) {}
              if ( "99".equals(recType) && nextTranKey != 0L )
              {
                continue;   // skip trailer record
              }
              else if ( "01".equals(recType) && nextTranKey != 0L )
              {
                // the next bytes appear to be a new detail record so process
                // the first bytes via the worker thread
                Thread thread = pool.getThread( new DdfExtDataWorker(0L,bytes,loadFilename) );
                thread.start();
                System.out.print("Loading " + ++recCount + "            \r"); //@
                
                // copy nextBytes into bytes and place into line
                bytes = new byte[nextBytes.length];
                System.arraycopy(nextBytes,0,bytes,0,nextBytes.length);
                line = new String(bytes);
                
                // check the block type on the nextBytes line
                blockType = Integer.parseInt(line.substring(75,78));
                if ( blockType == 13 || blockType == 14 )   // binary
                {
                  continue; // continue the while loop handling of binary record
                }
                else  
                {
                  break;    // non-binary data, exit loop and process normally
                }
              }
              else    // more binary data to be added to previous read
              {
                // create a new byte array large enough to hold the current
                // bytes (line) plus a new-line plus the nextBytes (second line)
                byte[] newBytes = new byte[bytes.length+nextBytes.length+1];
                
                // copy the contents of the current bytes (line) into new buffer
                System.arraycopy(bytes,0,newBytes,0,bytes.length);
                
                // re-add the line feed to the first bytes (line)
                newBytes[bytes.length] = '\n';
                
                // add the nextBytes (second line) to the new buffer
                System.arraycopy(nextBytes,0,newBytes,bytes.length+1,nextBytes.length);
                
                // replace the current bytes with the new byte buffer
                bytes = newBytes;
                
                // store so we can check
                //@BufferedWriter out = new BufferedWriter(new FileWriter("sample.dat",true));
                //@out.write(ByteUtil.hexString(bytes).replaceAll("0x",""));
                //@out.close();
              }
            }
          }   // end if binary block type
          
          // process the current bytes (line)
          Thread thread = pool.getThread( new DdfExtDataWorker(0L,bytes,loadFilename) );
          thread.start();
          System.out.print("Loading " + ++recCount + "            \r"); //@
          //@if ( recCount == 100 ) break;//@
          //@break;
        }
      }
      pool.waitForAllThreads();
      System.out.println();
    }
    catch( Exception e )
    {
      logEntry("loadDdfExtMC(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ in.close(); }catch( Exception ee ){}
    }
  }
  
  public void loadMif( String inputFilename )
  {
    BufferedReader  in            = null;
    String          line          = null;
    String          loadFilename  = inputFilename;
    ThreadPool      pool          = null;
    int             recCount      = 0;
  
    try
    {
      loadFilename = loadFilename.replace('\\','/');
      if ( loadFilename.indexOf("/") >= 0 )
      {
        loadFilename = loadFilename.substring(loadFilename.lastIndexOf("/")+1);
      }
      /*@lineinfo:generated-code*//*@lineinfo:2701^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1 ) \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2701^61*/
      
      in = new BufferedReader( new FileReader(inputFilename) );
    
      pool = new ThreadPool(100);
      while( (line = in.readLine()) != null )
      {
        if( "".equals(line.trim()) ) continue;    // skip blank lines
        
        if( "1".equals(line.substring(0,1)) )
        {
          if ( "cmif8008_031313_001.dat".equals(loadFilename) )
          {
            line = line.substring(0,332) + "00000000000000" + line.substring(332+32);  //@ hack to fix md5 problem
          }
          
          //@ protect the 22 unmasked merchant accounts
          String the22 = MesDefaults.getString(MesDefaults.CIELO_UNMASKED_MERCHANT_ACCOUNTS);
        
          if ( the22.indexOf(line.substring(160,170)) >= 0 && !"cmif8008_031213_001.dat".equals(loadFilename) )
          {
            continue;
          }
          
          Thread thread = pool.getThread( new MifWorker(line,loadFilename) );
          thread.start();
          System.out.print("Loading " + ++recCount + "            \r");
          //@if ( !"1012345880".equals(line.substring(160,170)) ) continue;  //@
          //@System.out.println(line);//@
          //@break;
        }
      }
      pool.waitForAllThreads();
      System.out.println();
    }
    catch( Exception e )
    {
      logEntry("loadMif(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ in.close(); }catch( Exception ee ){}
    }
  }
  
  public void loadHierarchy( String inputFilename, boolean fixOnly )
  {
    BufferedReader  in            = null;
    String          line          = null;
    String          loadFilename  = inputFilename;
    ThreadPool      pool          = null;
    int             recCount      = 0;
  
    try
    {
      loadFilename = loadFilename.replace('\\','/');
      if ( loadFilename.indexOf("/") >= 0 )
      {
        loadFilename = loadFilename.substring(loadFilename.lastIndexOf("/")+1);
      }
      /*@lineinfo:generated-code*//*@lineinfo:2761^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)  };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1 ) \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"43com.mes.startup.CieloFileLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2761^61*/
      
      in = new BufferedReader( new FileReader(inputFilename) );
    
      pool = new ThreadPool(100);
      while( (line = in.readLine()) != null )
      {
        if( "".equals(line.trim()) ) continue;    // skip blank lines
        
        if ( fixOnly )
        {
          long  merchantId    = Long.parseLong(line.substring(429,439));
          
          /*@lineinfo:generated-code*//*@lineinfo:2774^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    mif_cielo_hierarchy_bad mfhb
//              where   mfhb.merchant_number = :merchantId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    mif_cielo_hierarchy_bad mfhb\n            where   mfhb.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"44com.mes.startup.CieloFileLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2780^11*/
          if ( recCount == 0 ) continue;
        }
        Thread thread = pool.getThread( new HierarchyWorker(line,loadFilename) );
        thread.start();
        System.out.print("Loading " + ++recCount + "            \r");
        //@if ( !"1012345880".equals(line.substring(160,170)) ) continue;  //@
        //@System.out.println(line);//@
        //@break;
      }
      pool.waitForAllThreads();
      System.out.println();
    }
    catch( Exception e )
    {
      logEntry("loadHierarchy(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ in.close(); }catch( Exception ee ){}
    }
  }
  
  public synchronized void storeDuplicate( String tranKey )
  {
    BufferedWriter      out     = null;
    
    try
    {
      out = new BufferedWriter( new FileWriter("cddf_dups.txt",true) );
      out.write(tranKey);
      out.newLine();
    }
    catch( Exception e )
    {
      logEntry("storeDuplicate(" + tranKey + ")",e.toString());
    }
    finally
    {
      try{ out.close(); } catch( Exception ee ) {}
    }
  }
  
  public void updateMif()
  {
    ResultSetIterator     it            = null;
    ThreadPool            pool          = null;
    int                   recCount      = 0;
    ResultSet             resultSet     = null;
    String                heldWhereClause       = "";
  
    try
    {
      heldWhereClause = MesDefaults.getString(MesDefaults.WHERE_CLAUSE_FOR_UPDATE_CIELO_MERCHANTS);
      /*@lineinfo:generated-code*//*@lineinfo:2834^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchant_number
//          from    mif_cielo   mfc
//          where   
//              :heldWhereClause                  
//          order by merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  merchant_number\n        from    mif_cielo   mfc\n        where   \n             ");
   __sjT_sb.append(heldWhereClause);
   __sjT_sb.append("                   \n        order by merchant_number");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "45com.mes.startup.CieloFileLoader:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2841^7*/
      resultSet = it.getResultSet();
    
      pool = new ThreadPool(100);
      while( resultSet.next() )
      {
        Thread thread = pool.getThread( new UpdateMifWorker(resultSet.getLong("merchant_number")) );
        thread.start();
        System.out.print("Loading " + ++recCount + "            \r");
        //@break;
      }
      pool.waitForAllThreads();
      System.out.println();
    }
    catch( Exception e )
    {
      logEntry("updateMif()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  public static void main(String[] args)
  {
    CieloFileLoader         testEvent   = null;

    try
    {
      SQLJConnectionBase.initStandalonePool("ERROR");
      
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      testEvent = new CieloFileLoader();
      testEvent.connect(true);

      if ( "execute".equals(args[0]) )
      {
        testEvent.setEventArgs(args[1]);
        testEvent.execute();
      }
      else if ( "archiveDailyFile".equals(args[0]) )
      {
        testEvent.archiveDailyFile(args[1]);
      }
      else if ( "buildDTRecords".equals(args[0]) )
      {
        testEvent.buildDTRecords(args[1]);
      }
      else if ( "loadMifProducts".equals(args[0]) )
      {
        testEvent.loadMifProducts( args[1] );
      }
      else if ( "loadBaseIFile".equals(args[0]) )
      {
        testEvent.loadBaseIFile( args[1] );
      }
      else if ( "loadBaseIFiles".equals(args[0]) )
      {
        testEvent.loadBaseIFiles( args[1] );
      }
//@      else if ( "loadDdf".equals(args[0]) )
//@      {
//@        testEvent.loadDdf( args[1],rawOnly );
//@      }
//@      else if ( "loadDdfExt".equals(args[0]) )
//@      {
//@        testEvent.loadDdfExt( args[1] );
//@      }
      else if ( "loadMif".equals(args[0]) )
      {
        testEvent.loadMif( args[1] );
      }
      else if ( "loadHierarchy".equals(args[0]) )
      {
        boolean fixOnly = (args.length > 2 ? "true".equals(args[2]) : false);
        testEvent.loadHierarchy( args[1], fixOnly );
      }
      else if ( "updateMif".equals(args[0]) )
      {
        testEvent.updateMif();
      }
      else if ( "assign3xxxxxLevel".equals(args[0]) )
      {
        testEvent.assign3xxxxxLevel();
      }
      else if ( "addOrgNames".equals(args[0]) )
      {
        testEvent.addOrgNames();
      }
      else if ( "addMidNames".equals(args[0]) )
      {
        testEvent.addMidNames();
      }
      else if ( "addMbsBatches".equals(args[0]) )
      {
        testEvent.addMbsBatches(args[1]);
      }
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    catch(Exception e)
    {
    }
    finally
    {
      try{ testEvent.cleanUp(); } catch(Exception e){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/