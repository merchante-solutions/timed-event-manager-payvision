/*@lineinfo:filename=ChargebackUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/ChargebackUpload.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.ach.AchEntryData;
import com.mes.ach.AchOriginList;
import com.mes.chargeback.utils.ChargeBackConstants;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.support.TridentTools;
import sqlj.runtime.ResultSetIterator;

public class ChargebackUpload extends TsysFileBase
{
  static Logger log = Logger.getLogger(ChargebackUpload.class);
  
  private static final int          IDX_NTCR3           = 0;
  private static final int          IDX_MRCH3           = 1;
  private static final int          IDX_COUNT           = 2;

  private static final int          TI_VISA_DEBIT       = 0;
  private static final int          TI_VISA_CREDIT      = 1;
  private static final int          TI_MC_DEBIT         = 2;
  private static final int          TI_MC_CREDIT        = 3;
  private static final int          TI_COUNT            = 4;
  
  private static boolean    EventRunning          = false;
  private double[]          FileBatchTotals       = new double[IDX_COUNT];
  private boolean[]         TestGrid              = new boolean[TI_COUNT];
  
  private final static int[] ActiveBanks = 
  {
    mesConstants.BANK_ID_MES,
    mesConstants.BANK_ID_CBT,
    mesConstants.BANK_ID_STERLING,
    mesConstants.BANK_ID_MES_WF
  };
  
  public ChargebackUpload( )
  {
    PropertiesFilename = "chargeback.properties";
    EmailGroupSuccess  = MesEmails.MSG_ADDRS_OUTGOING_CB_NOTIFY;
    EmailGroupFailure  = MesEmails.MSG_ADDRS_OUTGOING_CB_FAILURE;
  }
  
  private void buildAdjustmentDetails( String loadFilename, BufferedWriter out )
  {
    String                debitCreditInd  = null;
    FlatFileRecord        ffd             = null;
    ResultSetIterator     it              = null;
    String                plan            = null;
    ResultSet             resultSet       = null;
    StringBuffer          temp            = new StringBuffer();
    int                   tranCode        = 0;
    
    try
    {
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_MRCH3);
      ffd.connect(true);
    
      /*@lineinfo:generated-code*//*@lineinfo:97^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(cb.bank_number,
//                         3858,'0','') ||
//                  to_char(cb.merchant_number)         as merchant_number,
//                  cba.action_code                     as action_code,
//                  ac.adj_tran_code                    as tran_code,
//                  cba.action_date                     as tran_date,
//                  decode(nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                         'AM',cb.reference_number,
//                         substr(cb.reference_number,12,11)
//                        )                             as ref_num,
//                  -- extract as an unsigned integer value
//                  abs(cb.tran_amount)                 as tran_amount,
//                  cb.tran_amount                      as tran_amount_signed,
//                  decode(nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                         'VS',decode(mf.sic_code,6010,'VA',6011,'VA','VS'),
//                         'MC',decode(mf.sic_code,6010,'MA',6011,'MA','MC'),
//                         'AM','AX',
//                         nvl(cb.card_type,decode_card_type(cb.card_number,0))
//                         )                            as card_type,
//                  cb.card_number                      as card_number,                       
//                  'U'                                 as volume_indicator,
//                  cb.debit_credit_ind                 as debit_credit_ind                       
//          from    network_chargebacks           cb,
//                  network_chargeback_activity   cba,
//                  chargeback_action_codes       ac,
//                  mif                           mf
//          where   cb.bank_number = :FileBankNumber and
//                  cb.incoming_date between (:FileActivityDate-180) and :FileActivityDate and
//                  cb.merchant_number != 0 and
//                  not cb.card_number_enc is null and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.load_filename = :loadFilename and
//                  ac.short_action_code = cba.action_code and
//                  not ac.adj_tran_code is null and
//                  mf.merchant_number = cb.merchant_number
//                  and nvl(mf.processor_id,0) = 0  -- tsys back end only
//          order by  cb.merchant_number, cb.card_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(cb.bank_number,\n                       3858,'0','') ||\n                to_char(cb.merchant_number)         as merchant_number,\n                cba.action_code                     as action_code,\n                ac.adj_tran_code                    as tran_code,\n                cba.action_date                     as tran_date,\n                decode(nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                       'AM',cb.reference_number,\n                       substr(cb.reference_number,12,11)\n                      )                             as ref_num,\n                -- extract as an unsigned integer value\n                abs(cb.tran_amount)                 as tran_amount,\n                cb.tran_amount                      as tran_amount_signed,\n                decode(nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                       'VS',decode(mf.sic_code,6010,'VA',6011,'VA','VS'),\n                       'MC',decode(mf.sic_code,6010,'MA',6011,'MA','MC'),\n                       'AM','AX',\n                       nvl(cb.card_type,decode_card_type(cb.card_number,0))\n                       )                            as card_type,\n                cb.card_number                      as card_number,                       \n                'U'                                 as volume_indicator,\n                cb.debit_credit_ind                 as debit_credit_ind                       \n        from    network_chargebacks           cb,\n                network_chargeback_activity   cba,\n                chargeback_action_codes       ac,\n                mif                           mf\n        where   cb.bank_number =  :1   and\n                cb.incoming_date between ( :2  -180) and  :3   and\n                cb.merchant_number != 0 and\n                not cb.card_number_enc is null and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.load_filename =  :4   and\n                ac.short_action_code = cba.action_code and\n                not ac.adj_tran_code is null and\n                mf.merchant_number = cb.merchant_number\n                and nvl(mf.processor_id,0) = 0  -- tsys back end only\n        order by  cb.merchant_number, cb.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.ChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.ChargebackUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        ffd.resetAllFields();
        
        debitCreditInd = resultSet.getString("debit_credit_ind");
        tranCode       = resultSet.getInt("tran_code");
      
        if ( false ) //inTestMode() )
        {
          boolean checkVal        = false;
          
          if ( debitCreditInd.equals("C") )
          {
            checkVal = TestGrid[ (tranCode == 9077) ? TI_VISA_CREDIT : TI_MC_CREDIT ];
            TestGrid[ (tranCode == 9077) ? TI_VISA_CREDIT : TI_MC_CREDIT ] = true;
          }
          else
          {
            checkVal = TestGrid[ (tranCode == 9077) ? TI_VISA_DEBIT : TI_MC_DEBIT ];
            TestGrid[ (tranCode == 9077) ? TI_VISA_DEBIT : TI_MC_DEBIT ] = true;
          }
          
          if ( checkVal == true )   // already had one of these
          {
            continue;   // skip
          }
        } // end if inTestMode()
        
        if ( debitCreditInd.equals("C") )
        {
          BatchCreditsAmount += resultSet.getDouble("tran_amount");
          BatchCreditsCount++;
          FileCreditsAmount += resultSet.getDouble("tran_amount");
          FileCreditsCount++;
        }
        else
        {          
          BatchDebitsAmount  += resultSet.getDouble("tran_amount");
          BatchDebitsCount++;
          FileDebitsAmount  += resultSet.getDouble("tran_amount");
          FileDebitsCount++;
        }
        
        switch( tranCode )
        {
          case 9071:    // debit from the merchant account
            FileBatchTotals[IDX_MRCH3] -= resultSet.getDouble("tran_amount_signed");
            break;
            
          case 9077:    // credit to the merchant account
            FileBatchTotals[IDX_MRCH3] += resultSet.getDouble("tran_amount_signed");
            break;
        }
        
        // build the net deposit string
        ffd.setFieldData("net_deposit", TridentTools.encodeCobolAmount(resultSet.getDouble("tran_amount_signed"),(ffd.findFieldByName("net_deposit")).getLength()));
        
        // setup the sales/credits field for the merchant statements
        if ( debitCreditInd.equals("C") )
        {
          ffd.setFieldData("credits_count",1);
          ffd.setFieldData("credits_amount",resultSet.getDouble("tran_amount"));
        }
        else
        {
          ffd.setFieldData("sales_count",1);
          ffd.setFieldData("sales_amount",resultSet.getDouble("tran_amount"));
        }
        
        // build the common 1 record
        //System.out.print("Building record ADJ1             \r");//@
        ffd.setAllFieldData(resultSet);
        
        // Visa/MC are decoded in the query, all other 
        // card types use the card bin to determine card type
        plan = resultSet.getString("card_type");
        if ( plan.equals("ER") )
        {
          plan = TridentTools.decodeVisakCardType( resultSet.getString("card_number") );
        }
        ffd.setFieldData("plan_rep",plan);
        ffd.setFieldData("seq_num",++FileRecordCount);
        out.write( ffd.spew() );
        out.newLine();
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("buildAdjDetails()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      try { ffd.cleanUp(); } catch(Exception ee) {}
    }
  }
  
  private void buildAdjustmentHeader( String loadFilename, BufferedWriter out )
  {
    FlatFileRecord    ffd               = null;
    ResultSetIterator it                = null;
    ResultSet         resultSet         = null;
    
    try
    {
      // build the batch header
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_TSYS_BATCH_HDR);
      ffd.setFieldData("seq_num", ++FileRecordCount);
      ffd.setFieldData("bank_number",FileBankNumber);
      ffd.setFieldData("batch_format_ind","MRCH3");
      
      ffd.setFieldData("trans_date_jul",Calendar.getInstance().getTime());
      ffd.setFieldData("batch_number",1);
      /*@lineinfo:generated-code*//*@lineinfo:254^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  trunc(sysdate)-5                    as default_tran_date,
//                  substr(cb.reference_number,12,11)   as adj_ref_num
//          from    network_chargebacks           cb,
//                  network_chargeback_activity   cba
//          where   cb.bank_number = :FileBankNumber and
//                  cb.incoming_date between (:FileActivityDate-180) and :FileActivityDate and
//                  cb.merchant_number != 0 and
//                  not cb.card_number_enc is null and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.load_filename = :loadFilename
//          order by cb.card_number,  cb.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  trunc(sysdate)-5                    as default_tran_date,\n                substr(cb.reference_number,12,11)   as adj_ref_num\n        from    network_chargebacks           cb,\n                network_chargeback_activity   cba\n        where   cb.bank_number =  :1   and\n                cb.incoming_date between ( :2  -180) and  :3   and\n                cb.merchant_number != 0 and\n                not cb.card_number_enc is null and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.load_filename =  :4  \n        order by cb.card_number,  cb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.ChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.ChargebackUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:267^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      ffd.setFieldData("tran_date_override",resultSet.getDate("default_tran_date"));
      ffd.setFieldData("ref_num_begin",resultSet.getLong("adj_ref_num"));
      resultSet.close();
      it.close();
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildAdjustmentHeader()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  public void buildAdjustmentTrailer( BufferedWriter out )
  {
    FlatFileRecord      ffd     = null;
      
    try
    {
      // create the batch trailer record
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_TSYS_BATCH_TRL);
      ffd.setFieldData("seq_num", ++FileRecordCount);
      ffd.setFieldData("batch_format_ind","MRCH3");
      ffd.setFieldData("trans_date_jul",Calendar.getInstance().getTime());
      ffd.setFieldData("bank_number", FileBankNumber);
      ffd.setFieldData("batch_number",1);
      ffd.setFieldData("batch_amount_net", (BatchDebitsAmount - BatchCreditsAmount));
      ffd.setFieldData("debits_amount", BatchDebitsAmount);
      ffd.setFieldData("credits_amount", BatchCreditsAmount);
      ffd.setFieldData("payments_amount", BatchPaymentsAmount);
      ffd.setFieldData("debits_count", BatchDebitsCount);
      ffd.setFieldData("credits_count", BatchCreditsCount);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildAdjustmentTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  private void buildChargebackDetails( String loadFilename, BufferedWriter out )
  {
    String                cardType  = null;
    int                   extRecId  = 0;
    String                extRecStr = null;
    FlatFileRecord        ffd       = null;
    ResultSetIterator     it        = null;
    ResultSet             resultSet = null;
    
    try
    {
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_NTCR3_COMMON1);
      ffd.connect(true);
    
      /*@lineinfo:generated-code*//*@lineinfo:334^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  -- common 1 fields
//                  cb.bank_number                  as bank_number,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.transaction_code,
//                          'MC', cbm.transaction_code,
//                          null )                  as incoming_tran_code,
//                  repr.represent_tran_code        as tran_code,                        
//                  ( rpad(dukpt_decrypt_wrapper(cb.card_number_enc), 16, ' ') || 
//                    decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.locator_code,
//                          'MC', cbm.locator_code,
//                          null )
//                  )                               as card_number_loc_code,
//                  nvl(cb.card_type,decode_card_type(cb.card_number,0))
//                                                  as card_type,                                                
//                  cb.card_number                  as card_number,
//                  cb.tran_date                    as tran_date,  
//                  cb.reference_number             as ref_num,                              
//                  abs(cb.tran_amount)             as tran_amount,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.original_amt,
//                          'MC', cbm.original_amt,
//                          null )                  as source_amount,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.original_currency_code,
//                          'MC', cbm.original_currency_code,
//                          null )                  as source_currency_code,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.cardholder_act_term_ind,
//                          'MC', cbm.cardholder_act_term_ind,
//                          null )                  as cat_indicator,
//                  cb.auth_code                    as auth_code,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.trans_id,
//                          'MC', ( rpad(nvl(cbm.banknet_ref_num,' '),9,' ') || 
//                                 rpad(nvl(cbm.banknet_date,' '),4,' ') ),
//                          null )                  as tran_id,
//                  cb.debit_credit_ind             as debit_credit_ind,
//                  lpad(cba.repr_reason_code,4,'0')as reason_code,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.fee_program_ind,
//                          null )                  as fee_program_ind,
//                  -- COMM2 => common 2 fields
//                  decode(cb.bank_number,
//                         3858,'0','') ||
//                  to_char(cb.merchant_number)     as merchant_number,
//                  nvl(mf.dba_name,cb.merchant_name) as dba_name,
//                  mf.dmcity                       as dba_city,
//                  mf.dmstate                      as dba_state,
//                  mf.dmzip                        as dba_zip,
//                  mf.country_code_syf             as dba_country_code,
//                  mf.sic_code                     as sic_code,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.terminal_id,
//                          'MC', cbm.terminal_id,
//                          null )                  as terminal_id,
//                  -- VISA* => visa extension record
//                  to_char(cbv.central_processing_date,
//                          'yddd')                 as central_proc_date,
//                  cbv.excluded_transaction_id_reason
//                                                  as excl_tran_id_reason_code,
//                  cbv.authorization_source        as auth_source_code,
//                  cbv.pos_terminal_capablities    as pos_term_cap,
//                  cbv.terminal_entry_mode         as terminal_entry_mode,
//                  cbv.cardholder_id_method        as cardholder_id_method,
//                  cbv.multiple_clearing_seq_num   as multi_clear_seq_num,
//                  cbv.international_fee_ind       as intl_fee_ind,
//                  cbv.prepaid_card_ind            as prepaid_card_ind,
//                  cba.user_message                as visa_member_message_block,
//                  -- use the common cb ref # in network_chargebacks
//  --                cbv.chargeback_ref_num          as cb_ref_num,
//                  cb.cb_ref_num                   as cb_ref_num,
//                  cba.document_indicator          as visa_doc_ind,
//                  least((cbv.usage_code+1),3)     as visa_usage_code,
//                  cbv.mail_phone_order_ind        as moto_ind,
//                  cbv.special_chargeback_ind      as special_cb_ind,
//                  cbv.atm_account_selection_id    as atm_account_sel,
//                  cbv.acquirer_member_id          as acq_member_id,
//                  cbv.special_condition_ind       as special_cond_ind,
//                  cbv.reimbursement_attr_code     as reimburse_attr_code,
//                  cbv.request_payment_service     as req_payment_service_ind,
//                  cbv.addtional_data_ind          as addtl_data_ind,
//                  cbv.settlement_flag             as settlement_flag,
//                  cbv.merchant_volume_ind         as merchant_vol_ind,
//                  cbv.adjustment_processing_ind   as adj_proc_ind,
//                  cbv.cashback_amount             as cashback_amount,
//                  cbv.national_reimbursement_fee  as national_reimburse_fee,
//                  cbv.honduras_ic_reimburse_fee   as honduras_reimburse_fee,
//                  cbv.mex_ic_reimburse_fee_ind    as mex_reimburse_fee_ind,
//                  cbv.tran_code_qualifier         as tran_qual_code,
//                  cbv.fax_number                  as fax_number,
//                  -- MCEUR => mc extension record
//                  cba.function_code               as function_code,
//                  -- use the common cb ref # in network_chargebacks, see visa rec
//  --                cbm.cb_ref_num                  as cb_ref_num,                
//                  cba.document_indicator          as mc_doc_ind,
//                  cba.user_message                as mc_member_message_block,
//                  cbm.conversion_rate             as conversion_rate,
//                  cbm.processing_code             as processing_code,
//                  cbm.expiration_date             as expiration_date,
//                  cbm.conversion_date             as conversion_date,
//                  cbm.pos_data_code               as pos_data_code,
//                  cbm.acquirer_institution_id     as acq_institution_id,
//                  cbm.system_trace_audit_num      as sys_trace_audit_num,
//                  cbm.transaction_trace_id        as tran_trace_id,
//                  cbm.elec_image_proc_code        as elec_img_code_id,
//                  cbm.transaction_fee_rule        as tran_fee_rule,
//                  cbm.transaction_type            as tran_type,
//                  cbm.sender_pc_endpoint_code     as mastercom_from_id,
//                  cbm.mastercom_pc_endpoint       as mastercom_to_id,
//                  cbm.partial_amt_ind             as partial_amount_ind,
//                  least((cbm.usage_code+1),3)     as mc_usage_code,                
//                  cbm.business_activity_ind       as mc_bus_act_ind,
//                  -- EXT3* => client extension record
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.batch_ref_number,
//                          'MC', cbm.batch_ref_number,
//                          null )                  as batch_ref_num,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.net_deport_amt,
//                          'MC', cbm.net_deport_amt,
//                          null )                  as net_deposit_amount,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.credit_debit_ind,
//                          'MC', cbm.credit_debit_ind,
//                          null )                  as credit_debit_ind,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.orig_tran_date_of_chgbk,
//                          'MC', cbm.orig_tran_date_of_chgbk,
//                          null )                  as org_tran_date_of_cb,
//                  mf.dmagent                      as assoc_number,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.original_amt,
//                          'MC', cbm.original_amt,
//                          null )                  as original_amount,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.issuer_bin,
//                          'MC', cbm.issuer_bin,
//                          null )                  as issuer_bin,
//                  cba.action_code                 as action_code,
//                  cba.action_date                 as action_date,                
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.deposit_date,
//                          'MC', cbm.deposit_date,
//                          null )                  as deposit_date,
//                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                          'VS', cbv.case_number,
//                          'MC', cbm.case_number,
//                          null )                  as case_number                                                                                                                                                                                                                                                                                          
//          from    network_chargebacks           cb,
//                  network_chargeback_activity   cba,
//                  network_chargeback_visa       cbv,
//                  network_chargeback_mc         cbm,
//                  network_chargeback_repr_codes repr,
//                  chargeback_action_codes       ac,
//                  mif                           mf
//          where   cb.bank_number = :FileBankNumber and
//                  cb.incoming_date between (:FileActivityDate-180) and :FileActivityDate and
//                  cb.merchant_number != 0 and
//                  not cb.card_number_enc is null and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.load_filename = :loadFilename and
//                  ac.short_action_code = cba.action_code and
//                  nvl(ac.represent,'N') = 'Y' and  -- REPR, REMC only
//                  cbv.load_sec(+) = cb.cb_load_sec and
//                  cbm.load_sec(+) = cb.cb_load_sec and
//                  repr.transaction_code = decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),
//                                                  'VS', cbv.transaction_code,
//                                                  'MC', cbm.transaction_code,
//                                                  -1 ) and
//                  mf.merchant_number(+) = cb.merchant_number
//          order by cb.card_number,  cb.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  -- common 1 fields\n                cb.bank_number                  as bank_number,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.transaction_code,\n                        'MC', cbm.transaction_code,\n                        null )                  as incoming_tran_code,\n                repr.represent_tran_code        as tran_code,                        \n                ( rpad(dukpt_decrypt_wrapper(cb.card_number_enc), 16, ' ') || \n                  decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.locator_code,\n                        'MC', cbm.locator_code,\n                        null )\n                )                               as card_number_loc_code,\n                nvl(cb.card_type,decode_card_type(cb.card_number,0))\n                                                as card_type,                                                \n                cb.card_number                  as card_number,\n                cb.tran_date                    as tran_date,  \n                cb.reference_number             as ref_num,                              \n                abs(cb.tran_amount)             as tran_amount,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.original_amt,\n                        'MC', cbm.original_amt,\n                        null )                  as source_amount,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.original_currency_code,\n                        'MC', cbm.original_currency_code,\n                        null )                  as source_currency_code,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.cardholder_act_term_ind,\n                        'MC', cbm.cardholder_act_term_ind,\n                        null )                  as cat_indicator,\n                cb.auth_code                    as auth_code,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.trans_id,\n                        'MC', ( rpad(nvl(cbm.banknet_ref_num,' '),9,' ') || \n                               rpad(nvl(cbm.banknet_date,' '),4,' ') ),\n                        null )                  as tran_id,\n                cb.debit_credit_ind             as debit_credit_ind,\n                lpad(cba.repr_reason_code,4,'0')as reason_code,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.fee_program_ind,\n                        null )                  as fee_program_ind,\n                -- COMM2 => common 2 fields\n                decode(cb.bank_number,\n                       3858,'0','') ||\n                to_char(cb.merchant_number)     as merchant_number,\n                nvl(mf.dba_name,cb.merchant_name) as dba_name,\n                mf.dmcity                       as dba_city,\n                mf.dmstate                      as dba_state,\n                mf.dmzip                        as dba_zip,\n                mf.country_code_syf             as dba_country_code,\n                mf.sic_code                     as sic_code,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.terminal_id,\n                        'MC', cbm.terminal_id,\n                        null )                  as terminal_id,\n                -- VISA* => visa extension record\n                to_char(cbv.central_processing_date,\n                        'yddd')                 as central_proc_date,\n                cbv.excluded_transaction_id_reason\n                                                as excl_tran_id_reason_code,\n                cbv.authorization_source        as auth_source_code,\n                cbv.pos_terminal_capablities    as pos_term_cap,\n                cbv.terminal_entry_mode         as terminal_entry_mode,\n                cbv.cardholder_id_method        as cardholder_id_method,\n                cbv.multiple_clearing_seq_num   as multi_clear_seq_num,\n                cbv.international_fee_ind       as intl_fee_ind,\n                cbv.prepaid_card_ind            as prepaid_card_ind,\n                cba.user_message                as visa_member_message_block,\n                -- use the common cb ref # in network_chargebacks\n--                cbv.chargeback_ref_num          as cb_ref_num,\n                cb.cb_ref_num                   as cb_ref_num,\n                cba.document_indicator          as visa_doc_ind,\n                least((cbv.usage_code+1),3)     as visa_usage_code,\n                cbv.mail_phone_order_ind        as moto_ind,\n                cbv.special_chargeback_ind      as special_cb_ind,\n                cbv.atm_account_selection_id    as atm_account_sel,\n                cbv.acquirer_member_id          as acq_member_id,\n                cbv.special_condition_ind       as special_cond_ind,\n                cbv.reimbursement_attr_code     as reimburse_attr_code,\n                cbv.request_payment_service     as req_payment_service_ind,\n                cbv.addtional_data_ind          as addtl_data_ind,\n                cbv.settlement_flag             as settlement_flag,\n                cbv.merchant_volume_ind         as merchant_vol_ind,\n                cbv.adjustment_processing_ind   as adj_proc_ind,\n                cbv.cashback_amount             as cashback_amount,\n                cbv.national_reimbursement_fee  as national_reimburse_fee,\n                cbv.honduras_ic_reimburse_fee   as honduras_reimburse_fee,\n                cbv.mex_ic_reimburse_fee_ind    as mex_reimburse_fee_ind,\n                cbv.tran_code_qualifier         as tran_qual_code,\n                cbv.fax_number                  as fax_number,\n                -- MCEUR => mc extension record\n                cba.function_code               as function_code,\n                -- use the common cb ref # in network_chargebacks, see visa rec\n--                cbm.cb_ref_num                  as cb_ref_num,                \n                cba.document_indicator          as mc_doc_ind,\n                cba.user_message                as mc_member_message_block,\n                cbm.conversion_rate             as conversion_rate,\n                cbm.processing_code             as processing_code,\n                cbm.expiration_date             as expiration_date,\n                cbm.conversion_date             as conversion_date,\n                cbm.pos_data_code               as pos_data_code,\n                cbm.acquirer_institution_id     as acq_institution_id,\n                cbm.system_trace_audit_num      as sys_trace_audit_num,\n                cbm.transaction_trace_id        as tran_trace_id,\n                cbm.elec_image_proc_code        as elec_img_code_id,\n                cbm.transaction_fee_rule        as tran_fee_rule,\n                cbm.transaction_type            as tran_type,\n                cbm.sender_pc_endpoint_code     as mastercom_from_id,\n                cbm.mastercom_pc_endpoint       as mastercom_to_id,\n                cbm.partial_amt_ind             as partial_amount_ind,\n                least((cbm.usage_code+1),3)     as mc_usage_code,                \n                cbm.business_activity_ind       as mc_bus_act_ind,\n                -- EXT3* => client extension record\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.batch_ref_number,\n                        'MC', cbm.batch_ref_number,\n                        null )                  as batch_ref_num,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.net_deport_amt,\n                        'MC', cbm.net_deport_amt,\n                        null )                  as net_deposit_amount,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.credit_debit_ind,\n                        'MC', cbm.credit_debit_ind,\n                        null )                  as credit_debit_ind,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.orig_tran_date_of_chgbk,\n                        'MC', cbm.orig_tran_date_of_chgbk,\n                        null )                  as org_tran_date_of_cb,\n                mf.dmagent                      as assoc_number,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.original_amt,\n                        'MC', cbm.original_amt,\n                        null )                  as original_amount,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.issuer_bin,\n                        'MC', cbm.issuer_bin,\n                        null )                  as issuer_bin,\n                cba.action_code                 as action_code,\n                cba.action_date                 as action_date,                \n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.deposit_date,\n                        'MC', cbm.deposit_date,\n                        null )                  as deposit_date,\n                decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                        'VS', cbv.case_number,\n                        'MC', cbm.case_number,\n                        null )                  as case_number                                                                                                                                                                                                                                                                                          \n        from    network_chargebacks           cb,\n                network_chargeback_activity   cba,\n                network_chargeback_visa       cbv,\n                network_chargeback_mc         cbm,\n                network_chargeback_repr_codes repr,\n                chargeback_action_codes       ac,\n                mif                           mf\n        where   cb.bank_number =  :1   and\n                cb.incoming_date between ( :2  -180) and  :3   and\n                cb.merchant_number != 0 and\n                not cb.card_number_enc is null and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.load_filename =  :4   and\n                ac.short_action_code = cba.action_code and\n                nvl(ac.represent,'N') = 'Y' and  -- REPR, REMC only\n                cbv.load_sec(+) = cb.cb_load_sec and\n                cbm.load_sec(+) = cb.cb_load_sec and\n                repr.transaction_code = decode( nvl(cb.card_type,decode_card_type(cb.card_number,0)),\n                                                'VS', cbv.transaction_code,\n                                                'MC', cbm.transaction_code,\n                                                -1 ) and\n                mf.merchant_number(+) = cb.merchant_number\n        order by cb.card_number,  cb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.ChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.ChargebackUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:508^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        cardType = resultSet.getString("card_type");
        
        if ( cardType.equals("VS") )
        {
          extRecStr   = "VISA*";
          extRecId    = MesFlatFiles.DEF_TYPE_NTCR3_VISA;
        }
        else if ( cardType.equals("MC") )
        {
          extRecStr   = "MCEUR";
          extRecId    = MesFlatFiles.DEF_TYPE_NTCR3_MCEUR;
        }
        else
        {    
          continue; // only support Visa and MC
        }
        
        if ( false ) //inTestMode() )
        {
          boolean checkVal        = false;
          String  debitCreditInd  = resultSet.getString("debit_credit_ind");
          
          if ( debitCreditInd.equals("C") )
          {
            checkVal = TestGrid[ cardType.equals("VS") ? TI_VISA_CREDIT : TI_MC_CREDIT ];
            TestGrid[ cardType.equals("VS") ? TI_VISA_CREDIT : TI_MC_CREDIT ] = true;
          }
          else
          {
            checkVal = TestGrid[ cardType.equals("VS") ? TI_VISA_DEBIT : TI_MC_DEBIT ];
            TestGrid[ cardType.equals("VS") ? TI_VISA_DEBIT : TI_MC_DEBIT ] = true;
          }
          
          if ( checkVal == true )   // already had one of these
          {
            continue;   // skip
          }
        }
        
        // build the common 1 record
        //System.out.print("Building record COMMON1             \r");//@
        ffd.setDefType(MesFlatFiles.DEF_TYPE_NTCR3_COMMON1);
        ffd.setAllFieldData(resultSet);
        ffd.setFieldData("seq_num",++FileRecordCount);
        out.write( ffd.spew() );
        out.newLine();
        
        // build the common 2 record
        //System.out.print("Building record COMMON2             \r");//@
        ffd.setDefType(MesFlatFiles.DEF_TYPE_NTCR3_COMMON2);
        ffd.setAllFieldData(resultSet);
        ffd.setFieldData("ext_record_ind", extRecStr );
        ffd.setFieldData("seq_num",++FileRecordCount);
        out.write( ffd.spew() );
        out.newLine();
        
        // build the card assoc record
        //System.out.print("Building record " + extRecStr + "         \r");//@
        ffd.setDefType( extRecId );
        ffd.setAllFieldData(resultSet);
        ffd.setFieldData("ext_record_ind", extRecStr );
        ffd.setFieldData("seq_num",++FileRecordCount);
        out.write( ffd.spew() );
        out.newLine();
        
/*        
        // build the client ext 3 record
        System.out.print("Building record EXT3*               \r");//@
        ffd.setDefType( MesFlatFiles.DEF_TYPE_NTCR3_EXT3 );
        ffd.setAllFieldData(resultSet);
        ffd.setFieldData("seq_num",++FileRecordCount);
        out.write( ffd.spew() );
        out.newLine();
*/        
        
        if ( resultSet.getString("debit_credit_ind").equals("C") )
        {
          BatchCreditsAmount += resultSet.getDouble("tran_amount");
          BatchCreditsCount++;
          FileCreditsAmount += resultSet.getDouble("tran_amount");
          FileCreditsCount++;
        }
        else
        {          
          BatchDebitsAmount  += resultSet.getDouble("tran_amount");
          BatchDebitsCount++;
          FileDebitsAmount  += resultSet.getDouble("tran_amount");
          FileDebitsCount++;
        }
        
        FileBatchTotals[IDX_NTCR3] += resultSet.getDouble("tran_amount");
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("buildDetails()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      try { ffd.cleanUp(); } catch(Exception ee) {}
    }
  }
  
  private void buildChargebackHeader( String loadFilename, BufferedWriter out )
  {
    FlatFileRecord    ffd               = null;
    ResultSetIterator it                = null;
    ResultSet         resultSet         = null;
    
    try
    {
      // build the batch header
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_TSYS_BATCH_HDR);
      ffd.setFieldData("seq_num", ++FileRecordCount);
      ffd.setFieldData("batch_format_ind","NTCR3");
      ffd.setFieldData("bank_number",FileBankNumber);
      ffd.setFieldData("trans_date_jul",Calendar.getInstance().getTime());
      ffd.setFieldData("batch_number",1);
      /*@lineinfo:generated-code*//*@lineinfo:634^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  trunc(sysdate)-5                    as default_tran_date,
//                  substr(cb.reference_number,12,11)   as adj_ref_num
//          from    network_chargebacks           cb,
//                  network_chargeback_activity   cba
//          where   cb.bank_number = :FileBankNumber and
//                  cb.incoming_date between (:FileActivityDate-180) and :FileActivityDate and
//                  cb.merchant_number != 0 and
//                  not cb.card_number_enc is null and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.load_filename = :loadFilename
//          order by cb.card_number,  cb.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  trunc(sysdate)-5                    as default_tran_date,\n                substr(cb.reference_number,12,11)   as adj_ref_num\n        from    network_chargebacks           cb,\n                network_chargeback_activity   cba\n        where   cb.bank_number =  :1   and\n                cb.incoming_date between ( :2  -180) and  :3   and\n                cb.merchant_number != 0 and\n                not cb.card_number_enc is null and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.load_filename =  :4  \n        order by cb.card_number,  cb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.ChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.ChargebackUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:647^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      ffd.setFieldData("tran_date_override",resultSet.getDate("default_tran_date"));
      ffd.setFieldData("ref_num_begin",resultSet.getLong("adj_ref_num"));
      resultSet.close();
      it.close();
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildChargebackHeader()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  public void buildChargebackTrailer( BufferedWriter out )
  {
    FlatFileRecord      ffd     = null;
      
    try
    {
      // create the batch trailer record
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_TSYS_BATCH_TRL);
      ffd.setFieldData("seq_num", ++FileRecordCount);
      ffd.setFieldData("batch_format_ind","NTCR3");
      ffd.setFieldData("trans_date_jul",Calendar.getInstance().getTime());
      ffd.setFieldData("bank_number", FileBankNumber);
      ffd.setFieldData("batch_number",1);
      ffd.setFieldData("batch_amount_net", (BatchDebitsAmount - BatchCreditsAmount));
      ffd.setFieldData("debits_amount", BatchDebitsAmount);
      ffd.setFieldData("credits_amount", BatchCreditsAmount);
      ffd.setFieldData("payments_amount", BatchPaymentsAmount);
      ffd.setFieldData("debits_count", BatchDebitsCount);
      ffd.setFieldData("credits_count", BatchCreditsCount);
      out.write( ffd.spew() );
      out.newLine();
    }
    catch(Exception e)
    {
      logEntry("buildChargebackTrailer()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    StringBuffer        buffer          = new StringBuffer();
    String              dateString      = null;
    String              fileType        = null;
    BufferedWriter      out             = null;
    int                 rowCount        = 0;
    String              workFilename    = null;
    
    if ( EventRunning == false )
    {
      try
      {
        EventRunning = true;
  
        // get an automated timeout exempt connection
        connect(true);
  
        for( int i = 0; i < ActiveBanks.length; ++i )
        {
          FileBankNumber = ActiveBanks[i];
    
          // reset the file counters
          FileCreditsAmount     = 0.0;
          FileDebitsAmount      = 0.0;
          FilePaymentsAmount    = 0.0;
          FileRecordCount       = 0;
          FileCreditsCount      = 0;
          FileDebitsCount       = 0;
    
          if ( TestFilename == null )
          {
            /*@lineinfo:generated-code*//*@lineinfo:737^13*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate) 
//                from    dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate)  \n              from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.ChargebackUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   FileActivityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:741^13*/
    
            // setup a temp filename
            dateString = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MMddyy");
            buffer.setLength(0);
            fileType = ("wcb" + String.valueOf(FileBankNumber));
            buffer.append(fileType);
            buffer.append("_");
            buffer.append(dateString);
            buffer.append("_");
            buffer.append( NumberFormatter.getPaddedInt( getFileId(fileType,dateString), 3 ) );
            buffer.append(".dat");
    
            workFilename = buffer.toString();
  
            /*@lineinfo:generated-code*//*@lineinfo:756^13*/

//  ************************************************************
//  #sql [Ctx] { update network_chargeback_activity cba
//                set     cba.load_filename = :workFilename
//                where   cba.cb_load_sec in
//                        (
//                          select  cb.cb_load_sec 
//                          from    network_chargebacks           cb,
//                                  mbs_banks                     mb
//                          where   cb.bank_number = :FileBankNumber 
//                                  and cb.incoming_date between (:FileActivityDate-180) and :FileActivityDate 
//                                  and cb.merchant_number != 0 
//                                  and not cb.card_number_enc is null
//                                  and mb.bank_number = cb.bank_number
//                                  and mb.processor_id = 0
//                        ) 
//                        and cba.load_filename is null
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update network_chargeback_activity cba\n              set     cba.load_filename =  :1  \n              where   cba.cb_load_sec in\n                      (\n                        select  cb.cb_load_sec \n                        from    network_chargebacks           cb,\n                                mbs_banks                     mb\n                        where   cb.bank_number =  :2   \n                                and cb.incoming_date between ( :3  -180) and  :4   \n                                and cb.merchant_number != 0 \n                                and not cb.card_number_enc is null\n                                and mb.bank_number = cb.bank_number\n                                and mb.processor_id = 0\n                      ) \n                      and cba.load_filename is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.ChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setInt(2,FileBankNumber);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setDate(4,FileActivityDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:773^13*/
          }          
          else  // allow for test files to be specified
          {
            workFilename    = TestFilename;
            FileBankNumber  = getFileBankNumber(TestFilename);
    
            /*@lineinfo:generated-code*//*@lineinfo:780^13*/

//  ************************************************************
//  #sql [Ctx] { select  max(cba.action_date) 
//                from    network_chargeback_activity     cba
//                where   cba.load_filename = :workFilename
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(cba.action_date)  \n              from    network_chargeback_activity     cba\n              where   cba.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.ChargebackUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   FileActivityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:785^13*/
          }
    
          // check for actions that would cause an NTCR3 or an MRCH3
          // batch to be generated and sent to TSYS.
          /*@lineinfo:generated-code*//*@lineinfo:790^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(cb.merchant_number) 
//              from    network_chargebacks           cb,
//                      network_chargeback_activity   cba,
//                      chargeback_action_codes       ac
//              where   cb.bank_number = :FileBankNumber and
//                      cb.incoming_date between (:FileActivityDate-180) and :FileActivityDate and
//                      cb.merchant_number != 0 and
//                      not cb.card_number_enc is null and
//                      cba.cb_load_sec = cb.cb_load_sec and
//                      cba.load_filename = :workFilename and
//                      ac.short_action_code = cba.action_code and
//                      (
//                        -- if this items is requires a MRCH3 
//                        -- or an NTCR3 record then count it
//                        not ac.adj_tran_code is null or
//                        nvl(ac.represent,'N') = 'Y'
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cb.merchant_number)  \n            from    network_chargebacks           cb,\n                    network_chargeback_activity   cba,\n                    chargeback_action_codes       ac\n            where   cb.bank_number =  :1   and\n                    cb.incoming_date between ( :2  -180) and  :3   and\n                    cb.merchant_number != 0 and\n                    not cb.card_number_enc is null and\n                    cba.cb_load_sec = cb.cb_load_sec and\n                    cba.load_filename =  :4   and\n                    ac.short_action_code = cba.action_code and\n                    (\n                      -- if this items is requires a MRCH3 \n                      -- or an NTCR3 record then count it\n                      not ac.adj_tran_code is null or\n                      nvl(ac.represent,'N') = 'Y'\n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.ChargebackUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:809^11*/
  
          log.debug("Bank Number   : " + FileBankNumber);
          log.debug("Activity Date : " + DateTimeFormatter.getFormattedDate(FileActivityDate,"MM/dd/yyyy"));
          log.debug("Work Filename : " + workFilename);
  
          // open the data file
          out = new BufferedWriter( new FileWriter( workFilename, false ) );
          
          // reset the file batch totals
          for( int idx = 0; idx < FileBatchTotals.length; ++idx )
          {
            FileBatchTotals[idx] = 0.0;
          }
    
          // build the transmission header for a worked chargeback file
          buildTransmissionHeader(out, ("B" + FileBankNumber + "WCB"), (FileBankNumber + "CR") );
          
          // this is done for all banks because representments have to be processed through TSYS
          processNTCR3(workFilename,out);    // build the NTCR3 batch
          
          // this step is different for banks for which we do the funding
          if( isTsysBank(FileBankNumber) )
          {
            processMRCH3(workFilename,out);    // build the MRCH3 batch
            
            // queue an entry in the MES ach process table to allow
            // for trailing chargeback activity to be processed by
            // MES once the account is converted to the MES backend
            /*@lineinfo:generated-code*//*@lineinfo:838^13*/

//  ************************************************************
//  #sql [Ctx] { insert into ach_trident_process
//                (
//                  process_type, process_sequence, load_filename
//                )
//                values
//                (
//                  :AchEvent.PT_CB_ADJ, 0, :workFilename
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into ach_trident_process\n              (\n                process_type, process_sequence, load_filename\n              )\n              values\n              (\n                 :1  , 0,  :2  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.ChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,ChargeBackConstants.PT_CB_ADJ.getIntValue());
   __sJT_st.setString(2,workFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:848^13*/
            
            // queue an entry in the mbs_process table to 
            // handle the loading of the billing summary data
            /*@lineinfo:generated-code*//*@lineinfo:852^13*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_process
//                (
//                  process_type, process_sequence, load_filename
//                )
//                values
//                (
//                  :DailyBillingSummary.PROC_TYPE_CB_FILE, 0, :workFilename
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_process\n              (\n                process_type, process_sequence, load_filename\n              )\n              values\n              (\n                 :1  , 0,  :2  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.ChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DailyBillingSummary.PROC_TYPE_CB_FILE);
   __sJT_st.setString(2,workFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:862^13*/
          }
          else
          {
            processMESChargebacks(workFilename);
          }
          buildTransmissionTrailer(out);
    
          out.close();          // close the file buffer
    
          if ( TestFilename == null ) // only send non-test files
          {
            // ftp the file to the outgoing directory
            sendFile(workFilename,
                     MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_HOST),
                     MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_USER),
                     MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_PASSWORD));
          }
          else
          {
            log.debug("\n\n");
            log.debug("Test Filename '" + TestFilename + "' complete (no transmit)");
          }
    
          if ( TestFilename != null )
          {
            // exit the loop through active banks.
            break;
          }
        }        
      }
      catch( Exception e )
      {
        logEvent(this.getClass().getName(), "execute()", e.toString());
        logEntry("execute()", e.toString());
      }
      finally
      {
        cleanUp();
        EventRunning = false;
      }
    }
    
    return( true );
  }
  
  protected boolean isTsysBank(int bankNumber)
  {
    boolean result = true;

    try
    {
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:915^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(bank_number)
//          
//          from    tsys_bank_numbers
//          where   bank_number = :bankNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(bank_number)\n         \n        from    tsys_bank_numbers\n        where   bank_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.ChargebackUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:921^7*/
      result = ( recCount > 0 );
    }
    catch(Exception e)
    {
      logEntry("isTsysBank(" + bankNumber + ")", e.toString());
    }
    
    return( result );
  }
  
  protected void processMESChargebacks( String workFilename )
  {
    AchEntryData        achData         = null;
    Vector              achEntries      = null;
    AchOriginList       achOriginList   = null;
    long                achSequence     = 0L;
    long                originNode      = 0L;
    long                traceNumber     = 0L;
    ResultSetIterator   it = null;
    ResultSet           rs = null;
    
    try
    {
      achOriginList = new AchOriginList();
      achEntries = new Vector();
      
      if ( inTestMode() )
      {
        /*@lineinfo:generated-code*//*@lineinfo:950^9*/

//  ************************************************************
//  #sql [Ctx] { select  max(ach.ach_sequence)   
//            from    ach_trident_detail    ach
//            where   ach.load_filename = :workFilename
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(ach.ach_sequence)    \n          from    ach_trident_detail    ach\n          where   ach.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.ChargebackUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   achSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:955^9*/
      }
      else    // production run, get a new sequence number 
      {
        /*@lineinfo:generated-code*//*@lineinfo:959^9*/

//  ************************************************************
//  #sql [Ctx] { select  ach_trident_file_sequence.nextval 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ach_trident_file_sequence.nextval  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.ChargebackUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   achSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:963^9*/
      }        
      
      /*@lineinfo:generated-code*//*@lineinfo:966^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(cb.bank_number,
//                         3858,'0','') ||
//                  to_char(cb.merchant_number)         as merchant_number,
//                  mf.dba_name                         as dba_name,
//                  o.org_group                         as deposit_merchant_number,
//                  o.org_name                          as deposit_dba_name,
//                  0                                   as roll_up_deposit,
//                  lpad(nvl(atn.adj_transit_routing,
//                    mf.charge_back_routing), 9, '0')  as transit_routing,
//                  nvl(atn.adj_dda,mf.charge_back_dda) as dda_num,
//                  trunc(sysdate)                      as post_date,
//                  'CCD'                               as class_code,
//                  :AchEntryData.ED_CHARGEBACK       as entry_desc,
//                  :achSequence                        as ach_sequence,
//                  1                                   as batch_number,
//                  cba.action_code,
//                  decode(cba.action_code,
//                    'D', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 0, 1),
//                    'C', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 1, 0),
//                    0)                                as sales_count,
//                  decode(cba.action_code,
//                    'D', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 0, 1),
//                    'C', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 1, 0),
//                    0) * abs(cb.tran_amount)          as sales_amount,
//                  decode(cba.action_code,
//                    'D', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 1, 0),
//                    'C', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 0, 1),
//                    0)                                as credits_count,
//                  decode(cba.action_code,
//                    'D', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 1, 0),
//                    'C', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 0, 1),
//                    0) * abs(cb.tran_amount)          as credits_amount,
//                  decode(cba.action_code,
//                    'D', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 1, -1),
//                    'C', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, -1, 1),
//                    -1) * abs(cb.tran_amount)         as net_amount
//          from    network_chargebacks         cb,
//                  network_chargeback_activity cba,
//                  chargeback_action_codes     ac,
//                  mif                         mf,
//                  ach_trident_nodes           atn,
//                  organization                o
//          where   cb.bank_number = :FileBankNumber
//                  and cb.incoming_date between (:FileActivityDate-180) and :FileActivityDate
//                  and cb.merchant_number != 0
//                  and cb.card_number_enc is not null
//                  and cba.cb_load_sec = cb.cb_load_sec
//                  and cba.load_filename = :workFilename
//                  and ac.short_action_code = cba.action_code
//                  and ac.adj_tran_code is not null
//                  and mf.merchant_number = cb.merchant_number
//                  and nvl(mf.trans_dest_deposit,mf.trans_dest_global) != 'X'
//                  and atn.hierarchy_node(+) = decode( nvl(mf.trans_dest_deposit,
//                                                          mf.trans_dest_global),
//                                                      'M', mf.association_node,
//                                                      0)
//                  and o.org_group = nvl(atn.merchant_number_default,mf.merchant_number)
//          order by cb.merchant_number, cb.card_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(cb.bank_number,\n                       3858,'0','') ||\n                to_char(cb.merchant_number)         as merchant_number,\n                mf.dba_name                         as dba_name,\n                o.org_group                         as deposit_merchant_number,\n                o.org_name                          as deposit_dba_name,\n                0                                   as roll_up_deposit,\n                lpad(nvl(atn.adj_transit_routing,\n                  mf.charge_back_routing), 9, '0')  as transit_routing,\n                nvl(atn.adj_dda,mf.charge_back_dda) as dda_num,\n                trunc(sysdate)                      as post_date,\n                'CCD'                               as class_code,\n                 :1         as entry_desc,\n                 :2                          as ach_sequence,\n                1                                   as batch_number,\n                cba.action_code,\n                decode(cba.action_code,\n                  'D', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 0, 1),\n                  'C', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 1, 0),\n                  0)                                as sales_count,\n                decode(cba.action_code,\n                  'D', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 0, 1),\n                  'C', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 1, 0),\n                  0) * abs(cb.tran_amount)          as sales_amount,\n                decode(cba.action_code,\n                  'D', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 1, 0),\n                  'C', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 0, 1),\n                  0)                                as credits_count,\n                decode(cba.action_code,\n                  'D', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 1, 0),\n                  'C', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 0, 1),\n                  0) * abs(cb.tran_amount)          as credits_amount,\n                decode(cba.action_code,\n                  'D', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, 1, -1),\n                  'C', decode (abs(cb.tran_amount)/nvl(cb.tran_amount,1), -1, -1, 1),\n                  -1) * abs(cb.tran_amount)         as net_amount\n        from    network_chargebacks         cb,\n                network_chargeback_activity cba,\n                chargeback_action_codes     ac,\n                mif                         mf,\n                ach_trident_nodes           atn,\n                organization                o\n        where   cb.bank_number =  :3  \n                and cb.incoming_date between ( :4  -180) and  :5  \n                and cb.merchant_number != 0\n                and cb.card_number_enc is not null\n                and cba.cb_load_sec = cb.cb_load_sec\n                and cba.load_filename =  :6  \n                and ac.short_action_code = cba.action_code\n                and ac.adj_tran_code is not null\n                and mf.merchant_number = cb.merchant_number\n                and nvl(mf.trans_dest_deposit,mf.trans_dest_global) != 'X'\n                and atn.hierarchy_node(+) = decode( nvl(mf.trans_dest_deposit,\n                                                        mf.trans_dest_global),\n                                                    'M', mf.association_node,\n                                                    0)\n                and o.org_group = nvl(atn.merchant_number_default,mf.merchant_number)\n        order by cb.merchant_number, cb.card_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.ChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,AchEntryData.ED_CHARGEBACK);
   __sJT_st.setLong(2,achSequence);
   __sJT_st.setInt(3,FileBankNumber);
   __sJT_st.setDate(4,FileActivityDate);
   __sJT_st.setDate(5,FileActivityDate);
   __sJT_st.setString(6,workFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.startup.ChargebackUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1026^7*/
      
      rs = it.getResultSet();
      
      while( rs.next() )
      {
        originNode = achOriginList.getMerchantOriginNode(rs.getLong("merchant_number"),AchEntryData.ED_CHARGEBACK);
        
        if ( originNode != 0L )
        {
          /*@lineinfo:generated-code*//*@lineinfo:1036^11*/

//  ************************************************************
//  #sql [Ctx] { select  ach_trident_trace_sequence.nextval
//              
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ach_trident_trace_sequence.nextval\n             \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.ChargebackUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   traceNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1041^11*/
          
          achData = new AchEntryData();
          achData.setData(rs);
          achData.setOriginNode(originNode);
          achData.setTraceNumber(traceNumber);
          achData.addStatementData(rs);
          achEntries.add(achData);
        }
      }
      
      rs.close();
      it.close();
      
      AchEntryData.storeVector(achEntries, false, false, ChargeBackConstants.NEXT_DAY_FUNDING_CLAUSE.getStringValue());
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "processMESChargebacks(" + workFilename + ")", e.toString());
      logEntry("processMESChargebacks(" + workFilename + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  protected void processMRCH3( String workFilename, BufferedWriter out )
  {
    int                 offset          = 0;
    int                 rowCount        = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1076^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(cb.merchant_number) 
//          from    network_chargebacks           cb,
//                  network_chargeback_activity   cba,
//                  chargeback_action_codes       ac,
//                  mif                           mf
//          where   cb.bank_number = :FileBankNumber and
//                  cb.incoming_date between (:FileActivityDate-180) and :FileActivityDate and
//                  cb.merchant_number != 0 and
//                  not cb.card_number_enc is null and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.load_filename = :workFilename and
//                  ac.short_action_code = cba.action_code and
//                  not ac.adj_tran_code is null
//                  and mf.merchant_number = cb.merchant_number
//                  and nvl(mf.processor_id,0) = 0  -- tsys back end only
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cb.merchant_number)  \n        from    network_chargebacks           cb,\n                network_chargeback_activity   cba,\n                chargeback_action_codes       ac,\n                mif                           mf\n        where   cb.bank_number =  :1   and\n                cb.incoming_date between ( :2  -180) and  :3   and\n                cb.merchant_number != 0 and\n                not cb.card_number_enc is null and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.load_filename =  :4   and\n                ac.short_action_code = cba.action_code and\n                not ac.adj_tran_code is null\n                and mf.merchant_number = cb.merchant_number\n                and nvl(mf.processor_id,0) = 0  -- tsys back end only";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.ChargebackUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1093^7*/
      
      if ( rowCount > 0 )
      {
        // build the file using the FlatFileDef
        BatchCreditsAmount     = 0.0;
        BatchDebitsAmount      = 0.0;
        BatchPaymentsAmount    = 0.0;
        BatchRecordCount       = 0;
        BatchCreditsCount      = 0;
        BatchDebitsCount       = 0;
      
        // reset the test grid 
        if ( inTestMode() )
        {
          for(int i = 0; i < TestGrid.length; ++i )
          {
            TestGrid[i] = false;
          }
        }
      
        // note that the work filename is used 
        // to query the chargebacks table only
        // the output filename is contained
        // in adjFilename.
        buildAdjustmentHeader(workFilename,out);
        buildAdjustmentDetails(workFilename,out);
        buildAdjustmentTrailer(out);
      }  
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processMRCH3()", e.toString());
      logEntry("processMRCH3()", e.toString());
    }
    finally
    {
    }
  }
  
  protected void processNTCR3( String workFilename, BufferedWriter out )
  {
    int                 rowCount        = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1139^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(cb.merchant_number) 
//          from    network_chargebacks           cb,
//                  network_chargeback_activity   cba,
//                  chargeback_action_codes       ac
//          where   cb.bank_number = :FileBankNumber and
//                  cb.incoming_date between (:FileActivityDate-180) and :FileActivityDate and
//                  cb.merchant_number != 0 and
//                  not cb.card_number_enc is null and
//                  cba.cb_load_sec = cb.cb_load_sec and
//                  cba.load_filename = :workFilename and
//                  ac.short_action_code = cba.action_code and
//                  nvl(ac.represent,'N') = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cb.merchant_number)  \n        from    network_chargebacks           cb,\n                network_chargeback_activity   cba,\n                chargeback_action_codes       ac\n        where   cb.bank_number =  :1   and\n                cb.incoming_date between ( :2  -180) and  :3   and\n                cb.merchant_number != 0 and\n                not cb.card_number_enc is null and\n                cba.cb_load_sec = cb.cb_load_sec and\n                cba.load_filename =  :4   and\n                ac.short_action_code = cba.action_code and\n                nvl(ac.represent,'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.ChargebackUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,FileActivityDate);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setString(4,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1153^7*/
      
      if ( rowCount > 0 )
      {
        // build the file using the FlatFileDef
        BatchCreditsAmount     = 0.0;
        BatchDebitsAmount      = 0.0;
        BatchPaymentsAmount    = 0.0;
        BatchRecordCount       = 0;
        BatchCreditsCount      = 0;
        BatchDebitsCount       = 0;
        
        // reset the test grid 
        if ( inTestMode() )
        {
          for(int i = 0; i < TestGrid.length; ++i )
          {
            TestGrid[i] = false;
          }
        }
      
        buildChargebackHeader(workFilename,out);
        buildChargebackDetails(workFilename,out);
        buildChargebackTrailer(out);
      }  
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processNTCR3()", e.toString());
      logEntry("processNTCR3()", e.toString());
    }
    finally
    {
    }
  }
  
  protected void sendStatusEmail(boolean success, String fileName, String message )
  {
    StringBuffer    status    = new StringBuffer(message);
    
    status.append("\nFile Totals By Batch\n");
    status.append("================================================\n");
    status.append("NTCR3 Amount  : " + MesMath.toCurrency(FileBatchTotals[IDX_NTCR3]));
    status.append("\n");
    status.append("MRCH3 Amount  : " + MesMath.toCurrency(FileBatchTotals[IDX_MRCH3]) );
    status.append("\n\n");
    
    super.sendStatusEmail(success,fileName,status.toString());
  }
  
  public static void main( String[] args )
  {
    ChargebackUpload        test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new ChargebackUpload();
      test.setTestFilename( (args.length == 0) ? null : args[0]);
      test.execute();
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/