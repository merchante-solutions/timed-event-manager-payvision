/*@lineinfo:filename=CBAutoIDocAttach*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

// log4j
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.PropertiesFile;


public class CBAutoIDocAttach extends AutoAttachBase
{

  // create class log category
  static Logger log = Logger.getLogger(CBAutoIDocAttach.class);

  public CBAutoIDocAttach()
  {
    super();
    msgSubject = "Auto Issuer Doc Attachment Notification";
  }

  protected void readProps() throws Exception
  {

    PropertiesFile props = new PropertiesFile("imageattach.properties");

    //above Strings will be retrieved from props
    FTPHOST         = props.getString("I_FTPHOST",FTPHOST);
    FTPUSER         = props.getString("I_FTPUSER",FTPUSER);
    FTPPASSWORD     = props.getString("I_FTPPASSWORD",FTPPASSWORD);
    IMAGE_DIRECTORY = props.getString("I_IMAGE_DIRECTORY",IMAGE_DIRECTORY);
    ADDRESS_REF     = props.getString("I_ADDRESS_REF",ADDRESS_REF);
    SOURCE          = ISSUER;

    REGEX           = props.getString("REGEX",REGEX);

  }

  /**
   * process()
   * main method whereby each file (image) is attached
   * and rebuttals are created/items moved in queue where
   * applicable
   **/
  protected void process()
  throws Exception{

    log.debug("START process()");

    AutoAttachBase.FileObj obj;

    //ensure FTP connection is live
    try
    {
      if( !SftpSec.isConnected() )
      {
        refreshSFTP();
      }
    }
    catch(Exception fail)
    {
      refreshSFTP();
    }

    for(int i = 0; i < fileList.size(); i++)
    {
      obj = (AutoAttachBase.FileObj)fileList.get(i);
      try
      {
        log.debug("obj CN: "+obj.getControlNum()+" is valid");

        attachFile(obj, "aui_");

        obj.message = "Successfully processed.";

        successList.add(obj);
      }
      catch (Exception e)
      {
        obj.message = "Unable to process: "+e.getMessage();

        errorList.add(obj);

        //add this, in case something happens to the sendError()
        //kinda redundant
        logEntry("process(" + String.valueOf(obj.fileName) + ")",e.toString());
      }
    }

    log.debug("\nValid files: " + fileList.size());
    log.debug("END process()");
  }


  public static void main( String[] args )
  {
    CBAutoIDocAttach     test      = null;

    try
    {
      SQLJConnectionBase.initStandalone();

      test = new CBAutoIDocAttach();
      test.execute();
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/