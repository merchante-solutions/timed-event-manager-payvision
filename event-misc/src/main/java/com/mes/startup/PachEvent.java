package com.mes.startup;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.pach.proc.DailyProcessingRun;
import com.mes.pach.proc.LoadReturnsJob;
import com.mes.pach.proc.PendCreditsJob;
import com.mes.pach.proc.PendSalesJob;
import com.mes.pach.proc.ProcessReturnsJob;
import com.mes.pach.proc.SettleCreditsJob;
import com.mes.pach.proc.SettleSalesJob;
import com.mes.pach.proc.SummarizerJob;
import com.mes.pach.proc.UpdatePostDaysJob;

public final class PachEvent extends EventBase
{
  static Logger log = Logger.getLogger(PachEvent.class);
  
  public static final String ARG_EVENT_TYPE = "event";
  public static final String ARG_SYS_TYPE   = "sys";

  public static final String ET_PEND        = "pend";
  public static final String ET_SETTLE      = "settle";
  public static final String ET_SUMMARIZE   = "summarize";
  public static final String ET_RETURNS     = "returns";
  public static final String ET_END_OF_DAY  = "eod";
  public static final String ET_LOAD_RET    = "loadret";
  public static final String ET_POST_UP     = "postup";

  public static final String SYS_PRODUCTION = "prod";
  public static final String SYS_TEST       = "test";

  private static String[] VALS_EVENT_TYPE   =
  {
    ET_PEND, 
    ET_SETTLE, 
    ET_SUMMARIZE, 
    ET_RETURNS, 
    ET_END_OF_DAY, 
    ET_LOAD_RET,
    ET_POST_UP
  };

  private static String[] VALS_SYS_TYPE     = { SYS_PRODUCTION, SYS_TEST };

  private static Map argValsMap = new HashMap();

  static
  {
    argValsMap.put(ARG_EVENT_TYPE,VALS_EVENT_TYPE);
    argValsMap.put(ARG_SYS_TYPE,VALS_SYS_TYPE);
  }

  private String getValidEventArg(String arg, int argNum)
  {
    String[] allowedValues = (String[])argValsMap.get(arg);
    if (allowedValues == null)
    {
      throw new NullPointerException("Event argument '" + arg + "' invalid");
    }

    String value = getEventArg(argNum).toLowerCase();
    for (int i = 0; i < allowedValues.length; ++i)
    {
      if (allowedValues[i].toLowerCase().equals(value))
      {
        return value;
      }
    }

    throw new RuntimeException("Invalid value '" + value 
      + "' for event argument " + argNum);
  }

  public boolean execute()
  {
    try
    {
      String eventType = getValidEventArg(ARG_EVENT_TYPE,0);
      boolean testFlag = !(getValidEventArg(ARG_SYS_TYPE,1).equals(SYS_PRODUCTION));

      log.debug("PachEvent executing: eventType=" + eventType 
        + ", testFlag=" + testFlag);

      if (eventType.equals(ET_PEND))
      {
        (new PendSalesJob(false,testFlag)).run();
        (new PendCreditsJob(false,testFlag)).run();
      }
      else if (eventType.equals(ET_SETTLE))
      {
        (new SettleSalesJob(false,testFlag)).run();
        (new SettleCreditsJob(false,testFlag)).run();
      }
      else if (eventType.equals(ET_SUMMARIZE))
      {
        (new SummarizerJob(false,testFlag)).run();
      }
      else if (eventType.equals(ET_RETURNS))
      {
        (new ProcessReturnsJob(false,testFlag)).run();
      }
      else if (eventType.equals(ET_END_OF_DAY))
      {
        (new DailyProcessingRun(false,testFlag,getHostName())).run();
      }
      else if (eventType.equals(ET_LOAD_RET))
      {
        (new LoadReturnsJob(false)).run();
      }
      else if (eventType.equals(ET_POST_UP))
      {
        (new UpdatePostDaysJob(false)).run();
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error",e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    return false;
  }
  
  public static void main(String[] args) {
  
    int exitVal = 0;
    
    try {
      PachEvent pe = new PachEvent();
      pe.setEventArgs(args);
      pe.execute();
    }
    catch (Exception e) {
      log.error(e);
      exitVal = -1;
    }
    
    System.exit(exitVal);
  }
}