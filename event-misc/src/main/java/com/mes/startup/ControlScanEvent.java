package com.mes.startup;

import java.util.Enumeration;
import com.mes.controlscan.SmartSyncAPI;
import com.mes.support.PropertiesFile;


public class ControlScanEvent extends EventBase {
     SmartSyncAPI ssa;
     PropertiesFile pf = null;

	public boolean execute() {
		boolean monthEndcheck = false;
		try {

			log.debug("loading control scan properties file");
			pf = new PropertiesFile();
			pf.load("controlscan.properties");

			if (getEventArgCount() >= 1)
				monthEndcheck = true;
			ssa = new SmartSyncAPI();
			String controlScanURL = pf.getProperty("ControlScanAPI.URL");

			if (pf.containsKey("ControlScanAPI.URL"))
				pf.remove(pf.get("ControlScanAPI.URL"));
			if (pf.containsKey("ControlScanAPI.Boarding.URL"))
				pf.remove(pf.get("ControlScanAPI.Boarding.URL"));

			/*
			 * call smart sync api for each portfolio added in controlscan
			 * properties file
			 */
			for (Enumeration e = pf.getPropertyNames(); e.hasMoreElements();) {
				String userName = (String) e.nextElement();
				if (!userName.equals("ControlScanAPI.URL") && !userName.equals("ControlScanAPI.Boarding.URL")) {
					String pwd = pf.getString(userName);
					log.debug("Calling portfolio " + userName);
					ssa.callAPI(controlScanURL, userName, pwd, monthEndcheck);
				}
			}

		} catch (Exception e) {
			logEntry("ControlScanEvent.execute()", e.toString());
		}

		return false;
	}

     public static void main(String args[]){
          ControlScanEvent cs = new ControlScanEvent();
          cs.setEventArgs(args);
          cs.execute();
          System.exit(0);

     }

}
