/*@lineinfo:filename=AccuLynkLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
// log4j
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import com.mes.support.StringUtilities;
import com.mes.tools.BlobHandler;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;


public class AccuLynkLoader extends EventBase
{

  // create class log category
  static Logger log = Logger.getLogger(AccuLynkLoader.class);

  private LinkedList fileList    = new LinkedList();
  private ArrayList successList = new ArrayList();
  private ArrayList failList    = null;

  private String filename       = "";

  private String header         ;
  private String ftphost        ;
  private String ftpuser        ;
  private String ftppassword    ;
  private String testFlag      ;
  private long mid              = 0L; 

  private boolean isTest        = false;

  public static long BIN_ACCULYNK    = 440000L;

  public AccuLynkLoader()
  {
    super();
  }

  //FOR testing======================
  public AccuLynkLoader(String[] args)
  {
    super();
    isTest = true;
    testFlag = "Y";
    for(int i=0;i<args.length;i++)
    {
      fileList.add(args[i]);
    }
  }
  //END testing code

  private void loadProps()
  {
    log.debug("in loadProps... ");
    try
    {
      PropertiesFile props = new PropertiesFile("acculynk.properties");

      ftphost         = props.getString("FTPHOST");
      ftpuser         = props.getString("FTPUSER");
      ftppassword     = props.getString("FTPPASSWORD");
      header          = props.getString("HEADER");
      testFlag        = props.getString("TESTFLAG");
      //mid             = props.getLong("MID",mid);

    }
    catch(Exception e)
    {
      logEntry("loadProps()", e.toString());
      log.error(e.getMessage());
    }
  }

 /**
   * METHOD execute()
   *
   * generates/processes auto image attachment for queue items
   * that have been rebutted - and moves item to proper queue
   */
  public boolean execute()
  {

    log.debug("in execute");

    boolean filesLoaded = false;
    String errMsg       = "Files in AcculynkLoader process (failed): \n";

    try
    {

      if(!isTest)
      {
        loadProps();
      }

      //find valid file(s)
      if(retrieveFileList())
      {
        log.debug("found files to process...");
        for(Iterator i = fileList.iterator(); i.hasNext();)
        {
          //grab and convert data
          loadFile((String)i.next());
        }

        //delete succesful files from host
        removeFiles();

        //determine success
        filesLoaded = failList == null;

      }
      else
      {
        //change message to indicate no files found
        errMsg       = "Files in AcculynkLoader process (none found): \n";
      }

    }
    catch(Exception e)
    {
      //leave false
      //need to do anything else?
        logEntry("execute()", e.toString());
    }
    finally
    {
      try
      {
        if(!filesLoaded)
        {
          emailNotification(errMsg, failList);
        }
      }
      catch(Exception e1)
      {
        logEntry("emailNotification()", e1.toString());
      }
    }

    return filesLoaded;
  }


  private boolean retrieveFileList()
    throws Exception
  {
    log.debug("In retrieveFileList");

    if(!isTest)
    {
      Iterator i = getSftpDirList(ftphost,ftpuser,ftppassword,null,false);

      String s;

      while(i.hasNext())
      {
        s = (String)i.next();

        //simply ensure acculynk files get in (processed) first
        if(s.indexOf("ACC")==0)
        {
          fileList.add(0,s);
        }
        else if(s.indexOf("rpt")==0 || s.indexOf("RPT2.")==4)
        {
          //add all report files as 'success' for removal
          //not doing anything with them yet
          successList.add(s);
        }
        else
        {
          fileList.add(s);
        }
      }
    }

    return (fileList.size()>0);
  }


  private void loadFile(String fileName)
    throws Exception
  {

    boolean isSuccess = true;

    log.debug("In loadFile");
    //initFTP
    //grab file
    //process file
    //closeFTP
    try
    {

      this.filename = fileName;

      int type = getFileType(fileName);

      if(isTest || isValidFile(fileName, type))
      {

        byte[] body = getData(fileName);

        //only add the file to the DB if not testing
        if(!isTest)
        {

          BlobHandler bHand = new BlobHandler();

          if(!bHand.insert(body, fileName, type))
          {
            logEntry("loadFile()::"+fileName, "Unable to save document before parse");
          }
        }
        else
        {
          log.debug("would have saved file to DB here...");
        }

        //get one long line of data
        String data = new String(body);

        switch(type)
        {
          //ACCULYNK file
          case mesConstants.FILETYPE_ACCULYNK:

            log.debug("hitting Acculynk data...");
            loadAcculynkData(data);
            break;

          //FISERV file
          case mesConstants.FILETYPE_FISERV:

            log.debug("hitting FiServ data...");
            loadFiservData(data);
            break;

          //ELAN file
          case mesConstants.FILETYPE_ELAN:
            log.debug("hitting Elan data...");
            loadElanData(data);
            //throw new Exception("Ignoring FiServ data, leaving on server");
            break;

          default:
            log.debug("ignoring file, leaving on server...");
            isSuccess = false;
        };

        if(isSuccess)
        {
          successList.add(fileName);
        }
      }
      else
      {
        logEntry("loadFile()::"+fileName,"DUPLICATE: File already processed... removing from host server.");
        //make sure we remove this duplicate from the server
        successList.add(fileName);
      }

    }
    catch(Exception e)
    {

     //e.printStackTrace();
     logEntry("loadFile()::"+fileName, e.getMessage());

     //add to fail list
     if(failList == null)
     {
       failList = new ArrayList();
     }

     failList.add(fileName);
    }
  }

  private boolean isValidFile(String filename, int type)
  {
    boolean result = true;

    try
    {
      int count = 0;

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:311^7*/

//  ************************************************************
//  #sql [Ctx] { select count(1)
//          
//          from document
//          where doc_name  = :filename
//          and doc_type_id = :type
//          and nvl(can_remove,'N') = 'N'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(1)\n         \n        from document\n        where doc_name  =  :1 \n        and doc_type_id =  :2 \n        and nvl(can_remove,'N') = 'N'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.AccuLynkLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,filename);
   __sJT_st.setInt(2,type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:319^7*/

      if(count > 0)
      {
        //duplicate found
        result = false;
      }
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }

    return result;
  }


  //SEE 'ACCULYNK' details below (if needed)
  private void loadAcculynkData(String data)
  throws Exception
  {
    log.debug("In loadAcculynkData");

    if(data == null)
    {
      return;
    }

    //break it by \n
    String[] lines = data.split("\\n");
    String[] pieces;

    try
    {

      connect();

      for(int i = 0; i < lines.length; i++)
      {

        pieces = lines[i].split("\\|");

        if(pieces[0].equals(header))
        {
          log.debug("Skipping header...");
          continue;
        }
        else
        {

          //look for the merchant ID in the file, 3rd piece in...
          mid = StringUtilities.stringToLong(pieces[2].substring(0,12),mid);

          log.debug("Saving mid..."+mid);

          //if found, go forward to save record
          if(mid > 0)
          {
            // get a new rec id
            long recId = 0;

            if(!isTest)
            {
              /*@lineinfo:generated-code*//*@lineinfo:385^15*/

//  ************************************************************
//  #sql [Ctx] { select  trident_capture_api_sequence.nextval 
//                  from    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trident_capture_api_sequence.nextval  \n                from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.AccuLynkLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:389^15*/
            }

            //get the TID from trident_profile
            String tid;

            if(pieces[2].length() < 20 )
            {
              //old version where we map the first valid TID to the MID
              tid = getTIDFromMID(mid);
            }
            else
            {
              //NEW 01/10: using complete term profile id from acculynk
              //ex. 94100009743000000001
              tid = pieces[2];
            }
            
            log.debug("tid..."+tid);
            
            // Retrieve the current USD batch ID/Num from the profile.
            String batchId = null;
            String batchNumber = null;
            if(!isTest) { // Exclude test transactions, which cannot settle.
              String q = "select current_batch_id, current_batch_number from trident_profile_api where terminal_id = ?";
               PreparedStatement ps = getPreparedStatement(q);
               ps.setString(1, tid);
               ResultSet rs = ps.executeQuery();
               if(rs.next()) {
                 batchId = rs.getString("current_batch_id");
                 batchNumber = rs.getString("current_batch_number");
               }
               else
                   log.warn("No current batch id / number found for tid "+tid);
               ps.close();
               rs.close();
            }
            
            log.debug("Batch ID: "+batchId);
            log.debug("Batch Number: "+batchNumber);

            //check card number
            String cardNum = pieces[5].length()>16?pieces[5].substring(0,16):pieces[5];

            //generate sql Date for transaction
            java.sql.Date tranDate = DateTimeFormatter.parseSQLDate(pieces[8].substring(0,10), "yyyy-MM-dd");

            //translate transaction type
            //transaction_type = "D"(MES) = "S"(ACC) sale or "C"(MES) = "R"(ACC) //credit/refund
            String tranType = pieces[3].equals("S")?"D":"C";

            //convert 9(8)v99 format
            double tranAmount = decodeTransAmount(pieces[7]);

            //convert other pieces for sqlj
            String cardPresent  = pieces[12];
            String motoInd      = pieces[14];
            String currCode     = pieces[16];
            String authCode     = pieces[9];
            String xTransID     = pieces[13];
            String netId        = pieces[19];
            String IDToRef      = pieces[20];
            //now that Acculynk is sending us numbers greater than 11 digits...
            if(IDToRef!=null && IDToRef.length()>11)
            {
              IDToRef = IDToRef.substring(IDToRef.length()-11);
            }


          //Map to Purchase Identifier field
            String custom1      = pieces[22];

          //Map to Client Reference field
            String custom2      = pieces[23];

            if(isTest)
            {
              log.debug("================");
              log.debug("mid "+mid);
              log.debug("recId "+recId);
              log.debug("tid "+tid);
              log.debug("cardNum "+cardNum);
              log.debug("tranType "+tranType);
              log.debug("tranAmount "+tranAmount);
              log.debug("cardPresent "+cardPresent);
              log.debug("motoInd "+motoInd);
              log.debug("currCode "+currCode);
              log.debug("authCode "+authCode);
              log.debug("xTransID "+xTransID);
              log.debug("netId "+netId);
              log.debug("IDToRef "+IDToRef);
              log.debug("custom1 "+custom1);
              log.debug("custom2 "+custom2);
              log.debug("================");
            }
            else
            {
              /*@lineinfo:generated-code*//*@lineinfo:486^15*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_capture_api
//                  (
//                     rec_id,
//                     terminal_id,
//                     merchant_number,
//                     card_type,
//                     card_number,
//                     card_present,
//                     moto_ecommerce_ind,
//                     transaction_date,
//                     debit_credit_indicator,
//                     currency_code,
//                     transaction_amount,
//                     transaction_type,
//                     auth_amount,
//                     reference_number,
//                     client_reference_number,
//                     purchase_id,
//                     auth_code,
//                     trident_tran_id,
//                     test_flag,
//                     server_name,
//                     debit_network_id,
//                     batch_id,
//                     batch_number
//                  )
//                  values
//                  (
//                    :recId,
//                    :tid,
//                    :mid,
//                    'DB',
//                    :cardNum,
//                    :cardPresent,
//                    :motoInd,
//                    :tranDate,
//                    :tranType,
//                    :currCode,
//                    :tranAmount,
//                    :tranType,
//                    :tranAmount,
//                    :IDToRef,
//                    :custom2,
//                    :custom1,
//                    :authCode,
//                    :xTransID,
//                    :testFlag,
//                    'Acculynk',
//                    decode_network_id(:netId),
//                    :batchId,
//                    :batchNumber
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into trident_capture_api\n                (\n                   rec_id,\n                   terminal_id,\n                   merchant_number,\n                   card_type,\n                   card_number,\n                   card_present,\n                   moto_ecommerce_ind,\n                   transaction_date,\n                   debit_credit_indicator,\n                   currency_code,\n                   transaction_amount,\n                   transaction_type,\n                   auth_amount,\n                   reference_number,\n                   client_reference_number,\n                   purchase_id,\n                   auth_code,\n                   trident_tran_id,\n                   test_flag,\n                   server_name,\n                   debit_network_id,\n                   batch_id,\n                   batch_number\n                )\n                values\n                (\n                   :1 ,\n                   :2 ,\n                   :3 ,\n                  'DB',\n                   :4 ,\n                   :5 ,\n                   :6 ,\n                   :7 ,\n                   :8 ,\n                   :9 ,\n                   :10 ,\n                   :11 ,\n                   :12 ,\n                   :13 ,\n                   :14 ,\n                   :15 ,\n                   :16 ,\n                   :17 ,\n                   :18 ,\n                  'Acculynk',\n                  decode_network_id( :19 ),\n                   :20 ,\n                   :21 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.AccuLynkLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setString(2,tid);
   __sJT_st.setLong(3,mid);
   __sJT_st.setString(4,cardNum);
   __sJT_st.setString(5,cardPresent);
   __sJT_st.setString(6,motoInd);
   __sJT_st.setDate(7,tranDate);
   __sJT_st.setString(8,tranType);
   __sJT_st.setString(9,currCode);
   __sJT_st.setDouble(10,tranAmount);
   __sJT_st.setString(11,tranType);
   __sJT_st.setDouble(12,tranAmount);
   __sJT_st.setString(13,IDToRef);
   __sJT_st.setString(14,custom2);
   __sJT_st.setString(15,custom1);
   __sJT_st.setString(16,authCode);
   __sJT_st.setString(17,xTransID);
   __sJT_st.setString(18,testFlag);
   __sJT_st.setString(19,netId);
   __sJT_st.setString(20,batchId);
   __sJT_st.setString(21,batchNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:540^15*/

            }//end isTest

          }
          else
          {
            //record missing record in log for retrieval
            //should never happen
            logEntry("loadAcculynkData()","Unable to determine MID :: "+ lines[i]);
          }

        }// end if header

      }//end for loop

      log.debug("...Done");

    }
    catch(Exception e)
    {
      log.debug("Insert FAILED: "+e.getMessage());
      logEntry("loadAcculynkData()",e.toString());
      //notify?
      throw e;
    }
    finally
    {
      cleanUp();
    }

  }

  private long getNewDebitRecId()
  {
    SQLJConnectionBase      ftpdb       = null;
    DefaultContext          ftpCtx      = null;
    long                    id          = 0L;

    try
    {
      // uses an mes schema sequence to generate these ids
      // not to be confused with mesload.charge_back_pk table ids
      ftpdb = new SQLJConnectionBase(MesDefaults.getString(MesDefaults.ACCULYNK_LOADER_FTPDB));
      ftpdb.connect();                // connect to ftp db
      ftpdb.setAutoCommit(false);     // disable auto-commit
      ftpCtx = ftpdb.getContext();    // store a ref to the context

      /*@lineinfo:generated-code*//*@lineinfo:588^7*/

//  ************************************************************
//  #sql [ftpCtx] { select      seq_trident_debit.nextval 
//          from        dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = ftpCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select      seq_trident_debit.nextval  \n        from        dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.AccuLynkLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   id = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:592^7*/
    }
    catch( Exception e )
    {
      id = -1L;
      try{ftpdb.logEntry( "getNewDebitRecId()", e.toString());}catch(Exception ee){}
    }
    finally
    {
      try{ ftpdb.cleanUp(); }catch(Exception ee){}
    }
    return(id);
  }

  private void loadFiservData(String data)
  throws Exception
  {

    if(data == null)
    {
      return;
    }

    //break it by \n
    String[] lines = data.split("\\n");

    int counter       = 0;
    int firstNum      = 0;
    List holder       = new ArrayList();
    List records      = new ArrayList();

    for(int i = 0; i < lines.length; i++)
    {

      //data file starts with "AC03" - ignore first line
      if(lines[i].charAt(0) == 'A')
      {
        continue;
      }
      else
      {
        holder.add(lines[i]);

        //build a list of Strings representing a detail record
        counter++;

        try
        {
          firstNum = Integer.parseInt(lines[i].substring(0,1));
        }
        catch(Exception e)
        {
          firstNum = 0;
        }

        //header has 4 lines... counter will move to 5, firstNum will be 1 when
        //header is complete - need to reconfigure here (footer handled as well)
        if( counter > firstNum )
        {
          log.debug("Resetting from header:: counter = " + counter +"; firstNum = "+ firstNum);

          //reset counter to match firstNum
          counter = 1;

          //clear list - want to ensure we only use the details lines
          holder.clear();

          //re-add the line data, as it is now valid
          holder.add(lines[i]);
        }

        if(counter == 6)
        {
          //final line has been reached for this detail record

          //create detail record, type 2 for FISERV
          DetailRecord rec = new DetailRecord(
            holder, mesConstants.FOREIGN_ID_TYPE_FISERV,getNewDebitRecId());

          //alter data to fit MES tables
          finesseRecord(rec);

          //add to the records list
          records.add(rec);

          //clear the holder
          holder.clear();

          //reset the counter
          counter = 0;
        }

        //really don't have to do anything for the footer,
        //will be handled in first condition above

      }
    }

    processDetailRecords(records);

  }

  private void loadElanData(String data)
  throws Exception
  {

    if(data == null)
    {
      return;
    }

    List records      = new ArrayList();
    List heldRecords      = new ArrayList();
    List lineItems    = new ArrayList();

    //break it by \n
    String[] lines = data.split("\\n");

    int counter       = 0;
    int firstNum      = 0;

    for(int i = 0; i < lines.length; i++)
    {

      //trans data starts with "NSD100A" - ignore all other lines
      if(lines[i].indexOf("NSD100A") != 0)
      {
        continue;
      }
      else
      {
        lineItems.clear();

        lineItems.add(lines[i]);

        DetailRecord rec = new DetailRecord(
          lineItems, mesConstants.FOREIGN_ID_TYPE_ELAN,getNewDebitRecId());

        if(rec.accepted)
        {

          finesseRecord(rec);

          if("0400".equals(rec.msg))
          {
            heldRecords.add(rec);
          }
          else
          {
            records.add(rec);
          }
        }
        else
        {
          log.debug("NOT ACCEPTED: "+lines[i]);
        }
      }
    }

    //need to filter possible reversals...
    DetailRecord hRec;
    DetailRecord cRec;

    log.debug("Records size = " + records.size());

    for(Iterator itr = heldRecords.iterator();itr.hasNext();)
    {
      hRec = (DetailRecord) itr.next();

      //loop through record list to remove held item
      for(Iterator itr2 = records.iterator();itr2.hasNext();)
      {
        cRec = (DetailRecord) itr2.next();
        if(hRec.equals(cRec))
        {
          log.debug("reversal:\n"+hRec.toString());
          log.debug("removing:\n"+cRec.toString());
          itr2.remove();
          break;
        }
      }
    }

    log.debug("Records size = " + records.size());

    processDetailRecords(records);

    log.debug("processDetailRecords RUNS HERE...");

  }

  private void processDetailRecords(List records)
  throws Exception
  {
    try
    {
      connect();

      setAutoCommit(false);

      DetailRecord dt;
      List problemRecords = new ArrayList();

      for(Iterator it = records.iterator(); it.hasNext();)
      {
        dt = (DetailRecord)it.next();

        if(!isTest)
        //if(true)
        {
          //insert here
          if(dt.isValid())
          {

            log.debug("inserting..."+dt.recId);
            //add to database
            //use BIN_ACCULYNK for bin
            /*@lineinfo:generated-code*//*@lineinfo:809^13*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_debit_financial
//                (
//                  rec_id,
//                  record_type,
//                  issuer_acquirer_indicator,
//                  settlement_amount,
//                  settlement_date,
//                  --//balanced_date,
//                  transaction_amount,
//                  acquiring_institution_id,
//                  merchant_number,
//                  card_acceptor_name,
//                  card_number,
//                  authorization_id_resp_code,
//                  network_id,
//                  transaction_identifier,
//                  processing_code,
//                  request_message_type,
//                  response_code,
//                  giv_flag,
//                  load_filename
//                )
//                values
//                (
//                  :dt.recId,
//                  :dt.provider,--//'FISERV','ELAN'
//                  'A',
//                  :dt.settleAmt,
//                  :dt.settleDate,
//                  --//:(dt.settleDate),
//                  :dt.transAmt,
//                  :BIN_ACCULYNK,
//                  :dt.mesMid,
//                  :dt.mesMerchName,
//                  :dt.cardNumber,
//                  :dt.authCode,
//                  :dt.mesNetworkId,
//                  :dt.tranId,
//                  :dt.procCode,
//                  :dt.mesMsg,
//                  '00',
//                  '1',
//                  :filename
//  
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into trident_debit_financial\n              (\n                rec_id,\n                record_type,\n                issuer_acquirer_indicator,\n                settlement_amount,\n                settlement_date,\n                --//balanced_date,\n                transaction_amount,\n                acquiring_institution_id,\n                merchant_number,\n                card_acceptor_name,\n                card_number,\n                authorization_id_resp_code,\n                network_id,\n                transaction_identifier,\n                processing_code,\n                request_message_type,\n                response_code,\n                giv_flag,\n                load_filename\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,--//'FISERV','ELAN'\n                'A',\n                 :3 ,\n                 :4 ,\n                --//:(dt.settleDate),\n                 :5 ,\n                 :6 ,\n                 :7 ,\n                 :8 ,\n                 :9 ,\n                 :10 ,\n                 :11 ,\n                 :12 ,\n                 :13 ,\n                 :14 ,\n                '00',\n                '1',\n                 :15 \n\n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.AccuLynkLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,dt.recId);
   __sJT_st.setString(2,dt.provider);
   __sJT_st.setDouble(3,dt.settleAmt);
   __sJT_st.setDate(4,dt.settleDate);
   __sJT_st.setDouble(5,dt.transAmt);
   __sJT_st.setLong(6,BIN_ACCULYNK);
   __sJT_st.setLong(7,dt.mesMid);
   __sJT_st.setString(8,dt.mesMerchName);
   __sJT_st.setString(9,dt.cardNumber);
   __sJT_st.setString(10,dt.authCode);
   __sJT_st.setInt(11,dt.mesNetworkId);
   __sJT_st.setLong(12,dt.tranId);
   __sJT_st.setString(13,dt.procCode);
   __sJT_st.setString(14,dt.mesMsg);
   __sJT_st.setString(15,filename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:856^13*/
          }
          else
          {
            //how to handle records that have issues?
            problemRecords.add("\nINVALID\n"+dt.toString());
          }

        }
        else
        {
          if(!dt.isValid())
          {
            log.debug("\nINVALID\n============\n"+dt.toString());
            problemRecords.add(dt.toString());
          }
          else
          {
            log.debug("\nVALID Detail Record\n============\n"+dt.toString());
          }
        }
      }


      commit();
      //email the issues, and cancel all if there's an issue
      //better to build clean again...
      if(problemRecords.size() > 0)
      {
        emailNotification(filename +"::Detail records identified but NOT processed:\n", problemRecords);
      }

    }
    catch (Exception e)
    {
      e.printStackTrace();
      rollback();
      logEntry("processDetailRecords()",e.toString());
      throw e;
    }
    finally
    {
      cleanUp();
    }

  }

  //Ctx already active from calling method
  private String getTIDFromMID(long mid)
    throws Exception
  {
    ResultSet rs          = null;
    ResultSetIterator it  = null;

    String tid            = "";

    /*@lineinfo:generated-code*//*@lineinfo:912^5*/

//  ************************************************************
//  #sql [Ctx] it = { select tp.terminal_id
//        from trident_profile tp
//        where merchant_number = :mid
//        and tp.profile_status = 'A'
//        and
//        (
//         nvl(tp.api_enabled,'N') = 'Y'
//         or exists
//            (
//              select  tpc.product_code
//              from    trident_product_codes tpc
//              where   tpc.product_code = nvl(tp.product_code,'XX')
//                      and nvl(tpc.trident_payment_gateway,'N') = 'Y'
//            )
//        )
//        order by tp.terminal_id asc
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select tp.terminal_id\n      from trident_profile tp\n      where merchant_number =  :1 \n      and tp.profile_status = 'A'\n      and\n      (\n       nvl(tp.api_enabled,'N') = 'Y'\n       or exists\n          (\n            select  tpc.product_code\n            from    trident_product_codes tpc\n            where   tpc.product_code = nvl(tp.product_code,'XX')\n                    and nvl(tpc.trident_payment_gateway,'N') = 'Y'\n          )\n      )\n      order by tp.terminal_id asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.AccuLynkLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,mid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.AccuLynkLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:930^5*/

    rs = it.getResultSet();

    if(rs.next())
    {
      tid = rs.getString("terminal_id");
    }
    else
    {
      throw new Exception("Unable to find valid Terminal ID for merchant.");
    }

    return tid;

  }



  private int getFileType(String fileName)
  {
    if(fileName.indexOf("pdf") == 0)
    {
      return mesConstants.FILETYPE_FISERV;
    }
    else if (fileName.indexOf("ACC") ==  0)
    {
      return mesConstants.FILETYPE_ACCULYNK;
    }
    else if (fileName.indexOf("USBRAW") ==  0)
    {
      return mesConstants.FILETYPE_ELAN;
    }
    else
    {
      return 0;
    }
  }

  private double decodeTransAmount(String transAmountNoDecimal)
  {
    double amt = 0.0d;

    try
    {
      amt = (Double.valueOf(transAmountNoDecimal)).doubleValue();
    }
    catch(NumberFormatException nfe)
    {
      log.debug("unable to get proper trans amount");
    }

    return amt/100;
  }

  private byte[] getData(String fileName)
    throws Exception
  {

    log.debug("In getData");

    byte[] body = null;

    //test code
    if(isTest)
    {
      //File file = new File("c:"+File.separator+"work"+File.separator+"acculynk"+File.separator+fileName);
      File file = new File("c:"+File.separator+"temp"+File.separator+"out"+File.separator+fileName);
      FileInputStream fis = new FileInputStream(file);

      long length = file.length();

      log.debug("length = " + length);

      if (length > Integer.MAX_VALUE) {
          log.debug("File is too large to process");
          return null;
      }

      body = new byte[(int)length];

      // Read
      int offset = 0;
      int numRead = 0;

      while (offset < body.length
             && (numRead=fis.read(body, offset, body.length-offset)) >= 0)
      {
          offset += numRead;
      }

      // Ensure all the bytes have been read in
      if (offset < body.length) {
          throw new IOException("Could not completely read file "+ file.getName());
      }

      // Close the input stream and return bytes
      fis.close();
    }
    else
    {
      body = ftpGet(fileName, null, ftphost,ftpuser,ftppassword,null,false);
    }

    return body;

  }

  private void removeFiles()
  {

    String name = "start";
    boolean sendEmail = false;
    try
    {
      for (Iterator it = successList.iterator(); it.hasNext();)
      {

        name = (String)it.next();
        log.debug("removing file..." + name);
        if(!isTest)
        {
          ftpDelete(name, ftphost,ftpuser,ftppassword,null);
          sendEmail = true;
        }
        else
        {
          log.debug(name + " would have been removed.");
        }
      }

      if(sendEmail)
      {
        emailNotification("Files removed by AcculynkLoader process (success): \n", successList);
      }

    }
    catch(Exception e)
    {
      logEntry("removeFiles()",e.toString());
    }

  }

  private void emailNotification(String subject, List msgs)
  throws Exception
  {
/*
    if(msgs==null || msgs.size()==0)
    {
      return;
    }
*/
    if(subject == null)
    {
      subject = "Undetermined Acculynk/Fiserv/Elan record issue";
    }

    Object obj;
    DetailRecord rec;
    String line;
    StringBuffer sb = new StringBuffer();

    if(msgs != null)
    {
      for (Iterator it = msgs.iterator(); it.hasNext();)
      {
        obj = it.next();
        if(obj instanceof DetailRecord)
        {
          rec = (DetailRecord)obj;
          for (Iterator it2 = rec.lineItems.iterator(); it2.hasNext();)
          {
            line = (String)it2.next();
            sb.append(line).append("\n");
          }
        }
        else if(obj instanceof String)
        {
          sb.append((String)obj).append("\n");
        }
      }
    }
    else
    {
      sb.append("List of items is empty - notification only");
    }

    if(!isTest)
    {
      //email message
      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_ACCULYNK_NOTIFY);
      msg.setSubject(subject);
      msg.setText(sb.toString());
      msg.send();
    }
    else
    {
      log.debug("\nEMAIL text body copy\n"+sb.toString());
    }

  }

  private void finesseRecord(DetailRecord rec)
  throws Exception
  {

    long _mesMid          = -1;
    String _mesMerchName  = "";

    try
    {

      connect();

      //STEP 1:
      //need to alter the MID to reflect the MES/Fiserv mapping
      //uses mes_foreign_id_map to map over to the MES MID
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1151^9*/

//  ************************************************************
//  #sql [Ctx] { select
//              nvl(fid.mes_mid, nvl(tp.merchant_number,'') ),
//              nvl(tp.merchant_name,'')
//            
//            from
//              mes_foreign_id_map fid,
//              trident_profile tp
//            where
//              fid.foreign_id = :rec.mid
//              and fid.foreign_id_type = :rec.type
//              and fid.terminal_id = trim(tp.terminal_id(+))
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select\n            nvl(fid.mes_mid, nvl(tp.merchant_number,'') ),\n            nvl(tp.merchant_name,'')\n           \n          from\n            mes_foreign_id_map fid,\n            trident_profile tp\n          where\n            fid.foreign_id =  :1 \n            and fid.foreign_id_type =  :2 \n            and fid.terminal_id = trim(tp.terminal_id(+))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.AccuLynkLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,rec.mid);
   __sJT_st.setInt(2,rec.type);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   _mesMid = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   _mesMerchName = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1166^9*/


      }
      catch(Exception ee)
      {
        logEntry("finesseRecord()::foreign ID translation",ee.getMessage());
      }

      rec.mesMid        = _mesMid;
      rec.mesMerchName  = _mesMerchName;

      int netId = 0;

      //STEPS 2 and 3:
      //need to alter the message to reflect MES messaging
      //need to translate the network ID
      switch(rec.type)
      {

        case(mesConstants.FOREIGN_ID_TYPE_FISERV):

          if(rec.msg.equals("1210"))
          {
            //debit indicator
            rec.mesMsg = "0200";

            //fiserv has this reversed to us
            if(rec.indicator.equals("DB"))
            {
              //return (debit to merchant, not cardholder)
              rec.procCode = "204000";
            }
            else if(rec.indicator.equals("CR"))
            {
              //sale (credit to merchant, not cardholder)
              rec.procCode = "004000";
            }
          }

          //STEP 3:
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:1209^13*/

//  ************************************************************
//  #sql [Ctx] { select visa_network_id 
//                from trident_debit_networks
//                where fiserv_network_id = :rec.networkId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select visa_network_id  \n              from trident_debit_networks\n              where fiserv_network_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.AccuLynkLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,rec.networkId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   netId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1214^13*/
          }
          catch(Exception ee)
          {
            //leaving it at 0
            logEntry("Fiserv finesseRecord()::network ID translation",ee.getMessage());
          }

          rec.mesNetworkId = netId;

          break;

        case(mesConstants.FOREIGN_ID_TYPE_ELAN):

          if(rec.msg.equals("0210"))
          {
            //debit indicator
            rec.mesMsg = "0200";

            if(rec.indicator.equals("25"))
            {
              //return (debit to merchant, not cardholder)
              rec.procCode = "204000";
            }
            else if(rec.indicator.equals("15"))
            {
              //sale (credit to merchant, not cardholder)
              rec.procCode = "004000";
            }
          }

          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:1247^13*/

//  ************************************************************
//  #sql [Ctx] { select visa_network_id 
//                from trident_debit_networks
//                where elan_network_id = :rec.networkId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select visa_network_id  \n              from trident_debit_networks\n              where elan_network_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.AccuLynkLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,rec.networkId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   netId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1252^13*/
          }
          catch(Exception ee)
          {
            //leaving it at 0
            logEntry("Elan finesseRecord()::network ID translation",ee.getMessage());
          }

          rec.mesNetworkId = netId;

          break;

        default:
          throw new Exception("Unable to match record type.");

      };

      //STEP 4: try and link
      //String ddtAuth = null;
/*
      try
      {
        #sql[Ctx]
        {
          select
              authorization_num
          into
              :ddtAuth
          from
              daily_detail_file_dt
          where
              merchant_account_number =  rec.mesMid and
              batch_date between rec.settleDate-3 and rec.settleDate+15 and
              cardholder_account_number = rec.cardNumber and
              card_type = 'DB' and
              nvl(dt.auth_amt,dt.transaction_amount) = rec.settleAmt and
              rownum = 1
        };

        log.debug("ddtAuth = " + ddtAuth);
      }
      catch(Exception e)
      {
        //leave it null
      }
*/
      //either what it found, or null
      //rec.authCode = ddtAuth;

    }
    catch(Exception e)
    {
      throw e;
    }
    finally
    {
      cleanUp();
    }

  }
  
  public class DetailRecord
  {

    public int type          = -1;
    public String provider   = "";
    public List lineItems    = null;

    public long   recId         = -1L;
    public long   mesMid        = -1L;
    public String mesMerchName  = "";
    public String mid           = "";
    public double settleAmt     = 0.0d;
    public double transAmt      = 0.0d;
    public String cardNumber    = "";
    public String authCode      = "";
    public String networkId     = "";
    public int    mesNetworkId  = 0;
    public long   tranId        = 0L;
    public String msg           = "";
    public String mesMsg        = "NA";
    public String indicator     = "";
    public String procCode      = "";
    public java.sql.Date settleDate   = null;

    public boolean accepted     = true;

    public DetailRecord(List lineItems, int type, long recId)
    {

      this.lineItems = lineItems;
      this.type = type;

      String line;

      if(type == mesConstants.FOREIGN_ID_TYPE_FISERV)
      {
        //Fiserv data
        provider = "FISERV";

        for(Iterator it = lineItems.listIterator();it.hasNext();)
        {
          line = (String)it.next();
          //log.debug(line);
          processFiservLine(line);
        }
      }
      else if(type == mesConstants.FOREIGN_ID_TYPE_ELAN)
      {
        //Elan data
        provider = "ELAN";

        processElanLine((String)lineItems.get(0));
      }

      this.recId = recId;
    }

    private void processFiservLine(String line)
    {
      int lineNum = 0;
      try
      {
        lineNum = Integer.parseInt(line.substring(0,1));
      }
      catch(Exception e)
      {}

      switch(lineNum)
      {
        case 1:
          //1 000005 1210 84072829 758000 00000003 123639 84024190 4803932305313343         6012 6012
          msg           = line.substring(7,11);
          mid           = line.substring(11,19);
          networkId     = line.substring(19,25);
          cardNumber    = line.substring(47,63);
          break;

        case 2:
          //2 000006 090521014109 0521 090521 450020 DB 000000000100 61000000 503 00 1 0521 0000 000 283 0
          indicator     = line.substring(35,37);
          try
          {
            settleAmt   = Double.parseDouble(line.substring(37,47) +"."+line.substring(47,49));
          }
          catch(Exception e){}
          authCode      = line.substring(71,74);
          break;

        case 3:
          //3 000007 0000 OH COLUMBUS        1785 OBRIEN ROAD   US 041001039        0000 123639
          try
          {
            tranId   = Long.parseLong(line.substring(71,77));
          }
          catch(Exception e){}
          break;

        case 4:
          //4 000008 090521 088 ACQ     840 100W0110400C 000000000100 000000000100 200020 D00000000 0000
          settleDate    = DateTimeFormatter.parseSQLDate("20" + line.substring(7,13),"yyyyMMdd");
          try
          {
            transAmt   = Double.parseDouble(line.substring(37,47) +"."+line.substring(47,49));
          }
          catch(Exception e){}
          break;

        case 5:
          //5 000009 C00000000 C00000000 WWW.2CHECKOUT.C          C00000000 BBN2 AC03
          break;

        case 6:
          //6 000010 00000000000000000000 15696016             000000000100
          break;

        default:
          log.debug("line type not found");
      };
    }

    private void processElanLine(String line)
    {
      log.debug("in processElanLine...");

      mid           =  line.substring(259,268); //will be mapped to mesMID

      try
      {
        settleAmt   = Double.parseDouble(line.substring(70,76) +"."+line.substring(76,78));
        transAmt    = Double.parseDouble(line.substring(49,55) +"."+line.substring(55,57));
      }
      catch(Exception e)
      {
        log.debug(line.substring(70,78)+" or "+line.substring(49,57));
      }

      cardNumber    =  line.substring(29,48).trim();//line.substring(190,209).trim();
      authCode      =  null;//for new linkToDDF code, since Elan doesn't provide
      networkId     =  line.substring(258,259);//line.substring(250,254);//issuer; 254,258 acq
      try
      {
        tranId   = Long.parseLong(line.substring(228,231));
      }
      catch(Exception e){}
      msg           =  line.substring(7,11);
      indicator     =  line.substring(11,13);
      procCode      =  line.substring(11,17);
      settleDate    =  DateTimeFormatter.parseSQLDate("20" + line.substring(238,244),"yyyyMMdd");
                       //line.substring(238,244);//YYMMDD

      //do this to ensure that only accepted transactions are included in the data load
      checkForFail(line.substring(103,106));
    }

    //necessary for Elan - not used with Fiserv
    public void checkForFail(String code)
    {
      int decode = 0;
      try
      {
        decode = Integer.parseInt(code);

        log.debug("checkForFail(CODE) = "+ code);

        //anything with a leading 0,1,5,6 and a trailer between 10-89
        if( (decode > 9 && decode < 90)    ||
            (decode > 109 && decode < 190) ||
            (decode > 509 && decode < 590) ||
            (decode > 609 && decode < 690)
          )
        {
          accepted = false;
        }
      }
      catch (Exception e)
      {
        //leave at 0
        logEntry("Elan checkForFail()::ironically failed",e.getMessage());
      }
    }

    public boolean isValid()
    {
      return (
                recId         != -1    &&
                mesMid        != -1    &&
                !mesMsg.equals("NA")
             );
    }

    public boolean equals(Object o)
    {
      if(!(o instanceof DetailRecord) )
      {
        return false;
      }

      DetailRecord dr = (DetailRecord)o;

      return  (
              this.cardNumber.equals(dr.cardNumber) &&
              this.mesMid     == dr.mesMid          &&
              this.transAmt   == dr.transAmt        &&
              this.networkId.equals(dr.networkId)   &&
              this.settleDate.equals(dr.settleDate)
              );
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append("recId =").append(recId).append("\n");
      sb.append("mid =").append(mid).append("\n");
      sb.append("mesMid =").append(mesMid).append("\n");
      sb.append("mesMerchName =").append(mesMerchName).append("\n");
      sb.append("settleDate =").append(settleDate.toString()).append("\n");
      sb.append("settleAmt =").append(settleAmt).append("\n");
      sb.append("transAmt =").append(transAmt).append("\n");
      sb.append("cardNumber =").append(cardNumber).append("\n");
      sb.append("authCode =").append(authCode).append("\n");
      sb.append("networkId =").append(networkId).append("\n");
      sb.append("mesNetworkId =").append(mesNetworkId).append("\n");
      sb.append("tranId =").append(tranId).append("\n");
      sb.append("indicator =").append(indicator).append("\n");
      sb.append("procCode =").append(procCode).append("\n");
      sb.append("msg =").append(msg).append("\n");
      sb.append("mesMsg =").append(mesMsg).append("\n");
      return sb.toString();
    }

  }



/***********************************************
/* METHODS FOR MANIPULATION
/***********************************************/


  public static void main( String[] args )
  {
    AccuLynkLoader     test      = null;

    try
    {
      SQLJConnectionBase.initStandalone();

      if(args!=null && args.length > 0)
      {
        test = new AccuLynkLoader(args);
      }
      else
      {
        test = new AccuLynkLoader();
      }

      //test.fixBorkedData();
      test.execute();

    }
    finally
    {
    }
  }

  private void fixBorkedData()
  {
    log.debug("RUNNING fixBorkedData()...");
    try
    {
      connect();

/*

      //Acculynk removal
      #sql[Ctx]
      {
        delete from
          trident_capture_api tapi
        where
          tapi.merchant_number in (941000078022, 941000057778)
        and
          tapi.server_name = 'Acculynk'
        and
          tapi.test_flag = 'Y'
      };

      //MID mapping
      #sql[Ctx]
      {
        insert into mes_foreign_id_map values
        ('84089404',1,941000096086)
      };

      #sql[Ctx]
      {
        insert into mes_foreign_id_map values
        ('84072829',1,941000086319)
      };


      //network id mapping
      int[][] ids = {
                      {758000,20},
                      {439000,13},
                      {702002,10},
                      {704002,17},
                      {712002,25},
                      {720002,18},
                      {773002,16},
                      {774002,3},
                    };

      for (int i = 0; i< ids.length; i++)
      {

        #sql[Ctx]
        {
          update trident_debit_networks
          set fiserv_network_id = :(ids[i][0])
          where visa_network_id = :(ids[i][1])
        };
      }

      //Acculynk BIN insert
        #sql[Ctx]
        {
          insert into trident_debit_bins
          values (394100000, 440000, 'MES Acculynk', 4)
        };

      //Fiserv removal
      #sql[Ctx]
      {
        delete from trident_debit_financial tdf
        where tdf.record_type = 'FISERV'
      };


      #sql[Ctx]
      {
        update trident_debit_financial
        set response_code = '00',
        giv_flag = '1',
        acquiring_institution_id = 440000
        where record_type = 'FISERV'
      };

      #sql[Ctx]
      {
        update trident_debit_financial set request_message_type = '0200', message_type = null, debit_credit_indicator = 'C'
        where record_type = 'FISERV'
      };


      #sql[Ctx]
      {
        update trident_debit_networks
        set acculynk_network_id = 'CU', fiserv_network_id = 524002
        where visa_network_id = 24
      };

      #sql[Ctx]
      {
        update trident_debit_networks
        set acculynk_network_id = 'QE', fiserv_network_id = 826000
        where visa_network_id = 29
      };

      #sql[Ctx]
      {
        update trident_debit_networks
        set acculynk_network_id = 'NT'
        where visa_network_id = 23
      };

      #sql[Ctx]
      {
        update trident_capture_api   tapi
        set tapi.load_filename = null
        where
        tapi.mesdb_timestamp >= (sysdate-1)
        and tapi.card_type = 'DB'
        and tapi.server_name ='Acculynk'
      };

      #sql[Ctx]
      {
        update trident_debit_financial
        set balanceD_date = NVL(settleMENT_date,SYSDATE), BALANCED = 'Y'
        where record_type = 'FISERV'
      };

      #sql[Ctx]
      {
        delete from trident_debit_financial
        where rec_id = 1881097
      };

      #sql[Ctx]
      {
        delete from q_data where id in
        (
        select qd.id from q_data qd, trident_debit_financial tdf
        where qd.item_type = 39 and qd.type=1801
        and qd.id = tdf.rec_id(+)
        and tdf.record_type = 'FISERV'
        )
      };

      //to find those items deleted in the above sql proc
      //select qd.* from q_log qd, trident_debit_financial tdf where
      //new_type = 1801 and qd.id = tdf.rec_id(+) and tdf.record_type = 'FISERV'

      #sql[Ctx]
      {
        update trident_debit_financial tdf set
        tdf.debit_credit_indicator = decode(tdf.debit_credit_indicator,'C','D','C' ),
        tdf.processing_code = decode(tdf.processing_code, 4000, 204000, 4000)
        where tdf.record_type = 'FISERV'
      };
*/

    }
    catch(Exception e)
    {
     //e.printStackTrace();
    }
    finally
    {
     cleanUp();
    }

  }


/*
NET_ID  LOGO  NETWORK_NAME

20, 758000  ACCL    ACCEL
13, 439000  AFFN    AFFN NETWORK
10, 702002  FIGSXT  STAR SOUTHEAST NETWORK
17, 704002  PULSXT  PULSE NETWORK
25, 712002  ALSKXT  ALASKA OPTION-
18, 720002  NYCEXT  NYCE NETWORK
16, 773002  MSTRXT  MAESTRO NETWORK
3,  774002  ILNKXT  INTERLINK (INDEPENDENT SPONSORS)

??, 826000  QEBTNE  QUEST EBT NETWORK
??, 827000  CQ01NE  JPM EFS-EBT
??, 524002  FT00XT  CREDIT UNION 24

*/

/*
  // ACCULYNK DETAILS
  //use TridentTools to handle various data pieces
  //such as encodeCardNumber(pieces[5]) to encode the card num

  //Detail (DTR) record breakdown:

  0 Record_Type               char(3) Record type identifier  'DTR'
  1 Record ID                 char(7) Record number in the clearing file  00000001 - 9999999
  2 merchant_cust_id          char(16)  Acquirer MID
  3 trans_code                char(1) Transaction Type Identifier "S" for Sales draft, "R" for Refund
  4 card_type                 char(2) Card Type Identifier  DB
  5 card_number               char(20)  The card number Left Justified - Trailing Spaces
  6 card_exp_date             char(4) Card Expiration date  MMYY
  7 Authorization_amount      char(10)  Amount to be deposited in 9(8)v99 format  Amount to be deposited in 9(8)v99 format
  8 authorization_date_time   char(20)  DateTime stamp  YYYY-MM-DD-THH:MM:SS
  9 Term_seq_num              char(6) EFT Auth Code 000001-999999
  10 POS_entry_mode           char(2) Credit - Debit - EBT POS Entry Mode Identifier  01
  11 cardholder_id_method     char(1) Method of identification used for transaction Auth 2, Return 1
  12 card_present_indicator   char(1) Credit - Debit - EBT  N
  13 TransID                  char(32)  Transaction ID  Alpha Numeric
  14 mpec_indicator           char(1) Mail-Phone-Electronic Commerce Indicator  1
  15 orig_response_code       char(2) Approval Decline Code 00-99
  16 auth_currency_type       char(3) Authorization Currency Type 840 = USD
  17 oaamt_cbamtfd            char(8) Original Authorization Amount Or Cashback Amount for Debit  Original amount authorized for this transaction in 9(6)v99 format, or the amount of cashback for debit card transaction
  18 AVS_result               char(1) Address Verification Service Result Code  single blank space
  19 Card_Logical_Network     char(2) Network ID for Debit cards  Network ID for Debit cards
                                      AF = AFFN
                                      AO = Alaska Option
                                      CU = Credit Union 24
                                      XL = Accel
                                      IL = Interlink
                                      MA = Maestro
                                      NT = NETS
                                      NY = NYCE
                                      PU = Pulse
                                      ST = Star
                                      TY = Shazam
  20 Merchant Order ID        char(12)  merchants order ID  Alpha Numeric
  21 convenience_fee_amt      char(8) The Convenience Fee amount for the transaction in 9(6)v99 format  9(6)v99
  22 Custom1                  char(128) Merchant Use, Passed to MeS Alpha Numeric
  23 Custom2                  char(128) Merchant Use, Passed to MeS Alpha Numeric
  24 Custom3                  char(128) Merchant Use, Passed to MeS Alpha Numeric
  25 Custom4                  char(128) Merchant Use, Passed to MeS Alpha Numeric
  26 Custom5                  char(128) Merchant Use, Passed to MeS Alpha Numeric


Ex file
piece 0  = DTR
piece 1  = 1
piece 2  = 8002758608
piece 3  = S
piece 4  = D
piece 5  = 
piece 6  = 1208
piece 7  = 0000008200
piece 8  = 2008-12-18T10:51:50.873
piece 9  = 012302
piece 10 = 01
piece 11 = 2
piece 12 = N
piece 13 = 25443f9198d44f3bb13ae8535ea3db9d
piece 14 = 1
piece 15 = 00
piece 16 = 840
piece 17 = 00008200
piece 18 =
piece 19 = PU
piece 20 =
piece 21 = 0.00
piece 22 = FullTest
piece 23 = specifies
piece 24 = all
piece 25 = four
piece 26 =
*/

}/*@lineinfo:generated-code*/