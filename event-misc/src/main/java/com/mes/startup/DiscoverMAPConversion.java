/*@lineinfo:filename=DiscoverMAPConversion*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class DiscoverMAPConversion extends EventBase
{
  static Logger log = Logger.getLogger(DiscoverMAPConversion.class);
  
  public DiscoverMAPConversion()
  {
  }
  
  private boolean convertMerchant(long merchantNumber)
  {
    boolean result = false;
    try
    {
      log.debug("  converting: " + merchantNumber);
      
      /*@lineinfo:generated-code*//*@lineinfo:25^7*/

//  ************************************************************
//  #sql [Ctx] { call convert_discover_map(:merchantNumber)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN convert_discover_map( :1 )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.DiscoverMAPConversion",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:28^7*/
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry( "convertMerchant(" + merchantNumber+ ")", e.toString());
    }
    
    return( result );
  }
  
  private boolean convertBank(int bankNumber)
  {
    boolean result = true;
    ResultSetIterator it = null;
    ResultSet         rs = null;
    Vector            items = new Vector();
    
    try
    {
      connect();
      
      log.debug("loading entries for bank: " + bankNumber);
      /*@lineinfo:generated-code*//*@lineinfo:52^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number
//          from    mif mf
//          where   mf.bank_number = :bankNumber
//                  and mf.date_stat_chgd_to_dcb is null
//                  and nvl(mf.discover_map,'N') = 'N'
//                  and nvl(mf.dmdsnum, 0) != 0
//          order by mf.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number\n        from    mif mf\n        where   mf.bank_number =  :1 \n                and mf.date_stat_chgd_to_dcb is null\n                and nvl(mf.discover_map,'N') = 'N'\n                and nvl(mf.dmdsnum, 0) != 0\n        order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.DiscoverMAPConversion",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.DiscoverMAPConversion",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:61^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        items.add(rs.getLong("merchant_number"));
      }
      
      rs.close();
      it.close();
      
      for(int i = 0; i < items.size(); ++i)
      {
        long merchantNumber = ((Long)(items.elementAt(i))).longValue();
        
        if( convertMerchant(merchantNumber) == false )
        {
          result = false;
          break;
        }
        
        if( i % 1000 == 0 )
        {
          log.debug("committing and reconnecting");
          commit();
          cleanUp();
          connect();
        }
      }
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("convertBank(" + bankNumber + ")", e.toString());
      result = false;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }
  
  public boolean execute()
  {
    // convert banks
    try
    {
      convertBank(3943);
      convertBank(3858);
      convertBank(3941);
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    
    return( true );
  }
  
  public static void main(String[] args)
  {
    SQLJConnectionBase.initStandalone();
    DiscoverMAPConversion map = new DiscoverMAPConversion();
    
    map.execute();
  }
}/*@lineinfo:generated-code*/