/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/contact/UBOCContactBean.java $

  Description:

  PdfStatementServlet

  Generates a pdf document containing a merchant's monthly statement.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/25/02 4:47p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.contact;

import java.util.Vector;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;

public class UBOCContactBean extends com.mes.database.SQLJConnectionBase
{
  public  String    salutation      = "";
  public  String    firstName       = "";
  public  String    lastName        = "";
  public  String    businessName    = "";
  public  String    ubocMerchant    = "";
  public  String    merchantNumber  = "";
  public  String    phoneAreaCode   = "";
  public  String    phonePrefix     = "";
  public  String    phoneNumber     = "";
  public  String    email           = "";
  public  String    contactMessage  = "";
  public  String    referer         = "";
  public  int       emailId         = 0;

  public  boolean   submitted       = false;
  
  public  Vector    errors          = new Vector();

  public UBOCContactBean()
  {
  }
  
  public boolean isBlank(String test)
  {
    boolean result = false;
    
    if(test == null || test.equals(""))
    {
      result = true;
    }
    
    return result;
  }
  
  private boolean isNumber(String test)
  {
    boolean result = false;
    
    try
    {
      int blah = Integer.parseInt(test);
      result = true;
    }
    catch(Exception e)
    {
      result = false;
    }
    
    return result;
  }
  
  private void addError(String error)
  {
    errors.add(error);
  }
  
  public boolean validate()
  {
    try
    {
      if(isBlank(firstName))
      {
        addError("Please provide us with your First Name");
      }
    
      if(isBlank(lastName))
      {
        addError("Please provide us with your Last Name");
      }
    
      if(isBlank(phoneAreaCode) || isBlank(phonePrefix) || isBlank(phoneNumber) ||
         !isNumber(phoneAreaCode) || !isNumber(phonePrefix) || !isNumber(phoneNumber) ||
         phoneAreaCode.length() < 3 || phonePrefix.length() < 3 || phoneNumber.length() < 4)
      {
        addError("Please provide us with your complete phone number including area code");
      }
    
      if(isBlank(email))
      {
        addError("Please provide us with your email address");
      }
      else
      {
        // check for a valid email address
        int firstAt = email.indexOf('@');
        
        if( firstAt == 0 || 
            email.indexOf('@', firstAt + 1) != -1 ||
            email.indexOf('.', firstAt + 2) == -1)
        {
          addError("Please provide a valid email address");
        }
      }
    }
    catch(Exception e)
    {
      logEntry("validate()", e.toString());
    }
    
    return(errors.size() == 0);
  }
  
  public void submitData()
  {
    MailMessage     msg     = null;
    StringBuffer    body    = new StringBuffer("");
    
    try
    {
      if(
          isBlank(firstName) &&
          isBlank(lastName) &&
          isBlank(businessName) &&
          isBlank(ubocMerchant) &&
          isBlank(merchantNumber) &&
          isBlank(phoneAreaCode) &&
          isBlank(phonePrefix) &&
          isBlank(phoneNumber) &&
          isBlank(email) &&
          isBlank(contactMessage) )
      {
        // user didn't enter any info
      }
      else
      {
        // build and send the email
        body.append("Source: http://www.merchante-solutions.com/jsp/uboc/contact.jsp\n");
        
        if(emailId == MesEmails.MSG_ADDRS_UBOC_CONTACT_DEMO)
        {
          body.append("(Reached from demo page)");
        }
        
        body.append("\n\n");
        
        body.append("CONTACT INFORMATION:\n");
        body.append("\nSalutation:          ");
        body.append(salutation);
        body.append("\nName:                ");
        body.append(firstName);
        body.append(" ");
        body.append(lastName);
        body.append("\nBusiness Name:       ");
        body.append(businessName);
        body.append("\nUnion Bank Merchant? ");
        body.append(ubocMerchant);
        body.append("\nMerchant Number:     ");
        body.append(merchantNumber);
        body.append("\nPhone Number:        ");
        body.append(phoneAreaCode);
        body.append("-");
        body.append(phonePrefix);
        body.append("-");
        body.append(phoneNumber);
        body.append("\nEmail Address:       ");
        body.append(email);
        body.append("\n\nMessage:\n");
        body.append("----------------\n");
        body.append(contactMessage);
        
        msg = new MailMessage();
        msg.setAddresses(emailId);
        
        // set the from address to be the user's email address
        msg.setFrom(email);
        msg.setSubject("Merchant e-Solutions Contact Form Submission");
        msg.setText(body.toString());
        msg.send();
      }
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
    }
  }

  public void setSalutation(String salutation)
  {
    this.salutation = salutation;
  }
  public void setFirstName(String   firstName)
  {
    this.firstName = firstName;
  }
  public void setLastName(String lastName)
  {
    this.lastName = lastName;
  }
  public void setBusinessName(String businessName)
  {
    this.businessName = businessName;
  }
  public void setUbocMerchant(String ubocMerchant)
  {
    this.ubocMerchant = ubocMerchant;
  }
  public void setMerchantNumber(String merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  public void setPhoneAreaCode(String phoneAreaCode)
  {
    this.phoneAreaCode = phoneAreaCode;
  }
  public void setPhonePrefix(String phonePrefix)
  {
    this.phonePrefix = phonePrefix;
  }
  public void setPhoneNumber(String phoneNumber)
  {
    this.phoneNumber = phoneNumber;
  }
  public void setEmail(String email)
  {
    this.email = email;
  }
  public void setContactMessage(String contactMessage)
  {
    this.contactMessage = contactMessage;
  }
  public void setSubmit(String submit)
  {
    this.submitted = true;
  }
  public void setReferer(String referer)
  {
    this.referer = referer;
  }
  public void setEmailId(String emailId)
  {
    try
    {
      this.emailId = Integer.parseInt(emailId);
    }
    catch(Exception e)
    {
    }
  }
  public void setEmailId(int emailId)
  {
    this.emailId = emailId;
  }
}