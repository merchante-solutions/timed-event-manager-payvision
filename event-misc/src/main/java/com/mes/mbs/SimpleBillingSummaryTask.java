/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/mbs/SimpleBillingSummaryTask.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $LastChangedDate: 2010-04-08 11:00:32 -0700 (Thu, 08 Apr 2010) $
  Version            : $Revision: 17164 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.tools.ToolsDb;

//@import com.mes.startup.*;

public class SimpleBillingSummaryTask extends BillingSummaryTask
{
  protected   int     BillingType       = -1;
  
  public SimpleBillingSummaryTask( int billingType )
  {
    BillingType = billingType;
  }
  
  public boolean doTask( String loadFilename, java.sql.Date activityDate )
  {
    boolean             retVal          = true;
    BillingDb           billingDb       = null;
    BillingGroup        billingGroup    = null;
    Vector              billingGroups   = new Vector();
    String              classname       = null;
    MerchantBilling     mb              = null;
    PreparedStatement   ps              = null;
    ResultSet           rs              = null;
    SimpleSummaryData   summaryData     = new SimpleSummaryData();
  
    try
    {
      billingDb = new BillingDb();
      billingDb.connect();
      
      long loadFileId = ToolsDb.loadFilenameToLoadFileId(loadFilename);
    
      ps = billingDb.getPreparedStatement(billingGroup.getSqlText());
      
      // jack: if there's only going to be one "simple" class, it needs to get billingType in param list;
      //       another option is to make two "simple" classes, one for file and one for daily.
      if ( BillingType == BT_FILE_ACTIVITY )
      {
        ps.setLong(1,loadFileId);
      }
      else if ( BillingType == BT_DAILY_ACTIVITY )
      {
        ps.setDate(1,activityDate);
        ps.setDate(2,activityDate);
        ps.setDate(3,activityDate);
      }
      rs = ps.executeQuery();
      
      int lastMerchantId = -1;
      while( rs.next() )
      {
        Vector  billingElements = billingGroup.getBillingElements();
        Date    lastActivityDate = null;
        
        for ( int j = 0; j < billingElements.size(); ++j )
        {
          BillingElement billingElement = (BillingElement)billingElements.elementAt(j);
          
          // extract the count/amount data from the result set
          summaryData.clear();
          summaryData.setActivityDate( rs.getDate("activity_date") );
          summaryData.setDataSource( loadFilename );
          summaryData.setDataSourceId( loadFileId );
          summaryData.setItemAmount( rs.getDouble(billingElement.getAmountColumnName()) );
          summaryData.setItemCount( rs.getInt(billingElement.getCountColumnName()) );
          summaryData.setItemSubclass( rs.getString("item_subclass") );
          summaryData.setItemType( billingElement.getItemType() );
          summaryData.setMerchantId( rs.getLong("merchant_number") );
          
          // change in date or merchant, need to reload pricing
          if ( summaryData.getMerchantId() != lastMerchantId || 
               !summaryData.getActivityDate().equals(lastActivityDate) )
          {
            mb = billingDb._loadMerchantBilling(summaryData.getMerchantId(),summaryData.getActivityDate());
          }
          
          // calculate the fees due for this billing element
          if ( summaryData.getItemCount() != 0 || 
               summaryData.getItemAmount() != 0.0 )
          {
            MerchantBillingElement el = mb.findItemByType(summaryData.getItemType(),
                                                          summaryData.getItemSubclass());
            if ( el != null )
            {
              summaryData.setFeesDue( (summaryData.getItemCount() * el.getPerItem()) + 
                                      (summaryData.getItemAmount() * el.getRate() * 0.01) );
            }
            billingDb._storeSimpleSummaryData(summaryData);
          }
          
          lastActivityDate = summaryData.getActivityDate();
        }   // for loop
      }
      rs.close();
      ps.close();
    }      
    catch( Exception e )
    {
      retVal = false;
    }
    finally
    {
      try{ billingDb.cleanUp(); } catch (Exception ee){}
    }
    return( retVal );
  }
}
