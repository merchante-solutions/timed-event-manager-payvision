/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/mbs/IcGroupElement.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $LastChangedDate: 2010-01-11 10:24:23 -0800 (Mon, 11 Jan 2010) $
  Version            : $Revision: 16890 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

import java.sql.Date;
import java.sql.ResultSet;

public class IcGroupElement
{
  protected   String    GroupLabel          = null;
  protected   String    IcCat               = null;
  protected   String    PlanType            = null;
  protected   Date      ValidDateBegin      = null;
  protected   Date      ValidDateEnd        = null;
  
  
  public IcGroupElement( ResultSet resultSet )
    throws java.sql.SQLException
  {
    GroupLabel      = resultSet.getString( "group_label" );
    IcCat           = resultSet.getString( "ic_cat" );
    PlanType        = resultSet.getString( "plan_type" );
    ValidDateBegin  = resultSet.getDate  ( "valid_date_begin" );
    ValidDateEnd    = resultSet.getDate  ( "valid_date_end" );
  }
  
  public String getGroupLabel()     { return( GroupLabel );     }
  public String getIcCat()          { return( IcCat );          }
  public String getPlanType()       { return( PlanType );       }
  public Date   getValidDateBegin() { return( ValidDateBegin ); }
  public Date   getValidDateEnd()   { return( ValidDateEnd );   }
  
  public void setGroupLabel     ( String value ) { GroupLabel      = value; }
  public void setIcCat          ( String value ) { IcCat           = value; }
  public void setPlanType       ( String value ) { PlanType        = value; }
  public void setValidDateBegin ( Date   value ) { ValidDateBegin  = value; }
  public void setValidDateEnd   ( Date   value ) { ValidDateEnd    = value; }
}
