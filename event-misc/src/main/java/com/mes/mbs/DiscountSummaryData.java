/*************************************************************************

  FILE: $URL$

  Description:

  Last Modified By   : $Author$
  Last Modified Date : $LastChangedDate$
  Version            : $Revision$

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

public class DiscountSummaryData extends SimpleSummaryData
{
  protected   int     VolumeType    = -1;
  
  public DiscountSummaryData( )
  {
  }
  
  public void clear()
  {
    super.clear();
    
    VolumeType = -1;
  }

  public int      getVolumeType()             { return( VolumeType ); }
  public void     setVolumeType( int value )  { VolumeType = value; } 
}
