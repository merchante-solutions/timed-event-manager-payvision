/*@lineinfo:filename=ViewProspects*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/prospects/ViewProspects.sqlj $

  Description:
  
  ViewProspects
  
  Manages sales prospects.
  
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 10 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.prospects;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.TreeSet;
import com.mes.forms.SortableReport;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;


public class ViewProspects extends SortableReport
{
  private long                userId            = -1L;
  
  // sort by types (which report column to sort by)
  public static final int     SB_MERCH_DBA      = 1;
  public static final int     SB_APP_SEQ_NUM    = 2;
  public static final int     SB_OUTLOOK        = 3;
  public static final int     SB_STATUS         = 4;
  public static final int     SB_CREATE_DATE    = 5;
  public static final int     SB_LAST_DEV       = 6;
  public static final int     SB_CALL_COUNT     = 7;
  public static final int     SB_CALL_TIME      = 8;
  public static final int     SB_MILEAGE        = 9;
  
  private TreeSet prospects;

  public class ProspectRecord implements Comparable
  {
    private SortByType  sbt;
    
    private long        appSeqNum;
    private String      merchantDba;
    private int         outlook;
    private int         status;
    private Timestamp   createDate;
    private Timestamp   lastDevDate;
    private int         callMinutes;
    private int         callCount;
    private int         callMileage;
    
    /*
    ** public ProspectRecord(ResultSet rs, SortByType sbt)
    **
    ** Constructor.
    **
    ** Takes a SortByType object and a result set.  A row of data is extracted
    ** from the result set and stored.
    */
    public ProspectRecord(ResultSet rs, SortByType sbt)
    {
      this.sbt = sbt;
      try
      {
        merchantDba = rs.getString    ("merchant_dba");
        appSeqNum   = rs.getLong      ("app_seq_num");
        callMinutes = rs.getInt       ("call_minutes");
        callCount   = rs.getInt       ("call_count");
        callMileage = rs.getInt       ("call_mileage");
        outlook     = rs.getInt       ("outlook");
        status      = rs.getInt       ("status");
        createDate  = rs.getTimestamp ("create_date");
        lastDevDate = rs.getTimestamp ("last_dev");
      }
      catch (Exception e)
      {
        merchantDba = e.toString();
      }
    }
    
    /*
    ** public int compareTo(Object that)
    **
    ** Based on the current sbt sort by value a -1, 0 or 1 is returned to
    ** indicate if the relevant column in that object is less than, equal to
    ** or greater than this objects matching column.
    **
    ** RETURNS: -1, 0 or 1 depending of if that object is less than, equal to
    **          or greater than this object.
    */
    public int compareTo(Object that)
    {
      ProspectRecord thatRow = (ProspectRecord)that;
      int result = 0;
    
      switch (sbt.getSortBy())
      {
        case SB_APP_SEQ_NUM:
          break;
          
        case SB_MERCH_DBA:
        default:
          result = compare(merchantDba,thatRow.merchantDba);
          break;
          
        case SB_OUTLOOK:
          result = compare(outlook,thatRow.outlook);
          break;
          
        case SB_STATUS:
          result = compare(status,thatRow.status);
          break;
          
        case SB_CREATE_DATE:
          result = compare(createDate,thatRow.createDate);
          break;
          
        case SB_LAST_DEV:
          result = compare(lastDevDate,thatRow.lastDevDate);
          break;
          
        case SB_CALL_COUNT:
          result = compare(callCount,thatRow.callCount);
          break;
          
        case SB_CALL_TIME:
          result = compare(callMinutes,thatRow.callMinutes);
          break;
          
        case SB_MILEAGE:
          result = compare(callMileage,thatRow.callMileage);
          break;
      }
        
      // always sort by appSeqNum if equal by other column
      if (result == 0)
      {
        result = compare(appSeqNum,thatRow.appSeqNum);
      }
          
      // if reverse sort specified, flip the result around
      if (sbt.getReverseSort())
      {
        result *= (-1);
      }
      
      return result;
    }
    
    public long   getAppSeqNum()    { return appSeqNum; }
    public int    getCallMinutes()  { return callMinutes; }
    public int    getCallCount()    { return callCount; }
    public int    getCallMileage()  { return callMileage; }
    public String getMerchantDba()  { return merchantDba; }
    
    public String getOutlook()
    {
      String outlook = "unknown";
      try 
      {
        outlook = DevelopProspect.qualities[this.outlook];
      }
      catch (Exception e) {}
      return outlook;
    }
    
    public String getStatus()
    {
      String status = "unknown";
      try 
      {
        status = DevelopProspect
          .getStatusDescription(Integer.toString(this.status));
      }
      catch (Exception e) {}
      return status;
    }
    
    public String getCreateDate()
    {
      return DateTimeFormatter
        .getFormattedDate(createDate,DateTimeFormatter.DEFAULT_DATE_FORMAT);
    }
          
    public String getLastDev()
    {
      return DateTimeFormatter
        .getFormattedDate(lastDevDate,DateTimeFormatter.DEFAULT_DATE_FORMAT);
    }
    
    public SortByType getSbt()
    {
      return sbt;
    }
  }
  
  /*
  ** public boolean load()
  **
  ** Loads all of user's prospects, stores them in prospects (a TreeSet).
  **
  ** RETURNS: true if no errors during load, else false.
  */
  public boolean load()
  {
    boolean loadOk = false;
    
    try
    {
      connect();
      
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:234^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    p.app_seq_num,
//                    p.merchant_dba,
//                    p.create_date,
//                    lpd.status,
//                    lpd.outlook,
//                    lpd.last_dev,
//                    cpd.call_minutes,
//                    cpd.call_count,
//                    cpd.call_mileage
//          
//          from      prospects p,
//                    ( select    quality   outlook,
//                                dev_date  last_dev,
//                                status,
//                                app_seq_num
//                      from      prospect_developments
//                      where     ( dev_date, app_seq_num ) in (  select    max(dev_date), 
//                                                                          app_seq_num
//                                                                from      prospect_developments 
//                                                                group by  app_seq_num ) ) lpd,
//                    ( select    sum(call_duration)  call_minutes,
//                                count(*)            call_count,
//                                sum(call_mileage)   call_mileage,
//                                app_seq_num
//                      from      prospect_developments
//                      where     call_duration > 0 
//                      group by  app_seq_num) cpd
//            
//          where     user_id = :userId
//                    and p.app_seq_num = lpd.app_seq_num (+)
//                    and p.app_seq_num = cpd.app_seq_num (+)
//  
//          order by  p.merchant_dba
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    p.app_seq_num,\n                  p.merchant_dba,\n                  p.create_date,\n                  lpd.status,\n                  lpd.outlook,\n                  lpd.last_dev,\n                  cpd.call_minutes,\n                  cpd.call_count,\n                  cpd.call_mileage\n        \n        from      prospects p,\n                  ( select    quality   outlook,\n                              dev_date  last_dev,\n                              status,\n                              app_seq_num\n                    from      prospect_developments\n                    where     ( dev_date, app_seq_num ) in (  select    max(dev_date), \n                                                                        app_seq_num\n                                                              from      prospect_developments \n                                                              group by  app_seq_num ) ) lpd,\n                  ( select    sum(call_duration)  call_minutes,\n                              count(*)            call_count,\n                              sum(call_mileage)   call_mileage,\n                              app_seq_num\n                    from      prospect_developments\n                    where     call_duration > 0 \n                    group by  app_seq_num) cpd\n          \n        where     user_id =  :1 \n                  and p.app_seq_num = lpd.app_seq_num (+)\n                  and p.app_seq_num = cpd.app_seq_num (+)\n\n        order by  p.merchant_dba";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.prospects.ViewProspects",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.prospects.ViewProspects",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:269^7*/
      ResultSet rs = it.getResultSet();
      
      prospects = new TreeSet();
      SortByType sbt = new SortByType(SB_MERCH_DBA);
      while (rs.next())
      {
        prospects.add(new ProspectRecord(rs,sbt));
      }
      
      rs.close();
      it.close();
      
      loadOk = true;
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::load(userId = " 
        + userId + ")";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
    return loadOk;
  }
  
  /*
  ** public TreeSet getProspects(int sortBy)
  **
  ** Attempts to load the prospects TreeSet with user's prospects.  If
  ** successful, the a new set is created with the specified sort order.
  **
  ** RETURNS: TreeSet reference to the prospect records sorted by the
  **          specified order.
  */
  public TreeSet getProspects(int sortBy)
  {
    TreeSet sortedSet = new TreeSet();
    try
    {
      if (load() && prospects.size() > 0)
      {
        ProspectRecord first = (ProspectRecord)prospects.first();
        first.getSbt().setSortBy(sortBy);
        for (Iterator i = prospects.iterator(); i.hasNext();)
        {
          sortedSet.add(i.next());
        }
      }
    }
    catch (Exception e) {}
    return sortedSet;
  }
  
  public void setUserId(String userId)
  {
    try
    {
      setUserId(Long.parseLong(userId));
    }
    catch(Exception e)
    {
      logEntry("setUserId(" + userId + ")", e.toString());
    }
  }
  public void setUserId(long userId)
  {
    this.userId = userId;
  }
}/*@lineinfo:generated-code*/