/*@lineinfo:filename=PipelineReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/prospects/PipelineReport.sqlj $

  Description:
  
  PipelineReport
  
  Generates prospect pipeline report data.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/25/02 11:24a $
  Version            : $Revision: 13 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.prospects;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import com.mes.constants.MesHierarchy;
import com.mes.forms.ComboDateField;
import com.mes.support.DateTimeFormatter;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class PipelineReport extends ProspectReport
{
  private ReportVector  reportData;
  private boolean       isLoaded;
  
  public PipelineReport(UserBean user)
  {
    super(user);
  }
  
  /**************************************************************************
  **
  ** Report Data Containers
  **
  **************************************************************************/

  public class ReportVector extends Vector
  {
    private Hashtable directReports = new Hashtable();

    public DirectReportVector addDirectReport(long drUserId, String drName)
    {
    	DirectReportVector directReport = (DirectReportVector)directReports.get(drUserId);
      if (directReport == null)
      {
        directReport = new DirectReportVector(drUserId,drName);
        directReports.put(drUserId, directReport);
        add(directReport);
      }
      
      return directReport;
    }

    public boolean addProspectRecord(ProspectRecord rec)
    {
      DirectReportVector dr = addDirectReport(rec.getDrUserId(),rec.getDrName());
      return dr.add(rec);
    }
  }
  
  public class DirectReportVector extends Vector
  {
    private long      drId;
    private String    drName;
    private Hashtable statuses = new Hashtable();
    
    public DirectReportVector(long drId, String drName)
    {
      this.drId = drId;
      this.drName = drName;
    }
    
    public long getDrId()
    {
      return drId;
    }
    public String getDrName()
    {
      return drName;
    }
    
    public boolean add(ProspectRecord rec)
    {
      String key = rec.getStatus();
      
      
      StatusVector status = (StatusVector)statuses.get(key);
      if (status == null)
      {
        status = new StatusVector(rec.getStatus());
        statuses.put(key,status);
        add(status);
      }
      
      return status.add(rec);
    }
    
    public String[] getStatusStrings()
    {
      String[] strs = new String[size()];
      int cnt = 0;
      for (Iterator i = iterator(); i.hasNext();)
      {
        strs[cnt++] = ((StatusVector)i.next()).getStatus();
      }
      return strs;
    }
    
    public StatusVector get(String status)
    {
      return (StatusVector)statuses.get(status);
    }
  }
  
  public class StatusVector extends Vector
  {
    private String status;
    
    public StatusVector(String status)
    {
      this.status = status;
    }
    
    public String getStatus()
    {
      return status;
    }
  }
  
  public class ProspectRecord
  {
    private String  drName;
    private String  repName;
    private String  merchantDba;
    private String  contactName;
    private String  outlook;
    private String  status;
    private String  createDate;
    private String  lastDevDate;
    private long    appSeqNum;
    private long    drUserId;
    private long    repUserId;
    private float   annualSales;
    private float   discountRevenue;
    
    private String error;
    
    private String getDescription(int idx, String[] list)
    {
      try
      {
        return list[idx];
      }
      catch (Exception e) {}
      
      return "[unknown]";
    }
    public ProspectRecord(ResultSet rs)
    {
      try
      {
        float discountRate;
        float centsPerTran;
        float averageTicket;
        appSeqNum       = rs.getLong        ("app_seq_num");
        drUserId        = rs.getLong        ("dr_user_id");
        drName          = rs.getString      ("dr_name");
        repUserId       = rs.getLong        ("rep_user_id");
        repName         = rs.getString      ("rep_name");
        merchantDba     = rs.getString      ("merchant_dba");
        contactName     = rs.getString      ("contact_name");
        annualSales     = rs.getFloat       ("annual_sales");
        averageTicket   = rs.getFloat       ("average_ticket");
        discountRate    = rs.getFloat       ("discount_rate");
        centsPerTran    = rs.getFloat       ("cents_per_tran");
        outlook         = getDescription(rs.getInt("outlook"),DevelopProspect.qualities);
        status          = DevelopProspect.getStatusDescription(rs.getString("status"));
        createDate      = DateTimeFormatter
          .getFormattedDate(rs.getTimestamp ("create_date"),
            DateTimeFormatter.DEFAULT_DATE_FORMAT);
        lastDevDate     = DateTimeFormatter
          .getFormattedDate(rs.getTimestamp ("last_dev_date"),
            DateTimeFormatter.DEFAULT_DATE_FORMAT);
        
        
        // calculated projected revenue
        long temp = (long)((annualSales * 100) * (discountRate / 100));
        float projectedDiscount = (temp / 100);
        temp = (long)((annualSales / averageTicket) * (centsPerTran * 100));
        projectedDiscount += (temp / 100);

        discountRevenue = projectedDiscount;
      }
      catch (Exception e)
      {
        error = e.toString();
      }
    }
    
    public long   getAppSeqNum()        { return appSeqNum; }
    public long   getDrUserId()         { return drUserId; }
    public String getDrName()           { return drName; }
    public long   getRepUserId()        { return repUserId; }
    public String getRepName()          { return repName; }
    public String getMerchantDba()      { return merchantDba; }
    public String getContactName()      { return contactName; }
    public float  getAnnualSales()      { return annualSales; }
    public float  getDiscountRevenue()  { return discountRevenue; }
    public String getOutlook()          { return outlook; }
    public String getStatus()           { return status; }
    public String getCreateDate()       { return createDate; }
    public String getLastDevDate()      { return lastDevDate; }
    
    public String getError()            { return error; }
  }
  
  /**************************************************************************
  **
  ** Database IO
  **
  **************************************************************************/

  /*
  ** public boolean loadProspectData()
  **
  ** Loads prospect data into a result set.
  **
  ** RETURNS: ResultSet containing query results, null if failed.
  */
  public ResultSet loadProspectData(long headId)
  {
    ResultSet rs = null;
    
    try
    {
      java.sql.Date fromDate = 
        ((ComboDateField)fields.getField("fromDate")).getSqlDate();
      java.sql.Date toDate = 
        ((ComboDateField)fields.getField("toDate")).getSqlDate();
      Calendar cal = Calendar.getInstance();
      cal.setTime(toDate);
      cal.add(Calendar.DATE,1);
      toDate = new java.sql.Date(cal.getTime().getTime());
      
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:272^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    p.app_seq_num,
//                    h1.descendent           dr_user_id,
//                    h2.descendent           rep_user_id,
//                    p.merchant_dba,
//                    p.contact_name,
//                    p.annual_sales,
//                    p.average_ticket,
//                    nvl(p.discount_rate,0)  discount_rate,
//                    nvl(p.cents_per_tran,0) cents_per_tran,
//                    ld.outlook,
//                    ld.status,
//                    p.create_date,
//                    ld.dev_date             last_dev_date,
//                    hn1.name                dr_name,
//                    hn2.name                rep_name
//                  
//          from      prospects   p,
//                    t_hierarchy h1,
//                    t_hierarchy h2,
//                    ( select    quality   outlook,
//                                status,
//                                dev_date,
//                                app_seq_num
//                      from      prospect_developments
//                      where     ( dev_date, app_seq_num ) in (  select    max(dev_date), 
//                                                                          app_seq_num
//                                                                from      prospect_developments 
//                                                                group by  app_seq_num ) ) ld,
//                    t_hierarchy_names hn1,
//                    t_hierarchy_names hn2
//  
//          where     h1.ancestor = :headId
//                    and h1.hier_type = :MesHierarchy.HT_PROSPECT_DB
//                    and h2.hier_type = :MesHierarchy.HT_PROSPECT_DB
//                    and ( ( h1.relation = 1 
//  		  	  	              and h1.descendent = h2.ancestor )
//  		  	  	         or ( h1.relation = 0 
//  				   	              and h1.ancestor = h2.descendent 
//  					                and h1.ancestor = h2.ancestor ) )
//                    and h2.descendent = p.user_id 
//                    and p.create_date between :fromDate and :toDate
//                    and p.app_seq_num = ld.app_seq_num
//                    and  h2.ancestor = hn1.hier_id
//                    and  h2.descendent = hn2.hier_id
//                  
//          order by  h2.ancestor,
//                    ld.status,
//                    h2.descendent, 
//                    ld.outlook
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    p.app_seq_num,\n                  h1.descendent           dr_user_id,\n                  h2.descendent           rep_user_id,\n                  p.merchant_dba,\n                  p.contact_name,\n                  p.annual_sales,\n                  p.average_ticket,\n                  nvl(p.discount_rate,0)  discount_rate,\n                  nvl(p.cents_per_tran,0) cents_per_tran,\n                  ld.outlook,\n                  ld.status,\n                  p.create_date,\n                  ld.dev_date             last_dev_date,\n                  hn1.name                dr_name,\n                  hn2.name                rep_name\n                \n        from      prospects   p,\n                  t_hierarchy h1,\n                  t_hierarchy h2,\n                  ( select    quality   outlook,\n                              status,\n                              dev_date,\n                              app_seq_num\n                    from      prospect_developments\n                    where     ( dev_date, app_seq_num ) in (  select    max(dev_date), \n                                                                        app_seq_num\n                                                              from      prospect_developments \n                                                              group by  app_seq_num ) ) ld,\n                  t_hierarchy_names hn1,\n                  t_hierarchy_names hn2\n\n        where     h1.ancestor =  :1 \n                  and h1.hier_type =  :2 \n                  and h2.hier_type =  :3 \n                  and ( ( h1.relation = 1 \n\t\t  \t  \t              and h1.descendent = h2.ancestor )\n\t\t  \t  \t         or ( h1.relation = 0 \n\t\t\t\t   \t              and h1.ancestor = h2.descendent \n\t\t\t\t\t                and h1.ancestor = h2.ancestor ) )\n                  and h2.descendent = p.user_id \n                  and p.create_date between  :4  and  :5 \n                  and p.app_seq_num = ld.app_seq_num\n                  and  h2.ancestor = hn1.hier_id\n                  and  h2.descendent = hn2.hier_id\n                \n        order by  h2.ancestor,\n                  ld.status,\n                  h2.descendent, \n                  ld.outlook";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.prospects.PipelineReport",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,headId);
   __sJT_st.setInt(2,MesHierarchy.HT_PROSPECT_DB);
   __sJT_st.setInt(3,MesHierarchy.HT_PROSPECT_DB);
   __sJT_st.setDate(4,fromDate);
   __sJT_st.setDate(5,toDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.prospects.PipelineReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:323^7*/
      rs = it.getResultSet();
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::loadProspectData()";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }

    return rs;
  }
  
  public boolean load()
  {
    boolean loadOk = false;
    
    try
    {
      connect();

      long headId = Long.parseLong(fields.getData("headId"));

      // get direct children of node, including self
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:349^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  h.descendent,
//                  hn.name
//                  
//          from    t_hierarchy h,
//                  t_hierarchy_names hn
//                  
//          where   h.ancestor = :headId
//                  and h.relation < 2
//                  and h.entity_type <> 3
//                  and h.hier_type = :MesHierarchy.HT_PROSPECT_DB
//                  and h.descendent = hn.hier_id
//                  
//          order by h.relation asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  h.descendent,\n                hn.name\n                \n        from    t_hierarchy h,\n                t_hierarchy_names hn\n                \n        where   h.ancestor =  :1 \n                and h.relation < 2\n                and h.entity_type <> 3\n                and h.hier_type =  :2 \n                and h.descendent = hn.hier_id\n                \n        order by h.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.prospects.PipelineReport",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,headId);
   __sJT_st.setInt(2,MesHierarchy.HT_PROSPECT_DB);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.prospects.PipelineReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:364^7*/
      ResultSet rs = it.getResultSet();
      
      // build a report vector, add direct reports
      reportData = new ReportVector();
      while (rs.next())
      {
        long    drId    = rs.getLong("descendent");
        String  drName  = rs.getString("name");
        reportData.addDirectReport(drId,drName);
        if (drId == headId)
        {
          headName = drName;
        }
      }
      rs.close();
      
      // load prospect data under the direct reports
      rs = loadProspectData(headId);
      while (rs.next())
      {
        reportData.addProspectRecord(new ProspectRecord(rs));
      }
      rs.close();
      
      loadOk = true;
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::load()";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
    isLoaded = loadOk;
    return loadOk;
  }
  
  /*
  ** public Vector getReportData()
  **
  ** Loads prospect report data if not already loaded, returns the data.
  **
  ** RETURNS: a Vector containing ProspectRecords.
  */
  public Vector getReportData()
  {
    if (isLoaded || load())
    {
      return reportData;
    }
    return null;
  }
  
  /**************************************************************************
  **
  ** Validation
  **
  **************************************************************************/

  /**************************************************************************
  **
  ** Static Convenience Methods
  **
  **************************************************************************/
}/*@lineinfo:generated-code*/