/*@lineinfo:filename=AssignProspect*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/prospects/AssignProspect.sqlj $

  Description:
  
  AssignProspect
  
  Manages sales prospects.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/25/02 11:24a $
  Version            : $Revision: 21 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.prospects;

import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.Condition;
import com.mes.forms.CurrencyField;
import com.mes.forms.DiscountField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;

public class AssignProspect extends FieldBean
{
  /**************************************************************************
  **
  ** Data Field Helpers
  **
  **************************************************************************/
  
  private class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      addElement("","-- select --");
      addElement("1","Sole Prop");
      addElement("2","Corporation");
      addElement("3","Partnership");
      addElement("4","Med/Lgl Corp");
      addElement("5","Est/Trust");
      addElement("6","Tax Exempt");
      addElement("7","Government");
      addElement("8","Lmtd Liab Co");
    }
  }
  
  private class IndustryTable extends DropDownTable
  {
    public IndustryTable()
    {
      addElement("","-- select --");
      addElement("1","Retail");
      addElement("2","Restaurant");
      addElement("3","Hotel");
      addElement("4","Motel");
      addElement("5","Services");
      addElement("6","Internet");
      addElement("7","Dir Mrkt");
      addElement("8","Other");
    }
  }
  
  private class LocationTable extends DropDownTable
  {
    public LocationTable()
    {
      addElement("","-- select --");
      addElement("1","Retail Storefront");
      addElement("2","Internet Storefront");
      addElement("3","Business Office");
      addElement("4","Private Residence");
      addElement("5","Other");
      addElement("6","Bank");
    }
  }
  
  private class YesNoTable extends DropDownTable
  {
    public YesNoTable()
    {
      addElement("","--");
      addElement("Y","Yes");
      addElement("N","No");
    }
  }
  
  public AssignProspect(UserBean user)
  {
    super(user);

    StateDropDownTable stateTable = new StateDropDownTable();
    
    // hidden fields
    fields.add(new HiddenField  ("appSeqNum"));
    fields.add(new HiddenField  ("userId"));
    
    // prospect info fields
    fields.add(new Field        ("merchantDBA",           25,   0,  false));
    fields.add(new Field        ("address1",              32,   0,  false));
    fields.add(new Field        ("address2",              32,   0,  true));
    fields.add(new Field        ("city",                  25,   0,  false));
    fields.add(new DropDownField("state",                 stateTable, false));
    fields.add(new Field        ("zip",                   9,    0,  false));
    fields.add(new CheckboxField("hasMail",               false));
    
    fields.add(new Field        ("contactName",           40,   32, false));
    fields.add(new PhoneField   ("contactPhone",          false));
    fields.add(new EmailField   ("contactEmail", true));
    
    // create subgroup for separate mailing address fields that is
    // only validated if hasMail checkbox is checked
    Field fHasMail = fields.getField("hasMail");
    Condition cHasMail = new FieldValueCondition(fHasMail,"y");
    FieldGroup addressFields = new FieldGroup("addressFields",cHasMail);
    addressFields.add(new Field        ("mailName",              25,   0,  true));
    addressFields.add(new Field        ("mailAddress1",          32,   0,  true));
    addressFields.add(new Field        ("mailCity",              25,   0,  true));
    addressFields.add(new DropDownField("mailState",             stateTable, true));
    addressFields.add(new Field        ("mailZip",               9,    0,  true));
    addressFields
      .addGroupValidation(new ConditionalRequiredValidation(cHasMail));
    // mail address 2 is always optional, so add it after setting the
    // conditional validation for the rest of the group
    addressFields.add(new Field        ("mailAddress2",          32,   0,  true));

    fields.add(addressFields);
    
    fields.add(new DropDownField("businessType",          new BusinessTypeTable(), false));
    fields.add(new DropDownField("businessIndustry",      new IndustryTable(), false));
    fields.add(new NumberField  ("percentMoto",           3,    3,  false, 0));
    fields.add(new DropDownField("businessLocation",      new LocationTable(), false));
    fields.add(new NumberField  ("businessLocationCount", 3,    0,  false, 0));
    fields.add(new TextareaField("businessDescription",   200,  3,  50, false));
    
    fields.add(new CurrencyField("monthlySales",          13,   9,  false));
    fields.add(new CurrencyField("annualSales",           13,   9,  false));
    fields.add(new CurrencyField("averageTicket",         10,   6,  false));
    fields.add(new DiscountField("discountRate",          true));
    fields.add(new CurrencyField("centsPerTran",          4,    5,  true));
    
    fields.add(new DropDownField("priorHistory",          new YesNoTable(), false));
    fields.add(new Field        ("changeReason",          200,  60, true));
    
    fields.add(new NumberField  ("repCode",               5,    5,  true, 0));
    fields.add(new NumberField  ("officerNumber",         5,    5,  true, 0));
    fields.add(new NumberField  ("profitCenter",          7,    7,  true, 0));
    fields.add(new DropDownField("assignRep",             new AssignRepDropDownTable(user), false));
    
    // submit button
    fields.add(new ButtonField  ("save"));

    fields.setGroupHtmlExtra("class=\"small\"");
  }
  
  /**************************************************************************
  **
  ** Database IO
  **
  **************************************************************************/
  
  /*
  ** public boolean store()
  **
  ** Stores the prospect data in the prospects table.
  **
  ** RETURNS: true if data was stored successfully else false.
  */
  public boolean store()
  {
    boolean storeOk = false;
    
    long appSeqNum  = getAppSeqNum();
    long userId     = getUserId();
      
    try
    {
      connect();

      // insert new records if new
      if ((appSeqNum == -1L))
      {
        // load an application sequence number
        /*@lineinfo:generated-code*//*@lineinfo:201^9*/

//  ************************************************************
//  #sql [Ctx] { select  application_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  application_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.prospects.AssignProspect",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:206^9*/
        
        // insert a new prospect record
        /*@lineinfo:generated-code*//*@lineinfo:209^9*/

//  ************************************************************
//  #sql [Ctx] { insert into prospects
//            ( app_seq_num,
//              user_id,
//              create_date,
//              
//              merchant_dba,
//              address1,
//              address2,
//              city,
//              state,
//              zip,
//              
//              contact_name,
//              contact_phone,
//              contact_email,
//              
//              mail_name,
//              mail_address1,
//              mail_address2,
//              mail_city,
//              mail_state,
//              mail_zip,
//              
//              business_type,
//              business_industry,
//              percent_moto,
//              business_description,
//              business_location,
//              business_location_count,
//              
//              monthly_sales,
//              annual_sales,
//              average_ticket,
//              discount_rate,
//              cents_per_tran,
//              
//              rep_code,
//              officer_number,
//              profit_center,
//              
//              prior_history,
//              change_reason )
//              
//            values
//            ( :appSeqNum,
//              :fields.getData("assignRep"),
//              sysdate,
//              
//              :fields.getData("merchantDBA"),
//              :fields.getData("address1"),
//              :fields.getData("address2"),
//              :fields.getData("city"),
//              :fields.getData("state"),
//              :fields.getData("zip"),
//              
//              :fields.getData("contactName"),
//              :fields.getData("contactPhone"),
//              :fields.getData("contactEmail"),
//              
//              :fields.getData("mailName"),
//              :fields.getData("mailAddress1"),
//              :fields.getData("mailAddress2"),
//              :fields.getData("mailCity"),
//              :fields.getData("mailState"),
//              :fields.getData("mailZip"),
//              
//              :fields.getData("businessType"),
//              :fields.getData("businessIndustry"),
//              :fields.getData("percentMoto"),
//              :fields.getData("businessDescription"),
//              :fields.getData("businessLocation"),
//              :fields.getData("businessLocationCount"),
//              
//              :fields.getData("monthlySales"),
//              :fields.getData("annualSales"),
//              :fields.getData("averageTicket"),
//              :fields.getData("discountRate"),
//              :fields.getData("centsPerTran"),
//              
//              :fields.getData("repCode"),
//              :fields.getData("officerNumber"),
//              :fields.getData("profitCenter"),
//              
//              :fields.getData("priorHistory"),
//              :fields.getData("changeReason") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_283 = fields.getData("assignRep");
 String __sJT_284 = fields.getData("merchantDBA");
 String __sJT_285 = fields.getData("address1");
 String __sJT_286 = fields.getData("address2");
 String __sJT_287 = fields.getData("city");
 String __sJT_288 = fields.getData("state");
 String __sJT_289 = fields.getData("zip");
 String __sJT_290 = fields.getData("contactName");
 String __sJT_291 = fields.getData("contactPhone");
 String __sJT_292 = fields.getData("contactEmail");
 String __sJT_293 = fields.getData("mailName");
 String __sJT_294 = fields.getData("mailAddress1");
 String __sJT_295 = fields.getData("mailAddress2");
 String __sJT_296 = fields.getData("mailCity");
 String __sJT_297 = fields.getData("mailState");
 String __sJT_298 = fields.getData("mailZip");
 String __sJT_299 = fields.getData("businessType");
 String __sJT_300 = fields.getData("businessIndustry");
 String __sJT_301 = fields.getData("percentMoto");
 String __sJT_302 = fields.getData("businessDescription");
 String __sJT_303 = fields.getData("businessLocation");
 String __sJT_304 = fields.getData("businessLocationCount");
 String __sJT_305 = fields.getData("monthlySales");
 String __sJT_306 = fields.getData("annualSales");
 String __sJT_307 = fields.getData("averageTicket");
 String __sJT_308 = fields.getData("discountRate");
 String __sJT_309 = fields.getData("centsPerTran");
 String __sJT_310 = fields.getData("repCode");
 String __sJT_311 = fields.getData("officerNumber");
 String __sJT_312 = fields.getData("profitCenter");
 String __sJT_313 = fields.getData("priorHistory");
 String __sJT_314 = fields.getData("changeReason");
   String theSqlTS = "insert into prospects\n          ( app_seq_num,\n            user_id,\n            create_date,\n            \n            merchant_dba,\n            address1,\n            address2,\n            city,\n            state,\n            zip,\n            \n            contact_name,\n            contact_phone,\n            contact_email,\n            \n            mail_name,\n            mail_address1,\n            mail_address2,\n            mail_city,\n            mail_state,\n            mail_zip,\n            \n            business_type,\n            business_industry,\n            percent_moto,\n            business_description,\n            business_location,\n            business_location_count,\n            \n            monthly_sales,\n            annual_sales,\n            average_ticket,\n            discount_rate,\n            cents_per_tran,\n            \n            rep_code,\n            officer_number,\n            profit_center,\n            \n            prior_history,\n            change_reason )\n            \n          values\n          (  :1 ,\n             :2 ,\n            sysdate,\n            \n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n            \n             :9 ,\n             :10 ,\n             :11 ,\n            \n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n            \n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n            \n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n            \n             :29 ,\n             :30 ,\n             :31 ,\n            \n             :32 ,\n             :33  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.prospects.AssignProspect",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_283);
   __sJT_st.setString(3,__sJT_284);
   __sJT_st.setString(4,__sJT_285);
   __sJT_st.setString(5,__sJT_286);
   __sJT_st.setString(6,__sJT_287);
   __sJT_st.setString(7,__sJT_288);
   __sJT_st.setString(8,__sJT_289);
   __sJT_st.setString(9,__sJT_290);
   __sJT_st.setString(10,__sJT_291);
   __sJT_st.setString(11,__sJT_292);
   __sJT_st.setString(12,__sJT_293);
   __sJT_st.setString(13,__sJT_294);
   __sJT_st.setString(14,__sJT_295);
   __sJT_st.setString(15,__sJT_296);
   __sJT_st.setString(16,__sJT_297);
   __sJT_st.setString(17,__sJT_298);
   __sJT_st.setString(18,__sJT_299);
   __sJT_st.setString(19,__sJT_300);
   __sJT_st.setString(20,__sJT_301);
   __sJT_st.setString(21,__sJT_302);
   __sJT_st.setString(22,__sJT_303);
   __sJT_st.setString(23,__sJT_304);
   __sJT_st.setString(24,__sJT_305);
   __sJT_st.setString(25,__sJT_306);
   __sJT_st.setString(26,__sJT_307);
   __sJT_st.setString(27,__sJT_308);
   __sJT_st.setString(28,__sJT_309);
   __sJT_st.setString(29,__sJT_310);
   __sJT_st.setString(30,__sJT_311);
   __sJT_st.setString(31,__sJT_312);
   __sJT_st.setString(32,__sJT_313);
   __sJT_st.setString(33,__sJT_314);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:296^9*/
        
        // insert referred to rep development record
        /*@lineinfo:generated-code*//*@lineinfo:299^9*/

//  ************************************************************
//  #sql [Ctx] { insert into prospect_developments
//          ( app_seq_num,
//            user_id,
//            dev_date,
//            status,
//            quality,
//            call_method,
//            call_date,
//            call_duration,
//            call_mileage,
//            notes )
//          values
//          ( :appSeqNum,
//            :userId,
//            sysdate,
//            0,
//            0,
//            0,
//            sysdate,
//            0,
//            0,
//            :"Referred to rep by " + user.getUserName() )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_315 = "Referred to rep by " + user.getUserName();
   String theSqlTS = "insert into prospect_developments\n        ( app_seq_num,\n          user_id,\n          dev_date,\n          status,\n          quality,\n          call_method,\n          call_date,\n          call_duration,\n          call_mileage,\n          notes )\n        values\n        (  :1 ,\n           :2 ,\n          sysdate,\n          0,\n          0,\n          0,\n          sysdate,\n          0,\n          0,\n           :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.prospects.AssignProspect",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,userId);
   __sJT_st.setString(3,__sJT_315);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:323^9*/
      }
      // update existing records
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:328^9*/

//  ************************************************************
//  #sql [Ctx] { update  prospects
//            
//            set     merchant_dba            = :fields.getData("merchantDBA"),
//                    address1                = :fields.getData("address1"),
//                    address2                = :fields.getData("address2"),
//                    city                    = :fields.getData("city"),
//                    state                   = :fields.getData("state"),
//                    zip                     = :fields.getData("zip"),
//                    
//                    contact_name            = :fields.getData("contactName"),
//                    contact_phone           = :fields.getData("contactPhone"),
//                    contact_email           = :fields.getData("contactEmail"),
//                    
//                    mail_name               = :fields.getData("mailName"),
//                    mail_address1           = :fields.getData("mailAddress1"),
//                    mail_address2           = :fields.getData("mailAddress2"),
//                    mail_city               = :fields.getData("mailCity"),
//                    mail_state              = :fields.getData("mailState"),
//                    mail_zip                = :fields.getData("mailZip"),
//                    
//                    business_type           = :fields.getData("businessType"),
//                    business_industry       = :fields.getData("businessIndustry"),
//                    percent_moto            = :fields.getData("percentMoto"),
//                    business_description    = :fields.getData("businessDescription"),
//                    business_location       = :fields.getData("businessLocation"),
//                    business_location_count = :fields.getData("businessLocationCount"),
//                    
//                    monthly_sales           = :fields.getData("monthlySales"),
//                    annual_sales            = :fields.getData("annualSales"),
//                    average_ticket          = :fields.getData("averageTicket"),
//                    discount_rate           = :fields.getData("discountRate"),
//                    cents_per_tran          = :fields.getData("centsPerTran"),
//            
//                    rep_code                = :fields.getData("repCode"),
//                    officer_number          = :fields.getData("officerNumber"),
//                    profit_center           = :fields.getData("profitCenter"),
//                    
//                    prior_history           = :fields.getData("priorHistory"),
//                    change_reason           = :fields.getData("changeReason")
//                    
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_316 = fields.getData("merchantDBA");
 String __sJT_317 = fields.getData("address1");
 String __sJT_318 = fields.getData("address2");
 String __sJT_319 = fields.getData("city");
 String __sJT_320 = fields.getData("state");
 String __sJT_321 = fields.getData("zip");
 String __sJT_322 = fields.getData("contactName");
 String __sJT_323 = fields.getData("contactPhone");
 String __sJT_324 = fields.getData("contactEmail");
 String __sJT_325 = fields.getData("mailName");
 String __sJT_326 = fields.getData("mailAddress1");
 String __sJT_327 = fields.getData("mailAddress2");
 String __sJT_328 = fields.getData("mailCity");
 String __sJT_329 = fields.getData("mailState");
 String __sJT_330 = fields.getData("mailZip");
 String __sJT_331 = fields.getData("businessType");
 String __sJT_332 = fields.getData("businessIndustry");
 String __sJT_333 = fields.getData("percentMoto");
 String __sJT_334 = fields.getData("businessDescription");
 String __sJT_335 = fields.getData("businessLocation");
 String __sJT_336 = fields.getData("businessLocationCount");
 String __sJT_337 = fields.getData("monthlySales");
 String __sJT_338 = fields.getData("annualSales");
 String __sJT_339 = fields.getData("averageTicket");
 String __sJT_340 = fields.getData("discountRate");
 String __sJT_341 = fields.getData("centsPerTran");
 String __sJT_342 = fields.getData("repCode");
 String __sJT_343 = fields.getData("officerNumber");
 String __sJT_344 = fields.getData("profitCenter");
 String __sJT_345 = fields.getData("priorHistory");
 String __sJT_346 = fields.getData("changeReason");
   String theSqlTS = "update  prospects\n          \n          set     merchant_dba            =  :1 ,\n                  address1                =  :2 ,\n                  address2                =  :3 ,\n                  city                    =  :4 ,\n                  state                   =  :5 ,\n                  zip                     =  :6 ,\n                  \n                  contact_name            =  :7 ,\n                  contact_phone           =  :8 ,\n                  contact_email           =  :9 ,\n                  \n                  mail_name               =  :10 ,\n                  mail_address1           =  :11 ,\n                  mail_address2           =  :12 ,\n                  mail_city               =  :13 ,\n                  mail_state              =  :14 ,\n                  mail_zip                =  :15 ,\n                  \n                  business_type           =  :16 ,\n                  business_industry       =  :17 ,\n                  percent_moto            =  :18 ,\n                  business_description    =  :19 ,\n                  business_location       =  :20 ,\n                  business_location_count =  :21 ,\n                  \n                  monthly_sales           =  :22 ,\n                  annual_sales            =  :23 ,\n                  average_ticket          =  :24 ,\n                  discount_rate           =  :25 ,\n                  cents_per_tran          =  :26 ,\n          \n                  rep_code                =  :27 ,\n                  officer_number          =  :28 ,\n                  profit_center           =  :29 ,\n                  \n                  prior_history           =  :30 ,\n                  change_reason           =  :31 \n                  \n          where   app_seq_num =  :32";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.prospects.AssignProspect",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_316);
   __sJT_st.setString(2,__sJT_317);
   __sJT_st.setString(3,__sJT_318);
   __sJT_st.setString(4,__sJT_319);
   __sJT_st.setString(5,__sJT_320);
   __sJT_st.setString(6,__sJT_321);
   __sJT_st.setString(7,__sJT_322);
   __sJT_st.setString(8,__sJT_323);
   __sJT_st.setString(9,__sJT_324);
   __sJT_st.setString(10,__sJT_325);
   __sJT_st.setString(11,__sJT_326);
   __sJT_st.setString(12,__sJT_327);
   __sJT_st.setString(13,__sJT_328);
   __sJT_st.setString(14,__sJT_329);
   __sJT_st.setString(15,__sJT_330);
   __sJT_st.setString(16,__sJT_331);
   __sJT_st.setString(17,__sJT_332);
   __sJT_st.setString(18,__sJT_333);
   __sJT_st.setString(19,__sJT_334);
   __sJT_st.setString(20,__sJT_335);
   __sJT_st.setString(21,__sJT_336);
   __sJT_st.setString(22,__sJT_337);
   __sJT_st.setString(23,__sJT_338);
   __sJT_st.setString(24,__sJT_339);
   __sJT_st.setString(25,__sJT_340);
   __sJT_st.setString(26,__sJT_341);
   __sJT_st.setString(27,__sJT_342);
   __sJT_st.setString(28,__sJT_343);
   __sJT_st.setString(29,__sJT_344);
   __sJT_st.setString(30,__sJT_345);
   __sJT_st.setString(31,__sJT_346);
   __sJT_st.setLong(32,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:371^9*/
      }
      
      storeOk = true;
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::store()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
    if (fields.getData("mailAddress1").length() > 0)
    {
      fields.setData("hasMail","y");
    }
      
    return storeOk;
  }
  
  /*
  ** public boolean load()
  **
  ** Loads a sales prospect from the prospects table.
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean load()
  {
    boolean loadOk = false;
    
    long appSeqNum  = getAppSeqNum();
      
    try
    {
      connect();
      
      String merchantDBA;
      String address1;
      String address2;
      String city;
      String state;
      String zip;
      
      String contactName;
      String contactPhone;
      String contactEmail;
      
      String mailName;
      String mailAddress1;
      String mailAddress2;
      String mailCity;
      String mailState;
      String mailZip;
      
      String businessType;
      String businessIndustry;
      String percentMoto;
      String businessDescription;
      String businessLocation;
      String businessLocationCount;
      
      String monthlySales;
      String annualSales;
      String averageTicket;
      String discountRate;
      String centsPerTran;
      
      String priorHistory;
      String changeReason;

      // load the prospect data
      /*@lineinfo:generated-code*//*@lineinfo:448^7*/

//  ************************************************************
//  #sql [Ctx] { select  merchant_dba,
//                  address1,
//                  address2,
//                  city,
//                  state,
//                  zip,
//                  
//                  contact_name,
//                  contact_phone,
//                  contact_email,
//                  
//                  mail_name,
//                  mail_address1,
//                  mail_address2,
//                  mail_city,
//                  mail_state,
//                  mail_zip,
//                  
//                  business_type,
//                  business_industry,
//                  percent_moto,
//                  business_description,
//                  business_location,
//                  business_location_count,
//                  
//                  monthly_sales,
//                  annual_sales,
//                  average_ticket,
//                  discount_rate,
//                  cents_per_tran,
//                  
//                  prior_history,
//                  change_reason
//                  
//          
//          
//          from    prospects
//          
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merchant_dba,\n                address1,\n                address2,\n                city,\n                state,\n                zip,\n                \n                contact_name,\n                contact_phone,\n                contact_email,\n                \n                mail_name,\n                mail_address1,\n                mail_address2,\n                mail_city,\n                mail_state,\n                mail_zip,\n                \n                business_type,\n                business_industry,\n                percent_moto,\n                business_description,\n                business_location,\n                business_location_count,\n                \n                monthly_sales,\n                annual_sales,\n                average_ticket,\n                discount_rate,\n                cents_per_tran,\n                \n                prior_history,\n                change_reason\n                \n         \n        \n        from    prospects\n        \n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.prospects.AssignProspect",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 28) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(28,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantDBA = (String)__sJT_rs.getString(1);
   address1 = (String)__sJT_rs.getString(2);
   address2 = (String)__sJT_rs.getString(3);
   city = (String)__sJT_rs.getString(4);
   state = (String)__sJT_rs.getString(5);
   zip = (String)__sJT_rs.getString(6);
   contactName = (String)__sJT_rs.getString(7);
   contactPhone = (String)__sJT_rs.getString(8);
   contactEmail = (String)__sJT_rs.getString(9);
   mailName = (String)__sJT_rs.getString(10);
   mailAddress1 = (String)__sJT_rs.getString(11);
   mailAddress2 = (String)__sJT_rs.getString(12);
   mailCity = (String)__sJT_rs.getString(13);
   mailState = (String)__sJT_rs.getString(14);
   mailZip = (String)__sJT_rs.getString(15);
   businessType = (String)__sJT_rs.getString(16);
   businessIndustry = (String)__sJT_rs.getString(17);
   percentMoto = (String)__sJT_rs.getString(18);
   businessDescription = (String)__sJT_rs.getString(19);
   businessLocation = (String)__sJT_rs.getString(20);
   businessLocationCount = (String)__sJT_rs.getString(21);
   monthlySales = (String)__sJT_rs.getString(22);
   annualSales = (String)__sJT_rs.getString(23);
   averageTicket = (String)__sJT_rs.getString(24);
   discountRate = (String)__sJT_rs.getString(25);
   centsPerTran = (String)__sJT_rs.getString(26);
   priorHistory = (String)__sJT_rs.getString(27);
   changeReason = (String)__sJT_rs.getString(28);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:521^7*/
      
      fields.setData("merchantDBA",merchantDBA);
      fields.setData("address1",address1);
      fields.setData("address2",address2);
      fields.setData("city",city);
      fields.setData("state",state);
      fields.setData("zip",zip);
      
      fields.setData("contactName",contactName);
      fields.setData("contactPhone",contactPhone);
      fields.setData("contactEmail",contactEmail);
      
      fields.setData("mailName",mailName);
      fields.setData("mailAddress1",mailAddress1);
      fields.setData("mailAddress2",mailAddress2);
      fields.setData("mailCity",mailCity);
      fields.setData("mailState",mailState);
      fields.setData("mailZip",mailZip);
      
      fields.setData("businessType",businessType);
      fields.setData("businessIndustry",businessIndustry);
      fields.setData("percentMoto",percentMoto);
      fields.setData("businessDescription",businessDescription);
      fields.setData("businessLocation",businessLocation);
      fields.setData("businessLocationCount",businessLocationCount);
      
      fields.setData("monthlySales",monthlySales);
      fields.setData("annualSales",annualSales);
      fields.setData("averageTicket",averageTicket);
      fields.setData("discountRate",discountRate);
      fields.setData("centsPerTran",centsPerTran);
      
      fields.setData("priorHistory",priorHistory);
      fields.setData("changeReason",changeReason);
      
      if (fields.getData("mailAddress1").length() > 0)
      {
        fields.setData("hasMail","y");
      }
      
      loadOk = true;
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::load(appSeqNum = " 
        + appSeqNum + ")";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
    return loadOk;
  }
}/*@lineinfo:generated-code*/