/*@lineinfo:filename=AssignRepDropDownTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/prospects/AssignRepDropDownTable.sqlj $

  Description:
  
  AssignRepDropDownTable

  Loads a list of names into a drop down table based that a user may assign
  prospects to.  
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 1/31/02 11:52a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.prospects;

import java.sql.ResultSet;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class AssignRepDropDownTable extends DropDownTable
{
  public AssignRepDropDownTable(UserBean user)
  {
    long userId = ProspectReport.getTopLevelId(user);
    
    try
    {
      connect();
      
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:50^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.user_id,
//                  u.name
//                  
//          from    users u,
//                  t_hierarchy h
//                  
//          where   u.user_id = h.descendent
//                  and h.ancestor = :userId
//                  and h.relation > 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  u.user_id,\n                u.name\n                \n        from    users u,\n                t_hierarchy h\n                \n        where   u.user_id = h.descendent\n                and h.ancestor =  :1 \n                and h.relation > 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.prospects.AssignRepDropDownTable",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.prospects.AssignRepDropDownTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:61^7*/
      ResultSet rs = it.getResultSet();
      
      while (rs.next())
      {
        addElement(rs);
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::AssignRepDropDownTable()";
      String desc = "userId = " + userId + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/