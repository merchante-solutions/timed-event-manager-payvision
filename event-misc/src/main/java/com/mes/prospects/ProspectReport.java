/*@lineinfo:filename=ProspectReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/prospects/ProspectReport.sqlj $

  Description:
  
  ProspectReport
  
  Parent class for prospect report beans.  
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/25/02 11:24a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.prospects;

import java.sql.ResultSet;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesUsers;
import com.mes.forms.DateRangeReport;
import com.mes.forms.HiddenField;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class ProspectReport extends DateRangeReport
{
  protected String        headName;
  
  protected ProspectReport()
  {
  }
  
  public ProspectReport(UserBean user)
  {
    super(user);
    fields.add(new HiddenField("headId"));

    fields.setData("headId",Long.toString(getTopLevelId()));
    
    fields.setGroupHtmlExtra("class=\"small\"");
  }
  
  public String getHeadName()
  {
    return headName;
  }
  
  /*
  ** private long getTopLevelId()
  **
  ** Loads the top level id of the user based on their prospect user type.
  ** Prospect peer users get to see one level above their user level, others
  ** see from their user level down.
  **
  ** RETURNS: the user's top level hierarchy id.
  */
  private long getTopLevelId()
  {
    // default to user id for top level
    long topLevelId = user.getUserId();
    
    // if user is a prospect peer then top level is one higher
    // than the user's id
    if (user.hasRight(MesUsers.RIGHT_PROSPECT_PEER))
    {
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:84^9*/

//  ************************************************************
//  #sql [Ctx] { select  ancestor 
//          
//            
//  
//            from    t_hierarchy
//  
//            where   descendent = :user.getUserId()
//                    and relation = 1
//                    and hier_type = :MesHierarchy.HT_PROSPECT_DB
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_418 = user.getUserId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ancestor \n        \n           \n\n          from    t_hierarchy\n\n          where   descendent =  :1 \n                  and relation = 1\n                  and hier_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.prospects.ProspectReport",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_418);
   __sJT_st.setInt(2,MesHierarchy.HT_PROSPECT_DB);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   topLevelId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:95^9*/
      }
      catch (Exception e)
      {
        String func = this.getClass().getName() + "::getToplevelId()";
        String desc = e.toString();
        System.out.println(func + ": " + desc);
        logEntry(func,desc);
      }
      finally
      {
        cleanUp();
      }
    }

    return topLevelId;
  }
  
  /*
  ** public static long getTopLevelId(UserBean user)
  **
  ** Static convenience function to allow external non report classes to
  ** access the getTopLevelId() method.
  **
  ** RETURNS: the user's top level hierarchy id.
  */
  public static long getTopLevelId(UserBean user)
  {
    ProspectReport pr = new ProspectReport();
    pr.user = user;
    return pr.getTopLevelId();
  }
      
  /*
  ** public long getParentId()
  **
  ** Determines the parent of the current headId as long as it is not
  ** above the userId in the hierarchy.  For prospect peers, the top
  ** id is considered to be the parent of the user id (so peer users
  ** can see things from one level higher than their user ids).
  **
  ** RETURNS: long hierarchy id above this headId if legal, else -1
  */
  public long getParentId()
  {
    long parentId = -1L;
    
    try
    {
      // if not currently at the top level then look up the parent
      // of the current level
      long headId = Long.parseLong(fields.getData("headId"));
      if (headId != getTopLevelId())
      {
        connect();
        
        // get parent of this node
        ResultSetIterator it = null;
        /*@lineinfo:generated-code*//*@lineinfo:153^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ancestor
//            from    t_hierarchy
//            where   descendent = :headId
//                    and relation = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ancestor\n          from    t_hierarchy\n          where   descendent =  :1 \n                  and relation = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.prospects.ProspectReport",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,headId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.prospects.ProspectReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:159^9*/
      
        ResultSet rs = it.getResultSet();
        if (rs.next())
        {
          parentId = rs.getLong("ancestor");
        }
        rs.close();
      }
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::getParentId()";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
    return parentId;
  }
}/*@lineinfo:generated-code*/