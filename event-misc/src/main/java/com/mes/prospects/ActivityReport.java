/*@lineinfo:filename=ActivityReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/prospects/ActivityReport.sqlj $

  Description:
  
  ActivityReport
  
  Generates prospect call activity report.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/25/02 11:24a $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.prospects;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import com.mes.constants.MesHierarchy;
import com.mes.forms.CheckboxField;
import com.mes.forms.ComboDateField;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class ActivityReport extends ProspectReport
{
  private boolean       isLoaded;
  private ReportVector  reportData;
  private int           activeProspectCount;
  private int           totalProspectCount;
  
  public ActivityReport(UserBean user)
  {
    super(user);
    fields.add(new CheckboxField("showCalls",false));
    getField("showCalls").setHtmlExtra("class=\"small\"");
  }
  
  public int getActiveProspectCount()
  {
    return activeProspectCount;
  }
  
  public int getTotalProspectCount()
  {
    return totalProspectCount;
  }
  
  public String renderOtherControlsRow()
  {
    StringBuffer renderData = new StringBuffer();
    
    try
    {
      renderData.append("          <tr>\n");
      renderData.append("            <td></td>\n");
      renderData.append("            <td valign=\"middle\" class=\"small\">\n");
      renderData.append("              " + fields.renderHtml("showCalls"));
      renderData.append("Show call records\n");
      renderData.append("            </td>\n");
      renderData.append("            <td></td>\n");
      renderData.append("          </tr>\n");
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::renderOtherControlsRow()"; 
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    
    return renderData.toString();
  }
  
  /**************************************************************************
  **
  ** Report Data Containers
  **
  **************************************************************************/

  private class StatVector extends Vector
  {
    protected int recCount;
    protected int duration;
    protected int mileage;
    protected int callCount;
    protected int byPhone;
    protected int inPerson;
    protected int newCount;
    protected int deadCount;
    protected int closedCount;
    
    public int getRecCount()    { return recCount; }
    public int getDuration()    { return duration; }
    public int getMileage()     { return mileage; }
    public int getCallCount()   { return callCount; }
    public int getByPhone()     { return byPhone; }
    public int getInPerson()    { return inPerson; }
    public int getNewCount()    { return newCount; }
    public int getDeadCount()   { return deadCount; }
    public int getClosedCount() { return closedCount; }

    protected void accumulateStats(CallRecord rec)
    {
      ++recCount;
      duration += rec.getDuration();
      mileage += rec.getMileage();
      if (rec.getStatus() != DevelopProspect.STAT_REFERRED)
      {
        ++callCount;
        if (rec.getMethod() == DevelopProspect.METHOD_BY_PHONE)
        {
          ++byPhone;
        }
        else
        {
          ++inPerson;
        }
      }
      else
      {
        ++newCount;
      }
      if (rec.getStatus() == DevelopProspect.STAT_DECLINED)
      {
        ++deadCount;
      }
      if (rec.getStatus() == DevelopProspect.STAT_CLOSED)
      {
        ++closedCount;
      }
    }
  }
  
  public class ReportVector extends StatVector
  {
    private Hashtable directReports = new Hashtable();

    public DirectReportVector addDirectReport(long drUserId, String drName)
    {
    	DirectReportVector directReport = (DirectReportVector)directReports.get(drUserId);
      if (directReport == null)
      {
        directReport = new DirectReportVector(drUserId,drName);
        directReports.put(drUserId, directReport);
        add(directReport);
      }
      
      return directReport;
    }

    public boolean add(CallRecord rec)
    {
      boolean addOk = false;
      
      DirectReportVector dr = addDirectReport(rec.getDrUserId(),rec.getDrName());
      addOk = dr.add(rec);
      
      accumulateStats(rec);

      return addOk;
    }
  }
  
  public class DirectReportVector extends StatVector
  {
    private long      drId;
    private String    drName;
    private Hashtable reps = new Hashtable();
    
    public DirectReportVector(long drId, String drName)
    {
      this.drId = drId;
      this.drName = drName;
    }
    
    public long getDrId()
    {
      return drId;
    }
    public String getDrName()
    {
      return drName;
    }
    
    public RepVector addRep(long repId, String repName)
    {
      RepVector rep = (RepVector)reps.get(repId);
      if (rep == null)
      {
        rep = new RepVector(repName,repId);
        reps.put(repId, rep);
        add(rep);
      }
      return rep;
    }

    public boolean add(CallRecord rec)
    {
      boolean addOk = false;
      
      RepVector rep = addRep(rec.getRepUserId(),rec.getRepName());
      addOk = rep.add(rec);
      
      accumulateStats(rec);
      
      return addOk;      
    }
    
    public String[] getRepNames()
    {
      String[] strs = new String[size()];
      int cnt = 0;
      for (Iterator i = iterator(); i.hasNext();)
      {
        strs[cnt++] = ((RepVector)i.next()).getRepName();
      }
      return strs;
    }
    
    public RepVector get(String repName)
    {
      return (RepVector)reps.get(repName);
    }
  }
  
  public class RepVector extends StatVector
  {
    private String repName;
    private long repId;
    
    public RepVector(String repName, long repId)
    {
      this.repName = repName;
      this.repId = repId;
    }
    
    public String getRepName()
    {
      return repName;
    }
    
    public long getRepId()
    {
      return repId;
    }

    public boolean add(CallRecord rec)
    {
      accumulateStats(rec);
      return super.add(rec);
    }
  }
  
  public class CallRecord
  {
    private String  drName;
    private String  repName;
    private String  merchantDba;
    private String  createDate;
    private String  devDate;
    private String  notes;
    private String  statusDesc;
    private int     status;
    private int     method;
    private int     duration;
    private int     mileage;
    private long    appSeqNum;
    private long    drUserId;
    private long    repUserId;
    
    private String error;
    
    private String getDescription(int idx, String[] list)
    {
      try
      {
        return list[idx];
      }
      catch (Exception e) {}
      
      return "[unknown]";
    }
    public CallRecord(ResultSet rs)
    {
      try
      {
        drName          = rs.getString      ("dr_name");
        repName         = rs.getString      ("rep_name");
        merchantDba     = rs.getString      ("merchant_dba");
        notes           = rs.getString      ("notes");
        createDate      = formatDate(rs.getTimestamp("create_date"));
        devDate         = formatDate(rs.getTimestamp("dev_date"));
        status          = rs.getInt         ("status");
        method          = rs.getInt         ("call_method");
        duration        = rs.getInt         ("call_duration");
        mileage         = rs.getInt         ("call_mileage");
        appSeqNum       = rs.getLong        ("app_seq_num");
        drUserId        = rs.getLong        ("dr_user_id");
        repUserId       = rs.getLong        ("rep_user_id");
        
        statusDesc      = 
          DevelopProspect.getStatusDescription(Integer.toString(status));
      }
      catch (Exception e)
      {
        error = e.toString();
      }
    }
    
    public long   getAppSeqNum()        { return appSeqNum; }
    public long   getDrUserId()         { return drUserId; }
    public String getDrName()           { return drName; }
    public long   getRepUserId()        { return repUserId; }
    public String getRepName()          { return repName; }
    public String getMerchantDba()      { return merchantDba; }
    public int    getStatus()           { return status; }
    public String getStatusDesc()       { return statusDesc; }
    public String getCreateDate()       { return createDate; }
    public String getDevDate()          { return devDate; }
    public int    getMethod()           { return method; }
    public int    getDuration()         { return duration; }
    public int    getMileage()          { return mileage; }
    public String getNotes()            { return notes; }
    
    public String getError()            { return error; }
  }
  
  /**************************************************************************
  **
  ** Database IO
  **
  **************************************************************************/

  /*
  ** public boolean loadCallData()
  **
  ** Loads prospect data into a result set.
  **
  ** RETURNS: ResultSet containing query results, null if failed.
  */
  public ResultSet loadCallData()
  {
    ResultSet rs = null;
    
    try
    {
      java.sql.Date fromDate = 
        ((ComboDateField)fields.getField("fromDate")).getSqlDate();
      java.sql.Date toDate = 
        ((ComboDateField)fields.getField("toDate")).getSqlDate();
      Calendar cal = Calendar.getInstance();
      cal.setTime(toDate);
      cal.add(Calendar.DATE,1);
      toDate = new java.sql.Date(cal.getTime().getTime());
    
      long headId = Long.parseLong(fields.getData("headId"));
      
      // load number of prospects that have had activity
      // currently activity is defined as any type of status 
      // (referral, call, decline, etc.)
      // need to put filters in if want to exclude certain statuses
      /*@lineinfo:generated-code*//*@lineinfo:385^7*/

//  ************************************************************
//  #sql [Ctx] { select    count(distinct(p.app_seq_num)) 
//          
//          from      prospects p,
//                    prospect_developments pd,
//                    t_hierarchy h1,
//                    t_hierarchy h2
//                    
//          where     h1.ancestor = :headId
//                    and h1.hier_type = :MesHierarchy.HT_PROSPECT_DB
//                    and ( ( h1.relation = 1 
//  		  	  	              and h1.descendent = h2.ancestor ) or
//  		  	  	            ( h1.relation = 0 
//  				   	              and h1.ancestor = h2.descendent 
//  					                and h1.ancestor = h2.ancestor ) )
//                    and h2.hier_type = :MesHierarchy.HT_PROSPECT_DB
//                    and h2.descendent = p.user_id 
//                    and p.app_seq_num = pd.app_seq_num
//                    and pd.dev_date between :fromDate and :toDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    count(distinct(p.app_seq_num))  \n        \n        from      prospects p,\n                  prospect_developments pd,\n                  t_hierarchy h1,\n                  t_hierarchy h2\n                  \n        where     h1.ancestor =  :1 \n                  and h1.hier_type =  :2 \n                  and ( ( h1.relation = 1 \n\t\t  \t  \t              and h1.descendent = h2.ancestor ) or\n\t\t  \t  \t            ( h1.relation = 0 \n\t\t\t\t   \t              and h1.ancestor = h2.descendent \n\t\t\t\t\t                and h1.ancestor = h2.ancestor ) )\n                  and h2.hier_type =  :3 \n                  and h2.descendent = p.user_id \n                  and p.app_seq_num = pd.app_seq_num\n                  and pd.dev_date between  :4  and  :5";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.prospects.ActivityReport",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,headId);
   __sJT_st.setInt(2,MesHierarchy.HT_PROSPECT_DB);
   __sJT_st.setInt(3,MesHierarchy.HT_PROSPECT_DB);
   __sJT_st.setDate(4,fromDate);
   __sJT_st.setDate(5,toDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeProspectCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:405^7*/

      // load total number of prospects
      /*@lineinfo:generated-code*//*@lineinfo:408^7*/

//  ************************************************************
//  #sql [Ctx] { select    count(distinct(p.app_seq_num)) 
//          
//          from      prospects p,
//                    t_hierarchy h1,
//                    t_hierarchy h2
//                    
//          where     h1.ancestor = :headId
//                    and ( ( h1.relation = 1 
//  		  	  	              and h1.descendent = h2.ancestor ) or
//  		  	  	            ( h1.relation = 0 
//  				   	              and h1.ancestor = h2.descendent 
//  					                and h1.ancestor = h2.ancestor ) )
//                    and h1.hier_type = :MesHierarchy.HT_PROSPECT_DB
//                    and h2.hier_type = :MesHierarchy.HT_PROSPECT_DB
//                    and h2.descendent = p.user_id 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    count(distinct(p.app_seq_num))  \n        \n        from      prospects p,\n                  t_hierarchy h1,\n                  t_hierarchy h2\n                  \n        where     h1.ancestor =  :1 \n                  and ( ( h1.relation = 1 \n\t\t  \t  \t              and h1.descendent = h2.ancestor ) or\n\t\t  \t  \t            ( h1.relation = 0 \n\t\t\t\t   \t              and h1.ancestor = h2.descendent \n\t\t\t\t\t                and h1.ancestor = h2.ancestor ) )\n                  and h1.hier_type =  :2 \n                  and h2.hier_type =  :3 \n                  and h2.descendent = p.user_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.prospects.ActivityReport",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,headId);
   __sJT_st.setInt(2,MesHierarchy.HT_PROSPECT_DB);
   __sJT_st.setInt(3,MesHierarchy.HT_PROSPECT_DB);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   totalProspectCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:425^7*/

      // load prospect calls under this hierarchy id
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:429^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    h1.descendent           dr_user_id,
//                    h2.descendent           rep_user_id,
//                    p.app_seq_num,
//                    p.merchant_dba,
//                    p.create_date,
//                    pd.status,
//                    pd.dev_date             dev_date,
//                    pd.call_method,
//                    pd.call_duration,
//                    pd.call_mileage,
//                    pd.notes,
//                    hn1.name                dr_name,
//                    hn2.name                rep_name
//                  
//          from      prospects p,
//                    prospect_developments pd,
//                    t_hierarchy h1,
//                    t_hierarchy h2,
//                    t_hierarchy_names hn1,
//                    t_hierarchy_names hn2
//  
//          where     h1.ancestor = :headId
//                    and ( ( h1.relation = 1 
//  		  	  	              and h1.descendent = h2.ancestor ) or
//  		  	  	            ( h1.relation = 0 
//  				   	              and h1.ancestor = h2.descendent 
//  					                and h1.ancestor = h2.ancestor ) )
//                    and h1.hier_type = :MesHierarchy.HT_PROSPECT_DB
//                    and h2.hier_type = :MesHierarchy.HT_PROSPECT_DB
//                    and h2.descendent = p.user_id 
//                    and p.app_seq_num = pd.app_seq_num
//                    and pd.dev_date between :fromDate and :toDate
//                    and hn1.hier_id = h2.ancestor
//                    and hn2.hier_id = h2.descendent
//                  
//          order by  h2.ancestor,
//                    h2.descendent,
//                    pd.dev_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    h1.descendent           dr_user_id,\n                  h2.descendent           rep_user_id,\n                  p.app_seq_num,\n                  p.merchant_dba,\n                  p.create_date,\n                  pd.status,\n                  pd.dev_date             dev_date,\n                  pd.call_method,\n                  pd.call_duration,\n                  pd.call_mileage,\n                  pd.notes,\n                  hn1.name                dr_name,\n                  hn2.name                rep_name\n                \n        from      prospects p,\n                  prospect_developments pd,\n                  t_hierarchy h1,\n                  t_hierarchy h2,\n                  t_hierarchy_names hn1,\n                  t_hierarchy_names hn2\n\n        where     h1.ancestor =  :1 \n                  and ( ( h1.relation = 1 \n\t\t  \t  \t              and h1.descendent = h2.ancestor ) or\n\t\t  \t  \t            ( h1.relation = 0 \n\t\t\t\t   \t              and h1.ancestor = h2.descendent \n\t\t\t\t\t                and h1.ancestor = h2.ancestor ) )\n                  and h1.hier_type =  :2 \n                  and h2.hier_type =  :3 \n                  and h2.descendent = p.user_id \n                  and p.app_seq_num = pd.app_seq_num\n                  and pd.dev_date between  :4  and  :5 \n                  and hn1.hier_id = h2.ancestor\n                  and hn2.hier_id = h2.descendent\n                \n        order by  h2.ancestor,\n                  h2.descendent,\n                  pd.dev_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.prospects.ActivityReport",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,headId);
   __sJT_st.setInt(2,MesHierarchy.HT_PROSPECT_DB);
   __sJT_st.setInt(3,MesHierarchy.HT_PROSPECT_DB);
   __sJT_st.setDate(4,fromDate);
   __sJT_st.setDate(5,toDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.prospects.ActivityReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:469^7*/
      rs = it.getResultSet();
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::loadCallData()";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }

    return rs;
  }
  
  public boolean load()
  {
    boolean loadOk = false;
    
    try
    {
      connect();
      
      long headId = Long.parseLong(fields.getData("headId"));
      
      // get direct children of node, including self
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:495^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  h.descendent,
//                  hn.name
//          from    t_hierarchy h,
//                  t_hierarchy_names hn
//          where   h.ancestor = :headId
//                  and h.relation < 2
//                  and h.entity_type <> 3
//                  and h.descendent = hn.hier_id
//                  and h.hier_type = :MesHierarchy.HT_PROSPECT_DB
//          order by h.relation asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  h.descendent,\n                hn.name\n        from    t_hierarchy h,\n                t_hierarchy_names hn\n        where   h.ancestor =  :1 \n                and h.relation < 2\n                and h.entity_type <> 3\n                and h.descendent = hn.hier_id\n                and h.hier_type =  :2 \n        order by h.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.prospects.ActivityReport",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,headId);
   __sJT_st.setInt(2,MesHierarchy.HT_PROSPECT_DB);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.prospects.ActivityReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:507^7*/
      ResultSet rs = it.getResultSet();
      
      // build a report vector, add direct reports
      reportData = new ReportVector();
      while (rs.next())
      {
        long    drId    = rs.getLong("descendent");
        String  drName  = rs.getString("name");
        reportData.addDirectReport(drId,drName);
        if (drId == headId)
        {
          headName = drName;
        }
      }
      rs.close();
      
      // load prospect data under the direct reports
      rs = loadCallData();
      while (rs.next())
      {
        reportData.add(new CallRecord(rs));
      }
      rs.close();
      
      loadOk = true;
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::load()";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
    isLoaded = loadOk;
    return loadOk;
  }
  
  /*
  ** public ReportVector getReportData()
  **
  ** Loads prospect report data if not already loaded, returns the data.
  **
  ** RETURNS: a Vector containing CallRecords.
  */
  public ReportVector getReportData()
  {
    if (isLoaded || load())
    {
      return reportData;
    }
    return null;
  }
  
  /**************************************************************************
  **
  ** Validation
  **
  **************************************************************************/

  /**************************************************************************
  **
  ** Static Convenience Methods
  **
  **************************************************************************/
}/*@lineinfo:generated-code*/