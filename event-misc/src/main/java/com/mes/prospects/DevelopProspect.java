/*@lineinfo:filename=DevelopProspect*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/prospects/DevelopProspect.sqlj $

  Description:
  
  DevelopProspect
  
  Manages prospect developments (sales calls and changes to prospect 
  status.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 4/25/02 11:24a $
  Version            : $Revision: 26 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.prospects;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.Vector;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.Condition;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.DiscountField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.ApplicationXDoc;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class DevelopProspect extends FieldBean
{
  public static final int   STAT_REFERRED     = 0;
  public static final int   STAT_FIRST_CALL   = 10;
  public static final int   STAT_SECOND_CALL  = 20;
  public static final int   STAT_DELIVERED    = 30;
  public static final int   STAT_ACCEPTED     = 40;
  public static final int   STAT_CLOSED       = 50;
  public static final int   STAT_DECLINED     = 60;
  
  public static final int   METHOD_BY_PHONE   = 0;
  public static final int   METHOD_IN_PERSON  = 1;
  
  public static final int   QUAL_EXCELLENT    = 0;
  public static final int   QUAL_GOOD         = 1;
  public static final int   QUAL_FAIR         = 2;
  public static final int   QUAL_POOR         = 3;
  
  /**************************************************************************
  **
  ** Data Field Helpers
  **
  **************************************************************************/
  
  public static String[][] statuses =
  {
    { "0",  "Referred to Rep" },
    { "10", "First Call" },
    { "20", "Second Call" },
    { "30", "Proposal Delivered" },
    { "40", "Proposal Accepted" },
    { "50", "Closed" },
    { "60", "Proposal Declined" }
  };
  public static String[] qualities =
  {
    "Excellent",
    "Good",
    "Fair",
    "Poor",
  };
  public static String[] callMethods =
  {
    "By Phone",
    "In Person"
  };
  
  private class ArrayTable extends DropDownTable
  {
    public ArrayTable(String[] elements)
    {
      for (int i=0; i < elements.length; ++i)
      {
        addElement(Integer.toString(i),elements[i]);
      }
    }
  }
        
  private class StatusTable extends DropDownTable
  {
    public StatusTable()
    {
      for (int i = 1; i < statuses.length; ++i)
      {
        addElement(statuses[i][0],statuses[i][1]);
      }
    }
  }

  private class CallDurationTable extends DropDownTable
  {
    public CallDurationTable()
    {
      addElement("10","10 minutes");
      addElement("20","20 minutes");
      addElement("30","30 minutes");
      addElement("45","45 minutes");
      addElement("60","1 hour");
      addElement("120","2 hours");
      addElement("180","3 hours");
    }
  }
  
  private class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      addElement("","-- select --");
      addElement("1","Sole Prop");
      addElement("2","Corporation");
      addElement("3","Partnership");
      addElement("4","Med/Lgl Corp");
      addElement("5","Est/Trust");
      addElement("6","Tax Exempt");
      addElement("7","Government");
      addElement("8","Lmtd Liab Co");
    }
  }
  
  private class IndustryTable extends DropDownTable
  {
    public IndustryTable()
    {
      addElement("","-- select --");
      addElement("1","Retail");
      addElement("2","Restaurant");
      addElement("3","Hotel");
      addElement("4","Motel");
      addElement("5","Internet");
      addElement("6","Services");
      addElement("7","Dir Mrkt");
      addElement("8","Other");
    }
  }
  
  private class LocationTable extends DropDownTable
  {
    public LocationTable()
    {
      addElement("","-- select --");
      addElement("1","Retail Storefront");
      addElement("2","Internet Storefront");
      addElement("3","Business Office");
      addElement("4","Private Residence");
      addElement("5","Other");
      addElement("6","Bank");
    }
  }
  
  private class YesNoTable extends DropDownTable
  {
    public YesNoTable()
    {
      addElement("","--");
      addElement("Y","Yes");
      addElement("N","No");
    }
  }
    
  /*
  ** public DevelopProspect(UserBean user)
  **
  ** Constructor.
  */
  public DevelopProspect(UserBean user)
  {
    super(user);

    StateDropDownTable stateTable = new StateDropDownTable();
    
    // hidden fields
    fields.add(     new HiddenField  ("appSeqNum"));
    Field fIsNew =  new HiddenField  ("isNew");
    fields.add(fIsNew);
    
    // submission button
    fields.add(new ButtonField  ("update"));
    fields.add(new ButtonField  ("submit"));
    fields.add(new ButtonField  ("apply"));
    fields.add(new ButtonField  ("assign"));
    fields.add(new ButtonField  ("del"));

    // create conditions that check for each button being pressed
    Field fUpdate = fields.getField("update");
    Field fSubmit = fields.getField("submit");
    FieldValueCondition cUpdatePressed = new FieldValueCondition(fUpdate,"update");
    FieldValueCondition cSubmitPressed = new FieldValueCondition(fSubmit,"submit");
    
    // create an or condition for the prospect info box that causes validation
    // of the info fields subgroup if either button was pressed
    OrCondition cInfoCondition = new OrCondition();
    cInfoCondition.add(cUpdatePressed);
    cInfoCondition.add(cSubmitPressed);
    
    // create the info and call subgroups with the above conditions
    FieldGroup infoFields = new FieldGroup("infoFields",cInfoCondition);
    FieldGroup callFields = new FieldGroup("callFields",cSubmitPressed);
    
    // create the prospect info subgroup fields
    infoFields.add(new Field        ("merchantDBA",           25,   0,    false));
    infoFields.add(new Field        ("address1",              32,   0,    false));
    infoFields.add(new Field        ("address2",              32,   0,    true));
    infoFields.add(new Field        ("city",                  25,   0,    false));
    infoFields.add(new DropDownField("state",                 stateTable, false));
    infoFields.add(new Field        ("zip",                   9,    0,    false));
    infoFields.add(new CheckboxField("hasMail",               false));
    
    infoFields.add(new Field        ("contactName",           40,   32,   false));
    infoFields.add(new PhoneField   ("contactPhone",          false));
    infoFields.add(new EmailField   ("contactEmail",          true));
    
    // create subgroup for separate mailing address fields that is
    // only validated if hasMail checkbox is checked
    Field fHasMail = infoFields.getField("hasMail");
    Condition cHasMail = new FieldValueCondition(fHasMail,"y");
    FieldGroup addressFields = new FieldGroup("addressFields",cHasMail);
    addressFields.add(new Field        ("mailName",              25,   0,  true));
    addressFields.add(new Field        ("mailAddress1",          32,   0,  true));
    addressFields.add(new Field        ("mailCity",              25,   0,  true));
    addressFields.add(new DropDownField("mailState",             stateTable, true));
    addressFields.add(new Field        ("mailZip",               9,    0,  true));
    addressFields
      .addGroupValidation(new ConditionalRequiredValidation(cHasMail));
    // mail address 2 is always optional, so add it after setting the
    // conditional validation for the rest of the group
    addressFields.add(new Field        ("mailAddress2",          32,   0,  true));
    
    // address fields are a subgroup of the prospect info subgroup
    infoFields.add(addressFields);
    
    infoFields.add(new DropDownField("businessType",          new BusinessTypeTable(), false));
    infoFields.add(new DropDownField("businessIndustry",      new IndustryTable(), false));
    infoFields.add(new NumberField  ("percentMoto",           3,    3,    false, 0));
    infoFields.add(new DropDownField("businessLocation",      new LocationTable(), false));
    infoFields.add(new NumberField  ("businessLocationCount", 3,    0,    false, 0));
    infoFields.add(new TextareaField("businessDescription",   200,  3,  50, false));
    
    infoFields.add(new CurrencyField("monthlySales",          13,   9,    false));
    infoFields.add(new CurrencyField("annualSales",           13,   9,    false));
    infoFields.add(new CurrencyField("averageTicket",         10,   6,    false));
    infoFields.add(new DiscountField("discountRate",          true));
    infoFields.add(new CurrencyField("centsPerTran",          4,    5,    true));
    
    infoFields.add(new DropDownField("priorHistory",          new YesNoTable(), false));
    infoFields.add(new Field        ("changeReason",          200,  60,   true));
    
    // create the call subgroup fields
    callFields.add(new DropDownField("status",                new StatusTable(), false));
    callFields.add(new DropDownField("quality",               new ArrayTable(qualities), false));
    callFields.add(new DropDownField("callMethod",            new ArrayTable(callMethods), false));
    callFields.add(new DateField    ("callDate",              Calendar.getInstance().getTime(), false));
    callFields.add(new DropDownField("callDuration",          new CallDurationTable(), false));
    callFields.add(new NumberField  ("callMileage",           4,    0,  true, 0));
    callFields.add(new TextareaField("notes",                 200,  2,  100, false));
    
    // make call mileage field required if call method is in person ("1")
    Field fCallMethod = callFields.getField("callMethod");
    Field fCallMileage = callFields.getField("callMileage");
    Condition cInPerson = new FieldValueCondition(fCallMethod,"1");
    fCallMileage.addValidation(new ConditionalRequiredValidation(cInPerson));
    
    Condition cIsNotNew = new FieldValueCondition(fIsNew,"no");
    FieldGroup reassignFields = new FieldGroup("reassignFields",cIsNotNew);
    Field fAssignRep = new DropDownField("assignRep", new AssignRepDropDownTable(user), false);
    reassignFields.add(fAssignRep);
    
    // add the info and call subgroups to the main fields group
    fields.add(infoFields);
    fields.add(callFields);
    fields.add(reassignFields);
    
    // set all fields to have small text style
    fields.setGroupHtmlExtra("class=\"small\"");
  }
  
  /**************************************************************************
  **
  ** Accessors
  **
  **************************************************************************/
  
  // summary data
  private String      currentStatus;
  private String      currentQuality;
  private int         minutesSpent;
  private int         daysOld;
  private int         totalMileage;
  private int         totalCalls;
  private float       projectedDiscount;
  
  public String getCurrentStatus()
  {
    return currentStatus;
  }
  
  public String getCurrentQuality()
  {
    return currentQuality;
  }
  
  public int getMinutesSpent()
  {
    return minutesSpent;
  }
  
  public int getDaysOld()
  {
    return daysOld;
  }
  
  public int getTotalMileage()
  {
    return totalMileage;
  }
  
  public int getTotalCalls()
  {
    return totalCalls;
  }
  
  public float getProjectedDiscount()
  {
    return projectedDiscount;
  }
  
  /**************************************************************************
  **
  ** Database IO
  **
  **************************************************************************/
  
  public static String getStatusDescription(String statusValue)
  {
    String description = "unknown";
    for (int i = 0; i < statuses.length; ++i)
    {
      if (statuses[i][0].equals(statusValue))
      {
        description = statuses[i][1];
        break;
      }
    }
    return description;
  }

  /*
  ** public class DevelopmentRecord
  **
  ** Contains a call record's data.
  */
  public class DevelopmentRecord
  {
    private String callDate;
    private String status;
    private String quality;
    private String callMethod;
    private String callDuration;
    private String callMileage;
    private String notes;
    private String error;
    
    private String getDescription(int idx, String[] list)
    {
      try
      {
        return list[idx];
      }
      catch (Exception e) {}
      
      return "[unknown]";
    }
    public DevelopmentRecord(ResultSet rs)
    {
      try
      {
        callDate        = DateTimeFormatter.getFormattedDate(
          rs.getTimestamp("call_date"), DateTimeFormatter.DEFAULT_DATE_FORMAT);
        status          = getStatusDescription(rs.getString("status"));
        quality         = getDescription(rs.getInt("quality"),qualities);
        callMethod      = getDescription(rs.getInt("call_method"),callMethods);
        callDuration    = rs.getString("call_duration");
        callMileage     = rs.getString("call_mileage");
        notes           = rs.getString("notes");

        if (status.equals("0"))
        {
          callMethod = "n/a";
          callDuration = "n/a";
        }
        
        if (rs.getInt("call_method") != 1 || status.equals("0"))
        {
          callMileage =   "n/a";
        }
      }
      catch (Exception e)
      {
        error = e.toString();
      }
    }
    
    public String getCallDate()     { return callDate; }
    public String getStatus()       { return status; }
    public String getQuality()      { return quality; }
    public String getCallMethod()   { return callMethod; }
    public String getCallDuration() { return callDuration; }
    public String getCallMileage()  { return callMileage; }
    public String getNotes()        { return notes; }
    public String getError()        { return error; }
  }
  
  /*
  ** public Vector getDevelopments()
  **
  ** Loads a Vector with the DevelopmentRecord objects associated with this
  ** prospect.
  **
  ** RETURNS: Vector of DevelopmentRecords.
  */
  public Vector getDevelopments()
  {
    Vector developments = null;

    long appSeqNum  = getAppSeqNum();
    
    try
    {
      connect();
      
      // load the prospect developments
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:460^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  call_date,
//                  status,
//                  quality,
//                  call_method,
//                  call_duration,
//                  call_mileage,
//                  notes
//                  
//          from    prospect_developments
//          
//          where   app_seq_num = :appSeqNum
//          
//          order by dev_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  call_date,\n                status,\n                quality,\n                call_method,\n                call_duration,\n                call_mileage,\n                notes\n                \n        from    prospect_developments\n        \n        where   app_seq_num =  :1 \n        \n        order by dev_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.prospects.DevelopProspect",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.prospects.DevelopProspect",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:475^7*/
      ResultSet rs = it.getResultSet();
      
      developments = new Vector();
      while (rs.next())
      {
        DevelopmentRecord rec = new DevelopmentRecord(rs);
        if (rec.getError() != null)
        {
          throw new Exception("Error loading development record: " + rec.getError());
        }
        developments.add(rec);
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::getDevelopments(appSeqNum = " 
        + appSeqNum + ")";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
    return developments;
  }
    
  /*
  ** public boolean getSummaryData()
  **
  ** Loads current summary information about the prospect based on the call
  ** history and most recent developments.
  **
  ** RETURNS: true if successfully loaded, else false.
  */
  public boolean getSummaryData()
  {
    boolean getOk = false;
    
    long appSeqNum  = getAppSeqNum();
      
    try
    {
      connect();
      
      // load most recent prospect status and quality
      int curStat = -1;
      int curQual = -1;
      java.sql.Date oldestDate;
      float discountRate;
      float centsPerTran;
      float monthlySales;
      float annualCardSales;
      float averageTicket;
      
      /*@lineinfo:generated-code*//*@lineinfo:536^7*/

//  ************************************************************
//  #sql [Ctx] { select    c.current_status,
//                    c.current_outlook,
//                    nvl(t.total_minutes,0),
//                    t.total_calls,
//                    t.total_mileage,
//                    d.oldest_date,
//                    nvl(p.discount_rate,0),
//                    nvl(p.cents_per_tran,0),
//                    nvl(p.monthly_sales,0),
//                    nvl(p.annual_sales,0),
//                    nvl(p.average_ticket,0)
//                    
//          
//                    
//          from      ( select  sum(call_duration)        total_minutes,
//                              nvl(sum(call_mileage),0)  total_mileage,
//                              count(*)                  total_calls
//                      from    prospect_developments
//                      where   app_seq_num = :appSeqNum
//                              and call_duration > 0 ) t,
//                    ( select  status    current_status,
//                              quality   current_outlook,
//                              rownum
//                      from    prospect_developments
//                      where   app_seq_num = :appSeqNum
//                              and dev_date in ( select  max(dev_date)
//                                                from    prospect_developments
//                                                where   app_seq_num = :appSeqNum )
//                              and rownum = 1 ) c,
//                    ( select    dev_date oldest_date
//                      from      prospect_developments
//                      where     app_seq_num = :appSeqNum
//                                and rownum = 1
//                      order by  dev_date asc ) d,
//                    prospects p
//                    
//          where     p.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    c.current_status,\n                  c.current_outlook,\n                  nvl(t.total_minutes,0),\n                  t.total_calls,\n                  t.total_mileage,\n                  d.oldest_date,\n                  nvl(p.discount_rate,0),\n                  nvl(p.cents_per_tran,0),\n                  nvl(p.monthly_sales,0),\n                  nvl(p.annual_sales,0),\n                  nvl(p.average_ticket,0)\n                  \n         \n                  \n        from      ( select  sum(call_duration)        total_minutes,\n                            nvl(sum(call_mileage),0)  total_mileage,\n                            count(*)                  total_calls\n                    from    prospect_developments\n                    where   app_seq_num =  :1 \n                            and call_duration > 0 ) t,\n                  ( select  status    current_status,\n                            quality   current_outlook,\n                            rownum\n                    from    prospect_developments\n                    where   app_seq_num =  :2 \n                            and dev_date in ( select  max(dev_date)\n                                              from    prospect_developments\n                                              where   app_seq_num =  :3  )\n                            and rownum = 1 ) c,\n                  ( select    dev_date oldest_date\n                    from      prospect_developments\n                    where     app_seq_num =  :4 \n                              and rownum = 1\n                    order by  dev_date asc ) d,\n                  prospects p\n                  \n        where     p.app_seq_num =  :5";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.prospects.DevelopProspect",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setLong(3,appSeqNum);
   __sJT_st.setLong(4,appSeqNum);
   __sJT_st.setLong(5,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 11) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(11,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   curStat = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   curQual = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   minutesSpent = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   totalCalls = __sJT_rs.getInt(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   totalMileage = __sJT_rs.getInt(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   oldestDate = (java.sql.Date)__sJT_rs.getDate(6);
   discountRate = __sJT_rs.getFloat(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   centsPerTran = __sJT_rs.getFloat(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   monthlySales = __sJT_rs.getFloat(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   annualCardSales = __sJT_rs.getFloat(10); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   averageTicket = __sJT_rs.getFloat(11); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:585^7*/

      // get status and quality strings
      currentStatus = getStatusDescription(Integer.toString(curStat));
      try { currentQuality = qualities[curQual]; }
      catch (Exception e) { currentQuality = "Unknown Outlook"; }

      // determine age of prospect
      long curMSecs = Calendar.getInstance().getTime().getTime();
      long diffMSecs = curMSecs - oldestDate.getTime();
      daysOld = (int)(diffMSecs / 86400000L);
      
      // calculated projected revenue
      long temp = (long)((annualCardSales * 100) * (discountRate / 100));
      projectedDiscount = (temp / 100);
      temp = (long)((annualCardSales / averageTicket) * (centsPerTran * 100));
      projectedDiscount += (temp / 100);
      
      getOk = true;
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::getSummaryData(appSeqNum = " 
        + appSeqNum + ")";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
    return getOk;
  }
  
  /*
  ** public boolean load()
  **
  ** Loads prospect and summary data.
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean load()
  {
    boolean loadOk = false;
    
    long appSeqNum  = getAppSeqNum();
    
    // if no appSeqNum value defined, can't load
    if (appSeqNum == -1L)
    {
      return false;
    }
    int prog = 0;
    try
    {
      connect();
      
      // local query result holders
      String merchantDBA;
      String address1;
      String address2;
      String city;
      String state;
      String zip;
      String contactName;
      String contactPhone;
      String contactEmail;
      String mailName;
      String mailAddress1;
      String mailAddress2;
      String mailCity;
      String mailState;
      String mailZip;
      String businessType;
      String businessIndustry;
      String percentMoto;
      String businessDescription;
      String businessLocation;
      String businessLocationCount;
      String monthlySales;
      String annualSales;
      String averageTicket;
      String discountRate;
      String centsPerTran;
      String priorHistory;
      String changeReason;

      // load the prospect data
      /*@lineinfo:generated-code*//*@lineinfo:675^7*/

//  ************************************************************
//  #sql [Ctx] { select  merchant_dba,
//                  address1,
//                  address2,
//                  city,
//                  state,
//                  zip,
//  
//                  contact_name,
//                  contact_phone,
//                  contact_email,
//                  
//                  mail_name,
//                  mail_address1,
//                  mail_address2,
//                  mail_city,
//                  mail_state,
//                  mail_zip,
//                  
//                  business_type,
//                  business_industry,
//                  percent_moto,
//                  business_description,
//                  business_location,
//                  business_location_count,
//  
//                  monthly_sales,
//                  annual_sales,
//                  average_ticket,
//                  discount_rate,
//                  cents_per_tran,
//                  
//                  prior_history,
//                  change_reason
//                  
//          
//          
//          from    prospects
//          
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merchant_dba,\n                address1,\n                address2,\n                city,\n                state,\n                zip,\n\n                contact_name,\n                contact_phone,\n                contact_email,\n                \n                mail_name,\n                mail_address1,\n                mail_address2,\n                mail_city,\n                mail_state,\n                mail_zip,\n                \n                business_type,\n                business_industry,\n                percent_moto,\n                business_description,\n                business_location,\n                business_location_count,\n\n                monthly_sales,\n                annual_sales,\n                average_ticket,\n                discount_rate,\n                cents_per_tran,\n                \n                prior_history,\n                change_reason\n                \n         \n        \n        from    prospects\n        \n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.prospects.DevelopProspect",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 28) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(28,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantDBA = (String)__sJT_rs.getString(1);
   address1 = (String)__sJT_rs.getString(2);
   address2 = (String)__sJT_rs.getString(3);
   city = (String)__sJT_rs.getString(4);
   state = (String)__sJT_rs.getString(5);
   zip = (String)__sJT_rs.getString(6);
   contactName = (String)__sJT_rs.getString(7);
   contactPhone = (String)__sJT_rs.getString(8);
   contactEmail = (String)__sJT_rs.getString(9);
   mailName = (String)__sJT_rs.getString(10);
   mailAddress1 = (String)__sJT_rs.getString(11);
   mailAddress2 = (String)__sJT_rs.getString(12);
   mailCity = (String)__sJT_rs.getString(13);
   mailState = (String)__sJT_rs.getString(14);
   mailZip = (String)__sJT_rs.getString(15);
   businessType = (String)__sJT_rs.getString(16);
   businessIndustry = (String)__sJT_rs.getString(17);
   percentMoto = (String)__sJT_rs.getString(18);
   businessDescription = (String)__sJT_rs.getString(19);
   businessLocation = (String)__sJT_rs.getString(20);
   businessLocationCount = (String)__sJT_rs.getString(21);
   monthlySales = (String)__sJT_rs.getString(22);
   annualSales = (String)__sJT_rs.getString(23);
   averageTicket = (String)__sJT_rs.getString(24);
   discountRate = (String)__sJT_rs.getString(25);
   centsPerTran = (String)__sJT_rs.getString(26);
   priorHistory = (String)__sJT_rs.getString(27);
   changeReason = (String)__sJT_rs.getString(28);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:748^7*/
      
      fields.setData("merchantDBA",merchantDBA);
      fields.setData("address1",address1);
      fields.setData("address2",address2);
      fields.setData("city",city);
      fields.setData("state",state);
      fields.setData("zip",zip);
      fields.setData("contactName",contactName);
      fields.setData("contactPhone",contactPhone);
      fields.setData("contactEmail",contactEmail);
      fields.setData("mailName",mailName);
      fields.setData("mailAddress1",mailAddress1);
      fields.setData("mailAddress2",mailAddress2);
      fields.setData("mailCity",mailCity);
      fields.setData("mailState",mailState);
      fields.setData("mailZip",mailZip);
      fields.setData("businessType",businessType);
      fields.setData("businessIndustry",businessIndustry);
      fields.setData("percentMoto",percentMoto);
      fields.setData("businessDescription",businessDescription);
      fields.setData("businessLocation",businessLocation);
      fields.setData("businessLocationCount",businessLocationCount);
      fields.setData("monthlySales",monthlySales);
      fields.setData("annualSales",annualSales);
      fields.setData("averageTicket",averageTicket);
      fields.setData("discountRate",discountRate);
      fields.setData("centsPerTran",centsPerTran);
      fields.setData("priorHistory",priorHistory);
      fields.setData("changeReason",changeReason);
      if (fields.getData("mailAddress1").length() > 0)
      {
        fields.setData("hasMail","y");
      }
      loadOk = true;
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::load(appSeqNum = " 
        + appSeqNum + ") " + prog;
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
    if (loadOk)
    {
      loadOk = getSummaryData();
    }
    
    if (loadOk)
    {
      fields.setData("isNew","no");
    }
    
    return loadOk;
  }
  
  /*
  ** public boolean store()
  **
  ** Inserts/updates the prospect information and inserts a new development
  ** record.
  **
  ** RETURNS: true if data was stored successfully else false.
  */
  public boolean store()
  {
    boolean storeOk = false;
    
    long appSeqNum  = getAppSeqNum();
    long userId     = getUserId();
      
    try
    {
      connect();
      
      boolean isNewProspect = (appSeqNum == -1L);

      // insert new records if new
      if (isNewProspect)
      {
        // load an application sequence number
        /*@lineinfo:generated-code*//*@lineinfo:835^9*/

//  ************************************************************
//  #sql [Ctx] { select  application_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  application_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.prospects.DevelopProspect",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:840^9*/
        
        // insert a new prospect record
        /*@lineinfo:generated-code*//*@lineinfo:843^9*/

//  ************************************************************
//  #sql [Ctx] { insert into prospects
//            ( app_seq_num,
//              user_id,
//              create_date,
//              
//              merchant_dba,
//              address1,
//              address2,
//              city,
//              state,
//              zip,
//              
//              contact_name,
//              contact_phone,
//              contact_email,
//              
//              mail_name,
//              mail_address1,
//              mail_address2,
//              mail_city,
//              mail_state,
//              mail_zip,
//              
//              business_type,
//              business_industry,
//              business_description,
//              business_location,
//              business_location_count,
//              
//              prior_history,
//              change_reason,
//              
//              monthly_sales,
//              annual_sales,
//              average_ticket,
//              discount_rate,
//              cents_per_tran,
//              percent_moto )
//            values
//            ( :appSeqNum,
//              :userId,
//              sysdate,
//              :fields.getData("merchantDBA"),
//              :fields.getData("address1"),
//              :fields.getData("address2"),
//              :fields.getData("city"),
//              :fields.getData("state"),
//              :fields.getData("zip"),
//              :fields.getData("contactName"),
//              :fields.getData("contactPhone"),
//              :fields.getData("contactEmail"),
//              :fields.getData("mailName"),
//              :fields.getData("mailAddress1"),
//              :fields.getData("mailAddress2"),
//              :fields.getData("mailCity"),
//              :fields.getData("mailState"),
//              :fields.getData("mailZip"),
//              :fields.getData("businessType"),
//              :fields.getData("businessIndustry"),
//              :fields.getData("businessDescription"),
//              :fields.getData("businessLocation"),
//              :fields.getData("businessLocationCount"),
//              :fields.getData("priorHistory"),
//              :fields.getData("changeReason"),
//              :fields.getData("monthlySales"),
//              :fields.getData("annualSales"),
//              :fields.getData("averageTicket"),
//              :fields.getData("discountRate"),
//              :fields.getData("centsPerTran"),
//              :fields.getData("percentMoto") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_347 = fields.getData("merchantDBA");
 String __sJT_348 = fields.getData("address1");
 String __sJT_349 = fields.getData("address2");
 String __sJT_350 = fields.getData("city");
 String __sJT_351 = fields.getData("state");
 String __sJT_352 = fields.getData("zip");
 String __sJT_353 = fields.getData("contactName");
 String __sJT_354 = fields.getData("contactPhone");
 String __sJT_355 = fields.getData("contactEmail");
 String __sJT_356 = fields.getData("mailName");
 String __sJT_357 = fields.getData("mailAddress1");
 String __sJT_358 = fields.getData("mailAddress2");
 String __sJT_359 = fields.getData("mailCity");
 String __sJT_360 = fields.getData("mailState");
 String __sJT_361 = fields.getData("mailZip");
 String __sJT_362 = fields.getData("businessType");
 String __sJT_363 = fields.getData("businessIndustry");
 String __sJT_364 = fields.getData("businessDescription");
 String __sJT_365 = fields.getData("businessLocation");
 String __sJT_366 = fields.getData("businessLocationCount");
 String __sJT_367 = fields.getData("priorHistory");
 String __sJT_368 = fields.getData("changeReason");
 String __sJT_369 = fields.getData("monthlySales");
 String __sJT_370 = fields.getData("annualSales");
 String __sJT_371 = fields.getData("averageTicket");
 String __sJT_372 = fields.getData("discountRate");
 String __sJT_373 = fields.getData("centsPerTran");
 String __sJT_374 = fields.getData("percentMoto");
   String theSqlTS = "insert into prospects\n          ( app_seq_num,\n            user_id,\n            create_date,\n            \n            merchant_dba,\n            address1,\n            address2,\n            city,\n            state,\n            zip,\n            \n            contact_name,\n            contact_phone,\n            contact_email,\n            \n            mail_name,\n            mail_address1,\n            mail_address2,\n            mail_city,\n            mail_state,\n            mail_zip,\n            \n            business_type,\n            business_industry,\n            business_description,\n            business_location,\n            business_location_count,\n            \n            prior_history,\n            change_reason,\n            \n            monthly_sales,\n            annual_sales,\n            average_ticket,\n            discount_rate,\n            cents_per_tran,\n            percent_moto )\n          values\n          (  :1 ,\n             :2 ,\n            sysdate,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.prospects.DevelopProspect",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,userId);
   __sJT_st.setString(3,__sJT_347);
   __sJT_st.setString(4,__sJT_348);
   __sJT_st.setString(5,__sJT_349);
   __sJT_st.setString(6,__sJT_350);
   __sJT_st.setString(7,__sJT_351);
   __sJT_st.setString(8,__sJT_352);
   __sJT_st.setString(9,__sJT_353);
   __sJT_st.setString(10,__sJT_354);
   __sJT_st.setString(11,__sJT_355);
   __sJT_st.setString(12,__sJT_356);
   __sJT_st.setString(13,__sJT_357);
   __sJT_st.setString(14,__sJT_358);
   __sJT_st.setString(15,__sJT_359);
   __sJT_st.setString(16,__sJT_360);
   __sJT_st.setString(17,__sJT_361);
   __sJT_st.setString(18,__sJT_362);
   __sJT_st.setString(19,__sJT_363);
   __sJT_st.setString(20,__sJT_364);
   __sJT_st.setString(21,__sJT_365);
   __sJT_st.setString(22,__sJT_366);
   __sJT_st.setString(23,__sJT_367);
   __sJT_st.setString(24,__sJT_368);
   __sJT_st.setString(25,__sJT_369);
   __sJT_st.setString(26,__sJT_370);
   __sJT_st.setString(27,__sJT_371);
   __sJT_st.setString(28,__sJT_372);
   __sJT_st.setString(29,__sJT_373);
   __sJT_st.setString(30,__sJT_374);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:915^9*/
      }
      // else update existing record
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:920^9*/

//  ************************************************************
//  #sql [Ctx] { update  prospects
//            
//            set     merchant_dba            = :fields.getData("merchantDBA"),
//                    address1                = :fields.getData("address1"),
//                    address2                = :fields.getData("address2"),
//                    city                    = :fields.getData("city"),
//                    state                   = :fields.getData("state"),
//                    zip                     = :fields.getData("zip"),
//                    contact_name            = :fields.getData("contactName"),
//                    contact_phone           = :fields.getData("contactPhone"),
//                    contact_email           = :fields.getData("contactEmail"),
//                    mail_name               = :fields.getData("mailName"),
//                    mail_address1           = :fields.getData("mailAddress1"),
//                    mail_address2           = :fields.getData("mailAddress2"),
//                    mail_city               = :fields.getData("mailCity"),
//                    mail_state              = :fields.getData("mailState"),
//                    mail_zip                = :fields.getData("mailZip"),
//                    business_type           = :fields.getData("businessType"),
//                    business_industry       = :fields.getData("businessIndustry"),
//                    business_description    = :fields.getData("businessDescription"),
//                    business_location       = :fields.getData("businessLocation"),
//                    business_location_count = :fields.getData("businessLocationCount"),
//                    prior_history           = :fields.getData("priorHistory"),
//                    change_reason           = :fields.getData("changeReason"),
//                    monthly_sales           = :fields.getData("monthlySales"),
//                    annual_sales            = :fields.getData("annualSales"),
//                    average_ticket          = :fields.getData("averageTicket"),
//                    discount_rate           = :fields.getData("discountRate"),
//                    cents_per_tran          = :fields.getData("centsPerTran"),
//                    percent_moto            = :fields.getData("percentMoto")
//            
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_375 = fields.getData("merchantDBA");
 String __sJT_376 = fields.getData("address1");
 String __sJT_377 = fields.getData("address2");
 String __sJT_378 = fields.getData("city");
 String __sJT_379 = fields.getData("state");
 String __sJT_380 = fields.getData("zip");
 String __sJT_381 = fields.getData("contactName");
 String __sJT_382 = fields.getData("contactPhone");
 String __sJT_383 = fields.getData("contactEmail");
 String __sJT_384 = fields.getData("mailName");
 String __sJT_385 = fields.getData("mailAddress1");
 String __sJT_386 = fields.getData("mailAddress2");
 String __sJT_387 = fields.getData("mailCity");
 String __sJT_388 = fields.getData("mailState");
 String __sJT_389 = fields.getData("mailZip");
 String __sJT_390 = fields.getData("businessType");
 String __sJT_391 = fields.getData("businessIndustry");
 String __sJT_392 = fields.getData("businessDescription");
 String __sJT_393 = fields.getData("businessLocation");
 String __sJT_394 = fields.getData("businessLocationCount");
 String __sJT_395 = fields.getData("priorHistory");
 String __sJT_396 = fields.getData("changeReason");
 String __sJT_397 = fields.getData("monthlySales");
 String __sJT_398 = fields.getData("annualSales");
 String __sJT_399 = fields.getData("averageTicket");
 String __sJT_400 = fields.getData("discountRate");
 String __sJT_401 = fields.getData("centsPerTran");
 String __sJT_402 = fields.getData("percentMoto");
   String theSqlTS = "update  prospects\n          \n          set     merchant_dba            =  :1 ,\n                  address1                =  :2 ,\n                  address2                =  :3 ,\n                  city                    =  :4 ,\n                  state                   =  :5 ,\n                  zip                     =  :6 ,\n                  contact_name            =  :7 ,\n                  contact_phone           =  :8 ,\n                  contact_email           =  :9 ,\n                  mail_name               =  :10 ,\n                  mail_address1           =  :11 ,\n                  mail_address2           =  :12 ,\n                  mail_city               =  :13 ,\n                  mail_state              =  :14 ,\n                  mail_zip                =  :15 ,\n                  business_type           =  :16 ,\n                  business_industry       =  :17 ,\n                  business_description    =  :18 ,\n                  business_location       =  :19 ,\n                  business_location_count =  :20 ,\n                  prior_history           =  :21 ,\n                  change_reason           =  :22 ,\n                  monthly_sales           =  :23 ,\n                  annual_sales            =  :24 ,\n                  average_ticket          =  :25 ,\n                  discount_rate           =  :26 ,\n                  cents_per_tran          =  :27 ,\n                  percent_moto            =  :28 \n          \n          where   app_seq_num =  :29";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.prospects.DevelopProspect",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_375);
   __sJT_st.setString(2,__sJT_376);
   __sJT_st.setString(3,__sJT_377);
   __sJT_st.setString(4,__sJT_378);
   __sJT_st.setString(5,__sJT_379);
   __sJT_st.setString(6,__sJT_380);
   __sJT_st.setString(7,__sJT_381);
   __sJT_st.setString(8,__sJT_382);
   __sJT_st.setString(9,__sJT_383);
   __sJT_st.setString(10,__sJT_384);
   __sJT_st.setString(11,__sJT_385);
   __sJT_st.setString(12,__sJT_386);
   __sJT_st.setString(13,__sJT_387);
   __sJT_st.setString(14,__sJT_388);
   __sJT_st.setString(15,__sJT_389);
   __sJT_st.setString(16,__sJT_390);
   __sJT_st.setString(17,__sJT_391);
   __sJT_st.setString(18,__sJT_392);
   __sJT_st.setString(19,__sJT_393);
   __sJT_st.setString(20,__sJT_394);
   __sJT_st.setString(21,__sJT_395);
   __sJT_st.setString(22,__sJT_396);
   __sJT_st.setString(23,__sJT_397);
   __sJT_st.setString(24,__sJT_398);
   __sJT_st.setString(25,__sJT_399);
   __sJT_st.setString(26,__sJT_400);
   __sJT_st.setString(27,__sJT_401);
   __sJT_st.setString(28,__sJT_402);
   __sJT_st.setLong(29,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:954^9*/
      }
      
      // if call submit button caused this store(), then insert new call
      // record based on call fields
      if (fields.getData("submit").equals("submit"))
      {
        Calendar cal = Calendar.getInstance();
        java.sql.Timestamp curTime = new java.sql.Timestamp(cal.getTime().getTime());
        
        if (isNewProspect)
        {
          // insert referred to rep development record
          /*@lineinfo:generated-code*//*@lineinfo:967^11*/

//  ************************************************************
//  #sql [Ctx] { insert into prospect_developments
//              ( app_seq_num,
//                user_id,
//                dev_date,
//                status,
//                quality,
//                call_method,
//                call_date,
//                call_duration,
//                call_mileage,
//                notes )
//              values
//              ( :appSeqNum,
//                :userId,
//                :curTime,
//                0,
//                0,
//                0,
//                :curTime,
//                0,
//                0,
//                :"Created by " + user.getUserName() )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_403 = "Created by " + user.getUserName();
   String theSqlTS = "insert into prospect_developments\n            ( app_seq_num,\n              user_id,\n              dev_date,\n              status,\n              quality,\n              call_method,\n              call_date,\n              call_duration,\n              call_mileage,\n              notes )\n            values\n            (  :1 ,\n               :2 ,\n               :3 ,\n              0,\n              0,\n              0,\n               :4 ,\n              0,\n              0,\n               :5  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.prospects.DevelopProspect",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,userId);
   __sJT_st.setTimestamp(3,curTime);
   __sJT_st.setTimestamp(4,curTime);
   __sJT_st.setString(5,__sJT_403);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:991^11*/
          
          cal.add(Calendar.SECOND,1);
          curTime = new java.sql.Timestamp(cal.getTime().getTime());
        }
        
        // insert a new call record
        /*@lineinfo:generated-code*//*@lineinfo:998^9*/

//  ************************************************************
//  #sql [Ctx] { insert into prospect_developments
//            ( app_seq_num,
//              user_id,
//              dev_date,
//              status,
//              quality,
//              call_method,
//              call_date,
//              call_duration,
//              call_mileage,
//              notes )
//            values
//            ( :appSeqNum,
//              :userId,
//              :curTime,
//              :fields.getData("status"),
//              :fields.getData("quality"),
//              :fields.getData("callMethod"),
//              :((DateField)fields.getField("callDate")).getSqlDateString(),
//              :fields.getData("callDuration"),
//              :fields.getData("callMileage"),
//              :fields.getData("notes") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_404 = fields.getData("status");
 String __sJT_405 = fields.getData("quality");
 String __sJT_406 = fields.getData("callMethod");
 String __sJT_407 = ((DateField)fields.getField("callDate")).getSqlDateString();
 String __sJT_408 = fields.getData("callDuration");
 String __sJT_409 = fields.getData("callMileage");
 String __sJT_410 = fields.getData("notes");
   String theSqlTS = "insert into prospect_developments\n          ( app_seq_num,\n            user_id,\n            dev_date,\n            status,\n            quality,\n            call_method,\n            call_date,\n            call_duration,\n            call_mileage,\n            notes )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.prospects.DevelopProspect",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,userId);
   __sJT_st.setTimestamp(3,curTime);
   __sJT_st.setString(4,__sJT_404);
   __sJT_st.setString(5,__sJT_405);
   __sJT_st.setString(6,__sJT_406);
   __sJT_st.setString(7,__sJT_407);
   __sJT_st.setString(8,__sJT_408);
   __sJT_st.setString(9,__sJT_409);
   __sJT_st.setString(10,__sJT_410);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1022^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:1024^9*/

//  ************************************************************
//  #sql [Ctx] { update  prospects
//            set     status = :fields.getData("status")
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_411 = fields.getData("status");
   String theSqlTS = "update  prospects\n          set     status =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.prospects.DevelopProspect",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_411);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1029^9*/
      }
      
      storeOk = true;
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::store()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
    if (fields.getData("mailAddress1").length() > 0)
    {
      fields.setData("hasMail","y");
    }
      
    if (storeOk)
    {
      fields.setData("appSeqNum",Long.toString(appSeqNum));
      fields.setData("isNew","no");
      getSummaryData();
    }
    
    return storeOk;
  }
  
  public boolean reassign()
  {
    boolean reassignOk = false;
    
    if (store())
    {
      try
      {
        connect();
        
        DropDownField assignRep = (DropDownField)getField("assignRep");
        
        // change the user id associated with the prospect
        /*@lineinfo:generated-code*//*@lineinfo:1074^9*/

//  ************************************************************
//  #sql [Ctx] { update  prospects
//            
//            set     user_id = :fields.getData("assignRep")
//            
//            where   app_seq_num = :getAppSeqNum()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_412 = fields.getData("assignRep");
 long __sJT_413 = getAppSeqNum();
   String theSqlTS = "update  prospects\n          \n          set     user_id =  :1 \n          \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.prospects.DevelopProspect",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_412);
   __sJT_st.setLong(2,__sJT_413);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1081^9*/
        
        // insert reassignment development record
        /*@lineinfo:generated-code*//*@lineinfo:1084^9*/

//  ************************************************************
//  #sql [Ctx] { insert into prospect_developments
//          ( app_seq_num,
//            user_id,
//            dev_date,
//            status,
//            quality,
//            call_method,
//            call_date,
//            call_duration,
//            call_mileage,
//            notes )
//          values
//          ( :getAppSeqNum(),
//            :user.getUserId(),
//            sysdate,
//            0,
//            0,
//            0,
//            sysdate,
//            0,
//            0,
//            :"Reassigned to " + assignRep.getDescription() + " by " + user.getUserName())
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_414 = getAppSeqNum();
 long __sJT_415 = user.getUserId();
 String __sJT_416 = "Reassigned to " + assignRep.getDescription() + " by " + user.getUserName();
   String theSqlTS = "insert into prospect_developments\n        ( app_seq_num,\n          user_id,\n          dev_date,\n          status,\n          quality,\n          call_method,\n          call_date,\n          call_duration,\n          call_mileage,\n          notes )\n        values\n        (  :1 ,\n           :2 ,\n          sysdate,\n          0,\n          0,\n          0,\n          sysdate,\n          0,\n          0,\n           :3 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.prospects.DevelopProspect",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_414);
   __sJT_st.setLong(2,__sJT_415);
   __sJT_st.setString(3,__sJT_416);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1108^9*/
        
        reassignOk = true;
      }
      catch (Exception e)
      {
        String func = this.getClass().getName() + "::reassign()";
        String desc = "appSeqNum = " + getAppSeqNum() + ", " + e.toString();
        System.out.println(func + ": " + desc);
        logEntry(func,desc);
      }
      finally
      {
        cleanUp();
      }
    }
    
    return reassignOk;
  }
  
  private boolean isNew = true;
  public boolean getIsNew()
  {
    return isNew;
  }
  
  public boolean startApplication()
  {
    boolean startOk = false;
    
    long appSeqNum = Long.parseLong(getData("appSeqNum"));
    
    try
    {
      connect();
      
      ApplicationXDoc app = ApplicationXDoc.getEmptyInstance(appSeqNum);
      
      Calendar cal = Calendar.getInstance();
      long controlNum = 10000000000L;
      controlNum += ((cal.get(Calendar.YEAR) - 2000) * 100000000L);
      controlNum += (cal.get(Calendar.DAY_OF_YEAR) * 100000L);
      controlNum += appSeqNum;
      
      String appSrcType = "";
      int screenSeqId = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1154^7*/

//  ************************************************************
//  #sql [Ctx] { select  appsrctype_code,
//                  screen_sequence_id
//                  
//          
//                  
//          from    org_app
//          
//          where   app_type = :user.getAppType()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_417 = user.getAppType();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  appsrctype_code,\n                screen_sequence_id\n                \n         \n                \n        from    org_app\n        \n        where   app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.prospects.DevelopProspect",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,__sJT_417);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSrcType = (String)__sJT_rs.getString(1);
   screenSeqId = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1165^7*/
        
      app.setData("application.app_seq_num",            getData("appSeqNum"));
      app.setData("application.appsrctype_code",        appSrcType);
      app.setData("application.app_created_date",       cal.getTime());
      app.setData("application.app_user_id",            user.getUserId());
      app.setData("application.app_user_login",         user.getLoginName());
      app.setData("application.app_type",               user.getAppType());
      app.setData("application.screen_sequence_id",     screenSeqId);

      app.setData("merchant.app_seq_num",               getData("appSeqNum"));
      app.setData("merchant.merc_cntrl_number",         controlNum);
      
      app.setData("merchant.merch_business_name",       getData("merchantDBA"));
      
      app.setData("address[0].app_seq_num",             getData("appSeqNum"));
      app.setData("address[0].address_line1",           getData("address1"));
      app.setData("address[0].address_line2",           getData("address2"));
      app.setData("address[0].address_city",            getData("city"));
      app.setData("address[0].countrystate_code",       getData("state"));
      app.setData("address[0].address_zip",             getData("zip"));
      app.setData("address[0].addresstype_code",        "1");
      
      StringTokenizer st = new StringTokenizer(getData("contactName"));
      String firstName = st.nextToken();
      String lastName = "";
      if (st.hasMoreTokens())
      {
        lastName = st.nextToken();
      }
      
      app.setData("merchcontact.app_seq_num",               getData("appSeqNum"));
      app.setData("merchcontact.merchcont_prim_first_name", firstName);
      app.setData("merchcontact.merchcont_prim_last_name",  lastName);
      app.setData("merchcontact.merchcont_prim_phone",      getData("contactPhone"));
      app.setData("merchcontact.merchcont_prim_email",      getData("contactEmail"));
      
      if (getData("hasMail").equals("y"))
      {
        app.setData("address[1].app_seq_num",         getData("appSeqNum"));
        app.setData("address[1].address_line1",       getData("mailAddress1"));
        app.setData("address[1].address_line2",       getData("mailAddress2"));
        app.setData("address[1].address_city",        getData("mailCity"));
        app.setData("address[1].countrystate_code",   getData("mailState"));
        app.setData("address[1].address_zip",         getData("mailZip"));
        app.setData("address[1].addresstype_code",    "2");
      }
      
      app.setData("merchant.bustype_code",            getData("businessType"));
      app.setData("merchant.industype_code",          getData("businessIndustry"));
      app.setData("merchant.merch_mail_phone_sales",  getData("percentMoto"));
      app.setData("merchant.merch_busgoodserv_descr", getData("businessDescription"));
      app.setData("merchant.loctype_code",            getData("businessLocation"));
      app.setData("merchant.merch_num_of_locations",  getData("businessLocationCount"));
      
      float monthlySales = (Float.parseFloat(getData("annualSales")) / 12);
      long temp = (long)((monthlySales + 0.005) * 100);
      monthlySales = ((float)temp) / 100;
      String monthlySalesStr = Float.toString(monthlySales);
      
      app.setData("merchant.merch_month_tot_proj_sales",  getData("monthlySales"));
      app.setData("merchant.merch_month_visa_mc_sales",   monthlySalesStr);
      app.setData("merchant.merch_average_cc_tran",       getData("averageTicket"));
      app.setData("merchant.merch_prior_cc_accp_flag",    getData("priorHistory"));
      String changeReason = getData("changeReason");
      if (changeReason.length() >50)
      {
        changeReason = changeReason.substring(0,50);
      }
      app.setData("merchant.merch_term_reason",           changeReason);
      
      /*
      XMLOutputter fmt = new XMLOutputter();
      fmt.setIndent(true);
      fmt.setIndentSize(2);
      fmt.setNewlines(true);
      fmt.output(app.getDoc(),System.out);
      */
 
      app.exportXml(appSeqNum);
      
      startOk = true;           
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::startApplication()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
    
    return startOk;
  }
}/*@lineinfo:generated-code*/