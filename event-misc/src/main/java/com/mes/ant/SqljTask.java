//package com.mes.ant;
//
//import java.io.IOException;
//import java.io.File;
//import java.util.*;
//
//import org.apache.tools.ant.BuildException;
//import org.apache.tools.ant.DirectoryScanner;
//import org.apache.tools.ant.Project;
//import org.apache.tools.ant.Task;
//import org.apache.tools.ant.taskdefs.Execute;
//import org.apache.tools.ant.taskdefs.ExecuteJava;
//import org.apache.tools.ant.taskdefs.Java;
//import org.apache.tools.ant.taskdefs.Javac;
//import org.apache.tools.ant.taskdefs.MatchingTask;
//import org.apache.tools.ant.types.Path;
//import org.apache.tools.ant.types.Commandline;
//import org.apache.tools.ant.types.CommandlineJava;
//import org.apache.tools.ant.util.FileUtils;
//
//import sqlj.tools.Sqlj;
//
//public class SqljTask extends MatchingTask 
//{
//  private String srcBaseDir;
//  private String destDir;
//  private String javac;
//  private String sqljPropsFile;
//  private String genBaseDir;
//  private String pkgDir;
//  private String sqljOptions;
//
//
//  private Vector    FilesToCompile      = new Vector();
//  private List      PackageNames        = new ArrayList();
//  private String    PackageList         = null;
//  private Map       Packages            = new Hashtable();
//  private Vector    PackagesToBuild     = new Vector();
//  
//  private Path      classpath;
//  
//  public void setPkgDir(String pkgDir) 
//  {
//    this.pkgDir = FileUtils.translatePath(pkgDir);
//  }
//  
//  public void setJavac(String javac)
//  {
//    this.javac = FileUtils.translatePath(javac);
//  }
//  
//  public void setSqljOptions(String opts)
//  {
//    this.sqljOptions = opts;
//  }
//  
//  public void setSrcBaseDir(String srcBaseDir) 
//  {
//    this.srcBaseDir = FileUtils.translatePath(srcBaseDir);
//  }
//
//  public void setDestDir(String destDir) 
//  {
//    this.destDir = FileUtils.translatePath(destDir);
//  }
//    
//  public void setGenBaseDir(String genBaseDir) 
//  {
//    this.genBaseDir = FileUtils.translatePath(genBaseDir);
//  }
//  
//  public void setClasspath(String pathStr) 
//  {
//    classpath = new Path(getProject(),pathStr);
//  }
//  
//  public void setPackages( String pkgList )
//  {
//    PackageList = pkgList;
//  }
//  
//  public Path createClasspath() 
//  {
//    classpath = new Path(getProject());
//    return classpath;
//  }
//  
//  public void execute() throws BuildException 
//  {
//    String  originalClassPath = System.getProperty("java.class.path");
//    List    packageList       = null;
//    String  packageToBuild    = null;
//    String  srcPath           = null;
//  
//    try 
//    {
//      checkAttributes();
//    
//      //@setIncludes("**/*.sqlj");
//      setIncludes("**/*.sqlj **/*.java");
//      
//      System.setProperty("java.class.path",classpath.toString());
//      
//      if ( pkgDir != null && !pkgDir.equals("") )
//      {
//        srcPath = (srcBaseDir + File.separator + pkgDir);
//      }
//      else
//      {
//        // setup the package dependencies
//        convertListToVector(PackageList);
//      
//        srcPath = srcBaseDir;
//      }
//      
//      String[] sources = 
//          getDirectoryScanner(new File(srcPath)).getIncludedFiles();
//          
//      for (int i = 0; i < sources.length; ++i) 
//      {
//        if (isOutOfDate(sources[i])) 
//        {
//          addToCompiles(sources[i]);
//        }
//      }
//    
//      if ( FilesToCompile.size() > 0 )
//      {
//        // if the ant target specified the package build order
//        // then build in the only the packages specified
//        if ( PackagesToBuild.size() > 0 )
//        {
//          for ( int i = 0; i < PackagesToBuild.size(); ++i )
//          {
//            packageToBuild = (String) PackagesToBuild.elementAt(i);
//            
//            // clear the files list
//            FilesToCompile.removeAllElements();
//            
//            // go through the list of packages to find matching names
//            for (Iterator it = PackageNames.iterator(); it.hasNext();) 
//            {
//              String  packageName = (String)it.next();
//              
//              if ( packageName.indexOf(packageToBuild) < 0 )
//              {
//                continue;
//              }
//              
//              packageList = (List)Packages.get(packageName);
//          
//              log("\nProcessing " + packageList.size() 
//                  + " files in " + packageName);
//              log("------------------------------------------------");
//          
//              for (Iterator f = packageList.iterator(); f.hasNext();) 
//              {
//                String toCompile = (String)f.next();
//                FilesToCompile.add(toCompile);
//              }
//            }
//            
//            // if there are files for this package
//            // that need to be compiled, then execute
//            // the compile on the current list
//            if ( FilesToCompile.size() > 0 )
//            {
//              compile();
//            }
//          }
//        }
//        else
//        {
//          // compile the complete file list
//          compile();
//        }          
//      }
//      else
//      {
//        log("no files to compile");
//      }        
//    }
//    finally 
//    {
//      System.setProperty("java.class.path",originalClassPath);
//    }
//  }
//  
//  private void checkAttributes() 
//  {
//    if (srcBaseDir == null) 
//    {
//      throw new BuildException("srcdir must be set");
//    }
//    if (destDir == null) 
//    {
//      throw new BuildException("destdir must be set");
//    }
//    if (genBaseDir == null) 
//    {
//      throw new BuildException("gendir must be set");
//    }
//    if (classpath == null) 
//    {
//      throw new BuildException("classpath must be set");
//    }
//  }
//  
//  private boolean isOutOfDate(String srcName) 
//  {
//    boolean     retVal        = true;
//    String      pkgDirStr     = "";
//    
//    try 
//    {
//      String genName  = srcName.substring(0,srcName.lastIndexOf('.')) + ".java";
//      String destName = srcName.substring(0,srcName.lastIndexOf('.')) + ".class";
//      
//      // setup for the addition of the package
//      // dir string to to the full path filename
//      // this is only necessary if the ant target
//      // specified a package to build via the pkgDir
//      // parameter.
//      if ( pkgDir != null && !pkgDir.equals("") )
//      {
//        pkgDirStr = (pkgDir + File.separator);
//      }
//      
//      File srcFile = new File(srcBaseDir + File.separator +
//                              pkgDirStr + 
//                              srcName);
//      
//      // always compare to .class file
//      File destFile = new File(destDir + File.separator + 
//                               pkgDirStr + 
//                               destName);
//      retVal = ( !destFile.exists() ||
//                 (destFile.lastModified() < srcFile.lastModified()) );
//                 
//      if ( retVal )
//      {
//        // out of date, remove any previously generated 
//        // java source to prevent conflict between versions
//        File genFile = new File(genBaseDir + File.separator + 
//                                pkgDirStr + 
//                                genName);
//        if ( genFile.exists() )
//        {                                
//          genFile.delete();
//        }          
//      }
//    }
//    catch (Exception e) 
//    {
//      throw new BuildException("isOutOfDate() error on '" + srcName + "' - " + e);
//    }
//    return( retVal );
//  }
//  
//  
//  private void addToCompiles(String toCompile) 
//  {
//    String cfilename        = toCompile;
//    String packageName      = null;
//    
//    if ( pkgDir != null && !pkgDir.equals("") )
//    {
//      // add the appropriate package (if provided)
//      cfilename   = (pkgDir + File.separator + toCompile);
//      packageName = pkgDir;
//    }
//    else
//    {
//      try
//      {
//        // extract the package name from the full filename
//        packageName = toCompile.substring(0,toCompile.lastIndexOf(File.separator));
//      }      
//      catch( Exception e )
//      {
//      }
//    }      
//    
//    List packageList = (List)Packages.get(packageName);
//    if (packageList == null) 
//    {
//      packageList = new ArrayList();
//      Packages.put(packageName,packageList);
//      PackageNames.add(packageName);
//    }
//    packageList.add(cfilename);
//    
//    // add this filename to the compile list
//    FilesToCompile.add(cfilename);
//  }
//  
//  private void convertListToVector( String csvList )
//  {
//    String                pname       = null;
//    StringTokenizer       tokens      = null;
//    
//    tokens = new StringTokenizer(csvList,",");
//    PackagesToBuild.removeAllElements();
//    while( tokens.hasMoreTokens() )
//    {
//      pname = tokens.nextToken().replace('.',File.separator.charAt(0)).trim();
//      if ( !pname.equals("") )
//      {
//        PackagesToBuild.add(pname);
//        //@ log("added package '" + pname + "'");
//      }        
//    }
//  }
//  
//  private void compile() 
//  {
//    StringBuffer  cmdLineBuf  = new StringBuffer();
//    Project       project     = null;
//    String        srcName     = null;
//    int           status      = -1;
//  
//    try 
//    {
//      cmdLineBuf   = new StringBuffer();
//      
//      // d:\sqlj9.2.01\bin\sqlj.exe -ser2class -C-deprecation 
//      // -compiler-executable=d:\jdk1.4.2\bin\javac.exe
//      // -dir=../../../classes/../_sqljtemp/ 
//      // -classpath=d:\sqlj9.2.01\lib\runtime12.jar;
//      // d:\sqlj9.2.01\lib\translator.jar;../../../classes;
//      // d:\oracle\ora9/jdbc/lib/classes12.zip;;
//      // d:\bea\weblogic81\server\lib\weblogic.jar;
//      // d:\bea\weblogic81\server\lib\ojdbc14.jar 
//      // -d ../../../classes  <filenames> 
//     
//      // get a reference to the current project
//      project = getProject();
//      
//      for ( int i = 0; i < FilesToCompile.size(); ++i )
//      {
//        srcName = (String)FilesToCompile.elementAt(i);
//        File srcFile  = project.resolveFile(srcBaseDir + File.separator + srcName);
//        if ( i > 0 ) { cmdLineBuf.append(" "); }
//        cmdLineBuf.append(srcFile);
//        log("added file: " + srcName);//@
//      }
//    
//      // add the command line options        
//      cmdLineBuf.append(" -dir=" + genBaseDir);       // .java
//      cmdLineBuf.append(" -d=" + destDir);            // .ser, .class
//      cmdLineBuf.append(" -C-classpath=" + classpath.toString());
//    
//      // if an options file exists in the build base
//      // directory then use the options
//      File sqljOptsFile = new File("sqlj.properties");
//      if ( sqljOptsFile.exists() )
//      {
//        cmdLineBuf.append(" -props=sqlj.properties");
//      }        
//    
//      if ( javac != null && !javac.equals("") )
//      {
//        cmdLineBuf.append(" -compiler-executable=" + javac );
//      }        
//    
//      // if the user specified additional options in build.xml
//      if ( sqljOptions != null && !sqljOptions.equals("") )
//      {
//        cmdLineBuf.append(" ");
//        cmdLineBuf.append(sqljOptions);
//      }
//  
//      //@log(cmdLineBuf.toString()); //@
//      status = Sqlj.statusMain(cmdLineBuf.toString().split(" "));
//      Sqlj.resetStaticVariables();
//    }
//    catch (Exception e) 
//    {
//      if (e instanceof BuildException) 
//      {
//        throw (BuildException) e;
//      }
//      throw new BuildException("doSqlj() error on '" + srcName + "' - " + e);
//    }
//    checkStatus(status);
//  }
//  
//  private void checkStatus(int status) 
//  {
//    switch (status) 
//    {
//      case Sqlj.STATUS_SUCCESS:
//          break;
//            
//      case Sqlj.STATUS_TRANSLATE_ERROR:
//          throw new BuildException("sqlj translation error");
//            
//      case Sqlj.STATUS_COMPILE_ERROR:
//          throw new BuildException("sqlj java compilation (1) error");
//            
//      case Sqlj.STATUS_CUSTOMIZE_ERROR:
//          throw new BuildException("sqlj profile customization error");
//            
//      case Sqlj.STATUS_INSTRUMENT_ERROR:
//          throw new BuildException("sqlj class instrumentation error" + " (line mapping)");
//            
//      case Sqlj.STATUS_SER2CLASS_ERROR:
//          throw new BuildException("sqlj .ser to .class conversion " + "error");
//            
//      case Sqlj.STATUS_CALL_JAVA_COMPILE:
//          throw new BuildException("sqlj java compilation (2) error");
//            
//      case Sqlj.STATUS_CALL_SER2CLASS_COMPILE:
//          throw new BuildException("sqlj ser2class compile error");
//          
//      default:
//          throw new BuildException("sqlj unknown error");
//    }
//  }
//}
//
