/*@lineinfo:filename=HierarchyTypeTable*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.tools;

import java.sql.ResultSet;
import com.mes.support.SyncLog;
import sqlj.runtime.ResultSetIterator;

public class HierarchyTypeTable extends DropDownTable
{
  /*
  ** CONSTRUCTOR public HierarchyTypeTable()
  */
  public HierarchyTypeTable()
  {
    getData();
  }
  
  /*
  ** METHOD protected boolean getData()
  **
  ** Loads the sales reps that belong to the group given.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean getData()
  {
    boolean             getOk   = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:36^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  hier_type,
//                  name
//          from    t_hierarchy_types
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  hier_type,\n                name\n        from    t_hierarchy_types";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.HierarchyTypeTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.HierarchyTypeTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:41^7*/
      
      rs = it.getResultSet();
      
      while (rs.next())
      {
        addElement(rs);
        getOk = true;
      }
      
      it.close();
    }
    catch (Exception e)
    {
      SyncLog.LogEntry(this.getClass().getName(), 
        "getData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return getOk;
  }
}/*@lineinfo:generated-code*/