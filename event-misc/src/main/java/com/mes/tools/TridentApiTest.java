/*************************************************************************

  FILE: $Archive: /Java/servlets/com/mes/tools/TridentApiTest.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-01 14:41:48 -0700 (Tue, 01 Apr 2008) $
  Version            : $Revision: 14711 $

  Change History:
     See SVN database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.mes.support.SyncLog;

public class TridentApiTest
{
  private static final String       PRODUCTION_URL  = "https://api.merchante-solutions.com/mes-api/tridentApi";
  private static final String       STAGING_URL     = "http://10.1.4.19:8081/mes-api/tridentApi";
  private static final String       TEST_URL        = "http://10.1.4.30:8081/mes-api/tridentApi";
  
  protected   String            ApiUrl                = TEST_URL;
  protected   Vector            Requests              = new Vector();
  protected   Vector            Responses             = new Vector();
  protected   Vector            TransactionIds        = new Vector();
  
  protected String executeRequest( String request )
  {
    StringBuffer        buf           = new StringBuffer();
    byte[]              bytes         = null;
    HttpURLConnection   con           = null;
    String              paramString   = "";
    String              response      = null;
    URL                 url           = null;
  
    try
    {
      // connect to the api
      url     = new URL( request );
      con     = (HttpURLConnection)url.openConnection();
      bytes   = paramString.getBytes();
      
      // Indicate that you will be doing input and output, 
      // that the method is POST, and that the content
      // length is the length of the byte array
      con.setDoOutput(true);
      con.setDoInput(true);

/*************    
      int index = 0;
      while(true)
      {
        try
        {
          if ( con.getHeaderField(index) == null )
          {
            break;
          }
          
          System.out.println("HeaderField " + index + " : " + 
                            con.getHeaderFieldKey(index) + " = " +
                            con.getHeaderField(index));
          ++index;
        }
        catch(Exception e)
        {
          break;
        }
      }
***************/      
      
      BufferedReader httpIn = new BufferedReader( new InputStreamReader(con.getInputStream()));
      response = httpIn.readLine();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry( this.getClass().getName() + "::executeRequest()", e.toString());
    }        
    finally
    {
      try{ con.disconnect(); }catch(Exception e){}
    }
    return( response );    
  }

  public String extractField( String fieldName, String responseString )
  {
    int           index   = responseString.indexOf(fieldName)+(fieldName.length()+1);
    StringBuffer  field   = new StringBuffer();
    
    while(  index < responseString.length() && 
            responseString.charAt(index) != '&' )
    {
      field.append(responseString.charAt(index++));
    }
    return( field.toString() );
  }
  
  public Vector getRequests( )
  {
    return( Requests );
  }
  
  public Vector getResponses( )
  {
    return( Responses );
  }
  
  public String preprocRequest( String rawRequest )
  {
    int           endIndex  = 0;
    int           index     = 0;
    int           reqIdx    = 0;
    String        retVal    = rawRequest;
    
    if ( (index = rawRequest.indexOf("[tid-")) >= 0 )
    {
      endIndex = index+5;
      while( rawRequest.charAt(endIndex) != ']' ) 
      {
        ++endIndex;
      }
      ++endIndex; // add one to include the ']'
      
      String replaceOld = rawRequest.substring(index,endIndex);
      
      System.out.println("replaceOld = " + replaceOld);//@
      
      // one based for user, zero based for the vector
      reqIdx = (Integer.parseInt(rawRequest.substring(index+5,endIndex-1)) - 1);
      
      StringBuffer  temp = new StringBuffer(rawRequest);
      temp.replace(index,endIndex,(String)TransactionIds.elementAt(reqIdx));
      retVal = temp.toString();
    }
    return( retVal );
  }
  
  public void processFile( String filename )
  {
    processFile( filename, ApiUrl );
  }
  
  protected void processFile( String filename, String url )
  {
    BufferedReader      in            = null;
    String              line          = null;
    String              request       = null;
    String              response      = null;
    int                 retryCount    = 0;
    boolean             retry         = false;
    String              tranId        = null;
  
    try
    {
      in = new BufferedReader( new FileReader(filename) );
      
      while( (line = in.readLine()) != null )
      {
        line = line.trim();   // remove leading and trailing whitespace
        
        // skip blank lines and comments
        if ( line.trim().equals("") || line.startsWith("--") || 
             line.startsWith("#") || line.startsWith("//") ) 
        {
          continue;   
        }
        
        retryCount = 20;   // reset retry count
      
        // convert any params
        line = preprocRequest(line);
        
        request = url + "?" + line;
        
        while( retryCount-- >= 0 )
        {
          retry     = false;  // only retry under certain conditions
          response  = executeRequest(request);
          tranId    = extractField("transaction_id",response);
          
//@          System.out.println(response);   //@
//@          System.out.println("Transaction ID: " + tranId); //@
        
          if ( tranId.equals("error") )
          {
            String errorCode = extractField("error_code",response);
          
            //@if ( errorCode.equals("201") )
            if ( false )
            {
              Thread.sleep(1000);
              System.out.print("Retry Count: " + retryCount + "        \r");
              retry = true;
            }
          }
          else
          {
            TransactionIds.addElement(tranId);    
          }
          
          if ( !retry ) 
          {
            break;  // exit retry loop
          }
        }          
        
        // store the params and response
        Requests.addElement(line);
        Responses.addElement(response);
      }
      in.close();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry( this.getClass().getName() + "::processFile(" + filename + ")", e.toString());
    }
    finally
    {
      try{ in.close(); }catch(Exception e){}
      System.out.println();
    }
  }
  
  public String processUploadFile( String fileContent )
  {
    BufferedWriter      out           = null;
    String              workFilename  = "api-test-scripts-" + String.valueOf(this);
    
    try
    {
      out = new BufferedWriter( new FileWriter(workFilename, true) );
      out.write(fileContent);
      out.close();
    }
    catch(Exception e)
    {
      SyncLog.LogEntry( this.getClass().getName() + "::processUploadFile()", e.toString());
    }
    return( workFilename );
  }
  
  public boolean submitData( HttpServletRequest request )
  {
	ServletFileUpload  fileUpload      = null;
    boolean     retVal          = false;
    String      workFilename    = null;
    
    try
    {
      fileUpload = new ServletFileUpload(new DiskFileItemFactory(1048576, new File("./")));
      ArrayList items = (ArrayList)(fileUpload.parseRequest(request));
      
      for( int i = 0; i < items.size(); ++i )
      {
        DiskFileItem fi = (DiskFileItem)items.get(i);
        
        if( fi.getFieldName().equals("serverType") )
        {
          String temp = fi.getString();
          if ( temp.equals("prod") )
          {
            ApiUrl = PRODUCTION_URL;
          }
          else if ( temp.equals("stage") )
          {
            ApiUrl = STAGING_URL;
          }
          else    // default to test
          {
            ApiUrl = TEST_URL;
          }
        }
        else if( fi.getFieldName().equals("apiTestScript") )
        {
          // process the data file to generate a work file
          workFilename = processUploadFile( fi.getString() );
        }
      }
      
      if ( workFilename != null )
      {
        processFile( workFilename );
        (new File(workFilename)).delete();  // remove temp file
      }
      retVal = true;
    }
    catch(Exception e)
    {
      SyncLog.LogEntry( this.getClass().getName() + "::submitData()", e.toString());
    }
    return( retVal );
  }
  
  public static void main( String[] args )
  {
    TridentApiTest      test      = null;
    
    try
    {
      test = new TridentApiTest();
      test.processFile(args[0],args[1]);
      
      Vector requests   = test.getRequests();
      Vector responses  = test.getResponses();
      for( int i = 0; i < requests.size(); ++i )
      {
        System.out.println("REQ: " + (String)requests.elementAt(i));
        System.out.println("RSP: " + (String)responses.elementAt(i));
      }
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
    }
  }
}