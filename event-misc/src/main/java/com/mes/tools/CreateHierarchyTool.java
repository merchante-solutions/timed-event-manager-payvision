/*@lineinfo:filename=CreateHierarchyTool*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/CreateHierarchyTool.sqlj $

  Description:
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-07-25 09:15:41 -0700 (Thu, 25 Jul 2013) $
  Version            : $Revision: 21336 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import com.mes.database.SQLJConnectionBase;

public class CreateHierarchyTool extends SQLJConnectionBase
{
  private String    bankNumber      = "3941";
  private String    group1          = "";
  private String    group1Old       = "";
  private String    group1Name      = "";
  private String    group2          = "";
  private String    group2Old       = "";
  private String    group2Name      = "";
  private String    group3          = "";
  private String    group3Old       = "";
  private String    group3Name      = "";
  private String    group4          = "";
  private String    group4Old       = "";
  private String    group4Name      = "";
  private String    group5          = "";
  private String    group5Old       = "";
  private String    group5Name      = "";
  private String    assoc           = "";
  private String    assocOld        = "";
  private String    assocName       = "";
  private int       assocNumber     = 0;
  private boolean   submitted       = false;

  private String    error           = "";
  
  /*
  ** CONSTRUCTOR HierarchyTool
  */
  public CreateHierarchyTool()
  {
    // turn autocommit off
    super(false);
  }
  
  /*
  ** METHOD setSuccess
  */
  private void setSuccess()
  {
    this.error = "<font color=\"green\">SUCCESS</font>";
  }
  
  /*
  ** METHOD setError
  ** 
  ** Adds an error string to the error list
  */
  private void setError(String error)
  {
    StringBuffer newError = new StringBuffer(this.error);
    
    newError.append("<font color=\"red\">");
    newError.append(error);
    newError.append("</font><br>");
    
    this.error = newError.toString();
  }
  
  /*
  ** METHOD convertGroupNumber
  **
  ** Takes a 4-digit bank number and a 6-digit group number and combines them
  ** into a 10-digit number
  */  
  private String convertGroupNumber(String groupNumber)
  {
    StringBuffer  newStr  = new StringBuffer("");
    
    newStr.append(bankNumber);
    newStr.append(groupNumber);
    
    return newStr.toString();
  }
  
  public boolean isBlank(String test)
  {
    return(test == null || test.equals(""));
  }
  
  /*
  ** METHOD updateGroups
  **
  ** Updates the groups table
  */
  public void updateGroups(String association, GroupList groups)
    throws java.sql.SQLException
  {
    long[]  ga = groups.getGroups();
    
    // delete any existing records for this association
    /*@lineinfo:generated-code*//*@lineinfo:122^5*/

//  ************************************************************
//  #sql [Ctx] { delete from groups
//        where       assoc_number = :association
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from groups\n      where       assoc_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.tools.CreateHierarchyTool",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,association);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:126^5*/
    
    // insert the groups
    /*@lineinfo:generated-code*//*@lineinfo:129^5*/

//  ************************************************************
//  #sql [Ctx] { insert into groups
//        (
//          assoc_number,
//          group_1,
//          group_2,
//          group_3,
//          group_4,
//          group_5,
//          group_6,
//          group_7,
//          group_8,
//          group_9,
//          group_10
//        )
//        values
//        (
//          :association,
//          :groups.getGroup(0), 
//          :groups.getGroup(1), 
//          :groups.getGroup(2), 
//          :groups.getGroup(3), 
//          :groups.getGroup(4), 
//          :groups.getGroup(5), 
//          :groups.getGroup(6), 
//          :groups.getGroup(7), 
//          :groups.getGroup(8), 
//          :groups.getGroup(9) 
//        )
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_50 = groups.getGroup(0);
 long __sJT_51 = groups.getGroup(1);
 long __sJT_52 = groups.getGroup(2);
 long __sJT_53 = groups.getGroup(3);
 long __sJT_54 = groups.getGroup(4);
 long __sJT_55 = groups.getGroup(5);
 long __sJT_56 = groups.getGroup(6);
 long __sJT_57 = groups.getGroup(7);
 long __sJT_58 = groups.getGroup(8);
 long __sJT_59 = groups.getGroup(9);
   String theSqlTS = "insert into groups\n      (\n        assoc_number,\n        group_1,\n        group_2,\n        group_3,\n        group_4,\n        group_5,\n        group_6,\n        group_7,\n        group_8,\n        group_9,\n        group_10\n      )\n      values\n      (\n         :1 ,\n         :2 , \n         :3 , \n         :4 , \n         :5 , \n         :6 , \n         :7 , \n         :8 , \n         :9 , \n         :10 , \n         :11  \n      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.tools.CreateHierarchyTool",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,association);
   __sJT_st.setLong(2,__sJT_50);
   __sJT_st.setLong(3,__sJT_51);
   __sJT_st.setLong(4,__sJT_52);
   __sJT_st.setLong(5,__sJT_53);
   __sJT_st.setLong(6,__sJT_54);
   __sJT_st.setLong(7,__sJT_55);
   __sJT_st.setLong(8,__sJT_56);
   __sJT_st.setLong(9,__sJT_57);
   __sJT_st.setLong(10,__sJT_58);
   __sJT_st.setLong(11,__sJT_59);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:159^5*/
  }
  
  private int createOrganization(String orgType, String orgName, String orgGroup)
    throws java.sql.SQLException
  {
    int     count;
    String  realGroupName = orgName;
    int orgNumber;
    
    // add entry to group_names if it doesn't already exist
    /*@lineinfo:generated-code*//*@lineinfo:170^5*/

//  ************************************************************
//  #sql [Ctx] { select  count(group_number) 
//        from    group_names
//        where   group_number = :orgGroup
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(group_number)  \n      from    group_names\n      where   group_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.CreateHierarchyTool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,orgGroup);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^5*/
    
    //@ System.out.println("  * Updating group_names table");
    if(count == 0)
    {
      // add the entry
      /*@lineinfo:generated-code*//*@lineinfo:181^7*/

//  ************************************************************
//  #sql [Ctx] { insert into group_names
//          (
//            group_number,
//            group_name
//          )
//          values
//          (
//            :orgGroup,
//            :orgName
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into group_names\n        (\n          group_number,\n          group_name\n        )\n        values\n        (\n           :1 ,\n           :2 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.tools.CreateHierarchyTool",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,orgGroup);
   __sJT_st.setString(2,orgName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:193^7*/
    }
    else
    {
      /*@lineinfo:generated-code*//*@lineinfo:197^7*/

//  ************************************************************
//  #sql [Ctx] { select  group_name 
//          from    group_names
//          where   group_number = :orgGroup
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  group_name  \n        from    group_names\n        where   group_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tools.CreateHierarchyTool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,orgGroup);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   realGroupName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:202^7*/
    }
    
    // see if organization already exists
    /*@lineinfo:generated-code*//*@lineinfo:206^5*/

//  ************************************************************
//  #sql [Ctx] { select  count(org_num) 
//        from    organization
//        where   org_group = :orgGroup
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(org_num)  \n      from    organization\n      where   org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tools.CreateHierarchyTool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,orgGroup);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:211^5*/
    
    if(count > 0)
    {
      //@ System.out.println("  * getting organization number");
      // get this organization's org number
      /*@lineinfo:generated-code*//*@lineinfo:217^7*/

//  ************************************************************
//  #sql [Ctx] { select  org_num 
//          from    organization
//          where   org_group = :orgGroup
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  org_num  \n        from    organization\n        where   org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.tools.CreateHierarchyTool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,orgGroup);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:222^7*/
      //@ System.out.println("  * org number = " + orgNumber);
      
      //@ System.out.println("  * updating organization table");
      // update the organization
      /*@lineinfo:generated-code*//*@lineinfo:227^7*/

//  ************************************************************
//  #sql [Ctx] { update  organization
//          set     org_type_code = :orgType,
//                  org_name = :realGroupName,
//                  org_updated_date = sysdate,
//                  org_updated_by = 'HierarchyTool'
//          where   org_group = :orgGroup
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  organization\n        set     org_type_code =  :1 ,\n                org_name =  :2 ,\n                org_updated_date = sysdate,\n                org_updated_by = 'HierarchyTool'\n        where   org_group =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.tools.CreateHierarchyTool",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,orgType);
   __sJT_st.setString(2,realGroupName);
   __sJT_st.setString(3,orgGroup);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:235^7*/
    }
    else
    {
      //@ System.out.println("  * getting organization number");
      // get a unique org_number
      /*@lineinfo:generated-code*//*@lineinfo:241^7*/

//  ************************************************************
//  #sql [Ctx] { select  org_num_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  org_num_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.tools.CreateHierarchyTool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:246^7*/
      //@ System.out.println("  * org number = " + orgNumber);
      
      //@ System.out.println("  * inserting new organization");
      // insert the organization
      /*@lineinfo:generated-code*//*@lineinfo:251^7*/

//  ************************************************************
//  #sql [Ctx] { insert into organization
//          (
//            org_num,
//            org_type_code,
//            org_name,
//            org_group,
//            org_created_date,
//            org_created_by
//          )
//          values
//          (
//            :orgNumber,
//            :orgType,
//            :realGroupName,
//            :orgGroup,
//            sysdate,
//            'HierarchyTool'
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into organization\n        (\n          org_num,\n          org_type_code,\n          org_name,\n          org_group,\n          org_created_date,\n          org_created_by\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n          sysdate,\n          'HierarchyTool'\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.tools.CreateHierarchyTool",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,orgNumber);
   __sJT_st.setString(2,orgType);
   __sJT_st.setString(3,realGroupName);
   __sJT_st.setString(4,orgGroup);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:271^7*/
    }
    
    //@ System.out.println("  * updating orgassoc");
    // add the reference to the orgassoc table
    /*@lineinfo:generated-code*//*@lineinfo:276^5*/

//  ************************************************************
//  #sql [Ctx] { delete from orgassoc
//        where  org_num = :orgNumber
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from orgassoc\n      where  org_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.tools.CreateHierarchyTool",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,orgNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:280^5*/
    
    /*@lineinfo:generated-code*//*@lineinfo:282^5*/

//  ************************************************************
//  #sql [Ctx] { insert into orgassoc
//        (
//          org_num,
//          group_number
//        )
//        values
//        (
//          :orgNumber,
//          :orgGroup
//        )
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into orgassoc\n      (\n        org_num,\n        group_number\n      )\n      values\n      (\n         :1 ,\n         :2 \n      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.tools.CreateHierarchyTool",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,orgNumber);
   __sJT_st.setString(2,orgGroup);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:294^5*/
    
    return orgNumber;
  }
  
  private void cleanParentOrg(int orgNumber)
    throws java.sql.SQLException
  {
    /*@lineinfo:generated-code*//*@lineinfo:302^5*/

//  ************************************************************
//  #sql [Ctx] { delete from parent_org
//        where  org_num = :orgNumber
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from parent_org\n      where  org_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.tools.CreateHierarchyTool",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,orgNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:306^5*/
  }
  
  private void parentLink(int orgNum, int parentOrgNum)
    throws java.sql.SQLException
  {
    /*@lineinfo:generated-code*//*@lineinfo:312^5*/

//  ************************************************************
//  #sql [Ctx] { insert into parent_org
//        (
//          org_num,
//          parent_org_num
//        )
//        values
//        (
//          :orgNum,
//          :parentOrgNum
//        )
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into parent_org\n      (\n        org_num,\n        parent_org_num\n      )\n      values\n      (\n         :1 ,\n         :2 \n      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.tools.CreateHierarchyTool",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,orgNum);
   __sJT_st.setInt(2,parentOrgNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:324^5*/
  }
  
  private void createOrganizations(int[] groupOrgs, GroupList groups)
    throws java.sql.SQLException
  {
    assocNumber   = createOrganization("a", assocName, assoc);
    
    for(int i=0; i < groups.getGroupSize(); ++i)
    {
      groupOrgs[i] = createOrganization("a", groups.getGroupName(i), String.valueOf(groups.getGroup(i)));
    }
  }
  
  private void updateParentOrg(int[] groupOrgs, GroupList groups)
    throws java.sql.SQLException
  {
    cleanParentOrg(assocNumber);
    
    for(int i=0; i < groupOrgs.length; ++i)
    {
      cleanParentOrg(groupOrgs[i]);
    }
    
    // get the bank node for this bank number
    //@ System.out.println("  * getting bank node");
    int bankOrgNum;
    String bankNode = bankNumber + "00000";
    
    /*@lineinfo:generated-code*//*@lineinfo:353^5*/

//  ************************************************************
//  #sql [Ctx] { select org_num 
//        from   organization
//        where  org_group = :bankNode
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select org_num  \n      from   organization\n      where  org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.tools.CreateHierarchyTool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,bankNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankOrgNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:358^5*/
    
    // link groups
    for(int i=0; i < groups.getGroupSize(); ++i)
    {
      if(i == 0)
      {
        // link to bank org
        parentLink(groupOrgs[i], bankOrgNum);
      }
      else
      {
        parentLink(groupOrgs[i], groupOrgs[i - 1]);
      }
    }
    
    // link association to last org
    parentLink(assocNumber, groupOrgs[groups.getGroupSize() - 1]);
  }
  
  private void cleanGroupAssoc()
    throws java.sql.SQLException
  {
    /*@lineinfo:generated-code*//*@lineinfo:381^5*/

//  ************************************************************
//  #sql [Ctx] { delete from group_assoc
//        where assoc_number = :assoc
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from group_assoc\n      where assoc_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.tools.CreateHierarchyTool",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,assoc);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:385^5*/
  }
  
  private void groupLinkAssoc(String assoc, String group)
    throws java.sql.SQLException
  {
    /*@lineinfo:generated-code*//*@lineinfo:391^5*/

//  ************************************************************
//  #sql [Ctx] { insert into group_assoc
//        (
//          group_number,
//          assoc_number
//        )
//        values
//        (
//          :group,
//          :assoc
//        )
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into group_assoc\n      (\n        group_number,\n        assoc_number\n      )\n      values\n      (\n         :1 ,\n         :2 \n      )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.tools.CreateHierarchyTool",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,group);
   __sJT_st.setString(2,assoc);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:403^5*/
  }
  
  private void updateViewHierarchy()
  { 
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:410^7*/

//  ************************************************************
//  #sql [Ctx] { update  view_hierarchy_refresh
//          set     need_refresh = 'Y'
//          where   id = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  view_hierarchy_refresh\n        set     need_refresh = 'Y'\n        where   id = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.tools.CreateHierarchyTool",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:415^7*/
    }
    catch(Exception e)
    {
      logEntry("updateViewHierarchy()", e.toString());
    }
  }
  
  private void updateGroupAssoc(GroupList groups)
    throws java.sql.SQLException
  {
    // clean it out
    cleanGroupAssoc();
    
    groupLinkAssoc(assoc, assoc);
    
    for(int i=0; i < groups.getGroupSize(); ++i)
    {
      groupLinkAssoc(assoc, String.valueOf(groups.getGroup(i)));
    }
  }
  
  private boolean validate()
  {
    int     recCount  = 0;
    boolean result    = true;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:444^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    mbs_banks
//          where   bank_number = :bankNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    mbs_banks\n        where   bank_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.tools.CreateHierarchyTool",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:450^7*/
      
      // check to make sure that all the group numbers are filled out
      if ( recCount == 0 )
      {
        result = false;
        setError("Bank number must be 3941 or 3942 or 3858 or 3943 or 3003");
      }
      
      // checking group 1
      if(group1 == null || group1.equals(""))
      {
        result = false;
        setError("Group 1 cannot be blank");
      }
      else if(group1.length() != 6)
      {
        result = false;
        setError("Group 1 must be 6 digits");
      }
      else if(!group1.substring(0, 1).equals("5") && !bankNumber.equals("3858"))
      {
        result = false;
        setError("Group 1 must start with a '5' (" + group1.substring(1, 2) + ")");
      }
      
      // checking group 2
      if(group2 == null || group2.equals(""))
      {
        result = false;
        setError("Group 2 cannot be blank");
      }
      else if(group2.length() != 6)
      {
        result = false;
        setError("Group 2 must be 6 digits");
      }
      else if(!group2.substring(0, 1).equals("4") && !bankNumber.equals("3858"))
      {
        result = false;
        setError("Group 2 must start with a '4' (" + group2.substring(1, 2) + ")");
      }
      
      // checking group 3
      if(group3 == null || group3.equals(""))
      {
        result = false;
        setError("Group 3 cannot be blank");
      }
      else if(group3.length() != 6)
      {
        result = false;
        setError("Group 3 must be 6 digits");
      }
      else if(!group3.substring(0, 1).equals("3") && !bankNumber.equals("3858"))
      {
        result = false;
        setError("Group 3 must start with a '3' (" + group3.substring(1, 2) + ")");
      }
      
      // checking group 4
      if( bankNumber.equals("3858") )
      {
        if(group4 != null && ! group4.equals(""))
        {
          result = false;
          setError("Group 4 must be blank for bank 3858");
        }
        
        if(group5 != null && ! group5.equals(""))
        {
          result = false;
          setError("Group 5 must be blank for bank 3858");
        }
      }
      else
      {
        if(group4 == null || group4.equals(""))
        {
          result = false;
          setError("Group 4 cannot be blank");
        }
        else if(group4.length() != 6)
        {
          result = false;
          setError("Group 4 must be 6 digits");
        }
        else if(!group4.substring(0, 1).equals("2"))
        {
          result = false;
          setError("Group 4 must start with a '2'");
        }
      
        // checking group 5
        if(group5 == null || group5.equals(""))
        {
          result = false;
          setError("Group 5 cannot be blank");
        }
        else if(group5.length() != 6)
        {
          result = false;
          setError("Group 5 must be 6 digits");
        }
        else if(!group5.substring(0, 1).equals("1"))
        {
          result = false;
          setError("Group 5 must start with a '1'");
        }
      }
      
      // checking association
      if(assoc == null || assoc.equals(""))
      {
        result = false;
        setError("Association cannot be blank");
      }
      else if(assoc.length() != 6)
      {
        result = false;
        setError("Association must be 6 digits");
      }
      else if(!assoc.substring(0,1).equals("9") &&
              !assoc.substring(0,1).equals("8") && 
              !assoc.substring(0,1).equals("7") &&
              !assoc.substring(0,1).equals("6") &&
              !assoc.substring(0,1).equals("5") &&
              !assoc.substring(0,1).equals("4"))
      {
        result = false;
        setError("Association must start with a '9', an '8', a '7', a '6', a '5', or a '4'");
      }
    }
    catch(Exception e)
    {
      result = false;
      logEntry("validate()", e.toString());
      setError("validate(): " + e.toString());
    }
    
    return result;
  }
  
  public void submitData()
  {
    submitData(true);
  }
  
  public void submitData(boolean performValidation)
  {
    String    progress    = "";
    boolean   success     = false;
    GroupList groups      = new GroupList();
    int[]     groupOrgs   = new int[10];
    boolean   debug       = false;
        
    try
    {
      connect();
      
      setAutoCommit(false);
      
      if(!performValidation || validate())
      {
        if(debug)
        {
          System.out.println("Validation successful, ready to roll");
          success = true;
          setSuccess();
        }
        else
        {
          // convert the group and association numbers to hierarchy nodes      
          progress = "converting groups";
          //@System.out.println(progress);
          assoc = convertGroupNumber(assoc);
      
          if(!isBlank(group1))
          {
            groups.add(Long.parseLong(convertGroupNumber(group1)), group1Name);
          }
      
          if(!isBlank(group2))
          {
            groups.add(Long.parseLong(convertGroupNumber(group2)), group2Name);
          }
      
          if(!isBlank(group3))
          {
            groups.add(Long.parseLong(convertGroupNumber(group3)), group3Name);
          }
      
          if(!isBlank(group4))
          {
            groups.add(Long.parseLong(convertGroupNumber(group4)), group4Name);
          }
      
          if(!isBlank(group5))
          {
            groups.add(Long.parseLong(convertGroupNumber(group5)), group5Name);
          }
      
          // update the groups table
          progress = "updating groups table";
          //@System.out.println(progress);
          updateGroups(assoc, groups);
      
          // create the organizations
          progress = "creating organizations";
          //@System.out.println(progress);
          createOrganizations(groupOrgs, groups);
      
          // create parent_org links
          progress = "updating parent_org";
          //@System.out.println(progress);
          updateParentOrg(groupOrgs, groups);
      
          // update group_assoc table
          progress = "updating group_assoc";
          //@System.out.println(progress);
          updateGroupAssoc(groups);
          
          // update view_hierarchy_refresh
          updateViewHierarchy();
      
          success = true;
          setSuccess();
        }
      }
      else
      {
        success = false;
      }
    }
    catch(Exception e)
    {
      //@ System.out.println("submitData(" + progress + "): " + e.toString());
      setError("submitData(" + progress + "): " + e.toString());
      logEntry("submitData(" + progress + ")", e.toString());
    }
    finally
    {
      if(success)
      {
        commit();
      }
      else
      {
        rollback();
      }
      
      cleanUp();
    }
  }

  public void setBankNumber(String bankNumber)
  {
    this.bankNumber = bankNumber;
  }
  public String getBankNumber()
  {
    return this.bankNumber;
  }
  public void setGroup1(String group1)
  {
    this.group1 = group1;
    this.group1Old = group1;
  }
  public String getGroup1()
  {
    return this.group1Old;
  }
  public void setGroup1Name(String group1Name)
  {
    this.group1Name = group1Name;
  }
  public String getGroup1Name()
  {
    return this.group1Name;
  }
  public void setGroup2(String group2)
  {
    this.group2 = group2;
    this.group2Old = group2;
  }
  public String getGroup2()
  {
    return this.group2Old;
  }
  public void setGroup2Name(String group2Name)
  {
    this.group2Name = group2Name;
  }
  public String getGroup2Name()
  {
    return this.group2Name;
  }
  public void setGroup3(String group3)
  {
    this.group3 = group3;
    this.group3Old = group3;
  }
  public String getGroup3()
  {
    return this.group3Old;
  }
  public void setGroup3Name(String group3Name)
  {
    this.group3Name = group3Name;
  }
  public String getGroup3Name()
  {
    return this.group3Name;
  }
  public void setGroup4(String group4)
  {
    this.group4 = group4;
    this.group4Old = group4;
  }
  public String getGroup4()
  {
    return this.group4Old;
  }
  public void setGroup4Name(String group4Name)
  {
    this.group4Name = group4Name;
  }
  public String getGroup4Name()
  {
    return this.group4Name;
  }
  public void setGroup5(String group5)
  {
    this.group5 = group5;
    this.group5Old = group5;
  }
  public String getGroup5()
  {
    return this.group5Old;
  }
  public void setGroup5Name(String group5Name)
  {
    this.group5Name = group5Name;
  }
  public String getGroup5Name()
  {
    return this.group5Name;
  }
  public void setAssoc(String assoc)
  {
    this.assoc = assoc;
    this.assocOld = assoc;
  }
  public String getAssoc()
  {
    return this.assocOld;
  }
  public void setAssocName(String assocName)
  {
    this.assocName = assocName;
  }
  public String getAssocName()
  {
    return this.assocName;
  }
  public void setSubmit(String submit)
  {
    submitted = true;
  }
  public boolean isSubmitted()
  {
    return submitted;
  }
  
  public String getError()
  {
    return this.error;
  }
  
  public static void main( String[] args )
  {
    CreateHierarchyTool   tool    = new CreateHierarchyTool();
    
    try
    {
      SQLJConnectionBase.setConnectString(SQLJConnectionBase.getDirectConnectString());
      
      tool.connect(true);
    
      if ( "createHierarchy".equals(args[0]) )
      {
        // set all the info necessary within the bean
        tool.setBankNumber(args[1]);
        tool.setGroup1(args[2]);
        tool.setGroup1Name(args[3]);
        tool.setGroup2(args[4]);
        tool.setGroup2Name(args[5]);
        tool.setGroup3(args[6]);
        tool.setGroup3Name(args[7]);
        tool.setGroup4(args[8]);
        tool.setGroup4Name(args[9]);
        tool.setGroup5(args[10]);
        tool.setGroup5Name(args[11]);
        tool.setAssoc(args[12]);
        tool.setAssocName(args[13]);
    
        tool.submitData();
      }
    }
    catch( Exception e )
    {
      String    error = tool.getError();
      String    assoc = ((args.length >= 13) ? args[12] : "missing-assoc");
      com.mes.support.SyncLog.LogEntry(error, "CreateHierarchyTool::createHierarchy(" + assoc + ")");
    }
    finally
    {
      tool.cleanUp();
    }      
  }
}/*@lineinfo:generated-code*/