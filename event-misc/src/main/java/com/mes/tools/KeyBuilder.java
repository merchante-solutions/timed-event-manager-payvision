package com.mes.tools;

import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
// log4j classes.
import org.apache.log4j.Category;


/**
 * KeyBuilder
 * 
 * Constructs concrete sub-classes of Key interface.
 * Although the Key interface has a build() method, it is non-static.
 * This is the intended public entrypoint to assemble Key objects.
 */
public final class KeyBuilder
{
  // create class log category
  static Category log = Category.getInstance(KeyBuilder.class.getName());


  // Singleton
  public static KeyBuilder getInstance()
  {
    if(_instance==null)
      _instance = new KeyBuilder();
    
    return _instance;
  }
  public static void killInstance()
  {
    _instance = null;
  }
  private static KeyBuilder _instance = null;
  ///
  
  public List buildMany(HttpServletRequest request)
    throws Key.NonExistantKeyException
  {
    Vector v = new Vector(0,1);
    Key key = null;
    
    // try AccountKey
    key = new AccountKey();
    if(key.build(request))
      v.addElement(key);
    
    // try EquipmentKey
    key = new EquipmentKey();
    if(key.build(request))
      v.addElement(key);
    
    // no or unknown key type
    if(v.size()==0)
      throw new Key.NonExistantKeyException("No or unknown key type specified in HTTP request.");
    
    return v;
  }

  public Key build(HttpServletRequest request)
    throws Key.NonExistantKeyException
  {
    Key key;
    
    // try AccountKey
    key = new AccountKey();
    if(key.build(request))
      return key;
    
    // try EquipmentKey
    key = new EquipmentKey();
    if(key.build(request))
      return key;
    
    // no or unknown key type
    throw new Key.NonExistantKeyException("No or unknown key type specified in HTTP request.");
  }
  
  public AccountKey buildAccountKey(HttpServletRequest request)
    throws Key.NonExistantKeyException
  {
    AccountKey key = new AccountKey();
    if(!key.build(request))
      throw new Key.NonExistantKeyException("No or invalid Account Key specified in HTTP request.");
    return key;
  }

  public EquipmentKey buildEquipmentKey(HttpServletRequest request)
    throws Key.NonExistantKeyException
  {
    EquipmentKey key = new EquipmentKey();
    if(!key.build(request))
      throw new Key.NonExistantKeyException("No or invalid Equipment Key specified in HTTP request.");
    return key;
  }

  public EquipmentActionKey buildEquipmentActionKey(HttpServletRequest request)
    throws Key.NonExistantKeyException
  {
    EquipmentActionKey key = new EquipmentActionKey();
    if(!key.build(request))
      throw new Key.NonExistantKeyException("No or invalid Equipment Action Key specified in HTTP request.");
    return key;
  }
}
