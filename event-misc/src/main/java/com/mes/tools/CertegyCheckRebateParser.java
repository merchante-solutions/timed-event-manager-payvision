/*@lineinfo:filename=CertegyCheckRebateParser*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /com/mes/tools/CertegyCheckRebateParser.sqlj $

  Description:  


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionDirect;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import sqlj.runtime.ResultSetIterator;

public class CertegyCheckRebateParser extends SQLJConnectionDirect
{
  // data column indexes
  public static final int         DC_LINE_1_OFFSET        = 0;
  public static final int         DC_STATION_ID           = DC_LINE_1_OFFSET;
  public static final int         DC_NAME                 = 1;
  public static final int         DC_CONTACT              = 2;
  public static final int         DC_TYPE                 = 3;
  public static final int         DC_START_DATE           = 4;
  public static final int         DC_RATE_OR_GRID         = 5;
  public static final int         DC_PRI_SEC_CODES        = 6;
  public static final int         DC_CURRENT_AMOUNT       = 7;
  public static final int         DC_CURRENT_AMOUNT_SIGN  = 8;
  public static final int         DC_NET_DEBIT_AMOUNT     = 9;
  public static final int         DC_NET_CREDIT_AMOUNT    = 10;
  public static final int         DC_ADJUSTED_AMOUNT      = 11;
  public static final int         DC_ADJUSTED_AMOUNT_SIGN = 12;
  public static final int         DC_REBATE_AMOUNT        = 13;
  public static final int         DC_REBATE_AMOUNT_SIGN   = 14;
  public static final int         DC_REVENUE_AMOUNT       = 15;
  public static final int         DC_REVENUE_AMOUNT_SIGN  = 16;
  public static final int         DC_MINIMUM_AMOUNT       = 17;
  public static final int         DC_MINIMUM_AMOUNT_SIGN  = 18;
  // line 2
  public static final int         DC_LINE_2_OFFSET        = 19;
  public static final int         DC_ACCOUNT_NUMBER       = DC_LINE_2_OFFSET;
  public static final int         DC_ADDRESS              = 20;
  public static final int         DC_PHONE                = 21;
  public static final int         DC_SIC                  = 22;
  public static final int         DC_STATUS               = 23;
  public static final int         DC_CODES                = 24;
  // line 3
  public static final int         DC_LINE_3_OFFSET        = 25;
  public static final int         DC_CITY                 = DC_LINE_3_OFFSET;
  public static final int         DC_STATE                = 26;
  public static final int         DC_ZIP                  = 27;
  public static final int         DC_COUNT                = 28;
  
  public static final int         DC_HDR_LINES            = 10;
  public static final int         DC_PAGE_LINE_COUNT      = 54;
  public static final String      DC_REPORT_ID            = " CM34701-R01";
  
  public static final char        EOF_CHAR            = (char)0x1a;
  
  public static int[] DataOffsets =
  {
    DC_LINE_1_OFFSET,
    DC_LINE_2_OFFSET,
    DC_LINE_3_OFFSET,
  };    
  
  public static final int[][][] RecordColOffsets = 
  {
    { // line 1 fields
      {   1,  15 },     // DC_STATION_ID        
      {  16,  36 },     // DC_NAME              
      {  37,  53 },     // DC_CONTACT           
      {  54,  56 },     // DC_TYPE              
      {  58,  66 },     // DC_START_DATE        
      {  67,  71 },     // DC_RATE_OR_GRID      
      {  73,  81 },     // DC_PRI_SEC_CODES     
      {  82,  96 },     // DC_CURRENT_AMOUNT    
      {  96,  97 },     // DC_CURRENT_AMOUNT_SIGN
      {  97,  106 },    // DC_NET_DEBIT_AMOUNT  
      {  106, 116 },    // DC_NET_CREDIT_AMOUNT 
      {  116, 129 },    // DC_ADJUSTED_AMOUNT   
      {  129, 130 },    // DC_ADJUSTED_AMOUNT_SIGN
      {  130, 139 },    // DC_REBATE_AMOUNT     
      {  139, 140 },    // DC_REBATE_AMOUNT_SIGN
      {  140, 148 },    // DC_REVENUE_AMOUNT    
      {  148, 149 },    // DC_REVENUE_AMOUNT_SIGN
      {  149, 157 },    // DC_MINIMUM_AMOUNT    
      {  157, 158 },    // DC_MINIMUM_AMOUNT_SIGN
    },                         
    { // line 2 fields
      {  1,  15 },      // DC_ACCOUNT_NUMBER   
      {  16, 36 },      // DC_ADDRESS          
      {  37, 49 },      // DC_PHONE            
      {  53, 57  },     // DC_SIC              
      {  61, 71  },     // DC_STATUS           
      {  72, 82  },     // DC_CODES
    },                      
    { // line 3 fields
      {  16,  38 },     // DC_CITY             
      {  39,  41 },     // DC_STATE            
      {  43,  48 },     // DC_ZIP              
    },                      
  };
  
  private boolean             DebugMode       = false;

  public CertegyCheckRebateParser( )
  {
    PropertiesFile      propFile  = null;
    
    // load the properties file
    try
    {
      propFile = new PropertiesFile("mes.properties");
    }
    catch( Exception e )
    {
      InputStream inputStream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream("resources/mes.properties");      
        
      propFile = new PropertiesFile( inputStream );
      
      try
      {
        inputStream.close();
      }
      catch(Exception re) {}
    }
    
    DebugMode = propFile.getBoolean("com.mes.debug",false);
  }
  
  public boolean archiveFile( String dataFilename )
  {
    int           bytesRead       = 0;
    String        flagFilename    = null;
    int           offset          = -1;
    String        progress        = "";
    boolean       success         = false;
    byte[]        zipData         = new byte[4096];
    String        zipFilename     = null;
    
    try
    {    
      try
      {
        // chop the extension from the data filename to build the flag filename
        offset = dataFilename.lastIndexOf('.');
        if ( offset < 0 )
        {
          flagFilename  = dataFilename + ".flg";
          zipFilename   = dataFilename + ".zip";
        }
        else
        {
          flagFilename  = dataFilename.substring(0,offset) + ".flg";
          zipFilename   = dataFilename.substring(0,offset) + ".zip";
        }
        
        // create the archive .zip file
        progress = "preparing files for archive";
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFilename));
        zos.setMethod(ZipOutputStream.DEFLATED);
    
        // zip up the .dat file
        FileInputStream zipInFile = new FileInputStream(dataFilename);
        ZipEntry  dataZipEntry = new ZipEntry(dataFilename);
        dataZipEntry.setMethod(ZipEntry.DEFLATED);
        dataZipEntry.setComment("Incoming Certegy Check Rebate");
    
        zos.putNextEntry(dataZipEntry);
        while(bytesRead != -1)
        {
          bytesRead = zipInFile.read(zipData);
      
          if(bytesRead != -1)
          {
            // write these bytes to the zip file
            zos.write(zipData, 0, bytesRead);
          }
        }
        zos.closeEntry();
        zipInFile.close();
    
        // close the zip file
        zos.close();
        
        progress = "archiving files";
        FTPClient ftp = new FTPClient(MesDefaults.getString(MesDefaults.DK_ARCHIVE_HOST), 21);
        
        ftp.login(MesDefaults.getString(MesDefaults.DK_ARCHIVE_USER), 
                  MesDefaults.getString(MesDefaults.DK_ARCHIVE_PASS));
        ftp.setType(FTPTransferType.BINARY);
        ftp.chdir("arc/monthly");
        
        // put the zip file and a flag file to 
        // indicate that this zip should be encrypted
        ftp.put( zipFilename, zipFilename );
        ftp.put( dataFilename.getBytes(), flagFilename );
        
        ftp.quit();
        
        new File(dataFilename).delete();
        new File(flagFilename).delete();
        new File(zipFilename).delete();
        
        success = true;
      }
      catch(Exception ftpe)
      {
        logEntry("archiveFile(" + dataFilename + ")", progress + ": " + ftpe.toString());
      }
    }      
    catch( Exception e )
    {
      logEntry("archiveFile(" + dataFilename + ")", e.toString());
    }
    finally
    {
    }
    return( success );
  }
  
  public void loadCertegyCheckRebateFile( String loadFilename )
  {
    Date                  activeDate        = null;
    String[]              colData           = null;
    int                   extraCharOffset   = 0;
    BufferedReader        in                = null;
    String                line              = null;
    int                   lineCount         = 0;
    long                  loadFileId        = 0L;
    long                  merchantId        = 0L;
    int[][]               offsets           = null;
    int                   recordOffset      = 0;
    int                   recordRow         = 0;
    String                token             = null;
    
    try
    {
      connect();
      
      // make sure there is an index for this filename
      /*@lineinfo:generated-code*//*@lineinfo:286^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.CertegyCheckRebateParser",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:289^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:291^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename) 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 )  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.CertegyCheckRebateParser",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:295^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:297^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    certegy_check_rebate   ccr
//          where   ccr.load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    certegy_check_rebate   ccr\n        where   ccr.load_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.tools.CertegyCheckRebateParser",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:302^7*/
      
      // allocate storage for the ref array
      colData = new String[DC_COUNT];
      
      // open the text file
      in = new BufferedReader( new FileReader( loadFilename ) );
      
      while( ( line = in.readLine() ) != null )
      {
        if ( line.indexOf(EOF_CHAR) >= 0 )
        {
          break;
        }
        
        // Extract the report run date
        if ( line.startsWith("  MP # ") )
        {
          // assume that the run date is in the month
          // after the active date of the data
          // e.g. if run date = 12/03/2004 then
          //         active date = 11/01/2004 (month of november 2004)
          //
          /*@lineinfo:generated-code*//*@lineinfo:325^11*/

//  ************************************************************
//  #sql [Ctx] { select  trunc( (trunc( to_date( substr(:line,125,8),'mm/dd/yy' ), 'month' ) - 1),'month')
//                      
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc( (trunc( to_date( substr( :1 ,125,8),'mm/dd/yy' ), 'month' ) - 1),'month')\n                     \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tools.CertegyCheckRebateParser",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,line);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:330^11*/
        }
        
        // VG is in the first section of all detail records
        if ( (line.length() > 56) && line.substring(54,56).equals("VG") )
        {
          for( int recordLine = 0; recordLine < 3; ++recordLine )
          {
            recordOffset  = DataOffsets[recordLine];
          
            // get the data offsets for this line in the record
            offsets = RecordColOffsets[recordLine];
            
            for ( int col = 0; col < offsets.length; ++col )
            {
              // extract the token
              token = line.substring( offsets[col][0],
                                      Math.min(line.length(),offsets[col][1]) ).trim();
              System.out.println("token: " + token);//@                                      
              
              switch( (recordOffset+col) )
              {
                case DC_CURRENT_AMOUNT_SIGN:
                case DC_ADJUSTED_AMOUNT_SIGN:
                case DC_REBATE_AMOUNT_SIGN:
                case DC_REVENUE_AMOUNT_SIGN:
                case DC_MINIMUM_AMOUNT_SIGN:
                  if ( token.equals("-") )
                  {
                    // insert the negative sign as the first character
                    // of the previous amount field.
                    colData[recordOffset+col-1] = (token + colData[recordOffset+col-1]);
                  }                    
                  break;
              }
              
              // store the column data
              colData[recordOffset+col] = token;
            }
            
            // read the next line of the record
            if ( (recordLine+1) < 3 ) 
            {
              line = in.readLine();   
            }
          }
          
          merchantId = resolveMerchantId( colData[DC_STATION_ID] );
          
          if ( DebugMode == true )
          {
            for ( int i = 0; i < DC_COUNT; ++i )
            {
              System.out.println( i + ": " + colData[i]);
            }
          } 
          
          System.out.println("adj amt: " + colData[DC_ADJUSTED_AMOUNT]);//@
          /*@lineinfo:generated-code*//*@lineinfo:388^11*/

//  ************************************************************
//  #sql [Ctx] { insert into certegy_check_rebate
//              ( 
//                merchant_number,
//                active_date,
//                station_id,
//                dba_name,
//                contract_name,
//                type,
//                start_date,
//                rate_or_grid,
//                pri_sec_codes,
//                current_amount,
//                net_debit_amount,
//                net_credit_amount,
//                adjusted_amount,
//                rebate_amount,
//                revenue_amount,
//                minimum_amount,
//                account_number,
//                address,
//                phone,
//                sic_code,
//                status,
//                codes,
//                city,
//                state,
//                zip,
//                mes_rebate_rate,
//                mes_rebate_amount,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :merchantId, 
//                :activeDate,
//                :colData[DC_STATION_ID],
//                :colData[DC_NAME],
//                :colData[DC_CONTACT],
//                :colData[DC_TYPE],
//                to_date( :colData[DC_START_DATE], 'mm/dd/rr' ),
//                :colData[DC_RATE_OR_GRID],
//                :colData[DC_PRI_SEC_CODES],
//                :colData[DC_CURRENT_AMOUNT],
//                :colData[DC_NET_DEBIT_AMOUNT],
//                :colData[DC_NET_CREDIT_AMOUNT],
//                :colData[DC_ADJUSTED_AMOUNT],
//                :colData[DC_REBATE_AMOUNT],
//                :colData[DC_REVENUE_AMOUNT],
//                :colData[DC_MINIMUM_AMOUNT],
//                :colData[DC_ACCOUNT_NUMBER],
//                :colData[DC_ADDRESS],
//                :colData[DC_PHONE],
//                :colData[DC_SIC],
//                :colData[DC_STATUS],
//                :colData[DC_CODES],
//                :colData[DC_CITY],
//                :colData[DC_STATE],
//                :colData[DC_ZIP],
//                50,
//                (:colData[DC_ADJUSTED_AMOUNT] * 0.5),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_26 = colData[DC_STATION_ID];
 String __sJT_27 = colData[DC_NAME];
 String __sJT_28 = colData[DC_CONTACT];
 String __sJT_29 = colData[DC_TYPE];
 String __sJT_30 = colData[DC_START_DATE];
 String __sJT_31 = colData[DC_RATE_OR_GRID];
 String __sJT_32 = colData[DC_PRI_SEC_CODES];
 String __sJT_33 = colData[DC_CURRENT_AMOUNT];
 String __sJT_34 = colData[DC_NET_DEBIT_AMOUNT];
 String __sJT_35 = colData[DC_NET_CREDIT_AMOUNT];
 String __sJT_36 = colData[DC_ADJUSTED_AMOUNT];
 String __sJT_37 = colData[DC_REBATE_AMOUNT];
 String __sJT_38 = colData[DC_REVENUE_AMOUNT];
 String __sJT_39 = colData[DC_MINIMUM_AMOUNT];
 String __sJT_40 = colData[DC_ACCOUNT_NUMBER];
 String __sJT_41 = colData[DC_ADDRESS];
 String __sJT_42 = colData[DC_PHONE];
 String __sJT_43 = colData[DC_SIC];
 String __sJT_44 = colData[DC_STATUS];
 String __sJT_45 = colData[DC_CODES];
 String __sJT_46 = colData[DC_CITY];
 String __sJT_47 = colData[DC_STATE];
 String __sJT_48 = colData[DC_ZIP];
 String __sJT_49 = colData[DC_ADJUSTED_AMOUNT];
   String theSqlTS = "insert into certegy_check_rebate\n            ( \n              merchant_number,\n              active_date,\n              station_id,\n              dba_name,\n              contract_name,\n              type,\n              start_date,\n              rate_or_grid,\n              pri_sec_codes,\n              current_amount,\n              net_debit_amount,\n              net_credit_amount,\n              adjusted_amount,\n              rebate_amount,\n              revenue_amount,\n              minimum_amount,\n              account_number,\n              address,\n              phone,\n              sic_code,\n              status,\n              codes,\n              city,\n              state,\n              zip,\n              mes_rebate_rate,\n              mes_rebate_amount,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1 , \n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n              to_date(  :7 , 'mm/dd/rr' ),\n               :8 ,\n               :9 ,\n               :10 ,\n               :11 ,\n               :12 ,\n               :13 ,\n               :14 ,\n               :15 ,\n               :16 ,\n               :17 ,\n               :18 ,\n               :19 ,\n               :20 ,\n               :21 ,\n               :22 ,\n               :23 ,\n               :24 ,\n               :25 ,\n              50,\n              ( :26  * 0.5),\n               :27 ,\n               :28 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.tools.CertegyCheckRebateParser",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,__sJT_26);
   __sJT_st.setString(4,__sJT_27);
   __sJT_st.setString(5,__sJT_28);
   __sJT_st.setString(6,__sJT_29);
   __sJT_st.setString(7,__sJT_30);
   __sJT_st.setString(8,__sJT_31);
   __sJT_st.setString(9,__sJT_32);
   __sJT_st.setString(10,__sJT_33);
   __sJT_st.setString(11,__sJT_34);
   __sJT_st.setString(12,__sJT_35);
   __sJT_st.setString(13,__sJT_36);
   __sJT_st.setString(14,__sJT_37);
   __sJT_st.setString(15,__sJT_38);
   __sJT_st.setString(16,__sJT_39);
   __sJT_st.setString(17,__sJT_40);
   __sJT_st.setString(18,__sJT_41);
   __sJT_st.setString(19,__sJT_42);
   __sJT_st.setString(20,__sJT_43);
   __sJT_st.setString(21,__sJT_44);
   __sJT_st.setString(22,__sJT_45);
   __sJT_st.setString(23,__sJT_46);
   __sJT_st.setString(24,__sJT_47);
   __sJT_st.setString(25,__sJT_48);
   __sJT_st.setString(26,__sJT_49);
   __sJT_st.setString(27,loadFilename);
   __sJT_st.setLong(28,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:454^11*/
        }
        lineCount++;
      }
    }
    catch( Exception e )
    {
      logEntry( "loadFile(" + loadFilename + ", " + lineCount + ")", e.toString() );
    }
    finally
    {
      try{ in.close(); } catch(Exception e) {}
    }
  }
  
  public String processUploadFile( String fileContext )
  {
    int                 dataOffset    = 0;
    int                 lineCount     = 0;
    String              line          = null;
    StringTokenizer     lines         = null;
    int                 offset        = 0;
    BufferedWriter      out           = null;
    Date                runDate       = null;
    String              tempFilename  = "ccr3941.dat";
    StringBuffer        workFilename  = new StringBuffer();
    
    try
    {
      lines = new StringTokenizer(fileContext,System.getProperty("line.separator"));
      out   = new BufferedWriter( new FileWriter(tempFilename, true) );
      
      while( lines.hasMoreTokens() )
      {
        line = lines.nextToken();
        
        switch( lineCount )
        {
          case 0:
            dataOffset = (line.indexOf("CM34700-R01") - 1);
            break;
            
          case 1:
            offset  = (line.indexOf("RUN DATE")+9);
            java.util.Date javaDate = DateTimeFormatter.parseDate(line.substring(offset,offset+8),"MM/dd/yy");
            runDate = new Date(javaDate.getTime());
            break;
        }
        line = line.substring(dataOffset);
        
        out.write(line);
        out.newLine();
        ++lineCount;
      }
      out.close();
      
      // build the work filename
      workFilename.append("ccr3941_");
      workFilename.append(DateTimeFormatter.getFormattedDate(runDate,"MMddyy"));
      workFilename.append("_001.dat");
      
      File f = new File(tempFilename);
      f.renameTo( new File(workFilename.toString()) );
    }
    catch(Exception e)
    {
      logEntry("processUploadFile()", e.toString());
    }
    return( workFilename.toString() );
  }
  
  public long resolveMerchantId( String stationId )
  {
    ResultSetIterator     it                = null;
    long                  merchantId        = 0L;
    ResultSet             resultSet         = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:533^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number
//          from    merchpayoption  mpo,
//                  merchant        mr,
//                  mif             mf
//          where   mpo.cardtype_code = :mesConstants.APP_CT_CHECK_AUTH and
//                  mpo.merchpo_tid = :stationId and
//                  mr.app_seq_num = mpo.app_seq_num and
//                  mf.merchant_number = mr.merch_number
//          order by mf.merchant_number                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number\n        from    merchpayoption  mpo,\n                merchant        mr,\n                mif             mf\n        where   mpo.cardtype_code =  :1  and\n                mpo.merchpo_tid =  :2  and\n                mr.app_seq_num = mpo.app_seq_num and\n                mf.merchant_number = mr.merch_number\n        order by mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tools.CertegyCheckRebateParser",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_CT_CHECK_AUTH);
   __sJT_st.setString(2,stationId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.tools.CertegyCheckRebateParser",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:544^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        merchantId = resultSet.getLong("merchant_number");
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      merchantId = 0L;
    }
    finally
    {
      try{it.close();}catch(Exception e){}
    }
    return( merchantId );
  }
  
  public boolean submitData( HttpServletRequest request )
  {
    ServletFileUpload  fileUpload      = null;
    boolean     retVal          = false;
    String      workFilename    = null;
    
    try
    {
      fileUpload = new ServletFileUpload(new DiskFileItemFactory(1048576, new File("./")));
      ArrayList items = (ArrayList)(fileUpload.parseRequest(request));
      
      for( int i = 0; i < items.size(); ++i )
      {
        DiskFileItem fi = (DiskFileItem)items.get(i);
        
        if( fi.getFieldName().equals("ccrFile") )
        {
          // process the data file to generate a work file
          workFilename = processUploadFile( fi.getString() );
          
          // load the work file into the database
          loadCertegyCheckRebateFile( workFilename );
          
          archiveFile( workFilename );
        }
      }
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
    }
    return( retVal );
  }
  
  protected long toLong( String value )
  {
    StringBuffer    newValue    = new StringBuffer();
    long            retVal      = 0L;
    
    try
    {
      // take order numbers (often *94100000000)
      // strip any non-numeric values and convert
      // new value to a long. 
      for( int i = 0; i < value.length(); ++i )
      {
        switch( value.charAt(i) )
        {
          case '0':
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8':
          case '9':
            newValue.append(value.charAt(i));
            break;
        }
      }
      if ( newValue.length() > 0 )
      {
        retVal = Long.parseLong(newValue.toString()); 
      }        
    }
    catch(Exception e)
    {
      logEntry( "toLong("+value+")",e.toString() );
    }
    return( retVal );
  }

  public static void main( String[] args )
  {
   CertegyCheckRebateParser       c     = null;
    
    try
    {
      c = new CertegyCheckRebateParser();
      c.connect();
      
//@      c.loadCertegyCheckRebateFile(args[0]);
      BufferedReader    in = new BufferedReader( new FileReader(args[0]) );
      StringBuffer fileContent = new StringBuffer();
      String line;
      
      while( (line = in.readLine()) != null )
      {
        fileContent.append(line);
        fileContent.append("\n");
      }
      in.close();
      String workFilename = c.processUploadFile(fileContent.toString());
      c.loadCertegyCheckRebateFile( workFilename );
    }
    catch(Exception e)
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ c.cleanUp(); } catch(Exception e){}
    }
  }  
}/*@lineinfo:generated-code*/