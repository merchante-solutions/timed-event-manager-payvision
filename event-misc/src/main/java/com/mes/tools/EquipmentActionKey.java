/*@lineinfo:filename=EquipmentActionKey*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.tools;

import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;


/**
 * EquipmentActionKey
 * 
 * Key for mesConstants.ACTION_CODE_{...} constants.
 */
public final class EquipmentActionKey extends SQLJConnectionBase implements Key
{
  // constants
  private static final String[] SUBKEY_NAMES = 
  {
     "EquipmentKey"
  };
  
  private static final String[] ITEM_NAMES = 
  {
     "actionCode"
     ,"id"
  };
  
  
  // data members
  private int                       actionCode       = mesConstants.ACTION_CODE_UNDEFINED;
  private String                    actionDesc       = "";  // non key item
  private long                      id               = 0L;
  private EquipmentKey              equipKey         = null;


  // construction
  public  EquipmentActionKey()
  {
  }
  public  EquipmentActionKey(int actionCode, String partNum, String serialNum)
  {
    this.equipKey = new EquipmentKey(partNum,serialNum);
    this.actionCode = actionCode;
  }
  public  EquipmentActionKey(int actionCode, EquipmentKey key)
  {
    this.equipKey = key;
    this.actionCode = actionCode;
  }
  public  EquipmentActionKey(int actionCode, long id)
  {
    this.id=id;
    this.actionCode = actionCode;
  }
  
  public boolean isSet()
  {
    switch(actionCode) {
      case mesConstants.ACTION_CODE_DEPLOYMENT_SWAP:
      case mesConstants.ACTION_CODE_SWAP:
      case mesConstants.ACTION_CODE_REPAIR:
      case mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE:
      case mesConstants.ACTION_CODE_PINPAD_EXCHANGE:
      case mesConstants.ACTION_CODE_CALLTAG:
      case mesConstants.ACTION_CODE_ACR:
        return (id>0L);
      case mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP:
      case mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL:
        return (equipKey!=null && equipKey.isSet());
      default:
        return false;
    }
  }
    
  public boolean exists() throws Key.NonExistantKeyException
  {
    if(!isSet())
      throw new Key.NonExistantKeyException();

    int cnt = 0;

    try {
      connect();
      switch(actionCode) {
        case mesConstants.ACTION_CODE_DEPLOYMENT_SWAP:
        case mesConstants.ACTION_CODE_SWAP:
          /*@lineinfo:generated-code*//*@lineinfo:88^11*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//              
//              from   equip_swap
//              where  swap_ref_num = :this.id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n             \n            from   equip_swap\n            where  swap_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.EquipmentActionKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,this.id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^11*/
          break;
        case mesConstants.ACTION_CODE_REPAIR:
          /*@lineinfo:generated-code*//*@lineinfo:97^11*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//              
//              from   equip_repair
//              where  repair_ref_num = :this.id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n             \n            from   equip_repair\n            where  repair_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.EquipmentActionKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,this.id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:103^11*/
          break;
        case mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP:
        case mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL:
          /*@lineinfo:generated-code*//*@lineinfo:107^11*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//              
//              from   equip_inventory
//              where  ei_part_number = :this.equipKey.getEquipInvKey().partNum
//                     and ei_serial_number = :this.equipKey.getEquipInvKey().serialNum
//                     and (ei_lrb > 0 or ei_deployed_date is not null)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n             \n            from   equip_inventory\n            where  ei_part_number =  :1 \n                   and ei_serial_number =  :2 \n                   and (ei_lrb > 0 or ei_deployed_date is not null)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.EquipmentActionKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,this.equipKey.getEquipInvKey().partNum);
   __sJT_st.setString(2,this.equipKey.getEquipInvKey().serialNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:115^11*/
          break;
        case mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE:
        case mesConstants.ACTION_CODE_PINPAD_EXCHANGE:
          /*@lineinfo:generated-code*//*@lineinfo:119^11*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//              
//              from   equip_exchange
//              where  exchange_ref_num = :this.id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n             \n            from   equip_exchange\n            where  exchange_ref_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tools.EquipmentActionKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,this.id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^11*/
          break;
        case mesConstants.ACTION_CODE_CALLTAG:
          /*@lineinfo:generated-code*//*@lineinfo:128^11*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//              
//              from   equip_calltag
//              where  ct_seq_num = :this.id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n             \n            from   equip_calltag\n            where  ct_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tools.EquipmentActionKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,this.id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:134^11*/
          break;
        case mesConstants.ACTION_CODE_ACR:
          /*@lineinfo:generated-code*//*@lineinfo:137^11*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//              
//              from   acr
//              where  acr_seq_num = :this.id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n             \n            from   acr\n            where  acr_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tools.EquipmentActionKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,this.id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^11*/
          break;
        default:
          return false;
      }
    }
    catch(Exception e) {
      logEntry("exists()",e.toString());
    }
    finally {
      cleanUp();
    }
    
    return (cnt>0);
  }

  // accessors
  public int getActionCode()
  {
    return actionCode;
  }

  public String getActionDesc()
  {
    if(!isSet())
      return "";

    if(actionDesc.length()<1) {

      try {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:174^9*/

//  ************************************************************
//  #sql [Ctx] { select  description
//            
//            from    equip_action_type
//            where   code = :actionCode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  description\n           \n          from    equip_action_type\n          where   code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.tools.EquipmentActionKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,actionCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   actionDesc = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^9*/
      }
      catch(Exception e) {
        logEntry("getActionDesc()",e.toString());
      }
      finally {
        cleanUp();
      }

    }

    return actionDesc;
  }
  
  public long getId()
  {
    return id;
  }

  public String getPartNum()
  {
    return equipKey==null? "":equipKey.getPartNum();
  }

  public String getSerialNum()
  {
    return equipKey==null? "":equipKey.getSerialNum();
  }

  // mutators
  // (NONE)
    
  public boolean equals(Object obj)
  {
    if(!(obj instanceof EquipmentActionKey) || !isSet())
      return false;
    
    EquipmentActionKey key = (EquipmentActionKey)obj;
    
    if(!key.isSet() || key.actionCode!=this.actionCode)
      return false;
    
    switch(actionCode) {
      case mesConstants.ACTION_CODE_DEPLOYMENT_SWAP:
      case mesConstants.ACTION_CODE_SWAP:
      case mesConstants.ACTION_CODE_REPAIR:
      case mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE:
      case mesConstants.ACTION_CODE_PINPAD_EXCHANGE:
      case mesConstants.ACTION_CODE_CALLTAG:
      case mesConstants.ACTION_CODE_ACR:
        return (key.id==this.id);
      case mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP:
      case mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL:
        return key.equipKey.equals(this.equipKey);
      default:
        return false;
    }

  }
    
  public void clear()
  {
    actionCode    = mesConstants.ACTION_CODE_UNDEFINED;
    equipKey  = null;
  }

  public String toString()
  {
    if(!isSet())
      return "Equipment Action key not set.";
    
    StringBuffer sb = new StringBuffer();
    
    switch(actionCode) {
      case mesConstants.ACTION_CODE_DEPLOYMENT_SWAP:
      case mesConstants.ACTION_CODE_SWAP:
      case mesConstants.ACTION_CODE_REPAIR:
      case mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE:
      case mesConstants.ACTION_CODE_PINPAD_EXCHANGE:
      case mesConstants.ACTION_CODE_CALLTAG:
      case mesConstants.ACTION_CODE_ACR:
        //sb.append("Action Code: ");
        //sb.append(actionCode);
        sb.append("Id: ");
        sb.append(id);
        break;
      case mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP:
      case mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL:
        sb.append(equipKey.toString());
        break;
    }

    return sb.toString();
  }
  
  public String toQueryString()
  {
      if(!isSet())
        return "";
      
    StringBuffer sb = new StringBuffer();
    
    switch(actionCode) {
      case mesConstants.ACTION_CODE_DEPLOYMENT_SWAP:
      case mesConstants.ACTION_CODE_SWAP:
      case mesConstants.ACTION_CODE_REPAIR:
      case mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE:
      case mesConstants.ACTION_CODE_PINPAD_EXCHANGE:
      case mesConstants.ACTION_CODE_CALLTAG:
      case mesConstants.ACTION_CODE_ACR:
        sb.append("actionCode=");
        sb.append(actionCode);
        sb.append("&id=");
        sb.append(id);
        break;
      case mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP:
      case mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL:
        sb.append(equipKey.toQueryString());
        break;
    }
    
    return sb.toString();
  }
  
  public boolean build(HttpServletRequest request)
  {
    clear();

    this.actionCode = HttpHelper.getInt(request,ITEM_NAMES[0]);
    
    switch(actionCode) {
      
      case mesConstants.ACTION_CODE_DEPLOYMENT_SWAP:
      case mesConstants.ACTION_CODE_SWAP:
      case mesConstants.ACTION_CODE_REPAIR:
      case mesConstants.ACTION_CODE_DEPLOYMENT_PINPAD_EXCHANGE:
      case mesConstants.ACTION_CODE_PINPAD_EXCHANGE:
      case mesConstants.ACTION_CODE_CALLTAG:
      case mesConstants.ACTION_CODE_ACR:
        this.id = HttpHelper.getLong(request,ITEM_NAMES[1],0L);
        break;
      
      // deployments
      case mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP:
      case mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL:
        this.equipKey = new EquipmentKey();
        this.equipKey.build(request);
        break;
    }

    return isSet();
  }
  
  public boolean                    hasSubKeys()
  {
    return true;
  }
  public String[]                   getSubKeyNames()
  {
    return SUBKEY_NAMES;
  }
  public Key                        getSubKey(String keyName) 
    throws KeyException
  {
    if(keyName==null)
      return null;
    
    if(keyName.equals(SUBKEY_NAMES[0])) {
      if(equipKey==null)
        throw new NonExistantKeyException("Sub-key '"+SUBKEY_NAMES[0]+"' does not exist for this Equipment Action key.");
      return equipKey;
    }
    
    throw new UnrecognizedKeyNameException("Unrecognized Sub-key name '"+keyName+"'.");
  }
  
  public boolean                    hasItems()
  {
    return true;
  }
  public String[]                   getItemNames()
  {
    return ITEM_NAMES;
  }
  public String                     getItemStringValue(String itemName) 
    throws KeyException
  {
    if(itemName == null || itemName.length()<1)
      return "";

    if(itemName.equals(ITEM_NAMES[0]))
      return Integer.toString(actionCode);

    if(itemName.equals(ITEM_NAMES[1]))
      return Long.toString(id);
    
    throw new Key.NonExistantKeyException("Item of name '"+itemName+"' does not exist..");
  }
  
}/*@lineinfo:generated-code*/