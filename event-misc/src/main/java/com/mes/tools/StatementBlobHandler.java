/*@lineinfo:filename=StatementBlobHandler*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/BlobHandler.sqlj $

  Description:  BlobHandler.sqlj

  Designed to enable the saving and accessing of Blob-based documents in
  the database. Uses tables DOCUMENT and DOCUMENT_TYPE.


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.io.InputStream;
// log4j
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import oracle.sql.BLOB;

public class StatementBlobHandler extends SQLJConnectionBase
{

  // create class log category
  static Logger log = Logger.getLogger(BlobHandler.class);

  private String merchNum = "";
  private long yearMonth  = 0;
  private long recNum     = 0;

  public StatementBlobHandler(String merchNum, long yearMonth)
  {
    this.merchNum  = merchNum;
    this.yearMonth = yearMonth;
  }


  public StatementBlobHandler(String merchNum, long yearMonth, long recNum)
  {
    this.merchNum   = merchNum;
    this.yearMonth  = yearMonth;
    this.recNum     = recNum;
  }

  public byte[] getDataBody()
  {
    byte[] fileBody = null;

    try
    {
      connect();

      BLOB b;

      /*@lineinfo:generated-code*//*@lineinfo:76^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT
//            statement_data
//          
//          FROM
//            merch_statements
//          WHERE
//            merch_num = :merchNum and
//            year_month = :yearMonth and
//            (
//              0 = :recNum or
//              rec_num = :recNum
//            )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT\n          statement_data\n         \n        FROM\n          merch_statements\n        WHERE\n          merch_num =  :1  and\n          year_month =  :2  and\n          (\n            0 =  :3  or\n            rec_num =  :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.StatementBlobHandler",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setLong(2,yearMonth);
   __sJT_st.setLong(3,recNum);
   __sJT_st.setLong(4,recNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:91^7*/

      // use the blob ref to read the blob data into stream
      int dataLen = (int)b.length();
      int chunkSize = 0;
      int dataRead = 0;
      InputStream is = b.getBinaryStream();
      fileBody = new byte[dataLen];
      while (chunkSize != -1)
      {
        chunkSize = is.read(fileBody,dataRead,dataLen - dataRead);
        dataRead += chunkSize;
      }
      is.close();
    }
    catch (Exception e1)
    {
      log.debug("getDataBody() exception = "+e1.getMessage());
      fileBody = null;
    }
    finally
    {
      cleanUp();
    }

    return fileBody;
  }

}/*@lineinfo:generated-code*/