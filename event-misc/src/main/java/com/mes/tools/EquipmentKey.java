/*@lineinfo:filename=EquipmentKey*//*@lineinfo:user-code*//*@lineinfo:1^1*/
package com.mes.tools;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;


/**
 * EquipmentKey
 * 
 * Wrapper around multiple equipment primary keys.
 * Used to establish a single point of reference to identify an equipment item entity.
 */
public final class EquipmentKey extends SQLJConnectionBase implements Key
{
  // create class log category
	static Logger log = Logger.getLogger(EquipmentKey.class);

  
  // constants
  private static final String[] SUBKEY_NAMES = 
  {
     "MerchEquipKey"
    ,"EquipInvKey"
    ,"TermAppProfileSummaryKey"
    ,"TidRequestKey"
  };
  
  
  // data members
  private int                       equipType        = mesConstants.EQUIPTYPE_UNDEFINED;
  private MerchEquipKey             merchEquipKey    = null;
  private EquipInvKey               equipInvKey      = null;
  private TermAppProfileSummaryKey  tapsKey          = null;
  private TidRequestKey             tidrKey          = null;


  /**
   * EquipInvKey
   */
  public class MerchEquipKey implements Key
  {
    // constants
    private final String[] ITEM_NAMES = 
    {
       "appSeqNum"
      ,"equipModel"
      ,"lendTypeCode"
    };
    
    // data members
    public long      appSeqNum             = 0L;
    public String    equipModel            = "";
    public int       lendTypeCode          = -1;

    public boolean isSet()
    {
      return (appSeqNum>0L || (equipModel!=null && equipModel.length()>0) || lendTypeCode>-1);
    }
    
    public boolean exists() throws Key.NonExistantKeyException
    {
      if(!isSet())
        throw new NonExistantKeyException();
      
      int cnt = 0;
      
      try {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:76^9*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//            
//            from   merchequipment
//            where  app_seq_num = :appSeqNum and equip_model = :equipModel and equiplendtype_code = :lendTypeCode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n           \n          from   merchequipment\n          where  app_seq_num =  :1  and equip_model =  :2  and equiplendtype_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.EquipmentKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,equipModel);
   __sJT_st.setInt(3,lendTypeCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:82^9*/
      }
      catch(Exception e) {
        logEntry("exists(merchequipment)",e.toString());
      }
      finally {
        cleanUp();
      }
      
      return (cnt>0);
    }
    
    public boolean   equals(Object obj)
    {
      if(!(obj instanceof MerchEquipKey) || !isSet())
        return false;
      
      MerchEquipKey key = (MerchEquipKey)obj;

      return key.appSeqNum==appSeqNum && (key.equipModel!=null && key.equipModel.equals(equipModel)) && key.lendTypeCode==lendTypeCode;
    }
    
    public void     clear()
    {
      appSeqNum             = 0L;
      equipModel            = "";
      lendTypeCode          = -1;
    }
    
    public String toString()
    {
      if(!isSet())
        return "Merch Equipment key not set.";
      
      StringBuffer sb = new StringBuffer();
      
      sb.append("App Seq Num: ");
      sb.append(appSeqNum);
      sb.append(", Equip Model: ");
      sb.append(equipModel);
      sb.append(", Lend Type Code: ");
      sb.append(lendTypeCode);
      
      return sb.toString();
    }
    
    public String toQueryString()
		{
			if (!isSet())
				return "";

			StringBuffer sb = new StringBuffer();
			sb.append(ITEM_NAMES[0]);
			sb.append('=');
			sb.append(appSeqNum);
			sb.append('&');
			sb.append(ITEM_NAMES[1]);
			sb.append('=');
			try {
				sb.append(URLEncoder.encode(equipModel, StandardCharsets.UTF_8.name()));
			} catch (UnsupportedEncodingException e) {
				log.error("URLEncoder.encode(equipModel UnsupportedEncodingException", e);
				sb.append(equipModel);
			}
			sb.append('&');
			sb.append(ITEM_NAMES[2]);
			sb.append('=');
			sb.append(lendTypeCode);
			return sb.toString();
		}
    
    public boolean build(HttpServletRequest request)
    {
      clear();
      this.appSeqNum    = HttpHelper.getLong(request,ITEM_NAMES[0],0L);
      this.equipModel   = HttpHelper.getString(request,ITEM_NAMES[1],"");
      this.lendTypeCode = HttpHelper.getInt(request,ITEM_NAMES[2],-1);
      return isSet();
    }
  
    public boolean                    hasSubKeys()
    {
      return false;
    }
    public String[]                   getSubKeyNames()
    {
      return null;
    }
    public Key                        getSubKey(String keyName) 
      throws KeyException
    {
      throw new NonExistantKeyException("No sub-keys exist for EquipmentKey.MerchEquipKey.");
    }
  
    public boolean                    hasItems()
    {
      return true;
    }
    public String[]                   getItemNames()
    {
      return ITEM_NAMES;
    }
    public String                     getItemStringValue(String itemName)
      throws KeyException
    {
      if(itemName==null)
        return null;

      if(itemName.equals(ITEM_NAMES[0]))
        return Long.toString(appSeqNum);

      if(itemName.equals(ITEM_NAMES[1]))
        return equipModel;

      if(itemName.equals(ITEM_NAMES[2]))
        return Integer.toString(lendTypeCode);

      throw new KeyException("Unrecognized key item name: '"+itemName+"'.");
    }

  }
    
  
  /**
   * EquipInvKey
   */
  public class EquipInvKey implements Key
  {
    // constants
    private final String[] ITEM_NAMES = 
    {
       "partNum"
      ,"serialNum"
    };
    
    // data members
    public String   partNum       = "";
    public String   serialNum     = "";

    // construction
    public EquipInvKey()
    {
    }
    public EquipInvKey(String pn, String sn)
    {
      this.partNum    = pn;
      this.serialNum  = sn;
      
      isSet();  
        // to force the auto-determining of the part num if only serial num specified
    }

    public String getPartNum()   { return partNum; }
    public String getSerialNum() { return serialNum; }

    public boolean isSet()
    {
      // attempt to pigeon hole the part number if only serial number specified
      //  since there are very few redundant serial numbers
      if(serialNum!=null && serialNum.length() > 0 && (partNum == null || partNum.length() < 1)) {
        //log.debug("EquipInvKey.isSet() - attempting to ascertain part number from serial number...");
        try {
          connect();
          /*@lineinfo:generated-code*//*@lineinfo:240^11*/

//  ************************************************************
//  #sql [Ctx] { select  ei_part_number
//              
//              from    equip_inventory
//              where   ei_serial_number = :serialNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ei_part_number\n             \n            from    equip_inventory\n            where   ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.EquipmentKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,serialNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   partNum = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:246^11*/
        }
        catch(Exception e) {
          
        }
        finally {
          cleanUp();
        }
      }
      
      //log.debug("EquipInvKey.isSet(): pn: "+partNum+", sn: "+serialNum);

      return ((partNum!=null && partNum.length()>0) && (serialNum!=null && serialNum.length()>0));
    }
    
    public boolean exists() throws Key.NonExistantKeyException
    {
      if(!isSet())
        throw new NonExistantKeyException();
      
      int cnt = 0;
      
      try {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:270^9*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//            
//            from   equip_inventory
//            where  ei_part_number = :partNum and ei_serial_number = :serialNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n           \n          from   equip_inventory\n          where  ei_part_number =  :1  and ei_serial_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.EquipmentKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,serialNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:276^9*/
      }
      catch(Exception e) {
        logEntry("exists(equip_inventory)",e.toString());
      }
      finally {
        cleanUp();
      }

      return (cnt==1);
    }
    
    public boolean   equals(Object obj)
    {
      if(!(obj instanceof EquipInvKey) || !isSet())
        return false;
      
      EquipInvKey key = (EquipInvKey)obj;

      return (key.partNum!=null && key.partNum.equals(partNum)) && (key.serialNum!=null && key.serialNum.equals(serialNum));
    }
    
    public void     clear()
    {
      partNum     = "";
      serialNum   = "";
    }
    
    public String toString()
    {
      if(!isSet())
        return "Equipment Inventory key not set.";
      
      StringBuffer sb = new StringBuffer();
      sb.append("Model ");
      sb.append(partNum);
      sb.append(", S/N ");
      sb.append(serialNum);
      return sb.toString();
    }

    public String toQueryString()
    {
      if(!isSet())
        return "";
      
      StringBuffer sb = new StringBuffer();
      sb.append(ITEM_NAMES[0]);
      sb.append('=');
      try {
		sb.append(URLEncoder.encode(partNum, StandardCharsets.UTF_8.name()));
	} catch (UnsupportedEncodingException e) {
		log.error("URLEncoder.encode(partNum UnsupportedEncodingException", e);
		sb.append(partNum);
	}
      sb.append('&');
      sb.append(ITEM_NAMES[1]);
      sb.append('=');
      try {
		sb.append(URLEncoder.encode(serialNum, StandardCharsets.UTF_8.name()));
	} catch (UnsupportedEncodingException e) {
		log.error("URLEncoder.encode(serialNum UnsupportedEncodingException", e);
		sb.append(serialNum);
	}
      return sb.toString();
    }
    
    public boolean build(HttpServletRequest request)
    {
      clear();
      this.partNum    = HttpHelper.getString(request,ITEM_NAMES[0],"");
      this.serialNum  = HttpHelper.getString(request,ITEM_NAMES[1],"");
      return isSet();
    }
  
    public boolean                    hasSubKeys()
    {
      return false;
    }
    public String[]                   getSubKeyNames()
    {
      return null;
    }
    public Key                        getSubKey(String keyName) 
      throws KeyException
    {
      throw new NonExistantKeyException("No sub-keys exist for EquipmentKey.EquipInvKey.");
    }
  
    public boolean                    hasItems()
    {
      return true;
    }
    public String[]                   getItemNames()
    {
      return ITEM_NAMES;
    }
    public String                     getItemStringValue(String itemName)
      throws KeyException
    {
      if(itemName==null)
        return null;

      if(itemName.equals(ITEM_NAMES[0]))
        return partNum;

      if(itemName.equals(ITEM_NAMES[1]))
        return serialNum;

      throw new KeyException("Unrecognized key item name: '"+itemName+"'.");
    }

  }
  
  
  /**
   * TermAppProfileSummaryKey
   */
  public class TermAppProfileSummaryKey implements Key
  {
    // constants
    private final String[] ITEM_NAMES = 
    {
       "vnum"
    };
    
    // data members
    public String   vnum       = "";

    public boolean isSet()
    {
      return (vnum!=null && vnum.length()>0);
    }
    
    public boolean exists() throws Key.NonExistantKeyException
    {
      if(!isSet())
        throw new NonExistantKeyException();
      
      int cnt = 0;
      
      try {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:409^9*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//            
//            from   merch_vnumber
//            where  vnumber = :vnum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n           \n          from   merch_vnumber\n          where  vnumber =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tools.EquipmentKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,vnum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:415^9*/
      }
      catch(Exception e) {
        logEntry("exists(merch_vnumber)",e.toString());
      }
      finally {
        cleanUp();
      }
      
      return (cnt>0);
    }
    
    public boolean   equals(Object obj)
    {
      if(!(obj instanceof TermAppProfileSummaryKey) || !isSet())
        return false;
      
      TermAppProfileSummaryKey key = (TermAppProfileSummaryKey)obj;

      return (key.vnum!=null && key.vnum.equals(vnum));
    }
    
    public void     clear()
    {
      vnum     = "";
    }
    
    public String toString()
    {
      if(!isSet())
        return "Terminal Application Profile Summary key not set.";
      
      StringBuffer sb = new StringBuffer();
      sb.append("VNumber: ");
      sb.append(vnum);
      return sb.toString();
    }

    public String toQueryString()
		{
			if (!isSet())
				return "";

			StringBuffer sb = new StringBuffer();
			sb.append(ITEM_NAMES[0]);
			sb.append('=');
			try {
				sb.append(URLEncoder.encode(vnum, StandardCharsets.UTF_8.name()));
			} catch (UnsupportedEncodingException e) {
				log.error("URLEncoder.encode(vnum UnsupportedEncodingException", e);
				sb.append(vnum);
			}
			return sb.toString();
		}
    
    public boolean build(HttpServletRequest request)
    {
      clear();
      this.vnum    = HttpHelper.getString(request,ITEM_NAMES[0],"");
      return isSet();
    }

    public boolean                    hasSubKeys()
    {
      return false;
    }
    public String[]                   getSubKeyNames()
    {
      return null;
    }
    public Key                        getSubKey(String keyName) 
      throws KeyException
    {
      throw new NonExistantKeyException("No sub-keys exist for EquipmentKey.TermAppProfileSummaryKey.");
    }
  
    public boolean                    hasItems()
    {
      return true;
    }
    public String[]                   getItemNames()
    {
      return ITEM_NAMES;
    }
    public String                     getItemStringValue(String itemName)
      throws KeyException
    {
      if(itemName==null)
        return null;

      if(itemName.equals(ITEM_NAMES[0]))
        return vnum;
      
      throw new KeyException("Unrecognized key item name: '"+itemName+"'.");
    }

  }

  
  /**
   * TidRequestKey
   */
  public class TidRequestKey implements Key
  {
    // constants
    private final String[] ITEM_NAMES = 
    {
        "requestId"
       ,"appSeqNum"
    };
    
    // data members
    private long     requestId       = 0L;
    private long     appSeqNum       = 0L;

    public long getRequestId()
      throws KeyException
    {
      if(!isSet())
        return 0L;

      if(requestId == 0L) {
        try {
          connect();
          /*@lineinfo:generated-code*//*@lineinfo:534^11*/

//  ************************************************************
//  #sql [Ctx] { select request_id
//              
//              from   mms_stage_info
//              where  app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select request_id\n             \n            from   mms_stage_info\n            where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tools.EquipmentKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   requestId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:540^11*/
        }
        catch(SQLException sqle) {
          if(sqle.getMessage().indexOf("multiple rows found for select into statement")>0)
            throw new Key.AmbiguousKeyException("Multiple records found for EquipmentKey.TidRequestKey: "+toString());
          else
            throw new Key.NonExistantKeyException("Request Id non-existant for EquipmentKey.TidRequestKey: "+toString());
        }
        catch(Exception e) {
          logEntry("getRequestId() EXCEPTION: ",e.toString());
        }
        finally {
          cleanUp();
        }
      }

      return requestId;
    }

    public long getAppSeqNum()
      throws KeyException
    {
      if(!isSet())
        return 0L;

      if(appSeqNum == 0L) {
        try {
          connect();
          /*@lineinfo:generated-code*//*@lineinfo:568^11*/

//  ************************************************************
//  #sql [Ctx] { select app_seq_num
//              
//              from   mms_stage_info
//              where  request_id = :requestId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select app_seq_num\n             \n            from   mms_stage_info\n            where  request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tools.EquipmentKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,requestId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:574^11*/
        }
        catch(SQLException sqle) {
          if(sqle.getMessage().indexOf("multiple rows found for select into statement")>0)
            throw new Key.AmbiguousKeyException("Multiple records found for EquipmentKey.TidRequestKey: "+toString());
          else
            throw new Key.NonExistantKeyException("App Seq Num non-existant for EquipmentKey.TidRequestKey: "+toString());
        }
        catch(Exception e) {
          logEntry("getRequestId() EXCEPTION: ",e.toString());
        }
        finally {
          cleanUp();
        }
      }

      return appSeqNum;
    }

    public boolean isSet()
    {
      return (requestId > 0L || appSeqNum > 0L);
    }
    
    public boolean exists() throws Key.NonExistantKeyException
    {
      if(!isSet())
        throw new NonExistantKeyException();
      
      int cnt = 0;
      
      try {
        connect();
        
        if(requestId > 0L) {

          /*@lineinfo:generated-code*//*@lineinfo:610^11*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//              
//              from   mms_stage_info
//              where  request_id = :requestId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n             \n            from   mms_stage_info\n            where  request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.tools.EquipmentKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,requestId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:616^11*/
        
        } else if(appSeqNum > 0L) {

          /*@lineinfo:generated-code*//*@lineinfo:620^11*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//              
//              from   mms_stage_info
//              where  app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n             \n            from   mms_stage_info\n            where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.tools.EquipmentKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:626^11*/

        }
      }
      catch(Exception e) {
        logEntry("exists(TidRequestKey)",e.toString());
      }
      finally {
        cleanUp();
      }
      
      return (cnt>0);
    }
    
    public boolean   equals(Object obj)
    {
      if(!(obj instanceof TidRequestKey) || !isSet())
        return false;
      
      TidRequestKey key = (TidRequestKey)obj;

      return (key.requestId == requestId || key.appSeqNum == appSeqNum);
    }
    
    public void     clear()
    {
      requestId     = 0L;
      appSeqNum     = 0L;
    }
    
    public String toString()
    {
      if(!isSet())
        return "TID Request key not set.";
      
      StringBuffer sb = new StringBuffer();
      
      sb.append("Request ID: ");
      sb.append(requestId);
      sb.append(", App Seq Num: ");
      sb.append(appSeqNum);
      
      return sb.toString();
    }

    public String toQueryString()
    {
      if(!isSet())
        return "";
      
      StringBuffer sb = new StringBuffer();
      
      if(requestId > 0L) {
        sb.append(ITEM_NAMES[0]);
        sb.append('=');
        sb.append(requestId);
      }
      
      if(appSeqNum > 0L) {
        sb.append(ITEM_NAMES[1]);
        sb.append('=');
        sb.append(appSeqNum);
      }
      
      return sb.toString();
    }
    
    public boolean build(HttpServletRequest request)
    {
      clear();
      
      this.requestId    = HttpHelper.getLong(request,ITEM_NAMES[0],0L);
      this.appSeqNum    = HttpHelper.getLong(request,ITEM_NAMES[1],0L);
      
      return isSet();
    }

    public boolean                    hasSubKeys()
    {
      return false;
    }
    public String[]                   getSubKeyNames()
    {
      return null;
    }
    public Key                        getSubKey(String keyName) 
      throws KeyException
    {
      throw new NonExistantKeyException("No sub-keys exist for EquipmentKey.TidRequestKey.");
    }
  
    public boolean                    hasItems()
    {
      return true;
    }
    public String[]                   getItemNames()
    {
      return ITEM_NAMES;
    }
    public String                     getItemStringValue(String itemName)
      throws KeyException
    {
      if(itemName==null)
        return null;

      if(itemName.equals(ITEM_NAMES[0]))
        return Long.toString(requestId);
      
      if(itemName.equals(ITEM_NAMES[1]))
        return Long.toString(appSeqNum);
      
      throw new KeyException("Unrecognized key item name: '"+itemName+"'.");
    }

  }


  // construction
  public  EquipmentKey()
  {
  }
  public  EquipmentKey(long appSeqNum, String equipModel, int lendTypeCode)
  {
    merchEquipKey = new MerchEquipKey();
    merchEquipKey.appSeqNum = appSeqNum;
    merchEquipKey.equipModel = equipModel;
    merchEquipKey.lendTypeCode = lendTypeCode;
    equipType = mesConstants.EQUIPTYPE_MERCHEQUIP;
  }
  public  EquipmentKey(String partNum, String serialNum)
  {
    equipInvKey = new EquipInvKey(partNum, serialNum);
    equipType = mesConstants.EQUIPTYPE_EQUIPINV;
  }
  public  EquipmentKey(String vnum)
  {
    tapsKey = new TermAppProfileSummaryKey();
    tapsKey.vnum = vnum;
    equipType = mesConstants.EQUIPTYPE_TERMAPP;
  }
  public  EquipmentKey(long requestId)
  {
    tidrKey = new TidRequestKey();
    tidrKey.requestId = requestId;
    equipType = mesConstants.EQUIPTYPE_TIDREQUEST;
  }
  
  public boolean isSet()
  {
    return ((merchEquipKey!=null && merchEquipKey.isSet()) || (equipInvKey!=null && equipInvKey.isSet()) || (tapsKey!=null && tapsKey.isSet()) || (tidrKey!=null && tidrKey.isSet()));
  }
    
  public boolean exists() throws Key.NonExistantKeyException
  {
    if(!isSet())
      throw new Key.NonExistantKeyException();

    switch(equipType) {
      case mesConstants.EQUIPTYPE_MERCHEQUIP:
        return merchEquipKey.exists();
      case mesConstants.EQUIPTYPE_EQUIPINV:
        return equipInvKey.exists();
      case mesConstants.EQUIPTYPE_TERMAPP:
        return tapsKey.exists();
      case mesConstants.EQUIPTYPE_TIDREQUEST:
        return tidrKey.exists();
      default:
        return false;
    }
  }

  // accessors
  public int getEquipType()
  {
    return equipType;
  }
  
  public MerchEquipKey getMerchEquipKey()
    throws KeyException
  {
    if(merchEquipKey == null)
      throw new NonExistantKeyException("Equipment key of type Merchant Equipment non-existant.");
      
    return merchEquipKey;
  }
  
  public EquipInvKey getEquipInvKey()
    throws KeyException
  {
    if(equipInvKey == null)
      throw new NonExistantKeyException("Equipment key of type MeS Equipment Inventory non-existant.");
    
    return equipInvKey;
  }
  
  public TermAppProfileSummaryKey getTermAppProfileSummaryKey()
    throws KeyException
  {
    if(tapsKey == null)
      throw new NonExistantKeyException("Equipment key of type Term App Profile Summary non-existant.");
    
    return tapsKey;
  }

  public TidRequestKey getTidRequestKey()
    throws KeyException
  {
    if(tidrKey == null)
      throw new NonExistantKeyException("Equipment key of type TID Request non-existant.");
    
    return tidrKey;
  }

  // convenience accessors

  public String getPartNum()
  {
    return equipInvKey==null? "":equipInvKey.partNum;
  }
  
  public String getSerialNum()
  {
    return equipInvKey==null? "":equipInvKey.serialNum;
  }

  public long getAppSeqNum()
  {
    return merchEquipKey==null? 0L:merchEquipKey.appSeqNum;
  }

  public String getEquipModel()
  {
    return merchEquipKey==null? "":merchEquipKey.equipModel;
  }

  public int getLendTypeCode()
  {
    return merchEquipKey==null? -1:merchEquipKey.lendTypeCode;
  }

  public String getVnum()
  {
    return tapsKey==null? "":tapsKey.vnum;
  }

  // mutators
  // (NONE)
    
  public boolean equals(Object obj)
  {
    if(!(obj instanceof EquipmentKey) || !isSet())
      return false;
    
    EquipmentKey key = (EquipmentKey)obj;
    
    if(!key.isSet() || key.getEquipType()!=this.equipType)
      return false;
    
    switch(equipType) {
      case mesConstants.EQUIPTYPE_EQUIPINV:
        return key.equipInvKey.equals(equipInvKey);
      case mesConstants.EQUIPTYPE_MERCHEQUIP:
        return key.merchEquipKey.equals(merchEquipKey);
      case mesConstants.EQUIPTYPE_TERMAPP:
        return key.tapsKey.equals(tapsKey);
      case mesConstants.EQUIPTYPE_TIDREQUEST:
        return key.tidrKey.equals(tapsKey);
    }
    
    return false;
  }
    
  public void clear()
  {
    equipType     = mesConstants.EQUIPTYPE_UNDEFINED;
    merchEquipKey = null;
    equipInvKey   = null;
    tapsKey       = null;
  }

  public String toString()
  {
    if(!isSet())
      return "No Equipment key set.";
    
    StringBuffer sb = new StringBuffer();
    
    switch(equipType) {
      case mesConstants.EQUIPTYPE_EQUIPINV:
        sb.append(equipInvKey.toString());
        break;
      case mesConstants.EQUIPTYPE_MERCHEQUIP:
        sb.append(merchEquipKey.toString());
        break;
      case mesConstants.EQUIPTYPE_TERMAPP:
        sb.append(tapsKey.toString());
        break;
      case mesConstants.EQUIPTYPE_TIDREQUEST:
        sb.append(tidrKey.toString());
        break;
    }
    
    return sb.toString();
  }
  
  public String toQueryString()
  {
      if(!isSet())
        return "";
      
    StringBuffer qs = new StringBuffer();
    
    switch(equipType) {
      case mesConstants.EQUIPTYPE_EQUIPINV:
        qs.append(this.equipInvKey.toQueryString());
        break;
      case mesConstants.EQUIPTYPE_MERCHEQUIP:
        qs.append(this.merchEquipKey.toQueryString());
        break;
      case mesConstants.EQUIPTYPE_TERMAPP:
        qs.append(this.tapsKey.toQueryString());
        break;
      case mesConstants.EQUIPTYPE_TIDREQUEST:
        qs.append(this.tidrKey.toQueryString());
        break;
    }

    qs.append("&equipType=");
    qs.append(this.equipType);
    
    return qs.toString();
  }
  
  public boolean build(HttpServletRequest request)
  {
    clear();
    
    this.equipType = HttpHelper.getInt(request,"equipType",mesConstants.EQUIPTYPE_UNDEFINED);
    
    switch(equipType) {
      case mesConstants.EQUIPTYPE_EQUIPINV:
        EquipInvKey eik = new EquipInvKey();
        eik.build(request);
        this.equipInvKey = eik;
        break;
      case mesConstants.EQUIPTYPE_MERCHEQUIP:
        MerchEquipKey mek = new MerchEquipKey();
        mek.build(request);
        this.merchEquipKey = mek;
        break;
      case mesConstants.EQUIPTYPE_TERMAPP:
        TermAppProfileSummaryKey tap = new TermAppProfileSummaryKey();
        tap.build(request);
        this.tapsKey = tap;
        break;
      case mesConstants.EQUIPTYPE_TIDREQUEST:
        TidRequestKey tidr = new TidRequestKey();
        tidr.build(request);
        this.tidrKey = tidr;
        break;
    }

    return isSet();
  }
  
  public boolean                    hasSubKeys()
  {
    return true;
  }
  public String[]                   getSubKeyNames()
  {
    return SUBKEY_NAMES;
  }
  public Key                        getSubKey(String keyName) 
    throws KeyException
  {
    if(keyName==null)
      return null;
    
    if(keyName.equals(SUBKEY_NAMES[0])) {
      if(merchEquipKey==null)
        throw new NonExistantKeyException("Sub-key '"+SUBKEY_NAMES[0]+"' does not exist for this Equipment key.");
      return merchEquipKey;
    }
    
    if(keyName.equals(SUBKEY_NAMES[1])) {
      if(equipInvKey==null)
        throw new NonExistantKeyException("Sub-key '"+SUBKEY_NAMES[1]+"' does not exist for this Equipment key.");
      return equipInvKey;
    }
    
    if(keyName.equals(SUBKEY_NAMES[2])) {
      if(tapsKey==null)
        throw new NonExistantKeyException("Sub-key '"+SUBKEY_NAMES[2]+"' does not exist for this Equipment key.");
      return tapsKey;
    }
    
    throw new UnrecognizedKeyNameException("Unrecognized Sub-key name '"+keyName+"'.");
  }
  
  public boolean                    hasItems()
  {
    return false;
  }
  public String[]                   getItemNames()
  {
    return null;
  }
  public String                     getItemStringValue(String itemName) 
    throws KeyException
  {
    throw new Key.NonExistantKeyException("EquipmentKey has no items.");
  }
  
}/*@lineinfo:generated-code*/