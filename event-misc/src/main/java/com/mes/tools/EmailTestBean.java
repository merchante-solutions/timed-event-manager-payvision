/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/EmailTestBean.java $

  Description:  
  
    MailMessage

    Uses the Java Mail API to create an email message and send it.
    Supports multi-part messages (allows file attachments) and allows
    multiple recipients (of TO, CC and BCC types) to be specified.
    A set of sender/recipient addresses may be defined in the db table
    EMAIL_ADDRESSES.  These sets may be referenced with the setAddresses()
    call.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/04/02 6:56p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import com.mes.net.MailMessage;

public class EmailTestBean
{
  private MailMessage     mailer          = null;
  private String          host            = "";
  private String          fromAddr        = "";
  private String          toAddr          = "";
  private String          subject         = "";
  private String          body            = "";
  private boolean         submitted       = false;
  
  private boolean         error           = false;
  private StringBuffer    errString       = new StringBuffer("");
  
  public EmailTestBean()
  {
  }
  
  public void sendEmail()
  {
    try
    {
      mailer = new MailMessage(host);
      mailer.setFrom(fromAddr);
      mailer.addTo(toAddr);
      mailer.setSubject(subject);
      mailer.setText(body);
      
      mailer.send(); 
    }
    catch(Exception e)
    {
      error = true;
      errString.append("sendEmail(): " + e.toString());
    }
  }
  
  public void setHost(String host)
  {
    this.host = host;
  }
  public String getHost()
  {
    return this.host;
  }
  public void setFromAddr(String fromAddr)
  {
    this.fromAddr = fromAddr;
  }
  public String getFromAddr()
  {
    return this.fromAddr;
  }
  public void setToAddr(String toAddr)
  {
    this.toAddr = toAddr;
  }
  public String getToAddr()
  {
    return this.toAddr;
  }
  public void setSubject(String subject)
  {
    this.subject = subject;
  }
  public String getSubject()
  {
    return this.subject;
  }  
  public void setBody(String body)
  {
    this.body = body;
  }
  public String getBody()
  {
    return this.body;
  }
  public boolean getError()
  {
    return this.error;
  }
  public String getErrString()
  {
    return this.errString.toString();
  }
  public void setSubmit(String submit)
  {
    this.submitted = true;
  }
  public boolean isSubmitted()
  {
    return this.submitted;
  }
  
}


