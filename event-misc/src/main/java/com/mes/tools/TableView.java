package com.mes.tools;

import java.util.Iterator;
import java.util.List;

public interface TableView
{
  public List           getRows();
  public void           setRows(List list);
  public Iterator       getRowIterator();
  public int            getNumRows();
  public int            getNumCols();
  public Key            getRowKey(int rowOrdinal);
  public Object         getRowObject(int rowOrdinal);
  public String         getLinkToDetailColumnName();
  public String         getDetailPageBaseUrl();
  public String         getQueryStringSuffix(); // used by getDetailPageQueryString()
  public void           setQueryStringSuffix(String v);
  public String         getDetailPageQueryString(int rowOrdinal);
  public String         getColumnName(int colOrdinal);
  public String[]       getColumnNames();
  public String         getCell(int rowOrdinal, int colOrdinal);
  public String         getHtmlCell(int rowOrdinal, int colOrdinal);
}

// NOTE: all ordinal parameters are 1-based counting.
