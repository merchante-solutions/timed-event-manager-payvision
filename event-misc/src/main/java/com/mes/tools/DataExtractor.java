package com.mes.tools;

public interface DataExtractor
{

  public boolean extract();
  public String getDataSource();
  public boolean validateFilename();

}