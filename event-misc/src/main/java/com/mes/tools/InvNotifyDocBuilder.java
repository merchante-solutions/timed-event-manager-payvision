/*@lineinfo:filename=InvNotifyDocBuilder*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  Description:  InvNotifyDocBuilder.sqlj

  Designed to handle dynamic building of Inventory Notification letters using PDF
  NOTE: this class uses the latest version of iText open source PDF generation
  itext-paulo-132.jar and is, therefore, under restrictions as provided by its
  authors, Bruno Lowagie and Paulo Soares. More information can be found at
  www.lowagie.com/iText/ and http://itextpdf.sourceforge.net/. Of course, PDF is
  a format created by Adobe... so all rights covered on that front too.

  //Copyright 2001 by Paulo Soares (iText)

  Copyright (C) 2000-2004 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.io.ByteArrayOutputStream;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.util.Calendar;
// log4j
import org.apache.log4j.Logger;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.TextField;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class InvNotifyDocBuilder extends DocBuilder
{

  // create class log category
  static Logger log = Logger.getLogger(InvNotifyDocBuilder.class);
  
  //details
  private String letter;
  private String dhlTrackNum;
  private long callTagId;
  private int letterType = 1;

  //date
  private String dateString;

  //letter types
  private final int GENERAL             = 1;
  private final int MOE_LOAN_SWAP       = 2;
  private final int MOE_LOAN_SWAP_BACK  = 3;
  private final int PINPAD_ENCRYPT      = 4;
  private final int PINPAD_REJECT       = 5;
  private final int PINPAD_SWAP         = 6;
  private final int RESTRICTED_SWAP     = 7;
  private final int SWAP                = 8;
  
  
  //PDF objects for doc generation
  private Document document = null;
  private PdfWriter writer = null;
  private ByteArrayOutputStream baos = null;

  //Fonts
  public static Font BIG_HEADER = new Font(Font.TIMES_ROMAN ,16f, Font.BOLD);
  public static Font HEADER_1 = new Font(Font.HELVETICA ,14f, Font.BOLD);
  public static Font HEADER_2 = new Font(Font.HELVETICA ,14f, Font.UNDERLINE);
  public static Font BODY_1 = new Font(Font.HELVETICA ,12f, Font.NORMAL);
  public static Font BODY_2 = new Font(Font.HELVETICA ,12f, Font.BOLD);
  public static Font BODY_3 = new Font(Font.HELVETICA ,10f, Font.NORMAL);
  public static Font BODY_4 = new Font(Font.HELVETICA ,12f, Font.ITALIC);
  public static Font FOOTER = new Font(Font.HELVETICA ,8f, Font.NORMAL);
  public static Font FOOTER_BOLD = new Font(Font.HELVETICA ,8f, Font.NORMAL);
  
  //inner class
  private _merchCallTagInfo info;
  
  public InvNotifyDocBuilder(String letter, long callTagId, String dhlTrackNum)
  {
    this.letter       = letter;
    this.callTagId    = callTagId;
    this.dhlTrackNum  = dhlTrackNum;
    init();
  }

  private void init()
  {
    
    info = new _merchCallTagInfo(callTagId);
    decodeLetterType();
    startDoc();

  }
  
  //Simply resolves the letter name to its int representation
  private void decodeLetterType()
  {
    String[][] letters = mesConstants.CT_LETTERS;
    for(int i = 0; i < letters.length; i++)
    {
      if( letter.equals(letters[i][1]) )
      {
        try
        {
          letterType = Integer.parseInt(letters[i][0]);
        }
        catch (Exception e)
        {
          //leave at 1;
        }
        break;
      }
    }
  }

  private void startDoc()
  {
    
    document = new Document(PageSize.A4, 50, 50, 50, 25);
    baos = new ByteArrayOutputStream();
    try
    {
      writer = PdfWriter.getInstance(document, baos);

      //generate date strings
      Calendar cal = Calendar.getInstance();
      DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
      dateString = df.format(cal.getTime());

      Phrase phrase = new Phrase("Merchant e-Solutions, Inc. ",FOOTER_BOLD);
      phrase.add(new Phrase("920 N. Argonne Rd. Suite 200 Spokane, WA 99212-2722",FOOTER));
      HeaderFooter footer = new HeaderFooter(phrase, false);
      footer.setAlignment(Element.ALIGN_CENTER);
      document.setFooter(footer);
      
      //open document
      document.open();

    }
    catch (Exception e)
    {
      log.debug("init Exception: "+ e.getMessage());
    }
    
  }

  public Object getResult()
  {
    if(document.isOpen())
    {
      document.close();
    }

    if( baos != null )
    {
      return baos.toByteArray();
    }
    else
    {
      return null;
      //TODO
      //generate standard exception PDF
    }
  }

  public void buildHeader()
  {

    try
    {
      PdfPTable table = new PdfPTable(2);
      table.setWidthPercentage(100);
      int[] widths = {50,50};
      table.setWidths(widths);     

      PdfPCell cell;
      Phrase phrase;
      
      //row 1
      try{
        Image logo = Image.getInstance("http://www.merchante-solutions.com/logos/ms_logo2.gif");
        logo.scalePercent(80f,80f);
        cell = new PdfPCell(logo, false);
        cell.setBorder(0);
        table.addCell(cell);
      }
      catch(Exception e1)
      {
        log.debug("buildHeader LOGO Exception: "+ e1.getMessage());
        //no graphic default
        phrase = new Phrase("MERCHANT SERVICES",BIG_HEADER);
        cell = new PdfPCell(phrase);
        cell.setBorder(0);
        table.addCell(cell);
        
      }
      
      phrase = new Phrase("920 North Argonne\n",BODY_3);
      phrase.add(new Phrase("Suite 200\n",BODY_3));
      phrase.add(new Phrase("Spokane WA 99212\n",BODY_3));
      phrase.add(new Phrase("Suite 200\n",BODY_3));
      cell = new PdfPCell(phrase);
      cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
      cell.setBorder(0);
      table.addCell(cell);

      //row 2
      phrase = new Phrase("\n\n\n");
      phrase.add(new Phrase(info.merchantNumber+"\n",BODY_3));
      phrase.add(new Phrase(info.getAddress(),BODY_3));
      phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n\n");
      phrase.add(new Phrase(dateString,BODY_3));
      cell = new PdfPCell(phrase);
      cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
      cell.setBorder(0);
      table.addCell(cell);      

      //row 3
      phrase = new Phrase("DHL Call Tag#: "+ dhlTrackNum, BODY_3);
      phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      cell.setColspan(2);
      table.addCell(cell);

      //row 4
      phrase = new Phrase("Subject: "+ getSubject() , HEADER_2);
      phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setHorizontalAlignment(Element.ALIGN_CENTER);
      cell.setBorder(0);
      cell.setColspan(2);
      table.addCell(cell);

      //row 5
      phrase = new Phrase("Dear Merchant:",BODY_1);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      cell.setColspan(2);
      table.addCell(cell);

      document.add(table);
    }
    catch(Exception e)
    {
      log.debug("buildHeader Exception: "+ e.getMessage());
    }

  }

  public void buildBody()
  {

    try
    {
          
      PdfPCell cell;
      Phrase phrase;
      PdfPCellEvent evt;
      
      PdfPTable table = new PdfPTable(1);
      table.setWidthPercentage(100);
      
      switch(letterType)
      {
        case GENERAL:
        
          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Today we received a request to pickup the following:",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);
          
          phrase = new Phrase("");
          cell = new PdfPCell(phrase);
          evt = new PdfPCellEvent() {
                  public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
                  {
                    PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                    try{
                      TextField app = new TextField(writer,position, "equip_"+letterType);
                      app.setBorderWidth(0);
                      app.setText("<<Enter equipment info here>>");
                      app.setOptions(app.MULTILINE | app.REQUIRED);
                      PdfFormField field = app.getTextField();
                      writer.addAnnotation(field);
                    }catch(Exception e){}
                  }
                };
          cell.setCellEvent(evt);
          cell.setMinimumHeight(15f);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n");
          phrase.add(new Phrase("Per our standard operating procedures, we are sending you a DHL Call Tag to pick up this equipment. At this time, we are requesting that you please box the equipment and return it to us utilizing the enclosed call tag.",BODY_1));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("*Please box the equipment and call DHL at 1-800-225-5345 for a pickup*",BODY_2));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          table.addCell(cell);
          
          break;
          
        case MOE_LOAN_SWAP:
          
          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Thank you for choosing Merchant Services as your credit card processing company.  Today we are sending a DHL Express Call Tag with a MeS loaner terminal:",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);
          
          phrase = new Phrase("");
          cell = new PdfPCell(phrase);
          evt = new PdfPCellEvent() {
                  public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
                  {
                    PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                    try{
                      TextField app = new TextField(writer,position, "outequip_"+letterType);
                      app.setBorderWidth(0);
                      app.setText("<<Enter equipment info here>>");
                      app.setOptions(app.MULTILINE | app.REQUIRED);
                      PdfFormField field = app.getTextField();
                      writer.addAnnotation(field);
                    }catch(Exception e){}
                  }
                };
          cell.setCellEvent(evt);
          cell.setMinimumHeight(15f);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n");
          phrase.add(new Phrase("In exchange for this, your merchant-owned terminal, so that we may have it encrypted:",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("");
          cell = new PdfPCell(phrase);
          evt = new PdfPCellEvent() {
                  public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
                  {
                    PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                    try{
                      TextField app = new TextField(writer, position, "inequip_"+letterType);
                      app.setBorderWidth(0);
                      app.setText("<<Enter equipment info here>>");
                      app.setOptions(app.MULTILINE | app.REQUIRED);
                      PdfFormField field = app.getTextField();
                      writer.addAnnotation(field);
                    }catch(Exception e){}
                  }
                };         
          cell.setCellEvent(evt);
          cell.setMinimumHeight(15f);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("At this time, we would like to remind you to please send back your merchant-owned terminal with the enclosed call tag. After it is encrypted, we will send it back to swap out the Merchant Services' Loaner Terminal.  IF WE DO NOT RECEIVE THE REQUESTED EQUIPMENT WITHIN 15 DAYS, YOU WILL BE CHARGED FOR THE LOANER UNIT.",BODY_2));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);          

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("If you do not receive regular shipments from DHL, you can call 1-800-225-5345 to schedule a pickup.",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);
          
          break;    
        
        
        case MOE_LOAN_SWAP_BACK:

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Thank you for choosing Merchant Services as your credit card processing company.  Today we are sending a DHL Call Tag with your original:",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);
          
          phrase = new Phrase("");
          cell = new PdfPCell(phrase);
          evt = new PdfPCellEvent() {
                  public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
                  {
                    PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                    try{
                      TextField app = new TextField(writer, position, "outequip_"+letterType);
                      app.setBorderWidth(0);
                      app.setText("<<Enter equipment info here>>");
                      app.setOptions(app.MULTILINE | app.REQUIRED);
                      PdfFormField field = app.getTextField();
                      writer.addAnnotation(field);
                    }catch(Exception e){}
                  }
                };
          cell.setCellEvent(evt);
          cell.setMinimumHeight(15f);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n");
          phrase.add(new Phrase("In exchange for this Merchant Services' loaner:",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("");
          cell = new PdfPCell(phrase);
          evt = new PdfPCellEvent() {
                  public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
                  {
                    PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                    try{
                      TextField app = new TextField(writer, position, "inequip_"+letterType);
                      app.setBorderWidth(0);
                      app.setText("<<Enter equipment info here>>");
                      app.setOptions(app.MULTILINE | app.REQUIRED);
                      PdfFormField field = app.getTextField();
                      writer.addAnnotation(field);
                    }catch(Exception e){}
                  }
                };         
          cell.setCellEvent(evt);
          cell.setMinimumHeight(15f);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("At this time, we would like to remind you to please send back the loaned equipment with the enclosed call tag. IF WE DO NOT RECEIVE THIS EQUIPMENT WITHIN 15 DAYS, YOU WILL BE CHARGED FOR THE LOANER UNIT.",BODY_2));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);          

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("If you do not receive regular shipments from DHL, you can call 1-800-225-5345 to schedule a pickup.",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);
          
          break;
        
        
        
        case PINPAD_ENCRYPT:
        
          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Thank you for choosing Merchant Services as your credit card processing company. We are happy to have you on board and look forward to meeting your future processing needs.",BODY_1));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);
          
          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Per our standard operating procedures, we are sending you an encrypted replacement PINpad. Your present PINpad is not compatible with your new programming. Please send the PINpad you are currently using to us; this original PINpad will not be returned to you.",BODY_1));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("*Please box your old equipment and call DHL at 1-800-225-5345 for a pickup*  IF WE DO NOT RECEIVE YOUR OLD PINpad WITHIN 15 DAYS, YOU WILL BE CHARGED $125.",BODY_2));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          table.addCell(cell);

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Steps for connecting your PINpad:\n",BODY_2));
          phrase.add(new Phrase("It is crucial to follow these steps so as not to \"blow out\" the programming on the equipment:\n",BODY_4));
          phrase.add(new Phrase("1. Unplug the power cord from the terminal so it is off.\n",BODY_1));
          phrase.add(new Phrase("2. Unplug the connected PINpad from the terminal.\n",BODY_1));
          phrase.add(new Phrase("3. Plug in the new PINpad that was shipped from Merchant Services.\n",BODY_1));
          phrase.add(new Phrase("4. Plug the power cord into the terminal, and you new PINpad is ready to go.\n",BODY_1));
          phrase.add(new Phrase("5. If you have any questions, call our Help Desk (1-888-288-2692) and simply inform the representative that you are swapping PINpads.\n",BODY_1));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);          
          
          break;
          
          
        case PINPAD_REJECT:
          
          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("We've recently mailed you an encrypted PINpad, as part of the Merchant Services PINpad exchange program, with an enclosed Call Tag to return your unencrypted PINpad.",BODY_1));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Unfortunately, the PINpad you sent will not support Merchant Services' encryption and cannot be used in the future. Per Merchant Services PINpad exchange Policy: ",BODY_1));
          phrase.add(new Phrase("If the merchant has a non-supportable model, the merchant must purchase a compatible stock model.",BODY_4));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Per your request, we are returning your original PINpad with a call tag for you to return the loaner we issued. Please package this PINpad and call DHL for pickup at 1-800-225-5345. We will credit your account when we receive the PINpad back at Merchant Services.",BODY_1));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);             
          break;      
        
        case PINPAD_SWAP:
          
          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Thank you for choosing Merchant Services as your credit card processing company. We are happy to have you on board and look forward to meeting your future processing needs.",BODY_1));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);
          
          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Enclosed is a swap PINpad. Please return the PINpad you are currently using to Merchant Services.",BODY_1));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("*Please box your old equipment and call DHL at 1-800-225-5345 for a pickup*\nIF WE DO NOT RECEIVE YOUR OLD PINpad WITHIN 15 DAYS, YOU WILL BE CHARGED $125.",BODY_2));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          cell.setHorizontalAlignment(Element.ALIGN_CENTER);
          table.addCell(cell);

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Steps for connecting your PINpad:\n",BODY_2));
          phrase.add(new Phrase("It is crucial to follow these steps so as not to \"blow out\" the programming on the equipment:\n",BODY_4));
          phrase.add(new Phrase("1. Unplug the power cord from the terminal so it is off.\n",BODY_1));
          phrase.add(new Phrase("2. Unplug the connected PINpad from the terminal.\n",BODY_1));
          phrase.add(new Phrase("3. Plug in the new PINpad that was shipped from Merchant Services.\n",BODY_1));
          phrase.add(new Phrase("4. Plug the power cord into the terminal, and you new PINpad is ready to go.\n",BODY_1));
          phrase.add(new Phrase("5. If you have any questions, call our Help Desk (1-888-288-2692) and simply inform the representative that you are swapping PINpads.\n",BODY_1));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell); 
          
          break;
          
        case RESTRICTED_SWAP:
        
          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Thank you for choosing Merchant Services as your credit card processing company.  Today we are sending a DHL Call Tag with a:",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);
          
          phrase = new Phrase("");
          cell = new PdfPCell(phrase);
          evt = new PdfPCellEvent() {
                  public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
                  {
                    PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                    try{
                      TextField app = new TextField(writer, position, "outequip_"+letterType);
                      app.setBorderWidth(0);
                      app.setText("<<Enter equipment info here>>");
                      app.setOptions(app.MULTILINE | app.REQUIRED);
                      PdfFormField field = app.getTextField();
                      writer.addAnnotation(field);
                    }catch(Exception e){}
                  }
                };
          cell.setCellEvent(evt);
          cell.setMinimumHeight(15f);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("Unfortunately we sent a:",BODY_1);
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("");
          cell = new PdfPCell(phrase);
          evt = new PdfPCellEvent() {
                  public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
                  {
                    PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                    try{
                      TextField app = new TextField(writer, position, "inequip_"+letterType);
                      app.setBorderWidth(0);
                      app.setText("<<Enter equipment info here>>");
                      app.setOptions(app.MULTILINE | app.REQUIRED);
                      PdfFormField field = app.getTextField();
                      writer.addAnnotation(field);
                    }catch(Exception e){}
                  }
                };         
          cell.setCellEvent(evt);
          cell.setMinimumHeight(15f);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("out of our restricted inventory and would like to have it returned as soon as possible.",BODY_1);
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("At this time, we would like to remind you to please send back the RESTRICTED equipment with the enclosed call tag.",BODY_2));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);          

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("If you do not receive regular shipments from DHL, you can call 1-800-225-5345 to schedule a pickup.",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);
          
          break;    
        
        case SWAP:

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("Thank you for choosing Merchant Services as your credit card processing company.  Today we are sending a DHL Express Call Tag with a replacement:",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);
          
          phrase = new Phrase("");
          cell = new PdfPCell(phrase);
          evt = new PdfPCellEvent() {
                  public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
                  {
                    PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                    try{
                      TextField app = new TextField(writer, position , "outequip_"+letterType);
                      app.setBorderWidth(0);
                      app.setText("<<Enter equipment info here>>");
                      app.setOptions(app.MULTILINE | app.REQUIRED);
                      PdfFormField field = app.getTextField();
                      writer.addAnnotation(field);
                    }catch(Exception e){}
                  }
                };
          cell.setCellEvent(evt);
          cell.setMinimumHeight(15f);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n");
          phrase.add(new Phrase("In exchange for your defective:",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("");
          cell = new PdfPCell(phrase);
          evt = new PdfPCellEvent() {
                  public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
                  {
                    PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                    try{
                      TextField app = new TextField(writer, position, "inequip_"+letterType);
                      app.setBorderWidth(0);
                      app.setText("<<Enter equipment info here>>");
                      app.setOptions(app.MULTILINE | app.REQUIRED);
                      PdfFormField field = app.getTextField();
                      writer.addAnnotation(field);
                    }catch(Exception e){}
                  }
                };         
          cell.setCellEvent(evt);
          cell.setMinimumHeight(15f);
          cell.setBorder(0);
          table.addCell(cell);

          phrase = new Phrase("\n");
          phrase.add(new Phrase("At this time, we would like to remind you to please send back the defective equipment with the enclosed call tag. IF WE DO NOT RECEIVE THE REQUESTED EQUIPMENT WITHIN 15 DAYS, YOU WILL BE CHARGED.",BODY_2));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);          

          phrase = new Phrase("\n\n");
          phrase.add(new Phrase("If you do not receive regular shipments from DHL, you can call 1-800-225-5345 to schedule a pickup.",BODY_1));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          table.addCell(cell);
          
          break;    

        default:
          phrase = new Phrase("\n");
          phrase.add(new Phrase("no body copy present",BODY_2));
          phrase.add(new Phrase("\n\n"));
          cell = new PdfPCell(phrase);
          cell.setBorder(0);
          cell.setMinimumHeight(400);
          table.addCell(cell);  
      }    

      document.add(table);
     
    }
    catch(Exception e)
    {
      log.debug("buildBody Exception: "+ e.getMessage());
    }
  }


  public void buildFooter()
  {

    PdfPTable table = new PdfPTable(1);
    table.setWidthPercentage(100);

    PdfPCell cell;
    Phrase phrase;

    try
    {
      
      phrase = new Phrase("\n");
      phrase.add(new Phrase("We realize that, as a merchant, you are busy and we certainly appreciate your efforts. If you have any questions, please do not hesitate to contact us.",BODY_1));
      phrase.add(new Phrase("\n\n"));
      phrase.add(new Phrase("Thank You,",BODY_1));
      phrase.add(new Phrase("\n\n"));
      phrase.add(new Phrase("Merchant Services",BODY_2));
      phrase.add(new Phrase("\n"));
      phrase.add(new Phrase("1-888-288-2692",BODY_1));
      phrase.add(new Phrase("\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);        

      document.add(table);
      
    }
    catch(Exception e)
    {
      log.debug("buildFooter Exception: "+ e.getMessage());
    }
  }

  private String getSubject()
  {
    String subject;
    
    switch(letterType)
    {
      case GENERAL:
      case MOE_LOAN_SWAP_BACK:
      case RESTRICTED_SWAP:
      case SWAP:
        subject = "Equipment Return";
        break;
      case MOE_LOAN_SWAP:
        subject = "Merchant-Owned Terminal Encryption";
        break;    
      case PINPAD_ENCRYPT:
        subject = "Return PINpad";
        break;    
      case PINPAD_REJECT:
        subject = "PINpad Exchange Return";
        break;      
      case PINPAD_SWAP:
        subject = "PINpad Swap";
        break;                
      default:
        subject = "";
    }
    
    return subject;
    
  }

  //Inner Class to hold data for doc generation
  private class _merchCallTagInfo
  {

    public  long ctid = 0L;

    public  String        merchantNumber          = null;
    public  String        name                    = null;
    public  String        bankNumber              = null;
    public  String        address1                = null;
    public  String        address2                = null;
    public  String        city                    = null;
    public  String        state                   = null;
    public  String        zip                     = null;

    _merchCallTagInfo(long ctid){

      this.ctid = ctid;
      init(ctid);
    }

    private void init(long ctid)
    {

      ResultSetIterator it;
      ResultSet rs;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:841^9*/

//  ************************************************************
//  #sql [Ctx] it = { select
//              ct.merch_number                         as merchant_number,
//              mif.bank_number                         as bank_number,
//              mif.dba_name                            as name,
//              mif.dmaddr                              as addr1_mes,
//              mif.address_line_3                      as addr2_mes,
//              mif.dmcity                              as city_mes,
//              mif.dmstate                             as state_mes,
//              mif.dmzip                               as zip_mes,
//              mif.addr1_line_1                        as addr1_oth,
//              mif.addr1_line_2                        as addr2_oth,
//              mif.city1_line_4                        as city_oth,
//              mif.state1_line_4                       as state_oth,
//              mif.zip1_line_4                         as zip_oth
//            from
//              equip_calltag     ct,
//              mif mif
//            where
//              ct.ct_seq_num = :ctid
//            and
//              mif.merchant_number(+) = ct.merch_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n            ct.merch_number                         as merchant_number,\n            mif.bank_number                         as bank_number,\n            mif.dba_name                            as name,\n            mif.dmaddr                              as addr1_mes,\n            mif.address_line_3                      as addr2_mes,\n            mif.dmcity                              as city_mes,\n            mif.dmstate                             as state_mes,\n            mif.dmzip                               as zip_mes,\n            mif.addr1_line_1                        as addr1_oth,\n            mif.addr1_line_2                        as addr2_oth,\n            mif.city1_line_4                        as city_oth,\n            mif.state1_line_4                       as state_oth,\n            mif.zip1_line_4                         as zip_oth\n          from\n            equip_calltag     ct,\n            mif mif\n          where\n            ct.ct_seq_num =  :1 \n          and\n            mif.merchant_number(+) = ct.merch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.InvNotifyDocBuilder",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ctid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.InvNotifyDocBuilder",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:864^9*/

        rs = it.getResultSet();

        if (rs.next())
        {
          merchantNumber  = rs.getString("merchant_number");
          bankNumber      = rs.getString("bank_number");
          name            = rs.getString("name");

          //generate address - different for mes and others
          if(bankNumber!=null && bankNumber.equals("3941"))
          {
            address1      = rs.getString("addr1_mes");
            address2      = rs.getString("addr2_mes");
            city          = rs.getString("city_mes");
            state         = rs.getString("state_mes");
            zip           = rs.getString("zip_mes");
          }
          else
          {
            address1      = rs.getString("addr1_oth");
            address2      = rs.getString("addr2_oth");
            city          = rs.getString("city_oth");
            state         = rs.getString("state_oth");
            zip           = rs.getString("zip_oth");
          }
        }

        rs.close();
        it.close();
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
      finally
      {
        try{ cleanUp(); }catch(Exception e1){}
      }
   }

   public String getAddress()
   {
     StringBuffer sb = new StringBuffer(32);
     sb.append(name).append("\n");
     sb.append(address1).append("\n");
     if(address2!=null && !address2.equals(""))
     {
       sb.append(address2).append("\n");
     }
     sb.append(city).append(", ").append(state).append(" ").append(zip);
     return sb.toString();
   }

  }

  //not working...
  private PdfPCellEvent getCellEvent()
  {
    
    return new PdfPCellEvent()
    {
      public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
      {
        PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
        try
        {
          
          String title;
          String text;
          
          if(cell instanceof EditablePdfPCell)
          {
            title = ((EditablePdfPCell)cell).getTitle();
            text = ((EditablePdfPCell)cell).getDefaultText();
          }
          else
          {
            title = "default";
            text = "<<enter info here>>";
          }
          
          float a,b,c,d,z;
          a = position.left();
          b = position.bottom();
          c = position.right();
          d = position.top();
          z = 0.5f*c;
          log.debug("("+a+","+b+") --> "+"("+z+","+d+")");
          log.debug("("+z+","+b+") --> "+"("+c+","+d+")");

          TextField app = new TextField(writer, new Rectangle(a,b,z,d), title);
          app.setBorderWidth(0);
          app.setText(text);
          app.setOptions(app.MULTILINE | app.REQUIRED);
          PdfFormField field = app.getTextField();
          writer.addAnnotation(field);
          
        }
        catch(Exception e)
        {
        }
      }
    };
  }
  
  private final class EditablePdfPCell extends PdfPCell
  {
    private String _defaultText;
    private String _title;
    
    public EditablePdfPCell(Phrase phrase, String title, String defaultText)
    {
      super(phrase);
      this._title = title;
      this._defaultText = defaultText;
    }
    
    public EditablePdfPCell(Phrase phrase)
    {
      super(phrase);
      this._title = "";
      this._defaultText = "<<enter info here>>";
    }
    
    public String getTitle() { return _title; }
    public String getDefaultText() { return _defaultText; }
    
  }

/*
      //breaks into two form fields on same row
      //example of form fields - DO NOT USE
      PdfPCellEvent evt = new PdfPCellEvent(){
          public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases){
              PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
              try{
                float a,b,c,d,z;
                a = position.left();
                b = position.bottom();
                c = position.right();
                d = position.top();
                z = 0.5f*c;
                log.debug("("+a+","+b+") --> "+"("+z+","+d+")");
                log.debug("("+z+","+b+") --> "+"("+c+","+d+")");
                //1
                TextField app = new TextField(writer, new Rectangle(a,b,z,d), "equipment1");
                //app.setBackgroundColor(Color.red);
                //app.setBorderColor(Color.blue);
                app.setBorderWidth(0);
                //app.setBorderStyle(PdfBorderDictionary.STYLE_BEVELED);
                app.setText("<enter equipment type here...>");
                //app.setAlignment(Element.ALIGN_CENTER);
                app.setOptions(app.MULTILINE | app.REQUIRED);
                //app.setRotation(90);
                PdfFormField field = app.getTextField();
                writer.addAnnotation(field);
                //2
                app = new TextField(writer, new Rectangle(z,b,c,d), "equipment2");
                //app.setBackgroundColor(Color.blue);
                //app.setBorderColor(Color.blue);
                app.setBorderWidth(0);
                //app.setBorderStyle(PdfBorderDictionary.STYLE_BEVELED);
                app.setText("<enter serial number here...>");
                //app.setAlignment(Element.ALIGN_CENTER);
                app.setOptions(app.MULTILINE | app.REQUIRED);
                //app.setRotation(90);
                field = app.getTextField();
                writer.addAnnotation(field);                
              }catch(Exception e){}
          }
      };
      
      
*/

}/*@lineinfo:generated-code*/