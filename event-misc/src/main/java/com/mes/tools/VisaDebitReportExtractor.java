/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/tools/VisaDebitReportExtractor.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-12-05 10:52:36 -0800 (Sat, 05 Dec 2009) $
  Version            : $Revision: 16784 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class VisaDebitReportExtractor
{
  private boolean             RemoveFirstChar       = false;
  private int                 ReportLineCount       = 0;
  
  private String readLine( BufferedReader in )
    throws Exception
  {
    String    line    = null;
    if ( (line = in.readLine()) != null )
    {
      if ( ++ReportLineCount == 1 )   // first line
      {
        RemoveFirstChar = line.startsWith("1");   // check for line #s
      } 
      if ( RemoveFirstChar )
      {
        line = line.substring(1);
      }
    }
    return( line );
  }
  
  public void extractReport( String filename, String reportId, String visaId )
  {
    BufferedReader    in                = null;
    BufferedWriter    out               = null;
    String            line              = null;
    
    try
    {
      ReportLineCount = 0;
      in = new BufferedReader( new FileReader( filename ) );
      
      while( (line = readLine(in)) != null )
      {
        if ( line.startsWith("REPORT ID:  " + reportId) )
        {
          String idLine = readLine(in);
          if ( idLine.indexOf(visaId) >= 0 )
          {
            out = new BufferedWriter( new FileWriter( reportId + ".txt" ) );
            out.write(line);
            out.newLine();
            out.write(idLine);
            out.newLine();
            do
            {
              idLine = readLine(in);
              out.write(idLine);
              out.newLine();
            }
            while( idLine.indexOf("END OF " + reportId) < 0 );
            break;  // done extracting
          }
        }
      }
      in.close();
      out.close();
    }
    catch( Exception e )
    {
      System.out.println("extractReport()" + ":"  + e.toString());
    }
  }

  public static void main( String[] args )
  {
    VisaDebitReportExtractor    widget  = null;
    
    try
    {
      widget = new VisaDebitReportExtractor();
      widget.extractReport( args[0], args[1], args[2] );
    }
    catch( Exception e )
    {
      System.out.println("main()" + e.toString());
    }
    finally
    {
    }
  }  
}