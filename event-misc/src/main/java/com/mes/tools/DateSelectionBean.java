/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/DateSelectionBean.java $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 11/08/01 10:42a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

public class DateSelectionBean
{
  public  int         currentYear   = Calendar.getInstance().get(Calendar.YEAR);
  public  Calendar    fromCalendar  = Calendar.getInstance();
  public  Calendar    toCalendar    = Calendar.getInstance();
  public  String      months[] = 
                      { 
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December" 
                      };
   
  public DateSelectionBean()
  {
  }
  
  public Date getRequestDate(HttpServletRequest request, String fromTo)
  {
    String    monthParam  = fromTo + "Month";
    String    dayParam    = fromTo + "Day";
    String    yearParam   = fromTo + "Year";
    Calendar  cal         = Calendar.getInstance();
    String    paramString = "";
    
    // extract the month
    paramString = request.getParameter(monthParam);
    if(paramString != null && ! paramString.equals(""))
    {
      try
      {
        cal.set(Calendar.MONTH, Integer.parseInt(paramString));
      }
      catch(Exception e)
      {
      }
    }
    
    // extract the day
    paramString = request.getParameter(dayParam);
    if(paramString != null && ! paramString.equals(""))
    {
      try
      {
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(paramString));
      }
      catch(Exception e)
      {
      }
    }
    
    // extract the year
    paramString = request.getParameter(yearParam);
    if(paramString != null && ! paramString.equals(""))
    {
      try
      {
        cal.set(Calendar.YEAR, Integer.parseInt(paramString));
      }
      catch(Exception e)
      {
      }
    }
    
    return cal.getTime();
  }
  
  public void initialize(HttpServletRequest request)
  {
    // set up the selected from and to date
    fromCalendar.setTime(getRequestDate(request, "from"));
    toCalendar.setTime(getRequestDate(request, "to"));
  }
}
