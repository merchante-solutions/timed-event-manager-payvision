/*@lineinfo:filename=GroupFixer*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/GroupFixer.sqlj $

  Description:
  
  GroupFixer

  Rebuilds a bank portfolio branch in the t_hierarchy tables.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/05/02 2:05p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.tools;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import com.mes.constants.MesHierarchy;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class GroupFixer extends SQLJConnectionBase
{
  public GroupFixer(String connectString)
  {
    super(connectString);
  }

  public GroupFixer()
  {
  }
  
  /*
  ** public class Node extends Vector
  **
  ** A Vector that also contains information about a node.  Thus a node
  ** that may contain children.
  */
  public class Node extends Vector
  {
    private long nodeId;
    private boolean isGroup;
    
    public Node(long nodeId, boolean isGroup)
    {
      this.nodeId = nodeId;
    }
    
    public long getNodeId()
    {
      return nodeId;
    }
    public boolean getIsGroup()
    {
      return isGroup;
    }
  }

  /*
  ** public class Hierarchy
  **
  ** Contains a hierarchy or a part of one.
  */
  public class Hierarchy
  {
    Hashtable hash = new Hashtable();
    Node rootNode;
    public Hierarchy(long nodeId)
    {
      rootNode = new Node(nodeId,true);
      hash.put(nodeId, rootNode);
    }
    
    public void addRow(ResultSet rs)
    {
      try
      {
        // build a vector from highest parent to lowest child 
        // (group 2...group n, assoc_num)
        // note that group 1 is implied, so is not included
        Vector row    = new Vector();
        for (int i = 2; i <= 10; ++i)
        {
          long group = rs.getLong("group_" + i);
          if (group > 0)
          {
            row.add(group);
          }
        }
        row.add(rs.getLong("assoc_number"));
      
        // add each successive node into the hierarchy as the child of the
        // node before it in the row vector, first row node is added as
        // child to the root node
        Node parentNode = rootNode;
        for (Iterator i = row.iterator(); i.hasNext();)
        {
          // see if the node has already been added into the hierarchy
          Long nodeKey = (Long)i.next();
          Node node = (Node)hash.get(nodeKey);
        
          // if node does not exist, create it, add it to the parent
          if (node == null)
          {
            node = new Node(nodeKey.longValue(),i.hasNext());
            hash.put(nodeKey,node);
            parentNode.add(node);
          }
        
          // this node will be the parent of the next in the row
          parentNode = node;
        }
      }
      catch (Exception e)
      {
        System.out.println("\n\naddRow() exception: " + e.toString());
      }
    }
    
    public Node getRootNode()
    {
      return rootNode;
    }
    
    public int getNodeCount()
    {
      return hash.size();
    }
  }
  
  /*
  ** private void displayHierarchy(Node node)
  **
  ** Recurses through a hierarchy tree and displays each node with indentation
  ** indicating the level of the node.
  */
  private int indent = 0;
  private void displayHierarchy(Node node)
  {
    for (int i = 0; i < indent; ++i) System.out.print(" ");
    System.out.println("Node: " + node.getNodeId());
    indent += 2;
    for (Iterator i = node.iterator(); i.hasNext(); )
    {
      displayHierarchy((Node)i.next());
    }
    indent -=2;
  }

  /*
  ** private void insertHierarchy(Node child, Node parent)
  **
  ** Recurses through a hierarchy tree and inserts each node into the
  ** hierarchy database.
  */
  private void insertHierarchy(Node child, Node parent)
  {
    // add all children of this child first
    for (Iterator i = child.iterator(); i.hasNext(); )
    {
      insertHierarchy((Node)i.next(),child);
    }
    
    // get the child id
    long childId = child.getNodeId();
    
    // in the special case of parent == null then 
    // the child is the top of this branch of the hierarchy,
    // add it to it's corresponding bank node
    long parentId = 0L;
    if (parent == null)
    {
      parentId = (childId / 1000000L) * 100000L;
    }
    // otherwise the child should be added as child of parent
    // indicated...
    else
    {
      parentId = parent.getNodeId();
    }
    
    // determine entity type
    int entityType =
      (child.getIsGroup() ? MesHierarchy.ET_GROUP : MesHierarchy.ET_ASSOCIATION);

    // insert this node
    HierarchyTools.insertNode(childId, 
                              entityType, 
                              Long.toString(childId),
                              parentId,
                              MesHierarchy.HT_BANK_PORTFOLIOS);
  }
  private void insertHierarchy(Node child)
  {
    insertHierarchy(child,null);
  }

  /*
  ** public void fix(String topGroup)
  **
  ** Rebuilds a bank portfolios hierarchy branch.
  */
  public void fix(String topGroup)
  {
    long startTime;
    int nodeCount;
    int rowCount;
    
    try
    {
      connect();
      
      // get the start node
      long startNode = Long.parseLong(topGroup);
      
      /*@lineinfo:generated-code*//*@lineinfo:233^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(assoc_number) 
//          from    groups
//          where   group_1 = :startNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(assoc_number)  \n        from    groups\n        where   group_1 =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.GroupFixer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,startNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:238^7*/
      
      // get all group records with the start node as the top level
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:242^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  assoc_number,
//                  group_2,
//                  group_3,
//                  group_4,
//                  group_5,
//                  group_6,
//                  group_7,
//                  group_8,
//                  group_9,
//                  group_10
//          from    groups
//          where   group_1 = :startNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  assoc_number,\n                group_2,\n                group_3,\n                group_4,\n                group_5,\n                group_6,\n                group_7,\n                group_8,\n                group_9,\n                group_10\n        from    groups\n        where   group_1 =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.GroupFixer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,startNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.tools.GroupFixer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:256^7*/
      ResultSet rs = it.getResultSet();

      System.out.println("Processing " + rowCount + " rows found in group with "
        + "top group " + startNode);

      // add each group row into the hierarchy
      Hierarchy hierarchy = new Hierarchy(startNode);
      while (rs.next())
      {
        hierarchy.addRow(rs);
      }
      
      rs.close();
      it.close();
      
      // display the built hierarchy
      //displayHierarchy(hierarchy.getRootNode());
      
      nodeCount = hierarchy.getNodeCount();
      System.out.println("Found " + nodeCount + " nodes in group rows");
      
      // rebuild the hierarchy
      startTime = Calendar.getInstance().getTime().getTime();
      insertHierarchy(hierarchy.getRootNode());
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::fixGroup()";
      String desc = "group = " + topGroup + ", " + e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** public static void fixGroup(String topGroup)
  **
  ** Rebuilds a hierarchy branch topped by topGroup.
  */
  public static void fixGroup(String topGroup)
  {
    GroupFixer fixer = new GroupFixer();
    fixer.fix(topGroup);
  }
}/*@lineinfo:generated-code*/