/*@lineinfo:filename=SalesHierarchyTree*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/SalesHierarchyTree.sqlj $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/10/04 3:23p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.util.Iterator;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.OrgBean;
import sqlj.runtime.ResultSetIterator;

public class SalesHierarchyTree extends SQLJConnectionBase
{
  private static OrgBean              patriarch           = null;
  private static SalesHierarchyTree   instance            = null;
  
  public static final int   DEFAULT_USER_ID           = 124955;
  
  private SalesHierarchyTree()
  {
  }
  
  public static SalesHierarchyTree getInstance()
  {
    try
    {
      if(instance == null)
      {
        instance = new SalesHierarchyTree();
        
        instance.makeTree();
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.support.SalesHierarchyTree::getInstance()", e.toString());
    }
    
    return instance;
  }
  
  public OrgBean getPatriarch()
  {
    return this.patriarch;
  }
  
  private synchronized void makeTree()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      // get patriarch data
      /*@lineinfo:generated-code*//*@lineinfo:82^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.user_id,
//                  u.name,
//                  sr.rep_id
//          from    users     u,
//                  sales_rep sr
//          where   u.user_id = :DEFAULT_USER_ID and
//                  u.user_id = sr.user_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  u.user_id,\n                u.name,\n                sr.rep_id\n        from    users     u,\n                sales_rep sr\n        where   u.user_id =  :1  and\n                u.user_id = sr.user_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.SalesHierarchyTree",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DEFAULT_USER_ID);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.SalesHierarchyTree",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:91^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        patriarch = new OrgBean(rs.getInt("user_id"), rs.getString("name"), rs.getString("rep_id"));
      }
      
      rs.close();
      it.close();
      
      procreate(patriarch);
      
    }
    catch(Exception e)
    {
      logEntry("makeTree()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  private synchronized void clearNodes(OrgBean node)
  {
    try
    {
      node.getChildren().clear();
    }
    catch(Exception e)
    {
      logEntry("clearNodes()", e.toString());
    }
  }
  
  private void procreate(OrgBean ob)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:136^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.user_id,
//                  u.name,
//                  sr.rep_id
//          from    users u,
//                  sales_rep sr
//          where   sr.rep_report_to = :ob.repId and
//                  sr.rep_terminate_date is null and
//                  sr.user_id = u.user_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  u.user_id,\n                u.name,\n                sr.rep_id\n        from    users u,\n                sales_rep sr\n        where   sr.rep_report_to =  :1  and\n                sr.rep_terminate_date is null and\n                sr.user_id = u.user_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.SalesHierarchyTree",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,ob.repId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.tools.SalesHierarchyTree",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:146^7*/
      
      rs = it.getResultSet();
      
      StringBuffer orgName = new StringBuffer("");
      while(rs.next())
      {
        orgName.setLength(0);
        orgName.append(rs.getString("rep_id"));
        orgName.append(" - ");
        orgName.append(rs.getString("name"));
        
        ob.addChild(rs.getInt("user_id"), orgName.toString(), rs.getString("rep_id"));
      }
      
      rs.close();
      it.close();
      
      // procreate each child
      for(Iterator i = ob.getChildren().iterator(); i.hasNext();)
      {
        procreate((OrgBean)(i.next()));
      }
    }
    catch(Exception e)
    {
      //@ System.out.println("procreate(" + ob.getOrgName() + "):" + e.toString());
      logEntry("procreate(" + ob.getOrgName() + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
}/*@lineinfo:generated-code*/