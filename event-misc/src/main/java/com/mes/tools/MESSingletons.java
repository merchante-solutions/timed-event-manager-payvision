/*************************************************************************

  FILE: $URL: $

  Description:
  
  MESSingletons
  
  Class for managing the various singletons available in the MES application

  Last Modified By   : $Author: tbaker $
  Last Modified Date : $Date: 2007-01-10 08:46:17 -0800 (Wed, 10 Jan 2007) $
  Version            : $Revision: 13276 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import com.mes.config.MesDefaults;
import com.mes.database.OracleConnectionPool;
import com.mes.support.HttpHelper;
import com.mes.tricipher.B2FSingleton;

public class MESSingletons
{
  public static void destroyAll()
  {
    try
    {
      System.out.println("Killing: OracleConnectionPool");
      OracleConnectionPool.destroy();
      
      System.out.println("Killing: HierarchyTree");
      HierarchyTree.destroy();
      
      System.out.println("Killing: MesDefaults");
      MesDefaults.destroy();
      
      System.out.println("Killing: HttpHelper");
      HttpHelper.destroy();
      
      System.out.println("Killing: B2FSingleton");
      B2FSingleton.destroy();
    }
    catch(Exception e)
    {
      System.out.println("Unable to destroy all singletons: " + e.toString());
    }
  }
}



