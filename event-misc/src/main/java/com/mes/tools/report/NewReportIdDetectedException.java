package com.mes.tools.report;

/**
 * Created with IntelliJ IDEA.
 * User: spushnof
 * Date: 3/15/13
 * Time: 2:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class NewReportIdDetectedException extends Exception
{
    public NewReportIdDetectedException()
    {
        super();
    }

    public NewReportIdDetectedException(String text)
    {
        super(text);
    }

    public NewReportIdDetectedException(Throwable throwable)
    {
        super(throwable);
    }

    public NewReportIdDetectedException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
