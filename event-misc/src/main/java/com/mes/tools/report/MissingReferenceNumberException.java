package com.mes.tools.report;

/**
 * Created with IntelliJ IDEA.
 * User: spushnof
 * Date: 3/13/13
 * Time: 11:33 AM
 * To change this template use File | Settings | File Templates.
 */
public class MissingReferenceNumberException extends Exception
{
    public MissingReferenceNumberException()
    {
        super();
    }

    public MissingReferenceNumberException(String text)
    {
        super(text);
    }

    public MissingReferenceNumberException(Throwable throwable)
    {
        super(throwable);
    }

    public MissingReferenceNumberException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}