/*@lineinfo:filename=MasterCardDebitReportExtractor*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.tools.report;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

/*************************************************************************

 FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/tools/MasterCardDebitReportExtractor.java $

 Description:

 Last Modified By   : $Author: spushnof $
 Last Modified Date : $Date: 2013-02-25 15:19:13 -0800 (Tue, 25 Feb 2013) $
 Version            : $Revision:  $

 Change History:
 See SVN database

 Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
 All rights reserved, Unauthorized distribution prohibited.

 This document contains information which is the proprietary
 property of Merchant e-Solutions, Inc.  This document is received in
 confidence and its contents may not be disclosed without the
 prior written consent of Merchant e-Solutions, Inc.

 **************************************************************************/

public class MasterCardDebitReportExtractor  extends ReportExtractorBase implements IReportExtractor
{
    /**
     * the current report id
     */
    private String              _CurrentReportId        = null;

    /**
     * the bank number
     */
    private int                 _bankNumber             = -1;

    /**
     * Master Card date format
     */
    final private String        DateFormat
                                        = DateTimeFormatter.DEFAULT_DATE_FORMAT;

    /**
     * report beginning tag
     */
    final private String ReportBeginning_Tag
                                        = "IP";

    /**
     * Clearing Cycle Acknowledgment Report
     */
    final private String        ClearingCycleAcknowledgementReport_Tag
                                        = "IP727010-AA";

    /**
     * Member report
     */
    final private String        MemberReport_Tag
                                        = "IP142110-AA";

    /**
     * Clearing Cycle Notification Report
     */
    final private String        ClearingCycleNotificationReport_Tag
                                        = "IP727020-AA";

    /**
     * "End of the report" tag
     */
    final private String        EndOfTheReport_Tag
                                        = "***END OF REPORT***";

    /**
     * "Start of the report" tag
     */
    final private String        StartOfTHeReport_Tag
                                        = "MASTERCARD WORLDWIDE";

    /**
     * Member id (of Merchant E-Solutions)
     */
    final private String        MemberId_Tag
                                        = "MEMBER ID:";

    /**
     * Run date tag
     */
    final private String        RunDate_Tag
                                        = "RUN DATE:";

    /**
     * File Id tag
     */
    final private String        FileId_Tag
                                        = "FILE ID:";

    /**
     * construction
     */
    public MasterCardDebitReportExtractor() throws WrongInputParametersException
    {
        // call parent class 1st
        super("Missing_MC_ICA_Numbers.txt", ReportExtractorBase.CardType_MC);
    }

    private int parseBankNumber(String idLine)
    {
        if (idLine.startsWith(MemberId_Tag))
        {
            try
            {
                return getBankNumber(parse_ICA_Number(idLine));
            }
            catch(MissingReferenceNumberException me)
            {
                System.out.println("Error in parsing bank number from the line >>> " + idLine);

                // can not get a valid bank number - skipping!
                System.out.println(me.getMessage());
                return -1;
            }
            catch (Exception e)
            {
                // can not get a valid bank number - skipping!
                java.io.StringWriter sw = new java.io.StringWriter();
                java.io.PrintWriter pw = new java.io.PrintWriter(sw);
                e.printStackTrace(pw);
                sw.toString();
                System.out.println(sw);
                return -1;
            }
        }
        return -1;
    }


    /**
     * gets the bank number for a given number
     * @param ica_number
     * @return a valid bank number
     * @throws Exception if no matching bank number is found
     */
    protected int getBankNumber(String ica_number) throws Exception
    {
        int bankNumber = -1;

        // NOTE: Hardcoded ICA numbers to bank numbers
        if ( 0 == "11606".compareTo(ica_number) )
        {
            bankNumber = 3941;
            return bankNumber;
        }

        // NOTE: Hardcoded ICA numbers to bank numbers
        if ( 0 == "12093".compareTo(ica_number) )
        {
            bankNumber = 3941;
            return bankNumber;
        }

        ResultSetIterator it    = null;
        ResultSet rs    = null;


        try
        {
            connect();

            /*@lineinfo:generated-code*//*@lineinfo:182^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  bank_number
//                          from    mbs_banks
//                          where   MC_ICA_NUMBER = :ica_number
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bank_number\n                        from    mbs_banks\n                        where   MC_ICA_NUMBER =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.report.MasterCardDebitReportExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,ica_number);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.report.MasterCardDebitReportExtractor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:187^13*/
            rs = it.getResultSet();
            while(rs.next())
            {
                bankNumber = rs.getInt("bank_number");
                break;  // assuming the bank number is unique!
            }

            rs.close();
            it.close();
        }
        catch(Exception e)
        {
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            e.printStackTrace(pw);
            sw.toString();
            System.out.println(sw);

            logEntry("getBankNumber(" + ica_number + ")", e);
        }
        finally
        {
            try { rs.close(); } catch(Exception e) {}
            try { it.close(); } catch(Exception e) {}
            cleanUp();
        }

        if ( -1 == bankNumber )
        {
            addErrorInformation(ica_number);

            // report an error
            throw new MissingReferenceNumberException( "ERROR: The bank number is missing (in mbs_banks table) for a given 'ica_number' (" + ica_number + ")." );
        }

        return bankNumber;
    }


    /**
     * accumulates report
     * @param line
     * @param in
     * @return true/false
     */
    protected boolean accumulateReport(String line, BufferedReader in)
    {
        boolean newReportIdDetected = false;
        StringBuffer out = null;
        String date = "";

        try
        {
            // read the initial 1st line of the report
            line = readLine(in);

            // extract report id
            String reportId = extract_ReportId(line);
            /**
             * If current report is not initialized at all,
             * then set it to be as "new report id"
             */
            _CurrentReportId = reportId;
            System.out.println("accumulateReport -> [reportId] >> " + reportId);

            // extract the date
            date = extract_ProcessingDate(line);
            System.out.println("accumulateReport -> [date] >> " + date);

            String idLine = "";

            out = new StringBuffer();
            out.append(line);
            out.append("\r\n");
            out.append(idLine);
            out.append("\r\n");

            boolean endOfFileDetected = false;

            // loop (till the end of the file)
            idLine = readLine(in);
            do
            {
                // read the next line
                /// System.out.println("accumulateReport -> [idLine] >> " + idLine);

                // if it is not null
                if(null != idLine)
                {
                    // if this a beginning of the report
                    if( isReportBeginning(idLine))
                    {
                        // extract report id
                        reportId = extract_ReportId(idLine);

                        // extract the date
                        date = extract_ProcessingDate(idLine);

                        // compare the report id that we just get from the file against the one that we store...
                        if (!isSameReport(reportId))
                        {
                            /*
                            System.out.println( "VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV" );
                            System.out.println( out.toString() );
                            System.out.println( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" );
                            */

                            /**
                             * it is not the same report id, so store what we have accumulate so far, dump it
                             * to the database and reset the output buffer
                             */
                            storeReport(_bankNumber, date,  _CurrentReportId, out.toString());

                            /**
                             * set current report to be the new one
                             */
                            _CurrentReportId = reportId;

                            /**
                             * clear out the output buffer
                             */
                            out.setLength(0);

                            /**
                             * continue to the next line in the report
                             */
                            continue;
                        }
                        else
                        {
                            // System.out.println("THE SAME REPORT!");
                        }
                    }

                    /**
                     * is it a bank number?...
                     */
                    if (idLine.startsWith(MemberId_Tag))
                    {
                        _bankNumber = parseBankNumber(idLine);
                        if (-1 == _bankNumber)
                        {
                            continue;
                        }
                    }

                    /**
                     * append line from the report
                     */
                    out.append(idLine);

                    /**
                     * add CR/LF separator
                     */
                    out.append("\r\n");
                }
                else
                {
                    System.out.println("Warning: can not process line, stopping parsing...");
                    // this has to be the end of the file (without End of the report tag)
                    break;
                }


                try
                {
                    idLine = readLine(in);
                }
                catch(Exception e1)
                {
                    endOfFileDetected = true;
                }

            /**
             * loop till the end of the file is detected
              */
            } while( !endOfFileDetected );

         }
        catch(Exception e)
        {
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            e.printStackTrace(pw);
            sw.toString();
            System.out.println(sw);
        }


        /*
        System.out.println( "FINAL STORE!!!!!" );
        System.out.println( "VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV" );
        System.out.println( out.toString() );
        System.out.println( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" );
        */

        // store the report in the database
        return storeReport(_bankNumber, date,  _CurrentReportId, out.toString());
    }

    /**
     * gets the report id from the report
     * @param line
     * @return reportID
     */
    protected String extract_ReportId(String line) throws Exception
    {
        String reportId = "";

        int offset_reportId = 0;

        if (isReportBeginning(line))
        {
            reportId = line.substring(offset_reportId+0,offset_reportId+11);
            reportId = reportId.trim();
        }

        if("".compareTo(reportId) == 0)
        {
            throw  new Exception("Report ID is NOT found");
        }

        return reportId;
    }

    /**
     * checks if it is a report beginning
     * @param line
     * @return
     */
    private boolean isReportBeginning(String line)
    {
        boolean result = line.startsWith(ReportBeginning_Tag) ? true : false;
        return result;
    }

    /**
     * Gets the processing date from the report
     * @param line
     * @return processing (or run) date
     */
    protected String extract_ProcessingDate(String line)
    {
        String processingDate   = "";
        int     offset          = 0;

        if ( (offset = line.indexOf(RunDate_Tag)) >= 0 )
        {
            processingDate = line.substring(offset+11,offset+18);
            DateFormat masterCardDateFormat = new SimpleDateFormat("MM/dd/yy", Locale.ENGLISH);
            DateFormat internalStorageDateFormat = new SimpleDateFormat("dd/MMM/yy", Locale.ENGLISH);
            try
            {
                processingDate = internalStorageDateFormat.format(masterCardDateFormat.parse(processingDate));
            }
            catch(ParseException pe)
            {
		String  parsedDate = "";

                parsedDate = line.substring(offset+10,offset+20);
                DateFormat masterCardDateFormat_Normalize = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
                DateFormat internalStorageDateFormat_Normalize = new SimpleDateFormat("dd/MMM/yy", Locale.ENGLISH);

                try
                {
                    processingDate = internalStorageDateFormat_Normalize.format(
                            masterCardDateFormat_Normalize.parse(parsedDate));
                    System.out.println( "Detected not standard date format. Parsed Date [" + parsedDate + "]. Normalized date: [" + processingDate + "]" );

                }
                catch(ParseException pe1)
                {
                    java.io.StringWriter sw = new java.io.StringWriter();
                    java.io.PrintWriter pw = new java.io.PrintWriter(sw);
                    pe1.printStackTrace(pw);
                    sw.toString();
                    System.out.println(sw);
                    System.out.println("Warning: Can not parse date. Using the current date.");
                    processingDate = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(), DateFormat);
                }
            }
        }
        else
        {
            System.out.println("Warning: Run date is not found. Using the current date.");
            processingDate = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(), DateFormat);
        }

        return processingDate;
    }


    /**
     * parses the ICA number from the MC report
     * @param line
     * @return
     * @throws Exception
     */
    private String parse_ICA_Number(String line) throws  Exception
    {
        String ICANumber = "";
        int offset = 0;
        if ( (offset = line.indexOf(MemberId_Tag)) >= 0 )
        {
            ICANumber = line.substring(offset+11,offset+22);

            // trim leading zeros
            ICANumber = ICANumber.replaceFirst("^0+(?!$)", "");
        }
        else
        {
            throw new Exception("Can not parse ICA number!");
        }

        return ICANumber;
    }

    /**
     * checks if we have the same port or not
     * @param newReportId - new report id
     * @return true if same report or false if not
     */
    private boolean isSameReport(String newReportId)
    {
        /**
         * if current is the same as new report id
         * then it is the same report id
         */
        if (0==_CurrentReportId.compareTo(newReportId))
        {
            return true;
        }
        /**
         * it is NOT the same report
         */
        return false;
    }

    /**
     * extracts one report
     * @param in
     * @param line
     * @return true if extracted or false if not
     * @throws Exception
     */
    protected boolean extractOneReport(BufferedReader in, String line) throws Exception
    {
        try
        {
            accumulateReport(line, in);
            return true;
        }
        catch( Exception e)
        {
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            e.printStackTrace(pw);
            sw.toString();
            System.out.println(sw);
        }
        return true;
    }


    /**
     * wrapper for conditional extraction of the report.
     * @param in
     * @param line
     * @return
     * @throws Exception
     */
    protected boolean onExtractOneReport(BufferedReader in, String line) throws Exception
    {
        boolean success = false;
        // if ( line.startsWith(ReportBeginning_Tag) )
        {
            success = extractOneReport(in, line);
        }
        return success;
    }



    public boolean extract(String filename)
    {
        System.out.println("extract 1");


        boolean success = false;
        BufferedReader      in                  = null;
        String              line                = null;
        try
        {
            // System.out.println("extract START");

            System.out.println("Processing report: " + filename );
            in = new BufferedReader( new FileReader( filename ) );
            /*
            while( (line = readLine(in)) != null )
            {
                // System.out.println("-- MC -- Main Loop: [" + line + "]");
                success = onExtractOneReport(in, line);
            }
            */

            success = onExtractOneReport(in, "");


            // System.out.println("extract END");
        }
        catch( Exception e )
        {
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            e.printStackTrace(pw);
            sw.toString();
            System.out.println(sw);

            logEntry("extract", e);
            success = false;
        }
        finally
        {
            // System.out.println("extract 3");
            dumpErrorsToFile();
            try
            {
                in.close();
            }
            catch( Exception ee )
            {
                logEntry("extract", ee);
                success = false;
            }
        }
        return success;
    }




    /**
     * the main program.
     * example of usage:
     * [program name] [mc_report06_051412_062745.txt]
     *      where report name is a full file name of the report.
     * @param args
     */
    public static void main( String[] args )
    {
        MasterCardDebitReportExtractor    widget  = null;

        try
        {
            widget = new MasterCardDebitReportExtractor();
            if (0==args.length)
            {
                System.out.println("Usage: report file name");
                System.exit(0);
            }
            widget.extract(args[0]);
        }
        catch( Exception e )
        {
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            e.printStackTrace(pw);
            sw.toString();
            System.out.println(sw);
        }
        finally
        {
        }

        System.exit(0);
    }
}/*@lineinfo:generated-code*/