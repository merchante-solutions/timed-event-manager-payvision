package com.mes.tools.report;

/**
 * Created with IntelliJ IDEA.
 * User: spushnof
 * Date: 3/13/13
 * Time: 10:37 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IReportExtractor
{
    /**
     * extracts the report
     * @param fileName a full file name of the report file
     * @return true if report was extracted or false if not
     */
    boolean extract(String fileName);
}
