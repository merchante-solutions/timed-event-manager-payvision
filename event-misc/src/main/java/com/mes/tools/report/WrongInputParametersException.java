package com.mes.tools.report;

/**
 * Created with IntelliJ IDEA.
 * User: spushnof
 * Date: 3/14/13
 * Time: 10:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class WrongInputParametersException extends Exception
{
    public WrongInputParametersException()
    {
        super();
    }

    public WrongInputParametersException(String text)
    {
        super(text);
    }

    public WrongInputParametersException(Throwable throwable)
    {
        super(throwable);
    }

    public WrongInputParametersException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
