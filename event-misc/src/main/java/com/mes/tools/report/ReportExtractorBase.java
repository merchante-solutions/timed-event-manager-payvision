/*@lineinfo:filename=ReportExtractorBase*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.tools.report;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Iterator;
import com.mes.database.SQLJConnectionBase;
import oracle.sql.BLOB;

/**
 * Created with IntelliJ IDEA.
 * User: spushnof
 * Date: 3/12/13
 * Time: 1:42 PM
 * To change this template use File | Settings | File Templates.
 */
abstract class ReportExtractorBase  extends SQLJConnectionBase
{
    /**
     * card type association
     */
    private String              _cardTypeAssociation = null;

     /**
     * flag that indicates removal of the 1st character
     */
    private boolean             _removeFirstChar       = false;

    /**
     * reports the line count
     */
    private int                 _reportLineCount       = 0;

    /**
     * table of unique bank identification numbers
     */
    private HashSet             _errorsRecords       = null;

    /**
     * errors file name
     */
    private String              _errorsFileName       = null;

    /**
     * card type - Visa
     */
    public static String  CardType_VS = "VS";

    /**
     * card type - Master Card
     */
    public static String  CardType_MC = "MC";

    /**
     * construction
     * @param errorsFileName
     * @param cardType
     */
    protected ReportExtractorBase(String errorsFileName, String cardType) throws WrongInputParametersException
    {
        _errorsFileName = errorsFileName;

        if (0 == cardType.compareTo(CardType_MC ) || 0 == cardType.compareTo(CardType_VS))
        {
            _cardTypeAssociation = cardType;
        }
        else
        {
            throw new WrongInputParametersException("Invalid card type.");
        }

        _errorsRecords = new HashSet();
    }

    /**
     * adds errors
     * @param errorText
     */
    protected void addErrorInformation(String errorText)  throws WrongInputParametersException
    {
        if (errorText.length() > 0)
        {
            // Add an element to the collection of sri numbers.
            // Note: it may be duplicates, but we ignore then.
            _errorsRecords.add(errorText);
        }
        else
        {
            throw new WrongInputParametersException("Error text must contain at least one character.");
        }
    }

    /**
     * dumps errors to file
     */
    /*private*/ protected void dumpErrorsToFile()
    {
        try
        {
            if (_errorsRecords.size() > 0)
            {
                FileWriter fw = new FileWriter(_errorsFileName);
                BufferedWriter out = new BufferedWriter(fw);
                Iterator it = _errorsRecords.iterator();
                while(it.hasNext())
                {
                    String sriNumber = (String) it.next();
                    out.write(sriNumber);
                    out.write(",");
                }
                out.close();
            }
        }
        catch (Exception e)
        {
            logEntry("dumpErrorsToFile", e);
        }
    }

    /**
     * Reads a single line from the report
     * @param in
     * @return a single line
     * @throws Exception
     */
    protected String readLine(BufferedReader in)
            throws Exception
    {
        String    line    = null;
        if ( (line = in.readLine()) != null )
        {
            if ( ++_reportLineCount == 1 )   // first line
            {
                _removeFirstChar = line.startsWith("1");   // check for line #s
            }
            if ( _removeFirstChar )
            {
                line = line.substring(1);
            }
        }
        return( line );
    }

    /**
     * extracts the report
     * @param filename a full file name of the report file
     * @return true if report was extracted or false if not
     */
    public boolean extract(String filename)
    {
        boolean success = false;
        BufferedReader      in                  = null;
        String              line                = null;
        try
        {
            logInfo("Processing report: " + filename );
            in = new BufferedReader( new FileReader( filename ) );
            while( (line = readLine(in)) != null )
            {
                // System.out.println("Main Loop: [" + line + "]");
                success = onExtractOneReport(in, line);
            }
        }
        catch( Exception e )
        {
            logEntry("extract", e);
            success = false;
        }
        finally
        {
            dumpErrorsToFile();
            try
            {
                in.close();
            }
            catch( Exception ee )
            {
                logEntry("extract", ee);
                success = false;
            }
        }
        return success;
    }

    /**
     * stores the row report in the database
     * @param bankNumber bank number
     * @param settlementDate settlement date
     * @param reportId report id
     * @param reportData report data
     * @return true if stored successfully or false if not
     */
    protected boolean storeReport(int bankNumber, String settlementDate, String reportId, String reportData)
    {
        boolean success                 = false;

        if (-1 == bankNumber)
        {
            System.out.println("Can not store report! bankNumber: (" + bankNumber +
                    ") settlementDate: (" + settlementDate +
                    ") reportId: (" + reportId + ")" );
            return false;
        }

        BLOB blobData                   = null;
        OutputStream bOut               = null;
        try
        {
            connect();
            setAutoCommit(false);

            // remove old blob entry
            /*@lineinfo:generated-code*//*@lineinfo:213^13*/

//  ************************************************************
//  #sql [Ctx] { delete  from card_association_reports
//                  where   BANK_NUMBER = :bankNumber
//                  and     SETTLEMENT_DATE = :settlementDate
//                  and     REPORT_ID = :reportId
//                  and     card_type = :_cardTypeAssociation
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  from card_association_reports\n                where   BANK_NUMBER =  :1 \n                and     SETTLEMENT_DATE =  :2 \n                and     REPORT_ID =  :3 \n                and     card_type =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.tools.report.ReportExtractorBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setString(2,settlementDate);
   __sJT_st.setString(3,reportId);
   __sJT_st.setString(4,_cardTypeAssociation);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:220^13*/

            /*@lineinfo:generated-code*//*@lineinfo:222^13*/

//  ************************************************************
//  #sql [Ctx] { insert into card_association_reports
//                      (
//                              bank_number,
//                              settlement_date,
//                              report_id,
//                              card_type,
//                              report_text
//                      )
//                  values (:bankNumber,:settlementDate,:reportId,:_cardTypeAssociation,empty_blob())
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into card_association_reports\n                    (\n                            bank_number,\n                            settlement_date,\n                            report_id,\n                            card_type,\n                            report_text\n                    )\n                values ( :1 , :2 , :3 , :4 ,empty_blob())";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.tools.report.ReportExtractorBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setString(2,settlementDate);
   __sJT_st.setString(3,reportId);
   __sJT_st.setString(4,_cardTypeAssociation);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:233^13*/

            // lock the record and add the blob data
            /*@lineinfo:generated-code*//*@lineinfo:236^13*/

//  ************************************************************
//  #sql [Ctx] { select  report_text 
//                  from    card_association_reports
//                  where   bank_number = :bankNumber
//                  and settlement_date = :settlementDate
//                  and report_id = :reportId
//                  and card_type = :_cardTypeAssociation
//                  for update
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  report_text  \n                from    card_association_reports\n                where   bank_number =  :1 \n                and settlement_date =  :2 \n                and report_id =  :3 \n                and card_type =  :4 \n                for update";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.report.ReportExtractorBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setString(2,settlementDate);
   __sJT_st.setString(3,reportId);
   __sJT_st.setString(4,_cardTypeAssociation);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   blobData = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:245^13*/

            bOut = blobData.getBinaryOutputStream();

            bOut.write( reportData.getBytes(), 0, (reportData.getBytes()).length );

            // flush and close the output buffer
            bOut.flush();
            bOut.close();

            // commit
            /*@lineinfo:generated-code*//*@lineinfo:256^13*/

//  ************************************************************
//  #sql [Ctx] { commit
//               };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:259^13*/

            System.out.println("Stored REPORT: bankNumber: (" + bankNumber +
                    ") settlementDate: (" + settlementDate +
                    ") reportId: (" + reportId + ")" );

            success = true;
        }
        catch( Exception e )
        {
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            e.printStackTrace(pw);
            sw.toString();
            System.out.println(sw);

            success = false;
        }
        finally
        {
            cleanUp();
        }

        return success;
    }


    /**
     * wrapper for conditional extraction of the report.
     * @param in
     * @param line
     * @return
     * @throws Exception
     */
    abstract protected boolean onExtractOneReport(BufferedReader in, String line)  throws Exception;

    /**
     * extracts one report
     * @param in
     * @param line
     * @return true if extracted or false if not
     * @throws Exception
     */
    abstract protected boolean extractOneReport(BufferedReader in, String line) throws Exception;

    /**
     * extracts processing date from the line of text
     * @param idLine
     * @return a processing date
     */
    abstract protected String extract_ProcessingDate(String idLine);

    /**
     * accumulate report
     * @param line
     * @param in
     * @return
     */
    abstract protected boolean accumulateReport(String line, BufferedReader in);

    /**
     * extracts the report id from the line of text
     * @param line
     * @return a report id
     * @throws Exception
     */
    abstract protected String extract_ReportId(String line) throws Exception;

    /**
     * gets the bank number
     * @param number
     * @return
     * @throws Exception
     */
    abstract protected int getBankNumber(String number) throws Exception;
}/*@lineinfo:generated-code*/