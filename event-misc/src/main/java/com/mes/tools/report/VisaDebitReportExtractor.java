/*@lineinfo:filename=VisaDebitReportExtractor*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/tools/VisaDebitReportExtractor.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-12-15 15:19:13 -0800 (Tue, 15 Dec 2009) $
  Version            : $Revision: 16816 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools.report;

import java.io.BufferedReader;
import java.sql.ResultSet;
import java.util.Calendar;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

/**
 * Visa debit report extractor
 */
public class VisaDebitReportExtractor  extends ReportExtractorBase implements IReportExtractor
{
    int _bankNumber = -1;


    /**
     * construction
     */
    public VisaDebitReportExtractor()  throws WrongInputParametersException
    {
        // call parent class 1st
        super("Missing_Visa_SRI_Numbers.txt", ReportExtractorBase.CardType_VS);
    }



    /**
     * extracts one report
     * @param in
     * @param line
     * @return true if extracted or false if not
     * @throws Exception
     */
    protected boolean extractOneReport(BufferedReader in, String line) throws Exception
    {
        boolean success = false;
        StringBuffer        out                 = null;

        try
        {
            success = accumulateReport(line, in);
        }
        catch(Exception e)
        {
            // can not get a valid bank number - skipping!
            logEntry("extractOneReport()",e.toString());
            //@System.out.println(e.getMessage());
            success = false;
        }

        return success;
    }


    /**
     * accumulates report
     * @param line
     * @param in
     * @return true/false
     */
    protected boolean accumulateReport(String line, BufferedReader in)
    {
        StringBuffer out = null;
        try
        {
            String reportId = extract_ReportId(line);
            String idLine = readLine(in);
            _bankNumber = getBankNumber(parse_SRINumber(idLine));
            String date = extract_ProcessingDate(idLine);

            out = new StringBuffer();
            out.append(line);
            out.append("\r\n");
            out.append(idLine);
            out.append("\r\n");
            do
            {
                idLine = readLine(in);
                if(null != idLine)
                {
                    out.append(idLine);
                    out.append("\r\n");
                }
                else
                {
                    System.out.println("read line got nothing!");
                }
            }
            while( idLine.indexOf("END OF " + reportId) < 0 );

            return storeReport(_bankNumber,  date, reportId, out.toString());
        }
        catch(Exception e)
        {
            //@java.io.StringWriter sw = new java.io.StringWriter();
            //@java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            //@e.printStackTrace(pw);
            //@sw.toString();
            //@System.out.println(sw);
            logEntry("accumulateReport()",e.toString());
        }

        return false;
    }


    /**
     * gets the SRI number from the report
     * @param idLine an idLine in the original report
     * @return a SRI number
     * @throws Exception if number is not found
     */
    private String parse_SRINumber(String idLine) throws Exception
    {
        String sriNumber = "";
        int offset = 0;
        if ( (offset = idLine.indexOf("REPORTING FOR: ")) >= 0 )
        {
            sriNumber = idLine.substring(offset+20,offset+30);
        }
        else
        {
            throw new Exception("Can not parse SRI number!");
        }

        return sriNumber;
    }

    /**
     * gets the processing date from the report
     * @param idLine
     * @return
     */
    protected String extract_ProcessingDate(String idLine)
    {
        String processingDate = "";
        int     offset    = 0;
        if ( (offset = idLine.indexOf("PROC DATE")) >= 0 )
        {
            processingDate = idLine.substring(offset+13,offset+20);
        }
        else
        {
            processingDate = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"ddMMMyy");
        }

        // System.out.println("processingDate = " + processingDate);

        return processingDate;
    }

    /**
     * gets the report id from the report
     * @param line
     * @return reportID
     */
    protected String extract_ReportId(String line) throws Exception
    {
        String reportId = "";

        int offset_reportId = 0;
        int temp_offset = 0;
        if ( (temp_offset = offset_reportId = line.indexOf("REPORT ID: ")) >= 0 )
        {
            reportId = line.substring(offset_reportId+12,offset_reportId+20);
            if(reportId.endsWith("-"))
            {
                offset_reportId = temp_offset;
                // this means that we have report ID longer that
                // we have expected, therefore read more!
                reportId = line.substring(offset_reportId+12,offset_reportId+21);
            }
        }

        reportId = reportId.trim();

        if("".compareTo(reportId) == 0)
        {
            throw  new Exception("Can not parse Report ID!");
        }

        return reportId;
    }

    /**
    * gets the bank number for a given SRI number
    * @param SRI_Number
    * @return a valid bank number
    * @throws Exception if no matching bank number is found
    */
    protected int getBankNumber(String SRI_Number) throws Exception
    {
        int bankNumber = -1;
        ResultSetIterator it    = null;
        ResultSet rs    = null;

        try
        {
            connect();

            /*@lineinfo:generated-code*//*@lineinfo:227^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  bank_number
//                          from    mbs_banks
//                          where   vs_sre_number = :SRI_Number
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bank_number\n                        from    mbs_banks\n                        where   vs_sre_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.report.VisaDebitReportExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,SRI_Number);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.report.VisaDebitReportExtractor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:232^17*/
            rs = it.getResultSet();
            while(rs.next())
            {
                bankNumber = rs.getInt("bank_number");
                break;  // assuming the bank number is unique!
            }

            rs.close();
            it.close();
        }
        catch(Exception e)
        {
            //@java.io.StringWriter sw = new java.io.StringWriter();
            //@java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            //@e.printStackTrace(pw);
            //@sw.toString();
            //@System.out.println(sw);
            logEntry("getBankNumber(" + SRI_Number + ")", e);
        }
        finally
        {
            try { rs.close(); } catch(Exception e) {}
            try { it.close(); } catch(Exception e) {}
            cleanUp();
        }

        if ( -1 == bankNumber )
        {
            // Add an element to the collection of sri numbers.
            // Note: it may be duplicates, but we ignore then.
            addErrorInformation(SRI_Number);

            // report an error
            throw new MissingReferenceNumberException( "ERROR: The Bank Number is missing (in mbs_banks table) for a given SRI Number (" + SRI_Number + ")." );
        }

        return bankNumber;
    }

    /**
     * wrapper for conditional extraction of the report.
     * @param in
     * @param line
     * @return
     * @throws Exception
     */
    protected boolean onExtractOneReport(BufferedReader in, String line) throws Exception
    {
        boolean success = false;
        if ( line.startsWith("REPORT ID:  ") )
        {
            success = extractOneReport(in, line);
        }
        return success;
    }

    /**
     * test Main method
     * @param args
     */
    public static void main( String[] args )
    {
        VisaDebitReportExtractor    widget  = null;
        try
        {
            widget = new VisaDebitReportExtractor();
            if (0==args.length)
            {
                System.out.println("Usage: report file name");
                System.exit(0);
            }
            widget.extract(args[0]);
            // widget.extract("C:\\Users\\spushnof\\Desktop\\report\\visa\\vss3941_051412_042828.txt");
        }
        catch( Exception e )
        {
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw);
            e.printStackTrace(pw);
            sw.toString();
            System.out.println(sw);
        }
        finally
        {
        }

        System.exit(0);
    }
}/*@lineinfo:generated-code*/