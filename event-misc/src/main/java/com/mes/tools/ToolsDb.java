/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/tools/ToolsDb.java $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-10-22 11:20:33 -0700 (Thu, 22 Oct 2009) $
  Version            : $Revision: 16621 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.mes.database.SQLJConnectionBase;

public class ToolsDb extends SQLJConnectionBase
{
  public static long loadFilenameToLoadFileId( String loadFilename )
  {
    return( new ToolsDb()._loadFilenameToLoadFileId(loadFilename) );
  }
  
  public long _loadFilenameToLoadFileId( String loadFilename )
  {
    CallableStatement   cs              = null;
    long                loadFileId      = 0L;
    PreparedStatement   ps              = null;

    try
    {
      connect(true);
      
      cs = getCallableStatement("{ call load_file_index_init(?) }");
      cs.setString( 1, loadFilename );
      cs.execute();
      
      String sqlText = 
        " select  load_filename_to_load_file_id(?)  as load_file_id   " + 
        " from    dual                                                ";
        
      ps = getPreparedStatement(sqlText);
      ps.setString(1,loadFilename);
      ResultSet rs = ps.executeQuery();
      rs.next();
      loadFileId = rs.getLong("load_file_id");
      rs.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry("_loadFilenameToLoadFileId( " + loadFilename + " )", e.toString() );
    }
    finally
    {
      try{ cs.close(); } catch( Exception ee ){}
      try{ ps.close(); } catch( Exception ee ){}
      cleanUp();
    }
    return( loadFileId );
  }
}