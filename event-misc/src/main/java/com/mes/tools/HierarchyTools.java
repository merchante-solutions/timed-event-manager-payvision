/*@lineinfo:filename=HierarchyTools*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/HierarchyTools.sqlj $

  Description:
  
  THierarchyTools

  Routines used to maintain t_hierarchy set of tables.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-02-06 11:17:09 -0800 (Tue, 06 Feb 2007) $
  Version            : $Revision: 13414 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.tools;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
import com.mes.constants.MesHierarchy;
import sqlj.runtime.ResultSetIterator;

public class HierarchyTools extends com.mes.database.SQLJConnectionBase
{
  // entity types
  private static final int  ET_HIER_ROOT  = 0;
  private static final int  ET_HIER_NODE  = 1;
  private static final int  ET_SALES_REP  = 2;
  private static final int  ET_MERCHANT   = 3;
  private static final int  ET_ASSOC      = 4;
  private static final int  ET_GROUP      = 5;
  
  private int hierarchyType;
  private int errorOrder = 0;
  
  /*
  ** private class Node
  **
  ** Describes a hierarchy node: id, name, type.
  */
  private class Node
  {
    long    id;
    String  name;
    int     type;
  
    public Node(long id, String name, int type)
    {
      this.id   = id;
      this.name = (name != null && !name.equals("") ? name : null);
      this.type = type;
    }
    
    public Node(long id, int type)
    {
      this.id   = id;
      this.type = type;
    }
    
    public long getId()
    {
      return id;
    }
    public String getName()
    {
      String retVal = "";
      
      if (name != null)
      {
        return name;
      }
      else
      {
        try
        {
          retVal = Long.toString(id);
        }
        catch (Exception e) { }
      }
      
      return retVal;
    }
    public int getType()
    {
      return type;
    }
  }
  
  /*
  ** private HierarchyTools(int hierarchyType)
  **
  ** CONSTRUCTOR
  **
  ** This class has a private constructor, which means that the class can
  ** only be used via it's public static methods.
  */
  private HierarchyTools(int hierarchyType)
  {
    super(false);
    this.hierarchyType = hierarchyType;
  }
  
  /*
  ** private int errorOrder()
  **
  ** Increments the current error order number and then returns it.
  **
  ** RETURNS: incremented error order.
  */
  private int errorOrder()
  {
    return ++errorOrder;
  }

  /*
  ** private void log(String message, long merchant, long assoc)
  **
  ** Logs an entry into the hierarchy_log table.
  */
  private void log(String message, long merchant, long assoc)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:140^7*/

//  ************************************************************
//  #sql [Ctx] { insert into t_hierarchy_log
//            ( log_date,
//              log_message,
//              log_merchant,
//              log_association)
//          values
//            ( sysdate,
//              :message,
//              :merchant,
//              :assoc )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into t_hierarchy_log\n          ( log_date,\n            log_message,\n            log_merchant,\n            log_association)\n        values\n          ( sysdate,\n             :1 ,\n             :2 ,\n             :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.tools.HierarchyTools",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,message);
   __sJT_st.setLong(2,merchant);
   __sJT_st.setLong(3,assoc);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:152^7*/
    }
    catch(Exception e)
    {
      logEntry(errorOrder(), "HierarchyTools.log()", e.toString());
    }
  }
  
  private void debug(String message)
  {
    try
    {
      int nextSeq = 0;
      /*@lineinfo:generated-code*//*@lineinfo:165^7*/

//  ************************************************************
//  #sql { select  debug_sequence.nextval 
//           
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = sqlj.runtime.ref.DefaultContext.getDefaultContext(); if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  debug_sequence.nextval \n          \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.HierarchyTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   nextSeq = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:170^7*/
    
      /*@lineinfo:generated-code*//*@lineinfo:172^7*/

//  ************************************************************
//  #sql { insert into t_debug_log
//            ( run_seq,
//              log_date,
//              log_message )
//          values
//            ( :nextSeq,
//              sysdate,
//              :message )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = sqlj.runtime.ref.DefaultContext.getDefaultContext(); if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into t_debug_log\n          ( run_seq,\n            log_date,\n            log_message )\n        values\n          (  :1 ,\n            sysdate,\n             :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.tools.HierarchyTools",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,nextSeq);
   __sJT_st.setString(2,message);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:182^7*/
    }
    catch (Exception e) { }
  }
  
  private void resetView()
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:193^7*/

//  ************************************************************
//  #sql [Ctx] { update  view_hierarchy_refresh
//          set     need_refresh = 'Y'
//          where   id = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  view_hierarchy_refresh\n        set     need_refresh = 'Y'\n        where   id = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.tools.HierarchyTools",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:198^7*/
      
      commit();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.tools.HierarchyTools::resetView()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** private void deleteNode(long nodeId)
  **   throws Exception
  **
  ** Deletes a node and all the node's descendents from the node's ancestors.
  ** Leaves the descendents of the node linked to the node itself.
  **
  **            1         1    node 2 is deleted, so 2, 3 and 4 are no
  **           /               longer descendents of 1, but 3 and 4 are
  **          2    =>  (2)     still descendents of 2 even though 2 itself
  **         /\        /\      is removed (as in 2 is no longer its own 
  **        3 4       3 4      ancestor).
  **
  ** { 110,121,132,142,220,231,241,330,440 } => { 110,231,241,330,440 }
  */
  private void deleteNode(long nodeId)
    throws Exception
  {
    try
    {
      // delete all descendents of node
      // from ancestors of node
      /*@lineinfo:generated-code*//*@lineinfo:234^7*/

//  ************************************************************
//  #sql [Ctx] { delete from t_hierarchy
//          where descendent in ( select  descendent
//                                from    t_hierarchy
//                                where   ancestor = :nodeId
//                                        and hier_type = :hierarchyType )
//                and ancestor in ( select  ancestor
//                                  from    t_hierarchy
//                                  where   descendent = :nodeId
//                                          and relation > 0
//                                          and hier_type = :hierarchyType )
//                and hier_type = :hierarchyType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from t_hierarchy\n        where descendent in ( select  descendent\n                              from    t_hierarchy\n                              where   ancestor =  :1 \n                                      and hier_type =  :2  )\n              and ancestor in ( select  ancestor\n                                from    t_hierarchy\n                                where   descendent =  :3 \n                                        and relation > 0\n                                        and hier_type =  :4  )\n              and hier_type =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.tools.HierarchyTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,hierarchyType);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setInt(4,hierarchyType);
   __sJT_st.setInt(5,hierarchyType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:247^7*/
      
      // delete the node itself
      /*@lineinfo:generated-code*//*@lineinfo:250^7*/

//  ************************************************************
//  #sql [Ctx] { delete from t_hierarchy
//          where descendent = :nodeId
//                and ancestor = :nodeId
//                and hier_type = :hierarchyType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from t_hierarchy\n        where descendent =  :1 \n              and ancestor =  :2 \n              and hier_type =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.tools.HierarchyTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setInt(3,hierarchyType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:256^7*/
      
      // remove the names record
      /*@lineinfo:generated-code*//*@lineinfo:259^7*/

//  ************************************************************
//  #sql [Ctx] { delete from t_hierarchy_names
//          where hier_id = :nodeId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from t_hierarchy_names\n        where hier_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.tools.HierarchyTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:263^7*/
    }
    catch (Exception e)
    {
      debugLog("deleteNode(): " + e.toString());
      logEntry(errorOrder(),"deleteNode(" + nodeId + ") hierarchyType = " 
        + hierarchyType, e.toString());
      throw new Exception("deleteNode() failure");
    }
  }

  /*
  ** private Node getRootNode() throws Exception
  **
  ** Builds a node for the root of the current hierarchy type.
  **
  ** RETURNS: Node containing the hierarchy root.
  */
  private Node getRootNode() throws Exception
  {
    Node rootNode = null;
    
    try
    {
      long rootId = 0L;
      String rootName = "";
      /*@lineinfo:generated-code*//*@lineinfo:289^7*/

//  ************************************************************
//  #sql [Ctx] { select  root_id, 
//                  name
//          
//          from    t_hierarchy_types
//          where   hier_type = :hierarchyType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  root_id, \n                name\n         \n        from    t_hierarchy_types\n        where   hier_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.tools.HierarchyTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,hierarchyType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rootId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   rootName = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:297^7*/
      
      rootNode = new Node(rootId,rootName,ET_HIER_ROOT);
    }
    catch (Exception e)
    {
      logEntry(errorOrder(),"HierarchyTools.getRootNode() hierarchyType = " 
        + hierarchyType,e.toString());
      throw new Exception("getRootNode() failure");
    }
    
    return rootNode;
  }
  
  /*
  ** private Vector buildNodeBranch(long   nodeId,
  **                                int    nodeType,
  **                                String nodeName,
  **                                long   parentId)
  **
  ** Builds a Vector of Node's starting with the new hierarchy node defined
  ** by the node parameters.  Based on the parent node id, the ancestors are
  ** added to the Vector, starting with the immediate parent of the new node
  ** and ending with the highest existing ancestor node.  When the parent id
  ** is given as zero the parent is assumed to be the hierarchy root.
  **
  ** RETURNS: Vector of Node objects, new node at index 0, root at end.
  */
  private Vector buildNodeBranch(long   nodeId,
                                 int    nodeType,
                                 String nodeName,
                                 long   parentId)
    throws Exception
  {
    Vector nodeBranch = new Vector();
    
    try
    {
      // create the new node, add it to the branch
      nodeBranch.add(new Node(nodeId,nodeName,nodeType));
      
      // if parent is specified, attempt to build node branch
      // from parent
      if (parentId > 0)
      {
        // get all ancestors of new node using parent id
        // order by relation
        ResultSetIterator it = null;
        /*@lineinfo:generated-code*//*@lineinfo:345^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    ancestor,
//                      entity_type
//            from      t_hierarchy
//            where     descendent = :parentId
//                      and hier_type = :hierarchyType
//            order by  relation
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ancestor,\n                    entity_type\n          from      t_hierarchy\n          where     descendent =  :1 \n                    and hier_type =  :2 \n          order by  relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.tools.HierarchyTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,parentId);
   __sJT_st.setInt(2,hierarchyType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.tools.HierarchyTools",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:353^9*/
    
        // load nodes into branch from direct parent to root
        ResultSet rs = it.getResultSet();
        long lastNodeId = 0L;
        while(rs.next())
        {
          lastNodeId = rs.getLong("ancestor");
          nodeBranch.add(new Node(lastNodeId,rs.getInt("entity_type")));
        }
      
        // close the iterator so cursors don't get consumed
        it.close();

        // if no parent currently exists, create one
        if (lastNodeId == 0L)
        {
          // create the parent node, add it to the branch
          nodeBranch.add(new Node(parentId,nodeType));
        }
      }
      // else no parent, link node directly to root
      else
      {
        nodeBranch.add(getRootNode());
      }
    }
    catch (Exception e)
    {
      logEntry(errorOrder(),"HierarchyTools.buildNodeBranch()",
        e.toString());
      throw new Exception("buildNodeBranch() failure");
    }
    
    return nodeBranch;
  }

  /*
  ** private void addNodeBranch(Vector nodeBranch)
  **
  ** Updates the hierarchy table to contain the relationships implied by the
  ** node branch.  This routine takes into account the possibility of children
  ** already existing for the new node being added and will propogate those
  ** descendent relationships up through the branch.
  */  
  private void addNodeBranch(Vector nodeBranch) throws Exception
  {
    try
    {
      // add the node relationships defined by the branch
      Node dNode = (Node)nodeBranch.get(0);
      long descendent = dNode.getId();
      int dType = dNode.getType();
      for (int aIdx = 0; aIdx < nodeBranch.size(); ++aIdx)
      {
        long ancestor = ((Node)nodeBranch.get(aIdx)).getId();
        /*@lineinfo:generated-code*//*@lineinfo:409^9*/

//  ************************************************************
//  #sql [Ctx] { insert into t_hierarchy
//              ( ancestor,
//                descendent,
//                relation,
//                entity_type,
//                hier_type )
//            values
//              ( :ancestor,
//                :descendent,
//                :aIdx,
//                :dType,
//                :hierarchyType )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into t_hierarchy\n            ( ancestor,\n              descendent,\n              relation,\n              entity_type,\n              hier_type )\n          values\n            (  :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.tools.HierarchyTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ancestor);
   __sJT_st.setLong(2,descendent);
   __sJT_st.setInt(3,aIdx);
   __sJT_st.setInt(4,dType);
   __sJT_st.setInt(5,hierarchyType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:423^9*/
        debug("branch add: anc=" + ancestor + " desc=" + descendent);
      }
      
      
      // find all descendents of the new node
      Node newNode = (Node)nodeBranch.get(0);
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:431^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  descendent,
//                  relation,
//                  entity_type
//          from    t_hierarchy
//          where   ancestor = :newNode.getId()
//                  and relation > 0
//                  and hier_type = :hierarchyType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_64 = newNode.getId();
  try {
   String theSqlTS = "select  descendent,\n                relation,\n                entity_type\n        from    t_hierarchy\n        where   ancestor =  :1 \n                and relation > 0\n                and hier_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.tools.HierarchyTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_64);
   __sJT_st.setInt(2,hierarchyType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.tools.HierarchyTools",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:440^7*/
      
      // for each descendent found, add it as a descendent of each
      // ancestor of the new node in the node branch
      ResultSet rs = it.getResultSet();
      while(rs.next())
      {
        // get the descendent data
        descendent    = rs.getLong("descendent");
        dType         = rs.getInt ("entity_type");
        int relation  = rs.getInt ("relation");
        
        // iterate up the ancestors in branch (skip self)
        // and add each descendent of new node to new node's
        // own ancestors
        Iterator i = nodeBranch.iterator();
        i.next();
        int nodeIdx = 1;
        while (i.hasNext())
        {
          long ancestor = ((Node)i.next()).getId();
          /*@lineinfo:generated-code*//*@lineinfo:461^11*/

//  ************************************************************
//  #sql [Ctx] { insert into t_hierarchy
//                ( ancestor,
//                  descendent,
//                  relation,
//                  entity_type,
//                  hier_type )
//              values
//                ( :ancestor,
//                  :descendent,
//                  :relation + nodeIdx,
//                  :dType,
//                  :hierarchyType )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_65 = relation + nodeIdx;
   String theSqlTS = "insert into t_hierarchy\n              ( ancestor,\n                descendent,\n                relation,\n                entity_type,\n                hier_type )\n            values\n              (  :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.tools.HierarchyTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ancestor);
   __sJT_st.setLong(2,descendent);
   __sJT_st.setInt(3,__sJT_65);
   __sJT_st.setInt(4,dType);
   __sJT_st.setInt(5,hierarchyType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:475^11*/
          debug("desc add: anc=" + ancestor + " desc=" + descendent);
          ++nodeIdx;
        }
      }
      
      // close the iterator so cursors don't get consumed
      it.close();
      
      // add the new node to t_hierarchy_names
      /*@lineinfo:generated-code*//*@lineinfo:485^7*/

//  ************************************************************
//  #sql [Ctx] { insert into t_hierarchy_names
//            ( hier_id,
//              hier_type,
//              entity_type,
//              name )
//          values
//            ( :newNode.getId(),
//              :hierarchyType,
//              :newNode.getType(),
//              :newNode.getName() )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_66 = newNode.getId();
 int __sJT_67 = newNode.getType();
 String __sJT_68 = newNode.getName();
   String theSqlTS = "insert into t_hierarchy_names\n          ( hier_id,\n            hier_type,\n            entity_type,\n            name )\n        values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.tools.HierarchyTools",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_66);
   __sJT_st.setInt(2,hierarchyType);
   __sJT_st.setInt(3,__sJT_67);
   __sJT_st.setString(4,__sJT_68);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:497^7*/
    }
    catch (Exception e)
    {
      logEntry(errorOrder(),"HierarchyTools.addNodeBranch()",
        e.toString());
      throw new Exception("addNodeBranch() failure");
    }
  }
  
  /*
  ** public long createNodeId(int hierarchyType) throws Exception
  **
  ** Creates a new node using the t_hid_sequence and the hierarchy type.
  **
  ** RETURNS: the new node id.
  */
  public long createNodeId(int hierarchyType) throws Exception
  {
    long newNodeId = -1L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:520^7*/

//  ************************************************************
//  #sql [Ctx] { select  t_hid_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  t_hid_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.tools.HierarchyTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   newNodeId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:525^7*/
      newNodeId += hierarchyType * 100000000L;
    }
    catch (Exception e)
    {
      logEntry(errorOrder(),"HierarchyTools.createNodeId()",
        e.toString());
      throw new Exception("createNodeId() failure");
    }
    
    return newNodeId;
  }
  
  /*
  ** private void insertNode(long    newNodeId,
  **                         int     nodeType,
  **                         String  nodeName,
  **                         long    parentId)
  **   throws Exception
  **
  ** Inserts a node into the hierarchy.
  */
  private void insertNode(long    newNodeId,
                          int     nodeType,
                          String  nodeName,
                          long    parentId)
    throws Exception
  {
    try
    {
      addNodeBranch(buildNodeBranch(newNodeId,nodeType,nodeName,parentId));
    }
    catch (Exception e)
    {
      logEntry(errorOrder(),"HierarchyTools.insertNode()",
        e.toString());
      throw new Exception("insertNode() failure");
    }
  }

  /*
  ** PUBLIC STATIC METHODS
  */


  private static void debugLog(String message)
  {
    HierarchyTools ht = new HierarchyTools(0);
    try
    {
      ht.connect();
      ht.debug(message);
      ht.commit();
    }
    catch (Exception e) { }
    finally
    {
      ht.cleanUp();
    }
  }
  
  public static long getNewNodeId(int hierarchyType)
  {
    long newNodeId = -1L;
    HierarchyTools ht = new HierarchyTools(hierarchyType);
    try
    {
      ht.connect();
      newNodeId = ht.createNodeId(hierarchyType);
      ht.commit();
    }
    catch (Exception e) { }
    finally
    {
      ht.cleanUp();
    }
    return newNodeId;
  }

  /*
  ** public static void logHierarchy(String message, long merchant, long assoc)
  **
  ** Logs an entry into t_hierarchy_log.
  */
  public static void logHierarchy(String message, long merchant, long assoc)
  {
    HierarchyTools ht = new HierarchyTools(0);
    try
    {
      ht.connect();
      ht.log(message,merchant,assoc);
      ht.commit();
    }
    catch (Exception e) { }
    finally
    {
      ht.cleanUp();
    }
  }

  /*
  ** public static void deleteNode(long nodeId, int hierarchyType)
  **
  ** Deletes a node from the hierarchy.
  */
  public static void deleteNode(long nodeId, int hierarchyType)
  {
    HierarchyTools ht = new HierarchyTools(hierarchyType);
    try
    {
      ht.connect();
      ht.deleteNode(nodeId);
      ht.commit();
      ht.log("deleteNode() successful",nodeId,0L);
    }
    catch (Exception e)
    {
      ht.rollback();
      ht.logEntry(ht.errorOrder(),"HierarchyTools.deleteNode()",e.toString());
      ht.log(e.toString(),nodeId,0L);
    }
    finally
    {
      ht.cleanUp();
    }
  }
  
  /* public static void insertNode(long    newNodeId,
  **                               int     nodeType,
  **                               String  nodeName,
  **                               long    parentId,
  **                               long    hierarchyType)
  **
  ** Inserts a node into the hierarchy.
  */                              
  public static void insertNode(long    newNodeId,
                                int     nodeType,
                                String  nodeName,
                                long    parentId,
                                int     hierarchyType)
  {
    HierarchyTools ht = new HierarchyTools(hierarchyType);
    try
    {
      ht.connect();
      if (newNodeId < 0)
      {
        newNodeId = ht.createNodeId(hierarchyType);
      }
      ht.deleteNode(newNodeId);
      ht.insertNode(newNodeId,nodeType,nodeName,parentId);
      ht.commit();
      ht.log("insertNode() successful",newNodeId,parentId);
    }
    catch (Exception e)
    {
      ht.rollback();
      ht.logEntry(ht.errorOrder(),"HierarchyTools.insertNode()",e.toString());
      ht.log(e.toString(),newNodeId,parentId);
    }
    finally
    {
      ht.cleanUp();
    }
  }                                

  /*
  ** public static moveNode(long   newNodeId,
  **                        long   oldNodeId,
  **                        int    nodeType,
  **                        String nodeName,
  **                        long   parentId,
  **                        int    hierarchyType)
  **
  ** Moves a node within the hierarchy from one parent to another.
  */
  public static void moveNode(long   newNodeId,
                              long   oldNodeId,
                              int    nodeType,
                              String nodeName,
                              long   parentId,
                              int    hierarchyType)
  {
    HierarchyTools ht = new HierarchyTools(hierarchyType);
    try
    {                               
      ht.connect();
      ht.deleteNode(oldNodeId);
      ht.deleteNode(newNodeId);
      ht.insertNode(newNodeId,nodeType,nodeName,parentId);
      ht.commit();
      ht.log("moveNode() successful",newNodeId,parentId);
    }
    catch (Exception e)
    {
      ht.rollback();
      debugLog("moveNode(): rolled back");
      debugLog("moveNode(): " + e.toString());
      ht.logEntry(ht.errorOrder(),"HierarchyTools.moveNode()",e.toString());
      ht.log(e.toString(),newNodeId,parentId);
    }
    finally
    {
      ht.cleanUp();
    }
  }
  
  public static void resetViewHierarchy()
  {
    HierarchyTools ht = new HierarchyTools(MesHierarchy.HT_BANK_PORTFOLIOS);
    
    try
    {
      ht.resetView();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.tools.HierarchyTools::resetViewHierarchy()", e.toString());
    }
  }
}/*@lineinfo:generated-code*/