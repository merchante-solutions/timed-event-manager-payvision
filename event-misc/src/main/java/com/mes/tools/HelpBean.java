/*@lineinfo:filename=HelpBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/HelpBean.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 9/24/02 2:19p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database         

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class HelpBean extends SQLJConnectionBase
{
  /*
  ** CONSTANTS
  */
  public static final int     HELP_CONTROL                    = 1;
  public static final int     HELP_BUSINFO                    = 2;
  public static final int     HELP_MERCHHISTORY               = 3;
  public static final int     HELP_BUSCHECKING                = 4;
  public static final int     HELP_PRIMEOWNER                 = 5;
  public static final int     HELP_SECOWNER                   = 6;
  public static final int     HELP_TRANSINFO                  = 7;
  public static final int     HELP_PAYOPTIONS                 = 8;
  public static final int     HELP_PRODSELECTION              = 9;
  public static final int     HELP_EQUIPNEEDED                = 10;
  public static final int     HELP_EQUIPOWNED                 = 11;
  public static final int     HELP_EQUIPFEATURES              = 12;
  public static final int     HELP_EQUIPFEATURESVFI           = 13;
  public static final int     HELP_VISAMCRATES                = 14;
  public static final int     HELP_OTHERPAYMENT               = 15;
  public static final int     HELP_MISCFEES                   = 16;
  public static final int     HELP_EQUIP                      = 17;
  public static final int     HELP_SITEINFO                   = 18;
  public static final int     HELP_SITEINFOEC                 = 19;
  public static final int     HELP_VSBUSINFO                  = 20;
  public static final int     HELP_VSBUSCHECKING              = 21;
  public static final int     HELP_VSPAYOPTIONS               = 22;
  public static final int     HELP_VSTRANSINFO                = 23;
  public static final int     HELP_VSCONTROL                  = 24;
  public static final int     HELP_VSCONTROL2                 = 25;
  public static final int     HELP_CBT_VISAMCRATES            = 26;
  public static final int     HELP_NOREF_BUSCHECKING          = 27;
  public static final int     HELP_SVB_PRODSELECTION          = 28;
  public static final int     HELP_SVB_VISAMCRATES            = 29;
  public static final int     HELP_OCB_VISAMCRATES            = 30;
  public static final int     HELP_SVB_BUSINFO                = 31;
  public static final int     HELP_CBT_MISCFEES               = 32;
  public static final int     HELP_VSBUSINESSINFO             = 33;
  public static final int     HELP_VSBUSCONTACT               = 34;
  public static final int     HELP_VSBUSPREMISES              = 35;
  public static final int     HELP_VSPRIMARYOWNER             = 36;
  public static final int     HELP_VSSECONDARYOWNER           = 37;
  public static final int     HELP_VSBUSINESSCHECKING         = 38;
  public static final int     HELP_VSMERCHANTHISTORY          = 39;
  public static final int     HELP_VSTRANSACTIONINFO          = 40;
  public static final int     HELP_VSPAYMENTOPTIONS           = 41;
  public static final int     HELP_VSCOMMENTS                 = 42;
  public static final int     HELP_SHIPPINGADDRESS            = 43;
  public static final int     HELP_EQUIPNEEDED_DIAL           = 44;
  public static final int     HELP_EQUIPOWNED_DIAL            = 45;
  public static final int     HELP_PRICINGCOMMENTS            = 46;
  public static final int     HELP_VSGENERAL                  = 47;
  public static final int     HELP_CBT_OTHERPAYMENT           = 48;
  public static final int     HELP_CBT_PINPADPAYMENT          = 49;
  public static final int     HELP_CBT_INTERCHANGEFEE         = 50;
  public static final int     HELP_PROF_UNDER_WATER           = 51;
  public static final int     HELP_VISAMCRATESDIS             = 52;
  public static final int     HELP_RECONCILE                  = 53;
  public static final int     HELP_DEPOSITSUMMARY             = 54;
  public static final int     HELP_TRANSUMMARY                = 55;
  public static final int     HELP_TRANDETAIL                 = 56;
  public static final int     HELP_INTERCHANGESUMMARY         = 57;
  public static final int     HELP_ADJUSTMENTSUMMARY          = 58;
  public static final int     HELP_REJECTS                    = 59;
  public static final int     HELP_MONTHLYSTATEMENTS          = 60;
  public static final int     HELP_INACTIVE_MERCHANTS         = 61;
  public static final int     HELP_ADJUSTMENTS                = 62;
  public static final int     HELP_AUTHORIZATIONS             = 63;
  public static final int     HELP_AUTHORIZATIONSUMMARY       = 64;
  public static final int     HELP_BATCH_DETAIL               = 65;
  public static final int     HELP_BATCH_SUMMARY              = 66;
  public static final int     HELP_BATCH_SUMMARY_CHAIN        = 67;
  public static final int     HELP_CHARGEBACK_ADJUSTMENTS     = 68;
  public static final int     HELP_CHARGEBACK_SUMMARY         = 69;
  public static final int     HELP_DEPOSIT_DETAIL             = 70;
  public static final int     HELP_DOWNGRADE_DETAIL           = 71;
  public static final int     HELP_DOWNGRADE_SUMMARY          = 72;
  public static final int     HELP_INTERCHANGE_DETAILS        = 73;
  public static final int     HELP_INTERCHANGE_SUMMARY        = 74;
  public static final int     HELP_MONTHLY_STATEMENT_SUMMARY  = 75;
  public static final int     HELP_MONTHLY_STATEMENT          = 76;
  public static final int     HELP_RECONCILE_SUMMARY          = 77;
  public static final int     HELP_REJECT_SUMMARY             = 78;
  public static final int     HELP_RETRIEVALS                 = 79;
  public static final int     HELP_RETRIEVAL_SUMMARY          = 80;
  public static final int     HELP_TRAN_DETAIL_SUMMARY        = 81;
  public static final int     HELP_UNMATCHED_CREDITS          = 82;

  /*
  ** FIELDS
  */
  private String                description       = "Unable to locate help screen for specified help ID";
  private String                helpDataInclude   = "";
  private String                title             = "Invalid Help ID";
  
  /*
  ** METHOD getData
  */
  public void getData(int helpId)
  {
    ResultSetIterator it    = null;
    ResultSet         rs    = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:143^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  hsi.title                   as title,
//                  hsi.description             as description,
//                  hsi.data                    as data_url
//          from    help_screen_info    hsi
//          where   hsi.help_id = :helpId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  hsi.title                   as title,\n                hsi.description             as description,\n                hsi.data                    as data_url\n        from    help_screen_info    hsi\n        where   hsi.help_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.HelpBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,helpId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.HelpBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:150^7*/
      
      rs = it.getResultSet();
      
      if (rs.next())
      {
        title           = rs.getString("title");
        description     = rs.getString("description");
        helpDataInclude = rs.getString("data_url");
      }
      it.close();
    }
    catch (Exception e)
    {
      logEntry("getData(" + helpId + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** ACCESSORS
  */
  public String getTitle()
  {
    return( title );
  }
  public String getDescription()
  {
    return( description );
  }
  public String getHelpDataInclude()
  {
    return( helpDataInclude );
  }
}/*@lineinfo:generated-code*/