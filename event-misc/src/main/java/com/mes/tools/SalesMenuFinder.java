/*@lineinfo:filename=SalesMenuFinder*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/SalesMenuFinder.sqlj $

  Description:
  
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/10/03 11:48a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class SalesMenuFinder extends SQLJConnectionBase
{
  public SalesMenuFinder()
  {
  }
  
  public String getSalesMenu(long hierarchyNode)
  {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    
    String              url       = "";
    
    try
    {
      connect();
      
      // find closest hierarchy parent in sales_menu_hierarchy
      /*@lineinfo:generated-code*//*@lineinfo:48^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  smh.url
//          from    sales_menu_hierarchy  smh,
//                  t_hierarchy           th
//          where   th.hier_type = 1 and
//                  th.descendent = :hierarchyNode and
//                  th.ancestor = smh.hierarchy_node
//          order by relation asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  smh.url\n        from    sales_menu_hierarchy  smh,\n                t_hierarchy           th\n        where   th.hier_type = 1 and\n                th.descendent =  :1  and\n                th.ancestor = smh.hierarchy_node\n        order by relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.SalesMenuFinder",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.SalesMenuFinder",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:57^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        url = rs.getString("url");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getSalesMenu(" + hierarchyNode + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return url;
  }
  
  public static String getSalesURL(long hierarchyNode)
  {
    SalesMenuFinder sf = new SalesMenuFinder();
    
    return sf.getSalesMenu(hierarchyNode);
  }
}/*@lineinfo:generated-code*/