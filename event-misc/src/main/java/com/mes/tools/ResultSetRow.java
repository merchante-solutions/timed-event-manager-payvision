/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/ZipField.sqlj $

  Description:
  
  ZipField
  
  A field containing a zip code.  Validates zip code against 
  rapp_app_valid_zipcodes.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.StringTokenizer;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;

public class ResultSetRow
{
  protected   int                   diffCount         = 0;
  protected   int                   columnCount       = 0;
  
  protected   Vector                columnNames       = null;
  protected   Vector                columnData        = null;

  static      Logger                log         = Logger.getLogger(ResultSetRow.class);  
  
  public ResultSetRow()
  {
  }
  
  public ResultSetRow(ResultSet rs)
  {
    try
    {
      ResultSetMetaData metaData = rs.getMetaData();
      
      columnCount = metaData.getColumnCount();
      
      columnNames = new Vector();
      columnData  = new Vector();
      
      for(int i=1; i<=columnCount; ++i)
      {
        // add column name
        columnNames.add(metaData.getColumnName(i));
        
        Object  obj   = rs.getObject(i);
        
        if(obj == null)
        {
          columnData.add("");
        }
        else
        {
          columnData.add(obj.toString());
        }
      }
    }
    catch(Exception e)
    {
      log.error(e.toString());
      com.mes.support.SyncLog.LogEntry("ResultSetRow()", e.toString());
    }
  }
  
  public String diff(ResultSetRow old)
  {
    StringBuffer  result            = new StringBuffer("");
    boolean       foundDifferences  = false;
    
    try
    { 
      if(old.getColumnCount() != getColumnCount())
      {
        throw new Exception("Rows have different number of columns");
      }
      
      result.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");
      
      for(int i=0; i<getColumnCount(); ++i)
      {
        if( !( ((String)(colData(i))).equals((String)(old.colData(i))) ) )
        {
          foundDifferences = true;
          result.append("<tr>");
          
          result.append("<td class=\"tableDataStrong\">");
          result.append(getPrettyColumnName(getColumnName(i)));
          result.append("</td>");
          
          result.append("<td class=\"tableData\">");
          result.append(" old: <font color=\"red\">" + (String)(old.colData(i)));
          result.append("</font>");
          result.append("</td>");
          
          result.append("<td class=\"tableData\">");
          result.append(" new: <font color=\"green\">" + (String)(colData(i)));
          result.append("</font>");
          result.append("</td>");
                    
          result.append("</tr>");
        }
      }
      
      if(!foundDifferences)
      {
        result.append("<tr><td class=\"tableDataStrong\" align=\"center\">No Changes Found</td></tr>");
      }
      
      result.append("</table>");
    }
    catch(Exception e)
    {
      log.error(e.toString());
      result.setLength(0);
      result.append(e.toString());
    }
    
    return result.toString();
  }
  
  public String colData(int index)
  {
    String result = "";
    
    try
    {
      result = columnData.elementAt(index).toString();
    }
    catch(Exception e)
    {
      result = e.toString();
      log.error(e.toString());
    }
    
    return result;
  }
  
  public String getColumnData(String columnName)
  {
    String retVal = "";
    
    for(int i=0; i<getColumnCount(); ++i)
    {
      if( getColumnName(i).equals(columnName.toUpperCase()) )
      {
        retVal = colData(i);
      }
    }
    
    return retVal;
  }
  
  public String getColumnName(int idx)
  {
    return (String)(columnNames.elementAt(idx));
  }
  
  public int getColumnCount()
  {
    return columnData.size();
  }
  
  protected String getPrettyColumnName(String colName)
  {
    StringBuffer result = new StringBuffer(colName.trim().toLowerCase());
    
    try
    {
      int index;
      
      // replace underscores with spaces and turn first chars to upper case
      result.setCharAt(0, Character.toUpperCase(result.charAt(0)));
      while((index = result.toString().indexOf("_")) >= 0 && index < result.length()-1)
      {
        result.replace(index, index+1, " ");
        result.setCharAt(index+1, Character.toUpperCase(result.charAt(index+1)));
      }
    }
    catch(Exception e)
    {
      log.error(e.toString());
      result.setLength(0);
      result.append(e.toString());
    }
    
    return result.toString();
  } 

  public String pgChangeDetails()
  {
    String          params        = null;
    String          name          = null;
    String          oldVal        = null;
    String          newVal        = null;
    StringBuffer    retVal        = new StringBuffer();
    StringTokenizer tokens        = null;
    StringTokenizer strTokens     = null;
    boolean         foundChanges  = false;
    

    params = getColumnData("pg_change_details");

    if( params != null )
    {
      String paramsStr = "";

      try { paramsStr = params.replaceAll("\\)\\,\\(", "&").replaceAll("\\(", "").replaceAll("\\)", ""); } catch( Exception e ) { paramsStr = ""; }
      
      strTokens = new StringTokenizer( paramsStr, "&");
      retVal.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");
     
      while( strTokens.hasMoreTokens() )
      {
        int    idx          = -1;
        String param        = null;
        String tkNameValue  = null;

        try { param = strTokens.nextToken(); } catch( Exception e ) { param = ""; }

        try
        {
          tokens = new StringTokenizer(param,"=>");
          tkNameValue = tokens.nextToken();
          idx    = tkNameValue.indexOf("[");
          name   = tkNameValue.substring(0, idx).trim();
          oldVal = tkNameValue.substring(idx).replaceAll("\\[","").replaceAll("\\]", "").trim();
        }
        catch( Exception e )
        {
          //ignore
        }

        try { newVal = tokens.nextToken().replaceAll("\\[","").replaceAll("\\]", "").trim(); } catch( Exception e ) { newVal = ""; }

        retVal.append("<tr>");
        retVal.append("<td class=\"tableDataStrong\">" + name + "</td>");
        retVal.append("<td class=\"tableData\">" + " old: <font color=\"red\">" + oldVal + "</font>" + "</td>");
        retVal.append("<td class=\"tableData\">" + " new: <font color=\"green\">" + newVal + "</font>" + "</td>");
        retVal.append("</tr>");
        foundChanges = true;
      }
      
      if(!foundChanges)
      {
        retVal.append("<tr><td class=\"tableDataStrong\" align=\"center\">No Changes Found</td></tr>");
      }

      retVal.append("</table>");
    }
    
    return(retVal.toString());
  }
  
  public boolean pgSettingsChanged()
  {
    String changes = getColumnData("pg_change_details");
    
    return( changes != null && !changes.equals("") );
  }
  
  public static void main(String[] args)
  {
    SQLJConnectionBase.initStandalone();
    
    ResultSetRow rsr = new ResultSetRow();
    
    System.out.println(rsr.getPrettyColumnName(args[0]));
  }
}
