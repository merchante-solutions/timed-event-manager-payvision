/*@lineinfo:filename=CertegyMerchantMapper*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/CertegyMerchantMapper.sqlj $

  Description:
    Utility for mapping Certegy Station ID and product to Merchant


  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 6/14/04 2:51p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

public class CertegyMerchantMapper extends com.mes.database.SQLJConnectionBase
{

  /*******************
  attributes
  *******************/
  
  private String stationId;
  private String productTypeString;
  private String merchantNumberString;
  private int productType;
  private long merchantNumber;  
  private boolean submitted;
  private StringBuffer message;
  
  /*******************
  CONSTANTS
  *******************/
  //from DB - CERTEGY_PRODUCT_TYPE
  public static final int WELCOME_CHECK = 1;
  public static final int ELEC_CHECK = 2;
  public static final String WELCOME_CHECK_STRING = "1";
  public static final String ELEC_CHECK_STRING = "2";  
  public static final String MERCHANT_INVALID = "Unable to process Merchant Number - please check entry.";
  public static final String STATION_ID_INVALID = "Unable to process Station ID.";
  public static final String SUBMISSION_INVALID = "Unable to process submission - please check all fields.";
  public static final String SUCCESS = "Entry successful!";
  public static final String DB_ERROR = "Unable to save data to database.";
  

  /*******************
  CONSTRUCTORS CertegyMerchantMapper
  *******************/  
  
  public CertegyMerchantMapper()
  {
    // turn auto commit off
    super(false);
    productTypeString=WELCOME_CHECK_STRING;
    merchantNumberString="";
    stationId="";
    message = new StringBuffer("Welcome to the Certegy Station ID Map Tool.");
  }
  
  
  /*******************
  public methods
  *******************/
  
  public void submitData()
  {
    
    if(validate()){
      try{
        commitData();
        message.append(getSuccessMessage());
        reset();        
      }catch(Exception e){
        message.append(e.getMessage());
        message.append(getErrorMessage(DB_ERROR));
      }
    }
  }
  
  /*******************
  private methods
  *******************/
  
  private void reset()
  {
   
    productTypeString=WELCOME_CHECK_STRING;
    merchantNumberString="";
    stationId="";
    productType=WELCOME_CHECK;
    merchantNumber=0L;  
    submitted=false;
  }
  
  private boolean validate()
  {
    
    boolean result = false;
    message = new StringBuffer();
    
    try{
      merchantNumber = Long.parseLong(merchantNumberString);
    }catch(NumberFormatException nfe){
      message.append(getErrorMessage(MERCHANT_INVALID));
    }
    
    try{
      productType = Integer.parseInt(productTypeString);
      
      switch (productType){
        
        case WELCOME_CHECK:
          if( stationId==null || !(stationId.length()==10 || stationId.length()==13)){
            message.append(getErrorMessage(STATION_ID_INVALID+" Station ID must be either 10 or 13 characters."));
          }
          break;
        
        case ELEC_CHECK:
          if( stationId==null || stationId.length()!=15){
            message.append(getErrorMessage(STATION_ID_INVALID+" Station ID must be 15 characters."));
          }
          break;
      }
      
    }catch(Exception e){
      message.append(getErrorMessage(SUBMISSION_INVALID));
    }
    
    if(message.length()==0){
      result=true;
    }
    
    return result;
  }
  
  
  private void commitData()
  throws Exception
  {
    
    try
    {
      connect();
      
      setAutoCommit(true);
      
      //clear out old one, if exists
      /*@lineinfo:generated-code*//*@lineinfo:159^7*/

//  ************************************************************
//  #sql [Ctx] { delete from certegy_merchant_product_map
//          where merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from certegy_merchant_product_map\n        where merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.tools.CertegyMerchantMapper",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:163^7*/
      
      //insert new one
      /*@lineinfo:generated-code*//*@lineinfo:166^7*/

//  ************************************************************
//  #sql [Ctx] { insert into certegy_merchant_product_map
//          (
//            merchant_number,
//            certegy_product_id,
//            station_id
//          )
//          values
//          (
//            :merchantNumber,
//            :productType,
//            :stationId
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into certegy_merchant_product_map\n        (\n          merchant_number,\n          certegy_product_id,\n          station_id\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.tools.CertegyMerchantMapper",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setInt(2,productType);
   __sJT_st.setString(3,stationId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^7*/
    }
    catch(Exception e)
    {
      throw e;
    }
    finally
    {    
      cleanUp();
    }      
  }
  
  private String getErrorMessage(String error)
  {
    StringBuffer temp = new StringBuffer(32);
    temp.append("<font color='#CC0000'>");
    temp.append(error);
    temp.append("</font><br>");
    return temp.toString();
  }
  
  private String getSuccessMessage()
  {
    StringBuffer temp = new StringBuffer(32);
    temp.append("<font color='#009933'>");
    temp.append(SUCCESS).append("</font><br>");
    temp.append("Merchant Number - ").append(merchantNumberString).append("<br>");
    temp.append("Product Type - ").append(getProductName(productType)).append("<br>");
    temp.append("Station ID - ").append(stationId).append("<br>");
    return temp.toString();
  }
  
  private String getProductName(int productType)
  {
    String name = "";
    switch (productType)
    {
        case WELCOME_CHECK:
          name = "Welcome Check";
          break;
        
        case ELEC_CHECK:
          name = "Elec Check";
          break;
    }
    return name;
  }
  
  /*******************
  gets
  *******************/
  
  public String getStationId(){
    return stationId;
  }
  
  public String getMerchantNumberString(){
    return merchantNumberString;
  }
  
  public String getProductTypeString(){
    return productTypeString;
  }

  public boolean isSubmitted(){
    return submitted;
  }
  
  public String getMessage(){
    return message.toString();
  }
  
  /*******************
  sets - all Strings due to JSP bean usage
  *******************/  
  
  public void setStationIdWelcome(String stationId){
    this.stationId = stationId;
  }

  public void setStationIdElec(String stationId){
    this.stationId = stationId;
  }

  public void setMerchantNumberString (String merchantNumberString){
    this.merchantNumberString = merchantNumberString;
  }
  
  public void setProductTypeString (String productTypeString){
    this.productTypeString = productTypeString;
  }

  public void setSubmit (String submitted){
    this.submitted = true;
  }
  

}/*@lineinfo:generated-code*/