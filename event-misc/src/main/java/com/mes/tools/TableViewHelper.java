package com.mes.tools;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;


public class TableViewHelper
{
  // create class log category
  static Category log = Category.getInstance(TableViewHelper.class.getName());

  // data members
  private Class             klass                 = null;
  private TableView         tv                    = null;
  private String[][]        columnData            = null;
  private SimpleDateFormat  sdf                   = null;
  
  /*
  columnData FORMAT:
  -----------------
  {column name}, {TableView implementing class' associated accessor method name}
  */
  
  // construction
  private TableViewHelper() {}
  public  TableViewHelper(Class klass, TableView tv, String[][] columnData)
  {
    reset(klass,tv,columnData);
  }
  
  public void reset(Class klass, TableView tv, String[][] columnData)
  {
    this.klass = klass;
    this.tv = tv;
    this.columnData = columnData;
    this.sdf = null;
  }
  
  public void setDateFormat(String fmt)
  {
    sdf = new SimpleDateFormat(fmt);
  }
  
  public String getColumnName(int colOrdinal)
  {
    try {
      return columnData[colOrdinal-1][0];
    }
    catch(Exception e) {
      return "";
    }
  }
  
  public String[] getColumnNames()
  {
    String[] rval = new String[columnData.length];
    
    for(int i=0; i<rval.length; i++)
      rval[i] = columnData[i][0];
    
    return rval;
  }
  
  public String[] getColumnAccessors()
  {
    String[] rval = new String[columnData.length];
    
    for(int i=0; i<rval.length; i++)
      rval[i] = columnData[i][0];
    
    return rval;
  }
  
  public boolean isLinkToDetailColumn(int colOrdinal)
  {
    return columnData[colOrdinal-1][0].equals(tv.getLinkToDetailColumnName());
  }
  
  public String getDetailPageQueryString(int rowOrdinal)
  {
    StringBuffer sb = new StringBuffer();
    
    Key key = tv.getRowKey(rowOrdinal);
    if(key!=null)
      sb.append(tv.getRowKey(rowOrdinal).toQueryString());
    
    String qsSuffix = tv.getQueryStringSuffix();
    if(qsSuffix==null || qsSuffix.length()<1)
      return sb.toString();
    
    if(qsSuffix.startsWith("&"))
      sb.append(qsSuffix);
    else {
      sb.append('&');
      sb.append(qsSuffix);
    }
    
    return sb.toString();
  }
  
  public Object getRowObject(int rowOrdinal)
  {
    // get the object based on the row ordinal
    Object o = null;
    try {
      o = tv.getRows().get(rowOrdinal-1);
    }
    catch(Exception e) {
      log.error("getRowObject() - EXCEPTION: '"+e.toString()+"'.");
    }
    return o;
  }
  
  public String getCell(int rowOrdinal, int colOrdinal)
  {
    // get the accessor name from the specified column ordinal
    String accessorName = null;
    try {
      accessorName = columnData[colOrdinal-1][1];
    }
    catch(Exception e) {
      return "";
    }
    //log.debug("getColumnData() accessorName="+accessorName);
    
    // get the object based on the row ordinal
    Object o = getRowObject(rowOrdinal);
    
    // invoke the equip data accessor by reflection
    String data;
    try {
      Method method = klass.getMethod(accessorName,null);
      
      Object odata = method.invoke(o,null);
      if(odata instanceof java.util.Date) {
        if(((java.util.Date)odata).getTime()==0L || sdf==null)
          data = "";
        else
          data = sdf.format((java.util.Date)odata);
      } else 
        data = odata.toString();
      
    }
    catch(Exception e) {
      data="";
    }
    
    return data;
  }
  
  /**
   * getHtmlCell()
   * 
   * Returns the final string of each table cell taking into account
   * the link column so that the presentation layer simply calls 
   * getHtmlCell() for each column for each row.
   */
  public String getHtmlCell(int rowOrdinal, int colOrdinal)
  {
    String cellData = tv.getCell(rowOrdinal,colOrdinal);
    
    // html-ize the cell data
    cellData = StringUtilities.fmtstr(cellData,"---");
    cellData = StringUtilities.replace(cellData,"  ","&nbsp;&nbsp;");
      
    if(isLinkToDetailColumn(colOrdinal)) {
      StringBuffer sb = new StringBuffer();
      
      String queryString = tv.getDetailPageQueryString(rowOrdinal);
    
      sb.append("<a href=\"");
      sb.append(tv.getDetailPageBaseUrl());
      if(queryString.length()>0) {
        sb.append("?");
        sb.append(queryString);
      }
      sb.append("\">");
      sb.append(cellData);
      sb.append("</a>");
      
      return sb.toString();
    } else
      return cellData;
  }

}
