/*@lineinfo:filename=ValidateHierarchyBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/ValidateHierarchyBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/23/01 5:08p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.util.Iterator;
import com.mes.support.OrgBean;

/*@lineinfo:generated-code*//*@lineinfo:32^1*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class OrgCursor
extends sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public OrgCursor(sqlj.runtime.profile.RTResultSet resultSet)
    throws java.sql.SQLException
  {
    super(resultSet);
    orgNumNdx = findColumn("orgNum");
    orgNameNdx = findColumn("orgName");
    orgTypeCodeNdx = findColumn("orgTypeCode");
    hierarchyNodeNdx = findColumn("hierarchyNode");
    m_rs = (oracle.jdbc.OracleResultSet) resultSet.getJDBCResultSet();
  }
  private oracle.jdbc.OracleResultSet m_rs;
  public int orgNum()
    throws java.sql.SQLException
  {
    int __sJtmp = m_rs.getInt(orgNumNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int orgNumNdx;
  public String orgName()
    throws java.sql.SQLException
  {
    return (String)m_rs.getString(orgNameNdx);
  }
  private int orgNameNdx;
  public String orgTypeCode()
    throws java.sql.SQLException
  {
    return (String)m_rs.getString(orgTypeCodeNdx);
  }
  private int orgTypeCodeNdx;
  public long hierarchyNode()
    throws java.sql.SQLException
  {
    long __sJtmp = m_rs.getLong(hierarchyNodeNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int hierarchyNodeNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:36^24*/
                       
/*@lineinfo:generated-code*//*@lineinfo:38^1*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class MerchCursor
extends sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public MerchCursor(sqlj.runtime.profile.RTResultSet resultSet)
    throws java.sql.SQLException
  {
    super(resultSet);
    merchantNumberNdx = findColumn("merchantNumber");
    m_rs = (oracle.jdbc.OracleResultSet) resultSet.getJDBCResultSet();
  }
  private oracle.jdbc.OracleResultSet m_rs;
  public long merchantNumber()
    throws java.sql.SQLException
  {
    long __sJtmp = m_rs.getLong(merchantNumberNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int merchantNumberNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:38^50*/
                           
public class ValidateHierarchyBean extends com.mes.database.SQLJConnection
{
  public ValidateHierarchyBean()
  {
  }
  
  public ValidateHierarchyBean( String connectString )
  {
    super(connectString);
  }
  
  public String getOrgName(long hierarchyNode)
  {
    String orgName = "";
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:56^7*/

//  ************************************************************
//  #sql [Ctx] { select  org_name 
//          from    organization
//          where   org_group = :hierarchyNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  org_name  \n        from    organization\n        where   org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.ValidateHierarchyBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:61^7*/
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::getOrgName (" + hierarchyNode + "): " + e.toString());
    }
    
    return orgName;
  }
  
  public int getOrgNum(long hierarchyNode)
  {
    int   orgNum = 0;
    
    try
    {
      // get the org number for this node
      /*@lineinfo:generated-code*//*@lineinfo:78^7*/

//  ************************************************************
//  #sql [Ctx] { select  org_num 
//          from    organization
//          where   org_group = :hierarchyNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  org_num  \n        from    organization\n        where   org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.ValidateHierarchyBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:83^7*/
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::getOrgNum (" + hierarchyNode + "): " + e.toString());
    }
    
    return orgNum;
  }
  
  public void nodeWalk(long startNode)
  {
    OrgBean     patriarch = null;
    
    try
    {
      // create the root node
      patriarch = new OrgBean(getOrgNum(startNode), getOrgName(startNode), 0L, startNode);
      
      // fill the tree
      procreate(patriarch);
      
      // walk the tree, looking for problems
      compareMerchants(patriarch, 0);
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::nodeWalk (" + startNode + "): " + e.toString());
    }
  }
  
  public boolean compareMerchants(OrgBean ob, int level)
  {
    boolean     retVal          = true;
    Iterator    orgIterator     = null;
    Iterator    childIterator   = null;
    Long        orgMerchant     = null;
    Long        childMerchant   = null;
    
    StringBuffer  pad           = new StringBuffer("");
    
    for(int i=0; i<level; ++i)
    {
      pad.append("  ");
    }
    
    System.out.println(pad.toString() + ob.getOrgName() + "(" + ob.getOrgNum() + ")");
    
    try
    {
      orgIterator = ob.getMerchants().iterator();
      childIterator = ob.getChildMerchants().iterator();
      
      if(childIterator.hasNext())
      {
        while(orgIterator.hasNext())
        {
          orgMerchant = (Long)(orgIterator.next());
        
          if(childIterator.hasNext())
          {
            childMerchant = (Long)(childIterator.next());
          
            while(!orgMerchant.equals(childMerchant))
            {
              if(orgMerchant.compareTo(childMerchant) > 0)
              {
                // merchant is part of children but not part of parent
                System.out.println(pad.toString() + "  * " + childMerchant + ",c");
            
                childMerchant = (Long)(childIterator.next());
              }
              else
              {
                // merchant is part of parent but not part of children
                System.out.println(pad.toString() + "  * " + orgMerchant + ",p");
            
                orgMerchant = (Long)(orgIterator.next());
              }
            }
          }
          else
          {
            System.out.println(pad.toString() + "  * " + orgMerchant + ",p1");
          }
        }
      
        // look for merchants that are part of the children but not part of parent
        while(childIterator.hasNext())
        {
          childMerchant = (Long)(childIterator.next());
          System.out.println(pad.toString() + "  * " + childMerchant + ",c1");
        }
      }
      
      // ok, move on to next org bean
      Iterator it = ob.getChildren().iterator();
    
      while(it.hasNext())
      {
        if(!compareMerchants((OrgBean)(it.next()), level + 1))
        {
          break;
        }
      }
      
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::compareMerchants (" + ob.getHierarchyNode() + "): " + e.toString());
      retVal = false;
    }
    
    return retVal;
    
  }
  
  public void procreate(OrgBean ob)
  {
    OrgCursor     orgCursor     = null;
    MerchCursor   mCursor       = null;
    
    try
    {
      // add the children of this organization
      /*@lineinfo:generated-code*//*@lineinfo:208^7*/

//  ************************************************************
//  #sql [Ctx] orgCursor = { select  p.org_num         orgNum,
//                  o.org_name        orgName,
//                  o.org_type_code   orgTypeCode,
//                  o.org_group       hierarchyNode
//          from    parent_org p,
//                  organization o
//          where   p.parent_org_num = :ob.getOrgNum() and
//                  o.org_num = p.org_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_70 = ob.getOrgNum();
  try {
   String theSqlTS = "select  p.org_num         orgNum,\n                o.org_name        orgName,\n                o.org_type_code   orgTypeCode,\n                o.org_group       hierarchyNode\n        from    parent_org p,\n                organization o\n        where   p.parent_org_num =  :1  and\n                o.org_num = p.org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.ValidateHierarchyBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_70);
   // execute query
   orgCursor = new com.mes.tools.OrgCursor(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.tools.ValidateHierarchyBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:218^7*/
      
      while(orgCursor.next())
      {
        if(!orgCursor.orgTypeCode().equals("m"))
        {
          ob.addChild( orgCursor.orgNum(), orgCursor.orgName(), 0L, orgCursor.hierarchyNode());
        }
      }
      try { orgCursor.close(); } catch(Exception ie) { }
      
      // add the merchant numbers that belong to this organization
      /*@lineinfo:generated-code*//*@lineinfo:230^7*/

//  ************************************************************
//  #sql [Ctx] mCursor = { select  merchant_number merchantNumber
//          from    group_merchant
//          where   org_num = :ob.getOrgNum()
//          order by merchant_number asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_71 = ob.getOrgNum();
  try {
   String theSqlTS = "select  merchant_number merchantNumber\n        from    group_merchant\n        where   org_num =  :1 \n        order by merchant_number asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tools.ValidateHierarchyBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_71);
   // execute query
   mCursor = new com.mes.tools.MerchCursor(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.tools.ValidateHierarchyBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:236^7*/
      
      while(mCursor.next())
      {
        ob.addMerchant(mCursor.merchantNumber());
      }
      try { mCursor.close(); } catch(Exception ie) { }
      
      // add the child merchants to this org bean
      Iterator it = ob.getChildren().iterator();
      while(it.hasNext())
      {
        OrgBean mob = (OrgBean)(it.next());
        /*@lineinfo:generated-code*//*@lineinfo:249^9*/

//  ************************************************************
//  #sql [Ctx] mCursor = { select  merchant_number merchantNumber
//            from    group_merchant
//            where   org_num = :mob.getOrgNum()
//            order by merchant_number asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_72 = mob.getOrgNum();
  try {
   String theSqlTS = "select  merchant_number merchantNumber\n          from    group_merchant\n          where   org_num =  :1 \n          order by merchant_number asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tools.ValidateHierarchyBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_72);
   // execute query
   mCursor = new com.mes.tools.MerchCursor(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.tools.ValidateHierarchyBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^9*/
        
        while(mCursor.next())
        {
          ob.addChildMerchant(mCursor.merchantNumber());
        }
        try { mCursor.close(); } catch(Exception ie) { }
      }
      
      for(Iterator itt = ob.getChildren().iterator(); itt.hasNext();)
      {
        procreate((OrgBean)(itt.next()));
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::procreate (" + ob.getOrgName() + ", " + ob.getOrgNum() + "): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/