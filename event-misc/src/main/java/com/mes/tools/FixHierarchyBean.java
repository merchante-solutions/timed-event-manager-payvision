/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/FixHierarchyBean.java $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/21/00 10:32a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.DBConnect;


public class FixHierarchyBean
{
  private static final String DB_CONNECT_STRING = "pool;ApplicationPool";


  private DBConnect           db    = new DBConnect(this, "FixHierarchyBean");
  private Connection          con   = null;

  // data members
  Vector    errors      = new Vector();

  public FixHierarchyBean()
  {
  }

  private boolean cleanHierarchy(long merchId)
  {
    boolean           success = false;
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               orgId   = 0;

    try
    {
      System.out.println("checking for specific merchant");
      // if this is for a specific merchant we need to get his org id
      if(merchId > 0L)
      {
        qs.append("select org_num from orgmerchant ");
        qs.append("where org_merchant_num = ?");
        ps = con.prepareStatement(qs.toString());
        ps.setLong(1, merchId);
        rs = ps.executeQuery();

        if(rs.next())
        {
          orgId = rs.getInt("org_num");
        }

        rs.close();
        ps.close();
      }

      System.out.println("cleaning group_merchant table");
      // clean group_merchant table
      qs.setLength(0);
      qs.append("delete from group_merchant ");
      if(merchId > 0L)
      {
        qs.append("where merchant_number = ?");
      }

      ps = con.prepareStatement(qs.toString());

      if(merchId > 0L)
      {
        ps.setLong(1, merchId);
      }

      ps.executeUpdate();
      ps.close();

      System.out.println("Cleaning parent_org table");
      // clean parent_org table
      qs.setLength(0);
      qs.append("delete from parent_org ");
      if(merchId > 0L)
      {
        qs.append("where org_num = ?");
      }

      ps = con.prepareStatement(qs.toString());

      if(merchId > 0L)
      {
        ps.setInt(1, orgId);
      }

      ps.executeUpdate();
      ps.close();

      success = true;
    }
    catch(Exception e)
    {
      errors.add("cleanHierarchy (" + merchId + "): " + e.toString());
    }
    finally
    {
      try
      {
        ps.close();
      }
      catch(Exception e)
      {
      }
      
      try
      {
        rs.close();
      }
      catch(Exception e)
      {
      }
    }

    return success;
  }

  private void checkOrg(int assoc)
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    int               newOrg      = 0;

    try
    {
      String assocString = String.valueOf(assoc);
      
      qs.append("select org_num from orgassoc ");
      qs.append("where group_number = ?");

      ps = con.prepareStatement(qs.toString());
      ps.setString(1, assocString);

      if( ! ps.executeQuery().next())
      {
        ps.close();

        // we have to create a new organization, so get a new org number
        ps = con.prepareStatement("select nvl(max(org_num), 0)+1 as neworg from organization");
        rs = ps.executeQuery();
        if(rs.next())
        {
          newOrg = rs.getInt("neworg");
        }
        rs.close();
        ps.close();

        // create the organization
        qs.setLength(0);
        qs.append("insert into organization (");
        qs.append("org_num, ");
        qs.append("org_type_code, ");
        qs.append("org_name) ");
        qs.append("values (?, ?, ?)");

        ps = con.prepareStatement(qs.toString());
        ps.setInt(1, newOrg);
        ps.setString(2, "a");
        ps.setString(3, assocString);
      
        ps.executeUpdate();
      
        // now create the orgassoc record
        ps.close();
        qs.setLength(0);
      
        qs.append("insert into orgassoc (");
        qs.append("org_num, ");
        qs.append("group_number) ");
        qs.append("values (?, ?)");
      
        ps = con.prepareStatement(qs.toString());
        ps.setInt(1, newOrg);
        ps.setString(2, assocString);
      
        ps.executeUpdate();
      
        ps.close();
      }
    }
    catch(Exception e)
    {
      errors.add("checkOrg (" + assoc + "): " + e.toString());
    }
    finally
    {
      try
      {
        ps.close();
      }
      catch(Exception e)
      {
      }
      
      try
      {
        rs.close();
      }
      catch(Exception e)
      {
      }
    }
  }
  
  private int getOrgNum(int assoc)
  {
    PreparedStatement   ps    = null;
    ResultSet           rs    = null;
    int                 orgId = 0;
    
    try
    {
      ps = con.prepareStatement("select org_num from orgassoc where group_number = ?");
      ps.setString(1, String.valueOf(assoc));
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        orgId = rs.getInt("org_num");
      }
      
      rs.close();
      ps.close();
      
    }
    catch(Exception e)
    {
      errors.add("getOrgNum (" + assoc + "): " + e.toString());
    }
    finally
    {
      try
      {
        ps.close();
      }
      catch(Exception e)
      {
      }
      
      try
      {
        rs.close();
      }
      catch(Exception e)
      {
      }
    }
    
    return orgId;
  }
  
  private void addParentOrg(int parent, int child)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    
    try
    {
      qs.append("select org_num from parent_org ");
      qs.append("where org_num = ? and parent_org_num = ?");
      
      ps = con.prepareStatement(qs.toString());
      ps.setString(1, String.valueOf(child));
      ps.setInt   (2, parent);
      
      if (! ps.executeQuery().next())
      {
        ps.close();
        qs.setLength(0);
        qs.append("insert into parent_org (");
        qs.append("org_num, ");
        qs.append("parent_org_num) ");
        qs.append("values (?, ?)");
      
        ps = con.prepareStatement(qs.toString());
      
        ps.setString(1, String.valueOf(child));
        ps.setInt   (2, parent);
      
        ps.executeUpdate();
      }
      
      ps.close();
    }
    catch(Exception e)
    {
      errors.add("addParentOrg (" + parent + ", " + child + "): " + e.toString());
    }
    finally
    {
      try
      {
        ps.close();
      }
      catch(Exception e)
      {
      }
    }
  }
  
  private void addGroupMerchant(int org, long merchId)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    
    try
    {
      qs.append("insert into group_merchant (");
      qs.append("org_num, ");
      qs.append("merchant_number) ");
      qs.append("values (?, ?)");
      
      ps = con.prepareStatement(qs.toString());
      ps.setInt(1, org);
      ps.setLong(2, merchId);
      
      ps.executeUpdate();
      ps.close();
    }
    catch(Exception e)
    {
      errors.add("addGroupMerchant(" + org + ", " + merchId + "): " + e.toString());
    }
    finally
    {
      try
      {
        ps.close();
      }
      catch(Exception e)
      {
      }
    }
  }

  private void createHierarchy(ResultSet rs)
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;

    long              merchId     = 0L;
    int               assoc       = 0;
    int               group1      = 0;
    int               group2      = 0;
    int               group3      = 0;
    int               group4      = 0;
    int               group5      = 0;
    int               merchOrgId  = 0;

    try
    {
      merchId     = rs.getLong("merchant_number");
      assoc       = Integer.parseInt(rs.getString("dmagent"));
      group1      = Integer.parseInt(rs.getString("group_1_association"));
      group2      = Integer.parseInt(rs.getString("group_2_association"));
      group3      = Integer.parseInt(rs.getString("group_3_association"));
      group4      = Integer.parseInt(rs.getString("group_4_association"));
      group5      = Integer.parseInt(rs.getString("group_5_association"));
      merchOrgId  = rs.getInt("org_num");

      // make sure the organizations exist
      checkOrg(assoc);
      checkOrg(group1);
      checkOrg(group2);
      checkOrg(group3);
      checkOrg(group4);
      checkOrg(group5);
      
      // get the org ids for the organizations
      int orgAssoc  = getOrgNum(assoc);
      int orgGroup1 = getOrgNum(group1);
      int orgGroup2 = getOrgNum(group2);
      int orgGroup3 = getOrgNum(group3);
      int orgGroup4 = getOrgNum(group4);
      int orgGroup5 = getOrgNum(group5);
      
      // add the parent_org entries
      addParentOrg(orgAssoc, merchOrgId);
      addParentOrg(orgGroup5, orgAssoc);
      addParentOrg(orgGroup4, orgGroup5);
      addParentOrg(orgGroup3, orgGroup4);
      addParentOrg(orgGroup2, orgGroup3);
      addParentOrg(orgGroup1, orgGroup2);
      addParentOrg(2, orgGroup1);
      
      // add the group_merchant entries
      addGroupMerchant(merchOrgId, merchId);
      addGroupMerchant(orgAssoc, merchId);
      addGroupMerchant(orgGroup1, merchId);
      addGroupMerchant(orgGroup2, merchId);
      addGroupMerchant(orgGroup3, merchId);
      addGroupMerchant(orgGroup4, merchId);
      addGroupMerchant(orgGroup5, merchId);
      addGroupMerchant(2, merchId);
    }
    catch(Exception e)
    {
      errors.add("createHierarchy: " + e.toString());
    }
    finally
    {
      try
      {
        ps.close();
      }
      catch(Exception e)
      {
      }
    }
  }

  public boolean fixHierarchy(long merchId)
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement merchPS     = null;
    ResultSet         merchRS     = null;
    boolean           success     = false;
    DBConnect         tempDB      = new DBConnect(this, "fixHierarchy");
    Connection        conn        = null;

    try
    {
      conn = tempDB.getConnection(DB_CONNECT_STRING);
      con = db.getConnection(DB_CONNECT_STRING);

      // first clean the hierarchy out with a toilet brush
      if(cleanHierarchy(merchId))
      {
        System.out.println("-----------");
        System.out.println("Successfully cleaned " + merchId);
        
        qs.append("select ");
        qs.append("a.merchant_number, ");
        qs.append("a.dmagent, ");
        qs.append("a.group_1_association, ");
        qs.append("a.group_2_association, ");
        qs.append("a.group_3_association, ");
        qs.append("a.group_4_association, ");
        qs.append("a.group_5_association, ");
        qs.append("b.org_num ");
        qs.append("from mif a, orgmerchant b ");
        qs.append("where a.merchant_number = b.org_merchant_num ");

        if(merchId > 0L)
        {
          qs.append("and a.merchant_number = ? ");
        }
        
        qs.append("order by merchant_number asc");
        

        merchPS = con.prepareStatement(qs.toString());

        if(merchId > 0L)
        {
          merchPS.setLong(1, merchId);
        }

        merchRS = merchPS.executeQuery();
        
        while(merchRS.next())
        {
          System.out.println("Creating hierarchy for " + merchRS.getLong("merchant_number"));
          createHierarchy(merchRS);
          
          if(errors.size() > 0)
          {
            break;
          }
        }

        merchRS.close();
        merchPS.close();

        if(errors.size() == 0)
        {
          success = true;
        }
      }
    }
    catch(Exception e)
    {
      errors.add("fixHierarchy: " + e.toString());
    }
    finally
    {
      if(db != null)
      {
        try
        {
          db.releaseConnection();
        }
        catch(Exception e)
        {
        }
      }
      
      try
      {
        tempDB.releaseConnection();
      }
      catch(Exception e)
      {
      }
    }

    return success;
  }

  public boolean fixHierarchy()
  {
    return(fixHierarchy(0));
  }

  public Vector getErrors()
  {
    return this.errors;
  }
}
