/*@lineinfo:filename=THierarchy*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/THierarchy.sqlj $

  Description:  
  
  Builds a navigable data structure containing the non-merchant nodes
  of a t_hierarchy style hierarchy of a given type.


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 9 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;
import com.mes.constants.MesHierarchy;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class THierarchy extends SQLJConnectionBase
{
  public static final int NO_CHILD_NO_SIB           = 0;
  public static final int OPEN_ROOT_NODE            = 1;
  public static final int CLOSED_ROOT_NODE          = 2;
  public static final int OPEN_CHILD_WITH_SIB       = 3;
  public static final int CLOSED_CHILD_WITH_SIB     = 4;
  public static final int NO_CHILD_WITH_SIB         = 5;
  public static final int OPEN_CHILD_WITH_NO_SIB    = 6;
  public static final int CLOSED_CHILD_WITH_NO_SIB  = 7;
  public static final int EMPTY_CHILD_WITH_NO_SIB   = 8;
  public static final int EMPTY_CHILD_WITH_SIB      = 9;
  
  private THierarchy(int viewType)
  {
    this.viewType = viewType;
    loadHierarchy();
  }
  
  public THierarchy()
  {
  }
  
  public class HierarchyNode extends Vector
  {
    private String nodeName;
    private long nodeId;
    private long parentId;
    private int depth;
    private int hierarchyType;
    private Vector children = new Vector();
    
    private boolean isRefreshed = true;
    private boolean isOpen = false;
    
    public HierarchyNode(long   nodeId,
                         String nodeName,
                         long   parentId,
                         int    depth,
                         int    hierarchyType)
    {
      this.nodeId         = nodeId;
      this.nodeName       = nodeName;
      this.parentId       = parentId;
      this.depth          = depth;
      this.hierarchyType  = hierarchyType;
    }
    
    public long getNodeId()
    {
      return nodeId;
    }
    public long getParentId()
    {
      return parentId;
    }
    public int getDepth()
    {
      return depth;
    }
    public String getNodeName()
    {
      return nodeName;
    }
    public int getHierarchyType()
    {
      return hierarchyType;
    }
  }

  private HierarchyNode root = null;
  private synchronized void loadHierarchy()
  {
    int prog=0;
    ResultSetIterator it = null;
    ResultSet         rs = null;
    try
    {
      connect();
      
      // create a root node based on hierarchy type
      Hashtable nodeHash = new Hashtable();
      long rootId = 0L;
      String rootName = null;
      /*@lineinfo:generated-code*//*@lineinfo:124^7*/

//  ************************************************************
//  #sql [Ctx] { select  root_id,
//                  name
//          
//          from    t_hierarchy_types
//          where   hier_type = :viewType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  root_id,\n                name\n         \n        from    t_hierarchy_types\n        where   hier_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.THierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,viewType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rootId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   rootName = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^7*/
      root = new HierarchyNode(rootId,rootName,-1L,0,viewType);
      nodeHash.put(rootId, root);
      
      prog=1;
      
      // select all non-merchant hierarchy nodes ordered by
      // distance from root to node
      /*@lineinfo:generated-code*//*@lineinfo:140^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    h2.ancestor                     parent_id,
//                    h2.descendent                   node_id,
//                    h1.relation                     depth,
//                    nvl(hn.name,'(no name given)')  name,
//                    h2.hier_type
//          from      t_hierarchy       h1,
//                    t_hierarchy       h2,
//                    t_hierarchy_names hn
//          where     h1.ancestor = :rootId
//                    and h1.entity_type <> :MesHierarchy.ET_MERCHANT
//                    and h1.relation > 0
//                    and h1.descendent = h2.descendent
//                    and h1.hier_type = h2.hier_type
//                    and h2.relation = 1
//                    and h2.entity_type <> :MesHierarchy.ET_MERCHANT
//                    and h2.descendent = hn.hier_id(+)
//          order by  h1.relation,
//                    hn.name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    h2.ancestor                     parent_id,\n                  h2.descendent                   node_id,\n                  h1.relation                     depth,\n                  nvl(hn.name,'(no name given)')  name,\n                  h2.hier_type\n        from      t_hierarchy       h1,\n                  t_hierarchy       h2,\n                  t_hierarchy_names hn\n        where     h1.ancestor =  :1 \n                  and h1.entity_type <>  :2 \n                  and h1.relation > 0\n                  and h1.descendent = h2.descendent\n                  and h1.hier_type = h2.hier_type\n                  and h2.relation = 1\n                  and h2.entity_type <>  :3 \n                  and h2.descendent = hn.hier_id(+)\n        order by  h1.relation,\n                  hn.name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.THierarchy",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,rootId);
   __sJT_st.setInt(2,MesHierarchy.ET_MERCHANT);
   __sJT_st.setInt(3,MesHierarchy.ET_MERCHANT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.tools.THierarchy",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:160^7*/
      rs = it.getResultSet();
      
      prog=2;
      
      // build/refresh hierarchy
      while (rs.next())
      {
        // create node
        long nodeId         = rs.getLong("node_id");
        long parentId       = rs.getLong("parent_id");
        int depth           = rs.getInt("depth");
        String name         = rs.getString("name");
        int hierarchyType   = rs.getInt("hier_type");
        
        // create a node
        HierarchyNode node = 
          new HierarchyNode(nodeId,name,parentId,depth,hierarchyType);
      
        // put node in hash table
        nodeHash.put(nodeId ,node);
    
        // locate parent
        HierarchyNode parent = (HierarchyNode)nodeHash.get(parentId);
        if (parent == null)
        {
          System.out.println("could not find parent " + parentId + " of child " + nodeId);
        }
      
        // add node as child to parent
        parent.add(node);
      }
      
      rs.close(); 
      it.close();
    }
    catch (Exception e)
    {
      logEntry("loadHierarchy()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  private HierarchyNode getHierarchyRoot()
  {
    return root;
  }
  
  // public static version
  public static HierarchyNode getHierarchy(int viewType)
  {
    THierarchy hBuilder = new THierarchy(viewType);
    return hBuilder.getHierarchyRoot();
  }
  
  // instance version
  public HierarchyNode getHierarchy()
  {
    loadHierarchy();
    return root;
  }

  private String submitVal = null;  
  public synchronized String getSubmitVal()
  {
    return submitVal;
  }
  public synchronized void setSubmitVal(String submitVal)
  {
    this.submitVal = submitVal;
  }
  
  private long nodeId;
  public synchronized long getNodeId()
  {
    return nodeId;
  }
  public void setNodeId(String nodeId)
  {
    try
    {
      setNodeId(Long.parseLong(nodeId));
    }
    catch(Exception e)
    {
      logEntry("setNodeId(" + nodeId + ")", e.toString());
    }
  }
  public synchronized void setNodeId(long nodeId)
  {
    this.nodeId = nodeId;
  }
  
  private String nodeName;
  public String getNodeName()
  {
    return nodeName;
  }
  public synchronized void setNodeName(String nodeName)
  {
    this.nodeName = nodeName;
  }
  
  private long parentId;
  public synchronized long getParentId()
  {
    return parentId;
  }
  public void setParentId(String parentId)
  {
    try
    {
      setParentId(Integer.parseInt(parentId));
    }
    catch(Exception e)
    {
      logEntry("setParentId(" + parentId + ")", e.toString());
    }
  }
  public synchronized void setParentId(long parentId)
  {
    this.parentId = parentId;
  }

  private int hierarchyType;
  public synchronized int getHierarchyType()
  {
    return hierarchyType;
  }
  public void setHierarchyType(String hierarchyType)
  {
    try
    {
      setHierarchyType(Integer.parseInt(hierarchyType));
    }
    catch(Exception e)
    {
      logEntry("setHierarchyType(" + hierarchyType + ")", e.toString());
    }
  }
  public synchronized void setHierarchyType(int hierarchyType)
  {
    this.hierarchyType = hierarchyType;
  }
  
  private long deleteNodeId;
  public synchronized long getDeleteNodeId()
  {
    return deleteNodeId;
  }
  public void setDeleteNodeId(String deleteNodeId)
  {
    try
    {
      setDeleteNodeId(Long.parseLong(deleteNodeId));
    }
    catch(Exception e)
    {
      logEntry("setDeleteNodeId(" + deleteNodeId + ")", e.toString());
    }
  }
  public synchronized void setDeleteNodeId(long deleteNodeId)
  {
    this.deleteNodeId = deleteNodeId;
  }
  
  private long shiftFromId;
  public synchronized long getShiftFromId()
  {
    return shiftFromId;
  }
  public void setShiftFromId(String shiftFromId)
  {
    try
    {
      setShiftFromId(Long.parseLong(shiftFromId));
    }
    catch(Exception e)
    {
      logEntry("setShiftFromId(" + shiftFromId + ")", e.toString());
    }
  }
  public synchronized void setShiftFromId(long shiftFromId)
  {
    this.shiftFromId = shiftFromId;
  }
  
  private long shiftToId;  
  public synchronized long getShiftToId()
  {
    return shiftToId;
  }
  public void setShiftToId(String shiftToId)
  {
    try
    {
      setShiftToId(Long.parseLong(shiftToId));
    }
    catch(Exception e)
    {
      logEntry("setShiftToId(" + shiftToId + ")", e.toString());
    }
  }
  public synchronized void setShiftToId(long shiftToId)
  {
    this.shiftToId = shiftToId;
  }
  
  private int viewType = MesHierarchy.HT_SALES_HIERARCHIES;
  public synchronized int getViewType()
  {
    return viewType;
  }
  public void setViewType(String viewType)
  {
    try
    {
      setViewType(Integer.parseInt(viewType));
    }
    catch(Exception e)
    {
      logEntry("setViewType(" + viewType + ")", e.toString());
    }
  }
  public synchronized void setViewType(int viewType)
  {
    this.viewType = viewType;
  }
  
  private Hashtable statusHash = new Hashtable();
  public synchronized boolean getNodeOpen(long nodeId)
  {
    boolean isOpen = false;
    try
    {
      isOpen = ((Boolean)statusHash.get(nodeId)).booleanValue();
    }
    catch (Exception e) {}
    
    return isOpen;
  }
  public synchronized void toggleStatus(long nodeId)
  {
    statusHash.put(nodeId, !getNodeOpen(nodeId));
  }
  
  public synchronized void insertNode()
  {
    // set to -1 if 0 to make hierarchy tools generate a node id
    nodeId = (nodeId == 0 ? -1L : nodeId);
    
    // always insert new nodes as entity type 1 (generic hierarchy node)
    HierarchyTools.insertNode(nodeId,1,nodeName,parentId,hierarchyType);
    
    // switch back to 0 if -1
    nodeId = (nodeId == -1L ? 0 : nodeId);
  }
  
  public synchronized void moveNode()
  {
    try
    {
      connect();
      
      int entityType = 0;
      
      // get node name and entity type
      /*@lineinfo:generated-code*//*@lineinfo:432^7*/

//  ************************************************************
//  #sql [Ctx] { select  hn.name,
//                  h.entity_type
//          
//          from    t_hierarchy h,
//                  t_hierarchy_names hn
//          where   h.descendent = :nodeId
//                  and h.hier_type = :hierarchyType
//                  and h.relation = 0
//                  and h.descendent = hn.hier_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  hn.name,\n                h.entity_type\n         \n        from    t_hierarchy h,\n                t_hierarchy_names hn\n        where   h.descendent =  :1 \n                and h.hier_type =  :2 \n                and h.relation = 0\n                and h.descendent = hn.hier_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.THierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,hierarchyType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   nodeName = (String)__sJT_rs.getString(1);
   entityType = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:444^7*/
      
      // if this is a sales rep, move the node indirectly by changing
      // the rep's report to and letting the sale_rep table trigger
      // handle the update
      if (entityType == MesHierarchy.ET_SALES_REP)
      {
        /*@lineinfo:generated-code*//*@lineinfo:451^9*/

//  ************************************************************
//  #sql [Ctx] { update  sales_rep
//            set     rep_report_to = :parentId
//            where   user_id = :nodeId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  sales_rep\n          set     rep_report_to =  :1 \n          where   user_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.tools.THierarchy",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,parentId);
   __sJT_st.setLong(2,nodeId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:456^9*/
      }
      else
      {
        HierarchyTools.moveNode(nodeId,nodeId,entityType,nodeName,parentId,hierarchyType);
      }
    }
    catch (Exception e)
    {
      logEntry("moveNode()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public synchronized void deleteNode()
  {
    try
    {
      connect();
      
      int entityType = 0;
      
      // get node name and entity type and hierarchy type
      /*@lineinfo:generated-code*//*@lineinfo:482^7*/

//  ************************************************************
//  #sql [Ctx] { select  hn.name,
//                  h.entity_type
//          
//          from    t_hierarchy h,
//                  t_hierarchy_names hn
//          where   h.descendent = :deleteNodeId
//                  and h.hier_type = :hierarchyType
//                  and h.relation = 0
//                  and h.descendent = hn.hier_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  hn.name,\n                h.entity_type\n         \n        from    t_hierarchy h,\n                t_hierarchy_names hn\n        where   h.descendent =  :1 \n                and h.hier_type =  :2 \n                and h.relation = 0\n                and h.descendent = hn.hier_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tools.THierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,deleteNodeId);
   __sJT_st.setInt(2,hierarchyType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   nodeName = (String)__sJT_rs.getString(1);
   entityType = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:494^7*/
      
      // only delete non-sales rep nodes for now, since there are related
      // sales rep and user records associated with the node that would be
      // left dangling
      if (entityType != MesHierarchy.ET_SALES_REP)
      {
        HierarchyTools.deleteNode(deleteNodeId,hierarchyType);
      }
    }
    catch (Exception e)
    {
      System.out.println("deleteNode(): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public synchronized void shiftNodes()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      connect();
      
      // get a list of nodes reporting to shiftFromId
      /*@lineinfo:generated-code*//*@lineinfo:524^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  descendent,
//                  hier_type
//          from    t_hierarchy
//          where   ancestor = :shiftFromId
//                  and hier_type = :hierarchyType
//                  and relation = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  descendent,\n                hier_type\n        from    t_hierarchy\n        where   ancestor =  :1 \n                and hier_type =  :2 \n                and relation = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tools.THierarchy",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,shiftFromId);
   __sJT_st.setInt(2,hierarchyType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.tools.THierarchy",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:532^7*/
      
      rs = it.getResultSet();
      
      long storedParentId     = getParentId();
      long storedNodeId       = getNodeId();
      int storedHierarchyType = getHierarchyType();
      
      setParentId(shiftToId);
      
      while (rs.next())
      {
        setNodeId(rs.getLong("descendent"));
        setHierarchyType(rs.getInt("hier_type"));
        moveNode();
      }
      
      rs.close();
      it.close();
      
      setParentId(storedParentId);
      setNodeId(storedNodeId);
      setHierarchyType(storedHierarchyType);
    }
    catch (Exception e)
    {
      System.out.println("shiftNodes(): " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public synchronized void renameNode()
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:574^7*/

//  ************************************************************
//  #sql [Ctx] { update  t_hierarchy_names
//          set     name = :nodeName
//          where   hier_id = :nodeId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  t_hierarchy_names\n        set     name =  :1 \n        where   hier_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.tools.THierarchy",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,nodeName);
   __sJT_st.setLong(2,nodeId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:579^7*/
    }
    catch (Exception e)
    {
      logEntry("renameNode()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
};/*@lineinfo:generated-code*/