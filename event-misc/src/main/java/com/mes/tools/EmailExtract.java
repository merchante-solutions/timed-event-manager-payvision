package com.mes.tools;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Iterator;
import java.util.zip.InflaterInputStream;
import com.mes.net.EmailQueueItem;
import masthead.formats.visak.Batch;

/**
 *  EmailExtract : - Connects to database, extracts BLOB messages from email_messages table based on the email_seq_num provided. 
 *  Arguments to class: EmailExtract hostname servicename user password emailseqnum
 *
 */
public class EmailExtract {
	public static void main(String[] arg) throws Exception {
		readFromDB(arg);
	}
	
	public static void readFromDB(String [] arg) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		Batch batch = null;
		Blob blobData = null;
		byte[] buffer = null;
		int chunkSize = 0;
		int dataLen = 0;
		int dataRead = 0;
		InputStream is = null;

		if (arg.length != 5) {
			System.out.println("Usage: java EmailExtract host service user password id");
			System.exit(1);
		}

		Class.forName("oracle.jdbc.driver.OracleDriver");

		String jdbcUrl = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(PORT=1521)(HOST="
				+ arg[0] + ")))(CONNECT_DATA=(service_name=" + arg[1] + ")))";
		System.out.println("Connecting to: " + jdbcUrl);
		System.out.println("Connecting as: " + arg[2]);

		con = DriverManager.getConnection(jdbcUrl, arg[2], arg[3]);

		String q = "select * from email_messages where email_seq_num=" + arg[4];
		System.out.println("Reading ID: " + arg[4]);

		ps = con.prepareStatement(q);
		rs = ps.executeQuery();
		rs.next();
		Blob b = rs.getBlob("message");
		byte [] content = readBlob(b);
		
		ByteArrayInputStream bais = new ByteArrayInputStream(content);
		InflaterInputStream iis = new InflaterInputStream(bais);
		ObjectInputStream ois = new ObjectInputStream(iis);

		EmailQueueItem result = (EmailQueueItem) ois.readObject();

		System.out.println("From: " + result.getFromString());
		System.out.println("To: " + result.getToString());
		System.out.println("Subject: " + result.getSubject());
		System.out.println("Content: " + result.getText());
		Iterator i = result.attachmentsIterator();
		FileOutputStream fos=null;
		try 
		{
			
		while (i.hasNext()) {
            EmailQueueItem.Attachment a = (EmailQueueItem.Attachment) i.next();
            System.out.println("Writing File: " + a.getFilename());
            fos = new FileOutputStream(arg[4] + "-" + a.getFilename());
            fos.write(a.getFiledata());	   
		}
		}catch(Exception ex){ }
		finally{
				if (fos != null) {
					fos.close();
				}
		}
		
		try { rs.close(); } catch (Exception ex) {	}
		try { ps.close(); } catch (Exception ex) {	}
		try { con.close(); } catch (Exception ex) {	}
		
		System.out.println(new Date() + " Connection closed... Done");
	}	
	
	public static byte [] readBlob(Blob b) throws Exception {
		int dataLen = (int)b.length();
		int chunkSize = 0;
		int dataRead = 0;
		
		System.out.println("Reading " + b.length() + " bytes");
		InputStream is = b.getBinaryStream();
		byte data[] = new byte[dataLen];
		while (chunkSize != -1)
		{
			chunkSize = is.read(data,dataRead,dataLen - dataRead);
			dataRead += chunkSize;
		}
		is.close();
		return data;
	}
}
