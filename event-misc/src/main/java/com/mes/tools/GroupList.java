/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/GroupList.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/12/02 10:04a $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

public class GroupList
{
  private long[]    groups        = new long[10];
  private String[]  groupNames    = new String[10];
  private int   groupSize     = 0;
  
  public void add(long group)
  {
    if(group > 0L && groupSize < 10L)
    {
      groups[groupSize++] = group;
    }
  }
  
  public void add(long group, String groupName)
  {
    if(group > 0L && groupSize < 10L)
    {
      groupNames[groupSize] = groupName;
      groups[groupSize++] = group;
    }
  }
  
  public int getGroupSize()
  {
    return groupSize;
  }
  
  public long getGroup(int idx)
  {
    long retVal = -1;
    
    if(idx < groupSize)
    {
      retVal = groups[idx];
    }
    
    return retVal;
  }
  
  public String getGroupName(int idx)
  {
    String retVal = "";
    
    if(idx < groupSize)
    {
      retVal = groupNames[idx];
    }
    
    return retVal;
  }
  
  public long[] getGroups()
  {
    return groups;
  }
  
  public String[] getGroupNames()
  {
    return groupNames;
  }
}
