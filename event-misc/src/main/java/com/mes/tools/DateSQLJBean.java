/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/DateSQLJBean.java $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/22/02 2:13p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import com.mes.database.SQLJConnectionBase;

public class DateSQLJBean extends SQLJConnectionBase
  implements Serializable
{
  private   boolean initialized       = false;
  public    int     currentYear       = 2000;
  public    int     fromDay           = 1;
  public    int     fromMonth         = 0;
  public    int     fromYear          = 2000;
  public    int     toDay             = 1;
  public    int     toMonth           = 0;
  public    int     toYear            = 2000;

  public    int     firstHour         = 0;
  public    int     firstMinute       = 0;
  public    int     firstSecond       = 0;

  public    int     lastHour          = 23;
  public    int     lastMinute        = 59;
  public    int     lastSecond        = 59;

  
  public  String      months[] = 
                      { 
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December" 
                      };
  
  public DateSQLJBean()
  {
  }
  
  public synchronized void initDate(int fromDayOffset, int toDayOffset)
  {
    try
    {
      if( ! initialized)
      {
        // set up the initial date to be the current date, but offset
        Calendar fromCal = Calendar.getInstance();
        currentYear = fromCal.get(Calendar.YEAR);
        fromCal.add(Calendar.DATE, fromDayOffset);
    
        Calendar toCal = Calendar.getInstance();
        toCal.add(Calendar.DATE, toDayOffset);
    
        // set data items
        fromMonth = fromCal.get(Calendar.MONTH);
        fromDay   = fromCal.get(Calendar.DAY_OF_MONTH);
        fromYear  = fromCal.get(Calendar.YEAR);
        toMonth   = toCal.get(Calendar.MONTH);
        toDay     = toCal.get(Calendar.DAY_OF_MONTH);
        toYear    = toCal.get(Calendar.YEAR);
      
        initialized = true;
      }
    }
    catch(Exception e)
    {
      logEntry("initDate(" + fromDayOffset + ", " + toDayOffset + ")", e.toString());
    }
  }
  
  public synchronized void setFromDay(String fromDay)
  {
    try
    {
      this.fromDay = Integer.parseInt(fromDay);
    }
    catch(Exception e)
    {
    }
  }
  public synchronized void setFromMonth(String fromMonth)
  {
    try
    {
      this.fromMonth = Integer.parseInt(fromMonth);
    }
    catch(Exception e)
    {
    }
  }
  public synchronized void setFromYear(String fromYear)
  {
    try
    {
      this.fromYear = Integer.parseInt(fromYear);
    }
    catch(Exception e)
    {
    }
  }
  public synchronized void setToDay(String toDay)
  {
    try
    {
      this.toDay = Integer.parseInt(toDay);
    }
    catch(Exception e)
    {
    }
  }
  public synchronized void setToMonth(String toMonth)
  {
    try
    {
      this.toMonth = Integer.parseInt(toMonth);
    }
    catch(Exception e)
    {
    }
  }
  public synchronized void setToYear(String toYear)
  {
    try
    {
      this.toYear = Integer.parseInt(toYear);
    }
    catch(Exception e)
    {
    }
  }
  
  public synchronized Date getFromDate()
  {
    Calendar cal = Calendar.getInstance();
    
    try
    {
      cal.clear();
      //sets year, month, and day but at very start of day
      cal.set(fromYear, fromMonth, fromDay, firstHour, firstMinute, firstSecond);

      //cal.set(Calendar.MONTH, fromMonth);
      //cal.set(Calendar.DAY_OF_MONTH, fromDay);
      //cal.set(Calendar.YEAR, fromYear);

    }
    catch(Exception e)
    {
      logEntry("getFromDate(): ", e.toString());
    }
    
    return cal.getTime();
  }
  public synchronized Date getToDate()
  {
    Calendar cal = Calendar.getInstance();
    
    try
    {
      cal.clear();
      //sets year, month, and day but at very end of day
      cal.set(toYear, toMonth, toDay, lastHour, lastMinute, lastSecond);

      //cal.set(Calendar.MONTH, toMonth);
      //cal.set(Calendar.DAY_OF_MONTH, toDay);
      //cal.set(Calendar.YEAR, toYear);
    }
    catch(Exception e)
    {
      logEntry("getFromDate(): ", e.toString());
    }
    
    return cal.getTime();
  }
  public synchronized java.sql.Date getSqlFromDate()
  {
    return(new java.sql.Date(getFromDate().getTime()));
  }
  public synchronized java.sql.Date getSqlToDate()
  {
    return(new java.sql.Date(getToDate().getTime()));
  }
  
  public synchronized void setProperties( HttpServletRequest request )
  {
    try
    {
      // set from date
      fromMonth = Integer.parseInt(request.getParameter("fromMonth"));
      fromDay   = Integer.parseInt(request.getParameter("fromDay"));
      fromYear  = Integer.parseInt(request.getParameter("fromYear"));
    }
    catch (Exception e)
    {
    }
    
    try
    {
      // set to date
      toMonth = Integer.parseInt(request.getParameter("toMonth"));
      toDay   = Integer.parseInt(request.getParameter("toDay"));
      toYear  = Integer.parseInt(request.getParameter("toYear"));
    }
    catch (Exception e)
    {
    }
  }
}
