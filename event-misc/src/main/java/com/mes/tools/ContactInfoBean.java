/*@lineinfo:filename=ContactInfoBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/ContactInfoBean.sqlj $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 12 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class ContactInfoBean extends SQLJConnectionBase
{
  // private data
  private Vector          contactItems  = new Vector();
  
  private int userId      = 0;
  
  // helper class
  public class ContactItem
  {
    String label;
    String value;

    public ContactItem(String label, String value)
    {
      this.label = label;
      
      this.value = (value == null) ? "" : value;
    }

    public String getLabel()
    {
      return this.label;
    }

    public String getValue()
    {
      return this.value;
    }
  }

  public ContactInfoBean()
  {
  }
  
  public void fillContactData()
  {
    ResultSetIterator   it      = null;
    StringBuffer        work    = new StringBuffer("");
    boolean             isRep   = false;
    String              repId   = "";
    
    try
    {
      connect();
      
      // get all pertinent user data
      /*@lineinfo:generated-code*//*@lineinfo:86^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.login_name        login_name,
//                  u.name              name,
//                  u.address1          address1,
//                  u.address2          address2,
//                  u.city              city,
//                  u.state             state,
//                  u.zip               zip,
//                  u.email             email,
//                  u.phone             phone,
//                  u.fax               fax,
//                  u.mobile            mobile,
//                  u.pager             pager,
//                  u.pager_pin         pager_pin,
//                  u.bank_group1       bank_group1,
//                  u.bank_group2       bank_group2,
//                  u.bank_group3       bank_group3,
//                  u.bank_group4       bank_group4,
//                  u.bank_group5       bank_group5,
//                  u.bank_group6       bank_group6,
//                  u.bank_assoc        bank_assoc,
//                  u.bank_num          bank_num,
//                  u.comments          comments,
//                  u.hierarchy_node    hierarchy_node,
//                  o.org_name          node_name,
//                  r.rep_id            rep_id
//          from    users               u,
//                  organization        o,
//                  sales_rep           r
//          where   u.user_id           = :this.userId and
//                  u.hierarchy_node    = o.org_group and
//                  u.user_id           = r.user_id(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  u.login_name        login_name,\n                u.name              name,\n                u.address1          address1,\n                u.address2          address2,\n                u.city              city,\n                u.state             state,\n                u.zip               zip,\n                u.email             email,\n                u.phone             phone,\n                u.fax               fax,\n                u.mobile            mobile,\n                u.pager             pager,\n                u.pager_pin         pager_pin,\n                u.bank_group1       bank_group1,\n                u.bank_group2       bank_group2,\n                u.bank_group3       bank_group3,\n                u.bank_group4       bank_group4,\n                u.bank_group5       bank_group5,\n                u.bank_group6       bank_group6,\n                u.bank_assoc        bank_assoc,\n                u.bank_num          bank_num,\n                u.comments          comments,\n                u.hierarchy_node    hierarchy_node,\n                o.org_name          node_name,\n                r.rep_id            rep_id\n        from    users               u,\n                organization        o,\n                sales_rep           r\n        where   u.user_id           =  :1  and\n                u.hierarchy_node    = o.org_group and\n                u.user_id           = r.user_id(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.ContactInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.ContactInfoBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:119^7*/
      
      ResultSet rs = it.getResultSet();
      
      if(rs.next())
      {
        contactItems.add(new ContactItem("User Name", rs.getString("name")));
        contactItems.add(new ContactItem("User Login", rs.getString("login_name")));
        
        work.setLength(0);
        work.append(rs.getString("hierarchy_node"));
        work.append(" - ");
        work.append(rs.getString("node_name"));
        
        contactItems.add(new ContactItem("User Node", work.toString()));
        
        contactItems.add(new ContactItem("User Address 1",  rs.getString("address1")));
        contactItems.add(new ContactItem("User Address 2",  rs.getString("address2")));
        contactItems.add(new ContactItem("User City",       rs.getString("city")));
        contactItems.add(new ContactItem("User State",      rs.getString("state")));
        contactItems.add(new ContactItem("User Zip",        rs.getString("zip")));
        
        String email = rs.getString("email");
        
        work.setLength(0);
        if(email != null && ! email.equals(""))
        {
          work.append("<a ref=\"mailto:");
          work.append(email);
          work.append("\">");
          work.append(email);
          work.append("</a>");
        }
        else
        {
          work.append("None Listed");
        }
        contactItems.add(new ContactItem("User Email",      work.toString()));
        
        contactItems.add(new ContactItem("User Phone",  rs.getString("phone")));
        contactItems.add(new ContactItem("User Fax",    rs.getString("fax")));
        contactItems.add(new ContactItem("User Mobile", rs.getString("mobile")));
        contactItems.add(new ContactItem("User Pager",  rs.getString("pager") + " pin " + 
                                                        rs.getString("pager_pin")));
        
        contactItems.add(new ContactItem("Bank Group1", rs.getString("bank_group1")));
        contactItems.add(new ContactItem("Bank Group2", rs.getString("bank_group2")));
        contactItems.add(new ContactItem("Bank Group3", rs.getString("bank_group3")));
        contactItems.add(new ContactItem("Bank Group4", rs.getString("bank_group4")));
        contactItems.add(new ContactItem("Bank Group5", rs.getString("bank_group5")));
        contactItems.add(new ContactItem("Bank Group6", rs.getString("bank_group6")));
        contactItems.add(new ContactItem("Bank Association",  rs.getString("bank_assoc")));
        contactItems.add(new ContactItem("Bank Number", rs.getString("bank_num")));
        contactItems.add(new ContactItem("Comments",    rs.getString("comments")));
        
        if(rs.getString("rep_id") != null)
        {
          isRep = true;
          repId = rs.getString("rep_id");
        }
      }
      
      it.close();
      
      if(isRep)
      {
        // get rep information
        /*@lineinfo:generated-code*//*@lineinfo:186^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  r.rep_id            rep_id,
//                    r.rep_ssn           rep_ssn,
//                    r.rep_title         rep_title,
//                    rt.rep_type_desc    rep_type_desc,
//                    rr.rep_region_desc  rep_region_desc,
//                    rp.rep_plan_desc    rep_plan_desc,
//                    r2.rep_name         manager
//            from    sales_rep           r,
//                    sales_rep           r2,
//                    rep_type            rt,
//                    rep_region          rr,
//                    rep_plan            rp
//            where   r.rep_id            = :repId and
//                    r.rep_type          = rt.rep_type(+) and
//                    r.rep_region        = rr.rep_region(+) and
//                    r.rep_plan_id       = rp.rep_plan_id(+) and
//                    r.rep_report_to     = r2.rep_id(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  r.rep_id            rep_id,\n                  r.rep_ssn           rep_ssn,\n                  r.rep_title         rep_title,\n                  rt.rep_type_desc    rep_type_desc,\n                  rr.rep_region_desc  rep_region_desc,\n                  rp.rep_plan_desc    rep_plan_desc,\n                  r2.rep_name         manager\n          from    sales_rep           r,\n                  sales_rep           r2,\n                  rep_type            rt,\n                  rep_region          rr,\n                  rep_plan            rp\n          where   r.rep_id            =  :1  and\n                  r.rep_type          = rt.rep_type(+) and\n                  r.rep_region        = rr.rep_region(+) and\n                  r.rep_plan_id       = rp.rep_plan_id(+) and\n                  r.rep_report_to     = r2.rep_id(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.ContactInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,repId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.tools.ContactInfoBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:205^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          contactItems.add(new ContactItem("Sales Rep Code",  rs.getString("rep_id")));
          contactItems.add(new ContactItem("SSN",             rs.getString("rep_ssn")));
          contactItems.add(new ContactItem("Title",           rs.getString("rep_title")));
          contactItems.add(new ContactItem("Reports To",      rs.getString("manager")));
          contactItems.add(new ContactItem("Sales Rep Type",  rs.getString("rep_type_desc")));
          contactItems.add(new ContactItem("Sales Region",    rs.getString("rep_region_desc")));
          contactItems.add(new ContactItem("Comp Plan",       rs.getString("rep_plan_desc")));
        }
        
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("::fillContactData()", e.toString());
      contactItems.add(new ContactItem("error", e.toString()));
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void fillSupplierContactData(int supplierId)
  {
    ResultSetIterator it        = null;
    StringBuffer      work      = new StringBuffer("");

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:243^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  es_supplier_name,
//                  es_supplier_street1,
//                  es_supplier_street2,
//                  es_supplier_city,
//                  es_supplier_state,
//                  es_supplier_zip,
//                  es_supplier_contact_email,
//                  es_supplier_contact_name,
//                  es_supplier_contact_phone,
//                  es_supplier_contact_fax
//          from    equip_supplier
//          where   es_supplier_id = :supplierId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  es_supplier_name,\n                es_supplier_street1,\n                es_supplier_street2,\n                es_supplier_city,\n                es_supplier_state,\n                es_supplier_zip,\n                es_supplier_contact_email,\n                es_supplier_contact_name,\n                es_supplier_contact_phone,\n                es_supplier_contact_fax\n        from    equip_supplier\n        where   es_supplier_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.ContactInfoBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,supplierId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.tools.ContactInfoBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:257^7*/
      
      ResultSet rs = it.getResultSet();

      if(rs.next())
      {
        contactItems.add(new ContactItem("Supplier Name", rs.getString("es_supplier_name")));

        contactItems.add(new ContactItem("Address 1",     rs.getString("es_supplier_street1")));
        contactItems.add(new ContactItem("Address 2",     rs.getString("es_supplier_street2")));
        contactItems.add(new ContactItem("City",          rs.getString("es_supplier_city")));
        contactItems.add(new ContactItem("State",         rs.getString("es_supplier_state")));
        contactItems.add(new ContactItem("Zip",           rs.getString("es_supplier_zip")));
        
        String email = rs.getString("es_supplier_contact_email");
        
        work.setLength(0);
        if(email != null && !email.equals(""))
        {
          work.append("<a ref=\"mailto:");
          work.append(email);
          work.append("\">");
          work.append(email);
          work.append("</a>");
        }
        
        contactItems.add(new ContactItem("Contact Name",   rs.getString("es_supplier_contact_name")));        
        contactItems.add(new ContactItem("Contact Email",  work.toString()));
        contactItems.add(new ContactItem("Contact Phone",  rs.getString("es_supplier_contact_phone")));
        contactItems.add(new ContactItem("Contact Fax",    rs.getString("es_supplier_contact_fax")));
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("::fillSupplierContactData(" + supplierId + ")", e.toString());
      contactItems.add(new ContactItem("error", e.toString()));
    }
    finally
    {
      cleanUp();
    }
  }  
  
  
  public Vector getContactItems()
  {
    return this.contactItems;
  }
  
  public void setUserId(String userId)
  {
    try
    {
      setUserid(Integer.parseInt(userId));
    }
    catch(Exception e)
    {
      logEntry("setUserId(" + userId + ")", e.toString());
    }
  }
  public void setUserid(String userId)
  {
    try
    {
      setUserid(Integer.parseInt(userId));
    }
    catch(Exception e)
    {
      logEntry("setUserid(" + userId + ")", e.toString());
    }
  }
  public void setUserid(int userId)
  {
    this.userId = userId;
  }
  
  public void setUserName(String userName)
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:341^7*/

//  ************************************************************
//  #sql [Ctx] { select  user_id
//          
//          from    users
//          where   login_name = :userName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  user_id\n         \n        from    users\n        where   login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tools.ContactInfoBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,userName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.userId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:347^7*/
    }
    catch(Exception e)
    {
      logEntry("setUserName(" + userName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void setId(String id)
  {
    try
    {
      setId(Integer.parseInt(id));
    }
    catch(Exception e)
    {
      logEntry("setId(" + id + ")", e.toString());
    }
  }
  public void setId(int id)
  {
    this.userId = id;
  }
}/*@lineinfo:generated-code*/