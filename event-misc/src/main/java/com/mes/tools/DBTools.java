/*@lineinfo:filename=DBTools*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.tools;

import java.sql.ResultSet;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class DBTools extends SQLJConnectionBase
{
  public DBTools()
  {
  }
  
  public int _getFileBankNumber(String fileName)
  {
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    int               bankNumber    = -1;
    int               prefixLength  = 0;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:26^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  lft.file_prefix           as prefix
//          from    mes_load_file_types     lft           
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  lft.file_prefix           as prefix\n        from    mes_load_file_types     lft";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.DBTools",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.DBTools",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:30^7*/
      rs = it.getResultSet();
      
      while(rs.next())
      {
        prefixLength = rs.getString("prefix").length();
        
        if(fileName.length() >= prefixLength &&
           fileName.substring(0,prefixLength).equals(rs.getString("prefix")))
        {
          bankNumber = Integer.parseInt(fileName.substring(prefixLength,(prefixLength+4)));
          break;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getFileBankNumber(" + fileName + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( bankNumber );
  }
  
  public long _loadFilenameToLoadFileId( String fileName )
  {
    long retVal = 0L;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:67^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :fileName )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.DBTools",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fileName);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:70^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:72^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:fileName)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.DBTools",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fileName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:77^7*/
    }
    catch(Exception e)
    {
      logEntry("loadFilenameToLoadFileId(" + fileName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( retVal );
  }
  
  public static int getFileBankNumber(String fileName, DefaultContext myCtx)
  {
    DBTools db = new DBTools();
    
    if(myCtx != null)
    {
      db.Ctx = myCtx;
    }
    
    return ( db._getFileBankNumber(fileName) );
  }
  
  public static long loadFilenameToLoadFileId( String fileName, DefaultContext myCtx)
  {
    DBTools db = new DBTools();
    
    if(myCtx != null)
    {
      db.Ctx = myCtx;
    }
    
    return ( db._loadFilenameToLoadFileId( fileName ) );
  }
  
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone();
      System.out.println(args[0] + ": " + getFileBankNumber(args[0], null));
    }
    catch(Exception e)
    {
      System.out.println("EXCEPTION: " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/