package com.mes.support;

import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.Vector;

/**
 * class TextMessageManager
 * 
 * Standalone class to manage a collection of text messages.
 */
public class TextMessageManager
{
  // constants
  // (none)
  
  // data members
  protected Vector        msgs;     // collection of messages
  
  
  /**
   * getNumMsgs()
   * 
   * Returns the total number of messages held in the TextMessageManager instance irregardless of the message type.
   */
  public int getNumMsgs()
  {
    return msgs.size();
  }
  
  public Enumeration getTxtMsgEnumeration(int txtmsg_type)
  {
    return new TxtMsgTypeEnumerator("",txtmsg_type);
  }
  
  /**
   * TxtMsgTypeEnumerator member class.
   * 
   * Text Message specific Enumerator in order to allow for filter-based enumerations.
   * Filter-based by way of discriminating text messages by their type.
   */
  protected class TxtMsgTypeEnumerator implements Enumeration
  {
    // data members
    int txtmsg_type;
    String client;
    Object current;
      
    // construction
    public TxtMsgTypeEnumerator(String client,int txtmsg_type)
    {
      this.txtmsg_type=txtmsg_type;
      this.client=client;
      this.current=null;
      
      if(msgs.size()<1)
        return;
      
      TextMessage tm;
      
      // iterate to current
      for(int i=0;i<msgs.size();i++) {
        tm=(TextMessage)msgs.elementAt(i);
        if(tm.getType()==txtmsg_type) {
          if(client!=null && client.length()>0) {
            // filter based on client
            if(!tm.getClient().equals(client))
              continue;
          }
          current=tm;
          break;
        }
      }

    }
    
    public boolean hasMoreElements()
    {
      return (current!=null);
    }
    
    public Object nextElement()
    {
      if(current==null)
        throw new NoSuchElementException("TextMessage");
      
      final Object value=current;
      
      current=null;
      
      // increment
      int i = msgs.indexOf(value);  // assume valid
      
      i++;  // goto the one after current
      
      TextMessage tm;
      
      while(i<msgs.size()) {
        tm=(TextMessage)msgs.elementAt(i);
        if(tm.getType()==txtmsg_type) {
          if(client!=null && client.length()>0) {
            // filter based on client
            if(!tm.getClient().equals(client))
              continue;
          }
          current=msgs.elementAt(i);
          break;
        } else
          i++;
      }

      return value;
    }
  
  } // Enumerator member class
  
  
  // class methods
  // (none)
  
  // object methods
  
  // construction
  public TextMessageManager()
  {
    msgs=new Vector(0,5);
  }
  
  
  /**
   * msg()
   * 
   * Posts an INFO type text message.
   */
  public void msg(String s)
  {
    if(s!=null && s.length()>0)
      msgs.addElement(new TextMessage(s,TextMessage.TXTMSGTYPE_INFO));
  }
  
  /**
   * wrn()
   * 
   * Posts a WARN type text message.
   */
  public void wrn(String s)
  {
    if(s!=null && s.length()>0)
      msgs.addElement(new TextMessage(s,TextMessage.TXTMSGTYPE_WARN));
  }
  
  /**
   * err()
   * 
   * Posts an ERROR type text message.
   */
  public void err(String s)
  {
    if(s!=null && s.length()>0)
      msgs.addElement(new TextMessage(s,TextMessage.TXTMSGTYPE_ERROR));
  }
  
  /**
   * clear()
   */
  public void clear()
  {
    this.msgs.removeAllElements();
  }
  public void clear(int txtmsg_type)
  {
    if(msgs.size()<1)
      return;
    
    for(int i=0;i<msgs.size();i++) {
      if(((TextMessage)msgs.elementAt(i)).getType()==txtmsg_type)
        msgs.removeElementAt(i--);
    }
  }

} // class TextMessage
