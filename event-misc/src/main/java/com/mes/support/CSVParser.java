/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/CSVParser.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 8/03/04 5:16p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Date;

public class CSVParser
{
  private   BufferedReader          InputFile         = null;
  protected int                     InputIndex        = 0;
  protected String                  InputLine         = null;
  
  // protected because this constructor should never be called
  protected CSVParser()
  {
  }

  public CSVParser( String filename )
    throws java.io.FileNotFoundException
  {
    InputFile = new BufferedReader( new FileReader( filename ) );
  }
  
  public void cleanUp()
  {
    try{ InputFile.close(); } catch( Exception e ) {}
    InputFile = null;
    InputLine = null;
  }
  
  public boolean nextLine()
  {
    try
    {
      if ( (InputLine = InputFile.readLine()) != null )
      {
        InputIndex = 0;
      }
    }
    catch( Exception e )
    {
      System.out.println( e.toString() );
    }
    finally
    {
    }      
    return( InputLine != null );
  }
  
  public Date nextTokenAsDate()
  {
    return( nextTokenAsDate("MM/dd/yyyy") );
  }
  
  public Date nextTokenAsDate( String format )
  {
    Date      retVal      = null;
    String    temp        = nextTokenAsString();
    
    if ( temp.length() > 0 )
    {
      retVal = DateTimeFormatter.parseDate( temp, format );
    }      
    return( retVal );
  }
  
  public double nextTokenAsDouble()
  {
    double                  retVal  = 0.0;
    String                  token   = nextTokenAsString();
    StringBuffer            temp    = new StringBuffer();
    
    for( int i = 0; i < token.length(); ++i )
    {
      switch( token.charAt(i) )
      {
        // automatically remove currency related data
        case '$':
        case ',':
        case ' ':
          break;
        
        default:
          temp.append(token.charAt(i));
          break;
      }
    }
    if ( temp.length() > 0 )
    {
      if ( temp.charAt(0) == '(' )
      {
        temp.setCharAt(0,'-');
      }
      if ( temp.charAt( temp.length()-1 ) == ')' )
      {
        temp.deleteCharAt( temp.length()-1 );
      }
      
      if ( !temp.toString().equals("-") )     // empty currency in Excel format
      {
        retVal = Double.parseDouble(temp.toString());
      }        
    }
    return( retVal );
  }
  
  public int nextTokenAsInt()
  {
    return( (int)nextTokenAsDouble() );
  }
  
  public long nextTokenAsLong()
  {
    return( (long)nextTokenAsDouble() );
  }
  
  public String nextTokenAsString()
  {
    char                  ch;
    boolean               done            = false;
    StringBuffer          token           = new StringBuffer();
    boolean               stringStarted   = false;
    
    try
    {
      while( (InputIndex < InputLine.length()) && !done )
      {
        ch = InputLine.charAt(InputIndex++);
        switch( ch )
        {
          case '"':
            stringStarted = !stringStarted;
            break;
            
          case ',':
            if ( stringStarted == true )
            { 
              token.append(ch);
            }
            else
            {
              done = true;
            }
            break;
            
          default:
            token.append(ch);
            break;
        }
      }
    }
    catch( Exception e )
    {
      // return blank if there is no token
      token.setLength(0);
    }
    return( token.toString() );
  }
}