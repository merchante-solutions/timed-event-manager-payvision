package com.mes.support;

import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;


public class HttpTextMessageManager extends TextMessageManager
{
  // data members
  private HttpServletRequest        httpRequest         = null;
  
  // construction
  public HttpTextMessageManager()
  {
    super();
  }
  
  public void setHttpRequest(HttpServletRequest v)
  {
    if(v!=null)
      httpRequest=v;
  }
  
  private String discoverHttpClient()
  {
    if(httpRequest==null)
      return "";
    
    return httpRequest.getRequestedSessionId();
  }
  
  public int getNumMsgs()
  {
    int count = 0;
    TextMessage tm;
    String client = discoverHttpClient();
    
    for(int i=0;i<msgs.size();i++) {
      tm=(TextMessage)msgs.elementAt(i);
      if(tm.getClient().equals(client))
        count++;
    }
    
    return count;
  }

  public Enumeration getTxtMsgEnumeration(int txtmsg_type)
  {
    return new TxtMsgTypeEnumerator(discoverHttpClient(),txtmsg_type);
  }

  public void msg(String s)
  {
    String client = discoverHttpClient();
    
    if(s!=null && s.length()>0 && client!=null && client.length()>0)
      msgs.addElement(new TextMessage(s,TextMessage.TXTMSGTYPE_INFO,client));
  }
  public void wrn(String s)
  {
    String client = discoverHttpClient();
    
    if(s!=null && s.length()>0 && client != null && client.length()>0)
      msgs.addElement(new TextMessage(s,TextMessage.TXTMSGTYPE_WARN,client));
  }
  public void err(String s)
  {
    String client = discoverHttpClient();
    
    if(s!=null && s.length()>0 && client != null && client.length()>0)
      msgs.addElement(new TextMessage(s,TextMessage.TXTMSGTYPE_ERROR,client));
  }
  
  public void clear()
  {
    if(msgs.size()<1)
      return;
    
    TextMessage tm;
    String client = discoverHttpClient();
    
    for(int i=0;i<msgs.size();i++) {
      tm=(TextMessage)msgs.elementAt(i);
      if(((TextMessage)msgs.elementAt(i)).getClient().equals(client))
        msgs.removeElementAt(i--);
    }
  }
  
} // class HttpTextMessageAdder
