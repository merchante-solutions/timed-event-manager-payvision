package com.mes.support;

public class TestServer
{
  public static final String  DOT_NET_URL = "www\\.merchante\\-solutions\\.net";
  /*
  ** transforms .net URL if test server environment
  */
  public static String transformDotNetURL(String baseURL)
  {
    String retVal = "";
    
    // replace instances of 'www.merchante-solutions.net' with encoded value
    String replaceVal = HttpHelper.getDotNetURL(null);
    
    retVal = baseURL.replaceAll(DOT_NET_URL, replaceVal);
    
    return( retVal );
  }
}