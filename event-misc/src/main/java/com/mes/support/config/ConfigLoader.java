package com.mes.support.config;

import java.io.File;

public class ConfigLoader {

  public static Config getFileConfig(File file) {
  
    return new Config(new FileConfigSource(file));
  }
}