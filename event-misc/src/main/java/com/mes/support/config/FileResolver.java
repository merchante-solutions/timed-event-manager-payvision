package com.mes.support.config;

import java.io.File;
import org.apache.log4j.Logger;

public class FileResolver {

  static Logger log = Logger.getLogger(FileResolver.class);
  
  /**
   * Allows an app to determine a file from several sources: from a system
   * property name or a file path string. Suggested uses: command line apps
   * that want to allow a default file to be overriden via command line 
   * arguments, etc.  If system property name is provided it will be 
   * prioritized over any file path string.
   */
  public static File resolveFile(String sysPropName, String filePathString)
    throws Exception
  {
  
    File file = null;
    
    if (sysPropName == null && filePathString == null) {
    
      throw new NullPointerException("Null system property and file name");
    }
    
    String filename = null;
    if (sysPropName != null) {
    
      filename = System.getProperty(sysPropName);
      log.debug("Resolved filename from system property " 
        + sysPropName + ": " + filename);
    }
    else {
    
      filename = filePathString;
      log.debug("Resolved filename from file path: " + filePathString);
    }
    
    File resolvedFile = new File(filename);
    if (!resolvedFile.exists()) {
    
      throw new Exception("File does not exist: " + resolvedFile);
    }
    
    if (resolvedFile.isDirectory()) {
    
      throw new Exception("File is directory: " + resolvedFile);
    }
    
    return resolvedFile;
  }
}