/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/CSVUploadParser.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 8/03/04 5:16p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.util.StringTokenizer;
import org.apache.commons.fileupload.FileItem;

public class CSVUploadParser extends CSVParser
{
  StringTokenizer Lines       = null;
  
  public CSVUploadParser(FileItem file)
  {
    Lines = new StringTokenizer(file.getString(), "\n");
  }
  
  public boolean nextLine()
  {
    boolean result = false;
    if(Lines.hasMoreTokens())
    {
      InputLine = Lines.nextToken();
      InputIndex = 0;
      result = true;
    }
    
    return result;
  }
}