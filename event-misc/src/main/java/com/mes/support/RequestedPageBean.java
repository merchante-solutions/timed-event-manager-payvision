/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/RequestedPageBean.java $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 9/19/02 10:50a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;

public class RequestedPageBean
  implements Serializable
{
  private   String    servletPath   = "";
  private   String    queryString   = "";
  
  public RequestedPageBean()
  {
    
  }
  
  public synchronized void setRequestedPage(HttpServletRequest request)
  {
    try
    {
      servletPath = request.getServletPath();
      queryString = request.getQueryString();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::setRequestedPage()", e.toString());
    }
  }
  
  public synchronized String getRequestedPage()
  {
    StringBuffer  requestedPage = new StringBuffer("");
    
    if(servletPath != null && ! servletPath.equals(""))
    {
      requestedPage.append(servletPath);
      
      if(queryString != null && ! queryString.equals(""))
      {
        requestedPage.append("?");
        requestedPage.append(queryString);
      }
    }
    
    return requestedPage.toString();
  }
  
  public synchronized String getServletPath()
  {
    return servletPath;
  }
  
  public synchronized String getQueryString()
  {
    return queryString;
  }
}
