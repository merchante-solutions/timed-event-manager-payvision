package com.mes.support;

/**
 * class TextMessage
 * 
 * Encapsulates and represents text messages.
 * One instance corres. to a single text message.
 */
public final class TextMessage extends Object
{
  // constants
  // text message type
  private static final int   TXTMSGTYPE_START           = 0;
  public static final int    TXTMSGTYPE_UNDEFINED       = TXTMSGTYPE_START;
  public static final int    TXTMSGTYPE_INFO            = 1;
  public static final int    TXTMSGTYPE_WARN            = 2;
  public static final int    TXTMSGTYPE_ERROR           = 3;
  private static final int   TXTMSGTYPE_END             = TXTMSGTYPE_ERROR;
  
  // data members
  private String        tmsg            = "";       
    // text message string
  private int           type            = TXTMSGTYPE_UNDEFINED;
    // TXTMSGTYPE_{...}
  private String        client          = "";
    // unique name of client that incurred this message
  
  
  // class methods
  // (none)
  
  // object methods
  
  // construction
  public TextMessage()
  {
  }
  public TextMessage(String tmsg)
  {
    this.tmsg=tmsg;
  }
  public TextMessage(String tmsg,int type)
  {
    this.tmsg=tmsg;
    this.type=type;
  }
  public TextMessage(String tmsg,int type,String client)
  {
    this.tmsg=tmsg;
    this.type=type;
    this.client=client;
  }
  
  // accessors
  
  public String getMessage()
  {
    return tmsg;
  }
  
  public int getType()
  {
    return type;
  }
  
  public String getClient()
  {
    return client;
  }
  
  public boolean msgExists()
  {
    return (tmsg.length()>0);
  }
  
  // mutators
  
  public void setMessage(String tmsg)
  {
    if(tmsg!=null)
      this.tmsg=tmsg;
  }
  
  /*
  public void setMessage(String tmsg,int type)
  {
    if(tmsg==null || (type<TXTMSGTYPE_START || type>TXTMSGTYPE_END))
      return;
    
    this.tmsg=tmsg;
    this.type=type;
  }
  */
  
  public void setMessage(String tmsg,int type,String client)
  {
    if(tmsg==null || (type<TXTMSGTYPE_START || type>TXTMSGTYPE_END))
      return;
    
    this.tmsg=tmsg;
    this.type=type;
    this.client=client;
  }

  public void clearMessage()
  {
    this.tmsg="";
    this.type=TXTMSGTYPE_UNDEFINED;
    this.client="";
  }

} // class TextMessage
