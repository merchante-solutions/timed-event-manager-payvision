/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/AgentBase.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 4/15/03 3:01p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

public abstract class AgentBase
{
  public static final int   ABF_HTML          = 0;
  public static final int   ABF_TEXT          = 1;
  public static final int   ABF_CSV           = 2;
  
  public byte[] execute( )
  {
    return( execute( ABF_HTML ) );
  }
  
  public abstract byte[] execute( int formatCode );
}