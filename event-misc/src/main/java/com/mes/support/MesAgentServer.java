/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/MesAgentServer.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.StringTokenizer;

public class MesAgentServer
  implements Runnable
{
  private static class CommandMapEntry
  {
    public String      Classname     = null;
    public String      Filename      = null;
    
    CommandMapEntry( String fname, String cname )
    {
      Filename  = fname;
      Classname = cname;
    }
  }
  
  private CommandMapEntry[]   CommandMap = 
  {
    new CommandMapEntry("/"         , "com.mes.support.MesAgents$PingAgent" ),
    new CommandMapEntry("/version"  , "com.mes.support.MesAgents$VersionAgent" ),
  };
  
  private Thread              AgentThread         = null;
  private String              AssignedTo          = null;
  private int                 HostPort            = 0;
    
  public MesAgentServer( )
  {
    WebLogicPropertiesFile  propFile    = null;
    
    try
    {
      propFile = new WebLogicPropertiesFile("mes.properties");
    }
    catch(Exception e)
    {
      InputStream inputStream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream("resources/mes.properties");      
        
      propFile = new WebLogicPropertiesFile( inputStream );

      try { inputStream.close(); } catch(Exception re) {}
    }
    
    HostPort    = propFile.getInt("com.mes.agentport", 0 );
    AssignedTo  = propFile.getString("com.mes.hostname", "<unknown>");
    
    AgentThread = new Thread(this); 
    AgentThread.start();
  }
  
  public void processRequest( OutputStream outStream, String className )
  {
    AgentBase                 agent   = null;
    Class                     c       = null;
    byte[]                    buf     = null;
    StringBuffer              hdr     = new StringBuffer();
    OutputStreamWriter        out     = null;
    
    try
    {
      c     = Class.forName(className);
      agent = (AgentBase) c.newInstance();
      buf   = agent.execute();
      
      if( buf != null && buf.length > 0 )
      {
        out = new OutputStreamWriter( outStream );
    
	      // Response header components:
	      // 1 - HTTP 1.0 response header
	      // 2 - Server time
	      // 3 - Server version
	      // 4 - MIME version
	      // 5 - MIME type
	      // 6 - Last-modified time
	      // 7 - Content length
	      // 8 - HTTP 1.0 End-of-header

	      // 1
        hdr.setLength(0);
        hdr.append("HTTP/1.0 200 OK\r\n");
        out.write(hdr.toString(),0,hdr.length());
        out.flush();

	      // 2
        hdr.setLength(0);
        hdr.append( "Date: " );
        hdr.append( DateTimeFormatter.getFormattedDate( Calendar.getInstance().getTime(), "MM/dd/yyyy hh:mm:ss" ) );
        hdr.append( "\r\n" );
        out.write(hdr.toString(),0,hdr.length());
        out.flush();

	      // 3
	      hdr.setLength(0);
        hdr.append("Server: MeS Support/1.0\r\n");
        out.write(hdr.toString(),0,hdr.length());
        out.flush();

	      // 4
	      hdr.setLength(0);
	      hdr.append("MIME-version: 1.0\r\n");
        out.write(hdr.toString(),0,hdr.length());
        out.flush();

	      // 5
	      hdr.setLength(0);
	      hdr.append("Content-type: text/html\r\n");
        out.write(hdr.toString(),0,hdr.length());
        out.flush();

	      // 6
	      hdr.setLength(0);
	      hdr.append("Last-modified: ");
        hdr.append( DateTimeFormatter.getFormattedDate( Calendar.getInstance().getTime(), "MM/dd/yyyy hh:mm:ss" ) );
        hdr.append( "\r\n" );
        out.write(hdr.toString(),0,hdr.length());
        out.flush();

	      // 7
	      hdr.setLength(0);
	      hdr.append("Content-length: ");
        hdr.append( buf.length );
        hdr.append("\r\n");
        out.write(hdr.toString(),0,hdr.length());
        out.flush();

	      // 8
	      hdr.setLength(0);
	      hdr.append("\r\n" );	// end-of-header
        out.write(hdr.toString(),0,hdr.length());
        out.flush();

        // just write to the raw output stream
        outStream.write(buf);
        outStream.flush();
      }
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }      
    finally
    {
      // encourage garbage collection
      c     = null;
      agent = null;
    }
  }
  
  public String readRequest( InputStream inStream )
  {
    String                filename    = null;
    BufferedReader        in          = null;
    String                line        = null;
    int                   lineNum     = 0;
    String                retVal      = null;
    
    try
    {
      in = new BufferedReader( new InputStreamReader( inStream ) );
    
      while( (line = in.readLine()) != null )
      {
        ++lineNum;
      
        switch( lineNum )
        {
          case 1:     // HTTP command must be in first line
            if ( line.toUpperCase().startsWith("GET") )
            {
              StringTokenizer   tokens = new StringTokenizer(line," ");
          
              tokens.nextToken();
              filename = tokens.nextToken();
          
              System.out.println( "Requesting: " + filename );
          
              for( int i = 0; i < CommandMap.length; ++i )
              {
                if( CommandMap[i].Filename.equals(filename) )
                {
                  retVal = CommandMap[i].Classname;
                  break;
                }
              }
            }
            break;
          
          default:
            break;
        }
        if ( line.equals("") )
        {
          break;
        }
        System.out.println(line);
      }
      System.out.println();
      System.out.println("End Input");
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
    }      
    
    return( retVal );
  }
  
  public void run( )
  {
    Socket                    s       = null;
    ServerSocket              server  = null;
  
    try
    {
      server = new ServerSocket(HostPort);
      
      while( true )
      {
        s = server.accept();    // wait for a connection
        
        if ( s != null )
        {
          processRequest( s.getOutputStream(), readRequest( s.getInputStream() ) );
          s.close();
        }
      } 
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
    }
  }
}  