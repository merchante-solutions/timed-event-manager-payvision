/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/MesAgents.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 4/15/03 3:02p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

public class MesAgents    // just a collection of small classes
{
  public static class PingAgent extends AgentBase
  {
    public byte[] execute( int formatCode )
    {
      StringBuffer              buf     = new StringBuffer();
  
      // load the html buffer
      buf.append("<html>\r\n");
      buf.append("<head>\r\n");
      buf.append("</head>\r\n");
      buf.append("<body>\r\n");
      buf.append("  Agent ID: ");
      buf.append( this.toString() );
      buf.append("\r\n");
      buf.append("<br>\r\n");
      buf.append("\r\n");
      buf.append("</body>");
      buf.append("</html>");
    
      return( buf.toString().getBytes() );
    }
  }
  
  public static class VersionAgent extends AgentBase
  {
    public byte[] execute( int formatCode )
    {
      StringBuffer              buf     = new StringBuffer();
  
      // load the html buffer
      buf.append("<html>\r\n");
      buf.append("<head>\r\n");
      buf.append("</head>\r\n");
      buf.append("<body>\r\n");
      buf.append("  Copyright(r) Merchant e-Solutions, Inc.\r\n");
      buf.append("  <br>                                   \r\n");
      buf.append("  2000, 2001, 2002, 2003                 \r\n");
      buf.append("  <br>                                   \r\n");
      buf.append("  All Rights Reserved                    \r\n");
      buf.append("  <br>                                   \r\n");
      buf.append("  System Version: ");
      buf.append( MesSystem.MES_SYSTEM_VERSION );
      buf.append("</body>");
      buf.append("</html>");
    
      return( buf.toString().getBytes() );
    }
  }
}  