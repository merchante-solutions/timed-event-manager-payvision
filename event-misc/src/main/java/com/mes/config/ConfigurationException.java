package com.mes.config;

/**
 * Created by vbannikov on 2/24/2015.
 */
public class ConfigurationException extends RuntimeException {
    public ConfigurationException(Exception e) throws Exception {
        super(e.getMessage());
    }

    public ConfigurationException() throws Exception {
        super();
    }
}
