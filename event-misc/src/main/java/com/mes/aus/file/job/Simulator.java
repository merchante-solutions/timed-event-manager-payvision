package com.mes.aus.file.job;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.AusAccount;
import com.mes.aus.AusBase;
import com.mes.aus.AusDb;
import com.mes.aus.InboundFile;
import com.mes.aus.OutboundFile;
import com.mes.aus.RequestFile;
import com.mes.aus.RequestFileBatch;
import com.mes.aus.RequestRecord;
import com.mes.aus.RequestRecordHandler;
import com.mes.aus.ResponseRecord;
import com.mes.aus.SysUtil;
import com.mes.aus.file.AbuObfGenerator;
import com.mes.aus.file.DauObfGenerator;
import com.mes.aus.file.GenUtil;
import com.mes.aus.file.ReqfProcessor;
import com.mes.aus.file.RspfGenerator;
import com.mes.aus.file.VauObfGenerator;
import com.mes.config.MesDefaults;

public class Simulator extends Job implements RequestRecordHandler {
	static Logger log = Logger.getLogger(Simulator.class);
	private boolean directFlag;
	static final int MAX_FREQUENT_ERRORS = 5;
	static final int MAX_TOTAL_ERRORS = 100;
	static final long REPORT_INTERVAL_MS = 60000L;
	// if too many errors happen within this interval
	// it will trigger shut down
	static final long FREQUENT_ERROR_MS = 15000L;

	private Calendar lastErrorTs;
	private Calendar lastReportTs;
	private int totalErrorCount;
	private int recentErrorCount;

	public Simulator(boolean directFlag) {
		super(directFlag);
		this.directFlag = directFlag;
	}
	public Simulator() {
		this(true);
	}
	private void logReport() {
		StringBuffer buf = new StringBuffer();
		buf.append("\nSTATUS REPORT");
		buf.append("\n-------------");
		buf.append("\nRecent Error Count: " + recentErrorCount);
		buf.append("\nTotal Error Count:  " + totalErrorCount);
		log.debug("" + buf);
		lastReportTs = Calendar.getInstance();
	}
	/**
	 * Generate a new valid exp date, derived from old date
	 */
	private String generateNewExpDate(String expDate) {
		// generate a new exp date that is not expired
		Calendar cal = Calendar.getInstance();
		int curYear = cal.get(cal.YEAR) % 100;
		int expYear = Integer.parseInt(expDate.substring(0, 2));
		while (expYear < curYear + 3)
			expYear += 3;
		expYear %= 100;
		return GenUtil.zeroPad("" + expYear, 2) + expDate.substring(2, 4);
	}

	private ResponseRecord createResponse(RequestRecord request, String rspCode, InboundFile ibf, String newAcctNum) {
		// create response record
		ResponseRecord response = new ResponseRecord();
		response.setCreateDate(Calendar.getInstance().getTime());
		response.setOldAcct(request.getAcct());
		response.setReqfId(request.getReqfId());
		response.setReqId(request.getReqId());
		response.setIbId(ibf.getIbId());
		response.setObId(ibf.getObId());
		response.setSysCode(ibf.getSysCode());
		response.setMerchNum(request.getMerchNum());
		response.setRspCode(rspCode);
		response.setTestFlag(AusBase.FLAG_YES);
		response.setCardId(request.getCardId());

		// new account data needed for some rsp codes
		if (SysUtil.RC_NEWACCT.equals(rspCode) || SysUtil.RC_NEWEXP.equals(rspCode) || SysUtil.RC_OVERRIDE.equals(rspCode)) {
			// make sure a new account num was provided
			if (newAcctNum == null) {
				throw new NullPointerException("Null account number not allowed for test request " + request);
			}
			// generate a new exp date
			String newExpDate = generateNewExpDate(request.getExpDate());

			// create new acct data for response
			AusAccount newAcct = new AusAccount(newAcctNum, newExpDate, AusAccount.determineAccountType(newAcctNum));
			response.setNewAcct(newAcct);
		}
		return response;
	}

	/**
	 * If request does not contain response generating data, returns null.
	 */
	private ResponseRecord generateResponse(RequestRecord request, InboundFile ibf) throws Exception {
		// test account numbers determine responses
		String cardNum = request.getAccountNum();
		ResponseRecord response = null;

		// abu test cases
		if (SysUtil.SYS_ABU.equals(ibf.getSysCode())) {
			// abu new acct MC1 -> MC2
			if (MesDefaults.getString(MesDefaults.SIMULATOR_MC1).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_NEWACCT, ibf, MesDefaults.getString(MesDefaults.SIMULATOR_MC2));
			}
			// abu new exp MC3
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_MC3).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_NEWEXP, ibf, MesDefaults.getString(MesDefaults.SIMULATOR_MC3));
			}
			// abu closed MC4
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_MC4).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_CLOSED, ibf, null);
			}
			// abu visa to mc VISA1 -> MC5 (new acct from abu)
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_VISA1).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_NEWACCT, ibf, MesDefaults.getString(MesDefaults.SIMULATOR_MC5));
			}
			// abu mc to visa MC6 -> VISA6 (closed from abu)
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_MC6).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_CLOSED, ibf, null);
			}
		}
		// vau test cases
		else if (SysUtil.SYS_VAU.equals(ibf.getSysCode())) {
			// vau visa to mc VISA1 -> MC5 (closed from vau)
			if (MesDefaults.getString(MesDefaults.SIMULATOR_VISA1).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_CLOSED, ibf, null);
			}
			// vau new acct VISA2 -> VISA3
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_VISA2).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_NEWACCT, ibf, MesDefaults.getString(MesDefaults.SIMULATOR_VISA3));
			}
			// vau new exp VISA4
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_VISA4).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_NEWEXP, ibf, MesDefaults.getString(MesDefaults.SIMULATOR_VISA4));
			}
			// vau closed VISA5
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_VISA5).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_CLOSED, ibf, null);
			}
			// vau mc to visa MC6 -> VISA6 (new acct from vau)
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_MC6).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_NEWACCT, ibf, MesDefaults.getString(MesDefaults.SIMULATOR_VISA6));
			}
		}
		// dau test cases
		else if (SysUtil.SYS_DAU.equals(ibf.getSysCode())) {
			// dau new acct DISC1 -> DISC2
			if (MesDefaults.getString(MesDefaults.SIMULATOR_DISC1).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_NEWACCT, ibf,	MesDefaults.getString(MesDefaults.SIMULATOR_DISC2));
			}
			// dau closed acct DISC3
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_DISC3).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_CLOSED, ibf, null);
			}
			// dau new exp date DISC4
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_DISC4).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_NEWEXP, ibf, MesDefaults.getString(MesDefaults.SIMULATOR_DISC4));
			}
			// dau call DISC5
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_DISC5).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_CALL, ibf, MesDefaults.getString(MesDefaults.SIMULATOR_DISC5));
			}
			// dau override DISC6 -> DISC7
			else if (MesDefaults.getString(MesDefaults.SIMULATOR_DISC6).equals(cardNum)) {
				response = createResponse(request, SysUtil.RC_OVERRIDE, ibf, MesDefaults.getString(MesDefaults.SIMULATOR_DISC7));
			}
		}
		++requestCount;
		responseCount += (response != null ? 1 : 0);
		return response;
	}

	private InboundFile curIbf;

	/**
	 * Called from db to process request records. Generate responses based on
	 * the request if needed. Persists them.
	 */
	public void handleRequests(RequestFile reqf, List requests) throws Exception {
		List responses = new ArrayList();
		for (Iterator i = requests.iterator(); i.hasNext();) {
			RequestRecord request = (RequestRecord) i.next();
			ResponseRecord response = generateResponse(request, curIbf);
			if (response != null) {
				responses.add(response);
			}
		}
		db.insertResponseRecords(responses);
		if (requestCount >= nextReport) {
			log.debug("Generating responses, requests: " + requestCount + ", responses: " + responseCount);
			nextReport += interval;
		}
	}

	private int requestCount;
	private int responseCount;
	private int interval;
	private int nextReport;
	private long startTs;

	private String getDurationStr(long durationMs) {
		StringBuffer buf = new StringBuffer();
		long workMs = durationMs;
		long secMs = 1000;
		long minMs = secMs * 60;
		long hourMs = minMs * 60;
		long dayMs = hourMs * 24;
		if (workMs >= dayMs) {
			buf.append("" + (workMs / dayMs) + " days");
			workMs %= dayMs;
		}
		if (workMs >= hourMs) {
			buf.append((buf.length() > 0 ? ", " : ""));
			buf.append("" + (workMs / hourMs) + " hours");
			workMs %= hourMs;
		}
		if (workMs >= minMs) {
			buf.append((buf.length() > 0 ? ", " : ""));
			buf.append("" + (workMs / minMs) + " mins");
			workMs %= minMs;
		}
		if (workMs >= secMs) {
			buf.append((buf.length() > 0 ? ", " : ""));
			buf.append("" + (workMs / secMs) + " secs");
			workMs %= secMs;
		}
		if (workMs >= 1) {
			buf.append((buf.length() > 0 ? ", " : ""));
			buf.append("" + workMs + " ms");
		}
		return "" + buf;
	}

	/**
	 * Generate responses for each request file.
	 */
	private int generateResponseRecords(List reqfList, InboundFile ibf, String sysType) throws Exception {
		int count = 0;
		curIbf = ibf;
		responseCount = 0;
		requestCount = 0;
		interval = 1000;
		nextReport = interval;
		startTs = Calendar.getInstance().getTimeInMillis();
		for (Iterator i = reqfList.iterator(); i.hasNext(); ++count) {
			// generate response data for request file
			RequestFile reqf = (RequestFile) i.next();
			log.debug("Generating responses for file: " + reqf);
			db.handleRequests(this, reqf, sysType);
			reqf.release();
		}
		log.debug("Response generation complete, requests: " + requestCount + ", responses: " + responseCount);
		long durationMs = Calendar.getInstance().getTimeInMillis() - startTs;
		long durationSecs = durationMs / 1000;
		durationSecs = (durationSecs == 0 ? 1 : durationSecs);
		log.debug("Processed in " + getDurationStr(durationMs) + ", " + (requestCount / durationSecs) + " requests/second");
		return count;
	}

	/**
	 * Generate dummy inbound file (no file data) for a test outbound file.
	 */
	private InboundFile generateInboundFile(OutboundFile obf) throws Exception {
		InboundFile ibf = new InboundFile();
		try {
			ibf.setIbId(db.getNewId());
			ibf.setCreateDate(Calendar.getInstance().getTime());
			ibf.setSysCode(obf.getSysCode());
			ibf.setFileName("sim-test-place-holder");
			ibf.setObId(obf.getObId());
			if (SysUtil.SYS_ABU.equals(obf.getSysCode())) {
				ibf.setFileType(SysUtil.FT_ABU_RSP);
			}
			else if (SysUtil.SYS_VAU.equals(obf.getSysCode())) {
				ibf.setFileType(SysUtil.FT_VAU_RSP);
			}
			else if (SysUtil.SYS_DAU.equals(obf.getSysCode())) {
				ibf.setFileType(SysUtil.FT_DAU_RSP);
			}
			else {
				ibf.setFileType("XXX");
			}
			ibf.setProcessFlag(AusBase.FLAG_YES);
			ibf.setTestFlag(AusBase.FLAG_YES);
			db.insertInboundFile(ibf, false);
			return ibf;
		}
		finally {
			try {
				ibf.release();
			}
			catch (Exception e) {}
		}
	}

	public void run() {
		try {
			AusDb db = new AusDb(directFlag);

			log.info("AUS Test Process Running");

			while (true) {
				try {
					// turned on when response file data is thought to be available
					boolean rspFlag = false;

					// process incoming test request files
					List reqfList = db.getRequestFilesWithStatus(RequestFile.ST_UPLOADED, true, true);
					ReqfProcessor rfp = new ReqfProcessor(directFlag);
					RequestFile reqf = null;
					for (Iterator i = reqfList.iterator(); i.hasNext();) {
						reqf = (RequestFile) i.next();
						try {
							if (reqf.getFileSize() > 0 && !reqf.empty()) {
								log.info("Processing test request file: " + reqf);
								rfp.process(reqf);
							}
							else {
								log.error("Empty request file received: " + reqf);
								reqf.setStatus(RequestFile.ST_ERROR);
								db.updateRequestFile(reqf, false);
							}
						}
						catch (Exception e) {
							if (reqf != null) {
								log.error("Unexpected error processing request file: " + reqf);
							}
							throw e;
						}
						finally {
							reqf.release();
						}
					}

					// simulate abu
					List batches = db.getOutboundRequestFileBatches(SysUtil.SYS_ABU, true);
					if (!batches.isEmpty()) {
						log.debug("Generating ABU test response data...");
						for (Iterator b = batches.iterator(); b.hasNext();) {
							RequestFileBatch batch = (RequestFileBatch) b.next();
							OutboundFile obf = null;
							InboundFile ibf = null;
							try {
								log.debug("calling AbuObfGenerator");
								obf = new AbuObfGenerator(directFlag, true).generateFile(batch);
								db.setOutboundFileDeliveryFlag(obf.getObId(), true);
								log.debug("generating dummy inbound file for obf: " + obf);
								ibf = generateInboundFile(obf);
								// if
								// (generateResponseRecords(reqfList,ibf,SysUtil.SYS_ABU)
								// > 0)
								if (generateResponseRecords(batch.getReqfList(), ibf, SysUtil.SYS_ABU) > 0) {
									rspFlag = true;
								}
							}
							finally {
								try {
									obf.release();
								}
								catch (Exception e) {}
							}
						}
					}

					// simulate vau
					batches = db.getOutboundRequestFileBatches(SysUtil.SYS_VAU, true);
					if (!batches.isEmpty()) {
						log.debug("Generating VAU test response data...");
						for (Iterator b = batches.iterator(); b.hasNext();) {
							RequestFileBatch batch = (RequestFileBatch) b.next();
							OutboundFile obf = null;
							InboundFile ibf = null;
							try {
								obf = new VauObfGenerator(directFlag, true).generateFile(batch);
								db.setOutboundFileDeliveryFlag(obf.getObId(),true);
								ibf = generateInboundFile(obf);
								// if
								// (generateResponseRecords(reqfList,ibf,SysUtil.SYS_VAU)
								// > 0)
								if (generateResponseRecords(batch.getReqfList(), ibf, SysUtil.SYS_VAU) > 0) {
									rspFlag = true;
								}
							}
							finally {
								try {
									obf.release();
								}
								catch (Exception e) {}
							}
						}
					}

					// simulate dau
					batches = db.getOutboundRequestFileBatches(SysUtil.SYS_DAU,	true);
					if (!batches.isEmpty()) {
						log.debug("Generating DAU test response data...");
						OutboundFile obf = null;
						InboundFile ibf = null;
						try {
							obf = new DauObfGenerator(directFlag, true).generateFile(batches);
							db.setOutboundFileDeliveryFlag(obf.getObId(), true);
							ibf = generateInboundFile(obf);
							for (Iterator b = batches.iterator(); b.hasNext();) {
								RequestFileBatch batch = (RequestFileBatch) b.next();
								// if
								// (generateResponseRecords(reqfList,ibf,SysUtil.SYS_DAU)
								// > 0)
								if (generateResponseRecords(batch.getReqfList(), ibf, SysUtil.SYS_DAU) > 0) {
									rspFlag = true;
								}
							}
						}
						finally {
							try {
								obf.release();
							}
							catch (Exception e) {}
						}
					}

					// process any generated test responses into response files
					if (rspFlag) {
						log.debug("Generating test response files...");
						RspfGenerator.generate(directFlag, true);
					}
				}
				catch (Exception e) {
					log.error("Unexpected error in main loop: " + e);
					Calendar errorTs = Calendar.getInstance();
					if (++recentErrorCount > MAX_FREQUENT_ERRORS) {
						throw new RuntimeException("Too many frequent errors");
					}
					if (++totalErrorCount > MAX_TOTAL_ERRORS) {
						throw new RuntimeException("Too many errors");
					}
					lastErrorTs = errorTs;
				}

				Calendar nowTs = Calendar.getInstance();
				if (lastErrorTs != null && nowTs.getTimeInMillis() - lastErrorTs.getTimeInMillis() > FREQUENT_ERROR_MS) {
					recentErrorCount = 0;
				}

				if (lastReportTs == null || nowTs.getTimeInMillis() - lastReportTs.getTimeInMillis() > REPORT_INTERVAL_MS) {
					logReport();
				}
				Thread.sleep(10000);
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "run()");
			logReport();
		}
	}
}
