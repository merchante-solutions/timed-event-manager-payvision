package com.mes.aus.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.aus.InboundFile;
import com.mes.aus.SysUtil;

public class AbuIbfLoader extends AutomatedProcess {
	static Logger log = Logger.getLogger(AbuIbfLoader.class);
	private File inFile;
	
	public AbuIbfLoader(File inFile, boolean directFlag) {
		super(directFlag);
		this.inFile = inFile;
	}
	private long extractObId(InboundFile ibf) throws Exception {
		long obId = 0;
		LineNumberReader rdr = null;
		InputStream in = null;
		try {
			// open reader on file data
			in = ibf.getInputStream();
			rdr = new LineNumberReader(new InputStreamReader(in));
			// discard header
			rdr.readLine();
			// look for the obId in the first detail record
			String line = rdr.readLine();
			if (line != null) {
				// abu response file detail records should contain
				// obId in discretionary data field 1, offset 26, length 10
				if (line.startsWith("D") && line.length() > 26) {
					String obIdStr = line.substring(16, 26).trim();
					if (obIdStr.length() > 0) {
						obId = Long.parseLong(obIdStr);
					}
				}
			}
			if (obId == 0) {
				log.debug("Warning: unable to extract outbound file id from " + "inbound file detail 1");
			}
		}
		catch(Exception e){
			log.info("extractObId failed");
		}
		finally {
			try {
				rdr.close();
			}
			catch (Exception e) {}
			try {
				in.close();
			}
			catch (Exception e) {}
		}
		return obId;
	}
	private InboundFile upload() {
		InputStream in = null;
		InboundFile ibf = null;
		try {
			log.debug("Begin ABU inbound file store to DB");
			ibf = new InboundFile();
			ibf.setIbId(db.getNewId());
			ibf.setCreateDate(Calendar.getInstance().getTime());
			ibf.setSysCode(SysUtil.SYS_ABU);
			ibf.setFileType(SysUtil.FT_ABU_RSP);
			ibf.setFileName(inFile.getName());
			in = new FileInputStream(inFile);
			ibf.setData(in);
			ibf.setObId(extractObId(ibf));
			db.insertInboundFile(ibf, true);
			log.debug("ABU inbound file store to DB completed");
			return ibf;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "upload(inFile=" + inFile + ")");
		}
		finally {
			try {
				in.close();
			}
			catch (Exception e) {}
			try {
				ibf.release();
			}
			catch (Exception e) {}
		}
		return null;
	}
	public static InboundFile uploadFile(File inFile, boolean directFlag) {
		return (new AbuIbfLoader(inFile, directFlag)).upload();
	}
	public static InboundFile uploadFile(File inFile) {
		return uploadFile(inFile, true);
	}
}