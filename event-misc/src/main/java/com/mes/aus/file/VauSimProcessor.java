package com.mes.aus.file;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import org.apache.log4j.Logger;
import com.mes.aus.OutboundFile;

public class VauSimProcessor extends AutomatedProcess {
	static Logger log = Logger.getLogger(VauSimProcessor.class);

	private static final int LST_HEADER = 1;
	private static final int LST_DETAIL = 2;
	private static final int LST_TRAILER = 3;
	private static final int LST_DONE = 4;

	private OutboundFile obf;

	public VauSimProcessor(OutboundFile obf, boolean directFlag) {
		super(directFlag);
		this.obf = obf;
	}
	public VauSimProcessor(OutboundFile obf) {
		this(obf, true);
	}
	private void processHeader(String line, int lineNum) {
		log.debug("" + lineNum + "header: " + line);
	}
	private void processDetail(String line, int lineNum) {
		log.debug("" + lineNum + "detail: " + line);
	}
	private void processTrailer(String line, int lineNum, int detailCount) {
		log.debug("" + lineNum + "trailer: " + line);
		log.debug("detail count: " + detailCount);
	}

	public void run() {
		LineNumberReader in = null;

		try {
			in = new LineNumberReader(new InputStreamReader(obf.getInputStream()));
			String line = null;
			int processState = LST_HEADER;
			int detailCount = 0;
			int lineNum = 0;

			while ((line = in.readLine()) != null) {
				log.debug("parsing line: '" + line + "'");
				lineNum = in.getLineNumber();
				switch (processState) {
					case LST_HEADER :
						processHeader(line, lineNum);
						processState = LST_DETAIL;
						break;

					case LST_DETAIL :
						if (!line.startsWith("1")) {
							processDetail(line, lineNum);
							++detailCount;
							break;
						}
						processState = LST_TRAILER;

					case LST_TRAILER :
						processTrailer(line, lineNum, detailCount);
						processState = LST_DONE;
						break;

					case LST_DONE :
						throw new RuntimeException(
								"Unexpected data after trailer");
				}
			}
			if (processState != LST_DONE)
				throw new RuntimeException("Unexpected end of file");
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "run()");
		}
		finally {
			try {
				in.close();
			}
			catch (Exception e) {}
			try {
				obf.release();
			}
			catch (Exception e) {}
		}
	}
}