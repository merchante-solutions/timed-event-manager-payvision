package com.mes.aus.file.job;

import java.io.File;
import org.apache.log4j.Logger;
import com.mes.aus.InboundFile;
import com.mes.aus.Notifier;
import com.mes.aus.file.AbuIbfLoader;

public class AbuIbXferJob extends Job {
	static Logger log = Logger.getLogger(AbuIbXferJob.class);
	public static final int ERR_CUTOFF = 5;

	public AbuIbXferJob(boolean directFlag) {
		super(directFlag);
	}
	public AbuIbXferJob() {
		this(true);
	}
	public void run() {
		try {
			// check error count
			if (db.getAbuIbXferErrCnt() >= ERR_CUTOFF) {
				log.error("Max error count exceeded, reset error count in aus_config");
				return;
			}

			// validate transfer directory
			String ibDirStr = db.getAbuIbXferLoc();
			File ibDir = new File(ibDirStr);
			if (!ibDir.exists() || !ibDir.isDirectory()) {
				throw new RuntimeException("Invalid inbound transfer location (see" + " aus_config): '" + ibDirStr + "'");
			}

			// upload any files, then delete them
			File[] ibFiles = ibDir.listFiles();
			for (int i = 0; i < ibFiles.length; ++i) {
				if (!ibFiles[i].isFile())
					continue;
				
				log.info("Uploading inbound response file: " + ibFiles[i]);
				InboundFile ibf = AbuIbfLoader.uploadFile(ibFiles[i]);
				if (ibf != null) {
					log.info("Uploaded.");
					// delete the file
					log.info("Deleting " + ibFiles[i]);
					ibFiles[i].delete();
					Notifier.notifyDeveloper("ABU Inbound File Transfer", "ABU inbound file transferred: " + ibf, directFlag);
				}
				else {
					log.info("Inbound file is null !!!");
				}
			}
		}
		catch (Exception e) {
			db.incrementAbuIbXferErrCnt();
			log.error("Error: " + e);
			notifyError(e, "run()");
		}
	}
	public static void showUsage() {
		System.out.println("\nUsage: java com.mes.aus.file.job.AbuIbXferJob -once | -loop\n  -once  run once\n  -loop  run continuously\n");
		System.exit(0);
	}
	public static void main(String[] args) {
		try {
			if (args.length != 1) {
				showUsage();
			}
			boolean runOnce = "-once".equals(args[0]);
			boolean runLoop = "-loop".equals(args[0]);
			if (!runOnce && !runLoop) {
				showUsage();
			}

			initLog4j();
			AbuIbXferJob job = new AbuIbXferJob();
			do {
				log.info("AbuIbXferJob executing.");
				job.run();
				try {
					if (runLoop)
						Thread.sleep(10000);
				}
				catch (Exception e) {}
			}
			while (runLoop);
		}
		finally {
			// kill the DiskCache cleanup thread
			System.exit(0);
		}
	}
}