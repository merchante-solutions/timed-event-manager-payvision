package com.mes.aus.file.job;

import java.io.File;
import java.io.FileOutputStream;
import org.apache.log4j.Logger;
import com.mes.aus.Notifier;
import com.mes.aus.OutboundFile;
import com.mes.aus.SysUtil;

public class AbuObXferJob extends Job {
	static Logger log = Logger.getLogger(AbuObXferJob.class);
	public static final int ERR_CUTOFF = 5;

	public AbuObXferJob(boolean directFlag) {
		super(directFlag);
	}
	public AbuObXferJob() {
		this(true);
	}
	private void downloadFile(OutboundFile obf, File obDir) {
		FileOutputStream out = null;

		try {
			File fobf = new File(obDir, obf.getFileName());
			log.debug("Downloading ABU outbound file " + fobf + "...");
			out = new FileOutputStream(fobf);
			obf.writeData(out);
			log.debug("Download complete.");
			// TODO: create a flag file?
		}
		catch (Exception e) {
			log.info("Failed to download file from Match server!!!");
			throw new RuntimeException(e);
		}
		finally {
			try {
				out.flush();
				out.close();
			}
			catch (Exception e) {}
		}
	}
	public void run() {
		try {
			// check error count
			if (db.getAbuObXferErrCnt() >= ERR_CUTOFF) {
				log.error("Max error count exceeded, reset error count in aus_config");
				return;
			}

			// validate transfer directory
			String obDirStr = db.getAbuObXferLoc();
			File obDir = new File(obDirStr);
			if (!obDir.exists() || !obDir.isDirectory()) {
				throw new RuntimeException("Invalid outbound xfer location (see" + " aus_config): '" + obDirStr + "'");
			}

			OutboundFile obf = null;
			while (db.getAbuObXferErrCnt() < ERR_CUTOFF && (obf = db.getXferOutboundFile(SysUtil.SYS_ABU)) != null) {
				try {
					downloadFile(obf, obDir);
					db.setOutboundFileDeliveryFlag(obf.getObId(), true);
					Notifier.notifyDeveloper("ABU Outbound File Transfer", "ABU outbound file transferred: " + obf, directFlag);
				}
				finally {
					obf.release();
				}
			}
		}
		catch (Exception e) {
			db.incrementAbuObXferErrCnt();
			log.error("Error: " + e);
			notifyError(e, "run()");
		}
	}

	public static void showUsage() {
		System.out.println("\nUsage: java com.mes.aus.file.job.AbuObXferJob "
						+ "-once | -loop\n  -once  run once\n  -loop  run continuously\n");
		System.exit(0);
	}

	public static void main(String[] args) {
		try {
			if (args.length != 1) {
				showUsage();
			}
			boolean runOnce = "-once".equals(args[0]);
			boolean runLoop = "-loop".equals(args[0]);
			if (!runOnce && !runLoop) {
				showUsage();
			}

			initLog4j();
			AbuObXferJob job = new AbuObXferJob();
			do {
				log.info("AbuObXferJob executing.");
				job.run();
				try {
					if (runLoop)
						Thread.sleep(10000);
				}
				catch (Exception e) {}
			}
			while (runLoop);
		}
		finally {
			// kill the DiskCache cleanup thread
			System.exit(0);
		}
	}
}