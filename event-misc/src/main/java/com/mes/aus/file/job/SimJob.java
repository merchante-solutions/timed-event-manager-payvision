package com.mes.aus.file.job;

import org.apache.log4j.Logger;
import com.mes.aus.OutboundFile;
import com.mes.aus.SysUtil;
import com.mes.aus.file.VauSimProcessor;

public class SimJob extends Job {
	static Logger log = Logger.getLogger(SimJob.class);
	public static final int ERR_CUTOFF = 5;

	public SimJob(boolean directFlag) {
		super(directFlag);
	}
	public SimJob() {
		this(true);
	}
	public void run() {
		try {
			OutboundFile obf = db.getSimOutboundFile(SysUtil.SYS_VAU);
			(new VauSimProcessor(obf, directFlag)).run();
			// obf = db.getSimOutboundFile(SysUtil.SYS_ABU);
			// (new AbuSimProcessor(obf,directFlag)).run();
		}
		catch (Exception e) {
			db.incrementAbuIbXferErrCnt();
			log.error("Error: " + e);
			notifyError(e, "run()");
		}
	}

	public static void showUsage() {
		System.out.println("\nUsage: java com.mes.aus.file.job.SimJob "
						+ "-once | -loop\n  -once  run once\n  -loop  run continuously\n");
		System.exit(0);
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			showUsage();
		}
		boolean runOnce = "-once".equals(args[0]);
		boolean runLoop = "-loop".equals(args[0]);
		if (!runOnce && !runLoop) {
			showUsage();
		}

		initLog4j();
		SimJob job = new SimJob();
		do {
			log.info("SimJob executing.");
			job.run();
			try {
				if (runLoop)
					Thread.sleep(10000);
			}
			catch (Exception e) {}
		}
		while (runLoop);
	}
}