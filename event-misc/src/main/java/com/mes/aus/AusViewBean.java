package com.mes.aus;

import org.apache.log4j.Logger;
import com.mes.mvc.mes.MesAction;
import com.mes.mvc.mes.MesViewBean;

public class AusViewBean extends MesViewBean
{
  static Logger log = Logger.getLogger(AusViewBean.class);

  private AusDb db = new AusDb();
  
  public AusViewBean(MesAction action)
  {
    super(action);
  }

  protected AusDb db()
  {
    if (db == null)
    {
      db = new AusDb();
    }
    return db;
  }
}
