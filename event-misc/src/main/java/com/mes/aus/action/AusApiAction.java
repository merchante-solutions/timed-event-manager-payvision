package com.mes.aus.action;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.ActivityRow;
import com.mes.aus.AusDb;
import com.mes.aus.AusUser;
import com.mes.aus.RequestFile;
import com.mes.aus.ResponseFile;
import com.mes.mvc.ApiException;
import com.mes.mvc.ApiMultipartFileHandler;
import com.mes.mvc.mes.MesApiAction;
import com.mes.mvc.mes.MesApiRequest;
import com.mes.mvc.mes.MesApiResponse;
import com.mes.mvc.mes.MesApiSystemErrorException;
import com.oreilly.servlet.multipart.FilePart;

public class AusApiAction extends MesApiAction
{
  static Logger log = Logger.getLogger(AusApiAction.class);

  protected AusDb db = new AusDb();

  // aus response codes
  public static final String RC_AUS_MERCH_NUM_ERROR   = "100";
  public static final String RC_AUS_STATUS_ERROR      = "101";
  public static final String RC_AUS_INVALID_RSPF_ID   = "102";

  // aus parameters
  public static final String PARM_STATUS_FILTER       = "statusFilter";
  public static final String PARM_REQF_ID             = "reqfId";
  public static final String PARM_RSPF_ID             = "rspfId";
  public static final String PARM_DAYS_BACK           = "daysBack";

  public static final String PARM_STATUS_COUNT        = "statusCount";
  public static final String PARM_STATUS              = "status_[0-9]+";
  public static final String PARM_STATUS_PFX          = "status_";
  public static final String PARM_STATUS_REQF_ID      = "reqfId_[0-9]+";
  public static final String PARM_STATUS_REQF_ID_PFX  = "reqfId_";
  public static final String PARM_STATUS_REQF_NAME    = "reqfName_[0-9]+";
  public static final String PARM_STATUS_REQF_NAME_PFX
                                                      = "reqfName_";
  public static final String PARM_STATUS_DL_COUNT     = "dlCount_[0-9]+";
  public static final String PARM_STATUS_DL_COUNT_PFX = "dlCount_";
  public static final String PARM_STATUS_RSPF_ID      = "rspfId_[0-9]+";
  public static final String PARM_STATUS_RSPF_ID_PFX  = "rspfId_";

  // list of valid filters in status requests
  private static List validFilters  = new ArrayList();

  static
  {
    validFilters.add(AusDb.FILT_ALL);
    validFilters.add(AusDb.FILT_RESPONSE);
    validFilters.add(AusDb.FILT_NEW);
    validFilters.add(AusDb.FILT_ERROR);
  };

  private RequestFileHandler fileHandler = new RequestFileHandler();
  private String userName;
  private String merchNum;
  
  private class RequestFileHandler implements ApiMultipartFileHandler
  {
    private List uploads = new ArrayList();
    
    public int uploadCount()
    {
      return uploads.size();
    }
    
    public boolean uploadReceived()
    {
      return uploadCount() > 0;
    }
    
    private void addRequestFile(RequestFile ausFile)
    {
      uploads.add(ausFile);
      log.debug("Request file received");
    }

    private RequestFile getRequestFile(int index)
    {
      if (index >= 0 && index < uploads.size())
      {
        return (RequestFile)uploads.get(index);
      }
      log.warn("Index " + index + " out of bounds on upload list");
      return null;
    }
        
    public String getUploadName(int index)
    {
      RequestFile reqf = getRequestFile(index);
      return reqf != null ? reqf.getFileName() : null;
    }
    
    /**
     * Files are too large to store in memory, they'll be in file
     * containers cached on disk.  This will always return null.
     */
    public byte[] getUploadData(int index)
    {
      return null;
    }
    
    /**
     * Generate request file to contain uploaded file.
     */
    public void handleFilePart(FilePart filePart)
    {
      try
      {
        if (filePart.getFileName() == null)
        {
          throw new ApiException(MesApiResponse.RC_UPLOAD_ERROR,
            "Upload error, missing file name");
        }
        
        // store file data in memory as FileUpload object
        InputStream in = filePart.getInputStream();
        
        RequestFile reqf = new RequestFile();
        reqf.setFileName(filePart.getFileName());
        reqf.setData(filePart.getInputStream());
        addRequestFile(reqf);
      }
      catch (ApiException ae)
      {
        throw ae;
      }
      catch (Exception e)
      {
        log.error("Error: " + e);
        e.printStackTrace();
        throw new ApiException(MesApiResponse.RC_UPLOAD_ERROR);
      }
    }
  }
  
  protected ApiMultipartFileHandler getMultipartFileHandler()
  {
    return fileHandler;
  }
  
  /**
   * Make sure user is linked to merchant number in AUS system.  Make sure
   * user has AUS API access rights.
   */
  private void validateUserMerchant()
  {
    // verify user has api right
    if (!user.hasRight(6023))
    {
      log.debug("user does not have api right");
      throw new ApiException(MesApiResponse.RC_AUTHENTICATE_ERROR);
    }

    // validate merch num
    userName = apiRequest.getParameter(MesApiRequest.PARM_USER_ID);
    merchNum = apiRequest.getParameter(MesApiRequest.PARM_MERCH_NUM);
    if (db.getMerchLinkForUser(userName,merchNum) == null)
    {
      throw new ApiException(RC_AUS_MERCH_NUM_ERROR);
    }
  }

  public void doAusUpload()
  {
    if (!fileHandler.uploadReceived())
    {
      throw new ApiException(
        MesApiResponse.RC_UPLOAD_ERROR,"File upload not received");
    }

    validateUserMerchant();

    // get ausr user object to determine test status
    AusUser ausrUser = db.getAusUser(userName);

    // store the reqf in the database
    RequestFile reqf = fileHandler.getRequestFile(0);
    if (reqf.empty()) 
    {
      throw new ApiException(
        MesApiResponse.RC_UPLOAD_ERROR,"File upload data missing");
    }

    try
    {
      reqf.setReqfId(db.getNewId());
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new MesApiSystemErrorException(""+e);
    }

    reqf.setCreateDate(Calendar.getInstance().getTime());
    reqf.setUserName(user.getLoginName());
    reqf.setAusrName(userName);
    reqf.setMerchNum(merchNum);
    reqf.setStatus(reqf.ST_UPLOADED);
    reqf.setTestFlag(ausrUser.getTestFlag());
    db.insertRequestFile(reqf,true);
    reqf.release();

    // return the reqfId 
    apiResponse.setParameter(PARM_REQF_ID,""+reqf.getReqfId());
  }

  public void doAusStatus()
  {
    validateUserMerchant();

    // validate status filter, default to all if not given
    String statusFilter = apiRequest.getParameter(PARM_STATUS_FILTER);
    statusFilter = (statusFilter != null ? statusFilter.toUpperCase() : AusDb.FILT_ALL);
    if (!validFilters.contains(statusFilter))
    {
      throw new ApiException(RC_AUS_STATUS_ERROR);
    }

    // get reqfId if present
    long reqfId = apiRequest.getParameterAsLong(PARM_REQF_ID);

    int daysBack = apiRequest.getParameterAsInt(PARM_DAYS_BACK);
    daysBack = daysBack != -1 ? daysBack : 4;

    // lookup activity
    List rows = db.lookupActivity(merchNum,statusFilter,reqfId,daysBack);

    // how many status sets will be present
    apiResponse.setParameter(PARM_STATUS_COUNT,""+rows.size());

    // generate status set parameters
    for (int i = 0; i < rows.size(); ++i)
    {
      ActivityRow row = (ActivityRow)rows.get(i);
      apiResponse.setParameter(PARM_STATUS_REQF_ID_PFX + i,
                                    ""+row.getReqfId());
      apiResponse.setParameter(PARM_STATUS_REQF_NAME_PFX + i,
                                    ""+row.getReqFileName());
      apiResponse.setParameter(PARM_STATUS_PFX + i,
                                    row.getStatus());
      apiResponse.setParameter(PARM_STATUS_DL_COUNT_PFX + i,
                                    ""+row.getDlCount());
      if (row.getStatus().equals("SUCCESS"))
      {
        apiResponse.setParameter(PARM_STATUS_RSPF_ID_PFX + i,
                                    ""+row.getRspfId());
      }
    }
  }

  public void doAusDownload()
  {
    validateUserMerchant();

    long rspfId = apiRequest.getParameterAsLong(PARM_RSPF_ID);

    // load response file, indicate if no file found or mismatched merch num
    ResponseFile rspf = db.getResponseFile(rspfId,true);
    if (rspf == null || !merchNum.equals(rspf.getMerchNum()))
    {
      throw new ApiException(RC_AUS_INVALID_RSPF_ID);
    }

    // send file data back in the response
    apiResponse.setDownloadFile(rspf.getFileName(),rspf.getInputStream());

    // update download count on response file
    rspf.setDlNum(rspf.getDlNum() + 1);
    rspf.setDlUser(userName);
    rspf.setDlDate(Calendar.getInstance().getTime());
    db.updateResponseFile(rspf,false);
  }
}
