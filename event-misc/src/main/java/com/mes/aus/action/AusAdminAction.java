package com.mes.aus.action;

import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.AusProfile;
import com.mes.aus.AusUser;
import com.mes.aus.MerchLink;
import com.mes.aus.Notifier;
import com.mes.aus.ProfileRef;
import com.mes.aus.UserRef;
import com.mes.aus.view.AusProfileLookupBean;
import com.mes.aus.view.AusUserLookupBean;
import com.mes.aus.view.FindMesUserBean;
import com.mes.aus.view.FindProfileMerchBean;
import com.mes.aus.view.MerchLookupBean;
import com.mes.aus.view.ProfileEditBean;
import com.mes.aus.view.UserEditBean;
import com.mes.mvc.Link;

public class AusAdminAction extends AusAction
{
  static Logger log = Logger.getLogger(AusAdminAction.class);

  public void doAusUserFindMesUser()
  {
    FindMesUserBean bean = new FindMesUserBean(this);
    doView("ausUserFindMesUser");
  }

  public void doAusUserAdd()
  {
    // fetch mes user ref
    String ausrName = getParm("ausrName");
    UserRef ref = db.getMesUserRef(ausrName);
    if (ref == null)
    {
      throw new RuntimeException("error loading user ref for ausrName '" 
        + ausrName + "'");
    }

    // create new aus user record for mes user
    AusUser ausUser = new AusUser();
    try
    {
      ausUser.setAusrId(db.getNewId());
    }
    catch (Exception e)
    {
      throw new RuntimeException(e);
    }
    ausUser.setCreateDate(Calendar.getInstance().getTime());
    ausUser.setUserName(user.getLoginName());
    ausUser.setAusrName(ausrName);
    ausUser.setAusrDesc(ref.getFullName());
    ausUser.setEnabledFlag(ausUser.FLAG_YES);
    if (!db.insertAusUser(ausUser))
    {
      throw new RuntimeException("error inserting aus_user record for"
        + " ausrName '" + ausrName + "'");
    }

    // if merch num associated with mes user then
    // automatically create aus merch num record
    // for the aus user
    String merchNum = getParm("merchNum",null);
    if (merchNum != null)
    {
      try
      {
        db.linkUserToMerch(ausUser.getAusrId(),merchNum);
        addFeedback("User automatically linked to merchant '" + merchNum + "'");
      }
      catch (Exception e)
      {
        log.error("Error: " + e);
        e.printStackTrace();
        addFeedback("Failed to auto linke merchant '" + merchNum + "'");
      }
    }

    // redirect to aus user edit view
    Link editLink = handler.getLink("ausUserEdit","ausUserEdit","ausrId=" 
      + ausUser.getAusrId(),null);
    Link backLink = handler.getBackLink("ausUserFindMesUser");
    setBackLink("ausUserEdit",backLink);
    redirectWithFeedback(editLink,"User '" + ausrName + "' created in AUS system");
  }

  public void doAusUserLookup()
  {
    AusUserLookupBean bean = new AusUserLookupBean(this);
    doView("ausUserLookup");
  }

  public void doAusUserEdit()
  {
    long ausrId = getParmLong("ausrId",-1L);
      
    List merchLinks = db.getAllMerchLinksForUser(ausrId);
    request.setAttribute("merchLinks",merchLinks);

    UserEditBean bean = new UserEditBean(this);
    if (bean.isAutoSubmitOk())
    {
      addFeedback("AUS user '" + bean.getAusUser().getAusrName() 
        + "' edits saved");
    }

    doView("ausUserEdit");
  }

  public void doAusUserDelete()
  {
    long ausrId = getParmLong("ausrId");
    AusUser ausUser = db.getAusUser(ausrId);
    if (isConfirmed())
    {
      db.deleteAusUser(ausrId);
      redirectWithFeedback(getBackLink(),"AUS user '"
        + ausUser.getAusrName() + "' deleted");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"AUS user '"
        + ausUser.getAusrName() + "' not deleted");
    }
    else
    {
      confirm("Remove AUS user '" + ausUser.getAusrName() 
        + "' from AUS system?");
    }
  }

  public void doAusMerchLookup()
  {
    long ausrId = getParmLong("ausrId");
    MerchLookupBean bean = new MerchLookupBean(this);
    doView("ausMerchLookup","Merchant Lookup for AUS User " 
      + bean.getAusrName());
  }

  public void doAusMerchLink()
  {
    String fbMsg = null;
    try
    {
      long ausrId = getParmLong("ausrId");
      String merchNum = getParm("merchNum");

      // create the link
      MerchLink link = db.linkUserToMerch(ausrId,merchNum);
      AusUser ausUser = db.getAusUser(ausrId);

      // feedback msg indicates success
      fbMsg = "Merchant " + merchNum + " linked to AUS user " 
        + ausUser.getAusrName();
    }
    catch (Exception e)
    {
      // feedback msg contains error msg
      fbMsg = e.getMessage();
    }

    // redirect back to the merch lookup backlink
    redirectWithFeedback(handler.getBackLink("ausMerchLookup"),fbMsg);
  }

  public void doAusMerchUnlink()
  {
    long ausrId = getParmLong("ausrId");
    AusUser ausUser = db.getAusUser(ausrId);
    String merchNum = getParm("merchNum");

    if (isConfirmed())
    {
      db.unlinkUserFromMerch(ausrId,merchNum);
      redirectWithFeedback(getBackLink(),"Merchant "
        + merchNum + " unlinked from AUS user '"
        + ausUser.getAusrName() + "'");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Unlink canceled");
    }
    else
    {
      confirm("Unlink merchant " + merchNum + " from AUS user '"
        + ausUser.getAusrName() + "'?");
    }
  }

  public void doAusNotifyTest()
  {
    long ausrId = getParmLong("ausrId");
    AusUser ausUser = db.getAusUser(ausrId);
    String ausrName = ausUser.getAusrName();
    if (ausUser.getEmail() == null || ausUser.getEmail().length() == 0)
    {
      redirectWithFeedback(getBackLink(),"Email recipients have not been set"
        + " for AUS user '" + ausrName + "'");
    }
    else if (isConfirmed())
    {
      Notifier n = new Notifier(false);
      n.notifyTestMessage(ausrName);
      redirectWithFeedback(getBackLink(),"Test notification message sent to"
        + " email recipients of AUS user '" + ausrName + "'");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Test notification message"
        + " canceled");
    }
    else
    {
      confirm("Send test notification email message to all email"
        + " recipients of AUS user '" + ausrName + "'?");
    }
  }
  
  public void doAusProfileLookup()
  {
    AusProfileLookupBean bean = new AusProfileLookupBean(this);
    doView("ausProfileLookup");
  }

  public void doAusProfileEdit()
  {
    String pid = getParm("profileId");
    AusProfile profile = db.getAusProfile(pid);
    request.setAttribute("profile",profile);
    ProfileEditBean bean = new ProfileEditBean(this);
    if (bean.isAutoSubmitOk())
    {
      addFeedback("AUS profile '" + bean.getProfile().getProfileId() 
        + "' edits saved");
    }
    doView("ausProfileEdit");
  }
  
  public void doAusProfileFind()
  {
    FindProfileMerchBean bean = new FindProfileMerchBean(this);
    doView("ausProfileFind");
  }

  public void doAusProfileCreate()
  {
    // fetch mes profile ref
    String profileId = getParm("profileId");
    ProfileRef ref = db.getProfileRef(profileId);
    if (ref == null)
    {
      throw new RuntimeException("error loading profile ref for profile ID '" 
        + profileId + "'");
    }

    // create new AUS profile record
    AusProfile profile = new AusProfile();
    profile.setProfileId(profileId);
    profile.setMerchNum(ref.getMerchNum());
    profile.setUserName(user.getLoginName());
    profile.setMerchName(ref.getMerchName());
    // HACK: setting these manually to default values
    profile.setVauFileType("VAU_MES");
    profile.setAbuFileType("ABU_MES");
    profile.setDauFileType("DAU_MES");
    profile.setAutoFlag(profile.FLAG_NO);
    profile.setAutoDay(1);
    profile.setAutoTestFlag(profile.FLAG_YES);
    MerchLink link = db.getDefaultMerchLinkForMerchNum(ref.getMerchNum());
    if (link != null)
    {
      profile.setAutoAusrId(link.getAusrId());
    }
    if (!db.insertAusProfile(profile))
    {
      throw new RuntimeException("error inserting aus_profile record for"
        + " profile '" + profileId + "'");
    }

    // redirect to profile edit view
    Link editLink = handler.getLink("ausProfileEdit","ausProfileEdit","profileId=" 
      + profileId,null);
    Link backLink = handler.getBackLink("ausProfileFind");
    setBackLink("ausProfileEdit",backLink);
    redirectWithFeedback(editLink,"Profile '" + profileId + "' created in AUS system");
  }

  public void doAusProfileDelete()
  {
    String profileId = getParm("profileId");
    AusProfile profile = db.getAusProfile(profileId);
    if (isConfirmed())
    {
      db.deleteAusProfile(profileId);
      redirectWithFeedback(getBackLink(),"AUS profile '"
        + profileId + "' deleted");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"AUS profile '"
        + profileId + "' not deleted");
    }
    else
    {
      confirm("Remove profile '" + profileId + "' from AUS system?");
    }
  }
}
