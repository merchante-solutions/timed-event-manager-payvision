package com.mes.aus.action;

import java.io.OutputStream;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.aus.ProcessError;
import com.mes.aus.RequestFile;
import com.mes.aus.ResponseFile;
import com.mes.aus.file.ReqfProcessor;
import com.mes.aus.view.LookupActivityBean;
import com.mes.aus.view.LookupRequestFilesBean;
import com.mes.aus.view.RequestFileUploadBean;

public class AusUserAction extends AusAction
{
  static Logger log = Logger.getLogger(AusUserAction.class);

  public void doAusUploadRequestFile()
  {
    RequestFileUploadBean bean = new RequestFileUploadBean(this);
    if (bean.isAutoSubmitOk())
    {
      addFeedback("Upload successful");
      request.setAttribute("reqf",bean.getRequestFile());
      doView("ausUploadRequestFile");
    }
    else
    {
      if (bean.isAutoSubmit())
      {
        addFeedback("Upload failed");
      }
      doView("ausUploadRequestFile");
    }
  }

  // AJAX
  public void doAusRefreshMerchNum()
  {
    String reqType = getParm("reqType",null);
    if ("lookup".equals(reqType))
    {
      sendAjaxResponse(LookupRequestFilesBean.refreshMerchNumField(request));
    }
    else if ("upload".equals(reqType))
    {
      sendAjaxResponse(RequestFileUploadBean.refreshMerchNumField(request));
    }
    else if ("activity".equals(reqType))
    {
      sendAjaxResponse(LookupActivityBean.refreshMerchNumField(request));
    }
    else
    {
      sendAjaxResponse("<b style='color:red'>Invalid request type '" + reqType + "'</b>");
    }
  }

  public void doAusLookupRequestFiles()
  {
    LookupRequestFilesBean bean = new LookupRequestFilesBean(this);
    doView("ausLookupRequestFiles");
  }

  public void doAusProcessRequestFile()
  {
    long reqfId = getParmLong("reqfId");
    RequestFile reqf = db.getRequestFile(reqfId,true);
    ReqfProcessor rfp = new ReqfProcessor(false);
    rfp.process(reqf);

    if (rfp.hasError())
    {
      int errorCount = rfp.getErrorCount();
      int errorNum;
      for (errorNum = 0; errorNum < errorCount && errorNum < 5; errorNum++)
      {
        ProcessError error = rfp.getError(errorNum);
        StringBuffer buf = new StringBuffer();
        buf.append(error.isFatal() ? "Fatal " : "");
        buf.append(error.isWarning() ? "Warning: " : "Error: ");
        buf.append(error.getErrorMsg());
        buf.append(" (line " + error.getLineNum() + ")");
        addFeedback(buf.toString());
      }
      if (errorNum < errorCount)
      {
        addFeedback("(" + (errorCount - errorNum) + " more errors)");
      }
    }

    if (!reqf.isTest() || rfp.hasFatalError())
    {
      doLinkRedirect(getBackLink());
    }
    else if (rfp.hasError())
    {
      redirectWithFeedback(getBackLink(),"Test upload " + reqfId + " processed (with errors)");
    }
    else
    {
      redirectWithFeedback(getBackLink(),"Upload " + reqfId + " processed successfully");
    }
  }

  public void doAusShowRequestFile()
  {
    long reqfId = getParmLong("reqfId");
    request.setAttribute("reqf",db.getRequestFile(reqfId,false));
    request.setAttribute("requestList",db.getRequests(reqfId));
    doView("ausShowRequestFile");
  }

  public void doAusLookupActivity()
  {
    LookupActivityBean bean = new LookupActivityBean(this);
    doView("ausLookupActivity");
  }

  public void doAusDownloadResponseFile()
  {
    ResponseFile rspFile = null;
    
    try
    {
      long rspfId = getParmLong("rspfId");
      rspFile = db.getResponseFile(rspfId,true);
      if (rspFile == null)
      {
        redirectWithFeedback(getBackLink(),"Unable to locate response file");
        return;
      }
      if (rspFile.empty())
      {
        redirectWithFeedback(getBackLink(),"File data not available");
        return;
      }
      response.setContentType("text/plain");
      response.setHeader("Content-Disposition","attachment; filename=\"" 
        + rspFile.getFileName() + "\";");

      OutputStream out = response.getOutputStream();
      rspFile.writeData(out);
      out.flush();

      rspFile.setDlNum(rspFile.getDlNum() + 1);
      rspFile.setDlUser(user.getLoginName());
      rspFile.setDlDate(Calendar.getInstance().getTime());
      db.updateResponseFile(rspFile,false);
    }
    catch (Exception e)
    {
      log.error("Error sending response file data: " + e);
      e.printStackTrace();
      redirectWithFeedback(getBackLink(),"Error downloading response file");
    }
    finally
    {
      try { rspFile.release(); } catch (Exception e) { }
    }
  }
      
}
