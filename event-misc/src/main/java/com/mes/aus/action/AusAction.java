package com.mes.aus.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.aus.AusDb;
import com.mes.mvc.mes.MesAction;

public class AusAction extends MesAction
{
  static Logger log = Logger.getLogger(AusAction.class);

  protected AusDb db = new AusDb();

  private static SimpleDateFormat defaultSdf = new SimpleDateFormat(
    "'<nobr>'EEE M/d/yy'</nobr> <nobr>'h:mma z'</nobr>'");

  private static String formatHtmlDate(Date date, SimpleDateFormat sdf)
  {
    if (date == null)
    {
      return "--";
    }
    return sdf.format(date);
  }
  public static String formatHtmlDate(Date date)
  {
    return formatHtmlDate(date,defaultSdf);
  }
  public static String formatHtmlDate(Date date, String formatStr)
  {
    return formatHtmlDate(date,new SimpleDateFormat(formatStr));
  }

  public void doAusTest()
  {
    doView("ausTest");
  }

  // AJAX
  public void doAusTestAjax()
  {
    sendAjaxResponse("Test ajax response 1");
  }

  // AJAX
  public void doAusTestAjax2()
  {
    sendAjaxResponse("Test ajax response 2");
  }
}
