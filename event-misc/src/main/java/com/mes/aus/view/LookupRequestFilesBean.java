package com.mes.aus.view;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.aus.AusDb;
import com.mes.aus.AusUser;
import com.mes.aus.AusViewBean;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.mes.MesAction;
import com.mes.tools.DropDownTable;

public class LookupRequestFilesBean extends AusViewBean
{
  static Logger log = Logger.getLogger(LookupRequestFilesBean.class);

  public static final String FN_AUSR_NAME     = "ausrName";
  public static final String FN_MERCH_NUM     = "merchNum";

  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List rows;

  public LookupRequestFilesBean(MesAction action)
  {
    super(action);
  }

  public class AusrNameTable extends DropDownTable
  {
    public AusrNameTable()
    {
      addElement("","any");
      addElements(db().getAusrNameDdItems());
    }
  }

  /**
   * This is a lazy loader that has two modes.  If initiated with
   * a value for ausrName it will load the table the first time
   * getItems() is called.  If an ausrName field is used to initialize,
   * the ausrName value will be fetched from the field.  Ajax usage
   * will init with an ausrName value, standard field bean usage will
   * init with the field as ausrName will not be set until later
   * in the life cycle of the bean.  The bean uses multipart encoding
   * which means request parameters will not be known until after the
   * uploaded file data is processed.
   */
  public static class MerchNumTable extends DropDownTable
  {
    private String ausrNameVal;
    private Field ausrNameField;
    private AusDb myDb = new AusDb();

    public MerchNumTable(String ausrNameVal)
    {
      this.ausrNameVal = ausrNameVal;
    }
    public MerchNumTable(Field ausrNameField)
    {
      this.ausrNameField = ausrNameField;
    }

    public List getItems()
    {
      // see if lazy load needs to happen
      if (super.getItems().isEmpty())
      {
        addElement("","any");
        if (ausrNameField != null)
        {
          ausrNameVal = ausrNameField.getData();
        }
        if (ausrNameVal != null && !ausrNameVal.equals(""))
        {
          addElements(myDb.getAusrMerchNumDdItems(ausrNameVal));
        }
      }
      return super.getItems();
    }
  }

  /**
   * Generates ajax response to create new merchant number field specific
   * to the given aus user name.
   */
  public static String refreshMerchNumField(HttpServletRequest request)
  {
    String ausrName = request.getParameter(FN_AUSR_NAME);
    Field mnField = new DropDownField(
      FN_MERCH_NUM,new MerchNumTable(ausrName),false);
    mnField.setHtmlExtra("style='width: 250px'");
    return mnField.renderHtml();
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      // determine if aus user has admin rights to pick aus user 
      // or if it should be hardcoded based on mes user login name
      Field ausrNameField = null;
      if (user.hasRight(6021))
      {
        ausrNameField = 
          new DropDownField(FN_AUSR_NAME,new AusrNameTable(),true);
      }
      else
      {
        AusUser ausUser = db().getAusUser(user.getLoginName());
        ausrNameField =
          new HiddenField(FN_AUSR_NAME,ausUser.getAusrName());
      }
      fields.add(ausrNameField);
      fields.add(new DropDownField(FN_MERCH_NUM,new MerchNumTable(ausrNameField),true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public List getRows()
  {
    if (rows == null)
    {
      rows = new ArrayList();
    }
    return rows;
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      String ausrName = getData(FN_AUSR_NAME);
      String merchNum = getData(FN_MERCH_NUM);
      rows = db().lookupRequestFiles(ausrName,merchNum);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}