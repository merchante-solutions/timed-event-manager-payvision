package com.mes.aus.view;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.aus.AusViewBean;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.mvc.mes.MesAction;

public class AusUserLookupBean extends AusViewBean
{
  static Logger log = Logger.getLogger(AusUserLookupBean.class);

  public static final String FN_SEARCH_TERM   = "searchTerm";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List rows;

  public AusUserLookupBean(MesAction action)
  {
    super(action);
  }
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new Field(FN_SEARCH_TERM,"User Name",64,32,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public List getRows()
  {
    if (rows == null)
    {
      rows = new ArrayList();
    }
    return rows;
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      String searchTerm = getData(FN_SEARCH_TERM);
      rows = db().lookupAusUsers(searchTerm);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}