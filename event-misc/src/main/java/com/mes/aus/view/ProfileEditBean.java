package com.mes.aus.view;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.aus.AusProfile;
import com.mes.aus.AusViewBean;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.DropDownField;
import com.mes.forms.HiddenField;
import com.mes.mvc.mes.MesAction;
import com.mes.tools.DropDownTable;

public class ProfileEditBean extends AusViewBean
{
  static Logger log = Logger.getLogger(ProfileEditBean.class);

  public static final String FN_PROFILE_ID      = "profileId";
  public static final String FN_VAU_FT          = "vauFileType";
  public static final String FN_ABU_FT          = "abuFileType";
  public static final String FN_DAU_FT          = "dauFileType";
  public static final String FN_AUTO_FLAG       = "autoFlag";
  public static final String FN_AUTO_USER       = "autoUser";
  public static final String FN_AUTO_DAY        = "autoDay";
  public static final String FN_AUTO_TEST_FLAG  = "autoTestFlag";
  public static final String FN_SUBMIT_BTN      = "submitBtn";

  private AusProfile profile;

  public ProfileEditBean(MesAction action)
  {
    super(action);
  }
  
  public class VauFileTypeTable extends DropDownTable
  {
    public VauFileTypeTable()
    {
      addElement("","select one");
      addElements(db().getOutboundFileTypeDdItems("VAU"));
    }
  }

  public class AbuFileTypeTable extends DropDownTable
  {
    public AbuFileTypeTable()
    {
      addElement("","select one");
      addElements(db().getOutboundFileTypeDdItems("ABU"));
    }
  }

  public class DauFileTypeTable extends DropDownTable
  {
    public DauFileTypeTable()
    {
      addElement("","select one");
      addElements(db().getOutboundFileTypeDdItems("DAU"));
    }
  }
  
  public class AutoUserTable extends DropDownTable
  {
    public AutoUserTable(String merchNum)
    {
      addElement("","select one");
      addElements(db().getAutoUserDdItems(merchNum));
    }
  }
  
  public class AutoDayTable extends DropDownTable
  {
    public AutoDayTable()
    {
      for (int i = 1; i <= 28; ++i)
      {
        addElement(""+i,""+i);
      }
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      String profileId = request.getParameter(FN_PROFILE_ID);
      profile = db().getAusProfile(profileId);
      fields.add(new HiddenField(FN_PROFILE_ID));
      fields.add(new DropDownField(FN_VAU_FT,new VauFileTypeTable(),false));
      fields.add(new DropDownField(FN_ABU_FT,new AbuFileTypeTable(),false));
      fields.add(new DropDownField(FN_DAU_FT,new DauFileTypeTable(),false));
      fields.add(new CheckField(FN_AUTO_FLAG,"Auto AUS","y",true));
      fields.add(new DropDownField(FN_AUTO_DAY,new AutoDayTable(),false));
      fields.add(new CheckField(FN_AUTO_TEST_FLAG,"Auto Test Mode","y",true));
      fields.add(new DropDownField(FN_AUTO_USER,new AutoUserTable(profile.getMerchNum()),true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      profile = db().getAusProfile(getField(FN_PROFILE_ID).getData());
      profile.setVauFileType(getData(FN_VAU_FT));
      profile.setAbuFileType(getData(FN_ABU_FT));
      profile.setDauFileType(getData(FN_DAU_FT));
      profile.setAutoFlag(getData(FN_AUTO_FLAG));
      profile.setAutoAusrId(getField(FN_AUTO_USER).asInt());
      profile.setAutoDay(getField(FN_AUTO_DAY).asInt());
      profile.setAutoTestFlag(getData(FN_AUTO_TEST_FLAG));
      db().updateAusProfile(profile);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      String profileId = getData(FN_PROFILE_ID);
      if (profileId != null)
      {
        profile = db().getAusProfile(profileId);
        setData(FN_VAU_FT,profile.getVauFileType());
        setData(FN_ABU_FT,profile.getAbuFileType());
        setData(FN_DAU_FT,profile.getDauFileType());
        setData(FN_AUTO_FLAG,profile.getAutoFlag());
        setData(FN_AUTO_USER,""+profile.getAutoAusrId());
        setData(FN_AUTO_DAY,profile.getAutoDay());
        setData(FN_AUTO_TEST_FLAG,profile.getAutoTestFlag());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  public AusProfile getProfile()
  {
    return profile;
  }
}