package com.mes.aus.view;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.aus.AusUser;
import com.mes.aus.AusViewBean;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.mes.MesAction;

public class UserEditBean extends AusViewBean
{
  static Logger log = Logger.getLogger(UserEditBean.class);

  public static final String FN_AUSR_ID       = "ausrId";
  public static final String FN_AUSR_DESC     = "ausrDesc";
  public static final String FN_TEST_FLAG     = "testFlag";
  public static final String FN_NOTIFY_FLAG   = "notifyFlag";
  public static final String FN_ENABLED_FLAG  = "enabledFlag";
  public static final String FN_TOKEN_FLAG    = "tokenFlag";
  public static final String FN_EMAIL         = "email";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private AusUser ausUser;

  public UserEditBean(MesAction action)
  {
    super(action);
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new HiddenField(FN_AUSR_ID));
      fields.add(new Field(FN_AUSR_DESC,"Search",64,32,false));
      fields.add(new CheckField(FN_TEST_FLAG,"Test Mode","y",true));
      fields.add(new CheckField(FN_ENABLED_FLAG,"AUS Enabled","y",true));
      fields.add(new CheckField(FN_NOTIFY_FLAG,"Notification Enabled","y",true));
      fields.add(new CheckField(FN_TOKEN_FLAG,"Tokenization Enabled","y",true));
      fields.add(new EmailField(FN_EMAIL,"Email Addresses",300,32,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      ausUser = db().getAusUser(getField(FN_AUSR_ID).asLong());
      ausUser.setAusrDesc(getData(FN_AUSR_DESC));
      ausUser.setTestFlag(getData(FN_TEST_FLAG));
      ausUser.setEnabledFlag(getData(FN_ENABLED_FLAG));
      ausUser.setNotifyFlag(getData(FN_NOTIFY_FLAG));
      ausUser.setTokenFlag(getData(FN_TOKEN_FLAG));
      ausUser.setEmail(getData(FN_EMAIL));
      db().updateAusUser(ausUser);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      long ausrId = getField(FN_AUSR_ID).asLong();
      if (ausrId > 0L)
      {
        ausUser = db().getAusUser(ausrId);
        setData(FN_AUSR_DESC,ausUser.getAusrDesc());
        setData(FN_TEST_FLAG,ausUser.getTestFlag());
        setData(FN_ENABLED_FLAG,ausUser.getEnabledFlag());
        setData(FN_NOTIFY_FLAG,ausUser.getNotifyFlag());
        setData(FN_TOKEN_FLAG,ausUser.getTokenFlag());
        setData(FN_EMAIL,ausUser.getEmail());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  public AusUser getAusUser()
  {
    return ausUser;
  }
}