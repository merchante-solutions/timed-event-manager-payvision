package com.mes.aus.view;

import java.io.InputStream;
import java.util.Calendar;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.aus.AusDb;
import com.mes.aus.AusUser;
import com.mes.aus.AusViewBean;
import com.mes.aus.RequestFile;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.mes.MesAction;
import com.mes.tools.DropDownTable;
import com.oreilly.servlet.multipart.FilePart;

public class RequestFileUploadBean extends AusViewBean
{
  static Logger log = Logger.getLogger(RequestFileUploadBean.class);

  public static final String RN_REQF_ID       = "reqfId";
  public static final String FN_AUSR_NAME     = "ausrName";
  public static final String FN_MERCH_NUM     = "merchNum";
  public static final String FN_FILE_NAME     = "fileName";

  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private RequestFile reqf;

  public RequestFileUploadBean(MesAction action)
  {
    super(action);
  }

  public class AusrNameTable extends DropDownTable
  {
    public AusrNameTable()
    {
      addElement("","select one");
      addElements(db().getAusrNameDdItems());
    }
  }

  /**
   * This is a lazy loader that has two modes.  If initiated with
   * a value for ausrName it will load the table the first time
   * getItems() is called.  If an ausrName field is used to initialize,
   * the ausrName value will be fetched from the field.  Ajax usage
   * will init with an ausrName value, standard field bean usage will
   * init with the field as ausrName will not be set until later
   * in the life cycle of the bean.  The bean uses multipart encoding
   * which means request parameters will not be known until after the
   * uploaded file data is processed.
   */
  public static class MerchNumTable extends DropDownTable
  {
    private String ausrNameVal;
    private Field ausrNameField;
    private AusDb myDb = new AusDb();

    public MerchNumTable(String ausrNameVal)
    {
      this.ausrNameVal = ausrNameVal;
    }
    public MerchNumTable(Field ausrNameField)
    {
      this.ausrNameField = ausrNameField;
    }

    public List getItems()
    {
      // see if lazy load needs to happen
      if (super.getItems().isEmpty())
      {
        if (ausrNameField != null)
        {
          ausrNameVal = ausrNameField.getData();
        }
        if (ausrNameVal != null && !ausrNameVal.equals(""))
        {
          addElements(myDb.getAusrMerchNumDdItems(ausrNameVal));
        }
        else
        {
          addElement("","(select user first)");
        }
      }
      return super.getItems();
    }
  }

  /**
   * Generates ajax response to create new merchant number field specific
   * to the given aus user name.
   */
  public static String refreshMerchNumField(HttpServletRequest request)
  {
    String ausrName = request.getParameter(FN_AUSR_NAME);
    Field mnField = new DropDownField(
      FN_MERCH_NUM,new MerchNumTable(ausrName),false);
    mnField.setHtmlExtra("style='width: 250px'");
    return mnField.renderHtml();
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new HiddenField(RN_REQF_ID)); // TODO: get rid of this

      // determine if aus user has admin rights to pick aus user 
      // or if it should be hardcoded based on mes user login name
      Field ausrNameField = null;
      if (user.hasRight(6021))
      {
        ausrNameField = 
          new DropDownField(FN_AUSR_NAME,new AusrNameTable(),false);
      }
      else
      {
        AusUser ausUser = db().getAusUser(user.getLoginName());
        ausrNameField =
          new HiddenField(FN_AUSR_NAME,ausUser.getAusrName());
      }
      fields.add(ausrNameField);
      fields.add(new DropDownField(FN_MERCH_NUM,new MerchNumTable(ausrNameField),false));
      fields.add(new Field(FN_FILE_NAME,"Upload File",32,32,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  /**
   * Upload file.  Overrides FieldBean stub to enable file uploading.
   * Stores the uploaded file data in an upload object.
   */
  protected void receiveFile(FilePart part)
  {
    InputStream in = null;

    try
    {
      // create a new upload object, store the
      // file name from the file part in it
      reqf = new RequestFile();
      reqf.setFileName(part.getFileName());

      in = part.getInputStream();
      log.debug("setting data with input stream...");
      reqf.setData(in);
      receiveFlag = true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      receiveFlag = false;
    }
    finally
    {
      try { in.close(); } catch (Exception e) { }
    }
  }
  
  private boolean receiveFlag = false;

  protected boolean autoSubmit()
  {
    try
    {
      // if file upload was successful, update the upload
      // record with the correct merch num, aus user and 
      // set the upload status to received
      if (receiveFlag && reqf != null)
      {
        // validate merch num
        String merchNum = getData(FN_MERCH_NUM);
        String ausrName = getData(FN_AUSR_NAME);
        if (db().getMerchLinkForUser(ausrName,merchNum) == null)
        {
          action.addFeedback("AUS user '" + ausrName 
            + "' not setup to upload files for merchant #" + merchNum);
          return false;
        }

        // get ausr user object to determine test status
        AusUser ausrUser = db().getAusUser(ausrName);

        // store the reqf in the database
        reqf.setReqfId(db().getNewId());
        reqf.setCreateDate(Calendar.getInstance().getTime());
        reqf.setUserName(user.getLoginName());
        reqf.setAusrName(ausrName);
        reqf.setMerchNum(merchNum);
        reqf.setStatus(reqf.ST_UPLOADED);
        reqf.setTestFlag(ausrUser.getTestFlag());
        db().insertRequestFile(reqf,true);
        reqf.release();

        // store the reqf id for the upload ok page (?)
        setData(RN_REQF_ID,""+reqf.getReqfId());

        // success
        return true;
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  // NOTE: for testing purposes
  public RequestFile getRequestFile()
  {
    long reqfId = getField(RN_REQF_ID).asLong();
    if (reqfId > 0)
    {
      return db().getRequestFile(reqfId,true);
    }
    return null;
  }
}