package com.mes.aus.view;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.aus.AusUser;
import com.mes.aus.AusViewBean;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.mes.MesAction;

public class MerchLookupBean extends AusViewBean
{
  static Logger log = Logger.getLogger(MerchLookupBean.class);

  public static final String FN_AUSR_ID       = "ausrId";
  public static final String FN_AUSR_NAME     = "ausrName";
  public static final String FN_SEARCH_TERM   = "searchTerm";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List rows;

  public MerchLookupBean(MesAction action)
  {
    super(action);
  }
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new HiddenField(FN_AUSR_ID));
      fields.add(new HiddenField(FN_AUSR_NAME));
      fields.add(new Field(FN_SEARCH_TERM,"Search Term",64,32,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public List getRows()
  {
    if (rows == null)
    {
      rows = new ArrayList();
    }
    return rows;
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      String searchTerm = getData(FN_SEARCH_TERM);
      long ausrId = getField(FN_AUSR_ID).asLong();
      rows = db().lookupMerchRefs(ausrId,searchTerm);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoLoad()
  {
    try
    {
      long ausrId = getField(FN_AUSR_ID).asLong();
      AusUser ausUser = db().getAusUser(ausrId);
      setData(FN_AUSR_NAME,ausUser.getAusrName());
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  public long getAusrId()
  {
    return getField(FN_AUSR_ID).asLong();
  }

  public String getAusrName()
  {
    return getData(FN_AUSR_NAME);
  }
}