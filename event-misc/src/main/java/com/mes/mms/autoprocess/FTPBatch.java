/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/FTPBatch.java $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 8/04/04 4:47p $
  Version            : $Revision: 24 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.Date;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;


/**
 * FTPBatch
 * --------
 *  - represents a single mms request ftp batch
 *  - accessed soley by FTPBatch_FileManager
 *  - one ftp batch can contain 0-n mms requests
 *  - ftp batch is per Vital's FTP Batch process specs. ref.: 'MMS Batch Product Offer - v1.0 January 2002'
 *  - there are 4 files assoc. w/ one ftp batch: text, error, list and data
 *     the text file is input and the other 3 are 'response' files

 *    ftp batch file naming conventions
 *    ---------------------------------
 *    textFilename:    "{app type src code}_{terminal application name}_{datestamp}.txt"
 *                  'app src type code' corres. syntatically to mesdb.application.app_src_type_code
                    'terminal application name'  is of format from Vital's respective ftp batch input file specification
 *                  'datestamp'              mmddyyyyhhmm
 *    errorFilename:    "{textFilename name}.err"
 *    listFilename:    "{textFilename name}.lst"
 *    dataFilename:    "{textFilename name}.dat"
 * 
 *    IMPT: all 3 ftp batch response files are ALWAYS sent w/in 24 hrs. of text file sent out.
 *        if no errors then both error file and list file will be empty ( 0 k)
 *        if no successes then data file will be empty (0 k)
 * 
 *    IMPT: error file is ignored since the list file has same data in addition to verbose error data
 */

public class FTPBatch extends Object implements Comparable
{
  // create class log category
  static Category log = Category.getInstance(FTPBatch.class.getName());
  
  // constants
  public static final int      FTPBATCH_FILETYPE_UNDEFINED     = 0;
  public static final int      FTPBATCH_FILETYPE_TEXT          = 1;
  public static final int      FTPBATCH_FILETYPE_DATA          = 2;
  public static final int      FTPBATCH_FILETYPE_ERROR         = 3;
  public static final int      FTPBATCH_FILETYPE_LIST          = 4;

  //private static final String  FTPBATCH_TEXTFILE_PREFIX        = "mesftpb_";
  private static final String  FTPBATCH_DATAFILE_SUFFIX        = ".dat";
  private static final String  FTPBATCH_ERRORFILE_SUFFIX       = ".err";
  private static final String  FTPBATCH_LISTFILE_SUFFIX        = ".lst";

  public static final int      MNRPBF                          = 200;
    // maximum number of allowed mms requests per ftp batch file
  

  // data members
  protected String outgoingDir;          // dir containing the textFile(s) (ftp batches)
  protected String responseDir;          // dir containing the ftp batch response(s)

  protected String textFilename;        // outgoing ftp batch file
  protected String dataFilename;        // response file - successful vnums listing
  protected String listFilename;        // response file - rejected vnums listing (W/ verbose error detail)
  protected String errorFilename;        // response file - rejected vnums listing
  
  // construction
  public FTPBatch(String outgoingDir, String responseDir, String textFilename)
  {
    this.outgoingDir  = outgoingDir;
    this.responseDir  = responseDir;
    this.textFilename = textFilename;
    
    final String baseName = textFilename.substring(0,textFilename.length()-4);
    
    errorFilename = baseName + ".err";
    listFilename  = baseName + ".lst";
    dataFilename  = baseName + ".dat";
  }
  
  public boolean equals(Object obj)
  {
    return (obj==null)? false:(obj==this)? true:((FTPBatch)obj).textFilename.equalsIgnoreCase(this.textFilename);
  }
  
  public int compareTo(Object o)
    throws ClassCastException
  {
    // cast the param to this type
    final FTPBatch ftpb = (FTPBatch)o;
    
    if(equals(ftpb))
    {
      return 0;
    }
    else 
    {
      final Date thisdate = getDateFromTextFilename(textFilename);
      final Date odate    = getDateFromTextFilename(ftpb.textFilename);
      return thisdate.compareTo(odate);
    }
  }
  
  /**
   * getTextFilenameFromResponseFilename()
   */
  public static String getTextFilenameFromResponseFilename(String responseFilename)
    throws Exception
  {
    try {
      return responseFilename.substring(0,responseFilename.length()-3)+"txt";
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
    return "ERROR";
  }

  /**
   * isResponsefile()
   *  - determines whether spec. file name is an incoming ftp batch response file
   */
  public static boolean isResponseFile(String filename)
  {
    try {
      
      return 
          filename.endsWith(FTPBATCH_DATAFILE_SUFFIX+".pgp")
          || filename.endsWith(FTPBATCH_ERRORFILE_SUFFIX+".pgp")
          || filename.endsWith(FTPBATCH_LISTFILE_SUFFIX+".pgp")
          || filename.endsWith(FTPBATCH_DATAFILE_SUFFIX)
          || filename.endsWith(FTPBATCH_ERRORFILE_SUFFIX)
          || filename.endsWith(FTPBATCH_LISTFILE_SUFFIX);
    }
    catch(Exception e) {
      log.error(e.toString());
    }
    
    return false;
  }

  /**
   * isEncryptedResponsefile()
   *  - determines whether spec. file name is an incoming ftp batch response file
   */
  public static boolean isEncryptedResponsefile(String filename)
  {
    try {
      
      return 
          filename.endsWith(FTPBATCH_DATAFILE_SUFFIX+".pgp")
          || filename.endsWith(FTPBATCH_ERRORFILE_SUFFIX+".pgp")
          || filename.endsWith(FTPBATCH_LISTFILE_SUFFIX+".pgp");
    }
    catch(Exception e) {
      log.error(e.toString());
    }
    
    return false;
  }

  /**
   * isTextFile()
   *  - determines whether spec. file name is an outgoing ftp batch file
   *    (which corres. to member var textFilename)
   */
  public static boolean isTextFile(String filename)
  {
    try {
      return filename.substring(filename.length()-4).equalsIgnoreCase(".txt");
    }
    catch(Exception e) {
      log.error(e.toString());
    }
    
    return false;
  }
  
  /**
   * calcTextFilename()
   *  - calculates the ftp batch text (outgoing) file name
   *  - text filename is a function of terminal application name and date
   */
  public static String calcTextFilename(String appSrcTypeCode, String termAppName, Date ftpBatchDate)
    throws Exception
  {
    Calendar clndr = Calendar.getInstance();
    clndr.setTime(ftpBatchDate);
    String datestamp = 
           StringUtilities.rightJustify(Integer.toString(clndr.get(Calendar.MONTH)+1), 2, '0')
          +StringUtilities.rightJustify(Integer.toString(clndr.get(Calendar.DAY_OF_MONTH)), 2, '0')
          +Integer.toString(clndr.get(Calendar.YEAR))
          +StringUtilities.rightJustify(Integer.toString(clndr.get(Calendar.HOUR_OF_DAY)), 2, '0')
          +StringUtilities.rightJustify(Integer.toString(clndr.get(Calendar.MINUTE)), 2, '0');
    
    return appSrcTypeCode + "_" + termAppName + "_" + datestamp + ".txt";
  }
  
  /**
   * getDateFromResponseFilename()
   * 
   * Extracts and converts to Date the date stamp contained within the response filename
   */
  public static Date getDateFromResponseFilename(String filename)
  {
    return !isResponseFile(filename)? null:getDateFromBatchFilename(filename);
  }
  
  /**
   * getDateFromTextFilename()
   * 
   * Extracts and converts to Date the date stamp contained within the text filename
   */
  public static Date getDateFromTextFilename(String filename)
  {
    return !isTextFile(filename)? null:getDateFromBatchFilename(filename);
  }
  
  /**
   * getDateFromBatchFilename()
   * 
   * Extracts and converts to Date the date stamp contained 
   * within the filename of either text or response files.
   */
  private static Date getDateFromBatchFilename(String filename)
  {
    try 
    {
      int    secondHyphen = filename.indexOf("_",(filename.indexOf("_")+1));
      String sdate        = filename.substring(secondHyphen+1, secondHyphen+9);

      log.info("***sdate= " + sdate);
    
      int mn,dy,yr;
      mn=StringUtilities.stringToInt(sdate.substring(0,2))-1;
      dy=StringUtilities.stringToInt(sdate.substring(2,4));
      yr=StringUtilities.stringToInt(sdate.substring(4,8));
      
      //log.debug("mn="+mn+", dy="+dy+", yr="+yr);
        
      Calendar clndr = Calendar.getInstance();
      clndr.clear();
      clndr.set(Calendar.YEAR,yr);
      clndr.set(Calendar.MONTH,mn);
      clndr.set(Calendar.DAY_OF_MONTH,dy);
      
      //log.debug("getDateFromTextFilename="+clndr.toString());

      Date tfdate=clndr.getTime();
      
      //log.debug("tfdate.toString()="+tfdate.toString());
      
      return tfdate;
    }
    catch(Exception e) 
    {
      log.error(e.getMessage());
    }
    return null;
  }
  
  public String getTextFilename() { return textFilename; }
  public String getErrorFilename() { return errorFilename; }
  public String getListFilename() { return listFilename; }
  public String getDataFilename() { return dataFilename; }
  
  /**
   * addMMSRequest()
   *  - adds the mms request to ftb batch outbound file
   *  - auto-creates outgoing file that corres. to member: textFilename if doesn't exist
   *  - enforces num allowed mms requests per ftp batch file limit
   *  - 'dir' arg is the directory containing the outgoing ftp batch file
   */
  public boolean addMMSRequest(MMSRequest mmsRequest)
  {
    try 
    {
      if(!isAssociatedMMSRequest(mmsRequest)) 
      {
        log.error("Add MMS Request failed: This ftp batch ('"+textFilename+"') NOT associated w/ mms request ('"+mmsRequest.getReqID()+"').");
        return false;
      }
      
      // get proper ftp batch template
      FTPBatch_Template ftpBT = FTPBatch_Template.getInstance(mmsRequest.getTermAppName());

      // get ftp file reference
      String absFilename = outgoingDir+File.separator+textFilename;
      File ftp_file = new File(absFilename);
    
      // create file if doesn't exist
      if(!ftp_file.exists()) 
      {
        ftp_file.createNewFile();
        log.info("FTP Batch text file '"+ftp_file.getName()+"' created.");
      }
      
      // check current number of mms requests
      //  by counting the number of newline chars in file - presuming one rec. per line
      FileReader fr = new FileReader(ftp_file);
      StringBuffer sb = new StringBuffer(1024*4);  // 4k - based on calculated maximum
      int c;
      while ((c = fr.read()) != -1)
      {
        sb.append((char)c);
      }
      String s = sb.toString();
      int i = -1,cnt = 0;
      while((i = s.indexOf('\n',++i)) != -1)
      {
        cnt++;
      }
      
      if(cnt > MNRPBF) 
      {
        fr.close();
        log.warn("Unable to add mms request ('"+mmsRequest.getReqID()+"')to ftp batch file ('"+textFilename+"').  Max number of allowed mms requests per text file ('"+MNRPBF+"') reached.  Operation failed.");
        return false;
      }
      fr.close();

      // mms request --> string (per appropriate ftp batch template)
      StringBuffer sLin = new StringBuffer(0);
      ftpBT.MMSRequestToRecord(mmsRequest,sLin);
        
      // add mms request ftp batch string to file
      log.debug("writing line to text file...");
      FileWriter fw = new FileWriter(absFilename,true);
      fw.write(sLin.toString());
      fw.close();
      
      log.info("MMS Request (Req. ID: '"+mmsRequest.getReqID()+"') added to ftp batch: '"+textFilename+"'.");
      
      return true;
    }
    catch(Exception e) 
    {
      log.error(e.getMessage());
    }

    return false;
  }
  
  /**
   * isAssociatedMMSRequest()
   * ------------------------
   *  - determines if mms request is part of this ftp batch object
   * 
   */
  public /*protected*/ boolean isAssociatedMMSRequest(MMSRequest mmsRequest)
  {
    try {
      
      int i = textFilename.indexOf('_');
      int j = textFilename.indexOf('_',i+1);
      String termAppName = textFilename.substring(i+1,j);
      log.debug("termAppName="+termAppName);
      
      return termAppName.equalsIgnoreCase(mmsRequest.getTermAppName());
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
    
    return false;
  }
  
  /**
   * updateMMSRequestWithResponse()
   * ------------------------------
   *  - update mms request arg w/ data in the mms request response (if present)
   *  - updates mms request arg w/ vnum and/or error data ONLY
   *  - RETURNS:
   *      - true:  if found 
   *      - false: if NOT found
   *      - Throws exception upon error
   */
  public boolean updateMMSRequestWithResponse(MMSRequest mmsRequest)
    throws Exception
  {
    
    try {
      
      //log.debug("updateMMSRequestWithResponse() - START");
      
      if(!isAssociatedMMSRequest(mmsRequest)) {
        log.debug("This ftp batch ('"+textFilename+"') not associated w/ mms request ('"+mmsRequest.getReqID()+"').");
        return false;
      }

      File f = null;
      BufferedReader br = null;
      String line = null, absFilename = null;
      FTPBatch_Template ftpBT = FTPBatch_Template.getInstance(mmsRequest.getTermAppName());
      log.debug("updateMMSRequestWithResponse(): Employing FTPBatch_Template of name: '"+ftpBT.getName()+"'.");
      MMSRequest crntMMSRequest = new MMSRequest();

      // load 'arrFldNmesToLoad'
      String[] arrUK = mmsRequest.getUniqueKey(MMSRequest.UNIQUEKEY_MERCNUM_TERMNUM);
      String[] arrFldNmesToLoad = new String[arrUK.length+1];
      for(int i=0; i<arrUK.length; i++)
        arrFldNmesToLoad[i]=arrUK[i];
      arrFldNmesToLoad[arrUK.length] = "VNUM";

      // check success (data) file first (one record per line)
      log.debug("updateMMSRequestWithResponse() - checking success data file...");
      absFilename = responseDir+File.separator+dataFilename;
      log.debug("absFilename="+absFilename);
      f = new File(absFilename);
      if(f.exists() && f.isFile()) {
        log.debug("found data response file.");
        br = new BufferedReader(new FileReader(absFilename));
        while((line=br.readLine()) != null) {
          crntMMSRequest.clear();
          if(!ftpBT.RecordToMMSRequest(crntMMSRequest,line,arrFldNmesToLoad))
            throw new Exception("updateMMSRequestWithResponse(): Error converting record to mms request (success file).  Aborted.");
          //log.debug("**** crntMMSRequest ****");
          //log.debug(crntMMSRequest.toString());
          //log.debug("************************");
          if(mmsRequest.isEqualUniqueKey(crntMMSRequest,MMSRequest.UNIQUEKEY_MERCNUM_TERMNUM)) {
            // found request! - update mms request arg w/ vnum
            mmsRequest.clearProcessErrors();
            mmsRequest.setVNum(crntMMSRequest.getVNum());
            log.info("Found mms request by unique key in data file (Request ID: '"+mmsRequest.getReqID()+"').  Updated mms request w/ VNUM: '"+mmsRequest.getVNum()+"'.");
            br.close();
            return true;
          }
        }
        br.close();
      }
      
      StringBuffer recordBuf = new StringBuffer(1024*4);  // assumed max. size of response record
      boolean bIsNewRecord = false, bLoadRecord=false, bNotEOF=false, bRecFound=false;

      // check error (list) file (multiple lines per record)
      log.debug("updateMMSRequestWithResponse() - checking error file ('"+listFilename+"')...");
      absFilename = responseDir+File.separator+listFilename;
      f = new File(absFilename);
      if(f.exists() && f.isFile()) {
        br = new BufferedReader(new FileReader(absFilename));
        while(true) {
          
          bNotEOF=((line=br.readLine()) != null);
          //log.debug(line!=null? "line="+line:"line==null");
          
          bIsNewRecord = (line != null)? (line.startsWith("I|V") || line.startsWith(" |null")) : false;
          //log.debug("bIsNewRecord="+bIsNewRecord);
          
          bLoadRecord = (recordBuf.length()>0 && (bIsNewRecord || !bNotEOF));
          //log.debug("bLoadRecord="+bLoadRecord);

          // load record
          if(bLoadRecord) {

            /*
            log.debug("*********** (loading this record...)");
            log.debug(recordBuf.toString());
            log.debug("***********");
            */
            
            crntMMSRequest.clear();
            if(!ftpBT.RecordToMMSRequest(crntMMSRequest,recordBuf.toString(),arrFldNmesToLoad))
              throw new Exception("updateMMSRequestWithResponse(): Error converting record to mms request (Error file).  Aborted.  (Record buf: '"+recordBuf.toString()+"')");
            if(mmsRequest.isEqualUniqueKey(crntMMSRequest,MMSRequest.UNIQUEKEY_MERCNUM_TERMNUM)) {
              // found request!
              /*
              log.debug("%%%%% (crntMMSRequest:)");
              log.debug(crntMMSRequest.toString());
              log.debug("%%%%%");
              */
              // update mms request arg w/ vnum
              mmsRequest.setVNum(crntMMSRequest.getVNum());
              // update mss request arg w/ errors (if present)
              if(crntMMSRequest.getNumProcessErrors() > 0)
                mmsRequest.setProcessErrors(crntMMSRequest.getProcessErrors());
              log.info("Found mms request by unique key in error list file (Request ID: '"+mmsRequest.getReqID()+"').  Updated mms request w/ VNUM: '"+mmsRequest.getVNum()+"'. and listed errors.");
              br.close();
              return true;
            }
            // reset record buffer
            recordBuf.delete(0,recordBuf.length());
          }
          
          // done?
          if(!bNotEOF && recordBuf.length()==0)
            break;
          
          // append line to record buffer?
          recordBuf.append(line);
          recordBuf.append(System.getProperty("line.separator"));
          
        }
        br.close();
      }
      
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
    
    log.debug("Did NOT find response for mms request of Request ID: '"+mmsRequest.getReqID()+"'.");
    
    return false;
  }

}  // FTPBatch
