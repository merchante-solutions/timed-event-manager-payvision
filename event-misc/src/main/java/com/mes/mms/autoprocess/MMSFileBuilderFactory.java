/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/MMSFileBuilderFactory.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
// log4j classes.
import org.apache.log4j.Logger;
import com.mes.support.PropertiesFile;


/**
 * MMSFileBuilderFactory
 * ---------------------
 *  - SINGLETON - single public point of contact for interacting w/ MMSFileBuilder instances
 *  - manages creating the appropriate sub-class
 *  - manages configuring the object from a text file of same name as sub-class name
 *    - ex: 'MMSFileBuilder_FTPBatch.cfg'
*/
public final class MMSFileBuilderFactory extends Object
{
  // create class log category
  static Logger log = Logger.getLogger(MMSFileBuilderFactory.class);
  
  // Singleton
  public static MMSFileBuilderFactory getInstance()
  {
    if(_instance==null)
      _instance = new MMSFileBuilderFactory();
    
    return _instance;
  }
  private static MMSFileBuilderFactory _instance = null;
  ///
  
  // data members
  private Hashtable method_instance_map;
    // maps MMSRequest.method <--> MMSFileBuilder sub class
    // key: {MMSRequest.method}
    // val: {MMSFileBuilder sub class name}
  private Hashtable mmsFileBuilders;
    // key: "{sub class name}"
    // val: sub class instance

  // constructor(s)
  private MMSFileBuilderFactory()
  {
    method_instance_map = new Hashtable();
    mmsFileBuilders = new Hashtable();
    
    // MMSREQUEST_METHOD_MAP property FORMAT: 
    //  "{mms request method 1}:{MMSFileBuilder sub class 1},...,{mms request method N}:{MMSFileBuilder sub class N}"

    // load up method_instance_map data member from properties file
    PropertiesFile props = null;
    
    try
    {
      props = new PropertiesFile("MMSFileBuilderFactory.properties");
    }
    catch(Exception e)
    {
    }
    
    String mims = props.getString("MMSREQUEST_METHOD_MAP");
    StringTokenizer st = new StringTokenizer(mims,",:",false);
    String mthd,cls;
    try {
      while(st.hasMoreTokens()) {
        mthd=st.nextToken();
        cls=st.nextToken();
        log.debug("about to add to method_instance_map (mthd="+mthd+", cls="+cls+")...");
        if(mthd!=null && cls!=null && mthd.length()>0 && cls.length()>0) {
          method_instance_map.put(mthd,cls);
          log.info("MMS Request method '"+mthd+"' is supported by class '"+cls+"'.");
        }
      }
    }
    catch(Exception e) {
      log.error("Error attempting to set MMSFileBuilderFactory.method_instance_map: '"+e.getMessage()+"'.");
    }

  }

  public void shutdown()
  {
    for(Enumeration e=mmsFileBuilders.elements(); e.hasMoreElements(); )
      ((MMSFileBuilder)e.nextElement()).shutdown();
  }
  
  public boolean BuildMMSFile(MMSRequest mmsRequest)
  {
    try {
      return getBuilderInstance(mmsRequest).BuildMMSFile(mmsRequest);
    }
    catch(Exception e) {
      log.error("MMSFileBuilderFactory.BuildMMSFile() Exception: "+e.getMessage());
      return false;
    }
  }
  
  public void DetermineStatus(MMSRequest mmsRequest)
  {
    try {
      getBuilderInstance(mmsRequest).DetermineStatus(mmsRequest);
    }
    catch(Exception e) {
      log.error("MMSFileBuilderFactory.DetermineStatus() Exception: "+e.getMessage());
    }
  }
  
  /**
   * getBuilderInstance
   * ------------------
   *  - returns the appropriate mms file builder object according to the mms request arg
   *  - returns "saved" mms file builders if present in 'mmsFileBuilders' member var
   *  - stores newly instantited mms file builders in 'mmsFileBuilders' member var for future use
   *  - throws exception when:
   *      - unable to resolve mmsRequest arg to a particular MMSFileBuilder
   *      - unable to configure newly instantiated MMSFileBuilder instance
   *  - use this f() for "mindless" access:
   *      String mmsReqID = getBuilderInstance(mmsRequest).getReqID();
   */
  private MMSFileBuilder getBuilderInstance(MMSRequest mmsRequest)
    throws Exception
  {
    String mmsFileBuilder_className;
    MMSFileBuilder mmsFB=null;

    if(mmsRequest==null)
      throw new Exception("MMSFileBuilderFactory.getBuilderInstance() - null MMSRequest argument specified.  Aborted.");
    
    // check builders list to see if already instantiated
    if((mmsFileBuilder_className=(String)method_instance_map.get(mmsRequest.getProcessMethod().toLowerCase()))==null) 
    {
      log.error("Unrecognized MMS Request method: '"+mmsRequest.getProcessMethod()+"'");
      throw new Exception("Unrecognized MMS Request method: '"+mmsRequest.getProcessMethod()+"'");
    
    } 
    else if((mmsFB=(MMSFileBuilder)mmsFileBuilders.get(mmsFileBuilder_className))==null) 
    {
      // instantiate, configure then add to builders list (for future use) then return instance reference
      log.debug("Instantiating (first time): '"+mmsFileBuilder_className+"'.");
      if(null!=(mmsFB = (MMSFileBuilder)Class.forName(mmsFileBuilder_className).newInstance())) 
      {
        // sub-class specific configuration
        String[] configParams = null;
        if(mmsFileBuilder_className.equals("com.mes.mms.autoprocess.MMSFileBuilder_FTPBatch")) 
        {
          PropertiesFile props = new PropertiesFile("MMSFileBuilderFactory.properties");
          configParams = new String[] { props.getString("FTP_BATCH_ROOT_DIR") };
        }
        
        if(!mmsFB.configure(configParams))
        {
          throw new Exception("MMSFileBuilder sub-class configuration FAILED.  Sub-class creation failed.");
        }
        
        // add mms request builder sub-class to internal hashtable
        mmsFileBuilders.put(mmsFileBuilder_className,mmsFB);
      } 
      else
      {
        throw new Exception("Unable to instantiate MMSFileBuilder sub-class.");
      }
    } 
    else 
    {
      // already instantiated
      log.debug("Returning already instantiated MMSFileBuilder: '"+mmsFileBuilder_className+"'");
      mmsFB = (MMSFileBuilder)mmsFileBuilders.get(mmsFileBuilder_className);
    }
      
    if(mmsFB==null) 
    {
      log.debug("mmsFB==null");
      throw new Exception("Unable to obtain non-null MMSFileBuilder instance");
    }
    
    return mmsFB;
      
  }
  
}
