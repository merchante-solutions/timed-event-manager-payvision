/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/MMSFileBuilder_FTPBatch.java $

  Description:  


  Last Modified By   : $Author: Jkirton $
  Last Modified Date : $Date: 5/30/02 2:39p $
  Version            : $Revision: 12 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;

import java.util.Date;
// log4j classes.
import org.apache.log4j.Category;


/**
 * MMSFileBuilder_FTPBatch
 * -----------------------
*/
public class MMSFileBuilder_FTPBatch extends MMSFileBuilder
{
  // create class log category
  static Category log = Category.getInstance(MMSFileBuilder_FTPBatch.class.getName());
  
  // data members
  String workingDir;
  
  // constructor(s)
  public MMSFileBuilder_FTPBatch()
    throws Exception
  {
    super();
    
    workingDir = null;
  }
  
  public boolean configure(String[] configParams)
  {
    try 
    {
      if(configParams==null) 
      {
        log.error("Configure failed for MMSFileBuilder_FTPBatch: NULL configParams arg specified.");
        return false;
      }
      if(configParams.length!=1) 
      {
        log.error("Configure failed for MMSFileBuilder_FTPBatch: NULL configParams arg specified.  One and only one config param (working directory) can be specified.");
        return false;
      }
      
      String workingDir = configParams[0];
      
      // instantiate the ftp batch file manager (thus ensuring proper dir. struct)
      FTPBatch_FileManager.ftpBatchRootDir=workingDir;
      if(null==FTPBatch_FileManager.getInstance()) 
      {
        log.error("Configure failed for MMSFileBuilder_FTPBatch: FTPBatch_FileManager.getInstance() returned null.");
        return false;
      }
      
      this.workingDir=workingDir;
      
      // start up clock thread to manage ftp'ing outbound ftp info periodically
      FTPBatch_FileManager.getInstance().clockThreadStartup();

      isConfigured=true;
    }
    catch(Exception e) 
    {
      isConfigured=false;
      log.error(e.getMessage());
    }
    
    return isConfigured;
  }

  public boolean isConfigured()
  {
    return isConfigured;
  }
  
  public void shutdown()
  {
    log.debug("MMSFileBuilder_FTPBatch - STARTING shutdown...");
    FTPBatch_FileManager.getInstance().clockThreadShutdown();
    log.debug("MMSFileBuilder_FTPBatch - Shutdown complete.");
  }
  
  public boolean BuildMMSFile(MMSRequest mmsRequest)
  {
    try 
    {
      if(!isConfigured) 
      {
        log.error("BuildMMSFile failed: MMSFileBuilder_FTPBatch instance not configured.");
        return false;
      }
      
      if(!IsValidMMSRequest(mmsRequest)) 
      {
        log.error("Unable to build MMS file via FTP Batch: Invalid MMS request (Req. ID: '"+mmsRequest.getReqID()+"'.");
        return false;
      }
      
      final boolean bRval = FTPBatch_FileManager.getInstance().ammendMMSRequest(mmsRequest);
      
      if(bRval)
      {
        log.info("MMS Request (Req. ID: '"+mmsRequest.getReqID()+"') successfully added to ftp batch.");
      }
      else
      {
        log.error("ERROR: MMS Request (Req. ID: '"+mmsRequest.getReqID()+"') was NOT added to ftp batch.");
      }
      
      return bRval;
    }
    catch(Exception e) 
    {
      log.error(e.getMessage());
      return false;
    }
  }
  
  public void DetermineStatus(MMSRequest mmsRequest)
  {
    if(!isConfigured) {
      log.error("DetermineStatus failed: MMSFileBuilder_FTPBatch instance not configured.");
      return;
    }
    
    // ensure that min. turnaround time has passed before delegating task to FTPBatch_FileManager
    
    log.debug("MMSFileBuilder_FTPBatch.DetermineStatus() - START");

    final Date now = new Date();
    final long milisDuration = now.getTime() - mmsRequest.getProcessStartDate().getTime();
    
    log.debug("MMSFileBuilder_FTPBatch.DetermineStatus() - milisDuration="+milisDuration);
    
    if(milisDuration >= FTPBatch_FileManager.getInstance().FTPBATCH_PROCESS_MILIS_MIN) {
      log.debug("Determine Status - Minimum turnaround time passed.  Checking in present response files...");
      if(!FTPBatch_FileManager.getInstance().checkResponse(mmsRequest))
        log.error("Check response for MMS Request (ID: '"+mmsRequest.getReqID()+"') failed.");
    }

  }

  protected boolean IsValidMMSRequest(MMSRequest mmsRequest)
  {
    // NOTE: called before actual build request sent out only
    
    // Rqmnts for valid instance:
      //  --------------------------
    // -  workingDir must be set
    //  -  reqID must be non-empty
    // -  alt. unique key fields must be non-empty
    
    try {
      
      if(mmsRequest.getReqID().length() < 1)
        return false;
    
      String[] arrUK = mmsRequest.getUniqueKey(MMSRequest.UNIQUEKEY_MERCNUM_TERMNUM);
    
      for(int i=0; i<arrUK.length; i++)
        if(mmsRequest.getRequestDataItemValue(arrUK[i]).length()<1)
          return false;
      
      return true;
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
    
    return false;
  }

  public String toString()
  {
    String s = new String();
    
    s += "ERROR: NOT IMPLEMENTED";
    
    return s;
  }
  
}