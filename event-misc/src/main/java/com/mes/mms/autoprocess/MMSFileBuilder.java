/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/MMSFileBuilder.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/24/02 10:32a $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;


/**
 * MMSFileBuilder
 * --------------
 * abstract base class
*/
public abstract class MMSFileBuilder extends Object
{
  // data members
  protected boolean isConfigured;
  
  // constructor
  public MMSFileBuilder() {}
  
  // methods
  public abstract boolean configure(String[] configParams);
  public abstract boolean BuildMMSFile(MMSRequest mmsRequest);
  public abstract void DetermineStatus(MMSRequest mmsRequest);
  protected abstract boolean IsValidMMSRequest(MMSRequest mmsRequest);

  public void shutdown() {}
}