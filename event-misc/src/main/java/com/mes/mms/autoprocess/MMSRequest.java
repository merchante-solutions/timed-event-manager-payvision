/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/MMSRequest.java $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 6/03/04 11:09a $
  Version            : $Revision: 11 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;
              
import java.util.Date;
import java.util.Enumeration;
//import java.lang.reflect.Field;
import java.util.Hashtable;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;


/**
 * MMSRequest
 * ----------
 * Encapsulates data necessary to submit a request to the MMS system for an MMS 
 * file.
 */
public class MMSRequest extends Object
{
  // create class log category
  static Category log = Category.getInstance(MMSRequest.class.getName());

  // constants
  public static final int    UNIQUEKEY_UNDEFINED              = 0;
  public static final int    UNIQUEKEY_MERCNUM_TERMNUM        = 1;

  /**
   * MMSRequestDataItem
   * ------------------
   * Atomic unit of mms request data
  */
  public class MMSRequestDataItem extends Object
  {
     // data members
    public String nativeName;
    public String aliasName;
    public String value;

    // construction
     /*
    public MMSRequestDataItem()
     {
      nativeName="";
      aliasName="";
      value="";
     }
    */
     public MMSRequestDataItem(String nativeName,String aliasName,String value)
      throws Exception
     {
      if(nativeName==null || nativeName.length()<1)
        throw new Exception("Unable to instantiate MMSRequestDataItem - Null or zero-length native name specified.");

      this.nativeName = nativeName;
      this.aliasName  = (aliasName!=null)? aliasName:"";
      this.value      = (value!=null)? value:"";
     }
    
    public String toString()
    {
      //return ("Native name: '"+nativeName+"', Alias name: '"+aliasName+"', Value: '"+value+"'");
      return (nativeName+" = '"+value+"'");
    }
    
  }

  
  /**
   * MMSRequestData
   * ------------------
   * Holds MMS request data in name/value pair format according to:
   * FORMAT:
   * ------
   * Name:  nativeName
   * Value: MMSRequestDataItem()
   *        - nativeName
   *        - mms screen name (aliasName)
   *        - value
  */
  class MMSRequestData extends Object
  {
     // data members
     private Hashtable dataItems;

    // constructor(s)
    public MMSRequestData() 
     {
      dataItems = new Hashtable();
     }
     
     // methods
    public String toString()
    {
      String s = new String();
      
      // output data items
      //MMSRequestDataItem di;
      for (Enumeration e = dataItems.keys(); e.hasMoreElements(); )
        s+=dataItems.get((String)e.nextElement()).toString()+"\n";
    
      return s;
    }
    
  }  // class MMSRequestData
  
  // data members
  private   String              reqID;
  private   String              processMethod;
  private   Date                processStartDate;
  private   Date                processEndDate;
  private   String              processStatus;
  private   String              processResponse;
  private   String              vnum;               // exists here as well as in 'mmsReqData' hashtable data member!
  protected MMSRequestData      mmsReqData;
  protected Vector              processErrors;
  protected String              appSrcTypeCode;
  
  // construction
  public MMSRequest()
  {
    mmsReqData        = new MMSRequestData();
    processErrors     = null;
    processStartDate  = new Date(0);
    processEndDate    = new Date(0);
    
    clear();  // initialize
  }

   // accessors
  public String getReqID()            { return reqID; }
  public String getProcessMethod()    { return processMethod; }
  public Date   getProcessStartDate() { return processStartDate; }
  public Date   getProcessEndDate()   { return processEndDate; }
  public String getProcessStatus()    { return processStatus; }
  public String getProcessResponse()  { return processResponse; }
  public String getVNum()             { return vnum; }
  
  public String getRequestDataItemValue(String nativeName)
  {
    return mmsReqData.dataItems.containsKey(nativeName)? 
          ((MMSRequestDataItem)mmsReqData.dataItems.get(nativeName)).value:"";
  }
  
  public String getRequestDataItemAliasName(String nativeName)
  {
    return mmsReqData.dataItems.containsKey(nativeName)? 
          ((MMSRequestDataItem)mmsReqData.dataItems.get(nativeName)).value:"";
  }
  
  public MMSRequestError getMMSRequestError(String erCode)
  {
    log.error("getMMSRequestError() - NOT IMPLEMENTED.");
    return null;
  }
  
  public String getTermAppName()
  {
    return mmsReqData.dataItems.containsKey("TERM_APPLICATION")? 
      ((MMSRequestDataItem)mmsReqData.dataItems.get("TERM_APPLICATION")).value:"";
  }
  
  public int getNumRequestDataItems()
  {
    return mmsReqData.dataItems.size();
  }
  
  public int getNumProcessErrors()
  {
    return (null==processErrors)? 0:processErrors.size();
  }
  
  public Vector getProcessErrors()
  {
    return processErrors;
  }
  
  public Enumeration getMMSRequestDataEnumerator()
  {
    return mmsReqData.dataItems.keys();
  }
  
  public Enumeration getProcessErrorsEnumerator()
    throws Exception
  {
    if(null==processErrors)
      throw new Exception("Unable to return process errors enumeration: No process errors exist.");
    return processErrors.elements();
  }
  
  /**
   * getProcessErrors_internal()
   * ---------------------------
   *  - use this ONLY to access member: 'processErrors'
   *  - safe accessor function for processErrors member var to manage lazy initialization
   */
  protected Vector getProcessErrors_internal()
  {
    if(null==processErrors)
      processErrors = new Vector(3,1);
    return processErrors;
  }

  // mutators
  public void setReqID(String v)            { if(v!=null) reqID=v; }
  public void setProcessMethod(String v)    { if(v!=null) processMethod=v; }
  public void setProcessStartDate(Date v)   { if(v!=null) processStartDate=v; }
  public void setProcessEndDate(Date v)     { if(v!=null) processEndDate=v; }
  public void setProcessStatus(String v)    { if(v!=null) processStatus=v; }
  public void setProcessResponse(String v)  { if(v!=null) processResponse=v; }
  
  public void setVNum(String v)
  {
    if(v==null)
      return;
    try {
      setRequestDataItem("VNUM","",v);
      this.vnum=v;
    }
    catch(Exception e) {
      log.error("setVNum() Exception: "+e.getMessage());
    }
  }
  
  public void setTermAppName(String v)
  {
    if(v==null || v.length()<1)
      return;
    
    try {
      if(!mmsReqData.dataItems.containsKey("TERM_APPLICATION"))
        mmsReqData.dataItems.put("TERM_APPLICATION",new MMSRequestDataItem("TERM_APPLICATION","",v));
      else
        ((MMSRequestDataItem)mmsReqData.dataItems.get("TERM_APPLICATION")).value=v;
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
  }

  public void setRequestDataItem(String nativeName, String aliasName, String value)
  {
    try {
      
      MMSRequestDataItem item = new MMSRequestDataItem(nativeName,aliasName,value);
    
      if(mmsReqData.dataItems.containsKey(item.nativeName)) {
        MMSRequestDataItem mmsRDI=(MMSRequestDataItem)mmsReqData.dataItems.get(item.nativeName);
        //mmsRDI.nativeName=item.nativeName;
        mmsRDI.aliasName=item.aliasName;
        mmsRDI.value=item.value;
      } else {
        MMSRequestDataItem mmsRDI = new MMSRequestDataItem(item.nativeName,item.aliasName,item.value);
        mmsReqData.dataItems.put(mmsRDI.nativeName,mmsRDI);
      }
      
      if(item.nativeName.equals("VNUM"))
        vnum=item.value;
    
    }
    catch(Exception e) {
      log.error("setRequestDataItem() Exception: "+e.getMessage());
    }
  }
  
  public void addProcessError(MMSRequestError mmsRequestError)
  {
    if(mmsRequestError!=null)
      getProcessErrors_internal().add(mmsRequestError);
  }
  
  public void setProcessErrors(Vector processErrors)
  {
    this.processErrors = processErrors;
  }
  
  public void clear()
  {
    mmsReqData.dataItems.clear();
    clearProcessErrors();
    
    reqID           = "";
    processMethod   = "";
    processStatus   = "";
    processResponse = "";
    vnum            = "";
    appSrcTypeCode  = "";
    processStartDate.setTime(0);
    processEndDate.setTime(0);
  }
  
  public String getAppSrcTypeCode()
  {
    return appSrcTypeCode;
  }

  public void setAppSrcTypeCode(String v)
  {
    if(v!=null)
      appSrcTypeCode = v;
  }
  
  public void clearProcessErrors()
  {
    if(processErrors!=null)
      processErrors.clear();
  }
  
  public final String[] getUniqueKey(int key)
    throws Exception
  {
    if(key == UNIQUEKEY_MERCNUM_TERMNUM) {
      return new String[] { "MERCH_NUMBER", "TERMINAL_NUMBER" };
    } else
      throw new Exception("getUniqueKey() Exception: Unhandled mms request unique key: '"+key+"'.");
    
  }
  
  public final boolean isEqualUniqueKey(MMSRequest v,int key)
    throws Exception
  {
    if(key == UNIQUEKEY_MERCNUM_TERMNUM) {
      return
        (!mmsReqData.dataItems.containsKey("MERCH_NUMBER") || !mmsReqData.dataItems.containsKey("TERMINAL_NUMBER") || !v.mmsReqData.dataItems.containsKey("MERCH_NUMBER") || !v.mmsReqData.dataItems.containsKey("TERMINAL_NUMBER"))?
          false:
          ((MMSRequestDataItem)mmsReqData.dataItems.get("MERCH_NUMBER")).value.equals(((MMSRequestDataItem)v.mmsReqData.dataItems.get("MERCH_NUMBER")).value)
           && ((MMSRequestDataItem)mmsReqData.dataItems.get("TERMINAL_NUMBER")).value.equals(((MMSRequestDataItem)v.mmsReqData.dataItems.get("TERMINAL_NUMBER")).value);
    } else
      throw new Exception("isEqualUniqueKey() Exception: Unhandled mms request unique key: '"+key+"'.");
  }
  
  public String toString()
  {
    //log.debug("mms request toString() - START");
    
    String s = "\nreqID="   +reqID
      +"\nprocessMethod="   +processMethod
      +"\nprocessStartDate="+processStartDate
      +"\nprocessEndDate="  +processEndDate
      +"\nprocessStatus="   +processStatus
      +"\nprocessResponse=" +processResponse
      +"\nvnum="            +vnum;
      
    if(getNumRequestDataItems() > 0) {
      s += "\n\nMMS Request Data:\n"
          +mmsReqData.toString();
    }
    
    // process errors (if any)
    if(getNumProcessErrors() > 0) {
      s += "\n\nMMS Request Errors:\n";
      for(int i=0; i<processErrors.size(); i++)
        s += processErrors.elementAt(i).toString()+"\n";
    }
    
    //log.debug("mms request toString() - END");

    return s;
  }
  
}
