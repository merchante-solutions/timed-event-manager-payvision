/*@lineinfo:filename=DBQueueOps_MMSRequest*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/DBQueueOps_MMSRequest.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 6/18/04 4:40p $
  Version            : $Revision: 22 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Enumeration;
import java.util.Hashtable;
// log4j classes.
import org.apache.log4j.Category;
import sqlj.runtime.ResultSetIterator;

/**
 * DBQueueOps_MMSRequest
 * ---------------------
 *  - Performs all MMSRequest db related ops
 
 *  MMS Request db struct
 *   ---------------------
 *   MMS_STAGE_INFO <1--1> MMS_EXPANDED_INFO <1--Mny> MMS_CURRENT_ERRORS
 *      
*/
public class DBQueueOps_MMSRequest extends com.mes.database.SQLJConnectionDirect
{
  // create class log category
  static Category log = Category.getInstance(DBQueueOps_MMSRequest.class.getName());
  
  // Singleton
  public static DBQueueOps_MMSRequest getInstance()
  {
    if(_instance==null)
      _instance = new DBQueueOps_MMSRequest();
    
    return _instance;
  }
  private static DBQueueOps_MMSRequest _instance = null;
  ///
  
  // constants
  public static final int DBQ_TYPE_INPUT    = 1;
  public static final int DBQ_TYPE_INPROC   = 2;
  public static final int DBQ_TYPE_PENDING  = 3;    // aka error
  public static final int DBQ_TYPE_OUTPUT   = 4;    // aka successful (activation)
    // in order to map both MMSRequest.processStatus and MMSRequest.processResponse 
    //  to a single variable
    
  // data members
  private int dbTransCount;

  // construction
  private DBQueueOps_MMSRequest()
  {
    // - use direct (JDBC) connect (not connection pool)
    // - disable auto commit
    super(false);
    
    dbTransCount=0;
  }
  
  /**
   * load()
   *  - SINGLE RECORD load f()
   *  - retreives mms request of request ID spec in mmsReqID arg into the mmsRequest arg
   *  - presumes rec. of given request id already exists
   *  - non-existant rec. (via ReqID) ==> error 
   */
  public synchronized boolean load(String mmsReqID,MMSRequest mmsRequest)
  {
    try {
      
      // clear out mms request - prep for load
      mmsRequest.clear();
      
      dbTransStart();
    
      // retreive mmsRequest record
      ResultSetIterator rsItr = null;
      /*@lineinfo:generated-code*//*@lineinfo:103^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { select  mms_stage_info.request_id as reqid
//                  ,mms_stage_info.*
//                  ,mms_expanded_info.*
//                  ,decode(at.separate_mms_file, 'Y', a.appsrctype_code, 'MESR') appsrctype_code
//          from    mms_stage_info
//                  ,mms_expanded_info
//                  ,application a
//                  ,app_type at
//          where   mms_stage_info.request_id=mms_expanded_info.request_id(+) 
//                  and mms_stage_info.request_id=:mmsReqID
//                  and process_method<>'MANUAL'
//                  and a.app_seq_num = mms_stage_info.app_seq_num
//                  and a.app_type = at.app_type_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mms_stage_info.request_id as reqid\n                ,mms_stage_info.*\n                ,mms_expanded_info.*\n                ,decode(at.separate_mms_file, 'Y', a.appsrctype_code, 'MESR') appsrctype_code\n        from    mms_stage_info\n                ,mms_expanded_info\n                ,application a\n                ,app_type at\n        where   mms_stage_info.request_id=mms_expanded_info.request_id(+) \n                and mms_stage_info.request_id= :1 \n                and process_method<>'MANUAL'\n                and a.app_seq_num = mms_stage_info.app_seq_num\n                and a.app_type = at.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.mms.autoprocess.DBQueueOps_MMSRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mmsReqID);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.mms.autoprocess.DBQueueOps_MMSRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:118^7*/
      ResultSet rs = rsItr.getResultSet();
      
      rs.next();  // get to record
      
      // single record rs --> obj
      if(!dbRecToObject(rs,mmsRequest)) {
        rs.close();
        log.error("Unable to load current MMS Request record into MMSRequest object. Load failed.");
        return false;
      }
      
      rs.close();
      rsItr.close();
      
      //log.debug("about to add any errors to mms request object (Req ID: '"+mmsRequest.getReqID()+"')...");
      
      // add any current errors to mms request object
      /*@lineinfo:generated-code*//*@lineinfo:136^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { select  request_id as reqid
//                  ,error_code
//                  ,error_type
//                  ,error_description
//          from    mms_current_errors
//          where   request_id = :mmsRequest.getReqID()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1809 = mmsRequest.getReqID();
  try {
   String theSqlTS = "select  request_id as reqid\n                ,error_code\n                ,error_type\n                ,error_description\n        from    mms_current_errors\n        where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.mms.autoprocess.DBQueueOps_MMSRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1809);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.mms.autoprocess.DBQueueOps_MMSRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:144^7*/
      rs = rsItr.getResultSet();
      
      // add error rs --> obj
      while(rs.next())
        mmsRequest.addProcessError(new MMSRequestError(rs.getString(1),rs.getString(2),rs.getString(3),"",rs.getString(4)));
        
      rs.close();
      rsItr.close();

      return true;
    }
    catch(Exception e) {
      log.error(e.getMessage());
      return false;
    }
    finally {
      dbTransEnd();
    }
  }

  
  /**
   * save()
   *  - SINGLE-RECORD save f()
   *  - saves given mms request to db
   *  - presumes that corres. rec. already exists and therefore only updates
   *  - non-existant rec. (via ReqID) ==> error 
   *  - does NOT update data kept in MMSRequest.MMSRequestData rather only 
   *     'primary' mms request data and assoc. mms ERRORs
   */
  public synchronized boolean save(MMSRequest mmsRequest)
  {
    try {
    
      dbTransStart();
        
      log.info("Saving MMS Request: ID: '"+mmsRequest.getReqID()+"', Status: '"+mmsRequest.getProcessStatus()+"', Response: '"+mmsRequest.getProcessResponse()+"', Vnum: '"+mmsRequest.getVNum()+"'...");
      
      // clear existing mms request errors
      //log.debug("save() - deleting existing mms request errors (Req ID: '"+mmsRequest.getReqID()+"')...");
      /*@lineinfo:generated-code*//*@lineinfo:185^7*/

//  ************************************************************
//  #sql [Ctx] { delete  mms_current_errors
//          where   request_id=:mmsRequest.getReqID()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1810 = mmsRequest.getReqID();
   String theSqlTS = "delete  mms_current_errors\n        where   request_id= :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.mms.autoprocess.DBQueueOps_MMSRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1810);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:189^7*/
      
      // insert any mms request errors
      //log.debug("save() - saving loaded mms request errors...");
      MMSRequestError mmsReqError = null;
      if(mmsRequest.getNumProcessErrors() > 0) {
        for(Enumeration e=mmsRequest.getProcessErrorsEnumerator(); e.hasMoreElements();) {
          mmsReqError=(MMSRequestError)e.nextElement();
      
          //log.debug("save() - inserting mms request error (Code: '"+mmsReqError.getCode()+"')...");

          /*@lineinfo:generated-code*//*@lineinfo:200^11*/

//  ************************************************************
//  #sql [Ctx] { insert into mms_current_errors
//                          (
//                            request_id
//                            ,error_code
//                            ,error_type
//                            ,error_description )
//              values      (
//                          :mmsRequest.getReqID()
//                          ,:mmsReqError.getCode()
//                          ,:mmsReqError.getType()
//                          ,:mmsReqError.getDesc() )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1811 = mmsRequest.getReqID();
 String __sJT_1812 = mmsReqError.getCode();
 String __sJT_1813 = mmsReqError.getType();
 String __sJT_1814 = mmsReqError.getDesc();
   String theSqlTS = "insert into mms_current_errors\n                        (\n                          request_id\n                          ,error_code\n                          ,error_type\n                          ,error_description )\n            values      (\n                         :1 \n                        , :2 \n                        , :3 \n                        , :4  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.mms.autoprocess.DBQueueOps_MMSRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1811);
   __sJT_st.setString(2,__sJT_1812);
   __sJT_st.setString(3,__sJT_1813);
   __sJT_st.setString(4,__sJT_1814);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:213^11*/
        }
      }
      //log.debug("save() - END of saving mms request errors.");
      
        // convert the java.util.Date to a java.sql.Timestamp
        java.sql.Timestamp sqlDate_start = new java.sql.Timestamp(mmsRequest.getProcessStartDate().getTime());
        java.sql.Timestamp sqlDate_end = new java.sql.Timestamp(mmsRequest.getProcessEndDate().getTime());

      // save primary mms request
      /*
      log.debug("save() - saving primary mms request...");
      log.debug("          mmsRequest.getProcessMethod()="+mmsRequest.getProcessMethod());
      log.debug("          sqlDate_start="+sqlDate_start);
      log.debug("          sqlDate_end="+sqlDate_end);
      log.debug("          mmsRequest.getProcessStatus()="+mmsRequest.getProcessStatus());
      log.debug("          mmsRequest.getProcessResponse()="+mmsRequest.getProcessResponse());
      log.debug("          mmsRequest.getVNum()="+mmsRequest.getVNum());
      log.debug("          mmsRequest.getReqID()="+mmsRequest.getReqID());
      */
      /*@lineinfo:generated-code*//*@lineinfo:233^7*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//          set     process_method      = :mmsRequest.getProcessMethod()
//                  ,process_start_date = :sqlDate_start
//                  ,process_end_date   = :sqlDate_end
//                  ,process_status     = :mmsRequest.getProcessStatus()
//                  ,process_response   = :mmsRequest.getProcessResponse()
//                  ,vnum               = :mmsRequest.getVNum()
//          where   request_id = :mmsRequest.getReqID()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1815 = mmsRequest.getProcessMethod();
 String __sJT_1816 = mmsRequest.getProcessStatus();
 String __sJT_1817 = mmsRequest.getProcessResponse();
 String __sJT_1818 = mmsRequest.getVNum();
 String __sJT_1819 = mmsRequest.getReqID();
   String theSqlTS = "update  mms_stage_info\n        set     process_method      =  :1 \n                ,process_start_date =  :2 \n                ,process_end_date   =  :3 \n                ,process_status     =  :4 \n                ,process_response   =  :5 \n                ,vnum               =  :6 \n        where   request_id =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.mms.autoprocess.DBQueueOps_MMSRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1815);
   __sJT_st.setTimestamp(2,sqlDate_start);
   __sJT_st.setTimestamp(3,sqlDate_end);
   __sJT_st.setString(4,__sJT_1816);
   __sJT_st.setString(5,__sJT_1817);
   __sJT_st.setString(6,__sJT_1818);
   __sJT_st.setString(7,__sJT_1819);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^7*/
      
      //log.debug("save() - committing db trans!...");
      /*@lineinfo:generated-code*//*@lineinfo:246^7*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:246^27*/
      //commit();
      
      return true;
    }
    catch(Exception e) {
      log.error(e.getMessage());
      return false;
    }
    finally {
      dbTransEnd();
    }

  }
  
  
  /**
   * load()
   *  - MULTI-RECORD load f() - MMSRequest implementation
   *  - returns number of loaded mms requests
   */
  public synchronized int load(int dbqType,Hashtable mmsRequests,boolean bOverwrite)
  {
    try {
    
      dbTransStart();
      
      ResultSet rs = null,rsErr = null;
      ResultSetIterator rsItr = null;
      
      if(dbqType == DBQ_TYPE_INPUT) {
        /*@lineinfo:generated-code*//*@lineinfo:277^9*/

//  ************************************************************
//  #sql [Ctx] rsItr = { select    mms_stage_info.request_id as reqid
//                      ,mms_stage_info.*
//                      ,mms_expanded_info.*
//                      ,decode(at.separate_mms_file, 'Y', a.appsrctype_code, 'MESR') appsrctype_code
//            from      mms_stage_info
//                      ,mms_expanded_info
//                      ,application a
//                      ,app_type    at
//            where     mms_stage_info.request_id=mms_expanded_info.request_id(+)
//                      and (process_status='new' or process_status='resubmitted')
//                      and process_method<>'MANUAL'
//                      and a.app_seq_num = mms_stage_info.app_seq_num  
//                      and a.app_type = at.app_type_code
//            order by  process_start_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mms_stage_info.request_id as reqid\n                    ,mms_stage_info.*\n                    ,mms_expanded_info.*\n                    ,decode(at.separate_mms_file, 'Y', a.appsrctype_code, 'MESR') appsrctype_code\n          from      mms_stage_info\n                    ,mms_expanded_info\n                    ,application a\n                    ,app_type    at\n          where     mms_stage_info.request_id=mms_expanded_info.request_id(+)\n                    and (process_status='new' or process_status='resubmitted')\n                    and process_method<>'MANUAL'\n                    and a.app_seq_num = mms_stage_info.app_seq_num  \n                    and a.app_type = at.app_type_code\n          order by  process_start_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.mms.autoprocess.DBQueueOps_MMSRequest",theSqlTS);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.mms.autoprocess.DBQueueOps_MMSRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:293^9*/
      } else if(dbqType == DBQ_TYPE_INPROC) {
        /*@lineinfo:generated-code*//*@lineinfo:295^9*/

//  ************************************************************
//  #sql [Ctx] rsItr = { select    mms_stage_info.request_id as reqid
//                      ,mms_stage_info.*
//                      ,mms_expanded_info.*
//                      ,decode(at.separate_mms_file, 'Y', a.appsrctype_code, 'MESR') appsrctype_code
//            from      mms_stage_info
//                      ,mms_expanded_info
//                      ,application a
//                      ,app_type    at
//            where     mms_stage_info.request_id=mms_expanded_info.request_id(+)
//                      and process_status='inproc'
//                      and process_method<>'MANUAL'
//                      and a.app_seq_num = mms_stage_info.app_seq_num  
//                      and a.app_type = at.app_type_code
//            order by  process_start_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mms_stage_info.request_id as reqid\n                    ,mms_stage_info.*\n                    ,mms_expanded_info.*\n                    ,decode(at.separate_mms_file, 'Y', a.appsrctype_code, 'MESR') appsrctype_code\n          from      mms_stage_info\n                    ,mms_expanded_info\n                    ,application a\n                    ,app_type    at\n          where     mms_stage_info.request_id=mms_expanded_info.request_id(+)\n                    and process_status='inproc'\n                    and process_method<>'MANUAL'\n                    and a.app_seq_num = mms_stage_info.app_seq_num  \n                    and a.app_type = at.app_type_code\n          order by  process_start_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.mms.autoprocess.DBQueueOps_MMSRequest",theSqlTS);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.mms.autoprocess.DBQueueOps_MMSRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:311^9*/
      } else if(dbqType == DBQ_TYPE_PENDING) {
        /*@lineinfo:generated-code*//*@lineinfo:313^9*/

//  ************************************************************
//  #sql [Ctx] rsItr = { select    mms_stage_info.request_id as reqid
//                      ,mms_stage_info.*
//                      ,mms_expanded_info.*
//                      ,decode(at.separate_mms_file, 'Y', a.appsrctype_code, 'MESR') appsrctype_code
//            from      mms_stage_info
//                      ,mms_expanded_info
//                      ,application a
//                      ,app_type    at
//            where     mms_stage_info.request_id=mms_expanded_info.request_id(+)
//                      and process_status='received' and process_response='pending'
//                      and process_method<>'MANUAL'
//                      and a.app_seq_num = mms_stage_info.app_seq_num  
//                      and a.app_type = at.app_type_code
//            order by  process_start_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mms_stage_info.request_id as reqid\n                    ,mms_stage_info.*\n                    ,mms_expanded_info.*\n                    ,decode(at.separate_mms_file, 'Y', a.appsrctype_code, 'MESR') appsrctype_code\n          from      mms_stage_info\n                    ,mms_expanded_info\n                    ,application a\n                    ,app_type    at\n          where     mms_stage_info.request_id=mms_expanded_info.request_id(+)\n                    and process_status='received' and process_response='pending'\n                    and process_method<>'MANUAL'\n                    and a.app_seq_num = mms_stage_info.app_seq_num  \n                    and a.app_type = at.app_type_code\n          order by  process_start_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.mms.autoprocess.DBQueueOps_MMSRequest",theSqlTS);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.mms.autoprocess.DBQueueOps_MMSRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:329^9*/
      } else if(dbqType == DBQ_TYPE_OUTPUT) {
        /*@lineinfo:generated-code*//*@lineinfo:331^9*/

//  ************************************************************
//  #sql [Ctx] rsItr = { select    mms_stage_info.request_id as reqid
//                      ,mms_stage_info.*
//                      ,mms_expanded_info.*
//                      ,decode(at.separate_mms_file, 'Y', a.appsrctype_code, 'MESR') appsrctype_code
//            from      mms_stage_info
//                      ,mms_expanded_info
//                      ,application a
//                      ,app_type    at
//            where     mms_stage_info.request_id=mms_expanded_info.request_id(+)
//                      and process_status='received' and process_response='success'
//                      and process_method<>'MANUAL'
//                      and a.app_seq_num = mms_stage_info.app_seq_num  
//                      and a.app_type = at.app_type_code
//            order by  process_start_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mms_stage_info.request_id as reqid\n                    ,mms_stage_info.*\n                    ,mms_expanded_info.*\n                    ,decode(at.separate_mms_file, 'Y', a.appsrctype_code, 'MESR') appsrctype_code\n          from      mms_stage_info\n                    ,mms_expanded_info\n                    ,application a\n                    ,app_type    at\n          where     mms_stage_info.request_id=mms_expanded_info.request_id(+)\n                    and process_status='received' and process_response='success'\n                    and process_method<>'MANUAL'\n                    and a.app_seq_num = mms_stage_info.app_seq_num  \n                    and a.app_type = at.app_type_code\n          order by  process_start_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.mms.autoprocess.DBQueueOps_MMSRequest",theSqlTS);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.mms.autoprocess.DBQueueOps_MMSRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:347^9*/
      }
      rs = rsItr.getResultSet();
      
      boolean bLoad = false;
      int numloaded = 0;
      MMSRequest mmsRequest = null;
      ResultSetIterator rsErrItr = null;

      while(rs.next()) {
        //log.debug("REQID="+rs.getString("REQID"));
        bLoad = bOverwrite? true : !mmsRequests.containsKey(rs.getString("REQID"));
        //log.debug("bLoad="+bLoad);
        if(bLoad) {
          mmsRequest = new MMSRequest();
          
          if(!dbRecToObject(rs,mmsRequest))
            log.error("Unable to load current MMS Request record (Request ID: '"+rs.getString("REQID")+"') into MMSRequest object.  Record skipped.");
          else {
            
            // add any current errors to mms request object
            /*@lineinfo:generated-code*//*@lineinfo:368^13*/

//  ************************************************************
//  #sql [Ctx] rsErrItr = { select  request_id as reqid
//                        ,error_code
//                        ,error_type
//                        ,error_description
//                from    mms_current_errors
//                where   request_id = :mmsRequest.getReqID()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1820 = mmsRequest.getReqID();
  try {
   String theSqlTS = "select  request_id as reqid\n                      ,error_code\n                      ,error_type\n                      ,error_description\n              from    mms_current_errors\n              where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.mms.autoprocess.DBQueueOps_MMSRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1820);
   // execute query
   rsErrItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.mms.autoprocess.DBQueueOps_MMSRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:376^13*/
            rsErr = rsErrItr.getResultSet();
      
            // add error rs --> obj
            while(rsErr.next())
              mmsRequest.addProcessError(new MMSRequestError(rsErr.getString(1),rsErr.getString(2),rsErr.getString(3),"",rsErr.getString(4)));
              
            rsErr.close();
            rsErrItr.close();

            // load
            mmsRequests.put(mmsRequest.getReqID(),mmsRequest);
            numloaded++;
          }
        }
      }
      
      rs.close();
      rsItr.close();
      
      return numloaded;
    }
    catch(Exception e) {
      log.error("load(MULTI) Exception: "+e.getMessage());
      mmsRequests.clear();
      return 0;
    }
    finally {
      dbTransEnd();
    }
    
  }  // load()
  
  /**
   * save()
   *  - MULTI-RECORD save f()
   *  - saves mms requests to db contained in hashtable arg
   *  - returns number of saved mms requests
   *  - calls SINGLE-RECORD save f() for each mms request in mms request hashtable arg
   */
  public synchronized boolean save(Hashtable mmsRequests)
  {
    try {
      
      dbTransStart();
    
      String REQID;
      MMSRequest mmsRequest = null;
    
      for(Enumeration e=mmsRequests.keys(); e.hasMoreElements(); ) {
        REQID=(String)e.nextElement();
        mmsRequest=(MMSRequest)mmsRequests.get(REQID);
        if(!save(mmsRequest)) {
          log.error("Error saving mms request (ReqID: '"+mmsRequest.getReqID()+"').  Aborted.");
          return false;
        }
      }
      
      return true;
    }
    catch(Exception e) {
      log.error("save(MULTI) Exception: '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      dbTransEnd();
    }
  }  // save()

  protected boolean dbRecToObject(ResultSet rs, MMSRequest mmsRequest)
  {
    try {
    
      ResultSetMetaData rsMtaDta = rs.getMetaData();
      String sDta;
      
      for(int i=1; i<=rsMtaDta.getColumnCount(); i++) {
      
        sDta = rs.getString(i);
        if(null == sDta)
          sDta = "";
          
        //log.debug(rsMtaDta.getColumnName(i)+" = "+sDta);
        
        if(rsMtaDta.getColumnName(i).equals("REQID"))
          mmsRequest.setReqID(sDta);
        else if(rsMtaDta.getColumnName(i).equals("APPSRCTYPE_CODE"))
          mmsRequest.setAppSrcTypeCode(sDta);
        else if(rsMtaDta.getColumnName(i).equals("PROCESS_METHOD"))
          mmsRequest.setProcessMethod(sDta);
        else if(rsMtaDta.getColumnName(i).equals("PROCESS_START_DATE")) {
          if(rs.getDate(i) != null)
            mmsRequest.setProcessStartDate(rs.getTimestamp(i));
        } else if(rsMtaDta.getColumnName(i).equals("PROCESS_END_DATE")) {
          if(rs.getDate(i) != null)
            mmsRequest.setProcessEndDate(rs.getTimestamp(i));
        } else if(rsMtaDta.getColumnName(i).equals("PROCESS_STATUS"))
          mmsRequest.setProcessStatus(sDta);
        else if(rsMtaDta.getColumnName(i).equals("PROCESS_RESPONSE"))
          mmsRequest.setProcessResponse(sDta);
        else if(rsMtaDta.getColumnName(i).equals("VNUM"))
          mmsRequest.setVNum(sDta);
        else
          mmsRequest.setRequestDataItem(rsMtaDta.getColumnName(i),"",sDta);
      }

      // add mode of operation - Insert(New account)
      mmsRequest.setRequestDataItem("MODE_OF_OPERATION","","I");
    
    }
    catch(Exception e) {
         log.error("dbRecToObject() - Exception: "+e.getMessage());
      return false;
    }
    
    return true;
  }
  
  public void dbTransStart()
  {
    try {
      
      if(dbTransCount<1 || isConnectionStale()) {
        log.info("Connecting to db...");
        connect();
        if(con==null)
          log.error("dbTransStart(): NULL db connection returned!");
        if(Ctx==null)
          log.error("dbTransStart(): NULL db Ctx returned!");
      }
    
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
    finally {
      dbTransCount++;
      log.debug("dbTransStart(): dbTransCount="+dbTransCount);
    }
  }
  
  public void dbTransEnd()
  {
    try {
      
      dbTransCount--;
      log.debug("dbTransEnd(): dbTransCount="+dbTransCount);
      
      if(dbTransCount<1) {
        log.info("Cleaning up db connection...");
        cleanUp();
      }
      
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
  }
  
}/*@lineinfo:generated-code*/