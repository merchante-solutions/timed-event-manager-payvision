/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/PersistQueue_MMSRequest_Input.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/24/02 10:32a $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;

// log4j classes.
import org.apache.log4j.Category;

/**
 * PersistQueue_MMSRequest_Input
 * -----------------------------
*/
public class PersistQueue_MMSRequest_Input extends PersistQueue_MMSRequest
{
  // create class log category
  static Category log = Category.getInstance(PersistQueue_MMSRequest_Input.class.getName());

  // construction
  public PersistQueue_MMSRequest_Input()
    throws Exception
  {
    super(true);
  }
  
  
  public void load()
    throws Exception
  {
    super.load(DBQueueOps_MMSRequest.DBQ_TYPE_INPUT,false);
  }
  
}
