/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/MMSRequestError.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/24/02 10:32a $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;

// log4j classes.
import org.apache.log4j.Category;

/**
 * MMSRequestError
 * ---------------
 * Container class for MMS request processing error information
 */
public class MMSRequestError extends Object
{
  // create class log category
  static Category log = Category.getInstance(MMSRequestError.class.getName());
  
  // data members
  protected String erID;
  protected String erCode;
  protected String erType;
  protected String erName;
  protected String erDesc;

  // construction
  public MMSRequestError()
  {
    this.erID="";
    this.erCode="";
    this.erType="";
    this.erName="";
    this.erDesc="";
  }
  public MMSRequestError(String erID,String erCode,String erType,String erName,String erDesc)
  {
    this.erID=erID;
    this.erCode=erCode;
    this.erType=erType;
    this.erName=erName;
    this.erDesc=erDesc;
  }
  
  public String getID() { return erID; }
  public String getCode() { return erCode; }
  public String getType() { return erType; }
  public String getName() { return erName; }
  public String getDesc() { return erDesc; }

  public void setID(String v) { if(v!=null) erID=v; }
  public void setCode(String v) { if(v!=null) erCode=v; }
  public void setType(String v) { if(v!=null) erType=v; }
  public void setName(String v) { if(v!=null) erName=v; }
  public void setDesc(String v) { if(v!=null) erDesc=v; }
  
  public String toString()
  {
    String s = new String();
    
    s = "erID='"+erID+
       "', erCode='"+erCode+
       "', erType='"+erType+
       "', erName='"+erName+
       "', erDesc='"+erDesc+"'";
    
    return s;
  }
}
