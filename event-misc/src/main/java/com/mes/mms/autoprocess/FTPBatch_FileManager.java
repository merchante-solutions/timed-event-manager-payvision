package com.mes.mms.autoprocess;

import java.io.File;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.mes.constants.mesConstants;
import com.mes.support.PGPCmdLine;
import com.mes.support.PropertiesFile;


/**
 * FTPBatch_FileManager
 * --------------------
 *  - Manages mms request ftp batch files that are sent to Vital for processing
 *  - ftp batch files are created, ammended,FTP'd and removed 
 *     from a spec. working directory by way of this class
 *  - each ftp batch file corresponds to a single terminal application
 *     (where each filename is that of MMSRequest.TERM_APPLICATION).value
 *  - for each ftp batch file, each row represents a single mms request
 *  - enforces the constraint:
 *     MAXIMUM 200 mms requests per ftp batch file
 *  - Encapsulates periodic ftp dispatch logic via a separate thread via implementing Runnable interface
 *  - Singleton object model - only one ftpBatch file manager can exist esp. considering thread logic employed
 */
public final class FTPBatch_FileManager extends Object implements Runnable
{
  // create class log category
  public static Category log = Category.getInstance(FTPBatch_FileManager.class.getName());

  // Singleton
  public static FTPBatch_FileManager getInstance()
  {
    if(_instance==null) 
    {
      try 
      {
        _instance = new FTPBatch_FileManager(ftpBatchRootDir);
      }
      catch(Exception e) 
      {
        _instance = null;
        log.error("FTPBatch_FileManager - Unable to instantiate due to exception: "+e.getMessage()+".");
      }
    }
    
    return _instance;
  }
  
  private static FTPBatch_FileManager _instance = null;


  // data members
  public static String  ftpBatchRootDir;  // MUST BE SET IN ORDER TO BE INSTANTIATED
  
  public  int       FTPBATCH_PROCESS_MILIS_MIN;
  private int       DISK_QUERY_FREQUENCY_MILIS;

  //i need to change these so they dont only occur once a day.. 
  //but rather atleast 3 times a day or better yet every so many milliseconds
  private int       FTPBATCH_HOUR24_OF_DAY_1;
  private int       FTPBATCH_HOUR24_OF_DAY_2;
  private int       FTPBATCH_HOUR24_OF_DAY_3;

  private int       RESPONSECHECK_HOUR24_OF_DAY_1;
  private int       RESPONSECHECK_HOUR24_OF_DAY_2;
  private int       RESPONSECHECK_HOUR24_OF_DAY_3;
    // minimum turnaround time
  
  private String    FTPBATCH_PGP_USERID;
    // assoc. w/ public key employed to encrypt data that is ftp outbound

  private String  SUBDIR_NAME_OUTGOING;
  private String  SUBDIR_NAME_RESPONSE;
  private String  SUBDIR_NAME_ARCHIVE;
  private String  SUBDIR_NAME_TEMPLATES;

  private int     ARCHIVE_AGE_MILIS;

  /**
   * class Ola
   *
   * Used to house a single ola's credentials for access onto Vital's ftp system
   */
  private class     Ola
  {
    public String   OLA_NAME       = "";
    public String   FTP_HOST       = "";
    public int      FTP_PORT       = 0;
    public String   FTP_USERNAME   = "";
    public String   FTP_PASSWORD   = "";
  };

  private Vector    olas = null;


  private boolean bResourceLock;
  // semaphore for managing concurrent resource access
  private Thread clockThread = null;
  private boolean isClockThreadStarted = false;

  // construction
  private FTPBatch_FileManager(String ftpBatchRootDir) throws Exception
  {
    
    log.debug("FTPBatch_FileManager(Constructor) - START");

    olas = new Vector(0,1);
    
    // configure
    //log.debug("file = '"+FTPBatch_FileManager.class.getName()+".properties'");
    PropertiesFile props = new PropertiesFile("FTPBatch_FileManager.properties");

    FTPBATCH_PROCESS_MILIS_MIN    = props.getInt("FTPBATCH_PROCESS_MILIS_MIN");
    DISK_QUERY_FREQUENCY_MILIS    = props.getInt("DISK_QUERY_FREQUENCY_MILIS");
    FTPBATCH_HOUR24_OF_DAY_1      = props.getInt("FTPBATCH_HOUR24_OF_DAY_1");
    FTPBATCH_HOUR24_OF_DAY_2      = props.getInt("FTPBATCH_HOUR24_OF_DAY_2");
    FTPBATCH_HOUR24_OF_DAY_3      = props.getInt("FTPBATCH_HOUR24_OF_DAY_3");
    RESPONSECHECK_HOUR24_OF_DAY_1 = props.getInt("RESPONSECHECK_HOUR24_OF_DAY_1");
    RESPONSECHECK_HOUR24_OF_DAY_2 = props.getInt("RESPONSECHECK_HOUR24_OF_DAY_2");
    RESPONSECHECK_HOUR24_OF_DAY_3 = props.getInt("RESPONSECHECK_HOUR24_OF_DAY_3");
    FTPBATCH_PGP_USERID           = props.getString("FTPBATCH_PGP_USERID");
    SUBDIR_NAME_OUTGOING          = props.getString("SUBDIR_NAME_OUTGOING");
    SUBDIR_NAME_RESPONSE          = props.getString("SUBDIR_NAME_RESPONSE");
    SUBDIR_NAME_ARCHIVE           = props.getString("SUBDIR_NAME_ARCHIVE");
    SUBDIR_NAME_TEMPLATES         = props.getString("SUBDIR_NAME_TEMPLATES");
    ARCHIVE_AGE_MILIS             = props.getInt("ARCHIVE_AGE_MILIS");
    
    // get all olas
    for(Enumeration e = props.getPropertyNames();e.hasMoreElements();) {
      String propName = (String)e.nextElement();
      log.debug("propName="+propName);
      try {
        if(propName.indexOf("FTP_USERNAME") >= 0) {
          Ola ola = new Ola();
          ola.OLA_NAME      = propName.substring(0,propName.indexOf('_'));
          log.debug("OLA_NAME="+ola.OLA_NAME);
          ola.FTP_HOST      = props.getString(ola.OLA_NAME + "_FTP_HOST");
          ola.FTP_PORT      = props.getInt(ola.OLA_NAME + "_FTP_PORT");
          ola.FTP_USERNAME  = props.getString(ola.OLA_NAME + "_FTP_USERNAME");
          ola.FTP_PASSWORD  = props.getString(ola.OLA_NAME + "_FTP_PASSWORD");
          olas.add(ola);
        }
      }
      catch(Exception ex) {
        log.error("Unable to load OLA: "+ex.getMessage());
      }
    }

    /*
    log.info("FTPBATCH_PROCESS_MILIS_MIN="+FTPBATCH_PROCESS_MILIS_MIN);
    log.info("DISK_QUERY_FREQUENCY_MILIS="+DISK_QUERY_FREQUENCY_MILIS);
    log.info("SUBDIR_NAME_ARCHIVE="+SUBDIR_NAME_ARCHIVE);
    log.info("FTPBATCH_HOUR24_OF_DAY_1="+FTPBATCH_HOUR24_OF_DAY_1);
    log.info("FTPBATCH_HOUR24_OF_DAY_2="+FTPBATCH_HOUR24_OF_DAY_2);
    log.info("FTPBATCH_HOUR24_OF_DAY_3="+FTPBATCH_HOUR24_OF_DAY_3);
    log.info("RESPONSECHECK_HOUR24_OF_DAY_1="+RESPONSECHECK_HOUR24_OF_DAY_1);
    log.info("RESPONSECHECK_HOUR24_OF_DAY_2="+RESPONSECHECK_HOUR24_OF_DAY_2);
    log.info("RESPONSECHECK_HOUR24_OF_DAY_3="+RESPONSECHECK_HOUR24_OF_DAY_3);
    log.info("FTPBATCH_PGP_USERID="+FTPBATCH_PGP_USERID);
    log.info("SUBDIR_NAME_OUTGOING="+SUBDIR_NAME_OUTGOING);
    log.info("SUBDIR_NAME_RESPONSE="+SUBDIR_NAME_RESPONSE);
    log.info("SUBDIR_NAME_ARCHIVE="+SUBDIR_NAME_ARCHIVE);
    log.info("ARCHIVE_AGE_MILIS="+ARCHIVE_AGE_MILIS);
    log.info("FTP_HOST="+FTP_HOST);
    log.info("FTP_PORT="+FTP_PORT);
    log.info("FTP_USERNAME="+FTP_USERNAME);
    log.info("FTP_PASSWORD="+FTP_PASSWORD);
    */

    // validate configuration
    String s;
    File f;

    // verify root directory is valid and exists
    s=ftpBatchRootDir;
    if(s.length()<1)
      throw new Exception("FTPBatch_FileManager construction exception: Empty root directory string specified.");
    else 
    {
      f = new File(s);
      if(!f.isDirectory())
        throw new Exception("FTPBatch_FileManager construction exception: Specified root directory: '"+s+"' is not a directory.");
    }

    // ensure outgoing dir exists
    s = ftpBatchRootDir+File.separator+SUBDIR_NAME_OUTGOING;
    f = new File(s);
    if(!f.isDirectory())
      throw new Exception("FTPBatch_FileManager construction exception: Outgoing sub-directory: '"+s+"' does not exist or is not a directory.");

    // ensure response dir exists
    s = ftpBatchRootDir+File.separator+SUBDIR_NAME_RESPONSE;
    f = new File(s);
    if(!f.isDirectory())
      throw new Exception("FTPBatch_FileManager construction exception: Response sub-directory: '"+f.getName()+"' does not exist or is not a directory.");
  
    // ensure archive dir exists
    s = ftpBatchRootDir+File.separator+SUBDIR_NAME_ARCHIVE;
    f = new File(s);
    if(!f.isDirectory())
      throw new Exception("FTPBatch_FileManager construction exception: Archive sub-directory: '"+s+"' does not exist or is not a directory.");

    // ensure templates dir exists
    s = ftpBatchRootDir+File.separator+SUBDIR_NAME_TEMPLATES;
    f = new File(s);
    if(!f.isDirectory())
      throw new Exception("FTPBatch_FileManager construction exception: Templates sub-directory: '"+s+"' does not exist or is not a directory.");
    
    // set ftp batch template sub dir static member to ensure file loading attempts
    FTPBatch_Template.DIR_TEMPLATES=ftpBatchRootDir+File.separator+SUBDIR_NAME_TEMPLATES;
    
    // set disk access related
    this.bResourceLock=false;

  }
  
  public synchronized void clockThreadStartup()
  {
    if(isClockThreadStarted)
      return;
    
    // spawn the clock thread
    log.debug("Starting ftpClock thread...");
    this.clockThread = new Thread(this, "ftpClock");
    this.clockThread.start();
    
  }
  
  public void clockThreadShutdown()
  {
    clockThread = null;
  }
  
  /**
   * run()
   *  - Polls the system time to do:
   *    - Periodic ftp batch transfer (upload) of files in outgoing dir
   *    - Periodic archiving of old responses (presumed to have all contained mms requests already dealt with)
   */
  public void run()
  {
    Thread myThread = Thread.currentThread();
    
    if(clockThread==myThread) {
      isClockThreadStarted=true;
    }
    
    while (clockThread == myThread) {
    
      log.info("Periodic ftp batch tasks STARTING.");

      Calendar clndr = Calendar.getInstance();
      final int crntHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
      log.debug("FTPBatch_FileManager - run() crntHour="+crntHour+", FTPBATCH_HOUR24_OF_DAY_1="+FTPBATCH_HOUR24_OF_DAY_1 + ", FTPBATCH_HOUR24_OF_DAY_2="+FTPBATCH_HOUR24_OF_DAY_2 + ", FTPBATCH_HOUR24_OF_DAY_3="+FTPBATCH_HOUR24_OF_DAY_3);

      // do ftp batch?
      if(crntHour == FTPBATCH_HOUR24_OF_DAY_1 || 
         crntHour == FTPBATCH_HOUR24_OF_DAY_2 || 
         crntHour == FTPBATCH_HOUR24_OF_DAY_3) 
      {
        log.info("Performing ftp batch...");
        doFTPBatch(); // moves files ftp'd to archive dir
      }
      
      // get response data?
      //log.debug("crntHour="+crntHour+", RESPONSECHECK_HOUR24_OF_DAY_1="+RESPONSECHECK_HOUR24_OF_DAY_1 + ", RESPONSECHECK_HOUR24_OF_DAY_2="+RESPONSECHECK_HOUR24_OF_DAY_2 + ", RESPONSECHECK_HOUR24_OF_DAY_3="+RESPONSECHECK_HOUR24_OF_DAY_3);
      if(crntHour == RESPONSECHECK_HOUR24_OF_DAY_1 || 
         crntHour == RESPONSECHECK_HOUR24_OF_DAY_2 || 
         crntHour == RESPONSECHECK_HOUR24_OF_DAY_3)
      {
        log.info("Performing ftp batch response retreival...");
        doFTPBatchResponseRetreival();  // get response files via ftp
      }

      // archive old responses (if any)
      log.info("Archiving old responses...");
      archiveOldResponses();
    
      log.info("Periodic ftp batch tasks COMPLETED.");

      // go back to your room
      try 
      {
        Thread.sleep(DISK_QUERY_FREQUENCY_MILIS);
      }
      catch(Exception e) 
      {
        log.debug("FTPBatch_FileManager - Thread.sleep exception occurred: "+e.getMessage());
      }

    }
    
    if(clockThread==null) {
      isClockThreadStarted=false;
    }

  }
  
  /**
   * ftpGetFile()
   * 
   * Performs an ftp get op incorporating retries.
   * 
   * @param   ftp               FTPClient valid instance.
   * @param   localPath         Local path filename of where to download the file.
   * @param   remoteFilename    Local remote filename of file to retreive.
   * 
   * @conditional               FTPClient 'ftp' param is assumed to have finite timeout specified otherwise process will hang.
   */
  private boolean ftpGetFile(FTPClient ftp,String localPath,String remoteFilename)
  {
    int attemptNum = 5; // do 5 retries max
    boolean bSuccess=false;
    
    do {
      try {
        log.info("Attempting to get ftp file '"+remoteFilename+"'...");
        ftp.get(localPath,remoteFilename);
        // if get here, assume success
        bSuccess=true;
        break;
      }
      catch(com.enterprisedt.net.ftp.FTPException  ftpe) {
        log.error("FTPException occurred attempting to get file '"+remoteFilename+"': '"+ftpe.getMessage()+"'.");
      }
      catch(java.io.InterruptedIOException iioe) {  // usu. means timed out
        log.error("IO Exception occurred attempting to get file '"+remoteFilename+"': '"+iioe.getMessage()+"'.");
      }
      catch(java.io.IOException ioe) {
        log.error("Interrupted IO Exception occurred attempting to get file '"+remoteFilename+"': '"+ioe.getMessage()+"'.");
      }
      // if get here, assume failed ftp get attempt
    } while(--attemptNum>0);
    
    if(bSuccess)
      log.info("ftp get file '"+remoteFilename+"' SUCCESSFUL.");
    else
      log.error("Unable to ftp get file '"+remoteFilename+"'.");
    
    return bSuccess;
  }
  
  private FTPClient getFTP(String appSrcTypeCode, boolean bUploadOrDownload)
  {
    FTPClient ftp = null;
    
    try {
    
      for(int i=0; i<olas.size(); i++) {  
        Ola ola = (Ola)olas.elementAt(i);
        if(ola.OLA_NAME.equals(appSrcTypeCode)) {
          
          log.info("Instantiating FTPClient (HOST: '"+ola.FTP_HOST+"', PORT: '"+ola.FTP_PORT+"', USERNAME: '"+ola.FTP_USERNAME+"', PASSWORD: '"+ola.FTP_PASSWORD+"')...");
          ftp = new FTPClient(ola.FTP_HOST, ola.FTP_PORT);
          
          log.info("FTPClient setting Timeout to 30 minutes...");
          ftp.setTimeout(1000*60*30); 
          log.info("FTPClient logging in...");
          ftp.login(ola.FTP_USERNAME, ola.FTP_PASSWORD);
          log.info("FTPClient Setting transfer type to BINARY...");
          ftp.setType(FTPTransferType.BINARY);
          log.info("FTPClient setting connect mode to ACTIVE...");
          ftp.setConnectMode(FTPConnectMode.ACTIVE);
          // CRITICAL - must issue this command else Socket error occurrs
          log.info("FTPClient changing directory to: '"+(bUploadOrDownload? "upload":"download")+"'...");
          ftp.chdir( (bUploadOrDownload? "upload":"download") );
      
          log.info("FTPClient returning as successful...");
          break;

        }
      }
      
    }
    catch(java.io.InterruptedIOException ioe) {
      log.error("getFTP(appSrcTypeCode:"+appSrcTypeCode+") Interrupted IO Exception occurred: '"+ioe.getMessage()+"'.");
      return null;
    }
    catch(Exception e) {
      log.error("getFTP(appSrcTypeCode:"+appSrcTypeCode+") Exception occurred: '"+e.getMessage()+"'.");
      return null;
    }

    return ftp;
  }
  
  
  /**
   * doFTPBatchResponseRetreival()
   * -----------------------------
   * -  Retreives ftp batch response files placing them in the local response directory
   * -  ASSUMPTION: files are assumed to be removed from ftp site once downloaded
   * -          SO do a sweeping retreival of response files from ftp site
   */
  private synchronized void doFTPBatchResponseRetreival()
  {
    // wait for resource availability and then lock resouces for method duration
    //  to ensure non-concurrent file access
    while(bResourceLock) {
      try {
        wait();
      }
      catch (InterruptedException e) {
      }
    }
    bResourceLock=true;
    
    log.debug("doFTPBatchResponseRetreival() - START");
    
    FTPClient ftp = null;
    
    try 
    {

      String        fnames[]          = null;
      final String  absPath           = ftpBatchRootDir+File.separator+SUBDIR_NAME_RESPONSE;
      String        localPathfilename = null;
      int           iFileCount        = 0;
      File          f                 = null;


      //loop through all olas and check each clients separate ftp account for files.. if found download them
      for(int w=0; w<olas.size(); w++) 
      {
        iFileCount = 0;
        Ola ola = (Ola)olas.elementAt(w);

        // config the ftp er
        if((ftp = getFTP(ola.OLA_NAME,false))==null) 
        {
          log.error("Ftp batch response retreival failed (" + ola.OLA_NAME + "): Unable to fully configure FTPClient object.");
          continue;
        }

        //get names of all files in ftp directory
        fnames = ftp.dir("");

        // download all found response fnames
        for(int i=0; i<fnames.length;i++) 
        {
        
          if(!FTPBatch.isResponseFile(fnames[i])) 
          {
            log.warn("Found non-response type file: '"+fnames[i]+"'.  File skipped.");
            continue;
          }

          log.info("downloading file: '"+fnames[i]+"'...");

          String appSrcTypeCode = fnames[i].substring(0,fnames[i].indexOf('_'));

          localPathfilename = absPath+File.separator+fnames[i];
        
          f = new File(localPathfilename);
        
          if(f.exists()) 
          {
            log.info("Found FTP Batch response file to download but local file of same name exists ('"+f.getName()+"').  File skipped.");
          } 
          else 
          {
            // download remote file
            if(ftpGetFile(ftp,localPathfilename,fnames[i]))  // robustify by incorporating retries.
            {
              log.info("Found FTP Batch response file to download: '"+fnames[i]+"' to '"+localPathfilename+"'.  Downloading...");
              iFileCount++;
            }
            else
            {
              log.error("ftp remote file '"+fnames[i]+"' was not successfully downloaded.");
            }
          }

        }
      
        log.info("Downloaded "+iFileCount+" FTP Batch Response files from " +ola.OLA_NAME+ " ftp directory.");


        // delete remote files 
        for(int t=0; t<fnames.length;t++) 
        {
          try 
          {
            log.info("Deleting remote ftp response file: '"+fnames[t]+"'...");
            ftp.delete(fnames[t]);
          }
          catch(Exception e) 
          {
            log.error("ftp delete file error occurred for remote file '"+fnames[t]+"'.  File skipped.");
          }
        }

        // ftp connection shutdown
        try 
        {
          if(ftp!=null)
            ftp.quit();
        }
        catch(Exception e) 
        {}

      }

      // decrypt the downloaded files (if necessary)
      PGPCmdLine    pgp     = new PGPCmdLine();
      StringBuffer  sb      = new StringBuffer(0);
      File          respdir = new File(ftpBatchRootDir+File.separator+SUBDIR_NAME_RESPONSE);

      fnames = respdir.list();

      for(int i=0; i<fnames.length;i++) 
      {
        if(FTPBatch.isEncryptedResponsefile(fnames[i])) 
        {
          log.debug("found encrypted response file: '"+fnames[i]+"'.");
          try 
          {
            pgp.decrypt("","",absPath+File.separator+fnames[i],sb);
            // now delete pgp file
            f = new File(absPath+File.separator+fnames[i]);
            if(!f.delete())
              log.error("Error attempting to delete file: '"+fnames[i]+"'.  Continuing.");
          }
          catch(Exception e) 
          {
            log.error("decrypt file exception: '"+fnames[i]+"'.  File ignored.");
          }
        }
      }
      
    
    }
    catch(Exception e) {
      log.error("doFTPBatchResponseRetreival() Exception: "+e.getMessage());
    }
    finally {
      
      bResourceLock=false;
      notifyAll();
    }
  
  }
  
  /**
   * archiveOldResponses()
   * ---------------------
   *  - queries response dir for files older than certain amount: 'old' files
   *  - if old files exist they are moved to archive dir
   *  - IMPT: old files DEFINED AS: 
   *      1. file timestamp > 2 days old
   *      2. filename date timestamp > 2 days old
   */
  private synchronized void archiveOldResponses()
  {
    // wait for resource availability and then lock resouces for method duration
    //  to ensure non-concurrent file access
    while(bResourceLock) {
      try {
        wait();
      }
      catch (InterruptedException e) {
      }
    }
    bResourceLock=true;
    
    log.debug("archiveOldResponses() - START");
    
    try {
      
      final long nowms = (new Date()).getTime();
      File archivefDir = null,responsefDir = new File(ftpBatchRootDir+File.separator+SUBDIR_NAME_RESPONSE);
      File responseFiles[] = responsefDir.listFiles();
      Date datelastmod;
      
      for(int i=0; i<responseFiles.length; i++) {
        
        // get date from file name date stamp
        if((datelastmod=FTPBatch.getDateFromResponseFilename(responseFiles[i].getName()))==null)
           log.error("Unable to determine date of FTP Batch Response file '"+responseFiles[i].getName()+"'.  File not acrhived.");
        else if((nowms - datelastmod.getTime()) > ARCHIVE_AGE_MILIS && ((nowms - responseFiles[i].lastModified()) > ARCHIVE_AGE_MILIS)) {
          log.info("Archiving old response file: '"+responseFiles[i].getName()+"')...");
          if(archivefDir == null)
            archivefDir = new File(ftpBatchRootDir+File.separator+SUBDIR_NAME_ARCHIVE);
          if(!responseFiles[i].renameTo(new File(archivefDir, responseFiles[i].getName())))
            log.error("File '"+responseFiles[i].getName()+"' NOT archived: Unable to rename file.");
        }
      }
    
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
    finally {
      bResourceLock=false;
      notifyAll();
    }
    
  }
    
    
  /**
   * doFTPBatch()
   *  - performs ftp upload of all found outgoing ftp batch files contained in outgoing dir
   *      that have date stamp not greater than current day
   *  - each found file is separately PGP encrypted then ftp'd 
   *  - file is archived (moved to archive sub-dir) upon successful pgpf/ftp
   *  - ASSUMPTION: Files encrypted via PGPi will:
   *     - have the encrypted file added ADJACENT (same dir) to unencrypted file
   *     - generate encrypted file ending in '*.asc' suffix
   */
  private synchronized void doFTPBatch()
  {

    // wait for resource availability and then lock resouces for method duration
    //  to ensure non-concurrent file access
    while(bResourceLock) {
      try {
        wait();
      }
      catch (InterruptedException e) {
      }
    }
    bResourceLock=true;
    
    FTPClient ftp = null;

    try {
      
      String encFilename,ftpFilename,localPath;
      StringBuffer encFilenameBuf = new StringBuffer(0);

      File fDir = new File(ftpBatchRootDir+File.separator+SUBDIR_NAME_OUTGOING);
      File archivefDir = new File(ftpBatchRootDir+File.separator+SUBDIR_NAME_ARCHIVE);
      File ftpF = null;
      String fnames[] = fDir.list();
      
      // ensure at least one file for ftping exists
      //log.debug("fnames.length="+fnames.length);
      if(fnames.length == 0) {
        log.info("No files found to batch.");
        return;
      }

      // config the pgp er
      log.debug("config the PGPCmdLine...");
      PGPCmdLine pgp = new PGPCmdLine();
      
      localPath = ftpBatchRootDir+File.separator+SUBDIR_NAME_OUTGOING;
      //log.debug("localPath="+localPath);
      
      Date crntDate=null,tfDate=null;

      // get the current date (w/o time part) for use as comparator
      Calendar clndr = Calendar.getInstance();
      Calendar clndrNow = Calendar.getInstance();
      clndrNow.setTime(new Date());
      clndr.clear();
      clndr.set(Calendar.YEAR,clndrNow.get(Calendar.YEAR));
      clndr.set(Calendar.MONTH,clndrNow.get(Calendar.MONTH));
      clndr.set(Calendar.DATE,clndrNow.get(Calendar.DATE));
      crntDate = clndr.getTime();
      //log.debug("crntDate="+crntDate);
      
      for(int i=0; i<fnames.length; i++) {
        
        String appSrcTypeCode = fnames[i].substring(0,fnames[i].indexOf('_'));
        log.debug("appSrcTypeCode="+appSrcTypeCode);
      
        // ftp connection shutdown
        try 
        {
          if(ftp!=null)
          {
            ftp.quit();
          }
        }
        catch(Exception e) 
        {}

        // config the ftp er
        if((ftp = getFTP(appSrcTypeCode,true))==null) {
          log.error("Do ftp batch failed for " + fnames[i] + ": Unable to fully configure FTPClient object.");
          continue;
        }
      
        if(FTPBatch.isTextFile(fnames[i])) {
          
          // ensure textfile is NOT based on FUTURE date (rather present or earlier)
          tfDate=FTPBatch.getDateFromTextFilename(fnames[i]);
          
          //log.debug("tfDate="+tfDate);
          
          if(tfDate.compareTo(crntDate) > 0) {
            log.info("Skipping future file: '"+fnames[i]+"'.");
            continue;
          }
          // at this point, file is deemed send-worthy

          // create adjacent PGPized file
          log.debug("Encrypting file '"+fnames[i]+"'...");
          try {
            pgp.encrypt("",FTPBATCH_PGP_USERID,ftpBatchRootDir+File.separator+SUBDIR_NAME_OUTGOING+File.separator+fnames[i],encFilenameBuf);
          }
          catch(Exception e) {
            log.error("FTP Batch Exception: Error occurred attempting to encrypt file '"+fnames[i]+"'.  File Skipped.");
            continue;
          }
          ftpF = new File(encFilenameBuf.toString());
          encFilename = ftpF.getName();  // extract filename from abs path file name
          log.info("File '"+fnames[i]+"' encrypted to file '"+encFilename+"'.");
          
          // FTP the PGPized file
          log.debug("FTPing file: '"+encFilename+"'...");
          ftp.put(encFilenameBuf.toString(),encFilename);
          
          // remove PGPized file since only need for ftp
          log.debug("Removing PGPized file: '"+encFilename+"'...");
          ftpF = new File(encFilenameBuf.toString());
          ftpF.delete();
          
          // archive file
          log.debug("Archiving file: '"+fnames[i]+"'...");
          ftpF = new File(localPath+File.separator+fnames[i]);
          archivefDir = new File(ftpBatchRootDir+File.separator+SUBDIR_NAME_ARCHIVE);
          if(!ftpF.renameTo(new File(archivefDir, ftpF.getName())))
            log.error("File '"+ftpF.getName()+"' NOT archived: Unable to rename file.");
          else
            log.info("File '"+ftpF.getName()+"' successfully archived.");
        
        }
      }
      
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
    finally {
      // ftp connection shutdown
      try 
      {
        if(ftp!=null)
        {
          ftp.quit();
        }
      }
      catch(Exception e) 
      {}
      bResourceLock=false;
      notifyAll();
    }
  }
  
  
  /**
   *  checkResponse()
   * ----------------
   *  - updates mmsRequest w/ mms request response data (if present)
   */
  public synchronized boolean checkResponse(MMSRequest mmsRequest)
  {

    // wait for resource availability and then lock resouce semaphore for method duration
    //  to ensure non-concurrent file access
    while(bResourceLock) {
      try {
        wait();
      }
      catch (InterruptedException e) {
      }
    }
    bResourceLock=true;

    try {
      
      log.debug("FTPBatch_FileManager.checkResponse() - START");

      // interrogate all files present in response dir
      //  creating distinct vector of corresponding FTPBatch objects
      //  (as there are 3 files per batch)
      String absPath = ftpBatchRootDir+File.separator+SUBDIR_NAME_RESPONSE;
      String fname = null;
      File dir = new File(absPath),f = null;
      FTPBatch ftpBatch = null;
      Vector ftpBatches = new Vector(3);  // presume max. of 3 co-existing set of ftp batch response files
      String filenames[] = dir.list();
      StringBuffer sb = new StringBuffer();
      PGPCmdLine pgp = new PGPCmdLine();
      
      for(int i=0; i<filenames.length; i++) {
        fname=filenames[i];
        // decrypt response file?
        if(FTPBatch.isEncryptedResponsefile(fname)) {
          log.info("Decrypting encrypted response file: '"+fname+"'...");
          try {
            pgp.decrypt("","",absPath+File.separator+fname,sb);
            // now delete pgp file
            f = new File(absPath+File.separator+fname);
            if(!f.delete())
              log.error("Error attempting to delete encrypted response file: '"+fname+"'.  Continuing.");
          }
          catch(Exception e) {
            log.error("Unable to decrypt encrypted response file: '"+fname+"'.  File ignored.");
          }
        }
        // process if response file
        if(FTPBatch.isResponseFile(fname)) {
          log.debug("found response file '"+fname+"'");
          // found response file so create corres. ftp batch object 
          //  and add to vector if not already added
          ftpBatch = new FTPBatch(ftpBatchRootDir+File.separator+SUBDIR_NAME_OUTGOING,ftpBatchRootDir+File.separator+SUBDIR_NAME_RESPONSE,FTPBatch.getTextFilenameFromResponseFilename(fname));
          if(ftpBatches.indexOf(ftpBatch) == -1)
            ftpBatches.addElement(ftpBatch);
        }
      }
      
      // Sort the just created distinct ftp batch vector by datestamp contained within filename
      //  in order to interrogate the LATEST received response files FIRST.
      // This will allow checking of whether mms request is already found in a later response file over an earlier response file
      //  such as the case for re-submittals.
      log.debug("Sorting ftp batch responses...");
      Collections.sort(ftpBatches); // ascending
      
      // iterate the just created distinct ftp batch vector 
      //  checking for presence of feedback on given mms request
      for(int i=ftpBatches.size()-1; i>=0; i--) {
        log.debug("checking ftp batch (textfile: '"+((FTPBatch)ftpBatches.get(i)).getTextFilename()+"')...");
        if(((FTPBatch)ftpBatches.get(i)).updateMMSRequestWithResponse(mmsRequest)) {
          // IMPT: update mms request - status/response and process end date
          mmsRequest.setProcessEndDate(new Date());
          mmsRequest.setProcessStatus(mesConstants.ETPPS_RECEIVED);
          // NOTE: successful when vnum present and NO errors
          mmsRequest.setProcessResponse(
                      (mmsRequest.getNumProcessErrors()==0 && mmsRequest.getVNum().length()>0)? 
                        mesConstants.ETPPR_SUCCESS:mesConstants.ETPPR_PENDING);
          return true;  // found mms request in responses
        }
      }
      
      return false;  // did not find mms request
    }
    catch(Exception e) {
      log.error(e.getMessage());
      return false;
    }
    finally {
      bResourceLock=false;
      notifyAll();
    }
  
  }
  
  /**
   * ammendMMSRequest()
   *  - adds mms request to the 'currently pending' ftp batch file
   *  - delegates the actual adding to FTPBatch.addMMSRequest
   *  - IMPT: outgoing ftp batch filename is determined by the current system date
   */
  public boolean ammendMMSRequest(MMSRequest mmsRequest)
  {
    try 
    {
      final Date now = new Date();
      Date textFilenameDate = new Date(now.getTime());
      final int crntHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
      
      
      if(crntHour > FTPBATCH_HOUR24_OF_DAY_3) 
      {
        // ammend to tomorrows file (to elim. sending 2 files of same name)
        Calendar clndr = Calendar.getInstance();
        clndr.setTime(textFilenameDate);
        clndr.roll(Calendar.DATE,true); // roll up 1 day (tomorrow)
        textFilenameDate.setTime(clndr.getTime().getTime());
      }

      final String ftpBatchFilename = FTPBatch.calcTextFilename(
        mmsRequest.getAppSrcTypeCode(),mmsRequest.getTermAppName(),textFilenameDate
      );
      log.debug("ftpBatchFilename="+ftpBatchFilename);
      
      FTPBatch ftpBatch = null;
      
      // delegate to FTPBatch class
      ftpBatch = new FTPBatch(ftpBatchRootDir+File.separator+SUBDIR_NAME_OUTGOING,ftpBatchRootDir+File.separator+SUBDIR_NAME_RESPONSE,ftpBatchFilename);
      final boolean bOK = ftpBatch.addMMSRequest(mmsRequest);
        
      // update mms request upon success
      if(bOK) 
      {
        mmsRequest.setProcessStartDate(now);
        mmsRequest.setProcessStatus(mesConstants.ETPPS_INPROCESS);
        log.info("MMS Request: ID:'"+mmsRequest.getReqID()+"' successfully posted to ftp batch file: '"+ftpBatchFilename+"'.");
      } 
      else
      {
        log.error("MMS Request: ID:'"+mmsRequest.getReqID()+"' NOT successfully posted to ftp batch file: '"+ftpBatchFilename+"'.");
      }
      
      return bOK;
    }
    catch(Exception e) 
    {
      log.error(e.getMessage());
    }

    return false;
  }
  
}  // FTPBatch_FileManager
