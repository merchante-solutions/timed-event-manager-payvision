/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/AutoprocessBean.java $

  Last Modified By   : $Author: Jkirton $
  Last Modified Date : $Date: 9/20/02 10:14a $
  Version            : $Revision: 11 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;

import org.apache.log4j.Category;
// log4j classes.
import org.apache.log4j.PropertyConfigurator;

/**
 * AutoprocessBean
 * ---------------
 * - Outside world sole contact point for running 'com.mes.mms.autoprocess' bean.
 * - Calling scope is responsible for 2 things to run this bean
 *    1. calling startup()
 *    2. calling shutdown() - necessary to properly terminate all threads
 * 
 *    Dependencies
 *    ------------
 *    - jakarta-log4j-1.1.3 (log4j.jar)
 *    - com.enterprisedt.net.ftp - FTPClient
 *    - MES java bean run environment
 *    - PGP Command Line v6.5.8
 * 
 *    Setup
 *    -----
 *      - PGP.exe v6.5.8 is required for the FTP Batch implementation of MMS Request file buliding
 *        (http://web.mit.edu/network/pgp.html)
 *        - Public key for encryption is required to be added to PGP pub keyring
 *        - MES Private key for response data decryption is required to be added to PGP private keyring
 *        - Environment variable: 'PGPPASS' is required to be set for non-interactive decryption
 *        - IMPORTANT: Sign the public key (via 'pgp -ks <public key>') 
 *           used to encrypt to ensure that the prompt: 
 *            'Are you sure you want to use this public key (y/n)?' does not show up
 *        - IMPORTANT: Ensure that our (Merchant e-Solutions') key is the "Default Signing Key"
 *            to avoid command line questions
 * 
 *      - All file i/o in this bean is relative to a specified working directory
 *        which can be either absolute OR relative itself.
 *        
 *      - Working dir structure:
 *        -{working directory}
 *          -bin (directory)
 *          -FTPBatch_FileManager.properties (file)
 *          -MMSFileBuilderFactory.properties (file)
 *          -MMSRequestProcessorManager.properties (file)
 * 
 */
public final class AutoprocessBean
{
  
  // create class log category
  static Category log = Category.getInstance(AutoprocessBean.class.getName());

  // construction
  public AutoprocessBean()
  {
    InitLogger("mms_autoprocess_Log4j.cfg");
  }

  /**
   * run()
   * -----
   * - Runs the MMS AutoprocessBean for a specified amount of time
   * - This function serves as a container to fully control running
   *    (starting and stopping) this bean 
   *    whereas the startup() and shutdown() functions serve to run this bean 
   *    for an indefinte amount of time
   */
  public final void run(long duration_millis)
  {
    log.info("*******************************");
    log.info("*** MMS Autoprocess STARTUP ***");
    log.info("*******************************");
    log.info("Starting MMS AutoprocessBean (duration: "+(duration_millis/(1000*60))+" minutes)...");
    if(startup()) {
      log.info("MMS AutoprocessBean started.");
      try {
        Thread.currentThread().sleep(duration_millis);
      }
      catch(InterruptedException ie) {
      }
      log.info("Shutting down MMS AutoprocessBean...");
      if(shutdown())
        log.info("MMS AutoprocessBean shutdown.");
      else
        log.error("Error occurred attempting to shutdown MMS AutoprocessBean.");
    }
    log.info("********************************");
    log.info("*** SHUTDOWN MMS Autoprocess ***");
    log.info("********************************");
  }
  
  /**
   * startup()
   * ---------
   * - Starts the MMS Autoprocess bean
   */
  public final boolean startup() 
  {
    return MMSRequestProcessorManager.startup();
  }
  
  /**
   * shutdown()
   * ---------
   * - Stops the MMS Autoprocess bean
   */
  public final boolean shutdown()
  {
    MMSRequestProcessorManager.shutdown();
    return !MMSRequestProcessorManager.isStarted();
  }
  
  public final boolean isStarted()
  {
    return MMSRequestProcessorManager.isStarted();
  }

  public static void main(String[] args)
  {
    try 
    {
      // get run duration time (allowed running time: 1 minute - 24 hours)
      if(args.length != 1) 
      {
        usage("One and only one argument can be specified.");
        return;
      }
      final long milli_duration = Long.parseLong(args[0]);
      if(milli_duration<60000 || milli_duration>86400000)
      {
        throw new Exception("The allowed milli-second running time range is 60000 (1 minute) - 86400000 (24 hours).");
      }
    
      // run
      AutoprocessBean apb = new AutoprocessBean();
      apb.run(milli_duration);
    }
    catch(Exception e) {
      log.error("MMS AutoprocessBean run Excepton: '"+e.getMessage()+"'");
    }

  }  // END main
  
  static void usage(String errMsg)
  {
    System.err.println(errMsg);
    System.err.println("\nUsage: AutoprocessBean -Duration [Milli-second value]\n");
    System.exit(1);
  }
  
  static void InitLogger(String configFile)
  {
    PropertyConfigurator.configure(configFile);
  }

} // class AutoprocessBean
