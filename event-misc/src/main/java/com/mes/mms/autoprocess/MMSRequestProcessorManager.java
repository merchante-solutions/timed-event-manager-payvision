/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/MMSRequestProcessorManager.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;

import java.util.Enumeration;
// log4j classes.
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.PropertiesFile;

/**
 * MMSRequestProcessorManager
 * --------------------------
*/
public final class MMSRequestProcessorManager extends Object implements Runnable
{
  // create class log category
  static Logger log = Logger.getLogger(MMSRequestProcessorManager.class);
  
  // Singleton
  public static MMSRequestProcessorManager getInstance()
  {
    if(_instance==null) 
    {
      try 
      {
        _instance = new MMSRequestProcessorManager();
      }
      catch(Exception e) 
      {
        _instance = null;
        log.error("MMSRequestProcessorManager - Unable to instantiate due to exception: "+e.getMessage()+".");
      }
    }
    return _instance;
  }
  private static MMSRequestProcessorManager _instance = null;
  ///

  
  // data members
  private long  PROCESS_FREQUENCY_MILIS;

  private boolean isStarted=false;
  private PersistQueue_MMSRequest_Input    mmsRequestQueue;    // input
  private PersistQueue_MMSRequest_InProc    mmsInProcQueue;
  //private PersistQueue_MMSRequest_Output    mmsDeploymentQueue;  // output - aka Activation
  //private PersistQueue_MMSRequest_Pending  mmsPendingQueue;    // output pending error correction
  
  private MMSFileBuilderFactory mmsFBF=null;

  private Thread clockThread = null;
  private boolean bResourceLock=false;

  
  // static startup/shutdown routines
  public static final boolean startup()
  {
    return (getInstance()==null) ? false : _instance.Startup();
  }
  
  public static final boolean isStarted()
  {
    return (_instance==null) ? false : _instance.isStarted;
  }
  
  public static final void shutdown()
  {
    if(_instance!=null)
    {
      _instance.Shutdown();
    }
  }
  
  // construction
  private MMSRequestProcessorManager()
    throws Exception
  {
    PropertiesFile props = new PropertiesFile("MMSRequestProcessorManager.properties");
    PROCESS_FREQUENCY_MILIS=props.getInt("PROCESS_FREQUENCY_MILIS");
      
    bResourceLock=false;

    if((mmsFBF = MMSFileBuilderFactory.getInstance())==null)
    {
      throw new Exception("Unable to instantiate MMSRequestProcessorManager: Unable to obtain MMSFileBuilderFactory instance.");
    }
    
    mmsRequestQueue = new PersistQueue_MMSRequest_Input();
    mmsInProcQueue = new PersistQueue_MMSRequest_InProc();
  }
  
  /**
   * run()
   *  - Polls the system time to do:
   *    - Periodic call of Go()
   */
  public void run()
  {
    Thread myThread = Thread.currentThread();
    
    if(!isStarted && (clockThread == myThread)) 
    {
      try 
      {
        // Enforce Direct db connections
        SQLJConnectionBase.setConnectString(SQLJConnectionBase.getDirectConnectString());
      
        // startup all queues
        log.info("Starting up MMS Request queue...");
        mmsRequestQueue.startup();
        log.info("Starting up In Process queue...");
        mmsInProcQueue.startup();
        //mmsDeploymentQueue.startup();
        //mmsPendingQueue.startup();

        isStarted=true;
        log.info("MMSRequestProcessorManager is now started.");
      }
      catch(Exception e) 
      {
        log.error(e.getMessage());
        isStarted=false;
        mmsInProcQueue.shutdown();
        mmsRequestQueue.shutdown();
      }

    }
    
    while (clockThread == myThread) 
    {
      Go();
      
      // go back to your room
      try 
      {
        Thread.sleep(PROCESS_FREQUENCY_MILIS);
      }
      catch(Exception e) 
      {
        log.error("FTPBatch_FileManager - Thread.sleep exception occurred: "+e.getMessage());
      }
    }
      
    if(isStarted && clockThread==null) 
    {
      log.info("Shutting down MMSFileBuilderFactory...");
      mmsFBF.shutdown();
    
      // shutdown all queues
      //mmsPendingQueue.shutdown();
      //mmsDeploymentQueue.shutdown();
      log.info("Shutting down In Process queue...");
      mmsInProcQueue.shutdown();
      log.info("Shutting down Request queue...");
      mmsRequestQueue.shutdown();
    
      isStarted=false;
      
      log.info("MMSRequestProcessorManager is now shutdown.");
    }
  }

  private final boolean Startup()
  {
    if(isStarted)
    {
      return true;
    }
    
    try 
    {
      // spawn the clock thread
      log.info("Starting ProcMngrClock thread...");
      this.clockThread = new Thread(this, "ProcMngrClock");
      this.clockThread.start();
      
      // wait max 60 seconds for thread to start
      int milli = 60*1000;
      while(--milli!=0 && !isStarted)
        Thread.currentThread().sleep(1);
      
    }
    catch(Exception e) 
    {
      log.error(e.getMessage());
    }
    
    return isStarted;
  }
  
  private synchronized final void Shutdown()
  {
    while(bResourceLock) 
    {
      try 
      {
        wait();
      }
      catch (InterruptedException e) 
      {
      }
    }
    bResourceLock=true;
    
    try 
    {
      if(!isStarted)
      {
        return;
      }
      
      log.info("MMSRequestProcessorManager beginning shutdown..");

      clockThread = null;

      // wait for clock thread to stop
      while(isStarted)
      {
        Thread.currentThread().sleep(1);
      }
    }
    catch(Exception e) 
    {
      log.error(e.getMessage());
    }
    finally 
    {
      bResourceLock=false;
      notifyAll();
    }
  }
  
  private synchronized final void Go()
  {
    while(bResourceLock) 
    {
      try 
      {
        wait();
      }
      catch (InterruptedException e) 
      {
      }
    }
    bResourceLock=true;

    try 
    {
      log.info("*** Go - START *************");

      log.debug(toString());

      MMSRequest mmsRequest = null;
      String queueElmKey = null;
      
      // 1. load new and re-submittal mms requests into request process input queue
      log.info("Go() - 1. loading new and re-submittal mms requests into input request queue...");
      mmsRequestQueue.load();
      log.debug("Go() - 1. Loaded "+mmsRequestQueue.getNumElements()+" requests into request queue.");
    
      // 2. initiate mms request file build process for mms requests in input queue
      //    updating persist then unloading from queue
      log.info("Go() - 2. beginning mms request file build process for loaded mms requests in input queue...");
      for(Enumeration e=mmsRequestQueue.getQueueElmntKeys(); e.hasMoreElements();) 
      {
        queueElmKey=(String)e.nextElement();
        mmsRequest=(MMSRequest)mmsRequestQueue.getElement(queueElmKey);
        
        // build the mms file
        if(mmsFBF.BuildMMSFile(mmsRequest))
        {
          log.info("MMS request (ID: '"+mmsRequest.getReqID()+"') file build process successful");
        }
        else
        {
          log.error("MMS request (ID: '"+mmsRequest.getReqID()+"') file build process FAILED.");
        }
      }
    
      // 3. update mms request info for each, just processed, mms request
      //    then unload the input queue and 'transfer' to in process queue
      log.info("Go() - 3. Update persist mms requests in Request queue, unload it then load In Process queue...");
      mmsRequestQueue.updatePersist();
      mmsRequestQueue.unload();
      mmsInProcQueue.load();
      log.debug("Go() - 3. Loaded "+mmsInProcQueue.getNumElements()+" requests into In Process queue.");
    
      // 4. check for mms request responses
      log.info("Go() - 4. checking for mms request responses...");
      for(Enumeration e=mmsInProcQueue.getQueueElmntKeys(); e.hasMoreElements();) 
      {
        queueElmKey=(String)e.nextElement();
        mmsRequest=(MMSRequest)mmsInProcQueue.getElement(queueElmKey);

        mmsFBF.DetermineStatus(mmsRequest);
        
        if(!mmsRequest.getProcessStatus().equals(mesConstants.ETPPS_INPROCESS)) 
        {
          // status changed so persist then unload
          mmsInProcQueue.updatePersist(queueElmKey);
          
          // PROPAGATE RESPONSE to redundant queues according to proces response 
          // for benefit of outside world
          try {
            if(mmsRequest.getProcessResponse().equals(mesConstants.ETPPR_SUCCESS)) {
              com.mes.ops.MmsStageSetupBean.updateVnumberTables(Long.parseLong(mmsRequest.getReqID()),mmsRequest.getVNum());
              log.info("Propagated successful MMS Request Response '"+mmsRequest.getReqID()+"' to vnumber tables.");
            } else {
              com.mes.queues.QueueTools.insertQueue(Long.parseLong(mmsRequest.getReqID()),com.mes.constants.MesQueues.Q_MMS_ERROR);
              log.info("Propagated non-successful MMS Request Response '"+mmsRequest.getReqID()+"' to MMS Error queue.");
            }
          }
          catch(Exception e2) {
            log.error("Error occurred while propagating MMS response: '"+e2.getMessage()+"'.  Continuing.");
          }
          ///
      
          mmsInProcQueue.unloadElement(queueElmKey);
          log.info("MMS request (ID: '"+mmsRequest.getReqID()+"') Status changed from '"+mesConstants.ETPPS_INPROCESS+"' to: '"+mmsRequest.getProcessStatus()+"'. ");
        }

      }

      log.info("*** Go - END ***************");
    }
    catch(Exception e) {
      log.error("Go() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      bResourceLock=false;
      notifyAll();
    }
  
    
  }  // Go()
  

  public synchronized String toString()
  {
    return
       System.getProperty("line.separator")+"==========================================="
      +System.getProperty("line.separator")+"Request Queue num elements: "+mmsRequestQueue.getNumElements()
      +System.getProperty("line.separator")+"In Proc Queue num elements: "+mmsInProcQueue.getNumElements()
      +System.getProperty("line.separator")+"==========================================="+System.getProperty("line.separator")
      ;
  }
  
}