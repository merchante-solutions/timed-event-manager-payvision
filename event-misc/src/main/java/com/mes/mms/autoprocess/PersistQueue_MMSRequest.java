/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/PersistQueue_MMSRequest.java $

  Description:  


  Last Modified By   : $Author: Jkirton $
  Last Modified Date : $Date: 11/15/02 9:23a $
  Version            : $Revision: 10 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;

// log4j classes.
import org.apache.log4j.Category;

/**
 * PersistQueue_MMSRequest
 * -----------------------
*/
public abstract class PersistQueue_MMSRequest extends PersistQueue
{
  // create class log category
  static Category log = Category.getInstance(PersistQueue_MMSRequest.class.getName());

  // data members
  protected DBQueueOps_MMSRequest the_dboMmsReq;
    // handles all db interaction

//  public static void beginSession()
//  {
//    //DBQueueOps_MMSRequest.getInstance().dbTransStart();
//  }
  
//  public static void endSession()
//  {
//    //DBQueueOps_MMSRequest.getInstance().dbTransEnd();
//  }
  
  // construction
  public PersistQueue_MMSRequest(boolean bInputOrOutput)
    throws Exception
  {
    super(PERSIST_METHOD_DB,bInputOrOutput);
    
    the_dboMmsReq = DBQueueOps_MMSRequest.getInstance();
  }
  
  
  public boolean addElement(String elmKey,Object objElement)
  {
    // ensure objElement is MMSRequest
    if(!objElement.getClass().getName().equals("MMSRequest")) 
    {
      log.error("addElement Error: object must be an MMSRequest.  Failed.");
      return false;
    }
    
    return super.addElement(elmKey,objElement);
  }
  
  protected void load(int dbqType,boolean bOverwrite)
    throws Exception
  {
    try 
    {
      // load all mms requests subject to file build process initiation
      final int numMMSRequests = the_dboMmsReq.load(dbqType,objElements,bOverwrite);
    }
    catch(Exception e) 
    {
      throw new Exception("MMS Request load exception: "+e.getMessage());
    }

  }
  
  public void loadElement(String elmKey)
    throws Exception
  {
    MMSRequest mmsRequest = objElements.containsKey(elmKey)? 
                    (MMSRequest)objElements.get(elmKey):new MMSRequest();
    
    if(!the_dboMmsReq.load(elmKey,mmsRequest))
    {
      return;
    }

    if(!objElements.containsKey(elmKey))
    {
      objElements.put(elmKey,mmsRequest);
    }
  }

  public void save()
    throws Exception
  {
    if(!the_dboMmsReq.save(objElements))
    {
      throw new Exception("Error attempting to save MMS Requests in input queue.");
    }
  }
  
  public void updatePersist()
    throws Exception
  {
    if(!the_dboMmsReq.save(objElements))
    {
      throw new Exception("Error attempting to persist MMS Requests in input queue.");
    }
  }

  public void updatePersist(String elmKey)
    throws Exception
  {
    if(!objElements.containsKey((String)elmKey))
    {
      throw new Exception("Error attempting to persist MMS Request of Req ID: '"+elmKey+"'.  No such mms request exists in this queue.");
    }
    MMSRequest mmsRequest = (MMSRequest)objElements.get((String)elmKey);
    if(!the_dboMmsReq.save(mmsRequest))
    {
      throw new Exception("Error attempting to persist MMS Request of Req ID: '"+elmKey+"'.");
    }
  }

}
