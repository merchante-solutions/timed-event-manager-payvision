/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/mms/autoprocess/FTPBatch_Template.java $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 8/04/04 4:47p $
  Version            : $Revision: 25 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mms.autoprocess;

import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;

/**
 * FTPBatch_Template
 * -----------------
 *  - Encapsulates the formatting rules required for outputing a 
 *    single mms request to file per Vital's FTP Batch process specs.
 *   - IMPT: field names specified in 'recTmplt' data member corres. 
 *          exactly to MMSRequestDataItem.nativeName
 */
public class FTPBatch_Template
{
  // create class log category
  static Category log = Category.getInstance(FTPBatch_Template.class.getName());
  
  // constants
  public static final char    FTPBATCHFILE_FIELD_DLMCHAR        = '|';
  public static String        DIR_TEMPLATES                     = null;
    // directory that contains 0 - n ftp batch templates
  
  // data members
  String name;
  String recTmplt;
  int    iRecSize;
    // {field lengths sum} + {num fields-1} + {1}
    //  where: {num fields-1}  corres. to number of dlm chars
    //         {1}             corres. to trailing record dlm char
  
  
  /**
   * getTemplateDefinitionFromFile()
   * -------------------------------
   * - attempts to load template definition from file
   * - template file name FORMAT: 
   *    '{mmsReqTermAppName}.txt'
   * - template definition file - line FORMAT:
   *   '{recTmplt},{iRecSize}'
   * 
   * @param   mmsReqTermAppName         input argument
   * @param   recTmplt                  output - filled in
   */
  private static boolean getTemplateDefinitionFromFile(String mmsReqTermAppName,StringBuffer recTmplt)
  {
    try 
    {
      recTmplt.delete(0,recTmplt.length());
    
      // construct template filename
      final String templateFilename = DIR_TEMPLATES+File.separator+mmsReqTermAppName+".txt";
      log.debug("templateFilename="+templateFilename);
 
      // attempt to open file
      File f = new File(templateFilename);
      String token=null;
      if(f.exists() && f.isFile()) {
        // template file exists so load it
        log.debug("found ftp batch template definiation file: '"+templateFilename+"'.");
        
        // load file contents
        FileReader fr = new FileReader(f);
        final int bufSiz = 1024*4;
        char[] cbuf = new char[bufSiz]; // 4k max buffer size
        int i=0,c;
        while((c = fr.read()) != -1) {
          if((char)c!='\r' && (char)c!='\n' && (char)c!=' ' && i<bufSiz)
            cbuf[i++]=(char)c;
        }
        fr.close();
        final String line=String.valueOf(cbuf,0,i);
        //log.debug("line='"+line+"'");
        
        // parse file contents
        if(line!=null && line.length()>5) {
          StringTokenizer fldTkns = new StringTokenizer(line,",",false);
          recTmplt.append(fldTkns.nextToken());
          return true;
        }
      }
    }
    catch(Exception e) {
      log.error("getTemplateDefinitionFromFile() EXCEPTION: '"+e.getMessage()+"'.");
    }
    
    return false;
  }
  
  private static boolean isErrorRecord(String record)
  {
    return (record.indexOf(" Errors Found.")>=0);
  }
  
  public String toString()
  {
    return 
      System.getProperty("line.separator")+"name="+name
      +System.getProperty("line.separator")+"recTmplt="+recTmplt
      +System.getProperty("line.separator")+"iRecSize="+iRecSize;
  }
  
  /**
   * getInstance()
   * -------------
   */
  public static FTPBatch_Template getInstance(String mmsReqTermAppName)
    throws Exception
  {
    log.debug("getInstance("+mmsReqTermAppName+") - START");
    
    String sT = null;
    int iRecSize = (1024*2);
    StringBuffer sb = new StringBuffer();
    
    if(!getTemplateDefinitionFromFile(mmsReqTermAppName,sb))
      throw new Exception("Unhandled MMS request terminal application name: '"+mmsReqTermAppName+"'.");
    else {
      sT=sb.toString();
    }

    return new FTPBatch_Template(mmsReqTermAppName,sT,iRecSize);

  }
  
  // construction
  private FTPBatch_Template(String name,String recTmplt,int iRecSize)
  {
    this.name=name;
    this.recTmplt=recTmplt;
    this.iRecSize=iRecSize;
  }
  
  // methods
  String  getName()         { return name; }
  String  getRecTmplt()     { return recTmplt; }
  int     getRecSize()      { return iRecSize; }
  
  /** 
   * fldtransform_toFtpBatch()
   * -------------------------
   *  - transforms format from native (mms request) --> ftp batch
   * 
   *  - ftp batch format specs:
   *    ----------------------
   *     - all fields UPPER CASE
   *     - left justified fixed width space padded fields delimeted w/ '|' char
   *     - NO ',' in merchant name or address fields
   *     - empty fields --> ' ' (single space)
   *     - VNUM field is sent as 8 char spaces
   */
  private String fldtransform_toFtpBatch(String mmsReqFldNme, String mmsReqFldVal,int iWidth)
  {
    try {
      
      //log.debug("fldtransform_toFtpBatch(): mmsReqFldVal='"+mmsReqFldVal+"', iWidth='"+iWidth+"'.");

      if(mmsReqFldNme.equals("VNUM"))
        return "        ";
      
      if((mmsReqFldVal==null) || mmsReqFldVal.equals(""))
        return " ";
        
      String fldVal;
      
      if(mmsReqFldVal.length() < iWidth)
        fldVal=StringUtilities.leftJustify(mmsReqFldVal.toUpperCase(),iWidth,' ');
      else if(mmsReqFldVal.length() > iWidth)
        fldVal=mmsReqFldVal.substring(0,iWidth).toUpperCase();
      else
        fldVal=mmsReqFldVal.toUpperCase();
      
      // field spec. validation
      
      // format Zip Code
      if(mmsReqFldNme.equals("BUSINESS_ZIP") && fldVal.length()>5)
        fldVal=fldVal.substring(0,5); // use first 5 digits only
      
      // CASH_BACK_LIMIT - implied decimal 5-char width
      else if(mmsReqFldNme.equals("CASH_BACK_LIMIT"))
        fldVal=StringUtilities.rightJustify(StringUtilities.strip(fldVal,'.'),5,'0');
      
      // SURCHARGE_AMOUNT - implied decimal 3-char width
      else if(mmsReqFldNme.equals("SURCHARGE_AMOUNT"))
        fldVal=StringUtilities.rightJustify(StringUtilities.strip(fldVal,'.'),3,'0');

      // MCC - must be 4 characters long
      else if(mmsReqFldNme.equals("MCC"))
        fldVal=StringUtilities.rightJustify(fldVal,4,'0');
      
      return fldVal;

    }
    catch(Exception e) {
      log.error("getFixedWidthString() - Exception: '"+e.getMessage()+"'.  Aborted.");
      return "ERROR";
    }
  }
  
  /**
   * fldtransform_toMMSRequest()
   * ---------------------------
   *  - maintain upper case
   *  - trim beg/end whitespace
   *  - ' ' --> ''
   */
  private String fldtransform_toMMSRequest(String ftpBatchFld)
  {
    if(ftpBatchFld.equals(" "))
      return "";
    
    return ftpBatchFld.trim();
  }
  
  /**
   * MMSRequestToRecord()
   *  - Converts data held in MMSRequest instance to string of format dictated 
   *    by this FTPBatch_Template instance
   *  - converted string represents single record in outgoing (text file) ftp batch file
   *  - fills 'record' arg
   */
  protected boolean MMSRequestToRecord(MMSRequest mmsRequest, StringBuffer recordBuf)
  {
    try {
      
      //log.debug("MMSRequestToRecord - START (mmsRequest.toString()="+mmsRequest.toString());

      if(recordBuf==null)
        return false;
      
      StringTokenizer fldTkns = new StringTokenizer(recTmplt,"|:",false);
      String token,fldName,fldValue;
      int fldWidth = 0,fldTWidth = 0,insPnt = 0;
          
      recordBuf.ensureCapacity(iRecSize+8);
      //log.debug("recordBuf.capacity()="+recordBuf.capacity());
      
      while(fldTkns.hasMoreTokens()) {
        
        fldName=fldTkns.nextToken();
        fldTWidth=Integer.parseInt(fldTkns.nextToken());
        fldValue = fldtransform_toFtpBatch(fldName,mmsRequest.getRequestDataItemValue(fldName),fldTWidth);
        
        //log.debug("fldName (width)="+fldName+" ("+fldTWidth+"), fldValue='"+fldValue+"'");
        
        recordBuf.insert(insPnt,FTPBATCHFILE_FIELD_DLMCHAR);
        recordBuf.insert(insPnt+1,fldValue);
        insPnt += fldValue.length()+1;
        
      }
      
      // remove preceeding fld dlm and add trailing record dlm
      recordBuf.deleteCharAt(0);
      recordBuf.append(FTPBATCHFILE_FIELD_DLMCHAR);
      recordBuf.append(System.getProperty("line.separator"));
      
      return true;
    }
    catch(Exception e) {
      log.error("MMSRequestToRecord - Exception: "+e.getMessage());
    }
    
    return false;
  }
  
  /**
   * RecordToMMSRequest()
   *  - existing contents of MMSRequest arg are preserved
   *  - if record is of type 'FTPBATCH_FILETYPE_LIST' then 'record' arg is presumed to contain ALL associated error info
   *  - error info is contained on more than one line (per mms batch specs.)
   *  - error info is ALWAYS loaded if encountered
   */
  public boolean RecordToMMSRequest(MMSRequest mmsRequest, String record)
  {
    return RecordToMMSRequest(mmsRequest,record,new String[] {"VNUM"});
  }
  
  public boolean RecordToMMSRequest(MMSRequest mmsRequest, String record, String[] arrFldNmesToLoad)
  {
    try {
      
      /*
      log.debug("(((((((((((");
      for(int i=0; i<arrFldNmesToLoad.length; i++) {
        log.debug("arrFldNmesToLoad["+i+"]='"+arrFldNmesToLoad[i]+"'.");
      }
      log.debug("(((((((((((");
      */
      
      //log.debug("RecordToMMSRequest() record='"+record+"'.");
      
      StringTokenizer recTkns = new StringTokenizer(record,"|\r\n",false);
      StringTokenizer tmpltTkns = new StringTokenizer(recTmplt,"|:",false);
      String recToken,fldName,fldValue;
      int index=0,iNumErrors=0;
      MMSRequestError mmsRE = null;
      
      final boolean bErrRec = isErrorRecord(record);
      int recTknNum = 0;

      while(recTkns.hasMoreTokens()) {
            
        recToken = recTkns.nextToken();
        recTknNum++;
        
        // 'null' --> ''
        if(recToken.equals("null"))
          recToken="";
        
        // error detail line
        if(bErrRec && recToken.indexOf("Error")>0 && recToken.indexOf("--")>0) {
            
          mmsRE = new MMSRequestError();
        
          // set error info
          index = recToken.indexOf("--");
          fldValue = recToken.substring(0,index);
          //log.debug("error type="+fldValue);
          mmsRE.setType(fldValue);
          fldValue = recToken.substring(index+2);
          //log.debug("error desc="+fldValue);
          mmsRE.setDesc(fldValue);
        
          // add error
          mmsRequest.addProcessError(mmsRE);

        // num errors line
        } else if(bErrRec && (index = recToken.indexOf(" Errors Found.")) >= 0) {
          iNumErrors = Integer.parseInt(recToken.substring(0,index));
          //log.debug("iNumErrors="+iNumErrors);
        
        // field value
        } else {
          fldName = tmpltTkns.nextToken();
          // load field?
          for(int i=0; i<arrFldNmesToLoad.length; i++) {
            if(fldName.equals(arrFldNmesToLoad[i]))
              mmsRequest.setRequestDataItem(fldName,"",fldtransform_toMMSRequest(recToken));
          }
          if(!tmpltTkns.hasMoreTokens()) {
            log.error("Error parsing MMS Record string: Token mismatch!  Aborted.");
            return false;
          }
          tmpltTkns.nextToken();  // throw away length
        }
        
      }
      
      //log.debug("RecordToMMSRequest() returning true");
      return true;
      
    }
    catch(Exception e) {
      log.error(e.getMessage());
    }
    
    return false;
  }
  
}  // FTPBatch_Template
