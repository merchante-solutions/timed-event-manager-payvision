/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/CancelQueueBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/29/02 4:28p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.screens.ScreenInfo;
import com.mes.screens.SequenceIdBean;

public class CancelQueueBean extends QueueBean
{
  public void fillReportMenuItems()
  {
    addReportMenuItem(new ReportMenuItem("Cancel Queue", "mes_queue.jsp?queueType=" + QueueConstants.QUEUE_CANCEL + "&queueStage=" + QueueConstants.Q_CANCEL_NEW));
  }
  
  public CancelQueueBean()
  {
    super("CancelQueueBean");
    fillReportMenuItems();
  }
  
  public CancelQueueBean(String msg)
  {
    super(msg);
    fillReportMenuItems();
  }
  
  public boolean getReasons(int reasonType)
  {
    boolean success = false;
    
    try
    {
      pstmt = getPreparedStatement("select * from qreason where qreason_type = ?");
      
      pstmt.setInt(1, reasonType);
      
      queueResultSet = pstmt.executeQuery();
      
      success = true;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getReasons: " + e.toString());
    }
    
    return success;
  }
  
  public boolean getCreditData(long controlNumber)
  {
    StringBuffer        qs        = new StringBuffer("");
    boolean             success   = false;
    
    try
    {
      qs.append("select a.sic_code, a.merch_invg_code, a.merch_met_table_number, b.merch_credit_grade, b.merch_credit_score, b.merch_credit_review_date, b.merch_debit_achpost_days, b.merch_credit_achpost_days ");
      qs.append("from merchant a, merchcredit b ");
      qs.append("where a.merc_cntrl_number = ? and a.app_seq_num = b.app_seq_num");
      
      pstmt = getPreparedStatement(qs.toString());
      pstmt.setLong(1, controlNumber);
      
      queueResultSet = pstmt.executeQuery();
      
      success = true;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCreditData: " + e.toString());
    }
    
    return success;
  }
  
  public boolean getMetTables()
  {
    boolean             success   = false;
    
    try
    {
      pstmt = getPreparedStatement("select * from met_tables order by met_table asc");
      
      queueResultSet = pstmt.executeQuery();
      
      success = true;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMetTables: " + e.toString());
    }
    
    return success;
  }
  
  public boolean getInvestigators()
  {
    boolean             success   = false;
    
    try
    {
      pstmt = getPreparedStatement("select * from investigators order by investigator_code asc");
      
      queueResultSet = pstmt.executeQuery();
      
      success = true;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getInvestigators: " + e.toString());
    }
    
    return success;
  }
  
  public String getLockInfo(long controlNumber)
  {
    // get application sequence number from controlnumber
    return super.getLockInfo(this.getAppSeqNum(controlNumber));
  }
  
  public boolean setLock(String lockedBy, long controlNumber)
  {
    return super.setLock(this.getAppSeqNum(controlNumber), com.mes.ops.QueueConstants.QUEUE_CREDIT, lockedBy);
  }
  
  public void getFunctionOptions(javax.servlet.jsp.JspWriter out, long appSeqNum)
  {
    SequenceIdBean  seqId = new SequenceIdBean();
    ScreenInfo      screen;
    Vector          screens;
    
    try
    {
      out.println("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" width=\"80%\">");
      
      seqId.getData(com.mes.constants.mesConstants.SEQUENCE_CREDIT_EVAL, appSeqNum);
                    
      screens = seqId.getScreens();
      
      for(int i=0; i < screens.size(); ++i)
      {
        screen = (ScreenInfo)screens.elementAt(i);
        
        String screenLink = screen.getJspName();
        String screenName = screen.getScreenDescription();
        out.println("  <tr>");
        out.println("    <td height=\"19\" valign=\"center\">");
        if(screen.isCompleted())
        {
          out.println("    <img src=\"/images/check1.jpg\">");
        }
        else
        {
          out.println("    <img src=\"/images/nocheck.jpg\">");
        }
        out.println("    </td>");
        out.println("    <td height=\"19\" valign=\"center\">");
        out.println("      <a href=\"" + screenLink + "?primaryKey=" + appSeqNum + "\">" + screenName + "</a>");
        out.println("    </td>");
        out.println("  </tr>");
      }
      out.println("</table>");
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFunctionOptions: " + e.toString());
    }
  }
  
  public boolean getQueueData(long queueKey, int queueType)
  {
    return(super.getQueueData(
            queueKey, 
            queueType,
            "app_queue_credit c, merchant d", 
            "c.*, d.merc_cntrl_number, d.merch_business_name",
            "and a.app_seq_num = c.app_seq_num and a.app_seq_num = d.app_seq_num"));
  }
  
  public long getAppUserType(long appSeqNum)
  {
    PreparedStatement       ps      = null;
    ResultSet               rs      = null;
    long                    retVal  = com.mes.constants.mesConstants.USER_DEFAULT;
    long                    userId  = 0L;
    
    try
    {
      ps = getPreparedStatement("select app_user from app_queue where app_seq_num = ?");
      ps.setLong(1, appSeqNum);
    
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        userId = rs.getLong("app_user");
        
        if(userId <= 0L)
        {
          retVal = com.mes.constants.mesConstants.USER_MERCHANT;
        }
        else
        {
          rs.close();
          rs = null;
          ps.close();
          ps = null;
          
          ps = getPreparedStatement("select user_type_id from orguser where user_id = ?");
          ps.setLong(1, userId);
          
          rs = ps.executeQuery();
          
          if(rs.next())
          {
            retVal = rs.getLong("user_type_id");
          }
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAppUserType: " + e.toString());
    }
    finally
    {
      if(rs != null)
      {
        try
        {
          rs.close();
        }
        catch(Exception e)
        {
        }
      }
      
      if(ps != null)
      {
        try
        {
          ps.close();
        }
        catch(Exception e)
        {
        }
      }
    }
    
    return retVal;
  }
  
}
