/*@lineinfo:filename=AccountSetupResearchBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AccountSetupResearchBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/22/02 2:12p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class AccountSetupResearchBean extends SQLJConnectionBase
{
  private ResultSetIterator it                  = null;

  private String            lookupValue         = "";
 
  private boolean           submitted           = false;
  private boolean           error               = false;

  public Vector             controlNums         = new Vector();
  public Vector             dbas                = new Vector(); 
  public Vector             merchantNums        = new Vector();
  public Vector             sendDates           = new Vector();
  
  public void AccountSetupResearchBean()
  {
  }
 
  
  public boolean validate()
  {
    if(lookupValue == null || lookupValue.equals(""))
    {
      error = true;
    }
    
    return !error;
     
  }

  /*
  ** METHOD public void getData()
  **
  */
  public void getData()
  {
    try
    {
      connect();
        
       
      /*@lineinfo:generated-code*//*@lineinfo:86^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  
//                  merch.merch_business_name,
//                  merch.merc_cntrl_number,
//                  merch.merch_number,
//                  acsc.date_processed
//  
//          from    
//                  merchant           merch,
//                  billcard           bill,
//                  app_setup_complete acsc
//  
//          where   
//                  merch.app_seq_num = bill.app_seq_num and
//                  merch.app_seq_num = acsc.app_seq_num  and
//                  bill.cardtype_code = 1 and
//                  (
//                    merch.merch_number        = :lookupValue or
//                    merch.merc_cntrl_number   = :lookupValue 
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  \n                merch.merch_business_name,\n                merch.merc_cntrl_number,\n                merch.merch_number,\n                acsc.date_processed\n\n        from    \n                merchant           merch,\n                billcard           bill,\n                app_setup_complete acsc\n\n        where   \n                merch.app_seq_num = bill.app_seq_num and\n                merch.app_seq_num = acsc.app_seq_num  and\n                bill.cardtype_code = 1 and\n                (\n                  merch.merch_number        =  :1  or\n                  merch.merc_cntrl_number   =  :2  \n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AccountSetupResearchBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,lookupValue);
   __sJT_st.setString(2,lookupValue);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AccountSetupResearchBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:107^7*/
        
        ResultSet rs = it.getResultSet();
        String dateStr = "";
        while(rs.next())
        {
          controlNums.add(rs.getString("merc_cntrl_number"));
          dbas.add(rs.getString("merch_business_name"));
          merchantNums.add(rs.getString("merch_number"));
          dateStr = DateTimeFormatter.getFormattedDate(rs.getTimestamp("date_processed"), "MM/dd/yy HH:mm");

          if(dateStr == null || dateStr.equals(""))
          {
            dateStr = "Scheduled For Next Upload";
          }

          sendDates.add(dateStr);
        }

        it.close();
        
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  
  /*
  ** ACCESSORS
  */
  public String getLookupValue()
  {
    return lookupValue;
  }
  public void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }

  public void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
  public boolean isSubmitted()
  {
    return this.submitted;
  }
 
  public boolean hasErrors()
  {
    return error;
  } 
  
}/*@lineinfo:generated-code*/