/*@lineinfo:filename=ChargeRecordsBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ChargeRecordsBean.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-12-24 11:13:34 -0800 (Wed, 24 Dec 2008) $
  Version            : $Revision: 15648 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class ChargeRecordsBean extends ExpandDataBean
{
  private Vector  chargeRecs         = new Vector();
  private Vector  appChargeRecs      = new Vector();

  private int     numRecords         = 0;
  private boolean addNew             = false;
  private String  transitRoute       = "";
  private String  checkActNum        = "";
  private String  minDisc            = "";
  private String  discountTaxInd     = "";
  private boolean ajfUsed            = false;
  private boolean cbfUsed            = false;

  private String  equipmentNotes     = "";
  private String  pricingNotes       = "";

  private int     appType            = 0;

  public Vector   billMethods        = new Vector();
  public Vector   billMethodsDesc    = new Vector();
  public Vector   taxInd             = new Vector();
  public Vector   taxIndDesc         = new Vector();
  public Vector   chrgTypes          = new Vector();
  public Vector   chrgTypesDesc      = new Vector();


  public ChargeRecordsBean()
  {
    billMethods.add("");
    billMethods.add("D");
    billMethods.add("R");
    billMethodsDesc.add("select");
    billMethodsDesc.add("D (debit)");
    billMethodsDesc.add("R (remit)");

    taxInd.add("");
    taxInd.add("N");
    taxInd.add("T");
    taxInd.add("E");
    taxInd.add("I");
    taxIndDesc.add("select");
    taxIndDesc.add("N (not applicable)");
    taxIndDesc.add("T (tax)");
    taxIndDesc.add("E (exempt)");
    taxIndDesc.add("I (immune)");

    chrgTypes.add("");
    chrgTypes.add("IMP");
    chrgTypes.add("POS");
    chrgTypes.add("MEM");
    chrgTypes.add("MIS");
    chrgTypes.add("MSG");
    chrgTypes.add("AJF");
    chrgTypes.add("CBF");
    chrgTypes.add("ACH");
    chrgTypes.add("XXX");
    chrgTypesDesc.add("select");
    chrgTypesDesc.add("IMP");
    chrgTypesDesc.add("POS");
    chrgTypesDesc.add("MEM");
    chrgTypesDesc.add("MIS");
    chrgTypesDesc.add("MSG");
    chrgTypesDesc.add("AJF");
    chrgTypesDesc.add("CBF");
    chrgTypesDesc.add("ACH");
    chrgTypesDesc.add("Delete");
  }

  private String getMiscDesc(long primaryKey, int miscCode)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    if(miscCode != mesConstants.APP_MISC_CHARGE_UNIQUE_FEE &&
       miscCode != mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP &&
       miscCode != mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE)
    {
      return result;
    }

    try
    {
      connect();

      if(miscCode == mesConstants.APP_MISC_CHARGE_UNIQUE_FEE)
      {
        result = "Unique Fee ";

        /*@lineinfo:generated-code*//*@lineinfo:135^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  siteinsp_unique_fee_desc
//            from    siteinspection
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  siteinsp_unique_fee_desc\n          from    siteinspection\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:140^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          result += isBlank(rs.getString("siteinsp_unique_fee_desc")) ? "" : " - " + rs.getString("siteinsp_unique_fee_desc");
        }

        rs.close();
        it.close();
      }
      else if(miscCode == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP  || miscCode == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE)
      {
        /*@lineinfo:generated-code*//*@lineinfo:154^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pos_desc
//            from    pos_category pc,
//                    merch_pos mp,
//                    application app
//            where   app.app_seq_num = :primaryKey and
//                    app.APP_TYPE not in (:mesConstants.APP_TYPE_VERISIGN, :mesConstants.APP_TYPE_NSI) and
//                    app.app_seq_num = mp.app_seq_num and
//                    mp.POS_CODE = pc.POS_CODE and
//                    pc.POS_TYPE = :mesConstants.POS_INTERNET
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pos_desc\n          from    pos_category pc,\n                  merch_pos mp,\n                  application app\n          where   app.app_seq_num =  :1  and\n                  app.APP_TYPE not in ( :2 ,  :3 ) and\n                  app.app_seq_num = mp.app_seq_num and\n                  mp.POS_CODE = pc.POS_CODE and\n                  pc.POS_TYPE =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_TYPE_VERISIGN);
   __sJT_st.setInt(3,mesConstants.APP_TYPE_NSI);
   __sJT_st.setInt(4,mesConstants.POS_INTERNET);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          if(miscCode == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP)
          {
            result = "Setup Fee-" + rs.getString("pos_desc");
          }
          else if(miscCode == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE)
          {
            result = "Monthly Fee-" + rs.getString("pos_desc");
          }
        }

        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("getMiscDesc(" + primaryKey + ", " + miscCode + ")", e.toString());
      result = "";
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return result;
  }

  public void getApplicationData(long primaryKey)
  {
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    StringBuffer      qs        = new StringBuffer("");
    ChargeRec         charge    = null;
    String            strDesc   = "";
    boolean           ownedRec  = false;
    int               ownedQty  = 0;
    String            ownedAmt  = "0.00";

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:215^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mc.*,
//                  md.misc_description
//          from    miscchrg mc,
//                  miscdescrs md
//          where   mc.app_seq_num = :primaryKey and
//                  mc.misc_code = md.misc_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mc.*,\n                md.misc_description\n        from    miscchrg mc,\n                miscdescrs md\n        where   mc.app_seq_num =  :1  and\n                mc.misc_code = md.misc_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:223^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        charge = new ChargeRec();
        strDesc = getMiscDesc(primaryKey, rs.getInt("misc_code"));
        charge.setChargeDesc( isBlank(strDesc) ? rs.getString("misc_description") : strDesc);
        charge.setChargeAmt( (isBlank(rs.getString("misc_chrg_amount"))           ? "" : rs.getString("misc_chrg_amount")) );
        charge.setChargeNum("---");
        charge.setChargeCode(rs.getInt("misc_code"));
        appChargeRecs.add(charge);
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:241^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  a.*,
//                  b.equiplendtype_description,
//                  c.equiptype_description,
//                  d.equip_descriptor
//          from    merchequipment  a,
//                  equiplendtype   b,
//                  equiptype       c,
//                  equipment       d
//          where   app_seq_num    = :primaryKey and
//                  a.equiplendtype_code = b.equiplendtype_code and
//                  a.equiptype_code = c.equiptype_code and
//                  a.equip_model    = d.equip_model
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  a.*,\n                b.equiplendtype_description,\n                c.equiptype_description,\n                d.equip_descriptor\n        from    merchequipment  a,\n                equiplendtype   b,\n                equiptype       c,\n                equipment       d\n        where   app_seq_num    =  :1  and\n                a.equiplendtype_code = b.equiplendtype_code and\n                a.equiptype_code = c.equiptype_code and\n                a.equip_model    = d.equip_model";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        do
        {
          if(rs.getInt("equiplendtype_code") == mesConstants.APP_EQUIP_OWNED)
          {
            ownedRec = true;
            ownedQty += rs.getInt("merchequip_equip_quantity");
            ownedAmt = isBlank(rs.getString("merchequip_amount")) ? ownedAmt : rs.getString("merchequip_amount");
          }
          else
          {
            charge = new ChargeRec();

            qs.setLength(0);
            qs.append(rs.getString("equiplendtype_description") + " " + rs.getString("equip_descriptor") + " *" + rs.getString("equip_model") + "*");
            qs.setLength(40);

            charge.setChargeDesc(qs.toString());
            charge.setChargeNum( (isBlank(rs.getString("merchequip_equip_quantity"))   ? "1" : rs.getString("merchequip_equip_quantity")) );
            charge.setChargeAmt( (isBlank(rs.getString("merchequip_amount"))           ? "0.00" : rs.getString("merchequip_amount")) );
            appChargeRecs.add(charge);
          }

        }while(rs.next());

        if(ownedRec)
        {
          charge = new ChargeRec();

          qs.setLength(0);
          qs.append("Owned equipment support fee             ");
          
          charge.setChargeDesc(qs.toString());
          charge.setChargeNum(Integer.toString(ownedQty));
          charge.setChargeAmt(ownedAmt);
          appChargeRecs.add(charge);
        }
      }

      rs.close();
      it.close();

      //get pricing notes from application
      /*@lineinfo:generated-code*//*@lineinfo:303^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  siteinsp_comment
//          from    siteinspection
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  siteinsp_comment\n        from    siteinspection\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:308^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.pricingNotes = rs.getString("siteinsp_comment");
      }

      rs.close();
      it.close();

      //get equipment notes from application
      /*@lineinfo:generated-code*//*@lineinfo:321^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equipment_comment
//          from    pos_features
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equipment_comment\n        from    pos_features\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:326^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.equipmentNotes = rs.getString("equipment_comment");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getApplicationData(" + primaryKey + ")", e.toString());
      addError("getApplicationData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }


  public void getData(long primaryKey)
  {
    getAppType(primaryKey);
    getChrg(primaryKey);
    getAcctInfo(primaryKey);
    getMinDiscInfo(primaryKey);
    getDiscTaxInd(primaryKey);
  }

  private void getAppType(long primaryKey)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:367^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//          
//          from    application
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.ChargeRecordsBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:373^7*/
    }
    catch(Exception e)
    {
      logEntry("getAppType(" + primaryKey + ")", e.toString());
      addError("getAppType: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void getChrg(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    ChargeRec         charge  = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:396^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  billoth_charge_msg,
//                  billoth_charge_type,
//                  billoth_charge_startdate,
//                  billoth_charge_expiredate,
//                  billoth_charge_frq,
//                  billoth_charge_times,
//                  billoth_charge_method,
//                  billoth_charge_amt,
//                  billoth_charge_tax_ind,
//                  nvl(misc_code, -1) misc_code
//          from    billother
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  billoth_charge_msg,\n                billoth_charge_type,\n                billoth_charge_startdate,\n                billoth_charge_expiredate,\n                billoth_charge_frq,\n                billoth_charge_times,\n                billoth_charge_method,\n                billoth_charge_amt,\n                billoth_charge_tax_ind,\n                nvl(misc_code, -1) misc_code\n        from    billother\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:410^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        do
        {
          charge = new ChargeRec();

          charge.setChargeDesc(   (isBlank(rs.getString("billoth_charge_msg"))          ? "" : rs.getString("billoth_charge_msg")) );
          charge.setChargeType(   (isBlank(rs.getString("billoth_charge_type"))         ? "" : rs.getString("billoth_charge_type")) );
          charge.setChargeStart(  (isBlank(rs.getString("billoth_charge_startdate"))    ? "" : rs.getString("billoth_charge_startdate")) );
          charge.setChargeExpire( (isBlank(rs.getString("billoth_charge_expiredate"))   ? "" : rs.getString("billoth_charge_expiredate")) );
          charge.setChargeFreq(   (isBlank(rs.getString("billoth_charge_frq"))          ? "NNNNNNNNNNNN" : rs.getString("billoth_charge_frq")) );
          charge.setChargeNum(    (isBlank(rs.getString("billoth_charge_times"))        ? "1" : rs.getString("billoth_charge_times")) );
          charge.setChargeMethod( (isBlank(rs.getString("billoth_charge_method"))       ? "D" : rs.getString("billoth_charge_method")) );
          charge.setChargeAmt(    (isBlank(rs.getString("billoth_charge_amt"))          ? "" : rs.getString("billoth_charge_amt")) );
          charge.setChargeTax(    (isBlank(rs.getString("billoth_charge_tax_ind"))      ? "" : rs.getString("billoth_charge_tax_ind")) );
          charge.setChargeCode( rs.getInt("misc_code") );

          chargeRecs.add(charge);

        }while(rs.next());
      }
      else
      {
        getMiscChrg(primaryKey);
        getEquip(primaryKey);
      }
    }
    catch(Exception e)
    {
      logEntry("getChrg(" + primaryKey + ")", e.toString());
      addError("getChrg: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getMiscChrg(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    ChargeRec         charge  = null;
    String            strDesc = "";

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:465^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  a.*,
//                  b.chrgbasis_code,
//                  b.misc_description
//          from    miscchrg a,
//                  miscdescrs b
//          where   a.app_seq_num = :primaryKey and
//                  a.misc_code != :mesConstants.APP_MISC_CHARGE_CHARGEBACK and
//                  a.misc_code = b.misc_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  a.*,\n                b.chrgbasis_code,\n                b.misc_description\n        from    miscchrg a,\n                miscdescrs b\n        where   a.app_seq_num =  :1  and\n                a.misc_code !=  :2  and\n                a.misc_code = b.misc_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_CHARGEBACK);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:475^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        //we dont build charge records for these
        if(rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_ACH_DEPOSIT_FEE     ||
           rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_VOICE_AUTH_FEE      ||
           rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_REFERRAL_AUTH_FEE)
        {
          continue;
        }

        charge = new ChargeRec();

        charge.setChargeCode(rs.getInt("misc_code"));

        if(rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP  ||
           rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE)
        {
          charge.setChargeDesc(getMiscDesc(primaryKey, rs.getInt("misc_code")));
        }
        else if(rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_UNIQUE_FEE ||
                rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_MISC1_FEE  ||
                rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_MISC2_FEE)
        {
          charge.setChargeDesc( isBlank(rs.getString("MISC_CHRGBASIS_DESCR")) ? "" : rs.getString("MISC_CHRGBASIS_DESCR"));
        }
        else
        {
          charge.setChargeDesc( isBlank(rs.getString("misc_description")) ? "" : rs.getString("misc_description"));
        }

        charge.setChargeStart(getStartDate()); // will be 1st of this month if before 16th or 1st of next month if after 15th

        //if this field is blank then use charge basis associated with fee..
        //but if its not blank.. then we use specified charge basis
        if(isBlank(rs.getString("MISC_CHRGBASIS_CODE")))
        {
          if(rs.getString("chrgbasis_code").equals("MC"))
          {
            charge.setChargeFreq("YYYYYYYYYYYY");
            charge.setChargeExpire("999999"); //never expires
            charge.setChargeNum("0");
          }
          else if(rs.getString("chrgbasis_code").equals("YR"))
          {
            charge.setChargeFreq(getNextMonthFlag());
            charge.setChargeExpire("999999"); //never expires
            charge.setChargeNum("0");
          }
          else
          {
            charge.setChargeFreq(getNextMonthFlag());
            charge.setChargeExpire(getExpireDate()); //will be 15th of month after start date
            charge.setChargeNum("1");
          }
        }
        else
        {
          if(rs.getString("MISC_CHRGBASIS_CODE").equals("MC"))
          {
            charge.setChargeFreq("YYYYYYYYYYYY");
            charge.setChargeExpire("999999"); //never expires
            charge.setChargeNum("0");
          }
          else if(rs.getString("MISC_CHRGBASIS_CODE").equals("YR"))
          {
            charge.setChargeFreq(getNextMonthFlag());
            charge.setChargeExpire("999999"); //never expires
            charge.setChargeNum("0");
          }
          else
          {
            charge.setChargeFreq(getNextMonthFlag());
            charge.setChargeExpire(getExpireDate()); //will be 15th of month after start date
            charge.setChargeNum("1");
          }
        }

        charge.setChargeMethod("D");
        charge.setChargeAmt( (isBlank(rs.getString("misc_chrg_amount"))           ? ""             : rs.getString("misc_chrg_amount")) );
        charge.setChargeTax("N");    //default to non applicable

        if(this.appType == mesConstants.APP_TYPE_CBT || this.appType == mesConstants.APP_TYPE_CBT_NEW)
        {
          charge.setChargeType("MIS");
        }
        else
        {
          charge.setChargeType("MEM");
        }
        
        // set annual pci compliance fee to always bill in November
        if( rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_PCI_COMPLIANCE )
        {
          charge.setChargeFreq("NNNNNNNNNNYN");
          
          if( appType != mesConstants.APP_TYPE_PAYPAL_REFERRAL &&
              (DateTimeFormatter.getCurDate()).compareTo(DateTimeFormatter.parseDate("01/01/2009", "mm/dd/yyyy")) < 0)
          {
            // set charge start to be december 08 to make sure this event doesn't fire in november 08
            charge.setChargeStart("12/01/08");
          }
        }

        chargeRecs.add(charge);
      }
      
      rs.close();
      it.close();
      
      // get odd charge records from app
      /*@lineinfo:generated-code*//*@lineinfo:589^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  substr(acr.description, 1, 40)  description,
//                  acr.charge_rec_type             charge_rec_type,
//                  acr.amount                      amount
//          from    appo_charge_recs acr
//          where   acr.app_seq_num = :primaryKey
//          order by acr.charge_rec_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  substr(acr.description, 1, 40)  description,\n                acr.charge_rec_type             charge_rec_type,\n                acr.amount                      amount\n        from    appo_charge_recs acr\n        where   acr.app_seq_num =  :1 \n        order by acr.charge_rec_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:597^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        charge = new ChargeRec();
        
        charge.setChargeDesc(rs.getString("description"));
        charge.setChargeStart(getStartDate());
        
        switch(rs.getInt("charge_rec_type"))
        {
          case AutoUploadChargeRecords.CHARGE_TYPE_SINGLE:
            // set charge frequence to first available month
            charge.setChargeFreq(getMonthFlag());
            charge.setChargeExpire(getExpireDate());
            break;
            
          case AutoUploadChargeRecords.CHARGE_TYPE_MONTHLY:
            charge.setChargeFreq("YYYYYYYYYYYY");
            charge.setChargeExpire("999999");
            break;
        }
        
        charge.setChargeNum("0");
        charge.setChargeAmt(rs.getString("amount"));
        charge.setChargeTax("N");
        charge.setChargeType("MIS");
        charge.setChargeMethod("D");
        
        chargeRecs.add(charge);
      }
      
      rs.close();
      it.close();
      
    }
    catch(Exception e)
    {
      logEntry("getMiscChrg(" + primaryKey + ")", e.toString());
      addError("getMiscChrg: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private int getChargeMonth()
  {
    int result = 0;
    try
    {
      int thisMonth = Calendar.getInstance().get(Calendar.MONTH);
      int thisDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
      
      result = thisMonth;
      
      // if today is after the 15th then set the month to be next month
      if(thisDay > 15)
      {
        if(thisMonth == 11)
        {
          result = 0;
        }
        else
        {
          ++result;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getChargeMonth()", e.toString());
    }
    return result;
  }
  
  private String getMonthFlag()
  {
    StringBuffer result = new StringBuffer("NNNNNNNNNNNN");
    
    int nextMonth = getChargeMonth();

    try
    {
      result = result.replace(nextMonth,nextMonth+1,"Y");
    }
    catch(Exception e)
    {
      // System.out.println("Error in getMonthFlag() - " + e.toString());
    }

    return result.toString();
  }
  
  private String getStartDate()
  {
    Calendar  rightNow  = Calendar.getInstance();
    int       thisMonth = rightNow.get(Calendar.MONTH); //0 = jan; 11 = dec
    int       thisDay   = rightNow.get(Calendar.DAY_OF_MONTH);
    int       thisYear  = rightNow.get(Calendar.YEAR);

    String    result    = "";

    if(thisDay > 15)
    {
      //if its the 12th month we move it to the first month
      if(thisMonth == 11)
      {
        result = "01/01/" + fixYear((thisYear + 1));
      }
      else //else we just add one
      {
        result = (thisMonth + 2) + "/01/" + fixYear(thisYear);
      }
    }
    else
    {
      if(thisMonth == 11)
      {
        result = "12/01/" + fixYear(thisYear);
      }
      else //else we just add one
      {
        result = (thisMonth + 1) + "/01/" + fixYear(thisYear);
      }
    }

    return padDate(result);
  }

  private String getNextYearStartDate()
  {
    Calendar  rightNow  = Calendar.getInstance();
    int       thisMonth = rightNow.get(Calendar.MONTH); //0 = jan; 11 = dec
    int       thisDay   = rightNow.get(Calendar.DAY_OF_MONTH);
    int       thisYear  = rightNow.get(Calendar.YEAR);

    //we want to make the start date in one year, so we add 1 to the year
    thisYear += 1;

    String    result    = "";

    if(thisDay > 15)
    {
      //if its the 12th month we move it to the first month
      if(thisMonth == 11)
      {
        result = "01/01/" + fixYear((thisYear + 1));
      }
      else //else we just add one
      {
        result = (thisMonth + 2) + "/01/" + fixYear(thisYear);
      }
    }
    else
    {
      if(thisMonth == 11)
      {
        result = "12/01/" + fixYear(thisYear);
      }
      else //else we just add one
      {
        result = (thisMonth + 1) + "/01/" + fixYear(thisYear);
      }
    }

    return padDate(result);
  }


  private String getExpireDate()
  {
    Calendar  rightNow  = Calendar.getInstance();
    int       thisMonth = rightNow.get(Calendar.MONTH); //0 = jan; 11 = dec
    int       thisDay   = rightNow.get(Calendar.DAY_OF_MONTH);
    int       thisYear  = rightNow.get(Calendar.YEAR);

    String    result    = "";

    if(thisDay > 15)
    {
      //if its the 12th month we move it to the first month
      if(thisMonth == 11)
      {
        result = "02/15/" + fixYear((thisYear + 1));
      }
      else if(thisMonth == 10)
      {
        result = "01/15/" + fixYear((thisYear + 1));
      }
      else //else we just add one
      {
        result = (thisMonth + 3) + "/15/" + fixYear(thisYear);
      }
    }
    else
    {
      if(thisMonth == 11)
      {
        result = "01/15/" + fixYear((thisYear + 1));
      }
      else //else we just add one
      {
        result = (thisMonth + 2) + "/15/" + fixYear(thisYear);
      }
    }

    return padDate(result);
  }

  private String padDate(String dte)
  {
    String result = dte;

    if(dte.length() == 7)
    {
      result = "0" + dte;
    }

    return result;
  }

  private String fixYear(int intYear)
  {
    String result = "";
    String strDate = Integer.toString(intYear);

    if(strDate.length() == 4)
    {
      result = strDate.substring(2); // gets only the last two digits
    }
    else
    {
      result = strDate;
    }

    return result;
  }

  private String getNextMonthFlag()
  {
    StringBuffer result = new StringBuffer("NNNNNNNNNNNN");

    //months start at 0 to 11

    Calendar rightNow = Calendar.getInstance();
    int thisMonth = rightNow.get(Calendar.MONTH);
    int thisDay   = rightNow.get(Calendar.DAY_OF_MONTH);

    //add 1 to this month so we can set next month

    int nextMonth = thisMonth;

    //if its after the 15th of the month then we set the default to the next month...
    //before the 15th we use the current month..
    if(thisDay > 15)
    {
      //if its the 12th month we move it to the first month
      if(thisMonth == 11)
      {
        nextMonth = 0;
      }
      else //else we just add one
      {
        nextMonth += 1;
      }
    }

    try
    {
      result = result.replace(nextMonth,nextMonth+1,"Y");
    }
    catch(Exception e)
    {
      // System.out.println("Error in getNextMonthFlag() - " + e.toString());
    }

    return result.toString();
  }

  private String addSlash(String dateStr)
  {
    String result = dateStr;

    if(isBlank(dateStr) || dateStr.equals("999999"))
    {
      return result;
    }

    if(dateStr.length() == 6)
    {
      result = dateStr.substring(0,2) + "/" + dateStr.substring(2,4) + "/" + dateStr.substring(4);
    }

    return result;

  }


  private void getEquip(long primaryKey)
  {
    StringBuffer      qs        = new StringBuffer();
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    ChargeRec         charge    = null;
    boolean           ownedRec  = false;
    int               ownedQty  = 0;
    double            ownedAmt  = 0.0;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:915^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  a.*,
//                  b.equiplendtype_description,
//                  c.equiptype_description,
//                  d.equip_descriptor,
//                  d.tsys_equip_desc
//          from    merchequipment  a,
//                  equiplendtype        b,
//                  equiptype            c,
//                  equipment            d
//          where   a.app_seq_num    = :primaryKey and
//                  a.equiplendtype_code = b.equiplendtype_code and
//                  a.equiptype_code = c.equiptype_code and
//                  a.equip_model    = d.equip_model
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  a.*,\n                b.equiplendtype_description,\n                c.equiptype_description,\n                d.equip_descriptor,\n                d.tsys_equip_desc\n        from    merchequipment  a,\n                equiplendtype        b,\n                equiptype            c,\n                equipment            d\n        where   a.app_seq_num    =  :1  and\n                a.equiplendtype_code = b.equiplendtype_code and\n                a.equiptype_code = c.equiptype_code and\n                a.equip_model    = d.equip_model";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:930^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        do
        {
          if(rs.getInt("equiplendtype_code") == mesConstants.APP_EQUIP_OWNED)
          {
            ownedRec = true;
            ownedQty += rs.getInt("merchequip_equip_quantity");
            ownedAmt = isBlank(rs.getString("merchequip_amount")) ? 0.0 : rs.getDouble("merchequip_amount");
          }
          else
          {

            //if there is no fee we dont need it
            if(isBlank(rs.getString("merchequip_amount")) || rs.getDouble("merchequip_amount") == 0.0)
            {
              continue;
            }

            charge = new ChargeRec();

            qs.setLength(0);
            qs.append(rs.getString("equiplendtype_description") + " " + rs.getString("tsys_equip_desc"));
            qs.setLength(40);

            charge.setChargeDesc(qs.toString());
            charge.setChargeStart(getStartDate()); // will be 1st of this month if before 16th or 1st of next month if after 15th

            if(rs.getInt("equiplendtype_code") == mesConstants.APP_EQUIP_RENT)
            {
              charge.setChargeFreq("YYYYYYYYYYYY");
              charge.setChargeExpire("999999"); //never expires
            }
            else
            {
              charge.setChargeFreq(getNextMonthFlag());
              charge.setChargeExpire(getExpireDate()); //will be 15th of month after start date
            }


            charge.setChargeNum( (isBlank(rs.getString("merchequip_equip_quantity"))         ? "1" : rs.getString("merchequip_equip_quantity")) );
            charge.setChargeMethod("D");
            charge.setChargeAmt( (isBlank(rs.getString("merchequip_amount"))         ? "0.00" : rs.getString("merchequip_amount")) );
            charge.setChargeTax("N");    //default to non applicable

            if((this.appType == mesConstants.APP_TYPE_CBT || this.appType == mesConstants.APP_TYPE_CBT_NEW) && rs.getInt("equiptype_code") == 4)//imprinter
            {
              charge.setChargeType("IMP");
            }
            else
            {
              charge.setChargeType("POS");
            }

            chargeRecs.add(charge);
          }

        }while(rs.next());

        rs.close();
        it.close();

        if(ownedRec  && ownedAmt > 0.0)
        {
          charge = new ChargeRec();

          qs.setLength(0);
          qs.append("Owned equipment support fee");
          qs.setLength(40);

          charge.setChargeDesc(qs.toString());
          charge.setChargeStart(getStartDate()); // will be 1st of this month if before 16th or 1st of next month if after 15th
          charge.setChargeFreq("YYYYYYYYYYYY");
          charge.setChargeExpire("999999"); //never expires
          charge.setChargeNum(Integer.toString(ownedQty));
          charge.setChargeMethod("D");
          charge.setChargeAmt(Double.toString(ownedAmt));
          charge.setChargeTax("N");    //default to non applicable
          charge.setChargeType("MIS");
          chargeRecs.add(charge);
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getEquip(" + primaryKey + ")", e.toString());
      addError("getChrg: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getAcctInfo(long primaryKey)
  {
    StringBuffer      qs  = new StringBuffer("");
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1040^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchbank_acct_num,
//                  merchbank_transit_route_num
//          from    merchbank
//          where   app_seq_num = :primaryKey and
//                  merchbank_acct_srnum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchbank_acct_num,\n                merchbank_transit_route_num\n        from    merchbank\n        where   app_seq_num =  :1  and\n                merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1047^7*/

      rs = it.getResultSet();

      if (rs.next())
      {
        this.checkActNum   = rs.getString("merchbank_acct_num");
        this.transitRoute  = rs.getString("merchbank_transit_route_num");
      }

      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("getAcctInfo(" + primaryKey + ")", e.toString());
      addError("getAcctInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getMinDiscInfo(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1082^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tranchrg_mmin_chrg
//          from    tranchrg
//          where   app_seq_num = :primaryKey and
//                  cardtype_code = :mesConstants.APP_CT_VISA
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tranchrg_mmin_chrg\n        from    tranchrg\n        where   app_seq_num =  :1  and\n                cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1088^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.minDisc =  isBlank(rs.getString("tranchrg_mmin_chrg")) ? "0.00" : rs.getString("tranchrg_mmin_chrg");
      }
      else
      {
        this.minDisc = "0.00";
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMinDiscInfo(" + primaryKey + ")", e.toString());
      addError("getMinDiscInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getDiscTaxInd(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1126^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_tax_disc_ind
//          from    merchant
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_tax_disc_ind\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.ops.ChargeRecordsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1131^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.discountTaxInd =  isBlank(rs.getString("merch_tax_disc_ind")) ? "N" : rs.getString("merch_tax_disc_ind");
      }
      else
      {
        this.discountTaxInd = "N";
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getDiscTaxInd(" + primaryKey + ")", e.toString());
      addError("getDiscTaxInd: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void updateData(HttpServletRequest req)
  {
    ChargeRec     charge  = null;
    StringBuffer  freq    = new StringBuffer();
    Vector        tempVec = new Vector();

    for(int i=0; i<numRecords; i++)
    {
      charge = new ChargeRec();

      charge.setChargeDesc( (isBlank(req.getParameter("chargeDesc" + i))     ? "" : req.getParameter("chargeDesc" + i)) );
      charge.setChargeType( (isBlank(req.getParameter("chargeType" + i))     ? "" : req.getParameter("chargeType" + i)) );
      charge.setChargeStart( (isBlank(req.getParameter("chargeStart" + i))   ? "" : req.getParameter("chargeStart" + i)) );
      charge.setChargeExpire( (isBlank(req.getParameter("chargeExpire" + i)) ? "" : req.getParameter("chargeExpire" + i)) );
      charge.setChargeCode( (isBlank(req.getParameter("chargeCode" + i))    ? -1 : Integer.parseInt(req.getParameter("chargeCode" + i))) );

      freq.setLength(0);
      for(int j=0; j<12; j++)
      {
        if(isBlank(req.getParameter("freq" + i + j)))
        {
          freq.append("N");
        }
        else
        {
          freq.append("Y");
        }
      }

      charge.setChargeFreq(freq.toString().trim());

      if(!isBlank(charge.getChargeType()) && !charge.getChargeType().equals("IMP") && !charge.getChargeType().equals("POS"))
      {
        charge.setChargeNum("0");
      }
      else
      {
        charge.setChargeNum( (isBlank(req.getParameter("chargeNum" + i))       ? "" : req.getParameter("chargeNum" + i)) );
      }

      charge.setChargeMethod( (isBlank(req.getParameter("chargeMethod" + i)) ? "" : req.getParameter("chargeMethod" + i)) );
      charge.setChargeAmt( (isBlank(req.getParameter("chargeAmt" + i))       ? "" : req.getParameter("chargeAmt" + i)) );
      charge.setChargeTax( (isBlank(req.getParameter("chargeTax" + i))       ? "" : req.getParameter("chargeTax" + i)) );

      if(!charge.getChargeType().equals("XXX"))
      {
        tempVec.add(charge);
      }
    }
    if(this.addNew)
    {
      tempVec.add(new ChargeRec("NNNNNNNNNNNN"));
    }
    this.chargeRecs = tempVec;
  }

  public boolean validate()
  {
    Date tempDate               = null;
    SimpleDateFormat dateFormat = new SimpleDateFormat ("MM/dd/yy");
    int   tempInt                 = 0;
    float tempFloat             = 0.0f;
    long  tempLong             = 0L;

    try
    {
      tempLong = Long.parseLong(this.checkActNum);
      if(tempLong < 0L)
      {
        addError("Checking Account # must be greater than 0");
      }
    }
    catch(Exception e)
    {
      addError("Please provide a valid Checking Account #");
    }

    try
    {
      tempLong = Long.parseLong(this.transitRoute);
      if(tempLong < 0L)
      {
        addError("Transit Routing # must be greater than 0");
      }
    }
    catch(Exception e)
    {
      addError("Please provide a valid Transit Routing #");
    }

    try
    {
      tempFloat = Float.parseFloat(this.minDisc);
      if(tempFloat >= 100.0f || tempFloat < 0.0f)
      {
        addError("Minimun Discount must be a value between $0.00 and $100.00");
      }
    }
    catch(Exception e)
    {
      addError("Please provide a valid value for Minimum Discount");
    }

    if(isBlank(this.discountTaxInd))
    {
      addError("Please select a Tax Indicator for the discount charge record");
    }

    for(int i=0; i < chargeRecs.size(); ++i)
    {

      if(isBlank(getChargeDesc(i)))
      {
        addError("Please specify a Charge Description for charge record " + (i+1));
      }

      if(isBlank(getChargeType(i)))
      {
        addError("Please specify a Charge Type for charge record " + (i+1));
      }
      else
      {
        if(!ajfUsed && getChargeType(i).equals("AJF"))
        {
          ajfUsed = true;
        }
        else if(ajfUsed && getChargeType(i).equals("AJF"))
        {
          addError("AJF Charge Type can occur only once");
        }
        if(!cbfUsed && getChargeType(i).equals("CBF"))
        {
          cbfUsed = true;
        }
        else if(cbfUsed && getChargeType(i).equals("CBF"))
        {
          addError("CBF Charge Type can occur only once");
        }
      }

      try
      {
        tempInt = Integer.parseInt(getChargeNum(i));
        if(!isBlank(getChargeType(i)) && !getChargeType(i).equals("IMP") && !getChargeType(i).equals("POS") && tempInt != 0)
        {
          addError("No. Charges for<b> " + getChargeType(i) + " </b>charge type must be zero");
        }
        else if(tempInt >= 100 || tempInt < 0)
        {
          addError("No. Charges must be a number between 0 and 100");
        }
      }
      catch(Exception e)
      {
        addError("Please specify a valid No. Charges for charge record " + (i + 1));
      }

      try
      {
        tempFloat = Float.parseFloat(getChargeAmt(i));
        if(tempFloat != 0.0f && getChargeType(i).equals("ACH"))
        {
          addError("ACH Charge Type requires amount to charge to equal 0");
        }
        if(tempFloat >= 100000.00f || tempInt < 0.0f)
        {
          addError("Charge Amount must be a value between $0.00 and $1000.00");
        }
      }
      catch(Exception e)
      {
        addError("Please specify a valid Charge Amount for charge record " + (i + 1));
        tempFloat = 0.0f;
      }

      if(getChargeFreq(i).equals("NNNNNNNNNNNN") && tempFloat > 0.0f && !getChargeType(i).equals("MSG"))
      {
        addError("Please specify the Charge Frequency for charge record " + (i+1));
      }

      try
      {
        tempDate = dateFormat.parse(addSlash(getChargeStart(i)));
        ((ChargeRec)chargeRecs.elementAt(i)).setChargeStart(dateFormat.format(tempDate));
      }
      catch(Exception e)
      {
        addError("Please provide a valid start date (mm/dd/yy) for charge record " + (i+1));
      }

      if(!getChargeExpire(i).equals("999999"))
      {
        try
        {
          tempDate = dateFormat.parse(addSlash(getChargeExpire(i)));
           ((ChargeRec)chargeRecs.elementAt(i)).setChargeExpire(dateFormat.format(tempDate));
        }
        catch(Exception e)
        {
          addError("Please provide a valid expiration date (mm/dd/yy) for charge record " + (i+1));
        }
      }

      if(isBlank(getChargeMethod(i)))
      {
        addError("Please specify a Charge Method for charge record " + (i+1));
      }

      if(isBlank(getChargeTax(i)))
      {
        addError("Please specify a Tax Indicator for charge record " + (i+1));
      }
    }

    return(! hasErrors());

  }

  public void submitData(HttpServletRequest req, long primaryKey)
  {
    submitChargeData(primaryKey);
    submitAccountData(primaryKey);
    submitMinDiscData(primaryKey);
  }

  private void submitChargeData(long primaryKey)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1391^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    billother
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    billother\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1396^7*/

      commit();

      for(int i=0; i<chargeRecs.size(); ++i)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1402^9*/

//  ************************************************************
//  #sql [Ctx] { insert into billother
//            (
//              billoth_charge_msg,
//              billoth_charge_type,
//              billoth_charge_startdate,
//              billoth_charge_expiredate,
//              billoth_charge_frq,
//              billoth_charge_times,
//              billoth_charge_method,
//              billoth_charge_amt,
//              billoth_charge_tax_ind,
//              misc_code,
//              app_seq_num,
//              billoth_sr_num
//            )
//            values
//            (
//              :getChargeDesc(i),
//              :getChargeType(i),
//              :getChargeStart(i),
//              :getChargeExpire(i),
//              :getChargeFreq(i),
//              :getChargeNum(i),
//              :getChargeMethod(i),
//              :getChargeAmt(i),
//              :getChargeTax(i),
//              :getChargeCode(i),
//              :primaryKey,
//              :i
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_739 = getChargeDesc(i);
 String __sJT_740 = getChargeType(i);
 String __sJT_741 = getChargeStart(i);
 String __sJT_742 = getChargeExpire(i);
 String __sJT_743 = getChargeFreq(i);
 String __sJT_744 = getChargeNum(i);
 String __sJT_745 = getChargeMethod(i);
 String __sJT_746 = getChargeAmt(i);
 String __sJT_747 = getChargeTax(i);
 int __sJT_748 = getChargeCode(i);
   String theSqlTS = "insert into billother\n          (\n            billoth_charge_msg,\n            billoth_charge_type,\n            billoth_charge_startdate,\n            billoth_charge_expiredate,\n            billoth_charge_frq,\n            billoth_charge_times,\n            billoth_charge_method,\n            billoth_charge_amt,\n            billoth_charge_tax_ind,\n            misc_code,\n            app_seq_num,\n            billoth_sr_num\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_739);
   __sJT_st.setString(2,__sJT_740);
   __sJT_st.setString(3,__sJT_741);
   __sJT_st.setString(4,__sJT_742);
   __sJT_st.setString(5,__sJT_743);
   __sJT_st.setString(6,__sJT_744);
   __sJT_st.setString(7,__sJT_745);
   __sJT_st.setString(8,__sJT_746);
   __sJT_st.setString(9,__sJT_747);
   __sJT_st.setInt(10,__sJT_748);
   __sJT_st.setLong(11,primaryKey);
   __sJT_st.setInt(12,i);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1434^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitChargeData(" + primaryKey + ")", e.toString());
      addError("submitChargeData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void submitAccountData(long primaryKey)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1454^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchbank
//          set     merchbank_transit_route_num = :transitRoute,
//                  merchbank_acct_num = :checkActNum
//          where   app_seq_num = :primaryKey and
//                  merchbank_acct_srnum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchbank\n        set     merchbank_transit_route_num =  :1 ,\n                merchbank_acct_num =  :2 \n        where   app_seq_num =  :3  and\n                merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,transitRoute);
   __sJT_st.setString(2,checkActNum);
   __sJT_st.setLong(3,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1461^7*/
    }
    catch(Exception e)
    {
      logEntry("submitAccountData(" + primaryKey + ")", e.toString());
      addError("submitAccountData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void submitMinDiscData(long primaryKey)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1480^7*/

//  ************************************************************
//  #sql [Ctx] { update  tranchrg
//          set     tranchrg_mmin_chrg = :minDisc
//          where   app_seq_num  = :primaryKey and
//                  cardtype_code in (1,4)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  tranchrg\n        set     tranchrg_mmin_chrg =  :1 \n        where   app_seq_num  =  :2  and\n                cardtype_code in (1,4)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,minDisc);
   __sJT_st.setLong(2,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1486^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1488^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_min_disc_charge  = :minDisc,
//                  merch_tax_disc_ind     = :discountTaxInd
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_min_disc_charge  =  :1 ,\n                merch_tax_disc_ind     =  :2 \n        where   app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.ops.ChargeRecordsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,minDisc);
   __sJT_st.setString(2,discountTaxInd);
   __sJT_st.setLong(3,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1494^7*/
    }
    catch(Exception e)
    {
      logEntry("submitMinDiscData(" + primaryKey + ")", e.toString());
      addError("submitMinDiscData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }


  private boolean isChecked(int idx, String str)
  {
    boolean result = false;
    try
    {
      char ch = Character.toLowerCase(str.charAt(idx));
      if( ch == 'y')
      {
        result = true;
      }
    }
    catch(Exception e)
    {}
    return result;
  }

  public int getNumAppChrgs()
  {
    return this.appChargeRecs.size();
  }

  public Vector getChargeRecs()
  {
    return this.chargeRecs;
  }
  public void setNumRecords(String numRecords)
  {
    try
    {
      setNumRecords(Integer.parseInt(numRecords));
    }
    catch(Exception e)
    {
    }
  }
  public void setNumRecords(int numRecords)
  {
    this.numRecords = numRecords;
  }
  public String getChargeDesc(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeDesc());
  }
  public String getChargeType(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeType());
  }
  public String getChargeStart(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeStart());
  }
  public String getChargeExpire(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeExpire());
  }
  public boolean getChargeFreq(int idx, int pos)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(isChecked(pos,charge.getChargeFreq()));
  }
  public String getChargeFreq(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeFreq());
  }
  public String getChargeNum(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeNum());
  }
  public String getChargeMethod(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeMethod());
  }
  public String getChargeAmt(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeAmt());
  }
  public String getChargeTax(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeTax());
  }

  public int getChargeCode(int idx)
  {
    ChargeRec charge = (ChargeRec)chargeRecs.elementAt(idx);
    return(charge.getChargeCode());
  }

  public String getAppChargeDesc(int idx)
  {
    ChargeRec charge = (ChargeRec)appChargeRecs.elementAt(idx);
    return(charge.getChargeDesc());
  }
  public String getAppChargeNum(int idx)
  {
    ChargeRec charge = (ChargeRec)appChargeRecs.elementAt(idx);
    return(charge.getChargeNum());
  }
  public String getAppChargeAmt(int idx)
  {
    ChargeRec charge = (ChargeRec)appChargeRecs.elementAt(idx);
    return(charge.getChargeAmt());
  }

  public String getEquipmentNotes()
  {
    return this.equipmentNotes;
  }
  public String getPricingNotes()
  {
    return this.pricingNotes;
  }



  public boolean isRecalculated()
  {
    return this.addNew;
  }

  public void setAddNew(String temp)
  {
    this.addNew = true;
  }

  public String getTransitRoute()
  {
    return this.transitRoute;
  }
  public String getCheckActNum()
  {
    return this.checkActNum;
  }

  public String getDiscountTaxInd()
  {
    return this.discountTaxInd;
  }

  public void setDiscountTaxInd(String taxInd)
  {
    this.discountTaxInd = taxInd;
  }

  public String getMinDisc()
  {
    return this.minDisc;
  }

  public void setTransitRoute(String transitRoute)
  {
    this.transitRoute =  transitRoute;
  }
  public void setCheckActNum(String checkActNum)
  {
    this.checkActNum = checkActNum;
  }
  public void setMinDisc(String minDisc)
  {
    this.minDisc = minDisc;
  }

  public class ChargeRec
  {
    private String chargeDesc     = "";
    private String chargeType     = "";
    private String chargeStart    = "";
    private String chargeExpire   = "";
    private String chargeFreq     = "";
    private String chargeNum      = "";
    private String chargeMethod   = "";
    private String chargeAmt      = "";
    private String chargeTax      = "";
    private int    chargeCode     = -1;

    ChargeRec()
    {}

    ChargeRec(String freq)
    {
      chargeFreq    = freq;
    }

    public void setChargeDesc(String chargeDesc)
    {
      this.chargeDesc = chargeDesc;
    }
    public void setChargeType(String chargeType)
    {
      this.chargeType = chargeType;
    }
    public void setChargeStart(String chargeStart)
    {
      this.chargeStart = chargeStart;
    }
    public void setChargeExpire(String chargeExpire)
    {
      this.chargeExpire = chargeExpire;
    }
    public void setChargeFreq(String chargeFreq)
    {
      this.chargeFreq = chargeFreq;
    }
    public void setChargeNum(String chargeNum)
    {
      this.chargeNum = chargeNum;
    }
    public void setChargeAmt(String chargeAmt)
    {
      this.chargeAmt = chargeAmt;
    }
    public void setChargeTax(String chargeTax)
    {
      this.chargeTax = chargeTax;
    }
    public void setChargeMethod(String chargeMethod)
    {
      this.chargeMethod = chargeMethod;
    }
    public void setChargeCode(int chargeCode)
    {
      this.chargeCode = chargeCode;
    }

    public String getChargeDesc()
    {
      return this.chargeDesc;
    }
    public String getChargeType()
    {
      return this.chargeType;
    }
    public String getChargeStart()
    {
      return this.chargeStart;
    }
    public String getChargeExpire()
    {
      return this.chargeExpire;
    }
    public String getChargeFreq()
    {
      return this.chargeFreq;
    }
    public String getChargeNum()
    {
      return this.chargeNum;
    }
    public String getChargeAmt()
    {
      return this.chargeAmt;
    }
    public String getChargeTax()
    {
      return this.chargeTax;
    }
    public String getChargeMethod()
    {
      return this.chargeMethod;
    }
    public int getChargeCode()
    {
      return this.chargeCode;
    }

  }



}/*@lineinfo:generated-code*/