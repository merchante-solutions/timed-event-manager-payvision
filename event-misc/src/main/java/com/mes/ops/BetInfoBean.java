/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/BetInfoBean.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-12 12:14:18 -0700 (Mon, 12 Mar 2007) $
  Version            : $Revision: 13531 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

public class BetInfoBean extends ExpandDataBean
{

  private static final int OVERALL_PLAN1 = -5;
  private static final int CARTE_PLAN1   = -3;

  private static final String AMEX_PLAN       = "AM";
  private static final String DISCOVER_PLAN   = "DS";
  private static final String JCB_PLAN        = "JC";
  private static final String DINERS_PLAN     = "DC";
  private static final String CARTE_PLAN      = "CB";
  private static final String OVERALL_PLAN    = "OV";

  private static final String VOICE_MEDIA     = "VO";
  private static final String TERMINAL_MEDIA  = "TE";
  private static final String ARU_MEDIA       = "AR";
  private static final String ECR_MEDIA       = "EC";
  private static final String BATCH_MEDIA     = "BA";
  private static final String FIXED_MEDIA     = "FX";


  private String betVisa            = "";
  private String betMC              = "";
  private String betDebit           = "";
  private String betSystem          = "";
  private String indBetVisa         = "";
  private String indBetMC           = "";
  
  private int    planMesAmex        = mesConstants.APP_CT_AMEX;
  private int    planMesDiscover    = mesConstants.APP_CT_DISCOVER;
  private int    planMesJcb         = mesConstants.APP_CT_JCB;
  private int    planMesDiners      = mesConstants.APP_CT_DINERS_CLUB;
  private int    planMesCarte       = CARTE_PLAN1;
  private int    planMesOver        = OVERALL_PLAN1;
 
  private String betAmexEcr           = "";
  private String betAmexBatch         = "";
  private String betAmexTerminal      = "";
  private String betAmexFixed         = "";
  private String betAmexAru           = "";
  private String betAmexVoice         = "";
  private String betAmexTrident       = "";
  private String vendorAmex           = "";
  
  private String betDiscoverEcr       = "";
  private String betDiscoverBatch     = "";
  private String betDiscoverTerminal  = "";
  private String betDiscoverFixed     = "";
  private String betDiscoverAru       = "";
  private String betDiscoverVoice     = "";
  private String betDiscoverTrident   = "";
  private String vendorDiscover       = "";

  private String betJcbEcr            = "";
  private String betJcbBatch          = "";
  private String betJcbTerminal       = "";
  private String betJcbFixed          = "";
  private String betJcbAru            = "";
  private String betJcbVoice          = "";
  private String betJcbTrident        = "";
  private String vendorJcb            = "";

  private String betDinersEcr         = "";
  private String betDinersBatch       = "";
  private String betDinersTerminal    = "";
  private String betDinersFixed       = "";
  private String betDinersAru         = "";
  private String betDinersVoice       = "";
  private String betDinersTrident     = "";
  private String vendorDiners         = "";

  private String betCarteEcr          = "";
  private String betCarteBatch        = "";
  private String betCarteTerminal     = "";
  private String betCarteFixed        = "";
  private String betCarteAru          = "";
  private String betCarteVoice        = "";
  private String betCarteTrident      = "";
  private String vendorCarte          = "";

  private String betOverTerminal      = "";
  private String betOverAru           = "";
  private String betOverVoice         = "";
  private String betOverFixed         = "";
  private String betOverEcr           = "";
  private String betOverBatch         = "";
  private String betOverTrident       = "";
  private String vendorOver           = "";
  
  private boolean betVisaReq        = true;
  private boolean betMCReq          = true;
  private boolean betSystemReq      = true;
  private boolean betDebitReq       = false;

  private boolean betOverTerminalReq      = false;
  private boolean betOverAruReq           = false;
  private boolean betOverVoiceReq         = false;
  private boolean betOverFixedReq         = false;
  private boolean betOverEcrReq           = false;
  private boolean betOverBatchReq         = false;

  private boolean betDinersEcrReq         = false;
  private boolean betDinersBatchReq       = false;
  private boolean betDinersTerminalReq    = false;
  private boolean betDinersFixedReq       = false;
  private boolean betDinersAruReq         = false;
  private boolean betDinersVoiceReq       = false;

  private boolean betAmexEcrReq           = false;
  private boolean betAmexBatchReq         = false;
  private boolean betAmexTerminalReq      = false;
  private boolean betAmexFixedReq         = false;
  private boolean betAmexAruReq           = false;
  private boolean betAmexVoiceReq         = false;

  private boolean betJcbEcrReq            = false;
  private boolean betJcbBatchReq          = false;
  private boolean betJcbTerminalReq       = false;
  private boolean betJcbFixedReq          = false;
  private boolean betJcbAruReq            = false;
  private boolean betJcbVoiceReq          = false;

  private boolean betDiscoverEcrReq       = false;
  private boolean betDiscoverBatchReq     = false;
  private boolean betDiscoverTerminalReq  = false;
  private boolean betDiscoverFixedReq     = false;
  private boolean betDiscoverAruReq       = false;
  private boolean betDiscoverVoiceReq     = false;

  private boolean betCarteEcrReq          = false;
  private boolean betCarteBatchReq        = false;
  private boolean betCarteTerminalReq     = false;
  private boolean betCarteFixedReq        = false;
  private boolean betCarteAruReq          = false;
  private boolean betCarteVoiceReq        = false;


  private Vector  cards             = new Vector();
  
  private int     appType           = 0;
  private boolean mes               = false;
  private boolean cbt               = false;
  private boolean transcom          = false;
  private boolean ims               = false;
  private int     productType       = 0;
  private String  productTypeDesc   = "";
  private int     planType          = 0;
  private boolean interDefault      = false;
  private String  internetType      = "";

  private String debitFee           = "";
  private String chargebackFee      = "";
  private String adminFee           = "";
  private String pricingComments    = "";

  public  Vector mesNonVendor       = new Vector();
  public  Vector mesNonVendorDesc   = new Vector();
  public  Vector mesDialVendor      = new Vector();
  public  Vector mesDialVendorDesc  = new Vector();
  public  Vector mesIntVendor       = new Vector();
  public  Vector mesIntVendorDesc   = new Vector();
  
  public  Vector mesNonMedia        = new Vector();
  public  Vector mesNonMediaDesc    = new Vector();
  public  Vector mesDialMedia       = new Vector();
  public  Vector mesDialMediaDesc   = new Vector();
  public  Vector mesIntMedia        = new Vector();
  public  Vector mesIntMediaDesc    = new Vector();
  public  Vector cbtVendor          = new Vector();
  public  Vector cbtVendorDesc      = new Vector();
  public  Vector cbtOverMedia1      = new Vector();
  public  Vector cbtOverMedia1Desc  = new Vector();
  public  Vector cbtOverMedia2      = new Vector();
  public  Vector cbtOverMedia2Desc  = new Vector();
  public  Vector cbtOverMedia3      = new Vector();
  public  Vector cbtOverMedia3Desc  = new Vector();
  public  Vector cbtMedia           = new Vector();
  public  Vector cbtMediaDesc       = new Vector();


  public BetInfoBean()
  {
    //media for cbt bank cards
    cbtMedia.add("");
    cbtMediaDesc.add("select");
    cbtMedia.add("TE");
    cbtMediaDesc.add("Terminal");
    cbtMedia.add("AR");
    cbtMediaDesc.add("ARU");
    cbtMedia.add("VO");
    cbtMediaDesc.add("Voice");
    cbtMedia.add("EC");
    cbtMediaDesc.add("ECR");
    cbtMedia.add("BA");
    cbtMediaDesc.add("Batch");
    cbtMedia.add("FX");
    cbtMediaDesc.add("Fixed");

    cbtOverMedia1.add("AR");
    cbtOverMedia1Desc.add("ARU");

    cbtOverMedia2.add("TE");
    cbtOverMedia2Desc.add("Terminal");

    cbtOverMedia3.add("VO");
    cbtOverMedia3Desc.add("Voice");
    
    //vendor list for cbt
    cbtVendor.add("");
    cbtVendorDesc.add("select");
    cbtVendor.add("VP");
    cbtVendorDesc.add("Vital");
    cbtVendor.add("ND");
    cbtVendorDesc.add("NDC");
    cbtVendor.add("MC");
    cbtVendorDesc.add("MAPP");
    cbtVendor.add("TS");
    cbtVendorDesc.add("TSYS");

    
    //vendor list for non-dialpay
    mesNonVendor.add("");
    mesNonVendorDesc.add("select");
    mesNonVendor.add("VP");
    mesNonVendorDesc.add("Vital");

    //vendor list for dial pay
    mesDialVendor.add("");
    mesDialVendorDesc.add("select");
    mesDialVendor.add("VP");
    mesDialVendorDesc.add("Vital");

    //vendor list for internet
    mesIntVendor.add("");
    mesIntVendorDesc.add("select");
    mesIntVendor.add("VP");
    mesIntVendorDesc.add("Vital");
  
    //media list for non-dialpay
    mesNonMedia.add("");
    mesNonMediaDesc.add("select");
    mesNonMedia.add("EC");
    mesNonMediaDesc.add("ECR");
    mesNonMedia.add("BA");
    mesNonMediaDesc.add("Batch");
    mesNonMedia.add("TE");
    mesNonMediaDesc.add("Terminal");
    mesNonMedia.add("FX");
    mesNonMediaDesc.add("Fixed");

    //media list for dial pay
    mesDialMedia.add("");
    mesDialMediaDesc.add("select");
    mesDialMedia.add("AR");
    mesDialMediaDesc.add("ARU");
    mesDialMedia.add("VO");
    mesDialMediaDesc.add("Voice");

    //media list for internet
    mesIntMedia.add("");
    mesIntMediaDesc.add("select");
    mesIntMedia.add("TE");
    mesIntMediaDesc.add("Terminal");
  }
  
  public void getData(long primaryKey)
  {
    getBetInfo(primaryKey);
  }

  public void setPageDefaults(String primaryKey)
  {
    try
    {
      setPageDefaults(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
    }
  }
  
  public void setPageDefaults(long primaryKey)  
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try 
    {
      ps = getPreparedStatement("select app_type from application where app_seq_num = ?");
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      
      if(rs.next())
      {
        appType = rs.getInt("app_type");
      }
      
      rs.close();
      ps.close();
        
      switch(appType)
      {
        case mesConstants.APP_TYPE_CBT:
        case mesConstants.APP_TYPE_CBT_NEW:
          mes = false;
          cbt = true;
        break;
        
        case mesConstants.APP_TYPE_TRANSCOM:
          mes = true;
          transcom = true;
          break;
          
        case mesConstants.APP_TYPE_DISCOVER_IMS:
          mes = true;
          ims = true;
          break;
          
        default:
          mes = true;
          break;
      }

      //get product type and solution from application
      String temp = "";
      temp  = "select a.pos_code, a.pos_type ";
      temp += "from merch_pos b, pos_category a ";
      temp += "where b.app_seq_num = ? and b.pos_code = a.pos_code";

      ps = getPreparedStatement(temp);
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      if(rs.next())
      {
        this.productType = rs.getInt("pos_type");
        int sol          = rs.getInt("pos_code");

        if(productType == mesConstants.APP_PAYSOL_INTERNET && (sol == mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK || sol == mesConstants.APP_MPOS_VERISIGN_PAYFLOW_PRO || sol == mesConstants.APP_MPOS_CYBERCASH_IC_VERIFY || sol == mesConstants.APP_MPOS_CYBERCASH_CASH_REGISTER || sol == mesConstants.APP_MPOS_CYBERCASH_WEB_AUTHORIZE))
        {
          this.interDefault = true;
        }
        
        if(productType == mesConstants.APP_PAYSOL_INTERNET)
        {
          switch(sol)
          {
            case mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK:
              this.internetType = "VeriSign Payflow Link";
            break;
            case mesConstants.APP_MPOS_VERISIGN_PAYFLOW_PRO:
              this.internetType = "VeriSign Payflow Pro";
            break;
            //add these constants to mesConstants then replace..
            case 211:
              this.internetType = "eStorefront Shopping Cart w/Payflow Link";
            break;
            case 212:
              this.internetType = "eStorefront Builder w/Payflow Link";
            break;
            case 213:
              this.internetType = "eStorefront Builder w/Payflow Pro";
            break;
            case mesConstants.APP_MPOS_VIRTUAL_NET:
              this.internetType = "Virtual Net";
            break;
            case mesConstants.APP_MPOS_CYBERSOURCE:
              this.internetType = "CyberSource";
            break;
            case mesConstants.APP_MPOS_CYBERCASH_IC_VERIFY:
              this.internetType = "Cybercash IC Verify";
            break;
            case mesConstants.APP_MPOS_CYBERCASH_CASH_REGISTER:
              this.internetType = "Cybercash Cash Register";
            break;
            case mesConstants.APP_MPOS_CYBERCASH_WEB_AUTHORIZE:
              this.internetType = "Cybercash Web Authorize";
            break;
            default:
              this.internetType = "Other";
            break;
          }
        }
        else
        {
          this.internetType = "";
          switch(sol)
          {
            case mesConstants.APP_MPOS_DIAL_TERMINAL:
              this.productTypeDesc = "EDC";
            break;
            case mesConstants.APP_MPOS_POS_PARTNER:
              this.productTypeDesc = "Pos Partner";
            break;
            case mesConstants.APP_MPOS_POS_PARTNER_2000:
              this.productTypeDesc = "Pos Partner 2000";
            break;
            case mesConstants.APP_MPOS_OTHER_VITAL_PRODUCT:
              this.productTypeDesc = "Other Vital Product";
            break;
            case mesConstants.APP_MPOS_DIAL_PAY:
              this.productTypeDesc = "";
            break;
            default:
              this.productTypeDesc = "Other";
            break;
          }
        }
      }
      ps.close();
      rs.close();

      //get chargeback fee from application
      temp = "";
      temp  = "select * ";
      temp += "from miscchrg ";
      temp += "where app_seq_num = ? ";

      ps = getPreparedStatement(temp);
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      while(rs.next())
      {
        if(rs.getInt("misc_code") == mesConstants.APP_MISC_CHARGE_CHARGEBACK)
        {
          chargebackFee = rs.getString("misc_chrg_amount");
        }
      }
      ps.close();
      rs.close();

      //get plan type from application
      temp = "";
      temp  = "select tranchrg_discrate_type ";
      temp += "from tranchrg ";
      temp += "where app_seq_num = ? and cardtype_code = ?";

      ps = getPreparedStatement(temp);
      ps.setLong(1, primaryKey);
      ps.setInt(2, mesConstants.APP_CT_VISA);

      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.planType = rs.getInt("tranchrg_discrate_type");
      }
      
      ps.close();
      rs.close();

      //get pricing notes from application
      temp = "";
      temp  = "select siteinsp_comment ";
      temp += "from siteinspection ";
      temp += "where app_seq_num = ? ";

      ps = getPreparedStatement(temp);
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.pricingComments = rs.getString("siteinsp_comment");
      }
      
      ps.close();
      rs.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setPageDefaults: " + e.toString());
      addError("setPageDefaults: " + e.toString());
    }
  }

  private String getFeeOnApp(int cardCode, long primaryKey, String perItemFee)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            result  = perItemFee;

    try 
    {
      ps = getPreparedStatement("select tranchrg_per_tran,tranchrg_pass_thru from tranchrg where app_seq_num = ? and cardtype_code = ? ");
      ps.setLong(1, primaryKey);
      ps.setInt(2, cardCode);

      rs = ps.executeQuery();
      
      if(rs.next())
      {
        if((cardCode == mesConstants.APP_CT_VISA || cardCode == mesConstants.APP_CT_MC) && isMes())
        {
          result = rs.getString("tranchrg_pass_thru");
          if(isBlank(result))
          {
            result = perItemFee;
          }
        }
        else
        {
          result = rs.getString("tranchrg_per_tran");
          if(isBlank(result))
          {
            result = perItemFee;
          }
        }
      }
      
      if(isBlank(result))
      {
        result = "";
      }

      ps.close();
      rs.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFeeOnApp: " + e.toString());
      addError("getFeeOnApp: " + e.toString());
    }

    return result;

  }
  
  private String getPerAuth(long primaryKey, int cardCode)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try 
    {
      ps = getPreparedStatement("select tranchrg_per_auth from tranchrg where app_seq_num = ? and cardtype_code = ? ");
      ps.setLong(1, primaryKey);
      ps.setInt(2, cardCode);

      rs = ps.executeQuery();
      
      if(rs.next())
      {
        result = rs.getString("tranchrg_per_auth");
        if(isBlank(result))
        {
          result = "";
        }
      }

      ps.close();
      rs.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPerAuth: " + e.toString());
      addError("getPerAuth: " + e.toString());
    }

    return result;
  }



  public void getCardInfo(long primaryKey)  
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try 
    {

      ps = getPreparedStatement("select * from billcard where app_seq_num = ? order by billcard_sr_number");
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      
      while(rs.next())
      {
        CardPlan card = new CardPlan(); 
        card.setCardType(rs.getInt("cardtype_code"));
        card.setCardDiscRate( (isBlank(rs.getString("billcard_disc_rate"))      ? "" : rs.getString("billcard_disc_rate")) );
        card.setCardPerItem( getFeeOnApp(rs.getInt("cardtype_code"), primaryKey, rs.getString("billcard_peritem_fee")) );
        card.setCardPerAuth( getPerAuth(primaryKey, rs.getInt("cardtype_code")) );

        if(rs.getInt("cardtype_code") == mesConstants.APP_CT_DEBIT)
        {
          this.debitFee = getFeeOnApp(rs.getInt("cardtype_code"), primaryKey, rs.getString("billcard_peritem_fee"));
        }

        cards.add(card);

        if(productType == mesConstants.APP_PAYSOL_INTERNET)
        {
          betOverFixedReq         = true;
        }
        else if(this.productType == mesConstants.APP_PAYSOL_DIAL_PAY)
        {
          betOverAruReq           = true;
          betOverVoiceReq         = true;
        }
        else
        {
          betOverTerminalReq      = true;
          betOverAruReq           = true;
          betOverVoiceReq         = true;
          betOverFixedReq         = true;
          betOverEcrReq           = true;
          betOverBatchReq         = true;
        }
        
        //this is always required i think.. so we always set it to vital
        vendorOver          = isBlank(vendorOver) ? "VP" : vendorOver;

        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_DINERS_CLUB:
            if(this.productType == mesConstants.APP_PAYSOL_DIAL_PAY)
            {
              betDinersAruReq         = true;
              betDinersVoiceReq       = true;
            }
            else
            {
              betDinersAruReq         = true;
              betDinersVoiceReq       = true;
              betDinersEcrReq         = true;
              betDinersBatchReq       = true;
              betDinersTerminalReq    = true;
              betDinersFixedReq       = true;
            }
            vendorDiners        = isBlank(vendorDiners)         ? "VP" : vendorDiners;
            break;

          case mesConstants.APP_CT_DISCOVER:
            if(this.productType == mesConstants.APP_PAYSOL_DIAL_PAY)
            {
              betDiscoverAruReq         = true;
              betDiscoverVoiceReq       = true;
            }
            else
            {
              betDiscoverAruReq         = true;
              betDiscoverVoiceReq       = true;
              betDiscoverEcrReq         = true;
              betDiscoverBatchReq       = true;
              betDiscoverTerminalReq    = true;
              betDiscoverFixedReq       = true;
            }
          
            vendorDiscover          = isBlank(vendorDiscover)       ? "VP" : vendorDiscover;
            break;

          case mesConstants.APP_CT_JCB:
            if(this.productType == mesConstants.APP_PAYSOL_DIAL_PAY)
            {
              betJcbAruReq         = true;
              betJcbVoiceReq       = true;
            }
            else
            {
              betJcbAruReq         = true;
              betJcbVoiceReq       = true;
              betJcbEcrReq         = true;
              betJcbBatchReq       = true;
              betJcbTerminalReq    = true;
              betJcbFixedReq       = true;
            }
          
            vendorJcb          = isBlank(vendorJcb)       ? "VP" : vendorJcb;
            break;

          case mesConstants.APP_CT_AMEX:
            if(this.productType == mesConstants.APP_PAYSOL_DIAL_PAY)
            {
              betAmexAruReq         = true;
              betAmexVoiceReq       = true;
            }
            else
            {
              betAmexAruReq         = true;
              betAmexVoiceReq       = true;
              betAmexEcrReq         = true;
              betAmexBatchReq       = true;
              betAmexTerminalReq    = true;
              betAmexFixedReq       = true;
            }
          
            vendorAmex          = isBlank(vendorAmex)       ? "VP" : vendorAmex;
            break;

          case mesConstants.APP_CT_DEBIT:
            betDebitReq           = true;
            break;
        }
      }

      if(!isMes())
      {
        getOtherCbtCards(primaryKey);
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCardInfo: " + e.toString());
      addError("getCardInfo: " + e.toString());
    }
  }
  

  public void getOtherCbtCards(long primaryKey)  
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try 
    {

      ps = getPreparedStatement("select * from tranchrg where app_seq_num = ?");
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      if(rs.next())
      {
        do
        {
          CardPlan card = new CardPlan(); 

          card.setCardType(rs.getInt("cardtype_code"));
          card.setCardDiscRate("");
          card.setCardPerItem((isBlank(rs.getString("tranchrg_per_tran"))      ? "" : rs.getString("tranchrg_per_tran")));
          card.setCardPerAuth((isBlank(rs.getString("tranchrg_per_auth"))      ? "" : rs.getString("tranchrg_per_auth")));

          switch(rs.getInt("cardtype_code"))
          {
            case mesConstants.APP_CT_DINERS_CLUB:
              if(this.productType == mesConstants.APP_PAYSOL_DIAL_PAY)
              {
                betDinersAruReq         = true;
                betDinersVoiceReq       = true;
              }
              else
              {
                betDinersAruReq         = true;
                betDinersVoiceReq       = true;
                betDinersEcrReq         = true;
                betDinersBatchReq       = true;
                betDinersTerminalReq    = true;
                betDinersFixedReq       = true;
              }
              cards.add(card);
              break;

            case mesConstants.APP_CT_DISCOVER:
              if(this.productType == mesConstants.APP_PAYSOL_DIAL_PAY)
              {
                betDiscoverAruReq         = true;
                betDiscoverVoiceReq       = true;
              }
              else
              {
                betDiscoverAruReq         = true;
                betDiscoverVoiceReq       = true;
                betDiscoverEcrReq         = true;
                betDiscoverBatchReq       = true;
                betDiscoverTerminalReq    = true;
                betDiscoverFixedReq       = true;
              }
              cards.add(card);
              break;
            case mesConstants.APP_CT_JCB:
              if(this.productType == mesConstants.APP_PAYSOL_DIAL_PAY)
              {
                betJcbAruReq         = true;
                betJcbVoiceReq       = true;
              }
              else
              {
                betJcbAruReq         = true;
                betJcbVoiceReq       = true;
                betJcbEcrReq         = true;
                betJcbBatchReq       = true;
                betJcbTerminalReq    = true;
                betJcbFixedReq       = true;
              }
              cards.add(card);
              break;
            case mesConstants.APP_CT_AMEX:
              if(this.productType == mesConstants.APP_PAYSOL_DIAL_PAY)
              {
                betAmexAruReq         = true;
                betAmexVoiceReq       = true;
              }
              else
              {
                betAmexAruReq         = true;
                betAmexVoiceReq       = true;
                betAmexEcrReq         = true;
                betAmexBatchReq       = true;
                betAmexTerminalReq    = true;
                betAmexFixedReq       = true;
              }
              cards.add(card);
              break;
          }
        }while(rs.next());
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getOtherCbtCards: " + e.toString());
      addError("getOtherCbtCards: " + e.toString());
    }
  }

  private void getBetInfo(long primaryKey)  
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try 
    {
      ps = getPreparedStatement("select * from billcard_bet where app_seq_num = ? ");
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      if(rs.next())
      {
        betVisa         = rs.getString("interchange_bet_visa");
        betMC           = rs.getString("interchange_bet_mc");
        indBetVisa      = rs.getString("ind_card_plan_visa");
        indBetMC        = rs.getString("ind_card_plan_mc");
        betDebit        = rs.getString("debit_net_bet");
        betSystem       = rs.getString("system_generated_bet");
      }
      else if(appType == mesConstants.APP_TYPE_BCB)
      {
        //default individual plan bets
        indBetVisa      = "0014";
        indBetMC        = "0014";
      }
      else if(isIms())
      {
        if(this.planType == mesConstants.IMS_RETAIL_RESTAURANT_PLAN)
        {
          betVisa = "4070";
          betMC   = "5070";
        }
        else if(this.planType == mesConstants.IMS_MOTO_INTERNET_DIALPAY_PLAN)
        {
          betVisa = "4071";
          betMC   = "5071";
        }  
        
        if(betDebitReq)
        {
          betDebit = "0340";
        }
        
        indBetVisa      = "*";
        indBetMC        = "*";
            
      }
      else if(isMes() && isInterchangeDefault())
      {
        betVisa         = "4034";
        betMC           = "5034";
        indBetVisa      = "*";
        indBetMC        = "*";
        if(betDebitReq && !isTranscom())
        {
          betDebit = "*";
        }
      }
      else if(isMes())
      {
        betVisa         = "40";
        betMC           = "50";
        indBetVisa      = "*";
        indBetMC        = "*";
        if(betDebitReq && !isTranscom())
        {
          betDebit = "*";
        }
      }
      else if(isCbt())
      {
        betSystem = "6999";
      }

      rs.close();
      ps.close();

      ps = getPreparedStatement("select * from billcard_auth_bet where app_seq_num = ? ");
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      boolean useBetDefaults = true;
      while(rs.next())
      {
        useBetDefaults = false;
        
        if(rs.getString("vendor_id").equals("IN"))
        {
          // trident bet
          if(rs.getString("plan_type").equals(AMEX_PLAN))
          {
            betAmexTrident = rs.getString("bet");
          }
          else if(rs.getString("plan_type").equals(DISCOVER_PLAN))
          {
            betDiscoverTrident = rs.getString("bet");
          }
          else if(rs.getString("plan_type").equals(JCB_PLAN))
          {
            betJcbTrident = rs.getString("bet");
          }
          else if(rs.getString("plan_type").equals(DINERS_PLAN))
          {
            betDinersTrident = rs.getString("bet");
          }
          else if(rs.getString("plan_type").equals(CARTE_PLAN))
          {
            betCarteTrident = rs.getString("bet");
          }
          else if(rs.getString("plan_type").equals(OVERALL_PLAN))
          {
            betOverTrident = rs.getString("bet");
          }
        }
        else
        {
          if((rs.getString("plan_type")).equals(AMEX_PLAN))
          {
            vendorAmex = rs.getString("vendor_id");

            //amex ecr media
            if((rs.getString("media_type")).equals(ECR_MEDIA))
            {
              this.betAmexEcr = rs.getString("bet");
            }
          
            //amex batch media
            if((rs.getString("media_type")).equals(BATCH_MEDIA))
            {
              this.betAmexBatch = rs.getString("bet");
            }
      
            //amex terminal media
            if((rs.getString("media_type")).equals(TERMINAL_MEDIA))
            {
              this.betAmexTerminal = rs.getString("bet");
            }

            //amex fixed media
            if((rs.getString("media_type")).equals(FIXED_MEDIA))
            {
              this.betAmexFixed = rs.getString("bet");
            }
      
            //amex aru media
            if((rs.getString("media_type")).equals(ARU_MEDIA))
            {
              this.betAmexAru = rs.getString("bet");
            }
      
            //amex voice media
            if((rs.getString("media_type")).equals(VOICE_MEDIA))
            {
              this.betAmexVoice = rs.getString("bet");
            }
          }
          else if((rs.getString("plan_type")).equals(DISCOVER_PLAN))
          {
            vendorDiscover = rs.getString("vendor_id");

            //Discover ecr media
            if((rs.getString("media_type")).equals(ECR_MEDIA))
            {
              this.betDiscoverEcr = rs.getString("bet");
            }
          
            //Discover batch media
            if((rs.getString("media_type")).equals(BATCH_MEDIA))
            {
              this.betDiscoverBatch = rs.getString("bet");
            }
      
            //Discover terminal media
            if((rs.getString("media_type")).equals(TERMINAL_MEDIA))
            {
              this.betDiscoverTerminal = rs.getString("bet");
            }

            //Discover fixed media
            if((rs.getString("media_type")).equals(FIXED_MEDIA))
            {
              this.betDiscoverFixed = rs.getString("bet");
            }
      
            //Discover aru media
            if((rs.getString("media_type")).equals(ARU_MEDIA))
            {
              this.betDiscoverAru = rs.getString("bet");
            }
      
            //Discover voice media
            if((rs.getString("media_type")).equals(VOICE_MEDIA))
            {
              this.betDiscoverVoice = rs.getString("bet");
            }
          }
          else if((rs.getString("plan_type")).equals(JCB_PLAN))
          {
            vendorJcb = rs.getString("vendor_id");

            //Jcb ecr media
            if((rs.getString("media_type")).equals(ECR_MEDIA))
            {
              this.betJcbEcr = rs.getString("bet");
            }
          
            //Jcb batch media
            if((rs.getString("media_type")).equals(BATCH_MEDIA))
            {
              this.betJcbBatch = rs.getString("bet");
            }
      
            //Jcb terminal media
            if((rs.getString("media_type")).equals(TERMINAL_MEDIA))
            {
              this.betJcbTerminal = rs.getString("bet");
            }

            //Jcb fixed media
            if((rs.getString("media_type")).equals(FIXED_MEDIA))
            {
              this.betJcbFixed = rs.getString("bet");
            }
      
            //Jcb aru media
            if((rs.getString("media_type")).equals(ARU_MEDIA))
            {
              this.betJcbAru = rs.getString("bet");
            }
      
            //Jcb voice media
            if((rs.getString("media_type")).equals(VOICE_MEDIA))
            {
              this.betJcbVoice = rs.getString("bet");
            }
          }
          else if((rs.getString("plan_type")).equals(DINERS_PLAN))
          {
            vendorDiners = rs.getString("vendor_id");

            //Diners ecr media
            if((rs.getString("media_type")).equals(ECR_MEDIA))
            {
              this.betDinersEcr = rs.getString("bet");
            }
          
            //Diners batch media
            if((rs.getString("media_type")).equals(BATCH_MEDIA))
            {
              this.betDinersBatch = rs.getString("bet");
            }
      
            //Diners terminal media
            if((rs.getString("media_type")).equals(TERMINAL_MEDIA))
            {
              this.betDinersTerminal = rs.getString("bet");
            }

            //Diners fixed media
            if((rs.getString("media_type")).equals(FIXED_MEDIA))
            {
              this.betDinersFixed = rs.getString("bet");
            }
      
            //Diners aru media
            if((rs.getString("media_type")).equals(ARU_MEDIA))
            {
              this.betDinersAru = rs.getString("bet");
            }
      
            //Diners voice media
            if((rs.getString("media_type")).equals(VOICE_MEDIA))
            {
              this.betDinersVoice = rs.getString("bet");
            }
          }
          else if((rs.getString("plan_type")).equals(CARTE_PLAN))
          {
            vendorCarte = rs.getString("vendor_id");

            //Carte ecr media
            if((rs.getString("media_type")).equals(ECR_MEDIA))
            {
              this.betCarteEcr = rs.getString("bet");
            }
          
            //Carte batch media
            if((rs.getString("media_type")).equals(BATCH_MEDIA))
            {
              this.betCarteBatch = rs.getString("bet");
            }
      
            //Carte terminal media
            if((rs.getString("media_type")).equals(TERMINAL_MEDIA))
            {
              this.betCarteTerminal = rs.getString("bet");
            }

            //Carte fixed media
            if((rs.getString("media_type")).equals(FIXED_MEDIA))
            {
              this.betCarteFixed = rs.getString("bet");
            }
      
            //Carte aru media
            if((rs.getString("media_type")).equals(ARU_MEDIA))
            {
              this.betCarteAru = rs.getString("bet");
            }
      
            //Carte voice media
            if((rs.getString("media_type")).equals(VOICE_MEDIA))
            {
              this.betCarteVoice = rs.getString("bet");
            }
          }
          else if((rs.getString("plan_type")).equals(OVERALL_PLAN))
          {
            vendorOver = rs.getString("vendor_id");

            //Overall Terminal media
            if((rs.getString("media_type")).equals(TERMINAL_MEDIA))
            {
              this.betOverTerminal    = rs.getString("bet");
              //this.vendorOverTerminal = rs.getString("vendor_id");
            }
            //Overall Aru media
            if((rs.getString("media_type")).equals(ARU_MEDIA))
            {
              this.betOverAru         = rs.getString("bet");
              //this.vendorOverAru      = rs.getString("vendor_id");
            }
            //Overall Voice media
            if((rs.getString("media_type")).equals(VOICE_MEDIA))
            {
              this.betOverVoice       = rs.getString("bet");
              //this.vendorOverVoice    = rs.getString("vendor_id");
            }
            if((rs.getString("media_type")).equals(FIXED_MEDIA))
            {
              this.betOverFixed       = rs.getString("bet");
              //this.vendorOverFixed    = rs.getString("vendor_id");
            }
            if((rs.getString("media_type")).equals(ECR_MEDIA))
            {
              this.betOverEcr         = rs.getString("bet");
              //this.vendorOverEcr      = rs.getString("vendor_id");
            }
            if((rs.getString("media_type")).equals(BATCH_MEDIA))
            {
              this.betOverBatch       = rs.getString("bet");
              //this.vendorOverBatch    = rs.getString("vendor_id");
            }
          }
        }
      }
      rs.close();
      ps.close();

      if(isIms() && useBetDefaults)
      {
        vendorOver        = "VP";
        vendorDiscover    = "VP";

        if(productType == mesConstants.APP_PAYSOL_DIAL_PAY)
        {
          this.betOverAru           = "6040";
          this.betOverVoice         = "8040";
        }
        else
        {
          this.betOverTerminal      = "7000";
          this.betOverFixed         = "9000";
          this.betOverEcr           = "3000";
          this.betOverBatch         = "2000";

          this.betDiscoverTerminal  = "7200";
          this.betDiscoverFixed     = "9200";
          this.betDiscoverEcr       = "3200";
          this.betDiscoverBatch     = "2200";
        }
      
      }


      //some variables not on form... but need to be set in backgroud...
/*
      if(this.productType == mesConstants.APP_PAYSOL_DIAL_PAY)
      {
        if(!isBlank(this.vendorOverVoice))
        {
          this.vendorOverAru = this.vendorOverVoice;
        }
        else
        {
          this.vendorOverVoice = this.vendorOverAru;
        }
      }
      else if(productType != mesConstants.APP_PAYSOL_INTERNET)
      {
        if(!isBlank(this.vendorOverVoice))
        {
          this.vendorOverTerminal = this.vendorOverVoice;
        }
        else
        {
          this.vendorOverVoice = this.vendorOverTerminal;
        }
      }
*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getBetInfo: " + e.toString());
      addError("getBetInfo: " + e.toString());
    }
  }
  
  public void submitData(HttpServletRequest req, long primaryKey)
  {

    StringBuffer      qs      = new StringBuffer();
    PreparedStatement ps      = null;

    try 
    {

      ps = getPreparedStatement("delete from billcard_bet where app_seq_num = ? ");
      ps.setLong(1, primaryKey);
      
      ps.executeUpdate();
    
      qs.append("insert into billcard_bet (           ");
      qs.append("interchange_bet_visa,                ");
      qs.append("interchange_bet_mc,                  ");
      qs.append("debit_net_bet,                       ");
      qs.append("system_generated_bet,                ");
      qs.append("ind_card_plan_visa,                  ");
      qs.append("ind_card_plan_mc,                    ");
      qs.append("app_seq_num) values (?,?,?,?,?,?,?)  ");
    
      ps = getPreparedStatement(qs.toString());

      ps.setString(1,this.betVisa);
      ps.setString(2,this.betMC);
      ps.setString(3,this.betDebit);
      ps.setString(4,this.betSystem);
      ps.setString(5,this.indBetVisa);
      ps.setString(6,this.indBetMC);
      ps.setLong(7,primaryKey);

      ps.executeUpdate();

      ps = getPreparedStatement("delete from billcard_auth_bet where app_seq_num = ? ");
      ps.setLong(1, primaryKey);
      
      ps.executeUpdate();
    
      qs.setLength(0);
      qs.append("insert into billcard_auth_bet (  ");
      qs.append("app_seq_num,                     ");
      qs.append("plan_type,                       ");
      qs.append("vendor_id,                       ");
      qs.append("num_of_bet,                      ");
      qs.append("media_type,                      ");
      qs.append("bet)                             ");
      qs.append("values (?,?,?,?,?,?)             ");
    
      ps = getPreparedStatement(qs.toString());

      int betNum = 0;

      //amex ecr media
      //if(betAmexEcrReq && !isBlank(betAmexEcr))
      if(!isBlank(betAmexEcr))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, AMEX_PLAN);     //plan type
        ps.setString(3, vendorAmex );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ECR_MEDIA);     //media type
        ps.setString(6, betAmexEcr);    //bet
        ps.executeUpdate();
      }
      //amex batch media
      //if(betAmexBatchReq && !isBlank(betAmexBatch))
      if(!isBlank(betAmexBatch))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, AMEX_PLAN);     //plan type
        ps.setString(3, vendorAmex );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, BATCH_MEDIA);     //media type
        ps.setString(6, betAmexBatch);    //bet
        ps.executeUpdate();
      }
      
      //amex terminal media
      //if(betAmexTerminalReq && !isBlank(betAmexTerminal))
      if(!isBlank(betAmexTerminal))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, AMEX_PLAN);     //plan type
        ps.setString(3, vendorAmex );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, TERMINAL_MEDIA);     //media type
        ps.setString(6, betAmexTerminal);    //bet
        ps.executeUpdate();
      }

      //amex fixed media
      //if(betAmexFixedReq && !isBlank(betAmexFixed))
      if(!isBlank(betAmexFixed))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, AMEX_PLAN);     //plan type
        ps.setString(3, vendorAmex );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, FIXED_MEDIA);     //media type
        ps.setString(6, betAmexFixed);    //bet
        ps.executeUpdate();
      }
      
      //amex aru media
      //if(betAmexAruReq && !isBlank(betAmexAru))
      if(!isBlank(betAmexAru))
      {   
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, AMEX_PLAN);     //plan type
        ps.setString(3, vendorAmex );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ARU_MEDIA);     //media type
        ps.setString(6, betAmexAru);    //bet
        ps.executeUpdate();
      }
      
      //amex voice media
      //if(betAmexVoiceReq && !isBlank(betAmexVoice))
      if(!isBlank(betAmexVoice))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, AMEX_PLAN);     //plan type
        ps.setString(3, vendorAmex );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, VOICE_MEDIA);     //media type
        ps.setString(6, betAmexVoice);    //bet
        ps.executeUpdate();
      }
      
      if(!isBlank(betAmexTrident))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, AMEX_PLAN);
        ps.setString(3, "IN");
        ps.setInt   (4, betNum);
        ps.setString(5, FIXED_MEDIA);
        ps.setString(6, betAmexTrident);
        ps.executeUpdate();
      }

      //discover ecr media
      //if(betDiscoverEcrReq && !isBlank(betDiscoverEcr))
      if(!isBlank(betDiscoverEcr))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DISCOVER_PLAN);     //plan type
        ps.setString(3, vendorDiscover );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ECR_MEDIA);     //media type
        ps.setString(6, betDiscoverEcr);    //bet
        ps.executeUpdate();
      }

      //discover batch media
      //if(betDiscoverBatchReq && !isBlank(betDiscoverBatch))
      if(!isBlank(betDiscoverBatch))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DISCOVER_PLAN);     //plan type
        ps.setString(3, vendorDiscover );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, BATCH_MEDIA);     //media type
        ps.setString(6, betDiscoverBatch);    //bet
        ps.executeUpdate();
      }
      
      //discover terminal media
      //if(betDiscoverTerminalReq && !isBlank(betDiscoverTerminal))
      if(!isBlank(betDiscoverTerminal))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DISCOVER_PLAN);     //plan type
        ps.setString(3, vendorDiscover );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, TERMINAL_MEDIA);     //media type
        ps.setString(6, betDiscoverTerminal);    //bet
        ps.executeUpdate();
      }
      
      //discover fixed media
      //if(betDiscoverFixedReq && !isBlank(betDiscoverFixed))
      if(!isBlank(betDiscoverFixed))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DISCOVER_PLAN);     //plan type
        ps.setString(3, vendorDiscover );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, FIXED_MEDIA);     //media type
        ps.setString(6, betDiscoverFixed);    //bet
        ps.executeUpdate();
      }
      
            
      //discover aru media
      //if(betDiscoverAruReq && !isBlank(betDiscoverAru))
      if(!isBlank(betDiscoverAru))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DISCOVER_PLAN);     //plan type
        ps.setString(3, vendorDiscover );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ARU_MEDIA);     //media type
        ps.setString(6, betDiscoverAru);    //bet
        ps.executeUpdate();
      }

      //discover voice media
      //if(betDiscoverVoiceReq && !isBlank(betDiscoverVoice))
      if(!isBlank(betDiscoverVoice))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DISCOVER_PLAN);     //plan type
        ps.setString(3, vendorDiscover );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, VOICE_MEDIA);     //media type
        ps.setString(6, betDiscoverVoice);    //bet
        ps.executeUpdate();
      }

      if(!isBlank(betDiscoverTrident))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DISCOVER_PLAN);
        ps.setString(3, "IN");
        ps.setInt   (4, betNum);
        ps.setString(5, FIXED_MEDIA);
        ps.setString(6, betDiscoverTrident);
        ps.executeUpdate();
      }

      //jcb ecr media
      //if(betJcbEcrReq && !isBlank(betJcbEcr))
      if(!isBlank(betJcbEcr))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, JCB_PLAN);     //plan type
        ps.setString(3, vendorJcb );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ECR_MEDIA);     //media type
        ps.setString(6, betJcbEcr);    //bet
        ps.executeUpdate();
      }

      //jcb batch media
      //if(betJcbBatchReq && !isBlank(betJcbBatch))
      if(!isBlank(betJcbBatch))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, JCB_PLAN);     //plan type
        ps.setString(3, vendorJcb);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, BATCH_MEDIA);     //media type
        ps.setString(6, betJcbBatch);    //bet
        ps.executeUpdate();
      }

      //jcb terminal media
      //if(betJcbTerminalReq && !isBlank(betJcbTerminal))
      if(!isBlank(betJcbTerminal))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, JCB_PLAN);     //plan type
        ps.setString(3, vendorJcb );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, TERMINAL_MEDIA);     //media type
        ps.setString(6, betJcbTerminal);    //bet
        ps.executeUpdate();
      }

      //jcb fixed media
      //if(betJcbFixedReq && !isBlank(betJcbFixed))
      if(!isBlank(betJcbFixed))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, JCB_PLAN);     //plan type
        ps.setString(3, vendorJcb );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, FIXED_MEDIA);     //media type
        ps.setString(6, betJcbFixed);    //bet
        ps.executeUpdate();
      }

      //jcb aru media
      //if(betJcbAruReq && !isBlank(betJcbAru))
      if(!isBlank(betJcbAru))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, JCB_PLAN);     //plan type
        ps.setString(3, vendorJcb );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ARU_MEDIA);     //media type
        ps.setString(6, betJcbAru);    //bet
        ps.executeUpdate();
      }

      //jcb voice media
      //if(betJcbVoiceReq && !isBlank(betJcbVoice))
      if(!isBlank(betJcbVoice))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, JCB_PLAN);     //plan type
        ps.setString(3, vendorJcb );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, VOICE_MEDIA);     //media type
        ps.setString(6, betJcbVoice);    //bet
        ps.executeUpdate();
      }

      if(!isBlank(betJcbTrident))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, JCB_PLAN);
        ps.setString(3, "IN");
        ps.setInt   (4, betNum);
        ps.setString(5, FIXED_MEDIA);
        ps.setString(6, betJcbTrident);
        ps.executeUpdate();
      }

      //diners ecr media
      //if(betDinersEcrReq && !isBlank(betDinersEcr))
      if(!isBlank(betDinersEcr))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DINERS_PLAN);     //plan type
        ps.setString(3, vendorDiners);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ECR_MEDIA);     //media type
        ps.setString(6, betDinersEcr);    //bet
        ps.executeUpdate();
      }
      
      //diners batch media
      //if(betDinersBatchReq && !isBlank(betDinersBatch))
      if(!isBlank(betDinersBatch))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DINERS_PLAN);     //plan type
        ps.setString(3, vendorDiners);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, BATCH_MEDIA);     //media type
        ps.setString(6, betDinersBatch);    //bet
        ps.executeUpdate();
      }
      
      //diners terminal media
      //if(betDinersTerminalReq && !isBlank(betDinersTerminal))
      if(!isBlank(betDinersTerminal))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DINERS_PLAN);     //plan type
        ps.setString(3, vendorDiners);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, TERMINAL_MEDIA);     //media type
        ps.setString(6, betDinersTerminal);    //bet
        ps.executeUpdate();
      }
      
      //diners fixed media
      //if(betDinersFixedReq && !isBlank(betDinersFixed))
      if(!isBlank(betDinersFixed))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DINERS_PLAN);     //plan type
        ps.setString(3, vendorDiners);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, FIXED_MEDIA);     //media type
        ps.setString(6, betDinersFixed);    //bet
        ps.executeUpdate();
      }

      //diners aru media
      //if(betDinersAruReq && !isBlank(betDinersAru))
      if(!isBlank(betDinersAru))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DINERS_PLAN);     //plan type
        ps.setString(3, vendorDiners);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ARU_MEDIA);     //media type
        ps.setString(6, betDinersAru);    //bet
        ps.executeUpdate();
      }

      //diners voice media
      //if(betDinersVoiceReq && !isBlank(betDinersVoice))
      if(!isBlank(betDinersVoice))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DINERS_PLAN);     //plan type
        ps.setString(3, vendorDiners);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, VOICE_MEDIA);     //media type
        ps.setString(6, betDinersVoice);    //bet
        ps.executeUpdate();
      }

      if(!isBlank(betDinersTrident))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, DINERS_PLAN);
        ps.setString(3, "IN");
        ps.setInt   (4, betNum);
        ps.setString(5, FIXED_MEDIA);
        ps.setString(6, betDinersTrident);
        ps.executeUpdate();
      }

      //carte ecr media
      //if(betCarteEcrReq && !isBlank(betCarteEcr))
      if(!isBlank(betCarteEcr))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, CARTE_PLAN);     //plan type
        ps.setString(3, vendorCarte );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ECR_MEDIA);     //media type
        ps.setString(6, betCarteEcr);    //bet
        ps.executeUpdate();
      }
      
      //carte batch media
      //if(betCarteBatchReq && !isBlank(betCarteBatch))
      if(!isBlank(betCarteBatch))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, CARTE_PLAN);     //plan type
        ps.setString(3, vendorCarte);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, BATCH_MEDIA);     //media type
        ps.setString(6, betCarteBatch);    //bet
        ps.executeUpdate();
      }
      
      //carte terminal media
      //if(betCarteTerminalReq && !isBlank(betCarteTerminal))
      if(!isBlank(betCarteTerminal))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, CARTE_PLAN);     //plan type
        ps.setString(3, vendorCarte );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, TERMINAL_MEDIA);     //media type
        ps.setString(6, betCarteTerminal);    //bet
        ps.executeUpdate();
      }
      
      //carte fixed media
      //if(betCarteFixedReq && !isBlank(betCarteFixed))
      if(!isBlank(betCarteFixed))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, CARTE_PLAN);     //plan type
        ps.setString(3, vendorCarte );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, FIXED_MEDIA);     //media type
        ps.setString(6, betCarteFixed);    //bet
        ps.executeUpdate();
      }

      //carte aru media
      //if(betCarteAruReq && !isBlank(betCarteAru))
      if(!isBlank(betCarteAru))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, CARTE_PLAN);     //plan type
        ps.setString(3, vendorCarte );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ARU_MEDIA);     //media type
        ps.setString(6, betCarteAru);    //bet
        ps.executeUpdate();
      }
      
      //carte voice media
      //if(betCarteVoiceReq && !isBlank(betCarteVoice))
      if(!isBlank(betCarteVoice))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, CARTE_PLAN);     //plan type
        ps.setString(3, vendorCarte );   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, VOICE_MEDIA);     //media type
        ps.setString(6, betJcbVoice);    //bet
        ps.executeUpdate();
      }
      
      if(!isBlank(betCarteTrident))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, CARTE_PLAN);
        ps.setString(3, "IN");
        ps.setInt   (4, betNum);
        ps.setString(5, FIXED_MEDIA);
        ps.setString(6, betCarteTrident);
        ps.executeUpdate();
      }

      //overall terminal media
      //if(betOverTerminalReq && !isBlank(betOverTerminal))
      if(!isBlank(betOverTerminal))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, OVERALL_PLAN);     //plan type
        ps.setString(3, vendorOver);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, TERMINAL_MEDIA);     //media type
        ps.setString(6, betOverTerminal);    //bet
        ps.executeUpdate();
      }

      //overall aru media
      //if(betOverAruReq && !isBlank(betOverAru))
      if(!isBlank(betOverAru))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, OVERALL_PLAN);     //plan type
        ps.setString(3, vendorOver);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ARU_MEDIA);     //media type
        ps.setString(6, betOverAru);    //bet
        ps.executeUpdate();
      }

      //overall voice media
      //if(betOverVoiceReq  && !isBlank(betOverVoice))
      if(!isBlank(betOverVoice))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, OVERALL_PLAN);     //plan type
        ps.setString(3, vendorOver);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, VOICE_MEDIA);     //media type
        ps.setString(6, betOverVoice);    //bet
        ps.executeUpdate();
      }

      //overall fixed media
      //if(betOverFixedReq  && !isBlank(betOverFixed))
      if(!isBlank(betOverFixed))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, OVERALL_PLAN);     //plan type
        ps.setString(3, vendorOver);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, FIXED_MEDIA);     //media type
        ps.setString(6, betOverFixed);    //bet
        ps.executeUpdate();
      }

      //overall ecr media
      //if(betOverEcrReq  && !isBlank(betOverEcr))
      if(!isBlank(betOverEcr))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, OVERALL_PLAN);     //plan type
        ps.setString(3, vendorOver);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, ECR_MEDIA);     //media type
        ps.setString(6, betOverEcr);    //bet
        ps.executeUpdate();
      }

      //overall batch media
      //if(betOverBatchReq  && !isBlank(betOverBatch))
      if(!isBlank(betOverBatch))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, OVERALL_PLAN);     //plan type
        ps.setString(3, vendorOver);   //vendor id
        ps.setInt   (4, betNum);        //num of bet
        ps.setString(5, BATCH_MEDIA);     //media type
        ps.setString(6, betOverBatch);    //bet
        ps.executeUpdate();
      }

      if(!isBlank(betOverTrident))
      {
        betNum++;
        ps.setLong  (1, primaryKey);
        ps.setString(2, OVERALL_PLAN);
        ps.setString(3, "IN");
        ps.setInt   (4, betNum);
        ps.setString(5, FIXED_MEDIA);
        ps.setString(6, betOverTrident);
        ps.executeUpdate();
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
      addError("submitData: " + e.toString());
    }
  }
  
  private void checkValidate(boolean req, String bet, String card)
  {
    int tempInt = 0;

    if(req)
    {
      if(!bet.equals("*"))
      {
        try
        {
          tempInt = Integer.parseInt(bet);  
          if(tempInt < 0 || tempInt >= 10000)
          {
            addError("BET # must be between 0 and 10000");
          }
        }
        catch(Exception e)
        {
          addError("Please enter a valid BET # for " + card);
        }
      }
    }
    else if(!isBlank(bet))
    {
      addError(card + " was not selected on the Merchant Discount Page");
    }      
  }
  
/********************  old checkvalidate 
  private void checkValidate(boolean req, String bet, String vendor, String card)
  {
    int tempInt = 0;

    if(req)
    {
      if(isBlank(vendor))
      {
        addError("Please select a vendor for " + card);
      }
      
      if(isBlank(bet))
      {
        addError("Please enter a valid BET # for " + card);
      }
      else if(!bet.equals("*"))
      {
        try
        {
          tempInt = Integer.parseInt(bet);  
          if(tempInt < 0 || tempInt >= 10000)
          {
            addError("BET # must be between 0 and 10000");
          }
        }
        catch(Exception e)
        {
          addError("Please enter a valid BET # for " + card);
        }
      }
    }
    else if(!isBlank(bet))
    {
      addError(card + " was not selected on the Merchant Discount Page");
    }      
  }
****************************/


  private void checkValidate(boolean req, String bet, String vendor, String card)
  {
    int tempInt = 0;

    if(!isBlank(bet) && isBlank(vendor))
    {
      addError("Please select a vendor for " + card);
    }

    if(!isBlank(bet) && !bet.equals("*"))
    {
      try
      {
        tempInt = Integer.parseInt(bet);  
        if(tempInt < 0 || tempInt >= 10000)
        {
          addError("BET # must be between 0 and 10000");
        }
      }
      catch(Exception e)
      {
        addError("Please enter a valid BET # for " + card);
      }
    }

  }


  private void checkValidateCbt(boolean req, String bet, String vendor, String card)
  {
    int tempInt = 0;

    if(!req && !isBlank(bet))
    {
      addError(card + " was not selected on the Merchant Discount Page");
    }
    else
    {
      if(isBlank(vendor) && !isBlank(bet))
      {
        addError("Please select a vendor for " + card);
      }
      else if(!isBlank(bet) && !bet.equals("*"))
      {
        try
        {
          tempInt = Integer.parseInt(bet);  
          if(tempInt < 0 || tempInt >= 10000)
          {
            addError("BET # must be between 0 and 10000");
          }
          if(isBlank(vendor))
          {
            addError("Please select a vendor for " + card);
          }
        }
        catch(Exception e)
        {
          addError("Please enter a valid BET # for " + card);
        }
      
      }
    } 
  }
  
  public boolean validate()
  {
    
    //always required
    checkValidate(betVisaReq,   betVisa,    "Visa Interchange");    
    checkValidate(betMCReq,     betMC,      "MC Interchange");    
    checkValidate(betVisaReq,   indBetVisa, "Visa Individual Card Plan");    
    checkValidate(betMCReq,     indBetMC,   "MC Individual Card Plan");    
    checkValidate(betSystemReq, betSystem,  "System Generated");    
    //not always required
    checkValidate(betDebitReq,  betDebit,   "Debit Networks");    

    checkValidate(betOverTerminalReq,     betOverTerminal,      vendorOver,           "Overall Terminal BET");    
    checkValidate(betOverAruReq,          betOverAru,           vendorOver,           "Overall ARU BET");    
    checkValidate(betOverVoiceReq,        betOverVoice,         vendorOver,           "Overall Voice BET");    
    checkValidate(betOverFixedReq,        betOverFixed,         vendorOver,           "Overall Fixed BET");    
    checkValidate(betOverEcrReq,          betOverEcr,           vendorOver,           "Overall ECR BET");    
    checkValidate(betOverBatchReq,        betOverBatch,         vendorOver,           "Overall Batch BET");    

    checkValidate(betAmexEcrReq,          betAmexEcr,           vendorAmex,           "Amex ECR BET");    
    checkValidate(betAmexBatchReq,        betAmexBatch,         vendorAmex,           "Amex Batch BET");    
    checkValidate(betAmexTerminalReq,     betAmexTerminal,      vendorAmex,           "Amex Termianl BET");    
    checkValidate(betAmexFixedReq,        betAmexFixed,         vendorAmex,           "Amex Fixed BET");    
    checkValidate(betAmexAruReq,          betAmexAru,           vendorAmex,           "Amex ARU BET");    
    checkValidate(betAmexVoiceReq,        betAmexVoice,         vendorAmex,           "Amex Voice BET");    
    
    checkValidate(betDiscoverEcrReq,      betDiscoverEcr,       vendorDiscover,       "Discover ECR BET");    
    checkValidate(betDiscoverBatchReq,    betDiscoverBatch,     vendorDiscover,       "Discover Batch BET");    
    checkValidate(betDiscoverTerminalReq, betDiscoverTerminal,  vendorDiscover,       "Discover Termianl BET");    
    checkValidate(betDiscoverFixedReq,    betDiscoverFixed,     vendorDiscover,       "Discover Fixed BET");    
    checkValidate(betDiscoverAruReq,      betDiscoverAru,       vendorDiscover,       "Discover ARU BET");    
    checkValidate(betDiscoverVoiceReq,    betDiscoverVoice,     vendorDiscover,       "Discover Voice BET");    
    
    checkValidate(betJcbEcrReq,           betJcbEcr,            vendorJcb,            "JCB ECR BET");    
    checkValidate(betJcbBatchReq,         betJcbBatch,          vendorJcb,            "JCB Batch BET");    
    checkValidate(betJcbTerminalReq,      betJcbTerminal,       vendorJcb,            "JCB Termianl BET");    
    checkValidate(betJcbFixedReq,         betJcbFixed,          vendorJcb,            "JCB Fixed BET");    
    checkValidate(betJcbAruReq,           betJcbAru,            vendorJcb,            "JCB ARU BET");    
    checkValidate(betJcbVoiceReq,         betJcbVoice,          vendorJcb,            "JCB Voice BET");    
    
    checkValidate(betDinersEcrReq,        betDinersEcr,         vendorDiners,         "Diners ECR BET");    
    checkValidate(betDinersBatchReq,      betDinersBatch,       vendorDiners,         "Diners Batch BET");    
    checkValidate(betDinersTerminalReq,   betDinersTerminal,    vendorDiners,         "Diners Termianl BET");    
    checkValidate(betDinersFixedReq,      betDinersFixed,       vendorDiners,         "Diners Fixed BET");    
    checkValidate(betDinersAruReq,        betDinersAru,         vendorDiners,         "Diners ARU BET");    
    checkValidate(betDinersVoiceReq,      betDinersVoice,       vendorDiners,         "Diners Voice BET");    
    
    checkValidate(betCarteEcrReq,         betCarteEcr,          vendorCarte,          "Carte ECR BET");    
    checkValidate(betCarteBatchReq,       betCarteBatch,        vendorCarte,          "Carte Batch BET");    
    checkValidate(betCarteTerminalReq,    betCarteTerminal,     vendorCarte,          "Carte Termianl BET");    
    checkValidate(betCarteFixedReq,       betCarteFixed,        vendorCarte,          "Carte Fixed BET");    
    checkValidate(betCarteAruReq,         betCarteAru,          vendorCarte,          "Carte ARU BET");    
    checkValidate(betCarteVoiceReq,       betCarteVoice,        vendorCarte,          "Carte Voice BET");    

    return(! hasErrors());
  }

  public void setBetVisa(String betVisa)
  {
    this.betVisa = betVisa.trim();
  }
  public void setBetMc(String betMC)
  {
    this.betMC = betMC.trim();
  }
  public void setIndBetVisa(String indBetVisa)
  {
    this.indBetVisa = indBetVisa.trim();
  }
  public void setIndBetMc(String indBetMC)
  {
    this.indBetMC = indBetMC.trim();
  }
  public void setBetDebit(String betDebit)
  {
    this.betDebit = betDebit.trim();
  }
  public void setBetSystem(String betSystem)
  {
    this.betSystem = betSystem.trim();
  }

  public String getBetVisa()
  {
    return this.betVisa;
  }
  public String getBetMc()
  {
    return this.betMC;
  }
  public String getIndBetVisa()
  {
    return this.indBetVisa;
  }
  public String getIndBetMc()
  {
    return this.indBetMC;
  }
  public String getBetDebit()
  {
    return (this.betDebit == null ? "" : this.betDebit);
  }
  public String getBetSystem()
  {
    return this.betSystem;
  }


  public String getBetAmexEcr()
  {
    return this.betAmexEcr;
  }
  public String getBetAmexBatch()
  {
    return this.betAmexBatch;
  }
  public String getBetAmexTerminal()
  {
    return this.betAmexTerminal;
  }
  public String getBetAmexFixed()
  {
    return this.betAmexFixed;
  }
  public String getBetAmexAru()
  {
    return this.betAmexAru;
  }
  public String getBetAmexVoice()
  {
    return this.betAmexVoice;
  }
  public String getBetAmexTrident()
  {
    return this.betAmexTrident;
  }

  public String getBetDiscoverEcr()
  {
    return this.betDiscoverEcr;
  }
  public String getBetDiscoverBatch()
  {
    return this.betDiscoverBatch;
  }
  public String getBetDiscoverTerminal()
  {
    return this.betDiscoverTerminal;
  }
  public String getBetDiscoverFixed()
  {
    return this.betDiscoverFixed;
  }
  public String getBetDiscoverAru()
  {
    return this.betDiscoverAru;
  }
  public String getBetDiscoverVoice()
  {
    return this.betDiscoverVoice;
  }
  public String getBetDiscoverTrident()
  {
    return this.betDiscoverTrident;
  }

  public String getBetJcbEcr()
  {
    return this.betJcbEcr;
  }
  public String getBetJcbBatch()
  {
    return this.betJcbBatch;
  }
  public String getBetJcbTerminal()
  {
    return this.betJcbTerminal;
  }
  public String getBetJcbFixed()
  {
    return this.betJcbFixed;
  }
  public String getBetJcbAru()
  {
    return this.betJcbAru;
  }
  public String getBetJcbVoice()
  {
    return this.betJcbVoice;
  }
  public String getBetJcbTrident()
  {
    return this.betJcbTrident;
  }
  
  public String getBetDinersEcr()
  {
    return this.betDinersEcr;
  }
  public String getBetDinersBatch()
  {
    return this.betDinersBatch;
  }
  public String getBetDinersTerminal()
  {
    return this.betDinersTerminal;
  }
  public String getBetDinersFixed()
  {
    return this.betDinersFixed;
  }
  public String getBetDinersAru()
  {
    return this.betDinersAru;
  }
  public String getBetDinersVoice()
  {
    return this.betDinersVoice;
  }
  public String getBetDinersTrident()
  {
    return this.betDinersTrident;
  }
  
  public String getBetCarteEcr()
  {
    return this.betCarteEcr;
  }
  public String getBetCarteBatch()
  {
    return this.betCarteBatch;
  }
  public String getBetCarteTerminal()
  {
    return this.betCarteTerminal;
  }
  public String getBetCarteFixed()
  {
    return this.betCarteFixed;
  }
  public String getBetCarteAru()
  {
    return this.betCarteAru;
  }
  public String getBetCarteVoice()
  {
    return this.betCarteVoice;
  }
  public String getBetCarteTrident()
  {
    return this.betCarteTrident;
  }
 
  public String getBetOverTerminal()
  {
    return this.betOverTerminal;
  }
  public String getBetOverAru()
  {
    return this.betOverAru;
  }
  public String getBetOverVoice()
  {
    return this.betOverVoice;
  }
  public String getBetOverFixed()
  {
    return this.betOverFixed;
  }
  public String getBetOverEcr()
  {
    return this.betOverEcr;
  }
  public String getBetOverBatch()
  {
    return this.betOverBatch;
  }
  public String getBetOverTrident()
  {
    return this.betOverTrident;
  }


  public void setBetAmexEcr(String betAmexEcr)
  {
    this.betAmexEcr = betAmexEcr;
  }
  public void setBetAmexBatch(String betAmexBatch)
  {
    this.betAmexBatch = betAmexBatch;
  }
  public void setBetAmexTerminal(String betAmexTerminal)
  {
    this.betAmexTerminal = betAmexTerminal;
  }
  public void setBetAmexFixed(String betAmexFixed)
  {
    this.betAmexFixed = betAmexFixed;
  }
  public void setBetAmexAru(String betAmexAru)
  {
    this.betAmexAru = betAmexAru;
  }
  public void setBetAmexVoice(String betAmexVoice)
  {
    this.betAmexVoice = betAmexVoice;
  }
  public void setBetAmexTrident(String betAmexTrident)
  {
    this.betAmexTrident = betAmexTrident;
  }

  public void setBetDiscoverEcr(String betDiscoverEcr)
  {
    this.betDiscoverEcr = betDiscoverEcr;
  }
  public void setBetDiscoverBatch(String betDiscoverBatch)
  {
    this.betDiscoverBatch = betDiscoverBatch;
  }
  public void setBetDiscoverTerminal(String betDiscoverTerminal)
  {
    this.betDiscoverTerminal = betDiscoverTerminal;
  }
  public void setBetDiscoverFixed(String betDiscoverFixed)
  {
    this.betDiscoverFixed = betDiscoverFixed;
  }
  public void setBetDiscoverAru(String betDiscoverAru)
  {
    this.betDiscoverAru = betDiscoverAru;
  }
  public void setBetDiscoverVoice(String betDiscoverVoice)
  {
    this.betDiscoverVoice = betDiscoverVoice;
  }
  public void setBetDiscoverTrident(String betDiscoverTrident)
  {
    this.betDiscoverTrident = betDiscoverTrident;
  }

  public void setBetJcbEcr(String betJcbEcr)
  {
    this.betJcbEcr = betJcbEcr;
  }
  public void setBetJcbBatch(String betJcbBatch)
  {
    this.betJcbBatch = betJcbBatch;
  }
  public void setBetJcbTerminal(String betJcbTerminal)
  {
    this.betJcbTerminal = betJcbTerminal;
  }
  public void setBetJcbFixed(String betJcbFixed)
  {
    this.betJcbFixed = betJcbFixed;
  }
  public void setBetJcbAru(String betJcbAru)
  {
    this.betJcbAru = betJcbAru;
  }
  public void setBetJcbVoice(String betJcbVoice)
  {
    this.betJcbVoice = betJcbVoice;
  }
  public void setBetJcbTrident(String betJcbTrident)
  {
    this.betJcbTrident = betJcbTrident;
  }

  public void setBetDinersEcr(String betDinersEcr)
  {
    this.betDinersEcr = betDinersEcr;
  }
  public void setBetDinersBatch(String betDinersBatch)
  {
    this.betDinersBatch = betDinersBatch;
  }
  public void setBetDinersTerminal(String betDinersTerminal)
  {
    this.betDinersTerminal = betDinersTerminal;
  }
  public void setBetDinersFixed(String betDinersFixed)
  {
    this.betDinersFixed = betDinersFixed;
  }
  public void setBetDinersAru(String betDinersAru)
  {
    this.betDinersAru = betDinersAru;
  }
  public void setBetDinersVoice(String betDinersVoice)
  {
    this.betDinersVoice = betDinersVoice;
  }
  public void setBetDinersTrident(String betDinersTrident)
  {
    this.betDinersTrident = betDinersTrident;
  }

  public void setBetCarteEcr(String betCarteEcr)
  {
    this.betCarteEcr = betCarteEcr;
  }
  public void setBetCarteBatch(String betCarteBatch)
  {
    this.betCarteBatch = betCarteBatch;
  }
  public void setBetCarteTerminal(String betCarteTerminal)
  {
    this.betCarteTerminal = betCarteTerminal;
  }
  public void setBetCarteFixed(String betCarteFixed)
  {
    this.betCarteFixed = betCarteFixed;
  }
  public void setBetCarteAru(String betCarteAru)
  {
    this.betCarteAru = betCarteAru;
  }
  public void setBetCarteVoice(String betCarteVoice)
  {
    this.betCarteVoice = betCarteVoice;
  }
  public void setBetCarteTrident(String betCarteTrident)
  {
    this.betCarteTrident = betCarteTrident;
  }

  public void setBetOverTerminal(String betOverTerminal)
  {
    this.betOverTerminal = betOverTerminal;
  }
  public void setBetOverAru(String betOverAru)
  {
    this.betOverAru = betOverAru;
  }
  public void setBetOverVoice(String betOverVoice)
  {
    this.betOverVoice = betOverVoice;
  }
  public void setBetOverFixed(String betOverFixed)
  {
    this.betOverFixed = betOverFixed;
  }
  public void setBetOverEcr(String betOverEcr)
  {
    this.betOverEcr = betOverEcr;
  }
  public void setBetOverBatch(String betOverBatch)
  {
    this.betOverBatch = betOverBatch;
  }
  public void setBetOverTrident(String betOverTrident)
  {
    this.betOverTrident = betOverTrident;
  }

  public String getVendorAmex()
  {
    return this.vendorAmex;
  }
  public String getVendorDiscover()
  {
    return this.vendorDiscover;
  }
  public String getVendorJcb()
  {
    return this.vendorJcb;
  }
  public String getVendorDiners()
  {
    return this.vendorDiners;
  }
  public String getVendorCarte()
  {
    return this.vendorCarte;
  }
  public String getVendorOver()
  {
    return this.vendorOver;
  }

  public void setVendorAmex(String vendorAmex)
  {
    this.vendorAmex = vendorAmex;
  }
  public void setVendorDiscover(String vendorDiscover)
  {
    this.vendorDiscover = vendorDiscover;
  }
  public void setVendorJcb(String vendorJcb)
  {
    this.vendorJcb = vendorJcb;
  }
  public void setVendorDiners(String vendorDiners)
  {
    this.vendorDiners = vendorDiners;
  }
  public void setVendorCarte(String vendorCarte)
  {
    this.vendorCarte = vendorCarte;
  }
  public void setVendorOver(String vendorOver)
  {
    this.vendorOver = vendorOver;
  }

  public String getPricingComments()
  {
    return this.pricingComments;
  }
 
  public String getChargebackFee()
  {
    String result = "";
    if(isBlank(this.chargebackFee))
    {
      result = "No chargeback fee on app";
    }
    else
    {
      try
      {
        float temp = Float.parseFloat(this.chargebackFee);
        result = "Chargeback fee on app " + NumberFormat.getCurrencyInstance(Locale.US).format(temp);
      }
      catch(Exception e)
      {
        result = "No chargeback fee on app";
      }
    }
    return result;
  }

  public String getDebitFee()
  {
    String result = "";
    if(isBlank(this.debitFee))
    {
      result = "No debit fee on app";
    }
    else
    {
      try
      {
        float temp = Float.parseFloat(this.debitFee);
        result = "Debit fee on app " + NumberFormat.getCurrencyInstance(Locale.US).format(temp);
      }
      catch(Exception e)
      {
        result = "No debit fee on app";
      }
    }
    return result;
  }

  public boolean isCbt()
  {
    return this.cbt;
  }

  public boolean isMes()
  {
    return this.mes;
  }
  public boolean isIms()
  {
    return this.ims;
  }
  public boolean isTranscom()
  {
    return this.transcom;
  }

  public boolean isInterchangeDefault()
  {
    return interDefault;
  }

  public int getProductType()
  {
    return productType;
  }

  public String getProductTypeDesc()
  {
    return productTypeDesc;
  }

  public String getInternetType()
  {
    return internetType;
  }


  public String getProductName()
  {
    String result = "unknown";
    
    switch(this.productType)
    {
      case mesConstants.APP_PAYSOL_DIAL_PAY:
        result = "Dialpay";
      break;
      case mesConstants.APP_PAYSOL_INTERNET:
        result = "Internet";
      break;
      default:
        result = "Non-Dialpay";      
      break;

    }
    return result;
  }

  public String getPricingPlan(String plan)
  {
    if( !plan.equals("MC") && 
        !plan.equals("Visa") &&
        !plan.equals("MC Check") &&
        !plan.equals("MC Business") &&
        !plan.equals("Visa Check") &&
        !plan.equals("Visa Business"))
    {
      return "---";
    }

    String result = "unknown";
    
    switch(this.planType)
    {
      case mesConstants.APP_PS_VARIABLE_RATE:
        result = "Variable Rate Plan";
      break;
      case mesConstants.APP_PS_FIXED_RATE:
        result = "Fixed Rate Plan";
      break;
      case mesConstants.APP_PS_INTERCHANGE:
        result = "Interchange Plan";
      break;
      case mesConstants.APP_PS_FIXED_PLUS_PER_ITEM:
        result = "Fixed Rate + Per Item Plan";
      break;
      case mesConstants.CBT_APP_BUNDLED_RATE_PLAN:
        result = "Bundled Rate Plan";
      break;
      case mesConstants.CBT_APP_UNBUNDLED_RATE_PLAN:
        result = "Unbundled Rate Plan";
      break;
      case mesConstants.CBT_APP_INTERCHANGE_PLUS_PLAN:
        result = "Interchange Plus Plan";
      break;
      case mesConstants.CBT_APP_PERITEM_DISCRATE_PLAN:
        result = "Discount Rate & Per Item Plan";
      break;
      case mesConstants.IMS_RETAIL_RESTAURANT_PLAN:
        result = "Discover IMS Retail/Restaurant Plan";
      break;
      case mesConstants.IMS_MOTO_INTERNET_DIALPAY_PLAN:
        result = "Discover IMS MOTO/Internet/DialPay Plan";
      break;

    }
    return result;
  }


/* 

  THIS CODE IS NOT USED.. IT IS REPLACED BY THE TWO FUNCTIONS 
  THAT APPEAR BEFORE DO_SEQUENCES IN THE JSP

  public void setMes(String temp)
  {
    this.mes = true;
  }
  public void setInterDefault(String temp)
  {
    this.interDefault = true;
  }

  public void setChargebackFee(String chargebackFee)
  {
    this.chargebackFee = chargebackFee
  }

  public void setDebitFee(String debitFee)
  {
    this.debitFee = debitFee;
  }

  public void setProductType(String temp)
  {
    try
    {
      this.productType = Integer.parseInt(temp);
    }
    catch(Exception e)
    {
      this.productType = 0;
    }
  }
*/


  public int getNumCards()
  {
    return cards.size();
  }
  public String getCardDiscRate(int idx)
  {
    CardPlan tempCard = (CardPlan)cards.elementAt(idx);
    return(tempCard.getCardDiscRate());
  }
  public String getCardPerItem(int idx)
  {
    CardPlan tempCard = (CardPlan)cards.elementAt(idx);
    return(tempCard.getCardPerItem());
  }
  public String getCardPerAuth(int idx)
  {
    CardPlan tempCard = (CardPlan)cards.elementAt(idx);
    return(tempCard.getCardPerAuth());
  }
  public String getCardPlan(int idx)
  {
    CardPlan tempCard = (CardPlan)cards.elementAt(idx);
    return(tempCard.getCardPlan());
  }

  public class CardPlan
  {
    private int cardType          = 0;
    private String discRate       = "";
    private String perItem        = "";
    private String perAuth        = "";

    CardPlan()
    {}

    public void setCardDiscRate(String discRate)
    {
      this.discRate = discRate;      
    }
    public void setCardPerItem(String perItem)
    {
      this.perItem = perItem;
    }
    public void setCardPerAuth(String perAuth)
    {
      this.perAuth = perAuth;
    }
    
    public void setCardType(int cardType)
    {
      this.cardType = cardType;
    }
    
    public String getCardDiscRate()
    {
      String result = "";
      result = this.discRate;
      return result;  
    }
    public String getCardPerItem()
    {
      String result = "";
      result = this.perItem;
      return result;  
    }
    public String getCardPerAuth()
    {
      String result = "";
      result = this.perAuth;
      return result;  
    }
    public String getCardPlan()
    {
      String result = "Unknown Card Type";
      switch(this.cardType)
      {
        case mesConstants.APP_CT_VISA:
          result = "Visa";
          break;
        case mesConstants.APP_CT_VISA_CHECK_CARD:
          result = "Visa Check";
          break;
        case mesConstants.APP_CT_VISA_BUSINESS_CARD:
          result = "Visa Business";
          break;
        case mesConstants.APP_CT_MC:
          result = "MC";
          break;
        case mesConstants.APP_CT_MC_CHECK_CARD:
          result = "MC Check";
          break;
        case mesConstants.APP_CT_MC_BUSINESS_CARD:
          result = "MC Business";
          break;
        case mesConstants.APP_CT_DINERS_CLUB:
          result = "Diners";
          break;
        case mesConstants.APP_CT_DISCOVER:
          result = "Discover";
          break;
        case mesConstants.APP_CT_AMEX:
          result = "Amex";
          break;
        case mesConstants.APP_CT_JCB:
          result = "JCB";
          break;
        case mesConstants.APP_CT_DEBIT:
          result = "Debit";
          break;
        case mesConstants.APP_CT_PRIVATE_LABEL_1:
          result = "Private Label 1";
          break;
        case mesConstants.APP_CT_EBT:
          result = "EBT";
          break;
      }
      return result;
    }

  }


}







