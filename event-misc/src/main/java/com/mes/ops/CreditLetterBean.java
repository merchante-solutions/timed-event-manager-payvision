/*@lineinfo:filename=CreditLetterBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/CreditLetterBean.sqlj $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-08-15 10:56:38 -0700 (Fri, 15 Aug 2008) $
  Version            : $Revision: 15266 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;

public class CreditLetterBean extends com.mes.screens.SequenceDataBean
{
  // constants
  public static final int     EMAIL_MERCHANT      = 1;
  public static final int     EMAIL_SOURCE        = 2;
  public static final int     EMAIL_PARTY         = 4;
  
  public static final int     NOTIFY_EMAIL        = 1;
  public static final int     NOTIFY_LETTER       = 2;
  
  public static final int     PARTY_MERCHANT      = 1;
  public static final int     PARTY_SOURCE        = 2;
  public static final int     PARTY_3RD           = 3;
  
  // bean data
  private int     approvalStatus          = 0;
  private String  shortReason             = "";
  private String  posCode                 = "";
  private String  reason                  = "";
  private int     email                   = 0;
  
  private boolean sendEmail               = false;
  
  private String  letterContents          = "";
  
  // emails
  private Notification  mEmail            = null;
  private Notification  sEmail            = null;
  private Notification  pEmail            = null;
  
  // letters
  private Notification  mLetter           = null;
  private Notification  sLetter           = null;
  private Notification  pLetter           = null;
 
  private String  mEmailHeader            = "";
  private String  mEmailSubject           = "";
  private String  merchSubject            = "";
  private String  merchLName              = "";
  private String  merchName               = "";
  private String  merchEmail              = "";

  private String  merchNamecc             = "";
  private String  merchEmailcc            = "";
  private String  merchNamebcc            = "";
  private String  merchEmailbcc           = "";
  
  private long    merchNumber             = 0L;
  private String  merchAddress1           = "";
  private String  merchAddress2           = "";
  private String  merchCity               = "";
  private String  merchState              = "";
  private String  merchZip                = "";
  private boolean mComposeLetter          = false;

  private String  sEmailHeader            = "";
  private String  sEmailSubject           = "";
  private String  sourceSubject           = "";
  private String  sourceLName             = "";
  private String  sourceName              = "";
  private String  sourceEmail             = "";

  private String  sourceNamecc            = "";
  private String  sourceEmailcc           = "";
  private String  sourceNamebcc           = "";
  private String  sourceEmailbcc          = "";

  private String  sourceAddress1          = "";
  private String  sourceAddress2          = "";
  private String  sourceCity              = "";
  private String  sourceState             = "";
  private String  sourceZip               = "";
  private boolean sComposeLetter          = false;

  private String  pEmailHeader            = "";
  private String  pEmailSubject           = "";
  private String  partySubject            = "";
  private String  partyLName              = "";
  private String  partyName               = "";
  private String  partyEmail              = "";

  private String  partyNamecc             = "";
  private String  partyEmailcc            = "";
  private String  partyNamebcc            = "";
  private String  partyEmailbcc           = "";

  private String  partyAddress1           = "";
  private String  partyAddress2           = "";
  private String  partyCity               = "";
  private String  partyState              = "";
  private String  partyZip                = "";
  private boolean pComposeLetter          = false;

  // option data
  // state options
  private Vector  stateOptions            = new Vector();
  public static final String states[] = 
  {
    "select", "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA",
    "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI",
    "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY",
    "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VT",
    "WA", "WI", "WV", "WY"
  };
  
  public void loadOptionData()
  {
    for(int i=0; i < states.length; ++i)
    {
      stateOptions.add(states[i]);
    }
  }
  
  public CreditLetterBean()
  {
    loadOptionData();
    
    mEmail  = new Notification(NOTIFY_EMAIL, PARTY_MERCHANT);
    sEmail  = new Notification(NOTIFY_EMAIL, PARTY_SOURCE);
    pEmail  = new Notification(NOTIFY_EMAIL, PARTY_3RD);
    
    mLetter = new Notification(NOTIFY_LETTER, PARTY_MERCHANT);
    sLetter = new Notification(NOTIFY_LETTER, PARTY_SOURCE);
    pLetter = new Notification(NOTIFY_LETTER, PARTY_3RD);
  }
  
  public void setDefaults(String primaryKey)
  {
    try
    {
      setDefaults(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
      logEntry("setDefaults(" + primaryKey + ")", e.toString());
    }
  }
  
  public void setDefaults(long primaryKey)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    StringBuffer        letter  = new StringBuffer("");
    
    try
    {
      connect();
      
      int groupCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:190^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(group_reason)
//          
//          from    app_queue_group_letters
//          where   group_reason = :shortReason
//                  and pos_code = :posCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(group_reason)\n         \n        from    app_queue_group_letters\n        where   group_reason =  :1 \n                and pos_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.CreditLetterBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,shortReason);
   __sJT_st.setString(2,posCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   groupCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^7*/
      
      if( groupCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:201^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  letter_subject,
//                    letter_contents
//            from    app_queue_group_letters
//            where   group_reason = :shortReason
//                    and pos_code = :posCode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  letter_subject,\n                  letter_contents\n          from    app_queue_group_letters\n          where   group_reason =  :1 \n                  and pos_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.CreditLetterBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,shortReason);
   __sJT_st.setString(2,posCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.CreditLetterBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:208^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          letter.append(rs.getString("letter_contents"));
          mEmailSubject = rs.getString("letter_subject");
          sEmailSubject = rs.getString("letter_subject");
        }
      }
      else
      {
        // look for generic reason code letter data (pos_code = "any")
        /*@lineinfo:generated-code*//*@lineinfo:222^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(group_reason)
//            
//            from    app_queue_group_letters
//            where   group_reason = :shortReason
//                    and pos_code = 'any'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(group_reason)\n           \n          from    app_queue_group_letters\n          where   group_reason =  :1 \n                  and pos_code = 'any'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.CreditLetterBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,shortReason);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   groupCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:229^9*/
        
        if( groupCount > 0 )
        {
          // use product level approval letter
          /*@lineinfo:generated-code*//*@lineinfo:234^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  letter_subject,
//                      letter_contents
//              from    app_queue_group_letters
//              where   group_reason = :shortReason
//                      and pos_code = 'any'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  letter_subject,\n                    letter_contents\n            from    app_queue_group_letters\n            where   group_reason =  :1 \n                    and pos_code = 'any'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.CreditLetterBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,shortReason);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.CreditLetterBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:241^11*/
        
          rs = it.getResultSet();
        
          if(rs.next())
          {
            letter.append(rs.getString("letter_contents"));
            mEmailSubject = rs.getString("letter_subject");
            sEmailSubject = rs.getString("letter_subject");
          }
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:254^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  app.app_type            app_type,
//                      mr.merch_business_name  merchant_name,
//                      aqcl.letter_part_1      letter_part_1,
//                      aqcl.letter_part_2      letter_part_2
//              from    app_queue_credit_letters  aqcl,
//                      application app,
//                      merchant mr
//              where   app.app_seq_num = :primaryKey and
//                      app.app_seq_num = mr.app_seq_num and
//                      (app.app_type = aqcl.app_type or aqcl.app_type = 9999) and
//                      (aqcl.credit_status = :approvalStatus or aqcl.credit_status = 9999)
//              order by aqcl.app_type, aqcl.credit_status      
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app.app_type            app_type,\n                    mr.merch_business_name  merchant_name,\n                    aqcl.letter_part_1      letter_part_1,\n                    aqcl.letter_part_2      letter_part_2\n            from    app_queue_credit_letters  aqcl,\n                    application app,\n                    merchant mr\n            where   app.app_seq_num =  :1  and\n                    app.app_seq_num = mr.app_seq_num and\n                    (app.app_type = aqcl.app_type or aqcl.app_type = 9999) and\n                    (aqcl.credit_status =  :2  or aqcl.credit_status = 9999)\n            order by aqcl.app_type, aqcl.credit_status";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.CreditLetterBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,approvalStatus);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.CreditLetterBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:268^11*/
      
          rs = it.getResultSet();
      
          if(rs.next())
          {
            letter.append("Dear ");
            letter.append(rs.getString("merchant_name"));
            letter.append(":\n\n");
            letter.append(rs.getString("letter_part_1"));
        
            if(rs.getString("letter_part_2") != null && !rs.getString("letter_part_2").equals(""))
            {
              letter.append("\n\n");
              letter.append(reason);
              letter.append("\n");
              letter.append(rs.getString("letter_part_2"));
            }
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setDefaults(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    letterContents = letter.toString();
    
    if(this.email == 0)
    {
      email = EMAIL_MERCHANT;
    }
  }
  
  public void getMerchantData(long primaryKey)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:316^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_business_name,
//                  merch_email_address,
//                  merch_credit_status,
//                  merch_number
//          from    merchant
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_business_name,\n                merch_email_address,\n                merch_credit_status,\n                merch_number\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.CreditLetterBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.CreditLetterBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:324^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        this.merchName      = rs.getString("merch_business_name");
        this.mEmailSubject  = rs.getString("merch_business_name");
        this.sEmailSubject  = rs.getString("merch_business_name");
        this.merchLName     = this.merchName;
        this.merchEmail     = rs.getString("merch_email_address");
        this.merchNumber    = rs.getLong  ("merch_number");
        this.approvalStatus = rs.getInt   ("merch_credit_status");
        //this.letterContents = rs.getString("merch_notes");
      }
      else
      {
        addError("merchant not found");
      }
      
      rs.close();
      it.close();
      
      // get mailing address, or business address if no mailing address
      /*@lineinfo:generated-code*//*@lineinfo:348^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1,
//                  address_line2,
//                  address_city,
//                  countrystate_code,
//                  address_zip
//          from    address
//          where   app_seq_num = :primaryKey and
//                  addresstype_code <= 2
//          order by addresstype_code desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1,\n                address_line2,\n                address_city,\n                countrystate_code,\n                address_zip\n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code <= 2\n        order by addresstype_code desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.CreditLetterBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.CreditLetterBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:359^7*/
      
      rs = it.getResultSet();
        
      if(rs.next())
      {
        this.merchAddress1  = rs.getString("address_line1");
        this.merchAddress2  = rs.getString("address_line2");
        this.merchCity      = rs.getString("address_city");
        this.merchState     = rs.getString("countrystate_code");
        this.merchZip       = rs.getString("address_zip");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMerchantData: " + e.toString());
      addError("getMerchantData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  public void getSourceData(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:394^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.name        name,
//                  u.email       email
//          from    users         u,
//                  application   a
//          where   a.app_seq_num     = :primaryKey and
//                  a.app_user_login  = u.login_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  u.name        name,\n                u.email       email\n        from    users         u,\n                application   a\n        where   a.app_seq_num     =  :1  and\n                a.app_user_login  = u.login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.CreditLetterBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.CreditLetterBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:402^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        this.sourceName = rs.getString("name");
        if(this.sourceName == null)
        {
          this.sourceName = "";
        }
        
        this.sourceEmail  = rs.getString("email");
        
        if(sourceEmail == null)
        {
          sourceEmail = "";
        }
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getSourceData: " + e.toString());
      addError("getSourceData: " + e.toString());
    }
  }
  
  public void getPartyData(long primaryKey)
  {
    try
    {
      
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPartyData: " + e.toString());
      addError("getPartyData: " + e.toString());
    }
  }
  
  public void getApprovalData(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:452^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  aq.app_status_reason,
//                  aq.app_status,
//                  mp.pos_code
//          from    app_queue aq,
//                  merch_pos mp
//          where   aq.app_queue_type = :QueueConstants.QUEUE_CREDIT
//                  and aq.app_seq_num = :primaryKey
//                  and aq.app_seq_num = mp.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  aq.app_status_reason,\n                aq.app_status,\n                mp.pos_code\n        from    app_queue aq,\n                merch_pos mp\n        where   aq.app_queue_type =  :1 \n                and aq.app_seq_num =  :2 \n                and aq.app_seq_num = mp.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.CreditLetterBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.QUEUE_CREDIT);
   __sJT_st.setLong(2,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.CreditLetterBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:462^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        shortReason = rs.getString("app_status_reason");
        
        // manually set short reason if this is an approval
        if( rs.getString("app_status").equals("101") )
        {
          shortReason = "AP";
        }
        
        posCode     = rs.getString("pos_code");
        ReasonHelperBean rhb = new ReasonHelperBean();
        rhb.setReasons(rs.getInt("app_status"),rs.getString("app_status_reason"));
        while(rhb.hasNext())
        {
          this.reason += (" - " + rhb.getNextLongDesc() + "\n");
        }
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getApprovalData: " + e.toString());
      addError("getApprovalData: " + e.toString());
    }
  }
  
  public void getNotifyData(long primaryKey)
  {
    ResultSetIterator it          = null;
    ResultSet         rs          = null;
    Notification      curNotify   = null;
    SimpleDateFormat  simpleDate  = new SimpleDateFormat("MM/dd/yyyy");
    SimpleDateFormat  simpleTime  = new SimpleDateFormat("hh:mm:ss");
    
    boolean     wasStale          = false;
    
    try
    {
      if(isConnectionStale())
      {
        wasStale = true;
        connect();
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:513^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  notify_type,
//                  notify_party,
//                  notify_subject,
//                  notify_header,
//                  notify_body,
//                  notify_address2,
//                  notify_name,
//                  notify_namecc,
//                  notify_namebcc,
//                  notify_addresscc,
//                  notify_addressbcc,
//                  notify_date
//          from    app_notify
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  notify_type,\n                notify_party,\n                notify_subject,\n                notify_header,\n                notify_body,\n                notify_address2,\n                notify_name,\n                notify_namecc,\n                notify_namebcc,\n                notify_addresscc,\n                notify_addressbcc,\n                notify_date\n        from    app_notify\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.CreditLetterBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.ops.CreditLetterBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:529^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        int type  = rs.getInt("notify_type");
        int party = rs.getInt("notify_party");
        
        curNotify = getNotify(type, party);
        
        // fill data
        curNotify.setNotified        ( true                                         );
        curNotify.setNotifySubject   ( rs.getString("notify_subject")               );
        curNotify.setNotifyHeader    ( rs.getString("notify_header")                );
        curNotify.setNotifyBody      ( rs.getString("notify_body")                  );
        curNotify.setNotifyAddress   ( rs.getString("notify_address2")              );
        curNotify.setNotifyName      ( rs.getString("notify_name")                  );
        curNotify.setNotifyNamecc    ( rs.getString("notify_namecc")                );
        curNotify.setNotifyNamebcc   ( rs.getString("notify_namebcc")               );
        curNotify.setNotifyAddresscc ( rs.getString("notify_addresscc")             );
        curNotify.setNotifyAddressbcc( rs.getString("notify_addressbcc")            );
        curNotify.setNotifyDate      ( simpleDate.format(rs.getDate("notify_date")) );
        curNotify.setNotifyTime      ( simpleTime.format(rs.getTime("notify_date")) );
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getNotifyData: " + e.toString());
      addError("getNotifyData: " + e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
  }
  
  public void getData(long primaryKey)
  {
    try
    {
      // if the connection hasn't been created yet, create it now.  the jsp
      // will handle cleaning up the connection.
      if(isConnectionStale())
      {
        connect();
      }
      
      getMerchantData (primaryKey);
      setDefaults     (primaryKey);
      getSourceData   (primaryKey);
      getPartyData    (primaryKey);
      getApprovalData (primaryKey);
      getNotifyData   (primaryKey);
    }
    catch(Exception e)
    {
      addError("Error in getData(): " + e.toString());
      logEntry("getData(" + primaryKey + ")", e.toString());
    }
  }
  
  public boolean validate(long primaryKey)
  {
    
    if((this.email & this.EMAIL_MERCHANT) > 0 && sendEmail())
    {
      if(isBlank(mEmailSubject))
      {
        addError("A subject is needed for the merchant email");
      }
      if(isBlank(merchEmail))
      {
        addError("An email address is needed for the merchant email");
      }
      if(!isBlank(merchNamecc) && isBlank(merchEmailcc))
      {
        addError("An email address is needed for the merchant email CC");
      }
      if(!isBlank(merchNamebcc) && isBlank(merchEmailbcc))
      {
        addError("An email address is needed for the merchant email BCC");
      }
    }

    if((this.email & this.EMAIL_SOURCE) > 0 && sendEmail())
    {
      if(isBlank(sEmailSubject))
      {
        addError("A subject is needed for the source email");
      }
      if(isBlank(sourceEmail))
      {
        addError("An email address is needed for the source email");
      }
      if(!isBlank(sourceNamecc) && isBlank(sourceEmailcc))
      {
        addError("An email address is needed for the source email CC");
      }
      if(!isBlank(sourceNamebcc) && isBlank(sourceEmailbcc))
      {
        addError("An email address is needed for the source email BCC");
      }

    }
    
    if((this.email & this.EMAIL_PARTY) > 0 && sendEmail())
    {
      if(isBlank(pEmailSubject))
      {
        addError("A subject is needed for the 3rd party email");
      }
      if(isBlank(partyEmail))
      {
        addError("An email address is needed for the 3rd party email");
      }
      if(!isBlank(partyNamecc) && isBlank(partyEmailcc))
      {
        addError("An email address is needed for the 3rd party email CC");
      }
      if(!isBlank(partyNamebcc) && isBlank(partyEmailbcc))
      {
        addError("An email address is needed for the 3rd party email BCC");
      }
    }
    
    if(hasErrors())
    {
      getNotifyData(primaryKey);
    }

    return (! hasErrors());
  }
  
  public void submitMerchantData(long primaryKey)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:673^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_notes = :letterContents
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_notes =  :1 \n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.ops.CreditLetterBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,letterContents);
   __sJT_st.setLong(2,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:678^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitMerchantData: " + e.toString());
      addError("submitMerchantData: " + e.toString());
    }
  }
  
  public void submitEmailData(long primaryKey)
  {
    String            message = "";
    MailMessage       pmail   = null;
    MailMessage       mmail   = null;
    MailMessage       smail   = null;

    try
    {
      connect();
      
      int appType = 0;
      
      // get app type of this application
      /*@lineinfo:generated-code*//*@lineinfo:701^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//          
//          from    application
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.CreditLetterBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:707^7*/ 
      
      // special email
      String fromAddr = "";
      switch(appType)
      {
        case  mesConstants.APP_TYPE_VPS_ISO:
          fromAddr = "ISO-CreditInformation@Verisign.com";
          break;
          
        default:
          fromAddr = "application@merchante-solutions.com";
          break;
      }
      
      if(sendEmail())
      {
        if(((this.email & this.EMAIL_PARTY) > 0) && (!isSent(primaryKey,this.NOTIFY_EMAIL,this.EMAIL_PARTY)))
        {
          message = isBlank(this.pEmailHeader) ? this.letterContents : this.pEmailHeader + "\n\n" + this.letterContents;
          pmail = new MailMessage();
          pmail.addTo(this.partyEmail);
          if(!isBlank(this.partyEmailcc))
          {
            pmail.addCC((this.partyNamecc + "<" + this.partyEmailcc + ">"));
          }
          if(!isBlank(this.partyEmailbcc))
          {
            pmail.addBCC((this.partyNamebcc + "<" + this.partyEmailbcc + ">"));
          }
          pmail.setFrom(fromAddr);
          pmail.setSubject(this.pEmailSubject);
          pmail.setText(message);
          pmail.send();
          submitNotifyData(primaryKey, this.NOTIFY_EMAIL, this.PARTY_3RD);
        }
      
        if(((this.email & this.EMAIL_MERCHANT) > 0) && (!isSent(primaryKey,this.NOTIFY_EMAIL,this.EMAIL_MERCHANT)))
        {
          message = isBlank(this.mEmailHeader) ? this.letterContents : this.mEmailHeader + "\n\n" + this.letterContents;
          mmail = new MailMessage();
          mmail.addTo(this.merchEmail);
          if(!isBlank(this.merchEmailcc))
          {
            mmail.addCC((this.merchNamecc + "<" + this.merchEmailcc + ">"));
          }
          if(!isBlank(this.merchEmailbcc))
          {
            mmail.addBCC((this.merchNamebcc + "<" + this.merchEmailbcc + ">"));
          }
          
          mmail.setFrom(fromAddr);
          mmail.setSubject(this.mEmailSubject);
          mmail.setText(message);
          mmail.send();
          submitNotifyData(primaryKey, this.NOTIFY_EMAIL, this.PARTY_MERCHANT);
        }

        if(((this.email & this.EMAIL_SOURCE) > 0) && (!isSent(primaryKey,this.NOTIFY_EMAIL,this.EMAIL_SOURCE)))
        {
          message = isBlank(this.sEmailHeader) ? this.letterContents : this.sEmailHeader + "\n\n" + this.letterContents;
          smail = new MailMessage();
          smail.addTo(this.sourceEmail);
          if(!isBlank(this.sourceEmailcc))
          {
            smail.addCC((this.sourceNamecc + "<" + this.sourceEmailcc + ">"));
          }
          if(!isBlank(this.sourceEmailbcc))
          {
            smail.addBCC((this.sourceNamebcc + "<" + this.sourceEmailbcc + ">"));
          }
          smail.setFrom(fromAddr);        
          smail.setSubject(this.sEmailSubject);
          smail.setText(message);
          smail.send();
          submitNotifyData(primaryKey, this.NOTIFY_EMAIL, this.PARTY_SOURCE);
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitEmailData: " + e.toString());
      addError("submitEmailData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void submitPrintData(long primaryKey)
  {
    if(isMcomposed())
    {
      submitNotifyData(primaryKey, this.NOTIFY_LETTER, this.PARTY_MERCHANT);
    }
    if(isScomposed())
    {
      submitNotifyData(primaryKey, this.NOTIFY_LETTER, this.PARTY_SOURCE);
    }
    if(isPcomposed())
    {
      submitNotifyData(primaryKey, this.NOTIFY_LETTER, this.PARTY_3RD);
    }
  }
 
  public void submitNotifyData(long primaryKey, int notifyType, int notifyParty)
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    String            tempAddress = "";

    try
    {
      if(isSent(primaryKey, notifyType, notifyParty))
      {
        qs.append("update app_notify set        ");
        qs.append("notify_subject     = ?,      ");
        qs.append("notify_header      = ?,      ");
        qs.append("notify_address2    = ?,      ");
        qs.append("notify_name        = ?,      ");
        qs.append("notify_namecc      = ?,      ");
        qs.append("notify_namebcc     = ?,      ");        
        qs.append("notify_addresscc   = ?,      ");
        qs.append("notify_addressbcc  = ?,      ");        
        qs.append("notify_body        = ?,      ");
        qs.append("notify_date        = sysdate ");
        qs.append("where                        ");
        qs.append("app_seq_num        = ? and   ");
        qs.append("notify_type        = ? and   ");
        qs.append("notify_party       = ?       ");
      }
      else
      {
        qs.append("Insert into app_notify (           ");
        qs.append("notify_subject,                    ");
        qs.append("notify_header,                     ");
        qs.append("notify_address2,                   ");
        qs.append("notify_name,                       ");
        qs.append("notify_namecc,                     ");
        qs.append("notify_namebcc,                    ");
        qs.append("notify_addresscc,                  ");
        qs.append("notify_addressbcc,                 ");
        qs.append("notify_body,                       ");
        qs.append("notify_date,                       ");
        qs.append("app_seq_num,                       ");
        qs.append("notify_type,                       ");
        qs.append("notify_party )                     ");
        qs.append("values (                           ");
        qs.append("?,?,?,?,?,?,?,?,?,sysdate,?,?,? )  ");
      }
      
      ps = getPreparedStatement(qs.toString());

      switch(notifyParty)
      {
        case PARTY_MERCHANT:
          if(notifyType == NOTIFY_EMAIL)
          {
            ps.setString(1, this.mEmailSubject);
            ps.setString(2, this.mEmailHeader);
            ps.setString(3, this.merchEmail);
            ps.setString(4, this.merchName);
            
            ps.setString(5, this.merchNamecc);
            ps.setString(6, this.merchNamebcc);
            ps.setString(7, this.merchEmailcc);
            ps.setString(8, this.merchEmailbcc);

          }
          else
          {
            ps.setString(1, "");
            ps.setString(2, "");
            
            tempAddress  = merchAddress1 + (isBlank(merchAddress1) ? "" : "<br>"); 
            tempAddress += merchAddress2 + (isBlank(merchAddress2) ? "" : "<br>"); 
            tempAddress += merchCity     + (isBlank(merchCity)     ? "" : ",&nbsp;"); 
            tempAddress += merchState    + (isBlank(merchState)    ? "" : "&nbsp;"); 
            tempAddress += merchZip;
            
            ps.setString(3, tempAddress);
            ps.setString(4, this.merchName);
            ps.setString(5, "");
            ps.setString(6, "");
            ps.setString(7, "");
            ps.setString(8, "");
          }
          break;
        
        case PARTY_SOURCE:
          if(notifyType == NOTIFY_EMAIL)
          {
            ps.setString(1, this.sEmailSubject);
            ps.setString(2, this.sEmailHeader);
            ps.setString(3, this.sourceEmail);
            ps.setString(4, this.sourceName);
            ps.setString(5, this.sourceNamecc);
            ps.setString(6, this.sourceNamebcc);
            ps.setString(7, this.sourceEmailcc);
            ps.setString(8, this.sourceEmailbcc);

          }
          else
          {
            ps.setString(1, "");
            ps.setString(2, "");

            tempAddress  = sourceAddress1 + (isBlank(sourceAddress1) ? "" : "<br>");
            tempAddress += sourceAddress2 + (isBlank(sourceAddress2) ? "" : "<br>"); 
            tempAddress += sourceCity     + (isBlank(sourceCity)     ? "" : ",&nbsp;"); 
            tempAddress += sourceState    + (isBlank(sourceState)    ? "" : "&nbsp;"); 
            tempAddress += sourceZip;
            
            ps.setString(3, tempAddress);
            ps.setString(4, this.sourceName);
            ps.setString(5, "");
            ps.setString(6, "");
            ps.setString(7, "");
            ps.setString(8, "");
          }
          break;
        
        case PARTY_3RD:
          if(notifyType == NOTIFY_EMAIL)
          {
            ps.setString(1, this.pEmailSubject);
            ps.setString(2, this.pEmailHeader);
            ps.setString(3, this.partyEmail);
            ps.setString(4, this.partyName);
            ps.setString(5, this.partyNamecc);
            ps.setString(6, this.partyNamebcc);
            ps.setString(7, this.partyEmailcc);
            ps.setString(8, this.partyEmailbcc);

          }
          else
          {
            ps.setString(1, this.partySubject);
            ps.setString(2, "");

            tempAddress  = partyAddress1 + (isBlank(partyAddress1) ? "" : "<br>");
            tempAddress += partyAddress2 + (isBlank(partyAddress2) ? "" : "<br>");
            tempAddress += partyCity     + (isBlank(partyCity)     ? "" : ",&nbsp;"); 
            tempAddress += partyState    + (isBlank(partyState)    ? "" : "&nbsp;"); 
            tempAddress += partyZip;
            
            ps.setString(3, tempAddress);
            ps.setString(4, this.partyName);
            ps.setString(5, "");
            ps.setString(6, "");
            ps.setString(7, "");
            ps.setString(8, "");
          }
          break;
      }
      
      if(notifyType == NOTIFY_EMAIL)
      {
        ps.setString(9, composeEmail(notifyParty));
      }
      else if(notifyType == NOTIFY_LETTER)
      {
        ps.setString(9, composeLetter(notifyParty, tempAddress));
      }      
      
      ps.setLong(10, primaryKey);
      ps.setInt(11, notifyType);
      ps.setInt(12, notifyParty);


      ps.executeUpdate();
      ps.close();
    }
    catch(Exception e)
    {
      logEntry("submitNotifyData(" + primaryKey + ")", e.toString());
      addError("submitNotifyData: " + e.toString());
    }
  } 
  public String composeEmail(int notifyParty)
  {
    String email = "";
    switch(notifyParty)
    {
      case PARTY_MERCHANT:
        email =  "<b>Date Sent:</b> " + (new Date()).toString() + "<br><br>";
        email += "<b>Recipient:</b> " + this.merchName + " < <a href=\"/jsp/credit/emailutility.jsp?to=" + this.merchEmail + "\">" + this.merchEmail + "</a> > " + "<br><br>";
        if(!isBlank(this.merchEmailcc))
        {
          email += "<b>CC:</b> " + this.merchNamecc + " < <a href=\"/jsp/credit/emailutility.jsp?to=" + this.merchEmailcc + "\">" + this.merchEmailcc + "</a> > " + "<br><br>";
        }
        if(!isBlank(this.merchEmailbcc))
        {
          email += "<b>BCC:</b> " + this.merchNamebcc + " < <a href=\"/jsp/credit/emailutility.jsp?to=" + this.merchEmailbcc + "\">" + this.merchEmailbcc + "</a> > " + "<br><br>";
        }
        email += "<b>Subject:</b> " + this.mEmailSubject +  "<br><br>";
        if(!isBlank(this.mEmailHeader))
        {
          email += "<b>" + this.mEmailHeader + "</b>" + "<br><br>";
          email += this.letterContents;
        }
        else
        {
          email += this.letterContents;
        }
        break;
      
      case PARTY_SOURCE:
        email =  "<b>Date Sent:</b> " + (new Date()).toString() + "<br><br>";
        email += "<b>Recipient:</b> " + this.sourceName + " < <a href=\"/jsp/credit/emailutility.jsp?to=" + this.sourceEmail + "\">" + this.sourceEmail + "</a> > " + "<br><br>";
        if(!isBlank(this.sourceEmailcc))
        {
          email += "<b>CC:</b> " + this.sourceNamecc + " < <a href=\"/jsp/credit/emailutility.jsp?to=" + this.sourceEmailcc + "\">" + this.sourceEmailcc + "</a> > " + "<br><br>";
        }
        if(!isBlank(this.sourceEmailbcc))
        {
          email += "<b>BCC:</b> " + this.sourceNamebcc + " < <a href=\"/jsp/credit/emailutility.jsp?to=" + this.sourceEmailbcc + "\">" + this.sourceEmailbcc + "</a> > " + "<br><br>";
        }
        email += "<b>Subject:</b> " + this.sEmailSubject +  "<br><br>";
        if(!isBlank(this.sEmailHeader))
        {
          email += "<b>" + this.sEmailHeader + "</b>" + "<br><br>";
          email += this.letterContents;
        }
        else
        {
          email += this.letterContents;
        }
        break;
      
      case PARTY_3RD:
        email =  "<b>Date Sent:</b> " + (new Date()).toString() + "<br><br>";
        email += "<b>Recipient:</b> " + this.partyName + " < <a href=\"/jsp/credit/emailutility.jsp?to=" + this.partyEmail + "\">" + this.partyEmail + "</a> > " + "<br><br>";
        if(!isBlank(this.partyEmailcc))
        {
          email += "<b>CC:</b> " + this.partyNamecc + " < <a href=\"/jsp/credit/emailutility.jsp?to=" + this.partyEmailcc + "\">" + this.partyEmailcc + "</a> > " + "<br><br>";
        }
        if(!isBlank(this.partyEmailbcc))
        {
          email += "<b>BCC:</b> " + this.partyNamebcc + " < <a href=\"/jsp/credit/emailutility.jsp?to=" + this.partyEmailbcc + "\">" + this.partyEmailbcc + "</a> > " + "<br><br>";
        }
        email += "<b>Subject:</b> " + this.pEmailSubject +  "<br><br>";
        if(!isBlank(this.pEmailHeader))
        {
          email += "<b>" + this.pEmailHeader + "</b>" + "<br><br>";
          email += this.letterContents;
        }
        else
        {
          email += this.letterContents;
        }
        break;
    }
    
    return email;
  }
  public String composeLetter(int notifyParty, String address)
  {
    String letter = "";
    switch(notifyParty)
    {
      case PARTY_MERCHANT:
        letter = this.merchLName  + (isBlank(merchLName)  ? "" : "<br>") + address + (isBlank(address) ? "" : "<br><br>") + this.merchSubject   + (isBlank(this.merchSubject)   ? "" : "<br><br>") + this.letterContents;
        break;
      
      case PARTY_SOURCE:
        letter = this.sourceLName + (isBlank(sourceLName) ? "" : "<br>") + address + (isBlank(address) ? "" : "<br><br>") + this.sourceSubject  + (isBlank(this.sourceSubject)  ? "" : "<br><br>") + this.letterContents;
        break;
      
      case PARTY_3RD:
        letter = this.partyLName  + (isBlank(partyLName)  ? "" : "<br>") + address + (isBlank(address) ? "" : "<br><br>") + this.partySubject   + (isBlank(this.partySubject)   ? "" : "<br><br>") + this.letterContents;
        break;
    }
    return letter;
  }
  public void submitData(HttpServletRequest req, long primaryKey)
  {
    //submitMerchantData(primaryKey);
    submitEmailData(primaryKey);
    submitPrintData(primaryKey);
    getNotifyData(primaryKey);
  }
  
  public int getApprovalStatus()
  {
    return this.approvalStatus;
  }
  
  public String getReason()
  {
    return this.reason;
  }

  public void setMemailHeader(String mEmailHeader)
  {
    this.mEmailHeader = mEmailHeader;
  }                                  
  public String getMemailHeader()
  {
    return this.mEmailHeader;
  }
  
  public void setMemailSubject(String mEmailSubject)
  {
    this.mEmailSubject = mEmailSubject;
  }                                  
  public String getMemailSubject()
  {
    System.out.println("mEmailSubject returning: " + mEmailSubject);
    return this.mEmailSubject;
  }
  
  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return this.merchName;
  }

  public void setMerchNamecc(String merchNamecc)
  {
    this.merchNamecc = merchNamecc;
  }
  public String getMerchNamecc()
  {
    return this.merchNamecc;
  }

  public void setMerchNamebcc(String merchNamebcc)
  {
    this.merchNamebcc = merchNamebcc;
  }
  public String getMerchNamebcc()
  {
    return this.merchNamebcc;
  }

  public void setMerchLname(String merchLName)
  {
    this.merchLName = merchLName;
  }
  public String getMerchLname()
  {
    return this.merchLName;
  }

  public void setMerchSubject(String merchSubject)
  {
    this.merchSubject = merchSubject;
  }
  public String getMerchSubject()
  {
    return this.merchSubject;
  }

  public void setMerchEmail(String merchEmail)
  {
    this.merchEmail = merchEmail;
    mEmail.setNotifyAddress(merchEmail);
  }
  public void setMerchEmailcc(String merchEmailcc)
  {
    this.merchEmailcc = merchEmailcc;
    mEmail.setNotifyAddresscc(merchEmailcc);
  }
  public void setMerchEmailbcc(String merchEmailbcc)
  {
    this.merchEmailbcc = merchEmailbcc;
    mEmail.setNotifyAddressbcc(merchEmailbcc);
  }

  public String getMerchEmail()
  {
    return this.merchEmail;
  }
  public String getMerchEmailcc()
  {
    return this.merchEmailcc;
  }
  public String getMerchEmailbcc()
  {
    return this.merchEmailbcc;
  }
  
  public void setMerchAddress1(String merchAddress1)
  {
    this.merchAddress1 = merchAddress1;
    mLetter.setNotifyAddress(merchAddress1);
  }
  public String getMerchAddress1()
  {
    return this.merchAddress1;
  }
  
  public void setMerchAddress2(String merchAddress2)
  {
    this.merchAddress2 = merchAddress2;
  }
  public String getMerchAddress2()
  {
    return this.merchAddress2;
  }
  
  public void setMerchCity(String merchCity)
  {
    this.merchCity = merchCity;
  }
  public String getMerchCity()
  {
    return this.merchCity;
  }
  
  public void setMerchState(String merchState)
  {
    if(merchState.equals("select"))
    {  
      merchState="";
    }
    
    this.merchState = merchState;
  }
  public String getMerchState()
  {
    String result = "";
    
    if(this.merchState != null)
    {
      result = this.merchState;
    }
    return result;
  }
  
  public void setMerchZip(String merchZip)
  {
    this.merchZip = merchZip;
  }
  public String getMerchZip()
  {
    return this.merchZip;
  }
  
  public void setSemailHeader(String sEmailHeader)
  {
    this.sEmailHeader = sEmailHeader;
  }                                  
  public String getSemailHeader()
  {
    return this.sEmailHeader;
  }
  
  public void setSemailSubject(String sEmailSubject)
  {
    this.sEmailSubject = sEmailSubject;
  }                                  
  public String getSemailSubject()
  {
    return this.sEmailSubject;
  }
  
  public void setSourceName(String sourceName)
  {
    this.sourceName = sourceName;
  }
  public String getSourceName()
  {
    return this.sourceName;
  }

  public void setSourceNamecc(String sourceNamecc)
  {
    this.sourceNamecc = sourceNamecc;
  }
  public String getSourceNamecc()
  {
    return this.sourceNamecc;
  }

  public void setSourceNamebcc(String sourceNamebcc)
  {
    this.sourceNamebcc = sourceNamebcc;
  }
  public String getSourceNamebcc()
  {
    return this.sourceNamebcc;
  }


  public void setSourceLname(String sourceLName)
  {
    this.sourceLName = sourceLName;
  }
  public String getSourceLname()
  {
    return this.sourceLName;
  }

  public void setSourceSubject(String sourceSubject)
  {
    this.sourceSubject = sourceSubject;
  }
  public String getSourceSubject()
  {
    return this.sourceSubject;
  }
  
  public void setSourceEmail(String sourceEmail)
  {
    this.sourceEmail = sourceEmail;
    sEmail.setNotifyAddress(sourceEmail);
  }
  public void setSourceEmailcc(String sourceEmailcc)
  {
    this.sourceEmailcc = sourceEmailcc;
    sEmail.setNotifyAddresscc(sourceEmailcc);
  }
  public void setSourceEmailbcc(String sourceEmailbcc)
  {
    this.sourceEmailbcc = sourceEmailbcc;
    sEmail.setNotifyAddressbcc(sourceEmailbcc);
  }
  
  public String getSourceEmail()
  {
    return this.sourceEmail;
  }
  public String getSourceEmailcc()
  {
    return this.sourceEmailcc;
  }
  public String getSourceEmailbcc()
  {
    return this.sourceEmailbcc;
  }
  
  public void setSourceAddress1(String sourceAddress1)
  {
    this.sourceAddress1 = sourceAddress1;
    sLetter.setNotifyAddress(sourceAddress1);
  }
  public String getSourceAddress1()
  {
    return this.sourceAddress1;
  }
  
  public void setSourceAddress2(String sourceAddress2)
  {
    this.sourceAddress2 = sourceAddress2;
  }
  public String getSourceAddress2()
  {
    return this.sourceAddress2;
  }
  
  public void setSourceCity(String sourceCity)
  {
    this.sourceCity = sourceCity;
  }
  public String getSourceCity()
  {
    return this.sourceCity;
  }
  
  public void setSourceState(String sourceState)
  {
    if(sourceState.equals("select"))
    {  
      sourceState="";
    }
    
    this.sourceState = sourceState;
  }
  public String getSourceState()
  {
    return this.sourceState;
  }
  
  public void setSourceZip(String sourceZip)
  {
    this.sourceZip = sourceZip;
  }
  public String getSourceZip()
  {
    return this.sourceZip;
  }
  
  public void setPartySubject(String partySubject)
  {
    this.partySubject = partySubject;
  }
  public String getPartySubject()
  {
    return this.partySubject;
  }
  
  public void setPemailHeader(String pEmailHeader)
  {
    this.pEmailHeader = pEmailHeader;
  }                                  
  public String getPemailHeader()
  {
    return this.pEmailHeader;
  }
  
  public void setPemailSubject(String pEmailSubject)
  {
    this.pEmailSubject = pEmailSubject;
  }                                  
  public String getPemailSubject()
  {
    return this.pEmailSubject;
  }
  
  public void setPartyName(String partyName)
  {
    this.partyName = partyName;
  }
  public String getPartyName()
  {
    return this.partyName;
  }

  public void setPartyNamecc(String partyNamecc)
  {
    this.partyNamecc = partyNamecc;
  }
  public String getPartyNamecc()
  {
    return this.partyNamecc;
  }

  public void setPartyNamebcc(String partyNamebcc)
  {
    this.partyNamebcc = partyNamebcc;
  }
  public String getPartyNamebcc()
  {
    return this.partyNamebcc;
  }


  public void setPartyLname(String partyLName)
  {
    this.partyLName = partyLName;
  }
  public String getPartyLname()
  {
    return this.partyLName;
  }
  
  public void setPartyEmail(String partyEmail)
  {
    this.partyEmail = partyEmail;
    pEmail.setNotifyAddress(partyEmail);
  }
  public void setPartyEmailcc(String partyEmailcc)
  {
    this.partyEmailcc = partyEmailcc;
    pEmail.setNotifyAddresscc(partyEmailcc);
  }
  public void setPartyEmailbcc(String partyEmailbcc)
  {
    this.partyEmailbcc = partyEmailbcc;
    pEmail.setNotifyAddressbcc(partyEmailbcc);
  }
  
  
  public String getPartyEmail()
  {
    return this.partyEmail;
  }
  public String getPartyEmailcc()
  {
    return this.partyEmailcc;
  }
  public String getPartyEmailbcc()
  {
    return this.partyEmailbcc;
  }
  
  public void setPartyAddress1(String partyAddress1)
  {
    this.partyAddress1 = partyAddress1;
    pLetter.setNotifyAddress(partyAddress1);
  }
  public String getPartyAddress1()
  {
    return this.partyAddress1;
  }
  
  public void setPartyAddress2(String partyAddress2)
  {
    this.partyAddress2 = partyAddress2;
  }
  public String getPartyAddress2()
  {
    return this.partyAddress2;
  }
  
  public void setPartyCity(String partyCity)
  {
    this.partyCity = partyCity;
  }
  public String getPartyCity()
  {
    return this.partyCity;
  }
  
  public void setPartyState(String partyState)
  {
    if(partyState.equals("select"))
    {  
      partyState="";
    }
    this.partyState = partyState;
  }
  public String getPartyState()
  {
    return this.partyState;
  }
  
  public void setPartyZip(String partyZip)
  {
    this.partyZip = partyZip;
  }
  public String getPartyZip()
  {
    return this.partyZip;
  }
  
  public int getEmail()
  {
    return this.email;
  }
  public void setEmail(String[] email)
  {
    for(int i=0; i < email.length; ++i)
    {
      int bit = parseInt(email[i]);
      this.email |= bit;
    }
  }
  public boolean isSent(long primaryKey, int notifyType, int notifyParty)
  {
    boolean           found = false;
    
    try
    {
      // make sure a connection is established.  the jsp will close it
      if(isConnectionStale())
      {
        connect();
      }
      
      int appCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1564^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    app_notify
//          where   notify_type   = :notifyType  and
//                  notify_party  = :notifyParty and
//                  app_seq_num   = :primaryKey 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    app_notify\n        where   notify_type   =  :1   and\n                notify_party  =  :2  and\n                app_seq_num   =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.CreditLetterBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,notifyType);
   __sJT_st.setInt(2,notifyParty);
   __sJT_st.setLong(3,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1572^7*/
      
      found = (appCount > 0);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isSent: " + e.toString());
      addError("isSent: " + e.toString());
    }
    return found;
  }
  public void setSendEmail(String temp)
  {
    this.sendEmail = true;
  }
  public boolean sendEmail()
  {
    return this.sendEmail;
  }
  public void setMcomposeLetter(String temp)
  {
    this.mComposeLetter = true;
  }
  public boolean isMcomposed()
  {
    return this.mComposeLetter;
  }
  public void setScomposeLetter(String temp)
  {
    this.sComposeLetter = true;
  }
  public boolean isScomposed()
  {
    return sComposeLetter;
  }
  public void setPcomposeLetter(String temp)
  {
    this.pComposeLetter = true;
  }
  public boolean isPcomposed()
  {
    return pComposeLetter;  
  }

  public Notification getNotify(int notifyType, int notifyParty)
  {
    Notification curNotify = null;
    
    switch(notifyParty)
    {
      case PARTY_MERCHANT:
        if(notifyType == NOTIFY_EMAIL)
        {
          curNotify = this.mEmail;
        }
        else
        {
          curNotify = this.mLetter;
        }
        break;
        
      case PARTY_SOURCE:
        if(notifyType == NOTIFY_EMAIL)
        {
          curNotify = this.sEmail;
        }
        else
        {
          curNotify = this.sLetter;
        }
        break;
        
      case PARTY_3RD:
        if(notifyType == NOTIFY_EMAIL)
        {
          curNotify = this.pEmail;
        }
        else
        {
          curNotify = this.pLetter;
        }
        break;
    }
    
    return curNotify;
  }

  public String getNotifyAddress(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotifyAddress());
  }

  public String getNotifyAddresscc(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotifyAddresscc());
  }

  public String getNotifyAddressbcc(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotifyAddressbcc());
  }

  public String getNotifyName(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotifyName());
  }
  
  public String getNotifyNamecc(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotifyNamecc());
  }

  public String getNotifyNamebcc(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotifyNamebcc());
  }

  public String getNotifySubject(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotifySubject());
  }
    
  public String getNotifyHeader(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotifyHeader());
  }
      
  public String getNotifyBody(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotifyBody());
  }
  
  public String getNotifyDate(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotifyDate());
  }
  
  public String getNotifyTime(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotifyTime());
  }
  
  public boolean getNotified(int notifyType, int notifyParty)
  {
    Notification curNotify = getNotify(notifyType, notifyParty);
    return(curNotify.getNotified());
  }

  public Vector getStateOptions()
  {
    return this.stateOptions;
  }



  public void setLetterContents(String letterContents)
  {
    
    this.letterContents = letterContents;
    
    mEmail.setNotifyBody(letterContents);
    sEmail.setNotifyBody(letterContents);
    pEmail.setNotifyBody(letterContents);

    mLetter.setNotifyBody(letterContents);
    sLetter.setNotifyBody(letterContents);
    pLetter.setNotifyBody(letterContents);
  }
  public String getLetterContents()
  {
    return this.letterContents;
  }

  // helper class
  public class Notification
  {
    private boolean notified;
    private int     notifyType;
    private int     notifyParty;
    private String  notifyAddress;
    private String  notifyAddresscc;
    private String  notifyAddressbcc;
    private String  notifySubject;
    private String  notifyHeader;
    private String  notifyBody;
    private String  notifyDate;
    private String  notifyTime;
    private String  notifyName;
    private String  notifyNamecc;
    private String  notifyNamebcc;

    public Notification(int notifyType, int notifyParty)
    {
      this.notifyType   = notifyType;
      this.notifyParty  = notifyParty;
      this.notified     = false;
    }

    public void setNotified(boolean notified)
    {
      this.notified = notified;
    }
    public boolean getNotified()
    {
      return this.notified;
    }
    public int getNotifyType()
    {
      return this.notifyType;
    }
    public int getNotifyParty()
    {
      return this.notifyParty;
    }

    public void setNotifyAddress(String notifyAddress)
    {
      this.notifyAddress = notifyAddress;
    }
    public String getNotifyAddress()
    {
      return this.notifyAddress;
    }

    public void setNotifyAddresscc(String notifyAddresscc)
    {
      this.notifyAddresscc = notifyAddresscc;
    }
    public String getNotifyAddresscc()
    {
      return this.notifyAddresscc;
    }

    public void setNotifyAddressbcc(String notifyAddressbcc)
    {
      this.notifyAddressbcc = notifyAddressbcc;
    }
    public String getNotifyAddressbcc()
    {
      return this.notifyAddressbcc;
    }
    
    public void setNotifyName(String notifyName)
    {
      this.notifyName = notifyName;
    }
    public String getNotifyName()
    {
      return this.notifyName;
    }

    public void setNotifyNamecc(String notifyNamecc)
    {
      this.notifyNamecc = notifyNamecc;
    }
    public String getNotifyNamecc()
    {
      return this.notifyNamecc;
    }

    public void setNotifyNamebcc(String notifyNamebcc)
    {
      this.notifyNamebcc = notifyNamebcc;
    }
    public String getNotifyNamebcc()
    {
      return this.notifyNamebcc;
    }

    public void setNotifySubject(String notifySubject)
    {
      this.notifySubject = notifySubject;
    }
    public String getNotifySubject()
    {
      return this.notifySubject;
    }

    public void setNotifyHeader(String notifyHeader)
    {
      this.notifyHeader = notifyHeader;
    }
    public String getNotifyHeader()
    {
      return this.notifyHeader;
    }

    public void setNotifyBody(String notifyBody)
    {
      this.notifyBody = notifyBody;
    }
    public String getNotifyBody()
    {
      return this.notifyBody;
    }

    public void setNotifyDate(String notifyDate)
    {
      this.notifyDate = notifyDate;
    }
    public String getNotifyDate()
    {
      return this.notifyDate;
    }

    public void setNotifyTime(String notifyTime)
    {
      this.notifyTime = notifyTime;
    }
    public String getNotifyTime()
    {
      return this.notifyTime;
    }
  }
}/*@lineinfo:generated-code*/