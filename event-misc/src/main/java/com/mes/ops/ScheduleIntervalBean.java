/*@lineinfo:filename=ScheduleIntervalBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ScheduleIntervalBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 3/07/03 10:57a $
  Version            : $Revision: 9 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import com.mes.constants.MesQueues;
import com.mes.queues.QueueTools;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class ScheduleIntervalBean extends ScheduleBaseBean
{
  public  static final String BLOCK_OUT_FUNCTION_ID = "XXXXXXXXXXXXXXXX";

  private long      id                              = -1L;
  private String    userId                          = "";
  private int       numOfIntervals                  = -1;
  private String    dateString                      = "";
  private String    functionId                      = "";
  private String    functionDesc                    = "";
  private String    duration                        = "";
  private String    dateAssigned                    = "Not Assigned";
  private boolean   assigned                        = false;
  private boolean   blockOut                        = false;
  private Vector    errors                          = new Vector();

  public ScheduleIntervalBean()
  {
    //initializes calendar to be current date/time with default schedule type
    super();
  }

  public ScheduleIntervalBean(int scheduleType, long id, String userId)
  {
    //initializes calendar to be current date/time and sets up schedule type info
    super(scheduleType);
   
    //sets time interval for selected schedule
    initialize(id, userId);
  }

  public void initialize(long id, String userId)
  {
    this.id           = id;
    this.userId       = userId;

    setIntervalCalendar(this.id);
  }

  private void setIntervalCalendar(long momentInTime)
  {
    //setup calendar
    setCalendarDate(momentInTime);

    //setup month
    setIntervalCalendar(getCalendarYear(), getCalendarMonth(), getCalendarDay(), getCalendar24Hour(), getCalendarMinute(), getCalendarSecond());
  }


  private void setIntervalCalendar(int selectedYear, int selectedMonth, int selectedDay, int selectedHour, int selectedMinute, int selectedSecond)
  {

    setCalendarDate(selectedYear, selectedMonth, selectedDay, selectedHour, selectedMinute, selectedSecond);

    dateString = getCalendarDayOfWeekDesc() + " " + getCalendarMonthDesc() + " " + getCalendarDay() + ", " + getCalendarYear() + "  "+ getCalendar12Hour() + ":" + fixZeroTimeIssue(getCalendarMinute()) + " " + getCalendarAmPmDesc();
    
    //System.out.println(getCalendarDayOfWeekDesc() + " " + getCalendarMonthDesc() + " " + getCalendarDay() + ", " + getCalendarYear() + " " + getCalendar12Hour() + ":" + fixZeroTimeIssue(getCalendarMinute()) + ":" + fixZeroTimeIssue(getCalendarSecond()) + "." + getCalendarMillisecond() + " " + getCalendarAmPmDesc() + "  " + getLongTimeOfCalendar());

    //figure out the duration drop down box limit.. 
    //the duration cant span two calendar days. thats the limitation of this schedule
    //also duration can not overlap over another appointment
    
    this.numOfIntervals   = 0;
    long nextAppointment  = getNextAppointment();

    int actualCalendarDay = getCalendarDay();
    while(actualCalendarDay == getCalendarDay())
    {

      if((getScheduleStartOfDay() <= getCalendar24Hour() && getCalendar24Hour() <= getScheduleEndOfDay()) && (getLongTimeOfCalendar() < nextAppointment))
      {
        this.numOfIntervals++;
      }

      //increment calendar by the number of minutes specified by type of schedule
      //eventually they will flip over to the next day and break out of this loop
      addToCalendarField(Calendar.MINUTE, getTimeInterval());
    }


  }


/*
  gets next assigned data so we can determine the limit of the duration.. we cant have two appointments overlapping now can we..
*/  
  private long getNextAppointment()
  {
    ResultSetIterator   it         = null;
    ResultSet           rs         = null;
    long                result     = 9223372036854775807L; //set it to some date a few thousand years from now

    try
    {
      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:140^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   id 
//  
//          from     schedule_data  
//  
//          where    type     = :getScheduleType() and 
//                   user_id  = :userId       and
//                   id > :id
//          order by id ASC
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_805 = getScheduleType();
  try {
   String theSqlTS = "select   id \n\n        from     schedule_data  \n\n        where    type     =  :1  and \n                 user_id  =  :2        and\n                 id >  :3 \n        order by id ASC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.ScheduleIntervalBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_805);
   __sJT_st.setString(2,userId);
   __sJT_st.setLong(3,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.ScheduleIntervalBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:150^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = rs.getLong("id");
      }

      rs.close();
      it.close();

    }
    catch(Exception e)
    {
      logEntry("getNextAppointment()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }




/*
  gets assigned data from database and stores it in subclass which is stored in hashmap with id + employee id as key.
*/  
  public void loadAssignment()
  {
    ResultSetIterator   it         = null;
    ResultSet           rs         = null;

    try
    {
      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:190^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   * 
//  
//          from     schedule_data  
//  
//          where    type     = :getScheduleType() and 
//                   id       = :id                  and
//                   user_id  = :userId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_806 = getScheduleType();
  try {
   String theSqlTS = "select   * \n\n        from     schedule_data  \n\n        where    type     =  :1  and \n                 id       =  :2                   and\n                 user_id  =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.ScheduleIntervalBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_806);
   __sJT_st.setLong(2,id);
   __sJT_st.setString(3,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.ScheduleIntervalBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:199^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        this.assigned     = true;
        this.dateAssigned = DateTimeFormatter.getFormattedDate(rs.getTimestamp("date_assigned"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
        
        //if submitted, but has errors.. we dont want to replace the data they tried to submit
        if(!hasErrors())
        {
          this.duration     = rs.getString("duration");
          this.functionId   = rs.getString("function_id");
          if(this.functionId.equals(BLOCK_OUT_FUNCTION_ID))
          {
            this.functionId = "";
            this.blockOut   = true;
          }
          this.functionDesc = rs.getString("function_desc");
        }
      
      }

      rs.close();
      it.close();

    }
    catch(Exception e)
    {
      logEntry("loadAssignment()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }

  public void deleteAssignment()
  {

    try
    {
      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:245^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from     schedule_data  
//          where    type     = :getScheduleType() and 
//                   id       = :id           and
//                   user_id  = :userId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_807 = getScheduleType();
   String theSqlTS = "delete \n        from     schedule_data  \n        where    type     =  :1  and \n                 id       =  :2            and\n                 user_id  =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.ScheduleIntervalBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_807);
   __sJT_st.setLong(2,id);
   __sJT_st.setString(3,userId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:252^7*/
    
    }
    catch(Exception e)
    {
      logEntry("deleteAssignment()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }

  public void submitAssignment()
  {
    //instead of doing an update..we just delete and submit fresh
    deleteAssignment();
    
    if(isBlank(this.functionId))
    {
      this.functionId = BLOCK_OUT_FUNCTION_ID;
    }

    try
    {
      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:280^7*/

//  ************************************************************
//  #sql [Ctx] { insert into schedule_data
//          (
//            type,
//            id,
//            user_id,
//            duration,
//            function_id,
//            function_desc,
//            date_assigned
//          )
//          values
//          (
//            :getScheduleType(),
//            :id,
//            :userId,
//            :duration,
//            :functionId,
//            :functionDesc,
//            sysdate
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_808 = getScheduleType();
   String theSqlTS = "insert into schedule_data\n        (\n          type,\n          id,\n          user_id,\n          duration,\n          function_id,\n          function_desc,\n          date_assigned\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          sysdate\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.ScheduleIntervalBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_808);
   __sJT_st.setLong(2,id);
   __sJT_st.setString(3,userId);
   __sJT_st.setString(4,duration);
   __sJT_st.setString(5,functionId);
   __sJT_st.setString(6,functionDesc);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:302^7*/

    }
    catch(Exception e)
    {
      logEntry("submitAssignment()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }


  private boolean appApproved(String merchantNumber)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
   
      /*@lineinfo:generated-code*//*@lineinfo:332^7*/

//  ************************************************************
//  #sql [Ctx] it = { select merch_credit_status
//          from  merchant            
//          where merch_number = :merchantNumber.trim()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_809 = merchantNumber.trim();
  try {
   String theSqlTS = "select merch_credit_status\n        from  merchant            \n        where merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.ScheduleIntervalBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_809);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.ScheduleIntervalBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:337^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        if(rs.getInt("merch_credit_status") == QueueConstants.CREDIT_APPROVE)
        {
          result = true;
        }
      }
 
      it.close();

      //check to see if in mif.. if in mif.. then its been approved
      if(!result)
      {
        /*@lineinfo:generated-code*//*@lineinfo:354^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchant_number
//            from    mif            
//            where merchant_number = :merchantNumber.trim()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_810 = merchantNumber.trim();
  try {
   String theSqlTS = "select  merchant_number\n          from    mif            \n          where merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.ScheduleIntervalBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_810);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.ScheduleIntervalBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:359^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          result = true;
        }
   
        it.close();
      }

    }
    catch(Exception e)
    {
      logEntry("appApproved()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;

  }




  public boolean isValid()
  {
    
    if(!this.blockOut)
    {
      if(isBlank(this.functionId))
      {
        errors.add("Appointment Id can not be blank, unless blocking out time interval");
      }
      else if(getScheduleType() == SCHEDULE_TYPE_ACTIVATION && !appApproved(this.functionId))
      {
        errors.add("This application has been declined, do not schedule for activation");        
      }
    }
    else
    {
      if(!isBlank(this.functionId))
      {
        errors.add("Appointment Id must be blank if blocking out time interval");
      }
    }

    if(isBlank(this.functionDesc))
    {
      errors.add("Appointment Desc can not be blank");
    }

    return !hasErrors();
  }

  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }

  public String getError(int idx)
  {
    return (String)errors.elementAt(idx);
  }

  public int getNumberOfErrors()
  {
    return errors.size();
  }


  public boolean isAssigned()
  {
    return this.assigned;
  }

  public String getDateString()
  {
    return this.dateString;
  }
  
  
  public int getNumOfIntervals()
  {
    return this.numOfIntervals;
  }

  public String getFunctionId()
  {
    return this.functionId;
  }

  public String getFunctionDesc()
  {
    return this.functionDesc;
  }

  public String getDuration()
  {
    return this.duration;
  }

  public String getDateAssigned()
  {
    return dateAssigned;
  }

  public boolean isBlockOut()
  {
    return this.blockOut;
  }


  public void setFunctionId(String functionId)
  {
    this.functionId = functionId;
  }

  public void setFunctionDesc(String functionDesc)
  {
    this.functionDesc = functionDesc;
  }

  public void setDuration(String duration)
  {
    this.duration = duration;
  }

  public void setBlockOut(boolean blockOut)
  {
    this.blockOut = blockOut;
  }


  public void executeAfterSubmitMethods()
  {
    switch(getScheduleType())
    {
      case SCHEDULE_TYPE_ACTIVATION:
        if(isBlockOut())
        {
          //dont even trip
        }
        else
        {

          //new queue system
          long appSeqNum = getSeqNumFromMerchNum(this.functionId.trim());
          if(appSeqNum > -1L)
          {
            if(QueueTools.getCurrentQueueType(appSeqNum, MesQueues.Q_ITEM_TYPE_ACTIVATION) != MesQueues.Q_ACTIVATION_REFERRED)
            {
              //moves from which ever type of activation queue its currently in.. (except referred queue)
              //to the scheduled activation queue
              QueueTools.moveQueueItem(appSeqNum, QueueTools.getCurrentQueueType(appSeqNum, MesQueues.Q_ITEM_TYPE_ACTIVATION), MesQueues.Q_ACTIVATION_SCHEDULED, null, "");
            }
          }

          //old queue system
          ActivationQueueBean.changeStage(QueueConstants.QUEUE_ACTIVATION, QueueConstants.Q_ACTIVATION_SCHEDULED, this.functionId, -1L);
        }
      break;
      
      default:
        //do nothing
      break;
    }
  }

  private long getSeqNumFromMerchNum(String merchNum)
  {

    long appseqnum      = -1L;
    boolean useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
     
      //get seqnum from merch num if exists
      /*@lineinfo:generated-code*//*@lineinfo:550^7*/

//  ************************************************************
//  #sql [Ctx] { select app_seq_num 
//          from   merchant
//          where  merch_number = :merchNum 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select app_seq_num  \n        from   merchant\n        where  merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.ScheduleIntervalBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appseqnum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:555^7*/
    }
    catch(Exception e)
    {
      logEntry("getSeqNumFromMerchNum()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
    return appseqnum;
  }



}/*@lineinfo:generated-code*/