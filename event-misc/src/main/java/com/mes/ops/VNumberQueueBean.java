/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/VNumberQueueBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/16/01 5:29p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

public class VNumberQueueBean extends QueueBean
{
  public void fillReportMenuItems()
  {
    addReportMenuItem(new ReportMenuItem("POS ID Pending Queue", "mes_queue.jsp?queueType=" + QueueConstants.QUEUE_VNUMBER + "&queueStage=" + QueueConstants.Q_VNUMBER_NEW));
    addReportMenuItem(new ReportMenuItem("POS ID Assigned Queue", "mes_queue.jsp?queueType=" + QueueConstants.QUEUE_VNUMBER + "&queueStage=" + QueueConstants.Q_VNUMBER_ASSIGNED));
  }
  
  public VNumberQueueBean()
  {
    super("VNumberQueueBean");
    fillReportMenuItems();
  }
  
  public VNumberQueueBean(String msg)
  {
    super(msg);
    fillReportMenuItems();
  }
  
  public boolean getQueueData(long queueKey, int queueType)
  {
    return(super.getQueueData(
            queueKey,
            queueType,
            "merchant merch",
            "merch.merc_cntrl_number, merch.merch_business_name",
            "and a.app_seq_num = merch.app_seq_num"));
  }
  
  public void getFunctionOptions(javax.servlet.jsp.JspWriter out, long appSeqNum)
  {
    try
    {
      out.println("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" width=\"80%\">");
      out.println("  <tr>");
      out.println("    <td height=\"19\" valign=\"center\">");
      out.println("      &nbsp;");
      out.println("    </td>");
      out.println("    <td height=\"19\" valign=\"center\">");
      out.println("      <a href=\"vnumber_assign.jsp?primaryKey=" + appSeqNum + "\">Assign V-Number(s)</a>");
      out.println("    </td>");
      out.println("  </tr>");
      out.println("</table>");
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFunctionOptions: " + e.toString());
    }
  }
}
