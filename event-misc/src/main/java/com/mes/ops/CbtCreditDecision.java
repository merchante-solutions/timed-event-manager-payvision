/*@lineinfo:filename=CbtCreditDecision*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/CbtCreditDecision.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  CbtCreditDecision

  Supports CB&T credit queue decision screen (cbt_credit_decision.jsp).

  Change History:
     See VSS database

  Copyright (C) 2003 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.queues.CbtDocumentQueue;
import com.mes.queues.QueueTools;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class CbtCreditDecision extends FieldBean
{
  // reason types (maps to q_credit_reasons.type)
  public static final int RT_DECLINE  = 1;
  public static final int RT_PEND     = 2;
  public static final int RT_CANCEL   = 3;
  public static final int RT_DOCS     = 4;

  // generic queue types
  public static final int GTQ_NEW       = 0;
  public static final int GTQ_PENDING   = 1;
  public static final int GTQ_DECLINED  = 2;
  public static final int GTQ_CANCELED  = 3;
  public static final int GTQ_APPROVED  = 4;

  /*
  ** public CbtCreditDecision(UserBean user)
  **
  ** Constructor.
  **
  ** Initializes fields used by credit decision screen form.
  */
  public CbtCreditDecision(UserBean user)
  {
    super(user);

    fields.add(new HiddenField("type"));
    fields.add(new HiddenField("id"));
    fields.add(new HiddenField("itemType"));
    fields.add(new HiddenField("startType"));

    fields.add(new ButtonField("approve"));
    fields.add(new ButtonField("pend"));
    fields.add(new ButtonField("decline"));
    fields.add(new ButtonField("cancel"));

    fields.setGroupHtmlExtra("class=\"smallText\"");
  }

  /*
  ** private void createCreditReasonFields(int type, String groupName,
  **   String itemName)
  **
  ** Loads all reason codes from q_credit_reasons that have the type
  ** assigned to the queue type in q_credit_types.  A checkbox field 
  ** is generated for each reason code found, named by appending the 
  ** code to the fieldName prefix.  The fields are placed in a field 
  ** group named with groupName, which is then placed into the beans 
  ** fields.
  */

  private void createCreditReasonFields(int qType, String groupName,
    String fieldPrefix)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      FieldGroup reasonGroup = new FieldGroup(groupName);

      connect();

      // load reasons
      /*@lineinfo:generated-code*//*@lineinfo:106^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  :fieldPrefix||'_'||qr.code field_name,
//                  qr.label
//          from    q_credit_reasons  qr,
//                  q_credit_types    qt
//          where   qt.type = :qType
//                  and qt.reason_type = qr.type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1 ||'_'||qr.code field_name,\n                qr.label\n        from    q_credit_reasons  qr,\n                q_credit_types    qt\n        where   qt.type =  :2 \n                and qt.reason_type = qr.type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.CbtCreditDecision",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fieldPrefix);
   __sJT_st.setInt(2,qType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.CbtCreditDecision",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:114^7*/

      // generate a checkbox field for each reason
      rs = it.getResultSet();
      while(rs.next())
      {
        String fieldName  = rs.getString("field_name");
        String label      = rs.getString("label");
        Field reason      = new CheckboxField(fieldName,label,false);
        reasonGroup.add(reason);
      }

      reasonGroup.setGroupHtmlExtra("class=\"smallText\"");
      fields.add(reasonGroup);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::createCreditReasonFields(" + groupName + "): " + e);
      logEntry("createCreditReasonFields(" + groupName + ")",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /*
  ** protected void preHandleRequest(HttpServletRequest request)
  **
  ** Create additional reason fields using type found in request.
  */
  protected void preHandleRequest(HttpServletRequest request)
  {
    // create reason code fields based on queue type
    String qType = request.getParameter("type");

    createCreditReasonFields(MesQueues.Q_CBT_DECLINED,  "declineReasons", "decline");
    createCreditReasonFields(MesQueues.Q_CBT_PENDING,   "pendReasons",    "pend");
    createCreditReasonFields(MesQueues.Q_CBT_CANCELLED, "cancelReasons",  "cancel");
  }
  
  /*
  ** protected void postHandleRequest(HttpServletRequest request)
  **
  ** Set startType if it's blank.
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    // copy type into startType if startType is not set
    if (fields.getField("startType").isBlank())
    {
      fields.setData("startType",fields.getData("type"));
    }
  }

  /*
  ** private String makeCodeString(String groupName)
  **
  ** Takes a groupName, gets the field group with that name and iterates
  ** through the fields contained by it.  Constructs a comma delimitted
  ** list of the codes contained in the field names (code is assumed to
  ** be the portion of the field name following the first underscore).
  **
  ** RETURNS: a String containing the codes separated by commas.
  */
  private String makeCodeString(String groupName)
  {
    StringBuffer codeString = new StringBuffer();
    try
    {
      boolean firstCode = true;
      for (Iterator i = getGroupIterator(groupName); i.hasNext(); )
      {
        Field field = (Field)i.next();
        if (field instanceof CheckboxField
            && ((CheckboxField)field).isChecked())
        {
          if (!firstCode)
          {
            codeString.append(",");
          }
          else
          {
            firstCode = false;
          }
          String name = field.getName();
          String code = name.substring(name.indexOf('_') + 1);
          codeString.append(code);
        }
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::makeCodeString(" + groupName + "): " + e);
      logEntry("makeCodeString(" + groupName + ")",e.toString());
    }
    return codeString.toString();
  }

  /*
  ** private void resetTypeFields(int queueType)
  **
  ** Resets all fields contained within a field group corresponding with
  ** the given queue type.
  */
  private void resetTypeFields(int queueType)
  {
    // determine the corresponding field group name from the queue type
    String groupName = null;
    switch (queueType)
    {
      case MesQueues.Q_CBT_PENDING:
      case MesQueues.Q_CBT_PENDING_HIGH_VOLUME:
        groupName = "pendReasons";
        break;

      case MesQueues.Q_CBT_CANCELLED:
      case MesQueues.Q_CBT_CANCELLED_HIGH_VOLUME:
        groupName = "cancelReasons";
        break;

      case MesQueues.Q_CBT_DECLINED:
      case MesQueues.Q_CBT_DECLINED_HIGH_VOLUME:
        groupName = "declineReasons";
        break;

      case MesQueues.Q_CBT_APPROVED:
      case MesQueues.Q_CBT_APPROVED_HIGH_VOLUME:
        groupName = "additionalDocs";
        break;

      default:
        break;
    }
    
    if (groupName != null)
    {
      FieldGroup group = (FieldGroup)fields.getField(groupName);
      if (group != null)
      {
        group.reset();
      }
    }
  }

  /*
  ** private int getGenericType(int actualType)
  **
  ** RETURNS: a generic queue type based on an actual queue type id.
  */
  private int getGenericType(int actualType)
  {
    if (actualType >= MesQueues.Q_CBT_NEW_HIGH_VOLUME)
    {
      return actualType - MesQueues.Q_CBT_NEW_HIGH_VOLUME;
    }
    return actualType - MesQueues.Q_CBT_NEW;
  }

  /*
  ** private int getActualType(int genericType)
  **
  ** Maps a generic type to the actual queue type that should be used based
  ** on the actual type.
  **
  ** RETURNS: the queue type to use.
  */
  private int getActualType(int genericType)
  {
    int actualType = Integer.parseInt(fields.getData("type"));
    if (actualType >= MesQueues.Q_CBT_NEW_HIGH_VOLUME)
    {
      return MesQueues.Q_CBT_NEW_HIGH_VOLUME + genericType;
    }
    return MesQueues.Q_CBT_NEW + genericType;
  }
  
  /*
  ** private void handleDocQueue(int oldGenType, int newGenType)
  **   throws Exception
  **
  ** Handles doc queue item removal/insertion when moving the credit queue
  ** item in or out of the decline/cancel queues.
  */
  private void handleDocQueue(long id, int oldGenType, int newGenType)
    throws Exception
  {
    // if cancel/decline remove the doc item
    if (newGenType == GTQ_DECLINED || newGenType == GTQ_CANCELED)
    {
      CbtDocumentQueue.removeFromQueue(id,user);
    }
    // else place back in the appropriate doc queue if "uncanceling/undeclining"
    else if (oldGenType == GTQ_DECLINED || oldGenType == GTQ_CANCELED)
    {
      int incompleteCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:314^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*)
//          
//          from    q_doc_data
//          where   id = :id
//                  and doc_required <> doc_received
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)\n         \n        from    q_doc_data\n        where   id =  :1 \n                and doc_required <> doc_received";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.CbtCreditDecision",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   incompleteCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:321^7*/
      int docQueueId = (incompleteCount > 0 ? 630 : 631);
      QueueTools.insertQueue(id,docQueueId,user);
    }
  }
  
  /*
  ** private void updateMerchCreditStatus(long appSeqNum, int genericType)
  **
  ** Updates the merch_credit_status field in merchant to a new status.
  **
  ** NOTE: connect() and cleanUp() NOT called.
  **
  ** RETURNS: true if success, else false.
  */
  private void updateMerchCreditStatus(long appSeqNum, int genericType)
  {
    int newStatus = QueueConstants.CBT_CREDIT_NEW;
    switch (genericType)
    {
      case GTQ_NEW:
        newStatus = QueueConstants.CBT_CREDIT_NEW;
        break;
        
      case GTQ_PENDING:
        newStatus = QueueConstants.CBT_CREDIT_PEND;
        break;
        
      case GTQ_DECLINED:
        newStatus = QueueConstants.CBT_CREDIT_DECLINE;
        break;
        
      case GTQ_CANCELED:
        newStatus = QueueConstants.CBT_CREDIT_CANCEL;
        break;
        
      case GTQ_APPROVED:
        newStatus = QueueConstants.CBT_REVIEW_COMPLETE;
        return;
    }
    
    try
    {
      // determine if the status should be modified
      // (not an mes production status yet)
      int curStatus = -1;
      /*@lineinfo:generated-code*//*@lineinfo:367^7*/

//  ************************************************************
//  #sql [Ctx] { select  merch_credit_status
//          
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_credit_status\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.CbtCreditDecision",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   curStatus = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:373^7*/
      
      // if currently a cbt specific status, then update it
      if (curStatus >= QueueConstants.CBT_CREDIT_NEW &&
          curStatus <= QueueConstants.CBT_REVIEW_COMPLETE)
      {
        // update the merch_credit_status column in the merchant table
        /*@lineinfo:generated-code*//*@lineinfo:380^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_credit_status = :newStatus
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n          set     merch_credit_status =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.CbtCreditDecision",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,newStatus);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:385^9*/
      }
    }
    catch (Exception e)
    {
      logEntry("updateMerchCreditStatus(" + appSeqNum + ", " + newStatus + ")",
        e.toString());
    }
  }

  /*
  ** public boolean autoSubmit()
  **
  ** Called during bean auto set.  Stores the credit data.
  **
  ** RETURNS: true if submit ok, else false.
  */
  public boolean autoSubmit()
  {
    _isAutoSubmitOk = false;

    try
    {
      int     newGenType  = -1;
      int     oldType     = Integer.parseInt(fields.getData("type"));
      int     oldGenType  = getGenericType(oldType);
      int     newType     = -1;
      long    id          = Long.parseLong(fields.getData("id"));
      String codeString   = "";
      
      // determine new type and reason code string based on submit type
      if (autoSubmitName.equals("pend"))
      {
        codeString        = makeCodeString("pendReasons");
        newGenType        = GTQ_PENDING;
      }
      else if (autoSubmitName.equals("decline"))
      {
        codeString        = makeCodeString("declineReasons");
        newGenType        = GTQ_DECLINED;
      }
      else if (autoSubmitName.equals("cancel"))
      {
        codeString        = makeCodeString("cancelReasons");
        newGenType        = GTQ_CANCELED;
      }
      else if (autoSubmitName.equals("approve"))
      {
        newGenType        = GTQ_APPROVED;
      }
      else if (autoSubmitName.equals("new"))
      {
        newGenType        = GTQ_NEW;
      }
      else
      {
        newGenType        = oldGenType;
      }
      newType             = getActualType(newGenType);

      // insert/update q_credit_data record
      connect();

      try
      {
        // assume this is new credit data, try to insert
        /*@lineinfo:generated-code*//*@lineinfo:451^9*/

//  ************************************************************
//  #sql [Ctx] { insert into q_credit_data
//            ( id,
//              codes )
//            values
//            ( :id,
//              :codeString )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_credit_data\n          ( id,\n            codes )\n          values\n          (  :1 ,\n             :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ops.CbtCreditDecision",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setString(2,codeString);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:459^9*/
      }
      catch (Exception e)
      {
        // record for this credit item must already exist, update it
        /*@lineinfo:generated-code*//*@lineinfo:464^9*/

//  ************************************************************
//  #sql [Ctx] { update  q_credit_data
//            set     codes = :codeString
//            where   id = :id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  q_credit_data\n          set     codes =  :1 \n          where   id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ops.CbtCreditDecision",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,codeString);
   __sJT_st.setLong(2,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:469^9*/
      }
      
      // if a newType is different than old type, use QueueTools to move
      // the queue item from the old queue to the new queue 
      if (oldType != newType)
      {
        // take action as required on the documentation queue item
        /*  No longer necessary due to new routing changes
        handleDocQueue(id,oldGenType,newGenType);
        */

        // move the queue item from old to new queue
        QueueTools.moveQueueItem(id,oldType,newType,user,null);

        // set the queue type in the type field to be the new queue
        String newTypeString = Integer.toString(newType);
        fields.setData("type",newTypeString);

        // add a queue type override flag to the request
        request.setAttribute("overrideType",newTypeString);
        
        // reset the checkboxes corresponding with the old type
        resetTypeFields(oldType);
        
        // update the merch credit status
        updateMerchCreditStatus(id,newGenType);
        
        // place into documentation queue if approving
        if (newGenType == GTQ_APPROVED)
        {
          long sourceUserId = 0L;
          /*@lineinfo:generated-code*//*@lineinfo:501^11*/

//  ************************************************************
//  #sql [Ctx] { select  u.user_id
//              
//              from    q_data qd,
//                      users  u
//              where   qd.id = :id
//                      and qd.type = :newType
//                      and qd.item_type = : MesQueues.Q_ITEM_TYPE_CBT_CREDIT
//                      and qd.source = u.login_name
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  u.user_id\n             \n            from    q_data qd,\n                    users  u\n            where   qd.id =  :1 \n                    and qd.type =  :2 \n                    and qd.item_type =  :3 \n                    and qd.source = u.login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.CbtCreditDecision",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,newType);
   __sJT_st.setInt(3, MesQueues.Q_ITEM_TYPE_CBT_CREDIT);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sourceUserId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:511^11*/
          
          UserBean sourceUser = new UserBean();
          sourceUser.getData(sourceUserId);
            
          InsertCreditQueue icq = new InsertCreditQueue();
          try
          {
            icq.connect();
            //icq.addToCbtDocQueue(id,sourceUser);
            //icq.addToMesCreditQueue(id,sourceUser);
          }
          catch (Exception e) {}
          finally
          {
            icq.cleanUp();
          }
        }
      }

      _isAutoSubmitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::autoSubmit(): " + e);
      logEntry("autoSubmit()",e.toString());
    }
    finally
    {
      cleanUp();
    }

    return _isAutoSubmitOk;
  }

  /*
  ** public void loadCodes(int queueType, String codeString)
  **
  ** Determines a field name prefix to use based on queueType, then
  ** parses codeString and sets each corresponding checkbox field whose
  ** name begins with the given prefix.
  */
  public void loadCodes(int queueType, String codeString)
  {
    String prefix = null;
    switch (queueType)
    {
      case MesQueues.Q_CBT_PENDING:
      case MesQueues.Q_CBT_PENDING_HIGH_VOLUME:
        prefix = "pend";
        break;

      case MesQueues.Q_CBT_CANCELLED:
      case MesQueues.Q_CBT_CANCELLED_HIGH_VOLUME:
        prefix = "cancel";
        break;

      case MesQueues.Q_CBT_DECLINED:
      case MesQueues.Q_CBT_DECLINED_HIGH_VOLUME:
        prefix = "decline";
        break;

      case MesQueues.Q_CBT_APPROVED:
      case MesQueues.Q_CBT_APPROVED_HIGH_VOLUME:
        prefix = "docitem";
        break;

      default:
        break;
    }

    if (prefix != null && codeString != null && codeString.length() > 0)
    {
      StringTokenizer tok = new StringTokenizer(codeString,",");
      while (tok.hasMoreTokens())
      {
        String code = tok.nextToken();
        Field field = fields.getField(prefix + "_" + code);
        if (field != null)
        {
          field.setData("y");
        }
      }
    }
  }

  /*
  ** public boolean autoLoad()
  **
  ** Called during bean auto set.  Loads the credit data.
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean autoLoad()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    _isAutoLoadOk = false;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:616^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  codes
//          from    q_credit_data
//          where   id = :fields.getData("id")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_734 = fields.getData("id");
  try {
   String theSqlTS = "select  codes\n        from    q_credit_data\n        where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.CbtCreditDecision",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_734);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.CbtCreditDecision",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:621^7*/
      rs = it.getResultSet();
      if (rs.next())
      {
        String codeString = rs.getString("codes");
        int queueType     = Integer.parseInt(fields.getData("type"));
        loadCodes(queueType,codeString);
      }
      _isAutoLoadOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::autoLoad(): " + e);
      logEntry("autoLoad()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }

    return _isAutoLoadOk;
  }
}/*@lineinfo:generated-code*/