/*@lineinfo:filename=ScheduleBaseBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ScheduleBaseBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/03/02 4:36p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class ScheduleBaseBean extends SQLJConnectionBase
{

  //schedule types
  public static final int     SCHEDULE_TYPE_ACTIVATION        = 1;

  //defaults to very first day of the month
  public static final int     DEFAULT_MONTH                   = 0;
  public static final int     DEFAULT_DATE                    = 1;
  public static final int     DEFAULT_HOUR                    = 0;
  public static final int     DEFAULT_MINUTE                  = 0;
  public static final int     DEFAULT_SECOND                  = 0;

  public static final String  NOT_VALID_USER                  = "not_valid";
  public static final String  VALID_ADMIN                     = "admin";
  public static final String  VALID_ADMIN_USER                = "admin_user";
  public static final String  VALID_USER                      = "user";

  public static final int     DEFAULT_SCHEDULE_TYPE           = SCHEDULE_TYPE_ACTIVATION;
  public static final int     DEFAULT_TIME_INTERVAL           = 30;  //in minutes
  public static final int     DEFAULT_SCHEDULE_START_OF_DAY   = 0;  //FIRST HOUR OF DAY 24 HOUR CLOCK
  public static final int     DEFAULT_SCHEDULE_END_OF_DAY     = 23;  //LAST HOUR OF DAY 24 HOUR CLOCK
  public static final String  DEFAULT_SCHEDULE_DESCRIPTION    = "Activation Schedule"; 
  public static final String  DEFAULT_SCHEDULE_FUNCTION_URL   = "/jsp/maintenance/view_account.jsp?merchant="; 

  //base info that pertains to schedule type
  private int                 scheduleType                    = DEFAULT_SCHEDULE_TYPE;
  private int                 timeInterval                    = DEFAULT_TIME_INTERVAL;
  private String              scheduleDesc                    = DEFAULT_SCHEDULE_DESCRIPTION;
  private String              functionUrl                     = DEFAULT_SCHEDULE_FUNCTION_URL;
  private int                 scheduleStartOfDay              = DEFAULT_SCHEDULE_START_OF_DAY;
  private int                 scheduleEndOfDay                = DEFAULT_SCHEDULE_END_OF_DAY;
  private Vector              users                           = new Vector();
  private Calendar            cal                             = null;

  public ScheduleBaseBean()
  {
    initializeBaseBean(DEFAULT_SCHEDULE_TYPE);
  }

  public ScheduleBaseBean(int scheduleType)
  {
    initializeBaseBean(scheduleType);
  }

  protected void initializeBaseBean(int scheduleType)
  {
    cal = Calendar.getInstance();
    setTypeInfo(scheduleType);  //gets general info for this schedule type
    getUserList();              //gets the users for this schedule type
  }

  public static long getCurrentTimeLong()
  {
    Calendar c = Calendar.getInstance();
    return (c.getTime()).getTime();
  }

  public static String getCurrentTimeStr()
  {
    Calendar c = Calendar.getInstance();
    return (c.getTime()).toString();
  }


  public String getCalendarMonthDesc()
  {
    return decodeMonth(cal.get(cal.MONTH));
  }
  public String getCalendarMonthDesc(int month)
  {
    return decodeMonth(month);
  }
  public int getCalendarMonth()
  {
    return cal.get(cal.MONTH);
  }


  public String getCalendarAmPmDesc()
  {
    return decodeAmPm(cal.get(cal.AM_PM));
  }
  public String getCalendarAmPmDesc(int ampm)
  {
    return decodeAmPm(ampm);
  }
  public int getCalendarAmPm()
  {
    return cal.get(cal.AM_PM);
  }



  public String getCalendarDayOfWeekDesc()
  {
    return decodeDay(cal.get(cal.DAY_OF_WEEK));
  }
  public String getCalendarDayOfWeekDesc(int dayOfWeek)
  {
    return decodeDay(dayOfWeek);
  }
  public int getCalendarDayOfWeek()
  {
    return cal.get(cal.DAY_OF_WEEK);
  }


  public int getCalendarDay()
  {
    return cal.get(cal.DAY_OF_MONTH);
  }

  public int getCalendarYear()
  {
    return cal.get(cal.YEAR);
  }


  public int getCalendar12Hour()
  {
    int result = cal.get(cal.HOUR);

    if(result == 0)
    {
      result = 12;
    }

    return result;
  }

  public int getCalendar24Hour()
  {
    return cal.get(cal.HOUR_OF_DAY);
  }


  public int getCalendarMinute()
  {
    return cal.get(cal.MINUTE);
  }

  public int getCalendarSecond()
  {
    return cal.get(cal.SECOND);
  }

  public int getCalendarMillisecond()
  {
    return cal.get(cal.MILLISECOND);
  }

  public long getLongCalendarTime()
  {
    return getLongTimeOfCalendar();
  }

  protected void clearCalendar()
  {
    cal.clear();
  }

  protected void setCalendarField(int field, int value)
  {
    cal.set(field, value);
  }
              
  protected void setCalendarDate(long millisecs)
  {
    cal.clear();
    cal.setTime(new Date(millisecs));
  }              
                   
  protected void setCalendarDate(int selectedYear)
  {
    cal.clear();
    cal.set(selectedYear, DEFAULT_MONTH, DEFAULT_DATE, DEFAULT_HOUR, DEFAULT_MINUTE, DEFAULT_SECOND);
  }

  protected void setCalendarDate(int selectedYear, int selectedMonth)
  {
    cal.clear();
    cal.set(selectedYear, selectedMonth, DEFAULT_DATE, DEFAULT_HOUR, DEFAULT_MINUTE, DEFAULT_SECOND);
  }

  protected void setCalendarDate(int selectedYear, int selectedMonth, int selectedDate)
  {
    cal.clear();
    cal.set(selectedYear, selectedMonth, selectedDate, DEFAULT_HOUR, DEFAULT_MINUTE, DEFAULT_SECOND);
  }

  protected void setCalendarDate(int selectedYear, int selectedMonth, int selectedDate, int selectedHour)
  {
    cal.clear();
    cal.set(selectedYear, selectedMonth, selectedDate, selectedHour, DEFAULT_MINUTE, DEFAULT_SECOND);
  }

  protected void setCalendarDate(int selectedYear, int selectedMonth, int selectedDate, int selectedHour, int selectedMinute)
  {
    cal.clear();
    cal.set(selectedYear, selectedMonth, selectedDate, selectedHour, selectedMinute, DEFAULT_SECOND);
  }

  protected void setCalendarDate(int selectedYear, int selectedMonth, int selectedDate, int selectedHour, int selectedMinute, int selectedSecond)
  {
    cal.clear();
    cal.set(selectedYear, selectedMonth, selectedDate, selectedHour, selectedMinute, selectedSecond);
  }

  protected int getCalendarFieldValue(int field)
  {
    return cal.get(field);
  }

  protected void addToCalendarField(int field, int amount)
  {
    cal.add(field, amount);
  }

  protected void rollCalendarFieldOneUnit(int field, boolean up)
  {
    cal.roll(field, up); 
  }
    
  protected void rollCalendarFieldByAmount(int field, int amount)
  {
    cal.roll(field, amount);
  } 

  protected long getLongTimeOfCalendar()
  {
    return (cal.getTime()).getTime();
  }

  protected String getCalendarString()
  {
    return cal.toString();
  }

  protected String getCalendarDateString()
  {
    return (cal.getTime()).toString();
  }


  protected int getActualMaximum(int field)
  {
    return cal.getActualMaximum(field);
  }


  protected String fixZeroTimeIssue(int num)
  {
    String result = "";
    
    try
    {
      result = Integer.toString(num);
    }
    catch(Exception e)
    {}

    if(num == 0)
    {
      result += "0";
    }

    return result;
    
  }

  private String decodeAmPm(int ampm)
  {
    String result = "";

    switch(ampm)
    {
      case Calendar.AM:
        result = "AM";
      break;
      case Calendar.PM:
        result = "PM";
      break;
    }

    return result;
  }

  private String decodeMonth(int month)
  {
    String result = "";

    switch(month)
    {
      case Calendar.JANUARY:
        result = "January";
      break;
      case Calendar.FEBRUARY:
        result = "February";
      break;
      case Calendar.MARCH:
        result = "March";
      break;
      case Calendar.APRIL:
        result = "April";
      break;
      case Calendar.MAY:
        result = "May";
      break;
      case Calendar.JUNE:
        result = "June";
      break;
      case Calendar.JULY:
        result = "July";
      break;
      case Calendar.AUGUST:
        result = "August";
      break;
      case Calendar.SEPTEMBER:
        result = "September";
      break;
      case Calendar.OCTOBER:
        result = "October";
      break;
      case Calendar.NOVEMBER:
        result = "November";
      break;
      case Calendar.DECEMBER:
        result = "December";
      break;
    }

    return result;
  }

  private String decodeDay(int day)
  {
    String result = "";

    switch(day)
    {
      case Calendar.SUNDAY:
        result = "Sunday";
      break;
      case Calendar.MONDAY:
        result = "Monday";
      break;
      case Calendar.TUESDAY:
        result = "Tuesday";
      break;
      case Calendar.WEDNESDAY:
        result = "Wednesday";
      break;
      case Calendar.THURSDAY:
        result = "Thursday";
      break;
      case Calendar.FRIDAY:
        result = "Friday";
      break;
      case Calendar.SATURDAY:
        result = "Saturday";
      break;
    }

    return result;
  }



/*
  gets users for this schedule type.
*/  
  private void getUserList()
  {
    ResultSetIterator   it         = null;
    ResultSet           rs         = null;

    try
    {
      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:425^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   * 
//          from     schedule_user  
//          where    type = :this.scheduleType and
//                   user_type != :VALID_ADMIN
//          order by user_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   * \n        from     schedule_user  \n        where    type =  :1  and\n                 user_type !=  :2 \n        order by user_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.ScheduleBaseBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.scheduleType);
   __sJT_st.setString(2,VALID_ADMIN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.ScheduleBaseBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:432^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        users.add(rs.getString("user_id"));
      }

      rs.close();
      it.close();

    }
    catch(Exception e)
    {
      logEntry("getUserList()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }

  public void setUserList(String userName)
  {
    users = new Vector();
    users.add(userName);
  }

  public static String getUserTypeStatic(String userName, int type)
  {
    ScheduleBaseBean sbb = new ScheduleBaseBean();
    return sbb.getUserType(userName,type);
  }

  public String getUserType(String userName, int type)
  {
    ResultSetIterator   it         = null;
    ResultSet           rs         = null;
    String              result     = NOT_VALID_USER;

    try
    {
      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:478^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   user_type 
//          from     schedule_user  
//          where    type     = :type and 
//                   user_id  = :userName
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   user_type \n        from     schedule_user  \n        where    type     =  :1  and \n                 user_id  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.ScheduleBaseBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,type);
   __sJT_st.setString(2,userName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.ScheduleBaseBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:484^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = rs.getString("user_type");
      }

      rs.close();
      it.close();

    }
    catch(Exception e)
    {
      logEntry("getUserType()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

/*
  setTypeInfo general info about the schedule type itself.
*/

  private void setTypeInfo(int scheduleType)
  {

    try
    {
      connect();
     
        /*@lineinfo:generated-code*//*@lineinfo:520^9*/

//  ************************************************************
//  #sql [Ctx] { select  type,
//                    time_interval,
//                    description,
//                    function_url, 
//                    hour_range_low, 
//                    hour_range_high   
//            
//            from    schedule_type
//            where   type = :scheduleType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  type,\n                  time_interval,\n                  description,\n                  function_url, \n                  hour_range_low, \n                  hour_range_high   \n           \n          from    schedule_type\n          where   type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.ScheduleBaseBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,scheduleType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 6) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(6,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.scheduleType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   this.timeInterval = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   this.scheduleDesc = (String)__sJT_rs.getString(3);
   this.functionUrl = (String)__sJT_rs.getString(4);
   this.scheduleStartOfDay = __sJT_rs.getInt(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   this.scheduleEndOfDay = __sJT_rs.getInt(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:536^9*/

    }
    catch(Exception e)
    {
      logEntry("setTypeInfo()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  public String getScheduleDesc()
  {
    return this.scheduleDesc;
  }
  
  public int getScheduleType()
  {
    return this.scheduleType;
  }

  public int getTimeInterval()
  {
    return this.timeInterval;
  }

  public int getScheduleStartOfDay()
  {
    return this.scheduleStartOfDay;
  }
  
  public int getScheduleEndOfDay()
  {
    return this.scheduleEndOfDay;
  }

  
  public String getFunctionUrl()
  {
    return this.functionUrl;
  }

  public int getUsersSize()
  {
    return this.users.size();
  }
  
  public String getUser(int idx)
  {
    return (String)this.users.elementAt(idx);
  }

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }

}/*@lineinfo:generated-code*/