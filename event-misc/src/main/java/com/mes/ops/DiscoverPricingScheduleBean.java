/*@lineinfo:filename=DiscoverPricingScheduleBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/DiscoverPricingScheduleBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/14/04 1:42p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.util.Date;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class DiscoverPricingScheduleBean extends SQLJConnectionBase
{
  private ResultSetIterator it                          = null;

  private String            dba                         = "";
  private String            merchantNum                 = "";
  private String            controlNum                  = "";

  private String            addressLine1                = "";
  private String            addressLine2                = "";
  private String            addressCity                 = "";
  private String            addressState                = "";
  private String            addressZip                  = "";
  
  private String            contactName                 = "";
  private String            contactPhone                = "";
  private String            owner1Name                  = "";
  private String            owner2Name                  = "";

  private String            fixedRate                   = "n/a";
  private String            perItemPlan                 = "n/a";
  private String            minimumMonthlyDiscount      = "n/a";
  private String            discountType                = "";

  private String            dinersFee                   = "n/a";
  private String            discoverFee                 = "n/a";
  private String            jcbFee                      = "n/a";
  private String            amexFee                     = "n/a";
  private String            debitFee                    = "n/a";
  private String            ebtFee                      = "n/a";
  private String            dialPayFee                  = "n/a";

  private String            chargebackFee               = "n/a";
  private String            internetServiceMonthlyFee   = "n/a";
  private String            internetServiceSetupFee     = "n/a";
  private String            helpDeskFee                 = "n/a";
  private String            merchantStatementFee        = "n/a";
  private String            webReportingFee             = "n/a";
  private String            newAccountAppFee            = "n/a";
  private String            newAccountSetupFee          = "n/a";
  private String            dialPayMonthlyFee           = "n/a";
  private String            multiMerchantFee            = "n/a";
  private String            accountServicingMonthlyFee  = "n/a";

  public DiscoverPricingScheduleBean()
  {
  }
 
  public String getDate()
  {
    DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
    return df.format(new Date());
  }
  
  private String buildMiscFee(ResultSet rs)
  {
    StringBuffer msg = new StringBuffer("");
    
    try
    {
      if( isBlank(rs.getString("misc_chrg_amount")) )
      {
        msg.append("n/a");
      }
      else
      {
        msg.append(MesMath.toCurrency(rs.getDouble("misc_chrg_amount")));
        msg.append(" ");
        msg.append(rs.getString("waive_message"));
      }
    }
    catch(Exception e)
    {
      logEntry("buildMiscFee()", e.toString());
    }
    
    return ( msg.toString() );
  }
  

  /*
  ** METHOD public void getData()
  **
  */
  public void getData(long primaryKey)
  {
    ResultSet rs = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:138^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code,
//                  tranchrg_mmin_chrg, 
//                  tranchrg_disc_rate,
//                  tranchrg_pass_thru,
//                  tranchrg_per_tran
//          from    tranchrg
//          where   app_seq_num  = :primaryKey    
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code,\n                tranchrg_mmin_chrg, \n                tranchrg_disc_rate,\n                tranchrg_pass_thru,\n                tranchrg_per_tran\n        from    tranchrg\n        where   app_seq_num  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.DiscoverPricingScheduleBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.DiscoverPricingScheduleBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:147^7*/
        
      rs = it.getResultSet();

      while(rs.next())
      {
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_VISA:
            fixedRate               = !isBlank(rs.getString("tranchrg_disc_rate")) ? (rs.getString("tranchrg_disc_rate") + " %") : "n/a";
            perItemPlan             = !isBlank(rs.getString("tranchrg_pass_thru")) ? MesMath.toCurrency(rs.getDouble("tranchrg_pass_thru")) : "n/a";
            minimumMonthlyDiscount  = !isBlank(rs.getString("tranchrg_mmin_chrg")) ? MesMath.toCurrency(rs.getDouble("tranchrg_mmin_chrg")) : "n/a";
          break;
          
          case mesConstants.APP_CT_DINERS_CLUB:
            dinersFee   = !isBlank(rs.getString("tranchrg_per_tran")) ? MesMath.toCurrency(rs.getDouble("tranchrg_per_tran")) : "n/a";
          break;
          
          case mesConstants.APP_CT_DISCOVER:
            discoverFee = !isBlank(rs.getString("tranchrg_per_tran")) ? MesMath.toCurrency(rs.getDouble("tranchrg_per_tran")) : "n/a";
          break;
          
          case mesConstants.APP_CT_JCB:
            jcbFee      = !isBlank(rs.getString("tranchrg_per_tran")) ? MesMath.toCurrency(rs.getDouble("tranchrg_per_tran")) : "n/a";
          break;
          
          case mesConstants.APP_CT_AMEX:
            amexFee     = !isBlank(rs.getString("tranchrg_per_tran")) ? MesMath.toCurrency(rs.getDouble("tranchrg_per_tran")) : "n/a";
          break;
          
          case mesConstants.APP_CT_DEBIT:
            debitFee    = !isBlank(rs.getString("tranchrg_per_tran")) ? MesMath.toCurrency(rs.getDouble("tranchrg_per_tran")) : "n/a";
          break;
          
          case mesConstants.APP_CT_EBT:
            ebtFee      = !isBlank(rs.getString("tranchrg_per_tran")) ? MesMath.toCurrency(rs.getDouble("tranchrg_per_tran")) : "n/a";
          break;
          
          case mesConstants.APP_CT_DIAL_PAY:
            dialPayFee  = !isBlank(rs.getString("tranchrg_per_tran")) ? MesMath.toCurrency(rs.getDouble("tranchrg_per_tran")) : "n/a";
            break;
            
          default:
          break;
        }
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:197^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mc.misc_code,
//                  mc.misc_chrg_amount,
//                  nvl(waive.waive_message, ' ') waive_message
//          from    miscchrg mc,
//                  (
//                    select    apw.charge_type                               charge_type,
//                              '(Waived for '||apw.waive_months||' months)'  waive_message
//                    from      app_promotion_waivers apw,
//                              application app
//                    where     app.app_seq_num = :primaryKey and
//                              app.app_type = apw.app_type and
//                              apw.enabled = 'Y' and
//                              app.app_created_date between apw.start_date and apw.end_date
//                  ) waive
//          where   mc.app_seq_num  = :primaryKey and
//                  mc.misc_code = waive.charge_type(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mc.misc_code,\n                mc.misc_chrg_amount,\n                nvl(waive.waive_message, ' ') waive_message\n        from    miscchrg mc,\n                (\n                  select    apw.charge_type                               charge_type,\n                            '(Waived for '||apw.waive_months||' months)'  waive_message\n                  from      app_promotion_waivers apw,\n                            application app\n                  where     app.app_seq_num =  :1  and\n                            app.app_type = apw.app_type and\n                            apw.enabled = 'Y' and\n                            app.app_created_date between apw.start_date and apw.end_date\n                ) waive\n        where   mc.app_seq_num  =  :2  and\n                mc.misc_code = waive.charge_type(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.DiscoverPricingScheduleBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.DiscoverPricingScheduleBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:215^7*/
        
      rs = it.getResultSet();

      while(rs.next())
      {
            
        switch(rs.getInt("misc_code"))
        {
          case mesConstants.APP_MISC_CHARGE_CHARGEBACK:
            chargebackFee               = buildMiscFee(rs);
            break;
          
          case mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE:
            internetServiceMonthlyFee   = buildMiscFee(rs);
            break;
          
          case mesConstants.APP_MISC_CHARGE_INTERNET_SERVICE_SETUP:
            internetServiceSetupFee     = buildMiscFee(rs);
            break;
          
          case mesConstants.APP_MISC_CHARGE_HELP_DESK:
            helpDeskFee                 = buildMiscFee(rs);
            break;
          
          case mesConstants.APP_MISC_CHARGE_MONTHLY_STATEMENT:
            merchantStatementFee        = buildMiscFee(rs);
            break;
          
          case mesConstants.APP_MISC_CHARGE_WEB_REPORTING:
            webReportingFee             = buildMiscFee(rs);
            break;
          
          case mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_APP:
            newAccountAppFee            = buildMiscFee(rs);
            break;
          
          case mesConstants.APP_MISC_CHARGE_NEW_ACCOUNT_SETUP:
            newAccountSetupFee          = buildMiscFee(rs);
            break;
          
          case mesConstants.APP_MISC_CHARGE_DIALPAY_MONTLY_FEE:
            dialPayMonthlyFee           = buildMiscFee(rs);
            break;
            
          case mesConstants.APP_MISC_CHARGE_MULTI_MERCHANT_FEE:
            multiMerchantFee            = buildMiscFee(rs);
            break;
            
          case mesConstants.APP_MISC_CHARGE_ACCOUNT_SERVICING:
            accountServicingMonthlyFee  = buildMiscFee(rs);
            break;
            
          default:
            break;
        }
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:276^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_number,
//                  merc_cntrl_number,
//                  merch_business_name,
//                  merch_dly_discount_flag
//          from    merchant
//          where   app_seq_num  = :primaryKey    
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_number,\n                merc_cntrl_number,\n                merch_business_name,\n                merch_dly_discount_flag\n        from    merchant\n        where   app_seq_num  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.DiscoverPricingScheduleBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.DiscoverPricingScheduleBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:284^7*/
        
      rs = it.getResultSet();

      if(rs.next())
      {
        
        dba             = rs.getString("merch_business_name");
        merchantNum     = rs.getString("merch_number");
        controlNum      = rs.getString("merc_cntrl_number");
        discountType    = isBlank(rs.getString("merch_dly_discount_flag")) || rs.getString("merch_dly_discount_flag").equals("N") ? "Monthly Discount" : "Daily Discount";

      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:301^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchcont_prim_first_name || ' ' || merchcont_prim_last_name  contact_name,
//                  merchcont_prim_phone                                          contact_phone
//          from    merchcontact
//          where   app_seq_num  = :primaryKey    
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchcont_prim_first_name || ' ' || merchcont_prim_last_name  contact_name,\n                merchcont_prim_phone                                          contact_phone\n        from    merchcontact\n        where   app_seq_num  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.DiscoverPricingScheduleBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.DiscoverPricingScheduleBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:307^7*/
        
      rs = it.getResultSet();

      if(rs.next())
      {
        contactName     = isBlank(rs.getString("CONTACT_NAME"))   ? "" : rs.getString("CONTACT_NAME").trim();
        contactPhone    = isBlank(rs.getString("CONTACT_PHONE"))  ? "" : formatPhone(rs.getString("CONTACT_PHONE"));
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:320^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_first_name || ' ' || busowner_last_name  owner_name,
//                  busowner_num                                      owner_num
//          from    businessowner
//          where   app_seq_num  = :primaryKey      and
//                  busowner_first_name is not null and
//                  busowner_last_name  is not null 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_first_name || ' ' || busowner_last_name  owner_name,\n                busowner_num                                      owner_num\n        from    businessowner\n        where   app_seq_num  =  :1       and\n                busowner_first_name is not null and\n                busowner_last_name  is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.DiscoverPricingScheduleBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.DiscoverPricingScheduleBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:328^7*/
        
      rs = it.getResultSet();

      while(rs.next())
      {

        switch(rs.getInt("OWNER_NUM"))
        {
          case 1:
            owner1Name     = isBlank(rs.getString("owner_name"))   ? "" : rs.getString("owner_name").trim();
          break;
          case 2:  
            owner2Name     = isBlank(rs.getString("owner_name"))   ? "" : rs.getString("owner_name").trim();
          break;
        }

      }   

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:350^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1,
//                  address_line2,
//                  address_city,
//                  address_zip,
//                  countrystate_code 
//          from    address
//          where   app_seq_num  = :primaryKey  and
//                  addresstype_code = 1 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1,\n                address_line2,\n                address_city,\n                address_zip,\n                countrystate_code \n        from    address\n        where   app_seq_num  =  :1   and\n                addresstype_code = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.DiscoverPricingScheduleBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.DiscoverPricingScheduleBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:360^7*/
        
      rs = it.getResultSet();

      if(rs.next())
      {

        addressLine1  = isBlank(rs.getString("address_line1"))     ? "" : rs.getString("address_line1").trim();
        addressLine2  = isBlank(rs.getString("address_line2"))     ? "" : rs.getString("address_line2").trim();
        addressCity   = isBlank(rs.getString("address_city"))      ? "" : rs.getString("address_city").trim();
        addressState  = isBlank(rs.getString("countrystate_code")) ? "" : rs.getString("countrystate_code").trim();
        addressZip    = isBlank(rs.getString("address_zip"))       ? "" : rs.getString("address_zip").trim();
      }   

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void setCoverSheetTimeStamp(String primaryKey)
  {
    try
    {
      setCoverSheetTimeStamp(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
      logEntry("setCoverSheetTimeStamp(" + primaryKey + ")", e.toString());
    }
  }
  public void setCoverSheetTimeStamp(long primaryKey)
  {

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:407^7*/

//  ************************************************************
//  #sql [Ctx] { update  packing_slip_info
//          set     coversheet_printed    =    sysdate
//          where   app_seq_num           =    :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  packing_slip_info\n        set     coversheet_printed    =    sysdate\n        where   app_seq_num           =     :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.ops.DiscoverPricingScheduleBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:412^7*/
    }
    catch(Exception e)
    {
      logEntry("setCoverSheetTimeStamp()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void setPricingScheduleTimeStamp(String primaryKey)
  {
    try
    {
      setPricingScheduleTimeStamp(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
      logEntry("setPricingScheduleTimeStamp(" + primaryKey + ")", e.toString());
    }
  }
  public void setPricingScheduleTimeStamp(long primaryKey)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:441^7*/

//  ************************************************************
//  #sql [Ctx] { update  packing_slip_info
//          set     pricingschedule_printed   =    sysdate
//          where   app_seq_num               =    :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  packing_slip_info\n        set     pricingschedule_printed   =    sysdate\n        where   app_seq_num               =     :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ops.DiscoverPricingScheduleBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:446^7*/
    }
    catch(Exception e)
    {
      logEntry("setPricingScheduleTimeStamp()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private String formatPhone(String phone)
  {
    if(phone.length() != 10)
    {
      return phone;
    }
    else
    {
      String f3 = phone.substring(0,3);
      String n3 = phone.substring(3,6);
      String l4 = phone.substring(6);

      return ("(" + f3 + ")" + " " + n3 + "-" + l4);
    }
  }


  public String getFixedRate()
  {
    return fixedRate;
  }

  public String getPerItemPlan()
  {
    return perItemPlan;
  }

  public String getMinimumMonthlyDiscount()
  {
    return minimumMonthlyDiscount;
  }

  public String getDinersFee()
  {
    return dinersFee;
  }

  public String getDiscoverFee()
  {
    return discoverFee;
  }

  public String getJcbFee()
  {
    return jcbFee;
  }

  public String getAmexFee()
  {
    return amexFee;
  }

  public String getDebitFee()
  {
    return debitFee;
  }

  public String getEbtFee()
  {
    return ebtFee;
  }
  
  public String getDialPayFee()
  {
    return dialPayFee;
  }
  
  public String getDialPayMonthlyFee()
  {
    return dialPayMonthlyFee;
  }
  
  public String getAccountServicingMonthlyFee()
  {
    return accountServicingMonthlyFee;
  }
  
  public String getMultiMerchantFee()
  {
    return multiMerchantFee;
  }


  public String getChargebackFee()
  {
    return chargebackFee;
  }
    
  public String getInternetServiceMonthlyFee()
  {
    return internetServiceMonthlyFee;
  }
  
  public String getInternetServiceSetupFee()
  {
    return internetServiceSetupFee;
  }
  
  public String getHelpDeskFee()
  {
    return helpDeskFee;
  }
  
  public String getMerchantStatementFee()
  {
    return merchantStatementFee;
  }
  
  public String getWebReportingFee()
  {
    return webReportingFee;
  }
  
  public String getNewAccountAppFee()
  {
    return newAccountAppFee;
  }
  
  public String getNewAccountSetupFee()
  {
    return newAccountSetupFee;
  }

  public String getDiscountType()
  {
    return discountType;
  }

  public String getDba()
  {
    return dba;
  }

  public String getMerchantNum()
  {
    return merchantNum;
  }

  public String getControlNum()
  {
    return controlNum;
  }

  public String getContactName()
  {
    return contactName.toUpperCase();
  }
  
  public String getContactPhone()
  {
    return contactPhone;
  }
  
  public String getOwner1Name()
  {
    return owner1Name.toUpperCase();
  }
  
  public String getOwner2Name()
  {
    return owner2Name.toUpperCase();
  }

  public String getAddressLine1()
  {
    return addressLine1;
  }

  public String getAddressLine2()
  {
    return addressLine2;
  }

  public String getAddressCity()
  {
    return addressCity;
  }

  public String getAddressState()
  {
    return addressState;
  }

  public String getAddressZip()
  {
    return addressZip;
  }


  public boolean isBlank(String test)
  {
    boolean result = false;

    if(test == null || test.equals("") || test.length() == 0)
    {
      result = true;
    }

    return result;
  }


}/*@lineinfo:generated-code*/