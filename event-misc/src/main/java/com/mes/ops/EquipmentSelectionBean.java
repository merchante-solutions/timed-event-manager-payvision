/*@lineinfo:filename=EquipmentSelectionBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/EquipmentSelectionBean.sqlj $

  Description:
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 11/08/02 4:33p $
  Version            : $Revision: 19 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class EquipmentSelectionBean extends InventoryBase
{
  private Vector equipToDeploy        = new Vector();
  private String model                = "";
  private int needed                  = 0;
  private String name                 = "";
  private String desc                 = "";
  private String lend                 = "";
  private String lendCode             = "";
  private String price                = "";
  private String merchantNum          = "";

  private String  serialNumber        = "";

  private boolean newSetup            = false;

  public EquipmentSelectionBean()
  {
  }

  public void findEquipment(String model, String serialNum, String mercNum)
  {
    ResultSetIterator     it      = null;

    try
    {
      connect();
      
      int merchTypeCode = getMerchTypeCode(mercNum);

      /*@lineinfo:generated-code*//*@lineinfo:71^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  class.ec_class_name,
//                  equip.equip_descriptor,
//                  mfgr.equipmfgr_mfr_name,
//                  inv.ei_serial_number,
//                  inv.ei_unit_cost,
//                  inv.ei_received_date,
//                  at1.inventory_owner_name
//          from    equip_class     class,
//                  equipment       equip,
//                  equipmfgr       mfgr,
//                  equip_inventory inv,
//                  app_type        at1
//          where   inv.ei_part_number    = :model and
//                  inv.ei_serial_number  = :serialNum and
//                  inv.ei_deployed_date  is null and
//                  inv.ei_owner          = :merchTypeCode and
//                  inv.ei_part_number    = equip.equip_model and
//                  class.ec_class_id     = inv.ei_class and
//                  equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and
//                  inv.ei_owner          = at1.app_type_code
//          order by inv.ei_received_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  class.ec_class_name,\n                equip.equip_descriptor,\n                mfgr.equipmfgr_mfr_name,\n                inv.ei_serial_number,\n                inv.ei_unit_cost,\n                inv.ei_received_date,\n                at1.inventory_owner_name\n        from    equip_class     class,\n                equipment       equip,\n                equipmfgr       mfgr,\n                equip_inventory inv,\n                app_type        at1\n        where   inv.ei_part_number    =  :1  and\n                inv.ei_serial_number  =  :2  and\n                inv.ei_deployed_date  is null and\n                inv.ei_owner          =  :3  and\n                inv.ei_part_number    = equip.equip_model and\n                class.ec_class_id     = inv.ei_class and\n                equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and\n                inv.ei_owner          = at1.app_type_code\n        order by inv.ei_received_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.EquipmentSelectionBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,model);
   __sJT_st.setString(2,serialNum);
   __sJT_st.setInt(3,merchTypeCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.EquipmentSelectionBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^7*/

      ResultSet rs = it.getResultSet();

      int i = 0;
      while(rs.next() && i < 12) //only get oldest 12...
      {
        EquipRec tempRec =  new EquipRec();
        tempRec.setSerialNumber(rs.getString("ei_serial_number"));
        tempRec.setProductName(rs.getString("equipmfgr_mfr_name"));
        tempRec.setProductDesc(rs.getString("equip_descriptor"));
        tempRec.setOwnerDesc(rs.getString("inventory_owner_name"));
        tempRec.setUnitCost(formatCurr(rs.getString("ei_unit_cost")));
        tempRec.setClassTypeDesc(rs.getString("ec_class_name"));
        tempRec.setReceiveDate(rs.getDate("ei_received_date"));
        equipToDeploy.add(tempRec);
        i++;
      }

      it.close();
    }
    catch(Exception e)
    {
      logEntry("findEquipment()", e.toString());
      addError("findEquipment: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  public String getMerchType(String mercNum)
  {
    String            result  = "unknown";

    connect();

    try
    {
      // see if there is an app in the system for this merchant
      int appCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:137^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant
//          where   merch_number = :mercNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant\n        where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.EquipmentSelectionBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,mercNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^7*/
      
      if(appCount > 0)
      {
        // get merch type from app_type
        /*@lineinfo:generated-code*//*@lineinfo:148^9*/

//  ************************************************************
//  #sql [Ctx] { select  at1.inventory_owner_name
//            
//            from    app_type    at1,
//                    application app,
//                    merchant    m,
//                    app_type    at2
//            where   m.merch_number = :mercNum and
//                    m.app_seq_num = app.app_seq_num and
//                    app.app_type = at2.app_type_code and
//                    at2.client_code = at1.app_type_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  at1.inventory_owner_name\n           \n          from    app_type    at1,\n                  application app,\n                  merchant    m,\n                  app_type    at2\n          where   m.merch_number =  :1  and\n                  m.app_seq_num = app.app_seq_num and\n                  app.app_type = at2.app_type_code and\n                  at2.client_code = at1.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.EquipmentSelectionBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,mercNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:160^9*/
      }
      else
      {
        // get merch type from mif and equip_default_owner
        /*@lineinfo:generated-code*//*@lineinfo:165^9*/

//  ************************************************************
//  #sql [Ctx] { select  edo.inventory_owner_name
//            
//            from    equip_default_owner   edo,
//                    mif                   m
//            where   m.merchant_number = :mercNum and
//                    m.bank_number = edo.bank_number          
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  edo.inventory_owner_name\n           \n          from    equip_default_owner   edo,\n                  mif                   m\n          where   m.merchant_number =  :1  and\n                  m.bank_number = edo.bank_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.EquipmentSelectionBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,mercNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:173^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("getMerchType()", e.toString());
      addError("getMerchType: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public int getMerchTypeCode(String merchantNumber)
  {
    int       result    = 0;
    boolean   wasStale  = false;
    
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      // see if there is an app in the system for this merchant
      int appCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:204^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant
//          where   merch_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant\n        where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.EquipmentSelectionBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^7*/
      
      if(appCount > 0)
      {
        // get merch type from app_type
        /*@lineinfo:generated-code*//*@lineinfo:215^9*/

//  ************************************************************
//  #sql [Ctx] { select  at1.app_type_code
//            
//            from    app_type    at1,
//                    application app,
//                    merchant    m,
//                    app_type    at2
//            where   m.merch_number = :merchantNumber and
//                    m.app_seq_num = app.app_seq_num and
//                    app.app_type = at2.app_type_code and
//                    at2.client_code = at1.app_type_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  at1.app_type_code\n           \n          from    app_type    at1,\n                  application app,\n                  merchant    m,\n                  app_type    at2\n          where   m.merch_number =  :1  and\n                  m.app_seq_num = app.app_seq_num and\n                  app.app_type = at2.app_type_code and\n                  at2.client_code = at1.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.EquipmentSelectionBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:227^9*/
      }
      else
      {
        // get merch type from mif and equip_default_owner
        /*@lineinfo:generated-code*//*@lineinfo:232^9*/

//  ************************************************************
//  #sql [Ctx] { select  edo.client_code
//            
//            from    equip_default_owner   edo,
//                    mif                   m
//            where   m.merchant_number = :merchantNumber and
//                    m.bank_number = edo.bank_number          
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  edo.client_code\n           \n          from    equip_default_owner   edo,\n                  mif                   m\n          where   m.merchant_number =  :1  and\n                  m.bank_number = edo.bank_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.EquipmentSelectionBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:240^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("getMerchTypeCode(" + merchantNumber + ")", e.toString());
      addError("getMerchType: " + e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
    
    return result;
  }


  public void getEquipment(String model, String mercNum)
  {
    ResultSetIterator   it      = null;
    
    try
    {
      connect();
      
      int merchTypeCode = getMerchTypeCode(mercNum);
      
      /*@lineinfo:generated-code*//*@lineinfo:270^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  class.ec_class_name,
//                  equip.equip_descriptor,
//                  mfgr.equipmfgr_mfr_name,
//                  inv.ei_serial_number,
//                  inv.ei_unit_cost,
//                  inv.ei_received_date,
//                  at1.inventory_owner_name
//          from    equipment equip, 
//                  equipmfgr mfgr, 
//                  equip_class class, 
//                  equip_inventory inv, 
//                  app_type at1 
//          where   inv.ei_part_number    = :model and
//                  inv.ei_deployed_date  is null and
//                  inv.ei_owner          = :merchTypeCode and
//                  inv.ei_part_number    = equip.equip_model and
//                  class.ec_class_id     = inv.ei_class and
//                  equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and
//                  at1.app_type_code = inv.ei_owner
//          order by inv.ei_received_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  class.ec_class_name,\n                equip.equip_descriptor,\n                mfgr.equipmfgr_mfr_name,\n                inv.ei_serial_number,\n                inv.ei_unit_cost,\n                inv.ei_received_date,\n                at1.inventory_owner_name\n        from    equipment equip, \n                equipmfgr mfgr, \n                equip_class class, \n                equip_inventory inv, \n                app_type at1 \n        where   inv.ei_part_number    =  :1  and\n                inv.ei_deployed_date  is null and\n                inv.ei_owner          =  :2  and\n                inv.ei_part_number    = equip.equip_model and\n                class.ec_class_id     = inv.ei_class and\n                equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and\n                at1.app_type_code = inv.ei_owner\n        order by inv.ei_received_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.EquipmentSelectionBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,model);
   __sJT_st.setInt(2,merchTypeCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.EquipmentSelectionBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:292^7*/

      ResultSet rs = it.getResultSet();

      int i = 0;
      while(rs.next() && i < 12) //only get oldest 12...
      {
        EquipRec tempRec =  new EquipRec();
        tempRec.setSerialNumber(rs.getString("ei_serial_number"));
        tempRec.setProductName(rs.getString("equipmfgr_mfr_name"));
        tempRec.setProductDesc(rs.getString("equip_descriptor"));
        tempRec.setOwnerDesc(rs.getString("inventory_owner_name"));
        tempRec.setUnitCost(formatCurr(rs.getString("ei_unit_cost")));
        tempRec.setClassTypeDesc(rs.getString("ec_class_name"));
        tempRec.setReceiveDate(rs.getDate("ei_received_date"));
        equipToDeploy.add(tempRec);
        i++;
      }

      it.close();
    }
    catch(Exception e)
    {
      logEntry("getEquipment()", e.toString());
      addError("getEquipment: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }

  public int getNumNeeded()
  {
    return equipToDeploy.size();
  }

  public void submitEquipment(long userId)
  {
    try
    {
      connect();
      
      if(isBlank(this.serialNumber))
      {
        this.needed = 0;
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:355^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_inventory 
//            set     ei_lrb              = :this.lendCode,
//                    ei_status           = :this.lendCode,
//                    ei_deployed_date    = sysdate,
//                    ei_merchant_number  = :this.merchantNum,
//                    ei_lrb_price        = :cleanUpPrice(this.price)
//            where   ei_part_number      = :this.model and 
//                    ei_serial_number    = :this.serialNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_786 = cleanUpPrice(this.price);
   String theSqlTS = "update  equip_inventory \n          set     ei_lrb              =  :1 ,\n                  ei_status           =  :2 ,\n                  ei_deployed_date    = sysdate,\n                  ei_merchant_number  =  :3 ,\n                  ei_lrb_price        =  :4 \n          where   ei_part_number      =  :5  and \n                  ei_serial_number    =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.ops.EquipmentSelectionBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.lendCode);
   __sJT_st.setString(2,this.lendCode);
   __sJT_st.setString(3,this.merchantNum);
   __sJT_st.setString(4,__sJT_786);
   __sJT_st.setString(5,this.model);
   __sJT_st.setString(6,this.serialNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:365^9*/

        this.needed--;
        addToTracking();
        addToHistory(this.model,this.serialNumber, userId, Integer.parseInt(this.lendCode), ("Deploy-" + this.lend + " to merchant #: " + this.merchantNum));
      }
    }
    catch(Exception e)
    {
      logEntry("submitEquipment()", e.toString());
      addError("submitEquipment: "+ e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private String cleanUpPrice(String num)
  {
    if(isBlank(num))
    {
      return "";
    }

    StringBuffer  digits      = new StringBuffer("");
    String        result      = "";

    try
    {
      for(int i=0; i<num.length(); ++i)
      {
        if(Character.isDigit(num.charAt(i)) || num.charAt(i) == '.')
        {
          digits.append(num.charAt(i));
        }
      }
      
      result = digits.toString();
    }
    catch(Exception e)
    {
      if(num != null && !num.equals(""))
      {
        logEntry("cleanUpPrice('" + num + "')", e.toString());
      }
    }
    
    return result;
  }

  private void addToTracking()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:420^7*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_merchant_tracking 
//          (
//            merchant_number,
//            action_date,
//            action,
//            ref_num_serial_num, 
//            action_desc 
//          )
//          values
//          (
//            :this.merchantNum,
//            sysdate,
//            :this.newSetup ? mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP : mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL,
//            :this.serialNumber,
//            :this.lendCode
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_787 = this.newSetup ? mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP : mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL;
   String theSqlTS = "insert into equip_merchant_tracking \n        (\n          merchant_number,\n          action_date,\n          action,\n          ref_num_serial_num, \n          action_desc \n        )\n        values\n        (\n           :1 ,\n          sysdate,\n           :2 ,\n           :3 ,\n           :4 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.ops.EquipmentSelectionBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.merchantNum);
   __sJT_st.setInt(2,__sJT_787);
   __sJT_st.setString(3,this.serialNumber);
   __sJT_st.setString(4,this.lendCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:438^7*/
    }
    catch(Exception e)
    {
      logEntry("addToTracking()", e.toString());
      addError("addToTracking: " + e.toString());
    }
  }

  public void recallEquipment(String model, String serialNum, String lendCode, long userId, String merchNum)
  {
    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:452^7*/

//  ************************************************************
//  #sql [Ctx] { update  equip_inventory
//          set     ei_lrb = :mesConstants.APP_EQUIP_IN_STOCK,
//                  ei_status = :mesConstants.APP_EQUIP_IN_STOCK,
//                  ei_deployed_date = null,
//                  ei_merchant_number = null,
//                  ei_lrb_price = 0
//          where   ei_part_number = :model and
//                  ei_serial_number = :serialNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_inventory\n        set     ei_lrb =  :1 ,\n                ei_status =  :2 ,\n                ei_deployed_date = null,\n                ei_merchant_number = null,\n                ei_lrb_price = 0\n        where   ei_part_number =  :3  and\n                ei_serial_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.ops.EquipmentSelectionBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_EQUIP_IN_STOCK);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_IN_STOCK);
   __sJT_st.setString(3,model);
   __sJT_st.setString(4,serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:462^7*/

      addToHistory(model, serialNum, userId, mesConstants.APP_EQUIP_IN_STOCK, "Returned from merchant # " + merchNum );
    }
    catch(Exception e)
    {
      logEntry("recallEquipment()", e.toString());
      addError("recallEquipment: "+ e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void cancelEquipment(String model, String serialNum, String lendCode, long userId, String merchNum)
  {
    ResultSetIterator it        = null;
    String            tempDate  = "";
    Date              td        = null;

    try
    {
      connect();

      int count = 0;

      /*@lineinfo:generated-code*//*@lineinfo:489^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei_deployed_date
//          from    equip_inventory
//          where   ei_serial_number = :serialNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei_deployed_date\n        from    equip_inventory\n        where   ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.EquipmentSelectionBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serialNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.ops.EquipmentSelectionBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:494^7*/

      ResultSet rs = it.getResultSet();

      if(rs.next())
      {
        try
        {
          DateFormat  df = new SimpleDateFormat("MM/dd/yy");
          tempDate = df.format(rs.getDate("ei_deployed_date"));
          if(!isBlank(tempDate))
          {
            DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
            td = fmt.parse(tempDate.trim());
          }
          else
          {
          }
        }
        catch(Exception e)
        {
          tempDate = "";
        }
      }

      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:521^7*/

//  ************************************************************
//  #sql [Ctx] { update  equip_inventory 
//          set     ei_lrb = :mesConstants.APP_EQUIP_IN_STOCK,
//                  ei_status = :mesConstants.APP_EQUIP_IN_STOCK,
//                  ei_deployed_date = null,
//                  ei_merchant_number = null,
//                  ei_lrb_price = 0
//          where   ei_part_number = :model and 
//                  ei_serial_number = :serialNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_inventory \n        set     ei_lrb =  :1 ,\n                ei_status =  :2 ,\n                ei_deployed_date = null,\n                ei_merchant_number = null,\n                ei_lrb_price = 0\n        where   ei_part_number =  :3  and \n                ei_serial_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.ops.EquipmentSelectionBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_EQUIP_IN_STOCK);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_IN_STOCK);
   __sJT_st.setString(3,model);
   __sJT_st.setString(4,serialNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:531^7*/

      addToHistory(model, serialNum, userId, mesConstants.APP_EQUIP_IN_STOCK, "Deployment to merchant # " + merchNum + " cancelled ");

      if(!isBlank(tempDate))
      {
        /*@lineinfo:generated-code*//*@lineinfo:537^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_merchant_tracking 
//            set     cancel_date = sysdate
//            where   action in (1,4,5) and 
//                    ref_num_serial_num = :serialNum and 
//                    trunc(action_date) = :new java.sql.Date(td.getTime()) and 
//                    merchant_number = :merchNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_788 = new java.sql.Date(td.getTime());
   String theSqlTS = "update  equip_merchant_tracking \n          set     cancel_date = sysdate\n          where   action in (1,4,5) and \n                  ref_num_serial_num =  :1  and \n                  trunc(action_date) =  :2  and \n                  merchant_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.ops.EquipmentSelectionBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,serialNum);
   __sJT_st.setDate(2,__sJT_788);
   __sJT_st.setString(3,merchNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:545^9*/
      }
      else
      {
      }
    }
    catch(Exception e)
    {
      logEntry("cancelEquipment()", e.toString());
      addError("cancelEquipment: "+ e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  public boolean validate()
  {
    return(! hasErrors());
  }
  public boolean shouldGoBack()
  {
    return (this.needed == 0);
  }
  public void setName(String name)
  {
    this.name = name;
  }
  public void setDesc(String desc)
  {
    this.desc = desc;
  }
  public void setLend(String lend)
  {
    this.lend = lend;
  }
  public void setLendCode(String lendCode)
  {
    this.lendCode = lendCode;
  }
  public void setPrice(String price)
  {
    this.price = price;
  }
  public void setMerchantNum(String merchantNum)
  {
    this.merchantNum = merchantNum;
  }
  public void setModel(String model)
  {
    this.model = model;
  }
  public void setNeeded(String needed)
  {
    try
    {
      this.needed = Integer.parseInt(needed);
    }
    catch(Exception e)
    {
      this.needed = 0;
    }
  }

  public String getModel()
  {
    return this.model;
  }

  public String getNeeded()
  {
    String result = "0";
    try
    {
      result = Integer.toString(this.needed);
    }
    catch(Exception e)
    {}

    return result;
  }

  public String getName()
  {
    return this.name;
  }
  public String getDesc()
  {
    return this.desc;
  }
  public String getLend()
  {
    return this.lend;
  }
  public String getLendCode()
  {
    return this.lendCode;
  }
  public String getPrice()
  {
    return this.price;
  }
  public String getMerchantNum()
  {
    return this.merchantNum;
  }

  public void setEquipment(String serialNumber)
  {
    if(isBlank(serialNumber))
    {
      this.serialNumber = "";
      return;
    }

    if(serialNumber.startsWith("NEW*"))
    {
      this.newSetup = true;
    }

    this.serialNumber = serialNumber.substring(4);
  }

  public String getClassTypeDesc(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getClassTypeDesc());
  }
  public String getUnitCost(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getUnitCost());
  }
  public String getProductName(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getProductName());
  }
  public String getProductDesc(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getProductDesc());
  }
  public String getOwnerDesc(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getOwnerDesc());
  }
  public String getSerialNumber(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getSerialNumber());
  }

  public String getReceiveDate(int idx)
  {
    EquipRec    equip  = (EquipRec)equipToDeploy.elementAt(idx);
    DateFormat  df     = new SimpleDateFormat("MM/dd/yy");
    String      result = df.format(equip.getReceiveDate());
    return result;
  }


  public class EquipRec
  {
    private String productName    = "";
    private String productDesc    = "";
    private String ownerDesc      = "";
    private String serialNumber   = "";
    private String unitCost       = "";
    private String classTypeDesc  = "";
    private Date   receiveDate    = null;

    EquipRec()
    {}

    public void setClassTypeDesc(String classTypeDesc)
    {
      this.classTypeDesc = classTypeDesc;
    }
    public void setUnitCost(String unitCost)
    {
      this.unitCost = unitCost;
    }
    public void setProductName(String productName)
    {
      this.productName = productName;
    }
    public void setProductDesc(String productDesc)
    {
      this.productDesc = productDesc;
    }
    public void setOwnerDesc(String ownerDesc)
    {
      this.ownerDesc = ownerDesc;
    }
    public void setSerialNumber(String serialNumber)
    {
      this.serialNumber = serialNumber;
    }
    public void setReceiveDate(Date tempDate)
    {
      this.receiveDate = tempDate;
    }

    public String getClassTypeDesc()
    {
      return this.classTypeDesc;
    }
    public String getUnitCost()
    {
      return this.unitCost;
    }
    public String getProductName()
    {
      return this.productName;
    }
    public String getProductDesc()
    {
      return this.productDesc;
    }
    public String getOwnerDesc()
    {
      return this.ownerDesc;
    }
    public String getSerialNumber()
    {
      return this.serialNumber;
    }
    public Date getReceiveDate()
    {
      return this.receiveDate;
    }
  }



}/*@lineinfo:generated-code*/