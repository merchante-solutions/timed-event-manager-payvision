/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/SetupDataBean.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Vector;
import javax.servlet.http.HttpServletResponse;
import com.mes.database.DBConnect;

public class SetupDataBean
{
  // constants
  private static final String   DB_CONNECT_STRING = "pool;ApplicationPool";
  
  // common data items
  private boolean               submit            = false;
  
  // utility variables
  private DBConnect             db                = null;
  private Connection            con               = null;
  private Vector                errors            = new Vector();
  
  protected final void init()
  {
    try
    {
      db = new DBConnect(this, "init");
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "constructor: " + e.toString());
    }
  }
  
  /*
  ** CONSTRUCTOR
  */  
  public SetupDataBean()
  {
    init();
  }
  
  /*
  ** FUNCTION getPreparedStatement
  */
  protected PreparedStatement getPreparedStatement(String query)
  {
    PreparedStatement   ps = null;
    try
    {
      if(con == null)
      {
        con = db.getConnection(DB_CONNECT_STRING);
      }
    
      if(con != null)
      {
        ps = con.prepareStatement(query);
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPreparedStatement: " + db.getErrorMessage());
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPreparedStatement: " + e.toString());
    }
    
    return ps;
  }
  
  /*
  ** FUNCTION getErrors
  */
  public Vector getErrors()
  {
    return this.errors;
  }
  
  /*
  ** FUNCTION addError
  */
  protected void addError(String error)
  {
    try
    {
      errors.add(error);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "addError: " + e.toString());
    }
  }
  
  /*
  ** FUNCTION hasErrors
  */
  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }
  
  /*
  ** FUNCTION display
  */
  public void display(HttpServletResponse response)
  {
    try
    {
      PrintWriter out = response.getWriter();
      out.print(displayData());
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(),"display: " + e.toString());
    }
  }
  
  /*
  ** FUNCTION cleanUp
  */
  public void cleanUp()
  {
    if(con != null)
    {
      try
      {
        db.releaseConnection();
      }
      catch(Exception e)
      {
      }
    }
  }
  
  /***************************************************************************
  * Overridable methods
  ***************************************************************************/
  /*
  ** FUNCTION getData
  */
  public void getData(long appSeqNum)
  {
  }
  
  /*
  ** FUNCTION submitData
  */
  public void submitData(long appSeqNum)
  {
  }
  
  /*
  ** FUNCTION setDefaults
  */
  public void setDefaults(String appSeqNum)
  {
    try
    {
      setDefaults(Long.parseLong(appSeqNum));
    }
    catch(Exception e)
    {
    }
  }
  public void setDefaults(long appSeqNum)
  {
  }
  
  /*
  ** FUNCTION validate
  */
  public boolean validate()
  {
    return(true);
  }
  
  /*
  ** FUNCTION displayData
  */
  public String displayData()
  {
    return "displayData() undefined";
  }
  
  /*
  ** FUNCTION canContinue
  */
  public boolean canContinue()
  {
    return true;
  }
  
  /***************************************************************************
  * Helper functions
  ***************************************************************************/
  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
  
  public boolean isNumber(String test)
  {
    boolean pass = false;
    
    try
    {
      long i = Long.parseLong(test);
      pass = true;
    }
    catch(Exception e)
    {
    }
    
    return pass;
  }
  
  public boolean isEmail(String test)
  {
    boolean pass = false;
    
    if(!isBlank(test) && test.indexOf('@') > 0)
    {
      pass = true;
    }
    
    return pass;
  }
  
  public long getDigits(String raw)
  {
    long          result      = 0L;
    StringBuffer  digits      = new StringBuffer("");
    
    for(int i=0; i<raw.length(); ++i)
    {
      if(Character.isDigit(raw.charAt(i)))
      {
        digits.append(raw.charAt(i));
      }
    }
    
    try
    {
      result = Long.parseLong(digits.toString());
    }
    catch(Exception e)
    {
      result = 0L;
    }
    
    return result;
  }
  
  public boolean isSubmitted()
  {
    return this.submit;
  }
  
  /***************************************************************************
  * Getters and Setters
  ***************************************************************************/
  public void setSubmit(String submit)
  {
    this.submit = true;
  }
  public boolean getSubmit()
  {
    return this.submit;
  }
}
