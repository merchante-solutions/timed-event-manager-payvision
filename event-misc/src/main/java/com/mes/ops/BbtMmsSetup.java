/*@lineinfo:filename=BbtMmsSetup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/BbtMmsSetup.sqlj $

  Description:

    AutoApprove

    Tools for auto approving (and auto uploading if necessary) an
    online application

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/14/04 4:11p $
  Version            : $Revision: 17 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.queues.QueueNotes;
import com.mes.queues.QueueTools;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class BbtMmsSetup extends SQLJConnectionBase
{
  public static final String VXP0P16  = "VXP0P16";
  public static final String VT1TP08  = "VT1TP08";
  public static final String VT1TPA7  = "VT1TPA7";
  public static final String VT1TPH6  = "VT1TPH6";
  public static final String VT1H385  = "VT1H385";
  public static final String VT13DA8  = "VT13DA8";
  public static final String VT46DA4  = "VT46DA4";
  public static final String VSVR046  = "VSVR046";
  public static final String VSTDB01  = "VSTDB01";
  public static final String D52C8072 = "52C8072";
  public static final String RR       = "RR";
  public static final String DRR      = "DRR";
  public static final String HTL      = "HTL";
  public static final String DHTL     = "DHTL";
  public static final String STAGE    = "STAGE";


  //holds all requests needed to setup merchant
  private Vector           requests               = new Vector();
  private boolean          placeInExceptionQueue  = false;
  private String           exceptionQueueNote     = "";

  /*
  ** BbtMmsSetup()
  */ 
  public BbtMmsSetup()
  {
    super();
  }
  
  /*
  ** BbtMmsSetup(DefaultContext Ctx) constructor
  **
  ** Latches on to passed default context for database interaction
  */
  public BbtMmsSetup(DefaultContext defCtx)
  {
    super();
    
    if(defCtx != null)
    {
      Ctx = defCtx;
    }
  }
  
  //update mms_updated field in merchant_discover_app table.. 
  //but only if discover request was sent back with an approval status
  private void setMmsUpdated(long appSeqNum)
  {
    try
    {
       
      int appCount = -1;

      /*@lineinfo:generated-code*//*@lineinfo:103^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant_discover_app_response
//          where   app_seq_num     = :appSeqNum and
//                  decision_status = 'A'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant_discover_app_response\n        where   app_seq_num     =  :1  and\n                decision_status = 'A'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.BbtMmsSetup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:110^7*/
  
      //account has had a request for discover and it has come back approved,
      //so we timestamp mms_updated.. because we just automatically carried out the mms part
      if(appCount > 0)
      {

        /*@lineinfo:generated-code*//*@lineinfo:117^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_discover_app
//            set     MMS_UPDATED             = sysdate
//            where   APP_SEQ_NUM             = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_discover_app\n          set     MMS_UPDATED             = sysdate\n          where   APP_SEQ_NUM             =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:122^9*/

      }
    }
    catch(Exception e)
    {
      logEntry("setMmsUpdated()", e.toString());
    }
  }

  private boolean isBbtAccount(long appSeqNum)
  {
    boolean isBbt   = true;
    int     appType = -1;

    try
    {
      //get app_type from application table
      /*@lineinfo:generated-code*//*@lineinfo:140^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//          
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.BbtMmsSetup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:146^7*/

      isBbt = (appType == mesConstants.APP_TYPE_BBT);

    }
    catch(Exception e)
    {
      logEntry("isBbtAccount()", e.toString());
    }

    return isBbt;
  }

  private boolean holdOffOnMms(long appSeqNum)
  {
    boolean             holdOff     = true;
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {
      int appCount = -1;
      

      //check to see if a request was sent to discover.. if so, was it a request type maintenance 
      //if it was... then we do not do auto mms for it...
      /*@lineinfo:generated-code*//*@lineinfo:172^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  request_type
//          from    merchant_discover_app
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  request_type\n        from    merchant_discover_app\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.BbtMmsSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:177^7*/

      //if discover has a request type of maintenance we do not do auto mms.. they will do it manually..
      rs = it.getResultSet();
      
      if(rs.next())
      {
        if(isBlank(rs.getString("request_type")) || rs.getString("request_type").equals("Maintenance"))
        {
          return true; //we dont do auto mms for maintenance discover requests
        }
      }
      else
      {
        return false; //there was no discover request so go ahead
      }
      
      rs.close();
      it.close();

      //if a discover request was sent out..and its a request type 'new' check to see if it came back approved or declined
      /*@lineinfo:generated-code*//*@lineinfo:198^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant_discover_app_response
//          where   app_seq_num = :appSeqNum and
//                  (
//                   decision_status = 'A' or 
//                   decision_status = 'D'
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant_discover_app_response\n        where   app_seq_num =  :1  and\n                (\n                 decision_status = 'A' or \n                 decision_status = 'D'\n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.BbtMmsSetup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:208^7*/

      holdOff = (appCount == 0);

    }
    catch(Exception e)
    {
      holdOff = false;
      logEntry("holdOffOnMms()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return holdOff;
  }

  private boolean getMmsGeneralInfo(long appSeqNum)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    MmsGeneralInfo      generalInfo = new MmsGeneralInfo();
    boolean             result      = true;
    boolean             stageOnly   = false;
    try
    {

      //set the appSeqNum in the mms general info class
      generalInfo.app_seq_num = appSeqNum;

      int busNature     = -1;
      int posType       = -1;
      int productType   = -1;
      int processor     = -1;

      //get what we need from the merchant table
      /*@lineinfo:generated-code*//*@lineinfo:246^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.asso_number,
//                  m.merch_mcc,
//                  m.merch_number,
//                  m.business_nature,
//                  m.processor,
//                  m.ebt_options,
//                  decode(mp.pos_code, null, -1, mp.pos_code) pos_type,
//                  decode(pc.pos_type, null, -1, pc.pos_type) product_type
//         
//          from    merchant      m,
//                  merch_pos     mp,
//                  pos_category  pc
//  
//          where   m.app_seq_num = :generalInfo.app_seq_num   and
//                  m.app_seq_num = mp.app_seq_num               and
//                  pc.pos_code   = mp.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.asso_number,\n                m.merch_mcc,\n                m.merch_number,\n                m.business_nature,\n                m.processor,\n                m.ebt_options,\n                decode(mp.pos_code, null, -1, mp.pos_code) pos_type,\n                decode(pc.pos_type, null, -1, pc.pos_type) product_type\n       \n        from    merchant      m,\n                merch_pos     mp,\n                pos_category  pc\n\n        where   m.app_seq_num =  :1    and\n                m.app_seq_num = mp.app_seq_num               and\n                pc.pos_code   = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,generalInfo.app_seq_num);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.BbtMmsSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:264^7*/

      //
      rs = it.getResultSet();
      
      if(rs.next())
      {
        posType       = rs.getInt("pos_type");
        productType   = rs.getInt("product_type");
        processor     = rs.getInt("processor");
        busNature     = rs.getInt("business_nature");



        //we dont do anything for this merchant if not edc or if processor is not vital
        if(processor != mesConstants.APP_PROCESSOR_TYPE_VITAL) 
        {
          System.out.println("returning false: processor = " + processor);
          result                = false;
          placeInExceptionQueue = true;
          exceptionQueueNote    = "Processor was not Vital";
          return result; //false return means it goes into exception queue   
        }

        if(productType != mesConstants.POS_DIAL_TERMINAL)
        {
          if(productType == mesConstants.POS_OTHER)
          {
            stageOnly = true;
          }
          else
          {
            System.out.println("returning false: productType = " + productType);
            result                = false;
            placeInExceptionQueue = true;
            exceptionQueueNote    = "Product Type was not EDC or Stage Only";
            return result; //false return means it goes into exception queue   
          }
        }

        if(rs.getInt("asso_number") == 1064)
        {
          generalInfo.chain = "077621";
          generalInfo.terminal_number = "0010";
        }
        else
        {
          generalInfo.chain = "000000";
          generalInfo.terminal_number = "0001";
        }

        generalInfo.ebt_trans_type  = isBlank(rs.getString("ebt_options")) ? "0" : rs.getString("ebt_options");
        if(generalInfo.ebt_trans_type.equals("0") || generalInfo.ebt_trans_type.equals("2"))
        {
          generalInfo.ebt_fcs_id = "0000000";
        }
        
        generalInfo.merch_number    = rs.getString("merch_number").substring(4) + "0";
        generalInfo.mcfs            = generalInfo.merch_number + generalInfo.store_number.substring(0,3);
        generalInfo.mcc             = rs.getString("merch_mcc");
        generalInfo.term_voice_id   = rs.getString("merch_number");
      }
      
      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:330^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  MERCHPO_CARD_MERCH_NUMBER,
//                  CARDTYPE_CODE,
//                  MERCHPO_SPLIT_DIAL
//         
//          from    merchpayoption
//  
//          where   app_seq_num = :generalInfo.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  MERCHPO_CARD_MERCH_NUMBER,\n                CARDTYPE_CODE,\n                MERCHPO_SPLIT_DIAL\n       \n        from    merchpayoption\n\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,generalInfo.app_seq_num);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.BbtMmsSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:339^7*/

      rs = it.getResultSet();
      
      while(rs.next())
      {
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_DINERS_CLUB:
            generalInfo.dinr    = (isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER")) || rs.getLong("MERCHPO_CARD_MERCH_NUMBER") <= 0L) ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
            generalInfo.dinr_ac = isBlank(generalInfo.dinr) ? "" : "1";
          break;

          case mesConstants.APP_CT_DISCOVER:
            generalInfo.disc    = (isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER")) || rs.getLong("MERCHPO_CARD_MERCH_NUMBER") <= 0L) ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
            generalInfo.disc_ac = isBlank(generalInfo.disc) ? "" : "1";
          break;

          case mesConstants.APP_CT_JCB:
            generalInfo.jcb     = (isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER")) || rs.getLong("MERCHPO_CARD_MERCH_NUMBER") <= 0L) ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
            generalInfo.jcb_ac  = isBlank(generalInfo.jcb) ? "" : "1";
          break;

          case mesConstants.APP_CT_AMEX:
            generalInfo.amex        = (isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER")) || rs.getLong("MERCHPO_CARD_MERCH_NUMBER") <= 0L) ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
            generalInfo.amex_ac     = isBlank(generalInfo.amex) ? "" : "1";
            generalInfo.split_dial  = rs.getString("MERCHPO_SPLIT_DIAL");
          break;

          case mesConstants.APP_CT_EBT:
            if(!generalInfo.ebt_fcs_id.equals("0000000") && !isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER")) && rs.getLong("MERCHPO_CARD_MERCH_NUMBER") > 0L)
            {
              generalInfo.ebt_fcs_id = rs.getString("MERCHPO_CARD_MERCH_NUMBER");
            }
          break;
          case mesConstants.APP_CT_DEBIT:
            generalInfo.sharing_groups            = "GWQ8ZV";
            generalInfo.merchant_aba              = "990024552";
            generalInfo.merch_settlement_agent    = "V070";
            generalInfo.reimbursement_attributes  = "Z";
          break;
        }
      }
      
      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:386^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_phone
//          from    address
//          where   app_seq_num  = :generalInfo.app_seq_num and
//                  addresstype_code = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_phone\n        from    address\n        where   app_seq_num  =  :1  and\n                addresstype_code = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,generalInfo.app_seq_num);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.BbtMmsSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:392^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        generalInfo.merch_phone           = rs.getString("address_phone");
        generalInfo.customer_service_num  = rs.getString("address_phone");
      }
      
      rs.close();
      it.close();

      // see if merchcontact contains contact name.
      // MMS requires two names so only use contact
      // name if both first and last were specified.
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:408^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchcontact
//          where   app_seq_num = :generalInfo.app_seq_num and
//                  not merchcont_prim_first_name is null and 
//                  not merchcont_prim_last_name is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchcontact\n        where   app_seq_num =  :1  and\n                not merchcont_prim_first_name is null and \n                not merchcont_prim_last_name is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.BbtMmsSetup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,generalInfo.app_seq_num);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:416^7*/

      if(recCount > 0)
      {
        // use merchcontact
        /*@lineinfo:generated-code*//*@lineinfo:421^9*/

//  ************************************************************
//  #sql [Ctx] { select  MERCHCONT_PRIM_FIRST_NAME || ' ' || MERCHCONT_PRIM_LAST_NAME contact_name
//            
//            from    merchcontact
//            where   app_seq_num  = :generalInfo.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  MERCHCONT_PRIM_FIRST_NAME || ' ' || MERCHCONT_PRIM_LAST_NAME contact_name\n           \n          from    merchcontact\n          where   app_seq_num  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.BbtMmsSetup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,generalInfo.app_seq_num);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   generalInfo.contact_name = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:427^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:431^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_first_name || ' ' || busowner_last_name contact_name
//            from    businessowner
//            where   app_seq_num  = :generalInfo.app_seq_num and
//                    busowner_num = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_first_name || ' ' || busowner_last_name contact_name\n          from    businessowner\n          where   app_seq_num  =  :1  and\n                  busowner_num = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,generalInfo.app_seq_num);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.BbtMmsSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:437^9*/
      
        rs = it.getResultSet();
        if(rs.next())
        {
          generalInfo.contact_name  = rs.getString("contact_name");
        }
        
        rs.close(); 
        it.close();
      }

      if(!isBlank(generalInfo.contact_name) && generalInfo.contact_name.length() > 25)
      {
        generalInfo.contact_name = generalInfo.contact_name.substring(0,25);
      }

      /*@lineinfo:generated-code*//*@lineinfo:454^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_name,
//                  address_line1,
//                  address_city,
//                  address_state,
//                  address_zip
//          from    app_setup_address
//          where   app_seq_num   = :generalInfo.app_seq_num and
//                  address_ind   = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_name,\n                address_line1,\n                address_city,\n                address_state,\n                address_zip\n        from    app_setup_address\n        where   app_seq_num   =  :1  and\n                address_ind   = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,generalInfo.app_seq_num);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.ops.BbtMmsSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:464^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        generalInfo.business_name     = rs.getString("address_name");

        if(generalInfo.business_name.length() > 20)
        {
          generalInfo.idle_prompt       = generalInfo.business_name.substring(0,20);
        }
        else
        {
          generalInfo.idle_prompt       = generalInfo.business_name;
        }

        generalInfo.business_address  = rs.getString("address_line1");

        if(!isBlank(generalInfo.business_address) && generalInfo.business_address.length() > 25)
        {
          generalInfo.business_address = generalInfo.business_address.substring(0,25);
        }

        generalInfo.business_city     = rs.getString("address_city");

        if(!isBlank(generalInfo.business_city) && generalInfo.business_city.length() > 13)
        {
          generalInfo.business_city = generalInfo.business_city.substring(0,13);
        }

        generalInfo.business_state    = rs.getString("address_state");
        generalInfo.business_zip      = rs.getString("address_zip");

        //sharing group is unique for businesses in west virginia.. so if they accept debit give them unique sharing group
        //sharing group gets set up above.. if debit is selected..so if not blank its selected
        if(generalInfo.business_state.equals("WV") && !isBlank(generalInfo.sharing_groups))
        {
          generalInfo.sharing_groups = "GWQ8ZV";
        }

      }
      
      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:509^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  access_code,
//                  auto_batch_close_time,
//                  header_line4,
//                  header_line5,
//                  footer,
//                  reset_ref_flag,
//                  invoice_prompt_flag,
//                  fraud_control_flag,
//                  fraud_control_option,
//                  PASSWORD_PROTECT_AUTHONLY, 
//                  PASSWORD_PROTECT_CREDIT, 
//                  PASSWORD_PROTECT_OFFLINE,
//                  terminal_reminder_time,
//                  tip_option_flag,
//                  clerk_enabled_flag,
//                  no_printer_needed,
//                  no_printer_owned,
//                  avs_flag,
//                  cash_back_flag,
//                  cash_back_limit,
//                  purchasing_card_flag,
//                  card_truncation_flag,
//                  debit_surcharge_amount,
//                  call_waiting_flag,
//                  cvv2_flag,
//                  fine_dining_flag,
//                  tip_time_sale
//          from    pos_features
//          where   app_seq_num   = :generalInfo.app_seq_num
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  access_code,\n                auto_batch_close_time,\n                header_line4,\n                header_line5,\n                footer,\n                reset_ref_flag,\n                invoice_prompt_flag,\n                fraud_control_flag,\n                fraud_control_option,\n                PASSWORD_PROTECT_AUTHONLY, \n                PASSWORD_PROTECT_CREDIT, \n                PASSWORD_PROTECT_OFFLINE,\n                terminal_reminder_time,\n                tip_option_flag,\n                clerk_enabled_flag,\n                no_printer_needed,\n                no_printer_owned,\n                avs_flag,\n                cash_back_flag,\n                cash_back_limit,\n                purchasing_card_flag,\n                card_truncation_flag,\n                debit_surcharge_amount,\n                call_waiting_flag,\n                cvv2_flag,\n                fine_dining_flag,\n                tip_time_sale\n        from    pos_features\n        where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,generalInfo.app_seq_num);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.ops.BbtMmsSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:541^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        //these two guys are always the same avs is used on application
        generalInfo.avs             = isBlank(rs.getString("avs_flag")) ? "N" : rs.getString("avs_flag");
        generalInfo.invoice_number  = isBlank(rs.getString("avs_flag")) ? "N" : rs.getString("avs_flag");

        if(busNature == mesConstants.BUSINESS_NATURE_KEYTAIL || busNature == mesConstants.BUSINESS_NATURE_MOTO)
        {
          generalInfo.avs             = "Y";
          generalInfo.invoice_number  = "Y";
        }

        if(!isBlank(rs.getString("auto_batch_close_time")))
        {
          generalInfo.auto_close_ind      = "1";
          generalInfo.auto_close_time     = getMilitaryTime(rs.getString("auto_batch_close_time"));
          generalInfo.auto_close_time2    = "0000";
          generalInfo.auto_close_reports  = "00";
          generalInfo.check_totals_ind    = "0";
          generalInfo.force_settle_ind    = "1";
        }
        else
        {
          generalInfo.auto_close_ind      = "0";
          generalInfo.auto_close_time     = "";
          generalInfo.auto_close_time2    = "";
          generalInfo.auto_close_reports  = "00";
          generalInfo.check_totals_ind    = "0";
          generalInfo.force_settle_ind    = "0";
        }

        generalInfo.prim_auth_dial_access = isBlank(rs.getString("access_code"))          ? ""  : rs.getString("access_code");
        generalInfo.sec_auth_dial_access  = isBlank(rs.getString("access_code"))          ? ""  : rs.getString("access_code");

        generalInfo.header_line4          = isBlank(rs.getString("header_line4"))         ? ""  : rs.getString("header_line4");
        generalInfo.header_line5          = isBlank(rs.getString("header_line5"))         ? ""  : rs.getString("header_line5");
        generalInfo.footer_line1          = isBlank(rs.getString("footer"))               ? ""  : rs.getString("footer");
        generalInfo.inv_prompt            = isBlank(rs.getString("invoice_prompt_flag"))  ? "N" : rs.getString("invoice_prompt_flag");

        /*
        if(busNature == mesConstants.BUSINESS_NATURE_RESTAURANT)
        {
          generalInfo.restaurant_processing = "Y";
          generalInfo.tip_option            = "Y";
        }
        else
        {
          generalInfo.restaurant_processing = isBlank(rs.getString("tip_option_flag"))      ? "N" : rs.getString("tip_option_flag");
          generalInfo.tip_option            = isBlank(rs.getString("tip_option_flag"))      ? "N" : rs.getString("tip_option_flag");
        }
        */

        //if any of these are 'Y', then we setup as a restaurant application... if these are all 'N' then we setup as retail
        generalInfo.restaurant_processing = isBlank(rs.getString("tip_option_flag"))      ? "N" : rs.getString("tip_option_flag");
        generalInfo.tip_option            = isBlank(rs.getString("tip_option_flag"))      ? "N" : rs.getString("tip_option_flag");
        generalInfo.tip_time_sale         = isBlank(rs.getString("tip_time_sale"))        ? "N" : rs.getString("tip_time_sale");

        generalInfo.srv_clk_processing    = isBlank(rs.getString("clerk_enabled_flag"))   ? "N" : rs.getString("clerk_enabled_flag");
        generalInfo.purchasing_card       = isBlank(rs.getString("purchasing_card_flag")) ? "N" : rs.getString("purchasing_card_flag");

        if(busNature == mesConstants.BUSINESS_NATURE_GOV_PURCHCARD)
        {
          generalInfo.purchasing_card = "Y";
        }
        
        //commented out cause its always Y
        //generalInfo.receipt_truncation    = isBlank(rs.getString("card_truncation_flag")) ? "N" : rs.getString("card_truncation_flag");

        if(!isBlank(rs.getString("debit_surcharge_amount")) && rs.getDouble("debit_surcharge_amount") > 0.0)
        {
          generalInfo.surcharge_ind       = "Y";
          generalInfo.surcharge_amount    = rs.getString("debit_surcharge_amount");
        }
        else
        {
          generalInfo.surcharge_ind       = "N";
          generalInfo.surcharge_amount    = "";
        }
        
        if(rs.getInt("PASSWORD_PROTECT_CREDIT") > 0)
        {
          generalInfo.pass_protect = StringUtilities.replaceAtPos(generalInfo.pass_protect, '1', 4);
        }
        if(rs.getInt("PASSWORD_PROTECT_AUTHONLY") > 0)
        {
          generalInfo.pass_protect = StringUtilities.replaceAtPos(generalInfo.pass_protect, '1', 5);
        }
        if(rs.getInt("PASSWORD_PROTECT_OFFLINE") > 0)
        {
          generalInfo.pass_protect = StringUtilities.replaceAtPos(generalInfo.pass_protect, '1', 6);
        }

        if(!isBlank(rs.getString("fraud_control_flag")) && rs.getString("fraud_control_flag").equalsIgnoreCase("Y") && !isBlank(rs.getString("fraud_control_option")))
        {
          switch(rs.getInt("fraud_control_option"))
          {
            case 1: //last 4 digits
              generalInfo.fraud = "11111111";
            break;
            
            case 2: //full card num/display mag stripe
              generalInfo.fraud = "22222222";
            break;
          }
        }

      }
      
      rs.close();
      it.close();

      if(stageOnly)
      {
        MmsRequest mmsReq = new MmsRequest(getNewReqId(),generalInfo);
        mmsReq.term_application           = STAGE;
        mmsReq.term_application_profgen   = mmsReq.term_application;
        mmsReq.manufacturer               = "na";
        mmsReq.equip_model                = "na";
        mmsReq.term_model                 = "na";
        requests.add(mmsReq);
      }
      else
      {
        //get terminals and try to figure out which terminal applications to use
        /*@lineinfo:generated-code*//*@lineinfo:668^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  me.equip_model,
//                    me.merchequip_equip_quantity,
//                    em.equipmfgr_mfr_name,
//                    eq.equiptype_code,
//                    eq.mms_equip_model
//  
//            from    merchequipment  me,
//                    equipment       eq,
//                    equipmfgr       em
//  
//            where   me.app_seq_num        = :generalInfo.app_seq_num  and
//                    me.equip_model        = eq.equip_model              and
//                    eq.equipmfgr_mfr_code = em.equipmfgr_mfr_code       and
//                    eq.equiptype_code in (1,5,6)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  me.equip_model,\n                  me.merchequip_equip_quantity,\n                  em.equipmfgr_mfr_name,\n                  eq.equiptype_code,\n                  eq.mms_equip_model\n\n          from    merchequipment  me,\n                  equipment       eq,\n                  equipmfgr       em\n\n          where   me.app_seq_num        =  :1   and\n                  me.equip_model        = eq.equip_model              and\n                  eq.equipmfgr_mfr_code = em.equipmfgr_mfr_code       and\n                  eq.equiptype_code in (1,5,6)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,generalInfo.app_seq_num);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.ops.BbtMmsSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:684^9*/
      
        rs = it.getResultSet();

        while(rs.next())
        {

          String termApp = getTermApp(busNature,rs.getString("mms_equip_model"),!isBlank(generalInfo.sharing_groups),(generalInfo.restaurant_processing.equals("Y") || generalInfo.tip_option.equals("Y") ||  generalInfo.tip_time_sale.equals("Y")));

          if(isBlank(termApp))                        
          {
            //add to exception queue then continue
            //i think were only going to add an account once to the exception queue if something comes up.. maybe we'll document why 
            //it was sent to exception queue.. but there will only be one entry in the queue per app_seq_num
            placeInExceptionQueue = true;
            exceptionQueueNote    = "MMS terminal application could not be found for one of the terminals selected on this account";
            continue;
          }
        
          //setup terminal application for each terminal, even if multiples of same kind
          for(int x=0; x<rs.getInt("merchequip_equip_quantity"); x++)
          {
            //each terminal application must have its own version of MmsGeneralInfo, so we instantiate a new instance of it
            //and assign the old general info to it... then later we will go through and make term app specific changes to the gen info
            MmsGeneralInfo tempGenInfo = new MmsGeneralInfo();
            tempGenInfo = generalInfo;

            MmsRequest mmsReq = new MmsRequest(getNewReqId(),tempGenInfo);
            mmsReq.term_application           = termApp;
            mmsReq.term_application_profgen   = mmsReq.term_application;
            mmsReq.manufacturer               = rs.getString("equipmfgr_mfr_name").toUpperCase();
            mmsReq.equip_model                = rs.getString("equip_model");
            mmsReq.term_model                 = rs.getString("mms_equip_model");

            requests.add(mmsReq);
          }
        }
        
        rs.close();
        it.close();

        //loop through each application and try to set printer and pinpads for each application
        //loop through each application and resolve discrepencies in general information
        for(int x=0; x<requests.size(); x++)
        { 
          MmsRequest tempReq = (MmsRequest)requests.elementAt(x);
          fixDiscrepencies(tempReq);
          getPinPad(tempReq);
          getPrinter(tempReq);
        }
      
      }
    }
    catch(Exception e)
    {
      logEntry("(getMmsGeneralInfo)", e.toString());
      result = false;
      placeInExceptionQueue = true;
      exceptionQueueNote    += "SQL call for this account crashed in mms setup";
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return result;
  }

  private void getPrinter(MmsRequest tempReq)
  {

    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {
      tempReq.printer_model   = "NO PRINTER";
      tempReq.printer_type    = "0";

      //SPECIAL CASES.. MUST BE BLANK FOR NO PRINTER INSTEAD OF 0
      if(tempReq.term_application.equals(VT1TP08) || 
         tempReq.term_application.equals(VT1TPA7) || 
         tempReq.term_application.equals(VT1TPH6) || 
         tempReq.term_application.equals(VT1H385) ||
         tempReq.term_application.equals(VT13DA8))
      {
        tempReq.printer_type  = "";
      }

      //these term apps and models have PRINTERS integrated, so they are always set
      if(tempReq.term_application.equals(VT46DA4))
      {
        tempReq.printer_model = "INTEGRATED";
        tempReq.printer_type  = "2";
        return;
      }
      else if(tempReq.term_application.equals(VSVR046) && (tempReq.term_model.equals("OMNI 3210") || tempReq.term_model.equals("OMNI 3200") || tempReq.term_model.equals("OMNI 470")))
      {
        tempReq.printer_model = "INTEGRATED";
        tempReq.printer_type  = "2";
        return;
      }
      else if(tempReq.term_application.equals(D52C8072))
      {
        tempReq.printer_model = "INTEGRATED";
        tempReq.printer_type  = "2";
        return;      
      }
      else if(tempReq.term_application.equals(RR) && (tempReq.term_model.equals("T7P") || tempReq.term_model.equals("T77")))
      {
        if(tempReq.term_model.equals("T7P"))
        {
          tempReq.printer_model = "INTEGRATED";
          tempReq.printer_type  = "1";
        }
        else if(tempReq.term_model.equals("T77"))
        {
          tempReq.printer_model = "INTEGRATED ROLL";
          tempReq.printer_type  = "7";
        }
        return;
      }
      else if(tempReq.term_application.equals(DRR) && (tempReq.term_model.equals("T7P") || tempReq.term_model.equals("T77")))
      {
        if(tempReq.term_model.equals("T7P"))
        {
          tempReq.printer_model = "INTEGRATED";
          tempReq.printer_type  = "1";
        }
        else if(tempReq.term_model.equals("T77"))
        {
          tempReq.printer_model = "INTEGRATED ROLL";
          tempReq.printer_type  = "7";
        }
        return;
      }
      else if(tempReq.term_application.equals(HTL) && (tempReq.term_model.equals("T7P") || tempReq.term_model.equals("T77")))
      {
        if(tempReq.term_model.equals("T7P"))
        {
          tempReq.printer_model = "INTEGRATED";
          tempReq.printer_type  = "1";
        }
        else if(tempReq.term_model.equals("T77"))
        {
          tempReq.printer_model = "INTEGRATED ROLL";
          tempReq.printer_type  = "7";
        }
        return;
      }
      else if(tempReq.term_application.equals(DHTL) && (tempReq.term_model.equals("T7P") || tempReq.term_model.equals("T77")))
      {
        if(tempReq.term_model.equals("T7P"))
        {
          tempReq.printer_model = "INTEGRATED";
          tempReq.printer_type  = "1";
        }
        else if(tempReq.term_model.equals("T77"))
        {
          tempReq.printer_model = "INTEGRATED ROLL";
          tempReq.printer_type  = "7";
        }
        return;
      }


      //get printers and try to figure out which terminal applications to use
      /*@lineinfo:generated-code*//*@lineinfo:852^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  me.equip_model,
//                  me.merchequip_equip_quantity,
//                  em.equipmfgr_mfr_name,
//                  eq.equiptype_code,
//                  eq.mms_equip_model
//  
//          from    merchequipment  me,
//                  equipment       eq,
//                  equipmfgr       em
//  
//          where   me.app_seq_num        = :tempReq.generalInfo.app_seq_num  and
//                  me.equip_model        = eq.equip_model              and
//                  eq.equipmfgr_mfr_code = em.equipmfgr_mfr_code       and
//                  eq.equiptype_code in (2)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  me.equip_model,\n                me.merchequip_equip_quantity,\n                em.equipmfgr_mfr_name,\n                eq.equiptype_code,\n                eq.mms_equip_model\n\n        from    merchequipment  me,\n                equipment       eq,\n                equipmfgr       em\n\n        where   me.app_seq_num        =  :1   and\n                me.equip_model        = eq.equip_model              and\n                eq.equipmfgr_mfr_code = em.equipmfgr_mfr_code       and\n                eq.equiptype_code in (2)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,tempReq.generalInfo.app_seq_num);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.ops.BbtMmsSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:868^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {
        if(tempReq.term_application.equals(VXP0P16))
        {
          
          if(!isBlank(rs.getString("mms_equip_model")))
          {
            if(rs.getString("mms_equip_model").equals("ROLL IDP 3530")    || 
               rs.getString("mms_equip_model").equals("ROLL IDP 560")     ||
               rs.getString("mms_equip_model").equals("ROLL IDP 562")     || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 200") || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 220") || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 250") || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 600") || 
               rs.getString("mms_equip_model").equals("DATA CARD SILENT PRT"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "5";
            }
          }

        }
        else if(tempReq.term_application.equals(VT1TP08))
        {
          if(!isBlank(rs.getString("mms_equip_model")))
          {
            if(rs.getString("mms_equip_model").equals("ROLL IDP 560")      ||
               rs.getString("mms_equip_model").equals("ROLL IDP 562")      || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 220")  || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 250")  || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 900R") || 
               rs.getString("mms_equip_model").equals("DATA CARD SILENT PRT"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "2";
            }
          }
        }
        else if(tempReq.term_application.equals(VT1TPA7))
        {
          if(!isBlank(rs.getString("mms_equip_model")))
          {
            if(rs.getString("mms_equip_model").equals("ROLL IDP 560")      ||
               rs.getString("mms_equip_model").equals("ROLL IDP 562")      || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 220")  || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 250")  || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 900R") || 
               rs.getString("mms_equip_model").equals("DATA CARD SILENT PRT"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "2";
            }
          }
        }
        else if(tempReq.term_application.equals(VT1TPH6))
        {
          if(!isBlank(rs.getString("mms_equip_model")))
          {
            if(rs.getString("mms_equip_model").equals("ROLL IDP 560")      ||
               rs.getString("mms_equip_model").equals("ROLL IDP 562")      || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 220")  || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 250")  || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 900R") || 
               rs.getString("mms_equip_model").equals("DATA CARD SILENT PRT"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "2";
            }
          }
        }
        else if(tempReq.term_application.equals(VT1H385))
        {
          if(!isBlank(rs.getString("mms_equip_model")))
          {
            if(rs.getString("mms_equip_model").equals("ROLL IDP 3530")    || 
               rs.getString("mms_equip_model").equals("ROLL IDP 560")     ||
               rs.getString("mms_equip_model").equals("ROLL IDP 562")     || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 200") || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 220") || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 250") || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 600") || 
               rs.getString("mms_equip_model").equals("DATA CARD SILENT PRT"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "2";
            }
          }
        }
        else if(tempReq.term_application.equals(VT13DA8))
        {
          if(!isBlank(rs.getString("mms_equip_model")))
          {
            if(rs.getString("mms_equip_model").equals("ROLL IDP 3530")    || 
               rs.getString("mms_equip_model").equals("ROLL IDP 560")     ||
               rs.getString("mms_equip_model").equals("ROLL IDP 562")     || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 200") || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 220") || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 250") || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 600") || 
               rs.getString("mms_equip_model").equals("DATA CARD SILENT PRT"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "2";
            }
          }
        }
        else if(tempReq.term_application.equals(VSTDB01))
        {
          if(!isBlank(rs.getString("mms_equip_model")))
          {
            if(rs.getString("mms_equip_model").equals("ROLL PRINTER 200") || 
               rs.getString("mms_equip_model").equals("ROLL PRINTER 250") || 
               rs.getString("mms_equip_model").equals("DATA CARD SILENT PRT"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "2";
            }
          }
        }
        else if(tempReq.term_application.equals(VT46DA4))
        {}
        else if(tempReq.term_application.equals(VSVR046))
        {
          if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("ROLL 350/355"))
          {
            tempReq.printer_model = rs.getString("mms_equip_model");
            tempReq.printer_type  = "3";
          }
          else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("ROLL PRINTER 250"))
          {
            tempReq.printer_model = rs.getString("mms_equip_model");
            tempReq.printer_type  = "1";
          }
          else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("ROLL PRINTER 300"))
          {
            tempReq.printer_model = rs.getString("mms_equip_model");
            tempReq.printer_type  = "2";
          }
          else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("ROLL PRINTER 900R"))
          {
            tempReq.printer_model = rs.getString("mms_equip_model");
            tempReq.printer_type  = "5";
          }
        }
        else if(tempReq.term_application.equals(D52C8072))
        {
          tempReq.printer_model = "3";
          tempReq.printer_type  = "1";
        }
        else if(tempReq.term_application.equals(RR))
        {
          if(tempReq.term_model.equals("T7E") || tempReq.term_model.equals("T8"))
          {
            if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("DATA CARD SILENT PRT"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "2";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P7 SPROCKET"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "1";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P8 ROLL"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "7";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P8 SPROCKET"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "8";
            }
          }
        }
        else if(tempReq.term_application.equals(DRR))
        {
          if(tempReq.term_model.equals("T7E") || tempReq.term_model.equals("T8"))
          {
            if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("DATA CARD SILENT PRT"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "2";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P7 SPROCKET"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "1";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P8 ROLL"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "7";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P8 SPROCKET"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "8";
            }
          }
        }
        else if(tempReq.term_application.equals(HTL))
        {
          if(tempReq.term_model.equals("T7E") || tempReq.term_model.equals("T8"))
          {
            if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("DATA CARD SILENT PRT"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "2";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P7 SPROCKET"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "1";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P8 ROLL"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "7";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P8 SPROCKET"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "8";
            }
          }
        }
        else if(tempReq.term_application.equals(DHTL))
        {
          if(tempReq.term_model.equals("T7E") || tempReq.term_model.equals("T8"))
          {
            if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("DATA CARD SILENT PRT"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "2";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P7 SPROCKET"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "1";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P8 ROLL"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "7";
            }
            else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("P8 SPROCKET"))
            {
              tempReq.printer_model = rs.getString("mms_equip_model");
              tempReq.printer_type  = "8";
            }
          }
        }
      }
      
      rs.close(); 
      it.close();
    }
    catch(Exception e)
    {
      logEntry("(getPrinter)", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }



  private void getPinPad(MmsRequest tempReq)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {
      //default each termapp to no pinpad available and no pinpad type... if a pinpad exist this will change
      tempReq.pinpad_available = "0";
      tempReq.pinpad_type      = "0";

      //these term apps and models have pin pads integrated, so they are always set
      if(tempReq.term_application.equals(VSVR046) && tempReq.term_model.equals("OMNI 3210"))
      {
        tempReq.pinpad_available = "";
        tempReq.pinpad_type      = "4";
        return;
      }
      else if(tempReq.term_application.equals(D52C8072) && (tempReq.term_model.equals("T-IPP") || tempReq.term_model.equals("T-IPPS")))
      {
        tempReq.pinpad_available = "";
        tempReq.pinpad_type      = "1";
        return;      
      }


      //get pinpads and try to figure out which terminal applications to use
      /*@lineinfo:generated-code*//*@lineinfo:1170^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  me.equip_model,
//                  me.merchequip_equip_quantity,
//                  em.equipmfgr_mfr_name,
//                  eq.equiptype_code,
//                  eq.mms_equip_model
//  
//          from    merchequipment  me,
//                  equipment       eq,
//                  equipmfgr       em
//  
//          where   me.app_seq_num        = :tempReq.generalInfo.app_seq_num  and
//                  me.equip_model        = eq.equip_model              and
//                  eq.equipmfgr_mfr_code = em.equipmfgr_mfr_code       and
//                  eq.equiptype_code in (3)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  me.equip_model,\n                me.merchequip_equip_quantity,\n                em.equipmfgr_mfr_name,\n                eq.equiptype_code,\n                eq.mms_equip_model\n\n        from    merchequipment  me,\n                equipment       eq,\n                equipmfgr       em\n\n        where   me.app_seq_num        =  :1   and\n                me.equip_model        = eq.equip_model              and\n                eq.equipmfgr_mfr_code = em.equipmfgr_mfr_code       and\n                eq.equiptype_code in (3)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,tempReq.generalInfo.app_seq_num);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.ops.BbtMmsSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1186^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {
        if(tempReq.term_application.equals(VXP0P16))
        {
        }
        else if(tempReq.term_application.equals(VT1TP08))
        {
        }
        else if(tempReq.term_application.equals(VT1TPA7))
        {
        }
        else if(tempReq.term_application.equals(VT1TPH6))
        {
        }
        else if(tempReq.term_application.equals(VT1H385))
        {
        }
        else if(tempReq.term_application.equals(VT13DA8))
        {
          tempReq.pinpad_available = "3";
          tempReq.pinpad_type      = "1";
        }
        else if(tempReq.term_application.equals(VT46DA4))
        {
          tempReq.pinpad_available = "3";
          tempReq.pinpad_type      = "1";
        }
        else if(tempReq.term_application.equals(VSTDB01))
        {
          tempReq.pinpad_available = "3";
          tempReq.pinpad_type      = "1";
        }
        else if(tempReq.term_application.equals(VSVR046))
        {
          if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("PIN PAD 1000"))
          {
            tempReq.pinpad_available = "";
            tempReq.pinpad_type      = "2";
          }
          else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("PIN PAD 102"))
          {
            tempReq.pinpad_available = "";
            tempReq.pinpad_type      = "3";
          }
          else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("PIN PAD 2000"))
          {
            tempReq.pinpad_available = "";
            tempReq.pinpad_type      = "8";
          }
          else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("PIN PAD 0470 IPP"))
          {
            tempReq.pinpad_available = "";
            tempReq.pinpad_type      = "4";
          }
        }
        else if(tempReq.term_application.equals(D52C8072))
        {
          tempReq.pinpad_available = "";
          tempReq.pinpad_type      = "2"; //if anything set it to t-pad
        }
        else if(tempReq.term_application.equals(RR))
        {
        }
        else if(tempReq.term_application.equals(DRR))
        {
          if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("S7"))
          {
            tempReq.pinpad_available = "3";
            tempReq.pinpad_type      = "3";
          }
          else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("S8"))
          {
            tempReq.pinpad_available = "3";
            tempReq.pinpad_type      = "4";
          }
        }
        else if(tempReq.term_application.equals(HTL))
        {
        }
        else if(tempReq.term_application.equals(DHTL))
        {
          if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("S7"))
          {
            tempReq.pinpad_available = "3";
            tempReq.pinpad_type      = "3";
          }
          else if(!isBlank(rs.getString("mms_equip_model")) && rs.getString("mms_equip_model").equals("S8"))
          {
            tempReq.pinpad_available = "3";
            tempReq.pinpad_type      = "4";
          }
        }
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("(getPinPad)", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  private void fixDiscrepencies(MmsRequest tempReq)
  {
    if(tempReq.term_application.equals(VXP0P16))
    {
    }
    else if(tempReq.term_application.equals(VT1TP08))
    {
      tempReq.generalInfo.tip_option = "1"; //ALWAYS EQUALS 1 (no tip/server/clerk)
    }
    else if(tempReq.term_application.equals(VT1TPA7))
    {
      tempReq.generalInfo.tip_option = "2"; //tip with server
    }
    else if(tempReq.term_application.equals(VT1TPH6))
    {
      tempReq.generalInfo.tip_option = "1"; //ALWAYS EQUALS 1 (no tip/server/clerk)
    }
    else if(tempReq.term_application.equals(VT1H385))
    {
      tempReq.generalInfo.tip_option = "1"; //retail... i dont know if this is right.. i dont care.. for now it works for me
    }
    else if(tempReq.term_application.equals(VT13DA8))
    {
      tempReq.generalInfo.atm_cancel_return      = "4"; //non-interlink returns
      tempReq.generalInfo.atm_receipt_sign_line  = "3"; //disabled

      if(tempReq.generalInfo.tip_option.equals("Y"))
      {
        tempReq.generalInfo.tip_option = "2"; //tip with server
      }
      else
      {
        tempReq.generalInfo.tip_option = "1"; //no tip/server/clerk
      }
    }
    else if(tempReq.term_application.equals(VT46DA4))
    {

      if(tempReq.generalInfo.merchant_aba.equals("990024552"))
      {
        tempReq.generalInfo.atm_cancel_return      = "4"; //non-interlink returns
        tempReq.generalInfo.atm_receipt_sign_line  = "1"; //disabled
      }
      else
      {
        tempReq.generalInfo.atm_cancel_return      = "0"; //non-interlink returns
        tempReq.generalInfo.atm_receipt_sign_line  = "1"; //disabled
      }

      if(tempReq.generalInfo.tip_option.equals("Y"))
      {
        tempReq.generalInfo.tip_option = "2"; //tip with server
      }
      else
      {
        tempReq.generalInfo.tip_option = "1"; //no tip/server/clerk
      }
    }
    else if(tempReq.term_application.equals(VSVR046))
    {
      tempReq.generalInfo.atm_cancel_return      = "4"; //non-interlink returns (not even used anyway for this term app)
      tempReq.generalInfo.atm_receipt_sign_line  = "0"; //disabled
    }
    else if(tempReq.term_application.equals(D52C8072))
    {
      tempReq.generalInfo.atm_cancel_return       = "2"; //interlink returns 
      tempReq.generalInfo.atm_receipt_sign_line   = "3"; //disabled (not even used anyway for this term app)
      tempReq.generalInfo.primary_key1            = "PRCH";
      tempReq.generalInfo.primary_key2            = "MOTO";

      if(tempReq.generalInfo.tip_option.equals("Y"))
      {
        tempReq.generalInfo.tip_option = "2"; //tip with server
      }
      else
      {
        tempReq.generalInfo.tip_option = "0"; //no tip
      }

    }

    else if(tempReq.term_application.equals(VSTDB01))
    {
      tempReq.generalInfo.atm_cancel_return       = "2"; //interlink returns 
      tempReq.generalInfo.atm_receipt_sign_line   = "3"; //disabled (not even used anyway for this term app)

      if(tempReq.generalInfo.tip_option.equals("Y"))
      {
        tempReq.generalInfo.tip_option = "2"; //tip with server
      }
      else
      {
        tempReq.generalInfo.tip_option = "1"; //no tip/clerk/server
      }

    }

    else if(tempReq.term_application.equals(RR))
    {
    }
    else if(tempReq.term_application.equals(DRR))
    {
    }
    else if(tempReq.term_application.equals(HTL))
    {
    }
    else if(tempReq.term_application.equals(DHTL))
    {
    }

  }


  //public static final int BUSINESS_NATURE_UNDEFINED       = 0;
  //public static final int BUSINESS_NATURE_RETAIL          = 1;
  //public static final int BUSINESS_NATURE_RESTAURANT      = 2;
  //public static final int BUSINESS_NATURE_LODGING         = 3;
  //public static final int BUSINESS_NATURE_GOV_PURCHCARD   = 4;
  //public static final int BUSINESS_NATURE_KEYTAIL         = 5;
  //public static final int BUSINESS_NATURE_MOTO            = 6;
  //public static final int BUSINESS_NATURE_INTERNET        = 7;


  private String getTermApp(int busNature, String terminal, boolean isDebit, boolean isRestaurant)
  {
    String result = "";

    if(!isDebit && terminal.equals("TRANZ 330"))
    {
      if(busNature == mesConstants.BUSINESS_NATURE_LODGING)
      {
        result = VT1TPH6;
      }
      else if(isRestaurant)
      { 
        result = VT1TPA7;
      }
      else
      {  
        result = VT1TP08;
      }
    }
    else if(isDebit && terminal.equals("TRANZ 330"))
    {
      result = VSTDB01;
    }
    else if(terminal.equals("TRANZ 380"))
    {
      switch(busNature)
      {
        case mesConstants.BUSINESS_NATURE_LODGING:
          result = VT1H385;
        break;
        
        default:
          //disabled for now.. probably will be replaced later on
          //rresult = VT13DA8;
        break;
      }
    }
    else if(terminal.equals("XL300") || terminal.equals("ZON JR XL"))
    {
      result = VXP0P16;
    }
    else if(terminal.equals("TRANZ 460"))
    {
      result = VT46DA4;
    }
    else if(terminal.equals("OMNI 395") || terminal.equals("OMNI 396") || terminal.equals("OMNI 470") || terminal.equals("OMNI 3200") || terminal.equals("OMNI 3210"))
    {
      //not certified yet so we make blank
      //result = VSVR046;
      result = "";
    }
    else if(terminal.equals("T-ONE") || terminal.equals("T-IPP") || terminal.equals("T-IPPS"))
    {
      result = D52C8072;
    }
    else if(terminal.equals("T7E") || terminal.equals("T7P") || terminal.equals("T77") || terminal.equals("T8"))
    {
      if(busNature == mesConstants.BUSINESS_NATURE_LODGING)
      {
        if(isDebit)
        {
          result = DHTL;
        }
        else
        {
          result = HTL;
        }
      }
      else
      {
        if(isDebit)
        {
          result = DRR;
        }
        else
        {
          result = RR;
        }
      }
    }

    return result;
  }

  private long getNewReqId()
  {
    long reqId = -1;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1511^7*/

//  ************************************************************
//  #sql [Ctx] { select   mms_application_sequence.nextval 
//          
//          from     dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select   mms_application_sequence.nextval \n         \n        from     dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.ops.BbtMmsSetup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   reqId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1516^7*/
    }
    catch(Exception e)
    {
      logEntry("(getNewReqId)", e.toString());
    }

    System.out.println("reqId: " + reqId);
    return reqId;
  }

  private String getMilitaryTime(String h12Time)
  {
    int    colon   = h12Time.indexOf(":");

    if(colon < 0)
    {
      return "2000"; //if time is not formated right.. we just set close time to 8pm
    }
    
    int hr        = Integer.parseInt(h12Time.substring(0,colon));
    int mn        = Integer.parseInt(h12Time.substring(colon+1, colon+3));
    boolean isPm  = (h12Time.indexOf("PM") > 0);

    if(hr == 12)
    {
      hr = 0;
    }

    hr *= 100;

    if(isPm)
    {
      hr += 1200;
    }

    hr += mn;

    return StringUtilities.rightJustify(Integer.toString(hr), 4, '0');
  }

  
  private boolean doMmsSetup(long appSeqNum, boolean overrideDiscover)
  {
    boolean result      = false;

    try
    {
      connect();

      //lets make sure its a bbt account before we proceed
      //if not bbt.. then we exit from this bean...
      if(!isBbtAccount(appSeqNum))
      {
        return true;
      }

      //if waiting for a discover account to be setup.. 
      //we hold off on setting up mms... in the case when we dont want to wait for discover to come back
      //we can over ride waiting for discover.. and have the account sent to mms without having a discover number..
      //then the user will have to go back into mms and manually update it later.. this was bbt's request
      if(!overrideDiscover && holdOffOnMms(appSeqNum))
      {
        return true;
      }


      //get all relevant information from application (doing any conversions if necessary) 
      //store in class which holds variables that represents the database fields since 
      //the mms piece feeds off the database
      if(getMmsGeneralInfo(appSeqNum))
      {
        //loop through terminal applications vector and store in database assigning unique requestId to each terminal app
        submitRequests();
      }

      if(placeInExceptionQueue)
      {
        QueueTools.insertQueue(appSeqNum, MesQueues.Q_MMS_EXCEPTION);
        QueueNotes.addNote(appSeqNum, MesQueues.Q_MMS_EXCEPTION, "SYSTEM", exceptionQueueNote);
      }
      
      //update mms_updated field in merchant_discover_app table.. 
      //but only if discover request was sent back with an approval status
      setMmsUpdated(appSeqNum);

      result = true;
    }
    catch(Exception e)
    {
      logEntry("doMmsSetup(" + appSeqNum + ")", e.toString());
      result = false;
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public static boolean bbtMmsSetup(long appSeqNum)
  {
    BbtMmsSetup bms = new BbtMmsSetup();
    return bms.doMmsSetup(appSeqNum, false);
  }
  
  public static boolean bbtMmsSetup(long appSeqNum, boolean overrideDiscover)
  {
    BbtMmsSetup bms = new BbtMmsSetup();
    return bms.doMmsSetup(appSeqNum, overrideDiscover);
  }


  public static boolean bbtMmsSetup(long appSeqNum, DefaultContext Ctx)
  {
    BbtMmsSetup bms = new BbtMmsSetup(Ctx);
    return bms.doMmsSetup(appSeqNum, false);
  }

  public boolean isBlank(String str)
  {
    if(str == null || str.equals(""))
    {
      return true;
    }

    return false;
  }

  public class MmsRequest
  {
    public long   request_id                = 0L;
    public String term_application          = "STAGE";
    public String term_application_profgen  = "";
    public MmsGeneralInfo  generalInfo      = null;

    public String manufacturer              = "";
    public String equip_model               = "";
    public String term_model                = "";
    public String printer_model             = "";
    public String printer_type              = "";
    public String pinpad_available          = "";
    public String pinpad_type               = "";

    public MmsRequest(long req, MmsGeneralInfo mgi)
    {
      this.request_id   = req;
      this.generalInfo  = mgi;
    }
  }


  public class MmsGeneralInfo
  {
    //mms_stage_info
    public long   app_seq_num               = 0L;
    public String special_handling          = "N";
    public String process_method            = "FTP";
    public String process_status            = "new";
    public String bin                       = "407314";
    public String agent                     = "000000";
    public String chain                     = "";
    public String merch_number              = "";
    public String store_number              = "4616";
    public String terminal_number           = "";
    public String business_name             = "";
    public String business_address          = "";
    public String business_city             = "";
    public String business_state            = "";
    public String business_zip              = "";
    public String merch_phone               = "";
    public String service_level             = "RAO";
    public String attachment_code           = "IT";
    public String mcc                       = "";
    public String disc                      = "";
    public String disc_ac                   = "";
    public String amex                      = "";
    public String amex_ac                   = "";
    public String split_dial                = "";
    public String dinr                      = "";
    public String dinr_ac                   = "";
    public String jcb                       = "";
    public String jcb_ac                    = "";
    public String term_voice_id             = "";
    public String voice_auth_phone          = "18002914840";
    public String cdset                     = "00";
    public String location_number           = "00001";
    public String time_zone                 = "";
    public String dst                       = "Y";
    public String contact_name              = "";
    public String project_num               = "";
    public String mcfs                      = "";
    public String customer_service_num      = "";
    public String profile_generator_code    = "1"; //1 for mms 2 vericenter for 3 for term master

    //mms_expanded_info
    public String amex_option                   = "";
    public String amex_pabx                     = "";
    public String atm_cancel_return             = "";
    public String atm_receipt_sign_line         = "";
    public String auto_close_ind                = "";
    public String auto_close_reports            = "";
    public String auto_close_time               = "";
    public String auto_close_time2              = "";
    public String auto_tip_percent              = "00";
    public String avs                           = "";
    public String baud_rate_amexhost            = "";
    public String baud_rate_host1               = "";
    public String card_present                  = "3";
    public String cash_back                     = "0";
    public String cash_back_limit               = "00000"; 
    public String cash_back_tip                 = "0"; 
    public String check_auth_phone              = "";
    public String check_host                    = "";
    public String check_host_format             = "";
    public String check_mid                     = "";
    public String check_payment                 = "";
    public String check_reader                  = "";
    public String check_totals_ind              = "";
    public String check_voice                   = "";
    public String clerk_server_mode             = "";
    public String debit_settle_num              = "";
    public String dial_speed                    = "";
    public String download_speed                = "";
    public String dup_tran                      = "";
    public String ebt_fcs_id                    = "";
    public String ebt_trans_type                = "";
    public String eci_type                      = "";
    public String folio_guest_check_processing  = "N";
    public String folio_room_prompt             = "";
    public String footer_line1                  = "";
    public String footer_line2                  = "";
    public String force_dial                    = "N";
    public String force_settle_ind              = "";
    public String fraud                         = "00000000";
    public String header_line4                  = "";
    public String header_line5                  = "";
    public String idle_prompt                   = "";
    public String invoice_number                = "";
    public String inv_num_keys                  = "";
    public String inv_prompt                    = "";
    public String merchant_aba                  = "000000000";
    public String merchant_id_amex              = "";
    public String merch_settlement_agent        = "V000";
    public String overage_percentage            = "";
    public String parent_vnumber                = "";
    public String pass_protect                  = "*0000000000000";
    public String primary_data_capture_phone    = "";
    public String primary_dial_access           = "";
    public String primary_key1                  = "L556";
    public String primary_key2                  = "L558";
    public String prim_auth_dial_access         = "";
    public String print_customer_copy           = "";
    public String prompt_for_clerk              = "N";
    public String purchasing_card               = "N";
    public String receipt_truncation            = "Y";
    public String reimbursement_attributes      = "0";
    public String reminder_msg                  = "";
    public String reminder_msg_ind              = "0";
    public String reminder_msg_time             = "";
    public String restaurant_processing         = "";
    public String riid                          = "";
    public String secondary_data_capture_phone  = "";
    public String secondary_dial_access         = "";
    public String sec_auth_dial_access          = "";
    public String settle_to_pc                  = "N";
    public String sharing_groups                = "";
    public String srv_clk_processing            = "";
    public String surcharge_amount              = "";
    public String surcharge_ind                 = "";
    public String time_difference               = "";
    public String tip_assist                    = "N";
    public String tip_assist_percent_1          = "00";
    public String tip_assist_percent_2          = "00";
    public String tip_assist_percent_3          = "00";
    public String tip_option                    = "";
    public String tip_time_sale                 = "";
    public String tm_options                    = "";
 
  }

  private void submitRequests()
  {

    try
    {
      
      for(int x=0; x<requests.size(); x++)
      { 
      
        MmsRequest tempReq = (MmsRequest)requests.elementAt(x);

        /*@lineinfo:generated-code*//*@lineinfo:1809^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mms_stage_info
//            (
//              request_id,
//              term_application,
//              term_application_profgen,
//              equip_model,
//              app_seq_num,
//              special_handling,
//              app_date,
//              process_method,
//              process_status,
//              bin,
//              agent,
//              chain,
//              merch_number,
//              store_number,
//              terminal_number,
//              business_name,
//              business_address,
//              business_city,
//              business_state,
//              business_zip,
//              merch_phone,
//              service_level,
//              attachment_code,
//              mcc,
//              disc,
//              disc_ac,
//              amex,
//              amex_ac,
//              split_dial,
//              dinr,
//              dinr_ac,
//              jcb,
//              jcb_ac,
//              term_voice_id,
//              voice_auth_phone,
//              cdset,
//              location_number,
//              time_zone,
//              dst,
//              contact_name,
//              project_num,
//              mcfs,
//              customer_service_num,
//              profile_generator_code
//  
//            )
//            values
//            (
//              :tempReq.request_id,
//              :tempReq.term_application,
//              :tempReq.term_application_profgen,
//              :tempReq.equip_model,
//              :tempReq.generalInfo.app_seq_num,
//              :tempReq.generalInfo.special_handling,
//              sysdate,
//              :tempReq.generalInfo.process_method,
//              :tempReq.generalInfo.process_status,
//              :tempReq.generalInfo.bin,
//              :tempReq.generalInfo.agent,
//              :tempReq.generalInfo.chain,
//              :tempReq.generalInfo.merch_number,
//              :tempReq.generalInfo.store_number,
//              :tempReq.generalInfo.terminal_number,
//              :tempReq.generalInfo.business_name,
//              :tempReq.generalInfo.business_address,
//              :tempReq.generalInfo.business_city,
//              :tempReq.generalInfo.business_state,
//              :tempReq.generalInfo.business_zip,
//              :tempReq.generalInfo.merch_phone,
//              :tempReq.generalInfo.service_level,
//              :tempReq.generalInfo.attachment_code,
//              :tempReq.generalInfo.mcc,
//              :tempReq.generalInfo.disc,
//              :tempReq.generalInfo.disc_ac,
//              :tempReq.generalInfo.amex,
//              :tempReq.generalInfo.amex_ac,
//              :tempReq.generalInfo.split_dial,
//              :tempReq.generalInfo.dinr,
//              :tempReq.generalInfo.dinr_ac,
//              :tempReq.generalInfo.jcb,
//              :tempReq.generalInfo.jcb_ac,
//              :tempReq.generalInfo.term_voice_id,
//              :tempReq.generalInfo.voice_auth_phone,
//              :tempReq.generalInfo.cdset,
//              :tempReq.generalInfo.location_number,
//              :tempReq.generalInfo.time_zone,
//              :tempReq.generalInfo.dst,
//              :tempReq.generalInfo.contact_name,
//              :tempReq.generalInfo.project_num,
//              :tempReq.generalInfo.mcfs,
//              :tempReq.generalInfo.customer_service_num,
//              :tempReq.generalInfo.profile_generator_code
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mms_stage_info\n          (\n            request_id,\n            term_application,\n            term_application_profgen,\n            equip_model,\n            app_seq_num,\n            special_handling,\n            app_date,\n            process_method,\n            process_status,\n            bin,\n            agent,\n            chain,\n            merch_number,\n            store_number,\n            terminal_number,\n            business_name,\n            business_address,\n            business_city,\n            business_state,\n            business_zip,\n            merch_phone,\n            service_level,\n            attachment_code,\n            mcc,\n            disc,\n            disc_ac,\n            amex,\n            amex_ac,\n            split_dial,\n            dinr,\n            dinr_ac,\n            jcb,\n            jcb_ac,\n            term_voice_id,\n            voice_auth_phone,\n            cdset,\n            location_number,\n            time_zone,\n            dst,\n            contact_name,\n            project_num,\n            mcfs,\n            customer_service_num,\n            profile_generator_code\n\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n            sysdate,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 ,\n             :34 ,\n             :35 ,\n             :36 ,\n             :37 ,\n             :38 ,\n             :39 ,\n             :40 ,\n             :41 ,\n             :42 ,\n             :43 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,tempReq.request_id);
   __sJT_st.setString(2,tempReq.term_application);
   __sJT_st.setString(3,tempReq.term_application_profgen);
   __sJT_st.setString(4,tempReq.equip_model);
   __sJT_st.setLong(5,tempReq.generalInfo.app_seq_num);
   __sJT_st.setString(6,tempReq.generalInfo.special_handling);
   __sJT_st.setString(7,tempReq.generalInfo.process_method);
   __sJT_st.setString(8,tempReq.generalInfo.process_status);
   __sJT_st.setString(9,tempReq.generalInfo.bin);
   __sJT_st.setString(10,tempReq.generalInfo.agent);
   __sJT_st.setString(11,tempReq.generalInfo.chain);
   __sJT_st.setString(12,tempReq.generalInfo.merch_number);
   __sJT_st.setString(13,tempReq.generalInfo.store_number);
   __sJT_st.setString(14,tempReq.generalInfo.terminal_number);
   __sJT_st.setString(15,tempReq.generalInfo.business_name);
   __sJT_st.setString(16,tempReq.generalInfo.business_address);
   __sJT_st.setString(17,tempReq.generalInfo.business_city);
   __sJT_st.setString(18,tempReq.generalInfo.business_state);
   __sJT_st.setString(19,tempReq.generalInfo.business_zip);
   __sJT_st.setString(20,tempReq.generalInfo.merch_phone);
   __sJT_st.setString(21,tempReq.generalInfo.service_level);
   __sJT_st.setString(22,tempReq.generalInfo.attachment_code);
   __sJT_st.setString(23,tempReq.generalInfo.mcc);
   __sJT_st.setString(24,tempReq.generalInfo.disc);
   __sJT_st.setString(25,tempReq.generalInfo.disc_ac);
   __sJT_st.setString(26,tempReq.generalInfo.amex);
   __sJT_st.setString(27,tempReq.generalInfo.amex_ac);
   __sJT_st.setString(28,tempReq.generalInfo.split_dial);
   __sJT_st.setString(29,tempReq.generalInfo.dinr);
   __sJT_st.setString(30,tempReq.generalInfo.dinr_ac);
   __sJT_st.setString(31,tempReq.generalInfo.jcb);
   __sJT_st.setString(32,tempReq.generalInfo.jcb_ac);
   __sJT_st.setString(33,tempReq.generalInfo.term_voice_id);
   __sJT_st.setString(34,tempReq.generalInfo.voice_auth_phone);
   __sJT_st.setString(35,tempReq.generalInfo.cdset);
   __sJT_st.setString(36,tempReq.generalInfo.location_number);
   __sJT_st.setString(37,tempReq.generalInfo.time_zone);
   __sJT_st.setString(38,tempReq.generalInfo.dst);
   __sJT_st.setString(39,tempReq.generalInfo.contact_name);
   __sJT_st.setString(40,tempReq.generalInfo.project_num);
   __sJT_st.setString(41,tempReq.generalInfo.mcfs);
   __sJT_st.setString(42,tempReq.generalInfo.customer_service_num);
   __sJT_st.setString(43,tempReq.generalInfo.profile_generator_code);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1906^9*/

        /*@lineinfo:generated-code*//*@lineinfo:1908^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mms_expanded_info
//            (
//              request_id,
//              manufacturer,
//              term_model,
//              printer_model,
//              printer_type,
//              pinpad_available,
//              pinpad_type,
//              amex_option,
//              amex_pabx,
//              atm_cancel_return,
//              atm_receipt_sign_line,
//              auto_close_ind,
//              auto_close_reports,
//              auto_close_time,
//              auto_close_time2,
//              auto_tip_percent,
//              avs,
//              baud_rate_amexhost,
//              baud_rate_host1,
//              card_present,
//              cash_back,
//              cash_back_limit,
//              cash_back_tip,
//              check_auth_phone,
//              check_host,
//              check_host_format,
//              check_mid,
//              check_payment,
//              check_reader,
//              check_totals_ind,
//              check_voice,
//              clerk_server_mode,
//              debit_settle_num,
//              dial_speed,
//              download_speed,
//              dup_tran,
//              ebt_fcs_id,
//              ebt_trans_type,
//              eci_type,
//              folio_guest_check_processing,
//              folio_room_prompt,
//              footer_line1,
//              footer_line2,
//              force_dial,
//              force_settle_ind,
//              fraud,
//              header_line4,
//              header_line5,
//              idle_prompt,
//              invoice_number,
//              inv_num_keys,
//              inv_prompt,
//              merchant_aba,
//              merchant_id_amex,
//              merch_settlement_agent,
//              overage_percentage,
//              parent_vnumber,
//              pass_protect,
//              primary_data_capture_phone,
//              primary_dial_access,
//              primary_key1,
//              primary_key2,
//              prim_auth_dial_access,
//              print_customer_copy,
//              prompt_for_clerk,
//              purchasing_card,
//              receipt_truncation,
//              reimbursement_attributes,
//              reminder_msg,
//              reminder_msg_ind,
//              reminder_msg_time,
//              restaurant_processing,
//              riid,
//              secondary_data_capture_phone,
//              secondary_dial_access,
//              sec_auth_dial_access,
//              settle_to_pc,
//              sharing_groups,
//              srv_clk_processing,
//              surcharge_amount,
//              surcharge_ind,
//              time_difference,
//              tip_assist,
//              tip_assist_percent_1,
//              tip_assist_percent_2,
//              tip_assist_percent_3,
//              tip_option,
//              tip_time_sale,
//              tm_options
//            )
//            values
//            (
//              :tempReq.request_id,
//              :tempReq.manufacturer,
//              :tempReq.term_model,
//              :tempReq.printer_model,
//              :tempReq.printer_type,
//              :tempReq.pinpad_available,
//              :tempReq.pinpad_type,
//              :tempReq.generalInfo.amex_option,
//              :tempReq.generalInfo.amex_pabx,
//              :tempReq.generalInfo.atm_cancel_return,
//              :tempReq.generalInfo.atm_receipt_sign_line,
//              :tempReq.generalInfo.auto_close_ind,
//              :tempReq.generalInfo.auto_close_reports,
//              :tempReq.generalInfo.auto_close_time,
//              :tempReq.generalInfo.auto_close_time2,
//              :tempReq.generalInfo.auto_tip_percent,
//              :tempReq.generalInfo.avs,
//              :tempReq.generalInfo.baud_rate_amexhost,
//              :tempReq.generalInfo.baud_rate_host1,
//              :tempReq.generalInfo.card_present,
//              :tempReq.generalInfo.cash_back,
//              :tempReq.generalInfo.cash_back_limit,
//              :tempReq.generalInfo.cash_back_tip,
//              :tempReq.generalInfo.check_auth_phone,
//              :tempReq.generalInfo.check_host,
//              :tempReq.generalInfo.check_host_format,
//              :tempReq.generalInfo.check_mid,
//              :tempReq.generalInfo.check_payment,
//              :tempReq.generalInfo.check_reader,
//              :tempReq.generalInfo.check_totals_ind,
//              :tempReq.generalInfo.check_voice,
//              :tempReq.generalInfo.clerk_server_mode,
//              :tempReq.generalInfo.debit_settle_num,
//              :tempReq.generalInfo.dial_speed,
//              :tempReq.generalInfo.download_speed,
//              :tempReq.generalInfo.dup_tran,
//              :tempReq.generalInfo.ebt_fcs_id,
//              :tempReq.generalInfo.ebt_trans_type,
//              :tempReq.generalInfo.eci_type,
//              :tempReq.generalInfo.folio_guest_check_processing,
//              :tempReq.generalInfo.folio_room_prompt,
//              :tempReq.generalInfo.footer_line1,
//              :tempReq.generalInfo.footer_line2,
//              :tempReq.generalInfo.force_dial,
//              :tempReq.generalInfo.force_settle_ind,
//              :tempReq.generalInfo.fraud,
//              :tempReq.generalInfo.header_line4,
//              :tempReq.generalInfo.header_line5,
//              :tempReq.generalInfo.idle_prompt,
//              :tempReq.generalInfo.invoice_number,
//              :tempReq.generalInfo.inv_num_keys,
//              :tempReq.generalInfo.inv_prompt,
//              :tempReq.generalInfo.merchant_aba,
//              :tempReq.generalInfo.merchant_id_amex,
//              :tempReq.generalInfo.merch_settlement_agent,
//              :tempReq.generalInfo.overage_percentage,
//              :tempReq.generalInfo.parent_vnumber,
//              :tempReq.generalInfo.pass_protect,
//              :tempReq.generalInfo.primary_data_capture_phone,
//              :tempReq.generalInfo.primary_dial_access,
//              :tempReq.generalInfo.primary_key1,
//              :tempReq.generalInfo.primary_key2,
//              :tempReq.generalInfo.prim_auth_dial_access,
//              :tempReq.generalInfo.print_customer_copy,
//              :tempReq.generalInfo.prompt_for_clerk,
//              :tempReq.generalInfo.purchasing_card,
//              :tempReq.generalInfo.receipt_truncation,
//              :tempReq.generalInfo.reimbursement_attributes,
//              :tempReq.generalInfo.reminder_msg,
//              :tempReq.generalInfo.reminder_msg_ind,
//              :tempReq.generalInfo.reminder_msg_time,
//              :tempReq.generalInfo.restaurant_processing,
//              :tempReq.generalInfo.riid,
//              :tempReq.generalInfo.secondary_data_capture_phone,
//              :tempReq.generalInfo.secondary_dial_access,
//              :tempReq.generalInfo.sec_auth_dial_access,
//              :tempReq.generalInfo.settle_to_pc,
//              :tempReq.generalInfo.sharing_groups,
//              :tempReq.generalInfo.srv_clk_processing,
//              :tempReq.generalInfo.surcharge_amount,
//              :tempReq.generalInfo.surcharge_ind,
//              :tempReq.generalInfo.time_difference,
//              :tempReq.generalInfo.tip_assist,
//              :tempReq.generalInfo.tip_assist_percent_1,
//              :tempReq.generalInfo.tip_assist_percent_2,
//              :tempReq.generalInfo.tip_assist_percent_3,
//              :tempReq.generalInfo.tip_option,
//              :tempReq.generalInfo.tip_time_sale,
//              :tempReq.generalInfo.tm_options
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mms_expanded_info\n          (\n            request_id,\n            manufacturer,\n            term_model,\n            printer_model,\n            printer_type,\n            pinpad_available,\n            pinpad_type,\n            amex_option,\n            amex_pabx,\n            atm_cancel_return,\n            atm_receipt_sign_line,\n            auto_close_ind,\n            auto_close_reports,\n            auto_close_time,\n            auto_close_time2,\n            auto_tip_percent,\n            avs,\n            baud_rate_amexhost,\n            baud_rate_host1,\n            card_present,\n            cash_back,\n            cash_back_limit,\n            cash_back_tip,\n            check_auth_phone,\n            check_host,\n            check_host_format,\n            check_mid,\n            check_payment,\n            check_reader,\n            check_totals_ind,\n            check_voice,\n            clerk_server_mode,\n            debit_settle_num,\n            dial_speed,\n            download_speed,\n            dup_tran,\n            ebt_fcs_id,\n            ebt_trans_type,\n            eci_type,\n            folio_guest_check_processing,\n            folio_room_prompt,\n            footer_line1,\n            footer_line2,\n            force_dial,\n            force_settle_ind,\n            fraud,\n            header_line4,\n            header_line5,\n            idle_prompt,\n            invoice_number,\n            inv_num_keys,\n            inv_prompt,\n            merchant_aba,\n            merchant_id_amex,\n            merch_settlement_agent,\n            overage_percentage,\n            parent_vnumber,\n            pass_protect,\n            primary_data_capture_phone,\n            primary_dial_access,\n            primary_key1,\n            primary_key2,\n            prim_auth_dial_access,\n            print_customer_copy,\n            prompt_for_clerk,\n            purchasing_card,\n            receipt_truncation,\n            reimbursement_attributes,\n            reminder_msg,\n            reminder_msg_ind,\n            reminder_msg_time,\n            restaurant_processing,\n            riid,\n            secondary_data_capture_phone,\n            secondary_dial_access,\n            sec_auth_dial_access,\n            settle_to_pc,\n            sharing_groups,\n            srv_clk_processing,\n            surcharge_amount,\n            surcharge_ind,\n            time_difference,\n            tip_assist,\n            tip_assist_percent_1,\n            tip_assist_percent_2,\n            tip_assist_percent_3,\n            tip_option,\n            tip_time_sale,\n            tm_options\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 ,\n             :34 ,\n             :35 ,\n             :36 ,\n             :37 ,\n             :38 ,\n             :39 ,\n             :40 ,\n             :41 ,\n             :42 ,\n             :43 ,\n             :44 ,\n             :45 ,\n             :46 ,\n             :47 ,\n             :48 ,\n             :49 ,\n             :50 ,\n             :51 ,\n             :52 ,\n             :53 ,\n             :54 ,\n             :55 ,\n             :56 ,\n             :57 ,\n             :58 ,\n             :59 ,\n             :60 ,\n             :61 ,\n             :62 ,\n             :63 ,\n             :64 ,\n             :65 ,\n             :66 ,\n             :67 ,\n             :68 ,\n             :69 ,\n             :70 ,\n             :71 ,\n             :72 ,\n             :73 ,\n             :74 ,\n             :75 ,\n             :76 ,\n             :77 ,\n             :78 ,\n             :79 ,\n             :80 ,\n             :81 ,\n             :82 ,\n             :83 ,\n             :84 ,\n             :85 ,\n             :86 ,\n             :87 ,\n             :88 ,\n             :89 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,tempReq.request_id);
   __sJT_st.setString(2,tempReq.manufacturer);
   __sJT_st.setString(3,tempReq.term_model);
   __sJT_st.setString(4,tempReq.printer_model);
   __sJT_st.setString(5,tempReq.printer_type);
   __sJT_st.setString(6,tempReq.pinpad_available);
   __sJT_st.setString(7,tempReq.pinpad_type);
   __sJT_st.setString(8,tempReq.generalInfo.amex_option);
   __sJT_st.setString(9,tempReq.generalInfo.amex_pabx);
   __sJT_st.setString(10,tempReq.generalInfo.atm_cancel_return);
   __sJT_st.setString(11,tempReq.generalInfo.atm_receipt_sign_line);
   __sJT_st.setString(12,tempReq.generalInfo.auto_close_ind);
   __sJT_st.setString(13,tempReq.generalInfo.auto_close_reports);
   __sJT_st.setString(14,tempReq.generalInfo.auto_close_time);
   __sJT_st.setString(15,tempReq.generalInfo.auto_close_time2);
   __sJT_st.setString(16,tempReq.generalInfo.auto_tip_percent);
   __sJT_st.setString(17,tempReq.generalInfo.avs);
   __sJT_st.setString(18,tempReq.generalInfo.baud_rate_amexhost);
   __sJT_st.setString(19,tempReq.generalInfo.baud_rate_host1);
   __sJT_st.setString(20,tempReq.generalInfo.card_present);
   __sJT_st.setString(21,tempReq.generalInfo.cash_back);
   __sJT_st.setString(22,tempReq.generalInfo.cash_back_limit);
   __sJT_st.setString(23,tempReq.generalInfo.cash_back_tip);
   __sJT_st.setString(24,tempReq.generalInfo.check_auth_phone);
   __sJT_st.setString(25,tempReq.generalInfo.check_host);
   __sJT_st.setString(26,tempReq.generalInfo.check_host_format);
   __sJT_st.setString(27,tempReq.generalInfo.check_mid);
   __sJT_st.setString(28,tempReq.generalInfo.check_payment);
   __sJT_st.setString(29,tempReq.generalInfo.check_reader);
   __sJT_st.setString(30,tempReq.generalInfo.check_totals_ind);
   __sJT_st.setString(31,tempReq.generalInfo.check_voice);
   __sJT_st.setString(32,tempReq.generalInfo.clerk_server_mode);
   __sJT_st.setString(33,tempReq.generalInfo.debit_settle_num);
   __sJT_st.setString(34,tempReq.generalInfo.dial_speed);
   __sJT_st.setString(35,tempReq.generalInfo.download_speed);
   __sJT_st.setString(36,tempReq.generalInfo.dup_tran);
   __sJT_st.setString(37,tempReq.generalInfo.ebt_fcs_id);
   __sJT_st.setString(38,tempReq.generalInfo.ebt_trans_type);
   __sJT_st.setString(39,tempReq.generalInfo.eci_type);
   __sJT_st.setString(40,tempReq.generalInfo.folio_guest_check_processing);
   __sJT_st.setString(41,tempReq.generalInfo.folio_room_prompt);
   __sJT_st.setString(42,tempReq.generalInfo.footer_line1);
   __sJT_st.setString(43,tempReq.generalInfo.footer_line2);
   __sJT_st.setString(44,tempReq.generalInfo.force_dial);
   __sJT_st.setString(45,tempReq.generalInfo.force_settle_ind);
   __sJT_st.setString(46,tempReq.generalInfo.fraud);
   __sJT_st.setString(47,tempReq.generalInfo.header_line4);
   __sJT_st.setString(48,tempReq.generalInfo.header_line5);
   __sJT_st.setString(49,tempReq.generalInfo.idle_prompt);
   __sJT_st.setString(50,tempReq.generalInfo.invoice_number);
   __sJT_st.setString(51,tempReq.generalInfo.inv_num_keys);
   __sJT_st.setString(52,tempReq.generalInfo.inv_prompt);
   __sJT_st.setString(53,tempReq.generalInfo.merchant_aba);
   __sJT_st.setString(54,tempReq.generalInfo.merchant_id_amex);
   __sJT_st.setString(55,tempReq.generalInfo.merch_settlement_agent);
   __sJT_st.setString(56,tempReq.generalInfo.overage_percentage);
   __sJT_st.setString(57,tempReq.generalInfo.parent_vnumber);
   __sJT_st.setString(58,tempReq.generalInfo.pass_protect);
   __sJT_st.setString(59,tempReq.generalInfo.primary_data_capture_phone);
   __sJT_st.setString(60,tempReq.generalInfo.primary_dial_access);
   __sJT_st.setString(61,tempReq.generalInfo.primary_key1);
   __sJT_st.setString(62,tempReq.generalInfo.primary_key2);
   __sJT_st.setString(63,tempReq.generalInfo.prim_auth_dial_access);
   __sJT_st.setString(64,tempReq.generalInfo.print_customer_copy);
   __sJT_st.setString(65,tempReq.generalInfo.prompt_for_clerk);
   __sJT_st.setString(66,tempReq.generalInfo.purchasing_card);
   __sJT_st.setString(67,tempReq.generalInfo.receipt_truncation);
   __sJT_st.setString(68,tempReq.generalInfo.reimbursement_attributes);
   __sJT_st.setString(69,tempReq.generalInfo.reminder_msg);
   __sJT_st.setString(70,tempReq.generalInfo.reminder_msg_ind);
   __sJT_st.setString(71,tempReq.generalInfo.reminder_msg_time);
   __sJT_st.setString(72,tempReq.generalInfo.restaurant_processing);
   __sJT_st.setString(73,tempReq.generalInfo.riid);
   __sJT_st.setString(74,tempReq.generalInfo.secondary_data_capture_phone);
   __sJT_st.setString(75,tempReq.generalInfo.secondary_dial_access);
   __sJT_st.setString(76,tempReq.generalInfo.sec_auth_dial_access);
   __sJT_st.setString(77,tempReq.generalInfo.settle_to_pc);
   __sJT_st.setString(78,tempReq.generalInfo.sharing_groups);
   __sJT_st.setString(79,tempReq.generalInfo.srv_clk_processing);
   __sJT_st.setString(80,tempReq.generalInfo.surcharge_amount);
   __sJT_st.setString(81,tempReq.generalInfo.surcharge_ind);
   __sJT_st.setString(82,tempReq.generalInfo.time_difference);
   __sJT_st.setString(83,tempReq.generalInfo.tip_assist);
   __sJT_st.setString(84,tempReq.generalInfo.tip_assist_percent_1);
   __sJT_st.setString(85,tempReq.generalInfo.tip_assist_percent_2);
   __sJT_st.setString(86,tempReq.generalInfo.tip_assist_percent_3);
   __sJT_st.setString(87,tempReq.generalInfo.tip_option);
   __sJT_st.setString(88,tempReq.generalInfo.tip_time_sale);
   __sJT_st.setString(89,tempReq.generalInfo.tm_options);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2094^9*/

        //insert place holders in vnumber tables
        /*@lineinfo:generated-code*//*@lineinfo:2097^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_vnumber
//            ( 
//              APP_SEQ_NUM,
//              REQUEST_ID,
//              TERMINAL_NUMBER,
//              STORE_NUMBER,
//              TIME_ZONE,
//              LOCATION_NUMBER,
//              APP_CODE,
//              EQUIP_MODEL,
//              PROFILE_GENERATOR_CODE
//            )
//            values
//            ( 
//              :tempReq.generalInfo.app_seq_num,
//              :tempReq.request_id,
//              :tempReq.generalInfo.terminal_number,
//              :tempReq.generalInfo.store_number,
//              705,
//              :tempReq.generalInfo.location_number,
//              :tempReq.term_application_profgen,
//              :tempReq.equip_model,
//              :tempReq.generalInfo.profile_generator_code
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_vnumber\n          ( \n            APP_SEQ_NUM,\n            REQUEST_ID,\n            TERMINAL_NUMBER,\n            STORE_NUMBER,\n            TIME_ZONE,\n            LOCATION_NUMBER,\n            APP_CODE,\n            EQUIP_MODEL,\n            PROFILE_GENERATOR_CODE\n          )\n          values\n          ( \n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n            705,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,tempReq.generalInfo.app_seq_num);
   __sJT_st.setLong(2,tempReq.request_id);
   __sJT_st.setString(3,tempReq.generalInfo.terminal_number);
   __sJT_st.setString(4,tempReq.generalInfo.store_number);
   __sJT_st.setString(5,tempReq.generalInfo.location_number);
   __sJT_st.setString(6,tempReq.term_application_profgen);
   __sJT_st.setString(7,tempReq.equip_model);
   __sJT_st.setString(8,tempReq.generalInfo.profile_generator_code);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2123^9*/

        /*@lineinfo:generated-code*//*@lineinfo:2125^9*/

//  ************************************************************
//  #sql [Ctx] { insert into v_numbers
//            ( 
//              APP_SEQ_NUM,
//              REQUEST_ID,
//              TERMINAL_NUMBER,
//              MODEL_INDEX,
//              STORE_NUMBER,
//              TIME_ZONE,
//              LOCATION_NUMBER,
//              APP_CODE,
//              EQUIP_MODEL,
//              POS_CODE,
//              AUTO_FLAG
//            )
//            values
//            ( 
//              :tempReq.generalInfo.app_seq_num,
//              :tempReq.request_id,
//              :tempReq.generalInfo.terminal_number,
//              :tempReq.generalInfo.terminal_number,
//              :tempReq.generalInfo.store_number,
//              705,
//              :tempReq.generalInfo.location_number,
//              :tempReq.term_application_profgen,
//              :tempReq.equip_model,
//              101,
//              'Y'
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into v_numbers\n          ( \n            APP_SEQ_NUM,\n            REQUEST_ID,\n            TERMINAL_NUMBER,\n            MODEL_INDEX,\n            STORE_NUMBER,\n            TIME_ZONE,\n            LOCATION_NUMBER,\n            APP_CODE,\n            EQUIP_MODEL,\n            POS_CODE,\n            AUTO_FLAG\n          )\n          values\n          ( \n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n            705,\n             :6 ,\n             :7 ,\n             :8 ,\n            101,\n            'Y'\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.ops.BbtMmsSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,tempReq.generalInfo.app_seq_num);
   __sJT_st.setLong(2,tempReq.request_id);
   __sJT_st.setString(3,tempReq.generalInfo.terminal_number);
   __sJT_st.setString(4,tempReq.generalInfo.terminal_number);
   __sJT_st.setString(5,tempReq.generalInfo.store_number);
   __sJT_st.setString(6,tempReq.generalInfo.location_number);
   __sJT_st.setString(7,tempReq.term_application_profgen);
   __sJT_st.setString(8,tempReq.equip_model);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2155^9*/

      }
    }
    catch(Exception e)
    {
      placeInExceptionQueue = true;
      exceptionQueueNote    = "Insert into mms_stage_info failed!";
      logEntry("(submitRequests)", e.toString());
    }
  }

}/*@lineinfo:generated-code*/