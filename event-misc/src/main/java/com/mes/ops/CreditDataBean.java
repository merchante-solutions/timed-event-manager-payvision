/*@lineinfo:filename=CreditDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/ops/CreditDataBean.sqlj $

  Description:
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-05-21 15:33:20 -0700 (Wed, 21 May 2008) $
  Version            : $Revision: 14888 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class CreditDataBean extends com.mes.screens.SequenceDataBean
{
  private static final int CHAIN_MERCHANT     = 2;
  private static final int ADD_CHAIN_MERCHANT = 3;

  // bean data
  private int           sicCode           = 0;
  private String        invCode           = "";
  private String        metCode           = "";
  private int           creditDelay       = 0;
  private int           debitDelay        = 0;
  private int           daysSuspend       = 0;

  private boolean       transcomOneDayDelay = false;

  //data expansion stuff
  private String        association       = "";
  private String        bankNumber        = "";
  private String        statusIndicator   = "";
  private String        incStatus         = "";
  private String        gender            = "";
  private String        userData1         = "";
  private String        userData2         = "";
  private String        userData3         = "";
  private String        userData4         = "";
  private String        userData5         = "";
  private String        userAccount1      = "";
  private String        userAccount2      = "";

  private boolean       optionalData      = false;
  private boolean       chainMerchant     = false;
  private boolean       variablePricing   = false;
  private String        allowAutoUpload   = "";
  // option data
  private Vector        investigators     = new Vector();
  private Vector        investigatorDescs = new Vector();
  private Vector        metTables         = new Vector();
  private Vector        metTableDescs     = new Vector();

  //data expansion stuff
  private Vector        statuses          = new Vector();
  private Vector        statusDescs       = new Vector();
  private Vector        genders           = new Vector();
  private Vector        genderDescs       = new Vector();
  private Vector        incStatuses       = new Vector();
  private Vector        incStatusDescs    = new Vector();

  private void setStatuses()
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:95^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    status
//          order by status_ind asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    status\n        order by status_ind asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.CreditDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:100^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        statuses.add(rs.getString("status_ind"));
        statusDescs.add(rs.getString("status_descr"));
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setStatuses(): " + e.toString());
      addError("setStatuses(): " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void setGenders()
  {
    genders.add("U");
    genderDescs.add("Unspecified");
    genders.add("M");
    genderDescs.add("Male");
    genders.add("F");
    genderDescs.add("Female");
  }

  private void setIncStatuses()
  {
    StringBuffer      qs      = new StringBuffer("");
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      // create info vectors
      /*@lineinfo:generated-code*//*@lineinfo:147^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    bustype
//          order by bustype_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    bustype\n        order by bustype_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.CreditDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:152^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        incStatuses.add(Integer.toString(rs.getInt("bustype_code")));
        incStatusDescs.add(rs.getString("bustype_desc"));
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setIncStatuses(): " + e.toString());
      addError("setIncStatuses(): " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void loadOptionData()
  {
    StringBuffer      desc        = new StringBuffer("");
    ResultSetIterator it          = null;
    ResultSet         rs          = null;
    DecimalFormat     FourDigits  = (DecimalFormat)NumberFormat.getNumberInstance();

    FourDigits.applyPattern("0000");

    // investigators
    investigators.add("");
    investigatorDescs.add("select from list");
    try
    {
      connect();

      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:196^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  investigator_code,
//                    investigator_fname,
//                    investigator_lname
//            from    investigators
//            order by investigator_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  investigator_code,\n                  investigator_fname,\n                  investigator_lname\n          from    investigators\n          order by investigator_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.CreditDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:203^9*/

        rs = it.getResultSet();

        while(rs.next())
        {
          desc.setLength(0);
          investigators.add(rs.getString("investigator_code"));

          desc.append(rs.getString("investigator_code"));
          desc.append(" (");
          desc.append(rs.getString("investigator_fname"));
          desc.append(" ");
          desc.append(rs.getString("investigator_lname"));
          desc.append(")");

          investigatorDescs.add(desc.toString());
        }

        rs.close();
        it.close();
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "loadOptionData (inv): " + e.toString());
        addError("loadOptionData (inv): " + e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
      }

      // metTables
      metTables.add("");
      metTableDescs.add("select from list");
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:241^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    met_tables_new
//            order by met_type, ave_ticket
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    met_tables_new\n          order by met_type, ave_ticket";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.CreditDataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:246^9*/

        rs = it.getResultSet();

        while(rs.next())
        {
          metTables.add(rs.getString("met_table_num"));

          desc.setLength(0);
          desc.append(rs.getString("met_table_num"));
          desc.append(" (");
          desc.append(rs.getString("met_type")                      + ", ");
          desc.append(formatCurrency(rs.getString("ave_ticket"))    + " avg ticket, ");
          desc.append(formatCurrency(rs.getString("monthly_sales")) + " sales, ");
          desc.append(rs.getString("dup_card_num")                  + " dup card, ");
          desc.append(formatCurrency(rs.getString("dup_amount"))    + " dup amt, ");
          desc.append(rs.getString("percent_keyed")                 + "% keyed");
          desc.append(")");
          metTableDescs.add(desc.toString());
        }

        rs.close();
        it.close();
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "loadOptionData (met): " + e.toString());
        addError("loadOptionData (met): " + e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
      }
    }
    catch(Exception se)
    {
      logEntry("loadOptionData()", se.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public CreditDataBean()
  {
    loadOptionData();
    setStatuses();
    setGenders();
    setIncStatuses();
  }

  public void getMerchantData(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:310^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_application_type,
//                  allow_auto_upload,
//                  asso_number,
//                  merch_gender,
//                  sic_code,
//                  merch_met_table_number,
//                  merch_invg_code,
//                  merch_bank_number,
//                  status_ind,
//                  merch_visainc_status_ind
//          from    merchant
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_application_type,\n                allow_auto_upload,\n                asso_number,\n                merch_gender,\n                sic_code,\n                merch_met_table_number,\n                merch_invg_code,\n                merch_bank_number,\n                status_ind,\n                merch_visainc_status_ind\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:324^7*/

      rs = it.getResultSet();
      
      if(rs.next())
      {
        this.sicCode          = rs.getInt("sic_code");
        this.metCode          = rs.getString("merch_met_table_number");
        this.invCode          = rs.getString("merch_invg_code");

        //from data expansion screen
        this.allowAutoUpload  = isBlank(rs.getString("allow_auto_upload")) ? "" : rs.getString("allow_auto_upload");
        this.association      = rs.getString("asso_number");
        this.bankNumber       = rs.getString("merch_bank_number");
        this.statusIndicator  = rs.getString("status_ind");
        this.incStatus        = rs.getString("merch_visainc_status_ind");
        this.gender           = rs.getString("merch_gender");
        this.chainMerchant    = (rs.getInt("merch_application_type") == CHAIN_MERCHANT || rs.getInt("merch_application_type") == ADD_CHAIN_MERCHANT);
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMerchantData: " + e.toString());
      addError("getMerchantData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void getCreditData(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:371^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchcredit
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchcredit\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.CreditDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:377^7*/
      
      if( recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:381^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_credit_achpost_days,
//                    merch_debit_achpost_days,
//                    MERCH_DAYS_SUSPEND
//            from    merchcredit
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_credit_achpost_days,\n                  merch_debit_achpost_days,\n                  MERCH_DAYS_SUSPEND\n          from    merchcredit\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:388^9*/

        rs = it.getResultSet();
      
        if(rs.next())
        {
          this.creditDelay  = rs.getInt("merch_credit_achpost_days");
          this.debitDelay   = rs.getInt("merch_debit_achpost_days");
          this.daysSuspend  = isBlank(rs.getString("MERCH_DAYS_SUSPEND")) ? 0 : rs.getInt("MERCH_DAYS_SUSPEND");
        }

        rs.close();
        it.close();
      }
      else
      {
        // preset suspense days for certain apps
        int appType = 0;
        int motoPercentage = 0;
        /*@lineinfo:generated-code*//*@lineinfo:407^9*/

//  ************************************************************
//  #sql [Ctx] { select  app.app_type,
//                    round(mr.merch_mail_phone_sales,0)
//            
//            from    application app,
//                    merchant mr
//            where   app.app_seq_num = :primaryKey and
//                    app.app_seq_num = mr.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app.app_type,\n                  round(mr.merch_mail_phone_sales,0)\n           \n          from    application app,\n                  merchant mr\n          where   app.app_seq_num =  :1  and\n                  app.app_seq_num = mr.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.CreditDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   motoPercentage = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:417^9*/
        
        if(appType == mesConstants.APP_TYPE_MESP && motoPercentage >= 50)
        {
          daysSuspend = 1;
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCreditData: " + e.toString());
      addError("getCreditData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void getMiscData(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:447^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  user_data_1,
//                  user_data_2,
//                  user_data_3,
//                  user_data_4,
//                  user_data_5,
//                  user_account_1,
//                  user_account_2
//          from    merchant_data
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  user_data_1,\n                user_data_2,\n                user_data_3,\n                user_data_4,\n                user_data_5,\n                user_account_1,\n                user_account_2\n        from    merchant_data\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:458^7*/

      rs = it.getResultSet();
      
      if(rs.next())
      {
        this.userData1     = rs.getString("user_data_1");
        this.userData2     = rs.getString("user_data_2");
        this.userData3     = rs.getString("user_data_3");
        this.userData4     = rs.getString("user_data_4");
        this.userData5     = rs.getString("user_data_5");
        this.userAccount1  = rs.getString("user_account_1");
        this.userAccount2  = rs.getString("user_account_2");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMiscData: " + e.toString());
      addError("getMiscData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void getShowOptionalData(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:498^7*/

//  ************************************************************
//  #sql [Ctx] it = { select nvl(at.auto_data_expand, 'N')  auto_data_expand
//          from   application app,
//                 app_type    at  
//          where  app.app_seq_num = :primaryKey
//                 and app.app_type = at.app_type_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select nvl(at.auto_data_expand, 'N')  auto_data_expand\n        from   application app,\n               app_type    at  \n        where  app.app_seq_num =  :1 \n               and app.app_type = at.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:505^7*/
      
      rs = it.getResultSet();

      if(rs.next())
      {
        this.optionalData = rs.getString("auto_data_expand").equals("Y") ? true : false;
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getShowOptionalData: " + e.toString());
      addError("getShowOptionalData: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public boolean showOptionalData()
  {
    return this.optionalData;
  }

  public void setShowOptionalData()
  {
    this.optionalData = true;
  }

  public boolean isChainMerchant()
  {
    return this.chainMerchant;
  }

  public void setChainMerchant()
  {
    this.chainMerchant = true;
  }


  public boolean isVariablePricing()
  {
    return this.variablePricing;
  }

  public void setVariablePricing()
  {
    this.variablePricing = true;
  }


  public void getData(long primaryKey)
  {
    getTranscomOneDayDelay(primaryKey);
    //getPricingPlan(primaryKey);
    getMerchantData(primaryKey);
    getCreditData(primaryKey);
    getMiscData(primaryKey);
    getShowOptionalData(primaryKey);
  }

  private void getPricingPlan(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:581^7*/

//  ************************************************************
//  #sql [Ctx] it = { select TRANCHRG_DISCRATE_TYPE
//          from   tranchrg
//          where  app_seq_num   = :primaryKey and
//                 CARDTYPE_CODE = :mesConstants.APP_CT_VISA
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select TRANCHRG_DISCRATE_TYPE\n        from   tranchrg\n        where  app_seq_num   =  :1  and\n               CARDTYPE_CODE =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:587^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        if(rs.getInt("TRANCHRG_DISCRATE_TYPE") == mesConstants.APP_PS_VARIABLE_RATE)
        {
          variablePricing = true;
        }
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPricingPlan: " + e.toString());
      addError("getPricingPlan: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getTranscomOneDayDelay(long primaryKey)
  {
    // default to false per PRF-612
    transcomOneDayDelay = false;
  }

  public void submitMerchantData(long primaryKey)
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:627^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     allow_auto_upload         = :allowAutoUpload,
//                  sic_code                  = :sicCode,
//                  merch_mcc                 = :sicCode,
//                  merch_met_table_number    = :metCode,
//                  merch_bank_number         = :bankNumber,
//                  status_ind                = :statusIndicator,
//                  merch_visainc_status_ind  = :incStatus,
//                  merch_gender              = :gender,
//                  merch_invg_code           = nvl(:invCode, merch_invg_code),
//                  asso_number               = nvl(:association, asso_number)
//          where   app_seq_num               = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     allow_auto_upload         =  :1 ,\n                sic_code                  =  :2 ,\n                merch_mcc                 =  :3 ,\n                merch_met_table_number    =  :4 ,\n                merch_bank_number         =  :5 ,\n                status_ind                =  :6 ,\n                merch_visainc_status_ind  =  :7 ,\n                merch_gender              =  :8 ,\n                merch_invg_code           = nvl( :9 , merch_invg_code),\n                asso_number               = nvl( :10 , asso_number)\n        where   app_seq_num               =  :11";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,allowAutoUpload);
   __sJT_st.setInt(2,sicCode);
   __sJT_st.setInt(3,sicCode);
   __sJT_st.setString(4,metCode);
   __sJT_st.setString(5,bankNumber);
   __sJT_st.setString(6,statusIndicator);
   __sJT_st.setString(7,incStatus);
   __sJT_st.setString(8,gender);
   __sJT_st.setString(9,invCode);
   __sJT_st.setString(10,association);
   __sJT_st.setLong(11,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:641^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitMerchantData: " + e.toString());
      addError("submitMerchantData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void submitMiscData(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:664^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant_data
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant_data\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.CreditDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:670^7*/

      if(recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:674^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_data
//            set     user_data_1     = substr(:userData1, 1, 4),
//                    user_data_2     = substr(:userData2, 1, 4),
//                    user_data_3     = substr(:userData3, 1, 4),
//                    user_data_4     = substr(:userData4, 1, 16),
//                    user_data_5     = substr(:userData5, 1, 16),
//                    user_account_1  = to_number(substr(:userAccount1, 1, 16)),
//                    user_account_2  = to_number(substr(:userAccount2, 1, 16))
//            where   app_seq_num     = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_data\n          set     user_data_1     = substr( :1 , 1, 4),\n                  user_data_2     = substr( :2 , 1, 4),\n                  user_data_3     = substr( :3 , 1, 4),\n                  user_data_4     = substr( :4 , 1, 16),\n                  user_data_5     = substr( :5 , 1, 16),\n                  user_account_1  = to_number(substr( :6 , 1, 16)),\n                  user_account_2  = to_number(substr( :7 , 1, 16))\n          where   app_seq_num     =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,userData1);
   __sJT_st.setString(2,userData2);
   __sJT_st.setString(3,userData3);
   __sJT_st.setString(4,userData4);
   __sJT_st.setString(5,userData5);
   __sJT_st.setString(6,userAccount1);
   __sJT_st.setString(7,userAccount2);
   __sJT_st.setLong(8,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:685^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:689^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_data
//            (
//              app_seq_num,
//              user_data_1,
//              user_data_2,
//              user_data_3,
//              user_data_4,
//              user_data_5,
//              user_account_1,
//              user_account_2
//            )
//            values
//            (
//              :primaryKey,
//              substr(:userData1, 1, 4),
//              substr(:userData2, 1, 4),
//              substr(:userData3, 1, 4),
//              substr(:userData4, 1, 16),
//              substr(:userData5, 1, 16),
//              to_number(substr(:userAccount1, 1, 16)),
//              to_number(substr(:userAccount2, 1, 16))
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant_data\n          (\n            app_seq_num,\n            user_data_1,\n            user_data_2,\n            user_data_3,\n            user_data_4,\n            user_data_5,\n            user_account_1,\n            user_account_2\n          )\n          values\n          (\n             :1 ,\n            substr( :2 , 1, 4),\n            substr( :3 , 1, 4),\n            substr( :4 , 1, 4),\n            substr( :5 , 1, 16),\n            substr( :6 , 1, 16),\n            to_number(substr( :7 , 1, 16)),\n            to_number(substr( :8 , 1, 16))\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setString(2,userData1);
   __sJT_st.setString(3,userData2);
   __sJT_st.setString(4,userData3);
   __sJT_st.setString(5,userData4);
   __sJT_st.setString(6,userData5);
   __sJT_st.setString(7,userAccount1);
   __sJT_st.setString(8,userAccount2);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:713^9*/
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitMiscData: " + e.toString());
      addError("submitMiscData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void submitCreditData(long primaryKey)
  {
    try
    {
      connect();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:735^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchcredit
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchcredit\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.ops.CreditDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:741^7*/
      
      if(recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:745^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchcredit
//            set     merch_credit_achpost_days = :creditDelay,
//                    merch_debit_achpost_days  = :debitDelay,
//                    merch_days_suspend        = :daysSuspend
//            where   app_seq_num               = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchcredit\n          set     merch_credit_achpost_days =  :1 ,\n                  merch_debit_achpost_days  =  :2 ,\n                  merch_days_suspend        =  :3 \n          where   app_seq_num               =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,creditDelay);
   __sJT_st.setInt(2,debitDelay);
   __sJT_st.setInt(3,daysSuspend);
   __sJT_st.setLong(4,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:752^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:756^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcredit
//            (
//              merch_credit_achpost_days,
//              merch_debit_achpost_days,
//              MERCH_DAYS_SUSPEND,
//              app_seq_num
//            )
//            values
//            (
//              :creditDelay,
//              :debitDelay,
//              :daysSuspend,
//              :primaryKey
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchcredit\n          (\n            merch_credit_achpost_days,\n            merch_debit_achpost_days,\n            MERCH_DAYS_SUSPEND,\n            app_seq_num\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,creditDelay);
   __sJT_st.setInt(2,debitDelay);
   __sJT_st.setInt(3,daysSuspend);
   __sJT_st.setLong(4,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:772^9*/
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitCreditData: " + e.toString());
      addError("submitCreditData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void submitData(HttpServletRequest req, long primaryKey)
  {
    if(! hasErrors())// && ! isBlank(this.association))
    {
      submitMerchantData(primaryKey);
      submitMiscData(primaryKey);
    }

    if(! hasErrors())
    {
      submitCreditData(primaryKey);
      doAssoSpecificUpdates(primaryKey);
    }
  }

  private void doAssoSpecificUpdates(long primaryKey)
  {
    try
    {
      connect();
      
      //if verisign group 3.. we change visa/mc disc rate to 2.85%
      if(this.association.equals("600143")) //versign group 3
      {
        /*@lineinfo:generated-code*//*@lineinfo:810^9*/

//  ************************************************************
//  #sql [Ctx] { update  tranchrg
//            set     tranchrg_disc_rate = 2.85
//            where   app_seq_num = :primaryKey and
//                    cardtype_code in (1, 4)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  tranchrg\n          set     tranchrg_disc_rate = 2.85\n          where   app_seq_num =  :1  and\n                  cardtype_code in (1, 4)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:816^9*/
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "doAssoSpecificUpdates: " + e.toString());
      addError("doAssoSpecificUpdates: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  protected boolean isTridentApp(long primaryKey)
  {
    boolean           result        = false;

    try
    {
      connect();
      
      int recCount = 0;
      
      // currently only sure indicator of a trident app is one that uses TVT
      /*@lineinfo:generated-code*//*@lineinfo:841^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(mp.app_seq_num)
//          
//          from    merch_pos mp,
//                  pos_category pc
//          where   mp.app_seq_num = :primaryKey and
//                  mp.pos_code = pc.pos_code and
//                  upper(nvl(pc.trident_only,'N')) = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mp.app_seq_num)\n         \n        from    merch_pos mp,\n                pos_category pc\n        where   mp.app_seq_num =  :1  and\n                mp.pos_code = pc.pos_code and\n                upper(nvl(pc.trident_only,'N')) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.ops.CreditDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:850^7*/
      
      result = (recCount > 0);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isTridentApp("+primaryKey+"): " + e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  public void setDefaults(long primaryKey)
  {
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    int               posType       = 0;
    int               processor     = 0;
    String            bbtCenterNum  = "";
    boolean           webReporting  = false;

    try
    {
      connect();
      
      ApplicationTypeBean   appType = new ApplicationTypeBean();
      appType.fillDataApp(primaryKey);

      // bankNumber
      if(isBlank(bankNumber))
      {
        bankNumber = Integer.toString(appType.getAppBankNumber());
      }

      /*@lineinfo:generated-code*//*@lineinfo:888^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.svb_cif_num,
//                  merch.svb_cd_num,
//                  merch.bustype_code,
//                  merch.industype_code,
//                  merch.merch_busgoodserv_descr,
//                  merch.merch_month_visa_mc_sales,
//                  merch.annual_vmc_sales,
//                  merch.merch_average_cc_tran,
//                  merch.merch_mail_phone_sales,
//                  merch.merch_referring_bank,
//                  substr(merch.merch_referring_bank,1,4)  referring_bank,
//                  merch.account_type,
//                  merch.processor,
//                  merch.web_reporting,
//                  decode(nvl(merch.existing_verisign,'N'), 'Y', 'OV', 'y', 'OV', poscat.pos_abbrev) pos_abbrev,
//                  poscat.pos_type
//          from    merchant     merch,
//                  merch_pos    pos,
//                  pos_category poscat
//          where   merch.app_seq_num  = pos.app_seq_num and
//                  pos.pos_code       = poscat.pos_code and
//                  merch.app_seq_num  = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.svb_cif_num,\n                merch.svb_cd_num,\n                merch.bustype_code,\n                merch.industype_code,\n                merch.merch_busgoodserv_descr,\n                merch.merch_month_visa_mc_sales,\n                merch.annual_vmc_sales,\n                merch.merch_average_cc_tran,\n                merch.merch_mail_phone_sales,\n                merch.merch_referring_bank,\n                substr(merch.merch_referring_bank,1,4)  referring_bank,\n                merch.account_type,\n                merch.processor,\n                merch.web_reporting,\n                decode(nvl(merch.existing_verisign,'N'), 'Y', 'OV', 'y', 'OV', poscat.pos_abbrev) pos_abbrev,\n                poscat.pos_type\n        from    merchant     merch,\n                merch_pos    pos,\n                pos_category poscat\n        where   merch.app_seq_num  = pos.app_seq_num and\n                pos.pos_code       = poscat.pos_code and\n                merch.app_seq_num  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:912^7*/
      
      rs = it.getResultSet();

      if(rs.next())
      {
        posType = rs.getInt("pos_type");

        bbtCenterNum = isBlank(rs.getString("merch_referring_bank")) ? "" : rs.getString("merch_referring_bank");
        webReporting = (!isBlank(rs.getString("web_reporting")) && rs.getString("web_reporting").toUpperCase().equals("Y"));

        if(!isBlank(rs.getString("processor")))
        {
          processor = rs.getInt("processor");
        }

        // incStatus - default to bustype_code
        if(isBlank(incStatus))
        {
          incStatus = rs.getString("bustype_code");
        }

        // user data 2
        if(isBlank(userData2) && bankNumber.equals("3941"))
        {
          userData2 = isBlank(rs.getString("referring_bank")) ? "" : rs.getString("referring_bank");
        }

        // user data 3
        if( isBlank(userData3) && (bankNumber.equals("3941") || bankNumber.equals("3942")) )
        {
          StringBuffer user3 = new StringBuffer("");

          user3.append(appType.getAppSource());

          // add the processor (default to 'V' for Vital)
          if(isTridentApp(primaryKey))
          {
            user3.append("T");
          }
          else
          {
            user3.append("V");
          }

          // add the product type abbreviation
          user3.append(rs.getString("pos_abbrev"));

          userData3 = user3.toString();
        }

        // user data 4
        if( isBlank(userData4) && (bankNumber.equals("3941") || bankNumber.equals("3942")) )
        {
          StringBuffer    user4       = new StringBuffer("");
          DecimalFormat   salesFormat = new DecimalFormat("00000000");
          DecimalFormat   avgFormat   = new DecimalFormat("0000");
          DecimalFormat   motoFormat  = new DecimalFormat("000");

          float ccSales = rs.getFloat("merch_month_visa_mc_sales");
          float ccAvg   = rs.getFloat("merch_average_cc_tran");
          float ccMoto  = rs.getFloat("merch_mail_phone_sales");

          // turn monthly sales into yearly sales
          ccSales = ccSales * 12;
          user4.append(salesFormat.format(ccSales));

          // average ticket
          user4.append(avgFormat.format(ccAvg));

          // moto pctg
          user4.append(motoFormat.format(ccMoto));

          userData4 = user4.toString();

          if(!isBlank(rs.getString("account_type")) && rs.getString("account_type").equals("C"))
          {
            userData4 += "C";
          }
          else
          {
            userData4 += "N";
          }

          if(!isBlank(rs.getString("svb_cif_num")))
          {
            userData4 = rs.getString("svb_cif_num");
          }
          
          if( ! isBlank(rs.getString("svb_cd_num")) )
          {
            userAccount1 = (new DecimalFormat("0000000000")).format(rs.getFloat("svb_cif_num")) +
                            (new DecimalFormat("000000")).format(rs.getFloat("svb_cd_num"));
          }
        }

        if(bankNumber.equals("3858"))
        {
          float ccSales   = rs.getFloat("annual_vmc_sales");

          userData1       = Float.toString(ccSales);
          //take off the last three numbers and replace them with a K
          int idx = userData1.indexOf(".");
          if(idx > 0)
          {
            //System.out.println("userData1 = " + userData1);
            userData1 = userData1.substring(0,idx);
          }

          if(userData1.length() >= 4)
          {
            userData1       = userData1.substring(0,(userData1.length() - 3)) + "K";
          }

          if(userData1.length() > 4)
          {
            userData1 = userData1.substring(0,4);
          }

          userData2       = rs.getString("merch_average_cc_tran");
          int decIdx = userData2.indexOf(".");
          if(decIdx > -1)
          {
            userData2 = userData2.substring(0,decIdx);
          }

          if(userData2.length() >= 4)
          {
            userData2 = userData2.substring(0,(userData2.length() - 3)) + "K";
          }

          userData3       = isBlank(userData3) ? rs.getString("merch_mail_phone_sales")  : userData3;

          userData4       = isBlank(userData4) ? rs.getString("merch_busgoodserv_descr") : userData4;
          if(userData4.length() > 16)
          {
            userData4 = userData4.substring(0,16);
          }
        }
      }
      
      rs.close();
      it.close();

      if(bankNumber.equals("3858"))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1058^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code,
//                    decode(merchpo_card_merch_number,
//                      0, '',
//                      to_char(merchpo_card_merch_number)) merchant_number
//            from    merchpayoption
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code,\n                  decode(merchpo_card_merch_number,\n                    0, '',\n                    to_char(merchpo_card_merch_number)) merchant_number\n          from    merchpayoption\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1066^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          switch(rs.getInt("cardtype_code"))
          {
            case mesConstants.APP_CT_AMEX:
              userAccount1 = isBlank(userAccount1)    ? rs.getString("merchant_number") : userAccount1;
              userAccount1 = userAccount1.equals("0") ? ""                                        : userAccount1;
            break;
            case mesConstants.APP_CT_DISCOVER:
              userAccount2 = isBlank(userAccount2)    ? rs.getString("merchant_number") : userAccount2;
              userAccount2 = userAccount2.equals("0") ? ""                                        : userAccount2;
            break;
            case mesConstants.APP_CT_SPS:
              userData5 = isBlank(userData5)          ? rs.getString("merchant_number") : userData5;
              break;
          }
        }
        rs.close();
        it.close();
      }

      if(bankNumber.equals("3867"))
      {
        userAccount2 = bbtCenterNum;

        if(webReporting)
        {
          userData3 = "WEB";
        }

        switch(posType)
        {
          case mesConstants.POS_TOUCHTONECAPTURE:
            userData1 = "TETC";
          break;
          case mesConstants.POS_DIAL_AUTH:
            userData1 = "DPAY";
          break;
          case mesConstants.POS_DIAL_TERMINAL:
          default:
            if(processor == mesConstants.APP_PROCESSOR_TYPE_VITAL)
            {
              userData1 = "VNET";
            }
            else if(processor == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_CENTRAL)
            {
              userData1 = "MAPP";
            }
            else if(processor == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST)
            {
              userData1 = "NDC";
            }
            else if(processor == mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH)
            {
              userData1 = "PAYT";
            }
          break;
        }

        /*@lineinfo:generated-code*//*@lineinfo:1129^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  referring_officer,
//                    check_provider
//            from    app_merch_bbt
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  referring_officer,\n                  check_provider\n          from    app_merch_bbt\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.ops.CreditDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.ops.CreditDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1135^9*/
        
        rs = it.getResultSet();

        if(rs.next())
        {
          userData4 = isBlank(rs.getString("referring_officer")) ? "" : rs.getString("referring_officer");

          if(!isBlank(rs.getString("check_provider")))
          {
            if(rs.getString("check_provider").equals(mesConstants.APP_BBT_CHECK_PROVIDER_GLOBAL))
            {
              userData2 = "GCHK";
            }
            else if(rs.getString("check_provider").equals(mesConstants.APP_BBT_CHECK_PROVIDER_TTECH))
            {
              userData2 = "TTEC";
            }
          }
        }

      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setDefaults: " + e.toString());
      addError("setDefaults: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public boolean validate()
  {
    try
    {
      connect();
      
      if(sicCode == 0)
      {
        addError("Please enter a valid Sic Code");
      }
      else
      {
        int recCount = 0;
      
        /*@lineinfo:generated-code*//*@lineinfo:1188^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(sic_code)
//            
//            from    sic_codes
//            where   sic_code = :sicCode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(sic_code)\n           \n          from    sic_codes\n          where   sic_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.ops.CreditDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,sicCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1194^9*/
      
        if(recCount == 0)
        {
          addError("The Sic Code you have chosen is invalid");
        }
      }

      if(isBlank(invCode))
      {
        addError("Please choose a valid investigator");
      }

      if(isBlank(metCode))
      {
        addError("Please choose a valid MET Table");
      }

      if(allowAutoUpload.equals("Y"))
      {
        if(isBlank(this.association))
        {
          addError("If allowing Auto Upload, you must provide an association number");
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "validate: " + e.toString());
      addError("validate: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return (! hasErrors());
  }

  public void setSicCode(String sicCode)
  {
    this.sicCode = parseInt(sicCode);
  }
  public String getSicCode()
  {
    String result = "";
    if(sicCode != 0)
    {
      result = Integer.toString(this.sicCode);
    }
    return result;
  }

  public void setInvCode(String invCode)
  {
    this.invCode = invCode;
  }
  public String getInvCode()
  {
    return blankIfNull(this.invCode);
  }
  public Vector getInvestigators()
  {
    return this.investigators;
  }
  public String getInvestigator(int i)
  {
    String result = "";
    try
    {
      result = (String)investigatorDescs.elementAt(i);
    }
    catch(Exception e)
    {
      result = "INVALID INDEX";
    }
    return result;
  }

  public void setMetCode(String metCode)
  {
    this.metCode = metCode;
  }
  public String getMetCode()
  {
    return blankIfNull(this.metCode);
  }
  public Vector getMetTables()
  {
    return this.metTables;
  }
  public String getMetTable(int i)
  {
    String result = "";
    try
    {
      result = (String)metTableDescs.elementAt(i);
    }
    catch(Exception e)
    {
      result = "INVALID INDEX (" + i + ")";
    }
    return result;
  }

  public void setAllowAutoUpload(String allowAutoUpload)
  {
    this.allowAutoUpload = allowAutoUpload;
  }

  public String getAllowAutoUpload()
  {
    return blankIfNull(this.allowAutoUpload);
  }

  public void setCreditDelay(String creditDelay)
  {
    this.creditDelay = parseInt(creditDelay);
  }
  public int getCreditDelay()
  {
    return this.creditDelay;
  }

  public void setDebitDelay(String debitDelay)
  {
    this.debitDelay = parseInt(debitDelay);
  }
  public int getDebitDelay()
  {
    return this.debitDelay;
  }
  public void setDaysSuspend(String daysSuspend)
  {
    this.daysSuspend = parseInt(daysSuspend);
  }
  public int getDaysSuspend()
  {
    return this.daysSuspend;
  }

  public boolean isTranscomOneDayDelay()
  {
    return this.transcomOneDayDelay;
  }

  public String getAssociation()
  {
    return blankIfNull(this.association);
  }
  public String getBankNumber()
  {
    return blankIfNull(this.bankNumber);
  }
  public String getStatusIndicator()
  {
    return blankIfNull(this.statusIndicator);
  }
  public String getIncStatus()
  {
    return blankIfNull(this.incStatus);
  }
  public String getGender()
  {
    return blankIfNull(this.gender);
  }
  public String getUserData1()
  {
    return blankIfNull(this.userData1);
  }
  public String getUserData2()
  {
    return blankIfNull(this.userData2);
  }
  public String getUserData3()
  {
    return blankIfNull(this.userData3);
  }
  public String getUserData4()
  {
    return blankIfNull(this.userData4);
  }
  public String getUserData5()
  {
    return blankIfNull(this.userData5);
  }
  public String getUserAccount1()
  {
    return blankIfNull(this.userAccount1);
  }
  public String getUserAccount2()
  {
    return blankIfNull(this.userAccount2);
  }


  public void setAssociation(String association)
  {
    this.association = association;
  }
  public void setBankNumber(String bankNumber)
  {
    this.bankNumber = bankNumber;
  }
  public void setStatusIndicator(String statusIndicator)
  {
    this.statusIndicator = statusIndicator;
  }
  public void setIncStatus(String incStatus)
  {
    this.incStatus = incStatus;
  }
  public void setGender(String gender)
  {
    this.gender = gender;
  }
  public void setUserData1(String userData1)
  {
    this.userData1 = userData1;
  }
  public void setUserData2(String userData2)
  {
    this.userData2 = userData2;
  }
  public void setUserData3(String userData3)
  {
    this.userData3 = userData3;
  }
  public void setUserData4(String userData4)
  {
    this.userData4 = userData4;
  }
  public void setUserData5(String userData5)
  {
    this.userData5 = userData5;
  }
  public void setUserAccount1(String userAccount1)
  {
    this.userAccount1 = userAccount1;
  }
  public void setUserAccount2(String userAccount2)
  {
    this.userAccount2 = userAccount2;
  }

  public Vector getStatuses()
  {
    return this.statuses;
  }
  public Vector getStatusDescs()
  {
    return this.statusDescs;
  }
  public Vector getGenders()
  {
    return this.genders;
  }
  public Vector getGenderDescs()
  {
    return this.genderDescs;
  }
  public Vector getIncStatuses()
  {
    return this.incStatuses;
  }
  public Vector getIncStatusDescs()
  {
    return this.incStatusDescs;
  }

  public String formatCurrency(String num)
  {
    try
    {
      if(isBlank(num))
      {
        return "open";
      }

      num = num.replace('f','l');
      num = num.replace('F','l');
      num = num.replace('d','l');
      num = num.replace('D','l');
      double check = Double.parseDouble(num);
    }
    catch(Exception e)
    {
      return "";
    }

    if(isBlank(num))
    {
      return "open";
    }

    char last;
    int length = num.length();
    int index  = num.indexOf('.');

    if(index == -1)
    {
      return "$" + num + ".00";
    }

    String result = "";
    if(length - index >= 4)
    {
      result = num.substring(0,index+3);
      last = num.charAt(index+3);
      if(last > '5')
      {
        try
        {
          index  = result.indexOf('.');
          String subStr1 = result.substring(0,index);
          String subStr2 = result.substring(index+1);
          int tempInt = Integer.parseInt(subStr2);
          tempInt += 1;
          if (tempInt == 100)
          {
            int tempInt2 = Integer.parseInt(subStr1);
            tempInt2 += 1;
            result = tempInt2 + ".00";
          }
          else if(tempInt == 1)
          {
            result = subStr1 + ".01";
          }
          else
          {
            result = subStr1 + "." + tempInt;
          }
        }
        catch(Exception e)
        {
          return "";
        }
      }
    }
    else if(length - index == 3)
    {
      result = num;
    }
    else if(length - index == 2)
    {
      result = num + "0";
    }
    else if(length - index == 1)
    {
      result = num + "00";
    }

    return "$" + result;
  }


}/*@lineinfo:generated-code*/