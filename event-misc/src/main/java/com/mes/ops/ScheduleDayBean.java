/*@lineinfo:filename=ScheduleDayBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ScheduleDayBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Vector;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class ScheduleDayBean extends ScheduleBaseBean
{
  public static final String INTERVAL_NOT_SHADED  = "#FFFFFF";
  public static final String INTERVAL_SHADED      = "#FFFFCC";
  public static final String INTERVAL_BLOCKED_OUT = "#CCFFCC";

  private String    dateString                    = "";
  private long      startOfDay                    = -1L;
  private long      endOfDay                      = -1L;
  private String    currentArrowPart              = "";

  private HashMap   assignedIntervals             = new HashMap();
  private HashMap   intervalDescs                 = new HashMap();
  private Vector    intervals                     = new Vector();
  private Vector    shadedStack                   = new Vector();

  public ScheduleDayBean()
  {
    //initializes calendar to be current date/time
    super();
  }

  public ScheduleDayBean(int scheduleType)
  {
    //initializes calendar to be current date/time
    super(scheduleType);
  }

  public void setDayCalendar(String momentInTime)
  {
    try
    {
      setDayCalendar(Long.parseLong(momentInTime));
    }
    catch(Exception e)
    {
      logEntry("setDayCalendar(" + momentInTime + ")", e.toString());
    }
  }

  public void setDayCalendar(long momentInTime)
  {
    //setup calendar
    setCalendarDate(momentInTime);

    //setup month
    setDayCalendar(getCalendarYear(), getCalendarMonth(), getCalendarDay());
  }


  public void setDayCalendar(int selectedYear, int selectedMonth, int selectedDay)
  {

    setCalendarDate(selectedYear, selectedMonth, selectedDay);

    //get the long description of the very first millisecond of the day
    startOfDay = getLongTimeOfCalendar();
    
    dateString = getCalendarDayOfWeekDesc() + " " + getCalendarMonthDesc() + " " + getCalendarDay() + ", " + getCalendarYear();
    
    int actualCalendarDay = getCalendarDay();
    while(actualCalendarDay == getCalendarDay())
    {

      //System.out.println(getCalendarDayOfWeekDesc() + " " + getCalendarMonthDesc() + " " + getCalendarDay() + ", " + getCalendarYear() + " " + getCalendar12Hour() + ":" + fixZeroTimeIssue(getCalendarMinute()) + ":" + fixZeroTimeIssue(getCalendarSecond()) + "." + getCalendarMillisecond() + " " + getCalendarAmPmDesc() + "  " + getLongTimeOfCalendar());

      if(getScheduleStartOfDay() <= getCalendar24Hour() && getCalendar24Hour() <= getScheduleEndOfDay())
      {
        intervals.add(Long.toString(getLongTimeOfCalendar()));
        intervalDescs.put(Long.toString(getLongTimeOfCalendar()), (getCalendar12Hour() + ":" + fixZeroTimeIssue(getCalendarMinute()) + " " + getCalendarAmPmDesc()));
      }
      //increment calendar by the number of minutes specified by type of schedule
      //eventually they will flip over to the next day and break out of this loop
      addToCalendarField(Calendar.MINUTE, getTimeInterval());
    }

    //we're out of the loop, so they day has changed to the next day
    //we take the year month and day and setup a new calendar so we reset it
    //to the very first millisecond of that new day.. then back it off 1 millisecond
    //to get the end of the day

    setCalendarDate(getCalendarYear(), getCalendarMonth(), getCalendarDay());

    //get the long description of the very last millisecond of the day
    endOfDay = getLongTimeOfCalendar() - 1L;


    //use startOfDay and endOfDay to get things scheduled for that day
    loadAssignedSchedule();
  }


/*
  gets assigned data from database and stores it in subclass which is stored in hashmap with id + employee id as key.
*/  
  private void loadAssignedSchedule()
  {
    ResultSetIterator   it         = null;
    ResultSet           rs         = null;

    try
    {
      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:148^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   * 
//  
//          from     schedule_data  
//  
//          where    type = :getScheduleType() and 
//                   id between :startOfDay and :endOfDay
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_804 = getScheduleType();
  try {
   String theSqlTS = "select   * \n\n        from     schedule_data  \n\n        where    type =  :1  and \n                 id between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.ScheduleDayBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_804);
   __sJT_st.setLong(2,startOfDay);
   __sJT_st.setLong(3,endOfDay);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.ScheduleDayBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:157^7*/
    
      rs = it.getResultSet();

      AssignedInterval ai = null;
      
      while(rs.next())
      {
        ai = new AssignedInterval();

        ai.setId          (rs.getLong("id"));
        ai.setUserId      (rs.getString("user_id"));
        ai.setDuration    (rs.getInt("duration"), getTimeInterval());
        ai.setFunctionId  (rs.getString("function_id"));
        ai.setFunctionDesc(rs.getString("function_desc"));
        ai.setDateAssigned(DateTimeFormatter.getFormattedDate(rs.getTimestamp("date_assigned"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));

        assignedIntervals.put((rs.getString("id") + rs.getString("user_id")), ai);
      }

      rs.close();
      it.close();

    }
    catch(Exception e)
    {
      logEntry("loadAssignedSchedule()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }

/*

  **********************************************************************
  these two methods set a stack to determine how many intervals
  get shaded based on the duration of an assignment.  This assumes that
  no duration of two appointments for one employee will ever overlap.

*/

  public void setShadedStack(String userId, int num, boolean blockOut)
  {
    String result = "";

    for(int i=0; i < num; i++)
    {
      if(blockOut)
      {
        result = userId + INTERVAL_BLOCKED_OUT;
      }
      else
      {
        result = userId + INTERVAL_SHADED;
      }
      shadedStack.add(result);
    }
  }

  public String getBackgroundColor(String userId)
  {
    String result = INTERVAL_NOT_SHADED;

    int    firstOccurrenceOfBlockedColor = shadedStack.indexOf((userId + INTERVAL_BLOCKED_OUT));
    int    firstOccurrenceOfShadedColor  = shadedStack.indexOf((userId + INTERVAL_SHADED));

    if(firstOccurrenceOfBlockedColor == -1 && firstOccurrenceOfShadedColor == -1)
    {
      result = INTERVAL_NOT_SHADED;
    }
    else if(firstOccurrenceOfShadedColor == -1)
    {
      result = INTERVAL_BLOCKED_OUT;
      userId += INTERVAL_BLOCKED_OUT;
    }
    else if(firstOccurrenceOfBlockedColor == -1)
    {
      result = INTERVAL_SHADED;
      userId += INTERVAL_SHADED;
    }
    else if(firstOccurrenceOfBlockedColor < firstOccurrenceOfShadedColor)
    {
      result = INTERVAL_BLOCKED_OUT;
      userId += INTERVAL_BLOCKED_OUT;
    }
    else if(firstOccurrenceOfShadedColor < firstOccurrenceOfBlockedColor)
    {
      result = INTERVAL_SHADED;
      userId += INTERVAL_SHADED;
    }

    if(!result.equals(INTERVAL_NOT_SHADED))
    {
      shadedStack.removeElement(userId);
      setArrowPart(userId);
    }

    return result;
  }

  //this method returns a character thats used to construct an arrow
  //showing that an assignment spans more than one interval

  private void setArrowPart(String userId)
  {
    String result = "\\/";  //"|<br>\\/";

    if(shadedStack.contains(userId))
    {
      //they request the arrow to be smaller...lets try this
      //result = "|<br>|";
        result = "|";
    }

    this.currentArrowPart = result;
  }

  public String getArrowPart()
  {
    return this.currentArrowPart;
  }

/*
  **********************************************************************
*/

  public String getDateString()
  {
    return this.dateString;
  }

  public int getIntervalsSize()
  {
    return this.intervals.size();
  }
  
  public String getInterval(int idx)
  {
    return (String)this.intervals.elementAt(idx);
  }

  public String getIntervalDescs(String key)
  {
    String result = "";
    
    if(intervalDescs.containsKey(key))
    {
      result = (String)intervalDescs.get(key);
    }

    return result;
  }

  public AssignedInterval getAssignedIntervals(String key1, String key2)
  {
    AssignedInterval result = null;

    String key    = key1 + key2;

    
    if(assignedIntervals.containsKey(key))
    {
      result = (AssignedInterval)assignedIntervals.get(key);
    }

    return result;
  }


  public class AssignedInterval
  {
    private long    id              = -1L;
    private String  userId          = "";
    private int     duration        = -1;
    private int     numOfIntervals  = -1;
    private String  functionId      = "";
    private String  functionDesc    = "";
    private String  dateAssigned    = "";

    public AssignedInterval()
    {
    }

    public void setId(long id)
    {
      this.id = id;
    }

    public void setUserId(String userId)
    {
      this.userId = userId;
    }

    public void setDuration(int duration, int timeOfInterval)
    {
      this.duration = duration;
      this.numOfIntervals = duration / timeOfInterval;
    }

    public void setFunctionId(String functionId)
    {
      this.functionId = functionId;
    }

    public void setFunctionDesc(String functionDesc)
    {
      this.functionDesc = functionDesc;
    }

    public void setDateAssigned(String dateAssigned)
    {
      this.dateAssigned = dateAssigned;
    }


    public long getId()
    {
      return this.id;
    }

    public String getUserId()
    {
      return this.userId;
    }

    public int getDuration()
    {                       
      return this.duration;
    }

    public int getNumOfIntervals()
    {
      return this.numOfIntervals;
    }

    public String getFunctionId()
    {
      return this.functionId;
    }

    public String getFunctionDesc()
    {
      return this.functionDesc;
    }

    public String getDateAssigned()
    {
      return this.dateAssigned;
    }





  }

}/*@lineinfo:generated-code*/