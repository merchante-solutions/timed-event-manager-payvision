package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.NumberField;

public class DataAssignItemBean extends BaseItemBean
{
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new NumberField("sicCode",   "SIC",4,6,true,0));
      fields.add(new NumberField("assocNum",  "Association #",6,6,true,0));

      fields.add(new ButtonField("submit"));
      fields.add(new ButtonField("cancel"));
    
      fields.setHtmlExtra("class=\"formText\"");
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  /**
   * Submits app data to merchant table.
   */
  private void submitAssignedData() throws Exception
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      // update merchant table data with assoc num and sic code
      String qs = " update  merchant            " +
                  " set     asso_number   = ?,  " +
                  "         app_sic_code  = ?   " +
                  " where   app_seq_num   = ?   ";

      ps = con.prepareStatement(qs);
      ps.setInt(1,fields.getField("assocNum").asInteger());
      ps.setInt(2,fields.getField("sicCode").asInteger());
      ps.setLong(3,appSeqNum);
      ps.execute();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  /**
   * Handles submits from jsp.  Can be triggered by a cancel button (store data
   * and move item to canceled queue) or a submit button (store data and if data
   * is complete move to completed queue, create new queue item in either a
   * the high risk sic queue or the pull credit queue).
   *
   * Returns true if no errors occur.
   */
  public boolean autoSubmit()
  {
    try
    {
      submitAssignedData();

      int     assocNum    = fields.getField("assocNum").asInteger();
      int     sicCode     = fields.getField("sicCode").asInteger();

      boolean isSubmit    = !fields.getField("submit").isBlank();
      boolean isCancel    = !fields.getField("cancel").isBlank();
      boolean isValid     = (assocNum > 0 && sicCode > 0);

      String  newQueueStr = null;

      if (isCancel)
      {
        ReviewQueueOps.doDataAssignCancel(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_DA_CANCELED);
      }
      else if (isSubmit && isValid)
      {
        ReviewQueueOps.doDataAssignComplete(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_DA_COMPLETED);
      }

      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoSubmit("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoSubmit(appSeqNum=" + appSeqNum + ")",e.toString());
    }

    return false;
  }

  /**
   * Load app data from merchant table.
   */
  private void loadAssignedData() throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  asso_number   assoc_num,  " +
                  "         app_sic_code  sic_code    " +
                  " from    merchant                  " +
                  " where   app_seq_num = ?           ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      setFields(rs);
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** public boolean autoLoad()
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean autoLoad()
  {
    try
    {
      loadAssignedData();
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::autoLoad(): " + e);
      logEntry("autoLoad()",e.toString());
    }

    return false;
  }
}
