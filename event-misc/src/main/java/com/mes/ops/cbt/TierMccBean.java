package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.NumberField;

public class TierMccBean extends BaseItemBean
{
  static Logger log = Logger.getLogger(TierMccBean.class);

  public static final String AN_CANCEL    = "actionCancel";
  public static final String AN_UNCANCEL  = "actionUncancel";

  public static final String FN_MCC       = "mcc";
  public static final String FN_SUBMIT    = "submit";

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new ButtonField(AN_CANCEL,"cancel","cancel"));
      fields.add(new ButtonField(AN_UNCANCEL,"uncancel","uncancel"));

      fields.add(new NumberField(FN_MCC,"MCC",4,6,true,0));

      fields.add(new ButtonField(FN_SUBMIT));
    
      fields.setHtmlExtra("class=\"formText\"");
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + ".createFields("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("createFields(appSeqNum=" + appSeqNum + ")",e.toString());
    }
  }

  /**
   * Action handling.  Users may cancel, uncancel or switch to manual
   * scoring.
   */
  public boolean autoAct()
  {
    log.debug("auto acting with action name: " + autoActionName);
    if (autoActionName.equals(AN_CANCEL))
    {
      TierMccQueueOps.doCancelMcc(appSeqNum,user);
      setNewQueue(TierMccQueueOps.Q_MCC_CANCELED);
    }
    // uncancel will move to manual scoring queue
    else if (autoActionName.equals(AN_UNCANCEL))
    {
      TierMccQueueOps.doUncancelMcc(appSeqNum,user);
      setNewQueue(TierMccQueueOps.Q_MCC_NEW);
      // HACK: need to auto load the mcc field here...
      autoLoad();
    }

    // clear the action field value
    setData(autoActionName,"");

    return true;
  }

  /**
   * Handles submits from jsp.  Can be triggered by a cancel button (store data
   * and move item to canceled queue) or a submit button (store data and if data
   * is complete move to completed queue, create new queue item in either a
   * the high risk sic queue or the pull credit queue).
   *
   * Returns true if no errors occur.
   */
  public boolean autoSubmit()
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      // update merchant table data with assoc num and sic code
      String qs = " update  merchant            " +
                  " set     app_sic_code  = ?   " +
                  " where   app_seq_num   = ?   ";

      ps = con.prepareStatement(qs);
      ps.setInt(1,fields.getField(FN_MCC).asInteger());
      ps.setLong(2,appSeqNum);
      ps.execute();

      TierMccQueueOps.doCompleteMcc(appSeqNum,user);
      setNewQueue(TierMccQueueOps.Q_MCC_COMPLETED);

      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoSubmit("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoSubmit(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }

    return false;
  }

  /*
  ** public boolean autoLoad()
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean autoLoad()
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  app_sic_code " + FN_MCC + " "
                + " from    merchant        "
                + " where   app_seq_num = ? ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      setFields(rs);
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoLoad("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoLoad(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    finally
    {
      cleanUp(ps,rs);
    }

    return false;
  }

  public boolean canSubmit()
  {
    return getType() == TierMccQueueOps.Q_MCC_NEW;
  }
  public boolean canCancel()
  {
    return getType() == TierMccQueueOps.Q_MCC_NEW;
  }
  public boolean canUncancel()
  {
    return getType() == TierMccQueueOps.Q_MCC_CANCELED;
  }
}
