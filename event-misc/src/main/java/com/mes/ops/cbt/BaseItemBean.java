/**
 * BaseItemBean
 *
 * A base class for cbt review queue function jsp field beans.  Sets up the base
 * fields common to all of these beans having to do with queue data that tells
 * the function page what queue an item is in, whether it has switched queues
 * due to user actions, and other needful queue info.
 *
 * When a user moves a queue item the item bean should use the setNewQueue method
 * to make the movement known to the function page logic.
 */
package com.mes.ops.cbt;

import javax.servlet.http.HttpServletRequest;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;

public class BaseItemBean extends FieldBean
{
  protected long appSeqNum;

  public static final String FN_TYPE        = "type";
  public static final String FN_ITEM_TYPE   = "itemType";
  public static final String FN_START_TYPE  = "startType";
  public static final String FN_ID          = "id";
  public static final String FN_APP_SEQ_NUM = "appSeqNum";

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    appSeqNum = Long.parseLong(request.getParameter("id"));

    try
    {
      fields.add(new HiddenField(FN_TYPE));
      fields.add(new HiddenField(FN_ITEM_TYPE));
      fields.add(new HiddenField(FN_START_TYPE));
      fields.add(new HiddenField(FN_ID));
      fields.addAlias(FN_ID,FN_APP_SEQ_NUM);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".createFields("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("createFields(appSeqNum=" + appSeqNum + ")",e.toString());
    }
  }

  /*
  ** protected void postHandleRequest(HttpServletRequest request)
  **
  ** Set startType if it's blank.
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    // copy type into startType if startType is not set
    if (fields.getField(FN_START_TYPE).isBlank())
    {
      fields.setData(FN_START_TYPE,fields.getData(FN_TYPE));
    }
  }

  /**
   * When a queue function has moved a queue item from one queue to another
   * this method is used to indicate the new queue item to the queue function
   * jsp.  The new queue value is flagged in the type field, which is noticed
   * by the page using the startType field...
   */
  protected void setNewQueue(int newQueue)
  {
    try
    {
      String newQueueStr = String.valueOf(newQueue);
      fields.setData(FN_TYPE,newQueueStr);
      request.setAttribute("overrideType",newQueueStr);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".setNewQueue("
        + "appSeqNum=" + appSeqNum + "): " + e.toString());
      logEntry("setNewQueue(appSeqNum=" + appSeqNum + ")",e.toString());
    }
  }

  public int getId()
  {
    return getField(FN_ID).asInteger();
  }

  public int getType()
  {
    return getField(FN_TYPE).asInteger();
  }

  public int getItemType()
  {
    return getField(FN_ITEM_TYPE).asInteger();
  }

  public int getStartType()
  {
    return getField(FN_START_TYPE).asInteger();
  }


}