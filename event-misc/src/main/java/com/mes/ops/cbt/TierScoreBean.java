package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;

public class TierScoreBean extends BaseItemBean
{
  static Logger log = Logger.getLogger(TierScoreBean.class);

  public static final String AN_RETRY       = "actionRetry";
  public static final String AN_CANCEL      = "actionCancel";
  public static final String AN_UNCANCEL    = "actionUncancel";

  public static final String FN_AUTO_SCORE  = "autoScore";
  public static final String FN_CBT_SCORE   = "cbtScore";
  public static final String FN_SUBMIT      = "submit";

  public class ScoreValidation implements Validation
  {
    public String getErrorText()
    {
      return "Cannot submit manual score, auto scoring has completed.";
    }

    public boolean validate(String data)
    {
      if (data == null || data.length() == 0)
      {
        return true;
      }

      PreparedStatement ps = null;
      ResultSet         rs = null;

      try
      {
        connect();

        // look for a row in credit score process table
        // that has not set date_completed, which
        // means it's safe to set the score
        String qs = " select  crp.date_completed,               "
                  + "         csa.average_score                 "
                  + " from    credit_requests_process crp,      "
                  + "         credit_score_average csa          "
                  + " where   crp.app_seq_num = ? and           "
                  + "         crp.app_seq_num = csa.app_seq_num ";      

        ps = con.prepareStatement(qs);
        ps.setLong(1,appSeqNum);
        rs = ps.executeQuery();
        return !rs.next() || rs.getDate("date_completed") == null || rs.getInt("average_score") <= 0;
      }
      catch (Exception e)
      {
        log.error("Error in score field validation: " + e);
      }
      finally
      {
        cleanUp(ps,rs);
      }

      return false;
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField(FN_AUTO_SCORE));
      fields.add(new NumberField(FN_CBT_SCORE,"Credit Score",4,6,true,0));
      fields.getField(FN_CBT_SCORE).addValidation(new ScoreValidation());

      fields.add(new ButtonField(AN_RETRY,"retry","retry"));
      fields.add(new ButtonField(AN_CANCEL,"cancel","cancel"));
      fields.add(new ButtonField(AN_UNCANCEL,"uncancel","uncancel"));

      fields.add(new ButtonField(FN_SUBMIT));
    
      fields.setHtmlExtra("class=\"formText\"");
      fields.setShowErrorText(true);
      //fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + ".createFields("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("createFields(appSeqNum=" + appSeqNum + ")",e.toString());
    }
  }

  public boolean autoAct()
  {
    log.debug("auto acting with action name: " + autoActionName);
    if (autoActionName.equals(AN_CANCEL))
    {
      TierScoreQueueOps.doCancelScore(appSeqNum,user);
      setNewQueue(TierScoreQueueOps.Q_CS_CANCELED);
    }
    // retry will trigger auto score retry
    else if (autoActionName.equals(AN_RETRY))
    {
      TierScoreQueueOps.doRetryScore(appSeqNum,user);
    }
    // uncancel will move back to auto score queue
    else if (autoActionName.equals(AN_UNCANCEL))
    {
      TierScoreQueueOps.doUncancelScore(appSeqNum,user);
      setNewQueue(TierScoreQueueOps.Q_CS_AUTO);
    }

    // clear the action field value
    setData(autoActionName,"");

    return true;
  }

  public boolean autoSubmit()
  {
    try
    {
      int score = fields.getField(FN_CBT_SCORE).asInteger();
      TierScoreQueueOps.doManualScore(appSeqNum,user,score);
      setNewQueue(TierScoreQueueOps.Q_CS_COMPLETED);
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoSubmit("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoSubmit(appSeqNum=" + appSeqNum + ")",e.toString());
    }

    return false;
  }

  public boolean autoLoad()
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  m.cbt_credit_score    cbt_score,      "
                + "         a.average_score       auto_score      "
                + " from    merchant m,                           "
                + "         credit_score_average a                "
                + " where   m.app_seq_num = ?                     "
                + "         and m.app_seq_num = a.app_seq_num (+) ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      setFields(rs);
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoLoad("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoLoad(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    finally
    {
      cleanUp(ps,rs);
    }

    return false;
  }

  public boolean canSubmit()
  {
    return getType() == TierScoreQueueOps.Q_CS_AUTO;
  }
  public boolean canCancel()
  {
    return getType() == TierScoreQueueOps.Q_CS_AUTO; 
  }
  public boolean canUncancel()
  {
    return getType() == TierScoreQueueOps.Q_CS_CANCELED;
  }
  public boolean canRetry()
  {
    return getField(FN_AUTO_SCORE).asInteger() == 0 
          && getType() == TierScoreQueueOps.Q_CS_AUTO
          && !(new TierAutoState(appSeqNum).isInProgress);
  }
}
