package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;

public class ReviewQueueLogs extends SQLJConnectionBase
{
  public static final int MIN_QUEUE = 2600;
  public static final int MAX_QUEUE = 3005;

  private List entries = new ArrayList();
  
  public ReviewQueueLogs(long appSeqNum)
  {
    loadEntries(appSeqNum);
  }
  
  public class LogEntry
  {
    private String dateStr;
    private String userName;
    private String description;
    
    public LogEntry(String dateStr, String userName, int oldQueue, 
      String oldQueueDesc, int newQueue, String newQueueDesc, int action)
    {
      this.dateStr = dateStr;
      this.userName = userName;
      switch (action)
      {
        // create
        case 1:
          description = "Created item in " + newQueueDesc + " Queue (" 
            + newQueue + ")";
          break;

        // delete
        case 3:
          description = "Deleted item in " + oldQueueDesc + " Queue (" 
            + oldQueue + ")";
          break;

        // move
        case 4:
          description = "Moved item from " + oldQueueDesc + " Queue (" 
            + oldQueue + ") to " + newQueueDesc + " Queue (" + newQueue + ")";
          break;

        default:
          description = "unknown action";
      }
    }

    public LogEntry(String dateStr, String userName, String description)
    {
      this.dateStr = dateStr;
      this.userName = userName;
      this.description = description;
    }

    public String getDateStr()
    {
      return dateStr;
    }

    public String getUserName()
    {
      return userName;
    }

    public String getDescription()
    {
      return description;
    }
  }

  private void loadEntries(long appSeqNum)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  l.user_name,                          "
                + "         l.action,                             "
                + "         l.old_type,                           "
                + "         l.new_type,                           "
                + "         l.log_date,                           "
                + "         l.description,                        "
                + "         t1.description old_desc,              "
                + "         t2.description new_desc               "
                + " from    q_log l,                              "
                + "         q_types t1,                           "
                + "         q_types t2                            "
                + " where   id = ?                                "
                + "         and l.user_name not like 'q_data%'    "
                + "         and ( ( l.old_type = -1               "
                + "                 or ( l.old_type >= ?          "
                + "                      and l.old_type < ? ) )   "
                + "               and                             "
                + "               ( l.new_type = -1               "
                + "                 or ( l.new_type >= ?          "
                + "                      and l.new_type < ? ) ) ) "
                + "         and l.old_type = t1.type (+)          "
                + "         and l.new_type = t2.type (+)          "
                + " order by l.log_seq_num desc                   ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setInt(2,MIN_QUEUE);
      ps.setInt(3,MAX_QUEUE);
      ps.setInt(4,MIN_QUEUE);
      ps.setInt(5,MAX_QUEUE);

      rs = ps.executeQuery();
      while (rs.next())
      {
        String dateStr = DateTimeFormatter.getFormattedDate(rs.getTimestamp("log_date"),"MM/dd/yy hh:mm:ss a");
        entries.add(new LogEntry(dateStr,
                                 rs.getString("user_name"),
                                 rs.getInt("old_type"),
                                 rs.getString("old_desc"),
                                 rs.getInt("new_type"),
                                 rs.getString("new_desc"),
                                 rs.getInt("action")));
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".loadEntries("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("loadEntries(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  public List getEntries()
  {
    return entries;
  }
}
