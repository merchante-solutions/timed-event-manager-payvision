package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;

public class CreditDecisionItemBean extends BaseItemBean
{
  // queue categories
  public static final int     QCAT_LOW_SCORE      = 1;
  public static final int     QCAT_HIGH_VOLUME    = 2;
  public static final int     QCAT_LARGE_TICKET   = 3;

  // reason types
  public static final String  RT_NONE             = "none";
  public static final String  RT_APPROVE          = "approve";
  public static final String  RT_PEND             = "pend";
  public static final String  RT_CANCEL           = "cancel";
  public static final String  RT_DECLINE          = "decline";

  private String getReasonGroupName(String reasonType)
  {
    return "fg" + reasonType;
  }

  private String getReasonFieldName(String reasonType, String code)
  {
    return reasonType + "_" + code;
  }

  private int getReasonCode(String reasonType)
  {
    if (reasonType.equals(RT_APPROVE))
    {
      return 4;
    }
    else if (reasonType.equals(RT_PEND))
    {
      return 2;
    }
    else if (reasonType.equals(RT_CANCEL))
    {
      return 3;
    }
    else if (reasonType.equals(RT_DECLINE))
    {
      return 1;
    }

    throw new RuntimeException("Invalid reason type: " + reasonType);
  }

  private void createCreditReasonFields(String reasonType)
  {
    PreparedStatement ps = null;
    ResultSet rs  = null;

    try
    {
      connect();

      String qs = " select  code, label       "
                + " from    q_credit_reasons  "
                + " where   type = ?          ";

      ps = con.prepareStatement(qs);
      ps.setInt(1,getReasonCode(reasonType));

      // generate a checkbox field for each reason
      rs = ps.executeQuery();
      FieldGroup reasonGroup = new FieldGroup(getReasonGroupName(reasonType));
      while(rs.next())
      {
        String code = rs.getString("code");
        String label = rs.getString("label");
        String fieldName = getReasonFieldName(reasonType,code);
        Field reason = new CheckboxField(fieldName,label,false);
        reasonGroup.add(reason);
      }
      fields.add(reasonGroup);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".createCreditReasonFields(" 
        + reasonType + "): " + e);
      logEntry("createCreditReasonFields(" + reasonType + ")",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      createCreditReasonFields(RT_DECLINE);
      createCreditReasonFields(RT_PEND);
      createCreditReasonFields(RT_CANCEL);

      fields.add(new ButtonField("approve"));
      fields.add(new ButtonField("pend"));
      fields.add(new ButtonField("decline"));
      fields.add(new ButtonField("cancel"));

      fields.setGroupHtmlExtra("class=\"smallText\"");
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".createFields("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("createFields(appSeqNum=" + appSeqNum + ")",e.toString());
    }
  }

  private String makeCodeString(String reasonType)
  {
    StringBuffer codeString = new StringBuffer();
    try
    {
      if (reasonType.equals(RT_APPROVE))
      {
        return "";
      }

      String groupName = getReasonGroupName(reasonType);
      for (Iterator i = getGroupIterator(groupName); i.hasNext(); )
      {
        Field field = (Field)i.next();
        if (field instanceof CheckboxField
            && ((CheckboxField)field).isChecked())
        {
          if (codeString.length() > 0)
          {
            codeString.append(",");
          }
          String name = field.getName();
          String code = name.substring(name.indexOf('_') + 1);
          codeString.append(code);
        }
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::makeCodeString(" + reasonType + "): " + e);
      logEntry("makeCodeString(" + reasonType + ")",e.toString());
    }
    return codeString.toString();
  }

  private void resetReasonGroup(String reasonType)
  {
    FieldGroup fg = (FieldGroup)fields.getField(getReasonGroupName(reasonType));
    if (fg != null)
    {
      fg.reset();
    }
  }

  private void resetIrrelevantReasons(String reasonType)
  {
    if (!reasonType.equals(RT_DECLINE))
    {
      resetReasonGroup(RT_DECLINE);
    }
    if (!reasonType.equals(RT_PEND))
    {
      resetReasonGroup(RT_PEND);
    }
    if (!reasonType.equals(RT_CANCEL))
    {
      resetReasonGroup(RT_CANCEL);
    }
  }

  private void submitReasonCodes(String reasonType) throws Exception
  {
    PreparedStatement ps = null;
    String qs = null;

    try
    {
      connect();

      String codeString = makeCodeString(reasonType);

      qs =  " delete from q_credit_data where id = ? ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.execute();
      ps.close();

      qs = " insert into q_credit_data ( id, codes ) values ( ?, ? ) ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setString(2,codeString);
      ps.execute();

      resetIrrelevantReasons(reasonType);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  private int getQueueCategory()
  {
    int queueType = fields.getField("type").asInteger();
    switch (queueType)
    {
      case ReviewQueueOps.Q_LS_NEW:
      case ReviewQueueOps.Q_LS_PENDED:
      case ReviewQueueOps.Q_LS_APPROVED:
      case ReviewQueueOps.Q_LS_CANCELED:
      case ReviewQueueOps.Q_LS_DECLINED:
        return QCAT_LOW_SCORE;

      case ReviewQueueOps.Q_HV_NEW:
      case ReviewQueueOps.Q_HV_PENDED:
      case ReviewQueueOps.Q_HV_APPROVED:
      case ReviewQueueOps.Q_HV_CANCELED:
      case ReviewQueueOps.Q_HV_DECLINED:
        return QCAT_HIGH_VOLUME;

      case ReviewQueueOps.Q_LT_NEW:
      case ReviewQueueOps.Q_LT_PENDED:
      case ReviewQueueOps.Q_LT_APPROVED:
      case ReviewQueueOps.Q_LT_CANCELED:
      case ReviewQueueOps.Q_LT_DECLINED:
        return QCAT_LARGE_TICKET;
    }
    throw new RuntimeException("Unsupported queueType (" + queueType + ")");
  }

  private void doPend()
  {
    switch (getQueueCategory())
    {
      case QCAT_LOW_SCORE:
        ReviewQueueOps.doLowScorePend(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_LS_PENDED);
        break;

      case QCAT_HIGH_VOLUME:
        ReviewQueueOps.doHighVolumePend(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_HV_PENDED);
        break;

      case QCAT_LARGE_TICKET:
        ReviewQueueOps.doLargeTicketPend(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_LT_PENDED);
        break;
    }
  }

  private void doApprove()
  {
    switch (getQueueCategory())
    {
      case QCAT_LOW_SCORE:
        ReviewQueueOps.doLowScoreApprove(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_LS_APPROVED);
        break;

      case QCAT_HIGH_VOLUME:
        ReviewQueueOps.doHighVolumeApprove(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_HV_APPROVED);
        break;

      case QCAT_LARGE_TICKET:
        ReviewQueueOps.doLargeTicketApprove(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_LT_APPROVED);
        break;
    }
  }

  private void doCancel()
  {
    switch (getQueueCategory())
    {
      case QCAT_LOW_SCORE:
        ReviewQueueOps.doLowScoreCancel(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_LS_CANCELED);
        break;

      case QCAT_HIGH_VOLUME:
        ReviewQueueOps.doHighVolumeCancel(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_HV_CANCELED);
        break;

      case QCAT_LARGE_TICKET:
        ReviewQueueOps.doLargeTicketCancel(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_LT_CANCELED);
        break;
    }
  }

  private void doDecline()
  {
    switch (getQueueCategory())
    {
      case QCAT_LOW_SCORE:
        ReviewQueueOps.doLowScoreDecline(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_LS_DECLINED);
        break;

      case QCAT_HIGH_VOLUME:
        ReviewQueueOps.doHighVolumeDecline(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_HV_DECLINED);
        break;

      case QCAT_LARGE_TICKET:
        ReviewQueueOps.doLargeTicketDecline(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_LT_DECLINED);
        break;
    }
  }

  public boolean autoSubmit()
  {
    try
    {
      if (autoSubmitName.equals("approve"))
      {
        submitReasonCodes(RT_APPROVE);
        doApprove();
      }
      else if (autoSubmitName.equals("pend"))
      {
        submitReasonCodes(RT_PEND);
        doPend();
      }
      else if (autoSubmitName.equals("cancel"))
      {
        submitReasonCodes(RT_CANCEL);
        doCancel();
      }
      else if (autoSubmitName.equals("decline"))
      {
        submitReasonCodes(RT_DECLINE);
        doDecline();
      }
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoSubmit("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoSubmit(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    return false;
  }

  private String getQueueReasonType()
  {
    int queueType = fields.getField("type").asInteger();
    switch (queueType)
    {
      case ReviewQueueOps.Q_LS_NEW:
      case ReviewQueueOps.Q_HV_NEW:
      case ReviewQueueOps.Q_LT_NEW:
        return RT_NONE;

      case ReviewQueueOps.Q_LS_PENDED:
      case ReviewQueueOps.Q_HV_PENDED:
      case ReviewQueueOps.Q_LT_PENDED:
        return RT_PEND;

      case ReviewQueueOps.Q_LS_APPROVED:
      case ReviewQueueOps.Q_HV_APPROVED:
      case ReviewQueueOps.Q_LT_APPROVED:
        return RT_APPROVE;

      case ReviewQueueOps.Q_LS_CANCELED:
      case ReviewQueueOps.Q_HV_CANCELED:
      case ReviewQueueOps.Q_LT_CANCELED:
        return RT_CANCEL;

      case ReviewQueueOps.Q_LS_DECLINED:
      case ReviewQueueOps.Q_HV_DECLINED:
      case ReviewQueueOps.Q_LT_DECLINED:
        return RT_DECLINE;
    }
    throw new RuntimeException("Invalid queue type: " + queueType);
  }

  public void loadReasonCodes() throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select nvl( codes, 'none' ) from q_credit_data where id = ? ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      if (rs.next())
      {
        String codes = rs.getString(1);
        if (codes == null || codes.length() == 0 || codes.equals("none")) return;

        String reasonType = getQueueReasonType();
        if (reasonType.equals(RT_NONE)) return;

        StringTokenizer tok = new StringTokenizer(codes,",");
        while (tok.hasMoreTokens())
        {
          String code = tok.nextToken();
          Field field = fields.getField(getReasonFieldName(reasonType,code));
          if (field != null)
          {
            field.setData("y");
          }
        }
      }
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  public boolean autoLoad()
  {
    try
    {
      loadReasonCodes();
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoLoad("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoLoad(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    return false;
  }

  public Iterator getReasonGroupIterator(String reasonType)
  {
    return getGroupIterator(getReasonGroupName(reasonType));
  }

  public boolean isApproved()
  {
    int queueType = fields.getField("type").asInteger();
    switch (queueType)
    {
      case ReviewQueueOps.Q_LS_APPROVED:
      case ReviewQueueOps.Q_HV_APPROVED:
      case ReviewQueueOps.Q_LT_APPROVED:
        return true;
    }
    return false;
  }
}
