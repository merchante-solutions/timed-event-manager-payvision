/**
 * Helper utility to allow .java classes to generate a SQLJ ResultSetIterator.
 * See com.mes.ops.cbt.ReviewQueue...
 */
package com.mes.ops.cbt;

import oracle.jdbc.OraclePreparedStatement;
import sqlj.runtime.ExecutionContext;
import sqlj.runtime.ExecutionContext.OracleContext;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.error.RuntimeRefErrors;
import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ref.OraRTResultSet;
import sqlj.runtime.ref.ResultSetIterImpl;

public class SqljHelper
{
  public static OracleContext getOracleContext(DefaultContext defCtx)
    throws java.sql.SQLException
  {
    if (defCtx==null) 
    {
      RuntimeRefErrors.raise_NULL_CONN_CTX();
    }

    OracleContext oraCtx = ((defCtx.getExecutionContext()==null) ? 
                            ExecutionContext.raiseNullExecCtx() : 
                            defCtx.getExecutionContext().getOracleContext());

    return oraCtx;
  }

  public static OraclePreparedStatement prepOracleStatement(
    DefaultContext defCtx, OracleContext oraCtx, String qs, String key) 
    throws java.sql.SQLException
  {
    return oraCtx.prepareOracleStatement(defCtx,key,qs);
  }
  public static OraclePreparedStatement prepOracleStatement(
    DefaultContext defCtx, OracleContext oraCtx, String qs) 
    throws java.sql.SQLException
  {
    return prepOracleStatement(defCtx,oraCtx,qs,null);
  }

  public static ResultSetIterator doOracleQuery(OracleContext oraCtx,
    OraclePreparedStatement oraPs, String key) throws java.sql.SQLException
  {
    return new ResultSetIterImpl(new OraRTResultSet(
              oraCtx.oracleExecuteQuery(),oraPs,key,null));
  }
  public static ResultSetIterator doOracleQuery(OracleContext oraCtx,
    OraclePreparedStatement oraPs) throws java.sql.SQLException
  {
    return doOracleQuery(oraCtx,oraPs,null);
  }

}

