package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.DisabledCheckboxField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;

public class DocumentationItemBean extends BaseItemBean
{
  public class LabelField extends Field
  {
    public LabelField(String fname, String labelText)
    {
      super(fname,0,0,true);
      setData(labelText);
    }
    
    protected String renderHtmlField()
    {
      StringBuffer html = new StringBuffer();
      html.append("<span " + getHtmlExtra() + ">");
      html.append(getRenderData());
      html.append("</span>");
      return html.toString();
    }
  }
  
  /*
  ** public void createRequiredDocumentFields()
  **
  ** Creates required document fields based on business type and annual
  ** V/MC volume.
  */
  public void createRequiredDocumentFields()
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();
      
      // determine merchant app conditions
      double  annualVolume      = -1L;
      int     businessType      = -1;
      String  businessTypeDesc  = null;

      String qs = " select  m.annual_vmc_sales,                 "
                + "         m.bustype_code,                     "
                + "         b.bustype_desc                      "
                + " from    merchant m,                         "
                + "         bustype b                           "
                + " where   app_seq_num = ?                     "
                + "         and m.bustype_code = b.bustype_code ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      if (rs.next())
      {
        annualVolume      = rs.getDouble(1);
        businessType      = rs.getInt(2);
        businessTypeDesc  = rs.getString(3);
      }

      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }

      boolean isCorp    = (businessType == 2);
      boolean isLlc     = (businessType == 8);
      boolean isPart    = (businessType == 3);
      boolean is250Plus = (annualVolume > 250000 && isCorp);
      boolean is100Plus = (annualVolume > 100000 && !isCorp);
      
      // generate business description
      StringBuffer busDesc = new StringBuffer();
      busDesc.append("Business Type: " + businessTypeDesc + ", ");
      if (!isCorp)
      {
        if (is100Plus)
        {
          busDesc.append("Greater than $100,000");
        }
        else
        {
          busDesc.append("Less than $100,000");
        }
      }
      else
      {
        if (is250Plus)
        {
          busDesc.append("Greater than $250,000");
        }
        else
        {
          busDesc.append("Less than $250,000");
        }
      }
      fields.setData("businessDescription",busDesc.toString());

      qs  = " select  doc_id,     "
          + "         description "
          + " from    q_doc_items "
          + " order by doc_id     ";

      ps = con.prepareStatement(qs);
      rs = ps.executeQuery();

      // determine which might apply to this merchant
      FieldGroup docGroups = new FieldGroup("docGroups");
      while (rs.next())
      {
        int docId = rs.getInt("doc_id");
        boolean docNeeded = false;
        boolean orNeeded = false;
        switch (docId)
        {
          case MesQueues.QDI_MERCHANT_AGREEMENT:
          case MesQueues.QDI_VMC_VOLUME_TICKET:
          case MesQueues.QDI_ONSITE_INSPECTION:
          case MesQueues.QDI_PRIVACY_POLICY:
          case MesQueues.QDI_PRIOR_ACTIVITY:
          case MesQueues.QDI_WEBSITE_COPY:
            docNeeded = true;
            break;
            
          case MesQueues.QDI_CREDIT_REPORT:
          case MesQueues.QDI_PERSONAL_GUARANTY:
            docNeeded = (!isCorp || !is250Plus);
            break;
            
          case MesQueues.QDI_LLC_PARTNERSHIP_RESOLUTION:
            docNeeded = (isLlc || isPart);
            orNeeded  = (docNeeded && is100Plus);
            break;
            
          case MesQueues.QDI_2_YEARS_FINANCIALS:
            docNeeded =    (!isCorp && is100Plus)
                        || ( isCorp && !is250Plus);
            break;
            
          case MesQueues.QDI_3_YEARS_FINANCIALS:
            docNeeded = (isCorp && is250Plus);
            break;
            
          case MesQueues.QDI_CORPORATE_RESOLUTION:
            docNeeded = isCorp;
            orNeeded  = (docNeeded && !is250Plus);
            break;
        }
        String description = rs.getString("description");
        if (orNeeded)
        {
          description = description + ", or";
        }

        // create relevant fields for this merchant app
        if (docNeeded)
        {
          FieldGroup docGroup = new FieldGroup("docGroup_" + docId);
          docGroup.add(
            new LabelField("doclabel_" + docId,description));
            
          if(docId == MesQueues.QDI_MERCHANT_AGREEMENT)
          {
            docGroup.add(new DisabledCheckboxField("docrequired_" + docId, "", true));
          }
          else
          {
            docGroup.add(new CheckboxField("docrequired_" + docId,false));
          }
          docGroup.add(new CheckboxField("docreceived_" + docId,false));
          docGroups.add(docGroup);
        }
      }
      fields.add(docGroups);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + ".createRequiredDocumentFields(" + appSeqNum + "): " + e);
      logEntry("createRequiredDocumentFields(" + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new LabelField("businessDescription","Unknown"));

      // need to grab the app seq num out of the request in order to
      // generate the doc fields
      createRequiredDocumentFields();

      fields.add(new ButtonField("update"));
      fields.add(new ButtonField("cancel"));
    
      fields.setGroupHtmlExtra("class=\"smallText\"");
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".createFields("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("createFields(appSeqNum=" + appSeqNum + ")",e.toString());
    }
  }

  /*
  ** public Field[][][] getDocFields()
  **
  ** Creates a multi-dimensional array of doc group fields.  First dimension
  ** indicates which category the requirement is (0-application checklist,
  ** 1-credit documentation).  Next dimension is an index within that category.
  ** Last dimension indicates which field it is (0-description label, 1-required
  ** checkbox, 2-received checkbox)
  **
  ** RETURNS: array of doc group fields.
  */
  public Field[][][] getDocFields()
  {
    // create arrays based on number of groups
    FieldGroup docGroups = (FieldGroup)fields.getField("docGroups");
    Field[][][] docFields = new Field[2][][];
    Field[][] appFields = new Field[6][3];
    Field[][] credFields = new Field[docGroups.size() - 6][3];
    docFields[0] = appFields;
    docFields[1] = credFields;
    
    // iterate through the groups, keep track of indexes for
    // both categories, place fields in array according to category
    // and type
    int[] fieldIndex = new int[2];
    for (Iterator i = docGroups.iterator(); i.hasNext();)
    {
      // determine the docId from the docGroup name,
      // and category from the docId
      FieldGroup docGroup = (FieldGroup)i.next();
      StringTokenizer tok = new StringTokenizer(docGroup.getName(),"_");
      tok.nextToken();
      int docId = 0;
      try
      {
        docId = Integer.parseInt(tok.nextToken());
      }
      catch (Exception e) {}
      int category = (docId > MesQueues.QDI_WEBSITE_COPY ? 1 : 0);
      
      // place fields in array
      docFields[category][fieldIndex[category]][0]
        = docGroup.getField("doclabel_" + docId);
      docFields[category][fieldIndex[category]][1]
        = docGroup.getField("docrequired_" + docId);
      docFields[category][fieldIndex[category]][2]
        = docGroup.getField("docreceived_" + docId);
      ++fieldIndex[category];
    }
    
    return docFields;
  }

  /**
   * Clears any old doc data items out of q_doc_data, then inserts doc items
   * as indicated by the doc fields.
   */
  private void submitDocData()
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      // clear out current set of doc data
      String qs = " delete from q_doc_data where id = ? ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.execute();
      ps.close();
      
      // setup a prepared statement to insert doc data
      qs  = " insert into q_doc_data  "
          + " ( id,                   "
          + "   doc_id,               "
          + "   doc_required,         "
          + "   doc_received )        "
          + " values                  "
          + " ( ?, ?, ?, ? )          ";
      ps = con.prepareStatement(qs);

      // iterate through doc groups
      FieldGroup docGroups = (FieldGroup)fields.getField("docGroups");
      int receivedCount = 0;
      int requiredCount = 0;
      for (Iterator i = docGroups.iterator(); i.hasNext();)
      {
        // extract doc group data
        FieldGroup docGroup = (FieldGroup)i.next();
        StringTokenizer tok = new StringTokenizer(docGroup.getName(),"_");
        tok.nextToken();
        String docId = tok.nextToken();
        String docRequired = docGroup.getData("docrequired_" + docId);
        String docReceived = docGroup.getData("docreceived_" + docId);

        // insert a doc data item
        ps.setLong(1,appSeqNum);
        ps.setString(2,docId);
        ps.setString(3,docRequired);
        ps.setString(4,docReceived);
        ps.execute();
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".submitDocData(" 
        + appSeqNum + "): " + e);
      logEntry("submitDocData(" + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  /**
   * Returns true if all required documentation items have been received (according
   * to q_doc_data table).  Also, if current queue is already complete then the app
   * is NOT considered completable, since it already is.
   */
  private boolean isDocCompletable()
  {
    // if previously completed, don't allow complete again
    if (fields.getField("type").asInteger() == ReviewQueueOps.Q_DOC_COMPLETED)
    {
      return false;
    }

    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      // count the number of required doc items that are not yet received
      String qs = " select  count(*)                "
                + " from    q_doc_data              "
                + " where   id = ?                  "
                + "         and doc_required = 'y'  "
                + "         and doc_received <> 'y' ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      return (!rs.next() || rs.getInt(1) == 0);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".isDocComplete(" 
        + appSeqNum + "): " + e);
      logEntry("isDocComplete(" + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }

    return false;
  }

  /**
   * Returns true if all required documentation besides pendable items (onsite
   * inspection) have been received (according to q_doc_data table).
   */
  private boolean isDocPendable()
  {
    // if previously pended, don't allow pend again
    if (fields.getField("type").asInteger() == ReviewQueueOps.Q_DOC_PENDED)
    {
      return false;
    }

    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      // count the number of required doc items that are not yet received
      String qs = " select  count(*)                "
                + " from    q_doc_data              "
                + " where   id = ?                  "
                + "         and doc_required = 'y'  "
                + "         and doc_received <> 'y' "
                + "         and doc_id <> ?         ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setInt(2,MesQueues.QDI_ONSITE_INSPECTION);
      rs = ps.executeQuery();
      return (!rs.next() || rs.getInt(1) == 0);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".isDocPendable(" 
        + appSeqNum + "): " + e);
      logEntry("isDocPendable(" + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }

    return false;
  }

  
  /**
   * Submits doc data to q_doc_data then performs any indicated queue operations
   * that are needed.
   */
  protected boolean autoSubmit()
  {
    long appSeqNum = getField("id").asLong();
    try
    {
      submitDocData();

      if (!fields.getField("cancel").isBlank())
      {
        ReviewQueueOps.doDocCancel(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_DOC_CANCELED);
      }
      else if (isDocCompletable())
      {
        ReviewQueueOps.doDocComplete(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_DOC_COMPLETED);
      }
      else if (isDocPendable())
      {
        ReviewQueueOps.doDocPend(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_DOC_PENDED);
      }
      else if (fields.getField("type").asInteger() 
                  == ReviewQueueOps.Q_DOC_CANCELED)
      {
        ReviewQueueOps.doDocUncancel(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_DOC_NEW);
      }


      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoSubmit(" 
      + appSeqNum + "): " + e);
      logEntry("autoSubmit(" + appSeqNum + ")",e.toString());
    }
    
    return false;
  }

  private void loadDocData()
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();
      
      String qs = " select  'docrequired_'||doc_id  required_name,  "
                + "         'docreceived_'||doc_id  received_name,  "
                + "         doc_required,                           "
                + "         doc_received                            "
                + " from    q_doc_data                              "
                + " where   id = ?                                  ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      while (rs.next())
      {
        String docRequired = rs.getString("doc_required").toLowerCase();
        String docReceived = rs.getString("doc_received").toLowerCase();
        String receivedName = rs.getString("received_name");
        String requiredName = rs.getString("required_name");
        fields.setData(receivedName,docReceived);
        fields.setData(requiredName,docRequired);
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".loadDocData(" 
        + appSeqNum + "): " + e);
      logEntry("loadDocData(" + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  protected boolean autoLoad()
  {
    try
    {
      loadDocData();
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + ".autoLoad(" + appSeqNum + "): " + e);
      logEntry("autoLoad(" + appSeqNum + ")",e.toString());
    }
    return false;
  }
}

