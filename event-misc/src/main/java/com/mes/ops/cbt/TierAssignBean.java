package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.tools.DropDownTable;

public class TierAssignBean extends BaseItemBean
{
  static Logger log = Logger.getLogger(TierAssignBean.class);

  public static final String AN_CANCEL    = "actionCancel";
  public static final String AN_UNCANCEL  = "actionUncancel";

  public static final String FN_TIER_NUM  = "tierNum";
  public static final String FN_SUBMIT    = "submit";

  public class TierNumberTable extends DropDownTable
  {
    public TierNumberTable()
    {
      addElement("","select one");
      addElement("1G","Tier 1G");
      addElement("2G","Tier 2G");
      addElement("3G","Tier 3G");
      addElement("4G","Tier 4G");
      addElement("1F","Tier 1F");
      addElement("2F","Tier 2F");
      addElement("3F","Tier 3F");
      addElement("4F","Tier 4F");
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new ButtonField(AN_CANCEL,"cancel","cancel"));
      fields.add(new ButtonField(AN_UNCANCEL,"uncancel","uncancel"));

      fields.add(new DropDownField(FN_TIER_NUM,new TierNumberTable(),false));

      fields.add(new ButtonField(FN_SUBMIT));
    
      fields.setHtmlExtra("class=\"formText\"");
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + ".createFields("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("createFields(appSeqNum=" + appSeqNum + ")",e.toString());
    }
  }

  /**
   * Action handling.  Users may cancel, uncancel or switch to manual
   * scoring.
   */
  public boolean autoAct()
  {
    log.debug("auto acting with action name: " + autoActionName);
    if (autoActionName.equals(AN_CANCEL))
    {
      TierAssignQueueOps.doCancelTierAssign(appSeqNum,user);
      setNewQueue(TierAssignQueueOps.Q_TIER_ASSIGN_CANCELED);
    }
    else if (autoActionName.equals(AN_UNCANCEL))
    {
      TierAssignQueueOps.doUncancelTierAssign(appSeqNum,user);
      setNewQueue(TierAssignQueueOps.Q_TIER_ASSIGN_NEW);
      // HACK: need to auto load the tier num field here...
      autoLoad();
    }

    // clear the action field value
    setData(autoActionName,"");

    return true;
  }

  public boolean autoSubmit()
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      // update merchant table data with assoc num and sic code
      String qs = " update  merchant            " +
                  " set     client_data_2 = ?   " +
                  " where   app_seq_num   = ?   ";

      ps = con.prepareStatement(qs);
      ps.setString(1,getData(FN_TIER_NUM));
      ps.setLong(2,appSeqNum);
      ps.execute();

      TierAssignQueueOps.doCompleteTierAssign(appSeqNum,user);
      setNewQueue(TierAssignQueueOps.Q_TIER_ASSIGN_COMPLETED);

      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoSubmit("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoSubmit(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }

    return false;
  }

  /*
  ** public boolean autoLoad()
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean autoLoad()
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  client_data_2 " + FN_TIER_NUM + " "
                + " from    merchant        "
                + " where   app_seq_num = ? ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      setFields(rs);
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoLoad("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoLoad(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    finally
    {
      cleanUp(ps,rs);
    }

    return false;
  }

  public boolean canSubmit()
  {
    return getType() == TierAssignQueueOps.Q_TIER_ASSIGN_NEW;
  }
  public boolean canCancel()
  {
    return getType() == TierAssignQueueOps.Q_TIER_ASSIGN_NEW;
  }
  public boolean canUncancel()
  {
    return getType() == TierAssignQueueOps.Q_TIER_ASSIGN_CANCELED;
  }
}
