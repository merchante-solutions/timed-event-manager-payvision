package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.forms.ComboDateField;
import com.mes.queues.ExtraColumnBean;
import com.mes.queues.QueueBase;
import com.mes.queues.QueueData;
import com.mes.support.SyncLog;
import com.mes.user.UserBean;
import oracle.jdbc.OraclePreparedStatement;
import sqlj.runtime.ExecutionContext.OracleContext;

public class ReviewQueue extends QueueBase
{
  public boolean dateRangeEnabled()
  {
    return true;
  }
  
  protected void loadQueueData(UserBean user)
  {
    OracleContext oraCtx = null;
    OraclePreparedStatement oraPs = null;

    try
    {
      java.sql.Date fromDate = 
        ((ComboDateField)fields.getField("fromDate")).getSqlDate();
      java.sql.Date toDate = 
        ((ComboDateField)fields.getField("toDate")).getSqlDate();

      oraCtx = SqljHelper.getOracleContext(Ctx);

      String qs = " select    qd.id                   id,                 "
                + "           m.merch_number          merchant_number,    "
                + "           qd.type                 type,               "
                + "           qd.item_type            item_type,          "
                + "           qd.owner                owner,              "
                + "           qd.date_created         date_created,       "
                + "           qd.source               source,             "
                + "           qd.affiliate            affiliate,          "
                + "           qd.last_changed         last_changed,       "
                + "           qd.last_user            last_user,          "
                + "           m.merch_business_name   description,        "
                + "           m.merch_email_address   merch_email,        "
                + "           u.name                  rep_name,           "
                + "           u.email                 rep_email,          "
                + "           qd.id                   control,            "
                + "           qd.locked_by            locked_by,          "
                + "           nvl(qn.note_count,0)    note_count,         "
                + "           qt.status               status,             "
                + "           nvl( m.cbt_credit_score, '--' )             "
                + "                                   score,              "
                + "           to_char( m.merch_average_cc_tran,           "
                + "                    'L999G999G999D99' )                "
                + "                                   ticket,             "
                + "           to_char( m.annual_vmc_sales,                "
                + "                    'L999G999G999D99' )                "
                + "                                   volume              "
                + "                                                       "
                + " from      q_data                  qd,                 "
                + "           ( select    id,                             "
                + "                       count(*)    note_count          "
                + "             from      q_notes                         "
                + "             group by id )         qn,                 "
                + "           q_types                 qt,                 "
                + "           merchant                m,                  "
                + "           users                   u                   "
                + "                                                       "
                + " where     qd.type = :1                                "
                + "           and qd.type = qt.type                       "
                + "           and qd.id = qn.id(+)                        "
                + "           and m.app_seq_num = qd.id                   "
                + "           and qd.source = u.login_name(+)             "
                + "           and trunc(qd.date_created)                  "
                + "                 between :2 and :3                     "
                + "                                                       "
                + " order by  qd.date_created asc                         ";
        
      oraPs = SqljHelper.prepOracleStatement(Ctx,oraCtx,qs);
      oraPs.setInt (1,this.type);
      oraPs.setDate(2,fromDate);
      oraPs.setDate(3,toDate);

      it = SqljHelper.doOracleQuery(oraCtx,oraPs);
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + ".loadQueueData(): " + e);
      logEntry("loadQueueData(" + user.getLoginName() + ")", e.toString());
    }
    finally
    {
      try { oraCtx.oracleCloseQuery(); } catch (Exception e) { }
    }
  }

  protected void loadQueueItem(long id)
  {
    OracleContext oraCtx = null;
    OraclePreparedStatement oraPs = null;

    try
    {
      oraCtx = SqljHelper.getOracleContext(Ctx);

      String qs = " select    qd.id                   id,                 "
                + "           m.merch_number          merchant_number,    "
                + "           qd.type                 type,               "
                + "           qd.item_type            item_type,          "
                + "           qd.owner                owner,              "
                + "           qd.date_created         date_created,       "
                + "           qd.source               source,             "
                + "           qd.affiliate            affiliate,          "
                + "           qd.last_changed         last_changed,       "
                + "           qd.last_user            last_user,          "
                + "           m.merch_business_name   description,        "
                + "           m.merch_email_address   merch_email,        "
                + "           u.name                  rep_name,           "
                + "           u.email                 rep_email,          "
                + "           qd.id                   control,            "
                + "           qd.locked_by            locked_by,          "
                + "           nvl(qn.note_count,0)    note_count,         "
                + "           qt.status               status,             "
                + "           nvl( m.cbt_credit_score, '--' )             "
                + "                                   score,              "
                + "           to_char( m.merch_average_cc_tran,           "
                + "                    'L999G999G999D99' )                "
                + "                                   ticket,             "
                + "           to_char( m.annual_vmc_sales,                "
                + "                    'L999G999G999D99' )                "
                + "                                   volume              "
                + "                                                       "
                + " from      q_data                  qd,                 "
                + "           ( select    id,                             "
                + "                       count(*)    note_count          "
                + "             from      q_notes                         "
                + "             group by id )         qn,                 "
                + "           q_types                 qt,                 "
                + "           merchant                m,                  "
                + "           users                   u                   "
                + "                                                       "
                + " where     qd.type = :1                                "
                + "           and qd.type = qt.type                       "
                + "           and qd.id = qn.id(+)                        "
                + "           and qd.id = :2                              "
                + "           and m.app_seq_num = qd.id                   "
                + "           and qd.source = u.login_name(+)             ";

      oraPs = SqljHelper.prepOracleStatement(Ctx,oraCtx,qs);
      oraPs.setInt (1,this.type);
      oraPs.setLong(2,id);

      it = SqljHelper.doOracleQuery(oraCtx,oraPs);
    }
    catch(Exception e)
    {
      e.printStackTrace();
      System.out.println(this.getClass().getName() + ".loadQueueItem(): " + e);
      logEntry("loadQueueItem(" + user.getLoginName() + ")", e.toString());
    }
    finally
    {
      try { oraCtx.oracleCloseQuery(); } catch (Exception e) { }
    }
  }

  public static class CbtQueueData extends QueueData
  {
    private String  score;
    private String  volume;
    private String  merchEmail;
    private String  repName;
    private String  repEmail;
    private String  ticket;

    public void setExtendedData(ResultSet rs)
    {
      try
      {
        setScore      (rs.getString("score"));
        setVolume     (rs.getString("volume"));
        setTicket     (rs.getString("ticket"));
        setMerchEmail (rs.getString("merch_email"));
        setRepName    (rs.getString("rep_name"));
        setRepEmail   (rs.getString("rep_email"));
      }
      catch(Exception e)
      {
        SyncLog.LogEntry(this.getClass().getName() + "::setData()", e.toString());
      }
    }

    public void setScore(String score)
    {
      this.score = score;
    }
    public String getScore()
    {
      return score;
    }

    public void setVolume(String volume)
    {
      this.volume = volume;
    }
    public String getVolume()
    {
      return volume;
    }

    public void setMerchEmail(String merchEmail)
    {
      this.merchEmail = merchEmail;
    }
    public String getMerchEmail()
    {
      return merchEmail;
    }

    public void setRepName(String repName)
    {
      this.repName = repName;
    }
    public String getRepName()
    {
      return repName;
    }

    public void setRepEmail(String repEmail)
    {
      this.repEmail = repEmail;
    }
    public String getRepEmail()
    {
      return repEmail;
    }

    public void setTicket(String ticket)
    {
      this.ticket = ticket;
    }
    public String getTicket()
    {
      return ticket;
    }
  }

  protected void loadExtraColumns(ExtraColumnBean extraColumns, UserBean user)
  {
    try
    {
      // add the extra columns
      extraColumns.addColumn("Volume","volume","","","C");
      extraColumns.addColumn("Ticket","ticket","","","C");
      extraColumns.addColumn("Score","score");
    }
    catch(Exception e)
    {
      logEntry("loadExtraColumns()", e.toString());
    }
  }

  protected QueueData getEmptyQueueData()
  {
    return new CbtQueueData();
  }

  public String getDescriptionURL()
  {
    return "/jsp/setup/merchinfo4.jsp?primaryKey=";
  }

  public int getMenuId()
  {
    int result = 0;

    switch (type)
    {
      // new cb&t review queues
      case MesQueues.Q_CBT_DATA_ASSIGN_NEW:
      case MesQueues.Q_CBT_DATA_ASSIGN_COMPLETED:
      case MesQueues.Q_CBT_DATA_ASSIGN_CANCELED:
        result = MesMenus.MENU_ID_CBT_DATA_ASSIGN_QUEUES;
        break;

      case MesQueues.Q_CBT_RISKY_SIC_NEW:
      case MesQueues.Q_CBT_RISKY_SIC_APPROVED:
      case MesQueues.Q_CBT_RISKY_SIC_DECLINED:
      case MesQueues.Q_CBT_RISKY_SIC_CANCELED:
        result = MesMenus.MENU_ID_CBT_RISKY_SIC_QUEUES;
        break;

      case MesQueues.Q_CBT_PULL_CREDIT_NEW:
      case MesQueues.Q_CBT_PULL_CREDIT_COMPLETED:
      case MesQueues.Q_CBT_PULL_CREDIT_CANCELED:
        result = MesMenus.MENU_ID_CBT_PULL_CREDIT_QUEUES;
        break;

      case MesQueues.Q_CBT_LOW_SCORE_NEW:
      case MesQueues.Q_CBT_LOW_SCORE_PENDED:
      case MesQueues.Q_CBT_LOW_SCORE_APPROVED:
      case MesQueues.Q_CBT_LOW_SCORE_DECLINED:
      case MesQueues.Q_CBT_LOW_SCORE_CANCELED:
        result = MesMenus.MENU_ID_CBT_LOW_SCORE_QUEUES;
        break;

      case MesQueues.Q_CBT_HIGH_VOLUME_NEW:
      case MesQueues.Q_CBT_HIGH_VOLUME_PENDED:
      case MesQueues.Q_CBT_HIGH_VOLUME_APPROVED:
      case MesQueues.Q_CBT_HIGH_VOLUME_DECLINED:
      case MesQueues.Q_CBT_HIGH_VOLUME_CANCELED:
        result = MesMenus.MENU_ID_CBT_HIGH_VOLUME_QUEUES;
        break;

      case MesQueues.Q_CBT_LARGE_TICKET_NEW:
      case MesQueues.Q_CBT_LARGE_TICKET_PENDED:
      case MesQueues.Q_CBT_LARGE_TICKET_APPROVED:
      case MesQueues.Q_CBT_LARGE_TICKET_DECLINED:
      case MesQueues.Q_CBT_LARGE_TICKET_CANCELED:
        result = MesMenus.MENU_ID_CBT_LARGE_TICKET_QUEUES;
        break;

      case MesQueues.Q_CBT_DOC_NEW:
      case MesQueues.Q_CBT_DOC_COMPLETED:
      case MesQueues.Q_CBT_DOC_CANCELED:
      case MesQueues.Q_CBT_DOC_PENDED:
        result = MesMenus.MENU_ID_CBT_DOC_QUEUES;
        break;
    }

    return result;
  }

  public String getBackLink()
  {
    return "/jsp/menus/cbt_queue_menu.jsp?com.mes.ReportMenuId=" + getMenuId();
  }

  public String getBackLinkDesc()
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  menu_title  "
                + " from    menu_types  "
                + " where   menu_id = ? ";
      ps = con.prepareStatement(qs);
      ps.setInt(1,getMenuId());

      rs = ps.executeQuery();
      if (rs.next())
      {
        return rs.getString("menu_title");
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + ".getBackLinkDesc(): " + e);
      logEntry("getBackLinkDesc()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }

    return "";
  }
}
