package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.mes.constants.MesQueues;
import com.mes.queues.QueueTools;
import com.mes.user.UserBean;

public class ReviewQueueOps extends BaseQueueOps
{
  /********************************************************************
   *
   * Data assignment ops
   *
   *******************************************************************/
  
  public static final int Q_DA_ITEM       = MesQueues.Q_ITEM_TYPE_CBT_DATA_ASSIGN;
  public static final int Q_DA_NEW        = MesQueues.Q_CBT_DATA_ASSIGN_NEW;
  public static final int Q_DA_COMPLETED  = MesQueues.Q_CBT_DATA_ASSIGN_COMPLETED;
  public static final int Q_DA_CANCELED   = MesQueues.Q_CBT_DATA_ASSIGN_CANCELED;

  private void addDataAssign(long appSeqNum, UserBean user) throws Exception
  {
    insertQueueItem(appSeqNum,Q_DA_NEW,CS_CLIENT_REVIEW,user,"addDataAssign");
  }

  private void cancelDataAssign(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_DA_COMPLETED,"cancelDataAssign");
    moveQueueItem(appSeqNum,Q_DA_CANCELED,CS_CLIENT_REVIEW,user,"cancelDataAssign");
  }

  /**
   * Returns true if the sic code of the given app is in the risky sic table.
   */
  private boolean appIsRiskySic(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  sic                     "
                + " from    cbt_risky_sics,         "
                + "         merchant                "
                + " where   app_seq_num = ?         "
                + "         and app_sic_code = sic  ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      String msg = "appSeqNum = " + appSeqNum + ": " + e;
      logEntry("appIsRiskySic()",msg);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  private void completeDataAssign(long appSeqNum, UserBean user) throws Exception
  {
    moveQueueItem(appSeqNum,Q_DA_COMPLETED,CS_CLIENT_REVIEW,user,"completeDataAssign");
    if (appIsRiskySic(appSeqNum))
    {
      addRiskySic(appSeqNum,user);
    }
    else
    {
      addPullCredit(appSeqNum,user);
    }
  }

  public static void doDataAssignAdd(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).addDataAssign(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doDataAssignCancel(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).cancelDataAssign(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doDataAssignComplete(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).completeDataAssign(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /********************************************************************
   *
   * Risky sic ops
   *
   *******************************************************************/

  public static final int Q_RS_ITEM       = MesQueues.Q_ITEM_TYPE_CBT_RISKY_SIC;
  public static final int Q_RS_NEW        = MesQueues.Q_CBT_RISKY_SIC_NEW;
  public static final int Q_RS_APPROVED   = MesQueues.Q_CBT_RISKY_SIC_APPROVED;
  public static final int Q_RS_CANCELED   = MesQueues.Q_CBT_RISKY_SIC_CANCELED;
  public static final int Q_RS_DECLINED   = MesQueues.Q_CBT_RISKY_SIC_DECLINED;

  /**
   * Adds entry to cbt_risky_sic_apps to record the original sic that was
   * found to be risky.  Inserts a queue item into the new risky sic queue 
   * for the app.
   */
  private void addRiskySic(long appSeqNum, UserBean user) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      // add an app row if one does not exist
      String qs = " select count(*) row_count  "
                + " from   cbt_risky_sic_apps  "
                + " where  app_seq_num = ?     ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      if (rs.next() && rs.getInt("row_count") == 0)
      {
        qs = " insert into cbt_risky_sic_apps     "
           + " ( select app_seq_num, app_sic_code "
           + "   from merchant                    "
           + "   where app_seq_num = ? )          ";

        ps = con.prepareStatement(qs);
        ps.setLong(1,appSeqNum);
        ps.execute();
      }

      // add risky sic queue item to new risky sic queue
      insertQueueItem(appSeqNum,Q_RS_NEW,CS_CLIENT_REVIEW,user,"addRiskySic");
    }
    catch (Exception e)
    {
      String msg = "appSeqNum = " + appSeqNum + ": " + e;
      logEntry("addRiskySic()",msg);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  private void cancelRiskySic(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_RS_APPROVED,"cancelRiskySic");
    moveQueueItem(appSeqNum,Q_RS_CANCELED,CS_CLIENT_CANCELED,user,"cancelRiskySic");
  }

  private void declineRiskySic(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_RS_APPROVED,"declineRiskySic");
    moveQueueItem(appSeqNum,Q_RS_DECLINED,CS_CLIENT_DECLINED,user,"declineRiskySic");
  }

  private void approveRiskySic(long appSeqNum, UserBean user) throws Exception
  {
    moveQueueItem(appSeqNum,Q_RS_APPROVED,CS_CLIENT_REVIEW,user,"approveRiskySic");
    addPullCredit(appSeqNum,user);
  }

  public static void doRiskySicAdd(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).addRiskySic(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doRiskySicCancel(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).cancelRiskySic(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doRiskySicDecline(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).declineRiskySic(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doRiskySicApprove(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).approveRiskySic(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /********************************************************************
   *
   * Pull credit ops
   *
   *******************************************************************/

  public static final int Q_PC_ITEM       = MesQueues.Q_ITEM_TYPE_CBT_PULL_CREDIT;
  public static final int Q_PC_NEW        = MesQueues.Q_CBT_PULL_CREDIT_NEW;
  public static final int Q_PC_COMPLETED  = MesQueues.Q_CBT_PULL_CREDIT_COMPLETED;
  public static final int Q_PC_CANCELED   = MesQueues.Q_CBT_PULL_CREDIT_CANCELED;

  private void addPullCredit(long appSeqNum, UserBean user) throws Exception
  {
    insertQueueItem(appSeqNum,Q_PC_NEW,CS_CLIENT_REVIEW,user,"addPullCredit");
  }

  private void cancelPullCredit(long appSeqNum, UserBean user) throws Exception
  {
    String opName = "cancelPullCredit";
    validateNotInQueue(appSeqNum,Q_PC_COMPLETED,opName);
    moveQueueItem(appSeqNum,Q_PC_CANCELED,CS_CLIENT_CANCELED,user,opName);
  }

  /**
   * Returns true if the average ticket of an app is over the large ticket
   * threshold defined in cbt_review_queue_parms.
   */
  private boolean appIsLargeTicket(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  p.name                              "
                + " from    cbt_credit_review_parms p,          "
                + "         merchant m                          "
                + " where   m.app_seq_num = ?                   "
                + "         and m.merch_average_cc_tran         "
                + "           >= to_number(p.value)             "
                + "         and p.name = 'large_ticket_amount'  ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      logEntry("appIsLargeTicket()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  /**
   * Returns true if the transaction volume of an app is over the high volume
   * threshold defined in cbt_review_queue_parms.
   */
  private boolean appIsHighVolume(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  p.name                                    "
                + " from    cbt_credit_review_parms p,                "
                + "         merchant m                                "
                + " where   m.app_seq_num = ?                         "
                + "         and m.annual_vmc_sales                    "
                + "           >= to_number(p.value)                   "
                + "         and p.name = 'high_volume_amount'         ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      logEntry("appIsHighVolume()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  /**
   * Returns true if the credit score of an app is under the low credit score
   * threshold defined in cbt_review_queue_parms.
   */
  private boolean appIsLowScore(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  p.name                          "
                + " from    cbt_credit_review_parms p,      "
                + "         merchant m                      "
                + " where   m.app_seq_num = ?               "
                + "         and cbt_credit_score            "
                + "           <= to_number(p.value)         "
                + "         and p.name = 'low_score_limit'  ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      logEntry("appIsLowScore()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  private void completePullCredit(long appSeqNum, UserBean user) throws Exception
  {
    moveQueueItem(appSeqNum,Q_PC_COMPLETED,CS_CLIENT_REVIEW,user,
      "completePullCredit");
    if (appIsLargeTicket(appSeqNum))
    {
      addLargeTicket(appSeqNum,user);
    }
    else if (appIsHighVolume(appSeqNum))
    {
      addHighVolume(appSeqNum,user);
    }
    else if (appIsLowScore(appSeqNum))
    {
      addLowScore(appSeqNum,user);
    }
    else
    {
      addDoc(appSeqNum,user);
    }
  }

  public static void doPullCreditAdd(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).addPullCredit(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doPullCreditCancel(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).cancelPullCredit(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doPullCreditComplete(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).completePullCredit(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /********************************************************************
   *
   * Large ticket ops
   *
   *******************************************************************/

  public static final int Q_LT_ITEM       = MesQueues.Q_ITEM_TYPE_CBT_CREDIT_REVIEW;
  public static final int Q_LT_NEW        = MesQueues.Q_CBT_LARGE_TICKET_NEW;
  public static final int Q_LT_APPROVED   = MesQueues.Q_CBT_LARGE_TICKET_APPROVED;
  public static final int Q_LT_CANCELED   = MesQueues.Q_CBT_LARGE_TICKET_CANCELED;
  public static final int Q_LT_DECLINED   = MesQueues.Q_CBT_LARGE_TICKET_DECLINED;
  public static final int Q_LT_PENDED     = MesQueues.Q_CBT_LARGE_TICKET_PENDED;

  private void addLargeTicket(long appSeqNum, UserBean user) throws Exception
  {
    insertQueueItem(appSeqNum,Q_LT_NEW,CS_CLIENT_REVIEW,user,"addLargeTicket");
  }

  private void cancelLargeTicket(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LT_APPROVED,"cancelLargeTicket");
    moveQueueItem(appSeqNum,Q_LT_CANCELED,CS_CLIENT_CANCELED,user,"cancelLargeTicket");
  }

  private void declineLargeTicket(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LT_APPROVED,"declineLargeTicket");
    moveQueueItem(appSeqNum,Q_LT_DECLINED,CS_CLIENT_DECLINED,user,"declineLargeTicket");
  }

  private void pendLargeTicket(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LT_APPROVED,"pendLargeTicket");
    moveQueueItem(appSeqNum,Q_LT_PENDED,CS_CLIENT_PENDED,user,"pendLargeTicket");
  }

  private void approveLargeTicket(long appSeqNum, UserBean user) throws Exception
  {
    moveQueueItem(appSeqNum,Q_LT_APPROVED,CS_CLIENT_REVIEW,user,"approveLargeTicket");
    addDoc(appSeqNum,user);
  }

  public static void doLargeTicketAdd(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).addLargeTicket(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doLargeTicketCancel(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).cancelLargeTicket(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doLargeTicketDecline(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).declineLargeTicket(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doLargeTicketPend(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).pendLargeTicket(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doLargeTicketApprove(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).approveLargeTicket(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /********************************************************************
   *
   * High volume ops
   *
   *******************************************************************/

  public static final int Q_HV_ITEM       = MesQueues.Q_ITEM_TYPE_CBT_CREDIT_REVIEW;
  public static final int Q_HV_NEW        = MesQueues.Q_CBT_HIGH_VOLUME_NEW;
  public static final int Q_HV_APPROVED   = MesQueues.Q_CBT_HIGH_VOLUME_APPROVED;
  public static final int Q_HV_CANCELED   = MesQueues.Q_CBT_HIGH_VOLUME_CANCELED;
  public static final int Q_HV_DECLINED   = MesQueues.Q_CBT_HIGH_VOLUME_DECLINED;
  public static final int Q_HV_PENDED     = MesQueues.Q_CBT_HIGH_VOLUME_PENDED;

  private void addHighVolume(long appSeqNum, UserBean user) throws Exception
  {
    insertQueueItem(appSeqNum,Q_HV_NEW,CS_CLIENT_REVIEW,user,"addHighVolume");
  }

  private void cancelHighVolume(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_HV_APPROVED,"cancelHighVolume");
    moveQueueItem(appSeqNum,Q_HV_CANCELED,CS_CLIENT_CANCELED,user,"cancelHighVolume");
  }

  private void declineHighVolume(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_HV_APPROVED,"declineHighVolume");
    moveQueueItem(appSeqNum,Q_HV_DECLINED,CS_CLIENT_DECLINED,user,"declineHighVolume");
  }

  private void pendHighVolume(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_HV_APPROVED,"pendHighVolume");
    moveQueueItem(appSeqNum,Q_HV_PENDED,CS_CLIENT_PENDED,user,"pendHighVolume");
  }

  private void approveHighVolume(long appSeqNum, UserBean user) throws Exception
  {
    moveQueueItem(appSeqNum,Q_HV_APPROVED,CS_CLIENT_REVIEW,user,"approveHighVolume");
    addDoc(appSeqNum,user);
  }

  public static void doHighVolumeAdd(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).addHighVolume(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doHighVolumeCancel(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).cancelHighVolume(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doHighVolumeDecline(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).declineHighVolume(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doHighVolumePend(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).pendHighVolume(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doHighVolumeApprove(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).approveHighVolume(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /********************************************************************
   *
   * Low score ops
   *
   *******************************************************************/

  public static final int Q_LS_ITEM       = MesQueues.Q_ITEM_TYPE_CBT_CREDIT_REVIEW;
  public static final int Q_LS_NEW        = MesQueues.Q_CBT_LOW_SCORE_NEW;
  public static final int Q_LS_APPROVED   = MesQueues.Q_CBT_LOW_SCORE_APPROVED;
  public static final int Q_LS_CANCELED   = MesQueues.Q_CBT_LOW_SCORE_CANCELED;
  public static final int Q_LS_DECLINED   = MesQueues.Q_CBT_LOW_SCORE_DECLINED;
  public static final int Q_LS_PENDED     = MesQueues.Q_CBT_LOW_SCORE_PENDED;

  private void addLowScore(long appSeqNum, UserBean user) throws Exception
  {
    insertQueueItem(appSeqNum,Q_LS_NEW,CS_CLIENT_REVIEW,user,"addLowScore");
  }

  private void cancelLowScore(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LS_APPROVED,"cancelLowScore");
    moveQueueItem(appSeqNum,Q_LS_CANCELED,CS_CLIENT_CANCELED,user,"cancelLowScore");
  }

  private void declineLowScore(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LS_APPROVED,"declineLowScore");
    moveQueueItem(appSeqNum,Q_LS_DECLINED,CS_CLIENT_DECLINED,user,"declineLowScore");
  }

  private void pendLowScore(long appSeqNum, UserBean user) throws Exception
  {
    validateNotInQueue(appSeqNum,Q_LS_APPROVED,"pendLowScore");
    moveQueueItem(appSeqNum,Q_LS_PENDED,CS_CLIENT_PENDED,user,"pendLowScore");
  }

  private void approveLowScore(long appSeqNum, UserBean user) throws Exception
  {
    moveQueueItem(appSeqNum,Q_LS_APPROVED,CS_CLIENT_REVIEW,user,"approveLowScore");
    addDoc(appSeqNum,user);
  }

  public static void doLowScoreAdd(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).addLowScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doLowScoreCancel(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).cancelLowScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doLowScoreDecline(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).declineLowScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doLowScorePend(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).pendLowScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doLowScoreApprove(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).approveLowScore(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  /********************************************************************
   *
   * Documentation ops
   *
   *******************************************************************/

  public static final int Q_DOC_ITEM      = MesQueues.Q_ITEM_TYPE_CBT_DOC;
  public static final int Q_DOC_NEW       = MesQueues.Q_CBT_DOC_NEW;
  public static final int Q_DOC_COMPLETED = MesQueues.Q_CBT_DOC_COMPLETED;
  public static final int Q_DOC_CANCELED  = MesQueues.Q_CBT_DOC_CANCELED;
  public static final int Q_DOC_PENDED    = MesQueues.Q_CBT_DOC_PENDED;

  /**
   * Returns true if the app is present in any of the documentation queues.
   */
  private boolean appInDocQueue(long appSeqNum) throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();
      
      String qs = " select  id                          "
                + " from    q_data                      "
                + " where   id = ?                      "
                + "         and type in ( ?, ?, ?, ? )  ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setInt(2,Q_DOC_NEW);
      ps.setInt(3,Q_DOC_COMPLETED);
      ps.setInt(4,Q_DOC_CANCELED);
      ps.setInt(5,Q_DOC_PENDED);

      rs = ps.executeQuery();
      return rs.next();
    }
    catch (Exception e)
    {
      logEntry("appIsLowScore()","appSeqNum " + appSeqNum + ": " + e);
      throw e;
    }
    finally
    {
      cleanUp(ps,rs);
    }
  }

  private void initDocData(long appSeqNum)
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      // clear out current set of doc data
      String qs = " delete from q_doc_data where id = ? ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.execute();
      ps.close();
      
      // setup a prepared statement to insert doc data
      qs  = " insert into q_doc_data  "
          + " ( id,                   "
          + "   doc_id,               "
          + "   doc_required,         "
          + "   doc_received )        "
          + " values                  "
          + " ( ?, ?, ?, ? )          ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setInt(2,MesQueues.QDI_MERCHANT_AGREEMENT);
      ps.setString(3,"Y");
      ps.setString(4,"N");
      ps.execute();
    }
    catch (Exception e)
    {
      logEntry("submitDocData()","appSeqNum " + appSeqNum + ": " + e);
    }
    finally
    {
      cleanUp(ps);
    }
  }

  private void addDoc(long appSeqNum, UserBean user) throws Exception
  {
    if (!appInDocQueue(appSeqNum))
    {
      insertQueueItem(appSeqNum,Q_DOC_NEW,CS_CLIENT_REVIEW,user,"addDoc");
      initDocData(appSeqNum);
    }
  }

  private void cancelDoc(long appSeqNum, UserBean user) throws Exception
  {
    String opName = "cancelDoc";
    validateInQueue(appSeqNum,Q_DOC_NEW,opName);
    moveQueueItem(appSeqNum,Q_DOC_CANCELED,CS_CLIENT_CANCELED,user,opName);
  }

  private void uncancelDoc(long appSeqNum, UserBean user) throws Exception
  {
    String opName = "uncancelDoc";
    validateInQueue(appSeqNum,Q_DOC_CANCELED,opName);
    moveQueueItem(appSeqNum,Q_DOC_NEW,CS_CLIENT_REVIEW,user,opName);
  }

  /**
   * "Pends" an app in the doc queues.  Apps in the pending queue are considered
   * complete for the purposes of MES (currently items will be pended if they
   * have received all doc items except onsite inspection), so when pending
   * the app is also placed into MES credit queues for MES setup.
   */
  private void pendDoc(long appSeqNum, UserBean user) throws Exception
  {
    String opName = "pendDoc";
    validateNotInQueue(appSeqNum,Q_DOC_PENDED,opName);
    validateNotInQueue(appSeqNum,Q_DOC_COMPLETED,opName);
    moveQueueItem(appSeqNum,Q_DOC_PENDED,CS_CLIENT_REVIEW,user,opName);

    // move into mes credit queues...
    addMes(appSeqNum,user);
  }

  /**
   * Moves documentation app into doc completed queue.  If app was not previously
   * pended then the app must also be passed to MES for account setup and have it's
   * status updated.
   */
  private void completeDoc(long appSeqNum, UserBean user) throws Exception
  {
    String opName = "completeDoc";
    validateNotInQueue(appSeqNum,Q_DOC_COMPLETED,opName);
    if (QueueTools.getCurrentQueueType(appSeqNum,Q_DOC_ITEM) == Q_DOC_PENDED)
    {
      moveQueueItem(appSeqNum,Q_DOC_COMPLETED,CS_NO_CHANGE,user,opName);
    }
    else
    {
      moveQueueItem(appSeqNum,Q_DOC_COMPLETED,CS_CLIENT_REVIEW,user,opName);
      addMes(appSeqNum,user);
    }
  }
  
  public static void doDocAdd(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).addDoc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doDocCancel(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).cancelDoc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doDocUncancel(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).uncancelDoc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doDocPend(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).pendDoc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }

  public static void doDocComplete(long appSeqNum, UserBean user)
  {
    try
    {
      (new ReviewQueueOps()).completeDoc(appSeqNum,user);
    }
    catch (Exception e)
    {
      throw new RuntimeException("" + e);
    }
  }
}