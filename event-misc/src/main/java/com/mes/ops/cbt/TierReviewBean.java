package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;

public class TierReviewBean extends BaseItemBean
{
  static Logger log = Logger.getLogger(TierReviewBean.class);

  // queue categories
  public static final int     QCAT_LOW_SCORE      = 1;
  public static final int     QCAT_HIGH_VOLUME    = 2;
  public static final int     QCAT_LARGE_TICKET   = 3;
  public static final int     QCAT_TIER_34        = 4;
  public static final int     QCAT_HIGH_VOLUME_34 = 5;
  public static final int     QCAT_FINANCIAL      = 6;

  // reason types
  public static final String  RT_NONE             = "none";
  public static final String  RT_APPROVE          = "approve";
  public static final String  RT_PEND             = "pend";
  public static final String  RT_CANCEL           = "cancel";
  public static final String  RT_DECLINE          = "decline";
  public static final String  RT_REFER            = "refer";

  private String getReasonGroupName(String reasonType)
  {
    return "fg" + reasonType;
  }

  private String getReasonFieldName(String reasonType, String code)
  {
    return reasonType + "_" + code;
  }

  private int getReasonCode(String reasonType)
  {
    if (reasonType.equals(RT_APPROVE))
    {
      return 4;
    }
    else if (reasonType.equals(RT_PEND))
    {
      return 2;
    }
    else if (reasonType.equals(RT_CANCEL))
    {
      return 3;
    }
    else if (reasonType.equals(RT_DECLINE))
    {
      return 1;
    }

    throw new RuntimeException("Invalid reason type: " + reasonType);
  }

  private void createCreditReasonFields(String reasonType)
  {
    PreparedStatement ps = null;
    ResultSet rs  = null;

    try
    {
      connect();

      String qs = " select  code, label       "
                + " from    q_credit_reasons  "
                + " where   type = ?          ";

      ps = con.prepareStatement(qs);
      ps.setInt(1,getReasonCode(reasonType));

      // generate a checkbox field for each reason
      rs = ps.executeQuery();
      FieldGroup reasonGroup = new FieldGroup(getReasonGroupName(reasonType));
      while(rs.next())
      {
        String code = rs.getString("code");
        String label = rs.getString("label");
        String fieldName = getReasonFieldName(reasonType,code);
        Field reason = new CheckboxField(fieldName,label,false);
        reasonGroup.add(reason);
      }
      fields.add(reasonGroup);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".createCreditReasonFields(" 
        + reasonType + "): " + e);
      logEntry("createCreditReasonFields(" + reasonType + ")",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      createCreditReasonFields(RT_DECLINE);
      createCreditReasonFields(RT_PEND);
      createCreditReasonFields(RT_CANCEL);

      fields.add(new ButtonField("approve"));
      fields.add(new ButtonField("pend"));
      fields.add(new ButtonField("decline"));
      fields.add(new ButtonField("cancel"));
      fields.add(new ButtonField("refer"));

      fields.setGroupHtmlExtra("class=\"smallText\"");
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".createFields("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("createFields(appSeqNum=" + appSeqNum + ")",e.toString());
    }
  }

  private String makeCodeString(String reasonType)
  {
    StringBuffer codeString = new StringBuffer();
    try
    {
      if ( reasonType.equals(RT_APPROVE) || reasonType.equals(RT_REFER) )
      {
        return "";
      }

      String groupName = getReasonGroupName(reasonType);
      for (Iterator i = getGroupIterator(groupName); i.hasNext(); )
      {
        Field field = (Field)i.next();
        if (field instanceof CheckboxField
            && ((CheckboxField)field).isChecked())
        {
          if (codeString.length() > 0)
          {
            codeString.append(",");
          }
          String name = field.getName();
          String code = name.substring(name.indexOf('_') + 1);
          codeString.append(code);
        }
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::makeCodeString(" + reasonType + "): " + e);
      logEntry("makeCodeString(" + reasonType + ")",e.toString());
    }
    return codeString.toString();
  }

  private void resetReasonGroup(String reasonType)
  {
    FieldGroup fg = (FieldGroup)fields.getField(getReasonGroupName(reasonType));
    if (fg != null)
    {
      fg.reset();
    }
  }

  private void resetIrrelevantReasons(String reasonType)
  {
    if (!reasonType.equals(RT_DECLINE))
    {
      resetReasonGroup(RT_DECLINE);
    }
    if (!reasonType.equals(RT_PEND))
    {
      resetReasonGroup(RT_PEND);
    }
    if (!reasonType.equals(RT_CANCEL))
    {
      resetReasonGroup(RT_CANCEL);
    }
  }

  private void submitReasonCodes(String reasonType) throws Exception
  {
    PreparedStatement ps = null;
    String qs = null;

    try
    {
      connect();

      String codeString = makeCodeString(reasonType);

      qs =  " delete from q_credit_data where id = ? ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.execute();
      ps.close();

      qs = " insert into q_credit_data ( id, codes ) values ( ?, ? ) ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      ps.setString(2,codeString);
      ps.execute();

      resetIrrelevantReasons(reasonType);
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  private int getQueueCategory()
  {
    int queueType = fields.getField("type").asInteger();
    switch (queueType)
    {
      case TierReviewQueueOps.Q_LOW_SCORE_NEW:
      case TierReviewQueueOps.Q_LOW_SCORE_PENDED:
      case TierReviewQueueOps.Q_LOW_SCORE_APPROVED:
      case TierReviewQueueOps.Q_LOW_SCORE_CANCELED:
      case TierReviewQueueOps.Q_LOW_SCORE_DECLINED:
        return QCAT_LOW_SCORE;

      case TierReviewQueueOps.Q_HIGH_VOLUME_NEW:
      case TierReviewQueueOps.Q_HIGH_VOLUME_PENDED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_APPROVED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_CANCELED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_DECLINED:
        return QCAT_HIGH_VOLUME;

      case TierReviewQueueOps.Q_LARGE_TKT_NEW:
      case TierReviewQueueOps.Q_LARGE_TKT_PENDED:
      case TierReviewQueueOps.Q_LARGE_TKT_APPROVED:
      case TierReviewQueueOps.Q_LARGE_TKT_CANCELED:
      case TierReviewQueueOps.Q_LARGE_TKT_DECLINED:
        return QCAT_LARGE_TICKET;

      case TierReviewQueueOps.Q_TIER_34_NEW:
      case TierReviewQueueOps.Q_TIER_34_PENDED:
      case TierReviewQueueOps.Q_TIER_34_APPROVED:
      case TierReviewQueueOps.Q_TIER_34_CANCELED:
      case TierReviewQueueOps.Q_TIER_34_DECLINED:
        return QCAT_TIER_34;

      case TierReviewQueueOps.Q_HIGH_VOLUME_34_NEW:
      case TierReviewQueueOps.Q_HIGH_VOLUME_34_PENDED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_34_APPROVED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_34_CANCELED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_34_DECLINED:
        return QCAT_HIGH_VOLUME_34;

      case TierReviewQueueOps.Q_FINANCIAL_NEW:
      case TierReviewQueueOps.Q_FINANCIAL_PENDED:
      case TierReviewQueueOps.Q_FINANCIAL_APPROVED:
      case TierReviewQueueOps.Q_FINANCIAL_CANCELED:
      case TierReviewQueueOps.Q_FINANCIAL_DECLINED:
        return QCAT_FINANCIAL;
    }
    throw new RuntimeException("Unsupported queueType (" + queueType + ")");
  }

  private void doPend()
  {
    switch (getQueueCategory())
    {
      case QCAT_LOW_SCORE:
        TierReviewQueueOps.doPendLowScore(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_LOW_SCORE_PENDED);
        break;

      case QCAT_HIGH_VOLUME:
        TierReviewQueueOps.doPendHighVolume(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_HIGH_VOLUME_PENDED);
        break;

      case QCAT_LARGE_TICKET:
        TierReviewQueueOps.doPendLargeTicket(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_LARGE_TKT_PENDED);
        break;

      case QCAT_TIER_34:
        TierReviewQueueOps.doPendTier34(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_TIER_34_PENDED);
        break;

      case QCAT_HIGH_VOLUME_34:
        TierReviewQueueOps.doPendHighVolume34(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_HIGH_VOLUME_34_PENDED);
        break;

      case QCAT_FINANCIAL:
        TierReviewQueueOps.doPendFinancial(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_FINANCIAL_PENDED);
        break;
    }
  }

  private void doApprove()
  {
    switch (getQueueCategory())
    {
      case QCAT_LOW_SCORE:
        TierReviewQueueOps.doApproveLowScore(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_LOW_SCORE_APPROVED);
        break;

      case QCAT_HIGH_VOLUME:
        TierReviewQueueOps.doApproveHighVolume(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_HIGH_VOLUME_APPROVED);
        break;

      case QCAT_LARGE_TICKET:
        TierReviewQueueOps.doApproveLargeTicket(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_LARGE_TKT_APPROVED);
        break;

      case QCAT_TIER_34:
        TierReviewQueueOps.doApproveTier34(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_TIER_34_APPROVED);
        break;

      case QCAT_HIGH_VOLUME_34:
        TierReviewQueueOps.doApproveHighVolume34(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_HIGH_VOLUME_34_APPROVED);
        break;

      case QCAT_FINANCIAL:
        TierReviewQueueOps.doApproveFinancial(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_FINANCIAL_APPROVED);
        break;
    }
  }

  private void doCancel()
  {
    switch (getQueueCategory())
    {
      case QCAT_LOW_SCORE:
        TierReviewQueueOps.doCancelLowScore(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_LOW_SCORE_CANCELED);
        break;

      case QCAT_HIGH_VOLUME:
        TierReviewQueueOps.doCancelHighVolume(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_HIGH_VOLUME_CANCELED);
        break;

      case QCAT_LARGE_TICKET:
        TierReviewQueueOps.doCancelLargeTicket(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_LARGE_TKT_CANCELED);
        break;

      case QCAT_TIER_34:
        TierReviewQueueOps.doCancelTier34(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_TIER_34_CANCELED);
        break;

      case QCAT_HIGH_VOLUME_34:
        TierReviewQueueOps.doCancelHighVolume34(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_HIGH_VOLUME_34_CANCELED);
        break;

      case QCAT_FINANCIAL:
        TierReviewQueueOps.doCancelFinancial(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_FINANCIAL_CANCELED);
        break;
    }
  }

  private void doDecline()
  {
    switch (getQueueCategory())
    {
      case QCAT_LOW_SCORE:
        TierReviewQueueOps.doDeclineLowScore(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_LOW_SCORE_DECLINED);
        break;

      case QCAT_HIGH_VOLUME:
        TierReviewQueueOps.doDeclineHighVolume(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_HIGH_VOLUME_DECLINED);
        break;

      case QCAT_LARGE_TICKET:
        TierReviewQueueOps.doDeclineLargeTicket(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_LARGE_TKT_DECLINED);
        break;

      case QCAT_TIER_34:
        TierReviewQueueOps.doDeclineTier34(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_TIER_34_DECLINED);
        break;

      case QCAT_HIGH_VOLUME_34:
        TierReviewQueueOps.doDeclineHighVolume34(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_HIGH_VOLUME_34_DECLINED);
        break;

      case QCAT_FINANCIAL:
        TierReviewQueueOps.doDeclineFinancial(appSeqNum,user);
        setNewQueue(TierReviewQueueOps.Q_FINANCIAL_DECLINED);
        break;
    }
  }
  
  private void doRefer()
  {
    PreparedStatement ps = null;
    try
    {
      connect();
      ps = con.prepareStatement("call convert_cbt_application(?)");
      
      ps.setLong(1, appSeqNum);
      
      ps.execute();
      
      switch( getQueueCategory() )
      {
        case QCAT_LOW_SCORE:
          setNewQueue(TierReviewQueueOps.Q_LOW_SCORE_CANCELED);
          break;

        case QCAT_HIGH_VOLUME:
          setNewQueue(TierReviewQueueOps.Q_HIGH_VOLUME_CANCELED);
          break;

        case QCAT_LARGE_TICKET:
          setNewQueue(TierReviewQueueOps.Q_LARGE_TKT_CANCELED);
          break;

        case QCAT_TIER_34:
          setNewQueue(TierReviewQueueOps.Q_TIER_34_CANCELED);
          break;

        case QCAT_HIGH_VOLUME_34:
          setNewQueue(TierReviewQueueOps.Q_HIGH_VOLUME_34_CANCELED);
          break;

        case QCAT_FINANCIAL:
          setNewQueue(TierReviewQueueOps.Q_FINANCIAL_CANCELED);
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("doRefer(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { ps.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public boolean autoSubmit()
  {
    try
    {
      if (autoSubmitName.equals("approve"))
      {
        submitReasonCodes(RT_APPROVE);
        doApprove();
      }
      else if (autoSubmitName.equals("pend"))
      {
        submitReasonCodes(RT_PEND);
        doPend();
      }
      else if (autoSubmitName.equals("cancel"))
      {
        submitReasonCodes(RT_CANCEL);
        doCancel();
      }
      else if (autoSubmitName.equals("decline"))
      {
        submitReasonCodes(RT_DECLINE);
        doDecline();
      }
      else if (autoSubmitName.equals("refer"))
      {
        submitReasonCodes(RT_REFER);
        doRefer();
      }
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoSubmit("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoSubmit(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    return false;
  }

  private String getQueueReasonType()
  {
    int queueType = fields.getField("type").asInteger();
    switch (queueType)
    {
      case TierReviewQueueOps.Q_LOW_SCORE_NEW:
      case TierReviewQueueOps.Q_HIGH_VOLUME_NEW:
      case TierReviewQueueOps.Q_LARGE_TKT_NEW:
      case TierReviewQueueOps.Q_TIER_34_NEW:
      case TierReviewQueueOps.Q_HIGH_VOLUME_34_NEW:
      case TierReviewQueueOps.Q_FINANCIAL_NEW:
        return RT_NONE;

      case TierReviewQueueOps.Q_LOW_SCORE_PENDED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_PENDED:
      case TierReviewQueueOps.Q_LARGE_TKT_PENDED:
      case TierReviewQueueOps.Q_TIER_34_PENDED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_34_PENDED:
      case TierReviewQueueOps.Q_FINANCIAL_PENDED:
        return RT_PEND;

      case TierReviewQueueOps.Q_LOW_SCORE_APPROVED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_APPROVED:
      case TierReviewQueueOps.Q_LARGE_TKT_APPROVED:
      case TierReviewQueueOps.Q_TIER_34_APPROVED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_34_APPROVED:
      case TierReviewQueueOps.Q_FINANCIAL_APPROVED:
        return RT_APPROVE;

      case TierReviewQueueOps.Q_LOW_SCORE_CANCELED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_CANCELED:
      case TierReviewQueueOps.Q_LARGE_TKT_CANCELED:
      case TierReviewQueueOps.Q_TIER_34_CANCELED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_34_CANCELED:
      case TierReviewQueueOps.Q_FINANCIAL_CANCELED:
        return RT_CANCEL;

      case TierReviewQueueOps.Q_LOW_SCORE_DECLINED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_DECLINED:
      case TierReviewQueueOps.Q_LARGE_TKT_DECLINED:
      case TierReviewQueueOps.Q_TIER_34_DECLINED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_34_DECLINED:
      case TierReviewQueueOps.Q_FINANCIAL_DECLINED:
        return RT_DECLINE;
    }
    throw new RuntimeException("Invalid queue type: " + queueType);
  }

  public void loadReasonCodes() throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select nvl( codes, 'none' ) from q_credit_data where id = ? ";
      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      if (rs.next())
      {
        String codes = rs.getString(1);
        if (codes == null || codes.length() == 0 || codes.equals("none")) return;

        String reasonType = getQueueReasonType();
        if (reasonType.equals(RT_NONE)) return;

        StringTokenizer tok = new StringTokenizer(codes,",");
        while (tok.hasMoreTokens())
        {
          String code = tok.nextToken();
          Field field = fields.getField(getReasonFieldName(reasonType,code));
          if (field != null)
          {
            field.setData("y");
          }
        }
      }
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  public boolean autoLoad()
  {
    try
    {
      loadReasonCodes();
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoLoad("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoLoad(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    return false;
  }

  public Iterator getReasonGroupIterator(String reasonType)
  {
    return getGroupIterator(getReasonGroupName(reasonType));
  }

  public boolean isApproved()
  {
    int queueType = fields.getField("type").asInteger();
    switch (queueType)
    {
      case TierReviewQueueOps.Q_LOW_SCORE_APPROVED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_APPROVED:
      case TierReviewQueueOps.Q_LARGE_TKT_APPROVED:
      case TierReviewQueueOps.Q_TIER_34_APPROVED:
      case TierReviewQueueOps.Q_HIGH_VOLUME_34_APPROVED:
      case TierReviewQueueOps.Q_FINANCIAL_APPROVED:
        return true;
    }
    return false;
  }
}
