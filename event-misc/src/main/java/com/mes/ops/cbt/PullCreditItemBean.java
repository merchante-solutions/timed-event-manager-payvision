package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.NumberField;

public class PullCreditItemBean extends BaseItemBean
{
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new NumberField("creditScore","Credit Score",4,6,true,0));

      fields.add(new ButtonField("submit"));
      fields.add(new ButtonField("cancel"));
    
      fields.setHtmlExtra("class=\"formText\"");
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + ".createFields("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("createFields(appSeqNum=" + appSeqNum + ")",e.toString());
    }
  }

  /**
   * Submits app data to merchant table.
   */
  private void submitCreditScore() throws Exception
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      // update merchant table data with assoc num and sic code
      String qs = " update  merchant              "
                + " set     cbt_credit_score = ?  "
                + " where   app_seq_num   = ?     ";

      ps = con.prepareStatement(qs);
      ps.setInt(1,fields.getField("creditScore").asInteger());
      ps.setLong(2,appSeqNum);
      ps.execute();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  /**
   * Handles submits from jsp.  Can be triggered by a cancel button (store data
   * and move item to canceled queue) or a submit button (store data and if data
   * is complete move to completed queue, create new queue item in either a
   * the high risk sic queue or the pull credit queue).
   *
   * Returns true if no errors occur.
   */
  public boolean autoSubmit()
  {
    try
    {
      submitCreditScore();

      if (!fields.getField("cancel").isBlank())
      {
        ReviewQueueOps.doPullCreditCancel(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_PC_CANCELED);
      }
      else if (fields.getField("creditScore").asInteger() > 0)
      {
        ReviewQueueOps.doPullCreditComplete(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_PC_COMPLETED);
      }

      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "autoSubmit("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoSubmit(appSeqNum=" + appSeqNum + ")",e.toString());
    }

    return false;
  }

  /**
   * Load app data from merchant table.
   */
  private void loadCreditScore() throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  cbt_credit_score credit_score "
                + " from    merchant                      "
                + " where   app_seq_num = ?               ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      setFields(rs);
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** public boolean autoLoad()
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean autoLoad()
  {
    try
    {
      loadCreditScore();
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoLoad("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoLoad(appSeqNum=" + appSeqNum + ")",e.toString());
    }

    return false;
  }
}
