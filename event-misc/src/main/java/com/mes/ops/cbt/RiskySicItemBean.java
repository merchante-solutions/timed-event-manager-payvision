package com.mes.ops.cbt;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;

public class RiskySicItemBean extends BaseItemBean
{
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField("riskySic",  "Risky SIC"));
      fields.add(new NumberField("sicCode",   "SIC",4,6,true,0));

      fields.add(new ButtonField("approve"));
      fields.add(new ButtonField("decline"));
      fields.add(new ButtonField("cancel"));
    
      fields.setHtmlExtra("class=\"formText\"");
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + ".createFields("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("createFields(appSeqNum=" + appSeqNum + ")",e.toString());
    }
  }

  /**
   * Submits sic data to the merchant table.
   */
  private void submitSicData() throws Exception
  {
    PreparedStatement ps = null;

    try
    {
      connect();

      // update merchant table data with assoc num and sic code
      String qs = " update  merchant            "
                + " set     app_sic_code  = ?   "
                + " where   app_seq_num   = ?   ";

      ps = con.prepareStatement(qs);
      ps.setInt(1,fields.getField("sicCode").asInteger());
      ps.setLong(2,appSeqNum);
      ps.execute();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  /**
   * Handles submits from jsp.  Can be triggered by a cancel button (store data
   * and move item to canceled queue) or a submit button (store data and if data
   * is complete move to completed queue, create new queue item in either a
   * the high risk sic queue or the pull credit queue).
   *
   * Returns true if no errors occur.
   */
  public boolean autoSubmit()
  {
    try
    {
      submitSicData();
      if (autoSubmitName.equals("cancel"))
      {
        ReviewQueueOps.doRiskySicCancel(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_RS_CANCELED);
      }
      else if (autoSubmitName.equals("decline"))
      {
        ReviewQueueOps.doRiskySicDecline(appSeqNum,user);
        setNewQueue(ReviewQueueOps.Q_RS_DECLINED);
      }
      else if (autoSubmitName.equals("approve"))
      {
        if (fields.getField("sicCode").asInteger() > 0)
        {
          ReviewQueueOps.doRiskySicApprove(appSeqNum,user);
          setNewQueue(ReviewQueueOps.Q_RS_APPROVED);
        }
      }
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoSubmit("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoSubmit(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    return false;
  }

  /**
   * Loads sic info from merchant and cbt_risky_sic_app tables.  If this is
   * first time coming to this queue item, insert an entry for the item's app
   * in cbt_risky_sic_app.
   */
  private void loadSicData() throws Exception
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();

      String qs = " select  m.app_sic_code      sic_code,     " 
                + "         r.sic               risky_sic     " 
                + " from    merchant            m,            " 
                + "         cbt_risky_sic_apps  r             " 
                + " where   m.app_seq_num = r.app_seq_num(+)  " 
                + "         and m.app_seq_num = ?             ";

      ps = con.prepareStatement(qs);
      ps.setLong(1,appSeqNum);
      rs = ps.executeQuery();
      setFields(rs);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) { }
      try { ps.close(); } catch (Exception e) { }
      cleanUp();
    }
  }

  /*
  ** public boolean autoLoad()
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean autoLoad()
  {
    try
    {
      loadSicData();
      return true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + ".autoLoad("
        + "appSeqNum=" + appSeqNum + "): " + e);
      logEntry("autoLoad(appSeqNum=" + appSeqNum + ")",e.toString());
    }
    return false;
  }
}
