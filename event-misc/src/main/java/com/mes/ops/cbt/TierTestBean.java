package com.mes.ops.cbt;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.NumberField;
import com.mes.tools.DropDownTable;

public class TierTestBean extends FieldBean
{
  static Logger log = Logger.getLogger(TierTestBean.class);

  public static final String FN_NAME      = "name";
  public static final String FN_BUS_TYPE  = "busType";
  public static final String FN_VOLUME    = "volume";
  public static final String FN_TICKET    = "ticket";
  public static final String FN_SCORE     = "score";
  public static final String FN_SUBMIT    = "submit";

  protected class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Sole Proprietorship");
      addElement("2","Corporation");
      addElement("3","Partnership");
      addElement("4","Medical or Legal Corporation");
      addElement("5","Association/Estate/Trust");
      addElement("6","Tax Exempt");
      addElement("7","Government");
      addElement("8","Limited Liability Company");
    }
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new Field(FN_NAME,"Merchant Name",30,30,false));
      fields.add(new DropDownField(FN_BUS_TYPE,"Type of Business",new BusinessTypeTable(),false));
      fields.add(new NumberField(FN_VOLUME,"Annual Volume",9,6,false,2));
      fields.add(new NumberField(FN_TICKET,"Average Ticket",9,6,false,2));
      fields.add(new NumberField(FN_SCORE,"Credit Score",4,6,true,0));
      fields.add(new ButtonField(FN_SUBMIT));
    
      fields.setHtmlExtra("class=\"formText\"");
      fields.setShowErrorText(true);
      //fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      log.error("Error creating fields: " + e);
    }
  }

  public boolean autoSubmit()
  {
    try
    {
      String name = getData(FN_NAME);
      int busType = getField(FN_BUS_TYPE).asInt();
      double volume = getField(FN_VOLUME).asDouble();
      double ticket = getField(FN_TICKET).asDouble();
      int score = getField(FN_SCORE).asInt();

      TierTestQueueOps.doTestApp(user,name,busType,volume,ticket,score);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error in autoSubmit(): " + e);
    }

    return false;
  }
}
