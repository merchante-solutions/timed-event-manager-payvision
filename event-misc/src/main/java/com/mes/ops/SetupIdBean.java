/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/SetupIdBean.java $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 11/13/02 9:23a $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Vector;
import com.mes.database.DBConnect;
import com.mes.screens.ScreenInfo;
import com.mes.screens.ScreenManager;

public class SetupIdBean
{
  // constants
  public final static long    SCREEN_GENERAL_INFO           = 0x0001L;
  public final static long    SCREEN_INDIVIDUAL_MERCHANT    = 0x0002L;
  public final static long    SCREEN_MERCHANT_DISCOUNT      = 0x0004L;
  public final static long    SCREEN_BET_TABLE              = 0x0008L;
  public final static long    SCREEN_DDA_CHARGE             = 0x0010L;
  public final static long    SCREEN_OTHER_CARD             = 0x0020L;

  public final static String  PAGE_GENERAL_INFO             = "mes_expand_general.jsp";
  public final static String  PAGE_INDIVIDUAL_MERCHANT      = "mes_expand_merchant.jsp";
  public final static String  PAGE_MERCHANT_DISCOUNT        = "mes_expand_discount.jsp";
  public final static String  PAGE_BET_TABLE                = "mes_expand_bet.jsp";
  public final static String  PAGE_DDA_CHARGE               = "mes_expand_charge.jsp";
  public final static String  PAGE_OTHER_CARD               = "mes_expand_card.jsp";

  public final static String  DESC_GENERAL_INFO             = "Merchant General Information";
  public final static String  DESC_INDIVIDUAL_MERCHANT      = "Individual Merchant Options";
  public final static String  DESC_MERCHANT_DISCOUNT        = "Merchant Discount Options";
  public final static String  DESC_BET_TABLE                = "BET Information";
  public final static String  DESC_DDA_CHARGE               = "DDAs and Charge Records";
  public final static String  DESC_OTHER_CARD               = "Other Card Type Merchant Numbers";

  // data members
  private long                appSeqNum                     = 0L;
  private long                appControlNum                 = 0L;
  private String              merchantName                  = "";
  private long                merchantNumber                = 0L;
  private long                completedScreens              = 0;
  private String              submittedDate                 = "";

  private SimpleDateFormat    simpleDate                    = new SimpleDateFormat("MM/dd/yyyy");
  private ScreenManager       setupScreens                  = new ScreenManager();

  /*
  ** METHOD init
  */
  private void init()
  {
    // reset values
    appSeqNum       = 0L;
    appControlNum   = 0L;
    merchantName    = "";
    merchantNumber  = 0L;
  }

  /*
  ** CONSTRUCTOR SetupIdBean
  */
  public SetupIdBean()
  {
    init();
  }

  /*
  ** METHOD loadScreens
  */
  private void loadScreens()
  {
    String      path      = "/jsp/credit/";

    setupScreens.clear();

    // add screens for data expansion
    setupScreens.addScreen("com.mes.ops.GeneralInfoBean",
                            SCREEN_GENERAL_INFO,
                            path + PAGE_GENERAL_INFO,
                            false,
                            DESC_GENERAL_INFO);

    setupScreens.addScreen("com.mes.ops.IndividualMerchantBean",
                            SCREEN_INDIVIDUAL_MERCHANT,
                            path + PAGE_INDIVIDUAL_MERCHANT,
                            false,
                            DESC_INDIVIDUAL_MERCHANT);

    setupScreens.addScreen("com.mes.ops.MerchantDiscountBean",
                            SCREEN_MERCHANT_DISCOUNT,
                            path + PAGE_MERCHANT_DISCOUNT,
                            false,
                            DESC_MERCHANT_DISCOUNT);

    setupScreens.addScreen("com.mes.ops.BETTableBean",
                            SCREEN_BET_TABLE,
                            path + PAGE_BET_TABLE,
                            false,
                            DESC_BET_TABLE);

    setupScreens.addScreen("com.mes.ops.DDAChargeBean",
                            SCREEN_DDA_CHARGE,
                            path + PAGE_DDA_CHARGE,
                            false,
                            DESC_DDA_CHARGE);

    setupScreens.addScreen("com.mes.ops.OtherCardBean",
                            SCREEN_OTHER_CARD,
                            path + PAGE_OTHER_CARD,
                            false,
                            DESC_OTHER_CARD);

    // scan list, update visited flags based on last screen visited
    for(int i=0; i < setupScreens.size(); ++i)
    {
      ScreenInfo s = setupScreens.screenAt(i);
      if((completedScreens & s.getScreenId()) > 0)
      {
        s.setCompleted(true);
      }
    }
  }

  /*
  ** METHOD getSetupDataBeans
  */
  public Vector getSetupDataBeans()
  {
    ScreenInfo    screen;
    Vector        beans   = new Vector();

    try
    {
      for(int i=0; i < setupScreens.size(); ++i)
      {
        screen = setupScreens.screenAt(i);
        if(screen.getBeanClass() != null)
        {
          beans.add(screen.newBeanInstance());
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getSetupDataBeans: " + e.toString());
    }

    return beans;
  }

  /*
  ** METHOD getData
  */
  public void getData(long appSeqNum)
  {
    DBConnect           db      = new DBConnect(this, "getData");
    Connection          con     = null;
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    StringBuffer        qs      = new StringBuffer("");

    try
    {
      init();

      qs.append("select ");
      qs.append("app_seq_num, ");
      qs.append("app_control_num, ");
      qs.append("merchant_number, ");
      qs.append("merchant_name, ");
      qs.append("screens_complete, ");
      qs.append("app_date ");
      qs.append("from merchant_data ");
      qs.append("where app_seq_num = ?");

      con = db.getConnection("pool;ApplicationPool");
      ps  = con.prepareStatement(qs.toString());

      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.appSeqNum        = rs.getLong  ("app_seq_num");
        this.appControlNum    = rs.getLong  ("app_control_num");
        this.merchantNumber   = rs.getLong  ("merchant_number");
        this.merchantName     = rs.getString("merchant_name");
        this.completedScreens = rs.getLong  ("screens_complete");
        this.submittedDate    = rs.getString("app_date");
      }
      else
      {
        // we need to add this data to the database
        qs.setLength(0);
        rs.close();
        ps.close();

        qs.append("select ");
        qs.append("merch.app_seq_num, ");
        qs.append("merch.merch_number, ");
        qs.append("merch.merc_cntrl_number, ");
        qs.append("merch.merch_business_name, ");
        qs.append("app.app_created_date ");
        qs.append("from merchant merch, application app ");
        qs.append("where merch.app_seq_num = ?");

        ps = con.prepareStatement(qs.toString());
        ps.setLong(1, appSeqNum);

        rs = ps.executeQuery();
        if(rs.next())
        {
          this.appSeqNum      = rs.getLong  ("app_seq_num");
          this.appControlNum  = rs.getLong  ("merc_cntrl_number");
          this.merchantNumber = rs.getLong  ("merch_number");
          this.merchantName   = rs.getString("merch_business_name");
          this.submittedDate  = simpleDate.format(rs.getDate("app_created_date"));

          rs.close();
          ps.close();

          qs.setLength(0);
          qs.append("insert into merchant_data (");
          qs.append("app_seq_num, ");
          qs.append("app_control_num, ");
          qs.append("merchant_number, ");
          qs.append("merchant_name, ");
          qs.append("screens_complete, ");
          qs.append("app_date) ");
          qs.append("values (?, ?, ?, ?, ?, ?)");

          ps = con.prepareStatement(qs.toString());
          ps.setLong  (1, this.appSeqNum);
          ps.setLong  (2, this.appControlNum);
          ps.setLong  (3, this.merchantNumber);
          ps.setString(4, this.merchantName);
          ps.setLong  (5, 0L);
          ps.setString(6, this.submittedDate);

          if(ps.executeUpdate() != 1)
          {
            com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: unable to add to merchant_data");
          }

          ps.close();
          rs.close();
        }
        else
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: unable to find application");
        }
      }

      loadScreens();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
    }
    finally
    {
      if(db != null)
      {
        try
        {
          db.releaseConnection();
        }
        catch(Exception e)
        {
        }
      }
    }
  }

  /*
  ** METHOD submitData
  */
  public void submitData(long screenId)
  {
    DBConnect           db      = new DBConnect(this, "submitData");
    Connection          con     = null;
    PreparedStatement   ps      = null;
    StringBuffer        qs      = new StringBuffer("");

    try
    {
      // get connection
      con = db.getConnection("pool;ApplicationPool");

      qs.append("update merchant_data ");
      qs.append("set screens_complete = ? ");
      qs.append("where app_seq_num = ?");

      ps = con.prepareStatement(qs.toString());
      ps.setLong(1, (screenId | completedScreens));
      ps.setLong(2, appSeqNum);

      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: can't update merchant_data");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
    }
    finally
    {
      if(db != null)
      {
        try
        {
          db.releaseConnection();
        }
        catch(Exception e)
        {
        }
      }
    }
  }

  /*
  ** METHOD hasPreviousPage
  */
  public boolean hasPreviousPage(long fromScreenId)
  {
    return setupScreens.hasPreviousPage(fromScreenId);
  }

  /*
  ** METHOD getPreviousPage
  */
  public String getPreviousPage(long fromScreenId)
  {
    return setupScreens.getPreviousPage(fromScreenId);
  }

  /*
  ** METHOD hasNextPage
  */
  public boolean hasNextPage(long fromScreenId, boolean ignoreComplete)
  {
    return setupScreens.hasNextPage(fromScreenId, ignoreComplete);
  }

  /*
  ** METHOD getNextPage
  */
  public String getNextPage(long fromScreenId)
  {
    return setupScreens.getNextPage(fromScreenId);
  }

  /*
  ** METHOD getPageDescription
  */
  public String getPageDescription(long screenId)
  {
    return setupScreens.getPageDescription(screenId);
  }

  /*
  ** METHOD getScreens
  */
  public Vector getScreens()
  {
    return setupScreens.getScreens();
  }

  /*
  ** METHOD getCurPage
  */
  public String getCurPage(long appSeqNum)
  {
    // fill bean with data from database
    getData(appSeqNum);

    // determine the first page that isn't completed
    long curScreen = 1L;

    while(curScreen < ScreenInfo.MAX_SCREENS)
    {
      if((curScreen & completedScreens) == 0)
      {
        break;
      }

      curScreen = curScreen << 1;
    }

    return(setupScreens.getNextPage(curScreen));
  }

  public void setAppSeqNum(String appSeqNum)
  {
    try
    {
      this.appSeqNum = Long.parseLong(appSeqNum);
    }
    catch(Exception e)
    {
      this.appSeqNum = 0L;
    }
  }
  public long getAppSeqNum()
  {
    return this.appSeqNum;
  }

  public void setAppControlNum(String appControlNum)
  {
    try
    {
      this.appControlNum = Long.parseLong(appControlNum);
    }
    catch(Exception e)
    {
      this.appControlNum = 0L;
    }
  }
  public long getAppControlNum()
  {
    return this.appControlNum;
  }

  public void setMerchantName(String merchantName)
  {
    this.merchantName = merchantName;
  }
  public String getMerchantName()
  {
    return this.merchantName;
  }

  public void setMerchantNumber(String merchantNumber)
  {
    try
    {
      this.merchantNumber = Long.parseLong(merchantNumber);
    }
    catch(Exception e)
    {
      this.merchantNumber = 0L;
    }
  }
  public long getMerchantNumber()
  {
    return this.merchantNumber;
  }

  public long getCompletedScreens()
  {
    return this.completedScreens;
  }

  public String getSubmittedDate()
  {
    return this.submittedDate;
  }
}
