/*@lineinfo:filename=SicCodes*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/SicCodes.sqlj $

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/29/02 4:18p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class SicCodes extends SQLJConnectionBase
{
  public Vector     sicCodes        = new Vector();
  
  public SicCodes()
  {
  }
  
  public void getSicCodes()
  {
    ResultSetIterator     it      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:46^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sort_code     sort_code,
//                  sic_code      sic_code,
//                  merchant_type description,
//                  floor_limit   floor_limit,
//                  mc_tcc        mc_tcc,
//                  visa_btb      visa_btb
//          from    sic_codes2
//          where   sic_code is not null
//          order by sort_code asc, sort_order asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sort_code     sort_code,\n                sic_code      sic_code,\n                merchant_type description,\n                floor_limit   floor_limit,\n                mc_tcc        mc_tcc,\n                visa_btb      visa_btb\n        from    sic_codes2\n        where   sic_code is not null\n        order by sort_code asc, sort_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.SicCodes",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.SicCodes",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:57^7*/
      
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {
        SicCode   sc = new SicCode(rs);
        
        sicCodes.add(sc);
      }
    }
    catch(Exception e)
    {
      logEntry("getSicCodes()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public class SicCode
  {
    public String       sortCode        = "";
    public String       sicCode         = "";
    public String       description     = "";
    public String       floorLimit      = "";
    public String       mcTCC           = "";
    public String       visaBTB         = "";
    
    public SicCode(ResultSet rs)
    {
      try
      {
        this.sortCode     = rs.getString("sort_code");
        this.sicCode      = rs.getString("sic_code");
        this.description  = rs.getString("description");
        this.floorLimit   = rs.getString("floor_limit");
        this.mcTCC        = rs.getString("mc_tcc");
        this.visaBTB      = rs.getString("visa_btb");
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry("com.mes.ops.SicCodes.SicCode()", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/