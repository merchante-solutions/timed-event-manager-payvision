/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/IndividualMerchantBean.java $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-07 15:59:41 -0700 (Mon, 07 Jul 2008) $
  Version            : $Revision: 15046 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

public class IndividualMerchantBean extends ExpandDataBean
{
  // data members
  private String      cpsFlag           = "";   // merchant.merch_visa_cps_ind          (moto = 1, hotel = 2, default = N)
  private String      psirfFlag         = "";   // merchant.merch_visa_psirf_flag       (retail with terminal = Y, otherwise N)
  private String      smktFlag          = "";   // merchant.merch_visa_smkt_flag        (sic 5411 = Y, otherwise N)
  private String      epsFlag           = "";   // merchant.merch_visa_eps_flag         (sic 5812, 5814, 7523, 7832 = Y otherwise N)
  private String      eirfFlag          = "";   // merchant.merch_visa_eirf_flag        (default to Y.  N is the other option)
  private String      meritFlag         = "";   // merchant.merch_mc_merit_flag         (moto = 1, retail w/terminal = 3, default = N)
  private String      siipFlag          = "";   // merchant.merch_mc_smkt_flag          (sic 5411 = Y, otherwise N)
  private String      prmFlag           = "";   // merchant.merch_mc_premier_flag       (sic between 3300 & 3999 or 4411 = Y, otherwise N)
  private String      catFlag           = "";   // merchant.merch_mc_pertocat_flag      (sic 5542 = Y, otherwise N)
  private String      whsFlag           = "";   // merchant.merch_mc_warehouse_flag     (sic 5300 = Y, otherwise N)
  private String      depositFlag       = "";   // merchant.merch_deposit_flag          (default to Y, other option is N)
  private String      riskCategory      = "";   // 
  private String      exceptionFlag     = "";   // merchant.merch_dly_excpt_flag        (default to Y, other option is N)
  private String      fraudFlag         = "";   // merchant.merch_fraud_flag            (default to Y, other option is N)
  private String      holdFlag          = "";   // merchant.merch_stat_hold_flag        (default to N, other option is Y)
  private String      achFlag           = "";   // merchant.merch_dly_ach_flag          (default to Y, other option is N)
  private String      printFlag         = "";   // merchant.merch_stat_print_flag       (default to Y, other option is N)
  private String      dollarFlag        = "";   // merchant.merch_interchg_dollar_flag  (default to G. other options are N, B, and *)
  private String      numberFlag        = "";   // merchant.merch_interchg_number_flag  (default to G. other options are N, B, and *)
  private String      draftStorageFlag  = "";   // merchant.merch_paper_draft_flag
  private String      vrsFlag           = "";   // merchant.merch_vrs_flag
  private String      plasticFlag       = "";   // merchant.merch_pos_add_ind
  private int         plasticNum        = 0;    // merchant.merch_pos_add_ind
  private String      dailyDiscountFlag = "";   // merchant.merch_dly_discount_flag

  private int         sicCode           = 0;
  private int         indType           = 0;
  private int         appType           = -1;
  private int         prodType          = 0;
  private int         bankNumber        = 0;

  private Vector      cpsFlags          = new Vector();
  private Vector      meritFlags        = new Vector();
  private Vector      yesNoFlags        = new Vector();
  private Vector      noYesFlags        = new Vector();
  private Vector      interchangeFlags  = new Vector();
  private Vector      storageFlags      = new Vector();
  private Vector      vrsFlags          = new Vector();

    
  public IndividualMerchantBean()
  {
    setCpsFlags();
    setMeritFlags();
    setYesNoFlags();
    setNoYesFlags();
    setInterchangeFlags();
    setFlags();
  }

  public void getData(long appSeqNum)
  {
    getFlagData(appSeqNum);
    setFlags(appSeqNum);
  }

  public void setDefaults(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      // get the sic code and industry type which will be needed for defaults
      qs.append("select                               ");
      qs.append("merch.sic_code,                      ");
      qs.append("merch.industype_code,                ");
      qs.append("app.app_type,                        ");
      qs.append("apt.app_bank_number                  ");
      qs.append("from merchant merch,                 ");
      qs.append("application   app,                   ");
      qs.append("app_type apt                         ");
      qs.append("where merch.app_seq_num = ? and      ");
      qs.append("merch.app_seq_num = app.app_seq_num  ");
      qs.append("and app.app_type = apt.app_type_code ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        appType = rs.getInt("app_type");
        bankNumber = rs.getInt("app_bank_number");

        if(!isBlank(rs.getString("sic_code")))
        {
          sicCode = rs.getInt("sic_code");
        }

        if(!isBlank(rs.getString("industype_code")))
        {
          indType = rs.getInt("industype_code");
        }
      }
      
      rs.close();
      ps.close();

      qs.setLength(0);
      qs.append("select                       ");
      qs.append("pc.pos_type                  ");
      qs.append("from pos_category pc,        ");
      qs.append("merch_pos         mp         ");
      qs.append("where mp.app_seq_num = ? and ");
      qs.append("mp.POS_CODE = pc.POS_CODE    ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();
      
      if(rs.next())
      {
        prodType = rs.getInt("pos_type");
      }
      
      rs.close();
      ps.close();


      // cpsFlag
      if(isBlank(cpsFlag))
      {
        if(bankNumber == 3941)
        {
          cpsFlag = "N";
        }
        else
        {
          switch(indType)
          {
            case 7:   // moto
              cpsFlag = "1";
              break;
            case 3:
              cpsFlag = "2";
              break;
            default:
              cpsFlag = "N";
              break;
          }
        }
      }
      
      // psirfFlag
      if(isBlank(psirfFlag))
      {
        if(bankNumber == 3941)
        {
          psirfFlag = "N";
        }
        else
        {
          psirfFlag = (indType == 1) ? "Y" : "N";
        }
      }
      
      // smktFlag
      if(isBlank(smktFlag))
      {
        if(bankNumber == 3941)
        {
          smktFlag = "N";
        }
        else
        {
          smktFlag = (sicCode == 5411) ? "Y" : "N";
        }
      }
      
      // epsFlag
      if(isBlank(epsFlag))
      {
        if(bankNumber == 3941)
        {
          epsFlag = "N";
        }
        else
        {
          switch(sicCode)
          {
            case 5812:
            case 5814:
            case 7523:
            case 7832:
              epsFlag = "Y";
              break;
          
            default:
              epsFlag = "N";
              break;
          }
        }
      }
      
      // eirfFlag
      if(isBlank(eirfFlag))
      {
        if(bankNumber == 3941)
        {
          eirfFlag = "N";
        }
        else
        {
          eirfFlag = "Y";
        }
      }
      
      // meritFlag
      if(isBlank(meritFlag))
      {
        if(bankNumber == 3941)
        {
          meritFlag = "N";
        }
        else
        {
          switch(indType)
          {
            case 7:   // moto
              meritFlag = "1";
              break;
            case 1:   // retail
              meritFlag = "3";
              break;
            default:
              meritFlag = "N";
              break;
          }
        }
      }

      long hnode = 0L;

      ps = getPreparedStatement("select hierarchy_node from org_app where app_type = ?");
      ps.setInt(1, appType);
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        hnode = rs.getLong("hierarchy_node");
      }

      rs.close();
      ps.close();

      if(bankNumber == 3941)
      {
        meritFlag = "N";
      }
      else
      {
        if(hnode != 9999999999L)
        {
          ps = getPreparedStatement("select count(ancestor) from t_hierarchy where hier_type = 1 and ancestor = 3941500001 and descendent = ?");
          ps.setLong(1, hnode);
          rs = ps.executeQuery();
      
          if(rs.next())
          {
            if(rs.getInt(1) > 0)
            {
              meritFlag = "N";
            }  
          }
          else
          {
            switch(appType)
            {
              case mesConstants.APP_TYPE_MES:
              case mesConstants.APP_TYPE_MES_NEW:
              case mesConstants.APP_TYPE_DISCOVER:
              case mesConstants.APP_TYPE_DISCOVER_IMS:
              case mesConstants.APP_TYPE_VERISIGN:
              case mesConstants.APP_TYPE_NSI:
                meritFlag = "N";
              break;
            }
          }
      
          rs.close();
          ps.close();
        }
        else
        {
          switch(appType)
          {
            case mesConstants.APP_TYPE_MES:
            case mesConstants.APP_TYPE_MES_NEW:
            case mesConstants.APP_TYPE_DISCOVER:
            case mesConstants.APP_TYPE_DISCOVER_IMS:
            case mesConstants.APP_TYPE_VERISIGN:
            case mesConstants.APP_TYPE_NSI:
              meritFlag = "N";
            break;
          }
        }
      }

      // siipFlag
      if(isBlank(siipFlag))
      {
        if(bankNumber == 3941)
        {
          siipFlag = "N";
        }
        else
        {
          siipFlag = (sicCode == 5411) ? "Y" : "N";
        }
      }
      
      // prmFlag
      if(isBlank(prmFlag))
      {
        if(bankNumber == 3941)
        {
          prmFlag = "N";
        }
        else
        {
          if((sicCode >= 3300 && sicCode <= 3999) || sicCode == 4411)
          {
            prmFlag = "Y";
          }
          else
          {
            prmFlag = "N";
          }
        }
      }
      
      // catFlag
      if(isBlank(catFlag))
      {
        if(bankNumber == 3941)
        {
          catFlag = "N";
        }
        else
        {
          catFlag = (sicCode == 5542) ? "Y" : "N";
        }
      }
      
      // whsFlag
      if(isBlank(whsFlag))
      {
        if(bankNumber == 3941)
        {
          whsFlag = "N";
        }
        else
        {
          whsFlag = (sicCode == 5300) ? "Y" : "N";
        }
      }
      
      // depositFlag
      if(isBlank(depositFlag))
      {
        depositFlag = "Y";
      }
      
      // exceptionFlag
      if(isBlank(exceptionFlag))
      {
        exceptionFlag = "Y";
      }
      
      // fraudFlag
      if(isBlank(fraudFlag))
      {
        fraudFlag = "Y";
      }
      
      // achFlag
      if(isBlank(achFlag))
      {
        achFlag = "Y";
      }
      
      // dollarFlag
      if(isBlank(dollarFlag))
      {
        if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_CBT_NEW) 
        {
          dollarFlag = "B";
        }
        else
        {
          dollarFlag = "G";
        }
      }
      
      // numberFlag
      if(isBlank(numberFlag))
      {
        if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_CBT_NEW)
        {
          numberFlag = "B";
        }
        else
        {
          numberFlag = "G";
        }
      }

      if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_CBT_NEW)
      {
        if(prodType == 1 || prodType == 9 || indType == 9)
        {
          psirfFlag = "Y";
          meritFlag = "3";         
        } 
        else
        {
          cpsFlag   = "1";
          meritFlag = "1";         
        }
      }

      if(prodType != mesConstants.APP_PAYSOL_DIAL_TERMINAL)
      {
        psirfFlag = "N";  
        eirfFlag  = "N";
      }
      
      printFlag = AutoUploadIndividualMerchant.getPrintFlag(appType);
      holdFlag = ( printFlag.equals("Y") ? "N" : "Y" );

      switch(appType)
      {
        case mesConstants.APP_TYPE_VERISIGN:
        case mesConstants.APP_TYPE_NSI:
        case mesConstants.APP_TYPE_PAYPAL_REFERRAL:
          dailyDiscountFlag = "D";
        break;
        
        case mesConstants.APP_TYPE_BCB:
          dailyDiscountFlag = "I";
          break;
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setDefaults: " + e.toString());
      addError("setDefaults: " + e.toString());
    }
  }


  private void setFlags(long appSeqNum)
  {
    StringBuffer      qs            = new StringBuffer("");
    PreparedStatement ps            = null;
    ResultSet         rs            = null;
    boolean           checkProduct  = false;

    try
    {
      
      //unconditionally default plasticFlag to 'N', if it hasnt been set already
      plasticFlag     = isBlank(plasticFlag)      ? "N" : plasticFlag;
      
      qs.append("select                 ");
      qs.append("sic_code,              ");
      qs.append("pricing_grid           ");
      qs.append("from merchant          ");
      qs.append("where app_seq_num = ?  ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        
        if(!isBlank(rs.getString("sic_code")))
        {
          switch(rs.getInt("sic_code"))
          {
            case 5411:
              //smktFlag        = isBlank(smktFlag)   ? "Y" : smktFlag;
            break;
            case 5542:
              catFlag         = isBlank(catFlag)    ? "Y" : catFlag;
            break;
            case 5814:
            case 7524:
            case 7833:
              epsFlag         = isBlank(epsFlag)    ? "Y" : epsFlag;
            break;
          }
        }

        setFlagsProduct(appSeqNum);

        if(!isBlank(rs.getString("pricing_grid")))
        {
          switch(rs.getInt("pricing_grid"))
          {
            case mesConstants.APP_PG_RETAIL:
              cpsFlag           = isBlank(cpsFlag)          ? "N" : cpsFlag;
              psirfFlag         = isBlank(psirfFlag)        ? "Y" : psirfFlag;
              smktFlag          = isBlank(smktFlag)         ? "N" : smktFlag;
              epsFlag           = isBlank(epsFlag)          ? "N" : epsFlag;
              eirfFlag          = isBlank(eirfFlag)         ? "N" : eirfFlag;
              meritFlag         = isBlank(meritFlag)        ? "3" : meritFlag;
              siipFlag          = isBlank(siipFlag)         ? "N" : siipFlag;
              prmFlag           = isBlank(prmFlag)          ? "N" : prmFlag;
              catFlag           = isBlank(catFlag)          ? "N" : catFlag;
              whsFlag           = isBlank(whsFlag)          ? "N" : whsFlag;
              draftStorageFlag  = isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
              vrsFlag           = isBlank(vrsFlag)          ? "N" : vrsFlag;
              plasticFlag       = isBlank(plasticFlag)      ? "N" : plasticFlag;
            break;
            
            case mesConstants.APP_PG_HOTEL:
              cpsFlag           = isBlank(cpsFlag)          ? "2" : cpsFlag;
              psirfFlag         = isBlank(psirfFlag)        ? "N" : psirfFlag;
              smktFlag          = isBlank(smktFlag)         ? "N" : smktFlag;
              epsFlag           = isBlank(epsFlag)          ? "N" : epsFlag;
              eirfFlag          = isBlank(eirfFlag)         ? "N" : eirfFlag;
              meritFlag         = isBlank(meritFlag)        ? "N" : meritFlag;
              siipFlag          = isBlank(siipFlag)         ? "N" : siipFlag;
              prmFlag           = isBlank(prmFlag)          ? "Y" : prmFlag;
              catFlag           = isBlank(catFlag)          ? "N" : catFlag;
              whsFlag           = isBlank(whsFlag)          ? "N" : whsFlag;
              draftStorageFlag  = isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
              vrsFlag           = isBlank(vrsFlag)          ? "N" : vrsFlag;
              plasticFlag       = isBlank(plasticFlag)      ? "N" : plasticFlag;
            break;
            
            case mesConstants.APP_PG_MOTO:
              cpsFlag           = isBlank(cpsFlag)          ? "1" : cpsFlag;
              psirfFlag         = isBlank(psirfFlag)        ? "N" : psirfFlag;
              smktFlag          = isBlank(smktFlag)         ? "N" : smktFlag;
              epsFlag           = isBlank(epsFlag)          ? "N" : epsFlag;
              eirfFlag          = isBlank(eirfFlag)         ? "N" : eirfFlag;
              meritFlag         = isBlank(meritFlag)        ? "1" : meritFlag;
              siipFlag          = isBlank(siipFlag)         ? "N" : siipFlag;
              prmFlag           = isBlank(prmFlag)          ? "N" : prmFlag;
              catFlag           = isBlank(catFlag)          ? "N" : catFlag;
              whsFlag           = isBlank(whsFlag)          ? "N" : whsFlag;
              draftStorageFlag  = isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
              vrsFlag           = isBlank(vrsFlag)          ? "N" : vrsFlag;
              plasticFlag       = isBlank(plasticFlag)      ? "N" : plasticFlag;
            break;  
          }
        }
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("setFlags: " + e.toString());
      addError("setFlags: " + e.toString());
    }
  }

  private void setFlagsProduct(long appSeqNum)
  {
    StringBuffer      qs            = new StringBuffer("");
    PreparedStatement ps            = null;
    ResultSet         rs            = null;

    try
    {
      
      qs.append("select b.pos_type ");
      qs.append("from merch_pos a, pos_category b ");
      qs.append("where a.app_seq_num = ? and a.pos_code = b.pos_code");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();
      if (rs.next())
      {
        switch (rs.getInt("pos_type"))
        {
          case mesConstants.APP_PAYSOL_DIAL_PAY:
            cpsFlag         = isBlank(cpsFlag)          ? "N" : cpsFlag;
            psirfFlag       = isBlank(psirfFlag)        ? "N" : psirfFlag;
            smktFlag        = isBlank(smktFlag)         ? "N" : smktFlag;
            epsFlag         = isBlank(epsFlag)          ? "N" : epsFlag;
            eirfFlag        = isBlank(eirfFlag)         ? "Y" : eirfFlag;
            meritFlag       = isBlank(meritFlag)        ? "1" : meritFlag;
            siipFlag        = isBlank(siipFlag)         ? "N" : siipFlag;
            prmFlag         = isBlank(prmFlag)          ? "N" : prmFlag;
            catFlag         = isBlank(catFlag)          ? "N" : catFlag;
            whsFlag         = isBlank(whsFlag)          ? "N" : whsFlag;
            draftStorageFlag= isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
            vrsFlag         = isBlank(vrsFlag)          ? "N" : vrsFlag;
            plasticFlag     = isBlank(plasticFlag)      ? "N" : plasticFlag;
          break;
          case mesConstants.APP_PAYSOL_INTERNET:
            cpsFlag         = isBlank(cpsFlag)          ? "1" : cpsFlag;
            psirfFlag       = isBlank(psirfFlag)        ? "N" : psirfFlag;
            smktFlag        = isBlank(smktFlag)         ? "N" : smktFlag;
            epsFlag         = isBlank(epsFlag)          ? "N" : epsFlag;
            eirfFlag        = isBlank(eirfFlag)         ? "N" : eirfFlag;
            meritFlag       = isBlank(meritFlag)        ? "1" : meritFlag;
            siipFlag        = isBlank(siipFlag)         ? "N" : siipFlag;
            prmFlag         = isBlank(prmFlag)          ? "N" : prmFlag;
            catFlag         = isBlank(catFlag)          ? "N" : catFlag;
            whsFlag         = isBlank(whsFlag)          ? "N" : whsFlag;
            draftStorageFlag= isBlank(draftStorageFlag) ? "N" : draftStorageFlag;
            vrsFlag         = isBlank(vrsFlag)          ? "N" : vrsFlag;
            plasticFlag     = isBlank(plasticFlag)      ? "N" : plasticFlag;
          break;
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("setFlagsProduct: " + e.toString());
      addError("setFlagsProduct: " + e.toString());
    }
  }



  private void getFlagData(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select                       ");
      qs.append("merch_visa_cps_ind,          ");
      qs.append("merch_visa_psirf_flag,       ");
      qs.append("merch_visa_smkt_flag,        ");
      qs.append("merch_visa_eps_flag,         ");
      qs.append("merch_visa_eirf_flag,        ");
      qs.append("merch_mc_merit_ind,          ");
      qs.append("merch_mc_smkt_flag,          ");
      qs.append("merch_mc_premier_flag,       ");
      qs.append("merch_mc_petrocat_flag,      ");
      qs.append("merch_mc_warehouse_flag,     ");
      qs.append("merch_deposit_flag,          ");
      qs.append("merch_dly_excpt_flag,        ");
      qs.append("merch_fraud_flag,            ");
      qs.append("merch_stat_hold_flag,        ");
      qs.append("merch_dly_ach_flag,          ");
      qs.append("merch_stat_print_flag,       ");
      qs.append("merch_interchg_dollar_flag,  ");
      qs.append("merch_interchg_number_flag,  ");
      qs.append("merch_paper_draft_flag,      ");
      qs.append("merch_vrs_flag,              ");
      qs.append("merch_pos_add_ind,           ");
      qs.append("merch_eqpt_dlv_add_pref,     ");
      qs.append("merch_dly_discount_flag      ");
      qs.append("from merchant                ");
      qs.append("where app_seq_num = ?        ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        cpsFlag           = isBlank(rs.getString("merch_visa_cps_ind"))         ? ""  : rs.getString("merch_visa_cps_ind");
        psirfFlag         = isBlank(rs.getString("merch_visa_psirf_flag"))      ? ""  : rs.getString("merch_visa_psirf_flag");
        smktFlag          = isBlank(rs.getString("merch_visa_smkt_flag"))       ? ""  : rs.getString("merch_visa_smkt_flag");
        epsFlag           = isBlank(rs.getString("merch_visa_eps_flag"))        ? ""  : rs.getString("merch_visa_eps_flag");
        eirfFlag          = isBlank(rs.getString("merch_visa_eirf_flag"))       ? ""  : rs.getString("merch_visa_eirf_flag");
        meritFlag         = isBlank(rs.getString("merch_mc_merit_ind"))         ? ""  : rs.getString("merch_mc_merit_ind");
        siipFlag          = isBlank(rs.getString("merch_mc_smkt_flag"))         ? ""  : rs.getString("merch_mc_smkt_flag");
        prmFlag           = isBlank(rs.getString("merch_mc_premier_flag"))      ? ""  : rs.getString("merch_mc_premier_flag");
        catFlag           = isBlank(rs.getString("merch_mc_petrocat_flag"))     ? ""  : rs.getString("merch_mc_petrocat_flag");
        whsFlag           = isBlank(rs.getString("merch_mc_warehouse_flag"))    ? ""  : rs.getString("merch_mc_warehouse_flag");
        depositFlag       = isBlank(rs.getString("merch_deposit_flag"))         ? ""  : rs.getString("merch_deposit_flag");
        exceptionFlag     = isBlank(rs.getString("merch_dly_excpt_flag"))       ? ""  : rs.getString("merch_dly_excpt_flag");
        fraudFlag         = isBlank(rs.getString("merch_fraud_flag"))           ? ""  : rs.getString("merch_fraud_flag");
        holdFlag          = isBlank(rs.getString("merch_stat_hold_flag"))       ? ""  : rs.getString("merch_stat_hold_flag");
        achFlag           = isBlank(rs.getString("merch_dly_ach_flag"))         ? ""  : rs.getString("merch_dly_ach_flag");
        printFlag         = isBlank(rs.getString("merch_stat_print_flag"))      ? ""  : rs.getString("merch_stat_print_flag");
        dollarFlag        = isBlank(rs.getString("merch_interchg_dollar_flag")) ? ""  : rs.getString("merch_interchg_dollar_flag");
        numberFlag        = isBlank(rs.getString("merch_interchg_number_flag")) ? ""  : rs.getString("merch_interchg_number_flag");
        draftStorageFlag  = isBlank(rs.getString("merch_paper_draft_flag"))     ? ""  : rs.getString("merch_paper_draft_flag");
        vrsFlag           = isBlank(rs.getString("merch_vrs_flag"))             ? ""  : rs.getString("merch_vrs_flag");
        plasticFlag       = isBlank(rs.getString("merch_pos_add_ind"))          ? ""  : rs.getString("merch_pos_add_ind");
        plasticNum        = isBlank(rs.getString("merch_eqpt_dlv_add_pref"))    ? 0   : rs.getInt("merch_eqpt_dlv_add_pref");
        dailyDiscountFlag = isBlank(rs.getString("merch_dly_discount_flag"))    ? ""  : rs.getString("merch_dly_discount_flag");
      }  
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFlagData: no record found");
        addError("getFlagData: no record found");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("getFlagData: " + e.toString());
      addError("getFlagData: " + e.toString());
    }
  }
  
  public void submitData(HttpServletRequest req, long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    
    try
    {
      qs.append("update merchant set ");
      qs.append("merch_visa_cps_ind         = ?, ");
      qs.append("merch_visa_psirf_flag      = ?, ");
      qs.append("merch_visa_smkt_flag       = ?, ");
      qs.append("merch_visa_eps_flag        = ?, ");
      qs.append("merch_visa_eirf_flag       = ?, ");
      qs.append("merch_mc_merit_ind         = ?, ");
      qs.append("merch_mc_smkt_flag         = ?, ");
      qs.append("merch_mc_premier_flag      = ?, ");
      qs.append("merch_mc_petrocat_flag     = ?, ");
      qs.append("merch_mc_warehouse_flag    = ?, ");
      qs.append("merch_deposit_flag         = ?, ");
      qs.append("merch_dly_excpt_flag       = ?, ");
      qs.append("merch_fraud_flag           = ?, ");
      qs.append("merch_stat_hold_flag       = ?, ");
      qs.append("merch_dly_ach_flag         = ?, ");
      qs.append("merch_stat_print_flag      = ?, ");
      qs.append("merch_interchg_dollar_flag = ?, ");
      qs.append("merch_interchg_number_flag = ?, ");
      qs.append("merch_paper_draft_flag     = ?, ");
      qs.append("merch_vrs_flag             = ?, ");
      qs.append("merch_pos_add_ind          = ?, ");
      qs.append("merch_eqpt_dlv_add_pref    = ?, ");
      qs.append("merch_dly_discount_flag    = ?  ");
      qs.append("where app_seq_num = ?");
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1, cpsFlag);
      ps.setString(2, psirfFlag);
      ps.setString(3, smktFlag);
      ps.setString(4, epsFlag);
      ps.setString(5, eirfFlag);
      ps.setString(6, meritFlag);
      ps.setString(7, siipFlag);
      ps.setString(8, prmFlag);
      ps.setString(9, catFlag);
      ps.setString(10, whsFlag);
      ps.setString(11, depositFlag);
      ps.setString(12, exceptionFlag);
      ps.setString(13, fraudFlag);
      ps.setString(14, holdFlag);
      ps.setString(15, achFlag);
      ps.setString(16, printFlag);
      ps.setString(17, dollarFlag);
      ps.setString(18, numberFlag);
      ps.setString(19, draftStorageFlag);
      ps.setString(20, vrsFlag);
      ps.setString(21, plasticFlag);
      ps.setInt   (22, plasticNum);
      ps.setString(23, dailyDiscountFlag);
      ps.setLong  (24, appSeqNum);
      
      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: unable to update merchant table");
        addError("submitData: unable to update merchant table");
      }
      
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
      addError("submitData: " + e.toString());
    }
  }

  public boolean validate()
  {
    if(plasticFlag.equals("Y") && plasticNum <= 0)
    {
      addError("If selecting 'Y' for Request Plastics, a number of plastics must be selected");
    }
    else if(plasticFlag.equals("N") && plasticNum > 0)
    {
      addError("If selecting 'N' for Request Plastics, none must be selected for number of plastics");
    }

    return(! hasErrors());
  }

  public void setCpsFlag(String cpsFlag)
  {
    this.cpsFlag = cpsFlag;
  }
  public String getCpsFlag()
  {
    return this.cpsFlag;
  }

  public void setPsirfFlag(String psirfFlag)
  {
    this.psirfFlag = psirfFlag;
  }
  public String getPsirfFlag()
  {
    return this.psirfFlag;
  }

  public void setSmktFlag(String smktFlag)
  {
    this.smktFlag = smktFlag;
  }
  public String getSmktFlag()
  {
    return this.smktFlag;
  }

  public void setEpsFlag(String epsFlag)
  {
    this.epsFlag = epsFlag;
  }
  public String getEpsFlag()
  {
    return this.epsFlag;
  }

  public void setEirfFlag(String eirfFlag)
  {
    this.eirfFlag = eirfFlag;
  }
  public String getEirfFlag()
  {
    return this.eirfFlag;
  }

  public void setMeritFlag(String meritFlag)
  {
    this.meritFlag = meritFlag;
  }
  public String getMeritFlag()
  {
    return this.meritFlag;
  }

  public void setSiipFlag(String siipFlag)
  {
    this.siipFlag = siipFlag;
  }
  public String getSiipFlag()
  {
    return this.siipFlag;
  }

  public void setPrmFlag(String prmFlag)
  {
    this.prmFlag = prmFlag;
  }
  public String getPrmFlag()
  {
    return this.prmFlag;
  }

  public void setCatFlag(String catFlag)
  {
    this.catFlag = catFlag;
  }
  public String getCatFlag()
  {
    return this.catFlag;
  }

  public void setWhsFlag(String whsFlag)
  {
    this.whsFlag = whsFlag;
  }
  public String getWhsFlag()
  {
    return this.whsFlag;
  }

  public void setDepositFlag(String depositFlag)
  {
    this.depositFlag = depositFlag;
  }
  public String getDepositFlag()
  {
    return this.depositFlag;
  }

  public void setRiskCategory(String riskCategory)
  {
    this.riskCategory = riskCategory;
  }
  public String getRiskCategory()
  {
    return this.riskCategory;
  }

  public void setExceptionFlag(String exceptionFlag)
  {
    this.exceptionFlag = exceptionFlag;
  }
  public String getExceptionFlag()
  {
    return this.exceptionFlag;
  }

  public void setFraudFlag(String fraudFlag)
  {
    this.fraudFlag = fraudFlag;
  }
  public String getFraudFlag()
  {
    return this.fraudFlag;
  }

  public void setHoldFlag(String holdFlag)
  {
    this.holdFlag = holdFlag;
  }
  public String getHoldFlag()
  {
    return this.holdFlag;
  }

  public void setAchFlag(String achFlag)
  {
    this.achFlag = achFlag;
  }
  public String getAchFlag()
  {
    return this.achFlag;
  }

  public void setPrintFlag(String printFlag)
  {
    this.printFlag = printFlag;
  }
  public String getPrintFlag()
  {
    return this.printFlag;
  }

  public void setDollarFlag(String dollarFlag)
  {
    this.dollarFlag = dollarFlag;
  }
  public String getDollarFlag()
  {
    return this.dollarFlag;
  }

  public void setNumberFlag(String numberFlag)
  {
    this.numberFlag = numberFlag;
  }
  public String getNumberFlag()
  {
    return this.numberFlag;
  }
  
  public void setDailyDiscountFlag(String dailyDiscountFlag)
  {
    this.dailyDiscountFlag = dailyDiscountFlag;
  }
  public String getDailyDiscountFlag()
  {
    return this.dailyDiscountFlag;
  }

  public void setDraftStorageFlag(String draftStorageFlag)
  {
    this.draftStorageFlag = draftStorageFlag;
  }
  public String getDraftStorageFlag()
  {
    return this.draftStorageFlag;
  }
  
  public void setVrsFlag(String vrsFlag)
  {
    this.vrsFlag = vrsFlag;
  }
  public String getVrsFlag()
  {
    return this.vrsFlag;
  }
  public Vector getVrsFlags()
  {
    return this.vrsFlags;
  }
  public Vector getStorageFlags()
  {
    return this.storageFlags;
  }

  public void setPlasticFlag(String plasticFlag)
  {
    this.plasticFlag = plasticFlag;
  }
  public String getPlasticFlag()
  {
    return this.plasticFlag;
  }

  public void setPlasticNum(String plasticNum)
  {
    try
    {
      this.plasticNum = Integer.parseInt(plasticNum);
    }
    catch(Exception e)
    {
      this.plasticNum = 0;
    }
  }
  public int getPlasticNum()
  {
    return this.plasticNum;
  }

  public void setDefaults(String appSeqNum)
  {
    try
    {
      setDefaults(Long.parseLong(appSeqNum));
    }
    catch(Exception e)
    {
    }
  }

  private void setCpsFlags()
  {
    cpsFlags.add("1");
    cpsFlags.add("2");
    cpsFlags.add("N");
  }
  public Vector getCpsFlags()
  {
    return this.cpsFlags;
  }
  
  private void setFlags()
  {
    storageFlags.add("Y");
    storageFlags.add("N");
    
    vrsFlags.add("N");
    vrsFlags.add("Y");
  }

  private void setMeritFlags()
  {
    meritFlags.add("1");
    meritFlags.add("3");
    meritFlags.add("N");
  }
  public Vector getMeritFlags()
  {
    return this.meritFlags;
  }
  
  private void setYesNoFlags()
  {
    yesNoFlags.add("Y");
    yesNoFlags.add("N");
  }

  private void setNoYesFlags()
  {
    noYesFlags.add("N");
    noYesFlags.add("Y");
  }

  public Vector getYesNoFlags()
  {
    return this.yesNoFlags;
  }

  public Vector getNoYesFlags()
  {
    return this.noYesFlags;
  }
  
  private void setInterchangeFlags()
  {
    interchangeFlags.add("G");
    interchangeFlags.add("N");
    interchangeFlags.add("B");
    interchangeFlags.add("*");
  }      
  public Vector getInterchangeFlags()
  {
    return this.interchangeFlags;
  }
}
