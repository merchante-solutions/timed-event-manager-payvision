/*@lineinfo:filename=QuestionableDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/QuestionableDataBean.sqlj $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;


public class QuestionableDataBean extends SQLJConnectionBase
{
  public static final int OUTSTANDING_ERRORS      = 1;
  public static final int CLEARED_ERRORS          = 2;

  private Vector outstandingErrors                = new Vector();
  private Vector clearedErrors                    = new Vector();
 
  private String dbaName                          = "";
  private String merchNum                         = "";
  private String controlNum                       = "";
  private String appTypeDesc                      = "";
  private String appCreatedDate                   = "";
  private int    posType                          = -1;
  private String posDesc                          = "";
  private String posCategory                      = "";

  private Timestamp           today                       = null;


  public QuestionableDataBean()
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:70^7*/

//  ************************************************************
//  #sql [Ctx] { select    sysdate 
//          from      dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    sysdate  \n        from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.QuestionableDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   today = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:74^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "constructor: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public int numOutstandingErrors()
  {
    return outstandingErrors.size();
  }

  public int numClearedErrors()
  {
    return clearedErrors.size();
  }

  private void getMerchantGeneralInfo(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:105^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   merch.merch_business_name,
//                   merch.merch_number,       
//                   merch.merc_cntrl_number,  
//                   app.appsrctype_code,      
//                   app.app_created_date,     
//                   pos.pos_type,             
//                   pos.pos_desc              
//          
//          from     merchant      merch,
//                   application   app,  
//                   pos_category  pos,  
//                   merch_pos     mpos  
//  
//          where    merch.app_seq_num = :primaryKey
//                   and merch.app_seq_num = app.app_seq_num 
//                   and merch.app_seq_num = mpos.app_seq_num
//                   and mpos.pos_code = pos.pos_code        
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   merch.merch_business_name,\n                 merch.merch_number,       \n                 merch.merc_cntrl_number,  \n                 app.appsrctype_code,      \n                 app.app_created_date,     \n                 pos.pos_type,             \n                 pos.pos_desc              \n        \n        from     merchant      merch,\n                 application   app,  \n                 pos_category  pos,  \n                 merch_pos     mpos  \n\n        where    merch.app_seq_num =  :1 \n                 and merch.app_seq_num = app.app_seq_num \n                 and merch.app_seq_num = mpos.app_seq_num\n                 and mpos.pos_code = pos.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.QuestionableDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.QuestionableDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        dbaName         = isBlank(rs.getString("merch_business_name"))  ? "" : rs.getString("merch_business_name");
        merchNum        = isBlank(rs.getString("merch_number"))         ? "" : rs.getString("merch_number");
        controlNum      = isBlank(rs.getString("merc_cntrl_number"))    ? "" : rs.getString("merc_cntrl_number");
        appTypeDesc     = isBlank(rs.getString("appsrctype_code"))      ? "" : rs.getString("appsrctype_code");
        appCreatedDate  = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("app_created_date"));
        posType         = rs.getInt("pos_type");
        posDesc         = isBlank(rs.getString("pos_desc"))             ? "" : rs.getString("pos_desc");

        switch(posType)
        {
          case mesConstants.POS_DIAL_TERMINAL:
            posCategory = "Dial Terminal (EDC)";
          break;
          case mesConstants.POS_INTERNET:
            posCategory = "Internet";
          break;
          case mesConstants.POS_PC:
            posCategory = "POS Partner";
          break;
          case mesConstants.POS_DIAL_AUTH:
            posCategory = "Dial Pay";
          break;
          case mesConstants.POS_OTHER:
            posCategory = "Other Vital Certified Product";
          break;
          case mesConstants.POS_STAGE_ONLY:
            posCategory = "Stage Only";
          break;
          case mesConstants.POS_GLOBAL_PC:
            posCategory = "Global PC Windows";
          break;
          default:
            posCategory = "unknown";
          break;
        }

      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMerchantGeneralInfo: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void getQuestionableData(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:192^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   mae.*, maec.error_desc 
//          from     merchant_app_errors mae, merchant_app_error_codes maec
//          where    app_seq_num  = :primaryKey and
//                   mae.error_code   = maec.error_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   mae.*, maec.error_desc \n        from     merchant_app_errors mae, merchant_app_error_codes maec\n        where    app_seq_num  =  :1  and\n                 mae.error_code   = maec.error_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.QuestionableDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.QuestionableDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:198^7*/
    
      rs = it.getResultSet();
      
      while(rs.next())
      {
       
        ErrorRec tempRec =  new ErrorRec();

        if(rs.getDate("error_date") != null)
        {
          tempRec.setErrorDate      (java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("error_date")));
        }

        if(rs.getDate("corrected_date") != null)
        {
          tempRec.setClearedDate    (java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("corrected_date")));
        }
        
        tempRec.setErrorCode        (rs.getString("error_code"));
        tempRec.setErrorDesc        (rs.getString("error_desc"));

        if(rs.getDate("corrected_date") == null)
        {
          outstandingErrors.add(tempRec);
        }
        else
        {
          clearedErrors.add(tempRec);
        }

      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getQuestionableData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void clearError(long primaryKey, String errorCode)
  {
    try 
    {

      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:251^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_app_errors
//          set     corrected_date     = :today
//          where   app_seq_num = :primaryKey and error_code = :errorCode and corrected_date is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_app_errors\n        set     corrected_date     =  :1 \n        where   app_seq_num =  :2  and error_code =  :3  and corrected_date is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.QuestionableDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setString(3,errorCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:256^7*/

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "clearError: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  //check to see if a certain error code exists.. or if any code exists when ERROR_ALL is passed in as the errorCode
  public String getErrorPageLink(long primaryKey, String errorCode, String pageLink, String pageLinkDesc)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    if(errorCode.equals(mesConstants.ERROR_ALL))
    {
      return getAllErrorPageLink(primaryKey,pageLink,pageLinkDesc);
    }
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:285^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   app_seq_num 
//          from     merchant_app_errors
//          where    app_seq_num  = :primaryKey and
//                   error_code   = :errorCode  and
//                   corrected_date is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   app_seq_num \n        from     merchant_app_errors\n        where    app_seq_num  =  :1  and\n                 error_code   =  :2   and\n                 corrected_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.QuestionableDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setString(2,errorCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.QuestionableDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:292^7*/
    
      rs = it.getResultSet();
   
      if(rs.next())
      {
        result = "<a target=\"_blank\" href=\"" + pageLink + "\">" + pageLinkDesc + "</a>";
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getErrorPageLink: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return result;
  }

  public String getAllErrorPageLink(long primaryKey, String pageLink, String pageLinkDesc)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:325^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   app_seq_num 
//          from     merchant_app_errors
//          where    app_seq_num  = :primaryKey and
//                   corrected_date is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   app_seq_num \n        from     merchant_app_errors\n        where    app_seq_num  =  :1  and\n                 corrected_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.QuestionableDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.QuestionableDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:331^7*/
    
      rs = it.getResultSet();
   
      if(rs.next())
      {
        result = "<a target=\"_blank\" href=\"" + pageLink + "\">" + pageLinkDesc + "</a>";
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAllErrorPageLink: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return result;
  }

  public boolean hasErrors(long primaryKey, String errorCode)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    boolean           result  = false;

    if(errorCode.equals(mesConstants.ERROR_ALL))
    {
      return hasAnyErrors(primaryKey);
    }
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:369^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   app_seq_num 
//          from     merchant_app_errors
//          where    app_seq_num  = :primaryKey and
//                   error_code   = :errorCode  and
//                   corrected_date is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   app_seq_num \n        from     merchant_app_errors\n        where    app_seq_num  =  :1  and\n                 error_code   =  :2   and\n                 corrected_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.QuestionableDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setString(2,errorCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.QuestionableDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:376^7*/
    
      rs = it.getResultSet();
   
      if(rs.next())
      {
        result = true;
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getErrorPageLink: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return result;
  }

  public boolean hasAnyErrors(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    boolean           result  = false;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:409^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   app_seq_num 
//          from     merchant_app_errors
//          where    app_seq_num  = :primaryKey and
//                   corrected_date is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   app_seq_num \n        from     merchant_app_errors\n        where    app_seq_num  =  :1  and\n                 corrected_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.QuestionableDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.QuestionableDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:415^7*/
    
      rs = it.getResultSet();
   
      if(rs.next())
      {
        result = true;
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAllErrorPageLink: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    return result;
  }





  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }
  
  public void getData(long primaryKey)
  {
    getMerchantGeneralInfo(primaryKey);
    getQuestionableData(primaryKey);
  }
  
  public void setDefaults(String primaryKey)
  {
    try
    {
      setDefaults(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
      logEntry("setDefaults(" + primaryKey + ")", e.toString());
    }
  }
  public void setDefaults(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setDefaults: " + e.toString());
    }
  }
  
  public String formatDate(Date raw)
  {
    String result   = "";
    if(raw == null)
    {
      return result;
    }
    try
    {
      DateFormat df   = new SimpleDateFormat("MM/dd/yy");
      result = df.format(raw);
      return result;
    }
    catch(Exception e)
    {
      System.out.println("date crashed = " + e.toString());
    }
    return result;
  }
  
    
  public String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;
      
      index = body.indexOf(' ');
      
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
      System.out.println("error will robinson %20");
    }
    return body;

  }
  
  public void submitData(HttpServletRequest req, long primaryKey)
  {
  }
 

  public String getDbaName()
  {
    return dbaName;
  }
  public String getMerchNum()
  {
    return merchNum;
  }
  public String getControlNum()
  {
    return controlNum;
  }
  public String getAppTypeDesc()
  {
    return appTypeDesc;
  }
  public String getAppCreatedDate()
  {
    return appCreatedDate;
  }
  public int getPosType()
  {
    return posType;
  }
  public String getPosDesc()
  {
    return posDesc;
  }
  public String getPosCategory()
  {
    return posCategory;
  }

  public String getErrorDate(int idx, int vec)
  {
    ErrorRec req = null;

    switch(vec)
    {
      case OUTSTANDING_ERRORS:
        req = (ErrorRec)outstandingErrors.elementAt(idx);
      break;
      case CLEARED_ERRORS:
        req = (ErrorRec)clearedErrors.elementAt(idx);
      break;
    }
    
    if(req == null)
    {
      return "";
    }

    return(req.getErrorDate());
  }

  public String getClearedDate(int idx, int vec)
  {
    ErrorRec req = null;

    switch(vec)
    {
      case OUTSTANDING_ERRORS:
        req = (ErrorRec)outstandingErrors.elementAt(idx);
      break;
      case CLEARED_ERRORS:
        req = (ErrorRec)clearedErrors.elementAt(idx);
      break;
    }

    if(req == null)
    {
      return "";
    }

    return(req.getClearedDate());
  }

  public String getErrorCode(int idx, int vec)
  {
    ErrorRec req = null;

    switch(vec)
    {
      case OUTSTANDING_ERRORS:
        req = (ErrorRec)outstandingErrors.elementAt(idx);
      break;
      case CLEARED_ERRORS:
        req = (ErrorRec)clearedErrors.elementAt(idx);
      break;
    }

    if(req == null)
    {
      return "";
    }

    return(req.getErrorCode());
  }

  public String getErrorDesc(int idx, int vec)
  {
    ErrorRec req = null;

    switch(vec)
    {
      case OUTSTANDING_ERRORS:
        req = (ErrorRec)outstandingErrors.elementAt(idx);
      break;
      case CLEARED_ERRORS:
        req = (ErrorRec)clearedErrors.elementAt(idx);
      break;
    }

    if(req == null)
    {
      return "";
    }

    return(req.getErrorDesc());
  }


  public class ErrorRec
  { 
    private String errorDate          = "";
    private String clearedDate        = "";
    private String errorCode          = "";
    private String errorDesc          = "";

    ErrorRec()
    {}

    public void setErrorDate(String errorDate)
    {
      this.errorDate = errorDate;
    }
    public void setClearedDate(String clearedDate)
    {
      this.clearedDate = clearedDate;
    }
    public void setErrorCode(String errorCode)
    {
      this.errorCode = errorCode;
    }
    public void setErrorDesc(String errorDesc)
    {
      this.errorDesc = errorDesc;
    }

    public String getErrorDate()
    {
      return this.errorDate;
    }
    public String getClearedDate()
    {
      return this.clearedDate;
    }
    public String getErrorCode()
    {
      return this.errorCode;
    }
    public String getErrorDesc()
    {
      return this.errorDesc;
    }

  }

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
 
}/*@lineinfo:generated-code*/