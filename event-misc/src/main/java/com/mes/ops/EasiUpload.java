package com.mes.ops;

import com.mes.database.SQLJConnectionBase;


public class EasiUpload extends SQLJConnectionBase
{
  public EasiUpload()
  {
    super(SQLJConnectionBase.getDirectConnectString());
  }
  
  public void run()
  {

    try
    {
      connect();

      EasiFileUpload upload = new EasiFileUpload(SQLJConnectionBase.getDirectConnectString());
      boolean problemFree = upload.execute();
      
      if(problemFree)
      {
        System.out.println("Files created!");
      }
      else
      {
        System.out.println("Exception: Files were not created!");
      }

    }
    catch (Exception e)
    {
      System.out.println("Exception: " + e.toString());
    }
    finally
    {
      cleanUp();
    }

  }
  
  public static void main(String[] args)
  {
    try
    {
      EasiUpload engine = new EasiUpload();
      engine.run();
    }
    catch(Exception e)
    {
      System.out.println("ERROR: " + e.toString());
    }
    finally
    {
      
    }
  }

}
