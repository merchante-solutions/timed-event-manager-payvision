/*@lineinfo:filename=BbtDiscoverBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/BbtDiscoverBean.sqlj $

  Description:                                    
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 9/29/04 4:19p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class BbtDiscoverBean extends SQLJConnectionBase
{
  private static final String DEFAULT_PARTNER_NUMBER    = "9966";

  private static final int    FIELD_LENGTH_DBANAME      = 30;
  private static final int    FIELD_LENGTH_DBAADD       = 30;
  private static final int    FIELD_LENGTH_DBACITY      = 13;
  private static final int    FIELD_LENGTH_PRINNAME     = 45;
  private static final int    FIELD_LENGTH_PRINTITLE    = 15;
  private static final int    FIELD_LENGTH_PRINADD      = 30;
  private static final int    FIELD_LENGTH_PRINCITY     = 13;
  private static final int    FIELD_LENGTH_CORPNAME     = 30;
  private static final int    FIELD_LENGTH_CORPADD      = 30;
  private static final int    FIELD_LENGTH_CORPADD2     = 30;
  private static final int    FIELD_LENGTH_CORPCITY     = 13;
  private static final int    FIELD_LENGTH_BANKACCT     = 17;
  private static final int    FIELD_LENGTH_URL          = 45;
  private static final int    FIELD_LENGTH_EMAIL        = 30;
  private static final int    FIELD_LENGTH_SALESREPID   = 15;

  private long                primaryKey                = 0L;
  private UserBean            user                      = null;
    
  private ResultSetIterator   it                        = null;

  private Timestamp           today                     = null;

  private String              businessDba               = "";
  private String              businessAddress           = "";
  private String              businessCity              = "";
  private String              businessState             = "";
  private String              businessZip               = "";
  private String              businessPhone             = "";
  private String              url                       = "";
  private String              email                     = "";
  private String              busType                   = "";
  private String              corporateName             = "";
  private String              corporateAddress          = "";
  private String              corporateAddressLine2     = "";
  private String              corporateCity             = "";
  private String              corporateState            = "";
  private String              corporateZip              = "";
  private String              yearsInBusiness           = "";
  private String              nonProfit                 = "";
  private String              taxId                     = "";
  private String              checkingAccountNum        = "";
  private String              transitRoute              = "";
  private String              merchantNumber            = "";
  private String              principalName             = "";
  private String              principalTitle            = "";
  private String              principalSsn              = "";
  private String              principalAddress          = "";          
  private String              principalCity             = "";
  private String              principalState            = "";
  private String              principalZip              = "";
  private String              annualSalesVolume         = "";
  private String              averageSaleAmount         = "";
  private String              moto                      = "";
  private String              mcc                       = "";
  private String              discountRate              = "";
  private String              discountPerItem           = "";
  private String              membershipFee             = "";
  private String              salesRepId                = "";
  private String              intermediateLevel2        = "";

  public  Vector              errors                    = new Vector();
   
  /*
  ** BbtDiscoverBean()
  */ 
  public BbtDiscoverBean()
  {
    super();
  }
  
  /*
  ** BbtDiscoverBean(DefaultContext Ctx) constructor
  **
  ** Latches on to passed default context for database interaction
  */
  public BbtDiscoverBean(DefaultContext defCtx)
  {
    super();
    
    if(defCtx != null)
    {
      Ctx = defCtx;
    }
  }

  public static boolean bbtDiscoverBean(long appSeqNum, UserBean user)
  {
    BbtDiscoverBean bdb = new BbtDiscoverBean();
    return bdb.doDiscoverSetup(appSeqNum, user);
  }
  
  public static boolean bbtDiscoverBean(long appSeqNum, UserBean user, DefaultContext Ctx)
  {
    BbtDiscoverBean bdb = new BbtDiscoverBean(Ctx);
    return bdb.doDiscoverSetup(appSeqNum, user);
  }

  private boolean doDiscoverSetup(long appSeqNum, UserBean user)
  {
    boolean success = false;
    
    try
    {
      connect();

      //set the primary key. all functions use this variable
      this.user       = user;
      this.primaryKey = appSeqNum;

      System.out.println("about to setup this stupid account: " + primaryKey);

      
      //this checks to see if discover is needed to be setup for this account..
      //it not, we just exit this bean like nothing happend.
      if(noDiscoverNeeded())
      {
        return true; //return true for success
      }

      
      //get all relevant information from application (doing any conversions if necessary) 
      //then stoer into discover table
      if(getAppContent())
      {
        System.out.println("Submitting Discover Application: " + primaryKey);
        success = submitAppContent();
      }
      
      //maybe use this in the future to put unsuccessful attempts into an exception queue telling them.. 
      //hey it aint going out for some reason.. either a database crash, or it didnt meet discovers validations.
      //but for now.. if it gets into the database.. we send it out.. we do no validation to discovers edits...
      //basically i dont care if it crashes.. thats their problem.. when it comes back it will end up in the error queue..
      //there the error will be explained and they can resend the file.. this time.. the edits will be in place... the reason for this
      //is that i dont want to setup an exception queue.. it doesnt seem that bbt likes queues.. so the less the better.. this way discover and
      //only discover does the validation.. well on the first pass it does.. then on the second pass.. i do a pre-validation.. using the existing discover rap bean..
      if(!success)
      {
        //QueueTools.insertQueue(appSeqNum, MesQueues.Q_MMS_EXCEPTION);
        //QueueNotes.addNote(appSeqNum, MesQueues.Q_MMS_EXCEPTION, "SYSTEM", exceptionQueueNote);
      }

    }
    catch(Exception e)
    {
      logEntry("doDiscoverSetup(" + primaryKey + ")", e.toString());
      success = false;
    }
    finally
    {
      cleanUp();
    }
    
    return success;
  }


  private boolean noDiscoverNeeded()
  {
    boolean             noDiscNeeded  = true;

    try
    {
      String discRate = null;
      
      /*@lineinfo:generated-code*//*@lineinfo:214^7*/

//  ************************************************************
//  #sql [Ctx] { select  merchpo_rate
//          
//          from    merchpayoption
//          where   app_seq_num   = :primaryKey and
//                  cardtype_code = 14
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merchpo_rate\n         \n        from    merchpayoption\n        where   app_seq_num   =  :1  and\n                cardtype_code = 14";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.BbtDiscoverBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   discRate = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:221^7*/
    
      noDiscNeeded = isBlank(discRate);
    }
    catch(Exception e)
    {
      noDiscNeeded = true;
      logEntry("trackRecordExists()", e.toString());
    }

    return noDiscNeeded;
  }

  /*
  ** METHOD public void getAppContent()
  **
  */
  public boolean getAppContent()
  {
    ResultSet   rs      = null;
    boolean     gotData = false;

    try
    {
          
      /*@lineinfo:generated-code*//*@lineinfo:246^7*/

//  ************************************************************
//  #sql [Ctx] { select    sysdate 
//          from      dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    sysdate  \n        from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.BbtDiscoverBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   today = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:250^7*/

      /*@lineinfo:generated-code*//*@lineinfo:252^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mr.merch_business_name              dba_name,
//                  add1.address_line1                  dba_address,
//                  add1.address_city                   dba_city,
//                  add1.countrystate_code              dba_state,
//                  add1.address_zip                    dba_zip,
//                  add1.address_phone                  dba_phone,
//                  mr.merch_web_url                    dba_url,
//                  mr.merch_email_address              dba_email,
//                  ( bus.busowner_first_name || ' ' || 
//                    bus.busowner_last_name )          principal_name,
//                  bus.busowner_ssn                    principal_ssn,
//                  bus.busowner_title                  principal_title,
//                  decode(add4.address_line1,     
//                         null, add1.address_line1,
//                         add4.address_line1 )         principal_address,
//                  decode(add4.address_city,      
//                         null, add1.address_city,
//                         add4.address_city )          principal_city,
//                  decode(add4.countrystate_code, 
//                         null, add1.countrystate_code, 
//                         add4.countrystate_code )     principal_state,
//                  decode(add4.address_zip,
//                         null, add1.address_zip,
//                         add4.address_zip )           principal_zip,
//                  mr.merch_federal_tax_id             federal_tax_id,
//                  decode(mr.merch_legal_name, 
//                         null, mr.merch_business_name, 
//                         mr.merch_legal_name )        corporate_name,
//                  add1.address_line1                  corporate_address,
//                  add1.address_line2                  corporate_address2,
//                  add1.address_city                   corporate_city,
//                  add1.countrystate_code              corporate_state,
//                  add1.address_zip                    corporate_zip,
//                  mr.sic_code                         merchant_cat_code,
//                  (100-mr.merch_mail_phone_sales)     moto_flag,
//                  pay.annual_sales                    annual_volume,
//                  pay.average_ticket                  average_ticket,
//                  mr.merch_years_at_loc               years_in_business,
//                  mr.bustype_code                     business_type,
//                  mr.business_nature                  business_nature,
//                  mr.discover_rep_id                  discover_rep_id,
//                  bank.merchbank_transit_route_num    transit_routing_num,
//                  bank.merchbank_acct_num             account_dda,
//                  mr.merch_number                     acquirer_merchant_num,
//                  pay.merchpo_rate                    discover_disc_rate,
//                  pay.merchpo_fee                     discover_per_item_rate,
//                  app.app_type                        app_type,
//                  decode( app.app_type,
//                          11, mr.merch_rep_code,  -- bb&t application
//                          app.app_user_login )        user_login,
//                  app.appsrctype_code                 intermediate_level2
//          from    merchant        mr,
//                  app_merch_bbt   amb,
//                  merchbank       bank,
//                  address         add1,
//                  address         add4,
//                  businessowner   bus,
//                  merchpayoption  pay,
//                  application     app
//          where   mr.app_seq_num   = :primaryKey           and
//                  mr.app_seq_num   = bank.app_seq_num      and
//                  mr.app_seq_num   = amb.app_seq_num       and
//                  mr.app_seq_num   = app.app_seq_num       and
//                  mr.app_seq_num   = add1.app_seq_num      and add1.addresstype_code  = 1    and
//                  (mr.app_seq_num  = add4.app_seq_num(+)   and 4 = add4.addresstype_code(+)) and
//                  mr.app_seq_num   = bus.app_seq_num       and bus.busowner_num       = 1    and
//                  mr.app_seq_num   = pay.app_seq_num(+)    and pay.cardtype_code      = 14
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mr.merch_business_name              dba_name,\n                add1.address_line1                  dba_address,\n                add1.address_city                   dba_city,\n                add1.countrystate_code              dba_state,\n                add1.address_zip                    dba_zip,\n                add1.address_phone                  dba_phone,\n                mr.merch_web_url                    dba_url,\n                mr.merch_email_address              dba_email,\n                ( bus.busowner_first_name || ' ' || \n                  bus.busowner_last_name )          principal_name,\n                bus.busowner_ssn                    principal_ssn,\n                bus.busowner_title                  principal_title,\n                decode(add4.address_line1,     \n                       null, add1.address_line1,\n                       add4.address_line1 )         principal_address,\n                decode(add4.address_city,      \n                       null, add1.address_city,\n                       add4.address_city )          principal_city,\n                decode(add4.countrystate_code, \n                       null, add1.countrystate_code, \n                       add4.countrystate_code )     principal_state,\n                decode(add4.address_zip,\n                       null, add1.address_zip,\n                       add4.address_zip )           principal_zip,\n                mr.merch_federal_tax_id             federal_tax_id,\n                decode(mr.merch_legal_name, \n                       null, mr.merch_business_name, \n                       mr.merch_legal_name )        corporate_name,\n                add1.address_line1                  corporate_address,\n                add1.address_line2                  corporate_address2,\n                add1.address_city                   corporate_city,\n                add1.countrystate_code              corporate_state,\n                add1.address_zip                    corporate_zip,\n                mr.sic_code                         merchant_cat_code,\n                (100-mr.merch_mail_phone_sales)     moto_flag,\n                pay.annual_sales                    annual_volume,\n                pay.average_ticket                  average_ticket,\n                mr.merch_years_at_loc               years_in_business,\n                mr.bustype_code                     business_type,\n                mr.business_nature                  business_nature,\n                mr.discover_rep_id                  discover_rep_id,\n                bank.merchbank_transit_route_num    transit_routing_num,\n                bank.merchbank_acct_num             account_dda,\n                mr.merch_number                     acquirer_merchant_num,\n                pay.merchpo_rate                    discover_disc_rate,\n                pay.merchpo_fee                     discover_per_item_rate,\n                app.app_type                        app_type,\n                decode( app.app_type,\n                        11, mr.merch_rep_code,  -- bb&t application\n                        app.app_user_login )        user_login,\n                app.appsrctype_code                 intermediate_level2\n        from    merchant        mr,\n                app_merch_bbt   amb,\n                merchbank       bank,\n                address         add1,\n                address         add4,\n                businessowner   bus,\n                merchpayoption  pay,\n                application     app\n        where   mr.app_seq_num   =  :1            and\n                mr.app_seq_num   = bank.app_seq_num      and\n                mr.app_seq_num   = amb.app_seq_num       and\n                mr.app_seq_num   = app.app_seq_num       and\n                mr.app_seq_num   = add1.app_seq_num      and add1.addresstype_code  = 1    and\n                (mr.app_seq_num  = add4.app_seq_num(+)   and 4 = add4.addresstype_code(+)) and\n                mr.app_seq_num   = bus.app_seq_num       and bus.busowner_num       = 1    and\n                mr.app_seq_num   = pay.app_seq_num(+)    and pay.cardtype_code      = 14";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.BbtDiscoverBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.BbtDiscoverBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:321^7*/

      rs = it.getResultSet();

      if(rs.next())
      {

        this.businessDba           = isBlank(rs.getString("DBA_NAME"))               ? "" : rs.getString("DBA_NAME");
        this.businessAddress       = isBlank(rs.getString("DBA_ADDRESS"))            ? "" : rs.getString("DBA_ADDRESS");
        this.businessCity          = isBlank(rs.getString("DBA_CITY"))               ? "" : rs.getString("DBA_CITY");
        this.businessState         = isBlank(rs.getString("DBA_STATE"))              ? "" : rs.getString("DBA_STATE");
        this.businessZip           = isBlank(rs.getString("DBA_ZIP"))                ? "" : rs.getString("DBA_ZIP");
        this.businessPhone         = isBlank(rs.getString("DBA_PHONE"))              ? "" : rs.getString("DBA_PHONE");
        this.url                   = isBlank(rs.getString("DBA_URL"))                ? "" : rs.getString("DBA_URL");
        this.email                 = isBlank(rs.getString("DBA_EMAIL"))              ? "" : rs.getString("DBA_EMAIL");
        this.principalName         = isBlank(rs.getString("PRINCIPAL_NAME"))         ? "" : rs.getString("PRINCIPAL_NAME").trim();
        this.principalTitle        = isBlank(rs.getString("PRINCIPAL_TITLE"))        ? "" : rs.getString("PRINCIPAL_TITLE");
        this.principalSsn          = isBlank(rs.getString("PRINCIPAL_SSN"))          ? "" : rs.getString("PRINCIPAL_SSN");
        this.principalSsn          = fixSsn(this.principalSsn);
        this.principalAddress      = isBlank(rs.getString("PRINCIPAL_ADDRESS"))      ? "" : rs.getString("PRINCIPAL_ADDRESS");
        this.principalCity         = isBlank(rs.getString("PRINCIPAL_CITY"))         ? "" : rs.getString("PRINCIPAL_CITY");
        this.principalState        = isBlank(rs.getString("PRINCIPAL_STATE"))        ? "" : rs.getString("PRINCIPAL_STATE");
        this.principalZip          = isBlank(rs.getString("PRINCIPAL_ZIP"))          ? "" : rs.getString("PRINCIPAL_ZIP");
        this.corporateName         = isBlank(rs.getString("CORPORATE_NAME"))         ? "" : rs.getString("CORPORATE_NAME");
        this.corporateAddress      = isBlank(rs.getString("CORPORATE_ADDRESS"))      ? "" : rs.getString("CORPORATE_ADDRESS");
        this.corporateAddressLine2 = isBlank(rs.getString("CORPORATE_ADDRESS2"))     ? "" : rs.getString("CORPORATE_ADDRESS2");
        this.corporateCity         = isBlank(rs.getString("CORPORATE_CITY"))         ? "" : rs.getString("CORPORATE_CITY");
        this.corporateState        = isBlank(rs.getString("CORPORATE_STATE"))        ? "" : rs.getString("CORPORATE_STATE");
        this.corporateZip          = isBlank(rs.getString("CORPORATE_ZIP"))          ? "" : rs.getString("CORPORATE_ZIP");
        this.yearsInBusiness       = isBlank(rs.getString("YEARS_IN_BUSINESS"))      ? "0" : rs.getString("YEARS_IN_BUSINESS");
        this.merchantNumber        = isBlank(rs.getString("ACQUIRER_MERCHANT_NUM"))  ? "" : rs.getString("ACQUIRER_MERCHANT_NUM");
        this.taxId                 = isBlank(rs.getString("FEDERAL_TAX_ID"))         ? "" : rs.getString("FEDERAL_TAX_ID");
        this.taxId                 = fixSsn(this.taxId);
        this.busType               = isBlank(rs.getString("BUSINESS_TYPE"))          ? "" : rs.getString("BUSINESS_TYPE");
        this.intermediateLevel2    = isBlank(rs.getString("intermediate_level2"))    ? "" : rs.getString("intermediate_level2");

        if(!isBlank(rs.getString("BUSINESS_TYPE")))
        {
          switch(rs.getInt("BUSINESS_TYPE"))
          {
            case 1: //sole proprietor
              this.busType    = "S";
              this.nonProfit  = "N";
              this.principalTitle = isBlank(this.principalTitle) ? "Owner" : this.principalTitle;
            break;
            case 2: //corporation
              this.busType    = "C";
              this.nonProfit  = "N";
              this.principalTitle = isBlank(this.principalTitle) ? "Officer" : this.principalTitle;
            break;
            case 3: //partnership
              this.busType    = "P";
              this.nonProfit  = "N";
              this.principalTitle = isBlank(this.principalTitle) ? "Partner" : this.principalTitle;
            break;
            case 6: //non profit
              this.busType    = "C";
              this.nonProfit  = "Y";
              this.principalTitle = isBlank(this.principalTitle) ? "Officer" : this.principalTitle;
            break;
          }
        }

        this.principalTitle        = isBlank(this.principalTitle)                    ? "Unknown"  : this.principalTitle;
        this.transitRoute          = isBlank(rs.getString("TRANSIT_ROUTING_NUM"))    ? ""         : rs.getString("TRANSIT_ROUTING_NUM");
        this.checkingAccountNum    = isBlank(rs.getString("ACCOUNT_DDA"))            ? ""         : rs.getString("ACCOUNT_DDA");
        this.mcc                   = isBlank(rs.getString("MERCHANT_CAT_CODE"))      ? ""         : rs.getString("MERCHANT_CAT_CODE");

        if(!isBlank(rs.getString("business_nature")))
        {
          if(rs.getInt("business_nature") == 6 || 
             rs.getInt("business_nature") == 5)
          {
            this.moto = "Y";
          }
          else
          {
            this.moto = "N";
          }
        }
    
        this.salesRepId             = isBlank(rs.getString("user_login"))             ? ""    : rs.getString("user_login");
        this.discountRate           = isBlank(rs.getString("DISCOVER_DISC_RATE"))     ? ""    : rs.getString("DISCOVER_DISC_RATE");


        this.averageSaleAmount      = isBlank(rs.getString("AVERAGE_TICKET"))         ? ""    : rs.getString("AVERAGE_TICKET");
        this.annualSalesVolume      = isBlank(rs.getString("ANNUAL_VOLUME"))          ? ""    : rs.getString("ANNUAL_VOLUME");

        //not setting up a membership fee for discover
        this.membershipFee          = "0";
        //setup ten cent per item fee
        this.discountPerItem        = isBlank(rs.getString("DISCOVER_per_item_RATE")) ? ".10" : rs.getString("DISCOVER_per_item_RATE");

        it.close();

        //all data was retrieved successfully
        gotData = true;
      }
      
      if(!gotData)
      {
        addError("Error: Data could not be retrieved for this merchant! Contact Admin");
      }
    }
    catch(Exception e)
    {
      logEntry("getAppContent()", e.toString());
      addError("Error: Complications occurred while retrieving data! Contact Admin");
      gotData = false;
    }

    return gotData;
  }

  /*
  ** METHOD public boolean submitAppContent()
  **
  */
  public boolean submitAppContent()
  {
    boolean submitted = true;
    
    try
    {
      //this makes sure no data element is too long to fit into merchant_discover_app table
      validate();

      if(!recordExists())
      {

        /*@lineinfo:generated-code*//*@lineinfo:451^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_discover_app
//            ( 
//              APP_SEQ_NUM,
//              APP_DATE,
//              PARTNER_NUM,
//              DBA_NAME,
//              DBA_ADDRESS,
//              DBA_CITY,
//              DBA_STATE,
//              DBA_ZIP,
//              DBA_PHONE,
//              DBA_URL,
//              DBA_EMAIL,
//              PRINCIPAL_NAME,
//              PRINCIPAL_TITLE,
//              PRINCIPAL_SSN,
//              PRINCIPAL_ADDRESS,
//              PRINCIPAL_CITY,
//              PRINCIPAL_STATE,
//              PRINCIPAL_ZIP,
//              CORPORATE_NAME,
//              CORPORATE_ADDRESS,
//              CORPORATE_ADDRESS2,
//              CORPORATE_CITY,
//              CORPORATE_STATE,
//              CORPORATE_ZIP,
//              YEARS_IN_BUSINESS,
//              ACQUIRER_MERCHANT_NUM,
//              FEDERAL_TAX_ID,
//              NON_PROFIT_FLAG,
//              BUSINESS_TYPE,
//              TRANSIT_ROUTING_NUM,
//              ACCOUNT_DDA,
//              DISCOVER_DISC_RATE,
//              DISCOVER_PER_ITEM_RATE,
//              MEMBERSHIP_FEE,
//              MERCHANT_CAT_CODE,
//              MOTO_FLAG,
//              AVERAGE_SALE,
//              ANNUAL_SALES,
//              TRANSMISSION_METHOD,
//              SALES_REP_ID,
//              intermediate_level2,
//              request_type,
//              submitted_by
//            )
//            values
//            ( 
//              :primaryKey,
//              :today,
//              :getPartnerNum(),
//              :businessDba,
//              :businessAddress,
//              :businessCity,
//              :businessState,
//              :businessZip,
//              :businessPhone,
//              :url,
//              :email,
//              :principalName,
//              :principalTitle,
//              :principalSsn,
//              :principalAddress,
//              :principalCity,
//              :principalState,
//              :principalZip,
//              :corporateName,
//              :corporateAddress,
//              :corporateAddressLine2,
//              :corporateCity,
//              :corporateState,
//              :corporateZip,
//              :yearsInBusiness,
//              :merchantNumber,
//              :taxId,
//              :nonProfit,
//              :busType,
//              :transitRoute,
//              :checkingAccountNum,
//              :discountRate,
//              :discountPerItem,
//              :membershipFee,
//              :mcc,
//              :moto,
//              :averageSaleAmount,
//              :annualSalesVolume,
//              'A',
//              :salesRepId,
//              :intermediateLevel2,
//              'New',
//              :user.getUserId()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_716 = getPartnerNum();
 long __sJT_717 = user.getUserId();
   String theSqlTS = "insert into merchant_discover_app\n          ( \n            APP_SEQ_NUM,\n            APP_DATE,\n            PARTNER_NUM,\n            DBA_NAME,\n            DBA_ADDRESS,\n            DBA_CITY,\n            DBA_STATE,\n            DBA_ZIP,\n            DBA_PHONE,\n            DBA_URL,\n            DBA_EMAIL,\n            PRINCIPAL_NAME,\n            PRINCIPAL_TITLE,\n            PRINCIPAL_SSN,\n            PRINCIPAL_ADDRESS,\n            PRINCIPAL_CITY,\n            PRINCIPAL_STATE,\n            PRINCIPAL_ZIP,\n            CORPORATE_NAME,\n            CORPORATE_ADDRESS,\n            CORPORATE_ADDRESS2,\n            CORPORATE_CITY,\n            CORPORATE_STATE,\n            CORPORATE_ZIP,\n            YEARS_IN_BUSINESS,\n            ACQUIRER_MERCHANT_NUM,\n            FEDERAL_TAX_ID,\n            NON_PROFIT_FLAG,\n            BUSINESS_TYPE,\n            TRANSIT_ROUTING_NUM,\n            ACCOUNT_DDA,\n            DISCOVER_DISC_RATE,\n            DISCOVER_PER_ITEM_RATE,\n            MEMBERSHIP_FEE,\n            MERCHANT_CAT_CODE,\n            MOTO_FLAG,\n            AVERAGE_SALE,\n            ANNUAL_SALES,\n            TRANSMISSION_METHOD,\n            SALES_REP_ID,\n            intermediate_level2,\n            request_type,\n            submitted_by\n          )\n          values\n          ( \n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 ,\n             :34 ,\n             :35 ,\n             :36 ,\n             :37 ,\n             :38 ,\n            'A',\n             :39 ,\n             :40 ,\n            'New',\n             :41 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.BbtDiscoverBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setTimestamp(2,today);
   __sJT_st.setString(3,__sJT_716);
   __sJT_st.setString(4,businessDba);
   __sJT_st.setString(5,businessAddress);
   __sJT_st.setString(6,businessCity);
   __sJT_st.setString(7,businessState);
   __sJT_st.setString(8,businessZip);
   __sJT_st.setString(9,businessPhone);
   __sJT_st.setString(10,url);
   __sJT_st.setString(11,email);
   __sJT_st.setString(12,principalName);
   __sJT_st.setString(13,principalTitle);
   __sJT_st.setString(14,principalSsn);
   __sJT_st.setString(15,principalAddress);
   __sJT_st.setString(16,principalCity);
   __sJT_st.setString(17,principalState);
   __sJT_st.setString(18,principalZip);
   __sJT_st.setString(19,corporateName);
   __sJT_st.setString(20,corporateAddress);
   __sJT_st.setString(21,corporateAddressLine2);
   __sJT_st.setString(22,corporateCity);
   __sJT_st.setString(23,corporateState);
   __sJT_st.setString(24,corporateZip);
   __sJT_st.setString(25,yearsInBusiness);
   __sJT_st.setString(26,merchantNumber);
   __sJT_st.setString(27,taxId);
   __sJT_st.setString(28,nonProfit);
   __sJT_st.setString(29,busType);
   __sJT_st.setString(30,transitRoute);
   __sJT_st.setString(31,checkingAccountNum);
   __sJT_st.setString(32,discountRate);
   __sJT_st.setString(33,discountPerItem);
   __sJT_st.setString(34,membershipFee);
   __sJT_st.setString(35,mcc);
   __sJT_st.setString(36,moto);
   __sJT_st.setString(37,averageSaleAmount);
   __sJT_st.setString(38,annualSalesVolume);
   __sJT_st.setString(39,salesRepId);
   __sJT_st.setString(40,intermediateLevel2);
   __sJT_st.setLong(41,__sJT_717);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:545^9*/
      }
      insertData();
      updateTransmissionStatus();
    }
    catch(Exception e)
    {
      submitted = false;
      logEntry("submitAppContent()", e.toString());
      addError("Error: Complications occurred while storing data!  Contact Admin.");
    }

    return submitted;
  }

  //gets partner num if apptype has their own partner num in the merchant_discover_app_partners table 
  //or else uses mes partner num (default)
  private String getPartnerNum()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    String              partnerNum  = DEFAULT_PARTNER_NUMBER;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:570^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mdap.partner_num 
//          from    MERCHANT_DISCOVER_APP_PARTERS mdap, 
//                  application app
//          where   app.app_seq_num = :primaryKey and 
//                  app.app_type    = mdap.apptype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mdap.partner_num \n        from    MERCHANT_DISCOVER_APP_PARTERS mdap, \n                application app\n        where   app.app_seq_num =  :1  and \n                app.app_type    = mdap.apptype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.BbtDiscoverBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.BbtDiscoverBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:577^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        partnerNum = isBlank(rs.getString("partner_num")) ? DEFAULT_PARTNER_NUMBER : rs.getString("partner_num");
      }
 
      it.close();
    }
    catch(Exception e)
    {
      partnerNum = DEFAULT_PARTNER_NUMBER;
      logEntry("getPartnerNum()", e.toString());
    }

    return partnerNum;
  }


  public void insertData()
  {

    try
    {

      if(!completedRecordExists()) //then we insert it into database, so it will be uploaded in file
      {
       
        /*@lineinfo:generated-code*//*@lineinfo:607^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_discover_app_complete
//            ( 
//              APP_SEQ_NUM,
//              DATE_COMPLETED
//            )
//            
//            values
//            ( 
//              :primaryKey,
//              :today
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant_discover_app_complete\n          ( \n            APP_SEQ_NUM,\n            DATE_COMPLETED\n          )\n          \n          values\n          ( \n             :1 ,\n             :2 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ops.BbtDiscoverBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setTimestamp(2,today);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:620^9*/
    
      }
    }
    catch(Exception e)
    {
      logEntry("insertData()", e.toString());
    }
  }

  private void updateTransmissionStatus()
  {
    try
    {
      int status    = QueueConstants.DEPT_STATUS_DISCOVER_RAP_TRANSMISSION;
      int deptCode  = QueueConstants.DEPARTMENT_DISCOVER_RAP;
      
      if(!trackRecordExists(deptCode))
      {
        /*@lineinfo:generated-code*//*@lineinfo:639^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_tracking
//            (
//              app_seq_num,
//              dept_code,
//              status_code
//            )
//            values
//            (
//              :primaryKey,
//              :deptCode,
//              :status
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_tracking\n          (\n            app_seq_num,\n            dept_code,\n            status_code\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.ops.BbtDiscoverBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,deptCode);
   __sJT_st.setInt(3,status);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:653^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("updateTransmissionStatus()", e.toString());
    }
  }

  private boolean completedRecordExists()
  {
    ResultSetIterator   it          = null;
    boolean             isCompleted = false;

    try
    {
      int appCount = 0;
     
      /*@lineinfo:generated-code*//*@lineinfo:671^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant_discover_app_complete
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant_discover_app_complete\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.BbtDiscoverBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:677^7*/
    
      isCompleted = appCount > 0;
  
    }
    catch(Exception e)
    {
      isCompleted = false;
      logEntry("completedRecordExists()", e.toString());
    }

    return isCompleted;
  }

  private boolean trackRecordExists(int dept)
  {
    ResultSetIterator   it              = null;
    boolean             trackRecExists  = false;

    try
    {
      int appCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:700^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    app_tracking
//          where   app_seq_num   = :primaryKey and
//                  dept_code     = :dept
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    app_tracking\n        where   app_seq_num   =  :1  and\n                dept_code     =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.BbtDiscoverBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,dept);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:707^7*/
    
      trackRecExists = appCount > 0;
    }
    catch(Exception e)
    {
      trackRecExists = false;
      logEntry("trackRecordExists()", e.toString());
    }

    return trackRecExists;
  }

  private boolean recordExists()
  {
    boolean recExists = false;

    try
    {
      
      int appCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:729^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant_discover_app
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant_discover_app\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.BbtDiscoverBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:735^7*/
      
      recExists = appCount > 0;
    }
    catch(Exception e)
    {
      recExists = false;
      logEntry("recordExists()", e.toString());
    }

    return recExists;
  }

  /*
  **VALIDATION
  */  
  public void validate()
  {
     if(!isBlank(businessDba) && businessDba.length() > FIELD_LENGTH_DBANAME)
     {
       businessDba = businessDba.substring(0,FIELD_LENGTH_DBANAME);
     }

     if(!isBlank(businessAddress) && businessAddress.length() > FIELD_LENGTH_DBAADD)
     {
       businessAddress = businessAddress.substring(0,FIELD_LENGTH_DBAADD);
     }

     if(!isBlank(businessCity) && businessCity.length() > FIELD_LENGTH_DBACITY)
     {
       businessCity = businessCity.substring(0,FIELD_LENGTH_DBACITY);
     }
     
     if(!isBlank(businessZip) && businessZip.length() > 5)
     {
       businessZip = businessZip.substring(0,5);
     }
     
     if(!isBlank(email) && email.length() > FIELD_LENGTH_EMAIL)
     {
       email = email.substring(0,FIELD_LENGTH_EMAIL);
     }

     if(!isBlank(corporateName) && corporateName.length() > FIELD_LENGTH_CORPNAME)
     {
       corporateName = corporateName.substring(0,FIELD_LENGTH_CORPNAME);
     }

     if(!isBlank(corporateAddress) && corporateAddress.length() > FIELD_LENGTH_CORPADD)
     {
       corporateAddress = corporateAddress.substring(0,FIELD_LENGTH_CORPADD);
     }
     
     if(!isBlank(corporateAddressLine2) && corporateAddressLine2.length() > FIELD_LENGTH_CORPADD2)
     {
       corporateAddressLine2 = corporateAddressLine2.substring(0,FIELD_LENGTH_CORPADD2);
     }

     if(!isBlank(corporateCity) && corporateCity.length() > FIELD_LENGTH_CORPCITY)
     {
       corporateCity = corporateCity.substring(0,FIELD_LENGTH_CORPCITY);
     }

     if(!isBlank(corporateZip) && corporateZip.length() > 5)
     {
       corporateZip = corporateZip.substring(0,5);
     }
    
     if(!isBlank(checkingAccountNum) && checkingAccountNum.length() > FIELD_LENGTH_BANKACCT)
     {
       checkingAccountNum = checkingAccountNum.substring(0,FIELD_LENGTH_BANKACCT);
     }

     if(!isBlank(principalName) && principalName.length() > FIELD_LENGTH_PRINNAME)
     {
       principalName = principalName.substring(0,FIELD_LENGTH_PRINNAME);
     }

     if(!isBlank(principalTitle) && principalTitle.length() > FIELD_LENGTH_PRINTITLE)
     {
       principalTitle = principalTitle.substring(0,FIELD_LENGTH_PRINTITLE);
     }

     if(!isBlank(principalAddress) && principalAddress.length() > FIELD_LENGTH_PRINADD)
     {
       principalAddress = principalAddress.substring(0,FIELD_LENGTH_PRINADD);
     }
     
     if(!isBlank(principalCity) && principalCity.length() > FIELD_LENGTH_PRINCITY)
     {
       principalCity = principalCity.substring(0,FIELD_LENGTH_PRINCITY);
     }

     if(!isBlank(principalZip) && principalZip.length() > 5)
     {
       principalZip = principalZip.substring(0,5);
     }
     
     if(!isBlank(salesRepId) && salesRepId.length() > FIELD_LENGTH_SALESREPID)
     {
       salesRepId = salesRepId.substring(0,FIELD_LENGTH_SALESREPID);
     }

     if(!isBlank(url) && url.length() > FIELD_LENGTH_URL)
     {
       url = url.substring(0,FIELD_LENGTH_URL);
     }
  }

  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }

  public void addError(String err)
  {
    try
    {
      errors.add(err);
    }
    catch(Exception e)
    {
    }
  } 

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
  

  public boolean isEmail(String test)
  {
    boolean pass = false;
    
    int firstAt = test.indexOf('@');
    if (!isBlank(test) && firstAt > 0 && 
        test.indexOf('@',firstAt + 1) == -1)
    {
      pass = true;
    }
    
    return pass;
  }

  private String fixSsn(String ssn)
  {
    String result = ssn;

    if(isBlank(ssn))
    {
      return "";
    }
    else if(ssn.length() == 8)
    {
      result = "0" + ssn;
    }
    else if(ssn.length() == 7)
    {
      result = "00" + ssn;
    }
    else if(ssn.length() == 6)
    {
      result = "000" + ssn;
    }

    return result;
  }

}/*@lineinfo:generated-code*/