/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/MerchantDiscountBean.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-12 12:14:18 -0700 (Mon, 12 Mar 2007) $
  Version            : $Revision: 13531 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

public class MerchantDiscountBean extends ExpandDataBean
{
  private CardInfo visa           = null;
  private CardInfo visaCheck      = null;
  private CardInfo mc             = null;
  private CardInfo diners         = null;
  private CardInfo discover       = null;
  private CardInfo amex           = null;
  private CardInfo jcb            = null;
  private CardInfo debit          = null;
  private CardInfo privateLabel1  = null;
  private CardInfo ebt            = null;
  private CardInfo mcCheck        = null;
  private CardInfo visaBusiness   = null;
  private CardInfo mcBusiness     = null;

  private final int NUM_CARDS     = 13;
  public  final int[] cards       = new int[NUM_CARDS];

  public Vector  discTypes        = new Vector();
  public Vector  discTypesDesc    = new Vector();
  public Vector  discMethods      = new Vector();
  public Vector  discMethodsDesc  = new Vector();

  private boolean cbt             = false;
  private boolean transcom        = false;

  public MerchantDiscountBean()
  {
    visa          = new CardInfo(mesConstants.APP_CT_VISA,            "Visa");
    visaCheck     = new CardInfo(mesConstants.APP_CT_VISA_CHECK_CARD, "Visa Check");
    mc            = new CardInfo(mesConstants.APP_CT_MC,              "MC");
    diners        = new CardInfo(mesConstants.APP_CT_DINERS_CLUB,     "Diners");
    discover      = new CardInfo(mesConstants.APP_CT_DISCOVER,        "Discover");
    amex          = new CardInfo(mesConstants.APP_CT_AMEX,            "Amex");
    jcb           = new CardInfo(mesConstants.APP_CT_JCB,             "JCB");
    debit         = new CardInfo(mesConstants.APP_CT_DEBIT,           "Debit");
    privateLabel1 = new CardInfo(mesConstants.APP_CT_PRIVATE_LABEL_1, "Private Label 1");
    ebt           = new CardInfo(mesConstants.APP_CT_EBT,             "EBT");
    mcCheck       = new CardInfo(mesConstants.APP_CT_MC_CHECK_CARD,   "MC Check");
    visaBusiness  = new CardInfo(mesConstants.APP_CT_VISA_BUSINESS_CARD,  "Visa Business");
    mcBusiness    = new CardInfo(mesConstants.APP_CT_MC_BUSINESS_CARD,    "MC Business");
    
    cards[0]  = mesConstants.APP_CT_VISA;
    cards[1]  = mesConstants.APP_CT_VISA_CHECK_CARD;
    cards[2]  = mesConstants.APP_CT_VISA_BUSINESS_CARD;
    cards[3]  = mesConstants.APP_CT_MC;
    cards[4]  = mesConstants.APP_CT_MC_CHECK_CARD;
    cards[5]  = mesConstants.APP_CT_MC_BUSINESS_CARD;
    cards[6]  = mesConstants.APP_CT_DINERS_CLUB;
    cards[7]  = mesConstants.APP_CT_DISCOVER;
    cards[8]  = mesConstants.APP_CT_AMEX;
    cards[9]  = mesConstants.APP_CT_JCB;
    cards[10] = mesConstants.APP_CT_DEBIT;
    cards[11] = mesConstants.APP_CT_PRIVATE_LABEL_1;
    cards[12] = mesConstants.APP_CT_EBT;

    discTypes.add("");
    discTypes.add("G");
    discTypes.add("N");
    discTypes.add("*");
    discTypesDesc.add("select");
    discTypesDesc.add("G (gross)");
    discTypesDesc.add("N (net)");
    discTypesDesc.add("* (default)");

    discMethods.add("");
    discMethods.add("F");
    discMethods.add("V");
    discMethods.add("P");
    discMethods.add("S");
    discMethods.add("A");

    discMethodsDesc.add("select");
    discMethodsDesc.add("F (fixed)");
    discMethodsDesc.add("V (variable)");
    discMethodsDesc.add("P (variable by plan)");
    discMethodsDesc.add("S (variable by assoc vol for 1 card)");
    discMethodsDesc.add("A (variable by assoc vol)");
  }

  public void getData(long primaryKey)
  {
    setCbt1(primaryKey); //checks to see if is cbt application
    getCardInfo(primaryKey);
  }

  private void setCbt1(long primaryKey)  
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;
    CardInfo          card    = null;

    try 
    {
      ps = getPreparedStatement("select app_type from application where app_seq_num = ? ");
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      if(rs.next())
      {
        if(rs.getInt("app_type") == com.mes.constants.mesConstants.APP_TYPE_CBT ||
           rs.getInt("app_type") == com.mes.constants.mesConstants.APP_TYPE_CBT_NEW)
        {
          this.cbt = true;
        }
        else if(rs.getInt("app_type") == com.mes.constants.mesConstants.APP_TYPE_TRANSCOM)
        {
          this.transcom = true;
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setCbt: " + e.toString());
      addError("setCbt: " + e.toString());
    }
  }

  private void getCardInfo(long primaryKey)  
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;
    CardInfo          card    = null;

    try 
    {
      ps = getPreparedStatement("select * from billcard where app_seq_num = ? ");
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      if(rs.next())
      {
        do
        {
 
          card = getCard(rs.getInt("cardtype_code"));
          if(card != null)
          {
            card.setCardSelected(true);
            card.setCardDiscType( (isBlank(rs.getString("billcard_disc_type"))      ? "" : rs.getString("billcard_disc_type")) );
            card.setCardDiscRate( (isBlank(rs.getString("billcard_disc_rate"))      ? "0.00" : rs.getString("billcard_disc_rate")) );
            card.setCardDrtTable( (isBlank(rs.getString("billcard_drt_number"))     ? "" : rs.getString("billcard_drt_number")) );
            card.setCardDiscMethod( (isBlank(rs.getString("billcard_disc_method"))  ? "" : rs.getString("billcard_disc_method")) );
            card.setCardPerItem( (isBlank(rs.getString("billcard_peritem_fee"))     ? "0.00" : rs.getString("billcard_peritem_fee")) );

            //card.setCardMinDisc( (isBlank(rs.getString("billcard_disc_min"))      ? "" : rs.getString("billcard_disc_min")) );
            card.setCardMinDisc("0000");
            card.setCardFloorLim( (isBlank(rs.getString("billcard_flr_limit"))      ? "0000" : rs.getString("billcard_flr_limit")) );

            card.setCardAchOption( (isBlank(rs.getString("billcard_ach_option"))        ? "" : rs.getString("billcard_ach_option")) );
            card.setCardAuthOption( (isBlank(rs.getString("billcard_auth_option"))      ? "" : rs.getString("billcard_auth_option")) );
          }
        }while(rs.next());
      }
      else
      {
        ps = getPreparedStatement("select * from tranchrg where app_seq_num = ? ");
        ps.setLong(1, primaryKey);

        rs = ps.executeQuery();
        if(rs.next())
        {
          do
          {
            card = getCard(rs.getInt("cardtype_code"));
            if(card != null)
            {
              card.setCardSelected(true);
              card.setCardDiscType("G");
              //card.setCardMinDisc( (isBlank(rs.getString("tranchrg_mmin_chrg")) ? "" : rs.getString("tranchrg_mmin_chrg")) );
              card.setCardMinDisc("0000");
              card.setCardFloorLim("0000");
              card.setCardDiscRate( (isBlank(rs.getString("tranchrg_disc_rate")) ? "0.00" : rs.getString("tranchrg_disc_rate")) );
                     
              if(isBlank(rs.getString("tranchrg_float_disc_flag")) || rs.getString("tranchrg_float_disc_flag").equals("N"))
              {
                card.setCardDiscMethod("F");
              }
              else
              {
                card.setCardDiscMethod("V");
              }
            
              if(isBlank(rs.getString("tranchrg_discrate_type"))) // not visa or mc take per item fee
              {
                if(rs.getInt("cardtype_code") == mesConstants.APP_CT_EBT)
                {
                  card.setCardPerItem( (isBlank(rs.getString("tranchrg_per_tran")) ? "0.00" : rs.getString("tranchrg_per_tran")) );
                }
                else
                {
                  card.setCardPerItem("0.00");
                }
              }
              else
              {
                if(isCbt() || isTranscom())
                {
                  card.setCardPerItem( (isBlank(rs.getString("tranchrg_per_tran")) ? "0.00" : rs.getString("tranchrg_per_tran")) );
                }
                else
                {
                  card.setCardPerItem( (isBlank(rs.getString("tranchrg_pass_thru")) ? "" : rs.getString("tranchrg_pass_thru")) );
                }
              }
              
              //if(card.getCardType() == mesConstants.APP_CT_VISA || card.getCardType() == mesConstants.APP_CT_MC)
              //{
                  card.setCardDrtTable("9999");                              
              //}
              //else
              //{
                //card.setCardDrtTable("0000");
              //}
            }
          }while(rs.next());
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCardInfo: " + e.toString());
      addError("getCardInfo: " + e.toString());
    }
  }
  
  public void submitData(HttpServletRequest req, long primaryKey)
  {

    StringBuffer      qs      = new StringBuffer();
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    CardInfo          card    = null;

    try 
    {

      ps = getPreparedStatement("delete from billcard where app_seq_num = ? ");
      ps.setLong(1, primaryKey);
      ps.executeUpdate();
    
      qs.append("insert into billcard (");
      qs.append("billcard_disc_type, ");
      qs.append("billcard_disc_method, ");
      qs.append("billcard_drt_number, ");
      qs.append("billcard_disc_rate, ");
      qs.append("billcard_peritem_fee, ");
      qs.append("billcard_disc_min, ");
      qs.append("billcard_flr_limit, ");
      qs.append("billcard_ach_option, ");
      qs.append("billcard_auth_option, ");
      qs.append("cardtype_code, ");
      qs.append("app_seq_num, billcard_sr_number) values (?,?,?,?,?,?,?,?,?,?,?,?) ");
    
      ps = getPreparedStatement(qs.toString());

    
      for(int i=0; i<cards.length; i++)
      {
        card = getCard(cards[i]);
        if(card.getCardSelected())
        {
          ps.setString(1,card.getCardDiscType());
          ps.setString(2,card.getCardDiscMethod());
          ps.setString(3,card.getCardDrtTable());
          ps.setString(4,card.getCardDiscRate());
          ps.setString(5,card.getCardPerItem());
          ps.setString(6,card.getCardMinDisc());
          ps.setString(7,card.getCardFloorLim());
          ps.setString(8,card.getCardAchOption());
          ps.setString(9,card.getCardAuthOption());
          ps.setInt(10,  card.getCardType());
          ps.setLong(11,primaryKey);
          ps.setInt(12,i);
          ps.executeUpdate();
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
      addError("submitData: " + e.toString());
    }
  }
  
  public void updateData(HttpServletRequest req)
  {
    CardInfo card = null;

    for(int i=0; i<cards.length; i++)
    {
      if(!isBlank(req.getParameter("cardSelected" + cards[i])))
      {
        card = getCard(cards[i]);
        card.setCardSelected(true);
        card.setCardDiscType(   (isBlank(req.getParameter("discType"   + cards[i]))    ? "" : req.getParameter("discType"     + cards[i])) );
        card.setCardDiscRate(   (isBlank(req.getParameter("discRate"   + cards[i]))    ? "" : req.getParameter("discRate"     + cards[i])) );
        card.setCardDrtTable(   (isBlank(req.getParameter("drtTable"   + cards[i]))    ? "" : req.getParameter("drtTable"     + cards[i])) );
        card.setCardDiscMethod( (isBlank(req.getParameter("discMethod" + cards[i]))    ? "" : req.getParameter("discMethod"   + cards[i])) );
        card.setCardPerItem(    (isBlank(req.getParameter("perItem"    + cards[i]))    ? "" : req.getParameter("perItem"      + cards[i])) );
        card.setCardMinDisc(    (isBlank(req.getParameter("minDisc"    + cards[i]))    ? "0000" : req.getParameter("minDisc"  + cards[i])) );
        card.setCardFloorLim(   (isBlank(req.getParameter("floorLim"   + cards[i]))    ? "0000" : req.getParameter("floorLim" + cards[i])) );
        card.setCardAchOption(  (isBlank(req.getParameter("achOption"  + cards[i]))    ? "" : req.getParameter("achOption"    + cards[i])) );
        card.setCardAuthOption( (isBlank(req.getParameter("authOption" + cards[i]))    ? "" : req.getParameter("authOption"   + cards[i])) );


      }
    }
  }
  
  public boolean validate()
  {
    //DecimalFormat   five        = new DecimalFormat("00.000");
    //DecimalFormat   six         = new DecimalFormat("0000.00");
    // DecimalFormat   four        = new DecimalFormat("0000");
    //user4.append(salesFormat.format(ccSales));
    
    float           tempFloat   = 0.0f;
    int             tempInt     = 0;
    

    CardInfo card = null;

    for(int i=0; i<cards.length; i++)
    {
      card = getCard(cards[i]);
      if(card.getCardSelected())
      {
        
        if(isBlank(card.getCardDiscType()))
        {
          addError("Please select a Disc Type for " + card.getCardPlan());
        }
        
        if(!isBlank(card.getCardDiscRate()))
        {
          try
          {
            tempFloat = Float.parseFloat(card.getCardDiscRate());
            if(tempFloat >= 100.0 || tempFloat < 0.0)
            {
              addError("Discount Rate for " + card.getCardPlan() + " must be between 0 and 100 % ");
            }
          }
          catch(Exception e)
          {
            addError("Please provide a valid Discount Rate for " + card.getCardPlan());
          }
        }

        if(isBlank(card.getCardDrtTable()))
        {
          addError("Please select a DRT# for " + card.getCardPlan());
        }
        else
        {
          try
          {
            tempInt = Integer.parseInt(card.getCardDrtTable());
            if(tempInt < 0)
            {
              addError("DRT# for " + card.getCardPlan() + " must be greater than zero");
            }
          }
          catch(Exception e)
          {
            addError("Please provide a valid DRT# for " + card.getCardPlan());
          }
        }

        if(isBlank(card.getCardDiscMethod()))
        {
          addError("Please select a Disc Method for " + card.getCardPlan());
        }

        if(!isBlank(card.getCardPerItem()))
        {
          try
          {
            tempFloat = Float.parseFloat(card.getCardPerItem());
            if(tempFloat >= 100.0 || tempFloat < 0.0)
            {
              addError("Per Item Amount for " + card.getCardPlan() + " must be between 0 and 100 dollars ");
            }
          }
          catch(Exception e)
          {
            addError("Please provide a valid Per Item Amount for " + card.getCardPlan());
          }
        }

        if(!isBlank(card.getCardMinDisc()))
        {
          try
          {
            tempInt = Integer.parseInt(card.getCardMinDisc());
            if(tempInt >= 10000 || tempInt < 0)
            {
              addError("Min Disc for " + card.getCardPlan() + " must be between 0 and 10000 dollars ");
            }
          }
          catch(Exception e)
          {
            addError("Please provide a valid Min Disc for " + card.getCardPlan());
          }
        }

        if(isBlank(card.getCardAchOption()))
        {
          addError("Please select an Ach option for " + card.getCardPlan());
        }
        if(isBlank(card.getCardAuthOption()))
        {
          addError("Please select an Auth option for " + card.getCardPlan());
        }

        if(!isBlank(card.getCardFloorLim()))
        {
          try
          {
            tempInt = Integer.parseInt(card.getCardFloorLim());
            if(tempInt >= 10000 || tempInt < 0)
            {
              addError("Floor Limit for " + card.getCardPlan() + " must be between 0 and 10000 dollars ");
            }
          }
          catch(Exception e)
          {
            addError("Please provide a valid Floor Limit for " + card.getCardPlan());
          }
        }

      }
      else
      {
        if(!isBlank(card.getCardDiscType()) || !isBlank(card.getCardDrtTable()) || !isBlank(card.getCardDiscMethod()))
        {
          addError("Please select the " + card.getCardPlan() + " card plan check box or erase all data for this card plan.");
        }
      }
    }
    return(! hasErrors());
  }

  public boolean isCbt()
  {
    return this.cbt;
  }

  public boolean isTranscom()
  {
    return this.transcom;
  }

  public void setCbt(String temp)
  {
    this.cbt = true;
  }

  public CardInfo getCard(int cardType)
  {
    CardInfo curCard = null;
    
    switch(cardType)
    {
      case mesConstants.APP_CT_VISA:
        curCard = this.visa;
        break;
      case mesConstants.APP_CT_VISA_CHECK_CARD:
        curCard = this.visaCheck;
        break;
      case mesConstants.APP_CT_VISA_BUSINESS_CARD:
        curCard = this.visaBusiness;
        break;
      case mesConstants.APP_CT_MC:
        curCard = this.mc;
        break;
      case mesConstants.APP_CT_MC_CHECK_CARD:
        curCard = this.mcCheck;
        break;
      case mesConstants.APP_CT_MC_BUSINESS_CARD:
        curCard = this.mcBusiness;
        break;
      case mesConstants.APP_CT_DINERS_CLUB:
        curCard = this.diners;
        break;
      case mesConstants.APP_CT_DISCOVER:
        curCard = this.discover;
        break;
      case mesConstants.APP_CT_AMEX:
        curCard = this.amex;
        break;
      case mesConstants.APP_CT_JCB:
        curCard = this.jcb;
        break;
      case mesConstants.APP_CT_DEBIT:
        curCard = this.debit;
        break;
      case mesConstants.APP_CT_PRIVATE_LABEL_1:
        curCard = this.privateLabel1;
        break;
      case mesConstants.APP_CT_EBT:
        curCard = this.ebt;
        break;
    }
    return curCard;
  }
  
  public boolean getCardSelected(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardSelected());
  }
  public String getCardDiscType(int cardType)
  {
    String result = "";
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardDiscType());
  }
  public String getCardDiscRate(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardDiscRate());
  }
  public String getCardDrtTable(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardDrtTable());
  }
  public String getCardDiscMethod(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardDiscMethod());
  }
  public String getCardPerItem(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardPerItem());
  }
  public String getCardMinDisc(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardMinDisc());
  }
  public String getCardFloorLim(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardFloorLim());
  }
  
  public String getCardAchOption(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardAchOption());
  }
  public String getCardAuthOption(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardAuthOption());
  }

  public String getCardPlan(int cardType)
  {
    CardInfo curCard = getCard(cardType);
    return(curCard.getCardPlan());
  }

  public class CardInfo
  {
    private int cardType          = 0;
    private boolean cardSelected  = false;
    private String cardPlan       = "";
    private String discType       = "";
    private String discRate       = "";
    private String drtTable       = "";
    private String discMethod     = "";
    private String perItem        = "";
    private String minDisc        = "";
    private String floorLim       = "";
    private String achOption      = "";
    private String authOption     = "";

    CardInfo(int cardType, String cardPlan)
    {
      this.cardType = cardType;
      this.cardPlan = cardPlan;  
    }

    public void setCardDiscType(String discType)
    {
      this.discType = discType;  
    }
    public void setCardDiscRate(String discRate)
    {
      this.discRate = discRate;      
    }
    public void setCardDrtTable(String drtTable)
    {
      this.drtTable = drtTable;  
    }
    public void setCardDiscMethod(String discMethod)
    {
      this.discMethod = discMethod;
    }
    public void setCardPerItem(String perItem)
    {
      this.perItem = perItem;
    }
    public void setCardMinDisc(String minDisc)
    {
      this.minDisc = minDisc;
    }
    public void setCardFloorLim(String floorLim)
    {
      this.floorLim = floorLim;      
    }

    public void setCardAchOption(String achOption)
    {
      this.achOption = achOption;  
    }
    public void setCardAuthOption(String authOption)
    {
      this.authOption = authOption;      
    }


    public void setCardSelected(boolean cardSelected)
    {
      this.cardSelected = cardSelected;      
    }

    public boolean getCardSelected()
    {
      return this.cardSelected;  
    }

    public int getCardType()
    {
      return this.cardType;  
    }

    public String getCardAchOption()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.achOption;
      }
      return result;  
    }
    public String getCardAuthOption()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.authOption;
      }
      return result;  
    }


    public String getCardDiscType()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.discType;
      }
      return result;  
    }
    public String getCardDiscRate()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.discRate;
      }
      return result;  
    }
    public String getCardDrtTable()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.drtTable;
      }
      return result;  
    }
    public String getCardDiscMethod()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.discMethod;
      }
      return result;  
    }
    public String getCardPerItem()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.perItem;
      }
      return result;  
    }
    public String getCardMinDisc()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.minDisc;
      }
      return result;  
    }
    public String getCardFloorLim()
    {
      String result = "";
      if(this.cardSelected)
      {
        result = this.floorLim;
      }
      return result;  
    }
    public String getCardPlan()
    {
      return this.cardPlan;      
    }


  }



}
