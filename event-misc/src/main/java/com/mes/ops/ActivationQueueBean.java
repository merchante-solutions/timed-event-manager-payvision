/*@lineinfo:filename=ActivationQueueBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ActivationQueueBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/18/02 9:47a $
  Version            : $Revision: 10 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Locale;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class ActivationQueueBean extends SQLJConnectionBase
{

  public  Vector              queueList               = new Vector();
  private Timestamp           today                   = null;
  private long                midnight                = 999999999999999999L;

  public ActivationQueueBean()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:59^7*/

//  ************************************************************
//  #sql [Ctx] { select    sysdate 
//          from      dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    sysdate  \n        from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.ActivationQueueBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   today = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:63^7*/

    }
    catch(Exception e)
    {
      logEntry("constructdawg()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    //this code sets midnight with the millisecond representation of the very first millisecond of the current day..
    //this is used to see if scheduled appointments have lapsed yet or what...
    Calendar cal = Calendar.getInstance();
    cal.set(cal.get(cal.YEAR), cal.get(cal.MONTH), cal.get(cal.DAY_OF_MONTH), 0, 0, 0);  
    midnight = (cal.getTime()).getTime();

  }

  public int numQueueList()
  {
    return queueList.size();
  }

  private long getSeqNumFromMerchNum(String merchNum)
  {

    long appseqnum = -1L;

    try
    {

      connect();
     
      //moves request out of programming queue.. into completed queue
      /*@lineinfo:generated-code*//*@lineinfo:99^7*/

//  ************************************************************
//  #sql [Ctx] { select app_seq_num 
//          from   merchant
//          where  merch_number = :merchNum 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select app_seq_num  \n        from   merchant\n        where  merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.ActivationQueueBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appseqnum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:104^7*/
    }
    catch(Exception e)
    {
      logEntry("getSeqNumFromMerchNum()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return appseqnum;
  }

  public static void changeStage(int type, int stage, String merchNum, long requestId)
  {
    ActivationQueueBean  aqb = new ActivationQueueBean();
    long appSeqNum = aqb.getSeqNumFromMerchNum(merchNum);
    aqb.moveToStage(type, stage, appSeqNum, requestId);
  }


  public void moveToStage(int type, int stage, long appSeqNum, long requestId)
  {
    try
    {
      connect();
     
      if(stage == QueueConstants.Q_ACTIVATION_COMPLETE || 
         stage == QueueConstants.Q_DEPLOYMENT_COMPLETE || 
         stage == QueueConstants.Q_PROGRAMMING_COMPLETE)
      {
        /*@lineinfo:generated-code*//*@lineinfo:135^9*/

//  ************************************************************
//  #sql [Ctx] { update  temp_activation_queue
//  
//            set     q_stage         = :stage,
//                    date_completed  = sysdate
//  
//            where   app_seq_num     = :appSeqNum and 
//                    request_id      = :requestId  and
//                    q_type          = :type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  temp_activation_queue\n\n          set     q_stage         =  :1 ,\n                  date_completed  = sysdate\n\n          where   app_seq_num     =  :2  and \n                  request_id      =  :3   and\n                  q_type          =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.ActivationQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,stage);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setLong(3,requestId);
   __sJT_st.setInt(4,type);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:145^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:149^9*/

//  ************************************************************
//  #sql [Ctx] { update  temp_activation_queue
//  
//            set     q_stage   = :stage
//  
//            where   app_seq_num = :appSeqNum and 
//                    request_id = :requestId  and
//                    q_type = :type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  temp_activation_queue\n\n          set     q_stage   =  :1 \n\n          where   app_seq_num =  :2  and \n                  request_id =  :3   and\n                  q_type =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.ActivationQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,stage);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setLong(3,requestId);
   __sJT_st.setInt(4,type);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:158^9*/
      }
      //ONLY IF IN PROGRAMMING QUEUE DO WE WANT TO MOVE OUT OF MMS
      if(type == QueueConstants.QUEUE_PROGRAMMING)
      {
        moveOutOfMmsQueue(appSeqNum);
      }

    }
    catch(Exception e)
    {
      logEntry("moveToStage()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void moveOutOfMmsQueue(long appSeqNum)
  {
    boolean             useCleanUp = false;
    ResultSetIterator   it         = null;
    ResultSet           rs         = null;

    
    try
    {
      
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
     
      //check to see if all requests came back successfull
      /*@lineinfo:generated-code*//*@lineinfo:194^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   * 
//  
//          from     mms_stage_info  
//  
//          where    app_seq_num = :appSeqNum and process_response != 'success'
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   * \n\n        from     mms_stage_info  \n\n        where    app_seq_num =  :1  and process_response != 'success'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.ActivationQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.ActivationQueueBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:202^7*/
    
      rs = it.getResultSet();

      if(!rs.next())
      {

        //if all requests have come back with success status.. then we move item out of mms queue.. 
        //this is clean up of previous step
        /*@lineinfo:generated-code*//*@lineinfo:211^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_queue
//  
//            set     app_queue_stage   = :QueueConstants.Q_MMS_COMPLETED
//  
//            where   app_seq_num = :appSeqNum and app_queue_type = :QueueConstants.QUEUE_MMS
//  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_queue\n\n          set     app_queue_stage   =  :1 \n\n          where   app_seq_num =  :2  and app_queue_type =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ops.ActivationQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.Q_MMS_COMPLETED);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,QueueConstants.QUEUE_MMS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:220^9*/

        //then we time stamp mms completed on account status page
        /*@lineinfo:generated-code*//*@lineinfo:223^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//  
//            set     date_completed   = :today,
//                    status_code      = 302
//  
//            where   app_seq_num = :appSeqNum and dept_code = :QueueConstants.DEPARTMENT_MMS
//  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n\n          set     date_completed   =  :1 ,\n                  status_code      = 302\n\n          where   app_seq_num =  :2  and dept_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.ops.ActivationQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_MMS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:233^9*/


      }
    }
    catch(Exception e)
    {
      logEntry("moveOutOfMmsQueue()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }

  public void getQueueData(int type, int stage, boolean special)
  {
    //if its not programming.. we send it to the other get data method... 
    if(type != QueueConstants.QUEUE_PROGRAMMING)
    {
      getActDepQueueData(type, stage);
      return;
    }
    
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:267^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   taq.*, 
//                   mms.vnum, 
//                   mms.term_application, 
//                   mms.special_handling, 
//                   mms.merch_number, 
//                   merch.merc_cntrl_number, 
//                   merch.merch_business_name,
//                   app.appsrctype_code
//  
//          from     temp_activation_queue taq, 
//                   mms_stage_info mms, 
//                   merchant merch,
//                   application app
//  
//          where    taq.q_type = :type and taq.q_stage = :stage and
//                   taq.app_seq_num = mms.app_seq_num   and 
//                   taq.app_seq_num = merch.app_seq_num and 
//                   taq.app_seq_num = app.app_seq_num   and
//                   taq.request_id = mms.request_id     and 
//                   mms.vnum is not null                and 
//                   process_response = 'success'
//  
//          order by taq.date_in_queue asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   taq.*, \n                 mms.vnum, \n                 mms.term_application, \n                 mms.special_handling, \n                 mms.merch_number, \n                 merch.merc_cntrl_number, \n                 merch.merch_business_name,\n                 app.appsrctype_code\n\n        from     temp_activation_queue taq, \n                 mms_stage_info mms, \n                 merchant merch,\n                 application app\n\n        where    taq.q_type =  :1  and taq.q_stage =  :2  and\n                 taq.app_seq_num = mms.app_seq_num   and \n                 taq.app_seq_num = merch.app_seq_num and \n                 taq.app_seq_num = app.app_seq_num   and\n                 taq.request_id = mms.request_id     and \n                 mms.vnum is not null                and \n                 process_response = 'success'\n\n        order by taq.date_in_queue asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.ActivationQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,type);
   __sJT_st.setInt(2,stage);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.ActivationQueueBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:292^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {

        ActivationRec rec = new ActivationRec();

        if(special)
        {

          if(!isBlank(rs.getString("special_handling")) && (rs.getString("special_handling")).toUpperCase().equals("Y"))
          {
            rec.setAppSeqNum      (rs.getString("app_seq_num"));
            rec.setRequestId      (rs.getString("request_id"));
            rec.setControlNum     (rs.getString("merc_cntrl_number"));
            rec.setAppTypeDesc    (rs.getString("appsrctype_code"));
            rec.setDba            (rs.getString("merch_business_name"));
            rec.setTermApplication(rs.getString("term_application"));
            rec.setVnum           (rs.getString("vnum"));
            rec.setQdate          (java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("date_in_queue")));
            rec.setMerchNumber    (rs.getString("merch_number"));
            
            queueList.add(rec);
          }
       
        }
        else
        {
          if(isBlank(rs.getString("special_handling")) || (rs.getString("special_handling")).toUpperCase().equals("N"))
          {
            rec.setAppSeqNum      (rs.getString("app_seq_num"));
            rec.setRequestId      (rs.getString("request_id"));
            rec.setAppTypeDesc    (rs.getString("appsrctype_code"));
            rec.setControlNum     (rs.getString("merc_cntrl_number"));
            rec.setDba            (rs.getString("merch_business_name"));
            rec.setTermApplication(rs.getString("term_application"));
            rec.setVnum           (rs.getString("vnum"));
            rec.setQdate          (java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("date_in_queue")));
            rec.setMerchNumber    (rs.getString("merch_number"));
            queueList.add(rec);
          }
        }

      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      logEntry("getQueueData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  

  public void getActDepQueueData(int type, int stage)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:362^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   taq.APP_SEQ_NUM,
//                   taq.REQUEST_ID,
//                   taq.Q_TYPE,
//                   taq.Q_STAGE, 
//                   taq.DATE_IN_QUEUE,
//                   merch.merch_number, 
//                   merch.merc_cntrl_number, 
//                   merch.merch_business_name,
//                   app.appsrctype_code,
//                   count(sdat.id) num_of_assignments,
//                   max(sdat.id)   last_id,
//                   count(sca.merchant_number) num_of_notes
//  
//          from     temp_activation_queue taq, 
//                   merchant merch,
//                   application app,
//                   schedule_data sdat,
//                   service_call_activation sca
//  
//          where    taq.q_type = :type and taq.q_stage = :stage and
//                   taq.app_seq_num = merch.app_seq_num and 
//                   taq.app_seq_num = app.app_seq_num   and
//                   to_char(merch.merch_number) = sdat.function_id(+) and
//                   merch.MERCH_NUMBER = sca.merchant_number(+)
//  
//          group by taq.APP_SEQ_NUM,
//                   taq.REQUEST_ID,
//                   taq.Q_TYPE,
//                   taq.Q_STAGE, 
//                   taq.DATE_IN_QUEUE,
//                   merch.merch_number,
//                   merch.merc_cntrl_number,
//                   merch.merch_business_name,
//                   app.appsrctype_code
//  
//          order by taq.date_in_queue asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   taq.APP_SEQ_NUM,\n                 taq.REQUEST_ID,\n                 taq.Q_TYPE,\n                 taq.Q_STAGE, \n                 taq.DATE_IN_QUEUE,\n                 merch.merch_number, \n                 merch.merc_cntrl_number, \n                 merch.merch_business_name,\n                 app.appsrctype_code,\n                 count(sdat.id) num_of_assignments,\n                 max(sdat.id)   last_id,\n                 count(sca.merchant_number) num_of_notes\n\n        from     temp_activation_queue taq, \n                 merchant merch,\n                 application app,\n                 schedule_data sdat,\n                 service_call_activation sca\n\n        where    taq.q_type =  :1  and taq.q_stage =  :2  and\n                 taq.app_seq_num = merch.app_seq_num and \n                 taq.app_seq_num = app.app_seq_num   and\n                 to_char(merch.merch_number) = sdat.function_id(+) and\n                 merch.MERCH_NUMBER = sca.merchant_number(+)\n\n        group by taq.APP_SEQ_NUM,\n                 taq.REQUEST_ID,\n                 taq.Q_TYPE,\n                 taq.Q_STAGE, \n                 taq.DATE_IN_QUEUE,\n                 merch.merch_number,\n                 merch.merc_cntrl_number,\n                 merch.merch_business_name,\n                 app.appsrctype_code\n\n        order by taq.date_in_queue asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.ActivationQueueBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,type);
   __sJT_st.setInt(2,stage);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.ActivationQueueBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:400^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {

        ActivationRec rec = new ActivationRec();

        rec.setAppSeqNum        (rs.getString("app_seq_num"));
        rec.setControlNum       (rs.getString("merc_cntrl_number"));
        rec.setAppTypeDesc      (rs.getString("appsrctype_code"));
        rec.setDba              (rs.getString("merch_business_name"));
        rec.setQdate            (java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("date_in_queue")));
        rec.setMerchNumber      (rs.getString("merch_number"));
        rec.setRequestId        (rs.getString("request_id"));
        rec.setNumOfAssignments (rs.getInt("num_of_assignments"));   
        rec.setLastId           (rs.getString("last_id"));
        rec.setNumOfNotes       (rs.getInt("num_of_notes"));   

        queueList.add(rec);
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      logEntry("getActDepQueueData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public String getCallNumColor(int idx)
  {
    //black   = hasnt been scheduled yet
    //green   = scheduled, date has not passed
    //yellow  = scheduled, contacted, yet rescheduling and further contact is needed
    //red     = scheduled, no action recorded, possibly missed call

    String result = "green";

    try
    {
      long scheduledTime = Long.parseLong(getLastId(idx));

       //this means.. no action was recorded for scheduled call.. so maybe the call was missed
      if(midnight > scheduledTime && getNumOfNotes(idx) < getNumOfAssignments(idx))
      {
        result = "red";
      }
      //this means there was atleast one recorded action for each scheduled call
      else if(midnight > scheduledTime && getNumOfNotes(idx) >= getNumOfAssignments(idx)) //and there is an action
      {
        result = "blue";
      }
    }
    catch(Exception e)
    {
      result="black"; 
    }

    return result;
  }

  public String getAppSeqNum(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getAppSeqNum();
  }
  public String getRequestId(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getRequestId();
  }

  public int getNumOfAssignments(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getNumOfAssignments();
  }

  public int getNumOfNotes(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getNumOfNotes();
  }
 
  public String getControlNum(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getControlNum();
  }

  public String getLastId(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getLastId();
  }

  public String getAppTypeDesc(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getAppTypeDesc();
  }

  public String getDba(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getDba();
  }

  public String getTermApplication(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getTermApplication();
  }

  public String getVnum(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getVnum();
  }
  
  public String getQdate(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getQdate();
  }

  public String getMerchNumber(int idx)
  {
    ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
    return rec.getMerchNumber();
  }


  public class  ActivationRec
  {
    private String appSeqNum          = "";
    private String requestId          = "";
    private String controlNum         = "";
    private String dba                = "";
    private String termApplication    = "";
    private String vnum               = "";
    private String qdate              = "";
    private String merchNumber        = "";
    private String appTypeDesc        = "";
    private int    numOfAssignments   = 0;
    private int    numOfNotes         = 0;
    private String lastId             = "";

    public void setAppSeqNum(String appSeqNum)
    {
      this.appSeqNum = appSeqNum;
    }
    public void setRequestId(String requestId)
    {
      this.requestId = requestId;
    }

    public void setNumOfAssignments(int numOfAssignments)
    {
      this.numOfAssignments = numOfAssignments;
    }
    
    public void setNumOfNotes(int numOfNotes)
    {
      this.numOfNotes = numOfNotes;
    }

    public void setLastId(String lastId)
    {
      this.lastId = lastId;
    }

    public void setAppTypeDesc(String appTypeDesc)
    {
      this.appTypeDesc = appTypeDesc;
    }

    public void setControlNum(String controlNum)
    {
      this.controlNum =  controlNum;
    }

    public void setDba(String dba)
    {
      this.dba = dba;
    }

    public void setTermApplication(String termApplication)
    {
      this.termApplication = termApplication;
    }

    public void setVnum(String vnum)
    {
      this.vnum = vnum;
    }
    
    public void setQdate(String qdate)
    {
      this.qdate = qdate;
    }

    public void setMerchNumber(String merchNumber)
    {
      this.merchNumber = merchNumber;
    }

    public String getMerchNumber()
    {
      return this.merchNumber;
    }

    public String getAppSeqNum()
    {
      return this.appSeqNum;
    }
    public String getRequestId()
    {
      return this.requestId;
    }

    public String getLastId()
    {
      return this.lastId;
    }

    public int getNumOfAssignments()
    {
      return this.numOfAssignments;
    }

    public int getNumOfNotes()
    {
      return this.numOfNotes;
    }

    public String getAppTypeDesc()
    {
      return this.appTypeDesc;
    }

    public String getControlNum()
    {
      return this.controlNum;
    }

    public String getDba()
    {
      return this.dba;
    }

    public String getTermApplication()
    {
      return this.termApplication;
    }

    public String getVnum()
    {
      return this.vnum;
    }
    
    public String getQdate()
    {
      return this.qdate;
    }

  }

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }


}/*@lineinfo:generated-code*/