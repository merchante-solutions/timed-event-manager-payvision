/*@lineinfo:filename=MerchantSummaryBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/MerchantSummaryBean.sqlj $

  Description:  
  
    MerchantSummaryBean

    Used to get summary information about a merchant.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class MerchantSummaryBean extends SQLJConnectionBase
{
  private boolean           loaded                = false;
  
  private long              appSeqNum             = -1;
  private String            dbaName               = null;
  private String            merchNum              = null;
  private String            ctrlNum               = null;
  private String            assocNum              = null;
  private String            bankNum               = null;
  
  /*
  ** METHOD public void getData()
  **
  ** Loads merchant data associated with the given application sequence number.
  */
  public void getData()
  {
    if (appSeqNum != -1)
    {
      try
      {
        connect();
        
        ResultSetIterator it = null;
        
        // merchant data
        /*@lineinfo:generated-code*//*@lineinfo:64^9*/

//  ************************************************************
//  #sql [Ctx] { select  m.merch_business_name, 
//                    m.merch_number,
//                    m.merc_cntrl_number,
//                    mif.dmagent,
//                    mif.bank_number
//                    
//            
//  
//            from    merchant m,
//                    mif
//                    
//            where   m.app_seq_num = :appSeqNum and
//                    m.merch_number = mif.merchant_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  m.merch_business_name, \n                  m.merch_number,\n                  m.merc_cntrl_number,\n                  mif.dmagent,\n                  mif.bank_number\n                  \n           \n\n          from    merchant m,\n                  mif\n                  \n          where   m.app_seq_num =  :1  and\n                  m.merch_number = mif.merchant_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.MerchantSummaryBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 5) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(5,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dbaName = (String)__sJT_rs.getString(1);
   merchNum = (String)__sJT_rs.getString(2);
   ctrlNum = (String)__sJT_rs.getString(3);
   assocNum = (String)__sJT_rs.getString(4);
   bankNum = (String)__sJT_rs.getString(5);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:83^9*/
        
        loaded = true;
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + 
          "::getData " + e.toString());
        logEntry("getData",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    else
    {
      loaded = false;
    }
  }
  public void getData(long newAppSeqNum)
  {
    setAppSeqNum(newAppSeqNum);
    getData();
  }
  
  /*
  ** ACCESSORS
  */
  public boolean isLoaded()
  {
    return loaded;
  }
  
  public long getAppSeqNum()
  {
    return appSeqNum;
  }
  public void setAppSeqNum(String newAppSeqNum)
  {
    try
    {
      setAppSeqNum(Long.parseLong(newAppSeqNum));
    }
    catch (Exception e) 
    {
      logEntry("setAppSeqNum(" + appSeqNum + ")", e.toString());
    }
  }
  public void setAppSeqNum(long newAppSeqNum)
  {
    appSeqNum = newAppSeqNum;
  }

  public String getDbaName()
  {
    return dbaName;
  }
  public String getMerchNum()
  {
    return merchNum;
  }
  public String getCtrlNum()
  {
    return ctrlNum;
  }
  public String getAssocNum()
  {
    return assocNum;
  }
  public String getBankNum()
  {
    return bankNum;
  }
}/*@lineinfo:generated-code*/