/*@lineinfo:filename=StatusSearchProfile*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/StatusSearchProfile.sqlj $

  Description:
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 8/13/01 4:57p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


public class StatusSearchProfile extends com.mes.database.SQLJConnectionBase
{
  public static final int       SEARCH_NONE             = -1;
  public static final int       SEARCH_DBA              = 1;
  public static final int       SEARCH_CONTROL_NUMBER   = 2;
  public static final int       SEARCH_MERCHANT_NUMBER  = 3;
  public static final int       SEARCH_SPECIFIC_DATE    = 4;
  public static final int       SEARCH_DATE_RANGE       = 5;
  
  private boolean       dataRetrieved       = false;
  private boolean       sales               = false;

  private int           searchCriteria      = SEARCH_NONE;
  private String        dba                 = "";
  private long          controlNumber       = -1;
  private long          merchantNumber      = -1;
  private Date          specificDate        = null;
  private Date          startDate           = null;
  private Date          endDate             = null;

  private int           status              = QueueConstants.DEPT_STATUS_ALL;
  private int           department          = QueueConstants.DEPARTMENT_ALL;
  private int           owner               = -1;
  private int           appType             = 0;

  private DateFormat    simpleDate          = new SimpleDateFormat("MM/dd/yy");

  private boolean       initialized         = false;
  public Vector         departments         = new Vector();
  public Vector         departmentDescs     = new Vector();
  public Vector         statuses            = new Vector();
  public Vector         statusDescs         = new Vector();
  public Vector         users               = new Vector();
  public Vector         userDescs           = new Vector();
  public Vector         types               = new Vector();
  public Vector         typeDescs           = new Vector();
  private Vector        reps                = new Vector();
  private Vector        userids             = new Vector();

  private Vector        errors              = new Vector();

  public StatusSearchProfile()
  {
  }

  public void setData(HttpServletRequest request)
  {
    String progress = "start of method";
    try
    {
      Enumeration enum2 = request.getParameterNames();
      
      errors.clear();
      
      if(enum2 != null && enum2.hasMoreElements())
      {
        // get data from request
        progress = "searchCriteria";
        searchCriteria  = HttpHelper.getInt   (request, "searchCriteria", searchCriteria);
        progress = "controlNumber";
        controlNumber   = HttpHelper.getLong  (request, "controlNumber", controlNumber);
        progress = "merchantNumber";
        merchantNumber  = HttpHelper.getLong  (request, "merchantNumber", merchantNumber);
        progress = "dba";
        dba             = HttpHelper.getString(request, "dba", dba);
        progress = "specificDate";
        specificDate    = HttpHelper.getDate  (request, "specificDate", "MM/dd/yy", specificDate);
        progress = "startDate";
        startDate       = HttpHelper.getDate  (request, "startDate", "MM/dd/yy", startDate);
        progress = "endDate";
        endDate         = HttpHelper.getDate  (request, "endDate", "MM/dd/yy", endDate);

        progress = "status";
        status          = HttpHelper.getInt   (request, "status", status);
        
        progress = "owner";
        owner          = HttpHelper.getInt   (request, "owner", owner);

        progress = "appType";
        appType          = HttpHelper.getInt   (request, "appType", appType);

        progress = "department";
        department          = HttpHelper.getInt   (request, "department", department);

        if(request.getParameter("sales") == null || (request.getParameter("sales")).equals(""))
        {
          this.sales = false;
        }
        else
        {
          this.sales = true;
        }
        dataRetrieved   = true;
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::setData()", e.toString());
      errors.add("setData (" + progress + "): " + e.toString());
    }
    
  }

  public void setDropDowns(UserBean ub)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    StringBuffer        desc    = new StringBuffer("");

    if(initialized)
    {
      return;
    }

    try
    {
      // make sure connection to database is hot
      connect();
      
      // application statuses
      /*@lineinfo:generated-code*//*@lineinfo:166^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    ds.status_code,
//                    ds.status_desc,
//                    dpt.dept_desc
//          from      dept_status ds,
//                    departments dpt
//          where     ds.dept_code = dpt.dept_code and ds.status_code != 0
//          order by  ds.status_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ds.status_code,\n                  ds.status_desc,\n                  dpt.dept_desc\n        from      dept_status ds,\n                  departments dpt\n        where     ds.dept_code = dpt.dept_code and ds.status_code != 0\n        order by  ds.status_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.StatusSearchProfile",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.StatusSearchProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^7*/

      rs = it.getResultSet();
      while(rs.next())
      {
        desc.setLength(0);
        if(rs.getInt("status_code") != QueueConstants.DEPT_STATUS_ALL)
        {
          desc.append(rs.getString("dept_desc"));
          desc.append(" - ");
        }
        desc.append(rs.getString("status_desc"));

        statuses.add(rs.getString("status_code"));
        statusDescs.add(desc.toString());
      }
      it.close();

      // departments
      /*@lineinfo:generated-code*//*@lineinfo:194^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    dept_code,
//                    dept_desc
//          from      departments
//          order by  dept_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    dept_code,\n                  dept_desc\n        from      departments\n        order by  dept_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.StatusSearchProfile",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.StatusSearchProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^7*/

      rs = it.getResultSet();
      while(rs.next())
      {
        departments.add(rs.getString("dept_code"));
        departmentDescs.add(rs.getString("dept_desc"));
      }
      it.close();

      // app types
      /*@lineinfo:generated-code*//*@lineinfo:211^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    app_type,
//                    app_name
//          from      org_app
//          order by  app_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    app_type,\n                  app_name\n        from      org_app\n        order by  app_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.StatusSearchProfile",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.StatusSearchProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:217^7*/

      rs = it.getResultSet();

      if(ub.hasRight(MesUsers.RIGHT_ACCOUNT_SERVICING) || ub.hasRight(MesUsers.RIGHT_APPLICATION_SUPER))
      {
        types.add("-1");
        typeDescs.add("All Apps");
      }
      
      while(rs.next())
      {
        if(ub.hasRight(MesUsers.RIGHT_ACCOUNT_SERVICING) ||
           ub.hasRight(MesUsers.RIGHT_APPLICATION_SUPER) ||
           ub.getAppType() == rs.getInt("app_type"))
        {
          types.add(rs.getString("app_type"));
          typeDescs.add(rs.getString("app_name"));
        }
      }
      it.close();

      // users
      /*@lineinfo:generated-code*//*@lineinfo:240^7*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct
//                    u.user_id,
//                    u.name
//          from      users u,
//                    user_to_group ug,
//                    user_group_to_right gr
//          where     (u.type_id = ug.user_id or u.user_id = ug.user_id) and
//                    ug.group_id = gr.group_id and
//                    gr.right_id = :MesUsers.RIGHT_APPLICATION_PROXY
//          order by  u.name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct\n                  u.user_id,\n                  u.name\n        from      users u,\n                  user_to_group ug,\n                  user_group_to_right gr\n        where     (u.type_id = ug.user_id or u.user_id = ug.user_id) and\n                  ug.group_id = gr.group_id and\n                  gr.right_id =  :1 \n        order by  u.name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.StatusSearchProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MesUsers.RIGHT_APPLICATION_PROXY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.StatusSearchProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:252^7*/

      rs = it.getResultSet();

      if(ub.hasRight(MesUsers.RIGHT_ACCOUNT_SERVICING) ||
         ub.hasRight(MesUsers.RIGHT_APPLICATION_MGR) ||
         ub.hasRight(MesUsers.RIGHT_APPLICATION_SUPER))
      {
        users.add("-1");
        userDescs.add("All Users");
      }

      while(rs.next())
      {
        if(ub.hasRight(MesUsers.RIGHT_ACCOUNT_SERVICING) ||
           ub.hasRight(MesUsers.RIGHT_APPLICATION_MGR) ||
           ub.hasRight(MesUsers.RIGHT_APPLICATION_SUPER) ||
           rs.getLong("user_id") == ub.getUserId())
        {
          users.add(rs.getString("user_id"));
          userDescs.add(rs.getString("name"));
        }
      }

      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::setDropDowns()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void setSalesDropDowns(UserBean ub)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    PreparedStatement   ps      = null;
    StringBuffer        desc    = new StringBuffer("");
    String              rep_id  = "";

    if(initialized)
    {
      return;
    }

    try
    {
      // make sure connection to database is hot
      connect();
      
      // application statuses
      /*@lineinfo:generated-code*//*@lineinfo:307^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    ds.status_code,
//                    ds.status_desc,
//                    dpt.dept_desc
//          from      dept_status ds,
//                    departments dpt
//          where     dpt.dept_code not in (:com.mes.ops.QueueConstants.DEPARTMENT_RISK,:com.mes.ops.QueueConstants.DEPARTMENT_CREDIT) and ds.dept_code = dpt.dept_code and ds.status_code != 0
//          order by  ds.status_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ds.status_code,\n                  ds.status_desc,\n                  dpt.dept_desc\n        from      dept_status ds,\n                  departments dpt\n        where     dpt.dept_code not in ( :1 , :2 ) and ds.dept_code = dpt.dept_code and ds.status_code != 0\n        order by  ds.status_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.StatusSearchProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,com.mes.ops.QueueConstants.DEPARTMENT_RISK);
   __sJT_st.setInt(2,com.mes.ops.QueueConstants.DEPARTMENT_CREDIT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.StatusSearchProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:316^7*/

      rs = it.getResultSet();
      while(rs.next())
      {
        desc.setLength(0);
        if(rs.getInt("status_code") != QueueConstants.DEPT_STATUS_ALL)
        {
          desc.append(rs.getString("dept_desc"));
          desc.append(" - ");
        }
        desc.append(rs.getString("status_desc"));

        statuses.add(rs.getString("status_code"));
        statusDescs.add(desc.toString());
      }
      it.close();


      users.add(Long.toString(ub.getUserId()));
      userDescs.add("My Apps");
      
      //add to vector so we can use to search for all user apps
      userids.add(Long.toString(ub.getUserId()));

      if(ub.isMember(MesUsers.GROUP_SALES_MANAGER))
      {
        users.add("-1");
        userDescs.add("All My Users");
      
        // users
        /*@lineinfo:generated-code*//*@lineinfo:347^9*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct rep_id
//            from sales_rep     
//            where user_id = :ub.getUserId()   
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_811 = ub.getUserId();
  try {
   String theSqlTS = "select distinct rep_id\n          from sales_rep     \n          where user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.StatusSearchProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_811);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.StatusSearchProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:352^9*/

        rs = it.getResultSet();
        if(rs.next())
        {
          rep_id = rs.getString("rep_id");
        }
        it.close();

        if(!rep_id.equals(""))
        {
          setManagerEmployees(rep_id);
        }  
        
        if(reps.size() > 0)
        {  
          String tempreps = "";
          tempreps = "rep_id = '" + (String)reps.elementAt(0) + "'";          
          for(int x=1; x<reps.size(); x++)
          {
            tempreps = tempreps + " or rep_id = '" + (String)reps.elementAt(x) + "'";
          }

          System.out.println("select rep_name, user_id from sales_rep where " + tempreps);
          
          ps = getPreparedStatement("select rep_name, user_id from sales_rep where " + tempreps);
          System.out.println("got ps");
          rs = ps.executeQuery();
          System.out.println("got rs");          
          while(rs.next())
          {
            System.out.println("inside rs");
            users.add(rs.getString("user_id"));
            userDescs.add(rs.getString("rep_name"));

            //add to userid so we can search all users
            userids.add(rs.getString("user_id"));
          }
          System.out.println("closing rs");
          rs.close();
          System.out.println("closing ps");
          ps.close();
          System.out.println("ps closed");
        }

      }//end if

      // app types
      /*@lineinfo:generated-code*//*@lineinfo:400^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    app_type,
//                    app_name
//          from      org_app
//          order by  app_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    app_type,\n                  app_name\n        from      org_app\n        order by  app_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.StatusSearchProfile",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.StatusSearchProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:406^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        types.add(rs.getString("app_type"));
        typeDescs.add(rs.getString("app_name"));
      }
      it.close();


    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::setSalesDropDowns()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void setManagerEmployees(String rep)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    try
    {
      connect();
    
      /*@lineinfo:generated-code*//*@lineinfo:437^7*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct rep_id
//          from sales_rep_parent     
//          where rep_report_to = :rep   
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct rep_id\n        from sales_rep_parent     \n        where rep_report_to =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.StatusSearchProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,rep);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.StatusSearchProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:442^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        reps.add(rs.getString("rep_id"));
        setManagerEmployees(rs.getString("rep_id"));
      }
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::setMangerEmployees()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public boolean validate()
  {
    switch(searchCriteria)
    {
      case SEARCH_NONE:
        break;
        
      case SEARCH_DBA:
        if(dba == null || dba.equals(""))
        {
          errors.add("Please enter a Business Name to search for");
        }
        break;
        
      case SEARCH_CONTROL_NUMBER:
        if(controlNumber == -1)
        {
          errors.add("Please enter a valid control number");
        }
        break;
        
      case SEARCH_MERCHANT_NUMBER:
        if(merchantNumber == -1)
        {
          errors.add("Please enter a valid merchant number");
        }
        break;
        
      case SEARCH_SPECIFIC_DATE:
        if(specificDate == null)
        {
          errors.add("Please enter a valid specific date to search for (MM/DD/YY)");
        }
        break;
        
      case SEARCH_DATE_RANGE:
        if(startDate == null)
        {
          errors.add("Please enter a valid start date (MM/DD/YY)");
        }
        
        if(endDate == null)
        {
          errors.add("Please enter a valid end date (MM/DD/YY)");
        }
        break;
        
      default:
        errors.add("Please choose a search criteria");
        break;
    }
    
    return(errors.size() == 0);
  }                           
  
  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }
  
  public Vector getErrors()
  {
    return errors;
  }
  
  public ResultSet getResultSet()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         result  = null;
    
    String            progress = "start of method";
    
    try
    {
      // connect to database
      connect();
      
      progress = "building query";
      // build base query
      qs.append("select distinct  app.app_created_date, ");
      qs.append("                 app.app_seq_num, ");
      qs.append("                 app.app_type, ");
      qs.append("                 merch.merc_cntrl_number, ");
      qs.append("                 merch.merch_business_name, ");
      qs.append("                 merch.merch_number, ");
      qs.append("                 u.name ");
      qs.append("from             application app, ");
      qs.append("                 merchant merch, ");
      qs.append("                 users u, ");
      qs.append("                 app_tracking apptrack, ");
      qs.append("                 app_queue aq ");
      qs.append("where            app.app_seq_num = merch.app_seq_num and ");
      qs.append("                 app.app_user_id = u.user_id and ");
      qs.append("                 app.app_seq_num = aq.app_seq_num ");
      
      // conditional query elements
      if(owner == -1 & isSales())
      {
        String tempids = "";
        tempids = (String)userids.elementAt(0);
        for(int x=1; x<userids.size(); x++)
        {
          tempids = tempids + "," + (String)userids.elementAt(x);
        }

        qs.append("and app.app_user_id in (" + tempids + ") ");
      }
      else if(owner != -1)
      {
        qs.append("and app.app_user_id = " + owner + " ");
      }
      
      if(appType != -1)
      {
        qs.append("and app.app_type = " + appType + " ");
      }
      
      if(status != QueueConstants.DEPT_STATUS_ALL ||
         department != QueueConstants.DEPARTMENT_ALL)
      {
        qs.append("and apptrack.app_seq_num = app.app_seq_num ");
        
        if(status != QueueConstants.DEPT_STATUS_ALL)
        {
          qs.append("and apptrack.status_code = " + status + " ");
        }
        
        if(department != QueueConstants.DEPARTMENT_ALL)
        {
          qs.append("and apptrack.dept_code = " + department + " ");
        }
      }
      
      progress = "getting prepared statement";
      switch(searchCriteria)
      {
        case SEARCH_NONE:
          qs.append("order by app.app_created_date desc");
          ps = getPreparedStatement(qs.toString());
          break;
        case SEARCH_DBA:
          qs.append("and upper(merch.merch_business_name) like ? escape \'\\\' ");
          qs.append("order by app.app_created_date desc ");
          ps = getPreparedStatement(qs.toString());
          ps.setString(1, "%" + dba.toUpperCase() + "%");
          break;
          
        case SEARCH_CONTROL_NUMBER:
          qs.append("and merch.merc_cntrl_number = ? ");
          qs.append("order by app.app_created_date desc ");
          ps = getPreparedStatement(qs.toString());
          ps.setLong(1, controlNumber);
          break;
          
        case SEARCH_MERCHANT_NUMBER:
          qs.append("and merch.merch_number = ? ");
          qs.append("order by app.app_created_date desc ");
          ps = getPreparedStatement(qs.toString());
          ps.setLong(1, merchantNumber);
          break;
          
        case SEARCH_SPECIFIC_DATE:
          qs.append("and trunc(app.app_created_date) = ? ");
          qs.append("order by app.app_created_date desc ");
          ps = getPreparedStatement(qs.toString());
          ps.setDate(1, new java.sql.Date(specificDate.getTime()));
          break;
          
        case SEARCH_DATE_RANGE:
          qs.append("and trunc(app.app_created_date) between ? and ? ");
          qs.append("order by app.app_created_date desc ");
          ps = getPreparedStatement(qs.toString());
          ps.setDate(1, new java.sql.Date(startDate.getTime()));
          ps.setDate(2, new java.sql.Date(endDate.getTime()));
          break;
      }
      
      progress = "getting result set";
      result = ps.executeQuery();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getResultSet()", e.toString());
      errors.add(progress + ": " + e.toString());
    }
    
    return result;
  }

  public String getDba()
  {
    return this.dba;
  }

  public String getControlNumber()
  {
    String result = "";
    if(this.controlNumber != -1)
    {
      result = Long.toString(controlNumber);
    }
    return result;
  }

  public String getMerchantNumber()
  {
    String result = "";
    if(this.merchantNumber != -1)
    {
      result = Long.toString(this.merchantNumber);
    }
    return result;
  }

  public String getSpecificDate()
  {
    String result = "";
    if(specificDate != null)
    {
      result = simpleDate.format(specificDate);
    }
    return result;
  }

  public String getStartDate()
  {
    String result = "";
    if(startDate != null)
    {
      result = simpleDate.format(startDate);
    }
    return result;
  }

  public boolean isSales()
  {
    return this.sales;
  }

  public String getEndDate()
  {
    String result = "";
    if(endDate != null)
    {
      result = simpleDate.format(endDate);
    }
    return result;
  }
  
  public int getSearchCriteria()
  {
    return searchCriteria;
  }
  
  public int getStatus()
  {
    return this.status;
  }
  public int getDepartment()
  {
    return this.department;
  }
  public int getOwner()
  {
    return this.owner;
  }
  public int getAppType()
  {
    return this.appType;
  }
  
  public boolean isSubmitted()
  {
    return this.dataRetrieved;
  }
  
  public boolean isBlank(String msg)
  {
    return((msg == null || msg.equals("")));
  }

}/*@lineinfo:generated-code*/