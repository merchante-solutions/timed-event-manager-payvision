/*@lineinfo:filename=EasiDownloadError*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/EasiDownloadError.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;

public class EasiDownloadError extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(EasiDownloadError.class);

  //error codes
  private static final int  NO_ERROR                      = -1;
  private static final int  NUM_RECS_DONT_MATCH           = 0;
  private static final int  NUM_RECS_CHECK_CRASH          = 1;
  private static final int  RECORD_TYPE_UNKNOWN           = 2;

  private static final int  MISSING_DATA                  = 10;
  private static final int  DISC_MERCH_NUM_NOT_BLANK      = 11;
  private static final int  DECIS_DATE_NOT_BLANK          = 12;
  private static final int  BLANK_DIS_CODE                = 13;
  private static final int  APP_SEQ_NUM_NOT_FOUND         = 14;
  private static final int  BLANK_ERROR_FIELD             = 15;

  //reset error count
  private static final int RESET_RECORD_COUNT             = 0;

  private Vector      records                             = new Vector();
  private Vector      emailRecs                           = new Vector();
   
  //this bean is called for each file and each file only has one partner num.. 
  //filePartnerNum makes currentPartnerNum obsolete, but well leave it alone cause it aint hurting anything
  private String      filePartnerNum                      = "";

  private String      currentPartnerNum                   = "";
  private String      headerDateTimestamp                 = "";
  private String      currentHeader                       = "";

  private int         recordCounter                       = RESET_RECORD_COUNT;
  private boolean     hasError                            = false;

  private HashMap     errorCodeMap                        = new HashMap();
  private Timestamp   today                               = null;


  public EasiDownloadError()
  {
    super(SQLJConnectionBase.getDirectConnectString());

    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();
 
      /*@lineinfo:generated-code*//*@lineinfo:100^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  * 
//          from    RAP_APP_ERROR_CODES
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  * \n        from    RAP_APP_ERROR_CODES";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.EasiDownloadError",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.EasiDownloadError",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:104^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        errorCodeMap.put(rs.getString("ERROR_CODE"),rs.getString("ERROR_DESC"));
      }
 
      rs.close();
      it.close();

      //gets the current data/time
      /*@lineinfo:generated-code*//*@lineinfo:117^7*/

//  ************************************************************
//  #sql [Ctx] { select    sysdate 
//          from      dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    sysdate  \n        from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.EasiDownloadError",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   today = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:121^7*/
    }
    catch(Exception e)
    {
      logEntry("constructor()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public boolean getData(File f)  
  {
    String            lineInfo  = "";
    boolean           result    = false;

    try 
    {
      LineNumberReader in  = new LineNumberReader(new FileReader(f));

      int i = 0;

      if(in != null)
      { 

        //clears all email recs
        emailRecs.setSize(0);
        
        lineInfo = in.readLine();
        while(lineInfo != null)
        {
          i++;
          
          if(parseLine(lineInfo, i)) //if true an error occurred
          {
            log.debug(lineInfo); // prints error records
          }

          lineInfo = in.readLine();
        }
    
        //once we update database we send email with records to show what has been updated and is ready to go
        if(emailRecs.size() > 0)
        {
          sendMail();
        }

      }

      in.close();

      result = true;

      log.debug("number of lines read " + i);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      log.error("getData: " + e.toString());
    }
    return result;
  }
  


  private boolean parseLine(String line, int linenum)
  {
    ResponseRec     rec                 = null;

    String          numRecs             = "";
    boolean         feeError            = false;
    int             errorCode           = NO_ERROR;

    String          recordType          = "";
    String          acquirerMerchNumber = "";
    String          errors              = "";

    recordType            = line.substring(0,2);
    
    if(recordType.equals("00")) //header
    {
      log.debug("got header");
      //header info
      currentPartnerNum   = line.substring(2,6);

      //the purpose of filePartnerNum is that it doesnt get reset until the bean's life is over...
      filePartnerNum      = currentPartnerNum;

      headerDateTimestamp = line.substring(8,32);
      currentHeader       = line;
    }
    else if(recordType.equals("01") || recordType.equals("02") || recordType.equals("03")) //record
    {
      
      recordCounter++;

      //record info
      acquirerMerchNumber = line.substring(469,489);
      errors              = line.substring(564);


      rec = new ResponseRec();

      rec.setAppSeqNum(getAppSeqNum(acquirerMerchNumber.trim()));

      //*******************TESTING ONLY**********************
      //rec.setAppSeqNum(Integer.toString(recordCounter));
      //*****************************************************

      rec.setAcquirerMerchNumber(acquirerMerchNumber.trim());
      rec.setErrors             (errors.trim());

      if(rec.isValid())
      {
        records.add(rec);
      }
      else
      {
        feeError  = true;
        errorCode = rec.getErrorCode();
      }

    }
    else if(recordType.equals("99")) //trailer
    {
      log.debug("got trailer");

      numRecs             = line.substring(6,11);

      //check to see that trailer num of recs and actual read num of recs are the same
      try
      {
        int tempNumRecs = Integer.parseInt(numRecs);
        
        if(recordCounter == tempNumRecs)
        {
          //if its all good.. we can update db and put these files up 
          submitData();

          if(hasError)//if a file format error occured.. notify admin.. they can check db_log
          {
            sendMailToAdmin(currentHeader);
          }
          
          //clear everything for next header/trailer set if exists
          currentPartnerNum         = "";
          headerDateTimestamp       = "";
          currentHeader             = "";
          
          records                   = new Vector();
          recordCounter             = RESET_RECORD_COUNT;
          hasError                  = false;
        }
        else
        {
          feeError  = true;
          errorCode = NUM_RECS_DONT_MATCH;
        }

        if(feeError)//if slipped through above.. then we send error email here
        {
          sendMailToAdmin(currentHeader);
        }
      }
      catch(Exception e)
      {
        feeError  = true;
        errorCode = NUM_RECS_CHECK_CRASH;
      }
      
      //reset current partner num and record counter because we hit the trailer.. a new partner num will be coming in now...
      currentPartnerNum   = "";
      recordCounter       = 0;
    }
    else if(recordType.startsWith("+"))
    {
      return false; //no error
    }
    else //error.. file record type not known
    {
      feeError  = true;
      errorCode = RECORD_TYPE_UNKNOWN;
    }



    if(feeError)
    {
      //write to db showing header for error record and line of error record and error desc
      try
      {
        //this says that an error occurred somewhere while reading records for one header/trailer set
        this.hasError = true;

        log.debug("fee error: " + decodeError(errorCode));

        connect();
        /*@lineinfo:generated-code*//*@lineinfo:321^9*/

//  ************************************************************
//  #sql [Ctx] { insert into RAP_UPLOAD_ERROR_LOG
//            ( 
//              ERROR_DATE,
//              HEADER,
//              ERROR_DESC,
//              LINE,
//              UPLOAD_TYPE
//            )
//            values
//            ( 
//              :today,
//              :currentHeader.substring(0,50),
//              :decodeError(errorCode),
//              :line,
//              :mesConstants.RAP_UPLOAD_TYPE_ERROR
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_754 = currentHeader.substring(0,50);
 String __sJT_755 = decodeError(errorCode);
   String theSqlTS = "insert into RAP_UPLOAD_ERROR_LOG\n          ( \n            ERROR_DATE,\n            HEADER,\n            ERROR_DESC,\n            LINE,\n            UPLOAD_TYPE\n          )\n          values\n          ( \n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.EasiDownloadError",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setString(2,__sJT_754);
   __sJT_st.setString(3,__sJT_755);
   __sJT_st.setString(4,line);
   __sJT_st.setInt(5,mesConstants.RAP_UPLOAD_TYPE_ERROR);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:339^9*/
      }
      catch(Exception e)
      {
        log.error(e.toString());
      }
      finally
      {
        cleanUp();
      }
    }

    return feeError;
  }

  public boolean submitData()
  {
    boolean           result        = true;

    log.debug("all up in submit data");
    try
    {


      connect(); //open up a connection which will be used over and over again...

      for(int x=0; x< this.records.size(); x++)
      {
        ResponseRec rec =  (ResponseRec)records.elementAt(x);
        
        if(recordExists(rec.getAppSeqNum()))
        {

          /*@lineinfo:generated-code*//*@lineinfo:372^11*/

//  ************************************************************
//  #sql [Ctx] { update  MERCHANT_DISCOVER_APP_RESPONSE
//            
//              set     DECISION_STATUS         = 'E',      
//                      DATE_LAST_UPDATED       = :today,
//                      ERROR_FLAGS             = :rec.getErrors()          
//                    
//              where   APP_SEQ_NUM             = :rec.getAppSeqNum() and
//                      acquirer_merchant_num   = :rec.getAcquirerMerchNumber()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_756 = rec.getErrors();
 String __sJT_757 = rec.getAppSeqNum();
 String __sJT_758 = rec.getAcquirerMerchNumber();
   String theSqlTS = "update  MERCHANT_DISCOVER_APP_RESPONSE\n          \n            set     DECISION_STATUS         = 'E',      \n                    DATE_LAST_UPDATED       =  :1 ,\n                    ERROR_FLAGS             =  :2           \n                  \n            where   APP_SEQ_NUM             =  :3  and\n                    acquirer_merchant_num   =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.EasiDownloadError",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setString(2,__sJT_756);
   __sJT_st.setString(3,__sJT_757);
   __sJT_st.setString(4,__sJT_758);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:383^11*/
        }
        else
        {
          log.debug("all up in rec no exist");

          /*@lineinfo:generated-code*//*@lineinfo:389^11*/

//  ************************************************************
//  #sql [Ctx] { insert into MERCHANT_DISCOVER_APP_RESPONSE
//              ( 
//                DECISION_STATUS,
//                DATE_LAST_UPDATED,
//                ERROR_FLAGS,
//                APP_SEQ_NUM,
//                ACQUIRER_MERCHANT_NUM
//              )
//              values
//              ( 
//                'E',
//                :today,
//                :rec.getErrors(),
//                :rec.getAppSeqNum(),
//                :rec.getAcquirerMerchNumber()
//              )
//  
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_759 = rec.getErrors();
 String __sJT_760 = rec.getAppSeqNum();
 String __sJT_761 = rec.getAcquirerMerchNumber();
   String theSqlTS = "insert into MERCHANT_DISCOVER_APP_RESPONSE\n            ( \n              DECISION_STATUS,\n              DATE_LAST_UPDATED,\n              ERROR_FLAGS,\n              APP_SEQ_NUM,\n              ACQUIRER_MERCHANT_NUM\n            )\n            values\n            ( \n              'E',\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ops.EasiDownloadError",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setString(2,__sJT_759);
   __sJT_st.setString(3,__sJT_760);
   __sJT_st.setString(4,__sJT_761);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:408^11*/
    
        }
        updateTrackingStatus(rec.getAppSeqNum());
        emailRecs.add("E" + "\t\t" + rec.getAcquirerMerchNumber() + "\n" + MesDefaults.PLATFORM_URL+"/jsp/credit/discover_rap_screen.jsp?primaryKey=" + rec.getAppSeqNum() + "\n\n");
      }
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
      log.error("error man: " + e.toString());
      result = false;
    } 
    finally
    {
      cleanUp();
    }
    
    return result;
  }

  private void sendMailToAdmin(String curHeader)
  {
    String message = "";
    String subject = "";
    
    subject = "Discover Rap Error Occurred (error file) -- " + today.toString();

    message = "Error occurred for this header:\n\n" + curHeader;

    try
    {
      MailMessage msg = new MailMessage(true);
      msg.setAddresses(MesEmails.MSG_ADDRS_RAP_RESPONSE_ADMIN);
      msg.setSubject(subject);
      msg.setText(message);
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendMail()",e.toString());
      log.error("discover Error Status email failed to send.");
    }  
  }

  private void sendMail()
  {
    String message = "";
    String subject = "";
    
    subject = "Discover Rap Errors -- " + today.toString();

    message = "Status \t Acquirer Merch # \n\n";

    for(int p=0; p<emailRecs.size(); p++)
    {
      message += (String)emailRecs.elementAt(p);
    }

    try
    {
      MailMessage msg = new MailMessage(true);

      if(filePartnerNum.equals("9849"))
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_RAP_RESPONSE_ADMIN);
      }
      else
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_RAP_RESPONSE_OPERATIONS);
      }

      msg.setSubject(subject);
      msg.setText(message);
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendMail()",e.toString());
      log.error("discover Error Status email failed to send.");
    }  
  }

  private void updateTrackingStatus(String appSeqNum)
  {
    try
    {
      int statusCode = QueueConstants.DEPT_STATUS_DISCOVER_RAP_TRANSMISSION_ERRORS;

      /*@lineinfo:generated-code*//*@lineinfo:497^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     status_code   = :statusCode
//          where   APP_SEQ_NUM   = :appSeqNum and
//                  dept_code     = :QueueConstants.DEPARTMENT_DISCOVER_RAP
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     status_code   =  :1 \n        where   APP_SEQ_NUM   =  :2  and\n                dept_code     =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ops.EasiDownloadError",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,statusCode);
   __sJT_st.setString(2,appSeqNum);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_DISCOVER_RAP);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:503^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updateTrackingStatus: " + e.toString());
      log.error("updateTrackingStatus() error: " + e.toString());
    }
    finally
    {
    }
  }

  private boolean recordExists(String appseqnum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {
      
      /*@lineinfo:generated-code*//*@lineinfo:524^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num 
//          from    MERCHANT_DISCOVER_APP_RESPONSE
//          where   app_seq_num = :appseqnum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num \n        from    MERCHANT_DISCOVER_APP_RESPONSE\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.EasiDownloadError",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,appseqnum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.EasiDownloadError",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:529^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      result = false;
      logEntry("recordExists()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    return result;
  }

  private String getAppSeqNum(String merchNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    String              result  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:564^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num 
//          from    merchant_discover_app
//          where   acquirer_merchant_num = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num \n        from    merchant_discover_app\n        where   acquirer_merchant_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.EasiDownloadError",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.EasiDownloadError",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:569^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = rs.getString("app_seq_num");
      }

      rs.close(); 
      it.close();
    }
    catch(Exception e)
    {
      result = "";
      logEntry("getAppSeqNum()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return result;
  }

  private String decodeErrorCodes(String errs)
  {
    String result = "";
    int numCodes  = 0;

    if(isBlank(errs))
    {
      return "NONE";
    }

    try
    {
      numCodes = errs.length()/2;
      switch(numCodes)
      {
        case 1:
          result =  (String)errorCodeMap.get(errs);
        break;
        case 2:
          result =  (String)errorCodeMap.get(errs.substring(0,2));
          result += "," + (String)errorCodeMap.get(errs.substring(2));
        break;
        case 3:
          result =  (String)errorCodeMap.get(errs.substring(0,2));
          result += "," + (String)errorCodeMap.get(errs.substring(2,4));
          result += "," + (String)errorCodeMap.get(errs.substring(4));
        break;
        case 4:
          result =  (String)errorCodeMap.get(errs.substring(0,2));
          result += "," + (String)errorCodeMap.get(errs.substring(2,4));
          result += "," + (String)errorCodeMap.get(errs.substring(4,6));
          result += "," + (String)errorCodeMap.get(errs.substring(6));
        break;
        case 5:
          result =  (String)errorCodeMap.get(errs.substring(0,2));
          result += "," + (String)errorCodeMap.get(errs.substring(2,4));
          result += "," + (String)errorCodeMap.get(errs.substring(4,6));
          result += "," + (String)errorCodeMap.get(errs.substring(6,8));
          result += "," + (String)errorCodeMap.get(errs.substring(8));
        break;
      }
    }
    catch(Exception e)
    {
    }
    
    if(isBlank(result))
    {
      return "NONE";
    }

    return result;
  }



  private String decodeError(int err)
  {
    String result = "";

    switch(err)
    {
      case NUM_RECS_DONT_MATCH:
        result = "Number of records read and number of records in trailer field 3 don't match";
      break;
      case NUM_RECS_CHECK_CRASH:
        result = "Number of records check crashed";
      break;
      case RECORD_TYPE_UNKNOWN:
        result = "Unknown record type";
      break;
      case MISSING_DATA:
        result = "Required data missing from record";
      break;
      case DISC_MERCH_NUM_NOT_BLANK:
        result = "Account pending, yet discover merchant number was present";
      break;
      case DECIS_DATE_NOT_BLANK:
        result = "Account pending, yet decision date was present";
      break;
      case BLANK_DIS_CODE:
        result = "Disposition code was blank and decision status was not pending";
      break;
      case APP_SEQ_NUM_NOT_FOUND:
        result = "Could not find appSeqNum from acquirer merchant account number";
      break;

    }
    return result;
  }


  public boolean isBlank(String test)
  {
    boolean pass = false;
  
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
  
    return pass;
  }

  public class ResponseRec
  {
    private static final int  NO_ERROR                       = -1;
    private static final int  APP_SEQ_NUM_NOT_FOUND          = 14;
    private static final int  BLANK_ERROR_FIELD              = 15;

    private String            acquirerMerchNumber            = "";
    private String            appSeqNum                      = "";
    private String            errors                         = "";
    private int               errorCode                      = NO_ERROR;

    ResponseRec()
    {}

    public void setAppSeqNum(String appSeqNum)
    {
      this.appSeqNum = appSeqNum;
    }
    public void setAcquirerMerchNumber(String acquirerMerchNumber)
    {
      this.acquirerMerchNumber = acquirerMerchNumber;
    }
    public void setErrorCode(int errorCode)
    {
      this.errorCode = errorCode;
    }
    public void setErrors(String errors)
    {
      this.errors = errors;
    }

    public String getAppSeqNum()
    {
      return this.appSeqNum;
    }

    public String getAcquirerMerchNumber()
    {
      return this.acquirerMerchNumber;
    }

    public int getErrorCode()
    {
      return this.errorCode;
    }
    public String getErrors()
    {
      return this.errors;
    }

    public boolean isValid()
    {
      boolean result = true;

      if(isBlank(appSeqNum))
      {
        result = false;
        setErrorCode(APP_SEQ_NUM_NOT_FOUND);
      }
      else if(isBlank(errors))
      {
        result = false;
        setErrorCode(BLANK_ERROR_FIELD);
      }  
      
      return result;
    }

    public boolean isBlank(String test)
    {
      boolean pass = false;
    
      if(test == null || test.equals("") || test.length() == 0)
      {
        pass = true;
      }
    
      return pass;
    }

  }

}/*@lineinfo:generated-code*/