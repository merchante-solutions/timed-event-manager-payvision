/*@lineinfo:filename=DiscoverRapBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/DiscoverRapBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-09-08 12:54:39 -0700 (Mon, 08 Sep 2008) $
  Version            : $Revision: 15328 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Locale;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


public class DiscoverRapBean extends SQLJConnectionBase
{
  private static final String DEFAULT_PARTNER_NUMBER    = "9415";

  private static final String DECISION_STATUS_APPROVED  = "A";
  private static final String DECISION_STATUS_DECLINED  = "D";
  private static final String DECISION_STATUS_PENDING   = "P";

  private static final String TRANSMISSION_METHOD_FAX   = "F";
  private static final String TRANSMISSION_METHOD_AUTO  = "A";
 
  private static final int    DISC_RATE_VALIDATION_STANDARD_RETAIL        = 1;
  private static final int    DISC_RATE_VALIDATION_MOTO_INTERNET          = 2;
  private static final int    DISC_RATE_VALIDATION_GOVERNMENT_CARD        = 3;
  private static final int    DISC_RATE_VALIDATION_GOVERNMENT_NO_CARD     = 4;
  private static final int    DISC_RATE_VALIDATION_GOVERNMENT_INTERNET    = 5;
  private static final int    DISC_RATE_VALIDATION_QUICK_SERVE_RESTAURANT = 6;
  private static final int    DISC_RATE_VALIDATION_STANDARD_RESTAURANT    = 7;
  private static final int    DISC_RATE_VALIDATION_GROCERY_SUPERMARKET    = 8;

  private static final int    FIELD_LENGTH_DBANAME      = 30;
  private static final int    FIELD_LENGTH_DBAADD       = 30;
  private static final int    FIELD_LENGTH_DBACITY      = 13;
  private static final int    FIELD_LENGTH_PRINNAME     = 45;
  private static final int    FIELD_LENGTH_PRINTITLE    = 15;
  private static final int    FIELD_LENGTH_PRINADD      = 30;
  private static final int    FIELD_LENGTH_PRINCITY     = 13;
  private static final int    FIELD_LENGTH_CORPNAME     = 30;
  private static final int    FIELD_LENGTH_CORPADD      = 30;
  private static final int    FIELD_LENGTH_CORPADD2     = 30;
  private static final int    FIELD_LENGTH_CORPCITY     = 13;
  private static final int    FIELD_LENGTH_BANKACCT     = 17;
  private static final int    FIELD_LENGTH_URL          = 45;
  private static final int    FIELD_LENGTH_EMAIL        = 30;
  private static final int    FIELD_LENGTH_SALESREPID   = 15;

  private long                primaryKey                = 0L;
  private long                merchNum                  = 0L;

  private UserBean            user                      = null;
  
  private boolean             submitted                 = false;
  private boolean             reSubmitted               = false;
  private boolean             manSubmitted              = false;
  private boolean             allowManUpdate            = false;

  private boolean             queuedForTrans            = false;
  private boolean             responseReceived          = false;
  private boolean             errorStatus               = false;

  private boolean             showTimestamps            = false;


  private Timestamp           today                     = null;

  private String              businessDba               = "";
  private String              businessAddress           = "";
  private String              businessCity              = "";

  private String              businessState             = "";
  private String              businessZip               = "";

  private String              businessPhone             = "";
  private String              url                       = "";
  private String              email                     = "";
  private String              busType                   = "";
  private String              corporateName             = "";
  private String              corporateAddress          = "";
  private String              corporateAddressLine2     = "";
  private String              corporateCity             = "";

  private String              corporateState            = "";
  private String              corporateZip              = "";

  private String              yearsInBusiness           = "";
  private String              nonProfit                 = "";
  private String              taxId                     = "";
  private String              checkingAccountNum        = "";
  private String              transitRoute              = "";
  private String              merchantNumber            = "";
  private String              principalName             = "";
  private String              principalTitle            = "";
  private String              principalSsn              = "";
  private String              principalAddress          = "";          
  private String              principalCity             = "";

  private String              principalState            = "";
  private String              principalZip              = "";

  private String              annualSalesVolume         = "";
  private String              averageSaleAmount         = "";
  private String              moto                      = "";
  private String              mcc                       = "";
  private String              discountRate              = "";
  private String              discountPerItem           = "";
  private String              membershipFee             = "";
  private String              salesRepId                = "";
  private String              intermediateLevel2        = "";
  private String              franchiseCode             = "";


  private String              transmissionMethod        = "";
  private String              transmissionStatus        = "OK";
  private String              tsysTimestamp             = "";
  private String              mmsTimestamp              = "";
  private String              forceMoveTimestamp        = "";


  //this comes from merchant_discover_app_completed table..  date_processed field
  private String              dateTimeOfTransmission    = "";

  //these come from merchant_discover_app_response table
  private String              dateTimeOfResponse        = "";
  private String              decision                  = "";
  private String              disposition               = "";
  private String              discoverMerchNum          = "";
  private String              acquirerMerchNum          = "";

  //variable for updating status info manually
  private String              manDecision               = "";  
  private String              manDisposition            = "";
  private String              manDiscNum                = "";
  private String              appSource                 = "";

  public  Vector              states                    = new Vector();
  public  Vector              errors                    = new Vector();
  public  Vector              sicCodes                  = new Vector();
  public  Vector              sicCodeDesc               = new Vector();

  public  Vector              decisionCodes             = new Vector();
  public  Vector              decisionCodeDesc          = new Vector();

  public  Vector              dispositionCodes          = new Vector();
  public  Vector              dispositionCodeDesc       = new Vector();
  
  public DiscoverRapBean()
  {
    fillDropDowns();
  }

  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */  
  public void fillDropDowns()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:199^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   countrystate_code 
//          from     countrystate
//          order by countrystate_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   countrystate_code \n        from     countrystate\n        order by countrystate_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.DiscoverRapBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:204^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        states.add(rs.getString("countrystate_code"));
      }
 
      it.close();
    

      /*@lineinfo:generated-code*//*@lineinfo:216^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   sic_code,merchant_type 
//          from     sic_codes
//          order by sic_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   sic_code,merchant_type \n        from     sic_codes\n        order by sic_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.DiscoverRapBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:221^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        sicCodes.add(rs.getString("sic_code"));
        sicCodeDesc.add(rs.getString("merchant_type"));
      }
 
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:233^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   DISPOSITION_CODE,DISPOSITION_DESC 
//          from     RAP_APP_DISPOSITION_CODES
//          order by DISPOSITION_CODE asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   DISPOSITION_CODE,DISPOSITION_DESC \n        from     RAP_APP_DISPOSITION_CODES\n        order by DISPOSITION_CODE asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.DiscoverRapBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:238^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        dispositionCodes.add(rs.getString("DISPOSITION_CODE"));
        dispositionCodeDesc.add(rs.getString("DISPOSITION_DESC"));
      }
 
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:250^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   DECISION_CODE,DECISION_DESC 
//          from     RAP_APP_DECISION_STATUS
//          where    DECISION_CODE != 'E'
//          order by DECISION_CODE asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   DECISION_CODE,DECISION_DESC \n        from     RAP_APP_DECISION_STATUS\n        where    DECISION_CODE != 'E'\n        order by DECISION_CODE asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.DiscoverRapBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:256^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        decisionCodes.add(rs.getString("DECISION_CODE"));
        decisionCodeDesc.add(rs.getString("DECISION_DESC"));
      }

      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:268^7*/

//  ************************************************************
//  #sql [Ctx] { select    sysdate 
//          from      dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    sysdate  \n        from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.DiscoverRapBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   today = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:272^7*/

    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public long startNewApplication(long merchNum)
  {
    long appseqnum  = -1L;
    long controlNum = -1L;
    int  bankNum    = -1;

    if(merchNum <= 0)
    {
      addError("This account can not be setup in discover");
      return -1;
    }

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:301^7*/

//  ************************************************************
//  #sql [Ctx] { select  bank_number
//          
//          from    mif
//          where   merchant_number = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  bank_number\n         \n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.DiscoverRapBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:307^7*/

      //its a valid merch_num so set the merchnum
      this.merchNum = merchNum;
      
      int appType = 0;
      
      switch(bankNum)
      {
        case 3858:
          appType = mesConstants.APP_TYPE_CBT_NEW;
          break;
          
        case 3941:
        default:
          appType = mesConstants.APP_TYPE_MES;
          break;
      }

      // create skeleton application in the database to work from
      /*@lineinfo:generated-code*//*@lineinfo:327^7*/

//  ************************************************************
//  #sql [Ctx] { call create_shadow_app(:this.merchNum, :appType)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN create_shadow_app( :1 ,  :2 )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,this.merchNum);
   __sJT_st.setInt(2,appType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:330^7*/

      //NOW WE GET THE APP SEQ NUM.. FROM THE APPLICATION THAT WAS EITHER ALREADY THERE OR THE ONE THE PROCEDURE JUST CREATED
      /*@lineinfo:generated-code*//*@lineinfo:333^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num 
//          
//          from    merchant
//          where   merch_number = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num \n         \n        from    merchant\n        where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.DiscoverRapBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appseqnum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:339^7*/
    }
    catch(Exception e)
    {
      logEntry(("startNewApplication(" + merchNum + ")"), e.toString());
      addError("This account can not be setup in discover (" + e.toString() + ")");
      appseqnum = -1;
    }
    finally
    {
      cleanUp();
    }

    return appseqnum;
  }

  
  public void getData(long pk)
  {
    getAppContent(pk);
    getAppResponse(pk);
  }

  /*
  ** METHOD public void getAppResponse()
  **
  */
  public void getAppResponse(long pk)
  {
    String  dateOfResponse = "";
    String  timeOfResponse = "";
    ResultSetIterator it = null;
    ResultSet         rs = null;

    try
    {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:376^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mdar.*, 
//                    rads.decision_desc, 
//                    radc.disposition_desc
//  
//            from    merchant_discover_app_response  mdar, 
//                    rap_app_decision_status         rads, 
//                    rap_app_disposition_codes       radc
//  
//            where   mdar.app_seq_num              = :pk and 
//                    mdar.decision_status          = rads.decision_code and
//                    mdar.disposition_reason_code  = radc.disposition_code(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mdar.*, \n                  rads.decision_desc, \n                  radc.disposition_desc\n\n          from    merchant_discover_app_response  mdar, \n                  rap_app_decision_status         rads, \n                  rap_app_disposition_codes       radc\n\n          where   mdar.app_seq_num              =  :1  and \n                  mdar.decision_status          = rads.decision_code and\n                  mdar.disposition_reason_code  = radc.disposition_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:389^9*/
        
        rs = it.getResultSet();
 
        if(rs.next())
        {
           responseReceived = true;          
           queuedForTrans   = true;
           
           dateOfResponse             = isBlank(rs.getString("date_last_updated"))        ? "" : java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("date_last_updated"));
           timeOfResponse             = isBlank(rs.getString("date_last_updated"))        ? "" : java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("date_last_updated"));

           if(!isBlank(dateOfResponse) && !isBlank(timeOfResponse))
           {
             dateTimeOfResponse = dateOfResponse + " " + timeOfResponse;
           }
           else if(!isBlank(dateOfResponse))
           {
             dateTimeOfResponse = dateOfResponse;
           }

           this.decision              = isBlank(rs.getString("decision_desc"))            ? "" : rs.getString("decision_desc");
           this.disposition           = isBlank(rs.getString("disposition_desc"))         ? "" : rs.getString("disposition_desc");
           
           if(!isBlank(rs.getString("DECISION_STATUS")) && (rs.getString("DECISION_STATUS")).equals("E"))
           {
             DiscoverErrorHelperBean errorBean = new DiscoverErrorHelperBean();
             boolean isErrorGood      = errorBean.setErrorReasons(rs.getString("ERROR_FLAGS"));
             this.disposition         = "<font color=red>Error In Fields:</font><BR>" + errorBean.getBrList();
             this.errorStatus         = true;
             this.transmissionStatus  = "ERROR";
           }

           this.discoverMerchNum      = isBlank(rs.getString("discover_merchant_num"))    ? "" : rs.getString("discover_merchant_num");
           
           if(!isBlank(this.discoverMerchNum))
           {
             this.showTimestamps = true;
           }

           this.acquirerMerchNum      = isBlank(rs.getString("acquirer_merchant_num"))    ? "" : rs.getString("acquirer_merchant_num");

           if(this.errorStatus) //check to see if it was retransmitted
           {
             if(retransmitted(pk))
             {
               transmissionStatus = "RETRANSMITTED";
             }
           }

        }
      
        rs.close();
        it.close();
        
    }
    catch(Exception e)
    {
      logEntry("getAppResponse()", e.toString());
      addError("Error: Complications occurred while retrieving data!  Please refresh.");
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public int getAppType(long pk)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    int                 appType     = -1;

    try
    {
      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:468^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type,
//                  appsrctype_code
//          from    application
//          where   app_seq_num   = :pk
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_type,\n                appsrctype_code\n        from    application\n        where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:474^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        appType = rs.getInt("app_type");
        appSource = rs.getString("appsrctype_code");
      }
 
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getAppType()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return appType;
  }



  private boolean retransmitted(long pk)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
     
      /*@lineinfo:generated-code*//*@lineinfo:519^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  status_code
//          from    app_tracking
//          where   app_seq_num   = :pk and
//                  dept_code     = :QueueConstants.DEPARTMENT_DISCOVER_RAP
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  status_code\n        from    app_tracking\n        where   app_seq_num   =  :1  and\n                dept_code     =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_DISCOVER_RAP);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:525^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        if(rs.getInt("status_code") == QueueConstants.DEPT_STATUS_DISCOVER_RAP_RETRANSMISSION)
        {
          result = true;
        }
      }
 
      it.close();

    }
    catch(Exception e)
    {
      logEntry("retransmitted()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }




  /*
  ** METHOD public void getAppContent()
  **
  */
  public void getAppContent(long pk)
  {
    String    dateOfTransmission = "";
    String    timeOfTransmission = "";

    String    dateOfTsys         = "";
    String    timeOfTsys         = "";

    String    dateOfMms          = "";
    String    timeOfMms          = "";

    String    dateOfForceMove    = "";
    String    timeOfForceMove    = "";
    
    ResultSet rs = null;
    ResultSetIterator it = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:583^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mda.*,
//                  mdac.date_processed
//          from    merchant_discover_app mda,
//                  merchant_discover_app_complete mdac
//          where   mda.app_seq_num = :pk and 
//                  mda.app_seq_num = mdac.app_seq_num(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mda.*,\n                mdac.date_processed\n        from    merchant_discover_app mda,\n                merchant_discover_app_complete mdac\n        where   mda.app_seq_num =  :1  and \n                mda.app_seq_num = mdac.app_seq_num(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:591^7*/
      
      rs = it.getResultSet();
 
      if(rs.next())
      {
        queuedForTrans             = true;

        this.primaryKey            = rs.getLong("APP_SEQ_NUM");
        this.businessDba           = isBlank(rs.getString("DBA_NAME"))               ? "" : rs.getString("DBA_NAME");
        this.businessAddress       = isBlank(rs.getString("DBA_ADDRESS"))            ? "" : rs.getString("DBA_ADDRESS");
        this.businessCity          = isBlank(rs.getString("DBA_CITY"))               ? "" : rs.getString("DBA_CITY");
        this.businessState         = isBlank(rs.getString("DBA_STATE"))              ? "" : rs.getString("DBA_STATE");
        this.businessZip           = isBlank(rs.getString("DBA_ZIP"))                ? "" : rs.getString("DBA_ZIP");
        this.businessPhone         = isBlank(rs.getString("DBA_PHONE"))              ? "" : rs.getString("DBA_PHONE");
        this.url                   = isBlank(rs.getString("DBA_URL"))                ? "" : rs.getString("DBA_URL");
        this.email                 = isBlank(rs.getString("DBA_EMAIL"))              ? "" : rs.getString("DBA_EMAIL");
        this.principalName         = isBlank(rs.getString("PRINCIPAL_NAME"))         ? "" : rs.getString("PRINCIPAL_NAME");
        this.principalTitle        = isBlank(rs.getString("PRINCIPAL_TITLE"))        ? "" : rs.getString("PRINCIPAL_TITLE");
        this.principalSsn          = isBlank(rs.getString("PRINCIPAL_SSN"))          ? "" : rs.getString("PRINCIPAL_SSN");
        this.principalSsn          = fixSsn(this.principalSsn);
        this.principalAddress      = isBlank(rs.getString("PRINCIPAL_ADDRESS"))      ? "" : rs.getString("PRINCIPAL_ADDRESS");
        this.principalCity         = isBlank(rs.getString("PRINCIPAL_CITY"))         ? "" : rs.getString("PRINCIPAL_CITY");
        this.principalState        = isBlank(rs.getString("PRINCIPAL_STATE"))        ? "" : rs.getString("PRINCIPAL_STATE");
        this.principalZip          = isBlank(rs.getString("PRINCIPAL_ZIP"))          ? "" : rs.getString("PRINCIPAL_ZIP");
        this.corporateName         = isBlank(rs.getString("CORPORATE_NAME"))         ? "" : rs.getString("CORPORATE_NAME");
        this.corporateAddress      = isBlank(rs.getString("CORPORATE_ADDRESS"))      ? "" : rs.getString("CORPORATE_ADDRESS");
        this.corporateAddressLine2 = isBlank(rs.getString("CORPORATE_ADDRESS2"))     ? "" : rs.getString("CORPORATE_ADDRESS2");
        this.corporateCity         = isBlank(rs.getString("CORPORATE_CITY"))         ? "" : rs.getString("CORPORATE_CITY");
        this.corporateState        = isBlank(rs.getString("CORPORATE_STATE"))        ? "" : rs.getString("CORPORATE_STATE");
        this.corporateZip          = isBlank(rs.getString("CORPORATE_ZIP"))          ? "" : rs.getString("CORPORATE_ZIP");
        this.yearsInBusiness       = isBlank(rs.getString("YEARS_IN_BUSINESS"))      ? "" : rs.getString("YEARS_IN_BUSINESS");
        this.merchantNumber        = isBlank(rs.getString("ACQUIRER_MERCHANT_NUM"))  ? "" : rs.getString("ACQUIRER_MERCHANT_NUM");
        this.taxId                 = isBlank(rs.getString("FEDERAL_TAX_ID"))         ? "" : rs.getString("FEDERAL_TAX_ID");
        this.taxId                 = fixSsn(this.taxId);
        this.nonProfit             = isBlank(rs.getString("NON_PROFIT_FLAG"))        ? "" : rs.getString("NON_PROFIT_FLAG");
        this.busType               = isBlank(rs.getString("BUSINESS_TYPE"))          ? "" : rs.getString("BUSINESS_TYPE");
        this.transitRoute          = isBlank(rs.getString("TRANSIT_ROUTING_NUM"))    ? "" : rs.getString("TRANSIT_ROUTING_NUM");
        this.checkingAccountNum    = isBlank(rs.getString("ACCOUNT_DDA"))            ? "" : rs.getString("ACCOUNT_DDA");
        this.discountRate          = isBlank(rs.getString("DISCOVER_DISC_RATE"))     ? "" : rs.getString("DISCOVER_DISC_RATE");
        this.discountPerItem       = isBlank(rs.getString("DISCOVER_per_item_RATE")) ? "" : rs.getString("DISCOVER_per_item_RATE");
        this.membershipFee         = isBlank(rs.getString("MEMBERSHIP_FEE"))         ? "" : rs.getString("MEMBERSHIP_FEE");
        this.mcc                   = isBlank(rs.getString("MERCHANT_CAT_CODE"))      ? "" : rs.getString("MERCHANT_CAT_CODE");
        this.moto                  = isBlank(rs.getString("MOTO_FLAG"))              ? "" : rs.getString("MOTO_FLAG");
        this.averageSaleAmount     = isBlank(rs.getString("AVERAGE_SALE"))           ? "" : rs.getString("AVERAGE_SALE");
        this.annualSalesVolume     = isBlank(rs.getString("ANNUAL_SALES"))           ? "" : rs.getString("ANNUAL_SALES");
        this.salesRepId            = isBlank(rs.getString("SALES_REP_ID"))           ? "" : rs.getString("SALES_REP_ID");
        this.transmissionMethod    = isBlank(rs.getString("transmission_method"))    ? "" : rs.getString("transmission_method");
        this.intermediateLevel2    = isBlank(rs.getString("INTERMEDIATE_LEVEL2"))    ? "" : rs.getString("INTERMEDIATE_LEVEL2");
        this.franchiseCode         = isBlank(rs.getString("FRANCHISE_CODE"))         ? "" : rs.getString("FRANCHISE_CODE");

        dateOfTransmission         = isBlank(rs.getString("date_processed"))               ? "not yet" : java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("date_processed"));
        timeOfTransmission         = isBlank(rs.getString("date_processed"))               ? "sent" : java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("date_processed"));
        dateTimeOfTransmission     = glueDate(dateOfTransmission,timeOfTransmission);

        dateOfTsys                 = isBlank(rs.getString("tsys_updated"))           ? "" : java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("tsys_updated"));
        timeOfTsys                 = isBlank(rs.getString("tsys_updated"))           ? "" : java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("tsys_updated"));
        tsysTimestamp              = glueDate(dateOfTsys,timeOfTsys);

        dateOfMms                  = isBlank(rs.getString("mms_updated"))            ? "" : java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("mms_updated"));
        timeOfMms                  = isBlank(rs.getString("mms_updated"))            ? "" : java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("mms_updated"));
        mmsTimestamp               = glueDate(dateOfMms,timeOfMms);

        dateOfForceMove            = isBlank(rs.getString("forced_to_mms"))            ? "" : java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("forced_to_mms"));
        timeOfForceMove            = isBlank(rs.getString("forced_to_mms"))            ? "" : java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("forced_to_mms"));
        forceMoveTimestamp         = glueDate(dateOfForceMove,timeOfForceMove);

      }
      else
      {
        rs.close();
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:664^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.merch_business_name           dba_name,
//                    add1.address_line1                  dba_address,
//                    add1.address_city                   dba_city,
//                    add1.countrystate_code              dba_state,
//                    substr(add1.address_zip,1,5)        dba_zip,
//                    add1.address_phone                  dba_phone,
//                    merch.merch_web_url                 dba_url,
//                    merch.merch_email_address           dba_email,
//                    merch.merch_business_establ_year    established_year,
//                    bus.busowner_first_name             principal_fname,
//                    bus.busowner_last_name              principal_lname,
//                    bus.busowner_ssn                    principal_ssn,
//                    bus.busowner_title                  principal_title,
//                    decode(add4.address_line1, null, add1.address_line1, add4.address_line1)                principal_address,
//                    decode(add4.address_city, null, add1.address_city, add4.address_city)                   principal_city,
//                    decode(add4.countrystate_code, null, add1.countrystate_code, add4.countrystate_code)    principal_state,
//                    decode(add4.address_zip, null, add1.address_zip, add4.address_zip)                      principal_zip,
//                    merch.merch_federal_tax_id          federal_tax_id,
//                    merch.merch_legal_name              corporate_name,
//                    add1.address_line1                  corporate_address,
//                    add1.address_line2                  corporate_address2,
//                    add1.address_city                   corporate_city,
//                    add1.countrystate_code              corporate_state,
//                    add1.address_zip                    corporate_zip,
//                    merch.sic_code                      merchant_cat_code,
//                    merch.merch_mail_phone_sales        moto_flag,
//                    merch.merch_month_visa_mc_sales     monthly_sales, 
//                    merch.merch_average_cc_tran         average_ticket,
//                    merch.bustype_code                  business_type,
//                    merch.discover_rep_id               discover_rep_id,
//                    decode(mf.transit_routng_num,
//                      null, bank.merchbank_transit_route_num,
//                      mf.transit_routng_num)            transit_routing_num,
//                    decode(mf.dda_num,
//                      null, bank.merchbank_acct_num,
//                      mf.dda_num)                       account_dda,
//                    merch.merch_years_in_business       years_in_business,
//                    merch.merch_number                  acquirer_merchant_num,
//                    pay.merchpo_rate                    discover_disc_rate,
//                    pay.merchpo_fee                     discover_per_item_rate,
//                    app.app_type                        app_type,
//                    app.app_user_login                  user_login,
//                    app.appsrctype_code                 intermediate_level2
//            from    merchant        merch,
//                    merchbank       bank,
//                    address         add1,
//                    address         add4,
//                    businessowner   bus,
//                    merchpayoption  pay,
//                    application     app,
//                    mif             mf
//            where   merch.app_seq_num = :pk and
//                    merch.app_seq_num = bank.app_seq_num(+) and
//                    merch.app_seq_num = app.app_seq_num and
//                    merch.app_seq_num = add1.app_seq_num and 
//                    add1.addresstype_code  = 1 and
//                    merch.app_seq_num = add4.app_seq_num(+) and 
//                    add4.addresstype_code(+) = 4 and
//                    merch.app_seq_num = bus.app_seq_num(+) and 
//                    bus.busowner_num(+)  = 1 and
//                    merch.app_seq_num = pay.app_seq_num(+) and 
//                    pay.cardtype_code(+) = 14 and
//                    merch.merch_number = mf.merchant_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.merch_business_name           dba_name,\n                  add1.address_line1                  dba_address,\n                  add1.address_city                   dba_city,\n                  add1.countrystate_code              dba_state,\n                  substr(add1.address_zip,1,5)        dba_zip,\n                  add1.address_phone                  dba_phone,\n                  merch.merch_web_url                 dba_url,\n                  merch.merch_email_address           dba_email,\n                  merch.merch_business_establ_year    established_year,\n                  bus.busowner_first_name             principal_fname,\n                  bus.busowner_last_name              principal_lname,\n                  bus.busowner_ssn                    principal_ssn,\n                  bus.busowner_title                  principal_title,\n                  decode(add4.address_line1, null, add1.address_line1, add4.address_line1)                principal_address,\n                  decode(add4.address_city, null, add1.address_city, add4.address_city)                   principal_city,\n                  decode(add4.countrystate_code, null, add1.countrystate_code, add4.countrystate_code)    principal_state,\n                  decode(add4.address_zip, null, add1.address_zip, add4.address_zip)                      principal_zip,\n                  merch.merch_federal_tax_id          federal_tax_id,\n                  merch.merch_legal_name              corporate_name,\n                  add1.address_line1                  corporate_address,\n                  add1.address_line2                  corporate_address2,\n                  add1.address_city                   corporate_city,\n                  add1.countrystate_code              corporate_state,\n                  add1.address_zip                    corporate_zip,\n                  merch.sic_code                      merchant_cat_code,\n                  merch.merch_mail_phone_sales        moto_flag,\n                  merch.merch_month_visa_mc_sales     monthly_sales, \n                  merch.merch_average_cc_tran         average_ticket,\n                  merch.bustype_code                  business_type,\n                  merch.discover_rep_id               discover_rep_id,\n                  decode(mf.transit_routng_num,\n                    null, bank.merchbank_transit_route_num,\n                    mf.transit_routng_num)            transit_routing_num,\n                  decode(mf.dda_num,\n                    null, bank.merchbank_acct_num,\n                    mf.dda_num)                       account_dda,\n                  merch.merch_years_in_business       years_in_business,\n                  merch.merch_number                  acquirer_merchant_num,\n                  pay.merchpo_rate                    discover_disc_rate,\n                  pay.merchpo_fee                     discover_per_item_rate,\n                  app.app_type                        app_type,\n                  app.app_user_login                  user_login,\n                  app.appsrctype_code                 intermediate_level2\n          from    merchant        merch,\n                  merchbank       bank,\n                  address         add1,\n                  address         add4,\n                  businessowner   bus,\n                  merchpayoption  pay,\n                  application     app,\n                  mif             mf\n          where   merch.app_seq_num =  :1  and\n                  merch.app_seq_num = bank.app_seq_num(+) and\n                  merch.app_seq_num = app.app_seq_num and\n                  merch.app_seq_num = add1.app_seq_num and \n                  add1.addresstype_code  = 1 and\n                  merch.app_seq_num = add4.app_seq_num(+) and \n                  add4.addresstype_code(+) = 4 and\n                  merch.app_seq_num = bus.app_seq_num(+) and \n                  bus.busowner_num(+)  = 1 and\n                  merch.app_seq_num = pay.app_seq_num(+) and \n                  pay.cardtype_code(+) = 14 and\n                  merch.merch_number = mf.merchant_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:729^9*/

        rs = it.getResultSet();
 
        if(rs.next())
        {
          this.businessDba           = isBlank(rs.getString("DBA_NAME"))               ? "" : rs.getString("DBA_NAME");
          this.businessAddress       = isBlank(rs.getString("DBA_ADDRESS"))            ? "" : rs.getString("DBA_ADDRESS");
          this.businessCity          = isBlank(rs.getString("DBA_CITY"))               ? "" : rs.getString("DBA_CITY");
          this.businessState         = isBlank(rs.getString("DBA_STATE"))              ? "" : rs.getString("DBA_STATE");
          this.businessZip           = isBlank(rs.getString("DBA_ZIP"))                ? "" : rs.getString("DBA_ZIP");
          this.businessPhone         = isBlank(rs.getString("DBA_PHONE"))              ? "" : rs.getString("DBA_PHONE");
          this.url                   = isBlank(rs.getString("DBA_URL"))                ? "" : rs.getString("DBA_URL");
          this.email                 = isBlank(rs.getString("DBA_EMAIL"))              ? "" : rs.getString("DBA_EMAIL");
          
          String principalFName      = isBlank(rs.getString("PRINCIPAL_FNAME"))        ? "" : rs.getString("PRINCIPAL_FNAME");
          String principalLName      = isBlank(rs.getString("PRINCIPAL_LNAME"))        ? "" : rs.getString("PRINCIPAL_LNAME");
          
          this.principalName         = principalFName + " " + principalLName;
          
          this.principalTitle        = isBlank(rs.getString("PRINCIPAL_TITLE"))        ? "" : rs.getString("PRINCIPAL_TITLE");
          this.principalSsn          = isBlank(rs.getString("PRINCIPAL_SSN"))          ? "" : rs.getString("PRINCIPAL_SSN");
          this.principalSsn          = fixSsn(this.principalSsn);
          this.principalAddress      = isBlank(rs.getString("PRINCIPAL_ADDRESS"))      ? "" : rs.getString("PRINCIPAL_ADDRESS");
          this.principalCity         = isBlank(rs.getString("PRINCIPAL_CITY"))         ? "" : rs.getString("PRINCIPAL_CITY");
          this.principalState        = isBlank(rs.getString("PRINCIPAL_STATE"))        ? "" : rs.getString("PRINCIPAL_STATE");
          this.principalZip          = isBlank(rs.getString("PRINCIPAL_ZIP"))          ? "" : rs.getString("PRINCIPAL_ZIP");
          this.corporateName         = isBlank(rs.getString("CORPORATE_NAME"))         ? "" : rs.getString("CORPORATE_NAME");
          this.corporateAddress      = isBlank(rs.getString("CORPORATE_ADDRESS"))      ? "" : rs.getString("CORPORATE_ADDRESS");
          this.corporateAddressLine2 = isBlank(rs.getString("CORPORATE_ADDRESS2"))     ? "" : rs.getString("CORPORATE_ADDRESS2");
          this.corporateCity         = isBlank(rs.getString("CORPORATE_CITY"))         ? "" : rs.getString("CORPORATE_CITY");
          this.corporateState        = isBlank(rs.getString("CORPORATE_STATE"))        ? "" : rs.getString("CORPORATE_STATE");
          this.corporateZip          = isBlank(rs.getString("CORPORATE_ZIP"))          ? "" : rs.getString("CORPORATE_ZIP");
          this.yearsInBusiness       = isBlank(rs.getString("YEARS_IN_BUSINESS"))      ? "" : rs.getString("YEARS_IN_BUSINESS");
          this.merchantNumber        = isBlank(rs.getString("ACQUIRER_MERCHANT_NUM"))  ? "" : rs.getString("ACQUIRER_MERCHANT_NUM");
          this.taxId                 = isBlank(rs.getString("FEDERAL_TAX_ID"))         ? "" : rs.getString("FEDERAL_TAX_ID");
          this.taxId                 = fixSsn(this.taxId);
          
          this.busType               = isBlank(rs.getString("BUSINESS_TYPE"))          ? "" : rs.getString("BUSINESS_TYPE");
          this.intermediateLevel2    = isBlank(rs.getString("intermediate_level2"))    ? "" : rs.getString("intermediate_level2");


          //default to N.. if bustype = non profit it will get set to Y
          this.nonProfit = "N";

          if(!isBlank(rs.getString("BUSINESS_TYPE")))
          {
            int tempInt = rs.getInt("BUSINESS_TYPE");

            switch(tempInt)
            {
              case 1:
                this.busType = "S";
                if(isBlank(this.principalTitle))
                {
                  this.principalTitle = "Owner";
                }
              break;
              case 2:
              case 4:
              case 5:
              case 7:
              case 9:
                this.busType = "C";
                if(isBlank(this.principalTitle))
                {
                  this.principalTitle = "Officer";
                }
              break;
              case 6:
                this.busType = "C";
                this.nonProfit = "Y";
                if(isBlank(this.principalTitle))
                {
                  this.principalTitle = "Officer";
                }
              break;
              case 3:
                this.busType = "P";
                if(isBlank(this.principalTitle))
                {
                  this.principalTitle = "Partner";
                }
              break;
            }
          }

          if(isBlank(this.principalTitle))
          {
            this.principalTitle = "Unknown";
          }

          if(!isBlank(rs.getString("ESTABLISHED_YEAR")))
          {
            String year = (today.toString()).substring(0,4);
            yearsInBusiness = getYears(year,rs.getString("ESTABLISHED_YEAR"));
          }

          this.transitRoute          = isBlank(rs.getString("TRANSIT_ROUTING_NUM"))    ? "" : rs.getString("TRANSIT_ROUTING_NUM");
          this.checkingAccountNum    = isBlank(rs.getString("ACCOUNT_DDA"))            ? "" : rs.getString("ACCOUNT_DDA");
          this.mcc                   = isBlank(rs.getString("MERCHANT_CAT_CODE"))      ? "" : rs.getString("MERCHANT_CAT_CODE");

          if(!isBlank(rs.getString("MOTO_FLAG")))
          {
            int tempInt = rs.getInt("MOTO_FLAG");
            if(tempInt < 50)
            {
              this.moto = "N";
            }
            else
            {
              this.moto = "Y";
            }
          }
      
          this.discountRate          = isBlank(rs.getString("DISCOVER_DISC_RATE"))     ? "" : rs.getString("DISCOVER_DISC_RATE");
          this.discountPerItem       = isBlank(rs.getString("DISCOVER_per_item_RATE")) ? "" : rs.getString("DISCOVER_per_item_RATE");
          this.averageSaleAmount     = isBlank(rs.getString("AVERAGE_TICKET"))         ? "" : rs.getString("AVERAGE_TICKET");
          
          if(!isBlank(rs.getString("MONTHLY_SALES")))
          {
            long tempLong = rs.getLong("MONTHLY_SALES");
            tempLong *= 12;
            this.annualSalesVolume = Long.toString(tempLong);
          }
          
          if(!isBlank(rs.getString("app_type")))
          {
            if(rs.getInt("app_type") == mesConstants.APP_TYPE_DISCOVER ||
               rs.getInt("app_type") == mesConstants.APP_TYPE_DISCOVER_IMS)
            {
              this.membershipFee = "0";
              this.salesRepId    = isBlank(rs.getString("discover_rep_id"))            ? "" : rs.getString("discover_rep_id");
            }
            else if(rs.getInt("app_type") == mesConstants.APP_TYPE_BBT)
            {
              this.membershipFee    = "";
              this.discountPerItem  = ".10";
              this.salesRepId       = isBlank(rs.getString("user_login"))                 ? "" : rs.getString("user_login");
            }
            else
            {
              this.membershipFee    = "";
              this.discountPerItem  = ".10";
              this.salesRepId       = isBlank(rs.getString("user_login"))                 ? "" : rs.getString("user_login");
            }

            if(rs.getInt("app_type") == mesConstants.APP_TYPE_SABRE)
            { 
              this.franchiseCode = "1886";
            }
          }
        }
        else
        {
          rs.close();
          it.close();
          
          /*@lineinfo:generated-code*//*@lineinfo:887^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.dba_name              dba_name,
//                      m.dmaddr                dmaddr,
//                      m.dmcity                dmcity,
//                      m.dmstate               dmstate,
//                      m.dmzip                 dmzip,
//                      m.phone_1               phone_1,
//                      m.bank_number           bank_number,
//                      m.fdr_corp_name         fdr_corp_name,
//                      m.addr1_line_1          addr1_line_1,
//                      m.addr1_line_2          addr1_line_2,
//                      m.city1_line_4          city1_line_4,
//                      m.state1_line_4         state1_line_4,
//                      m.zip1_line_4           zip1_line_4,
//                      m.merchant_number       merchant_number,
//                      m.federal_tax_id        federal_tax_id,
//                      m.transit_routng_num    transit_routng_num,
//                      m.dda_num               dda_num,
//                      m.class_code            class_code,
//                      gn.g2_owner_name        g2_owner_name, 
//                      gn.g2_owner_ssn         g2_owner_ssn
//              from    mif  m,
//                      monthly_extract_gn gn
//              where   m.merchant_number = :merchNum and
//                      m.merchant_number = gn.hh_merchant_number(+) and
//                      gn.hh_active_date(+) = add_months(trunc(sysdate, 'MONTH'), -1)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.dba_name              dba_name,\n                    m.dmaddr                dmaddr,\n                    m.dmcity                dmcity,\n                    m.dmstate               dmstate,\n                    m.dmzip                 dmzip,\n                    m.phone_1               phone_1,\n                    m.bank_number           bank_number,\n                    m.fdr_corp_name         fdr_corp_name,\n                    m.addr1_line_1          addr1_line_1,\n                    m.addr1_line_2          addr1_line_2,\n                    m.city1_line_4          city1_line_4,\n                    m.state1_line_4         state1_line_4,\n                    m.zip1_line_4           zip1_line_4,\n                    m.merchant_number       merchant_number,\n                    m.federal_tax_id        federal_tax_id,\n                    m.transit_routng_num    transit_routng_num,\n                    m.dda_num               dda_num,\n                    m.class_code            class_code,\n                    gn.g2_owner_name        g2_owner_name, \n                    gn.g2_owner_ssn         g2_owner_ssn\n            from    mif  m,\n                    monthly_extract_gn gn\n            where   m.merchant_number =  :1  and\n                    m.merchant_number = gn.hh_merchant_number(+) and\n                    gn.hh_active_date(+) = add_months(trunc(sysdate, 'MONTH'), -1)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:914^11*/

          rs = it.getResultSet();
   
          if(rs.next())
          {
            this.businessDba           = isBlank(rs.getString("DBA_NAME"))              ? "" : rs.getString("DBA_NAME");
            this.businessAddress       = isBlank(rs.getString("DMADDR"))                ? "" : rs.getString("DMADDR");
            this.businessCity          = isBlank(rs.getString("DMCITY"))                ? "" : rs.getString("DMCITY");
            this.businessState         = isBlank(rs.getString("DMSTATE"))               ? "" : rs.getString("DMSTATE");
            this.businessZip           = isBlank(rs.getString("DMZIP"))                 ? "" : rs.getString("DMZIP");
            this.businessPhone         = isBlank(rs.getString("PHONE_1"))               ? "" : rs.getString("PHONE_1");

            if(rs.getString("bank_number").equals("3867"))
            {
              this.intermediateLevel2    = "BBT";
            }

            this.principalName         = isBlank(rs.getString("G2_OWNER_NAME"))         ? "" : rs.getString("G2_OWNER_NAME");
            this.principalSsn          = isBlank(rs.getString("G2_OWNER_SSN"))          ? "" : rs.getString("G2_OWNER_SSN");
            this.principalSsn          = fixSsn(this.principalSsn);

            this.principalAddress      = isBlank(rs.getString("DMADDR"))                ? "" : rs.getString("DMADDR");
            this.principalCity         = isBlank(rs.getString("DMCITY"))                ? "" : rs.getString("DMCITY");
            this.principalState        = isBlank(rs.getString("DMSTATE"))               ? "" : rs.getString("DMSTATE");
            this.principalZip          = isBlank(rs.getString("DMZIP"))                 ? "" : rs.getString("DMZIP");

            this.corporateName         = isBlank(rs.getString("FDR_CORP_NAME"))         ? "" : rs.getString("FDR_CORP_NAME");
            this.corporateAddress      = isBlank(rs.getString("ADDR1_LINE_1"))          ? "" : rs.getString("ADDR1_LINE_1");
            this.corporateAddressLine2 = isBlank(rs.getString("ADDR1_LINE_2"))          ? "" : rs.getString("ADDR1_LINE_2");
            this.corporateCity         = isBlank(rs.getString("CITY1_LINE_4"))          ? "" : rs.getString("CITY1_LINE_4");
            this.corporateState        = isBlank(rs.getString("STATE1_LINE_4"))         ? "" : rs.getString("STATE1_LINE_4");
            this.corporateZip          = isBlank(rs.getString("ZIP1_LINE_4"))           ? "" : rs.getString("ZIP1_LINE_4");

            this.merchantNumber        = isBlank(rs.getString("MERCHANT_NUMBER"))       ? "" : rs.getString("MERCHANT_NUMBER");
            this.taxId                 = isBlank(rs.getString("FEDERAL_TAX_ID"))        ? "" : rs.getString("FEDERAL_TAX_ID");
            this.taxId                 = fixSsn(this.taxId);

            this.transitRoute          = isBlank(rs.getString("TRANSIT_ROUTNG_NUM"))    ? "" : rs.getString("TRANSIT_ROUTNG_NUM");
            this.checkingAccountNum    = isBlank(rs.getString("DDA_NUM"))               ? "" : rs.getString("DDA_NUM");
            this.mcc                   = isBlank(rs.getString("CLASS_CODE"))            ? "" : rs.getString("CLASS_CODE");

            this.membershipFee         = "";
            this.discountPerItem       = ".10";
            this.salesRepId            = user.getLoginName();
          }
          else
          {
            addError("This account can not be setup in discover");
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getAppContent()", e.toString());
      addError("Error: Complications occurred while retrieving data!  Please refresh.");
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  private String fixSsn(String ssn)
  {
    String result = ssn;

    if(isBlank(ssn))
    {
      return "";
    }
    else if(ssn.length() == 8)
    {
      result = "0" + ssn;
    }
    else if(ssn.length() == 7)
    {
      result = "00" + ssn;
    }
    else if(ssn.length() == 6)
    {
      result = "000" + ssn;
    }

    return result;
  }


  private String getYears(String curYear, String estabYear)
  {
    String result = "";

    try
    {
      int year1 = Integer.parseInt(curYear);
      int year2 = Integer.parseInt(estabYear);
      int years = year1 - year2;

      result    = Integer.toString(years);

      if(years >= 100)
      {
        result = "99";
      }
      else if(years <= 0)
      {
        result = "0";
      }
    }
    catch(Exception e)
    {
    }
    return result;

  }

  private String glueDate(String dateStr, String timeStr)
  {
    String result = "";

    if(!isBlank(dateStr) && !isBlank(timeStr))
    {
      result = dateStr + " " + timeStr;
    }
    else if(!isBlank(dateStr))
    {
      result = dateStr;
    }
    
    return result;

  }
  
  public void insertData(long pk)
  {
    boolean useCleanUp = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }

      if(!completedRecordExists(pk)) //then we insert it into database, so it will be uploaded in file
      {
       
        /*@lineinfo:generated-code*//*@lineinfo:1065^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_discover_app_complete
//            ( 
//              APP_SEQ_NUM,
//              DATE_COMPLETED
//            )
//            
//            values
//            ( 
//              :pk,
//              :today
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant_discover_app_complete\n          ( \n            APP_SEQ_NUM,\n            DATE_COMPLETED\n          )\n          \n          values\n          ( \n             :1 ,\n             :2 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   __sJT_st.setTimestamp(2,today);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1078^9*/
    
      }
    }
    catch(Exception e)
    {
      logEntry("insertData()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }

  public void resetDateProcessed(long pk)
  {
    boolean useCleanUp = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }

       
      /*@lineinfo:generated-code*//*@lineinfo:1108^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_discover_app_complete
//          set     DATE_PROCESSED          = null
//          where   APP_SEQ_NUM             = :pk
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_discover_app_complete\n        set     DATE_PROCESSED          = null\n        where   APP_SEQ_NUM             =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1113^7*/
    
    }
    catch(Exception e)
    {
      logEntry("updateRetransmissionStatus()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }


  private boolean completedRecordExists(long pk)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1145^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num 
//          from    merchant_discover_app_complete
//          where   app_seq_num = :pk
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num \n        from    merchant_discover_app_complete\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1150^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      result = false;
      logEntry("completedRecordExists()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }

  public void updateTsys(long pk)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1184^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_discover_app
//          set     tsys_updated            = :today          
//          where   app_seq_num             = :pk
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_discover_app\n        set     tsys_updated            =  :1           \n        where   app_seq_num             =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setLong(2,pk);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1189^7*/
    }
    catch(Exception e)
    {
      logEntry("updateTsys()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void updateMms(long pk)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1207^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_discover_app
//          set     mms_updated             = :today          
//          where   app_seq_num             = :pk
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_discover_app\n        set     mms_updated             =  :1           \n        where   app_seq_num             =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setLong(2,pk);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1212^7*/
    }
    catch(Exception e)
    {
      logEntry("updateTsys()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void forceToMms(long pk)
  {
    try
    {
      connect();
      //set forced to mms flag in database.. also change request type to maintenance so they will know they need to go back in 
      //and manually update mms..
      /*@lineinfo:generated-code*//*@lineinfo:1231^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_discover_app
//          set     forced_to_mms           = :today,
//                  request_type            = 'Maintenance'
//          where   APP_SEQ_NUM             = :pk
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_discover_app\n        set     forced_to_mms           =  :1 ,\n                request_type            = 'Maintenance'\n        where   APP_SEQ_NUM             =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setLong(2,pk);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1237^7*/
    }
    catch(Exception e)
    {
      logEntry("forceToMms()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }


  /*
  ** METHOD public void submitData()
  **
  */
  public void submitData()
  {
    try
    {

      if(recordExists())
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1264^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_discover_app
//            
//            set     DBA_NAME                = :businessDba,          
//                    APP_DATE                = :today,
//                    DBA_ADDRESS             = :businessAddress,      
//                    DBA_CITY                = :businessCity,         
//                    DBA_STATE               = :businessState,        
//                    DBA_ZIP                 = :businessZip,          
//                    DBA_PHONE               = :businessPhone,        
//                    DBA_URL                 = :url,                  
//                    DBA_EMAIL               = :email,
//                    PRINCIPAL_NAME          = :principalName,        
//                    PRINCIPAL_TITLE         = :principalTitle,       
//                    PRINCIPAL_SSN           = :principalSsn,         
//                    PRINCIPAL_ADDRESS       = :principalAddress,     
//                    PRINCIPAL_CITY          = :principalCity,        
//                    PRINCIPAL_STATE         = :principalState,       
//                    PRINCIPAL_ZIP           = :principalZip,         
//                    CORPORATE_NAME          = :corporateName,        
//                    CORPORATE_ADDRESS       = :corporateAddress,     
//                    CORPORATE_ADDRESS2      = :corporateAddressLine2,
//                    CORPORATE_CITY          = :corporateCity,        
//                    CORPORATE_STATE         = :corporateState,       
//                    CORPORATE_ZIP           = :corporateZip,         
//                    YEARS_IN_BUSINESS       = :yearsInBusiness,      
//                    ACQUIRER_MERCHANT_NUM   = :merchantNumber,       
//                    FEDERAL_TAX_ID          = :taxId,
//                    NON_PROFIT_FLAG         = :nonProfit,
//                    BUSINESS_TYPE           = :busType,
//                    TRANSIT_ROUTING_NUM     = :transitRoute,
//                    ACCOUNT_DDA             = :checkingAccountNum,
//                    DISCOVER_DISC_RATE      = :discountRate,
//                    DISCOVER_PER_ITEM_RATE  = :discountPerItem,
//                    MEMBERSHIP_FEE          = :membershipFee,
//                    MERCHANT_CAT_CODE       = :mcc,
//                    MOTO_FLAG               = :moto,
//                    AVERAGE_SALE            = :averageSaleAmount,
//                    ANNUAL_SALES            = :annualSalesVolume,
//                    SALES_REP_ID            = :salesRepId,
//                    FRANCHISE_CODE          = :franchiseCode
//  
//            where   APP_SEQ_NUM             = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_discover_app\n          \n          set     DBA_NAME                =  :1 ,          \n                  APP_DATE                =  :2 ,\n                  DBA_ADDRESS             =  :3 ,      \n                  DBA_CITY                =  :4 ,         \n                  DBA_STATE               =  :5 ,        \n                  DBA_ZIP                 =  :6 ,          \n                  DBA_PHONE               =  :7 ,        \n                  DBA_URL                 =  :8 ,                  \n                  DBA_EMAIL               =  :9 ,\n                  PRINCIPAL_NAME          =  :10 ,        \n                  PRINCIPAL_TITLE         =  :11 ,       \n                  PRINCIPAL_SSN           =  :12 ,         \n                  PRINCIPAL_ADDRESS       =  :13 ,     \n                  PRINCIPAL_CITY          =  :14 ,        \n                  PRINCIPAL_STATE         =  :15 ,       \n                  PRINCIPAL_ZIP           =  :16 ,         \n                  CORPORATE_NAME          =  :17 ,        \n                  CORPORATE_ADDRESS       =  :18 ,     \n                  CORPORATE_ADDRESS2      =  :19 ,\n                  CORPORATE_CITY          =  :20 ,        \n                  CORPORATE_STATE         =  :21 ,       \n                  CORPORATE_ZIP           =  :22 ,         \n                  YEARS_IN_BUSINESS       =  :23 ,      \n                  ACQUIRER_MERCHANT_NUM   =  :24 ,       \n                  FEDERAL_TAX_ID          =  :25 ,\n                  NON_PROFIT_FLAG         =  :26 ,\n                  BUSINESS_TYPE           =  :27 ,\n                  TRANSIT_ROUTING_NUM     =  :28 ,\n                  ACCOUNT_DDA             =  :29 ,\n                  DISCOVER_DISC_RATE      =  :30 ,\n                  DISCOVER_PER_ITEM_RATE  =  :31 ,\n                  MEMBERSHIP_FEE          =  :32 ,\n                  MERCHANT_CAT_CODE       =  :33 ,\n                  MOTO_FLAG               =  :34 ,\n                  AVERAGE_SALE            =  :35 ,\n                  ANNUAL_SALES            =  :36 ,\n                  SALES_REP_ID            =  :37 ,\n                  FRANCHISE_CODE          =  :38 \n\n          where   APP_SEQ_NUM             =  :39";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,businessDba);
   __sJT_st.setTimestamp(2,today);
   __sJT_st.setString(3,businessAddress);
   __sJT_st.setString(4,businessCity);
   __sJT_st.setString(5,businessState);
   __sJT_st.setString(6,businessZip);
   __sJT_st.setString(7,businessPhone);
   __sJT_st.setString(8,url);
   __sJT_st.setString(9,email);
   __sJT_st.setString(10,principalName);
   __sJT_st.setString(11,principalTitle);
   __sJT_st.setString(12,principalSsn);
   __sJT_st.setString(13,principalAddress);
   __sJT_st.setString(14,principalCity);
   __sJT_st.setString(15,principalState);
   __sJT_st.setString(16,principalZip);
   __sJT_st.setString(17,corporateName);
   __sJT_st.setString(18,corporateAddress);
   __sJT_st.setString(19,corporateAddressLine2);
   __sJT_st.setString(20,corporateCity);
   __sJT_st.setString(21,corporateState);
   __sJT_st.setString(22,corporateZip);
   __sJT_st.setString(23,yearsInBusiness);
   __sJT_st.setString(24,merchantNumber);
   __sJT_st.setString(25,taxId);
   __sJT_st.setString(26,nonProfit);
   __sJT_st.setString(27,busType);
   __sJT_st.setString(28,transitRoute);
   __sJT_st.setString(29,checkingAccountNum);
   __sJT_st.setString(30,discountRate);
   __sJT_st.setString(31,discountPerItem);
   __sJT_st.setString(32,membershipFee);
   __sJT_st.setString(33,mcc);
   __sJT_st.setString(34,moto);
   __sJT_st.setString(35,averageSaleAmount);
   __sJT_st.setString(36,annualSalesVolume);
   __sJT_st.setString(37,salesRepId);
   __sJT_st.setString(38,franchiseCode);
   __sJT_st.setLong(39,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1308^9*/

      }
      // update existing records
      else
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1316^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_discover_app
//            ( 
//              APP_SEQ_NUM,
//              APP_DATE,
//              PARTNER_NUM,
//              DBA_NAME,
//              DBA_ADDRESS,
//              DBA_CITY,
//              DBA_STATE,
//              DBA_ZIP,
//              DBA_PHONE,
//              DBA_URL,
//              DBA_EMAIL,
//              PRINCIPAL_NAME,
//              PRINCIPAL_TITLE,
//              PRINCIPAL_SSN,
//              PRINCIPAL_ADDRESS,
//              PRINCIPAL_CITY,
//              PRINCIPAL_STATE,
//              PRINCIPAL_ZIP,
//              CORPORATE_NAME,
//              CORPORATE_ADDRESS,
//              CORPORATE_ADDRESS2,
//              CORPORATE_CITY,
//              CORPORATE_STATE,
//              CORPORATE_ZIP,
//              YEARS_IN_BUSINESS,
//              ACQUIRER_MERCHANT_NUM,
//              FEDERAL_TAX_ID,
//              NON_PROFIT_FLAG,
//              BUSINESS_TYPE,
//              TRANSIT_ROUTING_NUM,
//              ACCOUNT_DDA,
//              DISCOVER_DISC_RATE,
//              DISCOVER_PER_ITEM_RATE,
//              MEMBERSHIP_FEE,
//              MERCHANT_CAT_CODE,
//              MOTO_FLAG,
//              AVERAGE_SALE,
//              ANNUAL_SALES,
//              TRANSMISSION_METHOD,
//              SALES_REP_ID,
//              intermediate_level2,
//              request_type,
//              submitted_by,
//              FRANCHISE_CODE
//            )
//            
//            values
//            ( 
//              :primaryKey,
//              :today,
//              :getPartnerNum(),
//              :businessDba,
//              :businessAddress,
//              :businessCity,
//              :businessState,
//              :businessZip,
//              :businessPhone,
//              :url,
//              :email,
//              :principalName,
//              :principalTitle,
//              :principalSsn,
//              :principalAddress,
//              :principalCity,
//              :principalState,
//              :principalZip,
//              :corporateName,
//              :corporateAddress,
//              :corporateAddressLine2,
//              :corporateCity,
//              :corporateState,
//              :corporateZip,
//              :yearsInBusiness,
//              :merchantNumber,
//              :taxId,
//              :nonProfit,
//              :busType,
//              :transitRoute,
//              :checkingAccountNum,
//              :discountRate,
//              :discountPerItem,
//              :membershipFee,
//              :mcc,
//              :moto,
//              :averageSaleAmount,
//              :annualSalesVolume,
//              :transmissionMethod,
//              :salesRepId,
//              :intermediateLevel2,
//              'Maintenance',
//              :user.getUserId(),
//              :franchiseCode
//            )
//  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_752 = getPartnerNum();
 long __sJT_753 = user.getUserId();
   String theSqlTS = "insert into merchant_discover_app\n          ( \n            APP_SEQ_NUM,\n            APP_DATE,\n            PARTNER_NUM,\n            DBA_NAME,\n            DBA_ADDRESS,\n            DBA_CITY,\n            DBA_STATE,\n            DBA_ZIP,\n            DBA_PHONE,\n            DBA_URL,\n            DBA_EMAIL,\n            PRINCIPAL_NAME,\n            PRINCIPAL_TITLE,\n            PRINCIPAL_SSN,\n            PRINCIPAL_ADDRESS,\n            PRINCIPAL_CITY,\n            PRINCIPAL_STATE,\n            PRINCIPAL_ZIP,\n            CORPORATE_NAME,\n            CORPORATE_ADDRESS,\n            CORPORATE_ADDRESS2,\n            CORPORATE_CITY,\n            CORPORATE_STATE,\n            CORPORATE_ZIP,\n            YEARS_IN_BUSINESS,\n            ACQUIRER_MERCHANT_NUM,\n            FEDERAL_TAX_ID,\n            NON_PROFIT_FLAG,\n            BUSINESS_TYPE,\n            TRANSIT_ROUTING_NUM,\n            ACCOUNT_DDA,\n            DISCOVER_DISC_RATE,\n            DISCOVER_PER_ITEM_RATE,\n            MEMBERSHIP_FEE,\n            MERCHANT_CAT_CODE,\n            MOTO_FLAG,\n            AVERAGE_SALE,\n            ANNUAL_SALES,\n            TRANSMISSION_METHOD,\n            SALES_REP_ID,\n            intermediate_level2,\n            request_type,\n            submitted_by,\n            FRANCHISE_CODE\n          )\n          \n          values\n          ( \n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 ,\n             :34 ,\n             :35 ,\n             :36 ,\n             :37 ,\n             :38 ,\n             :39 ,\n             :40 ,\n             :41 ,\n            'Maintenance',\n             :42 ,\n             :43 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setTimestamp(2,today);
   __sJT_st.setString(3,__sJT_752);
   __sJT_st.setString(4,businessDba);
   __sJT_st.setString(5,businessAddress);
   __sJT_st.setString(6,businessCity);
   __sJT_st.setString(7,businessState);
   __sJT_st.setString(8,businessZip);
   __sJT_st.setString(9,businessPhone);
   __sJT_st.setString(10,url);
   __sJT_st.setString(11,email);
   __sJT_st.setString(12,principalName);
   __sJT_st.setString(13,principalTitle);
   __sJT_st.setString(14,principalSsn);
   __sJT_st.setString(15,principalAddress);
   __sJT_st.setString(16,principalCity);
   __sJT_st.setString(17,principalState);
   __sJT_st.setString(18,principalZip);
   __sJT_st.setString(19,corporateName);
   __sJT_st.setString(20,corporateAddress);
   __sJT_st.setString(21,corporateAddressLine2);
   __sJT_st.setString(22,corporateCity);
   __sJT_st.setString(23,corporateState);
   __sJT_st.setString(24,corporateZip);
   __sJT_st.setString(25,yearsInBusiness);
   __sJT_st.setString(26,merchantNumber);
   __sJT_st.setString(27,taxId);
   __sJT_st.setString(28,nonProfit);
   __sJT_st.setString(29,busType);
   __sJT_st.setString(30,transitRoute);
   __sJT_st.setString(31,checkingAccountNum);
   __sJT_st.setString(32,discountRate);
   __sJT_st.setString(33,discountPerItem);
   __sJT_st.setString(34,membershipFee);
   __sJT_st.setString(35,mcc);
   __sJT_st.setString(36,moto);
   __sJT_st.setString(37,averageSaleAmount);
   __sJT_st.setString(38,annualSalesVolume);
   __sJT_st.setString(39,transmissionMethod);
   __sJT_st.setString(40,salesRepId);
   __sJT_st.setString(41,intermediateLevel2);
   __sJT_st.setLong(42,__sJT_753);
   __sJT_st.setString(43,franchiseCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1414^9*/
    
      }
      insertData(primaryKey);
      updateTransmissionStatus(primaryKey);
    }
    catch(Exception e)
    {
      logEntry("setData()", e.toString());
      addError("Error: Complications occurred while storing data!  Please resubmit.");
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** METHOD public void resubmitData()
  **
  */
  public void reSubmitData()
  {
    try
    {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1442^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_discover_app
//          
//          set     DBA_NAME                = :businessDba,          
//                  DBA_ADDRESS             = :businessAddress,      
//                  DBA_CITY                = :businessCity,         
//                  DBA_STATE               = :businessState,        
//                  DBA_ZIP                 = :businessZip,          
//                  DBA_PHONE               = :businessPhone,        
//                  DBA_URL                 = :url,                  
//                  DBA_EMAIL               = :email,
//                  PRINCIPAL_NAME          = :principalName,        
//                  PRINCIPAL_TITLE         = :principalTitle,       
//                  PRINCIPAL_SSN           = :principalSsn,         
//                  PRINCIPAL_ADDRESS       = :principalAddress,     
//                  PRINCIPAL_CITY          = :principalCity,        
//                  PRINCIPAL_STATE         = :principalState,       
//                  PRINCIPAL_ZIP           = :principalZip,         
//                  CORPORATE_NAME          = :corporateName,        
//                  CORPORATE_ADDRESS       = :corporateAddress,     
//                  CORPORATE_ADDRESS2      = :corporateAddressLine2,
//                  CORPORATE_CITY          = :corporateCity,        
//                  CORPORATE_STATE         = :corporateState,       
//                  CORPORATE_ZIP           = :corporateZip,         
//                  YEARS_IN_BUSINESS       = :yearsInBusiness,      
//                  ACQUIRER_MERCHANT_NUM   = :merchantNumber,       
//                  FEDERAL_TAX_ID          = :taxId,
//                  NON_PROFIT_FLAG         = :nonProfit,
//                  BUSINESS_TYPE           = :busType,
//                  TRANSIT_ROUTING_NUM     = :transitRoute,
//                  ACCOUNT_DDA             = :checkingAccountNum,
//                  DISCOVER_DISC_RATE      = :discountRate,
//                  DISCOVER_PER_ITEM_RATE  = :discountPerItem,
//                  MEMBERSHIP_FEE          = :membershipFee,
//                  MERCHANT_CAT_CODE       = :mcc,
//                  MOTO_FLAG               = :moto,
//                  AVERAGE_SALE            = :averageSaleAmount,
//                  ANNUAL_SALES            = :annualSalesVolume,
//                  SALES_REP_ID            = :salesRepId,
//                  FRANCHISE_CODE          = :franchiseCode
//  
//          where   APP_SEQ_NUM             = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_discover_app\n        \n        set     DBA_NAME                =  :1 ,          \n                DBA_ADDRESS             =  :2 ,      \n                DBA_CITY                =  :3 ,         \n                DBA_STATE               =  :4 ,        \n                DBA_ZIP                 =  :5 ,          \n                DBA_PHONE               =  :6 ,        \n                DBA_URL                 =  :7 ,                  \n                DBA_EMAIL               =  :8 ,\n                PRINCIPAL_NAME          =  :9 ,        \n                PRINCIPAL_TITLE         =  :10 ,       \n                PRINCIPAL_SSN           =  :11 ,         \n                PRINCIPAL_ADDRESS       =  :12 ,     \n                PRINCIPAL_CITY          =  :13 ,        \n                PRINCIPAL_STATE         =  :14 ,       \n                PRINCIPAL_ZIP           =  :15 ,         \n                CORPORATE_NAME          =  :16 ,        \n                CORPORATE_ADDRESS       =  :17 ,     \n                CORPORATE_ADDRESS2      =  :18 ,\n                CORPORATE_CITY          =  :19 ,        \n                CORPORATE_STATE         =  :20 ,       \n                CORPORATE_ZIP           =  :21 ,         \n                YEARS_IN_BUSINESS       =  :22 ,      \n                ACQUIRER_MERCHANT_NUM   =  :23 ,       \n                FEDERAL_TAX_ID          =  :24 ,\n                NON_PROFIT_FLAG         =  :25 ,\n                BUSINESS_TYPE           =  :26 ,\n                TRANSIT_ROUTING_NUM     =  :27 ,\n                ACCOUNT_DDA             =  :28 ,\n                DISCOVER_DISC_RATE      =  :29 ,\n                DISCOVER_PER_ITEM_RATE  =  :30 ,\n                MEMBERSHIP_FEE          =  :31 ,\n                MERCHANT_CAT_CODE       =  :32 ,\n                MOTO_FLAG               =  :33 ,\n                AVERAGE_SALE            =  :34 ,\n                ANNUAL_SALES            =  :35 ,\n                SALES_REP_ID            =  :36 ,\n                FRANCHISE_CODE          =  :37 \n\n        where   APP_SEQ_NUM             =  :38";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,businessDba);
   __sJT_st.setString(2,businessAddress);
   __sJT_st.setString(3,businessCity);
   __sJT_st.setString(4,businessState);
   __sJT_st.setString(5,businessZip);
   __sJT_st.setString(6,businessPhone);
   __sJT_st.setString(7,url);
   __sJT_st.setString(8,email);
   __sJT_st.setString(9,principalName);
   __sJT_st.setString(10,principalTitle);
   __sJT_st.setString(11,principalSsn);
   __sJT_st.setString(12,principalAddress);
   __sJT_st.setString(13,principalCity);
   __sJT_st.setString(14,principalState);
   __sJT_st.setString(15,principalZip);
   __sJT_st.setString(16,corporateName);
   __sJT_st.setString(17,corporateAddress);
   __sJT_st.setString(18,corporateAddressLine2);
   __sJT_st.setString(19,corporateCity);
   __sJT_st.setString(20,corporateState);
   __sJT_st.setString(21,corporateZip);
   __sJT_st.setString(22,yearsInBusiness);
   __sJT_st.setString(23,merchantNumber);
   __sJT_st.setString(24,taxId);
   __sJT_st.setString(25,nonProfit);
   __sJT_st.setString(26,busType);
   __sJT_st.setString(27,transitRoute);
   __sJT_st.setString(28,checkingAccountNum);
   __sJT_st.setString(29,discountRate);
   __sJT_st.setString(30,discountPerItem);
   __sJT_st.setString(31,membershipFee);
   __sJT_st.setString(32,mcc);
   __sJT_st.setString(33,moto);
   __sJT_st.setString(34,averageSaleAmount);
   __sJT_st.setString(35,annualSalesVolume);
   __sJT_st.setString(36,salesRepId);
   __sJT_st.setString(37,franchiseCode);
   __sJT_st.setLong(38,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1485^7*/

      resetDateProcessed(primaryKey);
      updateRetransmissionStatus(primaryKey);
    }
    catch(Exception e)
    {
      logEntry("resubmitData()", e.toString());
      addError("Error: Complications occurred while resubmitting data!  Please resubmit.");
    }
    finally
    {
      cleanUp();
    }
  }

  private void updateRetransmissionStatus(long pk)
  {
    boolean useCleanUp = false;
    
    try
    {
      int status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_RETRANSMISSION;

      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
     
      /*@lineinfo:generated-code*//*@lineinfo:1515^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     status_code   = :status
//          where   app_seq_num   = :pk and
//                  dept_code     = :QueueConstants.DEPARTMENT_DISCOVER_RAP
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     status_code   =  :1 \n        where   app_seq_num   =  :2  and\n                dept_code     =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,status);
   __sJT_st.setLong(2,pk);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_DISCOVER_RAP);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1521^7*/
    }
    catch(Exception e)
    {
      logEntry("updateRetransmissionStatus()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }










  private void updateTransmissionStatus(long pk)
  {
    boolean useCleanUp = false;
    
    try
    {
      int status = 0;

      if(wasSentViaTransmission())
      {
        status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_TRANSMISSION;
      }
      else
      {
        status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_FAX_TO_DISCOVER;
      }      

      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
     

      if(!trackRecordExists(pk, QueueConstants.DEPARTMENT_DISCOVER_RAP))
      {
        //connect();
        /*@lineinfo:generated-code*//*@lineinfo:1572^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_tracking
//            (
//              app_seq_num,
//              dept_code,
//              status_code
//            )
//            values
//            (
//              :pk,
//              :QueueConstants.DEPARTMENT_DISCOVER_RAP,
//              :status
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_tracking\n          (\n            app_seq_num,\n            dept_code,\n            status_code\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_DISCOVER_RAP);
   __sJT_st.setInt(3,status);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1586^9*/
      }
      else  
      {
        //connect();
        /*@lineinfo:generated-code*//*@lineinfo:1591^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//            set     status_code   = :status
//            where   app_seq_num   = :pk and
//                    dept_code     = :QueueConstants.DEPARTMENT_DISCOVER_RAP
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n          set     status_code   =  :1 \n          where   app_seq_num   =  :2  and\n                  dept_code     =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,status);
   __sJT_st.setLong(2,pk);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_DISCOVER_RAP);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1597^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("updateTransmissionStatus()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }

  private void updateResponseStatus(long pk)
  {
    
    boolean useCleanUp = false;

    try
    {
      int status = 0;

      if(manDecision.equals(DECISION_STATUS_APPROVED))
      {
        status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_LIVE_MANUAL;
      }
      else if(manDecision.equals(DECISION_STATUS_PENDING))
      {
        status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_PENDING;
      }
      else if(manDecision.equals(DECISION_STATUS_DECLINED))
      {
        status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_DECLINED;
      }      

      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      if(!trackRecordExists(pk, QueueConstants.DEPARTMENT_DISCOVER_RAP))
      {
        //connect();
        /*@lineinfo:generated-code*//*@lineinfo:1644^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_tracking
//            (
//              app_seq_num,
//              dept_code,
//              status_code
//            )
//            values
//            (
//              :pk,
//              :QueueConstants.DEPARTMENT_DISCOVER_RAP,
//              :status
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_tracking\n          (\n            app_seq_num,\n            dept_code,\n            status_code\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_DISCOVER_RAP);
   __sJT_st.setInt(3,status);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1658^9*/
      }
      else  
      {
        //connect();
        /*@lineinfo:generated-code*//*@lineinfo:1663^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//            set     status_code   = :status
//            where   app_seq_num   = :pk and
//                    dept_code     = :QueueConstants.DEPARTMENT_DISCOVER_RAP
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n          set     status_code   =  :1 \n          where   app_seq_num   =  :2  and\n                  dept_code     =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,status);
   __sJT_st.setLong(2,pk);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_DISCOVER_RAP);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1669^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("updateResponseStatus()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }


  public void submitManData()
  {

    try
    {
     
      if(responseRecordExists())
      {
        connect(); 

        /*@lineinfo:generated-code*//*@lineinfo:1696^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_discover_app_response
//          
//            set     DISCOVER_MERCHANT_NUM   = :manDiscNum,          
//                    DECISION_STATUS         = :manDecision,      
//                    DISPOSITION_REASON_CODE = :manDisposition,         
//                    RECEIVED_DATE           = :today,        
//                    DECISION_DATE           = :today,
//                    DATE_LAST_UPDATED       = :today,
//                    ERROR_FLAGS             = null
//  
//            where   APP_SEQ_NUM             = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_discover_app_response\n        \n          set     DISCOVER_MERCHANT_NUM   =  :1 ,          \n                  DECISION_STATUS         =  :2 ,      \n                  DISPOSITION_REASON_CODE =  :3 ,         \n                  RECEIVED_DATE           =  :4 ,        \n                  DECISION_DATE           =  :5 ,\n                  DATE_LAST_UPDATED       =  :6 ,\n                  ERROR_FLAGS             = null\n\n          where   APP_SEQ_NUM             =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,manDiscNum);
   __sJT_st.setString(2,manDecision);
   __sJT_st.setString(3,manDisposition);
   __sJT_st.setTimestamp(4,today);
   __sJT_st.setTimestamp(5,today);
   __sJT_st.setTimestamp(6,today);
   __sJT_st.setLong(7,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1709^9*/
      }
      else
      {
        connect(); 

        /*@lineinfo:generated-code*//*@lineinfo:1715^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_discover_app_response
//            ( 
//              DISCOVER_MERCHANT_NUM,
//              DECISION_STATUS,
//              DISPOSITION_REASON_CODE,
//              RECEIVED_DATE,
//              DECISION_DATE,
//              DATE_LAST_UPDATED,
//              ERROR_FLAGS,
//              APP_SEQ_NUM,
//              ACQUIRER_MERCHANT_NUM
//            )
//            values
//            ( 
//              :manDiscNum,
//              :manDecision,
//              :manDisposition,
//              :today,
//              :today,
//              :today,
//              null,
//              :primaryKey,
//              :merchantNumber
//            )
//  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant_discover_app_response\n          ( \n            DISCOVER_MERCHANT_NUM,\n            DECISION_STATUS,\n            DISPOSITION_REASON_CODE,\n            RECEIVED_DATE,\n            DECISION_DATE,\n            DATE_LAST_UPDATED,\n            ERROR_FLAGS,\n            APP_SEQ_NUM,\n            ACQUIRER_MERCHANT_NUM\n          )\n          values\n          ( \n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n            null,\n             :7 ,\n             :8 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,manDiscNum);
   __sJT_st.setString(2,manDecision);
   __sJT_st.setString(3,manDisposition);
   __sJT_st.setTimestamp(4,today);
   __sJT_st.setTimestamp(5,today);
   __sJT_st.setTimestamp(6,today);
   __sJT_st.setLong(7,primaryKey);
   __sJT_st.setString(8,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1742^9*/
      }
      updateResponseStatus(primaryKey);
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitManData: " + e.toString());
    } 
    finally
    {
      cleanUp();
    }
  }

  //gets partner num if apptype has their own partner num in the merchant_discover_app_partners table or else uses mes partner num (default)
  private String getPartnerNum()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    String              result      = DEFAULT_PARTNER_NUMBER;
    boolean             useCleanUp  = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1771^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mdap.partner_num 
//          from    MERCHANT_DISCOVER_APP_PARTERS mdap, application app
//          where   app.app_seq_num = :primaryKey and app.app_type = mdap.apptype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mdap.partner_num \n        from    MERCHANT_DISCOVER_APP_PARTERS mdap, application app\n        where   app.app_seq_num =  :1  and app.app_type = mdap.apptype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"30com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1776^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = isBlank(rs.getString("partner_num")) ? DEFAULT_PARTNER_NUMBER : rs.getString("partner_num");
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      result = DEFAULT_PARTNER_NUMBER;
      logEntry("getPartnerNum()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }

  private boolean trackRecordExists(long pk, int dept)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1818^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num 
//          from    app_tracking
//          where   app_seq_num   = :pk and
//                  dept_code     = :dept
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num \n        from    app_tracking\n        where   app_seq_num   =  :1  and\n                dept_code     =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   __sJT_st.setInt(2,dept);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1824^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      result = false;
      logEntry("trackRecordExists()", e.toString());
    }
    finally
    { 
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }

  private boolean recordExists()
  {
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      int appCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:1867^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant_discover_app
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant_discover_app\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.ops.DiscoverRapBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1873^7*/
      
      result = appCount > 0;
    }
    catch(Exception e)
    {
      result = false;
      logEntry("recordExists()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }

  private boolean responseRecordExists()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1908^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num 
//          from    merchant_discover_app_response
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num \n        from    merchant_discover_app_response\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"33com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1913^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      result = false;
      logEntry("recordExists()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }


  private boolean doesExists(String merchNum)
  {
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      int appCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:1957^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant_discover_app
//          where   app_seq_num = :this.primaryKey and
//                  acquirer_merchant_num = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant_discover_app\n        where   app_seq_num =  :1  and\n                acquirer_merchant_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.ops.DiscoverRapBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,this.primaryKey);
   __sJT_st.setString(2,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1964^7*/
      
      result = appCount > 0;
    }
    catch(Exception e)
    {
      result = false;
      logEntry("doesExists()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }

  /*
  **Manual Update VALIDATION
  */  

  public boolean manValidate()
  {

    if(isBlank(manDecision))
    {
      addError("Please select a Decision Status");
    }

    if(isBlank(manDisposition))
    {
      addError("Please select a Disposition Reason");
    }
    else if(manDisposition.equals("PM") && !manDecision.equals(DECISION_STATUS_PENDING))
    {
      addError("If selecting Pending Manual Decision for a disposition reason you must select Pending as decision status");
    }
    else if(manDisposition.equals("PD") && !manDecision.equals(DECISION_STATUS_PENDING))
    {
      addError("If selecting Pending Duplicate Check for a disposition reason you must select Pending as decision status");
    }
    else if(manDisposition.equals("IE") && !manDecision.equals(DECISION_STATUS_PENDING))
    {
      addError("If selecting Pending Error for a disposition reason you must select Pending as decision status");
    }

    if(isBlank(manDiscNum) && !manDisposition.equals("PD") && !manDisposition.equals("IE"))
    {
      addError("Please provide the Discover Merchant #");
    }

    return (!hasErrors());
  }

  /*
  **VALIDATION
  */  
  public boolean validate()
  {
     if(isBlank(businessDba))
     {
       addError("Please provide a Business Dba Name");
     }
     else if(businessDba.length() > FIELD_LENGTH_DBANAME)
     {
       addError("Business Name (DBA) is too long.  Truncate to 30 characters.");
     }

     if(isBlank(businessAddress))
     {
       addError("Please provide a Business Street Address");
     }
     else if(businessAddress.length() > FIELD_LENGTH_DBAADD)
     {
       addError("Business Street Address is too long.  Truncate to 30 characters.");
     }

     if(isBlank(businessCity))
     {
       addError("Please provide the City of your business address");
     }
     else if(businessCity.length() > FIELD_LENGTH_DBACITY)
     {
       addError("Business City is too long.  Truncate to 13 characters.");
     }
     
     if(isBlank(businessState))
     {
       addError("Please select the State of your business address");
     }
     
     if(isBlank(businessZip))
     {
       addError("Please provide the 5-digit zip code of your business address");
     }
     else if (countDigits(businessZip) != 5)
     {
       addError("Please provide a valid 5-digit business zip code");
     }
     else if(!isBlank(businessState)) //if zip and state present validate zipcode for state
     {
       businessZip = getDigitsOnly(businessZip);
       if(!validZipForState(businessState,businessZip))
       {
         addError("Business Zip Code " + businessZip + " is not a valid zip for Business State " + businessState);
       }
     }
     
     if(isBlank(businessPhone))
     {
       addError("Please provide your Business Phone Number");
     }
     else if (countDigits(businessPhone) != 10)
     {
       addError("Business Phone number must be a valid 10-digit number including area code");
     }
     else //we get digits only, we reformat when we display it
     {
       businessPhone = getDigitsOnly(businessPhone);
     }

     if(!isBlank(email) && !isEmail(email))
     {
       addError("Please provide a valid email address");
     }
     else if(!isBlank(email) && email.length() > FIELD_LENGTH_EMAIL)
     {
       addError("Email Address is too long.  Truncate to 30 characters.");
     }

     if(isBlank(busType))
     {
       addError("Please select the Type of Business");
     }
     //else if(busType.equals("C"))
     //{     
       if(isBlank(corporateName) && busType.equals("C"))
       {
         addError("Please provide the Name of your Corporation");
       }
       else if(!isBlank(corporateName) && corporateName.length() > FIELD_LENGTH_CORPNAME)
       {
         addError("Corporate Name is too long.  Truncate to 30 characters.");
       }

       if(isBlank(corporateAddress) && busType.equals("C"))
       {
         addError("Please provide the Address of your Corporation");
       }
       else if(!isBlank(corporateAddress) && corporateAddress.length() > FIELD_LENGTH_CORPADD)
       {
         addError("Corporate Address is too long.  Truncate to 30 characters.");
       }
       
       if(!isBlank(corporateAddressLine2) && corporateAddressLine2.length() > FIELD_LENGTH_CORPADD2)
       {
         addError("Corporate Address Line 2 is too long.  Truncate to 30 characters.");
       }

       if(isBlank(corporateCity) && busType.equals("C"))
       {
         addError("Please provide the City of your Corporation");
       }
       else if(!isBlank(corporateCity) && corporateCity.length() > FIELD_LENGTH_CORPCITY)
       {
         addError("Corporate City is too long.  Truncate to 13 characters.");
       }

       if(isBlank(corporateState) && busType.equals("C"))
       {
         addError("Please provide the State of your Corporation");
       }
       
       if(isBlank(corporateZip) && busType.equals("C"))
       {
         addError("Please provide the 5-digit Zip Code of your Corporation");
       }
       else if (!isBlank(corporateZip) && countDigits(corporateZip) != 5)
       {
         addError("Please provide a valid 5-digit Zip Code for your Corporation");
       }
       else if(!isBlank(corporateState) && !isBlank(corporateZip)) //if zip and state present validate zipcode for state
       {
         corporateZip = getDigitsOnly(corporateZip);
         if(!validZipForState(corporateState,corporateZip))
         {
           addError("Corporate Zip Code " + corporateZip + " is not a valid zip for Corporate State " + corporateState);
         }
       }
     //}

     if(isBlank(yearsInBusiness))
     {
       addError("Please select the number of years you have been in business");
     }

     if(isBlank(nonProfit))
     {
       addError("Please specify whether your business is a non-profit organization");
     }

     if(isBlank(taxId) && busType.equals("C"))
     {
       addError("If business type is Corporation, please provide a Federal Tax Id Number");
     }
     else if(!isBlank(taxId))
     {

       taxId = getDigitsOnly(taxId);

       if (taxId.length() != 9)
       {
         addError("Please provide a valid 9-digit Federal Tax Id Number");
       }
       else
       {

         String taxIdPrefix = taxId.substring(0,2);
         
         if(taxIdPrefix.equals("07") ||
            taxIdPrefix.equals("08") ||
            taxIdPrefix.equals("09") ||
            taxIdPrefix.equals("17") ||
            taxIdPrefix.equals("18") ||
            taxIdPrefix.equals("19") ||
            taxIdPrefix.equals("28") ||
            taxIdPrefix.equals("29") ||
            taxIdPrefix.equals("49") ||
            taxIdPrefix.equals("78") ||
            taxIdPrefix.equals("00") ||
            taxIdPrefix.equals("79")
           )
         {
           addError("Federal Tax Id can not begin with 00, 07, 08, 09, 17, 18, 19, 28, 29, 49, 78, or 79");
         }
         else if(repeatingOne(taxId) || repeatingTwo(taxId) || repeatingThree(taxId))
         {
           addError("Federal Tax Id can not have repeating numbers in groups of one, two and three. (ie. 111-11-1111, 121-21-2121, 123-12-3123, etc...");
         }
       }
     }

     if(isBlank(checkingAccountNum))
     {
       addError("Please provide your Checking Account Number");
     }
     else if(checkingAccountNum.length() > FIELD_LENGTH_BANKACCT)
     {
       addError("Checking Account Number is too long.  Truncate to 17 characters.");
     }

     if(isBlank(transitRoute))
     {
       addError("Please provide your ABA Transit Routing Number");
     }
     else if(countDigits(transitRoute) != 9)
     {
       addError("Please provide a valid 9-digit Transit Routing Number");
     }
     else //no fomatting 
     {
       transitRoute = getDigitsOnly(transitRoute);
     }

     if(!isBlank(transitRoute))
     {
       if(!validTransitRoute(transitRoute))
       {
         addError("Transit Routing Number '" + transitRoute + "' is not valid");
       }
     }
     
     if(isBlank(merchantNumber))
     {
       addError("Please provide your Acquirer Merchant Number");
     }
     else  
     {
       merchantNumber = getDigitsOnly(merchantNumber);
       if(doesExists(merchantNumber) && !isReSubmitted())
       {
         addError("This merchant has already applied for a Discover Merchant Account");
       }
     }
     
     if(isBlank(principalName))
     {
       addError("Please provide the Principal's Name");
     }
     else if(principalName.length() > FIELD_LENGTH_PRINNAME)
     {
       addError("Principal Name is too long.  Truncate to 45 characters.");
     }

     if(isBlank(principalTitle))
     {
       addError("Please provide the Principal's Title");
     }
     else if(principalTitle.length() > FIELD_LENGTH_PRINTITLE)
     {
       addError("Principal Title is too long.  Truncate to 15 characters.");
     }


     //if business type is not corporation we need this ssn#
     if(isBlank(principalSsn) && !busType.equals("C"))
     {
       addError("Please provide the Principal's Social Security Number");
     }
     else if(!isBlank(principalSsn))
     {
       principalSsn = getDigitsOnly(principalSsn);
       
       if (principalSsn.length() != 9)
       {
         addError("Please provide a valid 9-digit Social Security Number");
       }
       else
       {
       
         try
         {
           String ssnAreaNumber = principalSsn.substring(0,3);
  
           int ssnAreaNumberInt = Integer.parseInt(ssnAreaNumber);

           if(ssnAreaNumberInt == 0 || ssnAreaNumberInt == 666 || (ssnAreaNumberInt >= 730 && ssnAreaNumberInt <= 759) || (ssnAreaNumberInt >= 780 && ssnAreaNumberInt <= 799) )
           {
             addError("Social Security Number first 3 numbers can not begin with 000 or 666 and can not be between 730 and 759 or between 780 and 799 ");
           }
           else if(ssnAreaNumberInt >= 800)
           {
             addError("Social Security Number with first 3 numbers 800 and above have not been assigned yet ");
           }
         }
         catch(Exception e)
         {
           addError("Please provide a valid 9-digit Social Security Number");
         }

         if(principalSsn.equals("123456789"))
         {
           addError("Social Security Number can not equal 123-45-6789");
         }
         else if(repeatingOne(principalSsn) || repeatingTwo(principalSsn) || repeatingThree(principalSsn))
         {
           addError("Social Security Number can not have repeating numbers in groups of one, two and three. (ie. 111-11-1111, 121-21-2121, 123-12-3123, etc...");
         }
         else if( groupsOfFiveOrMore(principalSsn) )
         {
           addError("Social Security Number can not have five or more of the same consecutive numbers anywhere. (ie. 111-11-2222, 121-11-1111, 133-33-3333, etc...");
         }

       }

     }

     if(isBlank(principalAddress))
     {
       addError("Please provide the Principal's Address");
     }
     else if(principalAddress.length() > FIELD_LENGTH_PRINADD)
     {
       addError("Principal Address is too long.  Truncate to 30 characters.");
     }
     
     if(isBlank(principalCity))
     {
       addError("Please provide the Principal's City");
     }
     else if(principalCity.length() > FIELD_LENGTH_PRINCITY)
     {
       addError("Principal City is too long.  Truncate to 13 characters.");
     }

     if(isBlank(principalState))
     {
       addError("Please provide the Principal's State");
     }
     
     if(isBlank(principalZip))
     {
       addError("Please provide the Principal's 5-digit Zip Code");
     }
     else if (countDigits(principalZip) != 5)
     {
       addError("Please provide the principal's valid 5-digit Zip Code");
     }
     else if(!isBlank(principalState)) //if zip and state present validate zipcode for state
     {
       principalZip = getDigitsOnly(principalZip);
       if(!validZipForState(principalState,principalZip))
       {
         addError("Principal Zip Code " + principalZip + " is not a valid zip for Principal State " + principalState);
       }
     }
     
     if(isBlank(moto))
     {
       addError("Please select the % of mail/phone order/internet sales");
     }
     
     if(isBlank(mcc))
     {
       addError("Please select a Merchant Category Code");
     }


     if(isBlank(annualSalesVolume))
     {
       addError("Please provide the estimated Annual Sales Volume");
     }
     else
     {
       //annualSalesVolume = getDigitsOnly(annualSalesVolume);

       if(!isNumber(annualSalesVolume))
       {
         addError("Please provide a valid estimated Annual Sales Volume");
       }
     }

     if(isBlank(averageSaleAmount))
     {
       addError("Please provide the Average Sale Amount");
     }
     else
     {
       //averageSaleAmount = getDigitsOnly(averageSaleAmount);

       if(!isNumber(averageSaleAmount))
       {
         addError("Please provide a valid Average Sale Amount");
       }
     }
     
     if(isBlank(discountRate) || !isRealNumber(discountRate))
     {
       addError("Please provide a valid Discount Rate");
     }
     else
     {
       validateRate(discountRate);
     }

     if(!isBlank(discountPerItem) && !isRealNumber(discountPerItem))
     {
       addError("Please provide a valid Discount Per Item Rate");
     }

     if(isBlank(salesRepId))
     {
       addError("Please provide a Sales Rep Id");
     }
     else if(salesRepId.length() > FIELD_LENGTH_SALESREPID)
     {
       addError("Sales Rep Id is too long.  Truncate to 15 characters.");
     }

     if(!isBlank(url) && url.length() > FIELD_LENGTH_URL)
     {
       addError("Website Address is too long.  Truncate to 45 characters.");
     }
  
    return (!hasErrors());
  }


  private boolean validZipForState(String state, String zip)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;
    int                 zipNum  = 0;

    try
    {
      zipNum = Integer.parseInt(zip);

      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:2448^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  zip_low,zip_high
//          from    RAPP_APP_VALID_ZIPCODES
//          where   state_code = :state
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  zip_low,zip_high\n        from    RAPP_APP_VALID_ZIPCODES\n        where   state_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,state);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"35com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2453^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        int low   = rs.getInt("zip_low");
        int high  = rs.getInt("zip_high");
 
        if(zipNum >= low && zipNum <= high)
        {
          result = true;
        }
      }
 
      it.close();
    
      cleanUp();
    }
    catch(Exception e)
    {
      result = false;
      logEntry("validZipForState()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  private boolean groupsOfFiveOrMore(String data)
  {
    boolean result = false;

    try
    {
      String group51 = data.substring(0,5);
      String group52 = data.substring(1,6);
      String group53 = data.substring(2,7);
      String group54 = data.substring(3,8);
      String group55 = data.substring(4);

      String group61 = data.substring(0,6);
      String group62 = data.substring(1,7);
      String group63 = data.substring(2,8);
      String group64 = data.substring(3);

      String group71 = data.substring(0,7);
      String group72 = data.substring(1,8);
      String group73 = data.substring(2);

      String group81 = data.substring(0,8);
      String group82 = data.substring(1);

      if(repeatingOne(group51) ||
         repeatingOne(group52) ||
         repeatingOne(group53) ||
         repeatingOne(group54) ||
         repeatingOne(group55) ||
         repeatingOne(group61) ||
         repeatingOne(group62) ||
         repeatingOne(group63) ||
         repeatingOne(group64) ||
         repeatingOne(group71) ||
         repeatingOne(group72) ||
         repeatingOne(group73) ||
         repeatingOne(group81) ||
         repeatingOne(group82) 
        )
      {
        result = true;
      }

    }
    catch(Exception e)
    {
      System.out.println("groupsOfFiveOrMore: " + e.toString());
    }
    return result;
  }

  private boolean repeatingOne(String data) 
  {
    boolean result = true;

    try
    {
      String[]  numArray = new String[data.length()];

      for(int x=0; x<numArray.length; x++)
      {
        if(x == (numArray.length - 1))
        {
          numArray[x] = data.substring(x);
        }
        else
        {
          numArray[x] = data.substring(x, x+1);
        }
      }

      for(int x=0; x<numArray.length - 1; x++)
      {
        if(!numArray[x].equals(numArray[x+1]))
        {
          result = false;
        }
      }
    }
    catch(Exception e)
    {
      System.out.println("repeatingOne: " + e.toString());
    }

    return result;

  }
  
  
  private boolean repeatingTwo(String data) 
  {
    boolean result = false;

    String group1 = data.substring(0,2);
    String group2 = data.substring(2,4);
    String group3 = data.substring(4,6);
    String group4 = data.substring(6,8);

    if(group1.equals(group2) && group1.equals(group3) && group1.equals(group4))
    {
      result = true;
    }

    return result;

  }

  private boolean repeatingThree(String data)
  {
    boolean result = false;

    String group1 = data.substring(0,3);
    String group2 = data.substring(3,6);
    String group3 = data.substring(6);

    if(group1.equals(group2) && group1.equals(group3))
    {
      result = true;
    }

    return result;

  }



  private boolean validTransitRoute(String aba)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {

      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:2622^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  TRANSIT_ROUTING_NUM 
//          from    RAP_APP_BANK_ABA
//          where   TRANSIT_ROUTING_NUM = :aba
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  TRANSIT_ROUTING_NUM \n        from    RAP_APP_BANK_ABA\n        where   TRANSIT_ROUTING_NUM =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,aba);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"36com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2627^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      it.close();
    }
    catch(Exception e)
    {
      result = false;
      logEntry("validTransitRoute()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  private int getValidationType()
  {
    int validationType = 0;

    if(this.mcc.equals("9211") ||
       this.mcc.equals("9222") ||
       this.mcc.equals("9311") ||
       this.mcc.equals("9399"))
    {

      if(!this.moto.equals("Y"))
      {
        validationType = DISC_RATE_VALIDATION_GOVERNMENT_CARD;
      }
      else if(!isBlank(this.url))
      {
        validationType = DISC_RATE_VALIDATION_GOVERNMENT_INTERNET;
      }
      else if(this.moto.equals("Y"))
      {
        validationType = DISC_RATE_VALIDATION_GOVERNMENT_NO_CARD;
      }

    }
    else if(this.mcc.equals("5814"))
    {
      validationType = DISC_RATE_VALIDATION_QUICK_SERVE_RESTAURANT;
    }
    else if(this.mcc.equals("5812"))
    {
      validationType = DISC_RATE_VALIDATION_STANDARD_RESTAURANT;
    }
    else if(this.mcc.equals("5411"))
    {
      validationType = DISC_RATE_VALIDATION_GROCERY_SUPERMARKET;
    }
    else if(this.moto.equals("Y") || !isBlank(this.url))
    {
      validationType = DISC_RATE_VALIDATION_MOTO_INTERNET;
    }
    else
    {
      validationType = DISC_RATE_VALIDATION_STANDARD_RETAIL;
    }
      
    return validationType;      
  }


  private void validateRate(String discRate)
  {

    int validationType = getValidationType();

    try
    {
      double tempDub = Double.parseDouble(discRate);

      switch(validationType)
      {
        case DISC_RATE_VALIDATION_STANDARD_RETAIL:
          if(tempDub < 1.45 || tempDub > 3.00)
          {
            addError("The Discount Rate must be between 1.45 and 3.00 for this merchant");
          }        
        break;

        case DISC_RATE_VALIDATION_MOTO_INTERNET:
          if(tempDub < 1.85 || tempDub > 3.50)
          {
            addError("The Discount Rate must be between 1.85 and 3.50 for this merchant");
          }        
        break;

        case DISC_RATE_VALIDATION_GOVERNMENT_CARD:
          if(tempDub != 1.65)
          {
            addError("The Discount Rate must be 1.65% + $0.10 for this merchant");
          }        
        break;

        case DISC_RATE_VALIDATION_GOVERNMENT_NO_CARD:
          if(tempDub != 1.65)
          {
            addError("The Discount Rate must be 1.65% + $0.10 for this merchant");
          }        
        break;

        case DISC_RATE_VALIDATION_GOVERNMENT_INTERNET:
          if(tempDub != 1.65)
          {
            addError("The Discount Rate must be 1.65% + $0.10 for this merchant");
          }        
        break;
        
        case DISC_RATE_VALIDATION_QUICK_SERVE_RESTAURANT:
          if(tempDub != 2.25)
          {
            addError("The Discount Rate must be 2.25% + $0.05 for this merchant");
          }        
        break;

        case DISC_RATE_VALIDATION_STANDARD_RESTAURANT:
          if(tempDub < 1.22 || tempDub > 3.00)
          {
            addError("The Discount Rate must be between 1.22 and 3.00 for this merchant");
          }        
        break;

        case DISC_RATE_VALIDATION_GROCERY_SUPERMARKET:
          if(tempDub != 1.26)
          {
            addError("The Discount Rate must be 1.26 for this merchant");
          }        
        break;
      }

    }
    catch(Exception e)
    {  
    }
  }

  /*
  ** ACCESSORS
  */
  
  public String getBusinessDba()
  {
    return this.businessDba;
  }
  public String getBusinessAddress()
  {
    return this.businessAddress;
  }
  public String getBusinessCity()
  {
    return this.businessCity;
  }
  public String getBusinessState()
  {
    return this.businessState;
  } 
  public String getBusinessZip()
  {
    return this.businessZip;
  }
  public String getBusinessPhone()
  {
    return formatPhone(this.businessPhone);
  }
  public String getUrl()
  {
    return this.url;
  }
  public String getEmail()
  {
    return this.email;
  }
  public String getBusType()
  {
    return this.busType;
  }
  public String getCorporateName()
  {
    return this.corporateName;
  }
  public String getCorporateAddress()
  {
    return this.corporateAddress;
  }
  public String getCorporateAddressLine2()
  {
    return this.corporateAddressLine2;
  }
  public String getCorporateCity()
  {
    return this.corporateCity;
  }
  public String getCorporateState()
  {
    return this.corporateState;
  }
  public String getCorporateZip()
  {
    return this.corporateZip;
  }
  public String getYearsInBusiness()
  {
    return this.yearsInBusiness;
  }
  public String getNonProfit()
  {
    return this.nonProfit;
  }
  public String getTaxId()
  {
    return formatSsn(this.taxId);
  }
  public String getCheckingAccountNum()
  {
    return this.checkingAccountNum;
  }
  public String getTransitRoute()
  {
    return this.transitRoute;
  }
  public String getMerchantNumber()
  {
    return this.merchantNumber;
  }
  public String getPrincipalName()
  {
    return this.principalName;
  }
  public String getPrincipalTitle()
  {
    return this.principalTitle;
  }
  public String getPrincipalSsn()
  {
    return formatSsn(this.principalSsn);
  }
  public String getPrincipalAddress()
  {
    return this.principalAddress;
  }
  public String getPrincipalCity()
  {
    return this.principalCity;
  }
  public String getPrincipalState()
  {
    return this.principalState;
  }
  public String getPrincipalZip()
  {
    return this.principalZip;
  }
  public String getAnnualSalesVolume()
  {
    return this.annualSalesVolume;
  }
  public String getAverageSaleAmount()
  {
    return this.averageSaleAmount;
  }
  public String getMoto()
  {
    return this.moto;
  }
  public String getMcc()
  {
    return this.mcc;
  }
  public String getDiscountRate()
  {
    return this.discountRate;
  }
  
  public String getDiscountPerItem()
  {
    return this.discountPerItem;
  }

  public String getMembershipFee()
  {
    return this.membershipFee;
  }

  public String getSalesRepId()
  {
    return this.salesRepId;
  }
  public void setSalesRepId(String salesRepId)
  {
    this.salesRepId = salesRepId;
  }

  public String getIntermediateLevel2()
  {
    return this.intermediateLevel2;
  }
  public void setIntermediateLevel2(String intermediateLevel2)
  {
    this.intermediateLevel2 = intermediateLevel2;
  }

  public String getFranchiseCode()
  {
    return this.franchiseCode;
  }
  public void setFranchiseCode(String franchiseCode)
  {
    this.franchiseCode = franchiseCode;
  }

  public void setBusinessDba(String businessDba)
  {
    this.businessDba = businessDba;
  }
  public void setBusinessAddress(String businessAddress)
  {
    this.businessAddress = businessAddress;
  }
  public void setBusinessCity(String businessCity)
  {
    this.businessCity = businessCity;
  }
  public void setBusinessState(String businessState)
  {
    this.businessState = businessState;
  } 
  public void setBusinessZip(String businessZip)
  {
    this.businessZip = businessZip;
  }
  public void setBusinessPhone(String businessPhone)
  {
    this.businessPhone = businessPhone;
  }
  public void setUrl(String url)
  {
    this.url = url;
  }
  public void setEmail(String email)
  {
    this.email = email;
  }
  public void setBusType(String busType)
  {
    this.busType = busType;
  }
  public void setCorporateName(String corporateName)
  {
    this.corporateName = corporateName;
  }
  public void setCorporateAddress(String corporateAddress)
  {
    this.corporateAddress = corporateAddress;
  }
  public void setCorporateAddressLine2(String corporateAddressLine2)
  {
    this.corporateAddressLine2 = corporateAddressLine2;
  }
  public void setCorporateCity(String corporateCity)
  {
    this.corporateCity = corporateCity;
  }
  public void setCorporateState(String corporateState)
  {
    this.corporateState = corporateState;
  }
  public void setCorporateZip(String corporateZip)
  {
    this.corporateZip = corporateZip;
  }
  public void setYearsInBusiness(String yearsInBusiness)
  {
    this.yearsInBusiness = yearsInBusiness;
  }
  public void setNonProfit(String nonProfit)
  {
    this.nonProfit = nonProfit;
  }
  public void setTaxId(String taxId)
  {
    this.taxId = taxId;
  }
  public void setCheckingAccountNum(String checkingAccountNum)
  {
    this.checkingAccountNum = checkingAccountNum;
  }
  public void setTransitRoute(String transitRoute)
  {
    this.transitRoute = transitRoute;
  }
  public void setMerchantNumber(String merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  public void setPrincipalName(String principalName)
  {
    this.principalName = principalName;
  }
  public void setPrincipalTitle(String principalTitle)
  {
    this.principalTitle = principalTitle;
  }
  public void setPrincipalSsn(String principalSsn)
  {
    this.principalSsn = principalSsn;
  }
  public void setPrincipalAddress(String principalAddress)
  {
    this.principalAddress = principalAddress;
  }
  public void setPrincipalCity(String principalCity)
  {
    this.principalCity = principalCity;
  }
  public void setPrincipalState(String principalState)
  {
    this.principalState = principalState;
  }
  public void setPrincipalZip(String principalZip)
  {
    this.principalZip = principalZip;
  }
  public void setAnnualSalesVolume(String annualSalesVolume)
  {
    this.annualSalesVolume = annualSalesVolume;
  }
  public void setAverageSaleAmount(String averageSaleAmount)
  {
    this.averageSaleAmount = averageSaleAmount;
  }
  public void setMoto(String moto)
  {
    this.moto = moto;
  }
  public void setMcc(String mcc)
  {
    this.mcc = mcc;
  }
  public void setDiscountRate(String discountRate)
  {
    this.discountRate = discountRate;
  }
  
  public void setDiscountPerItem(String discountPerItem)
  {
    this.discountPerItem = discountPerItem;
  }

  public void setMembershipFee(String membershipFee)
  {
    this.membershipFee = membershipFee;
  }

  public long getPrimaryKey()
  {
    return this.primaryKey;
  }
  
  private void setMerchNumFromPrimaryKey()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:3105^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_number
//          from    merchant
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_number\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.ops.DiscoverRapBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"37com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3110^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        merchNum = rs.getLong("merch_number");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("setMerchantNumberFromPrimaryKey(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public void setPrimaryKey(String primaryKey)
  {
    try
    {
      this.primaryKey = Long.parseLong(primaryKey);
      
      setMerchNumFromPrimaryKey();
    }
    catch(Exception e)
    {
      addError("Error: Please exit and restart application");
      this.primaryKey = 0L;
    }
  }

/* get all status info */
  public String getDateTimeOfTransmission()
  {
    return formatResponseData(dateTimeOfTransmission);
  }
  public String getDateTimeOfResponse()
  {
    return formatResponseData(dateTimeOfResponse);
  }
  public String getDecision()
  {
    return formatResponseData(decision);
  }
  public String getDisposition()
  {
    return formatResponseData(disposition);
  }
  public String getDiscoverMerchNum()
  {
    return formatResponseData(discoverMerchNum);
  }
  public String getAcquirerMerchNum()
  {
    return formatResponseData(acquirerMerchNum);
  }

  private String formatResponseData(String data)
  {
    if(isBlank(data))
    {
      return "----";
    }

    return data;
  }

  public String getTsysTimestamp()
  {
    return this.tsysTimestamp;
  }
  
  public String getMmsTimestamp()
  {
    return this.mmsTimestamp;
  }

  public String getForceMoveTimestamp()
  { 
    return this.forceMoveTimestamp;
  }


/* manual changes variables*/
  public String getManDecision()
  {
    return this.manDecision;
  }  
  public String getManDisposition()
  {
    return this.manDisposition;
  }
  public String getManDiscNum()
  {
    return this.manDiscNum;
  }

  public void setManDecision(String manDecision)
  {
    this.manDecision = manDecision;
  }  
  public void setManDisposition(String manDisposition)
  {
    this.manDisposition = manDisposition;
  }
  public void setManDiscNum(String manDiscNum)
  {
    this.manDiscNum = manDiscNum;
  }


  public String getAppSource()
  {
    return this.appSource;
  }
  public void setAppSource(String appSource)
  {
    this.appSource = appSource;
  }
  public boolean wasSentViaFax()
  {
    return (this.transmissionMethod.equals(TRANSMISSION_METHOD_FAX));
  }

  public boolean wasSentViaTransmission()
  {
    return (this.transmissionMethod.equals(TRANSMISSION_METHOD_AUTO));
  }
  
  public boolean isSubmitted()
  {
    return this.submitted;
  }

  public boolean isErrorStatus()
  {
    return this.errorStatus;
  }

  public boolean isReSubmitted()
  {
    return this.reSubmitted;
  }

  public boolean isManSubmitted()
  {
    return this.manSubmitted;
  }
  
  public boolean isAllowManUpdate()
  {
    return this.allowManUpdate;
  }

  public boolean isShowTimestamps()
  {
    return this.showTimestamps;
  }

  public void dontAllowManUpdate()
  {
    this.allowManUpdate = false;
  }
  
  public void setAllowManUpdate(String temp)
  {
    this.allowManUpdate = true;
  }

  public boolean isQueuedForTrans()
  {
    return this.queuedForTrans;
  }

  public boolean isResponseReceived()
  {
    return this.responseReceived;
  }

  public void setSubmit(String temp)
  {
    this.submitted = true;
  }
  
  public void setReSubmit(String temp)
  {
    this.reSubmitted = true;
  }

  public void setUserBean(UserBean user)
  {
    this.user = user;
  }

  public void setManSubmit(String temp)
  {
    this.manSubmitted = true;
  }

  public void setTransSubmit(String temp)
  {
    this.transmissionMethod = TRANSMISSION_METHOD_AUTO;
    this.submitted = true;
  }

  public void setFaxSubmit(String temp)
  {
    this.transmissionMethod = TRANSMISSION_METHOD_FAX;
    this.submitted = true;
  }

  public String getTransmissionStatus()
  {
    return this.transmissionStatus;
  }

  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }

  public void addError(String err)
  {
    try
    {
      errors.add(err);
    }
    catch(Exception e)
    {
    }
  } 

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
  
  public boolean isNumber(String test)
  {
    boolean pass = false;
    
    try
    {
      long i = Long.parseLong(test);
      pass = true;
    }
    catch(Exception e)
    {
    }
    
    return pass;
  }

  public boolean isRealNumber(String test)
  {
    boolean pass = false;
    
    try
    {
      double i = Double.parseDouble(test);
      pass = true;
    }
    catch(Exception e)
    {
    }
    
    return pass;
  }

  public long getDigits(String raw)
  {
    long          result      = 0L;
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = Long.parseLong(digits.toString());
    }
    catch(Exception e)
    {
      if(raw != null && !raw.equals(""))
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDigits('" + raw + "'): " + e.toString());
      }
    }
    
    return result;
  }

  public String getDigitsOnly(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");
    String        result      = "";

    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = digits.toString();
    }
    catch(Exception e)
    {}
    
    return result;
  }

  
  public int countDigits(String raw)
  {
    int result = 0;
    
    for(int i=0; i<raw.length(); ++i)
    {
      if(Character.isDigit(raw.charAt(i)))
      {
        ++result;
      }
    }
    
    return result;
  }

  public boolean isEmail(String test)
  {
    boolean pass = false;
    
    int firstAt = test.indexOf('@');
    if (!isBlank(test) && firstAt > 0 && 
        test.indexOf('@',firstAt + 1) == -1)
    {
      pass = true;
    }
    
    return pass;
  }

  private String formatPhone(String phone)
  {
    if(isBlank(phone))
    {
      return "";
    }
    if(phone.length() != 10)
    {
      return "";
    }
    String areaCode   = phone.substring(0,3);
    String firstThree = phone.substring(3,6);
    String lastFour   = phone.substring(6);
    return ("(" + areaCode + ")" + " " + firstThree + "-" + lastFour);
  }

  private String formatSsn(String ssn)
  {
    if(isBlank(ssn))
    {
      return "";
    }
    if(ssn.length() != 9)
    {
      return ssn;
    }
    String part1   = ssn.substring(0,3);
    String part2   = ssn.substring(3,5);
    String part3   = ssn.substring(5);
    return (part1 + "-" + part2 + "-" + part3);
  }

  /*
  ** METHOD setSeqNum
  */
  private void setSeqNum()
  {
    ResultSetIterator   it            = null;
    
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:3518^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   application_sequence.nextval pk
//          from     dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   application_sequence.nextval pk\n        from     dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.ops.DiscoverRapBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"38com.mes.ops.DiscoverRapBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3522^7*/
    
      ResultSet rs = it.getResultSet();
 
      if(rs.next())
      {
        this.primaryKey = rs.getLong("pk");
      }
    }
    catch(Exception e)
    {
      logEntry("(setSeqNum)", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

}/*@lineinfo:generated-code*/