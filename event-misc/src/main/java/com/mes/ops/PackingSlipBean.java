/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/PackingSlipBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/07/04 4:18p $
  Version            : $Revision: 29 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.queues.QueueTools;
import com.mes.support.DateTimeFormatter;
import com.mes.user.UserBean;

public class PackingSlipBean extends com.mes.screens.SequenceDataBean
{
  public static final int SATURDAY_DELIVERY           = 1;
  public static final int PRIORITY_DELIVERY           = 2;
  public static final int STANDARD_OVERNIGHT          = 3;
  public static final int FEDEX_2DAY                  = 4;
  public static final int EXPRESS_SAVER               = 5;
  public static final int FEDEX_GROUND_DELIVERY       = 6;
  public static final int USPS_PRIORITY_MAIL          = 7;
  public static final int USPS_MAIL                   = 8;
  
  public static final int TIMEZONE_EASTERN            = 705;
  public static final int TIMEZONE_CENTRAL            = 706;
  public static final int TIMEZONE_MOUNTAIN           = 707;
  public static final int TIMEZONE_PACIFIC            = 708;
  public static final int TIMEZONE_ALASKA             = 709;
  public static final int TIMEZONE_ALEUTIAN           = 710;
  public static final int TIMEZONE_HAWAII             = 711;

  public static final String  SLIP_TYPE_SINGLE        = "single";
  public static final String  SLIP_TYPE_MULTI         = "multi";


  //static
  private String dbaName                          = "";
  private String merchNum                         = "";
  private String mcc                              = "";
  private String masterVnum                       = "";
  private String masterAppId                      = "";
  private String controlNum                       = "";
  private String appTypeDesc                      = "";
  private String appCreatedDate                   = "";
  private String contactName                      = "";
  private String contactPhone                     = "";

  private String physicalAddressDesc              = "";
  private String physicalAddressLine1             = "";
  private String physicalAddressLine2             = ""; 
  private String physicalAddressCity              = "";
  private String physicalAddressState             = "";
  private String physicalAddressZip               = "";

  //dynamic
  private String shippingMethodSingle             = "";
  private String timeZoneSingle                   = "";
  private String rushAuthorizationSingle          = "";

  private String shippingMethodMulti              = "";
  private String timeZoneMulti                    = "";
  private String rushAuthorizationMulti           = "";

  private String merchantType                     = "";
  private String dialPayType                      = "";
  private String shipEquipmentCheckBox            = "";
  private String callTagCheckBox                  = "";
  private String shipAddDifCheckBox               = "";

  //not really using shipAddDif anymore.. replaced with shipping fields
  private String shipAddDif                       = "";

  private String shippingName                     = "";
  private String shippingAddress1                 = "";
  private String shippingAddress2                 = "";
  private String shippingCity                     = "";
  private String shippingState                    = "";
  private String shippingZip                      = "";
  private String shippingPhone                    = "";
  private String shippingContactName              = "";

  private String merch2Id                         = "";
  private String merch2Dba                        = "";
  private String merch2Mcc                        = "";
  private String merch2Vnum                       = "";
  private String merch2AppId                      = "";
  private String merch3Id                         = "";
  private String merch3Dba                        = "";
  private String merch3Mcc                        = "";
  private String merch3Vnum                       = "";
  private String merch3AppId                      = "";
  private String merch4Id                         = "";
  private String merch4Dba                        = "";
  private String merch4Mcc                        = "";
  private String merch4Vnum                       = "";
  private String merch4AppId                      = "";
  private String merch5Id                         = "";
  private String merch5Dba                        = "";
  private String merch5Mcc                        = "";
  private String merch5Vnum                       = "";
  private String merch5AppId                      = "";
  private String merch6Id                         = "";
  private String merch6Dba                        = "";
  private String merch6Mcc                        = "";
  private String merch6Vnum                       = "";
  private String merch6AppId                      = "";
  private String visaAccepted                     = "";
  private String amexAccepted                     = "";
  private String discAccepted                     = "";
  private String debitAccepted                    = "";
  private String callTagItem                      = "";
  private String callTagItemSerialNum             = "";
  private String specialComment                   = "";
  private String discoverNumber                   = "No Discover Number";
  private String largePlate                       = "";
  private String smallPlate                       = "";
  private String imprinter                        = "";
  private String accessCode                       = "";
  private String accessCodeDesc                   = "";
  private String terminalStatus                   = "";
  private String printerStatus                    = "";
  private String pinpadStatus                     = "";
  private String slipType                         = "";
  private String conversionType                   = "New Account";

  //end dynamic items

  //discover printed material variables
  private String  coverSheetPrinted               = "";
  private String  pricingSchedulePrinted          = "";
  private boolean packingSlipSubmitted            = false;
  private boolean discoverApp                     = false;


  private int    numOfTerminals                   = 0;

  private int    currentStage                     = -1;
  private String currentStageDesc                 = "";

  private Vector vNums                            = new Vector();
  public Vector  terminals                        = new Vector();
  public boolean stageOnly                        = false;

  public Vector  method                           = new Vector();
  public Vector  methodDesc                       = new Vector();

  public Vector  managers                         = new Vector();
  public Vector  dialpayTypeDesc                  = new Vector();
  public Vector  merchantTypeDesc                 = new Vector();

  public Vector  timezone                         = new Vector();
  public Vector  timezoneDesc                     = new Vector();

  public Vector  equipStatusCode                  = new Vector();
  public Vector  equipStatus                      = new Vector();

  public Vector  printer                          = new Vector();
  public Vector  printerCode                      = new Vector();

  public Vector  pinpad                           = new Vector();
  public Vector  pinpadCode                       = new Vector();

  public Vector  terminal                         = new Vector();
  public Vector  terminalCode                     = new Vector();

  public Vector  stateCode                        = new Vector();
  public Vector  stateDesc                        = new Vector();

  public PackingSlipBean()
  {
    
    try
    {
      StringBuffer      qs      = new StringBuffer("");
      PreparedStatement ps      = null;
      ResultSet         rs      = null;
    
      qs.append("select                         ");
      qs.append("*                              "); 
      qs.append("from                           ");
      qs.append("equipment                      ");
      qs.append("where                          ");
      qs.append("equiptype_code in (?,?,?,?,?)  ");
      qs.append("order by equip_descriptor      ");

      ps = getPreparedStatement(qs.toString());

      ps.setInt(1, mesConstants.APP_EQUIP_TYPE_PRINTER);
      ps.setInt(2, mesConstants.APP_EQUIP_TYPE_PINPAD);
      ps.setInt(3, mesConstants.APP_EQUIP_TYPE_TERMINAL);
      ps.setInt(4, mesConstants.APP_EQUIP_TYPE_TERM_PRINTER);
      ps.setInt(5, mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD);

      rs = ps.executeQuery();
    
      while(rs.next())
      {
        switch(rs.getInt("equiptype_code"))
        {
          case mesConstants.APP_EQUIP_TYPE_PRINTER:
            printer.add(rs.getString("equip_descriptor"));
            printerCode.add(rs.getString("equip_model"));
          break;
          
          case mesConstants.APP_EQUIP_TYPE_PINPAD:
            pinpad.add(rs.getString("equip_descriptor"));
            pinpadCode.add(rs.getString("equip_model"));
          break;

          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
            terminal.add(rs.getString("equip_descriptor"));
            terminalCode.add(rs.getString("equip_model"));
          break;
        }
      }
      
      ps = getPreparedStatement("select * from countrystate");

      rs = ps.executeQuery();

      while(rs.next())
      {
        stateCode.add(rs.getString("countrystate_code"));
        stateDesc.add(rs.getString("countrystate_desc"));
      }

      rs.close();
      ps.close();




    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "constructor: " + e.toString());
      addError("Constructor Error: " + e.toString());
    }


    //old fedex shipping methods
/*
    method.add(Integer.toString(SATURDAY_DELIVERY));
    methodDesc.add("SATURDAY DELIVERY  (CHECK RECIPIENT'S ZIP CODE)");
    method.add(Integer.toString(PRIORITY_DELIVERY));
    methodDesc.add("PRIORITY OVERNIGHT (NEXT BUS. DAY AM DELIVERY)");
    method.add(Integer.toString(STANDARD_OVERNIGHT));
    methodDesc.add("STANDARD OVERNIGHT (NEXT BUS. DAY PM DELIVERY)");
    method.add(Integer.toString(FEDEX_2DAY));
    methodDesc.add("FED EX 2DAY (2ND DAY PM DELIVERY - DEFAULT)");
    method.add(Integer.toString(EXPRESS_SAVER));
    methodDesc.add("EXPRESS SAVER (1 TO 3 DAY SERVICE, PM DELIVERY)");
    method.add(Integer.toString(FEDEX_GROUND_DELIVERY));
    methodDesc.add("FED EX GROUND DELIVERY (REFER TO GROUND CHART)");
    method.add(Integer.toString(USPS_PRIORITY_MAIL));
    methodDesc.add("USPS PRIORITY MAIL (2-3 DAY, NO GUARANTEE)");
    method.add(Integer.toString(USPS_MAIL));
    methodDesc.add("USPS MAIL");
*/

    //new airborne shipping methods
    method.add(Integer.toString(mesConstants.SHIPPING_METHOD_2DAY));
    methodDesc.add("2nd Day: delivered by 5:00 p.m. second business day (default)");

    method.add(Integer.toString(mesConstants.SHIPPING_METHOD_OVERNIGHT_AM));
    methodDesc.add("Express: delivered by 12:00 noon next business day");

    method.add(Integer.toString(mesConstants.SHIPPING_METHOD_OVERNIGHT_SAT));
    methodDesc.add("Express Saturday: delivered by 5:00 p.m. Saturday (if available)");

    method.add(Integer.toString(mesConstants.SHIPPING_METHOD_STANDARD));
    methodDesc.add("Ground: delivered in 1 to 7 business days (no guarantee)");


    managers.add("STEPHEN PRINCE");
    managers.add("GEORGE LAKE");
    managers.add("JULIE CARRICK");
    managers.add("BILL BAYNE");
    managers.add("LORI BERRICHOA");
    managers.add("JUDY WASHINGTON");
    managers.add("BILL MANN");
    managers.add("MARK GRAHAM");
    managers.add("STACEY COLLINS");

    timezone.add(Integer.toString(TIMEZONE_PACIFIC));
    timezoneDesc.add("PDT PACIFIC 708");

    timezone.add(Integer.toString(TIMEZONE_EASTERN));
    timezoneDesc.add("EDT EASTERN 705");

    timezone.add(Integer.toString(TIMEZONE_CENTRAL));
    timezoneDesc.add("CDT CENTRAL 706");

    timezone.add(Integer.toString(TIMEZONE_MOUNTAIN));
    timezoneDesc.add("MDT MOUNTAIN 707");

    timezone.add(Integer.toString(TIMEZONE_ALASKA));
    timezoneDesc.add("AKDT ALASKA 709");

    timezone.add(Integer.toString(TIMEZONE_ALEUTIAN));
    timezoneDesc.add("ADT ALEUTIAN 710");

    timezone.add(Integer.toString(TIMEZONE_HAWAII));
    timezoneDesc.add("HST HAWAII 711");

    dialpayTypeDesc.add("AUTH ONLY");
    dialpayTypeDesc.add("AUTH W/CAPTURE");

    merchantTypeDesc.add("RESTAURANT");
    merchantTypeDesc.add("FINE DINING");
    merchantTypeDesc.add("RETAIL/MOTO");
    merchantTypeDesc.add("HOTEL");
    merchantTypeDesc.add("CASH ADVANCE");

    equipStatusCode.add(Integer.toString(mesConstants.APP_EQUIP_RENT));
    equipStatus.add("RENTAL");

    equipStatusCode.add(Integer.toString(mesConstants.APP_EQUIP_PURCHASE));
    equipStatus.add("PURCHASE");

    equipStatusCode.add(Integer.toString(mesConstants.APP_EQUIP_LEASE));
    equipStatus.add("LEASECOM");

    equipStatusCode.add(Integer.toString(mesConstants.APP_EQUIP_OWNED));
    equipStatus.add("MERCHOWN");

    equipStatusCode.add(Integer.toString(mesConstants.APP_EQUIP_LOAN));
    equipStatus.add("LOANER");

  }
  
  public int numTerminals()
  {
    return terminals.size();
  }

  public void setNumOfTerminals(String num)
  {
    try
    {
      this.numOfTerminals = Integer.parseInt(num);
    }
    catch(Exception e)
    {
      this.numOfTerminals = 0;
    }
  }

  public void updateEquipInfo(HttpServletRequest req)
  {
    for(int i=0; i < numOfTerminals; i++)
    {
      
      TerminalInfo ti = new TerminalInfo();
        
      ti.setVnumber(            isBlank(req.getParameter("vnumber"          + i))    ? "" : req.getParameter("vnumber"         + i) );
      ti.setTermApplication(    isBlank(req.getParameter("termApplication"  + i))    ? "" : req.getParameter("termApplication" + i) );
      ti.setTerminalModel(      isBlank(req.getParameter("terminalModel"    + i))    ? "" : req.getParameter("terminalModel"   + i) );
      ti.setPrinterModel(       isBlank(req.getParameter("printerModel"     + i))    ? "" : req.getParameter("printerModel"    + i) );
      ti.setPinpadModel(        isBlank(req.getParameter("pinpadModel"      + i))    ? "" : req.getParameter("pinpadModel"     + i) );
      ti.setTerminalRefurb(     isBlank(req.getParameter("terminalRefurb"   + i))    ? "" : req.getParameter("terminalRefurb"  + i) );
      ti.setPrinterRefurb(      isBlank(req.getParameter("printerRefurb"    + i))    ? "" : req.getParameter("printerRefurb"   + i) );
      ti.setPinpadRefurb(       isBlank(req.getParameter("pinpadRefurb"     + i))    ? "" : req.getParameter("pinpadRefurb"    + i) );
      ti.setTerminalSerialNum(  isBlank(req.getParameter("terminalSerialNum"  + i))  ? "" : req.getParameter("terminalSerialNum" + i) );
      ti.setPrinterSerialNum(   isBlank(req.getParameter("printerSerialNum"   + i))  ? "" : req.getParameter("printerSerialNum"  + i) );
      ti.setPinpadSerialNum(    isBlank(req.getParameter("pinpadSerialNum"    + i))  ? "" : req.getParameter("pinpadSerialNum"   + i) );
 
      terminals.add(ti);
   
    }

  }

/*
  private void getNewConversionAcct(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select  *              ");
      qs.append("from app_rc_merchant   ");
      qs.append("where app_seq_num = ?  ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
    
      if(rs.next())
      {
        conversionType = (!isBlank(rs.getString("account_type")) && rs.getString("account_type").equals("C")) ? "Conversion Account" : "New Account";
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getNewConversionAcct: " + e.toString());
      addError("getNewConversionAcct: " + e.toString());
    }
  }
*/

  private void getMerchantGeneralInfo(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select                                               ");
      qs.append("merch.merch_business_name,                            "); 
      qs.append("merch.merch_number,                                  ");
      qs.append("merch.merc_cntrl_number,                             ");
      qs.append("merch.sic_code,                                      ");
      qs.append("merch.account_type,                                  ");
      qs.append("app.appsrctype_code,                                 ");
      qs.append("app.app_created_date,                                ");
      qs.append("app.app_type,                                        ");
      qs.append("mcon.merchcont_prim_first_name,                      ");
      qs.append("mcon.merchcont_prim_last_name,                       ");
      qs.append("mcon.merchcont_prim_phone                            ");

      qs.append("from                                                 ");
      qs.append("merchant       merch,                                ");
      qs.append("application    app,                                  ");
      qs.append("merchcontact   mcon                                  ");

      qs.append("where merch.app_seq_num = ?                          ");
      qs.append("and merch.app_seq_num = app.app_seq_num              ");
      qs.append("and merch.app_seq_num = mcon.app_seq_num             ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      rs = ps.executeQuery();
    
      if(rs.next())
      {
        dbaName         = isBlank(rs.getString("merch_business_name"))        ? "" : rs.getString("merch_business_name");
        merchNum        = isBlank(rs.getString("merch_number"))               ? "" : rs.getString("merch_number");
        mcc             = isBlank(rs.getString("sic_code"))                   ? "" : rs.getString("sic_code");
        controlNum      = isBlank(rs.getString("merc_cntrl_number"))          ? "" : rs.getString("merc_cntrl_number");
        appTypeDesc     = isBlank(rs.getString("appsrctype_code"))            ? "" : rs.getString("appsrctype_code");
        
        discoverApp     = (rs.getInt("app_type") == mesConstants.APP_TYPE_DISCOVER || rs.getInt("app_type") == mesConstants.APP_TYPE_DISCOVER_IMS || rs.getInt("app_type") == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL);

        contactName     = isBlank(rs.getString("merchcont_prim_first_name"))  ? "" : (rs.getString("merchcont_prim_first_name") + " ");
        contactName     += isBlank(rs.getString("merchcont_prim_last_name"))  ? "" : rs.getString("merchcont_prim_last_name");
        contactName     = contactName.trim();
        contactPhone    = isBlank(rs.getString("merchcont_prim_phone"))       ? "" : rs.getString("merchcont_prim_phone");
        conversionType  = (!isBlank(rs.getString("account_type")) && rs.getString("account_type").equals("C")) ? "Conversion Account" : "New Account";
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMerchantGeneralInfo: " + e.toString());
      addError("getMerchantGeneralInfo: " + e.toString());
    }
  }

  private void getMerchantCardsAccepted(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select           ");
      qs.append("*                "); 
      qs.append("from             ");
      qs.append("merchpayoption   ");
      qs.append("where            ");
      qs.append("app_seq_num = ?  ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
    
      while(rs.next())
      {
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_VISA:
            visaAccepted = "Y";
          break;

          case mesConstants.APP_CT_AMEX:
            amexAccepted = "Y";
          break;

          case mesConstants.APP_CT_DISCOVER:
            discAccepted = "Y";
          break;

          case mesConstants.APP_CT_DEBIT:
            debitAccepted = "Y";
          break;
        }
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMerchantCardsAccepted: " + e.toString());
      addError("getMerchantCardsAccepted: " + e.toString());
    }
  }



  private void getPhysicalAddress(long primaryKey)
  {
    StringBuffer      qs          = new StringBuffer("");
    PreparedStatement ps          = null;
    ResultSet         rs          = null;

    try
    {
      qs.append("select *                                             ");
      qs.append("from address                                         ");
      qs.append("where app_seq_num = ? and addresstype_code = ?       ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      ps.setInt(2, mesConstants.ADDR_TYPE_BUSINESS);

      rs = ps.executeQuery();

      if(rs.next()) //first we try shipping address
      {
        physicalAddressLine1    = isBlank(rs.getString("address_line1"))      ? "" : rs.getString("address_line1");
        physicalAddressLine2    = isBlank(rs.getString("address_line2"))      ? "" : rs.getString("address_line2");
        physicalAddressCity     = isBlank(rs.getString("address_city"))       ? "" : rs.getString("address_city");
        physicalAddressState    = isBlank(rs.getString("countrystate_code"))  ? "" : rs.getString("countrystate_code");
        physicalAddressZip      = isBlank(rs.getString("address_Zip"))        ? "" : rs.getString("address_zip");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPhysicalAddress: " + e.toString());
      addError("getPhysicalAddress: " + e.toString());
    }
  }

/*********************** Not using this for now.. obviously, since its commented out ********************

  private void getTimeZoneFromMms(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {

      qs.append("select                       ");
      qs.append("tz.description               ");
      qs.append("from                         ");
      qs.append("mms_stage_info msi,          ");
      qs.append("time_zones     tz            ");
      qs.append("where                        ");
      qs.append("msi.app_seq_num = ?          ");
      qs.append("and msi.time_zone = tz.code  ");
      
      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
    
      if(rs.next())
      {
        this.timeZone     = isBlank(rs.getString("description")) ? "" : rs.getString("description"); 
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getTimeZoneFromMms: " + e.toString());
      addError("getTimeZoneFromMms: " + e.toString());
    }
  }

  private void getMmsEquipmentInfo(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {

      qs.append("select                                       ");
      qs.append("msi.term_application,                        ");
      qs.append("msi.vnum,                                    ");
      qs.append("mei.manufacturer,                            ");
      qs.append("mei.term_model,                              ");
      qs.append("mei.printer_model,                           ");
      qs.append("mei.pinpad_type,                             ");
      qs.append("msi.request_id                               ");
      qs.append("from                                         ");
      qs.append("mms_stage_info msi,                          ");
      qs.append("mms_expanded_info mei                        ");
      qs.append("where                                        ");
      qs.append("msi.app_seq_num = ?                          ");
      qs.append("and msi.request_id = mei.request_id(+)       ");
      
      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, primaryKey);

      rs = ps.excuteQuery();
    
      while(rs.next())
      {
        if(rs.getString("term_application") != null && rs.getString("term_application").equals("STAGE"))
        {
          stageOnly = true;
          continue;
        }

        TerminalInfo termInfo = new TerminalInfo();
        
        termInfo.setTerminalModel   (isBlank(rs.getString("term_model"))        ? "" : (rs.getString("manufacturer") + " " +rs.getString("term_model")));
        termInfo.setVnumber         (isBlank(rs.getString("vnum"))              ? "" : rs.getString("vnum"));
        termInfo.setTermApplication (isBlank(rs.getString("term_application"))  ? "" : rs.getString("term_application"));
        termInfo.setPrinterModel    (isBlank(rs.getString("printer_model"))     ? "" : rs.getString("printer_model"));
        termInfo.setRequestId       (isBlank(rs.getString("request_id"))        ? "" : rs.getString("request_id"));
        termInfo.setPinpadModel     (decodePinpadModel(rs.getString("term_application"), rs.getString("term_model"), rs.getString("pinpad_type")));
        terminals.add(termInfo); 
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMmsEquipmentInfo: " + e.toString());
      addError("getMmsEquipmentInfo: " + e.toString());
    }
  }

  private void getShippingMethod(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      
      qs.setLength(0);
      qs.append("select shipping_method ");
      qs.append("from transcom_merchant ");
      qs.append("where app_seq_num = ?  ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,primaryKey);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.shippingMethod = isBlank(rs.getString("shipping_method")) ? "" : rs.getString("shipping_method");
      }
      else
      {
        qs.setLength(0);
        qs.append("select shipping_method ");
        qs.append("from pos_features where app_seq_num = ?");

        ps = getPreparedStatement(qs.toString());

        ps.setLong(1,primaryKey);

        rs = ps.executeQuery();

        if(rs.next())
        {
          this.shippingMethod = isBlank(rs.getString("shipping_method")) ? "" : rs.getString("shipping_method");
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getShippingMethod: " + e.toString());
      addError("getShippingMethod: " + e.toString());
    }
  }

  private String decodePinpadModel(String termApp, String termModel, String pinpadType)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    String            result  = "NO PIN PAD";

    if(isBlank(termApp) || isBlank(termModel) || isBlank(pinpadType))
    {
      return result;
    }

    try
    {

      qs.append("select                                       ");
      qs.append("pinpad_model                                 ");
      qs.append("from                                         ");
      qs.append("mms_available_pinpads                        ");
      qs.append("where                                        ");
      qs.append("term_application = ? and                     ");
      qs.append("term_model       = ? and                     ");
      qs.append("pinpadtype_code  = ?                         ");
      
      ps = getPreparedStatement(qs.toString());

      ps.setString(1, termApp.trim());
      ps.setString(2, termModel.trim());
      ps.setString(3, pinpadType.trim());

      rs = ps.executeQuery();
    
      if(rs.next())
      {
        result = isBlank(rs.getString("pinpad_model"))     ? "" : rs.getString("pinpad_model");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "decodePinpadModel: " + e.toString());
      addError("decodePinpadModel: " + e.toString());
    }

    return result;
  }

*/


  private void getPackingSlipInfo(long primaryKey)
  {
  
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select                     ");
      qs.append("SHIPPING_METHOD,           ");   
      qs.append("TIME_ZONE,                 ");   
      qs.append("RUSH_AUTHORIZATION,        ");   
      qs.append("MERCHANT_TYPE,             ");           
      qs.append("DIALPAY_TYPE,              "); 
      qs.append("SHIP_EQUIPMENT_CHECK,      ");  
      qs.append("CALL_TAG_CHECK,            ");          
      qs.append("SHIP_ADD_DIF_CHECK,        ");      
      qs.append("SHIP_ADD_DIF,              ");  
      qs.append("SHIP_NAME,                 ");
      qs.append("SHIP_ADDRESS_LINE1,        ");
      qs.append("SHIP_ADDRESS_LINE2,        ");
      qs.append("SHIP_CITY,                 ");
      qs.append("SHIP_STATE,                ");
      qs.append("SHIP_ZIP,                  ");
      qs.append("SHIP_PHONE,                ");
      qs.append("SHIP_CONTACT_NAME,         ");
      qs.append("MERCH2_ID,                 ");               
      qs.append("MERCH2_DBA,                ");              
      qs.append("MERCH2_MCC,                ");              
      qs.append("MERCH2_VNUM,               ");             
      qs.append("MERCH2_APPID,              ");            
      qs.append("MERCH3_ID,                 ");               
      qs.append("MERCH3_DBA,                ");              
      qs.append("MERCH3_MCC,                ");              
      qs.append("MERCH3_VNUM,               "); 
      qs.append("MERCH3_APPID,              ");  
      qs.append("MERCH4_ID,                 ");  
      qs.append("MERCH4_DBA,                ");  
      qs.append("MERCH4_MCC,                ");  
      qs.append("MERCH4_VNUM,               ");  
      qs.append("MERCH4_APPID,              ");  
      qs.append("MERCH5_ID,                 ");  
      qs.append("MERCH5_DBA,                ");  
      qs.append("MERCH5_MCC,                ");  
      qs.append("MERCH5_VNUM,               ");  
      qs.append("MERCH5_APPID,              ");  
      qs.append("MERCH6_ID,                 ");  
      qs.append("MERCH6_DBA,                ");  
      qs.append("MERCH6_MCC,                ");  
      qs.append("MERCH6_VNUM,               ");  
      qs.append("MERCH6_APPID,              ");  
      qs.append("VISA_ACCEPTED,             ");  
      qs.append("AMEX_ACCEPTED,             ");  
      qs.append("DISC_ACCEPTED,             ");  
      qs.append("DEBIT_ACCEPTED,            ");  
      qs.append("CALL_TAG_ITEM,             ");  
      qs.append("CALL_TAG_ITEM_SERIAL_NUM,  ");
      qs.append("SPECIAL_COMMENT,           ");
      qs.append("LARGE_PLATE_CHECK,         ");
      qs.append("SMALL_PLATE_CHECK,         ");
      qs.append("IMPRINTER_CHECK,           ");
      qs.append("ACCESS_CODE_CHECK,         ");
      qs.append("ACCESS_CODE_DESC,          ");
      qs.append("TERMINAL_STATUS,           ");
      qs.append("PRINTER_STATUS,            ");
      qs.append("PINPAD_STATUS,             ");
      qs.append("SLIP_TYPE,                 ");
      qs.append("COVERSHEET_PRINTED,        ");
      qs.append("PRICINGSCHEDULE_PRINTED    ");
      qs.append("from PACKING_SLIP_INFO     ");
      qs.append("where app_seq_num = ?      ");
     
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
    
      if(rs.next())
      {
        this.slipType               = isBlank(rs.getString("SLIP_TYPE"))                ? "" : rs.getString("SLIP_TYPE");
        
        if(slipType.equals(SLIP_TYPE_SINGLE))
        {
          this.shippingMethodSingle     = isBlank(rs.getString("SHIPPING_METHOD"))      ? "" : rs.getString("SHIPPING_METHOD");
          this.timeZoneSingle           = isBlank(rs.getString("TIME_ZONE"))            ? "" : rs.getString("TIME_ZONE");
          this.rushAuthorizationSingle  = isBlank(rs.getString("RUSH_AUTHORIZATION"))   ? "" : rs.getString("RUSH_AUTHORIZATION");
        }
        else if(slipType.equals(SLIP_TYPE_MULTI))
        {
          this.shippingMethodMulti      = isBlank(rs.getString("SHIPPING_METHOD"))      ? "" : rs.getString("SHIPPING_METHOD");
          this.timeZoneMulti            = isBlank(rs.getString("TIME_ZONE"))            ? "" : rs.getString("TIME_ZONE");
          this.rushAuthorizationMulti   = isBlank(rs.getString("RUSH_AUTHORIZATION"))   ? "" : rs.getString("RUSH_AUTHORIZATION");
        }

        this.merchantType           = isBlank(rs.getString("MERCHANT_TYPE"))            ? "" : rs.getString("MERCHANT_TYPE");
        this.dialPayType            = isBlank(rs.getString("DIALPAY_TYPE"))             ? "" : rs.getString("DIALPAY_TYPE");
        this.shipEquipmentCheckBox  = isBlank(rs.getString("SHIP_EQUIPMENT_CHECK"))     ? "" : rs.getString("SHIP_EQUIPMENT_CHECK");
        this.callTagCheckBox        = isBlank(rs.getString("CALL_TAG_CHECK"))           ? "" : rs.getString("CALL_TAG_CHECK");
        this.shipAddDifCheckBox     = isBlank(rs.getString("SHIP_ADD_DIF_CHECK"))       ? "" : rs.getString("SHIP_ADD_DIF_CHECK");
        this.shipAddDif             = isBlank(rs.getString("SHIP_ADD_DIF"))             ? "" : rs.getString("SHIP_ADD_DIF");

        this.shippingName           = isBlank(rs.getString("SHIP_NAME"))                ? "" : rs.getString("SHIP_NAME");
        this.shippingAddress1       = isBlank(rs.getString("SHIP_ADDRESS_LINE1"))       ? "" : rs.getString("SHIP_ADDRESS_LINE1");
        this.shippingAddress2       = isBlank(rs.getString("SHIP_ADDRESS_LINE2"))       ? "" : rs.getString("SHIP_ADDRESS_LINE2");
        this.shippingCity           = isBlank(rs.getString("SHIP_CITY"))                ? "" : rs.getString("SHIP_CITY");
        this.shippingState          = isBlank(rs.getString("SHIP_STATE"))               ? "" : rs.getString("SHIP_STATE");
        this.shippingZip            = isBlank(rs.getString("SHIP_ZIP"))                 ? "" : rs.getString("SHIP_ZIP");
        this.shippingPhone          = isBlank(rs.getString("SHIP_PHONE"))               ? "" : rs.getString("SHIP_PHONE");
        this.shippingContactName    = isBlank(rs.getString("SHIP_CONTACT_NAME"))        ? "" : rs.getString("SHIP_CONTACT_NAME");
        
        this.merch2Id               = isBlank(rs.getString("MERCH2_ID"))                ? "" : rs.getString("MERCH2_ID");
        this.merch2Dba              = isBlank(rs.getString("MERCH2_DBA"))               ? "" : rs.getString("MERCH2_DBA");
        this.merch2Mcc              = isBlank(rs.getString("MERCH2_MCC"))               ? "" : rs.getString("MERCH2_MCC");
        this.merch2Vnum             = isBlank(rs.getString("MERCH2_VNUM"))              ? "" : rs.getString("MERCH2_VNUM");
        this.merch2AppId            = isBlank(rs.getString("MERCH2_APPID"))             ? "" : rs.getString("MERCH2_APPID");
        this.merch3Id               = isBlank(rs.getString("MERCH3_ID"))                ? "" : rs.getString("MERCH3_ID");
        this.merch3Dba              = isBlank(rs.getString("MERCH3_DBA"))               ? "" : rs.getString("MERCH3_DBA");
        this.merch3Mcc              = isBlank(rs.getString("MERCH3_MCC"))               ? "" : rs.getString("MERCH3_MCC");
        this.merch3Vnum             = isBlank(rs.getString("MERCH3_VNUM"))              ? "" : rs.getString("MERCH3_VNUM");
        this.merch3AppId            = isBlank(rs.getString("MERCH3_APPID"))             ? "" : rs.getString("MERCH3_APPID");
        this.merch4Id               = isBlank(rs.getString("MERCH4_ID"))                ? "" : rs.getString("MERCH4_ID");
        this.merch4Dba              = isBlank(rs.getString("MERCH4_DBA"))               ? "" : rs.getString("MERCH4_DBA");
        this.merch4Mcc              = isBlank(rs.getString("MERCH4_MCC"))               ? "" : rs.getString("MERCH4_MCC");
        this.merch4Vnum             = isBlank(rs.getString("MERCH4_VNUM"))              ? "" : rs.getString("MERCH4_VNUM");
        this.merch4AppId            = isBlank(rs.getString("MERCH4_APPID"))             ? "" : rs.getString("MERCH4_APPID");
        this.merch5Id               = isBlank(rs.getString("MERCH5_ID"))                ? "" : rs.getString("MERCH5_ID");
        this.merch5Dba              = isBlank(rs.getString("MERCH5_DBA"))               ? "" : rs.getString("MERCH5_DBA");
        this.merch5Mcc              = isBlank(rs.getString("MERCH5_MCC"))               ? "" : rs.getString("MERCH5_MCC");
        this.merch5Vnum             = isBlank(rs.getString("MERCH5_VNUM"))              ? "" : rs.getString("MERCH5_VNUM");
        this.merch5AppId            = isBlank(rs.getString("MERCH5_APPID"))             ? "" : rs.getString("MERCH5_APPID");
        this.merch6Id               = isBlank(rs.getString("MERCH6_ID"))                ? "" : rs.getString("MERCH6_ID");
        this.merch6Dba              = isBlank(rs.getString("MERCH6_DBA"))               ? "" : rs.getString("MERCH6_DBA");
        this.merch6Mcc              = isBlank(rs.getString("MERCH6_MCC"))               ? "" : rs.getString("MERCH6_MCC");
        this.merch6Vnum             = isBlank(rs.getString("MERCH6_VNUM"))              ? "" : rs.getString("MERCH6_VNUM");
        this.merch6AppId            = isBlank(rs.getString("MERCH6_APPID"))             ? "" : rs.getString("MERCH6_APPID");
        this.visaAccepted           = isBlank(rs.getString("VISA_ACCEPTED"))            ? "" : rs.getString("VISA_ACCEPTED");
        this.amexAccepted           = isBlank(rs.getString("AMEX_ACCEPTED"))            ? "" : rs.getString("AMEX_ACCEPTED");
        this.discAccepted           = isBlank(rs.getString("DISC_ACCEPTED"))            ? "" : rs.getString("DISC_ACCEPTED");
        this.debitAccepted          = isBlank(rs.getString("DEBIT_ACCEPTED"))           ? "" : rs.getString("DEBIT_ACCEPTED"); 
        this.callTagItem            = isBlank(rs.getString("CALL_TAG_ITEM"))            ? "" : rs.getString("CALL_TAG_ITEM");
        this.callTagItemSerialNum   = isBlank(rs.getString("CALL_TAG_ITEM_SERIAL_NUM")) ? "" : rs.getString("CALL_TAG_ITEM_SERIAL_NUM");
        this.specialComment         = isBlank(rs.getString("SPECIAL_COMMENT"))          ? "" : rs.getString("SPECIAL_COMMENT");
        this.largePlate             = isBlank(rs.getString("LARGE_PLATE_CHECK"))        ? "" : rs.getString("LARGE_PLATE_CHECK");
        this.smallPlate             = isBlank(rs.getString("SMALL_PLATE_CHECK"))        ? "" : rs.getString("SMALL_PLATE_CHECK");
        this.imprinter              = isBlank(rs.getString("IMPRINTER_CHECK"))          ? "" : rs.getString("IMPRINTER_CHECK");
        this.accessCode             = isBlank(rs.getString("ACCESS_CODE_CHECK"))        ? "" : rs.getString("ACCESS_CODE_CHECK");
        this.accessCodeDesc         = isBlank(rs.getString("ACCESS_CODE_DESC"))         ? "" : rs.getString("ACCESS_CODE_DESC");
        this.terminalStatus         = isBlank(rs.getString("TERMINAL_STATUS"))          ? "" : rs.getString("TERMINAL_STATUS");
        this.printerStatus          = isBlank(rs.getString("PRINTER_STATUS"))           ? "" : rs.getString("PRINTER_STATUS");
        this.pinpadStatus           = isBlank(rs.getString("PINPAD_STATUS"))            ? "" : rs.getString("PINPAD_STATUS");
        this.coverSheetPrinted      = isBlank(rs.getString("COVERSHEET_PRINTED"))       ? "" : DateTimeFormatter.getFormattedDate(rs.getTimestamp("COVERSHEET_PRINTED"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
        this.pricingSchedulePrinted = isBlank(rs.getString("PRICINGSCHEDULE_PRINTED"))  ? "" : DateTimeFormatter.getFormattedDate(rs.getTimestamp("PRICINGSCHEDULE_PRINTED"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
        this.packingSlipSubmitted   = true;

        //get equipment info
        getPackingSlipEquipment(primaryKey);

      }
      else //if nothing exists in packing slip table, we get defaults from application
      {
        getMerchantCardsAccepted(primaryKey);
        getEquipmentInfo        (primaryKey);
        getMiscEquipmentInfo    (primaryKey);
        getEquipmentStatus      (primaryKey);
        getShippingAddress      (primaryKey);
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPackingSlipInfo: " + e.toString());
      addError("getPackingSlipInfo: " + e.toString());
    }
  
  }


  private void setDiscoverNumber(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select                     ");
      qs.append("MERCHPO_CARD_MERCH_NUMBER  "); 
      qs.append("from                       ");
      qs.append("merchpayoption             ");
      qs.append("where                      ");
      qs.append("app_seq_num = ? and        ");
      qs.append("CARDTYPE_CODE = ?          ");

      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      ps.setInt(2, mesConstants.APP_CT_DISCOVER);

      rs = ps.executeQuery();
    
      if(rs.next())
      {
        if(rs.getString("merchpo_card_merch_number").equals("0"))
        {
          this.discoverNumber = "Not Yet Assigned";
        }
        else
        {
          this.discoverNumber = formatDiscoverNumber(rs.getString("merchpo_card_merch_number"));
        }
      }
     
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setDiscoverNumber: " + e.toString());
      addError("setDiscoverNumber: " + e.toString());
    }
  }

  private String formatDiscoverNumber(String discnum)
  {
    String result = discnum.trim();

    if(discnum.length() == 15)
    {
      String part1 = discnum.substring(0,5);
      String part2 = discnum.substring(5,9);
      String part3 = discnum.substring(9,12);
      String part4 = discnum.substring(12);
      result = part1 + " - " + part2 + " - " + part3 + " - " + part4;
    }
    return result;
  }

  private void getPackingSlipEquipment(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select * "); 
      qs.append("from packing_slip_equipment  ");
      qs.append("where app_seq_num = ?        ");
      qs.append("order by place_holder        ");

      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
    
      while(rs.next())
      {
        TerminalInfo ti = new TerminalInfo();
        
        ti.setVnumber(            isBlank(rs.getString("vnumber"))              ? "" : rs.getString("vnumber"));
        ti.setTermApplication(    isBlank(rs.getString("term_application"))     ? "" : rs.getString("term_application"));
        ti.setTerminalModel(      isBlank(rs.getString("terminal_model"))       ? "" : rs.getString("terminal_model"));
        ti.setTerminalRefurb(     isBlank(rs.getString("terminal_refurb"))      ? "" : rs.getString("terminal_refurb"));
        ti.setPrinterModel(       isBlank(rs.getString("printer_model"))        ? "" : rs.getString("printer_model"));
        ti.setPrinterRefurb(      isBlank(rs.getString("printer_refurb"))       ? "" : rs.getString("printer_refurb"));
        ti.setPinpadModel(        isBlank(rs.getString("pinpad_model"))         ? "" : rs.getString("pinpad_model"));
        ti.setPinpadRefurb(       isBlank(rs.getString("pinpad_refurb"))        ? "" : rs.getString("pinpad_refurb"));
        ti.setTerminalSerialNum(  isBlank(rs.getString("terminal_serial_num"))  ? "" : rs.getString("terminal_serial_num"));
        ti.setPrinterSerialNum(   isBlank(rs.getString("printer_serial_num"))   ? "" : rs.getString("printer_serial_num"));
        ti.setPinpadSerialNum(    isBlank(rs.getString("pinpad_serial_num"))    ? "" : rs.getString("pinpad_serial_num"));
 
        terminals.add(ti);
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPackingSlipEquipment: " + e.toString());
      addError("getPackingSlipEquipment: " + e.toString());
    }
  }


  private void getShippingAddress(long appSeqNum)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {

      qs.append("select                     ");
      qs.append("address_name,              ");
      qs.append("address_phone,             ");
      qs.append("address_line1,             ");
      qs.append("address_line2,             ");
      qs.append("address_city,              ");
      qs.append("countrystate_code,         ");
      qs.append("address_zip                ");
      qs.append("from address               ");
      qs.append("where app_seq_num = ? and  ");
      qs.append("addresstype_code = ?       ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);
      ps.setInt(2, mesConstants.ADDR_TYPE_SHIPPING);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.shippingName       = isBlank(rs.getString("address_name"))       ? "" : rs.getString("address_name");
        this.shippingAddress1   = isBlank(rs.getString("address_line1"))      ? "" : rs.getString("address_line1");
        this.shippingAddress2   = isBlank(rs.getString("address_line2"))      ? "" : rs.getString("address_line2");
        this.shippingCity       = isBlank(rs.getString("address_city"))       ? "" : rs.getString("address_city");
        this.shippingState      = isBlank(rs.getString("countrystate_code"))  ? "" : rs.getString("countrystate_code");
        this.shippingZip        = isBlank(rs.getString("address_zip"))        ? "" : rs.getString("address_zip");
        this.shippingPhone      = isBlank(rs.getString("address_phone"))      ? "" : rs.getString("address_phone");
        //found shipping address so we use it.. set box so that shipping address different than merchant address
        this.shipAddDifCheckBox = "Y";
      }

      qs.setLength(0);
      qs.append("select                 ");
      qs.append("shipping_contact_name, ");
      qs.append("shipping_method        ");
      qs.append("from transcom_merchant ");
      qs.append("where app_seq_num = ?  ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.shippingContactName  = isBlank(rs.getString("shipping_contact_name"))  ? "" : rs.getString("shipping_contact_name");
        //this.shippingMethod       = isBlank(rs.getString("shipping_method"))        ? "" : rs.getString("shipping_method");
      }

      qs.setLength(0);
      qs.append("select                 ");
      qs.append("shipping_contact_name, ");
      qs.append("shipping_method,       ");
      qs.append("shipping_comment       ");
      qs.append("from pos_features      ");
      qs.append("where app_seq_num = ?  ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.shippingContactName  = isBlank(rs.getString("shipping_contact_name"))  ? "" : rs.getString("shipping_contact_name");
        //this.shippingMethod       = isBlank(rs.getString("shipping_method"))        ? "" : rs.getString("shipping_method");
      }


    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getShippingAddress: " + e.toString());
      addError("getShippingAddress: " + e.toString());
    }
  }


  private void getMiscEquipmentInfo(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      
      qs.setLength(0);
      qs.append("select * ");
      qs.append("from merchequipment ");
      qs.append("where app_seq_num = ? and equiptype_code = ? ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, primaryKey);
      ps.setInt(2,  mesConstants.APP_EQUIP_TYPE_IMPRINTER);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.imprinter = "Y";
      }

      rs.close();
      ps.close();


      //check to see if any equipment is being bought, rented, leased. blah blah blah.. anything other than owned
      qs.setLength(0);
      qs.append("select * ");
      qs.append("from merchequipment ");
      qs.append("where app_seq_num = ? and equiplendtype_code != ? ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, primaryKey);
      ps.setInt(2,  mesConstants.APP_EQUIP_OWNED);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.shipEquipmentCheckBox = "Y";
      }

      rs.close();
      ps.close();

      //check to see if access code is being used
      qs.setLength(0);
      qs.append("select            ");
      qs.append("access_code_flag, ");
      qs.append("access_code       ");
      qs.append("from              ");
      qs.append("pos_features      ");
      qs.append("where             ");
      qs.append("app_seq_num = ?   ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();

      if(rs.next())
      {
        this.accessCode     = isBlank(rs.getString("access_code_flag")) ? "" : rs.getString("access_code_flag");
        this.accessCodeDesc = isBlank(rs.getString("access_code"))      ? "" : rs.getString("access_code");
      }

      rs.close();
      ps.close();


    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMiscEquipmentInfo: " + e.toString());
      addError("getMiscEquipmentInfo: " + e.toString());
    }
  }


  private void getMasterInfo(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select v_number,app_code "); 
      qs.append("from v_numbers           ");
      qs.append("where app_seq_num = ?    ");
      qs.append("and model_index = 1      ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
    
      if(rs.next())
      {
        this.masterVnum   = isBlank(rs.getString("v_number")) ? "" : rs.getString("v_number"); 
        this.masterAppId  = isBlank(rs.getString("app_code")) ? "" : rs.getString("app_code");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMasterInfo: " + e.toString());
      addError("getMasterInfo: " + e.toString());
    }
  }


  private void getEquipmentInfo(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select                       ");
      qs.append("vn.v_number,                 ");
      qs.append("vn.app_code,                 ");
      qs.append("vn.equip_model,              "); 
      qs.append("msi.equip_refurb             ");
      qs.append("from v_numbers vn,           ");
      qs.append("mms_stage_info msi           ");
      qs.append("where vn.app_seq_num = ?     ");
      qs.append("and vn.app_seq_num = msi.app_seq_num ");
      qs.append("and vn.v_number = msi.vnum   ");
      qs.append("and vn.pos_code = 101        ");
      qs.append("and vn.v_number is not null  ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
    
      while(rs.next())
      {
        TerminalInfo ti = new TerminalInfo();
        
        ti.setVnumber         ( isBlank(rs.getString("v_number"))     ? "" : rs.getString("v_number")     );
        ti.setTermApplication ( isBlank(rs.getString("app_code"))     ? "" : rs.getString("app_code")     );
        ti.setTerminalModel   ( isBlank(rs.getString("equip_model"))  ? "" : rs.getString("equip_model")  );
        ti.setTerminalRefurb  ( isBlank(rs.getString("equip_refurb")) ? "" : rs.getString("equip_refurb")  );

        terminals.add(ti);
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMasterInfo: " + e.toString());
      addError("getMasterInfo: " + e.toString());
    }
  }

  private void getEquipmentStatus(long primaryKey)
  {

    try
    {
      StringBuffer      qs      = new StringBuffer("");
      PreparedStatement ps      = null;
      ResultSet         rs      = null;
    
      qs.append("select                         ");
      qs.append("*                              "); 
      qs.append("from                           ");
      qs.append("merchequipment                 ");
      qs.append("where                          ");
      qs.append("app_seq_num = ? and            ");
      qs.append("equiptype_code in (?,?,?,?,?)  ");

      ps = getPreparedStatement(qs.toString());

      ps.setLong(1,primaryKey);
      ps.setInt(2, mesConstants.APP_EQUIP_TYPE_PRINTER);
      ps.setInt(3, mesConstants.APP_EQUIP_TYPE_PINPAD);
      ps.setInt(4, mesConstants.APP_EQUIP_TYPE_TERMINAL);
      ps.setInt(5, mesConstants.APP_EQUIP_TYPE_TERM_PRINTER);
      ps.setInt(6, mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD);

      rs = ps.executeQuery();
    
      while(rs.next())
      {
        switch(rs.getInt("equiptype_code"))
        {
          case mesConstants.APP_EQUIP_TYPE_PRINTER:
            printerStatus = rs.getString("equiplendtype_code");
          break;
          
          case mesConstants.APP_EQUIP_TYPE_PINPAD:
            pinpadStatus  = rs.getString("equiplendtype_code");
          break;

          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
            terminalStatus = rs.getString("equiplendtype_code");
          break;
        }
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipmentStatus: " + e.toString());
      addError("getEquipmentStatus: " + e.toString());
    }
  
  }

  private void getMmsStageInfo(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select app_queue_stage "); 
      qs.append("from app_queue         ");
      qs.append("where app_seq_num = ?  ");
      qs.append("and app_queue_type = ? ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      ps.setInt(2,  QueueConstants.QUEUE_MMS);

      rs = ps.executeQuery();
    
      if(rs.next())
      {
        this.currentStage     = rs.getInt("app_queue_stage"); 
        this.currentStageDesc = QueueConstants.getStageDescription(this.currentStage);
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMmsStageInfo: " + e.toString());
      addError("getMmsStageInfo: " + e.toString());
    }
  }

  private void moveToCompletedStage(long appSeqNum, UserBean user)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;

    try
    {
      //moves request out of programming new queue.. 
      //into programming completed queue
      qs.append("update temp_activation_queue "); 
      qs.append("set   q_stage      = ?       ");
      qs.append("where app_seq_num  = ?       ");
      qs.append("and   q_type       = ?       ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setInt(1,  QueueConstants.Q_PROGRAMMING_COMPLETE);
      ps.setLong(2, appSeqNum);
      ps.setInt(3,  QueueConstants.QUEUE_PROGRAMMING);

      ps.executeUpdate();

      for(int v=0; v<vNums.size(); v++)
      {
        long reqid = getRequestIdFromVnum((String)vNums.elementAt(v));
        QueueTools.moveQueueItem(reqid, QueueTools.getCurrentQueueType(reqid, MesQueues.Q_ITEM_TYPE_PROGRAMMING), MesQueues.Q_PROGRAMMING_COMPLETE, user, "Packing Slip Created");
      }

      moveOutOfMmsQueue(appSeqNum, user);

      addToDepQueues(appSeqNum, user);

      //will only add to activation queues if mes is to do training.
//      if(addToActivation(appSeqNum))
//      {
//        addToActQueues(appSeqNum, user);
//      }

      putInEquipmentQueue(user.getLoginName(), appSeqNum);
    }
    catch(Exception e)
    {
      logEntry("moveToCompletedStage()", e.toString());
    }
  }

  private long getRequestIdFromVnum(String vnumber)
  {
    StringBuffer        qs      = new StringBuffer("");
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    long                result  = -1;

    try
    {
      
      qs.append("select request_id    ");
      qs.append("from  mms_stage_info ");
      qs.append("where vnum = ?       ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setString(1,  vnumber);

      rs = ps.executeQuery();

      if(rs.next())
      {
        result = rs.getLong("request_id");
      }
    }
    catch(Exception e)
    {
      logEntry("getRequestIdFromVnum()", e.toString());
    }

    return result;

  }

  private void moveOutOfMmsQueue(long appSeqNum, UserBean user)
  {
    StringBuffer        qs      = new StringBuffer("");
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;

    try
    {
      
      //check to see if all requests came back successfully
      //moves request out of programming queue.. into completed queue
      qs.append("select *                       ");
      qs.append("from  mms_stage_info           ");
      qs.append("where app_seq_num = ? and      ");
      qs.append("process_response != 'success'  ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,  appSeqNum);

      rs = ps.executeQuery();

      if(!rs.next())
      {

        //if all requests have come back with success status.. then we move item out of mms queue.. 
        //this is clean up of previous step
        qs.setLength(0);
        qs.append("update app_queue               ");
        qs.append("set    app_queue_stage = ?     ");
        qs.append("where  app_seq_num     = ?     ");
        qs.append("and    app_queue_type  = ?     ");
      
        ps = getPreparedStatement(qs.toString());
        ps.setInt(1,   QueueConstants.Q_MMS_COMPLETED);
        ps.setLong(2,  appSeqNum);
        ps.setInt(3,   QueueConstants.QUEUE_MMS);

        ps.executeUpdate();

        //moves from which ever type of mms queue its currently in.. 
        //to the completed mms queue
        QueueTools.moveQueueItem(appSeqNum, QueueTools.getCurrentQueueType(appSeqNum, MesQueues.Q_ITEM_TYPE_MMS), MesQueues.Q_MMS_COMPLETE, user, null);


        //then we time stamp mms completed on account status page
        qs.setLength(0);
        qs.append("update app_tracking            ");
        qs.append("set date_completed = sysdate,  ");
        qs.append("status_code        = ?,        ");
        qs.append("contact_name       = ?         ");
        qs.append("where app_seq_num  = ?         ");
        qs.append("and dept_code      = ?         ");
        qs.append("and date_completed is null     ");
      
        ps = getPreparedStatement(qs.toString());
        ps.setInt(1,   302);
        ps.setString(2, user.getLoginName());
        ps.setLong(3,  appSeqNum);
        ps.setInt(4,   QueueConstants.DEPARTMENT_MMS);

        ps.executeUpdate();
      }
    }
    catch(Exception e)
    {
      logEntry("moveOutOfMmsQueue()", e.toString());
    }
  }

  private void addToDepQueues(long appSeqNum, UserBean user)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;

    try
    {
      
      //add to deployment
      if(!QueueTools.hasItemEnteredQueue(appSeqNum, MesQueues.Q_ITEM_TYPE_DEPLOYMENT))
      {
        QueueTools.insertQueue(appSeqNum, MesQueues.Q_DEPLOYMENT_NEW, user);
      }
      
      qs.append("insert into temp_activation_queue (  "); 
      qs.append("q_type,                              ");
      qs.append("q_stage,                             ");
      qs.append("request_id,                          ");
      qs.append("date_in_queue,                       ");
      qs.append("app_seq_num                          ");
      qs.append(") values (?,?,?,sysdate,?)           ");
      
      ps = getPreparedStatement(qs.toString());
 
      //put deployment entry into queue
      ps.setInt(1, QueueConstants.QUEUE_DEPLOYMENT);
      ps.setInt(2, QueueConstants.Q_DEPLOYMENT_NEW);
      ps.setInt(3, -1);
      ps.setLong(4, appSeqNum);

      if(!alreadExists(appSeqNum, QueueConstants.QUEUE_DEPLOYMENT))
      {
        ps.executeUpdate();
      }

      ps.close();

      //then we time stamp activation and deployment as received on account status page
      qs.setLength(0);
      qs.append("update app_tracking            ");
      qs.append("set date_received  = sysdate,  ");
      qs.append("status_code        = ?         ");
      qs.append("where app_seq_num  = ?         ");
      qs.append("and dept_code      = ?         ");
      qs.append("and date_received is null      ");
    
      ps = getPreparedStatement(qs.toString());
      
      //stamp deployment 
      ps.setInt(1,   501); //received status
      ps.setLong(2,  appSeqNum);
      ps.setInt(3,   QueueConstants.DEPARTMENT_DEPLOYMENT);
      ps.executeUpdate();

      ps.close();
    }
    catch(Exception e)
    {
      logEntry("addToDepQueues()", e.toString());
    }
  }

  private void addToActQueues(long appSeqNum, UserBean user)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;

    try
    {
      
      //add to activation
      if(!QueueTools.hasItemEnteredQueue(appSeqNum, MesQueues.Q_ITEM_TYPE_ACTIVATION))
      {
        QueueTools.insertQueue(appSeqNum, MesQueues.Q_ACTIVATION_NEW, user);
      }

      qs.append("insert into temp_activation_queue (  "); 
      qs.append("q_type,                              ");
      qs.append("q_stage,                             ");
      qs.append("request_id,                          ");
      qs.append("date_in_queue,                       ");
      qs.append("app_seq_num                          ");
      qs.append(") values (?,?,?,sysdate,?)           ");
      
      ps = getPreparedStatement(qs.toString());

      //put activation entry into queue
      ps.setInt(1, QueueConstants.QUEUE_ACTIVATION);
      ps.setInt(2, QueueConstants.Q_ACTIVATION_NEW);
      ps.setInt(3, -1);
      ps.setLong(4, appSeqNum);

      if(!alreadExists(appSeqNum, QueueConstants.QUEUE_ACTIVATION))
      {
        ps.executeUpdate();
      }

      ps.close();

      //then we time stamp activation and deployment as received on account status page
      qs.setLength(0);
      qs.append("update app_tracking            ");
      qs.append("set date_received  = sysdate,  ");
      qs.append("status_code        = ?         ");
      qs.append("where app_seq_num  = ?         ");
      qs.append("and dept_code      = ?         ");
      qs.append("and date_received is null      ");
    
      ps = getPreparedStatement(qs.toString());
      
      //stamp activation
      ps.setInt(1,   601); //received status
      ps.setLong(2,  appSeqNum);
      ps.setInt(3,   QueueConstants.DEPARTMENT_ACTIVATION);
      ps.executeUpdate();

      ps.close();
    }
    catch(Exception e)
    {
      logEntry("addToActQueues()", e.toString());
    }
  }


  public void putInEquipmentQueue(String loginName, long primaryKey)
  {
    StringBuffer      qs          = new StringBuffer();
    PreparedStatement ps          = null;
    ResultSet         rs          = null;
    Date              startDate   = null;
    String            appSource   = "";
    long              appUser     = 0;

    try 
    {

      //check to see if it has been added to equipment queue already
      qs.setLength(0);
      qs.append("select app_seq_num       ");
      qs.append("from app_queue           ");
      qs.append("where app_seq_num  = ?   ");
      qs.append("and app_queue_type = ?   ");

      ps = getPreparedStatement(qs.toString());
     
      ps.setLong(1, primaryKey);
      ps.setInt(2,  QueueConstants.QUEUE_EQUIPMENT);

      rs = ps.executeQuery();
      
      if(rs.next())
      {
        rs.close();
        ps.close();
    
        //dont try to add to database since it already exists
        return;
      }

      //if not then we add

      //add to equipment queue
      //QueueTools.insertQueue(primaryKey, MesQueues., user);


      //get some info
      qs.setLength(0);
      qs.append("select app_start_date,   ");
      qs.append("app_user,                ");
      qs.append("app_source               ");
      qs.append("from app_queue           ");
      qs.append("where app_seq_num = ?    ");
      qs.append("order by app_start_date  ");
   
      ps = getPreparedStatement(qs.toString());
     
      ps.setLong(1,primaryKey);
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        appSource = rs.getString("app_source");  
        appUser   = rs.getLong  ("app_user");
        startDate = rs.getDate  ("app_start_date");
      }
      rs.close();
      ps.close();

      qs.setLength(0);
      qs.append("insert into app_queue (  ");
      qs.append("app_seq_num,             ");
      qs.append("app_queue_type,          ");
      qs.append("app_queue_stage,         ");
      qs.append("app_status,              ");
      qs.append("app_user,                ");
      qs.append("app_start_date,          ");
      qs.append("app_source,              ");
      qs.append("app_last_date,           ");
      qs.append("app_last_user) values (?,?,?,?,?,sysdate,?,sysdate,?) ");
    
      ps = getPreparedStatement(qs.toString());
     
      ps.setLong  (1, primaryKey);
      ps.setInt   (2, QueueConstants.QUEUE_EQUIPMENT );
      ps.setInt   (3, QueueConstants.Q_EQUIPMENT_NEW );
      ps.setInt   (4, QueueConstants.Q_STATUS_APPROVED );
      ps.setLong  (5, appUser);
      
      //use sysdate for app_start_date instead of original start date.. 
      //we want time it entered queue to be start date
      //ps.setDate  (6, new java.sql.Date(startDate.getTime()));
      
      ps.setString(6, appSource);
      ps.setString(7, "SYSTEM");

      ps.executeUpdate();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "putInEquipmentQueue: " + e.toString());
      addError("putInEquipmentQueue: " + e.toString());
    }
  }


  public long getPrimaryKeyFromReqId(long reqId)
  {
    StringBuffer        qs      = new StringBuffer("");
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    long                result  = 0L;;

    try
    {
      
      //get primaryKey from request_id
      qs.append("select app_seq_num      ");
      qs.append("from  mms_stage_info    ");
      qs.append("where request_id = ?    ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, reqId);

      rs = ps.executeQuery();

      if(rs.next())
      {
        result = rs.getLong("app_seq_num");
      }
    }
    catch(Exception e)
    {
      logEntry("getPrimaryKeyFromReqId()", e.toString());
    }

    return result;

  }



  private boolean alreadExists(long appSeqNum, int queue)
  {
    StringBuffer        qs      = new StringBuffer("");
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {
      
      //check to see if all requests came back successfully
      //moves request out of programming queue.. into completed queue
      qs.append("select *                       ");
      qs.append("from  temp_activation_queue    ");
      qs.append("where app_seq_num = ? and      ");
      qs.append("q_type = ?                     ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      ps.setInt(2,  queue);

      rs = ps.executeQuery();

      if(rs.next())
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("alreadExists()", e.toString());
    }

    return result;

  }

/*
   they have to actively specify that training is to be done by someone 
   other than mes.. if they leave it blank.. then it goes into activation..
*/
  private boolean addToActivation(long appSeqNum)
  {
    StringBuffer        qs      = new StringBuffer("");
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {
      
      //check transcom merchant first
      qs.append("select phone_training    ");
      qs.append("from   transcom_merchant ");
      qs.append("where  app_seq_num = ?   ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(!isBlank(rs.getString("phone_training")) && rs.getString("phone_training").equals("M"))
        {
          result = true;
        }
      }

      //check pos_features table
      qs.setLength(0);
      qs.append("select phone_training    ");
      qs.append("from   pos_features      ");
      qs.append("where  app_seq_num = ?   ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(isBlank(rs.getString("phone_training")) || rs.getString("phone_training").equals("M"))
        {
          result = true;
        }
      }

    }
    catch(Exception e)
    {
      logEntry("addToActivation()", e.toString());
    }

    return result;

  }


  public boolean appApproved(long appSeqNum)
  {
    StringBuffer        qs      = new StringBuffer("");
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {
      //check to see if merch_credit_status == approved
      qs.append("select merch_credit_status ");
      qs.append("from  merchant             ");
      qs.append("where app_seq_num = ?      ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(rs.getInt("merch_credit_status") == QueueConstants.CREDIT_APPROVE)
        {
          result = true;
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      logEntry("appApproved()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }

    return result;

  }



  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }
  
  public String formatPhone(String fone)
  {
    String result = "";
    try
    {
      if(fone.length() == 10)
      {
        result = "(" + fone.substring(0,3) + ")" + " " + fone.substring(3,6) + "-" + fone.substring(6);
      }
      else
      {
        result = fone;
      }
    }
    catch(Exception e)
    {
      result = fone;
    }
    
    return result;
  }


  public void getStaticData(long primaryKey)
  {
    getMerchantGeneralInfo  (primaryKey);
    getMmsStageInfo         (primaryKey);
    getPhysicalAddress      (primaryKey);
    getMasterInfo           (primaryKey);
    setDiscoverNumber       (primaryKey);
//    getNewConversionAcct    (primaryKey);
  }

  //only get these when page is not submitted
  //not only that.. but we check to make sure info doesnt
  //exist in another table.. the main table for this page..
  public void getDynamicData(long primaryKey)
  {
    getPackingSlipInfo(primaryKey);
  }
  
  public void submitData(long primaryKey, UserBean user)
  {
    submitPackingSlipInfo(primaryKey);
    submitPackingSlipEquipment(primaryKey);
    moveToCompletedStage(primaryKey, user);
  }

  private void submitPackingSlipInfo(long primaryKey)
  {
  
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {

      qs.append("select app_seq_num from PACKING_SLIP_INFO where app_seq_num = ? ");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1,primaryKey);
      rs = ps.executeQuery();
      qs.setLength(0);

      if(rs.next())
      {
          qs.append("update PACKING_SLIP_INFO set   ");
          qs.append("SHIPPING_METHOD           = ?, ");
          qs.append("TIME_ZONE                 = ?, ");
          qs.append("RUSH_AUTHORIZATION        = ?, ");
          qs.append("MERCHANT_TYPE             = ?, ");
          qs.append("DIALPAY_TYPE              = ?, ");
          qs.append("SHIP_EQUIPMENT_CHECK      = ?, ");
          qs.append("CALL_TAG_CHECK            = ?, ");
          qs.append("SHIP_ADD_DIF_CHECK        = ?, ");
          qs.append("SHIP_ADD_DIF              = ?, ");
          
          qs.append("SHIP_NAME                 = ?, ");         
          qs.append("SHIP_ADDRESS_LINE1        = ?, ");
          qs.append("SHIP_ADDRESS_LINE2        = ?, ");
          qs.append("SHIP_CITY                 = ?, ");
          qs.append("SHIP_STATE                = ?, ");
          qs.append("SHIP_ZIP                  = ?, ");
          qs.append("SHIP_PHONE                = ?, ");
          qs.append("SHIP_CONTACT_NAME         = ?, ");

          qs.append("MERCH2_ID                 = ?, ");
          qs.append("MERCH2_DBA                = ?, ");
          qs.append("MERCH2_MCC                = ?, ");
          qs.append("MERCH2_VNUM               = ?, ");
          qs.append("MERCH2_APPID              = ?, ");
          qs.append("MERCH3_ID                 = ?, ");
          qs.append("MERCH3_DBA                = ?, ");
          qs.append("MERCH3_MCC                = ?, ");
          qs.append("MERCH3_VNUM               = ?, ");
          qs.append("MERCH3_APPID              = ?, ");
          qs.append("MERCH4_ID                 = ?, ");
          qs.append("MERCH4_DBA                = ?, ");
          qs.append("MERCH4_MCC                = ?, ");
          qs.append("MERCH4_VNUM               = ?, ");
          qs.append("MERCH4_APPID              = ?, ");
          qs.append("MERCH5_ID                 = ?, ");
          qs.append("MERCH5_DBA                = ?, ");
          qs.append("MERCH5_MCC                = ?, ");
          qs.append("MERCH5_VNUM               = ?, ");
          qs.append("MERCH5_APPID              = ?, ");
          qs.append("MERCH6_ID                 = ?, ");
          qs.append("MERCH6_DBA                = ?, ");
          qs.append("MERCH6_MCC                = ?, ");
          qs.append("MERCH6_VNUM               = ?, ");
          qs.append("MERCH6_APPID              = ?, ");
          qs.append("VISA_ACCEPTED             = ?, ");
          qs.append("AMEX_ACCEPTED             = ?, ");   
          qs.append("DISC_ACCEPTED             = ?, ");
          qs.append("DEBIT_ACCEPTED            = ?, ");
          qs.append("CALL_TAG_ITEM             = ?, ");
          qs.append("CALL_TAG_ITEM_SERIAL_NUM  = ?, ");
          qs.append("SPECIAL_COMMENT           = ?, ");
          qs.append("LARGE_PLATE_CHECK         = ?, ");
          qs.append("SMALL_PLATE_CHECK         = ?, ");
          qs.append("IMPRINTER_CHECK           = ?, ");
          qs.append("ACCESS_CODE_CHECK         = ?, ");
          qs.append("ACCESS_CODE_DESC          = ?, ");
          qs.append("TERMINAL_STATUS           = ?, ");
          qs.append("PRINTER_STATUS            = ?, ");
          qs.append("PINPAD_STATUS             = ?, ");
          qs.append("SLIP_TYPE                 = ?  ");
          qs.append("where app_seq_num         = ?  ");
      }
      else
      {
          qs.append("insert into PACKING_SLIP_INFO (");
          qs.append("SHIPPING_METHOD,               ");
          qs.append("TIME_ZONE,                     ");
          qs.append("RUSH_AUTHORIZATION,            ");
          qs.append("MERCHANT_TYPE,                 ");
          qs.append("DIALPAY_TYPE,                  ");
          qs.append("SHIP_EQUIPMENT_CHECK,          ");
          qs.append("CALL_TAG_CHECK,                ");
          qs.append("SHIP_ADD_DIF_CHECK,            ");
          qs.append("SHIP_ADD_DIF,                  ");
          qs.append("SHIP_NAME,                     ");         
          qs.append("SHIP_ADDRESS_LINE1,            ");
          qs.append("SHIP_ADDRESS_LINE2,            ");
          qs.append("SHIP_CITY,                     ");
          qs.append("SHIP_STATE,                    ");
          qs.append("SHIP_ZIP,                      ");
          qs.append("SHIP_PHONE,                    ");
          qs.append("SHIP_CONTACT_NAME,             ");
          qs.append("MERCH2_ID,                     ");
          qs.append("MERCH2_DBA,                    ");
          qs.append("MERCH2_MCC,                    ");
          qs.append("MERCH2_VNUM,                   ");
          qs.append("MERCH2_APPID,                  ");
          qs.append("MERCH3_ID,                     ");
          qs.append("MERCH3_DBA,                    ");
          qs.append("MERCH3_MCC,                    ");
          qs.append("MERCH3_VNUM,                   ");
          qs.append("MERCH3_APPID,                  ");
          qs.append("MERCH4_ID,                     ");
          qs.append("MERCH4_DBA,                    ");
          qs.append("MERCH4_MCC,                    ");
          qs.append("MERCH4_VNUM,                   ");
          qs.append("MERCH4_APPID,                  ");
          qs.append("MERCH5_ID,                     ");
          qs.append("MERCH5_DBA,                    ");
          qs.append("MERCH5_MCC,                    ");
          qs.append("MERCH5_VNUM,                   ");
          qs.append("MERCH5_APPID,                  ");
          qs.append("MERCH6_ID,                     ");
          qs.append("MERCH6_DBA,                    ");
          qs.append("MERCH6_MCC,                    ");
          qs.append("MERCH6_VNUM,                   ");
          qs.append("MERCH6_APPID,                  ");
          qs.append("VISA_ACCEPTED,                 ");
          qs.append("AMEX_ACCEPTED,                 ");   
          qs.append("DISC_ACCEPTED,                 ");
          qs.append("DEBIT_ACCEPTED,                ");
          qs.append("CALL_TAG_ITEM,                 ");
          qs.append("CALL_TAG_ITEM_SERIAL_NUM,      ");
          qs.append("SPECIAL_COMMENT,               ");
          qs.append("LARGE_PLATE_CHECK,             ");
          qs.append("SMALL_PLATE_CHECK,             ");
          qs.append("IMPRINTER_CHECK,               ");
          qs.append("ACCESS_CODE_CHECK,             ");
          qs.append("ACCESS_CODE_DESC,              ");
          qs.append("TERMINAL_STATUS,               ");
          qs.append("PRINTER_STATUS,                ");
          qs.append("PINPAD_STATUS,                 ");
          qs.append("SLIP_TYPE,                     ");
          qs.append("app_seq_num)                   ");
          qs.append("values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      }
      rs.close();
      ps.close();

      ps = getPreparedStatement(qs.toString());

      if(slipType.equals(SLIP_TYPE_SINGLE))
      {
        ps.setString(1, this.shippingMethodSingle     );
        ps.setString(2, this.timeZoneSingle           );
        ps.setString(3, this.rushAuthorizationSingle  );
      }
      else
      {
        ps.setString(1, this.shippingMethodMulti      );
        ps.setString(2, this.timeZoneMulti            );
        ps.setString(3, this.rushAuthorizationMulti   );
      }

      
      ps.setString(4, this.merchantType         );
      ps.setString(5, this.dialPayType          );
      ps.setString(6, this.shipEquipmentCheckBox);
      ps.setString(7, this.callTagCheckBox      );
      ps.setString(8, this.shipAddDifCheckBox   );
      ps.setString(9, this.shipAddDif           );
      ps.setString(10, this.shippingName        );
      ps.setString(11, this.shippingAddress1    );
      ps.setString(12, this.shippingAddress2    );
      ps.setString(13, this.shippingCity        );
      ps.setString(14, this.shippingState       );
      ps.setString(15, this.shippingZip         );
      ps.setString(16, this.shippingPhone       );
      ps.setString(17, this.shippingContactName );
      ps.setString(18,this.merch2Id             );
      ps.setString(19,this.merch2Dba            );
      ps.setString(20,this.merch2Mcc            );
      ps.setString(21,this.merch2Vnum           );
      ps.setString(22,this.merch2AppId          );
      ps.setString(23,this.merch3Id             );
      ps.setString(24,this.merch3Dba            );
      ps.setString(25,this.merch3Mcc            );
      ps.setString(26,this.merch3Vnum           );
      ps.setString(27,this.merch3AppId          );
      ps.setString(28,this.merch4Id             );
      ps.setString(29,this.merch4Dba            );
      ps.setString(30,this.merch4Mcc            );
      ps.setString(31,this.merch4Vnum           );
      ps.setString(32,this.merch4AppId          );
      ps.setString(33,this.merch5Id             );
      ps.setString(34,this.merch5Dba            );
      ps.setString(35,this.merch5Mcc            );
      ps.setString(36,this.merch5Vnum           );
      ps.setString(37,this.merch5AppId          );
      ps.setString(38,this.merch6Id             );
      ps.setString(39,this.merch6Dba            );
      ps.setString(40,this.merch6Mcc            );
      ps.setString(41,this.merch6Vnum           );
      ps.setString(42,this.merch6AppId          );
      ps.setString(43,this.visaAccepted         );
      ps.setString(44,this.amexAccepted         );
      ps.setString(45,this.discAccepted         );
      ps.setString(46,this.debitAccepted        );
      ps.setString(47,this.callTagItem          );
      ps.setString(48,this.callTagItemSerialNum );
      ps.setString(49,this.specialComment       );
      ps.setString(50,this.largePlate           );
      ps.setString(51,this.smallPlate           );
      ps.setString(52,this.imprinter            );
      ps.setString(53,this.accessCode           );
      ps.setString(54,this.accessCodeDesc       );
      ps.setString(55,this.terminalStatus       );
      ps.setString(56,this.printerStatus        );
      ps.setString(57,this.pinpadStatus         );
      ps.setString(58,this.slipType             );
      ps.setLong(59,primaryKey);

      ps.executeUpdate();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitPackingSlipInfo: " + e.toString());
      addError("submitPackingSlipInfo: " + e.toString());
    }
  
  }

  private void submitPackingSlipEquipment(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      
      ps = getPreparedStatement("delete from PACKING_SLIP_EQUIPMENT where app_seq_num = ?");

      ps.setLong(1, primaryKey);

      ps.executeUpdate();
      
      qs.append("insert into packing_slip_equipment ( ");
      qs.append("app_seq_num,                         ");
      qs.append("place_holder,                        ");
      qs.append("vnumber,                             ");
      qs.append("term_application,                    ");
      qs.append("terminal_model,                      ");
      qs.append("terminal_refurb,                     ");
      qs.append("printer_model,                       ");
      qs.append("printer_refurb,                      ");
      qs.append("pinpad_model,                        ");
      qs.append("pinpad_refurb,                       ");
      qs.append("terminal_serial_num,                 ");
      qs.append("printer_serial_num,                  ");
      qs.append("pinpad_serial_num)                   ");
      qs.append("values (?,?,?,?,?,?,?,?,?,?,?,?,?)   "); 

      ps = getPreparedStatement(qs.toString());
          
      vNums.clear();

      for(int i=0; i < numOfTerminals; i++)
      {
        TerminalInfo ti = (TerminalInfo)terminals.elementAt(i);
        
        ps.setLong(1,     primaryKey);
        ps.setInt(2,      i);
        ps.setString(3,   ti.getVnumber());

        if(!(ti.getVnumber().trim().equals("")))
        {
          vNums.add((ti.getVnumber().trim()).toUpperCase());
        }

        ps.setString(4,   ti.getTermApplication());
        ps.setString(5,   ti.getTerminalModel());
        ps.setString(6,   ti.getTerminalRefurb());

        ps.setString(7,   ti.getPrinterModel());
        ps.setString(8,   ti.getPrinterRefurb());

        ps.setString(9,   ti.getPinpadModel());
        ps.setString(10,  ti.getPinpadRefurb());

        ps.setString(11,  ti.getTerminalSerialNum());
        ps.setString(12,  ti.getPrinterSerialNum());
        ps.setString(13,  ti.getPinpadSerialNum());
 
        ps.executeUpdate();
      }
      
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitPackingSlipEquipment: " + e.toString());
      addError("submitPackingSlipEquipment: " + e.toString());
    }
  }

  public boolean isMmsNewStage()
  {
    return (this.currentStage == QueueConstants.Q_MMS_NEW);
  }
  public boolean isMmsPendStage()
  {
    return (this.currentStage == QueueConstants.Q_MMS_PEND);
  }
  public boolean isMmsCompletedStage()
  {
    return (this.currentStage == QueueConstants.Q_MMS_COMPLETED);
  }
  
  public String getCurrentStageDesc()
  {
    return this.currentStageDesc;
  }  
  public int getCurrentStage()
  {
    return this.currentStage;
  }

  public void setDefaults(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setDefaults: " + e.toString());
      addError("setDefaults: " + e.toString());
    }
  }
  
  public String formatDate(Date raw)
  {
    String result   = "";
    if(raw == null)
    {
      return result;
    }
    try
    {
      DateFormat df   = new SimpleDateFormat("MM/dd/yy");
      result = df.format(raw);
      return result;
    }
    catch(Exception e)
    {
      System.out.println("date crashed = " + e.toString());
    }
    return result;
  }
  
    
  public boolean validate(long primaryKey)
  {

    if(!appApproved(primaryKey))
    {
      addError("This application has been declined, please do not continue further with it");
    }

    if(isBlank(this.slipType))
    {
      addError("Please select whether this is a single or multi merchant");      
    }

    if(isBlank(shippingPhone))
    {                        
      //dont do anything.. aint no biggie.. not required
    }
    else if (countDigits(shippingPhone) != 10)
    {
      addError("Shipping Phone number must be a valid 10-digit number including area code");
    }
    else //we get digits only, we reformat when we display it
    {
      shippingPhone = getDigitsOnly(shippingPhone);
    }

    return(! hasErrors());
  }

  public String getDigitsOnly(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");
    String        result      = "";

    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = digits.toString();
    }
    catch(Exception e)
    {}
    
    return result;
  }

  
  public int countDigits(String raw)
  {
    int result = 0;
    
    for(int i=0; i<raw.length(); ++i)
    {
      if(Character.isDigit(raw.charAt(i)))
      {
        ++result;
      }
    }
    
    return result;
  }


  public String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;
      
      index = body.indexOf(' ');
      
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
      System.out.println("error will robinson %20");
    }
    return body;

  }
  
  public void submitData(HttpServletRequest req, long primaryKey)
  {
  }
 
  public String getMcc()
  {
    return this.mcc;
  }

  public String getShipEquipmentCheckBox()
  {
    return this.shipEquipmentCheckBox;
  }
  public String getCallTagCheckBox()
  {
    return this.callTagCheckBox;
  }
  public String getShipAddDifCheckBox()
  {
    return this.shipAddDifCheckBox;
  }
  public String getShipAddDif()
  {
    return this.shipAddDif;
  }
  
  public String getShippingName()
  {
    return this.shippingName;
  }
  public String getShippingAddress1()
  {
    return this.shippingAddress1;
  }
  public String getShippingAddress2()
  {
    return this.shippingAddress2;
  }
  public String getShippingCity()
  {
    return this.shippingCity;
  }
  public String getShippingState()
  {
    return this.shippingState;
  }
  public String getShippingZip()
  {
    return this.shippingZip;
  }
  public String getShippingPhone()
  {
    return formatPhone(this.shippingPhone);
  }
  public String getShippingContactName()
  {
    return this.shippingContactName;
  }
  
  
  public String getMerch2Id()
  {
    return this.merch2Id;
  }
  public String getMerch2Dba()
  {
    return this.merch2Dba;
  }
  public String getMerch2Mcc()
  {
    return this.merch2Mcc;
  }
  public String getMerch2Vnum()
  {
    return this.merch2Vnum;
  }
  public String getMerch2AppId()
  {
    return this.merch2AppId;
  }

  public String getMasterVnum()
  {
    return this.masterVnum;
  }
  public String getMasterAppId()
  {
    return this.masterAppId;
  }




  public String getMerch3Id()
  {
    return this.merch3Id;
  }
  public String getMerch3Dba()
  {
    return this.merch3Dba;
  }
  public String getMerch3Mcc()
  {
    return this.merch3Mcc;
  }
  public String getMerch3Vnum()
  {
    return this.merch3Vnum;
  }
  public String getMerch3AppId()
  {
    return this.merch3AppId;
  }

  public String getMerch4Id()
  {
    return this.merch4Id;
  }
  public String getMerch4Dba()
  {
    return this.merch4Dba;
  }
  public String getMerch4Mcc()
  {
    return this.merch4Mcc;
  }
  public String getMerch4Vnum()
  {
    return this.merch4Vnum;
  }
  public String getMerch4AppId()
  {
    return this.merch4AppId;
  }

  public String getMerch5Id()
  {
    return this.merch5Id;
  }
  public String getMerch5Dba()
  {
    return this.merch5Dba;
  }
  public String getMerch5Mcc()
  {
    return this.merch5Mcc;
  }
  public String getMerch5Vnum()
  {
    return this.merch5Vnum;
  }
  public String getMerch5AppId()
  {
    return this.merch5AppId;
  }

  public String getMerch6Id()
  {
    return this.merch6Id;
  }
  public String getMerch6Dba()
  {
    return this.merch6Dba;
  }
  public String getMerch6Mcc()
  {
    return this.merch6Mcc;
  }
  public String getMerch6Vnum()
  {
    return this.merch6Vnum;
  }
  public String getMerch6AppId()
  {
    return this.merch6AppId;
  }


  public String getVisaAccepted()
  {
    return this.visaAccepted;
  }
  public String getAmexAccepted()
  {
    return this.amexAccepted;
  }
  public String getDiscAccepted()
  {
    return this.discAccepted;
  }
  public String getDebitAccepted()
  {
    return this.debitAccepted;
  }
  public String getCallTagItem()
  {
    return this.callTagItem;
  }
  public String getCallTagItemSerialNum()
  {
    return this.callTagItemSerialNum;
  }
  public String getSpecialComment()
  {
    return this.specialComment;
  }
  public String getLargePlate()
  {
    return this.largePlate;
  }
  public String getSmallPlate()
  {
    return this.smallPlate;
  }
  public String getImprinter()
  {
    return this.imprinter;
  }
  public String getAccessCode()
  {
    return this.accessCode;
  }

  public String getAccessCodeDesc()
  {
    return this.accessCodeDesc;
  }


  public String getTerminalStatus()
  {
    return this.terminalStatus;
  }

  public String getPrinterStatus()
  {
    return this.printerStatus;
  }

  public String getPinpadStatus()
  {
    return this.pinpadStatus;
  }

  public String getDbaName()
  {
    return dbaName;
  }
  public String getMerchNum()
  {
    return merchNum;
  }
  public String getControlNum()
  {
    return controlNum;
  }
  public String getAppTypeDesc()
  {
    return appTypeDesc;
  }
  public String getAppCreatedDate()
  {
    return appCreatedDate;
  }

  public String getContactName()
  {
    return contactName;
  }

  public String getContactPhone()
  {
    return formatPhone(contactPhone);
  }

  public String getPhysicalAddressDesc()
  {
    return this.physicalAddressDesc;
  }
  public String getPhysicalAddressLine1()
  {
    return this.physicalAddressLine1;
  }
  public String getPhysicalAddressLine2()
  {
    return this.physicalAddressLine2;
  }
  public String getPhysicalAddressCity()
  {
    return this.physicalAddressCity;
  }
  public String getPhysicalAddressState()
  {
    return this.physicalAddressState;
  }
  public String getPhysicalAddressZip()
  {
    return this.physicalAddressZip;
  }

  public String getShippingMethodSingle()
  {
    return this.shippingMethodSingle;
  }
  public String getTimeZoneSingle()
  {
    return this.timeZoneSingle;
  }
  public String getRushAuthorizationSingle()
  {
    return this.rushAuthorizationSingle;
  }

  public String getShippingMethodMulti()
  {
    return this.shippingMethodMulti;
  }
  public String getTimeZoneMulti()
  {
    return this.timeZoneMulti;
  }
  public String getRushAuthorizationMulti()
  {
    return this.rushAuthorizationMulti;
  }


  public String getMerchantType()
  {
    return this.merchantType;
  }
  public String getDialPayType()
  {
    return this.dialPayType;
  }

  public String getSlipType()
  { 
    return this.slipType;
  }

  public boolean isStageOnly()
  {
    return this.stageOnly;
  }


  public String  getCoverSheetPrinted()
  {
    return this.coverSheetPrinted;
  }
  public String  getPricingSchedulePrinted()
  {
    return this.pricingSchedulePrinted;
  }
  public boolean isPackingSlipSubmitted()
  {
    return this.packingSlipSubmitted;
  }
  public boolean isDiscoverApp()
  {
    return this.discoverApp;
  }
  public String getConversionType()
  {
    return this.conversionType;
  }

//***************************settters
  public void setShipEquipmentCheckBox(String shipEquipmentCheckBox)
  {
    this.shipEquipmentCheckBox = shipEquipmentCheckBox;
  }

  public void setCallTagCheckBox(String callTagCheckBox)
  {
    this.callTagCheckBox = callTagCheckBox;
  }
  public void setShipAddDifCheckBox(String shipAddDifCheckBox)
  {
    this.shipAddDifCheckBox = shipAddDifCheckBox;
  }
  public void setShipAddDif(String shipAddDif)
  {
    this.shipAddDif = shipAddDif;
  }

  public void setShippingName(String shippingName)
  {
    this.shippingName = shippingName;
  }
  public void setShippingAddress1(String shippingAddress1)
  {
    this.shippingAddress1 = shippingAddress1;
  }
  public void setShippingAddress2(String shippingAddress2)
  {
    this.shippingAddress2 = shippingAddress2;
  }
  public void setShippingCity(String shippingCity)
  {
    this.shippingCity = shippingCity;
  }
  public void setShippingState(String shippingState)
  {
    this.shippingState = shippingState;
  }
  public void setShippingZip(String shippingZip)
  {
    this.shippingZip = shippingZip;
  }
  public void setShippingPhone(String shippingPhone)
  {
    this.shippingPhone = shippingPhone;
  }
  public void setShippingContactName(String shippingContactName)
  {
    this.shippingContactName = shippingContactName;
  }

  public void setMerch2Id(String merch2Id)
  {
    this.merch2Id = merch2Id;
  }

  public void setMerch2Dba(String merch2Dba)
  {
    this.merch2Dba = merch2Dba;
  }
  
  public void setMerch2Mcc(String merch2Mcc)
  {
    this.merch2Mcc = merch2Mcc;
  }

  public void setMerch2Vnum(String merch2Vnum)
  {
    this.merch2Vnum = merch2Vnum;
  }
  
  public void setMerch2AppId(String merch2AppId)
  {
    this.merch2AppId = merch2AppId;
  }

  public void setMasterVnum(String masterVnum)
  {
    this.masterVnum = masterVnum;
  }
  public void setMasterAppId(String masterAppId)
  {
    this.masterAppId = masterAppId;
  }

  public void setMerch3Id(String merch3Id)
  {
    this.merch3Id = merch3Id;
  }

  public void setMerch3Dba(String merch3Dba)
  {
    this.merch3Dba = merch3Dba;
  }
  
  public void setMerch3Mcc(String merch3Mcc)
  {
    this.merch3Mcc = merch3Mcc;
  }

  public void setMerch3Vnum(String merch3Vnum)
  {
    this.merch3Vnum = merch3Vnum;
  }
  
  public void setMerch3AppId(String merch3AppId)
  {
    this.merch3AppId = merch3AppId;
  }

  public void setMerch4Id(String merch4Id)
  {
    this.merch4Id = merch4Id;
  }

  public void setMerch4Dba(String merch4Dba)
  {
    this.merch4Dba = merch4Dba;
  }
  
  public void setMerch4Mcc(String merch4Mcc)
  {
    this.merch4Mcc = merch4Mcc;
  }

  public void setMerch4Vnum(String merch4Vnum)
  {
    this.merch4Vnum = merch4Vnum;
  }
  
  public void setMerch4AppId(String merch4AppId)
  {
    this.merch4AppId = merch4AppId;
  }

  public void setMerch5Id(String merch5Id)
  {
    this.merch5Id = merch5Id;
  }

  public void setMerch5Dba(String merch5Dba)
  {
    this.merch5Dba = merch5Dba;
  }
  
  public void setMerch5Mcc(String merch5Mcc)
  {
    this.merch5Mcc = merch5Mcc;
  }

  public void setMerch5Vnum(String merch5Vnum)
  {
    this.merch5Vnum = merch5Vnum;
  }
  
  public void setMerch5AppId(String merch5AppId)
  {
    this.merch5AppId = merch5AppId;
  }

  public void setMerch6Id(String merch6Id)
  {
    this.merch6Id = merch6Id;
  }

  public void setMerch6Dba(String merch6Dba)
  {
    this.merch6Dba = merch6Dba;
  }
  
  public void setMerch6Mcc(String merch6Mcc)
  {
    this.merch6Mcc = merch6Mcc;
  }

  public void setMerch6Vnum(String merch6Vnum)
  {
    this.merch6Vnum = merch6Vnum;
  }
  
  public void setMerch6AppId(String merch6AppId)
  {
    this.merch6AppId = merch6AppId;
  }


  public void setVisaAccepted(String visaAccepted)
  {
    this.visaAccepted = visaAccepted;
  }
  public void setAmexAccepted(String amexAccepted)
  {
    this.amexAccepted = amexAccepted;
  }
  public void setDiscAccepted(String discAccepted)
  {
    this.discAccepted = discAccepted;
  }
  public void setDebitAccepted(String debitAccepted)
  {
    this.debitAccepted = debitAccepted;
  }
  public void setCallTagItem(String callTagItem)
  {
    this.callTagItem = callTagItem;
  }
  public void setCallTagItemSerialNum(String callTagItemSerialNum)
  {
    this.callTagItemSerialNum = callTagItemSerialNum;
  }
  public void setSpecialComment(String specialComment)
  {
    this.specialComment = specialComment;
  }
  public void setLargePlate(String largePlate)
  {
    this.largePlate = largePlate;
  }
  public void setSmallPlate(String smallPlate)
  {
    this.smallPlate = smallPlate;
  }
  public void setImprinter(String imprinter)
  {
    this.imprinter = imprinter;
  }
  public void setAccessCode(String accessCode)
  {
    this.accessCode = accessCode;
  }

  public void setAccessCodeDesc(String accessCodeDesc)
  {
    this.accessCodeDesc = accessCodeDesc;
  }

  public void setShippingMethodSingle(String shippingMethodSingle)
  {
    this.shippingMethodSingle = shippingMethodSingle;
  }
  public void setTimeZoneSingle(String timeZoneSingle)
  {
    this.timeZoneSingle = timeZoneSingle;
  }
  public void setRushAuthorizationSingle(String rushAuthorizationSingle)
  {
    this.rushAuthorizationSingle = rushAuthorizationSingle;
  }
  
  public void setShippingMethodMulti(String shippingMethodMulti)
  {
    this.shippingMethodMulti = shippingMethodMulti;
  }
  public void setTimeZoneMulti(String timeZoneMulti)
  {
    this.timeZoneMulti = timeZoneMulti;
  }
  public void setRushAuthorizationMulti(String rushAuthorizationMulti)
  {
    this.rushAuthorizationMulti = rushAuthorizationMulti;
  }
  
  
  public void setMerchantType(String merchantType)
  {
    this.merchantType = merchantType;
  }
  public void setDialPayType(String dialPayType)
  {
    this.dialPayType = dialPayType;
  }

  public void setTerminalStatus(String terminalStatus)
  {
    this.terminalStatus = terminalStatus;
  }

  public void setPrinterStatus(String printerStatus)
  {
    this.printerStatus = printerStatus;
  }

  public void setPinpadStatus(String pinpadStatus)
  {
    this.pinpadStatus = pinpadStatus;
  }

  public void setSlipType(String slipType)
  {
    this.slipType = slipType;
  }

//****************************end of setters

  public String getDiscoverNumber()
  {
    return this.discoverNumber;
  }

  public String getRequestId(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getRequestId());
  }

  public String getVnumber(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getVnumber());
  }

  public String getTermApplication(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getTermApplication());
  }

  public String getTerminalModel(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getTerminalModel());
  }

  public String getPrinterModel(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getPrinterModel());
  }

  public String getPinpadModel(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getPinpadModel());
  }

  public String getTerminalRefurb(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getTerminalRefurb());
  }

  public String getPrinterRefurb(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getPrinterRefurb());
  }

  public String getPinpadRefurb(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getPinpadRefurb());
  }

  public String getTerminalSerialNum(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getTerminalSerialNum());
  }

  public String getPrinterSerialNum(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getPrinterSerialNum());
  }

  public String getPinpadSerialNum(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getPinpadSerialNum());
  }
  

  public class TerminalInfo
  { 

    private String requestId          = "";
    private String vnumber            = "";
    private String termApplication    = "";
    private String terminalModel      = "";
    private String terminalRefurb     = "";
    private String printerModel       = "";
    private String printerRefurb      = "";
    private String pinpadModel        = "";
    private String pinpadRefurb       = "";
    private String terminalSerialNum  = "";
    private String printerSerialNum   = "";
    private String pinpadSerialNum    = "";

    TerminalInfo()
    {}

    public void setRequestId(String requestId)
    {
      this.requestId = requestId;
    }
    public void setVnumber(String vnumber)
    {
      this.vnumber = vnumber;
    }
    public void setTermApplication(String termApplication)
    {
      this.termApplication = termApplication;
    }
    
    public void setTerminalModel(String terminalModel)
    {
      this.terminalModel = terminalModel;
    }
    public void setPrinterModel(String printerModel)
    {
      this.printerModel = printerModel;
    }
    public void setPinpadModel(String pinpadModel)
    {
      this.pinpadModel = pinpadModel;
    }

    public void setTerminalRefurb(String terminalRefurb)
    {
      this.terminalRefurb = terminalRefurb;
    }
    public void setPrinterRefurb(String printerRefurb)
    {
      this.printerRefurb = printerRefurb;
    }
    public void setPinpadRefurb(String pinpadRefurb)
    {
      this.pinpadRefurb = pinpadRefurb;
    }



    public void setTerminalSerialNum(String terminalSerialNum)
    {
      this.terminalSerialNum = terminalSerialNum;
    }
    public void setPrinterSerialNum(String printerSerialNum)
    {
      this.printerSerialNum = printerSerialNum;
    }
    public void setPinpadSerialNum(String pinpadSerialNum)
    {
      this.pinpadSerialNum = pinpadSerialNum;
    }


    public String getRequestId()
    {
      return this.requestId;
    }
    public String getVnumber()
    {
      return this.vnumber;
    }
    public String getTermApplication()
    {
      return this.termApplication;
    }

    public String getTerminalModel()
    {
      return this.terminalModel;
    }
    public String getPrinterModel()
    {
      return this.printerModel;
    }
    public String getPinpadModel()
    {
      return this.pinpadModel;
    }

    public String getTerminalRefurb()
    {
      return this.terminalRefurb;
    }
    public String getPrinterRefurb()
    {
      return this.printerRefurb;
    }
    public String getPinpadRefurb()
    {
      return this.pinpadRefurb;
    }

    public String getTerminalSerialNum()
    {
      return this.terminalSerialNum;
    }
    public String getPrinterSerialNum()
    {
      return this.printerSerialNum;
    }
    public String getPinpadSerialNum()
    {
      return this.pinpadSerialNum;
    }


  }

}
