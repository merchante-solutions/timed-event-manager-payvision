/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/GeneralInfoBean.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-06-16 10:54:10 -0700 (Mon, 16 Jun 2008) $
  Version            : $Revision: 14969 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

public class GeneralInfoBean extends ExpandDataBean
{
  // data members
  private long                association       = 0L;     // merchant.asso_number
  private long                merchantNumber    = 0L;     // merchant.merch_number
  private int                 sicCode           = 0;      // merchant.sic_code
  private int                 mccCode           = 0;      // merchant.merch_mcc
  private int                 bankNumber        = 0;      // merchant.merch_bank_number
  private String              statusIndicator   = "";     // merchant.status_ind
  private String              incStatus         = "";     // merchant.merch_visainc_status_ind
  private String              referringBank     = "";     // merchant.merch_referring_bank

  private String              annualVolume      = "";     // merchant.
  private String              averageTicket     = "";     // merchant.
  
  private String              gender            = "";     // merchant_data.gender
  private String              repCode           = "";     // merchant_data.rep_code
  private String              userData1         = "";     // merchant_data.user_data_1
  private String              userData2         = "";     // merchant_data.user_data_2
  private String              userData3         = "";     // merchant_data.user_data_3
  private String              userData4         = "";     // merchant_data.user_data_4
  private String              userData5         = "";     // merchant_data.user_data_5
  private String              userAccount1      = "";     // merchant_data.user_account_1
  private String              userAccount2      = "";     // merchant_data.user_account_2
  
  // incidental variables
  private Vector              statuses          = new Vector();
  private Vector              statusDescs       = new Vector();
  
  private Vector              genders           = new Vector();
  private Vector              genderDescs       = new Vector();
  
  private Vector              incStatuses       = new Vector();
  private Vector              incStatusDescs    = new Vector();
  
  private void setStatuses()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      // create info vectors
      qs.append("select * from status ");
      qs.append("order by status_ind asc");
    
      ps = getPreparedStatement(qs.toString());
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        statuses.add(rs.getString("status_ind"));
        statusDescs.add(rs.getString("status_descr"));
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setStatuses(): " + e.toString());
      addError("setStatuses(): " + e.toString());
    }
    finally
    {
      if(rs != null)
      {
        try
        {
          rs.close();
        }
        catch(Exception e)
        {
        }
      }
      if(ps != null)
      {
        try
        {
          ps.close();
        }
        catch(Exception e)
        {
        }
      }
    }
  }
  
  private void setGenders()
  {
    genders.add("U");
    genderDescs.add("Unspecified");
    genders.add("M");
    genderDescs.add("Male");
    genders.add("F");
    genderDescs.add("Female");
  }
  
  private void setIncStatuses()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      // create info vectors
      qs.append("select * from bustype ");
      qs.append("order by bustype_code asc");
    
      ps = getPreparedStatement(qs.toString());
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        incStatuses.add(Integer.toString(rs.getInt("bustype_code")));
        incStatusDescs.add(rs.getString("bustype_desc"));
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setIncStatuses(): " + e.toString());
      addError("setIncStatuses(): " + e.toString());
    }
    finally
    {
      if(rs != null)
      {
        try
        {
          rs.close();
        }
        catch(Exception e)
        {
        }
      }
      if(ps != null)
      {
        try
        {
          ps.close();
        }
        catch(Exception e)
        {
        }
      }
    }
  }
  
  public GeneralInfoBean()
  {
    setStatuses();
    setGenders();
    setIncStatuses();
  }
  
  public void getMerchantData(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select                     ");
      qs.append("asso_number,               ");
      qs.append("merch_number,              ");
      qs.append("merch_gender,              ");
      qs.append("sic_code,                  ");
      qs.append("merch_mcc,                 ");
      qs.append("merch_bank_number,         ");
      qs.append("status_ind,                ");
      qs.append("merch_visainc_status_ind,  ");
      qs.append("merch_referring_bank,      ");
      qs.append("merch_rep_code             ");
      qs.append("from merchant              ");
      qs.append("where app_seq_num = ?      ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        association       = rs.getLong  ("asso_number");
        merchantNumber    = rs.getLong  ("merch_number");
        sicCode           = rs.getInt   ("sic_code");
        mccCode           = rs.getInt   ("merch_mcc");
        bankNumber        = rs.getInt   ("merch_bank_number");
        statusIndicator   = rs.getString("status_ind");
        incStatus         = rs.getString("merch_visainc_status_ind");
        referringBank     = rs.getString("merch_referring_bank");
        gender            = rs.getString("merch_gender");
        repCode           = rs.getString("merch_rep_code");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMerchantData: " + e.toString());
      addError("getMerchantData: " + e.toString());
    }
  }
  
  public void getMiscData(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select                 ");
      qs.append("user_data_1,           ");
      qs.append("user_data_2,           ");
      qs.append("user_data_3,           ");
      qs.append("user_data_4,           ");
      qs.append("user_data_5,           ");
      qs.append("user_account_1,        ");
      qs.append("user_account_2         ");
      qs.append("from merchant_data     ");
      qs.append("where app_seq_num = ?  ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      
      rs = ps.executeQuery();
      if(rs.next())
      {
        userData1     = rs.getString("user_data_1");
        userData2     = rs.getString("user_data_2");
        userData3     = rs.getString("user_data_3");
        userData4     = rs.getString("user_data_4");
        userData5     = rs.getString("user_data_5");
        userAccount1  = rs.getString("user_account_1");
        userAccount2  = rs.getString("user_account_2");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMiscData: " + e.toString());
      addError("getMiscData: " + e.toString());
    }
  }
  
  public void getData(long primaryKey)
  {
    getMerchantData(primaryKey);
    getMiscData(primaryKey);
  }
  
  public void setDefaults(String primaryKey)
  {
    try
    {
      setDefaults(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
    }
  }
  
  protected boolean isTridentApp(long primaryKey)
  {
    StringBuffer      qs            = new StringBuffer("");
    PreparedStatement ps            = null;
    ResultSet         rs            = null;
    boolean           result        = false;
    
    try
    {
      // currently only sure indicator of a trident app is one that uses TVT
      qs.append("select  mp.pos_code ");
      qs.append("from    merch_pos mp ");
      qs.append("where   mp.app_seq_num = ?");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      
      rs = ps.executeQuery();
      
      if(rs.next() && rs.getInt("pos_code") == mesConstants.APP_MPOS_VIRTUAL_TERMINAL)
      {
        result = true;
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isTridentApp("+primaryKey+"): " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { ps.close(); } catch(Exception e) {}
    }
    
    return result;
  }
  
  public void setDefaults(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      ApplicationTypeBean   appType = new ApplicationTypeBean();
      appType.fillDataApp(primaryKey);
      
      // mccCode - default to sicCode
      if(mccCode == 0)
      {
        mccCode = sicCode;
      }
    
      // bankNumber
      if(bankNumber == 0)
      {
        bankNumber = appType.getAppBankNumber();
      }
      

      qs.append("select                                   ");
      qs.append("merch.svb_cif_num,                       ");
      qs.append("merch.bustype_code,                      ");
      qs.append("merch.industype_code,                    ");
      qs.append("merch.merch_busgoodserv_descr,           ");
      qs.append("merch.merch_month_visa_mc_sales,         ");
      qs.append("merch.merch_average_cc_tran,             ");
      qs.append("merch.merch_mail_phone_sales,            ");
      qs.append("merch.merch_referring_bank,              ");
      qs.append("merch.account_type,                      ");
      qs.append("decode(nvl(merch.existing_verisign,'N'),'Y','OV','y','OV', poscat.pos_abbrev) pos_abbrev ");
      qs.append("from                                     ");
      qs.append("merchant     merch,                      ");
      qs.append("merch_pos    pos,                        ");
      qs.append("pos_category poscat                      ");
      qs.append("where                                    ");
      qs.append("merch.app_seq_num  = pos.app_seq_num and ");
      qs.append("pos.pos_code       = poscat.pos_code and ");
      qs.append("merch.app_seq_num  = ?                   ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        // incStatus - default to bustype_code
        if(isBlank(incStatus))
        {
          incStatus = rs.getString("bustype_code");
        }

        // user data 2
        if(isBlank(userData2) && bankNumber == 3941)
        {
          userData2 = referringBank;
        }
                
        // user data 3
        if(isBlank(userData3) && (bankNumber == 3941 || bankNumber == 3942))
        {
          StringBuffer user3 = new StringBuffer("");
   
          user3.append(appType.getAppSource());
   
          // add the processor (default to 'V' for Vital)
          if(isTridentApp(primaryKey))
          {
            user3.append("T");
          }
          else
          {
            user3.append("V");
          }

          // add the product type abbreviation
          user3.append(rs.getString("pos_abbrev"));
          
          userData3 = user3.toString();
        }
        
        // user data 4
        if(isBlank(userData4) && bankNumber == 3941)
        {
          StringBuffer    user4       = new StringBuffer("");
          DecimalFormat   cifFormat   = new DecimalFormat("0000000000");
          DecimalFormat   cdFormat    = new DecimalFormat("000000");
          DecimalFormat   salesFormat = new DecimalFormat("00000000");
          DecimalFormat   avgFormat   = new DecimalFormat("0000");
          DecimalFormat   motoFormat  = new DecimalFormat("000");
          
          float ccSales = rs.getFloat("merch_month_visa_mc_sales");
          float ccAvg   = rs.getFloat("merch_average_cc_tran");
          float ccMoto  = rs.getFloat("merch_mail_phone_sales");
          
          // turn monthly sales into yearly sales
          ccSales = ccSales * 12;
          user4.append(salesFormat.format(ccSales));
          
          // average ticket
          user4.append(avgFormat.format(ccAvg));
          
          // moto pctg
          user4.append(motoFormat.format(ccMoto));
          
          userData4 = user4.toString();

          if(!isBlank(rs.getString("account_type")) && rs.getString("account_type").equals("C"))
          {
            userData4 += "C";
          }
          else
          {
            userData4 += "N";
          }

       
          if(appType.getAppType() == mesConstants.APP_TYPE_SVB)
          {
            try
            {
              float cifNum = 0;
              float cdNum  = 0;

              if(!isBlank(rs.getString("svb_cif_num")))
              {
                cifNum = rs.getFloat("svb_cif_num");
              }
              if(!isBlank(rs.getString("svb_cd_num")))
              {
                cdNum = rs.getFloat("svb_cd_num");
              }
            
              userAccount1 = cifFormat.format(cifNum) + cdFormat.format(cdNum);
            
            }
            catch(Exception e)
            {
            }
         }

      }


        if(bankNumber == 3858)
        {
          float ccSales   = rs.getFloat("merch_month_visa_mc_sales");
          ccSales         = ccSales * 12; // get annual sales from monthly sales

          userData1       = Float.toString(ccSales);
          //take off the last three numbers and replace them with a K
          int idx = userData1.indexOf(".");
          if(idx > 0)
          {
            userData1 = userData1.substring(0,idx);
          }
          
          if(userData1.length() >= 4)
          {
            userData1       = userData1.substring(0,(userData1.length() - 3)) + "K";
          }

          if(userData1.length() > 4)
          {
            userData1 = userData1.substring(0,4);
          }

          userData2       = rs.getString("merch_average_cc_tran");
          int decIdx = userData2.indexOf(".");
          if(decIdx > -1)
          {
            userData2 = userData2.substring(0,decIdx);
          }

          if(userData2.length() >= 4)
          {
            userData2 = userData2.substring(0,(userData2.length() - 3)) + "K";
          }



          userData3       = isBlank(userData3) ? rs.getString("merch_mail_phone_sales")  : userData3;

          userData4       = isBlank(userData4) ? rs.getString("merch_busgoodserv_descr") : userData4;
          if(userData4.length() > 16)
          {
            userData4 = userData4.substring(0,16);
          }
/*          
          switch(rs.getInt("industype_code"))
          {
            case mesConstants.APP_INDUSTYPE_RETAIL:
              userData4 = isBlank(userData4) ? "Retail" : userData4;
            break;
            case mesConstants.APP_INDUSTYPE_RESTAURANT:
              userData4 = isBlank(userData4) ? "Restaurant" : userData4;
            break;
            case mesConstants.APP_INDUSTYPE_HOTEL:
              userData4 = isBlank(userData4) ? "Hotel" : userData4;
            break;
            case mesConstants.APP_INDUSTYPE_MOTEL:
              userData4 = isBlank(userData4) ? "Motel" : userData4;
            break;
            case mesConstants.APP_INDUSTYPE_SERVICES:
              userData4 = isBlank(userData4) ? "Services" : userData4;
            break;
            case mesConstants.APP_INDUSTYPE_INTERNET:
              userData4 = isBlank(userData4) ? "Internet" : userData4;
            break;
            case mesConstants.APP_INDUSTYPE_DIRECTMARKET:
              userData4 = isBlank(userData4) ? "Direct Marketing" : userData4;
            break;
            default:
              userData4 = isBlank(userData4) ? "Unknown" : userData4;
            break;
          }
*/
        }
        
      }
      rs.close();
      ps.close();

      if(bankNumber == 3858)
      {
        qs.setLength(0);
        qs.append("select cardtype_code,merchpo_card_merch_number from merchpayoption ");
        qs.append("where app_seq_num = ?");
      
        ps = getPreparedStatement(qs.toString());
        ps.setLong(1, primaryKey);
      
        rs = ps.executeQuery();
        while(rs.next())
        {
          switch(rs.getInt("cardtype_code"))
          {
            case mesConstants.APP_CT_AMEX:
              userAccount1 = isBlank(userAccount1) ? rs.getString("merchpo_card_merch_number") : userAccount1;
              userAccount1 = userAccount1.equals("0") ? "" : userAccount1; 
            break;
            case mesConstants.APP_CT_DISCOVER:
              userAccount2 = isBlank(userAccount2) ? rs.getString("merchpo_card_merch_number") : userAccount2;
              userAccount2 = userAccount2.equals("0") ? "" : userAccount2; 
            break;
            case mesConstants.APP_CT_SPS:
              userData5    = isBlank(userData5) ? rs.getString("merchpo_card_merch_number") : userData5;
              userData5    = userData5.equals("0") ? "" : userData5;
              break;
          }
        }
        rs.close();
        ps.close();
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setDefaults: " + e.toString());
      addError("setDefaults: " + e.toString());
    }
  }
  
  public String formatCurr(String curr)
  {
    if(isBlank(curr))
    {
      return "";
    }
    
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }


  public void submitMerchantData(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    
    try
    {
      qs.append("update merchant set            ");
      qs.append("merch_number             = ?,  ");
      qs.append("asso_number              = ?,  ");
      qs.append("sic_code                 = ?,  ");
      qs.append("merch_mcc                = ?,  ");
      qs.append("merch_bank_number        = ?,  ");
      qs.append("status_ind               = ?,  ");
      qs.append("merch_visainc_status_ind = ?,  ");
      qs.append("merch_gender             = ?,  ");
      qs.append("merch_rep_code           = ?   ");
      qs.append("where app_seq_num        = ?   ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong  (1, merchantNumber);
      ps.setLong  (2, association);
      ps.setInt   (3, sicCode);
      ps.setInt   (4, mccCode);
      ps.setInt   (5, bankNumber);
      ps.setString(6, statusIndicator);
      ps.setString(7, incStatus);
      ps.setString(8, gender);
      ps.setString(9, repCode);
      ps.setLong  (10, primaryKey);
      
      if(ps.executeUpdate() != 1)
      {
        addError("unable to update merchant table");
      }
      
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitMerchantData: " + e.toString());
      addError("submitMerchantData: "+ e.toString());
    }
  }
  
  public void submitMiscData(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try
    {
      ps = getPreparedStatement("select app_seq_num from merchant_data where app_seq_num = ?");
      ps.setLong(1, primaryKey);
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        qs.append("update merchant_data set ");
        qs.append("user_data_1        = ?,  ");
        qs.append("user_data_2        = ?,  ");
        qs.append("user_data_3        = ?,  ");
        qs.append("user_data_4        = ?,  ");
        qs.append("user_data_5        = ?,  ");
        qs.append("user_account_1     = ?,  ");
        qs.append("user_account_2     = ?   ");
        qs.append("where app_seq_num  = ?   ");
      }
      else
      {
        qs.append("insert into merchant_data (  ");
        qs.append("user_data_1,                 ");
        qs.append("user_data_2,                 ");
        qs.append("user_data_3,                 ");
        qs.append("user_data_4,                 ");
        qs.append("user_data_5,                 ");
        qs.append("user_account_1,              ");
        qs.append("user_account_2,              ");
        qs.append("app_seq_num )                ");
        qs.append("values (?,?,?,?,?,?,?,?)     ");
      
      }
      
      ps = getPreparedStatement(qs.toString());
      
      ps.setString(1, userData1);
      ps.setString(2, userData2);
      ps.setString(3, userData3);
      ps.setString(4, userData4);
      ps.setString(5, userData5);
      ps.setString(6, userAccount1);
      ps.setString(7, userAccount2);
      ps.setLong  (8, primaryKey);
      
      if(ps.executeUpdate() != 1)
      {
        addError("unable to update merchant_data table");
      }
      
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitMiscData: " + e.toString());
      addError("submitMiscData: " + e.toString());
    }
  }
  
  public boolean validate()
  {
    if(merchantNumber == 0L)
    {
      addError("Please enter a valid Merchant Number");
    }
    if(!isBlank(repCode) && !isNumber(repCode))
    {
      addError("Please provide a valid numeric Rep Code");
    }

    if(association <= 0L)
    {
      addError("Please provide a valid association number");
    }

    if(bankNumber <= 0)
    {
      addError("Please provide a valid bank number");
    }

    return(! hasErrors());
  }
  
  public void submitData(HttpServletRequest req, long primaryKey)
  {
    submitMerchantData(primaryKey);
    submitMiscData(primaryKey);
  }
  
  public long getMerchantNumberLong()
  {
    return this.merchantNumber;
  }

  public String getMerchantNumber()
  {
    String result = "";
    if(merchantNumber > 0L)
    {
      result = Long.toString(merchantNumber);
    }
    return result;
  }

  public void setMerchantNumber(String merchantNumber)
  {
    try
    {
      this.merchantNumber = Long.parseLong(merchantNumber);
    }
    catch(Exception e)
    {
      this.merchantNumber = 0L;
    }
  }
  
  public void setAssociation(String association)
  {
    try
    {
      this.association = Long.parseLong(association);
    }
    catch(Exception e)
    {
      this.association = 0L;
    }
  }
  public String getAssociation()
  {
    String result = "";
    if(association > 0L)
    {
      result = Long.toString(association);
    }
    return result;
  }
  
  public void setSicCode(String sicCode)
  {
    try
    {
      this.sicCode = Integer.parseInt(sicCode);
    }
    catch(Exception e)
    {
      this.sicCode = 0;
    }
  }
  public String getSicCode()
  {
    String result = "";
    if(sicCode > 0)
    {
      result = Integer.toString(sicCode);
    }
    return result;
  }
  
  public void setMccCode(String mccCode)
  {
    try
    {
      this.mccCode = Integer.parseInt(mccCode);
    }
    catch(Exception e)
    {
      this.mccCode = 0;
    }
  }
  public String getMccCode()
  {
    String result = "";
    if(mccCode > 0)
    {
      result = Integer.toString(mccCode);
    }
    return result;
  }
  
  public void setBankNumber(String bankNumber)
  {
    try
    {
      this.bankNumber = Integer.parseInt(bankNumber);
    }
    catch(Exception e)
    {
      this.bankNumber = 0;
    }
  }
  public String getBankNumber()
  {
    String result = "";
    if(bankNumber > 0)
    {
      result = Integer.toString(bankNumber);
    }
    return result;
  }
  
  public void setStatusIndicator(String statusIndicator)
  {
    this.statusIndicator = statusIndicator;
  }
  public String getStatusIndicator()
  {
    return blankIfNull(this.statusIndicator);
  }
  public Vector getStatuses()
  {
    return this.statuses;
  }
  public Vector getStatusDescs()
  {
    return this.statusDescs;
  }
  
  public void setGender(String gender)
  {
    this.gender = gender;
  }
  public String getGender()
  {
    return blankIfNull(this.gender);
  }
  public Vector getGenders()
  {
    return this.genders;
  }
  public Vector getGenderDescs()
  {
    return this.genderDescs;
  }
  
  public void setIncStatus(String incStatus)
  {
    this.incStatus = incStatus;
  }
  public String getIncStatus()
  {
    return blankIfNull(this.incStatus);
  }
  
  public void setRepCode(String repCode)
  {
    this.repCode = repCode;
  }
  public String getRepCode()
  {
    return blankIfNull(this.repCode);
  }
  
  public void setUserData1(String userData1)
  {
    this.userData1 = userData1;
  }
  public String getUserData1()
  {
    return blankIfNull(this.userData1);
  }
  
  public void setUserData2(String userData2)
  {
    this.userData2 = userData2;
  }
  public String getUserData2()
  {
    return blankIfNull(this.userData2);
  }
  
  public void setUserData3(String userData3)
  {
    this.userData3 = userData3;
  }
  public String getUserData3()
  {
    return blankIfNull(this.userData3);
  }
  
  public void setUserData4(String userData4)
  {
    this.userData4 = userData4;
  }
  public String getUserData4()
  {
    return blankIfNull(this.userData4);
  }
  
  public void setUserData5(String userData5)
  {
    this.userData5 = userData5;
  }
  public String getUserData5()
  {
    return blankIfNull(this.userData5);
  }

  public void setUserAccount1(String userAccount1)
  {
    this.userAccount1 = userAccount1;
  }
  public String getUserAccount1()
  {
    return blankIfNull(this.userAccount1);
  }
  
  public void setUserAccount2(String userAccount2)
  {
    this.userAccount2 = userAccount2;
  }
  public String getUserAccount2()
  {
    return blankIfNull(this.userAccount2);
  }

  public Vector getIncStatuses()
  {
    return this.incStatuses;
  }
  
  public Vector getIncStatusDescs()
  {
    return this.incStatusDescs;
  }
  
  
  public String getReferringBank()
  {
    return blankIfNull(this.referringBank);
  }

  public void setAnnualVolume(String annualVolume)
  {
    this.annualVolume = annualVolume;
  }

  public String getAnnualVolume()
  {
    return blankIfNull(this.annualVolume);
  }

  public void setAverageTicket(String averageTicket)
  {
    this.averageTicket = averageTicket;
  }

  public String getAverageTicket()
  {
    return blankIfNull(this.averageTicket);
  }

}
