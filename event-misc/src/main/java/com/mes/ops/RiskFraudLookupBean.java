/*@lineinfo:filename=RiskFraudLookupBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/RiskFraudLookupBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 1/27/03 4:40p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class RiskFraudLookupBean extends SQLJConnectionBase
{
  private ResultSetIterator it                  = null;

  private String            lookupValue         = "";
 
  private boolean           submitted           = false;
  private boolean           error               = false;

  private Vector            searchResults       = new Vector();
  
  public void RiskFraudLookupBean()
  {
  }
 
  
  public boolean validate()
  {
    if(lookupValue == null || lookupValue.equals(""))
    {
      error = true;
    }
    
    return !error;
     
  }

  /*
  ** METHOD public void getData()
  **
  */
  public void getData()
  {
    long            longLookup          = -1L;
    String          stringLookup        = "";
    
    try
    {
      connect();
    
      try
      {
        longLookup = Long.parseLong(this.lookupValue.trim());
      }
      catch( NumberFormatException nfe )
      {
        longLookup = -1L;
      }
      
      stringLookup = "%" + lookupValue + "%";
          
      /*@lineinfo:generated-code*//*@lineinfo:96^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct
//                  mf.merchant_number, 
//                  mf.dba_name,
//                  mf.bank_number,
//                  mf.dda_num,
//                  mf.transit_routng_num,
//                  mf.federal_tax_id,
//                  mf.sic_code,
//                  mf.class_code,
//                  mf.inv_code_investigate,
//                  mf.met_table,
//                  mf.activation_date,
//                  mf.date_of_1st_deposit,
//                  mf.last_deposit_date,
//                  mf.last_deposit_amount,
//                  mf.phone_1,
//                  mf.phone_2_fax,
//                  mf.addr1_line_1,
//                  mf.addr1_line_2,
//                  mf.city1_line_4,
//                  mf.state1_line_4,
//                  mf.zip1_line_4,
//                  mf.addr2_line_1,
//                  mf.addr2_line_2,
//                  mf.city2_line_4,
//                  mf.state2_line_4,
//                  mf.zip2_line_4,
//                  mf.group_1_association,
//                  mf.group_2_association,
//                  mf.group_3_association,
//                  mf.group_4_association,
//                  mf.group_5_association,
//                  mf.group_6_association,
//                  mf.dmagent,
//                  mf.merchant_status,
//                  mf.date_billed_for_close
//          from    mif             mf
//          where   to_char(transit_routng_num)  =     :lookupValue    or
//                  to_char(sic_code)            =     :lookupValue    or
//                  to_char(zip1_line_4)         like  (:lookupValue || '%') or
//                  to_char(zip2_line_4)         like  (:lookupValue || '%') or
//                  to_char(phone_1)             =     :lookupValue    or
//                  to_char(phone_2_fax)         =     :lookupValue    or
//                  to_char(federal_tax_id)      =     :lookupValue    or
//                  mf.merchant_number in
//                    (
//                      select mr.merch_number
//                      from  businessowner bo,
//                            merchant      mr
//                      where bo.busowner_num = 1 and
//                            (
//                              lower(bo.busowner_first_name || ' ' || 
//                                    bo.busowner_last_name) like lower(:stringLookup) or
//                              bo.busowner_ssn = :longLookup
//                            ) and
//                            mr.app_seq_num = bo.app_seq_num
//                    )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct\n                mf.merchant_number, \n                mf.dba_name,\n                mf.bank_number,\n                mf.dda_num,\n                mf.transit_routng_num,\n                mf.federal_tax_id,\n                mf.sic_code,\n                mf.class_code,\n                mf.inv_code_investigate,\n                mf.met_table,\n                mf.activation_date,\n                mf.date_of_1st_deposit,\n                mf.last_deposit_date,\n                mf.last_deposit_amount,\n                mf.phone_1,\n                mf.phone_2_fax,\n                mf.addr1_line_1,\n                mf.addr1_line_2,\n                mf.city1_line_4,\n                mf.state1_line_4,\n                mf.zip1_line_4,\n                mf.addr2_line_1,\n                mf.addr2_line_2,\n                mf.city2_line_4,\n                mf.state2_line_4,\n                mf.zip2_line_4,\n                mf.group_1_association,\n                mf.group_2_association,\n                mf.group_3_association,\n                mf.group_4_association,\n                mf.group_5_association,\n                mf.group_6_association,\n                mf.dmagent,\n                mf.merchant_status,\n                mf.date_billed_for_close\n        from    mif             mf\n        where   to_char(transit_routng_num)  =      :1     or\n                to_char(sic_code)            =      :2     or\n                to_char(zip1_line_4)         like  ( :3  || '%') or\n                to_char(zip2_line_4)         like  ( :4  || '%') or\n                to_char(phone_1)             =      :5     or\n                to_char(phone_2_fax)         =      :6     or\n                to_char(federal_tax_id)      =      :7     or\n                mf.merchant_number in\n                  (\n                    select mr.merch_number\n                    from  businessowner bo,\n                          merchant      mr\n                    where bo.busowner_num = 1 and\n                          (\n                            lower(bo.busowner_first_name || ' ' || \n                                  bo.busowner_last_name) like lower( :8 ) or\n                            bo.busowner_ssn =  :9 \n                          ) and\n                          mr.app_seq_num = bo.app_seq_num\n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.RiskFraudLookupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,lookupValue);
   __sJT_st.setString(2,lookupValue);
   __sJT_st.setString(3,lookupValue);
   __sJT_st.setString(4,lookupValue);
   __sJT_st.setString(5,lookupValue);
   __sJT_st.setString(6,lookupValue);
   __sJT_st.setString(7,lookupValue);
   __sJT_st.setString(8,stringLookup);
   __sJT_st.setLong(9,longLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.RiskFraudLookupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:155^7*/
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {
        Result res = new Result();

        res.MerchantNumber      = processStr(rs.getString("MERCHANT_NUMBER"));
        res.DbaName             = processStr(rs.getString("DBA_NAME"));
        res.BankNumber          = processStr(rs.getString("BANK_NUMBER"));
        res.DdaNum              = processStr(rs.getString("DDA_NUM"));
        res.TransitRoutingNum   = processStr(rs.getString("TRANSIT_ROUTNG_NUM"));
        res.FederalTaxId        = processStr(rs.getString("FEDERAL_TAX_ID"));
        res.SicCode             = processStr(rs.getString("SIC_CODE"));
        res.ClassCode           = processStr(rs.getString("CLASS_CODE"));
        res.InvestigatorCode    = processStr(rs.getString("INV_CODE_INVESTIGATE"));
        res.MetTable            = processStr(rs.getString("MET_TABLE"));
        res.ActivationDate      = processStr(DateTimeFormatter.getFormattedDate(rs.getTimestamp("ACTIVATION_DATE"),     "MM/dd/yy HH:mm"));
        res.DateOf1stDeposit    = processStr(DateTimeFormatter.getFormattedDate(rs.getTimestamp("DATE_OF_1ST_DEPOSIT"), "MM/dd/yy HH:mm"));
        res.LastDepositDate     = processStr(DateTimeFormatter.getFormattedDate(rs.getTimestamp("LAST_DEPOSIT_DATE"),   "MM/dd/yy HH:mm"));
        res.LastDepositAmount   = processStr(rs.getString("LAST_DEPOSIT_AMOUNT"));
        res.Phone1              = processStr(rs.getString("PHONE_1"));
        res.Phone2Fax           = processStr(rs.getString("PHONE_2_FAX"));
        res.Add1Line1           = processStr(rs.getString("ADDR1_LINE_1"));
        res.Add1Line2           = processStr(rs.getString("ADDR1_LINE_2"));
        res.Add1City            = processStr(rs.getString("CITY1_LINE_4"));
        res.Add1State           = processStr(rs.getString("STATE1_LINE_4"));
        res.Add1Zip             = processStr(rs.getString("ZIP1_LINE_4"));
        res.Add2Line1           = processStr(rs.getString("ADDR2_LINE_1"));
        res.Add2Line2           = processStr(rs.getString("ADDR2_LINE_2"));
        res.Add2City            = processStr(rs.getString("CITY2_LINE_4"));
        res.Add2State           = processStr(rs.getString("STATE2_LINE_4"));
        res.Add2Zip             = processStr(rs.getString("ZIP2_LINE_4"));
        res.Group1Association   = processStr(rs.getString("GROUP_1_ASSOCIATION"));
        res.Group2Association   = processStr(rs.getString("GROUP_2_ASSOCIATION"));
        res.Group3Association   = processStr(rs.getString("GROUP_3_ASSOCIATION"));
        res.Group4Association   = processStr(rs.getString("GROUP_4_ASSOCIATION"));
        res.Group5Association   = processStr(rs.getString("GROUP_5_ASSOCIATION"));
        res.Group6Association   = processStr(rs.getString("GROUP_6_ASSOCIATION"));
        res.Association         = processStr(rs.getString("DMAGENT"));
        res.MerchantStatus      = processStr(rs.getString("MERCHANT_STATUS"));
        res.DateBilledForClose  = processStr(DateTimeFormatter.getFormattedDate(rs.getTimestamp("DATE_BILLED_FOR_CLOSE"), "MM/dd/yy HH:mm"));

        searchResults.add(res);
      }

      it.close();
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public String processStr(String str)
  {
    String result = "&nbsp;";
    if(str != null && !str.equals(""))
    {
      result = str.trim();
    }
    return result;
  }

  
  /*
  ** ACCESSORS
  */
  public String getLookupValue()
  {
    return lookupValue;
  }
  public void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue.trim();
  }

  public void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
  public boolean isSubmitted()
  {
    return this.submitted;
  }
 
  public boolean hasErrors()
  {
    return error;
  } 
  
  public Vector getSearchResults()
  {
    return this.searchResults;
  }

  public class Result
  {
    public String MerchantNumber      = "";
    public String DbaName             = "";
    public String BankNumber          = "";
    public String DdaNum              = "";
    public String TransitRoutingNum   = "";
    public String FederalTaxId        = "";
    public String SicCode             = "";
    public String ClassCode           = "";
    public String InvestigatorCode    = "";
    public String MetTable            = "";
    public String ActivationDate      = "";
    public String DateOf1stDeposit    = "";
    public String LastDepositDate     = "";
    public String LastDepositAmount   = "";
    public String Phone1              = "";
    public String Phone2Fax           = "";
    public String Add1Line1           = "";
    public String Add1Line2           = "";
    public String Add1City            = "";
    public String Add1State           = "";
    public String Add1Zip             = "";
    public String Add2Line1           = "";
    public String Add2Line2           = "";
    public String Add2City            = "";
    public String Add2State           = "";
    public String Add2Zip             = "";
    public String Group1Association   = "";
    public String Group2Association   = "";
    public String Group3Association   = "";
    public String Group4Association   = "";
    public String Group5Association   = "";
    public String Group6Association   = "";
    public String Association         = "";
    public String MerchantStatus      = "";
    public String DateBilledForClose  = "";
  }
 

}/*@lineinfo:generated-code*/