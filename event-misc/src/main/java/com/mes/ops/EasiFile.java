package com.mes.ops;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EasiFile
{
  public static final String NO_FLAG_FILE         = "";

  private int               LinesWritten          = 0;
  private File              UploadFile            = null;
  private File              FlagFile              = null;
  private FileWriter        Fwriter               = null;
  private boolean           FlagFileExists        = false;
  private String            FileIdentifier        = null;
  private boolean           FileCreated           = false;
  private boolean           FileDeleted           = false;
  private String            FileUniqueIdentifier  = null;
  private boolean           headerWrittenToFile   = false;

  /*
  ** CONSTRUCTOR public DceFileManager()
  **
  ** Default constructor.  Turns off autocommit.
  */
  public EasiFile()
  {}

  /*
  ** CONSTRUCTOR public DceFileManager(String connectionString)
  **
  ** Accepts a connection string.  Turns off autocommit.
  */
  public EasiFile(String fileIdentifier)
  {
    FileIdentifier        = fileIdentifier.trim();
    FileUniqueIdentifier  = "";
  }

  private void createFile() throws Exception
  {
    Calendar     cal          = Calendar.getInstance();
    DateFormat   df           = new SimpleDateFormat("MMddyyhhmmss");

    String storedFileName   = FileIdentifier + "_ERROR_" + df.format(cal.getTime()) + ".txt";
    String currentFileName  = "DISCOVER_TAPEFILE_" + FileIdentifier + ".dat";

    //flag file not currently used
    String flagFileName     = NO_FLAG_FILE;


    File currentFile = new File(currentFileName);
      
/*
    Here we check to see if a file already exists in the working directory.  If so, we
    rename it and move it to the archive old_files directory.  
*/      
    
    if(currentFile.exists())
    {
      File storedFile = new File(storedFileName);
      currentFile.renameTo(storedFile);
      currentFile.delete();
    }

/*    
    Then we create a new file in
    the working directory and associate a new file writer to it.
*/

    UploadFile  = new File(currentFileName);
    if(UploadFile.createNewFile())
    {
      Fwriter     = new FileWriter(UploadFile);
      FileCreated = true;
      FileDeleted = false;
    }
  
/*
    Create flag file or just link to it
*/

    if(!flagFileName.equals(NO_FLAG_FILE))
    {
      FlagFile  = new File(flagFileName);
      if(!FlagFile.exists())
      {
        //this will return true if the file gets created
        FlagFileExists = FlagFile.createNewFile();
      }
      else //it does already exist
      {
        FlagFileExists = true;
      }
    }
  }

  public void writeLineToFile(String[] fileDataLine) throws Exception
  {
    writeLineToFile(fileDataLine, true);
    
  }
  public void writeLineToFile(String[] fileDataLine, boolean countLine) throws Exception
  {
    if(!isFileCreated())
    {
      createFile();
    }

    for(int i=0; i < fileDataLine.length; i++)
    {
      Fwriter.write(fileDataLine[i]);
    }

    Fwriter.write("\n");
    
    if(countLine)
    {
      LinesWritten++;
    }

  }

  public String getFileName()
  {
    String fileName = "";

    try
    {
      if(isFileCreated())
      {
        fileName = UploadFile.getName();
      }
    }
    catch(Exception e)
    {
      System.out.println("getFileName: " + e.toString());
    }
    
    return fileName;
  }

  public String getFilePath()
  {

    String filePath = "";

    try
    {
      if(isFileCreated())
      {
        filePath = UploadFile.getAbsolutePath();
      }
    }
    catch(Exception e)
    {
      System.out.println("getFilePath: " + e.toString());
    }
    
    return filePath;

  }

  public String getFlagFileName()
  {
    String flagFileName = "";

    try
    {
      if(isFlagFileCreated())
      {
        flagFileName = FlagFile.getName();
      }
    }
    catch(Exception e)
    {
      System.out.println("getFlagFileName: " + e.toString());
    }
    
    return flagFileName;
  }

  public String getFlagFilePath()
  {

    String flagFilePath = "";

    try
    {
      if(isFlagFileCreated())
      {
        flagFilePath = FlagFile.getAbsolutePath();
      }
    }
    catch(Exception e)
    {
      System.out.println("getFlagFilePath: " + e.toString());
    }
    
    return flagFilePath;

  }

  public boolean isFlagFileCreated()
  {
    return FlagFileExists;
  }

  public String getFileIdentifier()
  {
    return FileIdentifier;
  }

  public boolean deleteFile()
  {
    try
    {
      if(isFileCreated())
      {
        FileDeleted = UploadFile.delete();
      }
    }
    catch(Exception e)
    {
      System.out.println("deleteFile: " + e.toString());
      //logEntry("deleteFile()",e.toString());
    }

    return FileDeleted;
  }

  public boolean doesFileExist()
  {
    boolean exists = false;

    if(UploadFile != null)
    {
      exists = UploadFile.exists();    
    }
    
    return exists;
  }

  public boolean isFileCreated()
  {
    return this.FileCreated;
  }
  
  public boolean isFileDeleted()
  {
    return this.FileDeleted;
  }

  public int getLinesWritten()
  {
    return this.LinesWritten;
  }

  public void closeFile()
  {
    try
    {
      Fwriter.close();
    }
    catch(Exception e)
    {
      System.out.println("Close File: " + e.toString());
    }
  }

  public String getFileUniqueIdentifier()
  {
    return this.FileUniqueIdentifier;
  }

  public boolean isHeaderWrittenToFile()
  {
    return headerWrittenToFile;
  }

  public void setHeaderWrittenToFile()
  {
    headerWrittenToFile = true;
  }


}
