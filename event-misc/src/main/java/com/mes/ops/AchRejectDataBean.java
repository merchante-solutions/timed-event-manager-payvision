/*@lineinfo:filename=AchRejectDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AchRejectDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/23/04 4:20p $
  Version            : $Revision: 20 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import com.mes.queues.QueueNotes;
import com.mes.queues.QueueTools;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


public class AchRejectDataBean extends SQLJConnectionBase
{
  public static final String NEW                  = "NEW";
  public static final String COLLECTED            = "COLLECTED";
  public static final String REVERSAL_OF_REVENUE  = "REVERSAL_OF_REVENUE"; 
  public static final String PENDING_LOSS         = "PENDING_LOSS";       
  public static final String RISK                 = "RISK";       
  public static final String COLLECTIONS          = "COLLECTIONS";
  public static final String WRITE_OFF            = "WRITE_OFF";
  public static final String LEGAL                = "LEGAL";
  public static final String MANUAL_DCE           = "MANUAL_DCE";
  public static final String FILE_DCE             = "FILE_DCE";
  public static final String AUTO_DCE             = "AUTO_DCE";
  public static final String DUPLICATED           = "DUPLICATED";
  public static final String COMPLETED            = "COMPLETED";
  public static final String NON_RETURNABLE       = "NON_RETURNABLE";


  //Extra statuses to compliment missing collections module - possibly temporary??????
  public static final String PROMISE_TO_PAY       = "PROMISE_TO_PAY";
  public static final String PARTIAL_PAYMENT      = "PARTIAL_PAYMENT";
  public static final String SETTLEMENT           = "SETTLEMENT";
  public static final String BROKEN_PROMISE       = "BROKEN_PROMISE";


  public Vector   rejectHistory   = new Vector();
  public RowData  currentReject   = null;

  private double  dollarAmount    = 0.0;
  
  public class RowData
  {
    public String       RejectSeqNum        = null;
    public String       Adden1              = null;
    public String       Adden2              = null;
    public String       DbaName             = null;
    public String       Dda                 = null;
    public String       Description         = null;
    public String       MerchantId          = null;
    public String       ReasonCode          = null;
    public String       CodeDesc            = null;
    public String       ReportDate          = null;
    public String       CompletedDate       = null;
    public String       RejectStatus        = null;
    public String       DateStatusUpdated   = null;
    public String       SourceStatusUpdated = null;
    public String       AppType             = null;
    public int          TransactionCode     = 0;
    public int          NumNotes            = 0;
    public int          QueueType           = -1;
    public String       AmountString        = null;
    public double       Amount              = 0.0;
    public String       Association         = null;
    public String       TransitRtgNum       = null;

    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      Adden1            = processString(resultSet.getString("adden_1"));
      Adden2            = processString(resultSet.getString("adden_2"));
      DbaName           = processString(resultSet.getString("dba_name"));
      Dda               = processString(resultSet.getString("dda"));
      Description       = processString(resultSet.getString("description"));
      MerchantId        = processString(resultSet.getString("merchant_number"));
      ReasonCode        = processString(resultSet.getString("reason_code"));
      CodeDesc          = processString(resultSet.getString("code_desc"));
      ReportDate        = processString(DateTimeFormatter.getFormattedDate(resultSet.getDate("report_date"), "MM/dd/yyyy"));
      AppType           = processString(resultSet.getString("app_type"));
      
      if(resultSet.getDate("completed_date") != null)
      {
        CompletedDate   = processString(DateTimeFormatter.getFormattedDate(resultSet.getDate("completed_date"), "MM/dd/yyyy"));
      }
      else
      {
        CompletedDate   = "open";
      }

      if(resultSet.getString("reject_status") == null)
      {
        RejectStatus        = "NEW";
        SourceStatusUpdated = "SYSTEM";
        if(resultSet.getDate("created_date") != null)
        {
          DateStatusUpdated   = processString(DateTimeFormatter.getFormattedDate(resultSet.getDate("created_date"), "MM/dd/yyyy"));
        }
        else
        {
          DateStatusUpdated   = processString(DateTimeFormatter.getFormattedDate(resultSet.getDate("report_date"), "MM/dd/yyyy"));
        }
      }
      else
      {
        RejectStatus        = processString(resultSet.getString("reject_status"));
        SourceStatusUpdated = processString(resultSet.getString("source_status_updated"));
        DateStatusUpdated   = processString(DateTimeFormatter.getFormattedDate(resultSet.getDate("date_status_updated"), "MM/dd/yyyy"));
      }

      Association       = resultSet.getString("association");
      TransitRtgNum     = resultSet.getString("transit_rtg_num");
      NumNotes          = resultSet.getInt("num_notes");
      TransactionCode   = resultSet.getInt("transaction_code");
      Amount            = resultSet.getDouble("amount");
      AmountString      = MesMath.toCurrency(resultSet.getDouble("amount"));
      RejectSeqNum      = processString(resultSet.getString("reject_seq_num"));
      
      if(resultSet.getString("queue_type") != null && !resultSet.getString("queue_type").equals(""))
      {
        QueueType       = resultSet.getInt("queue_type");
      }

    }
  }
  
  public AchRejectDataBean( )
  {
  }
  
  public boolean loadData(String rejectSeqNum)
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    boolean                       result            = false;

    try
    {

      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:182^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    ar.merchant_number      merchant_number,
//                    ar.reason_code          reason_code,
//                    arrc.description        code_desc,
//                    ar.dda                  dda,
//                    ar.transit_routing      transit_rtg_num,
//                    ar.transaction_code     transaction_code,
//                    ar.merchant_number      merchant_number,
//                    ar.merchant_name        dba_name,
//                    ar.entry_desc           description,
//  --                  ar.report_date          report_date,
//                    ar.settled_date         report_date,
//                    ar.adden_1              adden_1,
//                    ar.adden_2              adden_2,
//                    ar.amount               amount,
//                    ar.reject_seq_num       reject_seq_num,
//                    ar.effective_date       created_date,
//                    qd.last_changed         completed_date,
//                    qd.type                 queue_type,
//                    ars.reject_status       reject_status,
//                    ars.date_last_updated   date_status_updated,
//                    ars.source_last_updated source_status_updated,
//                    get_queue_note_count(ar.reject_seq_num,qd.type)    num_notes,
//                    decode(a.appsrctype_code, null, 'MESN', a.appsrctype_code) app_type,
//                    mi.dmagent              association
//         
//          from      ach_rejects             ar,
//                    q_data                  qd,
//                    q_data                  qd2,
//                    merchant                m,
//                    ach_reject_reason_codes arrc,
//                    ach_reject_status       ars,
//                    application             a,
//                    mif                     mi
//  
//          where     ar.reject_seq_num   = :rejectSeqNum         and
//                    ar.merchant_number  = mi.merchant_number    and
//                    ar.reject_seq_num   = ars.reject_seq_num(+) and
//                    ar.reason_code      = arrc.reason_code(+)   and
//                    ar.reject_seq_num   = qd.id(+)              and qd.item_type in (11,14,15,16,21) and
//                    (ar.merchant_number = m.merch_number(+)     and m.app_seq_num = a.app_seq_num(+)) 
//  
//          group by  ar.merchant_number,
//                    ar.reason_code,
//                    arrc.description,
//                    ar.dda,
//                    ar.transit_routing,
//                    ar.transaction_code,
//                    ar.merchant_number,
//                    ar.merchant_name,
//                    ar.entry_desc,
//  --                  ar.report_date,
//                    ar.settled_date,
//                    ar.adden_1,
//                    ar.adden_2,
//                    ar.amount,
//                    ar.reject_seq_num,
//                    ar.effective_date,
//                    qd.last_changed,
//                    qd.type,
//                    ars.reject_status,
//                    ars.date_last_updated,
//                    ars.source_last_updated,
//                    a.appsrctype_code,
//                    mi.dmagent
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ar.merchant_number      merchant_number,\n                  ar.reason_code          reason_code,\n                  arrc.description        code_desc,\n                  ar.dda                  dda,\n                  ar.transit_routing      transit_rtg_num,\n                  ar.transaction_code     transaction_code,\n                  ar.merchant_number      merchant_number,\n                  ar.merchant_name        dba_name,\n                  ar.entry_desc           description,\n--                  ar.report_date          report_date,\n                  ar.settled_date         report_date,\n                  ar.adden_1              adden_1,\n                  ar.adden_2              adden_2,\n                  ar.amount               amount,\n                  ar.reject_seq_num       reject_seq_num,\n                  ar.effective_date       created_date,\n                  qd.last_changed         completed_date,\n                  qd.type                 queue_type,\n                  ars.reject_status       reject_status,\n                  ars.date_last_updated   date_status_updated,\n                  ars.source_last_updated source_status_updated,\n                  get_queue_note_count(ar.reject_seq_num,qd.type)    num_notes,\n                  decode(a.appsrctype_code, null, 'MESN', a.appsrctype_code) app_type,\n                  mi.dmagent              association\n       \n        from      ach_rejects             ar,\n                  q_data                  qd,\n                  q_data                  qd2,\n                  merchant                m,\n                  ach_reject_reason_codes arrc,\n                  ach_reject_status       ars,\n                  application             a,\n                  mif                     mi\n\n        where     ar.reject_seq_num   =  :1          and\n                  ar.merchant_number  = mi.merchant_number    and\n                  ar.reject_seq_num   = ars.reject_seq_num(+) and\n                  ar.reason_code      = arrc.reason_code(+)   and\n                  ar.reject_seq_num   = qd.id(+)              and qd.item_type in (11,14,15,16,21) and\n                  (ar.merchant_number = m.merch_number(+)     and m.app_seq_num = a.app_seq_num(+)) \n\n        group by  ar.merchant_number,\n                  ar.reason_code,\n                  arrc.description,\n                  ar.dda,\n                  ar.transit_routing,\n                  ar.transaction_code,\n                  ar.merchant_number,\n                  ar.merchant_name,\n                  ar.entry_desc,\n--                  ar.report_date,\n                  ar.settled_date,\n                  ar.adden_1,\n                  ar.adden_2,\n                  ar.amount,\n                  ar.reject_seq_num,\n                  ar.effective_date,\n                  qd.last_changed,\n                  qd.type,\n                  ars.reject_status,\n                  ars.date_last_updated,\n                  ars.source_last_updated,\n                  a.appsrctype_code,\n                  mi.dmagent";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AchRejectDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,rejectSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AchRejectDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:248^7*/

      
      resultSet = it.getResultSet();
    
      if( resultSet.next() )
      {
        currentReject = new RowData( resultSet );
        result = true;
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadData", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }
    
    if(currentReject != null)
    {
      loadRejectHistory();
    }

    return result;
  }
  
  private void loadRejectHistory()
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      connect();

      // clear out vector to store reject history.
      rejectHistory.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:290^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    ar.merchant_number      merchant_number,
//                    ar.reason_code          reason_code,
//                    arrc.description        code_desc,
//                    ar.dda                  dda,
//                    ar.transit_routing      transit_rtg_num,
//                    ar.transaction_code     transaction_code,
//                    ar.merchant_number      merchant_number,
//                    ar.merchant_name        dba_name,
//                    ar.entry_desc           description,
//  --                  ar.report_date          report_date,
//                    ar.settled_date         report_date,
//                    ar.adden_1              adden_1,
//                    ar.adden_2              adden_2,
//                    ar.amount               amount,
//                    ar.reject_seq_num       reject_seq_num,
//                    qd.last_changed         completed_date,
//                    ar.effective_date       created_date,
//                    qd.type                 queue_type,
//                    ars.reject_status       reject_status,
//                    ars.date_last_updated   date_status_updated,
//                    ars.source_last_updated source_status_updated,
//                    get_queue_note_count(ar.reject_seq_num,qd.type) num_notes,
//                    decode(a.appsrctype_code, null, 'MESN', a.appsrctype_code) app_type,
//                    mi.dmagent              association
//          
//          from      ach_rejects             ar,
//                    q_data                  qd,
//                    merchant                m,
//                    ach_reject_reason_codes arrc,
//                    ach_reject_status       ars,
//                    application             a,
//                    mif                     mi
//          
//          where     ar.merchant_number  = :currentReject.MerchantId   and
//                    ar.merchant_number  = mi.merchant_number            and
//                    ar.reject_seq_num   = ars.reject_seq_num(+)         and
//                    ar.reason_code      = arrc.reason_code(+)           and
//                    ar.reject_seq_num   = qd.id(+) and qd.item_type in (11,14,15,16) and
//                    (ar.merchant_number = m.merch_number(+)             and m.app_seq_num = a.app_seq_num(+)) 
//  
//          group by  ar.merchant_number,
//                    ar.reason_code,
//                    arrc.description,
//                    ar.dda,
//                    ar.transit_routing,
//                    ar.transaction_code,
//                    ar.merchant_number,
//                    ar.merchant_name,
//                    ar.entry_desc,
//  --                  ar.report_date,
//                    ar.settled_date,
//                    ar.adden_1,
//                    ar.adden_2,
//                    ar.amount,
//                    ar.reject_seq_num,
//                    ars.reject_status,
//                    ars.date_last_updated,
//                    ars.source_last_updated,
//                    qd.last_changed,
//                    ar.effective_date,
//                    qd.type,
//                    a.appsrctype_code,
//                    mi.dmagent
//  
//  --        order by  ar.report_date DESC
//          order by  ar.settled_date DESC
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ar.merchant_number      merchant_number,\n                  ar.reason_code          reason_code,\n                  arrc.description        code_desc,\n                  ar.dda                  dda,\n                  ar.transit_routing      transit_rtg_num,\n                  ar.transaction_code     transaction_code,\n                  ar.merchant_number      merchant_number,\n                  ar.merchant_name        dba_name,\n                  ar.entry_desc           description,\n--                  ar.report_date          report_date,\n                  ar.settled_date         report_date,\n                  ar.adden_1              adden_1,\n                  ar.adden_2              adden_2,\n                  ar.amount               amount,\n                  ar.reject_seq_num       reject_seq_num,\n                  qd.last_changed         completed_date,\n                  ar.effective_date       created_date,\n                  qd.type                 queue_type,\n                  ars.reject_status       reject_status,\n                  ars.date_last_updated   date_status_updated,\n                  ars.source_last_updated source_status_updated,\n                  get_queue_note_count(ar.reject_seq_num,qd.type) num_notes,\n                  decode(a.appsrctype_code, null, 'MESN', a.appsrctype_code) app_type,\n                  mi.dmagent              association\n        \n        from      ach_rejects             ar,\n                  q_data                  qd,\n                  merchant                m,\n                  ach_reject_reason_codes arrc,\n                  ach_reject_status       ars,\n                  application             a,\n                  mif                     mi\n        \n        where     ar.merchant_number  =  :1    and\n                  ar.merchant_number  = mi.merchant_number            and\n                  ar.reject_seq_num   = ars.reject_seq_num(+)         and\n                  ar.reason_code      = arrc.reason_code(+)           and\n                  ar.reject_seq_num   = qd.id(+) and qd.item_type in (11,14,15,16) and\n                  (ar.merchant_number = m.merch_number(+)             and m.app_seq_num = a.app_seq_num(+)) \n\n        group by  ar.merchant_number,\n                  ar.reason_code,\n                  arrc.description,\n                  ar.dda,\n                  ar.transit_routing,\n                  ar.transaction_code,\n                  ar.merchant_number,\n                  ar.merchant_name,\n                  ar.entry_desc,\n--                  ar.report_date,\n                  ar.settled_date,\n                  ar.adden_1,\n                  ar.adden_2,\n                  ar.amount,\n                  ar.reject_seq_num,\n                  ars.reject_status,\n                  ars.date_last_updated,\n                  ars.source_last_updated,\n                  qd.last_changed,\n                  ar.effective_date,\n                  qd.type,\n                  a.appsrctype_code,\n                  mi.dmagent\n\n--        order by  ar.report_date DESC\n        order by  ar.settled_date DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.AchRejectDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,currentReject.MerchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.AchRejectDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:359^7*/
      
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        rejectHistory.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadRejectHistory", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      cleanUp();
    }
  }

  private void setRejectDollarAmount(long rejectSeqNum)
  {
    ResultSetIterator      it                = null;
    ResultSet              resultSet         = null;
    
    try
    {
      connect();
        

      /*@lineinfo:generated-code*//*@lineinfo:390^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    ar.amount dollar_amount
//          from      ach_rejects ar
//          where     ar.REJECT_SEQ_NUM  = :rejectSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ar.amount dollar_amount\n        from      ach_rejects ar\n        where     ar.REJECT_SEQ_NUM  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AchRejectDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,rejectSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.AchRejectDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:395^7*/


      resultSet = it.getResultSet();
    
      if( resultSet.next() )
      {
        dollarAmount = resultSet.getDouble("dollar_amount");
      }

      it.close();
       
    }  
    catch(Exception e)
    {
      logEntry("setRejectDollarAmount()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  private Vector getRejectSeqNums(int queueType, String merchantNum)
  {
    ResultSetIterator      it                = null;
    ResultSet              resultSet         = null;
    Vector                 rejectSeqNums     = new Vector();

    try
    {
      connect();
        

      /*@lineinfo:generated-code*//*@lineinfo:430^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    sum(ar.amount) dollar_amount
//  
//          from      ach_rejects ar, 
//                    q_data      qd 
//  
//          where     ar.MERCHANT_NUMBER = :merchantNum   and
//                    ar.REJECT_SEQ_NUM  = qd.ID          and
//                    qd.TYPE            = :queueType      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sum(ar.amount) dollar_amount\n\n        from      ach_rejects ar, \n                  q_data      qd \n\n        where     ar.MERCHANT_NUMBER =  :1    and\n                  ar.REJECT_SEQ_NUM  = qd.ID          and\n                  qd.TYPE            =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AchRejectDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   __sJT_st.setInt(2,queueType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.AchRejectDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:440^7*/


      resultSet = it.getResultSet();
    
      if( resultSet.next() )
      {
        dollarAmount = resultSet.getDouble("dollar_amount");
      }

      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:452^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    ar.REJECT_SEQ_NUM
//  
//          from      ach_rejects ar, 
//                    q_data      qd 
//  
//          where     ar.MERCHANT_NUMBER = :merchantNum   and
//                    ar.REJECT_SEQ_NUM  = qd.ID          and
//                    qd.TYPE            = :queueType      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ar.REJECT_SEQ_NUM\n\n        from      ach_rejects ar, \n                  q_data      qd \n\n        where     ar.MERCHANT_NUMBER =  :1    and\n                  ar.REJECT_SEQ_NUM  = qd.ID          and\n                  qd.TYPE            =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.AchRejectDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   __sJT_st.setInt(2,queueType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.AchRejectDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:462^7*/


      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        rejectSeqNums.add(resultSet.getString("reject_seq_num"));
      }

      it.close();   // this will also close the resultSet
        
    }  
    catch(Exception e)
    {
      logEntry("getRejectSeqNums()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return rejectSeqNums;
  }

  private int getCompletedQueue(int queueType)
  {
    int completedQueueType  = -1;
    int queueItemType       = QueueTools.getQueueItemType(queueType);
    
    switch(queueItemType)
    {
      case MesQueues.Q_ITEM_TYPE_ACH_REJECTS:
      case MesQueues.Q_ITEM_TYPE_NON_RETURNABLE:
        completedQueueType = MesQueues.Q_ACH_REJECT_COMPLETED;
      break;
      
      case MesQueues.Q_ITEM_TYPE_RISK:
        completedQueueType = MesQueues.Q_RISK_COMPLETED;
      break;
      
      case MesQueues.Q_ITEM_TYPE_COLLECTIONS:
        completedQueueType = MesQueues.Q_COLLECTIONS_COMPLETED;
      break;
      
      case MesQueues.Q_ITEM_TYPE_LEGAL:
        completedQueueType = MesQueues.Q_LEGAL_COMPLETED;
      break;
    }

    return completedQueueType;
  }

  private void moveQueues(long rejectSeqNum, int queueType, String achStatus, UserBean user)
  {

/*
    if(achStatus.equals(COLLECTED) || achStatus.equals(REVERSAL_OF_REVENUE)  || achStatus.equals(WRITE_OFF))
    {
      //move to the completed of which ever item type we are in (i.e. ach completed, risk complete, collections completed, legal completed)
      int completedQueueType = getCompletedQueue(queueType);
      QueueTools.moveQueueItem(rejectSeqNum, queueType, completedQueueType, user, "ACH reject moved to " + QueueTools.getQueueDescription(completedQueueType));
    }
    else if(achStatus.equals(RISK))
    {
      //move to risk
      QueueTools.moveQueueItem(rejectSeqNum, queueType, MesQueues.Q_RISK_NEW, user, "ACH reject moved to risk queue");
    }
    else if(achStatus.equals(COLLECTIONS))
    {
      //move to collections
      QueueTools.moveQueueItem(rejectSeqNum, queueType, MesQueues.Q_COLLECTIONS_NEW, user, "ACH reject moved to collections queue");
    }
    else if(achStatus.equals(LEGAL))
    {
      //move to legal queue
      QueueTools.moveQueueItem(rejectSeqNum, queueType, MesQueues.Q_LEGAL_NEW, user, "ACH reject moved to legal queue");
    }
    else if(achStatus.equals(MANUAL_DCE))
    {
      //move to completed
      QueueTools.moveQueueItem(rejectSeqNum, queueType, MesQueues.Q_ACH_REJECT_COMPLETED, user, "ACH reject moved to ach completed");
    }
    else if(achStatus.equals(FILE_DCE))
    {
      //move to completed
      QueueTools.moveQueueItem(rejectSeqNum, queueType, MesQueues.Q_ACH_REJECT_COMPLETED, user, "ACH reject moved to ach completed");
    }
    else if(achStatus.equals(DUPLICATED))
    {
      //move to completed
      QueueTools.moveQueueItem(rejectSeqNum, queueType, MesQueues.Q_ACH_REJECT_COMPLETED, user, "ACH reject moved to ach completed");
    }
*/

    if(achStatus.equals(COLLECTED)            || 
       achStatus.equals(COMPLETED)            ||
       achStatus.equals(REVERSAL_OF_REVENUE)  || 
       achStatus.equals(MANUAL_DCE)           || 
       achStatus.equals(FILE_DCE)             || 
       achStatus.equals(DUPLICATED))
    {
      //move to the completed of which ever item type we are in (i.e. ach completed, risk complete, collections completed, legal completed)
      int completedQueueType = getCompletedQueue(queueType);
      QueueTools.moveQueueItem(rejectSeqNum, queueType, completedQueueType, user, "ACH reject moved to " + QueueTools.getQueueDescription(completedQueueType));
    }
    else if(achStatus.equals(RISK))
    {
      //move to risk
      QueueTools.moveQueueItem(rejectSeqNum, queueType, MesQueues.Q_RISK_NEW, user, "ACH reject moved to risk queue");
    }
    else if(achStatus.equals(COLLECTIONS))
    {
      //move to collections
      QueueTools.moveQueueItem(rejectSeqNum, queueType, MesQueues.Q_COLLECTIONS_NEW, user, "ACH reject moved to collections queue");
    }
    else if(achStatus.equals(LEGAL) || achStatus.equals(WRITE_OFF))
    {
      //move to legal queue
      QueueTools.moveQueueItem(rejectSeqNum, queueType, MesQueues.Q_LEGAL_NEW, user, "ACH reject moved to legal queue");
    }
    else if(achStatus.equals(NON_RETURNABLE))
    {
      //move to non-turnable queue
      QueueTools.moveQueueItem(rejectSeqNum, queueType, MesQueues.Q_NON_RETURNABLE_NEW, user, "ACH reject moved to non-returnable queue");
    }
  }


  public void updateRejectStatus(long rejectSeqNum, int queueType, String achStatus, UserBean user, String merchantNum, boolean updateAllOpen)
  {
    try
    {
      Vector rejectSeqNums = null;

      // if they want to update all open in this queue.. we get all the reject_seq_num's
      if(updateAllOpen)
      {
        rejectSeqNums = getRejectSeqNums(queueType, merchantNum);
      }
      else //else we just get the current reject_seq_num and update that one alone
      {
        rejectSeqNums = new Vector();
        rejectSeqNums.add(Long.toString(rejectSeqNum));
        setRejectDollarAmount(rejectSeqNum);
      }

      for(int x=0; x<rejectSeqNums.size(); x++)
      {
        moveQueues(Long.parseLong((String)rejectSeqNums.elementAt(x)), queueType, achStatus, user);
        updateRejectStatus(Long.parseLong((String)rejectSeqNums.elementAt(x)), achStatus, user);
      }

      //then we can put 1 note in call tracking and 1 note for each entry in the q_notes
    
      String note = "Status changed to " + achStatus;
      
      if(updateAllOpen)
      {
        note += " for all open items in ";
      }
      else
      {
        note += " for reject sequence number " + rejectSeqNum + " in ";
      }

      note += QueueTools.getQueueDescription(queueType) + " queue. " + "<a href=\"/jsp/credit/org_ach_reject.jsp?reject_seq_num=" + rejectSeqNum + "\" target=\"_blank\">View</a>";

      if(updateAllOpen)
      {
        note += " Net Dollar Amount for all items = " + MesMath.toCurrency(dollarAmount);
      }
      else
      {
        note += " Dollar Amount for this item = " + MesMath.toCurrency(dollarAmount);
      }


      for(int x=0; x<rejectSeqNums.size(); x++)
      {
        QueueNotes.addNote(Long.parseLong((String)rejectSeqNums.elementAt(x)), queueType, user.getLoginName(), note, (Long.parseLong((String)rejectSeqNums.elementAt(x)) == rejectSeqNum));
        if((Long.parseLong((String)rejectSeqNums.elementAt(x)) == rejectSeqNum))
        {
          //System.out.println((String)rejectSeqNums.elementAt(x));
        }
      }
    
    }
    catch(Exception e)
    {
      logEntry("updateRejectStatus-public", e.toString());
    }
  }



  private void updateRejectStatus(long rejectSeqNum, String achStatus, UserBean user)
  {
    int     statusExists  = -1;
    boolean useCleanUp    = false;

    try
    {
      //System.out.println("in updateRejectStatus");
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
        
      /*@lineinfo:generated-code*//*@lineinfo:672^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(reject_seq_num)
//          
//          from    ach_reject_status
//          where   reject_seq_num = :rejectSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(reject_seq_num)\n         \n        from    ach_reject_status\n        where   reject_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.AchRejectDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,rejectSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   statusExists = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:678^7*/
        
      if(statusExists > 0) //then update
      {

        /*@lineinfo:generated-code*//*@lineinfo:683^9*/

//  ************************************************************
//  #sql [Ctx] { update  ach_reject_status
//            set     reject_status       = :achStatus,
//                    date_last_updated   = sysdate,
//                    source_last_updated = :user.getLoginName()
//            where   reject_seq_num      = :rejectSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_629 = user.getLoginName();
   String theSqlTS = "update  ach_reject_status\n          set     reject_status       =  :1 ,\n                  date_last_updated   = sysdate,\n                  source_last_updated =  :2 \n          where   reject_seq_num      =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.ops.AchRejectDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,achStatus);
   __sJT_st.setString(2,__sJT_629);
   __sJT_st.setLong(3,rejectSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:690^9*/

      }
      else //insert
      {

        /*@lineinfo:generated-code*//*@lineinfo:696^9*/

//  ************************************************************
//  #sql [Ctx] { insert into  ach_reject_status
//            (
//              reject_seq_num,
//              reject_status,
//              date_last_updated,
//              source_last_updated
//            )
//            values
//            (
//              :rejectSeqNum,
//              :achStatus,
//              sysdate,
//              :user.getLoginName()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_630 = user.getLoginName();
   String theSqlTS = "insert into  ach_reject_status\n          (\n            reject_seq_num,\n            reject_status,\n            date_last_updated,\n            source_last_updated\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n            sysdate,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ops.AchRejectDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,rejectSeqNum);
   __sJT_st.setString(2,achStatus);
   __sJT_st.setString(3,__sJT_630);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:712^9*/

      }
    
    }  
    catch(Exception e)
    {
      logEntry("updateRejectStatus(" + rejectSeqNum + ")", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  
  }



  
}/*@lineinfo:generated-code*/