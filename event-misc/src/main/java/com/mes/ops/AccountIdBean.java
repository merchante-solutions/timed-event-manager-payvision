/*@lineinfo:filename=AccountIdBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AccountIdBean.java $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-15 10:17:10 -0700 (Tue, 15 Jul 2008) $
  Version            : $Revision: 15087 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.io.Serializable;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class AccountIdBean extends com.mes.screens.SequenceIdBean
  implements Serializable
{
  private long    merchantNumber      = 0L;
  private long    appControlNum       = 0L;
  private String  merchantName        = "";
  private String  startDate           = "";
  private String  startTime           = "";
  private long    appUser             = 0L;
  private String  appType             = "";
  private int     integerAppType      = 0;
  private String  statusDescription   = "";
  private String  appSource           = "";
  private int     queueType           = 0;
  private int     queueStage          = 0;
  private String  ownerName           = "";

  /*
  ** METHOD getData
  */
  public synchronized void getData(long sequenceId, long primaryKey)
  {
    ResultSetIterator it          = null;
    ResultSet         rs          = null;
    SimpleDateFormat  simpleDate  = new SimpleDateFormat("MM/dd/yyyy");
    SimpleDateFormat  simpleTime  = new SimpleDateFormat("hh:mm:ss");

    try
    {
      connect();

      super.getData(sequenceId, primaryKey);

      switch((int)sequenceId)
      {
        case (int)mesConstants.SEQUENCE_CREDIT_EVAL:
          queueType = QueueConstants.QUEUE_CREDIT;
          break;
        case (int)mesConstants.SEQUENCE_DATA_EXPAND:
          queueType = QueueConstants.QUEUE_SETUP;
          break;
        case (int)mesConstants.SEQUENCE_EQUIPMENT_DEPLOYMENT:
          queueType = QueueConstants.QUEUE_EQUIPMENT;
          break;
      }

      /*@lineinfo:generated-code*//*@lineinfo:88^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.merch_number          merch_number,
//                  merch.merc_cntrl_number     merc_cntrl_number,
//                  merch.merch_business_name   merch_business_name,
//                  app.app_start_date          app_start_date,
//                  app.app_user                app_user,
//                  app.app_source              app_source,
//                  app.app_queue_stage         app_queue_stage,
//                  appl.appsrctype_code        appsrctype_code,
//                  appl.app_type               app_type,
//                  status.status_short_desc    status_short_desc
//          from    merchant    merch,
//                  app_queue   app,
//                  application appl,
//                  app_status  status
//          where   merch.app_seq_num = :primaryKey and
//                  merch.app_seq_num = app.app_seq_num and
//                  app.app_status = status.status_type and
//                  app.app_queue_type = :queueType and
//                  app.app_seq_num = appl.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.merch_number          merch_number,\n                merch.merc_cntrl_number     merc_cntrl_number,\n                merch.merch_business_name   merch_business_name,\n                app.app_start_date          app_start_date,\n                app.app_user                app_user,\n                app.app_source              app_source,\n                app.app_queue_stage         app_queue_stage,\n                appl.appsrctype_code        appsrctype_code,\n                appl.app_type               app_type,\n                status.status_short_desc    status_short_desc\n        from    merchant    merch,\n                app_queue   app,\n                application appl,\n                app_status  status\n        where   merch.app_seq_num =  :1  and\n                merch.app_seq_num = app.app_seq_num and\n                app.app_status = status.status_type and\n                app.app_queue_type =  :2  and\n                app.app_seq_num = appl.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AccountIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,queueType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AccountIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:109^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        merchantNumber    = rs.getLong  ("merch_number");
        appControlNum     = rs.getLong  ("merc_cntrl_number");
        merchantName      = rs.getString("merch_business_name");
        startDate         = simpleDate.format(rs.getDate("app_start_date"));
        startTime         = simpleTime.format(rs.getTime("app_start_date"));
        appUser           = rs.getLong  ("app_user");
        appType           = rs.getString("appsrctype_code");
        integerAppType    = rs.getInt("app_type");
        appSource         = rs.getString("app_source");
        statusDescription = rs.getString("status_short_desc");
        queueStage        = rs.getInt   ("app_queue_stage");
      }
      else
      {
        bypassQueue(primaryKey);
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:135^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_first_name,
//                  busowner_last_name
//          from    businessowner
//          where   app_seq_num = :primaryKey and
//                  busowner_num = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_first_name,\n                busowner_last_name\n        from    businessowner\n        where   app_seq_num =  :1  and\n                busowner_num = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.AccountIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.AccountIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:142^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        String firstName      = isBlank(rs.getString("busowner_first_name")) ? "" : rs.getString("busowner_first_name");
        String lastName       = isBlank(rs.getString("busowner_last_name")) ? "" : rs.getString("busowner_last_name");
        this.ownerName        = firstName + " " + lastName;
        this.ownerName        = this.ownerName.trim();
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getData(" + sequenceId + ", " + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    // get the merchant data specific to this application
  }

  protected void bypassQueue(long primaryKey)
  {
    ResultSetIterator it          = null;
    ResultSet         rs          = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:180^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_number,
//                  merc_cntrl_number,
//                  merch_business_name
//          from    merchant
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_number,\n                merc_cntrl_number,\n                merch_business_name\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AccountIdBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.AccountIdBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:187^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        merchantNumber    = rs.getLong  ("merch_number");
        appControlNum     = rs.getLong  ("merc_cntrl_number");
        merchantName      = rs.getString("merch_business_name");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("bypassQueue(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  /*
  ** METHOD getNextPage overriden from base class. Return user to menu if they cancel an app
  */
  public String getNextPage(long fromScreenId, boolean cancelled)
  {
    String result = "";
    if(cancelled)
    {
      result = "/jsp/credit/sup_mes_main.jsp";
    }
    else
    {
      result = super.getNextPage(fromScreenId);
    }
    return result;
  }

  public synchronized long getMerchantNumber()
  {
    return this.merchantNumber;
  }
  public synchronized String getOwnerName()
  {
    return this.ownerName;
  }
  public void setMerchantNumber(String merchantNumber)
  {
    try
    {
      setMerchantNumber(Long.parseLong(merchantNumber));
    }
    catch(Exception e)
    {
    }
  }
  public synchronized void setMerchantNumber(long merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  public synchronized long getAppControlNum()
  {
    return this.appControlNum;
  }
  public synchronized String getMerchantName()
  {
    return this.merchantName;
  }
  public synchronized String getStartDate()
  {
    return this.startDate;
  }
  public synchronized String getStartTime()
  {
    return this.startTime;
  }
  public synchronized long getAppUser()
  {
    return this.appUser;
  }
  public synchronized String getStatusDescription()
  {
    return this.statusDescription;
  }
  public synchronized String getAppSource()
  {
    return this.appSource;
  }
  public synchronized String getAppType()
  {
    return this.appType;
  }
  public synchronized int getIntegerAppType()
  {
    return this.integerAppType;
  }
  public synchronized int getQueueType()
  {
    return this.queueType;
  }
  public synchronized int getQueueStage()
  {
    return this.queueStage;
  }
}/*@lineinfo:generated-code*/