/*@lineinfo:filename=CreditStatusBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/CreditStatusBean.sqlj $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-07 15:59:41 -0700 (Mon, 07 Jul 2008) $
  Version            : $Revision: 15046 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.queues.QueueTools;
import com.mes.tools.AppNotifyBean;
import com.mes.user.UserBean;


public class CreditStatusBean extends com.mes.screens.SequenceDataBean
{
  private UserBean      userLogin             = null;
  
  // bean data
  private long          documents             = 0L;
  private String        miscDoc               = "";
  private int           status                = 0;
  
  private String        reason                = "";
  private boolean       cancelled             = false;
  private boolean       override              = false;

  private String        appSource             = "";
  private long          userId                = 0L;
  private String        userName              = "";
  private int           approvedAppType       = 0;
  private String        tier2Checked          = "";
  private String        isTier2               = "N";
  
  // option data
  private Vector        documentTypes         = new Vector();
  private Vector        documentDescriptions  = new Vector();
  private Vector        declineReasons        = new Vector();
  private Vector        declineDescriptions   = new Vector();
  private Vector        pendingReasons        = new Vector();
  private Vector        pendingDescriptions   = new Vector();
  private Vector        cancelReasons         = new Vector();
  private Vector        cancelDescriptions    = new Vector();
  
  private ReasonHelperBean declineReas        = new ReasonHelperBean();
  private ReasonHelperBean pendReas           = new ReasonHelperBean();


  private void loadOptionData()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    // document types
    try
    {
      qs.append("select ");
      qs.append("app_doc_code, ");
      qs.append("app_doc_desc, ");
      qs.append("app_doc_required ");
      qs.append("from app_doc ");
      qs.append("order by app_doc_code asc");
      
      ps = getPreparedStatement(qs.toString());
      rs = ps.executeQuery();
      
      while(rs.next())
      {
        documentTypes.add(rs.getLong("app_doc_code"));
        documentDescriptions.add(rs.getString("app_doc_desc"));
        
        if(rs.getInt("app_doc_required") > 0)
        {
          documents |= rs.getLong("app_doc_code");
        }
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "loadOptionData (doc): " + e.toString());
    }
    
    // decline/pending reasons
    try
    {
      cancelReasons.add("");
      cancelDescriptions.add("select from list");
      
      qs.setLength(0);
      qs.append("select ");
      qs.append("qreason_code, ");
      qs.append("qreason_type, ");
      qs.append("qreason_short_descr ");
      qs.append("from qreason ");
      qs.append("order by qreason_code asc");
      
      ps = getPreparedStatement(qs.toString());
      rs = ps.executeQuery();
      while(rs.next())
      {
        if(rs.getInt("qreason_type") == QueueConstants.Q_STATUS_DECLINED)
        {
          declineReasons.add(rs.getString("qreason_code"));
          declineDescriptions.add(rs.getString("qreason_code") + " " + rs.getString("qreason_short_descr"));
        }
        else if (rs.getInt("qreason_type") == QueueConstants.Q_STATUS_PENDING)
        {
          pendingReasons.add(rs.getString("qreason_code"));
          pendingDescriptions.add(rs.getString("qreason_code") + " " + rs.getString("qreason_short_descr"));
        }
        else if (rs.getInt("qreason_type") == QueueConstants.Q_STATUS_CANCELLED)
        {
          cancelReasons.add(rs.getString("qreason_code"));
          cancelDescriptions.add(rs.getString("qreason_code") + " " + rs.getString("qreason_short_descr"));
        }
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "loadOptionData (reason): " + e.toString());
    }
  }
  
  public CreditStatusBean()
  {
    loadOptionData();
  }
  
  public void updateData(HttpServletRequest aReq)
  {
    if(!isBlank(aReq.getParameter("submit")) && (aReq.getParameter("submit")).equals("Decline"))
    {
      for(int i=0; i<declineReasons.size(); i++)
      {
        if(!isBlank(aReq.getParameter("decline" + i)))
        {    
          if(isBlank(reason))
          {
            reason = aReq.getParameter("decline" + i);
          }
          else
          {
            reason += ("," + aReq.getParameter("decline" + i));
          }
        }
      }
      declineReas.setReasons(QueueConstants.Q_STATUS_DECLINED, reason);
    }
    else if(!isBlank(aReq.getParameter("submit")) && (aReq.getParameter("submit")).equals("Pend"))
    {
      for(int i=0; i<pendingReasons.size(); i++)
      {
        if(!isBlank(aReq.getParameter("pend" + i)))
        {    
          if(isBlank(reason))
          {
            reason = aReq.getParameter("pend" + i);
          }
          else
          {
            reason += ("," + aReq.getParameter("pend" + i));
          }
        }
      }
      pendReas.setReasons(QueueConstants.Q_STATUS_PENDING, reason);
    }
    else if(!isBlank(aReq.getParameter("submit")) && (aReq.getParameter("submit")).equals("Override"))
    {
      override=true;
    }
  }

  public void getData(long primaryKey)
  {
    StringBuffer        qs            = new StringBuffer("");
    PreparedStatement   ps            = null;
    ResultSet           rs            = null;

    try
    {
      qs.append("select app_status_reason,app_status ");
      qs.append("from app_queue ");
      qs.append("where ");
      qs.append("app_seq_num = ? and app_queue_type = ? and app_status in (?,?)");
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      ps.setInt(2,QueueConstants.QUEUE_CREDIT);
      ps.setInt(3,QueueConstants.Q_STATUS_DECLINED);
      ps.setInt(4,QueueConstants.Q_STATUS_PENDING);


      rs = ps.executeQuery();

      if(rs.next())
      {
         switch(rs.getInt("app_status"))
         {
           case QueueConstants.Q_STATUS_DECLINED:
             declineReas.setReasons(QueueConstants.Q_STATUS_DECLINED, rs.getString("app_status_reason"));
           break;
           case QueueConstants.Q_STATUS_PENDING:
             pendReas.setReasons(QueueConstants.Q_STATUS_PENDING, rs.getString("app_status_reason"));
           break;
         }
      }

      rs.close();
      ps.close();
      
      int appCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:241^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    app_credit_additional_data
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    app_credit_additional_data\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.CreditStatusBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:247^7*/
      
      if(appCount > 0)
      {
        String tier2String = "";
        /*@lineinfo:generated-code*//*@lineinfo:252^9*/

//  ************************************************************
//  #sql [Ctx] { select  tier_2
//            
//            from    app_credit_additional_data
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  tier_2\n           \n          from    app_credit_additional_data\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.CreditStatusBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   tier2String = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:258^9*/
        
        setTier2(tier2String);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      addError("getData: " + e.toString());
    }
  }

  public void submitData(HttpServletRequest req, long primaryKey)
  {
    QueueBean           qb            = new AppQueueBean("CreditStatusBean.submitData");
    int                 queueStage    = 0;
    int                 creditStatus  = 0;
    int                 queueType     = 0;
    StatusUpdateBean    sub           = null;
    AddQueueBean        aqb           = null;
    int                 recCount      = 0;
    int                 appType       = -1;

    String              progress = "start";
    
    try
    {
      connect();
      
      try
      {
        progress = "getting apptype";
        
        /*@lineinfo:generated-code*//*@lineinfo:291^9*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//            
//            from    application
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n           \n          from    application\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.CreditStatusBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:297^9*/
      
      }
      catch(Exception e)
      {
        logEntry("submitData(" + primaryKey + "): " + progress, e.toString());
      }


      if(override) 
      {
        /*@lineinfo:generated-code*//*@lineinfo:308^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_queue
//            set     app_queue_stage = :QueueConstants.Q_CREDIT_OVERRIDE
//            where   app_seq_num = :primaryKey and
//                    app_queue_type = :QueueConstants.QUEUE_CREDIT
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_queue\n          set     app_queue_stage =  :1 \n          where   app_seq_num =  :2  and\n                  app_queue_type =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.CreditStatusBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.Q_CREDIT_OVERRIDE);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,QueueConstants.QUEUE_CREDIT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:314^9*/
      }
      else
      {
        switch(status)
        {
          case QueueConstants.Q_STATUS_APPROVED:
            progress = "approved, checking sic & MET";
            /*@lineinfo:generated-code*//*@lineinfo:322^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//                
//                from    merchcredit
//                where   app_seq_num = :primaryKey
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n               \n              from    merchcredit\n              where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.CreditStatusBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:328^13*/
            
            if(recCount == 0)
            {
              addError("You must first assign the sic code, investigator, and MET table");
            }
          
            if(!hasErrors())
            {
              progress = "assigning merchant number";
              // assign a merchant number
              AssignMidBean amb = new AssignMidBean(Ctx);
        
              if(!amb.assignMid(primaryKey))
              { 
                logEntry("submitData()", "unable to assign mid for " + primaryKey);
              }
          
              // add to documentation queue
              progress = "adding to documentation queue";
              aqb = new AddQueueBean();
              aqb.connect();
          
              // don't add Transcom or CB&T apps to documentation queue
              if(approvedAppType != mesConstants.APP_TYPE_CBT &&
                 approvedAppType != mesConstants.APP_TYPE_CBT_NEW && 
                 approvedAppType != mesConstants.APP_TYPE_TRANSCOM &&
                 approvedAppType != mesConstants.APP_TYPE_SVB &&
                 approvedAppType != mesConstants.APP_TYPE_NBSC)
              {
                progress = "adding to documentation queue";
                if( aqb.insertQueue(primaryKey,
                      QueueConstants.QUEUE_DOCUMENTATION,
                      QueueConstants.Q_DOCUMENT_PENDING,
                      QueueConstants.Q_STATUS_DOCUMENT_NEEDED,
                      this.appSource,
                      this.userId,
                      this.userName) )
                {
                  // add to documentation sub-table
                  aqb.insertDocQueue(primaryKey, this.documents, this.miscDoc);
                }
              }
          
              progress = "adding to vnumber queue";
              // add to v-number queue
              aqb.insertQueue(primaryKey,
                QueueConstants.QUEUE_VNUMBER,
                QueueConstants.Q_VNUMBER_NEW,
                QueueConstants.Q_STATUS_VNUMBER_NEEDED,
                this.appSource,
                this.userId,
                this.userName);

              // add the v-number slots
              progress = "adding vnumber slots";
              VNumberBean.addSlots(primaryKey);
            
              // add the tier2 information if necessary
              progress = "adding tier 2";
              addTier2(primaryKey);
              
              progress = "adding verisign over auth fees";
              addVeriSignOverAuthPerItem(primaryKey);
            }
            queueStage    = QueueConstants.Q_SETUP_NEW;
            queueType     = QueueConstants.QUEUE_SETUP;
            creditStatus  = QueueConstants.CREDIT_APPROVE;
            break;
          
          case QueueConstants.Q_STATUS_DECLINED:
            queueStage    = QueueConstants.Q_CREDIT_DECLINE;
            queueType     = QueueConstants.QUEUE_CREDIT;
            creditStatus  = QueueConstants.CREDIT_DECLINE;
            break;

          case QueueConstants.Q_STATUS_CANCELLED:
            queueStage    = QueueConstants.Q_CANCEL_NEW;
            queueType     = QueueConstants.QUEUE_CANCEL;
            creditStatus  = QueueConstants.CREDIT_CANCEL;
            break;
          
          case QueueConstants.Q_STATUS_PENDING:
            queueStage    = QueueConstants.Q_CREDIT_PEND;
            queueType     = QueueConstants.QUEUE_CREDIT;
            creditStatus  = QueueConstants.CREDIT_PEND;
            break;
        }
        
        if(!hasErrors())
        {
          // remove from any existing queues other than credit so that updates
          // will not fail due to primary key constraints
          /*@lineinfo:generated-code*//*@lineinfo:421^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    app_queue
//              where   app_seq_num = :primaryKey and
//                      app_queue_type != :QueueConstants.QUEUE_CREDIT
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    app_queue\n            where   app_seq_num =  :1  and\n                    app_queue_type !=  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.CreditStatusBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,QueueConstants.QUEUE_CREDIT);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:427^11*/
          
          commit();
          
          // set credit status
          progress = "setting credit status";
          /*@lineinfo:generated-code*//*@lineinfo:433^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//              set     merch_credit_status = :creditStatus
//              where   app_seq_num = :primaryKey
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n            set     merch_credit_status =  :1 \n            where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.ops.CreditStatusBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,creditStatus);
   __sJT_st.setLong(2,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:438^11*/

          // update queue move into credit pend match queue
          /*@lineinfo:generated-code*//*@lineinfo:441^11*/

//  ************************************************************
//  #sql [Ctx] { update  app_queue
//              set     app_status = :this.status,
//                      app_queue_stage = :queueStage,
//                      app_status_reason = :this.reason,
//                      app_queue_type = :queueType
//              where   app_seq_num = :primaryKey and
//                      app_queue_type = :QueueConstants.QUEUE_CREDIT
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_queue\n            set     app_status =  :1 ,\n                    app_queue_stage =  :2 ,\n                    app_status_reason =  :3 ,\n                    app_queue_type =  :4 \n            where   app_seq_num =  :5  and\n                    app_queue_type =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ops.CreditStatusBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.status);
   __sJT_st.setInt(2,queueStage);
   __sJT_st.setString(3,this.reason);
   __sJT_st.setInt(4,queueType);
   __sJT_st.setLong(5,primaryKey);
   __sJT_st.setInt(6,QueueConstants.QUEUE_CREDIT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:450^11*/
          
          // update the credit status within the application tracking system
          progress = "updating app tracking";
          sub = new StatusUpdateBean();
          sub.connect();
          sub.updateCreditStatus(this.userName, primaryKey, this.status, this.reason);
        
          // perform any vendor-specific tasks (emailing, adding to queues, etc)
          AppNotifyBean anb = new AppNotifyBean();
        
          progress = "notifying";
          switch(status)
          {
            case QueueConstants.Q_STATUS_APPROVED:
              anb.notifyStatus(primaryKey, mesConstants.APP_STATUS_APPROVED, -1);
              break;
            
            case QueueConstants.Q_STATUS_DECLINED:
              anb.notifyStatus(primaryKey, mesConstants.APP_STATUS_DECLINED, -1);
              break;
            
            case QueueConstants.Q_STATUS_PENDING:
              anb.notifyStatus(primaryKey, mesConstants.APP_STATUS_PENDED, -1);
              break;
            
            case QueueConstants.Q_STATUS_CANCELLED:
              anb.notifyStatus(primaryKey, mesConstants.APP_STATUS_CANCELLED, -1);
              break;
          }
        }
/*******************************************************************************
                                 AUTO UPLOAD 
*******************************************************************************/

        boolean allowAutoUpload = false;
        String  autoUpload      = "";

        // update queue move into credit pend match queue
        /*@lineinfo:generated-code*//*@lineinfo:489^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(allow_auto_upload, 'N')
//            
//            from    merchant
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(allow_auto_upload, 'N')\n           \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.CreditStatusBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   autoUpload = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:495^9*/
        
        allowAutoUpload = autoUpload.equals("Y");
        
        // check for circumstances that should prevent auto-upload, including:
        //   * data in any comment fields
        //   * additional location
        if(allowAutoUpload)
        {
          allowAutoUpload = !AutoUpload.checkManualSetupRequirement(primaryKey);
        }

        //if approved check to see if association was assigned in credit.. if so.. then auto upload
        progress = "moving to auto-upload if necessary";
        if(status == QueueConstants.Q_STATUS_APPROVED && allowAutoUpload)
        {
          //automatically do the data expansion part
          AutoUpload.doAutoUpload(primaryKey);
          AutoUpload.submitToTape(primaryKey);
          AutoUpload.moveToMMS(primaryKey, null);
          AutoUpload.moveToSetupComplete(primaryKey, null);
        }

/***************************************************************************************
                             END AUTO UPLOAD
***************************************************************************************/

      }
    }
    catch(Exception e)
    {
      logEntry("submitData(" + primaryKey + "): " + progress, e.toString());
      addError("submitData: " + e.toString());
    }
    finally
    {
      cleanUp();
      
      if(sub != null)
      {
        sub.cleanUp();
      }
      
      if(aqb != null)
      {
        aqb.cleanUp();
      }
    }
  }
  
  private void moveAllQueuesToCompleted(long primaryKey)
  {
    StringBuffer        qs            = new StringBuffer("");
    PreparedStatement   ps            = null;
    ResultSet           rs            = null;
   
    try
    {

        qs.setLength(0);
        qs.append("update app_queue             ");
        qs.append("set    app_queue_stage   = ? ");
        qs.append("where  app_seq_num       = ? ");
        qs.append("and    app_queue_type    = ? ");
      
        ps = getPreparedStatement(qs.toString());

        ps.setLong(2, primaryKey);

        //setup queue
        ps.setInt(1,  QueueConstants.Q_SETUP_COMPLETE);
        ps.setInt(3,  QueueConstants.QUEUE_SETUP);
        ps.executeUpdate();

        //dont want to remove it from equipment queue..... why? im not sure.. ask david..
        //equip queue
        //ps.setInt(1,  QueueConstants.Q_EQUIPMENT_COMPLETE);
        //ps.setInt(3,  QueueConstants.QUEUE_EQUIPMENT);
        //ps.executeUpdate();

        //documentation queue
        ps.setInt(1,  QueueConstants.Q_DOCUMENT_RECEIVED);
        ps.setInt(3,  QueueConstants.QUEUE_DOCUMENTATION);
        ps.executeUpdate();

        //vnumber queue
        ps.setInt(1,  QueueConstants.Q_VNUMBER_ASSIGNED);
        ps.setInt(3,  QueueConstants.QUEUE_VNUMBER);
        ps.executeUpdate();

        //mms queue
        ps.setInt(1,  QueueConstants.Q_MMS_COMPLETED);
        ps.setInt(3,  QueueConstants.QUEUE_MMS);
        ps.executeUpdate();

        ps.close();

        qs.setLength(0);
        qs.append("update temp_activation_queue ");
        qs.append("set    q_stage       = ?     ");
        qs.append("where  app_seq_num   = ?     ");
        qs.append("and    q_type        = ?     ");
      
        ps = getPreparedStatement(qs.toString());

        ps.setLong(2, primaryKey);

        //programming queue
        ps.setInt(1,  QueueConstants.Q_PROGRAMMING_COMPLETE);
        ps.setInt(3,  QueueConstants.QUEUE_PROGRAMMING);
        ps.executeUpdate();

        //deployment queue
        ps.setInt(1,  QueueConstants.Q_DEPLOYMENT_COMPLETE);
        ps.setInt(3,  QueueConstants.QUEUE_DEPLOYMENT);
        ps.executeUpdate();

        //activation queue
        ps.setInt(1,  QueueConstants.Q_ACTIVATION_COMPLETE);
        ps.setInt(3,  QueueConstants.QUEUE_ACTIVATION);
        ps.executeUpdate();

        ps.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "moveAllQueuesToCompleted: " + e.toString());
      addError("moveAllQueuesToCompleted: " + e.toString());
    }
    finally
    {
    }
  }

  private void moveAllNewQueuesToCompleted(long id)
  {
    QueueTools.moveQueueItem(id, QueueTools.getCurrentQueueType(id, MesQueues.Q_ITEM_TYPE_MMS),        MesQueues.Q_MMS_COMPLETE,         null, "");
    QueueTools.moveQueueItem(id, QueueTools.getCurrentQueueType(id, MesQueues.Q_ITEM_TYPE_DEPLOYMENT), MesQueues.Q_DEPLOYMENT_COMPLETED, null, "");
    QueueTools.moveQueueItem(id, QueueTools.getCurrentQueueType(id, MesQueues.Q_ITEM_TYPE_ACTIVATION), MesQueues.Q_ACTIVATION_COMPLETED, null, "");
  }


  public boolean validate()
  {
    if(status != QueueConstants.Q_STATUS_APPROVED && isBlank(reason) && !override)
    {
      addError("You must choose a reason to decline or pend the account");
    }
    
    return (! hasErrors());
  }
  
  public void setDocuments(String[] documents)
  {
    this.documents = 0L;
    
    for(int i = 0; i < documents.length; ++i)
    {
      this.documents |= parseLong(documents[i]);
    }
  }
  public long getDocuments()
  {
    return this.documents;
  }
  
  public void setMiscDoc(String miscDoc)
  {
    this.miscDoc = miscDoc;
  }
  
  public void setStatus(String status)
  {
    this.status = parseInt(status);
    if(this.status == QueueConstants.Q_STATUS_CANCELLED)
    {
      cancelled = true;
    }
  }
  
  public void setUserLogin(UserBean ub)
  {
    this.userLogin = ub;
  }

  public int getStatus()
  {
    return this.status;
  }
  
  public void setReason(String reason)
  {
    this.reason = reason;
  }
  public String getReason()
  {
    return reason;
  }

  public String getDeclineReasonChecked(String code)
  {
    return declineReas.getChecked(code);  
  }

  public String getPendReasonChecked(String code)
  {
    return pendReas.getChecked(code);  
  }

  public Vector getDocumentTypes()
  {
    return this.documentTypes;
  }
  public String getDocDescription(int i)
  {
    String result = "";
    try
    {
      result = (String)documentDescriptions.elementAt(i);
    }
    catch(Exception e)
    {
      result = "INVALID INDEX (" + i + ")";
    }
    return result;
  }
  public Vector getDeclineReasons()
  {
    return this.declineReasons;
  }
  public String getDeclineReasonDescription(int i)
  {
    String result = "";
    try
    {
      result = (String)declineDescriptions.elementAt(i);
    }
    catch(Exception e)
    {
      result = "INVALID INDEX (" + i + ")";
    }
    return result;
  }
  public Vector getPendingReasons()
  {
    return this.pendingReasons;
  }
  public String getPendingReasonDescription(int i)
  {
    String result = "";
    try
    {
      result = (String)pendingDescriptions.elementAt(i);
    }
    catch(Exception e)
    {
      result = "INVALID INDEX (" + i + ")";
    }
    return result;
  }

  public Vector getCancelReasons()
  {
    return this.cancelReasons;
  }
  public String getCancelReasonDescription(int i)
  {
    String result = "";
    try
    {
      result = (String)cancelDescriptions.elementAt(i);
    }
    catch(Exception e)
    {
      result = "INVALID INDEX (" + i + ")";
    }
    return result;
  }
  
  public void setAppSource(String appSource)
  {
    this.appSource = appSource;
  }
  public void setUserId(String userId)
  {
    this.userId = parseLong(userId);
  }
  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public void setApprovedAppType(String appType)
  {
    try
    {
      this.approvedAppType = Integer.parseInt(appType);
    }
    catch(Exception e)
    {
    }
  }
  public boolean isCancelled()
  {
    return cancelled;
  }
  public void setTier2(String tier2)
  {
    if(tier2 != null && tier2.equals("Y"))
    {
      this.tier2Checked = "checked";
      this.isTier2 = "Y";
    }
    else
    {
      this.tier2Checked = "";
      this.isTier2 = "N";
    }
  }
  public String getTier2Checked()
  {
    return this.tier2Checked;
  }
  
  private void addVeriSignOverAuthPerItem(long appSeqNum)
  {
    try
    {
      connect();
      
      // determine if this app is susceptible to the over-auth fee
      int appCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:828^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(bva.app_type)
//          
//          from    billing_verisign_app_types bva,
//                  application app
//          where   app.app_seq_num = :appSeqNum and
//                  app.app_type = bva.app_type(+) and
//                  bva.enabled(+) = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(bva.app_type)\n         \n        from    billing_verisign_app_types bva,\n                application app\n        where   app.app_seq_num =  :1  and\n                app.app_type = bva.app_type(+) and\n                bva.enabled(+) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.CreditStatusBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:837^7*/
      
      if(appCount > 0)
      {
        // get per-auth amount and add merchant to billing_verisign_over_auth table
        /*@lineinfo:generated-code*//*@lineinfo:842^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(tc.app_seq_num)
//            
//            from    tranchrg tc
//            where   tc.app_seq_num = :appSeqNum and
//                    tc.cardtype_code = :mesConstants.APP_CT_INTERNET and
//                    nvl(tc.tranchrg_per_auth, 0.0) > 0.0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(tc.app_seq_num)\n           \n          from    tranchrg tc\n          where   tc.app_seq_num =  :1  and\n                  tc.cardtype_code =  :2  and\n                  nvl(tc.tranchrg_per_auth, 0.0) > 0.0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.CreditStatusBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_INTERNET);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:850^9*/
        
        if(appCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:854^11*/

//  ************************************************************
//  #sql [Ctx] { insert into billing_verisign_over_auth
//              (
//                hierarchy_node,
//                valid_date_begin,
//                valid_date_end,
//                per_auth_fee
//              )
//              select  mr.merch_number         hierarchy_node,
//                      trunc(sysdate)          valid_date_begin,
//                      to_date('31-Dec-9999')  valid_date_end,
//                      tc.tranchrg_per_auth    per_auth_fee
//              from    merchant mr,
//                      tranchrg tc
//              where   mr.app_seq_num = :appSeqNum and
//                      mr.app_seq_num = tc.app_seq_num and
//                      tc.cardtype_code = :mesConstants.APP_CT_INTERNET
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into billing_verisign_over_auth\n            (\n              hierarchy_node,\n              valid_date_begin,\n              valid_date_end,\n              per_auth_fee\n            )\n            select  mr.merch_number         hierarchy_node,\n                    trunc(sysdate)          valid_date_begin,\n                    to_date('31-Dec-9999')  valid_date_end,\n                    tc.tranchrg_per_auth    per_auth_fee\n            from    merchant mr,\n                    tranchrg tc\n            where   mr.app_seq_num =  :1  and\n                    mr.app_seq_num = tc.app_seq_num and\n                    tc.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.ops.CreditStatusBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_INTERNET);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:872^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("addVeriSignOverAuthPerItem(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void addTier2(long appSeqNum)
  {
    try
    {
      connect();
      
      // check to see if this item has already been inserted
      int appCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:895^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    app_credit_additional_data
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    app_credit_additional_data\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.CreditStatusBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:901^7*/
      
      if(appCount > 0)
      {
        // update
        /*@lineinfo:generated-code*//*@lineinfo:906^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_credit_additional_data
//            set     tier_2 = :this.isTier2
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_credit_additional_data\n          set     tier_2 =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.ops.CreditStatusBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.isTier2);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:911^9*/
      }
      else
      {
        // insert
        /*@lineinfo:generated-code*//*@lineinfo:916^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_credit_additional_data
//            (
//              app_seq_num,
//              tier_2
//            )
//            values
//            (
//              :appSeqNum,
//              :this.isTier2
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_credit_additional_data\n          (\n            app_seq_num,\n            tier_2\n          )\n          values\n          (\n             :1 ,\n             :2 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.ops.CreditStatusBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,this.isTier2);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:928^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("addTier2()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/