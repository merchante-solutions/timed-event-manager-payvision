/*@lineinfo:filename=HideApp*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: /Java/servlets/com/mes/utils/HideAppServlet.java $

  Description:
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import com.mes.constants.MesHierarchy;
import com.mes.database.SQLJConnectionBase;

public class HideApp extends SQLJConnectionBase
{
  private void doHide(int appSeqNum, long userNode)
  {
    try
    {
      connect();
      
      int creditStatus;
      /*@lineinfo:generated-code*//*@lineinfo:37^7*/

//  ************************************************************
//  #sql [Ctx] { select  merch_credit_status
//          
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_credit_status\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.HideApp",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   creditStatus = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:43^7*/
      
      // only if not finished or canceled
      if(creditStatus == 0 || creditStatus == 4 || creditStatus == 103)
      {
        int recCount = 0;
        // make sure user has rights to hide this app
        /*@lineinfo:generated-code*//*@lineinfo:50^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(ass.app_seq_num)
//            
//            from    app_status_summary ass,
//                    merchant mr
//            where   ass.app_seq_num = :appSeqNum and
//                    ass.app_seq_num = mr.app_seq_num and
//                    ( ass.user_node in
//                      (
//                        select  descendent
//                        from    t_hierarchy
//                        where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                                ancestor  = :userNode
//                      ) or
//                      mr.merch_number in
//                      (
//                        select  gm.merchant_number
//                        from    organization    o,
//                                group_merchant  gm
//                        where   o.org_group = :userNode and
//                                gm.org_num = o.org_num
//                      ) 
//                      or
//                      ass.association_node in
//                      (
//                        select  descendent
//                        from    t_hierarchy
//                        where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                                ancestor = :userNode
//                      )
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ass.app_seq_num)\n           \n          from    app_status_summary ass,\n                  merchant mr\n          where   ass.app_seq_num =  :1  and\n                  ass.app_seq_num = mr.app_seq_num and\n                  ( ass.user_node in\n                    (\n                      select  descendent\n                      from    t_hierarchy\n                      where   hier_type =  :2  and\n                              ancestor  =  :3 \n                    ) or\n                    mr.merch_number in\n                    (\n                      select  gm.merchant_number\n                      from    organization    o,\n                              group_merchant  gm\n                      where   o.org_group =  :4  and\n                              gm.org_num = o.org_num\n                    ) \n                    or\n                    ass.association_node in\n                    (\n                      select  descendent\n                      from    t_hierarchy\n                      where   hier_type =  :5  and\n                              ancestor =  :6 \n                    )\n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.HideApp",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(3,userNode);
   __sJT_st.setLong(4,userNode);
   __sJT_st.setInt(5,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(6,userNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:82^9*/
        
        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:86^11*/

//  ************************************************************
//  #sql [Ctx] { call hide_application(:appSeqNum)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN hide_application( :1 )\n          \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.HideApp",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:89^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("doHide(" + appSeqNum + ", " + userNode + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
    
  public static void hide(int appSeqNum, long userNode)
  {             
    try
    {
      HideApp worker = new HideApp();
      
      worker.doHide(appSeqNum, userNode);
    }
    catch(Exception e)
    {
    }
  }
}/*@lineinfo:generated-code*/