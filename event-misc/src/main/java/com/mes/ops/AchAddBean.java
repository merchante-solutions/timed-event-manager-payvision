/*@lineinfo:filename=AchAddBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AchAddBean.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-08-09 14:57:56 -0700 (Mon, 09 Aug 2010) $
  Version            : $Revision: 17687 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import com.mes.ach.AchEntryData;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class AchAddBean extends FieldBean
{
  // constants
  public static final String RECEIVE_ABA  = "061100606";
  public static final String COMP_NAME    = "MERCHE-SOLUTIONS";
  public static final String COMP_ID      = "9061112345";

  private class ReasonCodeTable extends DropDownTable
  {
    public ReasonCodeTable() 
    {
      ResultSetIterator           it          = null;
      ResultSet                   resultSet   = null;
      
      try
      {
        connect();
        addElement("","select");
        /*@lineinfo:generated-code*//*@lineinfo:72^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    REASON_CODE,
//                      REASON_CODE || ' - ' || DESCRIPTION
//            from      ACH_REJECT_REASON_CODES
//            where     INSTR(reason_code, 'C') <= 0
//            order by  REASON_CODE
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    REASON_CODE,\n                    REASON_CODE || ' - ' || DESCRIPTION\n          from      ACH_REJECT_REASON_CODES\n          where     INSTR(reason_code, 'C') <= 0\n          order by  REASON_CODE";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AchAddBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AchAddBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:79^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      catch(Exception e)
      {}
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }


  private class SecTable extends DropDownTable
  {
    public SecTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("CCD","CCD");
      addElement("COR","COR");
      addElement("PPD","PPD");
    }
  }

  private class TransactionCodeTable extends DropDownTable
  {
    public TransactionCodeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("21","21 - Credit");
      addElement("26","26 - Debit");
    }
  }

  private class DescriptionTable extends DropDownTable
  {
    public DescriptionTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement(AchEntryData.ED_MERCH_DEP, AchEntryData.ED_MERCH_DEP);
      addElement(AchEntryData.ED_CHARGEBACK, AchEntryData.ED_CHARGEBACK);
      addElement(AchEntryData.ED_MERCHANT_FEES, AchEntryData.ED_MERCHANT_FEES);
      addElement(AchEntryData.ED_ADJUSTMENTS, AchEntryData.ED_ADJUSTMENTS);
    }
  }


  private class ReportNameTable extends DropDownTable
  {
    public ReportNameTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("ACDDAENT-01/RET","ACDDAENT-01/RET");
      addElement("ACDDAENT-01/RTX","ACDDAENT-01/RTX");
    }
  }

  private class BankNumberTable extends DropDownTable
  {
    public BankNumberTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("3941","3941");
      addElement("3858","3858");
    }
  }

  private class DateValidation implements Validation
  {
    String                  ErrorMessage      = null;
   
    public DateValidation()
    {
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }

    public boolean validate( String fdata )
    {
      ErrorMessage = null;

      try
      {
        fdata = addSlash(fdata);
        DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT);
        Date aDate = fmt.parse(fdata.trim());
      }
      catch( Exception e )
      {
        ErrorMessage = "Please provide a valid date. (mm/dd/yy)";
      }        
      return( ErrorMessage == null );
    }

    public String addSlash(String data) throws Exception
    {
      if(data.length() != 6)
      {
        throw new Exception("Date must be 6 digits long without slashes (/)");
      }

      String first2   = data.substring(0,2);
      String second2  = data.substring(2,4);
      String third2   = data.substring(4);

      return (first2 + "/" + second2 + "/" + third2);
    }
  }


  /*
  ** CONSTRUCTOR
  */  
  public AchAddBean()
  {
  }
  
  
  public void init()
  {
    fields.removeAllFields();
    
    OnlyOneValidation     onlyOneVal      = null;
    Field                 temp            = null;
    Validation            val             = null;

    try
    {    

      //DROP DOWNS
      fields.add(new DropDownField("reportName",      new ReportNameTable(),      true));
      fields.add(new DropDownField("sec",             new SecTable(),             true));
      fields.add(new DropDownField("reasonCode",      new ReasonCodeTable(),      false));
      fields.add(new DropDownField("transactionCode", new TransactionCodeTable(), false));
      fields.add(new DropDownField("bankNumber",      new BankNumberTable(),      false));
      fields.add(new DropDownField("entryDesc",       new DescriptionTable(),     false));

      //number fields
      fields.add(new NumberField("amount",          8,  15, false, 2));
      fields.add(new NumberField("merchantNumber",  16, 20, false, 0));
      fields.add(new NumberField("dda",             13, 15, false, 0)); //cut off leading zeros
      fields.add(new NumberField("transitRouting",  8,  15, false, 0)); //first 8 numbers only
      fields.add(new NumberField("originalTrace",   15, 15, true,  0));
      fields.add(new NumberField("returnTrace",     15, 15, true,  0));

      //date fields
      fields.add(new Field("reportDate",      6, 10, false));
      fields.getField("reportDate").addValidation( new DateValidation() );

      //fields.add(new Field("settledDate",     6, 15, true));
      //fields.getField("settledDate").addValidation( new DateValidation() );

      //fields.add(new Field("effectiveDate",   6, 15, true));
      //fields.getField("effectiveDate").addValidation( new DateValidation() );


      //string fields
      //fields.add(new Field("merchantName",    16, 20, false));
      fields.add(new Field("adden1",           9, 15, true));
      fields.add(new Field("adden2",          10, 15, true));


      addHtmlExtra("class=\"formFields\"");
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
  }
  



  /*
  ** FUNCTION setDefaults
  **
  **  This should be overloaded by derived classes if necessary
  */
  public void setDefaults()
  {
  }
  
  
  // 
  //  setProperties
  //
  //  This method is used to load class members only.  Form
  //  field data is loaded through the setFields(request) 
  //  method.  Derived classes that overload this method
  //  MUST call this instance through the super reference.
  //  
  public void setProperties( HttpServletRequest request )
  { 
  } 
 
  public void setUser(UserBean ub)
  {
    user = ub;
  }


  /***************************************************************************
  * Overridable methods
  ***************************************************************************/
  /*
  ** FUNCTION loadData
  **
  **  Loads the data for this application from the database.
  **  Must be overloaded by derived classes.
  */
  public void loadData()
  {
  }

  private String addSlash(String data)
  {
    String first2   = data.substring(0,2);
    String second2  = data.substring(2,4);
    String third2   = data.substring(4);

    return (first2 + "/" + second2 + "/" + third2 + "/");
  }

  
  /*
  ** FUNCTION storeData
  **
  **  Stores the data from the current bean into the database.
  **  Must be overloaded by derived classes.
  */
  public void storeData()
  {
    //long rejectSeqNum = getNewRejectSeqNum();

    //if(rejectSeqNum > 0L)
    //{

      try
      {

        connect();

        /*@lineinfo:generated-code*//*@lineinfo:337^9*/

//  ************************************************************
//  #sql [Ctx] { insert into  ach_rejects
//            (
//              --REJECT_SEQ_NUM, 
//              REASON_CODE, 
//              ORIGINAL_TRACE, 
//              EFFECTIVE_DATE, 
//              TRANSIT_ROUTING, 
//              SETTLED_DATE, 
//              SEC, 
//              COMP_ID, 
//              DDA, 
//              RETURN_TRACE, 
//              TRANSACTION_CODE, 
//              TRANSACTION_TYPE, 
//              AMOUNT, 
//              MERCHANT_NUMBER, 
//              MERCHANT_NAME, 
//              RECV_ABA, 
//              COMP_NAME, 
//              ENTRY_DESC, 
//              REPORT_NAME, 
//              REPORT_DATE, 
//              ADDEN_1, 
//              ADDEN_2, 
//              BANK_NUMBER, 
//              LOAD_FILENAME 
//            )
//            values
//            (
//              --:rejectSeqNum,
//              :fields.getField("reasonCode").getData(),
//              :fields.getField("originalTrace").getData(),
//              :getSqlDate(addSlash(fields.getField("reportDate").getData()),"MM/dd/yy"),
//              :fields.getField("transitRouting").getData(),
//              :getSqlDate(addSlash(fields.getField("reportDate").getData()),"MM/dd/yy"),
//              :fields.getField("sec").getData(),
//              :COMP_ID,
//              : padField(fields.getField("dda").asLong(), 13),
//              :fields.getField("returnTrace").getData(),
//              :fields.getField("transactionCode").getData(),
//              :getTransactionType(fields.getField("amount").asDouble()),
//              :fields.getField("amount").getData(),
//              :fields.getField("merchantNumber").getData(),
//              :getMerchantName(fields.getField("merchantNumber").getData()),
//              :RECEIVE_ABA,
//              :COMP_NAME,
//              :fields.getField("entryDesc").getData(),
//              :fields.getField("reportName").getData(),
//              :getSqlDate(addSlash(fields.getField("reportDate").getData()),"MM/dd/yy"),
//              :fields.getField("adden1").getData(),
//              :fields.getField("adden2").getData(),
//              :fields.getField("bankNumber").getData(),
//              :getUserLoginName()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_609 = fields.getField("reasonCode").getData();
 String __sJT_610 = fields.getField("originalTrace").getData();
 java.sql.Date __sJT_611 = getSqlDate(addSlash(fields.getField("reportDate").getData()),"MM/dd/yy");
 String __sJT_612 = fields.getField("transitRouting").getData();
 java.sql.Date __sJT_613 = getSqlDate(addSlash(fields.getField("reportDate").getData()),"MM/dd/yy");
 String __sJT_614 = fields.getField("sec").getData();
 String __sJT_615 =  padField(fields.getField("dda").asLong(), 13);
 String __sJT_616 = fields.getField("returnTrace").getData();
 String __sJT_617 = fields.getField("transactionCode").getData();
 String __sJT_618 = getTransactionType(fields.getField("amount").asDouble());
 String __sJT_619 = fields.getField("amount").getData();
 String __sJT_620 = fields.getField("merchantNumber").getData();
 String __sJT_621 = getMerchantName(fields.getField("merchantNumber").getData());
 String __sJT_622 = fields.getField("entryDesc").getData();
 String __sJT_623 = fields.getField("reportName").getData();
 java.sql.Date __sJT_624 = getSqlDate(addSlash(fields.getField("reportDate").getData()),"MM/dd/yy");
 String __sJT_625 = fields.getField("adden1").getData();
 String __sJT_626 = fields.getField("adden2").getData();
 String __sJT_627 = fields.getField("bankNumber").getData();
 String __sJT_628 = getUserLoginName();
   String theSqlTS = "insert into  ach_rejects\n          (\n            --REJECT_SEQ_NUM, \n            REASON_CODE, \n            ORIGINAL_TRACE, \n            EFFECTIVE_DATE, \n            TRANSIT_ROUTING, \n            SETTLED_DATE, \n            SEC, \n            COMP_ID, \n            DDA, \n            RETURN_TRACE, \n            TRANSACTION_CODE, \n            TRANSACTION_TYPE, \n            AMOUNT, \n            MERCHANT_NUMBER, \n            MERCHANT_NAME, \n            RECV_ABA, \n            COMP_NAME, \n            ENTRY_DESC, \n            REPORT_NAME, \n            REPORT_DATE, \n            ADDEN_1, \n            ADDEN_2, \n            BANK_NUMBER, \n            LOAD_FILENAME \n          )\n          values\n          (\n            --:rejectSeqNum,\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.AchAddBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_609);
   __sJT_st.setString(2,__sJT_610);
   __sJT_st.setDate(3,__sJT_611);
   __sJT_st.setString(4,__sJT_612);
   __sJT_st.setDate(5,__sJT_613);
   __sJT_st.setString(6,__sJT_614);
   __sJT_st.setString(7,COMP_ID);
   __sJT_st.setString(8,__sJT_615);
   __sJT_st.setString(9,__sJT_616);
   __sJT_st.setString(10,__sJT_617);
   __sJT_st.setString(11,__sJT_618);
   __sJT_st.setString(12,__sJT_619);
   __sJT_st.setString(13,__sJT_620);
   __sJT_st.setString(14,__sJT_621);
   __sJT_st.setString(15,RECEIVE_ABA);
   __sJT_st.setString(16,COMP_NAME);
   __sJT_st.setString(17,__sJT_622);
   __sJT_st.setString(18,__sJT_623);
   __sJT_st.setDate(19,__sJT_624);
   __sJT_st.setString(20,__sJT_625);
   __sJT_st.setString(21,__sJT_626);
   __sJT_st.setString(22,__sJT_627);
   __sJT_st.setString(23,__sJT_628);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:393^9*/
      }
      catch(Exception e)
      {
        logEntry("storeData()", e.toString());
      }
      finally
      {
        cleanUp();
      }
    
    //}
  }

  private String padField(long value, int size)
  {
    String result = "";

    try
    {
      result = Long.toString(value);
      int padNeeded = 13 - result.length();
      
      for(int i=0; i<padNeeded; i++)
      {
        result = " " + result;
      }
    }
    catch(Exception e)
    {
    }

    return result;
  }

  private String getMerchantName(String merchNum)
  {
    boolean useCleanUp = false;
    String  result     = "??????";

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:441^7*/

//  ************************************************************
//  #sql [Ctx] { select  dba_name 
//          from    mif
//          where   merchant_number = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  dba_name  \n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AchAddBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:446^7*/

    }
    catch(Exception e)
    {
      logEntry("getMerchantName()", e.toString());
      result = "??????";
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    if(result.length() > 16)
    {
      return (result.substring(0,16)).toUpperCase();
    }
    else
    {
      return result.toUpperCase();
    }
  }

  private String getTransactionType(double amount)
  {
    if(amount < 0.0)
    {
      return "DR";
    }
    else
    {
      return "CR";
    }
  }

  private long getNewRejectSeqNum()
  {
    long rejectNum = 0L;
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:491^7*/

//  ************************************************************
//  #sql [Ctx] { select  seq_ach_rejects.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  seq_ach_rejects.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AchAddBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rejectNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:495^7*/

    }
    catch(Exception e)
    {
      logEntry("getNewRejectSeqNum()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    //System.out.println("The reject seq num is " + rejectNum);

    return rejectNum;
  }
}/*@lineinfo:generated-code*/