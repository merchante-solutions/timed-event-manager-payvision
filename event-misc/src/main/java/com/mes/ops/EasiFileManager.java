package com.mes.ops;

import java.util.HashMap;
import java.util.Vector;


public class EasiFileManager
{

  private HashMap           EasiFileHash        = new HashMap();
  private Vector            FileIdentifierList  = new Vector();
  private EasiFile          CurrentFile         = null;

  /*
  ** CONSTRUCTOR public DceFileManager()
  **
  ** Default constructor.
  */
  public EasiFileManager()
  {
  }

  public EasiFile getEasiFile(String fileIdentifier)
  {
    return getEfFromHash(fileIdentifier, true);
  }

  public EasiFile getEasiFile(String fileIdentifier, boolean getNewEf)
  {
    return getEfFromHash(fileIdentifier, getNewEf);
  }

  public void saveEasiFile(EasiFile ef)
  {
    putEfInHash(ef);
  }


  public int getFileIdentifierListSize()
  {
    return FileIdentifierList.size();
  }

  public String getFileIdentifierFromList(int idx)
  {
    return (String)FileIdentifierList.elementAt(idx);
  }

  public void closeFiles()
  {
    for(int x=0; x<getFileIdentifierListSize(); x++)
    {
      CurrentFile = getEfFromHash(getFileIdentifierFromList(x), false);
      if(CurrentFile != null)
      {
        CurrentFile.closeFile();
        putEfInHash(CurrentFile);
      }
    }
  }

  public void deleteFiles()
  {
    for(int x=0; x<getFileIdentifierListSize(); x++)
    {
      CurrentFile = getEfFromHash(getFileIdentifierFromList(x), false);
      if(CurrentFile != null)
      {
        CurrentFile.deleteFile();
        putEfInHash(CurrentFile);
      }
    }
  }

  private EasiFile getEfFromHash(String fileIdentifier)
  {
    return getEfFromHash(fileIdentifier, true);
  }

  private EasiFile getEfFromHash(String fileIdentifier, boolean getNewEf)
  {
    if(EasiFileHash.containsKey(fileIdentifier))
    {
      return ((EasiFile)EasiFileHash.get(fileIdentifier));
    }
    else if(getNewEf)
    {
      return (new EasiFile(fileIdentifier));
    }
    else
    {
      return null;
    }
  }

  private void putEfInHash(EasiFile ef)
  {
    EasiFileHash.put(ef.getFileIdentifier(), ef);
    addFileIdentifier(ef.getFileIdentifier());
  }

  private void addFileIdentifier(String fileIdentifier)
  {
    if(!FileIdentifierList.contains(fileIdentifier))
    {
      FileIdentifierList.add(fileIdentifier);
    }
  }      

}
