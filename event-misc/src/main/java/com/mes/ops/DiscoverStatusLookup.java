/*@lineinfo:filename=DiscoverStatusLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/DiscoverStatusLookup.sqlj $

  Description:
  
  StateDropDownTable
  
  Loads a list of state abbreviations from the countrystate db table
  into a drop down table.
  
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 9/24/04 12:21p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.TreeSet;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.SortableReport;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DropDownTable;
import com.mes.tools.HierarchyTree;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class DiscoverStatusLookup extends SortableReport
{ 
  HiddenField   isSubmitFlag    = new HiddenField("isSubmit");
  HiddenField   sortByField     = new HiddenField("sortBy");
  ButtonField   searchButton    = new ButtonField("search");
  DateField     fromDate        = new DateField( "fromDate","Submit Date From: ", true );
  DateField     toDate          = new DateField( "toDate","To:", true ); 

  SortByType    sbt             = new SortByType(SB_SEND_DATE * -1);
  UserBean      user            = null;

  //constructor
  public DiscoverStatusLookup()
  {
    sbt.setSortBy(SB_SEND_DATE * -1);
    fields.add(isSubmitFlag);

    fromDate.setDateFormat("MM/dd/yyyy");
    fields.add(fromDate);

    toDate.setDateFormat("MM/dd/yyyy");
    fields.add(toDate);
    
    // default date range to prior 30 days
    Calendar cal = Calendar.getInstance();
    toDate.setData(DateTimeFormatter.getFormattedDate(cal.getTime(), "MM/dd/yyyy"));
    cal.add(Calendar.DATE, -30);
    fromDate.setData(DateTimeFormatter.getFormattedDate(cal.getTime(), "MM/dd/yyyy"));

    fields.add(new Field("lookupValue",25,0,false));
    fields.add(searchButton);
    fields.add(sortByField);
    fields.add(new DropDownField("status",    new DiscoverStatusDropDownTable(),    false));
    fields.add(new DropDownField("transType", new DiscoverTransTypeDropDownTable(), false));
    fields.add(new DropDownField("reqType",   new DiscoverReqTypeDropDownTable(),   false));
    
    sortByField.setData(Integer.toString(sbt.getSortBy()));
    
    fields.addHtmlExtra("class=\"formFields\"");
  }
 
  protected class DiscoverReqTypeDropDownTable extends DropDownTable
  {
    public DiscoverReqTypeDropDownTable()
    {
      addElement("-1",          "All Request Types");
      addElement("New",         "New");
      addElement("Maintenance", "Maintenance");
    }
  }
 
  
  public String formatDate(java.util.Date date)
  {
   return DateTimeFormatter.getFormattedDate(date, DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
  }
  
  public static final int   SB_APP_SEQ_NUM        = 1;
  public static final int   SB_DBA_NAME           = 2;
  public static final int   SB_MERCH_NUM          = 3;
  public static final int   SB_DISC_NUM           = 4;
  public static final int   SB_STATUS             = 5;
  public static final int   SB_SUBMIT_DATE        = 6;
  public static final int   SB_SEND_TYPE          = 7;
  public static final int   SB_SEND_DATE          = 8;
  public static final int   SB_RESPONSE_DATE      = 9;
  public static final int   SB_STATUS_DESC        = 10;
  public static final int   SB_TSYS               = 11;
  public static final int   SB_MMS                = 12;
  public static final int   SB_REQUEST_TYPE       = 13;
  public static final int   SB_SUBMITTED_BY       = 14;
  public static final int   SB_REP_CODE           = 15;

  private TreeSet reportData;
  
  public class StatusRecord implements Comparable
  {
    private SortByType sbt;
  
    boolean isGood        = false;
    boolean isErrorGood   = false;
    String  error         = null;
    
    long      appSeqNum;
    String    dbaName;
    String    merchantNum;
    String    discoverNum;
    String    status;
    String    reasonCode;
    String    errorFlags;
    String    dispositionDesc;
    String    statusDesc;
    String    sendType;
    String    requestType;
    String    submittedByCode;
    String    submittedByDesc;
    String    tsys;
    String    mms;
    Timestamp submitDate;
    Timestamp receiveDate;
    Timestamp sendDate;
    Timestamp responseDate;
    String    RepCode;
    
    public StatusRecord(ResultSet rs, SortByType sbt)
    {
      this.sbt = sbt;

      try
      {
        appSeqNum       = rs.getLong     ("app_seq_num");
        dbaName         = rs.getString   ("dba_name");
        merchantNum     = rs.getString   ("acquirer_merchant_num");
        discoverNum     = rs.getString   ("discover_merchant_num");
        status          = rs.getString   ("decision_status");
        reasonCode      = rs.getString   ("disposition_reason_code");
        dispositionDesc = rs.getString   ("disposition_desc");
        errorFlags      = rs.getString   ("error_flags");

        DiscoverErrorHelperBean errorBean = new DiscoverErrorHelperBean();
        isErrorGood = errorBean.setErrorReasons(errorFlags);
        
        sendType        = rs.getString   ("send_type");
        requestType     = rs.getString   ("request_type");
        submittedByCode = rs.getString   ("submitted_by");
        submittedByDesc = rs.getString   ("submitted_by_desc");


        sendDate        = rs.getTimestamp("send_date");
        responseDate    = rs.getTimestamp("response_date");
        tsys            = (rs.getTimestamp("TSYS_UPDATED") == null ? "N" : "Y");
        mms             = (rs.getTimestamp("MMS_UPDATED")  == null ? "N" : "Y");
        
        RepCode         = rs.getString("rep_code");

//        submitDate      = rs.getTimestamp("submit_date");
//        receiveDate     = rs.getTimestamp("receive_date");

        if (status.equals("Error"))
        {
          if(isErrorGood)
          {
            statusDesc = "<font color=red>Error In Fields:</font><BR>" + errorBean.getBrList(); 
          }
          else
          {
            statusDesc = "Error codes: " + errorFlags; 
          }
        }
        else if(status.equals("In Process"))
        {
          status = "In&nbsp;Process";
          statusDesc = "";
        }
        else
        {
          statusDesc = "" + reasonCode + " - " + dispositionDesc;
        }

        isGood = true;
      }
      catch (Exception e)
      {
        error = e.toString();
        System.out.println(error);
      }
    }
    
    public long     getAppSeqNum()        { return appSeqNum;                                 }
    public String   getDbaName()          { return formatBlankValue(dbaName);                 }
    public String   getMerchantNum()      { return formatBlankValue(merchantNum);             }
    public String   getRepCode()          { return formatBlankValue(RepCode);                 }
    public String   getDiscoverNum()      { return formatBlankValue(discoverNum);             }
    public String   getDecisionStatus()   { return formatBlankValue(status);                  }
    public String   getReasonCode()       { return formatBlankValue(reasonCode);              }
    public String   getDispositionDesc()  { return formatBlankValue(dispositionDesc);         }
    public String   getErrorFlags()       { return errorFlags;                                }
    public String   getStatusDesc()       { return formatBlankValue(statusDesc);              }
    public String   getSendType()         { return formatBlankValue(sendType);                }
    public String   getRequestType()      { return formatBlankValue(requestType);             }
    public String   getSubmittedByCode()  { return formatBlankValue(submittedByCode);         }
    public String   getSubmittedByDesc()  { return formatBlankValue(submittedByDesc);         }

    public String   getSubmitDate()       { return formatDate(submitDate);                    }
    public String   getReceiveDate()      { return formatDate(receiveDate);                   }
    public String   getSendDate()         { return formatBlankValue(formatDate(sendDate));    }
    public String   getResponseDate()     { return formatBlankValue(formatDate(responseDate));}
    public String   getTsys()             { return formatBlankValue(tsys);                    }
    public String   getMms()              { return formatBlankValue(mms);                     }
    
    public boolean  getIsGood()           { return isGood; }
    public String   getError()            { return error; }

    public String formatBlankValue(String test)
    {
      String result = test;

      if(test == null || test.equals(""))
      {
        result = "-----";
      }

      return result;
    }
    
    public int compareTo(Object o)
    {
      StatusRecord that = (StatusRecord)o;
      int result = 0;
      
      try
      {
        that = (StatusRecord)o;
        
        switch (sbt.getSortBy())
        {
          case SB_APP_SEQ_NUM:
            result = compare(appSeqNum,that.appSeqNum);
            break;
          
          case SB_DBA_NAME:
          default:
            result = compare(dbaName.toUpperCase(),that.dbaName.toUpperCase());
            break;
          
          case SB_MERCH_NUM:
            result = compare(merchantNum,that.merchantNum);
            break;
          
          case SB_SEND_TYPE:
            result = compare(sendType,that.sendType);
            break;

          case SB_REQUEST_TYPE:
            result = compare(requestType,that.requestType);
            break;

          case SB_SUBMITTED_BY:
            result = compare(submittedByDesc,that.submittedByDesc);
            break;

          case SB_SEND_DATE:
            result = compare(sendDate,that.sendDate);
            break;
            
          case SB_RESPONSE_DATE:
            result = compare(responseDate,that.responseDate);
            break;

          case SB_DISC_NUM:
            result = compare(discoverNum,that.discoverNum);
            break;
          
          case SB_STATUS:
            result = compare(status,that.status);
            break;

          case SB_STATUS_DESC:
            result = compare(statusDesc,that.statusDesc);
            break;
          
          case SB_SUBMIT_DATE:
            result = compare(submitDate,that.submitDate);
            break;

          case SB_TSYS:
            result = compare(tsys,that.tsys);
            break;

          case SB_MMS:
            result = compare(mms,that.mms);
            break;
            
          case SB_REP_CODE:
            result = compare(RepCode,that.RepCode);
            break;
        }
      }
      catch (Exception e) {}
      
      if (result == 0)
      {
        try
        {
          result = compare(appSeqNum,that.appSeqNum);
        }
        catch (Exception e) {}
      }
          
      // if reverse sort specified, flip the result around
      if (sbt.getReverseSort())
      {
        result *= (-1);
      }
      
      return result;
    }
  }
  
  public boolean dataLoaded = false;
  public boolean isFirstTime = true;
  
  public void getData() throws Exception
  {
    long                  nodeId        = 0L;
    ResultSetIterator     it            = null;
    ResultSet             rs            = null;
    
    try
    {
      connect();
      
      nodeId        = user.getHierarchyNode();
      if ( nodeId == HierarchyTree.DEFAULT_HIERARCHY_NODE )
      {
        nodeId = 394100000L;
      }
      
      String lookupValue = getData("lookupValue");

      String status      = getData("status");
      if (status.trim().equals(""))
      {
        status = "-1";
      }


      String transType   = getData("transType");
      if (transType.trim().equals(""))
      {
        transType = "-1";
      }

      String reqType     = getData("reqType");
      if (reqType.trim().equals(""))
      {
        reqType = "-1";
      }

      String partNum     = (user.getAppType() == mesConstants.APP_TYPE_BBT) ? "9849" : "-1";

      // long version of lookup value
      long longLookup = -2L;
      try
      {
        longLookup = Long.parseLong(lookupValue.trim());
      }
      catch (Exception e) { }
      
      // string version (with wildcards) of lookup value
      String stringLookup = "";
      if (lookupValue.trim().equals(""))
      {
        stringLookup = "passall";
      }
      else
      {
        stringLookup = "%" + lookupValue.toUpperCase() + "%";
      }
      
      java.sql.Date fromDate  = this.fromDate.getSqlDate();
      java.sql.Date toDate    = this.toDate.getSqlDate();

      /*@lineinfo:generated-code*//*@lineinfo:412^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  i.app_seq_num,
//                  i.dba_name,
//                  i.app_date                 send_date,
//                  i.acquirer_merchant_num,
//                  mr.merch_rep_code       as rep_code,
//                  i.TSYS_UPDATED,
//                  i.MMS_UPDATED,
//                  u.login_name      submitted_by,
//                  u.name            submitted_by_desc,
//                  decode(i.request_type,null,'Maintenance',i.request_type) request_type,
//                  r.discover_merchant_num,
//                  r.disposition_reason_code,
//                  r.error_flags,
//                  r.date_last_updated        response_date,
//                  t.trans_type_desc          send_type,
//                  decode(s.DECISION_DESC, null, 'In Process', s.DECISION_DESC)            decision_status,                 
//                  d.disposition_desc
//          from    t_hierarchy                    th,
//                  app_status_app_types           asat,
//                  application                    app,
//                  MERCHANT_DISCOVER_APP          i,
//                  MERCHANT_DISCOVER_APP_RESPONSE r,
//                  rap_app_disposition_codes      d,
//                  RAP_APP_DECISION_STATUS        s,
//                  MERCHANT_DISCOVER_APP_TYPES    t,
//                  users                          u,
//                  merchant                       mr
//          where   th.hier_type = 1
//                  and th.ancestor = :nodeId
//                  and asat.hierarchy_node = th.descendent
//                  and app.app_type = asat.app_type
//                  and i.app_seq_num = app.app_seq_num
//                  and i.app_seq_num = r.app_seq_num(+) 
//                  and ( 
//                        (:fromDate is null and trunc(i.app_date) >= trunc(sysdate)-180) or  
//                        (not :fromDate is null and trunc(i.app_date) between :fromDate and :toDate)
//                      )
//                  and r.disposition_reason_code = d.disposition_code(+)
//                  and r.decision_status         = s.decision_code(+)
//                  and i.transmission_method     = t.trans_type_code(+)
//                  and i.submitted_by            = u.user_id(+) 
//                  --and i.mms_updated is null
//                  and ('-1' = :partNum   or i.partner_num         = :partNum)
//                  and ('-1' = :status    or r.decision_status     = :status)
//                  and ('-1' = :transType or i.transmission_method = :transType)
//                  and ('-1' = :reqType   or i.request_type        = :reqType)
//                  and ( 'passall'                   = :stringLookup
//                        or i.acquirer_merchant_num  = :longLookup
//                        or r.app_seq_num            = :longLookup
//                        or upper(i.dba_name)     like :stringLookup )
//                  and mr.app_seq_num(+) = i.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  i.app_seq_num,\n                i.dba_name,\n                i.app_date                 send_date,\n                i.acquirer_merchant_num,\n                mr.merch_rep_code       as rep_code,\n                i.TSYS_UPDATED,\n                i.MMS_UPDATED,\n                u.login_name      submitted_by,\n                u.name            submitted_by_desc,\n                decode(i.request_type,null,'Maintenance',i.request_type) request_type,\n                r.discover_merchant_num,\n                r.disposition_reason_code,\n                r.error_flags,\n                r.date_last_updated        response_date,\n                t.trans_type_desc          send_type,\n                decode(s.DECISION_DESC, null, 'In Process', s.DECISION_DESC)            decision_status,                 \n                d.disposition_desc\n        from    t_hierarchy                    th,\n                app_status_app_types           asat,\n                application                    app,\n                MERCHANT_DISCOVER_APP          i,\n                MERCHANT_DISCOVER_APP_RESPONSE r,\n                rap_app_disposition_codes      d,\n                RAP_APP_DECISION_STATUS        s,\n                MERCHANT_DISCOVER_APP_TYPES    t,\n                users                          u,\n                merchant                       mr\n        where   th.hier_type = 1\n                and th.ancestor =  :1 \n                and asat.hierarchy_node = th.descendent\n                and app.app_type = asat.app_type\n                and i.app_seq_num = app.app_seq_num\n                and i.app_seq_num = r.app_seq_num(+) \n                and ( \n                      ( :2  is null and trunc(i.app_date) >= trunc(sysdate)-180) or  \n                      (not  :3  is null and trunc(i.app_date) between  :4  and  :5 )\n                    )\n                and r.disposition_reason_code = d.disposition_code(+)\n                and r.decision_status         = s.decision_code(+)\n                and i.transmission_method     = t.trans_type_code(+)\n                and i.submitted_by            = u.user_id(+) \n                --and i.mms_updated is null\n                and ('-1' =  :6    or i.partner_num         =  :7 )\n                and ('-1' =  :8     or r.decision_status     =  :9 )\n                and ('-1' =  :10  or i.transmission_method =  :11 )\n                and ('-1' =  :12    or i.request_type        =  :13 )\n                and ( 'passall'                   =  :14 \n                      or i.acquirer_merchant_num  =  :15 \n                      or r.app_seq_num            =  :16 \n                      or upper(i.dba_name)     like  :17  )\n                and mr.app_seq_num(+) = i.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.DiscoverStatusLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,fromDate);
   __sJT_st.setDate(3,fromDate);
   __sJT_st.setDate(4,fromDate);
   __sJT_st.setDate(5,toDate);
   __sJT_st.setString(6,partNum);
   __sJT_st.setString(7,partNum);
   __sJT_st.setString(8,status);
   __sJT_st.setString(9,status);
   __sJT_st.setString(10,transType);
   __sJT_st.setString(11,transType);
   __sJT_st.setString(12,reqType);
   __sJT_st.setString(13,reqType);
   __sJT_st.setString(14,stringLookup);
   __sJT_st.setLong(15,longLookup);
   __sJT_st.setLong(16,longLookup);
   __sJT_st.setString(17,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.DiscoverStatusLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:465^7*/

      rs = it.getResultSet();
      reportData = new TreeSet();
      while (rs.next())
      {
        reportData.add(new StatusRecord(rs,sbt));
      }
      
      rs.close();
      it.close();
      
      dataLoaded = true;
    }
    catch (Exception e)
    {
      // log the error...
      String func = this.getClass().getName() + "::getData()"; 
      String desc = e.toString();
      logEntry(func,desc);
      
      // ...but still throw the exception
      throw new Exception(e.getMessage());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public Iterator iterator() throws Exception
  {
    // load data if this is a new search 
    // or first time to the page
    if (isFirstTime || isSubmitFlag.getData().equals("Y"))
    {
      isFirstTime = false;
      getData();
    }
    else
    {
      // if sort by is new, re-sort the report data
      int fieldSortBy = Integer.parseInt(sortByField.getData());
      int sbtSortBy = sbt.getSortBy() * (sbt.getReverseSort() ? -1 : 1);
      
      if (fieldSortBy != sbtSortBy)
      {
        sbt.setSortBy(fieldSortBy);
        TreeSet ts = new TreeSet();
        for (Iterator i = reportData.iterator(); i.hasNext();)
        {
          ts.add(i.next());
        }
        reportData = ts;
      }
    }
    
    // return an iterator into the report data
    return reportData.iterator();
  }
  
  public int getSortByType()
  {
    return Integer.parseInt(sortByField.getData());
  }
  
  public void setUserBean(UserBean user)
  {
    this.user = user;
  }

}/*@lineinfo:generated-code*/