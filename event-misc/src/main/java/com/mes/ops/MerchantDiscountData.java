/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/MerchantDiscountData.java $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/21/00 10:32a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.DBConnect;

public class MerchantDiscountData
{
  public static final String  DB_CONNECT_STRING       = "pool;ApplicationPool";

  public class DiscountData
  {
    public int        cardType;
    public int        serialNo;
    public String     discType;
    public float      discRate;
    public int        dRT;
    public String     discMethod;
    public float      perItem;
    public float      minDisc;
    public int        floorLimit;
    public String     betNumber;
    public String     authBet;
    public String     interchangeBet;

    public DiscountData()
    {
      cardType          = 0;
      serialNo          = 0;
      discType          = "";
      discRate          = 0;
      dRT               = 0;
      discMethod        = "";
      perItem           = 0;
      minDisc           = 0;
      floorLimit        = 0;
      betNumber         = "";
      authBet           = "";
      interchangeBet    = "";
    }
  }

  private Vector      discounts;

  public MerchantDiscountData()
  {
    discounts = new Vector();
  }
  
  public DiscountData getDiscount(int idx)
  {
    return (DiscountData)discounts.elementAt(idx);
  }

  public void fillDiscountData(long appSeqNum)
  {
    DBConnect         db        = new DBConnect(this, "fillDiscountData");
    Connection        con       = null;
    StringBuffer      qs        = new StringBuffer("");
    PreparedStatement ps        = null;
    ResultSet         rs        = null;

    try
    {
      con = db.getConnection(DB_CONNECT_STRING);

      qs.append("select ");
      qs.append("cardtype_code, ");
      qs.append("billcard_disc_type, ");
      qs.append("billcard_sr_number, ");
      qs.append("billcard_disc_method, ");
      qs.append("billcard_drt_number, ");
      qs.append("billcard_disc_rate, ");
      qs.append("billcard_peritem_fee, ");
      qs.append("billcard_disc_min, ");
      qs.append("billcard_flr_limit, ");
      qs.append("billcard_bet_number, ");
      qs.append("billcard_auth_bet, ");
      qs.append("billcard_interchg_bet_number ");
      qs.append("from billcard where app_seq_num = ?");

      ps = con.prepareStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      while(rs.next())
      {
        DiscountData    dd    = new DiscountData();

        dd.cardType         = rs.getInt   ("cardtype_code");
        dd.serialNo         = rs.getInt   ("billcard_sr_number");
        dd.discType         = rs.getString("billcard_disc_type");
        dd.discRate         = rs.getFloat ("billcard_disc_rate");
        dd.dRT              = rs.getInt   ("billcard_drt_number");
        dd.discMethod       = rs.getString("billcard_disc_method");
        dd.perItem          = rs.getFloat ("billcard_peritem_fee");
        dd.minDisc          = rs.getFloat ("billcard_disc_min");
        dd.floorLimit       = rs.getInt   ("billcard_flr_limit");
        dd.betNumber        = rs.getString("billcard_bet_number");
        dd.authBet          = rs.getString("billcard_auth_bet");
        dd.interchangeBet   = rs.getString("billcard_interchg_bet_number");

        discounts.add(dd);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "fillDiscountData: " + e.toString());
    }
    finally
    {
      try
      {
        rs.close();
      }
      catch(Exception e)
      {
      }

      try
      {
        ps.close();
      }
      catch(Exception e)
      {
      }

      try
      {
        db.releaseConnection();
      }
      catch(Exception e)
      {
      }
    }
  }
}
