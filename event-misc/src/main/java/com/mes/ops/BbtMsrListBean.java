/*@lineinfo:filename=BbtMsrListBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/BbtMsrListBean.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 5/24/04 4:48p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.CityStateZipField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class BbtMsrListBean extends FieldBean
{

  public Vector msrList = new Vector();

  /*
  ** CONSTRUCTOR
  */  
  public BbtMsrListBean()
  {
  }
  
  
  public void init()
  {
    fields.removeAllFields();
    
    try
    {    

      //number fields
      fields.add(new NumberField("repId",             5,  15, false, 0));
      fields.add(new Field("repName",                 40, 40, false));
      fields.add(new Field("addressLine1",            32, 40, false));
      fields.add(new Field("addressLine2",            32, 40, true));
      fields.add(new CityStateZipField("addressCsz",  "City, State, Zip",30,false));
      fields.add(new PhoneField("addressPhone",       "Phone Number",true));
      fields.add(new PhoneField("addressFax",         "Fax Number",true));

      addHtmlExtra("class=\"formFields\"");
    }
    catch( Exception e )
    {
      logEntry("init()",e.toString());
    }
  }
  
  /*
  ** FUNCTION setDefaults
  **
  **  This should be overloaded by derived classes if necessary
  */
  public void setDefaults()
  {
  }
  
  
  // 
  //  setProperties
  //
  //  This method is used to load class members only.  Form
  //  field data is loaded through the setFields(request) 
  //  method.  Derived classes that overload this method
  //  MUST call this instance through the super reference.
  //  
  public void setProperties( HttpServletRequest request )
  { 
  } 
 
  public void setUser(UserBean ub)
  {
    user = ub;
  }


  /***************************************************************************
  * Overridable methods
  ***************************************************************************/
  /*
  ** FUNCTION loadData
  **
  **  Loads the data for this application from the database.
  **  Must be overloaded by derived classes.
  */
  public void loadData()
  {
    if(fields.getField("repId").isBlank() || fields.getField("repId").asInteger() <= 0)
    {
      return;
    }
    
    long              repId = fields.getField("repId").asInteger();
    ResultSetIterator it    = null;

    try
    {
      connect();
      
      // business location address (no PO Boxes)
      /*@lineinfo:generated-code*//*@lineinfo:137^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rep_id              rep_id,
//                  rep_name            rep_name,
//                  address_line1       address_line1,
//                  address_line2       address_line2,
//                  address_city        address_csz_city,
//                  countrystate_code   address_csz_state,
//                  address_zip         address_csz_zip,
//                  address_phone       address_phone,
//                  address_fax         address_fax
//          from    app_merch_bbt_reps
//          where   rep_id = :repId 
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rep_id              rep_id,\n                rep_name            rep_name,\n                address_line1       address_line1,\n                address_line2       address_line2,\n                address_city        address_csz_city,\n                countrystate_code   address_csz_state,\n                address_zip         address_csz_zip,\n                address_phone       address_phone,\n                address_fax         address_fax\n        from    app_merch_bbt_reps\n        where   rep_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.BbtMsrListBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,repId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.BbtMsrListBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:151^7*/

      setFields(it.getResultSet());
      it.close();
    }
    catch (Exception e)
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  //deletes item from database
  public boolean deleteItem(String repId)
  {
    boolean result = true;

    try
    {
      connect();

      //delete the record for the rep
      /*@lineinfo:generated-code*//*@lineinfo:176^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    app_merch_bbt_reps
//          where   REP_ID = :repId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    app_merch_bbt_reps\n        where   REP_ID =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.BbtMsrListBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,repId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:181^7*/
    }
    catch(Exception e)
    {
      logEntry("deleteItem()", e.toString());
      result = false;
    }
    finally
    {
      cleanUp();
    }

    return result;
  }


  /*
  ** FUNCTION storeData
  **
  **  Stores the data from the current bean into the database.
  **  Must be overloaded by derived classes.
  */
  public void storeData()
  {
    try
    {

      connect();

      //first delete the old one if one exists.. then insert the updated one
      /*@lineinfo:generated-code*//*@lineinfo:211^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    app_merch_bbt_reps
//          where   REP_ID = :fields.getField("repId").getData()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_718 = fields.getField("repId").getData();
  try {
   String theSqlTS = "delete\n        from    app_merch_bbt_reps\n        where   REP_ID =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.BbtMsrListBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_718);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^7*/


      /*@lineinfo:generated-code*//*@lineinfo:219^7*/

//  ************************************************************
//  #sql [Ctx] { insert into  app_merch_bbt_reps
//          (
//            REP_ID, 
//            REP_NAME, 
//            ADDRESS_LINE1, 
//            ADDRESS_LINE2, 
//            ADDRESS_CITY, 
//            ADDRESS_ZIP, 
//            COUNTRYSTATE_CODE, 
//            ADDRESS_PHONE, 
//            ADDRESS_FAX 
//          )
//          values
//          (
//            :fields.getField("repId").getData(),
//            :fields.getField("repName").getData(),
//            :fields.getField("addressLine1").getData(),
//            :fields.getField("addressLine2").getData(),
//            :fields.getField("addressCszCity").getData(),
//            :fields.getField("addressCszZip").getData(),
//            :fields.getField("addressCszState").getData(),
//            :fields.getField("addressPhone").getData(),
//            :fields.getField("addressFax").getData()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_719 = fields.getField("repId").getData();
 String __sJT_720 = fields.getField("repName").getData();
 String __sJT_721 = fields.getField("addressLine1").getData();
 String __sJT_722 = fields.getField("addressLine2").getData();
 String __sJT_723 = fields.getField("addressCszCity").getData();
 String __sJT_724 = fields.getField("addressCszZip").getData();
 String __sJT_725 = fields.getField("addressCszState").getData();
 String __sJT_726 = fields.getField("addressPhone").getData();
 String __sJT_727 = fields.getField("addressFax").getData();
   String theSqlTS = "insert into  app_merch_bbt_reps\n        (\n          REP_ID, \n          REP_NAME, \n          ADDRESS_LINE1, \n          ADDRESS_LINE2, \n          ADDRESS_CITY, \n          ADDRESS_ZIP, \n          COUNTRYSTATE_CODE, \n          ADDRESS_PHONE, \n          ADDRESS_FAX \n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.BbtMsrListBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_719);
   __sJT_st.setString(2,__sJT_720);
   __sJT_st.setString(3,__sJT_721);
   __sJT_st.setString(4,__sJT_722);
   __sJT_st.setString(5,__sJT_723);
   __sJT_st.setString(6,__sJT_724);
   __sJT_st.setString(7,__sJT_725);
   __sJT_st.setString(8,__sJT_726);
   __sJT_st.setString(9,__sJT_727);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:245^7*/
    }
    catch(Exception e)
    {
      logEntry("storeData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void loadMsrList()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:266^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    rep_id            rep_id,
//                    rep_name          rep_name,
//                    address_line1     address_line1,
//                    address_line2     address_line2,
//                    address_city      address_csz_city,
//                    countrystate_code address_csz_state,
//                    address_zip       address_csz_zip,
//                    address_phone     address_phone,
//                    address_fax       address_fax
//          from      app_merch_bbt_reps
//          order by  rep_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    rep_id            rep_id,\n                  rep_name          rep_name,\n                  address_line1     address_line1,\n                  address_line2     address_line2,\n                  address_city      address_csz_city,\n                  countrystate_code address_csz_state,\n                  address_zip       address_csz_zip,\n                  address_phone     address_phone,\n                  address_fax       address_fax\n        from      app_merch_bbt_reps\n        order by  rep_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.BbtMsrListBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.BbtMsrListBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:279^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        Msr msr = new Msr(rs);
        msrList.add(msr);
      }

      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("loadMsrList()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public class Msr
  {
    public String repId           = "";
    public String repName         = "";
    public String addressLine1    = "";
    public String addressLine2    = "";
    public String addressCity     = "";
    public String addressState    = "";
    public String addressZip      = "";
    public String addressPhone    = "";
    public String addressFax      = "";
  
    public Msr(ResultSet rs)
    {
      try
      {
        repId         = clearNulls(rs.getString("rep_id"));
        repName       = clearNulls(rs.getString("rep_name"));
        addressLine1  = clearNulls(rs.getString("address_line1"));
        addressLine2  = clearNulls(rs.getString("address_line2"));
        addressCity   = clearNulls(rs.getString("address_csz_city"));
        addressState  = clearNulls(rs.getString("address_csz_state"));
        addressZip    = clearNulls(rs.getString("address_csz_zip"));
        addressPhone  = clearNulls(rs.getString("address_phone"));
        addressFax    = clearNulls(rs.getString("address_fax"));
      }
      catch(Exception e)
      {
        System.out.println("Error in MSR class: " + e.toString());
      }
    }

    private String clearNulls(String str)
    {
      if(str == null)
      {
        return "";
      }

      return str.trim();
    }
  }


}/*@lineinfo:generated-code*/