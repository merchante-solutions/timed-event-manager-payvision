/*@lineinfo:filename=TimezoneTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/TimezoneTable.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/30/01 9:06p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class TimezoneTable extends DropDownTable
{
  /*
  ** CONSTRUCTOR public TimezoneTable()
  */
  public TimezoneTable()
  {
    getData();
  }
  
  public TimezoneTable(DefaultContext Ctx)
  {
    this.Ctx = Ctx;
    
    getData();
  }
  
  /*
  ** METHOD protected boolean getData()
  **
  ** Loads the timezone list.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean getData()
  {
    boolean             getOk   = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:70^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    code,
//                    description
//          from      time_zones
//          order by  code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    code,\n                  description\n        from      time_zones\n        order by  code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.TimezoneTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.TimezoneTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:76^7*/
      
      rs = it.getResultSet();
      
      addElement("", "select");
      
      while (rs.next())
      {
        addElement(rs);
        getOk = true;
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return getOk;
  }
}/*@lineinfo:generated-code*/