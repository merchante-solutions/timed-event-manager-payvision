/*@lineinfo:filename=InventoryBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/InventoryBase.sqlj $

  Description:  


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/23/04 3:18p $
  Version            : $Revision: 16 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class InventoryBase extends com.mes.screens.SequenceDataBean
{
  public static final int       ES_IN_STOCK                 = 0;
  public static final int       ES_DEPLOYED_PURCHASE        = 1;
  public static final int       ES_DEPLOYED_RENT            = 2;
  public static final int       ES_IN_STOCK_ENCRYPTED       = 3;
  public static final int       ES_OBSOLETE                 = 4;
  public static final int       ES_DEPLOYED_LEASE           = 5;
  public static final int       ES_DEPLOYED_UNKNOWN         = 6;
  public static final int       ES_REPAIR_POS_PORTAL        = 7;
  public static final int       ES_REPAIR_OTHER             = 8;
  public static final int       ES_HELP_DESK                = 9;
  public static final int       ES_TRAINING                 = 10;
  public static final int       ES_ON_LOAN                  = 11;
  public static final int       ES_DEFECTIVE                = 12;
  public static final int       ES_MISSING                  = 13;
  public static final int       ES_LOST                     = 14;
  public static final int       ES_NOT_YET_RECOVERED        = 15;
  public static final int       ES_REPAIR_VMS               = 16;
  public static final int       ES_DEAD                     = 17;
  public static final int       ES_TRANSFERRED              = 18;
    
  public class InventoryListItem
  {
    private   String        Description   = null;
    private   String        KeyValue      = null;
    private   String        PartNumber    = null;
    private   String        SerialNumber  = null;
    
    public InventoryListItem( String desc, String value )
    {
      Description   = desc;
      KeyValue      = value;
    }
    
    public InventoryListItem( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      PartNumber    = resultSet.getString("part_number");
      SerialNumber  = resultSet.getString("serial_number");
      Description   = resultSet.getString("equip_desc");
      KeyValue      = resultSet.getString("key_value");
    }
    
    public String getDescription( )
    {
      return( Description );
    }
    
    public String getKeyValue( )
    {
      return( KeyValue );
    }
  }

  public class OwnerListItem
  {
    public    int           AppTypeCode   = -1;
    public    String        OwnerName     = null;
    
    public OwnerListItem( String name, int appTypeCode )
    {
      AppTypeCode   = appTypeCode;
      OwnerName     = name;
    }
    
    public OwnerListItem( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      OwnerName     = resultSet.getString("owner_name");
      AppTypeCode   = resultSet.getInt("app_type");
    }
  }
  
  // member variables
  protected     Vector              AvailableInventory    = null;
  protected     Vector              OwnerList             = null;
  
  public InventoryBase()
  {
  }

  public void addToHistory(String partNumber, String serialNumber, long userId, int status )
  {
    addToHistory( partNumber, serialNumber, userId, status, null, null, mesConstants.APP_TYPE_UNKNOWN );
  }
  
  
  public void addToHistory(String partNumber, String serialNumber, long userId, int status, String actionList, String note )
  {
    addToHistory( partNumber, serialNumber, userId, status, actionList, note, mesConstants.APP_TYPE_UNKNOWN );
  }
  
  public void addToHistory(String partNumber, String serialNumber, long userId, int status, String actionList )
  {
    addToHistory( partNumber, serialNumber, userId, status, actionList, null, mesConstants.APP_TYPE_UNKNOWN );
  }

  public void addToHistory(String partNumber, String serialNumber, long userId, int status, String actionList, String note, int owner )
  {
    String          partNum       = partNumber;
    
    try
    {
      if ( isBlank(partNum) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:142^9*/

//  ************************************************************
//  #sql [Ctx] { select  ei.ei_part_number     
//            from    equip_inventory   ei
//            where   ei.ei_serial_number = :serialNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ei.ei_part_number      \n          from    equip_inventory   ei\n          where   ei.ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,serialNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   partNum = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:147^9*/
      }
      
      if ( owner == mesConstants.APP_TYPE_UNKNOWN )
      {
        /*@lineinfo:generated-code*//*@lineinfo:152^9*/

//  ************************************************************
//  #sql [Ctx] { select  ei.ei_owner 
//            from    equip_inventory     ei
//            where   ei.ei_part_number = :partNum and
//                    ei.ei_serial_number = :serialNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ei.ei_owner  \n          from    equip_inventory     ei\n          where   ei.ei_part_number =  :1  and\n                  ei.ei_serial_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,serialNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   owner = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:158^9*/
      }        
      
      /*@lineinfo:generated-code*//*@lineinfo:161^7*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_history 
//          (
//            part_number, 
//            eh_serial_number,
//            eh_user,
//            eh_date,
//            eh_action_performed,
//            eh_note,
//            equip_status,
//            owner
//          ) 
//          values
//          (
//            :partNum,
//            :serialNumber,
//            :userId,
//            sysdate,
//            :actionList,
//            :note,
//            :status,
//            :owner
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into equip_history \n        (\n          part_number, \n          eh_serial_number,\n          eh_user,\n          eh_date,\n          eh_action_performed,\n          eh_note,\n          equip_status,\n          owner\n        ) \n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n          sysdate,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.InventoryBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,serialNumber);
   __sJT_st.setLong(3,userId);
   __sJT_st.setString(4,actionList);
   __sJT_st.setString(5,note);
   __sJT_st.setInt(6,status);
   __sJT_st.setInt(7,owner);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:185^7*/
    }
    catch(Exception e)
    {
      logEntry("addToHistory()", e.toString());
      addError("addToHistory: " + e.toString());
    }
  }
  
  public boolean doesExist(String partNum, String serNum, int appType)
  {
    int  itemCount   = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:200^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( ei.ei_serial_number ) 
//          from    equip_inventory ei
//          where   ei.ei_part_number = :partNum and
//                  ei.ei_serial_number = :serNum and
//                  (-1 = :appType or ei.ei_owner = :appType)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( ei.ei_serial_number )  \n        from    equip_inventory ei\n        where   ei.ei_part_number =  :1  and\n                ei.ei_serial_number =  :2  and\n                (-1 =  :3  or ei.ei_owner =  :4 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,serNum);
   __sJT_st.setInt(3,appType);
   __sJT_st.setInt(4,appType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:207^7*/
    }  
    catch(Exception e)
    {
      logEntry("submitEquipment()", e.toString());
      addError("submitEquipment: " + e.toString());
    }
    return( itemCount > 0 );
  }
  
  public boolean doesExist(String partNum, String serNum)
  {
    return doesExist(partNum, serNum, mesConstants.APP_TYPE_ALL_EQUIP);
  }


  public String formatCurrencyString( String curr )
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {
    }
    return( result );
  }
  
  public Vector getAvailableInventory( int appType, boolean allButt )
  {
    if ( AvailableInventory == null )
    {
      if(allButt)
      {
        loadAvailableInventoryExcept( appType );
      }
      else
      {
        loadAvailableInventory( appType );
      }
    }
    return( AvailableInventory );
  }
  
  public Vector getAvailableInventory( int appType )
  {
    return getAvailableInventory(appType, false );
  }


  public int getClass(String partNum, String serNum)
  {
    int               result  = -1;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:266^7*/

//  ************************************************************
//  #sql [Ctx] { select  ei_class    
//          
//          from    equip_inventory
//          where   ei_part_number    = :partNum and
//                  ei_serial_number  = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ei_class    \n         \n        from    equip_inventory\n        where   ei_part_number    =  :1  and\n                ei_serial_number  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,serNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:273^7*/
    }  
    catch(Exception e)
    {
      logEntry("getClass()", e.toString());
      addError("getClass: " + e.toString());
    }
    return( result );
  }
  
  public String getEquipmentDesc( String partNumber )
  {
    String          retVal      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:289^7*/

//  ************************************************************
//  #sql [Ctx] { select  equip_descriptor 
//          
//          from    equipment
//          where   equip_model = :partNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  equip_descriptor \n         \n        from    equipment\n        where   equip_model =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,partNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:295^7*/
    }  
    catch(Exception e)
    {
      logEntry("getEquipmentDesc()", e.toString());
    }
    return( retVal );
  }
  
  public String getEquipmentPartNumber( String serialNum )
  {
    String          retVal      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:310^7*/

//  ************************************************************
//  #sql [Ctx] { select  ei_part_number 
//          
//          from    equip_inventory
//          where   ei_serial_number = :serialNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ei_part_number \n         \n        from    equip_inventory\n        where   ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,serialNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:316^7*/
    }  
    catch(Exception e)
    {
      logEntry("getEquipmentPartNumber()", e.toString());
    }
    return( retVal );
  }


  public int getEquipmentStatus( String partNum, String serialNum )
  {
    int               retVal      = -1;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:332^7*/

//  ************************************************************
//  #sql [Ctx] { select  ei.ei_status 
//          from    equip_inventory     ei
//          where   ei.ei_part_number = :partNum and
//                  ei.ei_serial_number = :serialNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ei.ei_status  \n        from    equip_inventory     ei\n        where   ei.ei_part_number =  :1  and\n                ei.ei_serial_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,serialNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:338^7*/
    }
    catch( Exception e )
    {
      logEntry( "getEquipmentStatus()", e.toString() );
    }
    return( retVal );
  }
  
  public String getOwnerDesc( int appType )
  {
    String          retVal      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:353^7*/

//  ************************************************************
//  #sql [Ctx] { select  inventory_owner_name    
//          from    app_type 
//          where   app_type_code = :appType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  inventory_owner_name     \n        from    app_type \n        where   app_type_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:358^7*/
    }  
    catch(Exception e)
    {
      logEntry("getOwnerDesc()", e.toString());
    }
    return( retVal );
  }
  
  public Vector getOwnerList()
  {
    if ( OwnerList == null )
    {
      loadOwnerList();
    }
    return( OwnerList );
  }
  
  public String getStatusDesc( int statusCode )
  {
    String result = "";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:382^7*/

//  ************************************************************
//  #sql [Ctx] { select  equip_status_desc   
//          from    equip_status
//          where   equip_status_id = :statusCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  equip_status_desc    \n        from    equip_status\n        where   equip_status_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,statusCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:387^7*/
    }
    catch(Exception e)
    {
      logEntry("getStatusDesc()",e.toString());
    }
    return( result );
  }
  
  public boolean isDate( String date )
  {
    int index1  = date.indexOf('/');
    int index2  = date.lastIndexOf('/');
    String mon  = date.substring(0,index1);
    String day  = date.substring(index1+1,index2);
    String year = date.substring(index2+1);
    boolean result = true;
    try
    {
      int dayInt = Integer.parseInt(day);
      int monInt = Integer.parseInt(mon);
      int yearInt = Integer.parseInt(year);
      if(dayInt > 31 || dayInt < 1)
        result = false;
      if(monInt > 12 || monInt < 1)
        result = false;
      if(yearInt < 0)
        result = false;
    }
    catch(Exception e)
    {
      result = false;
    }
    return( result );
  }
  
  public boolean isInStock(String partNum, String serNum, int appType)
  {
    int             lrb     = -1;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:428^7*/

//  ************************************************************
//  #sql [Ctx] { select  ei.ei_lrb  
//          from    equip_inventory   ei
//          where   ei.ei_part_number   = :partNum and
//                  ei.ei_serial_number = :serNum  and
//                  (-1 = :appType or ei.ei_owner = :appType)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ei.ei_lrb   \n        from    equip_inventory   ei\n        where   ei.ei_part_number   =  :1  and\n                ei.ei_serial_number =  :2   and\n                (-1 =  :3  or ei.ei_owner =  :4 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,serNum);
   __sJT_st.setInt(3,appType);
   __sJT_st.setInt(4,appType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   lrb = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:435^7*/                
    }  
    catch(Exception e)
    {
      logEntry("isInStock()", e.toString());
      addError("isInStock: " + e.toString());
    }
    return( lrb == mesConstants.APP_EQUIP_IN_STOCK );
    
  }

  public boolean isInStock(String partNum, String serNum)
  {
    return isInStock(partNum, serNum, mesConstants.APP_TYPE_ALL_EQUIP);
  }


  public boolean isInInventory(String serNum)
  {
    boolean           result  = false;
    
    try
    {
      if(! isBlank(serNum))
      {
        int equipCount = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:462^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(ei_serial_number)
//            
//            from    equip_inventory
//            where   ei_serial_number = :serNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ei_serial_number)\n           \n          from    equip_inventory\n          where   ei_serial_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,serNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   equipCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:468^9*/
        
        if(equipCount > 0)
        {
          result = true;
        }
      }
    }  
    catch(Exception e)
    {
      logEntry("isInInventory(" + serNum + ")", e.toString());
      addError("isInInventory: " + e.toString());
    }
    
    return result;
  }
  
  public boolean isMerchant( long merchantId )
  {
    int               itemCount   = 0;
    boolean           wasStale    = false;
    
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:498^7*/

//  ************************************************************
//  #sql [Ctx] { select count(mf.merchant_number) 
//          from    mif       mf
//          where   mf.merchant_number = :merchantId 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(mf.merchant_number)  \n        from    mif       mf\n        where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:503^7*/
      
      if ( itemCount == 0 )
      {
        // look for an application that has not 
        // been entered into TSYS yet (unlikely)
        /*@lineinfo:generated-code*//*@lineinfo:509^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(mr.merch_number) 
//            from    merchant    mr
//            where   mr.merch_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mr.merch_number)  \n          from    merchant    mr\n          where   mr.merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:514^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("isMerchant(" + merchantId + ")", e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
    
    return( itemCount > 0 );
  }
  
  public boolean isMesInventory(String partNum, String serNum)
  {
    int               owner       = -1;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:538^7*/

//  ************************************************************
//  #sql [Ctx] { select  ei.ei_owner    
//          from    equip_inventory     ei
//          where   ei.ei_part_number    = :partNum and
//                  ei.ei_serial_number  = :serNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ei.ei_owner     \n        from    equip_inventory     ei\n        where   ei.ei_part_number    =  :1  and\n                ei.ei_serial_number  =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.ops.InventoryBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,partNum);
   __sJT_st.setString(2,serNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   owner = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:544^7*/
    }  
    catch(Exception e)
    {
      logEntry("isMesInventory()", e.toString());
      addError("isMesInventory: " + e.toString());
    }
    return( owner == mesConstants.APP_TYPE_MES );
  }
  
  public boolean isNumber(String num)
  {
    boolean     retVal = false;
    
    try
    {
      double temp = Double.parseDouble(num);
      retVal       = true;
    }
    catch(Exception e)
    {
    }
    return( retVal );
  }
  
  public void loadAvailableInventoryExcept(int appType)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    if ( AvailableInventory == null )
    {
      AvailableInventory = new Vector();
    }
    else
    {
      AvailableInventory.removeAllElements();
    }
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:585^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_serial_number               as serial_number,
//                  ei.ei_part_number                 as part_number,
//                  ei.ei_serial_number || ' - ' || 
//                  equip.equip_descriptor          as equip_desc,
//                  ei.ei_part_number || ';' || 
//                  ei.ei_serial_number             as key_value,
//                  clas.ec_class_name                as class_name
//          from    equipment       equip,
//                  equip_inventory ei,
//                  equip_class     clas
//          where   ei.ei_class = clas.ec_class_id and 
//                  ei.ei_part_number = equip.equip_model and 
//                  ei.ei_lrb = 0 and 
//                  (-1 = :appType or ei.ei_owner != :appType)
//          order by  equip.equip_descriptor, 
//                    ei.ei_serial_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_serial_number               as serial_number,\n                ei.ei_part_number                 as part_number,\n                ei.ei_serial_number || ' - ' || \n                equip.equip_descriptor          as equip_desc,\n                ei.ei_part_number || ';' || \n                ei.ei_serial_number             as key_value,\n                clas.ec_class_name                as class_name\n        from    equipment       equip,\n                equip_inventory ei,\n                equip_class     clas\n        where   ei.ei_class = clas.ec_class_id and \n                ei.ei_part_number = equip.equip_model and \n                ei.ei_lrb = 0 and \n                (-1 =  :1  or ei.ei_owner !=  :2 )\n        order by  equip.equip_descriptor, \n                  ei.ei_serial_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.ops.InventoryBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   __sJT_st.setInt(2,appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.ops.InventoryBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:603^7*/
      rs = it.getResultSet();

      AvailableInventory.addElement( new InventoryListItem( "select one","" ) );
      
      while(rs.next())
      {
        AvailableInventory.addElement( new InventoryListItem( rs ) );
      }
      rs.close();
      it.close();
    }  
    catch(Exception e)
    {
      logEntry("loadAvailableInventoryExcept()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ){ }
    }
  }


  public void loadAvailableInventory( int appType )
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    if ( AvailableInventory == null )
    {
      AvailableInventory = new Vector();
    }
    else
    {
      AvailableInventory.removeAllElements();
    }
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:642^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.ei_serial_number               as serial_number,
//                  ei.ei_part_number                 as part_number,
//                  ei.ei_serial_number || ' - ' || 
//                    equip.equip_descriptor          as equip_desc,
//                  ei.ei_part_number || ';' || 
//                    ei.ei_serial_number             as key_value,
//                  clas.ec_class_name                as class_name
//          from    equipment equip,
//                  equip_inventory ei,
//                  equip_class clas
//          where   ei.ei_class = clas.ec_class_id and 
//                  ei.ei_part_number = equip.equip_model and 
//                  ei.ei_lrb = 0 and 
//                  (-1 = :appType or ei.ei_owner = :appType)
//          order by  equip.equip_descriptor, ei.ei_serial_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.ei_serial_number               as serial_number,\n                ei.ei_part_number                 as part_number,\n                ei.ei_serial_number || ' - ' || \n                  equip.equip_descriptor          as equip_desc,\n                ei.ei_part_number || ';' || \n                  ei.ei_serial_number             as key_value,\n                clas.ec_class_name                as class_name\n        from    equipment equip,\n                equip_inventory ei,\n                equip_class clas\n        where   ei.ei_class = clas.ec_class_id and \n                ei.ei_part_number = equip.equip_model and \n                ei.ei_lrb = 0 and \n                (-1 =  :1  or ei.ei_owner =  :2 )\n        order by  equip.equip_descriptor, ei.ei_serial_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.ops.InventoryBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   __sJT_st.setInt(2,appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.ops.InventoryBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:659^7*/
      rs = it.getResultSet();

      AvailableInventory.addElement( new InventoryListItem( "select one","" ) );
      
      while(rs.next())
      {
        AvailableInventory.addElement( new InventoryListItem( rs ) );
      }
      rs.close();
      it.close();
    }  
    catch(Exception e)
    {
      logEntry("loadAvailableInventory()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ){ }
    }
  }
  
  public void loadOwnerList()
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    if ( OwnerList == null )
    {
      OwnerList = new Vector();
    }
    else
    {
      OwnerList.removeAllElements();
    }
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:697^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_type_code           as app_type,
//                  inventory_owner_name    as owner_name
//          from    app_type 
//          where   SEPARATE_INVENTORY = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_type_code           as app_type,\n                inventory_owner_name    as owner_name\n        from    app_type \n        where   SEPARATE_INVENTORY = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.ops.InventoryBase",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.ops.InventoryBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:703^7*/
      rs = it.getResultSet();

      OwnerList.addElement( new OwnerListItem("select owner", -1) );
      
      while(rs.next())
      {
        OwnerList.addElement( new OwnerListItem(rs) );
      }
      rs.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry( "loadOwnerList()",e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  public String replaceNewLine(String newLine)
  {
    String      retVal = "";
    
    if(!isBlank(newLine))
    {
      String body = newLine;
      try
      {
        StringBuffer    bodyBuff = null;
        int             index = 0;
      
        index = body.indexOf('\n');
      
        while(index > 0)
        {
          bodyBuff = new StringBuffer(body);
          bodyBuff.replace(index,index+1,"<BR>");
          body = bodyBuff.toString();
          index = body.indexOf('\n', index);
        }
        retVal = body;
      }
      catch(Exception e)
      {
        System.out.println("error replace new line");
      }
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/