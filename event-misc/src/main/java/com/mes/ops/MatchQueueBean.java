/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/MatchQueueBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 9/24/02 12:08p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

public class MatchQueueBean extends QueueBean
{
  public void fillReportMenuItems()
  {
    addReportMenuItem(new ReportMenuItem("Match Pend Queue", "mes_queue.jsp?queueType=" + QueueConstants.QUEUE_MATCH + "&queueStage=" + QueueConstants.Q_MATCH_PEND));
  }
  
  public MatchQueueBean()
  {
    super("MatchQueueBean");
    fillReportMenuItems();
  }
  
  public MatchQueueBean(String msg)
  {
    super(msg);
    fillReportMenuItems();
  }
  
  public boolean getQueueData(long queueKey, int queueType)
  {
    return(super.getQueueData(
            queueKey,
            queueType,
            "merchant merch",
            "merch.merc_cntrl_number, merch.merch_business_name",
            "and a.app_seq_num = merch.app_seq_num"));
  }
  
  public void getFunctionOptions(javax.servlet.jsp.JspWriter out, long appSeqNum)
  {
    try
    {
      out.println("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" width=\"80%\">");

      out.println("  <tr>");
      out.println("    <td height=\"19\" valign=\"center\">");
      out.println("      &nbsp;");
      out.println("    </td>");
      out.println("    <td height=\"19\" valign=\"left\">");
      out.println("      <a href=\"/jsp/credit/match_inquiry.jsp?asn=" + appSeqNum + "\">View Match Inquiry Results</a>");
      out.println("    </td>");
      out.println("  </tr>");

      out.println("</table>");
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFunctionOptions: " + e.toString());
    }
  }
}
