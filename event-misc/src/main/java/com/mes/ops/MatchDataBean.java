/*@lineinfo:filename=MatchDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/MatchDataBean.sqlj $

  Description:

  Extends the date range report bean to generate a sales activity report.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 8/28/02 11:21a $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class MatchDataBean extends SQLJConnectionBase
{
  private long    controlNumber         = 0L;
  private long    appSeqNum             = 0L;
  private String  matchResponse         = "";
  private String  matchResponseDesc     = "";
  
  public MatchDataBean()
  {
  }
  
  public MatchDataBean(DefaultContext Ctx)
  {
    this.Ctx = Ctx;
  }
  
  private void setMatchData()
  {
    ResultSetIterator   it      = null;
    try
    {
      connect();
      
      if(appSeqNum == 0L)
      {
        if(controlNumber > 0L)
        {
          // get appSeqNum from control number
          /*@lineinfo:generated-code*//*@lineinfo:62^11*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num
//              
//              from    merchant
//              where   merc_cntrl_number = :controlNumber
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num\n             \n            from    merchant\n            where   merc_cntrl_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.MatchDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,controlNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:68^11*/
        }
        else
        {
          // no control number so appSeqNum will be empty and match data will be null
        }
      }
    
      // get match data from MATCH_REQUESTS table using appSeqNUm
      /*@lineinfo:generated-code*//*@lineinfo:77^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(mr.response_action, ' ')    response,
//                  md.description                  description
//          from    match_requests mr,
//                  match_response_action_desc md
//          where   mr.app_seq_num = :appSeqNum and
//                  mr.request_action = md.request_action and
//                  mr.response_action = md.response_action
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(mr.response_action, ' ')    response,\n                md.description                  description\n        from    match_requests mr,\n                match_response_action_desc md\n        where   mr.app_seq_num =  :1  and\n                mr.request_action = md.request_action and\n                mr.response_action = md.response_action";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.MatchDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.MatchDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:86^7*/

      ResultSet rs = it.getResultSet();
      
      if(it.next())
      {
        this.matchResponse      = rs.getString("response");
        this.matchResponseDesc  = rs.getString("description");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("setMatchData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public String getMatchResponse()
  {
    return this.matchResponse;
  }
  
  public String getMatchResponseDesc()
  {
    return this.matchResponseDesc;
  }
  
  public void setPrimaryKey(String primaryKey)
  {
    try
    {
      this.appSeqNum = Long.parseLong(primaryKey);
      setMatchData();
    }
    catch(Exception e)
    {
      logEntry("setPrimaryKey(" + primaryKey + ")", e.toString());
    }
  }
  
  public void setControlNumber(String controlNumber)
  {
    try
    {
      if(controlNumber != null && !controlNumber.equals(""))
      {
        this.controlNumber = Long.parseLong(controlNumber);
        setMatchData();
      }
    }
    catch(Exception e)
    {
      logEntry("setControlNumber(" + controlNumber + ")", e.toString());
    }
  }
}/*@lineinfo:generated-code*/