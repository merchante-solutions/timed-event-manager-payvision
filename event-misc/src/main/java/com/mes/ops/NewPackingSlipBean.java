/*@lineinfo:filename=NewPackingSlipBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/NewPackingSlipBean.sqlj $

  Description:
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import com.mes.config.MesDefaults;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.queues.QueueTools;
import com.mes.screens.SequenceDataBean;
import com.mes.support.DateTimeFormatter;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class NewPackingSlipBean extends SequenceDataBean
{
  public static final int SHIPPING_COMMENT_LENGTH     = 150;
  public static final int EQUIPMENT_COMMENT_LENGTH    = 300;

  public static final int SATURDAY_DELIVERY           = 1;
  public static final int PRIORITY_DELIVERY           = 2;
  public static final int STANDARD_OVERNIGHT          = 3;
  public static final int FEDEX_2DAY                  = 4;
  public static final int EXPRESS_SAVER               = 5;
  public static final int FEDEX_GROUND_DELIVERY       = 6;
  public static final int USPS_PRIORITY_MAIL          = 7;
  public static final int USPS_MAIL                   = 8;

  public static final int TIMEZONE_EASTERN            = 705;
  public static final int TIMEZONE_CENTRAL            = 706;
  public static final int TIMEZONE_MOUNTAIN           = 707;
  public static final int TIMEZONE_PACIFIC            = 708;
  public static final int TIMEZONE_ALASKA             = 709;
  public static final int TIMEZONE_ALEUTIAN           = 710;
  public static final int TIMEZONE_HAWAII             = 710;

  //static
  private String dbaName                          = "";
  private String merchNum                         = "";
  private String mcc                              = "";
  private String controlNum                       = "";
  private String appTypeDesc                      = "";
  private String appCreatedDate                   = "";
  private String contactName                      = "";
  private String contactPhone                     = "";
  private String conversionType                   = "New Account";

  //dynamic
  private String submittedBy                      = "";
  private String submittedDate                    = "Not Yet Submitted";

  private String shippingMethodSingle             = "";
  private String timeZoneSingle                   = "";
  private String rushAuthorizationSingle          = "";

  private String merchantType                     = "";
  private String dialPayType                      = "";

  private String rushOrderCheckBox                = "";
  private String shipEquipmentCheckBox            = "";
  private String callTagCheckBox                  = "";
  private String callTagItem                      = "";
  private String callTagItemSerialNum             = "";
  private String accessCodeDesc                   = "";

  private String physicalAddressLine1             = "";
  private String physicalAddressLine2             = "";
  private String physicalAddressCity              = "";
  private String physicalAddressState             = "";
  private String physicalAddressZip               = "";

  private String shippingName                     = "";
  private String shippingAddress1                 = "";
  private String shippingAddress2                 = "";
  private String shippingCity                     = "";
  private String shippingState                    = "";
  private String shippingZip                      = "";
  private String shippingPhone                    = "";
  private String shippingContactName              = "";
  private String shippingComment                  = "";

  private String equipmentComment                 = "";
  private String airbill                          = "";

  private String visaAccepted                     = "";
  private String amexAccepted                     = "";
  private String discAccepted                     = "";
  private String debitAccepted                    = "";
  private String discoverNumber                   = "No Discover Number";
  private String ebtAccepted                      = "";
  private String CheckAccepted                    = "";
  private String CheckProvider                    = "";
  private String ValutecAccepted                  = "";

  private String pcCard                           = "N";
  private String pcCardLevelII                    = "N";
  private String pcCardLevelIII                   = "N";

  private String specialComment                   = "";
  private String imprinterPlateQty                = "";
  private String tipOptionCheck                   = "";
  //end dynamic items

  //discover printed material variables
  private String  coverSheetPrinted               = "";
  private String  pricingSchedulePrinted          = "";
  private boolean packingSlipSubmitted            = false;
  private boolean discoverApp                     = false;

  private boolean elmhurstNonDeployApp            = false;

  private int    numOfTerminals                   = 0;

  private Vector  vNums                           = new Vector();
  private Vector  terminals                       = new Vector();
  private Vector  multiMerchants                  = new Vector();

  public  Vector  method                          = new Vector();
  public  Vector  methodDesc                      = new Vector();

  public  Vector  managers                        = new Vector();
  public  Vector  dialpayTypeDesc                 = new Vector();
  public  Vector  merchantTypeDesc                = new Vector();

  public  Vector  timezone                        = new Vector();
  public  Vector  timezoneDesc                    = new Vector();

  public  Vector  equipStatusCode                 = new Vector();
  public  Vector  equipStatus                     = new Vector();

  public  Vector  printer                         = new Vector();
  public  Vector  printerCode                     = new Vector();

  public  Vector  pinpad                          = new Vector();
  public  Vector  pinpadCode                      = new Vector();

  public  Vector  terminal                        = new Vector();
  public  Vector  terminalCode                    = new Vector();

  public  Vector  stateCode                       = new Vector();
  public  Vector  stateDesc                       = new Vector();

  public NewPackingSlipBean()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:183^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    equipment
//          where   equiptype_code in
//                  (
//                    :mesConstants.APP_EQUIP_TYPE_PRINTER,
//                    :mesConstants.APP_EQUIP_TYPE_PINPAD,
//                    :mesConstants.APP_EQUIP_TYPE_TERMINAL,
//                    :mesConstants.APP_EQUIP_TYPE_TERM_PRINTER,
//                    :mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD
//                  )
//          order by equip_descriptor
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    equipment\n        where   equiptype_code in\n                (\n                   :1 ,\n                   :2 ,\n                   :3 ,\n                   :4 ,\n                   :5 \n                )\n        order by equip_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_EQUIP_TYPE_PRINTER);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_TYPE_PINPAD);
   __sJT_st.setInt(3,mesConstants.APP_EQUIP_TYPE_TERMINAL);
   __sJT_st.setInt(4,mesConstants.APP_EQUIP_TYPE_TERM_PRINTER);
   __sJT_st.setInt(5,mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:196^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        switch(rs.getInt("equiptype_code"))
        {
          case mesConstants.APP_EQUIP_TYPE_PRINTER:
            printer.add(rs.getString("equip_descriptor"));
            printerCode.add(rs.getString("equip_model"));
          break;

          case mesConstants.APP_EQUIP_TYPE_PINPAD:
            pinpad.add(rs.getString("equip_descriptor"));
            pinpadCode.add(rs.getString("equip_model"));
          break;

          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
            terminal.add(rs.getString("equip_descriptor"));
            terminalCode.add(rs.getString("equip_model"));
          break;
        }
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:226^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    countrystate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    countrystate";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.NewPackingSlipBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:230^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        stateCode.add(rs.getString("countrystate_code"));
        stateDesc.add(rs.getString("countrystate_code"));
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("constructor", e.toString());
      addError("Constructor Error: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    String newPackingSlipBeanManagers = "";
    try {
      newPackingSlipBeanManagers = MesDefaults.getString(MesDefaults.NEW_PACKING_SLIP_BEAN_MANGERS_LIST);
    } catch (Exception e) {
      //Nothing to do
    }
    if (newPackingSlipBeanManagers == null) {
      newPackingSlipBeanManagers = "";
    }
    String[] managersNamesList = newPackingSlipBeanManagers.split(",");
    for (int i = 0; i < managersNamesList.length; i++) {
      managers.add(managersNamesList[i].trim());
    }


    timezone.add(Integer.toString(TIMEZONE_PACIFIC));
    timezoneDesc.add("PDT PACIFIC 708");

    timezone.add(Integer.toString(TIMEZONE_EASTERN));
    timezoneDesc.add("EDT EASTERN 705");

    timezone.add(Integer.toString(TIMEZONE_CENTRAL));
    timezoneDesc.add("CDT CENTRAL 706");

    timezone.add(Integer.toString(TIMEZONE_MOUNTAIN));
    timezoneDesc.add("MDT MOUNTAIN 707");

    timezone.add(Integer.toString(TIMEZONE_ALASKA));
    timezoneDesc.add("AKDT ALASKA 709");

    timezone.add(Integer.toString(TIMEZONE_ALEUTIAN));
    timezoneDesc.add("ADT ALEUTIAN 710");

    timezone.add(Integer.toString(TIMEZONE_HAWAII));
    timezoneDesc.add("HST HAWAII 710");

    dialpayTypeDesc.add("AUTH ONLY");
    dialpayTypeDesc.add("AUTH W/CAPTURE");

    merchantTypeDesc.add("RESTAURANT");
    merchantTypeDesc.add("FINE DINING");
    merchantTypeDesc.add("RETAIL/MOTO");
    merchantTypeDesc.add("HOTEL");
    merchantTypeDesc.add("CASH ADVANCE");

    equipStatusCode.add(Integer.toString(mesConstants.APP_EQUIP_RENT));
    equipStatus.add("RENTAL");

    equipStatusCode.add(Integer.toString(mesConstants.APP_EQUIP_PURCHASE));
    equipStatus.add("PURCHASE");

    equipStatusCode.add(Integer.toString(mesConstants.APP_EQUIP_LEASE));
    equipStatus.add("LEASECOM");

    equipStatusCode.add(Integer.toString(mesConstants.APP_EQUIP_OWNED));
    equipStatus.add("MERCHOWN");

    equipStatusCode.add(Integer.toString(mesConstants.APP_EQUIP_LOAN));
    equipStatus.add("LOANER");
  }

  private void getMerchantGeneralInfo(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:324^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.merch_business_name,
//                  merch.merch_number,
//                  merch.merc_cntrl_number,
//                  merch.sic_code,
//                  merch.account_type,
//                  app.appsrctype_code,
//                  app.app_created_date,
//                  app.app_type,
//                  mcon.merchcont_prim_first_name,
//                  mcon.merchcont_prim_last_name,
//                  mcon.merchcont_prim_phone
//          from    merchant       merch,
//                  application    app,
//                  merchcontact   mcon
//          where   merch.app_seq_num = :primaryKey
//                  and merch.app_seq_num = app.app_seq_num
//                  and merch.app_seq_num = mcon.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.merch_business_name,\n                merch.merch_number,\n                merch.merc_cntrl_number,\n                merch.sic_code,\n                merch.account_type,\n                app.appsrctype_code,\n                app.app_created_date,\n                app.app_type,\n                mcon.merchcont_prim_first_name,\n                mcon.merchcont_prim_last_name,\n                mcon.merchcont_prim_phone\n        from    merchant       merch,\n                application    app,\n                merchcontact   mcon\n        where   merch.app_seq_num =  :1 \n                and merch.app_seq_num = app.app_seq_num\n                and merch.app_seq_num = mcon.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:343^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        dbaName         = isBlank(rs.getString("merch_business_name"))        ? "" : rs.getString("merch_business_name");
        merchNum        = isBlank(rs.getString("merch_number"))               ? "" : rs.getString("merch_number");
        mcc             = isBlank(rs.getString("sic_code"))                   ? "" : rs.getString("sic_code");
        controlNum      = isBlank(rs.getString("merc_cntrl_number"))          ? "" : rs.getString("merc_cntrl_number");
        appTypeDesc     = isBlank(rs.getString("appsrctype_code"))            ? "" : rs.getString("appsrctype_code");

        discoverApp           = (rs.getInt("app_type") == mesConstants.APP_TYPE_DISCOVER || rs.getInt("app_type") == mesConstants.APP_TYPE_DISCOVER_IMS || rs.getInt("app_type") == mesConstants.APP_TYPE_DISCOVER_BANK_REFERRAL);
        elmhurstNonDeployApp  = (rs.getInt("app_type") == mesConstants.APP_TYPE_ELM_NON_DEPLOY);

        contactName     = isBlank(rs.getString("merchcont_prim_first_name"))  ? "" : (rs.getString("merchcont_prim_first_name") + " ");
        contactName     += isBlank(rs.getString("merchcont_prim_last_name"))  ? "" : rs.getString("merchcont_prim_last_name");
        contactName     = contactName.trim();
        contactPhone    = isBlank(rs.getString("merchcont_prim_phone"))       ? "" : rs.getString("merchcont_prim_phone");
        conversionType = (!isBlank(rs.getString("account_type")) && rs.getString("account_type").equals("C")) ? "Conversion Account" : "New Account";
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMerchantGeneralInfo(" + primaryKey + ")", e.toString());
      addError("getMerchantGeneralInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getMerchantCardsAccepted(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:390^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code,
//                  merchpo_provider_name
//          from    merchpayoption
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code,\n                merchpo_provider_name\n        from    merchpayoption\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:396^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_VISA:
            visaAccepted = "Y";
          break;

          case mesConstants.APP_CT_AMEX:
            amexAccepted = "Y";
          break;

          case mesConstants.APP_CT_DISCOVER:
            discAccepted = "Y";
          break;

          case mesConstants.APP_CT_DEBIT:
            debitAccepted = "Y";
          break;

          case mesConstants.APP_CT_EBT:
            ebtAccepted = "Y";
          break;

          case mesConstants.APP_CT_CHECK_AUTH:
            CheckAccepted = "Y";
            CheckProvider = MmsManagementBean.decodeCheckProviderName(rs.getString("merchpo_provider_name"));
          break;

          case mesConstants.APP_CT_VALUTEC_GIFT_CARD:
            ValutecAccepted = "Y";
          break;
        }
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMerchantCardsAccepted(" + primaryKey + ")", e.toString());
      addError("getMerchantCardsAccepted: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getPackingSlipInfo(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:460^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  submitted_by,
//                  submitted_date,
//                  shipping_method,
//                  time_zone,
//                  rush_authorization,
//                  merchant_type,
//                  dialpay_type,
//                  ship_equipment_check,
//                  call_tag_check,
//                  rush_order_check,
//                  ship_name,
//                  ship_address_line1,
//                  ship_address_line2,
//                  ship_city,
//                  ship_state,
//                  ship_zip,
//                  ship_phone,
//                  ship_contact_name,
//                  shipping_comment,
//                  equipment_comment,
//                  airbill_num,
//                  visa_accepted,
//                  amex_accepted,
//                  disc_accepted,
//                  debit_accepted,
//                  ebt_accepted,
//                  check_accepted                    as check_accepted,
//                  mpo.merchpo_provider_name         as check_provider,
//                  valutec_accepted                  as valutec_accepted,
//                  call_tag_item,
//                  call_tag_item_serial_num,
//                  special_comment,
//                  access_code_desc,
//                  imprinter_plate_qty,
//                  tip_option_check,
//                  coversheet_printed,
//                  pricingschedule_printed
//          from    packing_slip_info     psi,
//                  merchpayoption        mpo
//          where   psi.app_seq_num = :primaryKey and
//                  mpo.app_seq_num(+) = psi.app_seq_num and
//                  mpo.cardtype_code(+) = :mesConstants.APP_CT_CHECK_AUTH
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  submitted_by,\n                submitted_date,\n                shipping_method,\n                time_zone,\n                rush_authorization,\n                merchant_type,\n                dialpay_type,\n                ship_equipment_check,\n                call_tag_check,\n                rush_order_check,\n                ship_name,\n                ship_address_line1,\n                ship_address_line2,\n                ship_city,\n                ship_state,\n                ship_zip,\n                ship_phone,\n                ship_contact_name,\n                shipping_comment,\n                equipment_comment,\n                airbill_num,\n                visa_accepted,\n                amex_accepted,\n                disc_accepted,\n                debit_accepted,\n                ebt_accepted,\n                check_accepted                    as check_accepted,\n                mpo.merchpo_provider_name         as check_provider,\n                valutec_accepted                  as valutec_accepted,\n                call_tag_item,\n                call_tag_item_serial_num,\n                special_comment,\n                access_code_desc,\n                imprinter_plate_qty,\n                tip_option_check,\n                coversheet_printed,\n                pricingschedule_printed\n        from    packing_slip_info     psi,\n                merchpayoption        mpo\n        where   psi.app_seq_num =  :1  and\n                mpo.app_seq_num(+) = psi.app_seq_num and\n                mpo.cardtype_code(+) =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_CT_CHECK_AUTH);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:504^7*/

      rs = it.getResultSet();

      if( rs.next() )
      {
        this.shippingMethodSingle     = isBlank(rs.getString("shipping_method"))          ? "" : rs.getString("shipping_method");
        this.timeZoneSingle           = isBlank(rs.getString("time_zone"))                ? "" : rs.getString("time_zone");
        this.rushAuthorizationSingle  = isBlank(rs.getString("rush_authorization"))       ? "" : rs.getString("rush_authorization");
        this.merchantType             = isBlank(rs.getString("merchant_type"))            ? "" : rs.getString("merchant_type");
        this.dialPayType              = isBlank(rs.getString("dialpay_type"))             ? "" : rs.getString("dialpay_type");
        this.shipEquipmentCheckBox    = isBlank(rs.getString("ship_equipment_check"))     ? "" : rs.getString("ship_equipment_check");
        this.callTagCheckBox          = isBlank(rs.getString("call_tag_check"))           ? "" : rs.getString("call_tag_check");
        this.rushOrderCheckBox        = isBlank(rs.getString("rush_order_check"))         ? "" : rs.getString("rush_order_check");
        this.shippingName             = isBlank(rs.getString("ship_name"))                ? "" : rs.getString("ship_name");
        this.shippingAddress1         = isBlank(rs.getString("ship_address_line1"))       ? "" : rs.getString("ship_address_line1");
        this.shippingAddress2         = isBlank(rs.getString("ship_address_line2"))       ? "" : rs.getString("ship_address_line2");
        this.shippingCity             = isBlank(rs.getString("ship_city"))                ? "" : rs.getString("ship_city");
        this.shippingState            = isBlank(rs.getString("ship_state"))               ? "" : rs.getString("ship_state");
        this.shippingZip              = isBlank(rs.getString("ship_zip"))                 ? "" : rs.getString("ship_zip");
        this.shippingPhone            = isBlank(rs.getString("ship_phone"))               ? "" : rs.getString("ship_phone");
        this.shippingContactName      = isBlank(rs.getString("ship_contact_name"))        ? "" : rs.getString("ship_contact_name");
        this.shippingComment          = isBlank(rs.getString("shipping_comment"))         ? "" : rs.getString("shipping_comment");
        this.equipmentComment         = isBlank(rs.getString("equipment_comment"))        ? "" : rs.getString("equipment_comment");
        this.airbill                  = isBlank(rs.getString("airbill_num"))              ? "" : rs.getString("airbill_num");
        this.submittedBy              = isBlank(rs.getString("submitted_by"))             ? "" : rs.getString("submitted_by");
        this.submittedDate            = isBlank(rs.getString("submitted_date"))           ? "Not Yet Submitted" : DateTimeFormatter.getFormattedDate(rs.getTimestamp("SUBMITTED_DATE"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
        this.visaAccepted             = isBlank(rs.getString("visa_accepted"))            ? "" : rs.getString("visa_accepted");
        this.amexAccepted             = isBlank(rs.getString("amex_accepted"))            ? "" : rs.getString("amex_accepted");
        this.discAccepted             = isBlank(rs.getString("disc_accepted"))            ? "" : rs.getString("disc_accepted");
        this.debitAccepted            = isBlank(rs.getString("debit_accepted"))           ? "" : rs.getString("debit_accepted");
        this.ebtAccepted              = isBlank(rs.getString("ebt_accepted"))             ? "" : rs.getString("ebt_accepted");
        this.CheckAccepted            = isBlank(rs.getString("check_accepted"))           ? "" : rs.getString("check_accepted");
        this.CheckProvider            = MmsManagementBean.decodeCheckProviderName(rs.getString("check_provider"));
        this.ValutecAccepted          = isBlank(rs.getString("valutec_accepted"))         ? "" : rs.getString("valutec_accepted");
        this.callTagItem              = isBlank(rs.getString("call_tag_item"))            ? "" : rs.getString("call_tag_item");
        this.callTagItemSerialNum     = isBlank(rs.getString("call_tag_item_serial_num")) ? "" : rs.getString("call_tag_item_serial_num");
        this.specialComment           = isBlank(rs.getString("special_comment"))          ? "" : rs.getString("special_comment");
        this.accessCodeDesc           = isBlank(rs.getString("access_code_desc"))         ? "" : rs.getString("access_code_desc");
        this.coverSheetPrinted        = isBlank(rs.getString("coversheet_printed"))       ? "" : DateTimeFormatter.getFormattedDate(rs.getTimestamp("coversheet_printed"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
        this.pricingSchedulePrinted   = isBlank(rs.getString("pricingschedule_printed"))  ? "" : DateTimeFormatter.getFormattedDate(rs.getTimestamp("pricingschedule_printed"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT);
        this.imprinterPlateQty        = isBlank(rs.getString("imprinter_plate_qty"))      ? "" : rs.getString("imprinter_plate_qty");
        this.tipOptionCheck           = isBlank(rs.getString("tip_option_check"))         ? "" : rs.getString("tip_option_check");
        this.packingSlipSubmitted     = true;
      }
      else //if nothing exists in packing slip table, we get defaults from application
      {
        getMerchantCardsAccepted(primaryKey);
        getMiscEquipmentInfo(primaryKey);
        getShippingAddress(primaryKey);
        setApplicationInfo(primaryKey);
      }

      //try this to correct address issues
      if(isBlank(this.shippingAddress1))
      {
        getShippingAddress(primaryKey);
      }

      rs.close();
      it.close();

      getPcCardInfo(primaryKey);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPackingSlipInfo: " + e.toString());
      addError("getPackingSlipInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getPcCardInfo(long primaryKey)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();

      // first look in merch_pos (new apps)
      /*@lineinfo:generated-code*//*@lineinfo:591^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(mp.pp_level_ii, 'N')  pp_level_ii,
//                  nvl(mp.pp_level_iii, 'N') pp_level_iii
//          from    merch_pos mp
//          where   mp.app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(mp.pp_level_ii, 'N')  pp_level_ii,\n                nvl(mp.pp_level_iii, 'N') pp_level_iii\n        from    merch_pos mp\n        where   mp.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:597^7*/

      rs = it.getResultSet();
      if(rs.next())
      {
        if(rs.getString("pp_level_ii").equals("Y"))
        {
          pcCard = "Y";
          pcCardLevelII = "Y";
        }
        if(rs.getString("pp_level_iii").equals("Y"))
        {
          pcCard = "Y";
          pcCardLevelIII = "Y";
        }
      }
      rs.close();
      it.close();

      // now check pos_features
      /*@lineinfo:generated-code*//*@lineinfo:617^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(pf.purchasing_card_flag, 'N') pc_card
//          from    pos_features pf
//          where   pf.app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(pf.purchasing_card_flag, 'N') pc_card\n        from    pos_features pf\n        where   pf.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:622^7*/

      rs = it.getResultSet();
      if(rs.next())
      {
        if(rs.getString("pc_card").equals("Y"))
        {
          pcCard = "Y";
        }
      }
      rs.close();
      it.close();

      // finally check transcom_merchant for transcom apps
      /*@lineinfo:generated-code*//*@lineinfo:636^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(tm.purchasing_card_flag, 'N') pc_card
//          from    transcom_merchant tm
//          where   tm.app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(tm.purchasing_card_flag, 'N') pc_card\n        from    transcom_merchant tm\n        where   tm.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:641^7*/

      rs = it.getResultSet();
      if(rs.next())
      {
        if(rs.getString("pc_card").equals("Y"))
        {
          pcCard = "Y";
        }
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getPcCardInfo(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void setApplicationInfo(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      //decode industry type
      /*@lineinfo:generated-code*//*@lineinfo:674^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  industype_code
//          from    merchant
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  industype_code\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:679^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        if(!isBlank(rs.getString("industype_code")))
        {
          switch(rs.getInt("industype_code"))
          {
            case 2:
              merchantType = "RESTAURANT";
            break;

            case 3:
            case 4:
              merchantType = "HOTEL";
            break;

            case 9:
              merchantType = "CASH ADVANCE";
            break;

            default:
              merchantType = "RETAIL/MOTO";
            break;
          }
        }
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:712^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pos_code
//          from    merch_pos
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pos_code\n        from    merch_pos\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:717^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        if(rs.getInt("pos_code") == 401) //dialpay
        {
          dialPayType = "AUTH W/CAPTURE";
        }
        else
        {
          dialPayType = "AUTH ONLY";
        }
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("setApplicationInfo(" + primaryKey + ")", e.toString());
      addError("setApplicationInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void setDiscoverNumber(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:758^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchpo_card_merch_number
//          from    merchpayoption
//          where   app_seq_num = :primaryKey and
//                  cardtype_code = :mesConstants.APP_CT_DISCOVER
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchpo_card_merch_number\n        from    merchpayoption\n        where   app_seq_num =  :1  and\n                cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_CT_DISCOVER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:764^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        if(rs.getString("merchpo_card_merch_number").equals("0"))
        {
          this.discoverNumber = "Not Yet Assigned";
        }
        else
        {
          this.discoverNumber = formatDiscoverNumber(rs.getString("merchpo_card_merch_number"));
        }
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("setDiscoverNumber(" + primaryKey + ")", e.toString());
      addError("setDiscoverNumber: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private String formatDiscoverNumber(String discnum)
  {
    String result = discnum.trim();

    if(discnum.length() == 15)
    {
      String part1 = discnum.substring(0,5);
      String part2 = discnum.substring(5,9);
      String part3 = discnum.substring(9,12);
      String part4 = discnum.substring(12);
      result = part1 + " - " + part2 + " - " + part3 + " - " + part4;
    }
    return result;
  }

  private void getPhysicalAddress(long primaryKey)
  {
    ResultSetIterator it          = null;
    ResultSet         rs          = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:820^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1,
//                  address_line2,
//                  address_city,
//                  countrystate_code,
//                  address_zip
//          from    address
//          where   app_seq_num = :primaryKey and
//                  addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1,\n                address_line2,\n                address_city,\n                countrystate_code,\n                address_zip\n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:830^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        physicalAddressLine1    = isBlank(rs.getString("address_line1"))      ? "" : rs.getString("address_line1");
        physicalAddressLine2    = isBlank(rs.getString("address_line2"))      ? "" : rs.getString("address_line2");
        physicalAddressCity     = isBlank(rs.getString("address_city"))       ? "" : rs.getString("address_city");
        physicalAddressState    = isBlank(rs.getString("countrystate_code"))  ? "" : rs.getString("countrystate_code");
        physicalAddressZip      = isBlank(rs.getString("address_Zip"))        ? "" : rs.getString("address_zip");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPhysicalAddress: " + e.toString());
      addError("getPhysicalAddress: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getShippingAddress(long appSeqNum)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:868^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_name,
//                  address_phone,
//                  address_line1,
//                  address_line2,
//                  address_city,
//                  countrystate_code,
//                  address_zip
//          from    address
//          where   app_seq_num = :appSeqNum and
//                  addresstype_code = :mesConstants.ADDR_TYPE_SHIPPING
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_name,\n                address_phone,\n                address_line1,\n                address_line2,\n                address_city,\n                countrystate_code,\n                address_zip\n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_SHIPPING);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:880^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.shippingName       = isBlank(rs.getString("address_name"))       ? "" : rs.getString("address_name");
        this.shippingAddress1   = isBlank(rs.getString("address_line1"))      ? "" : rs.getString("address_line1");
        this.shippingAddress2   = isBlank(rs.getString("address_line2"))      ? "" : rs.getString("address_line2");
        this.shippingCity       = isBlank(rs.getString("address_city"))       ? "" : rs.getString("address_city");
        this.shippingState      = isBlank(rs.getString("countrystate_code"))  ? "" : rs.getString("countrystate_code");
        this.shippingZip        = isBlank(rs.getString("address_zip"))        ? "" : rs.getString("address_zip");
        this.shippingPhone      = isBlank(rs.getString("address_phone"))      ? "" : rs.getString("address_phone");
      }

      rs.close();
      it.close();

      if(isBlank(this.shippingAddress1))
      {
        // see if address type was selected in pos_features
        int   addrType = -1;
      
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:905^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(addresstype_code, -1)
//              
//              from    pos_features
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(addresstype_code, -1)\n             \n            from    pos_features\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.ops.NewPackingSlipBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   addrType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:911^11*/
        }
        catch(java.sql.SQLException se)
        {
          addrType = -1;
        }
        
        if(addrType != -1)
        {
          /*@lineinfo:generated-code*//*@lineinfo:920^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_name,
//                      address_phone,
//                      address_line1,
//                      address_line2,
//                      address_city,
//                      countrystate_code,
//                      address_zip
//              from    address
//              where   app_seq_num = :appSeqNum and
//                      addresstype_code = :addrType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_name,\n                    address_phone,\n                    address_line1,\n                    address_line2,\n                    address_city,\n                    countrystate_code,\n                    address_zip\n            from    address\n            where   app_seq_num =  :1  and\n                    addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,addrType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:932^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:936^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_name,
//                      address_phone,
//                      address_line1,
//                      address_line2,
//                      address_city,
//                      countrystate_code,
//                      address_zip
//              from    address
//              where   app_seq_num = :appSeqNum and
//                      addresstype_code = :mesConstants.ADDR_TYPE_MAILING
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_name,\n                    address_phone,\n                    address_line1,\n                    address_line2,\n                    address_city,\n                    countrystate_code,\n                    address_zip\n            from    address\n            where   app_seq_num =  :1  and\n                    addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_MAILING);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:948^11*/
        }

        rs = it.getResultSet();

        if(rs.next())
        {
          this.shippingName       = isBlank(rs.getString("address_name"))       ? "" : rs.getString("address_name");
          this.shippingAddress1   = isBlank(rs.getString("address_line1"))      ? "" : rs.getString("address_line1");
          this.shippingAddress2   = isBlank(rs.getString("address_line2"))      ? "" : rs.getString("address_line2");
          this.shippingCity       = isBlank(rs.getString("address_city"))       ? "" : rs.getString("address_city");
          this.shippingState      = isBlank(rs.getString("countrystate_code"))  ? "" : rs.getString("countrystate_code");
          this.shippingZip        = isBlank(rs.getString("address_zip"))        ? "" : rs.getString("address_zip");
          this.shippingPhone      = isBlank(rs.getString("address_phone"))      ? "" : rs.getString("address_phone");
        }

        rs.close();
        it.close();
      }

      if(isBlank(this.shippingAddress1))
      {
        /*@lineinfo:generated-code*//*@lineinfo:970^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_name,
//                    address_phone,
//                    address_line1,
//                    address_line2,
//                    address_city,
//                    countrystate_code,
//                    address_zip
//            from    address
//            where   app_seq_num = :appSeqNum and
//                    addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_name,\n                  address_phone,\n                  address_line1,\n                  address_line2,\n                  address_city,\n                  countrystate_code,\n                  address_zip\n          from    address\n          where   app_seq_num =  :1  and\n                  addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:982^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          this.shippingName       = isBlank(rs.getString("address_name"))       ? "" : rs.getString("address_name");
          this.shippingAddress1   = isBlank(rs.getString("address_line1"))      ? "" : rs.getString("address_line1");
          this.shippingAddress2   = isBlank(rs.getString("address_line2"))      ? "" : rs.getString("address_line2");
          this.shippingCity       = isBlank(rs.getString("address_city"))       ? "" : rs.getString("address_city");
          this.shippingState      = isBlank(rs.getString("countrystate_code"))  ? "" : rs.getString("countrystate_code");
          this.shippingZip        = isBlank(rs.getString("address_zip"))        ? "" : rs.getString("address_zip");
          this.shippingPhone      = isBlank(rs.getString("address_phone"))      ? "" : rs.getString("address_phone");
        }

        rs.close();
        it.close();
      }

      if(isBlank(this.shippingName))
      {
        this.shippingName = this.dbaName;
      }

      /*@lineinfo:generated-code*//*@lineinfo:1006^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  shipping_contact_name,
//                  shipping_method,
//                  shipping_comment
//          from    transcom_merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  shipping_contact_name,\n                shipping_method,\n                shipping_comment\n        from    transcom_merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1013^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.shippingContactName  = isBlank(rs.getString("shipping_contact_name"))  ? "" : rs.getString("shipping_contact_name");
        this.shippingComment      = isBlank(rs.getString("shipping_comment"))       ? "" : rs.getString("shipping_comment");
        this.shippingMethodSingle = isBlank(rs.getString("shipping_method"))        ? "" : rs.getString("shipping_method");
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:1027^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pf.shipping_contact_name  shipping_contact_name,
//                  pf.shipping_method        shipping_method,
//                  pf.shipping_comment       shipping_comment,
//                  psm.message||' '||pf.equipment_comment      equipment_comment
//          from    pos_features  pf,
//                  application app,
//                  packing_slip_message psm
//          where   pf.app_seq_num = :appSeqNum and
//                  pf.app_seq_num = app.app_seq_num and
//                  app.app_type = psm.app_type(+) and
//                  lower(nvl(psm.enabled, 'y')) = 'y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pf.shipping_contact_name  shipping_contact_name,\n                pf.shipping_method        shipping_method,\n                pf.shipping_comment       shipping_comment,\n                psm.message||' '||pf.equipment_comment      equipment_comment\n        from    pos_features  pf,\n                application app,\n                packing_slip_message psm\n        where   pf.app_seq_num =  :1  and\n                pf.app_seq_num = app.app_seq_num and\n                app.app_type = psm.app_type(+) and\n                lower(nvl(psm.enabled, 'y')) = 'y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1040^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.shippingContactName  = isBlank(rs.getString("shipping_contact_name"))  ? "" : rs.getString("shipping_contact_name");
        this.shippingComment      = isBlank(rs.getString("shipping_comment"))       ? "" : rs.getString("shipping_comment");
        this.shippingMethodSingle = isBlank(rs.getString("shipping_method"))        ? "" : rs.getString("shipping_method");
        this.equipmentComment     = isBlank(rs.getString("equipment_comment"))      ? "" : rs.getString("equipment_comment");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getShippingAddress(" + appSeqNum + ")", e.toString());
      addError("getShippingAddress: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getMiscEquipmentInfo(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      //check to see if access code is being used or if tip option is turned on
      /*@lineinfo:generated-code*//*@lineinfo:1078^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  access_code_flag,
//                  access_code,
//                  tip_option_flag
//          from    pos_features
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  access_code_flag,\n                access_code,\n                tip_option_flag\n        from    pos_features\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1085^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.accessCodeDesc = isBlank(rs.getString("access_code"))          ? "" : rs.getString("access_code");
        this.tipOptionCheck = isBlank(rs.getString("tip_option_flag"))      ? "" : rs.getString("tip_option_flag");
      }

      rs.close();
      it.close();

      //get quantity of imprinter plates
      /*@lineinfo:generated-code*//*@lineinfo:1099^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchequip_equip_quantity
//          from    merchequipment
//          where   app_seq_num = :primaryKey and
//                  equiptype_code = 9
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchequip_equip_quantity\n        from    merchequipment\n        where   app_seq_num =  :1  and\n                equiptype_code = 9";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1105^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.imprinterPlateQty = isBlank(rs.getString("merchequip_equip_quantity")) ? "" : rs.getString("merchequip_equip_quantity");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMiscEquipmentInfo(" + primaryKey + ")", e.toString());
      addError("getMiscEquipmentInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void loadShippingMethods( long appSeqNum )
  {
    ResultSetIterator   it              = null;
    ResultSet           rs              = null;
    String              shippingMethod  = null;

    try
    {
      connect();

      // clear the existing items
      method.removeAllElements();
      methodDesc.removeAllElements();

      try
      {
        // get the shipping method from the current packing slip
        /*@lineinfo:generated-code*//*@lineinfo:1147^9*/

//  ************************************************************
//  #sql [Ctx] { select  psi.shipping_method 
//            from    packing_slip_info     psi
//            where   psi.app_seq_num = :appSeqNum
//  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  psi.shipping_method  \n          from    packing_slip_info     psi\n          where   psi.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.ops.NewPackingSlipBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   shippingMethod = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1154^9*/
      }
      catch( Exception ee )
      {
        // ignore, shipping method not set yet
      }

      // new airborne shipping methods
      /*@lineinfo:generated-code*//*@lineinfo:1162^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  code,
//                  (description || ': ' || long_description)    as code_desc
//          from    shipping_methods
//          where   trunc(nvl( ( select  app.app_created_date
//                               from    application app
//                               where   app.app_seq_num(+) = :appSeqNum ),
//                             sysdate
//                           )
//                       ) between from_date and to_date or
//                  code = nvl(:shippingMethod,'-1')
//          order by display_order, from_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  code,\n                (description || ': ' || long_description)    as code_desc\n        from    shipping_methods\n        where   trunc(nvl( ( select  app.app_created_date\n                             from    application app\n                             where   app.app_seq_num(+) =  :1  ),\n                           sysdate\n                         )\n                     ) between from_date and to_date or\n                code = nvl( :2 ,'-1')\n        order by display_order, from_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,shippingMethod);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1175^7*/
      rs = it.getResultSet();

      while( rs.next() )
      {
        method.add(rs.getString("code"));
        methodDesc.add(rs.getString("code_desc"));
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadShippingMethods()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void moveToCompletedStage(long appSeqNum, UserBean user)
  {
    try
    {
      connect();

      //moves request out of programming new queue..
      //into programming completed queue
      /*@lineinfo:generated-code*//*@lineinfo:1204^7*/

//  ************************************************************
//  #sql [Ctx] { update temp_activation_queue
//          set   q_stage      = :QueueConstants.Q_PROGRAMMING_COMPLETE
//          where app_seq_num  = :appSeqNum
//          and   q_type       = :QueueConstants.QUEUE_PROGRAMMING
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update temp_activation_queue\n        set   q_stage      =  :1 \n        where app_seq_num  =  :2 \n        and   q_type       =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.Q_PROGRAMMING_COMPLETE);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,QueueConstants.QUEUE_PROGRAMMING);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1210^7*/

      for(int v=0; v<vNums.size(); v++)
      {
        long reqid = getRequestIdFromVnum((String)vNums.elementAt(v));
        QueueTools.moveQueueItem(reqid, QueueTools.getCurrentQueueType(reqid, MesQueues.Q_ITEM_TYPE_PROGRAMMING), MesQueues.Q_PROGRAMMING_COMPLETE, user, "Packing Slip Created");
      }

      moveOutOfMmsQueue(appSeqNum, user);

      if(!elmhurstNonDeployApp)
      {
        addToDepQueues(appSeqNum, user);
      }

      if(!elmhurstNonDeployApp)
      {
        putInEquipmentQueue(user.getLoginName(), appSeqNum);
      }
    }
    catch(Exception e)
    {
      logEntry("moveToCompletedStage(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void moveOutOfEquipManagementQueue()
  {
    try
    {
      String userName = "SYSTEM";
      if(!isBlank(submittedBy))
      {
        userName = submittedBy;
      }

      //send to equipment management completed queue
      EquipmentManagementBean  emb = new EquipmentManagementBean();
      emb.moveToNewStage(merchNum, QueueConstants.Q_EQUIPMENT_COMPLETE, userName, false);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("moveOutOfEquipManagementQueue", e.toString());
    }
  }

  private long getRequestIdFromVnum(String vnumber)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    long                result  = -1;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1270^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  request_id
//          from    mms_stage_info
//          where   vnum = :vnumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  request_id\n        from    mms_stage_info\n        where   vnum =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,vnumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1275^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        result = rs.getLong("request_id");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getRequestIdFromVnum(" + vnumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return result;

  }

  private void moveOutOfMmsQueue(long appSeqNum, UserBean user)
  {
    try
    {
      connect();

      //if all requests have come back with success status.. then we move item out of mms queue..
      //this is clean up of previous step
      /*@lineinfo:generated-code*//*@lineinfo:1310^7*/

//  ************************************************************
//  #sql [Ctx] { update app_queue
//          set    app_queue_stage = :QueueConstants.Q_MMS_COMPLETED
//          where  app_seq_num     = :appSeqNum
//          and    app_queue_type  = :QueueConstants.QUEUE_MMS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update app_queue\n        set    app_queue_stage =  :1 \n        where  app_seq_num     =  :2 \n        and    app_queue_type  =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.Q_MMS_COMPLETED);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,QueueConstants.QUEUE_MMS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1316^7*/
      
      // add to q_activation_process to make sure it gets routed to the activaiton queue if necessary
      /*@lineinfo:generated-code*//*@lineinfo:1319^7*/

//  ************************************************************
//  #sql [Ctx] { insert into q_activation_process
//          (
//            app_seq_num
//          )
//          values
//          (
//            :appSeqNum
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_activation_process\n        (\n          app_seq_num\n        )\n        values\n        (\n           :1 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1329^7*/

      //moves from which ever type of mms queue its currently in..
      //to the completed mms queue
      QueueTools.moveQueueItem(appSeqNum, QueueTools.getCurrentQueueType(appSeqNum, MesQueues.Q_ITEM_TYPE_MMS), MesQueues.Q_MMS_COMPLETE, user, null);

      //then we time stamp mms completed on account status page
      /*@lineinfo:generated-code*//*@lineinfo:1336^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     date_completed = sysdate,
//                  contact_name       = :submittedBy,
//                  status_code        = 302
//          where   app_seq_num = :appSeqNum
//                  and dept_code      = :QueueConstants.DEPARTMENT_MMS
//                  and date_completed is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     date_completed = sysdate,\n                contact_name       =  :1 ,\n                status_code        = 302\n        where   app_seq_num =  :2 \n                and dept_code      =  :3 \n                and date_completed is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,submittedBy);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_MMS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1345^7*/
    }
    catch(Exception e)
    {
      logEntry("moveOutOfMmsQueue(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void addToDepQueues(long appSeqNum, UserBean user)
  {
    try
    {
      connect();

      //add to deployment. set rush if its a rush..
      boolean isRush = rushOrderCheckBox.equals("Y");

      if(!QueueTools.hasItemEnteredQueue(appSeqNum, MesQueues.Q_ITEM_TYPE_DEPLOYMENT))
      {
        QueueTools.insertQueue(appSeqNum, MesQueues.Q_DEPLOYMENT_NEW, user, isRush);
      }
      else
      {
        if(isRush)
        {
          QueueTools.setIsRush(appSeqNum, MesQueues.Q_DEPLOYMENT_NEW);
        }
      }

      if(!alreadExists(appSeqNum, QueueConstants.QUEUE_DEPLOYMENT))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1380^9*/

//  ************************************************************
//  #sql [Ctx] { insert into temp_activation_queue
//            (
//              q_type,
//              q_stage,
//              request_id,
//              date_in_queue,
//              app_seq_num
//            )
//            values
//            (
//              :QueueConstants.QUEUE_DEPLOYMENT,
//              :QueueConstants.Q_DEPLOYMENT_NEW,
//              -1,
//              sysdate,
//              :appSeqNum
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into temp_activation_queue\n          (\n            q_type,\n            q_stage,\n            request_id,\n            date_in_queue,\n            app_seq_num\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n            -1,\n            sysdate,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.QUEUE_DEPLOYMENT);
   __sJT_st.setInt(2,QueueConstants.Q_DEPLOYMENT_NEW);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1398^9*/
      }

      //then we time stamp activation and deployment as received on account status page
      /*@lineinfo:generated-code*//*@lineinfo:1402^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     date_received  = sysdate,
//                  status_code = 501
//          where   app_seq_num  = :appSeqNum
//                  and dept_code =  :QueueConstants.DEPARTMENT_DEPLOYMENT
//                  and date_received is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     date_received  = sysdate,\n                status_code = 501\n        where   app_seq_num  =  :1 \n                and dept_code =   :2 \n                and date_received is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_DEPLOYMENT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1410^7*/
    }
    catch(Exception e)
    {
      logEntry("addToDepQueues(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void addToActQueues(long appSeqNum, UserBean user)
  {
    try
    {
      connect();

      //add to activation
      if(!QueueTools.hasItemEnteredQueue(appSeqNum, MesQueues.Q_ITEM_TYPE_ACTIVATION))
      {
        QueueTools.insertQueue(appSeqNum, MesQueues.Q_ACTIVATION_NEW, user);
      }

      if(!alreadExists(appSeqNum, QueueConstants.QUEUE_ACTIVATION))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1436^9*/

//  ************************************************************
//  #sql [Ctx] { insert into temp_activation_queue
//            (
//              q_type,
//              q_stage,
//              request_id,
//              date_in_queue,
//              app_seq_num
//            )
//            values
//            (
//              :QueueConstants.QUEUE_ACTIVATION,
//              :QueueConstants.Q_ACTIVATION_NEW,
//              -1,
//              sysdate,
//              :appSeqNum
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into temp_activation_queue\n          (\n            q_type,\n            q_stage,\n            request_id,\n            date_in_queue,\n            app_seq_num\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n            -1,\n            sysdate,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.QUEUE_ACTIVATION);
   __sJT_st.setInt(2,QueueConstants.Q_ACTIVATION_NEW);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1454^9*/
      }

      //then we time stamp activation and deployment as received on account status page
      /*@lineinfo:generated-code*//*@lineinfo:1458^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     date_received     = sysdate,
//                  status_code       = 601
//          where   app_seq_num       = :appSeqNum
//                  and dept_code     = :QueueConstants.DEPARTMENT_ACTIVATION
//                  and date_received is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     date_received     = sysdate,\n                status_code       = 601\n        where   app_seq_num       =  :1 \n                and dept_code     =  :2 \n                and date_received is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"31com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_ACTIVATION);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1466^7*/
    }
    catch(Exception e)
    {
      logEntry("addToActQueues(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void putInEquipmentQueue(String loginName, long primaryKey)
  {
    ResultSetIterator it              = null;
    ResultSet         rs              = null;
    Date              startDate       = null;
    String            appSource       = "";
    long              appUser         = 0;
    boolean           alreadyInQueue  = false;

    try
    {
      connect();

      //check to see if it has been added to equipment queue already
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1493^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    app_queue
//          where   app_seq_num  = :primaryKey
//                  and app_queue_type = :QueueConstants.QUEUE_EQUIPMENT
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    app_queue\n        where   app_seq_num  =  :1 \n                and app_queue_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.ops.NewPackingSlipBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,QueueConstants.QUEUE_EQUIPMENT);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1500^7*/

      //dont try to add to database if it already exists
      alreadyInQueue = (recCount > 0);

      //if not already in queue then we add
      if(!alreadyInQueue)
      {
        //add to equipment queue
        /*@lineinfo:generated-code*//*@lineinfo:1509^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_start_date,
//                    app_user,
//                    app_source
//            from    app_queue
//            where   app_seq_num = :primaryKey
//            order by app_start_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_start_date,\n                  app_user,\n                  app_source\n          from    app_queue\n          where   app_seq_num =  :1 \n          order by app_start_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"33com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1517^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          appSource = rs.getString("app_source");
          appUser   = rs.getLong  ("app_user");
          startDate = rs.getDate  ("app_start_date");
        }

        rs.close();
        it.close();

        /*@lineinfo:generated-code*//*@lineinfo:1531^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_queue
//            (
//              app_seq_num,
//              app_queue_type,
//              app_queue_stage,
//              app_status,
//              app_user,
//              app_start_date,
//              app_source,
//              app_last_date,
//              app_last_user,
//              is_rush
//            )
//            values
//            (
//              :primaryKey,
//              :QueueConstants.QUEUE_EQUIPMENT,
//              :QueueConstants.Q_EQUIPMENT_NEW,
//              :QueueConstants.Q_STATUS_APPROVED,
//              :appUser,
//              sysdate,
//              :appSource,
//              sysdate,
//              'SYSTEM',
//              :rushOrderCheckBox.equals("Y") ? 1 : 0
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_797 = rushOrderCheckBox.equals("Y") ? 1 : 0;
   String theSqlTS = "insert into app_queue\n          (\n            app_seq_num,\n            app_queue_type,\n            app_queue_stage,\n            app_status,\n            app_user,\n            app_start_date,\n            app_source,\n            app_last_date,\n            app_last_user,\n            is_rush\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n            sysdate,\n             :6 ,\n            sysdate,\n            'SYSTEM',\n             :7 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"34com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,QueueConstants.QUEUE_EQUIPMENT);
   __sJT_st.setInt(3,QueueConstants.Q_EQUIPMENT_NEW);
   __sJT_st.setInt(4,QueueConstants.Q_STATUS_APPROVED);
   __sJT_st.setLong(5,appUser);
   __sJT_st.setString(6,appSource);
   __sJT_st.setInt(7,__sJT_797);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1559^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1563^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_queue
//            set     is_rush           = :rushOrderCheckBox.equals("Y") ? 1 : 0
//            where   app_seq_num       = :primaryKey and
//                    app_queue_type    = :QueueConstants.QUEUE_EQUIPMENT
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_798 = rushOrderCheckBox.equals("Y") ? 1 : 0;
   String theSqlTS = "update  app_queue\n          set     is_rush           =  :1 \n          where   app_seq_num       =  :2  and\n                  app_queue_type    =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"35com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_798);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,QueueConstants.QUEUE_EQUIPMENT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1569^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("putInEquipmentQueue(" + primaryKey + ")", e.toString());
      addError("putInEquipmentQueue: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public long getPrimaryKeyFromReqId(long reqId)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    long                result  = 0L;;

    try
    {
      connect();

      //get primaryKey from request_id
      /*@lineinfo:generated-code*//*@lineinfo:1596^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num
//          from    mms_stage_info
//          where   request_id = :reqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num\n        from    mms_stage_info\n        where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"36com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1601^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        result = rs.getLong("app_seq_num");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getPrimaryKeyFromReqId(" + reqId + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return result;
  }

  private boolean addToActivation(long appSeqNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {
      connect();

      //check transcom merchant first
      /*@lineinfo:generated-code*//*@lineinfo:1638^7*/

//  ************************************************************
//  #sql [Ctx] it = { select phone_training
//          from   transcom_merchant
//          where  app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select phone_training\n        from   transcom_merchant\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"37com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1643^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        if(!isBlank(rs.getString("phone_training")) && rs.getString("phone_training").equals("M"))
        {
          result = true;
        }
      }

      rs.close();
      it.close();

      //check pos_features table
      /*@lineinfo:generated-code*//*@lineinfo:1659^7*/

//  ************************************************************
//  #sql [Ctx] it = { select phone_training
//          from   pos_features
//          where  app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select phone_training\n        from   pos_features\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"38com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1664^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        if(!isBlank(rs.getString("phone_training")) && rs.getString("phone_training").equals("M"))
        {
          result = true;
        }
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("addToActivation(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return result;
  }

  private boolean alreadExists(long appSeqNum, int queue)
  {
    boolean             result  = false;

    try
    {
      connect();

      //check to see if all requests came back successfully
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1703^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    temp_activation_queue
//          where   app_seq_num = :appSeqNum and
//                  q_type = :queue
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    temp_activation_queue\n        where   app_seq_num =  :1  and\n                q_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.ops.NewPackingSlipBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,queue);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1710^7*/

      result = (recCount > 0);
    }
    catch(Exception e)
    {
      logEntry("alreadExists(" + appSeqNum + ", " + queue + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  public boolean appApproved(long appSeqNum)
  {
    boolean             result  = false;

    try
    {
      connect();

      //check to see if merch_credit_status == approved
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1736^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant
//          where   app_seq_num = :appSeqNum and
//                  merch_credit_status = :QueueConstants.CREDIT_APPROVE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant\n        where   app_seq_num =  :1  and\n                merch_credit_status =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"40com.mes.ops.NewPackingSlipBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,QueueConstants.CREDIT_APPROVE);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1743^7*/

      result = (recCount > 0);
    }
    catch(Exception e)
    {
      logEntry("appApproved(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  private void getMultiMerchantInfo(long primaryKey)
  {
    ResultSetIterator it          = null;
    ResultSet         rs          = null;
    String            masterCntrl = "";

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1769^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  msi.merch_number,
//                  msi.business_name,
//                  msi.mcc,
//                  msi.vnum
//          from    mms_stage_info msi,
//                  merchant mr1,
//                  merchant mr2
//          where   mr1.app_seq_num = :primaryKey and
//                  mr1.multi_merch_master_cntrl is not null and
//                  mr1.multi_merch_master_cntrl = mr2.multi_merch_master_cntrl and
//                  mr2.app_seq_num != mr1.app_seq_num and
//                  mr2.app_seq_num = msi.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  msi.merch_number,\n                msi.business_name,\n                msi.mcc,\n                msi.vnum\n        from    mms_stage_info msi,\n                merchant mr1,\n                merchant mr2\n        where   mr1.app_seq_num =  :1  and\n                mr1.multi_merch_master_cntrl is not null and\n                mr1.multi_merch_master_cntrl = mr2.multi_merch_master_cntrl and\n                mr2.app_seq_num != mr1.app_seq_num and\n                mr2.app_seq_num = msi.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"41com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"41com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1783^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        MultiMerchant mm = new MultiMerchant();

        mm.setMerchantId(   isBlank(rs.getString("merch_number"))   ? "" : rs.getString("merch_number"));
        mm.setMerchantDba(  isBlank(rs.getString("business_name"))  ? "" : rs.getString("business_name"));
        mm.setMerchantMcc(  isBlank(rs.getString("mcc"))            ? "" : rs.getString("mcc"));
        mm.setMerchantVnum( isBlank(rs.getString("vnum"))           ? "" : rs.getString("vnum"));

        multiMerchants.add(mm);
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMultiMerchantInfo(" + primaryKey + ")", e.toString());
      addError("getMultiMerchantInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void getStaticData(long primaryKey)
  {
    getMerchantGeneralInfo  (primaryKey);
    getPhysicalAddress      (primaryKey);
    setDiscoverNumber       (primaryKey);
    getEquipmentInfo        (primaryKey);
    getMultiMerchantInfo    (primaryKey);
  }

  //only get these when page is not submitted
  //not only that.. but we check to make sure info doesnt
  //exist in another table.. the main table for this page..
  public void getDynamicData(long primaryKey)
  {
    getPackingSlipInfo(primaryKey);
  }

  public void submitData(long primaryKey, UserBean user)
  {
    submitPackingSlipInfo(primaryKey, user);
    moveToCompletedStage(primaryKey, user);
  }

  private void submitPackingSlipInfo(long primaryKey, UserBean user)
  {
    int               recCount  = 0;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1846^7*/

//  ************************************************************
//  #sql [Ctx] { select count(app_seq_num) 
//          from PACKING_SLIP_INFO
//          where app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(app_seq_num)  \n        from PACKING_SLIP_INFO\n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.ops.NewPackingSlipBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1851^7*/

      if ( recCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1855^9*/

//  ************************************************************
//  #sql [Ctx] { update  packing_slip_info
//            set     shipping_method           = :this.shippingMethodSingle    ,
//                    time_zone                 = :this.timeZoneSingle          ,
//                    rush_authorization        = :this.rushAuthorizationSingle ,
//                    merchant_type             = :this.merchantType            ,
//                    dialpay_type              = :this.dialPayType             ,
//                    ship_equipment_check      = :this.shipEquipmentCheckBox   ,
//                    call_tag_check            = :this.callTagCheckBox         ,
//                    rush_order_check          = :this.rushOrderCheckBox       ,
//                    ship_name                 = :this.shippingName            ,
//                    ship_address_line1        = :this.shippingAddress1        ,
//                    ship_address_line2        = :this.shippingAddress2        ,
//                    ship_city                 = :this.shippingCity            ,
//                    ship_state                = :this.shippingState           ,
//                    ship_zip                  = :this.shippingZip             ,
//                    ship_phone                = :this.shippingPhone           ,
//                    ship_contact_name         = :this.shippingContactName     ,
//                    shipping_comment          = :this.shippingComment         ,
//                    equipment_comment         = :this.equipmentComment        ,
//                    airbill_num               = :this.airbill                 ,
//                    visa_accepted             = :this.visaAccepted            ,
//                    amex_accepted             = :this.amexAccepted            ,
//                    disc_accepted             = :this.discAccepted            ,
//                    debit_accepted            = :this.debitAccepted           ,
//                    ebt_accepted              = :this.ebtAccepted             ,
//                    check_accepted            = :this.CheckAccepted           ,
//                    valutec_accepted          = :this.ValutecAccepted         ,
//                    call_tag_item             = :this.callTagItem             ,
//                    call_tag_item_serial_num  = :this.callTagItemSerialNum    ,
//                    special_comment           = :this.specialComment          ,
//                    access_code_desc          = :this.accessCodeDesc          ,
//                    imprinter_plate_qty       = :this.imprinterPlateQty       ,
//                    tip_option_check          = :this.tipOptionCheck          ,
//                    submitted_by              = :this.submittedBy             ,
//                    submitted_date            = sysdate
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  packing_slip_info\n          set     shipping_method           =  :1     ,\n                  time_zone                 =  :2           ,\n                  rush_authorization        =  :3  ,\n                  merchant_type             =  :4             ,\n                  dialpay_type              =  :5              ,\n                  ship_equipment_check      =  :6    ,\n                  call_tag_check            =  :7          ,\n                  rush_order_check          =  :8        ,\n                  ship_name                 =  :9             ,\n                  ship_address_line1        =  :10         ,\n                  ship_address_line2        =  :11         ,\n                  ship_city                 =  :12             ,\n                  ship_state                =  :13            ,\n                  ship_zip                  =  :14              ,\n                  ship_phone                =  :15            ,\n                  ship_contact_name         =  :16      ,\n                  shipping_comment          =  :17          ,\n                  equipment_comment         =  :18         ,\n                  airbill_num               =  :19                  ,\n                  visa_accepted             =  :20             ,\n                  amex_accepted             =  :21             ,\n                  disc_accepted             =  :22             ,\n                  debit_accepted            =  :23            ,\n                  ebt_accepted              =  :24              ,\n                  check_accepted            =  :25            ,\n                  valutec_accepted          =  :26          ,\n                  call_tag_item             =  :27              ,\n                  call_tag_item_serial_num  =  :28     ,\n                  special_comment           =  :29           ,\n                  access_code_desc          =  :30           ,\n                  imprinter_plate_qty       =  :31        ,\n                  tip_option_check          =  :32           ,\n                  submitted_by              =  :33              ,\n                  submitted_date            = sysdate\n          where   app_seq_num =  :34";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"43com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.shippingMethodSingle);
   __sJT_st.setString(2,this.timeZoneSingle);
   __sJT_st.setString(3,this.rushAuthorizationSingle);
   __sJT_st.setString(4,this.merchantType);
   __sJT_st.setString(5,this.dialPayType);
   __sJT_st.setString(6,this.shipEquipmentCheckBox);
   __sJT_st.setString(7,this.callTagCheckBox);
   __sJT_st.setString(8,this.rushOrderCheckBox);
   __sJT_st.setString(9,this.shippingName);
   __sJT_st.setString(10,this.shippingAddress1);
   __sJT_st.setString(11,this.shippingAddress2);
   __sJT_st.setString(12,this.shippingCity);
   __sJT_st.setString(13,this.shippingState);
   __sJT_st.setString(14,this.shippingZip);
   __sJT_st.setString(15,this.shippingPhone);
   __sJT_st.setString(16,this.shippingContactName);
   __sJT_st.setString(17,this.shippingComment);
   __sJT_st.setString(18,this.equipmentComment);
   __sJT_st.setString(19,this.airbill);
   __sJT_st.setString(20,this.visaAccepted);
   __sJT_st.setString(21,this.amexAccepted);
   __sJT_st.setString(22,this.discAccepted);
   __sJT_st.setString(23,this.debitAccepted);
   __sJT_st.setString(24,this.ebtAccepted);
   __sJT_st.setString(25,this.CheckAccepted);
   __sJT_st.setString(26,this.ValutecAccepted);
   __sJT_st.setString(27,this.callTagItem);
   __sJT_st.setString(28,this.callTagItemSerialNum);
   __sJT_st.setString(29,this.specialComment);
   __sJT_st.setString(30,this.accessCodeDesc);
   __sJT_st.setString(31,this.imprinterPlateQty);
   __sJT_st.setString(32,this.tipOptionCheck);
   __sJT_st.setString(33,this.submittedBy);
   __sJT_st.setLong(34,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1893^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1897^9*/

//  ************************************************************
//  #sql [Ctx] { insert into packing_slip_info
//            (
//              shipping_method,
//              time_zone,
//              rush_authorization,
//              merchant_type,
//              dialpay_type,
//              ship_equipment_check,
//              call_tag_check,
//              rush_order_check,
//              ship_name,
//              ship_address_line1,
//              ship_address_line2,
//              ship_city,
//              ship_state,
//              ship_zip,
//              ship_phone,
//              ship_contact_name,
//              shipping_comment,
//              equipment_comment,
//              airbill_num,
//              visa_accepted,
//              amex_accepted,
//              disc_accepted,
//              debit_accepted,
//              ebt_accepted,
//              check_accepted,
//              valutec_accepted,
//              call_tag_item,
//              call_tag_item_serial_num,
//              special_comment,
//              access_code_desc,
//              imprinter_plate_qty,
//              tip_option_check,
//              submitted_by,
//              submitted_date,
//              app_seq_num
//            )
//            values
//            (
//              :this.shippingMethodSingle,
//              :this.timeZoneSingle,
//              :this.rushAuthorizationSingle,
//              :this.merchantType,
//              :this.dialPayType,
//              :this.shipEquipmentCheckBox,
//              :this.callTagCheckBox,
//              :this.rushOrderCheckBox,
//              :this.shippingName,
//              :this.shippingAddress1,
//              :this.shippingAddress2,
//              :this.shippingCity,
//              :this.shippingState,
//              :this.shippingZip,
//              :this.shippingPhone,
//              :this.shippingContactName,
//              :this.shippingComment,
//              :this.equipmentComment,
//              :this.airbill,
//              :this.visaAccepted,
//              :this.amexAccepted,
//              :this.discAccepted,
//              :this.debitAccepted,
//              :this.ebtAccepted,
//              :this.CheckAccepted,
//              :this.ValutecAccepted,
//              :this.callTagItem,
//              :this.callTagItemSerialNum,
//              :this.specialComment,
//              :this.accessCodeDesc,
//              :this.imprinterPlateQty,
//              :this.tipOptionCheck,
//              :this.submittedBy,
//              sysdate,
//              :primaryKey
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into packing_slip_info\n          (\n            shipping_method,\n            time_zone,\n            rush_authorization,\n            merchant_type,\n            dialpay_type,\n            ship_equipment_check,\n            call_tag_check,\n            rush_order_check,\n            ship_name,\n            ship_address_line1,\n            ship_address_line2,\n            ship_city,\n            ship_state,\n            ship_zip,\n            ship_phone,\n            ship_contact_name,\n            shipping_comment,\n            equipment_comment,\n            airbill_num,\n            visa_accepted,\n            amex_accepted,\n            disc_accepted,\n            debit_accepted,\n            ebt_accepted,\n            check_accepted,\n            valutec_accepted,\n            call_tag_item,\n            call_tag_item_serial_num,\n            special_comment,\n            access_code_desc,\n            imprinter_plate_qty,\n            tip_option_check,\n            submitted_by,\n            submitted_date,\n            app_seq_num\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 ,\n            sysdate,\n             :34 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"44com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.shippingMethodSingle);
   __sJT_st.setString(2,this.timeZoneSingle);
   __sJT_st.setString(3,this.rushAuthorizationSingle);
   __sJT_st.setString(4,this.merchantType);
   __sJT_st.setString(5,this.dialPayType);
   __sJT_st.setString(6,this.shipEquipmentCheckBox);
   __sJT_st.setString(7,this.callTagCheckBox);
   __sJT_st.setString(8,this.rushOrderCheckBox);
   __sJT_st.setString(9,this.shippingName);
   __sJT_st.setString(10,this.shippingAddress1);
   __sJT_st.setString(11,this.shippingAddress2);
   __sJT_st.setString(12,this.shippingCity);
   __sJT_st.setString(13,this.shippingState);
   __sJT_st.setString(14,this.shippingZip);
   __sJT_st.setString(15,this.shippingPhone);
   __sJT_st.setString(16,this.shippingContactName);
   __sJT_st.setString(17,this.shippingComment);
   __sJT_st.setString(18,this.equipmentComment);
   __sJT_st.setString(19,this.airbill);
   __sJT_st.setString(20,this.visaAccepted);
   __sJT_st.setString(21,this.amexAccepted);
   __sJT_st.setString(22,this.discAccepted);
   __sJT_st.setString(23,this.debitAccepted);
   __sJT_st.setString(24,this.ebtAccepted);
   __sJT_st.setString(25,this.CheckAccepted);
   __sJT_st.setString(26,this.ValutecAccepted);
   __sJT_st.setString(27,this.callTagItem);
   __sJT_st.setString(28,this.callTagItemSerialNum);
   __sJT_st.setString(29,this.specialComment);
   __sJT_st.setString(30,this.accessCodeDesc);
   __sJT_st.setString(31,this.imprinterPlateQty);
   __sJT_st.setString(32,this.tipOptionCheck);
   __sJT_st.setString(33,this.submittedBy);
   __sJT_st.setLong(34,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1975^9*/
      }

      commit();
    }
    catch(Exception e)
    {
      logEntry("submitPackingSlipInfo(" + primaryKey + ")", e.toString());
      addError("submitPackingSlipInfo: " + e.toString());
    }
    finally
    {
      getTimeSubmitted(primaryKey);
      addAirbillToCallTracking(user);
      cleanUp();
    }
  }

  private void addAirbillToCallTracking(UserBean user)
  {
    if(! isBlank(this.airbill) && ! isBlank(this.merchNum))
    {
      String            note    = "Airbill # " + this.airbill;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:2003^9*/

//  ************************************************************
//  #sql [Ctx] { insert into service_calls
//            (
//              merchant_number,
//              call_date,
//              type,
//              other_description,
//              login_name,
//              status,
//              notes,
//              billable,
//              client_generated
//            )
//            values
//            (
//              :merchNum,
//              sysdate,
//              99,
//              'Deployment',
//              :user.getLoginName(),
//              2,
//              :note,
//              'N',
//              'N'
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_799 = user.getLoginName();
   String theSqlTS = "insert into service_calls\n          (\n            merchant_number,\n            call_date,\n            type,\n            other_description,\n            login_name,\n            status,\n            notes,\n            billable,\n            client_generated\n          )\n          values\n          (\n             :1 ,\n            sysdate,\n            99,\n            'Deployment',\n             :2 ,\n            2,\n             :3 ,\n            'N',\n            'N'\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"45com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setString(2,__sJT_799);
   __sJT_st.setString(3,note);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2029^9*/
      }
      catch(Exception e)
      {
        logEntry("addAirBillToCallTracking()", e.toString());
        addError("addAirbillToCallTracking: " + e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }

  private void getTimeSubmitted(long primaryKey)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2049^7*/

//  ************************************************************
//  #sql [Ctx] { select  decode(submitted_date,
//                    null, 'Not Yet Submitted',
//                    to_char(submitted_date, 'mm/dd/yy hh:mi PM'))
//          
//          from    packing_slip_info
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(submitted_date,\n                  null, 'Not Yet Submitted',\n                  to_char(submitted_date, 'mm/dd/yy hh:mi PM'))\n         \n        from    packing_slip_info\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"46com.mes.ops.NewPackingSlipBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   submittedDate = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2057^7*/
    }
    catch(Exception e)
    {
      logEntry("getTimeSubmitted(" + primaryKey + ")", e.toString());
      addError("getTimeSubmitted: " + e.toString());
      submittedDate = "Not Yet Submitted";
    }
    finally
    {
      cleanUp();
    }
  }

  public boolean validate(long primaryKey)
  {
    if(!appApproved(primaryKey))
    {
      addError("This application has been declined, please do not continue further with it");
    }

    if(rushOrderCheckBox.equals("Y") && isBlank(rushAuthorizationSingle))
    {
      addError("If rush order is checked, you must supply an authorizing manager");
    }
    else if((rushOrderCheckBox.equals("N") || rushOrderCheckBox.equals("")) && !isBlank(rushAuthorizationSingle))
    {
      addError("Check rush order if supplying an authorizing manager, else deselect authorizing manager");
    }

    if(isBlank(shipEquipmentCheckBox))
    {
      addError("Please indicate whether equipment is to be shipped");
    }

    if(shippingComment.length() > SHIPPING_COMMENT_LENGTH)
    {
      addError("Shipping comments must be limited to 150 characters.  Currently it is " + shippingComment.length() + " characters.");
    }

    if(equipmentComment.length() > EQUIPMENT_COMMENT_LENGTH)
    {
      addError("Equipment comments must be limited to 250 characters.  Currently it is " + equipmentComment.length() + " characters.");
    }

    if(isBlank(shippingPhone))
    {
      //dont do anything.. aint no biggie.. not required
    }
    else if (countDigits(shippingPhone) != 10)
    {
      addError("Shipping Phone number must be a valid 10-digit number including area code");
    }
    else //we get digits only, we reformat when we display it
    {
      shippingPhone = getDigitsOnly(shippingPhone);
    }

    return(! hasErrors());
  }

  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }

  public String formatPhone(String fone)
  {
    String result = "";
    try
    {
      if(fone.length() == 10)
      {
        result = "(" + fone.substring(0,3) + ")" + " " + fone.substring(3,6) + "-" + fone.substring(6);
      }
      else
      {
        result = fone;
      }
    }
    catch(Exception e)
    {
      result = fone;
    }

    return result;
  }

  public String formatDate(Date raw)
  {
    String result   = "";
    if(raw == null)
    {
      return result;
    }
    try
    {
      DateFormat df   = new SimpleDateFormat("MM/dd/yy");
      result = df.format(raw);
      return result;
    }
    catch(Exception e)
    {
      System.out.println("date crashed = " + e.toString());
    }
    return result;
  }

  public String getDigitsOnly(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");
    String        result      = "";

    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }

      result = digits.toString();
    }
    catch(Exception e)
    {}

    return result;
  }


  public int countDigits(String raw)
  {
    int result = 0;

    for(int i=0; i<raw.length(); ++i)
    {
      if(Character.isDigit(raw.charAt(i)))
      {
        ++result;
      }
    }

    return result;
  }


  public String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;

      index = body.indexOf(' ');

      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
      System.out.println("error will robinson %20");
    }
    return body;

  }

  public String getMcc()
  {
    return this.mcc;
  }

  public String getShipEquipmentCheckBox()
  {
    return this.shipEquipmentCheckBox;
  }
  public String getCallTagCheckBox()
  {
    return this.callTagCheckBox;
  }

  public String getRushOrderCheckBox()
  {
    return this.rushOrderCheckBox;
  }

  public String getPhysicalAddressLine1()
  {
    return this.physicalAddressLine1;
  }
  public String getPhysicalAddressLine2()
  {
    return this.physicalAddressLine2;
  }
  public String getPhysicalAddressCity()
  {
    return this.physicalAddressCity;
  }
  public String getPhysicalAddressState()
  {
    return this.physicalAddressState;
  }
  public String getPhysicalAddressZip()
  {
    return this.physicalAddressZip;
  }

  public String getShippingName()
  {
    return this.shippingName;
  }
  public String getShippingAddress1()
  {
    return this.shippingAddress1;
  }
  public String getShippingAddress2()
  {
    return this.shippingAddress2;
  }
  public String getShippingCity()
  {
    return this.shippingCity;
  }
  public String getShippingState()
  {
    return this.shippingState;
  }
  public String getShippingZip()
  {
    return this.shippingZip;
  }
  public String getShippingPhone()
  {
    return formatPhone(this.shippingPhone);
  }
  public String getShippingContactName()
  {
    return this.shippingContactName;
  }
  public String getShippingComment()
  {
    return this.shippingComment;
  }
  public String getEquipmentComment()
  {
    return this.equipmentComment;
  }
  public String getAirbill()
  {
    return this.airbill;
  }
  public String getVisaAccepted()
  {
    return this.visaAccepted;
  }
  public String getAmexAccepted()
  {
    return this.amexAccepted;
  }
  public String getDiscAccepted()
  {
    return this.discAccepted;
  }
  public String getDebitAccepted()
  {
    return this.debitAccepted;
  }
  public String getEbtAccepted()
  {
    return this.ebtAccepted;
  }
  public String getCheckAccepted()
  {
    return(this.CheckAccepted);
  }
  public String getCheckProviderName()
  {
    return(this.CheckProvider);
  }
  public String getValutecAccepted()
  {
    return(this.ValutecAccepted);
  }

  public String getPcCard()
  {
    return pcCard;
  }

  public String getPcCardLevelII()
  {
    return pcCardLevelII;
  }

  public String getPcCardLevelIII()
  {
    return pcCardLevelIII;
  }

  public String getCallTagItem()
  {
    return this.callTagItem;
  }
  public String getCallTagItemSerialNum()
  {
    return this.callTagItemSerialNum;
  }
  public String getSpecialComment()
  {
    return this.specialComment;
  }
  public String getAccessCodeDesc()
  {
    return this.accessCodeDesc;
  }
  public String getDbaName()
  {
    return dbaName;
  }

  public String getDbaNameLine1()
  {
    String result = this.dbaName;

    if(this.dbaName.length() > 20)
    {
      result = this.dbaName.substring(0,20);
    }

    return result;
  }

  public String getDbaNameLine2()
  {
    String result = "";

    if(this.dbaName.length() > 20)
    {
      result = this.dbaName.substring(20);
    }

    return result;
  }


  public String getMerchNum()
  {
    return merchNum;
  }
  public String getControlNum()
  {
    return controlNum;
  }
  public String getAppTypeDesc()
  {
    return appTypeDesc;
  }
  public String getAppCreatedDate()
  {
    return appCreatedDate;
  }

  public String getContactName()
  {
    return contactName;
  }

  public String getContactPhone()
  {
    return formatPhone(contactPhone);
  }

  public String getShippingMethodSingle()
  {
    return this.shippingMethodSingle;
  }
  public String getTimeZoneSingle()
  {
    return this.timeZoneSingle;
  }
  public String getRushAuthorizationSingle()
  {
    return this.rushAuthorizationSingle;
  }

  public String getSubmittedBy()
  {
    return this.submittedBy;
  }
  public String getSubmittedDate()
  {
    return this.submittedDate;
  }
  public String getMerchantType()
  {
    return this.merchantType;
  }
  public String getDialPayType()
  {
    return this.dialPayType;
  }

  public String getImprinterPlateQty()
  {
    return this.imprinterPlateQty;
  }
  public String getTipOptionCheck()
  {
    return this.tipOptionCheck;
  }

  public String  getCoverSheetPrinted()
  {
    return this.coverSheetPrinted;
  }
  public String  getPricingSchedulePrinted()
  {
    return this.pricingSchedulePrinted;
  }
  public boolean isPackingSlipSubmitted()
  {
    return this.packingSlipSubmitted;
  }
  public boolean isDiscoverApp()
  {
    return this.discoverApp;
  }

  public String getDiscoverNumber()
  {
    return this.discoverNumber;
  }

  public String getConversionType()
  {
    return this.conversionType;
  }


//***************************settters
  public void setSubmittedBy(String submittedBy)
  {
    this.submittedBy = submittedBy;
  }

  public void setShipEquipmentCheckBox(String shipEquipmentCheckBox)
  {
    this.shipEquipmentCheckBox = shipEquipmentCheckBox;
  }

  public void setCallTagCheckBox(String callTagCheckBox)
  {
    this.callTagCheckBox = callTagCheckBox;
  }

  public void setRushOrderCheckBox(String rushOrderCheckBox)
  {
    this.rushOrderCheckBox = rushOrderCheckBox;
  }

  public void setShippingName(String shippingName)
  {
    this.shippingName = shippingName;
  }
  public void setShippingAddress1(String shippingAddress1)
  {
    this.shippingAddress1 = shippingAddress1;
  }
  public void setShippingAddress2(String shippingAddress2)
  {
    this.shippingAddress2 = shippingAddress2;
  }
  public void setShippingCity(String shippingCity)
  {
    this.shippingCity = shippingCity;
  }
  public void setShippingState(String shippingState)
  {
    this.shippingState = shippingState;
  }
  public void setShippingZip(String shippingZip)
  {
    this.shippingZip = shippingZip;
  }
  public void setShippingPhone(String shippingPhone)
  {
    this.shippingPhone = shippingPhone;
  }
  public void setShippingContactName(String shippingContactName)
  {
    this.shippingContactName = shippingContactName;
  }

  public void setShippingComment(String shippingComment)
  {
    this.shippingComment = shippingComment;
  }

  public void setEquipmentComment(String equipmentComment)
  {
    this.equipmentComment = equipmentComment;
  }

  public void setAirbill(String airbill)
  {
    this.airbill = airbill;
  }

  public void setVisaAccepted(String visaAccepted)
  {
    this.visaAccepted = visaAccepted;
  }
  public void setAmexAccepted(String amexAccepted)
  {
    this.amexAccepted = amexAccepted;
  }
  public void setDiscAccepted(String discAccepted)
  {
    this.discAccepted = discAccepted;
  }
  public void setDebitAccepted(String debitAccepted)
  {
    this.debitAccepted = debitAccepted;
  }
  public void setEbtAccepted(String ebtAccepted)
  {
    this.ebtAccepted = ebtAccepted;
  }
  public void setCheckAccepted(String accepted)
  {
    this.CheckAccepted = accepted;
  }
  public void setValutecAccepted(String accepted)
  {
    this.ValutecAccepted = accepted;
  }
  public void setCallTagItem(String callTagItem)
  {
    this.callTagItem = callTagItem;
  }
  public void setCallTagItemSerialNum(String callTagItemSerialNum)
  {
    this.callTagItemSerialNum = callTagItemSerialNum;
  }
  public void setSpecialComment(String specialComment)
  {
    this.specialComment = specialComment;
  }

  public void setAccessCodeDesc(String accessCodeDesc)
  {
    this.accessCodeDesc = accessCodeDesc;
  }

  public void setShippingMethodSingle(String shippingMethodSingle)
  {
    this.shippingMethodSingle = shippingMethodSingle;
  }
  public void setTimeZoneSingle(String timeZoneSingle)
  {
    this.timeZoneSingle = timeZoneSingle;
  }
  public void setRushAuthorizationSingle(String rushAuthorizationSingle)
  {
    this.rushAuthorizationSingle = rushAuthorizationSingle;
  }

  public void setMerchantType(String merchantType)
  {
    this.merchantType = merchantType;
  }
  public void setDialPayType(String dialPayType)
  {
    this.dialPayType = dialPayType;
  }

  public void setImprinterPlateQty(String imprinterPlateQty)
  {
    this.imprinterPlateQty = imprinterPlateQty;
  }
  public void setTipOptionCheck(String tipOptionCheck)
  {
    this.tipOptionCheck = tipOptionCheck;
  }

//****************************end of setters

  private void getEquipmentInfo(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      //equipment from application
      /*@lineinfo:generated-code*//*@lineinfo:2670^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  etq.part_number   qrg_part_number,
//                  me.*,
//                  equip.equip_descriptor,
//                  elt.equiplendtype_description
//          from    merchequipment  me,
//                  equipment       equip,
//                  equiplendtype   elt,
//                  equip_termapp_qrg etq,
//                  (
//                    select  equip_model,
//                            term_application_profgen,
//                            front_end
//                    from    mms_stage_info
//                    where   app_seq_num = :primaryKey
//                  ) msi,
//                  merchant mr
//          where   me.app_seq_num        = :primaryKey and
//                  me.equiptype_code     != 9 and
//                  me.equip_model        = equip.equip_model and
//                  me.equiplendtype_code = elt.equiplendtype_code and
//                  me.equip_model = msi.equip_model(+) and
//                  me.app_seq_num = mr.app_seq_num and
//                  msi.term_application_profgen = etq.terminal_application(+) and
//                  (
//                    etq.termapp_type is null or
//                    (
//                      etq.termapp_type = 'any' and
//                      (etq.model_code = 'na' or etq.model_code = me.equip_model)
//                    ) or
//                    (
//                      etq.termapp_type = nvl(mr.terminal_application, 'Retail') and
//                      (etq.model_code = 'na' or etq.model_code = me.equip_model)
//                    )
//                  ) and
//                  msi.front_end = etq.front_end(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  etq.part_number   qrg_part_number,\n                me.*,\n                equip.equip_descriptor,\n                elt.equiplendtype_description\n        from    merchequipment  me,\n                equipment       equip,\n                equiplendtype   elt,\n                equip_termapp_qrg etq,\n                (\n                  select  equip_model,\n                          term_application_profgen,\n                          front_end\n                  from    mms_stage_info\n                  where   app_seq_num =  :1 \n                ) msi,\n                merchant mr\n        where   me.app_seq_num        =  :2  and\n                me.equiptype_code     != 9 and\n                me.equip_model        = equip.equip_model and\n                me.equiplendtype_code = elt.equiplendtype_code and\n                me.equip_model = msi.equip_model(+) and\n                me.app_seq_num = mr.app_seq_num and\n                msi.term_application_profgen = etq.terminal_application(+) and\n                (\n                  etq.termapp_type is null or\n                  (\n                    etq.termapp_type = 'any' and\n                    (etq.model_code = 'na' or etq.model_code = me.equip_model)\n                  ) or\n                  (\n                    etq.termapp_type = nvl(mr.terminal_application, 'Retail') and\n                    (etq.model_code = 'na' or etq.model_code = me.equip_model)\n                  )\n                ) and\n                msi.front_end = etq.front_end(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"47com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"47com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2707^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        for(int x=0; x<rs.getInt("MERCHEQUIP_EQUIP_QUANTITY"); x++)
        {
          TerminalInfo ti = new TerminalInfo();

          ti.setEquipmentModel(     isBlank(rs.getString("equip_model"))                ? "" : rs.getString("equip_model")              );
          ti.setEquipmentDesc(      isBlank(rs.getString("equip_descriptor"))           ? "" : rs.getString("equip_descriptor")         );
          ti.setEquipmentLendType(  isBlank(rs.getString("equiplendtype_description"))  ? "" : rs.getString("equiplendtype_description"));
          ti.setEquipmentLendTypeCode(rs.getInt("equiplendtype_code"));

          if(rs.getInt("equiplendtype_code") == mesConstants.APP_EQUIP_OWNED)
          {
            ti.setEquipmentStatus( "OWNED" );
          }
          else
          {
            ti.setEquipmentStatus( "NEEDED" );
          }

          //*****************Need to find a way to specify refurbished equipment***********************
          if(rs.getInt("prod_option_id") == 23) //23 = refurbished product option
          {
            ti.setEquipmentCondition("Refurbished");
          }

          ti.setQrgPartNumber(blankIfNull(rs.getString("qrg_part_number")));

          terminals.add(ti);
        }
      }

      rs.close();
      it.close();

      //equipment deployed
      /*@lineinfo:generated-code*//*@lineinfo:2747^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ei.*,
//                  equip.equip_descriptor,
//                  elt.equiplendtype_description,
//                  ec.ec_class_name
//          from    equip_inventory ei,
//                  equipment       equip,
//                  equip_class     ec,
//                  equiplendtype   elt,
//                  merchant        mr
//          where   mr.app_seq_num = :primaryKey and
//                  mr.merch_number = ei.ei_merchant_number and
//                  ei.ei_part_number = equip.equip_model and
//                  ei.ei_lrb         = elt.equiplendtype_code and
//                  ei.ei_class       = ec.ec_class_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ei.*,\n                equip.equip_descriptor,\n                elt.equiplendtype_description,\n                ec.ec_class_name\n        from    equip_inventory ei,\n                equipment       equip,\n                equip_class     ec,\n                equiplendtype   elt,\n                merchant        mr\n        where   mr.app_seq_num =  :1  and\n                mr.merch_number = ei.ei_merchant_number and\n                ei.ei_part_number = equip.equip_model and\n                ei.ei_lrb         = elt.equiplendtype_code and\n                ei.ei_class       = ec.ec_class_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"48com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"48com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2763^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        TerminalInfo ti = getTerminalInfo(rs.getString("ei_part_number"), rs.getInt("ei_lrb"));

        if(ti != null)
        {
          ti.setEquipmentSerialNum( isBlank(rs.getString("ei_serial_number"))           ? "" : rs.getString("ei_serial_number")         );
          ti.setEquipmentCondition( isBlank(rs.getString("ec_class_name"))              ? "" : rs.getString("ec_class_name")            );
          ti.setEquipmentStatus( "DEPLOYED" );
        }
        else
        {
          ti = new TerminalInfo();

          ti.setEquipmentModel(     isBlank(rs.getString("ei_part_number"))             ? "" : rs.getString("ei_part_number")           );
          ti.setEquipmentSerialNum( isBlank(rs.getString("ei_serial_number"))           ? "" : rs.getString("ei_serial_number")         );
          ti.setEquipmentDesc(      isBlank(rs.getString("equip_descriptor"))           ? "" : rs.getString("equip_descriptor")         );
          ti.setEquipmentCondition( isBlank(rs.getString("ec_class_name"))              ? "" : rs.getString("ec_class_name")            );
          ti.setEquipmentLendType(  isBlank(rs.getString("equiplendtype_description"))  ? "" : rs.getString("equiplendtype_description"));
          ti.setEquipmentLendTypeCode(rs.getInt("ei_lrb"));
          ti.setEquipmentStatus( "DEPLOYED" );

          terminals.add(ti);
        }
      }

      rs.close();
      it.close();

      getMMSInfo(primaryKey);
    }
    catch(Exception e)
    {
      logEntry("getEquipmentInfo(" + primaryKey + ")", e.toString());
      addError("getEquipmentInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void getMMSInfo(long primaryKey)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2820^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vn.v_number,
//                  vn.app_code,
//                  vn.equip_model,
//                  equip.equip_descriptor,
//                  msi.request_id,
//                  msi.equip_refurb,
//                  etq.part_number qrg_part_number,
//                  nvl(efe.description, 'Unknown') host
//          from    v_numbers      vn,
//                  equipment      equip,
//                  mms_stage_info msi,
//                  equip_termapp_qrg etq,
//                  merchant mr,
//                  equip_front_ends efe
//          where   vn.app_seq_num = :primaryKey and
//                  vn.app_seq_num   = msi.app_seq_num and
//                  vn.v_number      = msi.vnum and
//                  vn.v_number is not null and
//                  vn.equip_model = equip.equip_model(+) and
//                  vn.app_seq_num = mr.app_seq_num(+) and
//                  vn.app_code = etq.terminal_application(+) and
//                  nvl(etq.front_end, 0) = msi.front_end and
//                  (
//                    etq.termapp_type is null or
//                    (
//                      etq.termapp_type = 'any' and
//                      (etq.model_code = 'na' or etq.model_code = vn.equip_model)
//                    ) or
//                    (
//                      mr.terminal_application is null and
//                      (etq.termapp_type = 'any' or etq.termapp_type = 'Retail') and
//                      (etq.model_code = 'na' or etq.model_code = vn.equip_model)
//                    ) or
//                    (
//                      etq.termapp_type = mr.terminal_application and
//                      (etq.model_code = 'na' or etq.model_code = vn.equip_model)
//                    )
//                  ) and
//                  msi.front_end = efe.front_end(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vn.v_number,\n                vn.app_code,\n                vn.equip_model,\n                equip.equip_descriptor,\n                msi.request_id,\n                msi.equip_refurb,\n                etq.part_number qrg_part_number,\n                nvl(efe.description, 'Unknown') host\n        from    v_numbers      vn,\n                equipment      equip,\n                mms_stage_info msi,\n                equip_termapp_qrg etq,\n                merchant mr,\n                equip_front_ends efe\n        where   vn.app_seq_num =  :1  and\n                vn.app_seq_num   = msi.app_seq_num and\n                vn.v_number      = msi.vnum and\n                vn.v_number is not null and\n                vn.equip_model = equip.equip_model(+) and\n                vn.app_seq_num = mr.app_seq_num(+) and\n                vn.app_code = etq.terminal_application(+) and\n                nvl(etq.front_end, 0) = msi.front_end and\n                (\n                  etq.termapp_type is null or\n                  (\n                    etq.termapp_type = 'any' and\n                    (etq.model_code = 'na' or etq.model_code = vn.equip_model)\n                  ) or\n                  (\n                    mr.terminal_application is null and\n                    (etq.termapp_type = 'any' or etq.termapp_type = 'Retail') and\n                    (etq.model_code = 'na' or etq.model_code = vn.equip_model)\n                  ) or\n                  (\n                    etq.termapp_type = mr.terminal_application and\n                    (etq.model_code = 'na' or etq.model_code = vn.equip_model)\n                  )\n                ) and\n                msi.front_end = efe.front_end(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"49com.mes.ops.NewPackingSlipBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"49com.mes.ops.NewPackingSlipBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2861^7*/

      rs = it.getResultSet();

      int h = 0;
      while(rs.next())
      {
        h++;

        TerminalInfo ti = getTerminalInfo(rs.getString("equip_model"));

        if(ti != null)
        {
          ti.setVnumber         ( isBlank(rs.getString("v_number"))     ? "" : rs.getString("v_number")   );
          ti.setTermApplication ( isBlank(rs.getString("app_code"))     ? "" : rs.getString("app_code")   );
          ti.setRequestId       ( isBlank(rs.getString("request_id"))   ? "" : rs.getString("request_id") );

          if(ti.getEquipmentStatus().equals("NEEDED") && rs.getString("equip_refurb") != null && rs.getString("equip_refurb").equals("Y"))
          {
            ti.setEquipmentCondition("Refurbished");
          }
          
          ti.setHost( rs.getString("host") );
        }
        else
        {
          ti = new TerminalInfo();

          ti.setVnumber         ( isBlank(rs.getString("v_number"))     ? "" : rs.getString("v_number")   );
          ti.setTermApplication ( isBlank(rs.getString("app_code"))     ? "" : rs.getString("app_code")   );
          ti.setRequestId       ( isBlank(rs.getString("request_id"))   ? "" : rs.getString("request_id") );

          //this situation is for stage only builds... so there is no equipment associated
          ti.setEquipmentModel("NOEQUIP" + h);
          ti.setEquipmentSerialNum("N/A");

          if(isBlank(rs.getString("equip_descriptor")))
          {
            ti.setEquipmentDesc("N/A");
          }
          else
          {
            ti.setEquipmentDesc(rs.getString("equip_descriptor"));
          }

          ti.setEquipmentCondition("N/A");
          ti.setEquipmentLendType("N/A");
          ti.setEquipmentStatus("N/A");
          ti.setHost( rs.getString("host") );

          terminals.add(ti);
        }
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMMSInfo(" + primaryKey + ")", e.toString());
      addError("getMMSInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private TerminalInfo getTerminalInfo(String model)
  {
    for(int i=0; i<terminals.size(); i++)
    {
      if(getEquipmentModel(i).equals(model) && getVnumber(i).equals("N/A"))
      {
        return (TerminalInfo)terminals.elementAt(i);
      }
    }
    return null;
  }

  private TerminalInfo getTerminalInfo(String model, int lendType)
  {
    for(int i=0; i<terminals.size(); i++)
    {
      if(getEquipmentModel(i).equals(model) && getEquipmentLendTypeCode(i) == lendType && getEquipmentSerialNum(i).equals("N/A"))
      {
        return (TerminalInfo)terminals.elementAt(i);
      }
    }
    return null;
  }


  public int numTerminals()
  {
    return terminals.size();
  }

  //**************TERMINAL ONLY****************
  public String getRequestId(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getRequestId());
  }
  public String getVnumber(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getVnumber());
  }
  public String getTermApplication(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getTermApplication());
  }
  public String getHost(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getHost());
  }
  //********************************************

  public String getEquipmentModel(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getEquipmentModel());
  }

  public String getEquipmentDesc(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getEquipmentDesc());
  }

  public String getEquipmentCondition(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getEquipmentCondition());
  }

  public String getEquipmentLendType(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getEquipmentLendType());
  }

  public int getEquipmentLendTypeCode(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getEquipmentLendTypeCode());
  }

  public String getEquipmentStatus(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getEquipmentStatus());
  }

  public String getQrgPartNumber(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return (term.getQrgPartNumber());
  }

  //************ALL EQUIPMENT EXCEPT MERCHANT OWNED
  public String getEquipmentSerialNum(int idx)
  {
    TerminalInfo term = (TerminalInfo)terminals.elementAt(idx);
    return(term.getEquipmentSerialNum());
  }


  public String getMerchantId(int idx)
  {
    MultiMerchant mm = (MultiMerchant)multiMerchants.elementAt(idx);
    return (mm.getMerchantId());
  }

  public String getMerchantDba(int idx)
  {
    MultiMerchant mm = (MultiMerchant)multiMerchants.elementAt(idx);
    return (mm.getMerchantDba());
  }

  public String getMerchantMcc(int idx)
  {
    MultiMerchant mm = (MultiMerchant)multiMerchants.elementAt(idx);
    return (mm.getMerchantMcc());
  }

  public String getMerchantVnum(int idx)
  {
    MultiMerchant mm = (MultiMerchant)multiMerchants.elementAt(idx);
    return (mm.getMerchantVnum());
  }

  public int getNumMultiMerchants()
  {
    return this.multiMerchants.size();
  }
  public boolean hasMultiMerchants()
  {
    return (this.multiMerchants.size() > 0);
  }

  public class MultiMerchant
  {
    private String merchantId       = "";
    private String merchantDba      = "";
    private String merchantMcc      = "";
    private String merchantVnum     = "";

    public void setMerchantId(String merchantId)
    {
      this.merchantId = merchantId;
    }

    public void setMerchantDba(String merchantDba)
    {
      this.merchantDba = merchantDba;
    }

    public void setMerchantMcc(String merchantMcc)
    {
      this.merchantMcc = merchantMcc;
    }

    public void setMerchantVnum(String merchantVnum)
    {
      this.merchantVnum = merchantVnum;
    }

    public String getMerchantId()
    {
      return this.merchantId;
    }

    public String getMerchantDba()
    {
      return this.merchantDba;
    }

    public String getMerchantMcc()
    {
      return this.merchantMcc;
    }

    public String getMerchantVnum()
    {
      return this.merchantVnum;
    }
  }

  public class TerminalInfo
  {

    private String requestId              = "";
    private String vnumber                = "N/A";
    private String termApplication        = "N/A";
    private String host                   = "Unknown";

    private String equipmentModel         = "";
    private String equipmentSerialNum     = "N/A";
    private String equipmentDesc          = "";
    private String equipmentCondition     = "N/A";
    private String equipmentLendType      = "";
    private int    equipmentLendTypeCode  = -1;
    private String equipmentStatus        = "";
    private String qrgPartNumber          = "";

    TerminalInfo()
    {}

    public void setRequestId(String requestId)
    {
      this.requestId = requestId;
    }
    public void setVnumber(String vnumber)
    {
      this.vnumber = vnumber;
    }
    public void setTermApplication(String termApplication)
    {
      this.termApplication = termApplication;
    }
    public void setHost(String host)
    {
      this.host = host;
      
      if(this.host == null)
      {
        this.host = "";
      }
    }
    public void setEquipmentModel(String equipmentModel)
    {
      this.equipmentModel = equipmentModel;
    }
    public void setEquipmentSerialNum(String equipmentSerialNum)
    {
      this.equipmentSerialNum = equipmentSerialNum;
    }
    public void setEquipmentDesc(String equipmentDesc)
    {
      this.equipmentDesc = equipmentDesc;
    }
    public void setEquipmentCondition(String equipmentCondition)
    {
      this.equipmentCondition = equipmentCondition;
    }
    public void setEquipmentLendType(String equipmentLendType)
    {
      this.equipmentLendType = equipmentLendType;
    }
    public void setEquipmentLendTypeCode(int equipmentLendTypeCode)
    {
      this.equipmentLendTypeCode = equipmentLendTypeCode;
    }
    public void setEquipmentStatus(String equipmentStatus)
    {
      this.equipmentStatus = equipmentStatus;
    }
    public void setQrgPartNumber(String qrgPartNumber)
    {
      this.qrgPartNumber = qrgPartNumber;
    }


    public String getRequestId()
    {
      return this.requestId;
    }
    public String getVnumber()
    {
      return this.vnumber;
    }
    public String getTermApplication()
    {
      return this.termApplication;
    }
    public String getHost()
    {
      return( this.host );
    }
    public String getEquipmentModel()
    {
      return this.equipmentModel;
    }
    public String getEquipmentSerialNum()
    {
      return this.equipmentSerialNum;
    }
    public String getEquipmentDesc()
    {
      return this.equipmentDesc;
    }
    public String getEquipmentCondition()
    {
      return this.equipmentCondition;
    }
    public String getEquipmentLendType()
    {
      return this.equipmentLendType;
    }
    public int getEquipmentLendTypeCode()
    {
      return this.equipmentLendTypeCode;
    }
    public String getEquipmentStatus()
    {
      return this.equipmentStatus;
    }
    public String getQrgPartNumber()
    {
      return this.qrgPartNumber;
    }
  }
}/*@lineinfo:generated-code*/