/*@lineinfo:filename=OtherCardBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/ops/OtherCardBean.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-02-21 14:46:45 -0800 (Wed, 21 Feb 2007) $
  Version            : $Revision: 13474 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.equipment.profile.EquipProfileGeneratorDictator;
import com.mes.queues.QueueTools;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class OtherCardBean extends ExpandDataBean
{
  private String merchDiners      = "";
  private String merchAmex        = "";
  private String amexEsa          = "n/a";
  private String merchJCB         = "";
  private String merchDiscover    = "";
  private String discoverRap      = "n/a";
  private String merchEBT         = "";
  private String merchPCIS        = "";
  private String merchPCIF        = "";
  private String merchPCI         = "";

  private String errorDesc        = "";
  private String userLogin        = "";
  private UserBean user           = null;
  private boolean merchDinersReq      = false;
  private boolean merchAmexReq        = false;
  private boolean merchJCBReq         = false;
  private boolean merchDiscoverReq    = false;
  private boolean merchEBTReq         = false;

  private boolean moveToCompletedOk   = true;
  private int     nextStage           = QueueConstants.Q_SETUP_QA;

  public  Vector  merchPcisNum  = new Vector();
  public  Vector  merchPcisDesc = new Vector();


  public OtherCardBean()
  {
    merchPcisNum.add(""); 
    merchPcisNum.add("010450");
    merchPcisNum.add("011100");
    merchPcisNum.add("011101");
    merchPcisNum.add("011102");
    merchPcisDesc.add("Select"); 
    merchPcisDesc.add("010450 Retail");
    merchPcisDesc.add("011100 Restaurant");
    merchPcisDesc.add("011101 Lodging");
    merchPcisDesc.add("011102 Retail/Lodging");
  }

  public void getData(long primaryKey)
  {
    getOtherCardInfo(primaryKey);
  }

  public void checkForErrors(long primaryKey, int qStage)
  {
    try 
    {
      QuestionableDataBean qdb = new QuestionableDataBean();
      
      if(qdb.hasErrors(primaryKey, mesConstants.ERROR_ALL) && qStage == QueueConstants.Q_SETUP_QA)
      {
        addError("Cannot leave QA Stage until all Possible Errors have been cleared.  " + qdb.getErrorPageLink(primaryKey, mesConstants.ERROR_ALL, ("/jsp/credit/questionabledata.jsp?primaryKey=" + primaryKey), "Click Here") + " to view and clear errors.");
      }

    }
    catch(Exception e)
    {
      logEntry("checkForErrors(" + primaryKey + ")", e.toString());
      addError("checkForErrors: " + e.toString());
    }
  }

  private void getOtherCardInfo(long primaryKey)  
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;

    try 
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:119^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    merchpayoption
//          where   app_seq_num = :primaryKey and
//                  cardtype_code not in 
//                  (
//                    :mesConstants.APP_CT_VISA,
//                    :mesConstants.APP_CT_VISA_CHECK_CARD,
//                    :mesConstants.APP_CT_VISA_BUSINESS_CARD,
//                    :mesConstants.APP_CT_MC,
//                    :mesConstants.APP_CT_MC_CHECK_CARD,
//                    :mesConstants.APP_CT_MC_BUSINESS_CARD
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    merchpayoption\n        where   app_seq_num =  :1  and\n                cardtype_code not in \n                (\n                   :2 ,\n                   :3 ,\n                   :4 ,\n                   :5 ,\n                   :6 ,\n                   :7 \n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.OtherCardBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setInt(3,mesConstants.APP_CT_VISA_CHECK_CARD);
   __sJT_st.setInt(4,mesConstants.APP_CT_VISA_BUSINESS_CARD);
   __sJT_st.setInt(5,mesConstants.APP_CT_MC);
   __sJT_st.setInt(6,mesConstants.APP_CT_MC_CHECK_CARD);
   __sJT_st.setInt(7,mesConstants.APP_CT_MC_BUSINESS_CARD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.OtherCardBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:133^7*/

      rs = it.getResultSet();
      
      while(rs.next())
      {
        switch(rs.getInt("cardtype_code")) 
        {
          case mesConstants.APP_CT_DEBIT:
            merchEBT      = rs.getString("merchpo_card_merch_number");
            break;
          case mesConstants.APP_CT_DINERS_CLUB:
            merchDiners   = rs.getString("merchpo_card_merch_number");
            break;
          case mesConstants.APP_CT_DISCOVER: 
            merchDiscover = rs.getString("merchpo_card_merch_number");
            discoverRap   = isBlank(rs.getString("merchpo_rate")) ? "n/a" : rs.getString("merchpo_rate");
            break;
          case mesConstants.APP_CT_JCB:
            merchJCB      = rs.getString("merchpo_card_merch_number");
            break;
          case mesConstants.APP_CT_AMEX:
            merchAmex     = rs.getString("merchpo_card_merch_number");
            amexEsa       = isBlank(rs.getString("merchpo_rate")) ? "n/a" : rs.getString("merchpo_rate");
            break;
          default:
            break;
        }
      }
      
      rs.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:166^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    billcard
//          where   app_seq_num = :primaryKey and
//                  cardtype_code not in 
//                  (
//                    :mesConstants.APP_CT_VISA,
//                    :mesConstants.APP_CT_VISA_CHECK_CARD,
//                    :mesConstants.APP_CT_VISA_BUSINESS_CARD,
//                    :mesConstants.APP_CT_MC,
//                    :mesConstants.APP_CT_MC_CHECK_CARD,
//                    :mesConstants.APP_CT_MC_BUSINESS_CARD
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    billcard\n        where   app_seq_num =  :1  and\n                cardtype_code not in \n                (\n                   :2 ,\n                   :3 ,\n                   :4 ,\n                   :5 ,\n                   :6 ,\n                   :7 \n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.OtherCardBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,mesConstants.APP_CT_VISA);
   __sJT_st.setInt(3,mesConstants.APP_CT_VISA_CHECK_CARD);
   __sJT_st.setInt(4,mesConstants.APP_CT_VISA_BUSINESS_CARD);
   __sJT_st.setInt(5,mesConstants.APP_CT_MC);
   __sJT_st.setInt(6,mesConstants.APP_CT_MC_CHECK_CARD);
   __sJT_st.setInt(7,mesConstants.APP_CT_MC_BUSINESS_CARD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.OtherCardBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        switch(rs.getInt("cardtype_code")) 
        {
          case mesConstants.APP_CT_DEBIT:
            //this isnt required if debit if selected, 
            //its optional used for food stamps
            merchEBTReq    = false;
          break;
          case mesConstants.APP_CT_DINERS_CLUB:
            merchDinersReq = true;
          break;
          case mesConstants.APP_CT_DISCOVER: 
            merchDiscoverReq = true;
          break;
          case mesConstants.APP_CT_JCB:
            merchJCBReq = true;
          break;
          case mesConstants.APP_CT_AMEX:
            merchAmexReq = true;
          break;
        }
      }
      
      rs.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:211^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(to_char(merch_amex_pcid_num),
//                    decode(sic_code,
//                            5812, '011100',
//                            5814, '011100',
//                            7011, '011102',
//                            '010450'))
//          
//          from    merchant
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(to_char(merch_amex_pcid_num),\n                  decode(sic_code,\n                          5812, '011100',\n                          5814, '011100',\n                          7011, '011102',\n                          '010450'))\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.OtherCardBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchPCI = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:222^7*/

      this.merchPCIF = this.merchPCI;
      this.merchPCIS = this.merchPCI;
    }
    catch(Exception e)
    {
      logEntry("getOtherCardInfo(" + primaryKey + ")", e.toString());
      addError("getOtherCardInfo: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  private void updateCardMerchNumber(String merchNo, long primaryKey, int cardType)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:244^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchpayoption
//          set     merchpo_card_merch_number = :merchNo
//          where   app_seq_num = :primaryKey and
//                  cardtype_code = :cardType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchpayoption\n        set     merchpo_card_merch_number =  :1 \n        where   app_seq_num =  :2  and\n                cardtype_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.OtherCardBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNo);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,cardType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:250^7*/
    }
    catch(Exception e)
    {
      logEntry("updateCardMerchNumber(" + merchNo + ", " + primaryKey + ", " + cardType + ")", e.toString());
    }
  }
  
  private void resolveUserObject(HttpServletRequest request)
  {
    try
    {
      if(user == null)
      {
        HttpSession session = request.getSession();
        if (session != null)
        {
          user = (UserBean)session.getAttribute("UserLogin");
        }
      }
    }
    catch(Exception e)
    {
      logEntry("resolveUserObject()", e.toString());
      addError("resolveUserObject(): " + e.toString());
    }
  }
  
  public void submitData(HttpServletRequest req, long primaryKey)
  {
    try 
    {
      connect();
      
      resolveUserObject(req);
      
      if(this.merchEBTReq)
      {
        updateCardMerchNumber(merchEBT, primaryKey, mesConstants.APP_CT_DEBIT);
      }
      if(this.merchDinersReq)
      {
        updateCardMerchNumber(merchDiners, primaryKey, mesConstants.APP_CT_DINERS_CLUB);
      }
      if(this.merchDiscoverReq)
      {
        updateCardMerchNumber(merchDiscover, primaryKey, mesConstants.APP_CT_DISCOVER);
      }
      if(this.merchJCBReq)
      {
        updateCardMerchNumber(merchJCB, primaryKey, mesConstants.APP_CT_JCB);
      }
      if(this.merchAmexReq)
      {
        updateCardMerchNumber(merchAmex, primaryKey, mesConstants.APP_CT_AMEX);
      }

      /*@lineinfo:generated-code*//*@lineinfo:307^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_amex_pcid_num = :merchPCI
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_amex_pcid_num =  :1 \n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ops.OtherCardBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchPCI);
   __sJT_st.setLong(2,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:312^7*/

      moveToNextQueue(primaryKey);
    }
    catch(Exception e)
    {
      logEntry("submitData(" + primaryKey + ")", e.toString());
      addError("submitData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void updateQueue(int newStage, long primaryKey, int qType, int qStage)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:331^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_queue
//          set     app_queue_stage = :newStage
//          where   app_seq_num = :primaryKey and
//                  app_queue_type = :qType and
//                  app_queue_stage = :qStage
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_queue\n        set     app_queue_stage =  :1 \n        where   app_seq_num =  :2  and\n                app_queue_type =  :3  and\n                app_queue_stage =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ops.OtherCardBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,newStage);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,qType);
   __sJT_st.setInt(4,qStage);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:338^7*/
    }
    catch(Exception e)
    {
      logEntry("updateQueue(" + newStage + ", " + primaryKey + ", " + qType + ", " + qStage + ")", e.toString());
    }
  }
  
  private boolean isInQueue(long primaryKey, int qType, int qStage)
  {
    boolean result = false;
    
    try
    {
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:353^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    app_queue
//          where   app_seq_num = :primaryKey and
//                  app_queue_type = :qType and
//                  app_queue_stage = :qStage
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    app_queue\n        where   app_seq_num =  :1  and\n                app_queue_type =  :2  and\n                app_queue_stage =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.OtherCardBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,qType);
   __sJT_st.setInt(3,qStage);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:361^7*/
      
      result = ( recCount > 0 );
    }
    catch(Exception e)
    {
      logEntry("isInQueue(" + primaryKey + ", " + qType + ", " + qStage + ")", e.toString());
    }
    
    return ( result );
  }
  
  private void moveToNextQueue(long primaryKey)
  {
    try 
    {
      if(isInQueue(primaryKey, QueueConstants.QUEUE_SETUP, QueueConstants.Q_SETUP_QA_CORRECTION))
      {
        updateQueue(QueueConstants.Q_SETUP_COMPLETE, 
                    primaryKey,
                    QueueConstants.QUEUE_SETUP,
                    QueueConstants.Q_SETUP_QA_CORRECTION);
                    
        nextStage = QueueConstants.Q_SETUP_QA_CORRECTION;
      }
      // move from QA to completed (only if in QA already)
      else if(isInQueue(primaryKey, QueueConstants.QUEUE_SETUP, QueueConstants.Q_SETUP_QA))
      {
        if(moveToCompletedOk)
        {
          updateQueue(QueueConstants.Q_SETUP_COMPLETE, 
                      primaryKey, 
                      QueueConstants.QUEUE_SETUP,
                      QueueConstants.Q_SETUP_QA);
        }
        else
        {
          updateQueue(QueueConstants.Q_SETUP_QA_CORRECTION, 
                      primaryKey, 
                      QueueConstants.QUEUE_SETUP,
                      QueueConstants.Q_SETUP_QA);
        }
      
        nextStage = QueueConstants.Q_SETUP_QA;
      
        //add note if note is present
        if(!isBlank(errorDesc))
        {
          //add note..
          AppNoteBean anb = new AppNoteBean();
        
          anb.setAppSeqNum(primaryKey);
          anb.setUserId(userLogin);
          anb.setAppNote(errorDesc);
          anb.setQueueStage(QueueConstants.Q_SETUP_QA_CORRECTION);
          anb.setDepartment(QueueConstants.DEPARTMENT_TSYS);
          anb.submitData();
        }
      }
      else if(isInQueue(primaryKey, QueueConstants.QUEUE_SETUP, QueueConstants.Q_SETUP_NEW))
      {
        // this means the account is done being data-expanded and is ready for TSYS
        /*@lineinfo:generated-code*//*@lineinfo:423^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_setup_complete
//            (
//              app_seq_num,
//              date_completed
//            )
//            values
//            (
//              :primaryKey,
//              sysdate
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_setup_complete\n          (\n            app_seq_num,\n            date_completed\n          )\n          values\n          (\n             :1 ,\n            sysdate\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ops.OtherCardBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:435^9*/
        
        // move to QA queue
        updateQueue(QueueConstants.Q_SETUP_QA, 
                    primaryKey,
                    QueueConstants.QUEUE_SETUP,
                    QueueConstants.Q_SETUP_NEW);
                    
        nextStage = QueueConstants.Q_SETUP_QA;
        
        // move to MMS Queues        
        int appType = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:448^9*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//            
//            from    application
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n           \n          from    application\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.OtherCardBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:454^9*/
        
        // determine the profile generator (i.e. mms, vericentre, termmaster ...)
        EquipProfileGeneratorDictator epgd = new EquipProfileGeneratorDictator();
        int appLevelProfGen = epgd.getAppLevelProfileGeneratorCode(primaryKey);

        // determine the appropriate mms setup queue as a function of the app level profile generator
        int qType = MesQueues.Q_NONE;
        
        switch(appLevelProfGen) 
        {
          case mesConstants.PROFGEN_VCTM:
            qType = MesQueues.Q_MMS_VCTM_NEW;
            break;
            
          default:
            if(appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_CBT_NEW)
            {
              qType = MesQueues.Q_CBT_MMS_NEW;
            }
            else
            {
              qType = MesQueues.Q_MMS_NEW;
            }
            break;
        }
    
        // put in new mms setup queue
        QueueTools.insertQueue(primaryKey, qType, user);

        // fix time stamps so app tracking is correct
        fixTimeStamps(primaryKey, userLogin);
      }
      else if(isInQueue(primaryKey, QueueConstants.QUEUE_SETUP, QueueConstants.Q_SETUP_MANUAL))
      {
        updateQueue(QueueConstants.Q_SETUP_COMPLETE,
                    primaryKey,
                    QueueConstants.QUEUE_SETUP,
                    QueueConstants.Q_SETUP_MANUAL);
                    
        nextStage = QueueConstants.Q_SETUP_MANUAL;
      }
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("moveToNextQueue(" + primaryKey + ")", e.toString());
      addError("moveToQaQueue: " + e.toString());
    }
  }  
  
  private void fixTimeStamps(long primaryKey, String loginName)
  {
    try 
    {
      //updates tsys department to completed status and time stamps completed
      /*@lineinfo:generated-code*//*@lineinfo:511^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     date_completed = sysdate,
//                  status_code = 202,
//                  contact_name = :loginName
//          where   app_seq_num = :primaryKey and
//                  dept_code = :QueueConstants.DEPARTMENT_TSYS
//                  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     date_completed = sysdate,\n                status_code = 202,\n                contact_name =  :1 \n        where   app_seq_num =  :2  and\n                dept_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.ops.OtherCardBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_TSYS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:520^7*/

      //updates mms department to received status and time stamps received
      /*@lineinfo:generated-code*//*@lineinfo:523^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     date_received = sysdate,
//                  status_code = 301
//          where   app_seq_num = :primaryKey and
//                  dept_code = :QueueConstants.DEPARTMENT_MMS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     date_received = sysdate,\n                status_code = 301\n        where   app_seq_num =  :1  and\n                dept_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.ops.OtherCardBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_MMS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:530^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "fixTimeStamps: " + e.toString());
      addError("fixTimeStamps: " + e.toString());
    }
  }
  
  private boolean isValidMerchNum(String merchNum)
  {
    boolean result    = false;
    long    tempLong  = 0L;

    try
    {
      tempLong = Long.parseLong(merchNum);
      if(tempLong > 0)
      {
        result = true;
      }
    }
    catch(Exception e)
    {
    }
    return result;
  }
  
  private void validData(boolean req, String merchNum, String card, String desc)
  {
    if(req && (isBlank(merchNum) || merchNum.equals("0")))
    {
      addError(card + " was selected on the Merchant Discount Page so you must provide a valid" + desc);
    }
    else if(req && !isBlank(merchNum))
    {
      if(!isValidMerchNum(merchNum))
      {
        addError("Please provide a valid " + desc + " for " + card);
      }
    }    
    else if(!req && !isBlank(merchNum))
    {
      addError(card + " was not selected on the Merchant Discount Page so no " + desc + " is needed");
    }

  }
  public boolean validate()
  {
    validData(this.merchDinersReq,    this.merchDiners,   "Diners Card",    "merchant number");
    validData(this.merchAmexReq,      this.merchAmex,     "Amex Card",      "SE number");
    validData(this.merchJCBReq,       this.merchJCB,      "JCB Card",       "merchant number");
    validData(this.merchDiscoverReq,  this.merchDiscover, "Discover Card",  "merchant number");

    //no validation needed for debit and fcs
    //validData(this.merchEBTReq,       this.merchEBT,      "Debit Card",     "EBT FCS #");
    
    if((!isBlank(this.merchPCIS) && !isBlank(this.merchPCIF)) && (!this.merchPCIF.equals(this.merchPCIS)))
    {
      addError("Either select Amex PCI # from list or fill Amex PCI in");
    }
    else
    {
      if((!isBlank(this.merchPCIS) && !isBlank(this.merchPCIF)) && (this.merchPCIF.equals(this.merchPCIS)))
      {
        this.merchPCI = this.merchPCIS;
      }
      else if(isBlank(this.merchPCIS) && !isBlank(this.merchPCIF))
      {
        this.merchPCI = this.merchPCIF;
      }
      else if(!isBlank(this.merchPCIS) && isBlank(this.merchPCIF))
      {
        this.merchPCI = this.merchPCIS;
      }
      //if(this.merchAmexReq && isBlank(this.merchPCI))
      //{
        //addError("Please select an Amex PCI # from list OR fill in the Amex PCI");
      //}
    }
    
    if(!isBlank(this.merchPCI) && isValidMerchNum(this.merchPCI))
    {
      if(this.merchPCI.length() > 6)
      {
        addError("Amex PCI # must be less than 6 digits long");
      }
    }
    else if(!isBlank(this.merchPCI) && !isValidMerchNum(this.merchPCI))
    {
      addError("Please provide a valid 6 digit numerical Amex PCI");
    }  
   
    return(! hasErrors());
  }

  public void setMerchDiners(String merchDiners)
  {
    this.merchDiners = merchDiners.trim();
  }
  public void setMerchAmex(String merchAmex)
  {
    this.merchAmex = merchAmex.trim();
  }
  public void setMerchJcb(String merchJCB)
  {
    this.merchJCB = merchJCB.trim();
  }
  public void setMerchDiscover(String merchDiscover)
  {
    this.merchDiscover = merchDiscover.trim();
  }
  public void setMerchEbt(String merchEBT)
  {
    this.merchEBT = merchEBT.trim();
  }
  public void setMerchPcif(String merchPCIF)
  {
    this.merchPCIF = merchPCIF;
  }
  public void setMerchPcis(String merchPCIS)
  {
    this.merchPCIS = merchPCIS;
  }


  public String getMerchEbt()
  {
    return this.merchEBT;
  }
  public String getMerchDiscover()
  {
    return this.merchDiscover;
  }
  public String getMerchJcb()
  {
    return this.merchJCB;
  }
  public String getMerchAmex()
  {
    return this.merchAmex;
  }
  public String getMerchDiners()
  {
    return this.merchDiners;
  }

  public String getMerchPcif()
  {
    return this.merchPCIF;
  }
  public String getMerchPcis()
  {
    return this.merchPCIS;
  }

  public String getAmexEsa()
  {
    return amexEsa;
  }
  public String getDiscoverRap()
  {
    return discoverRap;
  }

  public void setAmexEsa(String amexEsa)
  {
    this.amexEsa = amexEsa;
  }
  public void setDiscoverRap(String discoverRap)
  {
    this.discoverRap = discoverRap;
  }

  public void setMoveToCompletedOk(boolean bool)
  {
    this.moveToCompletedOk = bool;
  }

                            
  public void setMerchDinersReq(String merchDinersReq)
  {
    this.merchDinersReq = true;
  }
  public void setMerchAmexReq(String merchAmexReq)
  {
    this.merchAmexReq = true;
  }
  public void setMerchJcbReq(String merchJCBReq)
  {
    this.merchJCBReq = true;
  }
  public void setMerchDiscoverReq(String merchDiscoverReq)
  {
    this.merchDiscoverReq = true;
  }
  public void setMerchEbtReq(String merchEBTReq)
  {
    this.merchEBTReq = true;
  }

  public void setErrorDesc(String errorDesc)
  {
    this.errorDesc = errorDesc;
  }
  public String getErrorDesc()
  {
    return this.errorDesc;
  }

  public void setUserLogin(String userLogin)
  {
    this.userLogin = userLogin;
  }
  public String getUserLogin()
  {
    return this.userLogin;
  }

  public String getMerchDinersReq()
  {
    String result = "";
    if(this.merchDinersReq)
    {
      result = "<input type=\"hidden\" name=\"merchDinersReq\" value=\"true\">";
    }
    return result;
  }
  public String getMerchAmexReq()
  {
    String result = "";
    if(this.merchAmexReq)
    {
      result = "<input type=\"hidden\" name=\"merchAmexReq\" value=\"true\">";
    }
    return result;
  }
  public String getMerchJcbReq()
  {             
    String result = "";
    if(this.merchJCBReq)
    {
      result = "<input type=\"hidden\" name=\"merchJcbReq\" value=\"true\">";
    }
    return result;
  }
  public String getMerchDiscoverReq()
  {
    String result = "";
    if(this.merchDiscoverReq)
    {
      result = "<input type=\"hidden\" name=\"merchDiscoverReq\" value=\"true\">";
    }
    return result;
  }
  public String getMerchEbtReq()
  {
    String result = "";
    if(this.merchEBTReq)
    {
      result = "<input type=\"hidden\" name=\"merchEbtReq\" value=\"true\">";
    }
    return result;
  }

  public int getNextStage()
  {
    return this.nextStage;
  }

}/*@lineinfo:generated-code*/