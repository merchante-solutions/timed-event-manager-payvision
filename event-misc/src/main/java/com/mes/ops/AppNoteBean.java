/*@lineinfo:filename=AppNoteBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AppNoteBean.sqlj $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 11/19/03 12:41p $
  Version            : $Revision: 12 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Vector;
import sqlj.runtime.ResultSetIterator;

public class AppNoteBean 
  extends com.mes.database.SQLJConnectionBase
{
  public static int     ERR_INVALID_SEQ_NUM     = 0x01;
  public static int     ERR_EMPTY_NOTE          = 0x02;
  public static int     ERR_DATABASE_ERR        = 0x04;
  
  public final static int ORDERBY_DEPARTMENT    = 1;
  public final static int ORDERBY_DATE          = 2;
  public final static int ORDERBY_USER          = 3;
  
  public final static int SZ_APP_NOTE_MESSAGE   = 500;

  private long          appSeqNum               = 0L;
  private long          controlNumber           = 0L;
  private String        merchantName            = "";
  public  Timestamp     noteDateTime            = null;
  private String        noteDate                = "";
  private String        noteTime                = "";
  private String        userId                  = "";
  private String        appNote                 = "";
  private int           queueStage              = 0;
  private int           department              = com.mes.ops.QueueConstants.DEPARTMENT_ALL;
  private int           sortOrder               = 2;
  private String        orderBy                 = "";
  private String        backPage                = "";
  private boolean       salesNotes              = false;
  private Vector        appNotes                = null;

  public int submitData()
  {
    int                 error       = isValid();
    boolean             useCleanUp  = false;

    if(error == 0)
    {
      try
      {
        if(isConnectionStale())
        {
          connect();
          useCleanUp = true;
        }
        
        int tempDepartment = this.department;
        if(isSalesNotes())
        {
          tempDepartment = com.mes.ops.QueueConstants.DEPARTMENT_SALES;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:89^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_notes
//            (
//              app_seq_num,
//              app_queue_stage,
//              app_note_date,
//              app_note_user,
//              app_note_message,
//              app_department
//            )
//            values
//            (
//              :appSeqNum,
//              :queueStage,
//              sysdate,
//              :userId,
//              :appNote,
//              :tempDepartment
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_notes\n          (\n            app_seq_num,\n            app_queue_stage,\n            app_note_date,\n            app_note_user,\n            app_note_message,\n            app_department\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n            sysdate,\n             :3 ,\n             :4 ,\n             :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.ops.AppNoteBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,queueStage);
   __sJT_st.setString(3,userId);
   __sJT_st.setString(4,appNote);
   __sJT_st.setInt(5,tempDepartment);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:109^9*/

        //if deployment note.. we get merchnumber then put in call tracking notes..
        if(queueStage == QueueConstants.Q_EQUIPMENT_NEW || this.department == QueueConstants.DEPARTMENT_DEPLOYMENT)
        {
          String merchantNumber = null;

          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:118^13*/

//  ************************************************************
//  #sql [Ctx] { select merch_number 
//                from   merchant
//                where  app_seq_num = :appSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select merch_number  \n              from   merchant\n              where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.AppNoteBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantNumber = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:123^13*/
          }
          catch(Exception e)
          {
          }

          
          if(merchantNumber != null && !merchantNumber.equals(""))
          {
            //put note in call tracking..
            /*@lineinfo:generated-code*//*@lineinfo:133^13*/

//  ************************************************************
//  #sql [Ctx] { insert into service_calls
//                            (
//                              merchant_number,
//                              call_date,
//                              type,
//                              other_description,
//                              login_name,
//                              call_back,
//                              status,
//                              notes,
//                              billable,
//                              client_generated
//                            )
//                            values
//                            (
//                              :merchantNumber,
//                              sysdate,
//                              14, -- internal so that clients will not be billed
//                              'Deployment',
//                              :userId,
//                              'N',
//                              2,
//                              :appNote,
//                              'N',
//                              'N'
//                            )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into service_calls\n                          (\n                            merchant_number,\n                            call_date,\n                            type,\n                            other_description,\n                            login_name,\n                            call_back,\n                            status,\n                            notes,\n                            billable,\n                            client_generated\n                          )\n                          values\n                          (\n                             :1 ,\n                            sysdate,\n                            14, -- internal so that clients will not be billed\n                            'Deployment',\n                             :2 ,\n                            'N',\n                            2,\n                             :3 ,\n                            'N',\n                            'N'\n                          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.AppNoteBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   __sJT_st.setString(2,userId);
   __sJT_st.setString(3,appNote);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^13*/
          }
        }

      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
        error |= ERR_DATABASE_ERR;
      }
      finally
      {
        if(useCleanUp)
        {
          cleanUp();
        }
      }
    }
    
    return error;
  }

  
  public int isValid()
  {
    int error = 0;
    
    if(appSeqNum <= 0)
    {
      error |= ERR_INVALID_SEQ_NUM;
    }
    
    if(appNote == null || appNote.equals(""))
    {
      error |= ERR_EMPTY_NOTE;
    }
    
    return error;
  }
  
  public String getDateString(Timestamp dt)
  {
    String        dtString = "";
    DateFormat    dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    
    dtString = dateFormat.format(dt);
    
    return dtString;
  }
  
  public String getTimeString(Timestamp dt)
  {
    String        tmString = "";
    DateFormat    dateFormat = new SimpleDateFormat("hh:mm:ss");
    
    tmString = dateFormat.format(dt);
    
    return tmString;
  }
  

  public int getAppNoteCount(long appSeqNum)
  {
    int                 noteCount = 0;
 
    try
    {
      connect();
      
      if(department == QueueConstants.DEPARTMENT_ALL)
      {
        /*@lineinfo:generated-code*//*@lineinfo:232^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//            from    app_notes
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n          from    app_notes\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AppNoteBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   noteCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:241^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//            from    app_notes
//            where   app_seq_num = :appSeqNum and
//                    app_department = :department
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n          from    app_notes\n          where   app_seq_num =  :1  and\n                  app_department =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.AppNoteBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,department);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   noteCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:247^9*/
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAppNotes: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return noteCount;
  }

  public Iterator getAppNotes(long appSeqNum)
  {
    Iterator            notes = null;
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    try
    {
      connect();
      
      if(appNotes == null)
      {
        appNotes = new Vector();

          switch(sortOrder)
          {
            case ORDERBY_DEPARTMENT:
              /*@lineinfo:generated-code*//*@lineinfo:279^15*/

//  ************************************************************
//  #sql [Ctx] it = { select    app_seq_num,
//                            app_queue_stage,
//                            app_note_date,
//                            app_note_user,
//                            app_note_message,
//                            app_department
//                  from      app_notes
//                  where     app_seq_num = :appSeqNum
//                  order by  app_department,app_note_date
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    app_seq_num,\n                          app_queue_stage,\n                          app_note_date,\n                          app_note_user,\n                          app_note_message,\n                          app_department\n                from      app_notes\n                where     app_seq_num =  :1 \n                order by  app_department,app_note_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.AppNoteBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.AppNoteBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:290^15*/
              break;
            case ORDERBY_USER:
              /*@lineinfo:generated-code*//*@lineinfo:293^15*/

//  ************************************************************
//  #sql [Ctx] it = { select    app_seq_num,
//                            app_queue_stage,
//                            app_note_date,
//                            app_note_user,
//                            app_note_message,
//                            app_department
//                  from      app_notes
//                  where     app_seq_num = :appSeqNum
//                  order by  app_note_user,app_note_date
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    app_seq_num,\n                          app_queue_stage,\n                          app_note_date,\n                          app_note_user,\n                          app_note_message,\n                          app_department\n                from      app_notes\n                where     app_seq_num =  :1 \n                order by  app_note_user,app_note_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.AppNoteBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.AppNoteBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:304^15*/
              break;
            default:
              /*@lineinfo:generated-code*//*@lineinfo:307^15*/

//  ************************************************************
//  #sql [Ctx] it = { select    app_seq_num,
//                            app_queue_stage,
//                            app_note_date,
//                            app_note_user,
//                            app_note_message,
//                            app_department
//                  from      app_notes
//                  where     app_seq_num = :appSeqNum
//                  order by  app_note_date
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    app_seq_num,\n                          app_queue_stage,\n                          app_note_date,\n                          app_note_user,\n                          app_note_message,\n                          app_department\n                from      app_notes\n                where     app_seq_num =  :1 \n                order by  app_note_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.AppNoteBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.AppNoteBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:318^15*/
              break;
          }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          AppNoteBean newAppNote = 
            new AppNoteBean(
              rs.getLong("app_seq_num"), 
              rs.getTimestamp("app_note_date"), 
              rs.getString("app_note_user"), 
              rs.getString("app_note_message"), 
              rs.getInt("app_queue_stage"), 
              rs.getInt("app_department"));
          
          appNotes.add(newAppNote);
        }
      }
      
      notes = appNotes.iterator();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAppNotes: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return notes;
  }
  
  public AppNoteBean()
  {
  }
  
  public AppNoteBean(long appSeqNum, Timestamp noteDate, String userId, String appNote, int queueStage, int department)
  {
    this.appSeqNum    = appSeqNum;
    this.noteDateTime = noteDate;
    this.noteDate     = getDateString(noteDate);
    this.noteTime     = getTimeString(noteDate);
    this.userId       = userId;
    this.appNote      = appNote;
    this.queueStage   = queueStage;
    this.department   = department;
  }
  
  public void setAppSeqNum(String appSeqNum)
  {
    try
    {
      setAppSeqNum(Long.parseLong(appSeqNum));
    }
    catch(Exception e)
    {
      logEntry("setAppSeqNum(" + appSeqNum + ")", e.toString());
    }
  }
  
  public void setAppSeqNum(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
    
    try
    {
      connect();
      
      // get control number
      /*@lineinfo:generated-code*//*@lineinfo:390^7*/

//  ************************************************************
//  #sql [Ctx] { select  merc_cntrl_number 
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merc_cntrl_number  \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.AppNoteBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   controlNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:395^7*/
      
      // merchant name
      /*@lineinfo:generated-code*//*@lineinfo:398^7*/

//  ************************************************************
//  #sql [Ctx] { select  merch_business_name 
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_business_name  \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.AppNoteBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:403^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setAppSeqNum: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public long getAppSeqNum()
  {
    return this.appSeqNum;
  }
  
  public void setSalesNotes(String bool)
  {
    salesNotes = true;
  }
  public void setUserId(String userId)
  {
    this.userId = userId;
  }
  
  public String getUserId()
  {
    return this.userId;
  }
  
  public void setAppNote(String appNote)
  {
    if(appNote != null)
    {
      this.appNote = (appNote.length() > SZ_APP_NOTE_MESSAGE) ? appNote.substring(0, SZ_APP_NOTE_MESSAGE) : appNote;
    }
  }
  
  public String getAppNote()
  {
    return this.appNote;
  }
  
  public String getNoteDate()
  {
    return this.noteDate;
  }
  
  public String getNoteTime()
  {
    return this.noteTime;
  }
  
  public String getMerchantName()
  {
    return this.merchantName;
  }
  
  public long getControlNumber()
  {
    return this.controlNumber;
  }
  
  public void setQueueStage(String queueStage)
  {
    try
    {
      setQueueStage(Integer.parseInt(queueStage));
    }
    catch(Exception e)
    {
      logEntry("setQueueStage(" + queueStage + ")", e.toString());
    }
  }
  public void setQueueStage(int queueStage)
  {
    this.queueStage = queueStage;
  }
  
  public int getQueueStage()
  {
    return this.queueStage;
  }

  public void setDepartment(String department)
  {
    try
    {
      setDepartment(Integer.parseInt(department));
    }
    catch(Exception e)
    {
      logEntry("setDepartment(" + department + ")", e.toString());
    }
  }
  public void setDepartment(int department)
  {
    this.department = department;
  }

  public void setSortOrder(String sortOrder)
  {
    try
    {
      setSortOrder(Integer.parseInt(sortOrder));
    }
    catch(Exception e)
    {
      logEntry("setSortOrder(" + sortOrder + ")", e.toString());
    }
  }
  public void setSortOrder(int sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  
  public int getDepartment()
  {
    return this.department;
  }

  public void setBackPage(String backPage)
  {
    this.backPage = backPage;
  }
  
  public String getBackPage()
  {
    return this.backPage;
  }
  public boolean isSalesNotes()
  {
    return this.salesNotes;
  }
}/*@lineinfo:generated-code*/