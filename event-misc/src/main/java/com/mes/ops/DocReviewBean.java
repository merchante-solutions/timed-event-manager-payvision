/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/DocReviewBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 9 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.mes.database.DBConnect;

public class DocReviewBean
{
  public static final String  DB_CONNECT_STRING       = "pool;ApplicationPool";
  public static final int     ERR_INVALID_SEQ_NO      = 0x0001;
  public static final int     ERR_NOT_ASSIGNED        = 0x0002;
  public static final int     ERR_INVALID_REF_NO      = 0x0004;
  public static final int     ERR_DATABASE_ERR        = 0x8000;
  
  private long          appSeqNum           = 0L;
  private String        lastError           = "";
  private long          documents           = 0L;
  private long          refNum              = 0L;
  private int           miscDocuments       = 0;
  private int           error               = 0;

  private DBConnect     db                  = null;
  private Connection    con                 = null;
  
  private int isValid()
  {
        
    if(appSeqNum == 0L)
    {
      error |= ERR_INVALID_SEQ_NO;
    }
    
    return error;
  }
  
  public String getLastError()
  {
    return this.lastError;
  }
  
  public DocReviewBean()
  {
  }
  
  public void setAppSeqNum(String appSeqNum)
  {
    try
    {
      setAppSeqNum(Long.parseLong(appSeqNum));
    }
    catch(Exception e)
    {
    }
  }
  public void setAppSeqNum(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }

  public void setRefNum(String refNum)
  {
    if(refNum == null || refNum.equals(""))
    {
      this.refNum = 0L;
      return;
    }
    
    try
    {
      this.refNum = Long.parseLong(refNum);
    }
    catch(Exception e)
    {
      error |= ERR_INVALID_REF_NO;
      this.refNum = 0L;
    }
  }
  
  public void setDocuments(String[] documents)
  {
    for(int i=0; i<documents.length; ++i)
    {
      try
      {
        this.documents += Long.parseLong(documents[i]);
      }
      catch(Exception e)
      {
      }
    }
  }
  
  public void setMiscDocuments(String miscDocuments)
  {
    try
    {
      this.miscDocuments = Integer.parseInt(miscDocuments);
    }
    catch(Exception e)
    {
    }
  }
  
  public int submitData(String user)
  {
    StringBuffer        qs            = new StringBuffer("");
    PreparedStatement   ps            = null;
    ResultSet           rs            = null;
    
    try
    {
      db = new DBConnect(this, "submitData");
      
      if(error == 0)
      {
        if(con == null)
        {
          con = db.getConnection(DB_CONNECT_STRING);
        }
        
        // update the queue
        qs.append("update app_queue_document ");
        qs.append("set    app_doc_received = ?, ");
        qs.append("       app_doc_misc_recvd = ?, ");
        qs.append("       app_doc_reference_num = ? ");
        qs.append("where  app_seq_num = ?");
        ps = con.prepareStatement(qs.toString());
        ps.setLong(1, this.documents);
        ps.setInt(2, this.miscDocuments);
        if(this.refNum == 0L)
        {
          ps.setNull(3, java.sql.Types.FLOAT);
        }
        else
        {
          ps.setLong(3, this.refNum);
        }
        ps.setLong(4, this.appSeqNum);
        
        if(ps.executeUpdate() == 1)
        {
          ps.close();
          
          qs.setLength(0);
          ps = con.prepareStatement("select * from app_queue_document where app_seq_num = ?");
          ps.setLong(1, this.appSeqNum);
          
          rs = ps.executeQuery();
          
          if(rs.next())
          {
            if(rs.getLong("app_doc_need") == rs.getLong("app_doc_received") &&
               rs.getInt("app_doc_misc_recvd") > 0)
            {
              // this item is ready to leave the documentation queue
              qs.setLength(0);
              qs.append("update app_queue ");
              qs.append("set    app_queue_stage = ?, ");
              qs.append("       app_status = ?, ");
              qs.append("       app_last_user = ?, ");
              qs.append("       app_last_date = sysdate ");
              qs.append("where  app_seq_num = ? and ");
              qs.append("       app_queue_type = ?");
              
              ps.close();
              ps = con.prepareStatement(qs.toString());
              ps.setInt(1, com.mes.ops.QueueConstants.Q_DOCUMENT_RECEIVED);
              ps.setInt(2, com.mes.ops.QueueConstants.Q_STATUS_DOCUMENT_RECEIVED);
              ps.setString(3, user);
              ps.setLong(4, this.appSeqNum);
              ps.setInt(5, com.mes.ops.QueueConstants.QUEUE_DOCUMENTATION);
              
              if(ps.executeUpdate() != 1)
              {
                error |= ERR_NOT_ASSIGNED;
              }
              
              ps.close();
            }
            
            rs.close();
          }
          else
          {
            error |= ERR_DATABASE_ERR;
          }
        }
        else
        {
          error |= ERR_NOT_ASSIGNED;
        }
      }
    }
    catch(Exception e)
    {
      error |= ERR_DATABASE_ERR;
      lastError = e.toString();
    }
    finally
    {
      if(db != null)
      {
        db.releaseConnection();
      }
    }
    
    return error;
  }
}
