/*@lineinfo:filename=NonBankcardBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/NonBankcardBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/10/04 3:27p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.support.SyncLog;
import com.mes.tools.AppNotifyBean;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class NonBankcardBean extends com.mes.database.SQLJConnectionBase
{
  private String    submitVal   = "";
  private boolean   submit      = false;
  private boolean   delete      = false;
  private Vector    errors      = new Vector();
  private Vector    cards       = new Vector();
  
  // new card type
  private int       cardType;
  private String    merchId;
  private boolean   splitDial;
  
  // card type record helper class
  public class CardTypeRecord
  {
    private int       cardType;
    private String    typeDesc;
    private String    merchId;
    private boolean   splitDial;
    
    public CardTypeRecord(int cardType, 
                          String typeDesc,
                          String merchId, 
                          boolean splitDial)
    {
      this.cardType   = cardType;
      this.typeDesc   = typeDesc;
      this.merchId    = merchId;
      this.splitDial  = splitDial;
    }
    
    public int getCardType()
    {
      return cardType;
    }
    public String getTypeDesc()
    {
      return typeDesc;
    }
    public String getMerchId()
    {
      return merchId;
    }
    public boolean getSplitDial()
    {
      return splitDial;
    }
  }
  
  // NonBankcardTable, used to populate a drop down list of nonbankcard
  // card types that may be assigned to a merchant.
  public class NonBankcardTable extends DropDownTable
  {
    public NonBankcardTable()
    {
      getData();
    }
  
    protected boolean getData()
    {
      ResultSetIterator     it      = null;
      ResultSet             rs      = null;
      
      // current valid card types are dinr, disc, jcb and amex
      int validCards[] = 
      {
        mesConstants.APP_CT_DINERS_CLUB,
        mesConstants.APP_CT_DISCOVER,
        mesConstants.APP_CT_JCB,
        mesConstants.APP_CT_AMEX
      };
    
      boolean getOk = false;

      try
      {
        connect();
      
        /*@lineinfo:generated-code*//*@lineinfo:117^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    cardtype_code,
//                      cardtype_desc
//            from      cardtype
//            order by  cardtype_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    cardtype_code,\n                    cardtype_desc\n          from      cardtype\n          order by  cardtype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.NonBankcardBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.NonBankcardBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:123^9*/
        
        rs = it.getResultSet();
        
        while (rs.next())
        {
          for (int i = 0; i < validCards.length; ++i)
          {
            // add any card type in the valid card list
            if (rs.getInt("cardtype_code") == validCards[i])
            {
              addElement(rs);
            }
          }
            
          getOk = true;
        }
        
        it.close();
      }
      catch (Exception e)
      {
        SyncLog.LogEntry(this.getClass().getName(), 
          "getData: " + e.toString());
      }
      finally
      {
        cleanUp();
      }
    
      return getOk;
    }
  }
  
  // return a new instance of drop down list table
  public DropDownTable getNonBankcardTable()
  {
    return new NonBankcardTable();
  }

  public void writeCardData(long appSeqNum)
  {
    try
    {
      // delete the old set of nonbank cards
      /*@lineinfo:generated-code*//*@lineinfo:168^7*/

//  ************************************************************
//  #sql [Ctx] { delete from merchpayoption
//          where app_seq_num = :appSeqNum and
//                cardtype_code <> 1 and
//                cardtype_code <> 4
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from merchpayoption\n        where app_seq_num =  :1  and\n              cardtype_code <> 1 and\n              cardtype_code <> 4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.NonBankcardBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:174^7*/
      
      // write the new set of nonbank cards
      for (int i = 0; i < cards.size(); ++i)
      {
        CardTypeRecord rec = (CardTypeRecord)cards.get(i);
        boolean amexSplitDial = (rec.getCardType() == 
          mesConstants.APP_CT_AMEX && rec.getSplitDial());
        /*@lineinfo:generated-code*//*@lineinfo:182^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchpayoption
//            (
//              app_seq_num,
//              card_sr_number,
//              merchpo_card_merch_number,
//              cardtype_code,
//              merchpo_split_dial
//            )
//            values
//            (
//              :appSeqNum,
//              :i + 3,
//              :rec.getMerchId(),
//              :rec.getCardType(),
//              :(amexSplitDial ? "Y" : "N")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_800 = i + 3;
 String __sJT_801 = rec.getMerchId();
 int __sJT_802 = rec.getCardType();
 String __sJT_803 = (amexSplitDial ? "Y" : "N");
   String theSqlTS = "insert into merchpayoption\n          (\n            app_seq_num,\n            card_sr_number,\n            merchpo_card_merch_number,\n            cardtype_code,\n            merchpo_split_dial\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.NonBankcardBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,__sJT_800);
   __sJT_st.setString(3,__sJT_801);
   __sJT_st.setInt(4,__sJT_802);
   __sJT_st.setString(5,__sJT_803);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^9*/
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        "::writeCardData(appSeqNum = " + appSeqNum + "): " +
        e.toString());
      logEntry(this.getClass().getName() + 
        "::writeCardData(seq# = " + appSeqNum + ")",
        e.toString());
    }
  }

  public void assignCard(long appSeqNum)
  {
    try
    {
      // load a cardtype desc from db
      String typeDesc;
      /*@lineinfo:generated-code*//*@lineinfo:220^7*/

//  ************************************************************
//  #sql [Ctx] { select cardtype_desc 
//          from   cardtype
//          where  cardtype_code = :cardType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select cardtype_desc  \n        from   cardtype\n        where  cardtype_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.NonBankcardBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,cardType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   typeDesc = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:225^7*/
      
      // add the new card type to 
      // the current set of nonbank cards
      cards.add(new CardTypeRecord(cardType,typeDesc,merchId,splitDial));
  
      // write the set to the db
      writeCardData(appSeqNum);

      // do notification
      AppNotifyBean nb = new AppNotifyBean();
      nb.setCardType(cardType);
      nb.notifyStatus(appSeqNum,mesConstants.APP_STATUS_ADDED_CARD,-1);
    }
    catch (Exception e)
    {
      logEntry(this.getClass().getName() + 
        "::assignCard(appSeqNum = " + appSeqNum + ")",
        e.toString());
    }
  }
        
  public void deleteCard(long appSeqNum)
  {
    // delete the card type from the set
    for (int i = 0; i < cards.size(); ++i)
    {
      // if the card type is located, remove it from the cards
      // and write the trimmed set to the db
      if (((CardTypeRecord)cards.get(i)).getCardType() == cardType)
      {
        cards.remove(i);
        
        writeCardData(appSeqNum);

        // do notification
        AppNotifyBean nb = new AppNotifyBean();
        nb.setCardType(cardType);
        nb.notifyStatus(appSeqNum,mesConstants.APP_STATUS_REMOVED_CARD,-1);
        break;
      }
    }
  }
  
  public void submitData(long appSeqNum)
  {
    // if delete flag turned on then
    // delete the current specified card type
    if(delete)
    {
      deleteCard(appSeqNum);
    }
    // else insert current card type into the db
    else
    {
      assignCard(appSeqNum);
    }
  }
  
  public void loadCardData(long appSeqNum)
  {
    try
    {
      // reset the card set to empty vector
      cards = new Vector();
      
      // query for all non bankcard types
      ResultSetIterator it = null;
      ResultSet         rs = null;
      /*@lineinfo:generated-code*//*@lineinfo:294^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    mpo.merchpo_card_merch_number,
//                    mpo.cardtype_code,
//                    mpo.merchpo_split_dial,
//                    ct.cardtype_desc
//                    
//          from      merchpayoption mpo,
//                    cardtype ct
//                    
//          where     mpo.app_seq_num = :appSeqNum and
//                    mpo.cardtype_code <> 1 and
//                    mpo.cardtype_code <> 4 and
//                    mpo.cardtype_code = ct.cardtype_code
//                    
//          order by  mpo.cardtype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mpo.merchpo_card_merch_number,\n                  mpo.cardtype_code,\n                  mpo.merchpo_split_dial,\n                  ct.cardtype_desc\n                  \n        from      merchpayoption mpo,\n                  cardtype ct\n                  \n        where     mpo.app_seq_num =  :1  and\n                  mpo.cardtype_code <> 1 and\n                  mpo.cardtype_code <> 4 and\n                  mpo.cardtype_code = ct.cardtype_code\n                  \n        order by  mpo.cardtype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.NonBankcardBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.NonBankcardBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:310^7*/
      
      // load the results into internal card type storage vector
      rs = it.getResultSet();
      while (rs.next())
      {
        boolean splitDial = false;
        
        if(rs.getString("merchpo_split_dial") != null &&
           rs.getString("merchpo_split_dial").equals("Y"))
        {
          splitDial = true;
        }
        
        cards.add(new CardTypeRecord(
            rs.getInt("cardtype_code"),
            rs.getString("cardtype_desc"),
            rs.getString("merchpo_card_merch_number"),
            splitDial));
      }
    }
    catch(Exception e)
    {
      logEntry(this.getClass().getName() + 
        "::loadCardData(appSeqNum = " + appSeqNum + ")",
        e.toString());
    }
  }
  
  /*
  ** Validation and error handling
  */
  public boolean cardAlreadyAssigned(long appSeqNum)
  {
    int numCount = 0;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:347^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(cardtype_code) 
//          from    merchpayoption
//          where   app_seq_num = :appSeqNum and 
//                  cardtype_code = :cardType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cardtype_code)  \n        from    merchpayoption\n        where   app_seq_num =  :1  and \n                cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.NonBankcardBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   numCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:353^7*/
    }
    catch (Exception e)
    {
      logEntry(this.getClass().getName() + 
        "::cardAlreadyAssigned(appSeqNum = " + appSeqNum + ")",
        e.toString());
    }
    
    return (numCount > 0);
  }
  public void addError(String errorMsg)
  {
    errors.add(errorMsg);
  }
  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }
  public boolean validate(long appSeqNum)
  {
    if(!delete)
    {
      if (cardAlreadyAssigned(appSeqNum))
      {
        addError("Unable to assign card type because card type is already assigned.");
      }
      
      if (merchId == null || merchId.length() < 8)
      {
        addError("Merchant number is invalid");
      }
    }
        
    return hasErrors();
  }
  public Vector getErrors()
  {
    return this.errors;
  }
  
  /*
  ** Accessors
  */
  public int getCardType()
  {
    return cardType;
  }
  public void setCardType(String cardType)
  {
    try
    {
      setCardType(Integer.parseInt(cardType));
    }
    catch(Exception e)
    {
      logEntry("setCardType(" + cardType + ")", e.toString());
    }
  }
  public void setCardType(int newCardType)
  {
    cardType = newCardType;
  }
  
  public String getMerchId()
  {
    return merchId;
  }
  public void setMerchId(String newMerchId)
  {
    merchId = newMerchId;
  }
  
  public boolean getSplitDial()
  {
    return splitDial;
  }
  public void setSplitDial(String splitDialVal)
  {
    splitDial = splitDialVal.equals("Y");
  }
  
  public Vector getCards()
  {
    return cards;
  }

  public boolean isSubmitted()
  {
    return submit;
  }
  public void setSubmit(String newSubmitVal)
  {
    submitVal = newSubmitVal;
    submit = true;
  }

  public void setDeleteCard(String deleteVal)
  {
    delete = true;
  }
  public boolean isDelete()
  {
    return delete;
  }
}/*@lineinfo:generated-code*/