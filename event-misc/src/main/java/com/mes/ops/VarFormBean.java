/*@lineinfo:filename=VarFormBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/VarFormBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 4/28/04 3:34p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class VarFormBean extends SQLJConnectionBase
{
  
  public static final String VS_ACQUIRER                    = "Merchant e-Solutions";
  public static final String VS_ACQUIRER_CONTACT            = "CLIENT SUPPORT";
  public static final String VS_ACQUIRER_PHONE              = "1-888-288-2692";
  public static final String VS_ACQUIRER_BIN                = "433239";
  public static final String VS_STORE_NUM                   = "0001";
  public static final String VS_TERMINAL_NUMBER             = "0001";
  public static final String VS_AGENT_BIN                   = "000000";
  public static final String VS_AGENT_CHAIN_NUM             = "600055";
  public static final String VS_MERCHANT_LOCATION_NUM       = "00001";
         
  public static final String OTHER_ACQUIRER_BIN             = "433239";
  public static final String OTHER_STORE_NUM                = "0001";
  public static final String OTHER_TERMINAL_NUMBER          = "0001";
  public static final String OTHER_AGENT_BIN                = "000000";
  public static final String OTHER_AGENT_CHAIN_NUM          = "000000";
  public static final String OTHER_MERCHANT_LOCATION_NUM    = "00001";
  public static final String OTHER_COUNTRY_CODE             = "840";
  public static final String OTHER_PRIMARY_AUTH             = "1-800-370-8507";
  public static final String OTHER_SECONDARY_AUTH           = "1-877-488-0358";
  public static final String OTHER_PRIMARY_SETTLEMENT       = "1-800-411-6902";
  public static final String OTHER_SECONDARY_SETTLEMENT     = "1-877-488-0757";
  
  public static final String DBA_REPLACEMENT                = "XXXDBAXXX";
  public static final String MID_REPLACEMENT                = "XXXMIDXXX";
  public static final String CARDTYPES_REPLACEMENT          = "XXXCARDTYPESXXX";
  public static final String PHONENUM_REPLACEMENT           = "XXXPHONENUMXXX";

  public static final int    DBA_REPLACEMENT_LENGTH         = 9;     
  public static final int    MID_REPLACEMENT_LENGTH         = 9;
  public static final int    CARDTYPES_REPLACEMENT_LENGTH   = 15;
  public static final int    PHONENUM_REPLACEMENT_LENGTH    = 14;

  public static final String DEFAULT_EMAIL_FROM             = "help@merchante-solutions.com";
  public static final String DEFAULT_EMAIL_SUBJECT          = "Account Setup Form";
  public static final String DEFAULT_EMAIL_BODY             = ("Dear Internet Merchant:\n\n" +
                                                               "Thank you for choosing Merchant e-Solutions as your credit card transaction processor.  " +
                                                               "It is our pleasure to serve you.\n\n" +  
                                                               "Attached to this e-mail message is a form containing all the information necessary " +
                                                               "to complete your registration with ______________.\n\n" +
                                                               "You need to open the attachment to view the information that is necessary " +
                                                               "to complete your ____________ Registration.  If you have difficulty opening " +
                                                               "the attachment OR did not receive the attachment, please call 1-888-288-2692 for assistance.\n\n" +
                                                               "Should you have questions regarding your credit card account, web reporting or merchant statements " +
                                                               "please call us (toll-free) 1-888-288-2692 or, if it is more convenient, you can e-mail us at " +
                                                               "help@merchante-solutions.com\n\n" +
                                                               "*Note - Shipping to a foreign address may result in additional chargebacks and loss of product.  " +
                                                               "Unless you know your customer, we advise that you do not ship to a foreign address OR " +
                                                               "to an address that does not match the address of the cardholder's credit card account.\n\n" + 
                                                               "Again, thank you for giving us the opportunity to serve you!\n\n");

  public static final String DEFAULT_VSEMAIL_BODY           = ("Dear Valued Customer:\n\n" +
                                                               "Welcome to Merchant e-Solutions! Thank you for choosing us as your credit card processor.  This "   +
                                                               "message contains important information regarding your merchant account.  Please take a moment to "  +
                                                               "review your merchant account information.  FOR YOUR PROTECTION PLEASE REVIEW THE WELCOME KIT AND "  +
                                                               "THE OPERATING GUIDE VIA THE HYPERLINK BELOW FOR IMPORTANT IMFORMATION ON ADDRESS VERIFICATION "     +
                                                               "SERVICE (AVS), AND FRAUD PREVENTION.\n\n"               +
                                                               "Business Name:          "+DBA_REPLACEMENT+"\n"          +
                                                               "Merchant Number (MID):  "+MID_REPLACEMENT+"\n"          +
                                                               "Card Types Accepted:    "+CARDTYPES_REPLACEMENT+"\n\n"  +
                                                               "Partner:                merchantes (case sensitive)\n"  +
                                                               "Login:                  "+MID_REPLACEMENT+"\n"          +
                                                               "Password:               mes"+PHONENUM_REPLACEMENT+"\n\n"+
                                                               "If corrections are necessary, please contact us at 888-288-2692 or via e-mail at " +
                                                               "help@merchante-solutions.com.\n\n" +
                                                               "VeriSign Set-Up Documents\n\n" +
                                                               "We have developed a welcome kit to aid you in the set up of your VeriSign solution.  Please download and " +
                                                               "review your Welcome Kit at http://www.merchante-solutions.com/forms/vs_gateway_user_guide.pdf.  Please review and become familiar with this " +
                                                               "information, as it contains important data regarding your merchant account.  If you have difficulty "  +
                                                               "opening the attachment, please call 1-888-288-2692 for assistance.\n\n" +
                                                               "Customer Support\n\n" +
                                                               "Customer Service professionals are available by phone 24 Hours a Day, 7 Days a Week, toll-free at 888-288-2692. " +
                                                               "Please call should you require assistance with your merchant account.  You can also reach us via e-mail at " +
                                                               "help@merchante-solutions.com.\n\n" +
                                                               "Your User ID is your Merchant e-Solutions Merchant Account Number (MID - see above).  Your initial Password is the " +
                                                               "area code and phone number we have on file for your business.  (We urge you to change this Password as soon as possible.)\n\n" +
                                                               "Thank you for your business.\n\n" +
                                                               "Sincerely,\n\n" +
                                                               "Hani Hajje\n" +
                                                               "e-Commerce Manger\n" +
                                                               "Merchant e-Solutions\n\n" +
                                                               "https://www.merchante-solutions.com/jsp/secure/sLogin.jsp\n"); 
  
  private String otherTerminalId          = "";
  private String otherMerchantName        = "";    
  private String otherMerchantNum         = "";    
  private String otherCity                = "";    
  private String otherState               = "";    
  private String otherBinNum              = "";    
  private String otherAgentBin            = "";    
  private String otherChain               = "";    
  private String otherStoreNum            = "";    
  private String otherTerminalNum         = "";    
  private String otherMccSic              = "";    
  private String otherZipCode             = "";    
  private String otherCountryCode         = "";    
  private String otherLocationNum         = "";    
  private String otherTimeZoneDiff        = "";
  private String otherPrimaryAuth         = "";    
  private String otherSecondaryAuth       = "";    
  private String otherPrimarySettlement   = "";    
  private String otherSecondarySettlement = "";    
  private String otherAmexActNum          = "";    
  private String otherDiscActNum          = "";    
  private String otherDinersActNum        = "";    
  private String otherJcbActNum           = "";    
  private String vsMerchantName1          = "";    
  private String vsAcquirer               = "";    
  private String vsAcquirerContact        = "";    
  private String vsAcquirerPhone          = "";    
  private String vsMerchantAccountNum     = "";    
  private String vsAmexActNum             = "";    
  private String vsDiscActNum             = "";    
  private String vsDinersActNum           = "";    
  private String vsJcbActNum              = "";    
  private String vsAcquirerBin            = "";    
  private String vsMerchantId             = "";    
  private String vsStoreNum               = "";    
  private String vsTerminalNum            = "";    
  private String vsZipCode                = "";    
  private String vsTimeZone               = "";    
  private String vsCategory               = "";    
  private String vsMerchantName2          = "";    
  private String vsMerchantPhone          = "";    
  private String vsMerchantState          = "";    
  private String vsAgentBin               = "";    
  private String vsAgentChainNum          = "";    
  private String vsMerchantLocationNum    = "";    
  private String vsTid                    = "";    
  
  private boolean veriSign                = false;
  private boolean veriSignVA              = false;
  
  private String emailTo                  = "";
  private String emailFrom                = "";
  private String emailSubject             = "";
  private String emailBody                = "";
  private String acceptedCards            = "";



  public void setDefaults()
  {
    /*
    otherBinNum               = OTHER_ACQUIRER_BIN;    
    otherAgentBin             = OTHER_AGENT_BIN;    
    otherChain                = OTHER_AGENT_CHAIN_NUM;    
    otherStoreNum             = OTHER_STORE_NUM;    
    otherTerminalNum          = OTHER_TERMINAL_NUMBER;    
    otherCountryCode          = OTHER_COUNTRY_CODE;    
    otherLocationNum          = OTHER_MERCHANT_LOCATION_NUM;    
    otherPrimaryAuth          = OTHER_PRIMARY_AUTH;    
    otherSecondaryAuth        = OTHER_SECONDARY_AUTH;    
    otherPrimarySettlement    = OTHER_PRIMARY_SETTLEMENT;    
    otherSecondarySettlement  = OTHER_SECONDARY_SETTLEMENT;    

    vsAcquirer                = VS_ACQUIRER;    
    vsAcquirerContact         = VS_ACQUIRER_CONTACT;    
    vsAcquirerPhone           = VS_ACQUIRER_PHONE;    
    vsAcquirerBin             = VS_ACQUIRER_BIN;    
    vsStoreNum                = VS_STORE_NUM;
    vsTerminalNum             = VS_TERMINAL_NUMBER;
    vsAgentBin                = VS_AGENT_BIN;    
    vsAgentChainNum           = VS_AGENT_CHAIN_NUM;    
    vsMerchantLocationNum     = VS_MERCHANT_LOCATION_NUM;    
    */

    emailFrom                 = DEFAULT_EMAIL_FROM;
    emailSubject              = DEFAULT_EMAIL_SUBJECT;
    emailBody                 = DEFAULT_EMAIL_BODY;

    if(veriSign)
    {
      replaceInfo();
      setVerisignDefaults();
    }

  }
  
  private void setVerisignDefaults()
  {
    try
    {
      // establish subject line
      emailSubject = vsMerchantName1 + " Merchant Account Setup";
      
      StringBuffer vsBody = new StringBuffer("");

      if(isVeriSignVA())
      {
        vsBody.append("*** NOTE IMPORTANT TEXT FILES ATTACHED: Merchant Agreement, Pricing Schedule and Account Setup Form ***\n");
        vsBody.append("\n");
      }
      vsBody.append("\n");
      vsBody.append("Welcome to Merchant e-Solutions!\n");
      vsBody.append("\n");
      vsBody.append("\n");
      vsBody.append("Business Name:          ");
      vsBody.append(vsMerchantName1);
      vsBody.append("\n");
      vsBody.append("Merchant Number (MID):  ");
      vsBody.append(vsMerchantAccountNum);
      vsBody.append("\n");
      vsBody.append("Card Types Accepted:    ");
      vsBody.append(acceptedCards);
      vsBody.append("\n\n\n");
      vsBody.append("Thank you for choosing Merchant e-Solutions as your credit card processor.  This message contains important information regarding your merchant account.  Please take a few moments to review these topics:\n");
      vsBody.append("\n");
      vsBody.append("* Your Account Information (Is it correct?)\n");
      vsBody.append("\n");
      vsBody.append("* Protecting Yourself From Credit Card Fraud\n");
      vsBody.append("\n");
      if(isVeriSignVA())
      {
        vsBody.append("* Merchant Agreement (attached)\n");
        vsBody.append("\n");
      }
      vsBody.append("* Using Your Account Setup Form (VAR Form) to Activate Your VeriSign Gateway\n");
      vsBody.append("\n");
      vsBody.append("* Additional Resources from Merchant e-Solutions and VeriSign\n");
      vsBody.append("\n\n");
      vsBody.append("ACTIVATE YOUR VERISIGN GATEWAY - login to the VeriSign Manager (https://payments.verisign.com/manager) using the login and password you created for your VeriSign account, click on the 'Activate' banner and follow the steps to complete activation. You may be asked to enter the merchant account details specified in the VAR form (Account Setup Form) attached to this email. VeriSign support is available at 888-883-9770 and vps-support@verisign.com.\n");
      vsBody.append("\n\n");
      vsBody.append("PROTECT YOURSELF FROM CREDIT CARD FRAUD - Review such documents as Internet Best Practices, AVS Tutorial and VeriSign Welcome Kit - go to http://www.merchante-solutions.com/resources/productresources.htm\n");
      vsBody.append("\n\n");
      vsBody.append("USER GUIDES:\n");
      vsBody.append("- User Guide for Internet Merchants Using the VeriSign Gateway - http://www.merchante-solutions.com/forms/vs_gateway_user_guide.pdf\n");
      vsBody.append("\n");
      vsBody.append("- VeriSign Welcome Kit - http://www.merchante-solutions.com/resources/resources/res_net__verisignWelcome.htm\n");
      vsBody.append("\n");
      vsBody.append("- Merchant Account Reports - http://www.merchante-solutions.com/resources/resources/res_misc_reporting1.htm\n");
      vsBody.append("\n\n");
      vsBody.append("HELP:\n");
      vsBody.append("- Merchant e-Solutions Help Desk is available 24x7 at 888-288-2692 and help@merchante-solutions.com\n");
      vsBody.append("\n");
      vsBody.append("- VeriSign Technical Support is available Mon-Fri, 7:00am-6:00pm Pacific Time at 888-883-9770 and vps-support@verisign.com\n");
      vsBody.append("\n\n");
      vsBody.append("MONTHLY STATEMENTS - are found on the Merchant e-Solutions Web-site. To view and download reports and monthly statements, login at https://www.merchante-solutions.com/jsp/secure/sLogin.jsp. Your User ID is your Merchant e-Solutions Merchant Account Number (MID - see above).  Your initial Password is the area code and contact phone number we have on file for your business.  (We urge you to change this Password as soon as possible. Click \"Profile Change\" after you have logged in).\n");
      vsBody.append("\n\n");
      vsBody.append("INTERNATIONAL SHIPPING - AVS outside the U.S. is currently not available. Shipping to a foreign address may result in additional chargebacks and loss of product. Merchant e-Solutions strongly suggests that you do not ship to a foreign address OR to an address that does not match the address of the cardholder's credit card account. You can call Visa/MasterCard at (800) 291-4840 to verify the cardholder's address and status of the account. Choose transaction code 22 for AVS.\n");
      vsBody.append("\n\n");
      vsBody.append("If you have any questions about this email, your Merchant e-Solutions merchant account or processing procedures, please contact our 24-hour Help Desk at 888-288-2692 or help@merchante-solutions.com.\n");
      vsBody.append("\n\n");
      vsBody.append("Thank you for your business.\n");
      vsBody.append("\n");
      vsBody.append("Sincerely,\n");
      vsBody.append("\n");
      vsBody.append("The Merchant e-Solutions Team\n");
      vsBody.append("\n");
      vsBody.append("http://www.merchante-solutions.com\n");
      
      
      emailBody = vsBody.toString();
      
    }
    catch(Exception e)
    {
      logEntry("setVerisignDefaults()", e.toString());
    }
  }

  public void setFields(HttpServletRequest req)
  {
    otherTerminalId          = HttpHelper.getString(req, "otherTerminalId");
    otherMerchantName        = HttpHelper.getString(req, "otherMerchantName");
    otherMerchantNum         = HttpHelper.getString(req, "otherMerchantNum");
    otherCity                = HttpHelper.getString(req, "otherCity");
    otherState               = HttpHelper.getString(req, "otherState");
    otherBinNum              = HttpHelper.getString(req, "otherBinNum");
    otherAgentBin            = HttpHelper.getString(req, "otherAgentBin");
    otherChain               = HttpHelper.getString(req, "otherChain");
    otherStoreNum            = HttpHelper.getString(req, "otherStoreNum");
    otherTerminalNum         = HttpHelper.getString(req, "otherTerminalNum");
    otherMccSic              = HttpHelper.getString(req, "otherMccSic");
    otherZipCode             = HttpHelper.getString(req, "otherZipCode");
    otherCountryCode         = HttpHelper.getString(req, "otherCountryCode");
    otherLocationNum         = HttpHelper.getString(req, "otherLocationNum");
    otherTimeZoneDiff        = HttpHelper.getString(req, "otherTimeZoneDiff");
    otherPrimaryAuth         = HttpHelper.getString(req, "otherPrimaryAuth");
    otherSecondaryAuth       = HttpHelper.getString(req, "otherSecondaryAuth");
    otherPrimarySettlement   = HttpHelper.getString(req, "otherPrimarySettlement");
    otherSecondarySettlement = HttpHelper.getString(req, "otherSecondarySettlement");
    otherAmexActNum          = HttpHelper.getString(req, "otherAmexActNum");
    otherDiscActNum          = HttpHelper.getString(req, "otherDiscActNum");
    otherDinersActNum        = HttpHelper.getString(req, "otherDinersActNum");
    otherJcbActNum           = HttpHelper.getString(req, "otherJcbActNum");
    vsMerchantName1          = HttpHelper.getString(req, "vsMerchantName1");
    vsAcquirer               = HttpHelper.getString(req, "vsAcquirer");
    vsAcquirerContact        = HttpHelper.getString(req, "vsAcquirerContact");
    vsAcquirerPhone          = HttpHelper.getString(req, "vsAcquirerPhone");
    vsMerchantAccountNum     = HttpHelper.getString(req, "vsMerchantAccountNum");
    vsAmexActNum             = HttpHelper.getString(req, "vsAmexActNum");
    vsDiscActNum             = HttpHelper.getString(req, "vsDiscActNum");
    vsDinersActNum           = HttpHelper.getString(req, "vsDinersActNum");
    vsJcbActNum              = HttpHelper.getString(req, "vsJcbActNum");
    vsAcquirerBin            = HttpHelper.getString(req, "vsAcquirerBin");
    vsMerchantId             = HttpHelper.getString(req, "vsMerchantId");
    vsStoreNum               = HttpHelper.getString(req, "vsStoreNum");
    vsTerminalNum            = HttpHelper.getString(req, "vsTerminalNum");
    vsZipCode                = HttpHelper.getString(req, "vsZipCode");
    vsTimeZone               = HttpHelper.getString(req, "vsTimeZone");
    vsCategory               = HttpHelper.getString(req, "vsCategory");
    vsMerchantName2          = HttpHelper.getString(req, "vsMerchantName2");
    vsMerchantPhone          = HttpHelper.getString(req, "vsMerchantPhone");
    vsMerchantState          = HttpHelper.getString(req, "vsMerchantState");
    vsAgentBin               = HttpHelper.getString(req, "vsAgentBin");
    vsAgentChainNum          = HttpHelper.getString(req, "vsAgentChainNum");
    vsMerchantLocationNum    = HttpHelper.getString(req, "vsMerchantLocationNum");
    vsTid                    = HttpHelper.getString(req, "vsTid");
    veriSign                 = HttpHelper.getString(req, "veriSign").equals("true");
    veriSignVA               = HttpHelper.getString(req, "veriSignVA").equals("true");
    
    emailTo                  = HttpHelper.getString(req, "emailTo");
    emailFrom                = HttpHelper.getString(req, "emailFrom");
    emailSubject             = HttpHelper.getString(req, "emailSubject");
    emailBody                = HttpHelper.getString(req, "emailBody");
  }


  public void getData(long primaryKey)
  {
    ResultSetIterator     it                  = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:378^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    msi.*,
//                    app.APP_TYPE,
//                    merch.merch_email_address
//          from      mms_stage_info msi,
//                    application    app,
//                    merchant       merch
//          where     msi.app_seq_num = :primaryKey       and
//                    msi.app_seq_num = app.app_seq_num   and
//                    msi.app_seq_num = merch.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    msi.*,\n                  app.APP_TYPE,\n                  merch.merch_email_address\n        from      mms_stage_info msi,\n                  application    app,\n                  merchant       merch\n        where     msi.app_seq_num =  :1        and\n                  msi.app_seq_num = app.app_seq_num   and\n                  msi.app_seq_num = merch.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.VarFormBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.VarFormBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:389^7*/
      
      ResultSet rs = it.getResultSet();
    
      if(rs.next())
      {
        /*  disabling these per PRF-173
        otherTerminalId           = clearNulls(rs.getString("VNUM"));
        vsTid                     = clearNulls(rs.getString("VNUM"));    

        otherMerchantName         = clearNulls(rs.getString("BUSINESS_NAME"));    
        vsMerchantName1           = clearNulls(rs.getString("BUSINESS_NAME"));    
        vsMerchantName2           = clearNulls(rs.getString("BUSINESS_NAME"));    

        otherMerchantNum          = clearNulls(rs.getString("MERCH_NUMBER"));    
        vsMerchantId              = clearNulls(rs.getString("MERCH_NUMBER"));    
        vsMerchantAccountNum      = clearNulls(rs.getString("MERCH_NUMBER"));    
 
        otherCity                 = clearNulls(rs.getString("BUSINESS_CITY"));    
        otherState                = clearNulls(rs.getString("BUSINESS_STATE"));    
        vsMerchantState           = clearNulls(rs.getString("BUSINESS_STATE"));    
        otherZipCode              = clearNulls(rs.getString("BUSINESS_ZIP"));    
        vsZipCode                 = clearNulls(rs.getString("BUSINESS_ZIP"));    
        vsMerchantPhone           = clearNulls(rs.getString("MERCH_PHONE"));    
                                    
        otherMccSic               = clearNulls(rs.getString("MCC"));    
        vsCategory                = clearNulls(rs.getString("MCC"));    
                                     
        otherTimeZoneDiff         = clearNulls(rs.getString("TIME_ZONE"));
        vsTimeZone                = clearNulls(rs.getString("TIME_ZONE"));    
                                     
        otherAmexActNum           = clearNulls(rs.getString("AMEX"));    
        vsAmexActNum              = clearNulls(rs.getString("AMEX"));    
        otherDiscActNum           = clearNulls(rs.getString("DISC"));    
        vsDiscActNum              = clearNulls(rs.getString("DISC"));    
        otherDinersActNum         = clearNulls(rs.getString("DINR"));    
        vsDinersActNum            = clearNulls(rs.getString("DINR"));    
        otherJcbActNum            = clearNulls(rs.getString("JCB"));    
        vsJcbActNum               = clearNulls(rs.getString("JCB"));    
        */
                                  
        emailTo                   = clearNulls(rs.getString("merch_email_address"));
                                    
        veriSign                  = (rs.getInt("APP_TYPE") == mesConstants.APP_TYPE_VERISIGN || rs.getInt("APP_TYPE") == mesConstants.APP_TYPE_VERISIGN_V2);
        veriSignVA                = (rs.getInt("APP_TYPE") == mesConstants.APP_TYPE_VERISIGN_V2);
      }
    
      it.close();
      
    }
    catch(Exception e)
    {
      logEntry("getData(" + primaryKey + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
 
  private void replaceInfo()
  {
    acceptedCards = "Visa;Mastercard";

    if(!isBlank(vsAmexActNum))
    {
      if(isBlank(acceptedCards))
      {
        acceptedCards += "Amex";
      }
      else
      {
        acceptedCards += ";Amex";
      }
    }

    if(!isBlank(vsDiscActNum))
    {
      if(isBlank(acceptedCards))
      {
        acceptedCards += "Discover";
      }
      else
      {
        acceptedCards += ";Discover";
      }
    }
    
    if(!isBlank(vsDinersActNum))                
    {
      if(isBlank(acceptedCards))
      {
        acceptedCards += "Diners";
      }
      else
      {
        acceptedCards += ";Diners";
      }
    }
    
    if(!isBlank(vsJcbActNum))                   
    {
      if(isBlank(acceptedCards))
      {
        acceptedCards += "Jcb";
      }
      else
      {
        acceptedCards += ";Jcb";
      }
    }
  }

  
  public String replaceAll(String mainStr, String regexStr, String replacStr)
  {
    String part1      = "";
    String part2      = "";
    int    regIdx     = mainStr.indexOf(regexStr);
    int    regLength  = regexStr.length();

    if(regIdx > -1)
    {
      part1 = mainStr.substring(0,regIdx);
      part2 = mainStr.substring(regIdx + regLength);

      return part1 + replacStr + part2;
    }

    return mainStr;
  }

  
  public boolean isBlank(String test)
  {
    if( test == null || (test.trim()).equals("") )
    {
      return true;
    }

    return false;
  }

  public String clearNulls(String str)
  {
    if(str == null)
    {
      return "";
    }
    else
    {
      return str;
    }
  }

  public void sendEmail()
  {
    try
    {
      MailMessage mail = new MailMessage();
      mail.setFrom    (emailFrom);
      mail.addTo      (emailTo);
      mail.setSubject (emailSubject);
      mail.setText    (emailBody);
      mail.addStringAsTextFile("varform.txt", buildAttachment());
      
      if(isVeriSignVA())
      {
        mail.addStringAsTextFile("PricingSchedule.txt", buildPricingSchedule());
        mail.addStringAsTextFile("MerchantAgreement.txt", buildMerchantAgreement());
      }
      
      mail.send();
    }
    catch(Exception e)
    {
      logEntry("sendEmail", e.toString());
    }
  }
  
  private String buildPricingSchedule()
  {
    StringBuffer result = new StringBuffer("");

    result.append("Thank you for choosing Merchant e-Solutions\n");
    result.append("\n");
    result.append("\n");
    result.append("Your merchant account has been setup and is ready for your use.  Following is a full disclosure of applicable fees for your new merchant account, including monthly fees due at the end of each month.\n");
    result.append("\n");
    result.append("1. Visa & MasterCard Rates & Fees\n");
    result.append("\n");
    result.append("Discount Rate  2.35%\n");
    result.append("Transaction Fee  $.30 per item\n");
    result.append("Qualified Reward or Commercial Cards 0.45% (additional fee)\n");
    result.append("Non-Qualification Downgrade  1.75% + $0.10 (additional fee)\n");
    result.append(" \n");
    result.append("2. Other Payment Type Fees\n");
    result.append("\n");
    result.append("Discover/Novus  $.3 per item\n");
    result.append("American Express  $.3 per item\n");
    result.append("\n");
    result.append("3. Miscellaneous Fees\n");
    result.append("\n");
    result.append("Setup Fee  $0\n");
    result.append("Chargeback Fee  $20 per item\n");
    result.append("ACH Reject Fee  $15 per item\n");
    result.append("Monthly Service Fee  $15\n");
    result.append("Minimum Monthly Fee  $20\n");
    result.append("\n");
    result.append("\n");
    result.append("If you have any questions about your new merchant account, please call 1-888-288-2692.\n");
    
    return result.toString();
  }
  
  private String buildMerchantAgreement()
  {
    StringBuffer result = new StringBuffer("");
    
    result.append("Merchant e-Solutions, Inc.\n");
    result.append("\n");
    result.append("Merchant Agreement\n");
    result.append("\n");
    result.append("This agreement is between Merchant e-Solutions (\"MeS\"), a Delaware corporation, and a business (\"Merchant\") that has requested payment processing services through MeS. Merchant understands that submission of a Merchant Account Application is 1) subject to approval by MeS and, 2) signifies Merchant's acceptance of the terms of this Merchant Agreement and Pricing Schedule.\n");
    result.append("\n");
    result.append("1. Card Acceptance: Merchant agrees to accept payment options in accordance with association, network, and issuer guidelines. Merchant desires to accept the following payment options: [ ] Credit/Business Cards, [ ] Consumer Debit/Prepaid Cards, [ ] Both. If an option is not specified, MeS will assume Merchant desires to accept Both. In the event Merchant desires not to accept Credit/Business Cards or Debit/Prepaid Cards, Merchant is responsible for examining cards and indicating non-acceptance to cardholders as appropriate. If Merchant desires not to accept Credit/Business Cards or Debit/Prepaid Cards but then submits those payment types for transaction processing, the transactions will be subject to appropriate interchange and other normal transaction fees. \n");
    result.append("\n");
    result.append("2. Payment Processing: Merchant agrees to process payments as instructed in operating cards and guides provided by MeS in conjunction with payment processing solutions. \n");
    result.append("\n");
    result.append("3. Payment Solutions: Merchant agrees to use the payment processing solution for which the merchant has applied and to notify MeS of any changes, including the addition or deletion of payment options and the addition or deletion of terminals or other equipment. If Merchant uses a payment solution not provided by MeS, Merchant is solely responsible for all risks associated with using such solution. \n");
    result.append("\n");
    result.append("4. Card Verification: Merchant agrees to follow proper card acceptance procedures: \n");
    result.append("\n");
    result.append("For Merchants, described as merchants who operate their business from a retail establishment, procedures include an examination of a card to verify the presence of appropriate card marks, cardholder signature, and valid start and expiration dates. \n");
    result.append("\n");
    result.append("For Internet or MOTO Merchants, described as merchants who provide goods and services through the Internet or through Mail Order or Telephone Order (MOTO), the use of AVS (Address Verification Service) is required for all transactions processed. \n");
    result.append("\n");
    result.append("5. Card Present/Not Present: Merchant understands the card must be present at the time of a transaction in order for the merchant to perform the transaction. Merchant can process transactions in which the card is not present (for example, telephone, mail order, internet sales) only: \n");
    result.append("\n");
    result.append("For Merchants, if the Merchant has notified MeS prior to processing such transactions. \n");
    result.append("\n");
    result.append("For Internet or MOTO Merchants: if the Merchant operates in a Card Not Present environment they must have previously notified MeS of this environment. Failure to use AVS may result in higher transaction fees and may result in cancellation of your merchant account should you experience an increase in fraudulent transactions. \n");
    result.append("\n");
    result.append("Merchant understands that when a card is not present at the time of a transaction, a higher degree of risk exists that can result in higher transaction fees or possible chargebacks. \n");
    result.append("\n");
    result.append("6. Authorization: Merchant understands that an authorization must be obtained for each transaction, regardless of dollar amount, and included on the sales receipt. \n");
    result.append("\n");
    result.append("In the event of the failure of electronic point-of-sale equipment for Merchants, or failure of payment gateway for the Internet merchant, Merchant agrees to obtain voice authorizations as instructed in operating cards and guides provided by MeS. \n");
    result.append("\n");
    result.append("Merchant understands that an authorization is not a guarantee of payment, but an indication that at the time of the authorization, the card account is open and has available funds. \n");
    result.append("\n");
    result.append("Merchant agrees not to accept a card or submit a transaction for settlement when authorization is denied. \n");
    result.append("\n");
    result.append("Merchant will not try to gain an authorization by splitting the amount of a transaction into smaller, separate amounts. \n");
    result.append("\n");
    result.append("7. Sales Receipts: If the Merchant uses an electronic printer connected to an electronic point-of-sale device, Merchant must obtain the cardholder's signature on the printed sales receipt. \n");
    result.append("\n");
    result.append("Printed receipts must include the following information: 1) Merchant name, 2) Type of transaction, 3) Value of the transaction, 4) Date of the transaction, 5) Terminal location where the transaction occurred, 6) Account number of the cardholder, and 7) Authorization number. \n");
    result.append("\n");
    result.append("If Merchant uses an electronic printer but keys the card number into the terminal, Merchant must: 1) Obtain the cardholder's signature on the printer receipt, 2) Imprint the card on a sales draft using an imprinter and MeS supplied imprinter plate, 3) Complete the entire imprinted sales draft, 4) Obtain the cardholder's signature on the imprinted sales draft. \n");
    result.append("\n");
    result.append("The Internet Merchant must provide customers a sales confirmation through e-mail. Sales confirmation must include the following: 1) Merchant name, 2) Type of transaction, 3) Description of the items purchased and their value, 4) Date of the transaction and date shipped, 5), Truncated Account number of the cardholder, and 6) Authorization number. \n");
    result.append("\n");
    result.append("In all cases, Merchant is responsible for ensuring the legibility and completeness of sales receipts and sales confirmations. \n");
    result.append("\n");
    result.append("8. Back-Up Procedures: When Merchant is unable to obtain electronic authorization due to malfunctioning equipment, communication interruption or other circumstances, Merchant shall handle authorizations in accordance with back-up procedures outlined in the operating guide. All non-bankcard authorizations shall be handled in accordance with the guidelines of the issuing companies of those cards. \n");
    result.append("\n");
    result.append("9. Cardholder Signatures: A Merchant is responsible for obtaining the cardholder's signature on each sales receipt and verifying the signature on the sales receipt matches the signature on the card. \n");
    result.append("\n");
    result.append("Cardholder signatures are required on all sales receipts unless Merchant has arranged with MeS to process transactions in which cards are not present. \n");
    result.append("\n");
    result.append("If Merchant is presented with an unsigned card, Merchant shall request further identification for verification. \n");
    result.append("\n");
    result.append("For Internet or MOTO Merchants, cardholder signatures are not applicable. \n");
    result.append("\n");
    result.append("10. Returns: Merchant agrees to clearly disclose Merchant's return policy to cardholders and to honor the return policy as disclosed. \n");
    result.append("\n");
    result.append("For Merchants, clear disclosure consists of displaying applicable refund options on sales receipts near the signature area and in type at least 1/4 inch high. \n");
    result.append("\n");
    result.append("For Internet Merchants, the return policy must be clearly displayed at the merchant's internet-based store. \n");
    result.append("\n");
    result.append("Transactions fulfilled as a telephone order cannot be protected by any restrictive return policies, as proper written disclosure cannot be made at the time of purchase. \n");
    result.append("\n");
    result.append("In the case of returns in which Merchant extends a refund for purchases made with a charge card, Merchant shall not make any cash refunds but process a credit transaction according to instructions contained in operating cards and guides provided by MeS. \n");
    result.append("\n");
    result.append("Merchants must complete a credit voucher signed by the cardholder at the time of a return. Internet Merchants must complete a credit voucher and email them to the cardholder at the time of a return. \n");
    result.append("\n");
    result.append("Merchant understands that when credits are issued, Merchant's designated business checking account is debited for the credit amounts. \n");
    result.append("\n");
    result.append("Per item transaction fees apply to credits as well as purchases. \n");
    result.append("\n");
    result.append("11. Settlement: Merchant agrees to balance and submit transactions for settlement each business day. Merchant understands that by not submitting transactions for settlement each business day, a higher degree of risk exists that can result in higher transaction fees and possible chargebacks. \n");
    result.append("\n");
    result.append("12. Payments: All payments to Merchant for authorized transactions shall be made through the Automated Clearing House (ACH) and to the business checking account designated by Merchant in the Merchant Account Application. \n");
    result.append("\n");
    result.append("The time between the day of a transaction and payment to Merchant is dependent on when transactions are submitted and processed for settlement, when payments are processed by the ACH, and when Merchant's bank receives and processes payments from the ACH. \n");
    result.append("\n");
    result.append("Payments owed Merchant may be made in full, less discount, credits, chargebacks, fees, and other adjustments. \n");
    result.append("\n");
    result.append("Payments owed to MeS may be deducted from incoming transactions or debited against Merchant's designated business checking account at a time determined by MeS. \n");
    result.append("\n");
    result.append("13. Business Checking: Merchant authorizes MeS, in accordance with this agreement, or other obligations owed to MeS, or its agents, to initiate credit and debit entries to Merchant's business checking account, or any other account maintained by Merchant at any financial institution that receives ACH files. This authority shall remain in effect until MeS has received written notification from Merchant of Merchant's termination of this agreement, Merchant has satisfied all payments owed to MeS, and all transactions submitted by Merchant have exceeded dispute and chargeback time limitations. \n");
    result.append("\n");
    result.append("14. Sales Receipt Storage: Merchant agrees to store and maintain sales receipts for at least 2 years following the date of transaction. At the end of this period, Merchant shall destroy the records, leaving no legible information. \n");
    result.append("\n");
    result.append("15. Equipment: Merchant must possess appropriate equipment for processing transactions in accordance with their merchant account, i.e. properly installed and programmed terminals, magnetic stripe readers, PIN pads, printers, software providing access to Internet Payment Gateways, etc. \n");
    result.append("\n");
    result.append("16. MeS-Provided Equipment: Upon Merchant's request, MeS shall provide terminals and related equipment to Merchant for a fee. MeS shall be responsible for the maintenance of MeS provided equipment provided Merchant pays for all maintenance or repairs required as a result of Merchant's negligence or misuse of equipment. Merchant shall notify MeS immediately concerning any noticeable malfunction of or damage to the equipment. Equipment is the sole property of MeS and Merchant has no interest or property rights. \n");
    result.append("\n");
    result.append("17. Retrieval Request: Merchant agrees to mail or fax copies of sales receipts to MeS within 24 hours of receiving a retrieval request from MeS. Merchant understands that failure to respond to a retrieval request within the time period with a copy of the transaction and proof of delivery to MeS shall constitute a waiver of all rights of Merchant to dispute the chargeback. This may result in higher transaction fees and termination by MeS of Merchant's account. For Merchants the sales draft must be legible, accurate, complete, and signed by the cardholder. For Internet Merchants a facsimile of the e-mail sent to the cardholder must be provided. \n");
    result.append("\n");
    result.append("18. Chargebacks: Merchant agrees to pay MeS for transactions successfully challenged by cardholders or card issuers in accordance with association guidelines relating to chargebacks and to permit MeS to debit Merchant's designated business checking account or other account as a result of chargebacks. \n");
    result.append("\n");
    result.append("Chargebacks can be caused by, but not limited to: 1) Illegible or incomplete sales drafts, 2) Duplicate sales drafts from the same transaction, 3) Cardholder disputes regarding the product, service, or price, 4) Merchant violations of association agreements, 5) Unauthorized transactions, 6) Transactions performed with counterfeit, altered, or expired cards. \n");
    result.append("\n");
    result.append("Merchant authorizes MeS at its discretion to establish a reserve account held by MeS with an amount determined by MeS to be used to offset chargebacks. \n");
    result.append("\n");
    result.append("MeS has the right to increase or decrease the amount of the reserve account depending on Merchant's chargeback activity or other risk factors determined by MeS. In the event of termination of Merchant's account by Merchant or MeS, MeS has the right to have a reserve account remain in effect for Merchant for at least 180 days following the date of termination of Merchant's account. \n");
    result.append("\n");
    result.append("If Merchant owes MeS amounts under the terms of this agreement and fails to pay in full within 24 hours of a request by MeS, MeS has the right to consider amounts as uncollected and can initiate collection procedures, which may include the use of outside collection agents. \n");
    result.append("\n");
    result.append("Merchant shall be responsible for any expenses incurred by MeS or its agents in an effort to collect amounts owed by Merchant. \n");
    result.append("\n");
    result.append("In the event that MeS incurs fines from the associations due to excessive chargebacks, Merchant is responsible for such fines and authorizes MeS to debit merchant's designated business checking account accordingly. \n");
    result.append("\n");
    result.append("19. Merchant Warranties: Merchant warrants and agrees to fully comply with all federal, state, and local laws, rules and regulations, as amended periodically, including the Federal Truth-in-Lending Act and Regulation Z. \n");
    result.append("\n");
    result.append("Merchant warrants that each transaction presented by Merchant to MeS for payment: \n");
    result.append("Is a valid transaction completed in accordance with association and network regulations and according to instructions provided in operating cards and guides provided by MeS. \n");
    result.append("\n");
    result.append("Is submitted following the delivery of merchandise or services to cardholders as described in the sales receipt. \n");
    result.append("\n");
    result.append("Was completed by Merchant identified in the Merchant Account Application and no other entity. \n");
    result.append("\n");
    result.append("Has not imposed, directly or indirectly, separate or additional fees or surcharges to the cardholder. \n");
    result.append("\n");
    result.append("Is not subject to any lien. \n");
    result.append("\n");
    result.append("Merchant warrants that: \n");
    result.append("\n");
    result.append("To the best of Merchant's knowledge, the cardholder involved in a transaction has no reason to dispute the transaction. \n");
    result.append("\n");
    result.append("In the event of a cardholder or card issuer dispute, Merchant shall hold MeS harmless. \n");
    result.append("Merchant warrants and agrees that without the cardholder's written consent, Merchant shall not sell, purchase, exchange, or disclose a cardholder's account number or other information to any third party for any reason other than to Merchant's agents for the purpose of assisting Merchant in the delivery of merchandise or services as part of the transaction, or to MeS, the card associations or networks, or pursuant to a government request. \n");
    result.append("\n");
    result.append("20. Fraud/Factoring: Merchant shall not accept, deposit, process, or enter into Merchant's terminal, a fraudulent sale, or any sale made by any other merchant. If Merchant does so, MeS can immediately terminate Merchant's account, place payments owed to Merchant on hold for at least 180 days, and add Merchant to the Terminated Merchant File. This action may result in preventing the Merchant from accepting card payments again or establishing another merchant account. \n");
    result.append("\n");
    result.append("21. Payment Withholding: In the event MeS believes fraud by Merchant has occurred or Merchant fails to provide funds for debits due to chargebacks, credits, or fees, MeS may hold all payments owed to Merchant for submitted transactions until Merchant fulfills financial obligations to MeS. \n");
    result.append("\n");
    result.append("Merchant understands that payments to Merchant's account that result from submitted transactions are provisional and may be debited from Merchant's account in accordance with association and network rules. \n");
    result.append("\n");
    result.append("If Merchant ceases to do business and does not claim funds held under this provision within 180 days, MeS may retain these funds. \n");
    result.append("22. Terminated Merchant File: Merchant understands that MeS can add Merchant to the Terminated Merchant File if Merchant fails to comply with the terms of this Agreement or the provisions of the association rules. \n");
    result.append("\n");
    result.append("23. Discount/Fees: \"Discount\" refers to a percentage of the gross transactions processed by Merchant. \"Interchange\" refers to amounts assessed by the associations for the processing of transactions. \"Fees\" refers to amounts charged for any other purposes, including per transaction fees, chargeback fees, equipment use fees, or fees for other miscellaneous services. \n");
    result.append("\n");
    result.append("Amounts owed by Merchant to cover discount, interchange, or fees, are identified in a Pricing Schedule provided in conjunction with this Agreement. \n");
    result.append("\n");
    result.append("MeS has the right to modify the amounts as necessary to offset any increase in costs experienced by MeS due to: association or network changes in rules, regulations, or operating procedures; any additional requirements imposed by federal or state government agencies or regulatory authorities; increases in telecommunication or other operating costs; or other increases in costs associated with providing services to Merchant under the terms of this Agreement. \n");
    result.append("\n");
    result.append("Merchant agrees to pay the fees and charges identified in the Pricing Schedule, incorporated herein, and understands the Pricing Schedule may be modified, amended, or supplemented in accordance with this Agreement. \n");
    result.append("\n");
    result.append("Merchant understands that MeS agrees to provide at least 30 days advanced written notice to Merchant in the event of changes to the Pricing Schedule. \n");
    result.append("\n");
    result.append("24. Initial Discount: Merchant understands that initial discount rates assessed by MeS are based on Merchant's projected sales volume, average transaction amount, and card acceptance practices. If Merchant's actual sales volume and average transaction amount are less than Merchant's projected sales volume and average transaction amount, MeS reserves the right to adjust discount rates to reflect Merchant's actual sales volume and average transaction amount without 30 days advanced written notice. \n");
    result.append("\n");
    result.append("25. Merchant Account Application: Merchant warrants that all information provided in the Merchant Account Application is complete and accurate. \n");
    result.append("\n");
    result.append("Merchant understands that if MeS finds information provided in the Merchant Account Application to be inaccurate, MeS has the right to immediately terminate Merchant's account. \n");
    result.append("\n");
    result.append("Merchant understands that discount rates, interchange, and other fees are based on sales volumes and average ticket identified by merchant in the Merchant Account Application. \n");
    result.append("\n");
    result.append("Should Merchant's actual sales volumes and average ticket vary from information provided in the Merchant Account Application, Merchant agrees that MeS can adjust discount, interchange, and fees accordingly, without prior notice to Merchant. \n");
    result.append("\n");
    result.append("26. Billing: Merchant must notify MeS of any billing error within 60 days of the billing date. \n");
    result.append("\n");
    result.append("27. Term: The initial term of this Agreement shall be for one year, subject to approval by MeS, and shall renew for each successive one-year term unless either party provides the other with written notice prior to the expiration of the current term. \n");
    result.append("\n");
    result.append("28. Termination: If Merchant terminates this Agreement before the end of the initial one-year term, MeS may charge merchant a $100 early termination fee. Upon termination of this Agreement, Merchant agrees to pay all amounts owed to MeS by Merchant, including the early termination fee if warranted. MeS can debit amounts owed to MeS by Merchant from Merchant's business checking account. MeS shall have the right to terminate this Agreement at any time without cause. \n");
    result.append("\n");
    result.append("29. Indemnity: Merchant agrees to indemnify and hold harmless MeS from any claims, damages, costs, fees, and expenses, including reasonable attorneys' fees and expenses arising from: \n");
    result.append("\n");
    result.append("Merchant's provision, failure to provide, or alleged failure to provide, goods or services to cardholders. \n");
    result.append("\n");
    result.append("Merchant's breach of terms or warranties set forth in this Agreement. \n");
    result.append("\n");
    result.append("Any action by any federal or state agency, authority, or regulatory body involving Merchant. \n");
    result.append("\n");
    result.append("Any claim for funds owed by Merchant. Merchant acknowledges liability for the actions, or failure to act, of Merchant's employees and agents in regard to this Agreement. \n");
    result.append("\n");
    result.append("30. Force Majeure: Both parties will be released from liability if unable to perform as specified due to wars, riots, acts of God, etc. MeS liability to Merchant shall not exceed the amount of the sales draft and MeS shall not be liable for any incidental or consequential damages. MeS accepts no responsibility other than authorization and electronic capture services for non-bankcards. For check authorization, validation, or guarantee service, MeS accepts no responsibility other than programming the electronic equipment to connect Merchant to third-party vendor. \n");
    result.append("\n");
    result.append("31. Notices: Notices to Merchant will be sent to the same address provided by Merchant for the delivery of billing statements or other communications. \n");
    result.append("\n");
    result.append("Merchant shall notify MeS in writing at least 30 calendar days prior to any change in Merchant's name or location. \n");
    result.append("Notices to MeS must be written and will be deemed received when delivered in person or by other means providing a record of receipt from the U.S. Postal Service or other express mail or Messenger service. \n");
    result.append("\n");
    result.append("Notices to MeS must be sent to: Credit Operations, 920 North Argonne Road, Suite 200, Spokane, WA 99212. \n");
    result.append("\n");
    result.append("32. Arbitration: Any controversy or claim between or among the parties hereto will be determined by binding arbitration in accordance with the Federal Arbitration Act, applicable state law, and an arbitration administrator determined by MeS. Judgment on any arbitration award may be entered in any court having jurisdiction. Any party to this Agreement may bring an action, including a summary or expedited proceeding, to compel arbitration of any controversy or claim to which this Agreement applies in any court having jurisdiction over such action in the state of California. \n");
    result.append("\n");
    result.append("33. Assignment: Merchant may not assign any of its rights or delegate any of its obligations under this Agreement without the prior written permission of MeS. Any such assignment or delegation does not limit Merchant's obligations, indemnities, or liability to MeS. This Agreement is binding upon the parties hereto and to their respective successors and assigns. \n");
    result.append("\n");
    result.append("34. Amendment: MeS has the right to amend any of the terms of this Agreement provided MeS provides written notice to Merchant at least 30 calendar days prior to the effective date of the amendment. Continuing to process transactions constitutes Merchant's acceptance of amendments. \n");
    result.append("\n");
    result.append("35. Attorney's Fees: If a legal or arbitration proceeding is commenced in connection with any dispute under this Agreement, the prevailing party, as determined by the court or arbitrators, will be entitled to recover from the other attorneys' fees, costs, and in-house expenses incurred in connection with such action or proceeding. \n");
    result.append("\n");
    result.append("36. Governing Law: This Agreement shall be governed by the laws of the State of California and shall, in addition, be subject to the by-laws and operating regulations of the card associations and networks. Merchant shall indemnify and hold MeS harmless for any costs, fees, or expenses, which MeS may incur in enforcing its rights hereunder. \n");
    result.append("\n");
    result.append("37. Miscellaneous: If any court finds any portion of this Agreement invalid or unenforceable, the remaining provisions shall remain in force. \n");
    result.append("\n");
    result.append("38. Guarantee: The owners or officers, as indicated in the Merchant Account Application, individually and collectively agree to guarantee to MeS, its agents and successors, the prompt and complete payment of all debts and obligations that result from the establishment of an account under the terms of this Agreement. The guarantee will remain in effect until all said debts and obligations are paid in full, not withstanding the termination or amendment of this Agreement. \n");
    result.append("\n");
    result.append("American Express(c), Discover(c), Diners Club(c), Carte Blanche(c), and JCB(c) require separate approval. Merchant e-Solutions is a registered service provider for Columbus Bank and Trust, Columbus, GA. Debit network sponsorship is through Carrollton Bank, Baltimore, MD.\n");
    
    return result.toString();

  }
  

  private String buildAttachment()
  {
    String result = "";

    if(isVeriSign())
    {
      result = "\nMerchant e-Solutions\nP.O. Box 13305, Spokane, WA  99213-3305\n\n\n";
      result += "VeriSign Merchant Account Setup Form\nPlease choose VITAL as your processor\n\n";
      result += "Merchant Name      \t\t" + getVsMerchantName1()       + "\n";
      result += "Acquirer           \t\t" + getVsAcquirer()            + "\n";
      result += "Acquirer Contact   \t\t" + getVsAcquirerContact()     + "\n";
      result += "Acquirer Phone     \t\t" + getVsAcquirerPhone()       + "\n";
      result += "Merchant Account # \t\t" + getVsMerchantAccountNum()  + "\n\n";

      result += "  Additional Card Types: Choose VITAL as your processor\n\n";

      result += "American Express   \t\t" + getVsAmexActNum()          + "\n";
      result += "Discover           \t\t" + getVsDiscActNum()          + "\n";
      result += "Diners             \t\t" + getVsDinersActNum()        + "\n";
      result += "JCB                \t\t" + getVsJcbActNum()           + "\n";
      result += "Visa/Mastercard    \t\t" + "Choose VITAL as your processor\n\n";
      
      result += "      VeriSign Specific Information:\n\n";

      result += "Acquirer Bin       \t\t" + getVsAcquirerBin()         + "\n";
      result += "Merchant ID        \t\t" + getVsMerchantId()          + "\n";
      result += "Store Number       \t\t" + getVsStoreNum()            + "\n";
      result += "Terminal Number    \t\t" + getVsTerminalNum()         + "\n";
      result += "Zip Code           \t\t" + getVsZipCode()             + "\n";
      result += "Time Zone          \t\t" + getVsTimeZone()            + "\n";
      result += "Category           \t\t" + getVsCategory()            + "\n";
      result += "Merchant Name      \t\t" + getVsMerchantName2()       + "\n";
      result += "Merchant Phone     \t\t" + getVsMerchantPhone()       + "\n";
      result += "Merchant State     \t\t" + getVsMerchantState()       + "\n";
      result += "Agent Bin          \t\t" + getVsAgentBin()            + "\n";
      result += "Agent Chain Number \t\t" + getVsAgentChainNum()       + "\n";
      result += "Merchant Location #\t\t" + getVsMerchantLocationNum() + "\n";
      result += "TID (V#)           \t\t" + getVsTid()                 + "\n";
    }
    else
    {
      result = "\nMerchant e-Solutions\n920 N. Argonne Rd. Suite 200 Spokane WA, 99212\n\n\n";
      result += "VITAL PROPRIETARY SOFTWARE MERCHANT\n     CONFIGURATION INFORMATION\n\n";

      result += "Terminal ID            \t\t" + getOtherTerminalId()           + "\n";
      result += "Merchant Name          \t\t" + getOtherMerchantName()         + "\n";
      result += "Merchant Number        \t\t" + getOtherMerchantNum()          + "\n";
      result += "City                   \t\t" + getOtherCity()                 + "\n";
      result += "State                  \t\t" + getOtherState()                + "\n";
      result += "BIN Number             \t\t" + getOtherBinNum()               + "\n";
      result += "Agent                  \t\t" + getOtherAgentBin()             + "\n";
      result += "Chain                  \t\t" + getOtherChain()                + "\n";
      result += "Store Number           \t\t" + getOtherStoreNum()             + "\n";
      result += "Terminal Number        \t\t" + getOtherTerminalNum()          + "\n";
      result += "MCC/SIC                \t\t" + getOtherMccSic()               + "\n";
      result += "Zip Code               \t\t" + getOtherZipCode()              + "\n";
      result += "Country Code           \t\t" + getOtherCountryCode()          + "\n";
      result += "Location Number        \t\t" + getOtherLocationNum()          + "\n";
      result += "Time Zone Difference   \t\t" + getOtherTimeZoneDiff()         + "\n";
      result += "Primary Authorization  \t\t" + getOtherPrimaryAuth()          + "\n";
      result += "Secondary Authorization\t\t" + getOtherSecondaryAuth()        + "\n";
      result += "Primary Settlement     \t\t" + getOtherPrimarySettlement()    + "\n";
      result += "Secondary Settlement   \t\t" + getOtherSecondarySettlement()  + "\n";
      result += "American Express       \t\t" + getOtherAmexActNum()           + "\n";
      result += "Discover               \t\t" + getOtherDiscActNum()           + "\n";
      result += "Diners #               \t\t" + getOtherDinersActNum()         + "\n";
      result += "JCB                    \t\t" + getOtherJcbActNum()            + "\n";
    }

    return result;

  }


  public String getOtherTerminalId()          { return otherTerminalId;           }
  public String getOtherMerchantName()        { return otherMerchantName;         }
  public String getOtherMerchantNum()         { return otherMerchantNum;          }
  public String getOtherCity()                { return otherCity;                 }
  public String getOtherState()               { return otherState;                }
  public String getOtherBinNum()              { return otherBinNum;               }    
  public String getOtherAgentBin()            { return otherAgentBin;             }
  public String getOtherChain()               { return otherChain;                }
  public String getOtherStoreNum()            { return otherStoreNum;             }
  public String getOtherTerminalNum()         { return otherTerminalNum;          }
  public String getOtherMccSic()              { return otherMccSic;               }
  public String getOtherZipCode()             { return otherZipCode;              }
  public String getOtherCountryCode()         { return otherCountryCode;          }
  public String getOtherLocationNum()         { return otherLocationNum;          }  
  public String getOtherTimeZoneDiff()        { return otherTimeZoneDiff;         }
  public String getOtherPrimaryAuth()         { return otherPrimaryAuth;          }  
  public String getOtherSecondaryAuth()       { return otherSecondaryAuth;        }    
  public String getOtherPrimarySettlement()   { return otherPrimarySettlement;    } 
  public String getOtherSecondarySettlement() { return otherSecondarySettlement;  }   
  public String getOtherAmexActNum()          { return otherAmexActNum;           }
  public String getOtherDiscActNum()          { return otherDiscActNum;           }
  public String getOtherDinersActNum()        { return otherDinersActNum;         }   
  public String getOtherJcbActNum()           { return otherJcbActNum;            }

  public String getVsMerchantName1()          { return vsMerchantName1;           }
  public String getVsAcquirer()               { return vsAcquirer;                }
  public String getVsAcquirerContact()        { return vsAcquirerContact;         }
  public String getVsAcquirerPhone()          { return vsAcquirerPhone;           }
  public String getVsMerchantAccountNum()     { return vsMerchantAccountNum;      }
  public String getVsAmexActNum()             { return vsAmexActNum;              }
  public String getVsDiscActNum()             { return vsDiscActNum;              }
  public String getVsDinersActNum()           { return vsDinersActNum;            }
  public String getVsJcbActNum()              { return vsJcbActNum;               }
  public String getVsAcquirerBin()            { return vsAcquirerBin;             }
  public String getVsMerchantId()             { return vsMerchantId;              }
  public String getVsStoreNum()               { return vsStoreNum;                }
  public String getVsTerminalNum()            { return vsTerminalNum;             }
  public String getVsZipCode()                { return vsZipCode;                 }
  public String getVsTimeZone()               { return vsTimeZone;                }
  public String getVsCategory()               { return vsCategory;                }
  public String getVsMerchantName2()          { return vsMerchantName2;           }
  public String getVsMerchantPhone()          { return vsMerchantPhone;           }
  public String getVsMerchantState()          { return vsMerchantState;           }
  public String getVsAgentBin()               { return vsAgentBin;                }
  public String getVsAgentChainNum()          { return vsAgentChainNum;           }
  public String getVsMerchantLocationNum()    { return vsMerchantLocationNum;     }
  public String getVsTid()                    { return vsTid;                     }

  public boolean isVeriSign()                 { return veriSign;                  }
  public boolean isVeriSignVA()               { return veriSignVA;                }

  public String getEmailTo()                  { return emailTo;                   }
  public String getEmailFrom()                { return emailFrom;                 }
  public String getEmailSubject()             { return emailSubject;              }
  public String getEmailBody()                { return emailBody;                 }

}/*@lineinfo:generated-code*/