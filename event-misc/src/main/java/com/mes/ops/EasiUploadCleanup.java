package com.mes.ops;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.mes.database.SQLJConnectionBase;



public class EasiUploadCleanup extends SQLJConnectionBase
{
  
  private final static String FILE_DIR                = "C:\\Program Files\\Symantec\\Procomm Plus\\Upload";
  private final static String FILE_NAME_9966          = "DISCOVER_TAPEFILE_9966.txt";
  private final static String FILE_NAME_9968          = "DISCOVER_TAPEFILE_9968.txt";
  private final static String FILE_NAME_9849          = "DISCOVER_TAPEFILE_9849.txt";

  private final static String ARCHIVE_DIR_9966        = "C:\\Program Files\\Symantec\\Procomm Plus\\Upload\\Production Discover Files 9966\\";
  private final static String ARCHIVE_DIR_9968        = "C:\\Program Files\\Symantec\\Procomm Plus\\Upload\\Production Discover Files 9968\\";
  private final static String ARCHIVE_DIR_9849        = "C:\\Program Files\\Symantec\\Procomm Plus\\Upload\\Production Discover Files 9849\\";

  private final static String ARCHIVE_FILE_NAME_9966  = "DISCOVER_TAPEFILE_9966_";
  private final static String ARCHIVE_FILE_NAME_9968  = "DISCOVER_TAPEFILE_9968_";
  private final static String ARCHIVE_FILE_NAME_9849  = "DISCOVER_TAPEFILE_9849_";

  private String dateString = "";
  private String timeString = "";

  public EasiUploadCleanup()
  {
    super(SQLJConnectionBase.getDirectConnectString());
    
    // get current date and time
    Calendar   cal  = Calendar.getInstance();
    dateString = (new SimpleDateFormat("MMddyy")).format(cal.getTime());
    timeString = (new SimpleDateFormat("HHmm")).format(cal.getTime());
  }


  public void run()
  {
    File   cDir             = null;
    File   fileSrc9966      = null;
    File   fileSrc9968      = null;
    File   fileSrc9849      = null;

    String archiveFileName9966  = ARCHIVE_DIR_9966 + ARCHIVE_FILE_NAME_9966 + dateString + "_" + timeString + ".txt";
    String archiveFileName9968  = ARCHIVE_DIR_9968 + ARCHIVE_FILE_NAME_9968 + dateString + "_" + timeString + ".txt";
    String archiveFileName9849  = ARCHIVE_DIR_9849 + ARCHIVE_FILE_NAME_9849 + dateString + "_" + timeString + ".txt";

    try
    {
      connect();

      cDir        = new File(FILE_DIR);
      fileSrc9966 = new File(cDir, FILE_NAME_9966);
      fileSrc9968 = new File(cDir, FILE_NAME_9968);
      fileSrc9849 = new File(cDir, FILE_NAME_9849);
          
      if(fileSrc9966.exists())
      {
        fileSrc9966.renameTo(new File(archiveFileName9966));
      }
          
      if(fileSrc9968.exists())
      {
        fileSrc9968.renameTo(new File(archiveFileName9968));
      }
    
      if(fileSrc9849.exists())
      {
        fileSrc9849.renameTo(new File(archiveFileName9849));
      }
    }
    catch (Exception e)
    {
      System.out.println("Exception: " + e.toString());
    }
    finally
    {
      cleanUp();
    }

  }
  
  public static void main(String[] args)
  {
    try
    {
      EasiUploadCleanup engine = new EasiUploadCleanup();
      engine.run();
    }
    catch(Exception e)
    {
      System.out.println("ERROR: " + e.toString());
    }
    finally
    {
    }
  }
}
