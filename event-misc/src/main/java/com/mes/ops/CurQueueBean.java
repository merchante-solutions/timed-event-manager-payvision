/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/CurQueueBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 11 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.io.File;
import java.io.Serializable;

public class CurQueueBean
  implements Serializable
{
  // class fields
  private int     queueType     = 0;
  private int     queueStage    = 0;
  private long    controlNumber = 0L;
  private long    appSeqNum     = 0L;
  private String  lockedBy;
  private boolean research      = false;

  /**
   * isValidState()
   * 
   * Is the current state of this bean valid?
   *  I.e. Has the appSeqNum and queueType been set?
   * 
  */
  public synchronized boolean isValidState()
  {
    return (appSeqNum>0L && queueType>0);
  }

  // lock
  public synchronized boolean getLock(String lockedBy)
  {
    boolean         lockSuccess = false;
    QueueBean       qb = null;
    File            lockFile = null;
    
    // make the lock filename
    StringBuffer lockFilename = new StringBuffer("");
    try
    {
      // get a new QueueBean to do the database work
      qb = new QueueBean("getLock");
      
      // create a lockfile
      lockFilename.append(Long.toString(this.controlNumber));
      lockFilename.append("_" + Integer.toString(this.queueType));
      lockFilename.append(".lck");
      
      lockFile = new File(lockFilename.toString());
      lockFile.deleteOnExit();
      
      if(! qb.isAlreadyLocked(appSeqNum, queueType, lockedBy) && lockFile.createNewFile())
      {
        //  update the queue
        if(qb.setLock(appSeqNum, queueType, lockedBy))
        {
          lockSuccess = true;
        }
        else
        {
          // some database problem
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getLock: Couldn't set lock");
        }
      }
      
      // get locked information for display purposes
      setLockedBy(qb.getLockInfo(appSeqNum));
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(1, this.getClass().getName(), "getLock: " + e.toString());
    }
    finally
    {
      if(qb != null)
      {
        qb.cleanUp("getLock");
      }
      
      if(lockFile != null)
      {
        lockFile.delete();
      }
    }
    
    return lockSuccess;
  }
  
  public synchronized void recordQueueChange(String user, String message, int newStatus, int queueType)
  {
    QueueBean                     qb = null;
    StringBuffer                  qs = new StringBuffer("");
    java.sql.PreparedStatement    ps    = null;
    
    try
    {
      qb = new QueueBean("recordQueueChange");
        
      // first update the app_queue table to show the last user
      qs.append("update app_queue ");
      qs.append("set    app_last_user = ?, ");
      qs.append("       app_last_date = sysdate ");
      qs.append("where  app_seq_num = ? and ");
      qs.append("       app_queue_type = ?");
      
      ps = qb.getPreparedStatement(qs.toString());
      ps.setString(1, user);
      ps.setLong(2, getAppSeqNum());
      ps.setInt(3, queueType);
      
      ps.executeUpdate();
        
      // now add an entry to the history table to show the change
      ps.close();
      ps = null;
      qs.setLength(0);
      
      qs.append("insert into app_queue_history ");
      qs.append("(app_seq_num, app_queue_stage, app_queue_process, app_queue_user, app_queue_date, app_status_new, app_change_reason) ");
      qs.append("values (?, ?, ?, ?, sysdate, ?, ?)");
      
      ps = qb.getPreparedStatement(qs.toString());
      ps.setLong(1, getAppSeqNum());
      ps.setInt(2, getQueueType());
      ps.setInt(3, getQueueStage());
      ps.setString(4, user);
      ps.setInt(5, newStatus);
      ps.setString(6, message);
      
      if(ps.executeUpdate() != 1)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "recordQueueChange: Unable to insert app_queue_history");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "recordQueueChange: " + e.toString());
    }
    finally
    {
      try { ps.close(); } catch(Exception e) {}
      
      qb.cleanUp();
    }
  }
  
  // accessors
  public synchronized void setLockedBy(String lockedBy)
  {
    this.lockedBy = lockedBy;
  }
  
  public synchronized String getLockedBy()
  {
    return this.lockedBy;
  }
  
  public synchronized void setQueueType(String queueType)
  {
    try
    {
      setQueueType(Integer.parseInt(queueType));
    }
    catch(Exception e)
    {
    }
  }
  public synchronized void setQueueType(int queueType)
  {
    this.queueType = queueType;
  }
  
  public synchronized int getQueueType()
  {
    return this.queueType;
  }
  
  public synchronized void setQueueStage(String queueStage)
  {
    try
    {
      setQueueStage(Integer.parseInt(queueStage));
    }
    catch(Exception e)
    {
    }
  }
  public synchronized void setQueueStage(int queueStage)
  {
    this.queueStage = queueStage;
  }
  
  public synchronized int getQueueStage()
  {
    return this.queueStage;
  }
  
  public synchronized void setControlNumber(long controlNumber)
  {
    QueueBean     qb          = null;
    
    // set the private attribute
    this.controlNumber = controlNumber;
    
    try
    {
      // get a new QueueBean to do the database work
      qb = new QueueBean("setControlNumber");
      
      this.appSeqNum = qb.getAppSeqNum(controlNumber);
    }
    catch(Exception e)
    {
      this.appSeqNum = 0;
    }
    finally
    {
      if(qb != null)
      {
        qb.cleanUp("setControlNumber");
      }
    }
  }
  
  public synchronized void setControlNumber(String controlNumber)
  {
    try
    {
      setControlNumber(Long.parseLong(controlNumber));
    }
    catch(Exception e)
    {
    }
  }
  
  public synchronized void setSeqNo(String seqNo)
  {
    try
    {
      setSeqNo(Long.parseLong(seqNo));
    }
    catch(Exception e)
    {
    }
  }

  public synchronized void setSeqNo(long seqNo)
  {
    QueueBean     qb          = null;
    
    // set the private attribute
    this.appSeqNum = seqNo;
    
    try
    {
      // get a new QueueBean to do the database work
      qb = new QueueBean("getCntrlNum");
      
      this.controlNumber = qb.getCntrlNum(seqNo);
    }
    catch(Exception e)
    {
      this.appSeqNum = 0;
    }
    finally
    {
      if(qb != null)
      {
        qb.cleanUp("getCntrlNum");
      }
    }
  }

  public long getAppSeqNum()
  {
    return this.appSeqNum;
  }
  
  public long getControlNumber()
  {
    return this.controlNumber;
  }

  public void setResearch(boolean research)
  {
    this.research = research;
  }

  public boolean isResearch()
  {
    return this.research;
  }

}
