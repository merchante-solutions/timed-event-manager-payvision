/*@lineinfo:filename=StatusDetailBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/StatusDetailBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/31/03 10:37a $
  Version            : $Revision: 16 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.ops;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Vector;
import com.mes.constants.MesQueues;
import com.mes.queues.QueueTools;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


public class StatusDetailBean extends com.mes.database.SQLJConnectionBase
{
  public class DepartmentStatus
  {
    private long    appSeqNum;
    private int     deptCode;
    private String  deptDesc;
    private int     statusCode;
    private String  statusDesc;
    private Date    dateReceived;
    private Time    timeReceived;
    private Date    dateCompleted;
    private Time    timeCompleted;
    private String  contactName;
    private boolean allowDropDown;
    private boolean useSecondRow;

    private SimpleDateFormat    simpleDate = new SimpleDateFormat("MM-dd-yy ");
    private SimpleDateFormat    simpleTime = new SimpleDateFormat("hh:mm aa");
    
    /*
    ** CONSTRUCTOR
    */
    public DepartmentStatus(ResultSet rs)
    {
      try
      {
        appSeqNum     = rs.getLong("app_seq_num");
        deptCode      = rs.getInt("dept_code");
        deptDesc      = rs.getString("dept_desc");
        statusCode    = rs.getInt("status_code");
        statusDesc    = rs.getString("status_desc");
        dateReceived  = rs.getDate("date_received");
        timeReceived  = rs.getTime("date_received");
        dateCompleted = rs.getDate("date_completed");
        timeCompleted = rs.getTime("date_completed");
        contactName   = rs.getString("contact_name");
        allowDropDown = (rs.getString("allow_dropdown") != null && rs.getString("allow_dropdown").equals("Y"));
        useSecondRow  = (rs.getString("use_second_row") != null && rs.getString("use_second_row").equals("Y"));
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "constructor: " + e.toString());
      }
    }

    public boolean dropDownAllowed()
    {
      return this.allowDropDown;
    }
    public boolean secondRowUsed()
    {
      return this.useSecondRow;
    }
    public long getAppSeqNum()
    {
      return this.appSeqNum;
    }    
    public int getDeptCode()
    {
      return this.deptCode;
    }
    public String getDeptDesc()
    {
      return this.deptDesc;
    }
    public int getStatusCode()
    {
      return this.statusCode;
    }
    public String getStatusDesc()
    {
      return this.statusDesc;
    }
    public String getDateReceived()
    {
      StringBuffer dateStr = new StringBuffer("");
      
      if(this.dateReceived != null)
      {
        try
        {
          dateStr.append(simpleDate.format(dateReceived));
          dateStr.append(simpleTime.format(timeReceived));
        }
        catch(Exception e)
        {
          dateStr.setLength(0);
        }
      }
      
      return dateStr.toString();
    }
    public String getDateCompleted()
    {
      StringBuffer  dateStr = new StringBuffer("");
      
      if(this.dateCompleted != null)
      {
        try
        {
          dateStr.append(simpleDate.format(dateCompleted));
          dateStr.append(simpleTime.format(timeCompleted));
        }
        catch(Exception e)
        {
          dateStr.setLength(0);
        }
      }
      
      return dateStr.toString();
    }
    public String getContactName()
    {
      if(this.contactName == null)
      {
        this.contactName = "";
      }
      
      return this.contactName;
    }
  }
  
  // private data members
  private long        primaryKey          = 0L;
  private int         dept                = 0;
  private int         completedDate       = 0;
  private int         receivedDate        = 0;
  
  private boolean     submit              = false;
  private int         newStatus           = 0;

  private SimpleDateFormat    simpleDate  = new SimpleDateFormat("MM-dd-yy ");
  private SimpleDateFormat    simpleTime  = new SimpleDateFormat("hh:mm aa");
  
  private String      dbaName             = "";
  private String      merchantNumber      = "----";
  private String      controlNumber       = "HAPPYJOY";
  private String      vnumbers            = "";
  private boolean     viewReport          = false;
  private String      report              = "";
  private String      toDay               = "";
  private String      toMonth             = "";
  private String      toYear              = "";
  private String      fromMonth           = "";
  private String      fromDay             = "";
  private String      fromYear            = "";
  private String      sourceName          = "";
  private int         appType             = 0;

  private Vector      departments         = new Vector();
  private Vector      statusCodes         = new Vector();
  private Vector      statusDescs         = new Vector();
  private Vector      statusDepts         = new Vector();
  
  public static final int   RECONCILE     = 1;
  public static final int   DEPOSIT       = 2;
  public static final int   TRANSACTION   = 3;
  public static final int   REJECTS       = 4;
  public static final int   ADJUSTMENTS   = 5;
  public static final int   INTERCHANGE   = 6;
  public static final int   MONTHLYSTATE  = 7;


  /*
  ** CONSTRUCTOR
  */
  public StatusDetailBean()
  {
  }
  
  /*
  ** METHOD submitData
  **
  ** Allows status and dates to be updated
  */
  public void submitData(UserBean ub)
  {
    String progress = "";
    
    String userName = ub.getLoginName();
    
    try
    {
      if(receivedDate > 0)
      {
        progress = "updating received date";
        /*@lineinfo:generated-code*//*@lineinfo:229^9*/

//  ************************************************************
//  #sql [Ctx] { update    app_tracking
//            set       date_received = sysdate,
//                      contact_name  = :userName
//            where     app_seq_num   = :primaryKey and
//                      dept_code     = :dept and
//                      date_received is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update    app_tracking\n          set       date_received = sysdate,\n                    contact_name  =  :1 \n          where     app_seq_num   =  :2  and\n                    dept_code     =  :3  and\n                    date_received is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.ops.StatusDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,userName);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,dept);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:237^9*/
      }

      if(completedDate > 0)
      {
        // set the completion date for this dept
        progress = "updating completed date";
        /*@lineinfo:generated-code*//*@lineinfo:244^9*/

//  ************************************************************
//  #sql [Ctx] { update    app_tracking
//            set       date_completed  = sysdate,
//                      contact_name    = :userName
//            where     app_seq_num     = :primaryKey and
//                      dept_code       = :dept and
//                      date_completed is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update    app_tracking\n          set       date_completed  = sysdate,\n                    contact_name    =  :1 \n          where     app_seq_num     =  :2  and\n                    dept_code       =  :3  and\n                    date_completed is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.StatusDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,userName);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,dept);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:252^9*/
        
        // add to activation queue if this was the MMS department
        if(dept == QueueConstants.DEPARTMENT_MMS)
        {
          QueueTools.insertQueue(primaryKey, MesQueues.Q_ACTIVATE_NEW, ub);
        }
      }
      
      if(submit && newStatus > 0)
      {
        progress = "updating status";
        /*@lineinfo:generated-code*//*@lineinfo:264^9*/

//  ************************************************************
//  #sql [Ctx] { update    app_tracking
//            set       status_code = :newStatus
//            where     app_seq_num = :primaryKey and
//                      dept_code   = :dept
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update    app_tracking\n          set       status_code =  :1 \n          where     app_seq_num =  :2  and\n                    dept_code   =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.StatusDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,newStatus);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,dept);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:270^9*/
      }

      if((newStatus - dept) == 1)
      {
        progress = "reseting timeStamp and userName";
        /*@lineinfo:generated-code*//*@lineinfo:276^9*/

//  ************************************************************
//  #sql [Ctx] { update    app_tracking
//            set       date_completed  = null,
//                      date_received   = null,
//                      contact_name    = null
//            where     app_seq_num     = :primaryKey and
//                      dept_code       = :dept
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update    app_tracking\n          set       date_completed  = null,\n                    date_received   = null,\n                    contact_name    = null\n          where     app_seq_num     =  :1  and\n                    dept_code       =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.StatusDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,dept);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:284^9*/
      }
    
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::submitData()", progress + ": " + e.toString());
    }
  }
  
  public void resetAppTracking()
  {
    try
    {
      // try to add app tracking
      /*@lineinfo:generated-code*//*@lineinfo:299^7*/

//  ************************************************************
//  #sql [Ctx] { call add_app_tracking(:primaryKey)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN add_app_tracking( :1 )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.StatusDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:302^7*/
      
      this.vnumbers = "";
    }
    catch(Exception e)
    {
      logEntry("resetAppTracking(" + primaryKey + ")", e.toString());
    }
  }

  public String getSecondRowData(int deptCode)
  {
    String result = "";

    switch(deptCode)
    {
      case QueueConstants.DEPARTMENT_TSYS:
        result = getDiscoverAddedToTsys();
      break;
      case QueueConstants.DEPARTMENT_MMS:
        result = getDiscoverAddedToMms();
      break;
    }

    return result;
  }

  private String getDiscoverAddedToTsys()
  {
    ResultSet         rs      = null;
    ResultSetIterator it      = null;
    String            result  = "";

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:337^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    tsys_updated
//          from      merchant_discover_app
//          where     app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    tsys_updated\n        from      merchant_discover_app\n        where     app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.StatusDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.StatusDetailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:342^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        if(rs.getTimestamp("tsys_updated") != null)
        {
          result = "<b>Discover Added to TSYS " + simpleDate.format(rs.getDate("tsys_updated")) + " " + simpleTime.format(rs.getTime("tsys_updated")) + "</b>";
        }
        else
        {
          result = "<b>Discover NOT YET Added to TSYS</b>";
        }
      }

      
      it.close();
    }
    catch(Exception e1)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getDiscoverAddedToTsys", e1.toString());
    }

    return result;
  }
  
  private String getDiscoverAddedToMms()
  {
    ResultSet         rs      = null;
    ResultSetIterator it      = null;
    String            result  = "";

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:377^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    mms_updated
//          from      merchant_discover_app
//          where     app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mms_updated\n        from      merchant_discover_app\n        where     app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.StatusDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.StatusDetailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:382^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        
        if(rs.getTimestamp("mms_updated") != null)
        {
          result = "<b>Discover Added to MMS " + simpleDate.format(rs.getDate("mms_updated")) + " " + simpleTime.format(rs.getTime("mms_updated")) + "</b>";
        }
        else
        {
          result = "<b>Discover NOT YET Added to MMS</b>";
        }
      }
      
      it.close();
    }
    catch(Exception e1)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getDiscoverAddedToMms", e1.toString());
    }

    return result;
  }

  /*
  ** METHOD getData
  */
  public void getData()
  {
    ResultSet         rs      = null;
    ResultSetIterator it      = null;
    
    // get statuses
    try
    {
      statusCodes.clear();
      statusDescs.clear();
      statusDepts.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:424^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    status_code,
//                    status_desc,
//                    dept_code
//          from      dept_status
//          order by  status_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    status_code,\n                  status_desc,\n                  dept_code\n        from      dept_status\n        order by  status_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.StatusDetailBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.StatusDetailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:431^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        if(rs.getInt("status_code") != QueueConstants.DEPT_STATUS_ALL)
        {
          statusCodes.add(rs.getString("status_code"));
          statusDescs.add(rs.getString("status_desc"));
          statusDepts.add(rs.getInt("dept_code"));
        }
      }
      
      it.close();
    }
    catch(Exception e1)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + ":: getting statuses", e1.toString());
    }
    
    // get merchant data
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:455^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    merch_number,
//                    merc_cntrl_number,
//                    merch_business_name
//          from      merchant
//          where     app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    merch_number,\n                  merc_cntrl_number,\n                  merch_business_name\n        from      merchant\n        where     app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.StatusDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.StatusDetailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:462^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        setDbaName(rs.getString("merch_business_name"));
        setMerchantNumber(rs.getLong("merch_number"));
        setControlNumber(rs.getLong("merc_cntrl_number"));
      }
      
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData (merchant): " + e.toString());
    }
    

    // get vnumber data
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:484^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    vnumber
//          from      merch_vnumber
//          where     app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    vnumber\n        from      merch_vnumber\n        where     app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.StatusDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.ops.StatusDetailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:489^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        if(this.vnumbers.equals(""))
        {
          this.vnumbers = rs.getString("vnumber");
        }
        else
        {
          this.vnumbers = this.vnumbers + ";" + rs.getString("vnumber");
        }        
      }
      
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData (vnumber): " + e.toString());
    }



    // get status details
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:517^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   at.app_seq_num,
//                   at.dept_code,
//                   at.status_code,
//                   at.date_received,
//                   at.date_completed,
//                   at.contact_name,
//                   dept.dept_desc,
//                   dept.allow_dropdown,
//                   dept.use_second_row,
//                   nvl(stat.status_desc, '&nbsp;') status_desc
//          from     app_tracking at,
//                   departments dept,
//                   dept_status stat
//          where    at.app_seq_num = :primaryKey and
//                   at.dept_code = dept.dept_code and
//                   at.status_code = stat.status_code(+)
//          order by dept_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   at.app_seq_num,\n                 at.dept_code,\n                 at.status_code,\n                 at.date_received,\n                 at.date_completed,\n                 at.contact_name,\n                 dept.dept_desc,\n                 dept.allow_dropdown,\n                 dept.use_second_row,\n                 nvl(stat.status_desc, '&nbsp;') status_desc\n        from     app_tracking at,\n                 departments dept,\n                 dept_status stat\n        where    at.app_seq_num =  :1  and\n                 at.dept_code = dept.dept_code and\n                 at.status_code = stat.status_code(+)\n        order by dept_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.StatusDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.StatusDetailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:536^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        DepartmentStatus deptStatus = new DepartmentStatus(rs);
        departments.add(deptStatus);
      }
      
      it.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData (details): " + e.toString());
    }
    
    
    // get application source
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:557^7*/

//  ************************************************************
//  #sql [Ctx] { select  u.login_name,
//                  a.app_type
//          
//          from    users u, application a
//          where   a.app_seq_num = :primaryKey and
//                  u.user_id = a.app_user_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  u.login_name,\n                a.app_type\n         \n        from    users u, application a\n        where   a.app_seq_num =  :1  and\n                u.user_id = a.app_user_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.StatusDetailBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sourceName = (String)__sJT_rs.getString(1);
   appType = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:566^7*/
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData (app source): " + e.toString());
    }
  }
  
  
  public boolean hasReports()
  {
    ResultSet         rs      = null;
    ResultSetIterator it      = null;
    boolean           result  = false;
    
    
    try //checks to see if valid merchant number.. if not.. returns false
    {
      long temp = Long.parseLong(this.merchantNumber);
      result = true;
    }
    catch(Exception e)
    {
      result = false;
    }
    
    if(!result)
    {
      return result;
    }
    else
    {
      result = false;
    }
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:602^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    merchant_number
//          from      mif
//          where     merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    merchant_number\n        from      mif\n        where     merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.StatusDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.ops.StatusDetailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:607^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        result = true;
      }
      else
      {
        result = false;
      }
      it.close();
    }
    catch(Exception e1)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::checking reports ", e1.toString());
    }
    return result;
  }
  
  /*
  ** ACCESSORS
  */
  public Vector getStatusCodes()
  {
    return this.statusCodes;
  }
  public Vector getStatusDescs()
  {
    return this.statusDescs;
  }
  public Vector getStatusDepts()
  {
    return this.statusDepts;
  }
  public Vector getDepartments()
  {
    return this.departments;
  }
  
  public void setDbaName(String dbaName)
  {
    this.dbaName = dbaName;
  }
  public String getDbaName()
  {
    return this.dbaName;
  }
  
  public void setMerchantNumber(long merchantNumber)
  {
    if(merchantNumber == 0)
    {
      this.merchantNumber = "----";
    }
    else
    {
      this.merchantNumber = Long.toString(merchantNumber);
    }
  }
  public String getMerchantNumber()
  {
    return this.merchantNumber;
  }
  public String getVNumber()
  {
    return this.vnumbers;
  }

  public void setMerchantNumber(String temp)
  {
    this.merchantNumber = temp;
  }
  
  public void setControlNumber(String controlNumber)
  {
    try
    {
      setControlNumber(Long.parseLong(controlNumber));
    }
    catch(Exception e)
    {
      logEntry("setControlNumber(" + controlNumber + ")", e.toString());
    }
  }
  
  public void setControlNumber(long controlNumber)
  {
    try
    {
      this.controlNumber = Long.toString(controlNumber);
    }
    catch(Exception e)
    {
      this.controlNumber = "N/A";
    }
  }
  public String getControlNumber()
  {
    return this.controlNumber;
  }
  
  public void setPrimaryKey(String primaryKey)
  {
    try
    {
      this.primaryKey = Long.parseLong(primaryKey);
    }
    catch(Exception e)
    {
      this.primaryKey = 0L;
    }
  }
  public long getPrimaryKey()
  {
    return this.primaryKey;
  }
  
  public void setDept(String dept)
  {
    try
    {
      this.dept = Integer.parseInt(dept);
    }
    catch(Exception e)
    {
      this.dept=0;
    }
  }
  public int getDept()
  {
    return this.dept;
  }
  
  public void setCompletedDate(String completedDate)
  {
    try
    {
      this.completedDate = Integer.parseInt(completedDate);
    }
    catch(Exception e)
    {
      this.completedDate = 0;
    }
  }
  public int getCompletedDate()
  {
    return this.completedDate;
  }
  
  public void setReceivedDate(String receivedDate)
  {
    try
    {
      this.receivedDate = Integer.parseInt(receivedDate);
    }
    catch(Exception e)
    {
      this.receivedDate = 0;
    }
  }
  public int getReceivedDate()
  {
    return this.receivedDate;
  }
  
  public void setSubmit(String submit)
  {
    this.submit = true;
  }
  
  public void setNewStatus(String newStatus)
  {
    try
    {
      this.newStatus = Integer.parseInt(newStatus);
    }
    catch(Exception e)
    {
      this.newStatus = 0;
    }
  }
  public void setViewReport(String temp)
  {
    viewReport = true;
  }
  public boolean isViewReport()
  {
    return viewReport;
  }

  public void setReport(String temp)
  {
    int tempReport = 0;
    try
    {
      tempReport = Integer.parseInt(temp);
    }
    catch(Exception e)
    {
      tempReport = 0;
    }
  
    switch(tempReport)
    {
      case 1:
        report = "/jsp/reports/merch_reconcile_details.jsp?";
        break;
      case 2:
        report = "/jsp/reports/merch_deposit_sum.jsp?";
        break;
      case 3:
        report = "/jsp/reports/merch_trans_sum.jsp?";
        break;
      case 4:
        report = "/jsp/reports/merch_rejects.jsp?";
        break;
      case 5:
        report = "/jsp/reports/merch_adjustments.jsp?";
        break;
      case 6:
        report = "/jsp/reports/merch_interchange.jsp?";
        break;
      case 7:
        report = "/jsp/reports/mo_statements.jsp?";
        break;
      default:
        report = "";
        break;
    }
  }

  public String getReport()
  {
    StringBuffer result = new StringBuffer(report);
    result.append("com.mes.fromDay=" + fromDay + "&");
    result.append("com.mes.fromMonth=" + fromMonth + "&");
    result.append("com.mes.fromYear=" + fromYear + "&");

    result.append("com.mes.toDay=" + toDay + "&");
    result.append("com.mes.toMonth=" + toMonth + "&");
    result.append("com.mes.toYear=" + toYear + "&");
    result.append("com.mes.MerchantId=" + merchantNumber);
    return result.toString();
  }

  public void setToDay(String temp)
  {
    toDay = temp;
  }
  public void setToMonth(String temp)
  {
    toMonth = temp;
  }
  public void setToYear(String temp)
  {
    toYear = temp;
  }
  public void setFromDay(String temp)
  {
    fromDay = temp;
  }
  public void setFromMonth(String temp)
  {
    fromMonth = temp;
  }
  public void setFromYear(String temp)
  {
    fromYear = temp;
  }
  
  public String getSourceName()
  {
    return sourceName;
  }
  
  public int getAppType()
  {
    return appType;
  }
}/*@lineinfo:generated-code*/