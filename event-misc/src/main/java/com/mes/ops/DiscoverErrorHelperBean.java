/*@lineinfo:filename=DiscoverErrorHelperBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/DiscoverErrorHelperBean.sqlj $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/12/03 5:32p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import sqlj.runtime.ResultSetIterator;


public class DiscoverErrorHelperBean extends com.mes.database.SQLJConnectionBase
{
  private HashMap       errorCodes              = new HashMap();

  private Vector        reasons                 = null;
  private int           currentIdx              = -1;
  private boolean       moreAvailable           = false;

  public DiscoverErrorHelperBean()
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:54^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    ERROR_CODE, ERROR_DESC
//          from      RAP_APP_ERROR_CODES
//          order by  ERROR_CODE asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    ERROR_CODE, ERROR_DESC\n        from      RAP_APP_ERROR_CODES\n        order by  ERROR_CODE asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.DiscoverErrorHelperBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.DiscoverErrorHelperBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:59^7*/
        
      rs = it.getResultSet();
        
      while(rs.next())
      {
        errorCodes.put(rs.getString("ERROR_CODE"),rs.getString("ERROR_DESC"));
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "DiscoverErrorHelperBean(constructor): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public DiscoverErrorHelperBean(String connectString)
  {
    super(connectString);
  }

  private void resetVars()
  {
    reasons                 = null;
    currentIdx              = -1;
    moreAvailable           = false;
  }
  
  public boolean setErrorReasons(String reasonString)
  {
    boolean result = true;

    if(isBlank(reasonString))
    {
      resetVars();
      return false;
    }
    else
    {
      reasonString = reasonString.trim();
    }
    
    try
    {

      //break up reason string every 2 chars = 1 error reason

      String tempStr = "";
      reasons = new Vector();

      result = ((reasonString.length() % 2) == 0);
      
      if(!result)
      {
        return result;
      }

      int numErrorReasons = reasonString.length() / 2;

      for(int x=0; x<numErrorReasons; ++x)
      {
        switch(x)
        {
          case 0:
            if(numErrorReasons > 1)
            {
              tempStr = reasonString.substring(0,2);
            }
            else
            {
              tempStr = reasonString;
            }
          break;
          
          case 1:
            if(numErrorReasons > 2)
            {  
              tempStr = reasonString.substring(2,4);
            }
            else
            {
              tempStr = reasonString.substring(2);
            }
          break;
          
          case 2:
            if(numErrorReasons > 3)
            {  
              tempStr = reasonString.substring(4,6);
            }
            else
            {
              tempStr = reasonString.substring(4);
            }
          break;
          
          case 3:
            if(numErrorReasons > 4)
            {  
              tempStr = reasonString.substring(6,8);
            }
            else
            {
              tempStr = reasonString.substring(6);
            }
          break;
          
          case 4:
            tempStr = reasonString.substring(8);
          break;
        }

        //add the last code to vector
        reasons.add(tempStr);
      }
    }
    catch(Exception e)
    {
      resetVars();
      result = false;
    }
    return result;
  }

  public boolean hasNext()
  {
    try
    {
      currentIdx++;
      if(currentIdx < reasons.size())
      {
        moreAvailable = true;
      }
      else
      {
        moreAvailable = false;
      }
    }
    catch(Exception e)
    {
      resetVars();
    }  
    return moreAvailable;
  }

  public String getNextCode()
  {
    String result = "";
    if(!moreAvailable)
    {
      return "";
    }

    try
    {
      result = isBlank((String)reasons.elementAt(currentIdx)) ? "" : (String)reasons.elementAt(currentIdx);
    }
    catch(Exception e)
    {
      result = "";
    }

    return result;
  }

  public String getNextErrorDesc()
  {
    String result = "";
    if(!moreAvailable)
    {
      return "";
    }

    try
    {
      result = isBlank((String)errorCodes.get((String)reasons.elementAt(currentIdx))) ? "" : (String)errorCodes.get((String)reasons.elementAt(currentIdx));
    }
    catch(Exception e)
    {
      result = "";
    }
    return result;
  }

  public String getBrList()
  {
    String result = "";

    try
    {
      for(int p=0; p<reasons.size(); p++)
      {
        result += ("&nbsp;&nbsp;" + (isBlank((String)errorCodes.get((String)reasons.elementAt(p))) ? "" : (String)errorCodes.get((String)reasons.elementAt(p))) + "<BR>");
      }
    }
    catch(Exception e)
    {
      result = "";
    }
    return result;
  }

  public String getNewLineList()
  {
    String result = "";

    try
    {
      for(int p=0; p<reasons.size(); p++)
      {
        result += isBlank((String)errorCodes.get((String)reasons.elementAt(p))) ? "" : (String)errorCodes.get((String)reasons.elementAt(p)) + "\n";
      }
    }
    catch(Exception e)
    {
      result = "";
    }
    return result;
  }


  public String getErrorDesc(String singleCode)
  {
    String result = "";

    try
    {
      result = isBlank((String)errorCodes.get(singleCode)) ? "" : (String)errorCodes.get(singleCode);
    }
    catch(Exception e)
    {
      result = "";
    }
    return result;
  }


  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }

}/*@lineinfo:generated-code*/