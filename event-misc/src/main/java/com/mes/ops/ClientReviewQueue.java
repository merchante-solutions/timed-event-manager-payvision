/*@lineinfo:filename=ClientReviewQueue*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ClientReviewQueue.sqlj $

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2008-11-12 09:52:51 -0800 (Wed, 12 Nov 2008) $
  Version            : $Revision: 15520 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesMenus;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.forms.ComboDateField;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.queues.QueueBase;
import com.mes.queues.QueueTools;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;

public class ClientReviewQueue extends QueueBase
{
  public boolean dateRangeEnabled()
  {
    // disable date range for now
    return false;
  }
  
  protected void loadQueueData(UserBean user)
  {
    try
    {
      java.sql.Date fromDate = 
        ((ComboDateField)fields.getField("fromDate")).getSqlDate();
      java.sql.Date toDate = 
        ((ComboDateField)fields.getField("toDate")).getSqlDate();
        
      if( user.getHierarchyNode() == 9999999999L  ||
          user.getHierarchyNode() == 394100000L   ||
          user.getHierarchyNode() == 3941500001L)
      {
        /*@lineinfo:generated-code*//*@lineinfo:58^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    qd.id                 id,
//                      qd.type               type,
//                      qd.item_type          item_type,
//                      qd.owner              owner,
//                      qd.date_created       date_created,
//                      qd.source             source,
//                      qd.affiliate          affiliate,
//                      qd.last_changed       last_changed,
//                      qd.last_user          last_user,
//                      m.merch_number        merchant_number,
//                      m.merch_business_name description,
//                      u.name                rep_name,
//                      qd.id                 control,
//                      qd.locked_by          locked_by,
//                      nvl(qn.note_count,0)  note_count,
//                      qt.status             status
//            from      q_data                qd,
//                      q_types               qt,
//                      ( select    qn.id,
//                                  count(qn.id)  note_count
//                        from      q_notes qn,
//                                  q_data qd
//                        where     qd.type = :this.type and
//                                  qd.id = qn.id(+)
//                        group by qn.id )    qn,
//                      merchant              m,
//                      application           app,
//                      users                 u,
//                      t_hierarchy           th
//            where     qd.type = :this.type
//                      and qd.type = qt.type
//                      and qd.id = qn.id(+)
//                      and qd.id = m.app_seq_num
//                      and qd.id = app.app_seq_num
//                      and app.app_type in (0, 28)
//                      and app.app_user_login = u.login_name
//                      and u.hierarchy_node = th.descendent
//                      and th.ancestor = :user.getHierarchyNode()
//            order by  qd.date_created asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_749 = user.getHierarchyNode();
  try {
   String theSqlTS = "select    qd.id                 id,\n                    qd.type               type,\n                    qd.item_type          item_type,\n                    qd.owner              owner,\n                    qd.date_created       date_created,\n                    qd.source             source,\n                    qd.affiliate          affiliate,\n                    qd.last_changed       last_changed,\n                    qd.last_user          last_user,\n                    m.merch_number        merchant_number,\n                    m.merch_business_name description,\n                    u.name                rep_name,\n                    qd.id                 control,\n                    qd.locked_by          locked_by,\n                    nvl(qn.note_count,0)  note_count,\n                    qt.status             status\n          from      q_data                qd,\n                    q_types               qt,\n                    ( select    qn.id,\n                                count(qn.id)  note_count\n                      from      q_notes qn,\n                                q_data qd\n                      where     qd.type =  :1  and\n                                qd.id = qn.id(+)\n                      group by qn.id )    qn,\n                    merchant              m,\n                    application           app,\n                    users                 u,\n                    t_hierarchy           th\n          where     qd.type =  :2 \n                    and qd.type = qt.type\n                    and qd.id = qn.id(+)\n                    and qd.id = m.app_seq_num\n                    and qd.id = app.app_seq_num\n                    and app.app_type in (0, 28)\n                    and app.app_user_login = u.login_name\n                    and u.hierarchy_node = th.descendent\n                    and th.ancestor =  :3 \n          order by  qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.ClientReviewQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setInt(2,this.type);
   __sJT_st.setLong(3,__sJT_749);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.ClientReviewQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:99^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:103^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    qd.id                 id,
//                      qd.type               type,
//                      qd.item_type          item_type,
//                      qd.owner              owner,
//                      qd.date_created       date_created,
//                      qd.source             source,
//                      qd.affiliate          affiliate,
//                      qd.last_changed       last_changed,
//                      qd.last_user          last_user,
//                      m.merch_number        merchant_number,
//                      m.merch_business_name description,
//                      u.name                rep_name,
//                      qd.id                 control,
//                      qd.locked_by          locked_by,
//                      nvl(qn.note_count,0)  note_count,
//                      qt.status             status
//            from      q_data                qd,
//                      q_types               qt,
//                      ( select    qn.id,
//                                  count(qn.id)  note_count
//                        from      q_notes qn,
//                                  q_data qd
//                        where     qd.type = :this.type and
//                                  qd.id = qn.id(+)
//                        group by qn.id )    qn,
//                      merchant              m,
//                      application           app,
//                      users                 u,
//                      t_hierarchy           th
//            where     qd.type = :this.type
//                      and qd.type = qt.type
//                      and qd.id = qn.id(+)
//                      and qd.id = m.app_seq_num
//                      and qd.id = app.app_seq_num
//                      and app.app_user_login = u.login_name
//                      and u.hierarchy_node = th.descendent
//                      and th.ancestor = :user.getHierarchyNode()
//            order by  qd.date_created asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_750 = user.getHierarchyNode();
  try {
   String theSqlTS = "select    qd.id                 id,\n                    qd.type               type,\n                    qd.item_type          item_type,\n                    qd.owner              owner,\n                    qd.date_created       date_created,\n                    qd.source             source,\n                    qd.affiliate          affiliate,\n                    qd.last_changed       last_changed,\n                    qd.last_user          last_user,\n                    m.merch_number        merchant_number,\n                    m.merch_business_name description,\n                    u.name                rep_name,\n                    qd.id                 control,\n                    qd.locked_by          locked_by,\n                    nvl(qn.note_count,0)  note_count,\n                    qt.status             status\n          from      q_data                qd,\n                    q_types               qt,\n                    ( select    qn.id,\n                                count(qn.id)  note_count\n                      from      q_notes qn,\n                                q_data qd\n                      where     qd.type =  :1  and\n                                qd.id = qn.id(+)\n                      group by qn.id )    qn,\n                    merchant              m,\n                    application           app,\n                    users                 u,\n                    t_hierarchy           th\n          where     qd.type =  :2 \n                    and qd.type = qt.type\n                    and qd.id = qn.id(+)\n                    and qd.id = m.app_seq_num\n                    and qd.id = app.app_seq_num\n                    and app.app_user_login = u.login_name\n                    and u.hierarchy_node = th.descendent\n                    and th.ancestor =  :3 \n          order by  qd.date_created asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.ClientReviewQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,this.type);
   __sJT_st.setInt(2,this.type);
   __sJT_st.setLong(3,__sJT_750);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.ClientReviewQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("getQueueData(" + user.getLoginName() + ")", e.toString());
    }
  }
  
  protected void loadQueueItem(long id)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:156^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    qd.id                 id,
//                    qd.type               type,
//                    qd.item_type          item_type,
//                    qd.owner              owner,
//                    qd.date_created       date_created,
//                    qd.source             source,
//                    qd.affiliate          affiliate,
//                    qd.last_changed       last_changed,
//                    qd.last_user          last_user,
//                    m.merch_number        merchant_number,
//                    m.merch_business_name description,
//                    u.name                rep_name,
//                    qd.id                 control,
//                    qd.locked_by          locked_by,
//                    nvl(qn.note_count,0)  note_count,
//                    qt.status             status
//          from      q_data                qd,
//                    q_types               qt,
//                    (
//                      select  count(qn.id)  note_count,
//                              qn.id
//                      from    q_notes qn
//                      where   qn.id = :id
//                      group by qn.id
//                    ) qn,
//                    merchant              m,
//                    users                 u
//          where     qd.id = :id
//                    and qd.type = :this.type
//                    and qd.type = qt.type
//                    and qd.id = qn.id(+)
//                    and qd.id = m.app_seq_num
//                    and qd.source = u.login_name(+)      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    qd.id                 id,\n                  qd.type               type,\n                  qd.item_type          item_type,\n                  qd.owner              owner,\n                  qd.date_created       date_created,\n                  qd.source             source,\n                  qd.affiliate          affiliate,\n                  qd.last_changed       last_changed,\n                  qd.last_user          last_user,\n                  m.merch_number        merchant_number,\n                  m.merch_business_name description,\n                  u.name                rep_name,\n                  qd.id                 control,\n                  qd.locked_by          locked_by,\n                  nvl(qn.note_count,0)  note_count,\n                  qt.status             status\n        from      q_data                qd,\n                  q_types               qt,\n                  (\n                    select  count(qn.id)  note_count,\n                            qn.id\n                    from    q_notes qn\n                    where   qn.id =  :1 \n                    group by qn.id\n                  ) qn,\n                  merchant              m,\n                  users                 u\n        where     qd.id =  :2 \n                  and qd.type =  :3 \n                  and qd.type = qt.type\n                  and qd.id = qn.id(+)\n                  and qd.id = m.app_seq_num\n                  and qd.source = u.login_name(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.ClientReviewQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,id);
   __sJT_st.setInt(3,this.type);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.ClientReviewQueue",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:191^7*/
    }
    catch(Exception e)
    {
      logEntry("loadQueueItem(" + this.type + ", " + id + ")", e.toString());
    }
  }
  
  public String getDescriptionURL()
  {
    return "/jsp/setup/merchinfo4.jsp?primaryKey=";
  }
  
  public int getMenuId()
  {
    return MesMenus.MENU_ID_CLIENT_REVIEW_QUEUES;
  }
  
  public String getBackLink()
  {
    return "/jsp/menus/cbt_queue_menu.jsp?com.mes.ReportMenuId=" + getMenuId();
  }

  public String getBackLinkDesc()
  {
    String result = "";

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:222^7*/

//  ************************************************************
//  #sql [Ctx] { select  menu_title
//          
//          from    menu_types
//          where   menu_id = :getMenuId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_751 = getMenuId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  menu_title\n         \n        from    menu_types\n        where   menu_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.ClientReviewQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,__sJT_751);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:228^7*/
    }
    catch(Exception e)
    {
      logEntry("getBackLinkDesc()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public void setType(int newType)
  {
    super.setType(newType);
    
    String [][] buttons = null;
    
    switch(newType)
    {
      case MesQueues.Q_CLIENT_REVIEW_NEW:
        buttons = new String[][]
        {
          { "Approve",  Integer.toString(MesQueues.Q_CLIENT_REVIEW_APPROVED)  },
          { "Pend",     Integer.toString(MesQueues.Q_CLIENT_REVIEW_PEND)      },
          { "Decline",  Integer.toString(MesQueues.Q_CLIENT_REVIEW_DECLINE)   },
          { "Cancel",   Integer.toString(MesQueues.Q_CLIENT_REVIEW_CANCELED)  }
        };
        break;
        
      case MesQueues.Q_CLIENT_REVIEW_PEND:
        buttons = new String[][]
        {
          { "Approve",  Integer.toString(MesQueues.Q_CLIENT_REVIEW_APPROVED)  },
          { "Decline",  Integer.toString(MesQueues.Q_CLIENT_REVIEW_DECLINE)   },
          { "Cancel",   Integer.toString(MesQueues.Q_CLIENT_REVIEW_CANCELED)  }
        };
        break;
        
      case MesQueues.Q_CLIENT_REVIEW_DECLINE:
        buttons = new String[][]
        {
          { "Approve",  Integer.toString(MesQueues.Q_CLIENT_REVIEW_APPROVED)  },
          { "Pend",     Integer.toString(MesQueues.Q_CLIENT_REVIEW_PEND)      },
          { "Cancel",   Integer.toString(MesQueues.Q_CLIENT_REVIEW_CANCELED)  }
        };
        break;
        
      case MesQueues.Q_CLIENT_REVIEW_CANCELED:
        buttons = new String[][]
        {
          { "Approve",  Integer.toString(MesQueues.Q_CLIENT_REVIEW_APPROVED)  },
          { "Pend",     Integer.toString(MesQueues.Q_CLIENT_REVIEW_PEND)      },
          { "Decline",  Integer.toString(MesQueues.Q_CLIENT_REVIEW_DECLINE)   }
        };
        break;
        
      default:
        break;
    }
    
    // if there are actions provide a way to select and provide comments
    if (buttons != null)
    {
      fields.add( new RadioButtonField( "action", buttons, -1, false, "Must select an action" ) );
      fields.add( new TextareaField(    "actionComments",500,8,80,true));
    }
  }
  
  private void setAppStatus(long appSeqNum, int appStatus)
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:305^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_credit_status = :appStatus
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_credit_status =  :1 \n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ops.ClientReviewQueue",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appStatus);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:310^7*/
    }
    catch(Exception e)
    {
      logEntry("setAppStatus(" + appStatus + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private boolean skipAccountSetup(long appSeqNum)
  {
    boolean result = false;
    
    try
    {
      int recCount = 0;
      
      // if app user name is a conversion user then skip the credit and account setup steps
      /*@lineinfo:generated-code*//*@lineinfo:331^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    application
//          where   app_seq_num = :appSeqNum
//                  and app_user_login = '3942_conversion'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    application\n        where   app_seq_num =  :1 \n                and app_user_login = '3942_conversion'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.ClientReviewQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:338^7*/
      
      result = ( recCount > 0 );
    }
    catch(Exception e)
    {
      logEntry("skipAccountSetup(" + appSeqNum + ")", e.toString());
    }
    
    return( result );
  }
  
  public void doAction(HttpServletRequest request)
  {
    // do queue move
    Field   comments    = getField("actionComments");
    int     destQueueId = HttpHelper.getInt(request, "action", -1);
    long    itemId      = HttpHelper.getLong(request, "id", -1);
    
    // get user id of app submitter
    try
    {
      connect();
      
      String  loginName   = "";  
      /*@lineinfo:generated-code*//*@lineinfo:363^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_user_login
//          
//          from    application
//          where   app_Seq_num = :itemId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_user_login\n         \n        from    application\n        where   app_Seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.ClientReviewQueue",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,itemId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loginName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:369^7*/
      
      UserBean appUser = new UserBean();
      appUser.getData(loginName);
      
      QueueTools.moveQueueItem(itemId, this.type, destQueueId, this.getUser(), comments.getData());
      
      switch(destQueueId)
      {
        case MesQueues.Q_CLIENT_REVIEW_APPROVED:
          if( skipAccountSetup(itemId) )
          {
            // auto approve with manual conversion
            AutoApprove.autoApprove(itemId, Ctx, true);
          }
          else
          {
            // insert into MES credit queue
            (new InsertCreditQueue(Ctx)).afterClientReviewQueue(itemId, appUser, this.getUser());
          }
          break;
        
        case MesQueues.Q_CLIENT_REVIEW_PEND:
          setAppStatus(itemId, mesConstants.APP_STATUS_CLIENT_PEND);
          break;
        
        case MesQueues.Q_CLIENT_REVIEW_DECLINE:
          setAppStatus(itemId, mesConstants.APP_STATUS_CLIENT_DECLINE);
          break;
        
        case MesQueues.Q_CLIENT_REVIEW_CANCELED:
          setAppStatus(itemId, mesConstants.APP_STATUS_CLIENT_CANCELED);
          break;
        
        default:
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("doAction(" + itemId + ", " + destQueueId + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
}/*@lineinfo:generated-code*/