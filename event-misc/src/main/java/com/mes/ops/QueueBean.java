/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/QueueBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-03-12 12:14:18 -0700 (Mon, 12 Mar 2007) $
  Version            : $Revision: 13531 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;
import com.mes.database.DBConnect;

public class QueueBean
{
  public static final String  DB_CONNECT_STRING         = "pool;ApplicationPool";
  public static final int     IS_RUSH_ON                = 1;

  protected PreparedStatement pstmt                     = null;
  protected ResultSet         queueResultSet            = null;
  
  protected DBConnect         db                        = null;
  protected Connection        con                       = null;
  protected String            dateFrom                  = "";
  protected Date              queryDateFrom             = null;
  protected String            dateTo                    = "";
  protected Date              queryDateTo               = null;
  protected String            merchDBA                  = "";
  protected String            merchNum                  = "";
  protected String            controlNumber             = "";
  protected long              queryControlNumber        = 0L;
  protected long              appSeqNo                  = 0L;
  protected String            appSource                 = "";
  protected int               queueType                 = 0;
  protected int               queueStage                = 0;
  
  public Vector               reportMenuItems           = null;
  
  public void Initialize(String msg)
  {
    reportMenuItems = new Vector();
    db = new DBConnect(this, "Initialize");
  }
  
  public void addReportMenuItem(ReportMenuItem rmi)
  {
    reportMenuItems.add(rmi);
  }
    
  public QueueBean(String msg)
  {
    Initialize(msg);
  }
  
  public QueueBean()
  {
    Initialize("auto-instantiation");
  }
  
  public String getQueueDescription(int QueueType)
  {
    return QueueConstants.getQueueDescription(QueueType);
  }
  
  public String getStageDescription(int QueueStage)
  {
    return QueueConstants.getStageDescription(QueueStage);
  }
  
  public String getStageString(int QueueStage)
  {
    return QueueConstants.getStageString(QueueStage);
  }
  
  public Vector getReportMenuItems()
  {
    return this.reportMenuItems;
  }
  
  public PreparedStatement getPreparedStatement(String query)
  {
    PreparedStatement   ps = null;
    try
    {
      if(con == null)
      {
        con = db.getConnection(DB_CONNECT_STRING);
      }
    
      if(con != null)
      {
        ps = con.prepareStatement(query);
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPreparedStatement: " + db.getErrorMessage());
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getPreparedStatement: " + e.toString());
    }
    
    return ps;
  }
  
  public void unlockAll(String user)
  {
    PreparedStatement   ps = null;
    
    try
    {
      if(con == null)
      {
        con = db.getConnection(DB_CONNECT_STRING);
      }
    
      if(con != null)
      {      
        StringBuffer qs = new StringBuffer("");
    
        qs.append("update app_queue ");
        qs.append("set app_queue_lock = '' ");
        qs.append("where app_queue_lock = ?");
        
        ps = getPreparedStatement(qs.toString());
        
        ps.setString(1, user);
        ps.executeUpdate();
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "unlockAll: " + db.getErrorMessage());
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "unlockAll: " + e.toString());
    }
    finally
    {
      if(ps != null)
      {
        try
        {
          ps.close();
        }
        catch (Exception e)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "unlockAll: " + e.toString());
        }
      }
    }
  }
  
  public long getAppSeqNum(long controlNumber)
  {
    PreparedStatement ps    = null;
    ResultSet         rs    = null;
    long              seqNo = 0L;
    
    try
    {
      ps = getPreparedStatement("select app_seq_num from merchant where merc_cntrl_number = ?");
      ps.setLong(1, controlNumber);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        seqNo = rs.getLong("app_seq_num");
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAppSeqNum: application not found");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAppSeqNum: " + e.toString());
    }
    finally
    {
      if(rs != null)
      {
        try
        {
          rs.close();
        }
        catch(Exception e)
        {
        }
      }
      
      if(ps != null)
      {
        try
        {
          ps.close();
        }
        catch(Exception e)
        {
        }
      }
    }
    
    return seqNo;
  }

  public long getCntrlNum(long primaryKey)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    long              cntrlNo = 0L;
    
    try
    {
      ps = getPreparedStatement("select merc_cntrl_number from merchant where app_seq_num = ?");
      ps.setLong(1, primaryKey);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        cntrlNo = rs.getLong("merc_cntrl_number");
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCntrlNum: application not found");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getCntrlNum: " + e.toString());
    }
    finally
    {
      if(rs != null)
      {
        try
        {
          rs.close();
        }
        catch(Exception e)
        {
        }
      }
      
      if(ps != null)
      {
        try
        {
          ps.close();
        }
        catch(Exception e)
        {
        }
      }
    }
    
    return cntrlNo;
  }



  public void getFunctionOptions(javax.servlet.jsp.JspWriter out, long appSeqNum)
  {
    try
    {
      out.println("<font color=\"red\">Unknown bean type</font>");
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFunctionOptions: " + e.toString());
    }
  }
  
  public boolean setLock(long queueKey, int queueType, String lockedBy)
  {
    StringBuffer        qs          = new StringBuffer("");
    boolean             isLocked    = false;
    PreparedStatement   ps          = null;
    
    try
    {
      qs.append("update app_queue ");
      qs.append("set app_queue_lock = ? ");
      qs.append("where app_seq_num = ? and app_queue_type = ?");

      ps = getPreparedStatement(qs.toString());      
      
      if(ps != null)
      {
        ps.setString(1, lockedBy);
        ps.setLong(2, queueKey);
        ps.setInt(3, queueType);
      
        if(ps.executeUpdate() == 1)
        {
          isLocked = true;
        }
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setLock: " + db.getErrorMessage());
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(1, this.getClass().getName(), "setLock: " + e.toString());
    }
    finally
    {
      if(ps != null)
      {
        try
        {
          ps.close();
        }
        catch(Exception e)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setLock: " + e.toString());
        }
      }
    }
    
    return isLocked;
  }
  
  public boolean isAlreadyLocked(long controlNumber, String lockedBy)
  {
    boolean             isLocked      = false;
    PreparedStatement   ps            = null;
    ResultSet           rs            = null;
    
    try
    {
      if(con == null)
      {
        con = db.getConnection(DB_CONNECT_STRING);
      }
      
      if(con != null)
      {
        StringBuffer qs = new StringBuffer("");
      
        qs.append("select app_queue_lock ");
        qs.append("from app_queue a, merchant b ");
        qs.append("where a.app_seq_num = b.app_seq_num and b.merc_cntrl_number = " + controlNumber);
      
        ps = con.prepareStatement(qs.toString());
      
        rs = ps.executeQuery();
      
        if(rs.next())
        {
          String locker = rs.getString("app_queue_lock");
          if(locker != null && ! locker.equals("") && ! locker.equals(lockedBy))
          {
            isLocked = true;
          }
        }
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isAlreadyLocked: " + db.getErrorMessage());
      }
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isAlreadyLocked: " + e.toString());
    }
    finally
    {
      if(ps != null)
      {
        try
        {
          ps.close();
        }
        catch(Exception e)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isAlreadyLocked: " + e.toString());
        }
      }
      
      if(rs != null)
      {
        try
        {
          rs.close();
        }
        catch(Exception e)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isAlreadyLocked: " + e.toString());
        }
      }
    }
    
    return isLocked;
  }
  

  public boolean isAlreadyLocked(long seqNum, int qType, String lockedBy)
  {
    boolean             isLocked      = false;
    PreparedStatement   ps            = null;
    ResultSet           rs            = null;
    
    try
    {
      if(con == null)
      {
        con = db.getConnection(DB_CONNECT_STRING);
      }
      
      if(con != null)
      {
        StringBuffer qs = new StringBuffer("");
      
        qs.append("select app_queue_lock      ");
        qs.append("from app_queue a           ");
        qs.append("where a.app_seq_num    = ? ");
        qs.append("and   a.app_queue_type = ? ");
      
        ps = con.prepareStatement(qs.toString());
        ps.setLong(1, seqNum);
        ps.setInt(2, qType);

        rs = ps.executeQuery();
      
        if(rs.next())
        {
          String locker = rs.getString("app_queue_lock");
          if(locker != null && ! locker.equals("") && ! locker.equals(lockedBy))
          {
            isLocked = true;
          }
        }
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isAlreadyLocked: " + db.getErrorMessage());
      }
    }
    catch (Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isAlreadyLocked: " + e.toString());
    }
    finally
    {
      if(ps != null)
      {
        try
        {
          ps.close();
        }
        catch(Exception e)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isAlreadyLocked: " + e.toString());
        }
      }
      
      if(rs != null)
      {
        try
        {
          rs.close();
        }
        catch(Exception e)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "isAlreadyLocked: " + e.toString());
        }
      }
    }
    
    return isLocked;
  }



  public String getLockInfo(long queueKey)
  {
    StringBuffer        qs = new StringBuffer("");
    String              lockedBy = "";
    PreparedStatement   ps = null;
    ResultSet           rs = null;
    
    try
    {
      qs.append("select app_queue_lock ");
      qs.append("from app_queue ");
      qs.append("where app_seq_num = ?");
      
      ps = getPreparedStatement(qs.toString());
      
      if(ps != null)
      {
        ps.setLong(1, queueKey);
        
        // execute statement
        rs = ps.executeQuery();
        
        if(rs.next())
        {
          lockedBy = rs.getString("app_queue_lock");
        }
        else
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getLockInfo: no lock in database");
        }
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getLockInfo: prepared statement is null");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(1, this.getClass().getName(), "getLockInfo: " + e.toString());
    }
    finally
    {
      if(rs != null)
      {
        try
        {
          rs.close();
        }
        catch(Exception e)
        {
        }
      }
      
      if(ps != null)
      {
        try
        {
          ps.close();
        }
        catch(Exception e)
        {
        }
      }
    }
    
    return(lockedBy);
  }
  
  protected Date StrToDate(String dateString, boolean addDay)
  {
    java.util.Date  javaDate = null;
    java.sql.Date   returnDate = null;
    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    java.util.StringTokenizer st = new java.util.StringTokenizer(dateString, "/");
    
    String sMonth     = "";
    String sDay       = "";
    String sYear      = "";
    StringBuffer RealYear = new StringBuffer("");
    StringBuffer RealDate = new StringBuffer("");
    
    if(st.hasMoreTokens())
    {
      sMonth = st.nextToken();
    }
    
    if(st.hasMoreTokens())
    {
      sDay = st.nextToken();
    }
    
    if(st.hasMoreTokens())
    {
      sYear = st.nextToken();
    }
    
    if(sYear.length() == 2)
    {
      // insert a "20" on the front.  I'm guessing this code won't be used past
      // the year 2100, so this should be safe
      RealYear.append("20");
    }
    RealYear.append(sYear);
    
    // build the new date
    RealDate.append(sMonth);
    RealDate.append("/");
    RealDate.append(sDay);
    RealDate.append("/");
    RealDate.append(RealYear.toString());
    
    try
    {
      if(dateString == null || dateString.equals(""))
      {
        javaDate = new java.util.Date();
      }
      else
      {
        javaDate = dateFormat.parse(RealDate.toString());
      }
    }
    catch( java.text.ParseException e)
    {
      com.mes.support.SyncLog.LogEntry(1, this.getClass().getName(), "StrToDate: " + e.toString());
    }
    
    if(javaDate == null)
    {
      returnDate = null;
    }
    else
    {
      if(addDay)
      {
        // add one to the day
        Calendar cal = Calendar.getInstance();
        cal.setTime(javaDate);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        javaDate = cal.getTime();
      }
      
      returnDate = new java.sql.Date(javaDate.getTime());
    }
    
    return returnDate;
  }
  
  public ResultSet getDocumentTypes()
  {
    ResultSet          rs          = null;
    PreparedStatement  ps          = null;
    
    try
    {
      ps = getPreparedStatement("select * from app_doc order by app_doc_code");
      if(ps != null)
      {
        rs = ps.executeQuery();
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDocumentTypes: prepared statement is null");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDocumentTypes: " + e.toString());
    }
    
    return rs;
  }
  
  public boolean getQueueData(long queueKey, int queueType)
  {
    StringBuffer        qs              = new StringBuffer("");
    boolean             success         = false;
    
    try
    {
      qs.append("select ");
      qs.append("aq.*, ");
      qs.append("merch.merc_cntrl_number, ");
      qs.append("merch.merch_business_name, ");
      qs.append("merch.merch_number, ");
      qs.append("app.*, ");
      qs.append("appstat.* ");
      qs.append("from ");
      qs.append("app_queue aq, ");
      qs.append("merchant merch, ");
      qs.append("application app, ");
      qs.append("app_status appstat ");
      qs.append("where ");
      qs.append("app.app_seq_num = aq.app_seq_num and ");
      qs.append("aq.app_seq_num = merch.app_seq_num and ");
      qs.append("aq.app_status = appstat.status_type and ");
      qs.append("aq.app_seq_num = ? ");
      qs.append("and aq.app_queue_type = ?");

      pstmt = getPreparedStatement(qs.toString());        
      pstmt.setLong(1, queueKey);
      pstmt.setInt(2, queueType);
        
      queueResultSet = pstmt.executeQuery();
        
      success = true;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getQueueData: " + e.toString());
    }
    
    return success;
  }
  
  public boolean getQueueData(long queueKey)
  {
    StringBuffer        qs              = new StringBuffer("");
    boolean             success         = false;
    
    try
    {
      qs.append("select ");
      qs.append("aq.*, ");
      qs.append("app.*, ");
      qs.append("merch.merc_cntrl_number, ");
      qs.append("merch.merch_business_name, ");
      qs.append("merch.merch_number         ");
      qs.append("from ");
      qs.append("app_queue aq, ");
      qs.append("application app, ");
      qs.append("merchant merch ");
      qs.append("where ");
      qs.append("aq.app_seq_num = ? ");
      qs.append("and aq.app_queue_type != 4 ");
      qs.append("and aq.app_seq_num = app.app_seq_num ");
      qs.append("and aq.app_seq_num = merch.app_seq_num ");

      pstmt = getPreparedStatement(qs.toString());        
      pstmt.setLong(1, queueKey);
        
      queueResultSet = pstmt.executeQuery();
        
      success = true;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getQueueData: " + e.toString());
    }
    
    return success;
  }
  public boolean getQueueData(long queueKey, int queueType, String subTables, String subFields, String subConditions)
  {
    StringBuffer        qs              = new StringBuffer("");
    boolean             success         = false;
    
    try
    {
      qs.append("select ");
      qs.append("a.*, ");
      qs.append("appstat.*, ");
      qs.append("app.*, ");
      qs.append(subFields);
      qs.append(" from ");
      qs.append("app_queue a, ");
      qs.append("app_status appstat, ");
      qs.append("application app, ");
      qs.append(subTables);
      qs.append(" where ");
      qs.append("a.app_seq_num = ? and ");
      qs.append("a.app_queue_type = ? and ");
      qs.append("a.app_status = appstat.status_type and ");
      qs.append("a.app_seq_num = app.app_seq_num ");
      qs.append(subConditions);
      
      pstmt = getPreparedStatement(qs.toString());        
      pstmt.setLong(1, queueKey);
      pstmt.setInt(2, queueType);
        
      queueResultSet = pstmt.executeQuery();
        
      success = true;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getQueueData: " + e.toString());
    }
    
    return success;
  }
  
  public String getUserTypeDescription(long userType)
  {
    String description = "Unknown User Type";
    
    switch((int)userType)
    {
      case com.mes.constants.mesConstants.USER_MERCHANT:
        description = "Merchant";
        break;
      case com.mes.constants.mesConstants.USER_MES:
        description = "MES Employee";
        break;
      case com.mes.constants.mesConstants.USER_BANK:
        description = "Community Bank";
        break;
      case com.mes.constants.mesConstants.USER_SALES:
        description = "MES Sales";
        break;
      case com.mes.constants.mesConstants.USER_DEFAULT:
        description = "Generic User";
        break;
    }
    
    return description;
  }
  
  public boolean getContactInfo(long userType, long appSeqNum)
  {
    boolean             success     = false;
    StringBuffer        qs          = null;
    
    switch((int)userType)
    {
      case com.mes.constants.mesConstants.USER_MERCHANT:
        qs = getMerchantContactInfo(appSeqNum);
        break;
        
      case com.mes.constants.mesConstants.USER_SALES:
        qs = getSalesContactInfo(appSeqNum);
        break;
        
      case com.mes.constants.mesConstants.USER_MES:
      case com.mes.constants.mesConstants.USER_BANK:
      case com.mes.constants.mesConstants.USER_DEFAULT:
      default:
        qs = getUserContactInfo(appSeqNum);
        break;
    }
    
    try
    {
      if(qs != null)
      {
        if(con == null)
        {
          con = db.getConnection(DB_CONNECT_STRING);
        }
      
        if(con != null)
        {
          pstmt = con.prepareStatement(qs.toString());
          pstmt.setLong(1, appSeqNum);
          queueResultSet = pstmt.executeQuery();
        
          success = true;
        }
        else
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getContactInfo: " + db.getErrorMessage());
        } 
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getContactInfo: Empty query string");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getContactInfo: " + e.toString());
    }
    
    return success;
  }
  
  protected StringBuffer getUserContactInfo(long appSeqNum)
  {
    StringBuffer        qs          = new StringBuffer("");
    
    qs.append("select a.* from orguser a, app_queue b ");
    qs.append("where a.user_id = b.app_user and b.app_seq_num = ?");
    
    return qs;
  }
  
  protected StringBuffer getMerchantContactInfo(long appSeqNum)
  {
    StringBuffer        qs          = new StringBuffer("");
    
    qs.append("select a.merch_mailing_name, a.merch_business_name, a.merch_email_address, a.merch_address_attn, b.* ");
    qs.append("from merchant a, address b ");
    qs.append("where a.app_seq_num = b.app_seq_num and b.addresstype_code = 1 and a.app_seq_num = ?");
    
    return qs;
  }
  
  protected StringBuffer getSalesContactInfo(long appSeqNum)
  {
    StringBuffer        qs          = new StringBuffer("");
    
    qs.append("select a.user_email_address, b.* ");
    qs.append("from orguser a, sales_rep b, app_queue c ");
    qs.append("where a.user_id = b.user_id and a.user_id = c.app_user and c.app_seq_num = ?");
        
    return qs;
  }
  
  public void moveExpiredAccounts()
  {
    PreparedStatement   ps = null;
    
    try
    {
      if(con == null)
      {
        con = db.getConnection(DB_CONNECT_STRING);
      }
    
      if(con != null)
      {      
        StringBuffer qs = new StringBuffer("");


        qs.append("update app_queue             ");
        qs.append("set app_queue_stage  = ?     ");
        qs.append("where app_queue_type = ? and ");
        qs.append("app_queue_stage      = ? and ");
        qs.append("(months_between(sysdate,app_start_date) > .25)  ");

        
        ps = getPreparedStatement(qs.toString());
        
        ps.setInt(1, QueueConstants.Q_SETUP_COMPLETE);
        ps.setInt(2, QueueConstants.QUEUE_SETUP);
        ps.setInt(3, QueueConstants.Q_SETUP_QA);
        ps.executeUpdate();
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "moveExpiredAccounts: " + db.getErrorMessage());
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "moveExpiredAccounts: " + e.toString());
    }
    finally
    {
      if(ps != null)
      {
        try
        {
          ps.close();
        }
        catch (Exception e)
        {
          com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "moveExpiredAccounts: " + e.toString());
        }
      }
    }
  }

  public boolean getQueueFiltered(String subTable)
  {
    boolean             retVal                          = false;
    StringBuffer        qs                              = new StringBuffer("");
    int                 curParam                        = 2;
    int                 dateFromParam                   = 0;
    int                 dateToParam                     = 0;
    int                 controlParam                    = 0;
    
    // build the query string
    /* NEW VERSION THAT INCLUDES GROUP INFO
    qs.append("select distinct ");
    qs.append("a.app_seq_num, ");
    qs.append("a.merc_cntrl_number, ");
    qs.append("a.merch_business_name, ");
    qs.append("a.merch_prior_cc_accp_flag, ");
    qs.append("e.group_id, ");
    qs.append("app.appsrctype_code, ");
    qs.append("b.*, ");
    qs.append("c.status_short_desc, ");
    qs.append("d.* ");
    qs.append("from merchant a, application app, app_queue b, app_status c, " + subTable + " d, user_to_group e ");
    qs.append("where ");
    qs.append("a.app_seq_num = b.app_seq_num and ");
    qs.append("a.app_seq_num = d.app_seq_num and ");
    qs.append("a.app_seq_num = app.app_seq_num and ");
    qs.append("app.app_user_id = e.user_id(+) and ");
    qs.append("b.app_status = c.status_type ");
    qs.append("and b.app_queue_type = ? and ");
    qs.append("b.app_queue_stage = ? and ");
    qs.append("904 = e.group_id(+) ");
    */
    
    qs.append("select  m.merc_cntrl_number              merc_cntrl_number,        ");
    qs.append("m.app_seq_num                            app_seq_num,              ");
    qs.append("m.merch_business_name                    merch_business_name,      ");
    qs.append("m.merch_number                           merch_number,             ");
    qs.append("app.app_user_id                          app_user,                 ");
    qs.append("app.app_user_login                       app_source,               ");
    qs.append("app.appsrctype_code                      appsrctype_code,          ");
    qs.append("aq.app_last_user                         app_last_user,            ");
    qs.append("aq.app_last_date                         app_last_date,            ");
    qs.append("aq.app_queue_lock                        app_queue_lock,           ");
    qs.append("aq.app_start_date                        app_start_date,           ");
    qs.append("aq.app_status_reason                     app_status_reason,        ");
    qs.append("aq.is_rush                               app_is_rush,              ");
    qs.append("aps.status_short_desc                    status_short_desc,        ");
    qs.append("decode(qd.date_created,null,'NO','YES')  printed,                  ");
    qs.append("st.*                                                               ");
    qs.append("from         merchant    m,                                        ");
    qs.append("application  app,                                                  ");
    qs.append("app_queue    aq,                                                   ");
    qs.append("app_status   aps,                                                  ");
    qs.append("q_data       qd,                                                   ");
    qs.append(subTable);
    qs.append(" st ");
    qs.append("where    m.app_seq_num       = app.app_seq_num and                 ");
    qs.append("         m.app_seq_num       = st.app_seq_num(+) and               ");
    qs.append("         m.app_seq_num       = aq.app_seq_num and                  ");
    qs.append("         (m.app_seq_num      = qd.id(+) and 1302 = qd.type(+)) and ");
    qs.append("         aq.app_queue_type   = ? and                               ");
    qs.append("         aq.app_queue_stage  = ? and                               ");
    qs.append("         aq.app_status       = aps.status_type                     ");
    
    // add filtering for dates
    if(queryDateFrom != null && queryDateTo != null && queryDateTo.after(queryDateFrom))
    {
      qs.append(" and aq.app_start_date between ? and ?");
      dateFromParam = ++curParam;
      dateToParam = ++curParam;
    }
    
    // add filtering for control number
    if(queryControlNumber > 0L)
    {
      qs.append(" and m.merc_cntrl_number = ?");
      controlParam = ++curParam;
    }
    
    // add filtering for application source
    if(!appSource.equals(""))
    {
      qs.append(" and upper(aq.app_source) like '%" + appSource.toUpperCase() + "%' ");
    }
    
    // add filtering for merchant DBA name
    if(!merchDBA.equals(""))
    {
      qs.append(" and upper(m.merch_business_name) like '%");
      qs.append(merchDBA.toUpperCase());
      qs.append("%'");
    }
    
    if(queueStage == QueueConstants.Q_SETUP_QA)
    {
      qs.append(" order by aq.is_rush desc, aq.app_last_date asc");
    }
    else
    {
      qs.append(" order by aq.is_rush desc, aq.app_start_date asc");
    }
    
    System.out.println(qs.toString());
    
    // ready to create the prepared statement
    try
    {
      pstmt = getPreparedStatement(qs.toString());
      
      if(pstmt != null)
      {
        pstmt.setInt(1, queueType);
        pstmt.setInt(2, queueStage);
      
        // set creation date parameters
        if(dateFromParam > 0)
        {
          pstmt.setDate(dateFromParam, queryDateFrom);
          pstmt.setDate(dateToParam, queryDateTo);
        }
      
        // set control number parameter
        if(controlParam > 0)
        {
          pstmt.setLong(controlParam, queryControlNumber);
        }
      
        // execute statement
        queueResultSet = pstmt.executeQuery();
        
        retVal = true;
      }
      else
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getQueueFiltered: statement was null");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(1, this.getClass().getName(), "getQueueFiltered: " + e.toString());
    }
    
    return retVal;
  }
  
  public void cleanUp()
  {
    // clean up resources

//@@    if(queueResultSet != null && !queueResultSet.is)
//@@    {
//@@      try
//@@      {
//@@        queueResultSet.close();
//@@      }
//@@      catch (SQLException e)
//@@      {
//@@        com.mes.support.SyncLog.LogEntry(1, this.getClass().getName(), "Closing resultSet: " + e.toString());
//@@      }
//@@    }
//@@    
//@@    if(pstmt != null)
//@@    {
//@@      try
//@@      {
//@@        pstmt.clos;
//@@      }
//@@      catch (SQLException e)
//@@      {
//@@        com.mes.support.SyncLog.LogEntry(1, this.getClass().getName(), "Closing prepared statement: " + e.toString());
//@@      }
//@@    }
    
    if(db != null)
    {
      try
      {
        db.releaseConnection();
      }
      catch (Exception e)
      {
        com.mes.support.SyncLog.LogEntry(1, this.getClass().getName(), "Closing DBConnect: " + e.toString());
      }
    }
  }
  
  public void cleanUp(String msg)
  {
    if(db != null)
    {
      try
      {
        db.releaseConnection();
      }
      catch (Exception e)
      {
        com.mes.support.SyncLog.LogEntry(1, this.getClass().getName(), "Closing DBConnect: " + e.toString());
      }
    }
  }
  
  // accessors
  public ResultSet getQueueResultSet()
  {
    return this.queueResultSet;
  }
  
  public void setDateFrom(String dateFrom)
  {
    this.dateFrom = dateFrom;
    queryDateFrom = StrToDate(dateFrom, false);
  }
  
  public String getDateFrom()
  {
    return this.dateFrom;
  }
  
  public void setDateTo(String dateTo)
  {
    this.dateTo = dateTo;
    queryDateTo = StrToDate(dateTo, true);
  }
  
  public String getDateTo()
  {
    return this.dateTo;
  }
  
  public void setControlNumber(long controlNumber)
  {
    queryControlNumber = controlNumber;
  }
  
  public void setControlNumber(String controlNumber)
  {
    try
    {
      this.controlNumber = controlNumber;
      queryControlNumber = Long.parseLong(controlNumber);
    }
    catch (NumberFormatException e)
    {
      this.controlNumber = "";
      queryControlNumber = 0L;
    }
  }
  
  public String getControlNumber()
  {
    return this.controlNumber;
  }
  
  public void setAppSource(String appSource)
  {
    this.appSource = appSource;
  }
  
  public String getAppSource()
  {
    return this.appSource;
  }
  
  public void setMerchDBA(String merchDBA)
  {
    this.merchDBA = merchDBA;
  }
  
  public String getMerchDBA()
  {
    return this.merchDBA;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  
  public String getMerchNum()
  {
    return this.merchNum;
  }
  
  public void setQueueType(String queueType)
  {
    try
    {
      setQueueType(Integer.parseInt(queueType));
    }
    catch(Exception e)
    {
    }
  }
  public void setQueueType(int queueType)
  {
    this.queueType = queueType;
  }
  
  public int getQueueType()
  {
    return this.queueType;
  }
  
  public void setQueueStage(String queueStage)
  {
    try
    {
      setQueueStage(Integer.parseInt(queueStage));
    }
    catch(Exception e)
    {
    }
  }
  public void setQueueStage(int queueStage)
  {
    this.queueStage = queueStage;
  }
  
  public int getQueueStage(int queueStage)
  {
    return this.queueStage;
  }

  public int getQueueStage()
  {
    return this.queueStage;
  }
  
}
