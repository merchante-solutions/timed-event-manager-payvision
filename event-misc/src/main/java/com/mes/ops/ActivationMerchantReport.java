/*@lineinfo:filename=ActivationMerchantReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ActivationMerchantReport.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 11/21/02 4:40p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import com.mes.constants.MesQueues;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesCalendar;
import com.mes.support.MesMath;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class ActivationMerchantReport extends DateSQLJBean
{
  private Date fromSqlDate  = null;
  private Date toSqlDate    = null; 
  private long fromLongDate = -1L;
  private long toLongDate   = -1L; 

  private HashMap merchantStatHash  = new HashMap();
  private Vector  merchantList      = new Vector();
  private int     numberCompleted   = 0;
  private int     numberInQueue     = 0;
  private double  totalElpasedDays  = 0.0;

  public ActivationMerchantReport()
  {
  }

  private void setDates()
  {
    //set the from date and too date.. 
    setFromSqlDate(getSqlFromDate());
    setToSqlDate(getSqlToDate());
    setFromLongDate(getFromDate().getTime());
    setToLongDate(getToDate().getTime());
  }

  //gets data to build report
  public void getData()
  {
    ResultSetIterator   it         = null;
    ResultSet           rs         = null;

    //clear hashmap and vector.. get fresh data
    merchantStatHash  = new HashMap();
    merchantList      = new Vector();
    totalElpasedDays  = 0.0;
    numberCompleted   = 0;
    numberInQueue     = 0;

    setDates();

    try
    {
      connect();
     
      //get general info and appointments
      /*@lineinfo:generated-code*//*@lineinfo:93^7*/

//  ************************************************************
//  #sql [Ctx] it = { select sd.id,
//                 sd.user_id, 
//                 m.merch_business_name, 
//                 sd.function_id,
//                 to_date(decode(qd1.date_created, null, decode(qd2.date_created, null, decode(qd3.date_created, null, decode(qd4.date_created, null, null, qd4.date_created), qd3.date_created), qd2.date_created), qd1.date_created)) date_in_queue,
//                 qd4.last_changed  date_completed
//          from  schedule_data         sd,
//                merchant              m,
//                q_data                qd1,
//                q_data                qd2,
//                q_data                qd3,
//                q_data                qd4
//          where sd.type = 1 and 
//                sd.id between :fromLongDate and :toLongDate and
//                sd.function_id != :ScheduleIntervalBean.BLOCK_OUT_FUNCTION_ID and
//                sd.function_id = m.merch_number and
//                (m.app_seq_num = qd1.id(+) and :MesQueues.Q_ACTIVATION_NEW = qd1.type(+))       and
//                (m.app_seq_num = qd2.id(+) and :MesQueues.Q_ACTIVATION_PENDING = qd2.type(+))   and
//                (m.app_seq_num = qd3.id(+) and :MesQueues.Q_ACTIVATION_SCHEDULED = qd3.type(+)) and
//                (m.app_seq_num = qd4.id(+) and :MesQueues.Q_ACTIVATION_COMPLETED = qd4.type(+))
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sd.id,\n               sd.user_id, \n               m.merch_business_name, \n               sd.function_id,\n               to_date(decode(qd1.date_created, null, decode(qd2.date_created, null, decode(qd3.date_created, null, decode(qd4.date_created, null, null, qd4.date_created), qd3.date_created), qd2.date_created), qd1.date_created)) date_in_queue,\n               qd4.last_changed  date_completed\n        from  schedule_data         sd,\n              merchant              m,\n              q_data                qd1,\n              q_data                qd2,\n              q_data                qd3,\n              q_data                qd4\n        where sd.type = 1 and \n              sd.id between  :1  and  :2  and\n              sd.function_id !=  :3  and\n              sd.function_id = m.merch_number and\n              (m.app_seq_num = qd1.id(+) and  :4  = qd1.type(+))       and\n              (m.app_seq_num = qd2.id(+) and  :5  = qd2.type(+))   and\n              (m.app_seq_num = qd3.id(+) and  :6  = qd3.type(+)) and\n              (m.app_seq_num = qd4.id(+) and  :7  = qd4.type(+))";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.ActivationMerchantReport",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,fromLongDate);
   __sJT_st.setLong(2,toLongDate);
   __sJT_st.setString(3,ScheduleIntervalBean.BLOCK_OUT_FUNCTION_ID);
   __sJT_st.setInt(4,MesQueues.Q_ACTIVATION_NEW);
   __sJT_st.setInt(5,MesQueues.Q_ACTIVATION_PENDING);
   __sJT_st.setInt(6,MesQueues.Q_ACTIVATION_SCHEDULED);
   __sJT_st.setInt(7,MesQueues.Q_ACTIVATION_COMPLETED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.ActivationMerchantReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:116^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {

        String merchant = rs.getString("function_id");

        //adds employee to vector containing list of employees (each entry unique)
        addMerchant(merchant);
  
        MerchantStats stats = null;

        //gets rec for merchant.. gets old one if already exists or starts new one
        stats = getRecFromHash(merchant, true);
        
        if(!stats.isGeneralDataSet())
        {
          stats.setMerchantName(rs.getString("merch_business_name"));
          stats.setMerchantNumber(rs.getString("function_id"));

          boolean startTimeOk = false;
          boolean endTimeOk   = false;
          if(rs.getDate("date_in_queue") == null)
          {
            stats.setActivationStartDate("---");
            startTimeOk = false;
          }
          else
          {
            stats.setActivationStartDate(DateTimeFormatter.getFormattedDate(rs.getTimestamp("date_in_queue"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
            startTimeOk = true;
          }

          if(rs.getDate("date_completed") == null)
          {
            stats.setActivationEndDate("---");
            endTimeOk = false;
          }
          else
          {
            numberCompleted++;
            stats.setActivationEndDate(DateTimeFormatter.getFormattedDate(rs.getTimestamp("date_completed"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));
            endTimeOk = true;
          }


          if(startTimeOk && endTimeOk)
          {
            stats.setActivationElapseTime( getElapsedDays(MesCalendar.millisecsBetween(rs.getDate("date_in_queue"), rs.getDate("date_completed"))) + " Days");
            totalElpasedDays += getElapsedDays(MesCalendar.millisecsBetween(rs.getDate("date_in_queue"), rs.getDate("date_completed")));
            //System.out.println("Total Elapsed Days: " + totalElpasedDays);
          }
          else
          {
            stats.setActivationElapseTime("---");
            if(startTimeOk)
            {
              totalElpasedDays += getElapsedDays(MesCalendar.millisecsBetween(rs.getDate("date_in_queue"), (new Date(ScheduleBaseBean.getCurrentTimeLong()))));
              //System.out.println("Total Elapsed Days: " + totalElpasedDays);
            }
          }
        }

        stats.setAppointment(DateTimeFormatter.getFormattedDate(new Date(rs.getLong("id")), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT), rs.getString("user_id"));

        putRecInHash(stats, merchant);
    
        stats = null;
        
      }
      
      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:192^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    sca.*, 
//                    sc.notes
//          from      service_call_activation sca,
//                    service_calls sc
//          where     trunc(sca.call_date) between :fromSqlDate and :toSqlDate and
//                    sca.sequence = sc.sequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sca.*, \n                  sc.notes\n        from      service_call_activation sca,\n                  service_calls sc\n        where     trunc(sca.call_date) between  :1  and  :2  and\n                  sca.sequence = sc.sequence";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.ActivationMerchantReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.ActivationMerchantReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        String merchant = rs.getString("merchant_number");

        MerchantStats stats = null;

        //gets rec for merchant.. null if not found
        stats = getRecFromHash(merchant, false);
        
        if(stats != null)
        {
          stats.setCallStatus(DateTimeFormatter.getFormattedDate(rs.getTimestamp("call_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT), rs.getString("login_name"), rs.getString("activation_status"), rs.getString("notes"));
          putRecInHash(stats, merchant);
        }
    
        stats = null;
      }
      
      rs.close();
      it.close();


      /*@lineinfo:generated-code*//*@lineinfo:226^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    count(app_seq_num)
//  
//          from      temp_activation_queue
//  
//          where     trunc(DATE_IN_QUEUE) between :fromSqlDate and :toSqlDate and
//                    q_type = :QueueConstants.QUEUE_ACTIVATION
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    count(app_seq_num)\n\n        from      temp_activation_queue\n\n        where     trunc(DATE_IN_QUEUE) between  :1  and  :2  and\n                  q_type =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.ActivationMerchantReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   __sJT_st.setInt(3,QueueConstants.QUEUE_ACTIVATION);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.ActivationMerchantReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:234^7*/

      rs = it.getResultSet();
    
      if(rs.next())
      {
        numberInQueue = rs.getInt(1);
      }
      
      rs.close();
      it.close();

    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private double getElapsedDays(long elapsedTime)
  {
    return MesMath.round( ((double)((double)elapsedTime / (float)MesCalendar.ONE_DAY_MS)), 1 );
  }

  private void putRecInHash(MerchantStats stats, String merchant)
  {
    merchantStatHash.put(merchant, stats);
  }

  public MerchantStats getRecFromHash(String merchant, boolean startNewOk)
  {
    if(merchantStatHash.containsKey(merchant))
    {
      return ((MerchantStats)merchantStatHash.get(merchant));
    }
    else if(startNewOk)
    {
      return (new MerchantStats(merchant));
    }
    else
    {
      return null;
    }
  }


  private void addMerchant(String merchant)
  {
    if(!merchantList.contains(merchant))
    {
      merchantList.add(merchant);
    }
  }      
      
  public Vector getMerchantListVector()
  {
    return this.merchantList;
  }
 
  public int getNumberOfMerchants()
  {
    return this.merchantList.size();
  }
          
  public int getNumberCompleted()
  {
    return this.numberCompleted;
  }        
                
  public int getNumberInQueue()
  {
    return this.numberInQueue;
  }

  public double getAveElapsedDays()
  {
    return MesMath.round((this.totalElpasedDays / getNumberOfMerchants()), 1);
  }                
                            
  public void setFromSqlDate(Date  fromDate)
  {
    this.fromSqlDate = fromDate;
  }

  public void setToSqlDate(Date toDate)
  {
    this.toSqlDate = toDate;
  }

  public void setFromLongDate(long fromDate)
  {
    this.fromLongDate = fromDate;
  }
  public void setToLongDate(long toDate)
  {
    this.toLongDate = toDate;
  } 

  public class MerchantStats
  {
    private String merchantName         = "";
    private String merchantNumber       = "";
    private String activationStartDate  = "";
    private String activationEndDate    = "";
    private String activationElapseTime = "";

    private Vector appointments         = new Vector();
    private Vector callStatuses         = new Vector();

    
    public MerchantStats(String merchantNumber)
    {
      this.merchantNumber = merchantNumber;
    }

    public boolean isGeneralDataSet()
    {
      boolean result = false;
      
      if(this.merchantName != null && !this.merchantName.equals("") && this.merchantNumber != null && !this.merchantNumber.equals(""))
      {
        result = true;
      }

      return result;
    }

    //getter
    public String getMerchantName()
    {
      return this.merchantName;
    }

    public String getMerchantNumber()
    {
      return this.merchantNumber;
    }

    public String getActivationStartDate()
    {
      return this.activationStartDate;
    }
    
    public String getActivationEndDate()
    {
      return this.activationEndDate;
    }

    public String getActivationElapseTime()
    {
      return this.activationElapseTime;
    }

    //setters
    public void setMerchantName(String merchantName)
    {
      this.merchantName = merchantName;
    }
    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = merchantNumber;
    }

    public void setActivationStartDate(String activationStartDate)
    {
      this.activationStartDate = activationStartDate;
    }
    
    public void setActivationEndDate(String activationEndDate)
    {
      this.activationEndDate = activationEndDate;
    }

    public void setActivationElapseTime(String activationElapseTime)
    {
      this.activationElapseTime = activationElapseTime;
    }

    public void setAppointment(String dateTime, String user)
    {
      Appointment appt = new Appointment(dateTime,user);
      appointments.add(appt);
    }

    public Vector getAppointments()
    {
      return this.appointments;
    }

    public void setCallStatus(String dateTime, String user, String status, String note)
    {
      CallStatus call = new CallStatus(dateTime,user,status,note);
      callStatuses.add(call);
    }

    public Vector getCallStatuses()
    {
      return this.callStatuses;
    }

    //appointments class
    public class Appointment 
    {
      private String apptDateTime = "";
      private String apptUser     = "";

      public Appointment(String dateTime, String user)
      {
        this.apptDateTime = dateTime;
        this.apptUser     = user;
      }

      public String getApptDateTime()
      {
        return this.apptDateTime;
      }
      public String getApptUser()
      {
        return this.apptUser;
      }

    }
   
    //status and notes class
    public class CallStatus
    {
      private String callDateTime = "";
      private String callUser     = "";
      private String callStatus   = "";
      private String callNote     = "";

      public CallStatus(String dateTime, String user, String status, String note)
      {
        this.callDateTime = dateTime;
        this.callUser     = user;
        this.callStatus   = status;
        this.callNote     = note;
      }

      public String getCallDateTime()
      {
        return this.callDateTime;
      }
      
      public String getCallUser()
      {
        return this.callUser;
      }
      
      public String getCallStatus()
      {
        return this.callStatus;
      }
      
      public String getCallNote()
      {
        return this.callNote;
      }
    }
  }
}/*@lineinfo:generated-code*/