/*@lineinfo:filename=MmsExpandedFieldsBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/MmsExpandedFieldsBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/14/04 4:09p $
  Version            : $Revision: 18 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class MmsExpandedFieldsBean extends SQLJConnectionBase
{

  private final static String TERMAPP_STAGE               = mesConstants.MMS_STAGE_BUILD_TERM_APP;
  private final static String TERMAPP_VERICENTRE          = mesConstants.DEFAULT_TERMAPP_VERICENTRE;
  private final static String TERMAPP_TERMMASTER          = mesConstants.DEFAULT_TERMAPP_TERMMASTER;
  private final static String TERMAPP_DIALPAY             = "DIALPAY";
  private final static String TERMAPP_VSVR035             = "VSVR035";
  private final static String TERMAPP_VT13DA7             = "VT13DA7";
  private final static String TERMAPP_VT1TP08             = "VT1TP08";
  private final static String TERMAPP_VT1TPA7             = "VT1TPA7";
  private final static String TERMAPP_VT1TPH6             = "VT1TPH6";
  private final static String TERMAPP_VT13DA8             = "VT13DA8"; 
  private final static String TERMAPP_VT1H385             = "VT1H385";
  private final static String TERMAPP_VT46DA4             = "VT46DA4";
  private final static String TERMAPP_VSTDB01             = "VSTDB01";
  private final static String TERMAPP_DRR                 = "DRR";
  private final static String TERMAPP_RR                  = "RR";
  private final static String TERMAPP_DHTL                = "DHTL";
  private final static String TERMAPP_HTL                 = "HTL";
  private final static String TERMAPP_VXP0P16             = "VXP0P16";
  private final static String TERMAPP_52C8072             = "52C8072";



  public  final static String ALASKA_OPTION               = "3";
  public  final static String VISA_CHECK_CARD2            = "5";
  public  final static String SHAZAM                      = "7";
  public  final static String MAESTRO                     = "8";
  public  final static String ACCEL                       = "E";
  public  final static String INTERLINK                   = "G";
  public  final static String TYME                        = "H";
  public  final static String EBT_NETWORK                 = "K";
  public  final static String PULSE                       = "L";
  public  final static String CASH_STATION                = "N";
  public  final static String STAR                        = "Q";
  public  final static String MONEY_STATION               = "S";
  public  final static String AFFN                        = "U";
  public  final static String VISA                        = "V";
  public  final static String HONOR                       = "W";
  public  final static String INFYANK24NYCE               = "Y";
  public  final static String MAC                         = "Z";

  // TermMaster Options (on/off switches)
  public  final static String TMOPT_PASSWORDPROTECT       = "P";
  public  final static String TMOPT_REFUND                = "R";
  public  final static String TMOPT_SETTLE                = "S";
  public  final static String TMOPT_ADJUSTMENT            = "A";
  public  final static String TMOPT_VOID                  = "V";
  public  final static String TMOPT_OFFLINETRANSACTIONS   = "O";
  public  final static String TMOPT_REPORTS               = "T";

  private String              applicationShown            = TERMAPP_STAGE;
  private String              printerTypeModel            = "";
  private String              pinpadTypeAvailable         = "";

  private String              atmCancelReturn             = "";
  private String              atmSignLine                 = "";
  private String              autoCloseInd                = "";
  private String              autoCloseReports            = "";
  private String              autoCloseTime               = "";
  private String              autoCloseTime2              = "";
  private String              autoTipPercent              = "";
  private String              avs                         = "";
  private String              cardPresent                 = "";
  private String              cashBack                    = "";
  private String              cashBackLimit               = "";
  private String              cashBackTip                 = "";
  private String              checkAuthPhone              = "";
  private String              checkMid                    = "";
  private String              checkPayment                = "";
  private String              checkTotalsInd              = "";
  private String              checkVoice                  = "";
  private String              dupTran                     = "";
  private String              ebtTransType                = "";
  private String              ebtFcsId                    = "";
  private String              folioGuestCheckProcessing   = "";
  private String              folioRoomPrompt             = "";
  private String              footerLine1                 = "";
  private String              footerLine2                 = "";
  private String              forceDial                   = "";
  private String              forceSettleInd              = "";
  private String              fraud                       = "";
  private String              headerLine4                 = "";
  private String              headerLine5                 = "";
  private String              idlePrompt                  = "";
  private String              invNumKeys                  = "";
  private String              invoiceNumber               = "";
  private String              invPrompt                   = "";
  private String              manufacturer                = "";
  private String              merchantAba                 = "";
  private String              merchantSettlementAgent     = "";
  private String              parentVnumber               = "";
  private String              passProtect                 = "";
  private String              pinpadAvailable             = "";
  private String              pinpadType                  = "";
  private String              primaryAuthDialAccess       = "";
  private String              primaryDataCapturePhone     = "";
  private String              primaryDialAccess           = "";
  private String              primaryKey1                 = "";
  private String              primaryKey2                 = "";
  private String              printerModel                = "";
  private String              printerType                 = "";
  private String              promptForClerk              = "";
  private String              purchasingCard              = "";
  private String              receiptTrunc                = "";
  private String              reimbursementAttribute      = "";
  private String              reminderMsg                 = "";
  private String              reminderMsgInd              = "";
  private String              reminderMsgTime             = "";
  private String              restaurantProcessing        = "";
  private String              secondaryAuthDialAccess     = "";
  private String              secondaryDataCapturePhone   = "";
  private String              secondaryDialAccess         = "";
  private String              settleToPc                  = "";
  private String              sharingGroups               = "";
  private String              srvclkProcessing            = "";
  private String              surchargeInd                = "";
  private String              surchargeAmount             = "";
  private String              termModel                   = "";
  private String              tipAssist                   = "";
  private String              tipAssistPercent1           = "";
  private String              tipAssistPercent2           = "";
  private String              tipAssistPercent3           = "";
  private String              tipOption                   = "";
  private String              tipTimeSale                 = "";

  // Vericentre spec fields
  private String              timeDifference              = "";
  private String              eciType                     = "";
  private String              clerkServerMode             = "";
  private String              overagePercentage           = "";
  private String              checkReader                 = "";
  private String              riid                        = "";
  private String              checkHost                   = "";
  private String              checkHostFormat             = "";
  private String              amexOption                  = "";
  private String              merchantIdAmex              = "";
  private String              baudRateHost1               = "";
  private String              baudRateAmexHost            = "";
  private String              amexPabx                    = "";
    
  // TermMaster spec fields
  private String              debitSettleNum              = "";
  private String              downloadSpeed               = "";
  private String              dialSpeed                   = "";
  private String              tmOptions                   = "";
  
  public  Vector              errors                      = new Vector();

  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }

  public void addError(String err)
  {
    try
    {
      errors.add(err);
    }
    catch(Exception e)
    {
    }
  } 

  public boolean validate(String termApp)
  {
    //if its not one of these.. then we cant submit it to mms
    if(!termApp.equals(TERMAPP_VSVR035) && 
       !termApp.equals(TERMAPP_VT13DA7) && 
       !termApp.equals(TERMAPP_VT13DA8) && 
       !termApp.equals(TERMAPP_VT1H385) && 
       !termApp.equals(TERMAPP_DRR)     && 
       !termApp.equals(TERMAPP_RR)      && 
       !termApp.equals(TERMAPP_DHTL)    && 
       !termApp.equals(TERMAPP_HTL)     && 
       !termApp.equals(TERMAPP_VT46DA4) && 
       !termApp.equals(TERMAPP_VT1TP08) &&
       !termApp.equals(TERMAPP_VT1TPA7) &&
       !termApp.equals(TERMAPP_VT1TPH6) &&
       !termApp.equals(TERMAPP_VXP0P16) &&
       !termApp.equals(TERMAPP_52C8072) &&
       !termApp.equals(TERMAPP_VSTDB01) &&
       !termApp.equals(TERMAPP_STAGE)   &&
       !termApp.equals(TERMAPP_VERICENTRE) &&
       !termApp.equals(TERMAPP_TERMMASTER))

    {
      //addError("Cannot yet submit applications for terminal application " + termApp);
    }

    if(termApp.equals(TERMAPP_VSVR035))
    {
      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }
      else if(!isBlank(termModel))
      {
        if(termModel.equals("OMNI 395") || termModel.equals("OMNI 396"))
        {
          if(printerTypeModel.equals("2*INTEGRATED"))
          {
            addError("Printer Model not valid for Terminal Model " + termModel);
          }
        }
        else if(termModel.equals("OMNI 470") || termModel.equals("OMNI 3200"))
        {
          if(!printerTypeModel.equals("2*INTEGRATED"))
          {
            addError("Integrated Printer Model must be selected for Terminal Model " + termModel);
          }
        }
      }

      if(isBlank(fraud))
      {
        addError("Please specify Fraud option");
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(!isBlank(checkPayment) && isBlank(checkMid))
      {
        addError("If selecting a Check Payment Type, please specify Check Mid.");
      }
       
      if(!isBlank(checkPayment) && isBlank(checkVoice))
      {
        addError("If selecting a Check Payment Type, please specify Check Voice Id #.");
      }

      if(!isBlank(checkPayment) && isBlank(checkAuthPhone))
      {
        addError("If selecting a Check Payment Type, please specify Check Authorization Phone #.");
      }

      if(isBlank(checkPayment) && (!isBlank(checkMid) || !isBlank(checkVoice) || !isBlank(checkAuthPhone)))
      {
        addError("If not selecting a Check Payment Type then Check Mid, Check Voice Id #, and Check Authorization Phone # must all be blank.");
      }


      if(isBlank(tipOption))
      {
        addError("Please specify Tip Option");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(invoiceNumber))
      {
        addError("Please specify Invoice Number");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }


      if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
      {
        addError("Please specify an Auto Close Time in military format 0001 - 2359");
      }
      else if(autoCloseInd.equals("0") && !isBlank(autoCloseTime))
      {
        addError("If No Auto Close selected, Auto Close Time must be blank");
      }
      else if(!isBlank(autoCloseTime))
      {
        try
        {
          int tempClose = Integer.parseInt(autoCloseTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Auto Close Time must be in military format (0001 - 2400)");
        }
      }



      //latest additions
      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 25)
      {
        addError("Header Line 4 can be no more than 25 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 25)
      {
        addError("Header Line 5 can be no more than 25 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 40)
      {
        addError("Footer Line 1 can be no more than 40 characters");
      }
      if(!isBlank(this.footerLine2) && this.footerLine2.length() > 40)
      {
        addError("Footer Line 2 can be no more than 40 characters");
      }

      if(isBlank(this.surchargeInd))
      {
        addError("Please select whether there is a surcharge or no surcharge");
      }
      else if(this.surchargeInd.equals("Y"))
      {
        if(isBlank(this.surchargeAmount))
        {
          addError("Please specify a surcharge amount");
        }
        else if(!isValidSurcharge(this.surchargeAmount))
        {
          addError("Please provide a valid surcharge amount greater than 0 and less than 10");
        }
      }
      //end of latest additions






      
      if(isBlank(merchantAba))
      {
        addError("Please provide a Merchant ABA #");
      }
      else if(merchantAba.length() != 9)
      {
        addError("Merchant ABA # must be 9 digits long");
      }
      else if(!isNumber(merchantAba))
      {
        addError("Merchant ABA # must be a valid 9 digit number");
      }
      else if(!merchantAba.equals("000000000"))
      {
        if(isBlank(merchantSettlementAgent))
        {
          addError("Please specify Merchant Settlement Agent");
        }
      
        if(isBlank(reimbursementAttribute))
        {
          addError("Please specify Reimbursement Attribute");
        }

        if(isBlank(atmSignLine))
        {
          addError("Please specify ATM Receipt Signature Line");
        }
         
        if(isBlank(cashBack))
        {
          addError("Please specify Cash Back");
        }

        if(isBlank(pinpadTypeAvailable))
        {
          addError("Please select a Pinpad Model");
        }
        else if(!termModel.equals("OMNI 470") && pinpadTypeAvailable.equals("3*4"))
        {
          addError("Pin Pad 0470 can only be selected if setting up an OMNI 470 terminal");
        }

        if(isBlank(ebtTransType))
        {
          addError("Please select an EBT Trans Type");
        }

        if((ebtTransType.equals("1") || ebtTransType.equals("3")) && isBlank(ebtFcsId))
        {
          addError("Please provide an EBT FCS Id");
        }
        else if((ebtTransType.equals("1") || ebtTransType.equals("3")) && !isBlank(ebtFcsId) && !isNumber(ebtFcsId))
        {
          addError("Please provide a valid EBT FCS Id number");
        }
        else if(!ebtTransType.equals("1") && !ebtTransType.equals("3"))
        {
          this.ebtFcsId = "0000000";
        }

        if(isBlank(sharingGroups))
        {
          addError("Please select the debit sharing groups used");
        }
        else if(sharingGroups.length() >10)
        {
          addError("No More than 10 sharing groups can be selected.  Visa must be one of them.");
        }
        else
        {
          sharingGroups = fixSharingGroups();
        }

      }
      else if(merchantAba.equals("000000000"))
      {
        this.merchantSettlementAgent  = "V000";
        this.reimbursementAttribute   = "0";
        this.atmSignLine              = "0";
        this.cashBack                 = "0";
        this.pinpadTypeAvailable      = "0";
        this.pinpadType               = "0";
        this.ebtTransType             = "0";
        this.ebtFcsId                 = "0000000";
        this.sharingGroups            = "";
      }
    }
    
//**********************************************************************    
    
    else if(termApp.equals(TERMAPP_VSTDB01))
    {
    
      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }

      if(isBlank(fraud))
      {
        addError("Please specify Fraud option");
      }

      if(isBlank(passProtect))
      {
        addError("Please specify Password Protect option");
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(!isBlank(checkPayment) && isBlank(checkMid))
      {
        addError("If selecting a Check Payment Type, please specify Check Mid.");
      }
       
      if(!isBlank(checkPayment) && isBlank(checkVoice))
      {
        addError("If selecting a Check Payment Type, please specify Check Voice Id #.");
      }

      if(!isBlank(checkPayment) && isBlank(checkAuthPhone))
      {
        addError("If selecting a Check Payment Type, please specify Check Authorization Phone #.");
      }

      if(isBlank(checkPayment) && (!isBlank(checkMid) || !isBlank(checkVoice) || !isBlank(checkAuthPhone)))
      {
        addError("If not selecting a Check Payment Type then Check Mid, Check Voice Id #, and Check Authorization Phone # must all be blank.");
      }


      if(isBlank(tipOption))
      {
        addError("Please specify Tip Option");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else if(autoCloseInd.equals("1"))
      {
        if(isBlank(checkTotalsInd))
        {
          addError("Please specify a Check Totals option");
        }
        
        if(isBlank(forceSettleInd))
        {
          addError("Please specify a Force Settle option");
        }
      }
      else if(autoCloseInd.equals("0"))
      {
        this.checkTotalsInd = "0";
        this.forceSettleInd = "0";
      }


      if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
      {
        addError("Please specify an Auto Close Time in military format 0001 - 2400");
      }
      else if(autoCloseInd.equals("0") && !isBlank(autoCloseTime))
      {
        addError("If No Auto Close selected, Auto Close Time must be blank");
      }
      else if(!isBlank(autoCloseTime))
      {
        try
        {
          int tempClose = Integer.parseInt(autoCloseTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Auto Close Time must be in military format (0001 - 2400)");
        }
      }
      
      if(isBlank(reminderMsgInd))
      {
        addError("Please specify Reminder Message Indicator");
      }

      if(reminderMsgInd.equals("1") && isBlank(reminderMsgTime))
      {
        addError("Please specify a Reminder Message Time in military format 0001 - 2400");
      }
      else if(reminderMsgInd.equals("0") && !isBlank(reminderMsgTime))
      {
        addError("If No Reminder Message selected, Reminder Message Time must be blank");
      }
      else if(!isBlank(reminderMsgTime))
      {
        try
        {
          int tempClose = Integer.parseInt(reminderMsgTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Reminder Message Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Reminder Message Time must be in military format (0001 - 2400)");
        }
        
        if(isBlank(reminderMsg))
        {
          addError("Please specify a Reminder Message");
        }
      }



      //latest additions
      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 16)
      {
        addError("Idle Prompt can be no more than 16 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 25)
      {
        addError("Header Line 4 can be no more than 25 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 25)
      {
        addError("Header Line 5 can be no more than 25 characters");
      }

      if(isBlank(this.primaryKey1))
      {
        addError("Please select a primary key 1");
      }

      if(isBlank(this.primaryKey2))
      {
        addError("Please select a primary key 2");
      }

      //end of latest additions






      
      if(isBlank(merchantAba))
      {
        addError("Please provide a Merchant ABA #");
      }
      else if(merchantAba.length() != 9)
      {
        addError("Merchant ABA # must be 9 digits long");
      }
      else if(!isNumber(merchantAba))
      {
        addError("Merchant ABA # must be a valid 9 digit number");
      }
      else if(!merchantAba.equals("000000000"))
      {
        if(isBlank(merchantSettlementAgent))
        {
          addError("Please specify Merchant Settlement Agent");
        }
      
        if(isBlank(reimbursementAttribute))
        {
          addError("Please specify Reimbursement Attribute");
        }

        if(isBlank(atmCancelReturn))
        {
          addError("Please specify ATM Cancel Return");
        }
         
        if(isBlank(cashBackTip))
        {
          addError("Please specify Cash Back Tip");
        }

        if(isBlank(pinpadTypeAvailable))
        {
          addError("Please select a Pinpad Model");
        }

        if(isBlank(sharingGroups))
        {
          addError("Please select the debit sharing groups used");
        }
        else if(sharingGroups.length() >10)
        {
          addError("No More than 10 sharing groups can be selected.  Visa must be one of them.");
        }
        else
        {
          sharingGroups = fixSharingGroups();
        }

      }
      else if(merchantAba.equals("000000000"))
      {
        this.merchantSettlementAgent  = "V000";
        this.reimbursementAttribute   = "0";
        this.atmCancelReturn          = "0";
        this.cashBackTip              = "0";
        this.pinpadTypeAvailable      = "0";
        this.pinpadType               = "0";
        this.sharingGroups            = "";
      }
    
    }
    
    
    else if(termApp.equals(TERMAPP_52C8072)) 
    {


      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }

      if(isBlank(fraud))
      {
        addError("Please specify Fraud option");
      }

      if(isBlank(passProtect))
      {
        addError("Please specify Password Protect option");
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(isBlank(tipOption))
      {
        addError("Please specify Tip Option");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(invoiceNumber))
      {
        addError("Please specify Invoice Number");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }


      if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
      {
        addError("Please specify an Auto Close Time in military format 0001 - 2359");
      }
      else if(autoCloseInd.equals("0") && !isBlank(autoCloseTime))
      {
        addError("If No Auto Close selected, Auto Close Time must be blank");
      }
      else if(!isBlank(autoCloseTime))
      {
        try
        {
          int tempClose = Integer.parseInt(autoCloseTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Auto Close Time must be in military format (0001 - 2400)");
        }
      }

      //latest additions
      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 25)
      {
        addError("Header Line 4 can be no more than 25 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 25)
      {
        addError("Header Line 5 can be no more than 25 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 40)
      {
        addError("Footer Line 1 can be no more than 40 characters");
      }
      if(!isBlank(this.footerLine2) && this.footerLine2.length() > 40)
      {
        addError("Footer Line 2 can be no more than 40 characters");
      }

      if(isBlank(this.surchargeInd))
      {
        addError("Please select whether there is a surcharge or no surcharge");
      }
      else if(this.surchargeInd.equals("Y"))
      {
        if(isBlank(this.surchargeAmount))
        {
          addError("Please specify a surcharge amount");
        }
        else if(!isValidSurcharge(this.surchargeAmount))
        {
          addError("Please provide a valid surcharge amount greater than 0 and less than 10");
        }
      }

      if(isBlank(this.primaryKey1))
      {
        addError("Please select a primary key 1");
      }

      if(isBlank(this.primaryKey2))
      {
        addError("Please select a primary key 2");
      }

      //end of latest additions

      
      if(isBlank(merchantAba))
      {
        addError("Please provide a Merchant ABA #");
      }
      else if(merchantAba.length() != 9)
      {
        addError("Merchant ABA # must be 9 digits long");
      }
      else if(!isNumber(merchantAba))
      {
        addError("Merchant ABA # must be a valid 9 digit number");
      }
      else if(!merchantAba.equals("000000000"))
      {
        if(isBlank(merchantSettlementAgent))
        {
          addError("Please specify Merchant Settlement Agent");
        }
      
        if(isBlank(atmCancelReturn))
        {
          addError("Please select ATM Cancel Return");
        }

        if(isBlank(cashBackTip))
        {
          addError("Please select Cash Back Tip");
        }

        if(isBlank(cashBackLimit))
        {
          addError("Please specify Cash Back Limit.  If no cash back, enter '00000' ");
        }

        if(isBlank(pinpadTypeAvailable))
        {
          addError("Please select a Pinpad Model");
        }

        if(isBlank(sharingGroups))
        {
          addError("Please select the debit sharing groups used");
        }
        else if(sharingGroups.length() >10)
        {
          addError("No More than 10 sharing groups can be selected.  Visa must be one of them.");
        }
        else
        {
          sharingGroups = fixSharingGroups();
        }

      }
      else if(merchantAba.equals("000000000"))
      {
        this.merchantSettlementAgent  = "V000";
        this.atmCancelReturn          = "0";
        this.cashBackTip              = "0";
        this.cashBackLimit            = "00000";
        this.pinpadTypeAvailable      = " ";
        this.pinpadType               = "0";
        this.sharingGroups            = "";
      }

    }

    else if(termApp.equals(TERMAPP_VXP0P16))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }

      if(isBlank(fraud))
      {
        addError("Please specify Fraud option");
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(!isBlank(checkPayment) && isBlank(checkMid))
      {
        addError("If selecting a Check Payment Type, please specify Check Mid.");
      }
       
      if(!isBlank(checkPayment) && isBlank(checkVoice))
      {
        addError("If selecting a Check Payment Type, please specify Check Voice Id #.");
      }

      if(!isBlank(checkPayment) && isBlank(checkAuthPhone))
      {
        addError("If selecting a Check Payment Type, please specify Check Authorization Phone #.");
      }

      if(isBlank(checkPayment) && (!isBlank(checkMid) || !isBlank(checkVoice) || !isBlank(checkAuthPhone)))
      {
        addError("If not selecting a Check Payment Type then Check Mid, Check Voice Id #, and Check Authorization Phone # must all be blank.");
      }

      //latest additions
      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 16)
      {
        addError("Idle Prompt can be no more than 16 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 25)
      {
        addError("Header Line 4 can be no more than 25 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 25)
      {
        addError("Header Line 5 can be no more than 25 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 40)
      {
        addError("Footer Line 1 can be no more than 40 characters");
      }
      if(!isBlank(this.footerLine2) && this.footerLine2.length() > 40)
      {
        addError("Footer Line 2 can be no more than 40 characters");
      }

      //end of latest additions
    }

    else if(termApp.equals(TERMAPP_VT13DA7))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }

      if(isBlank(fraud))
      {
        addError("Please specify Fraud option");
      }

      if(isBlank(passProtect))
      {
        addError("Please specify Password Protect option");
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(!isBlank(checkPayment) && isBlank(checkMid))
      {
        addError("If selecting a Check Payment Type, please specify Check Mid.");
      }
       
      if(!isBlank(checkPayment) && isBlank(checkVoice))
      {
        addError("If selecting a Check Payment Type, please specify Check Voice Id #.");
      }

      if(!isBlank(checkPayment) && isBlank(checkAuthPhone))
      {
        addError("If selecting a Check Payment Type, please specify Check Authorization Phone #.");
      }

      if(isBlank(checkPayment) && (!isBlank(checkMid) || !isBlank(checkVoice) || !isBlank(checkAuthPhone)))
      {
        addError("If not selecting a Check Payment Type then Check Mid, Check Voice Id #, and Check Authorization Phone # must all be blank.");
      }


      if(isBlank(tipOption))
      {
        addError("Please specify Tip Option");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(invoiceNumber))
      {
        addError("Please specify Invoice Number");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else if(autoCloseInd.equals("1"))
      {
        if(isBlank(checkTotalsInd))
        {
          addError("Please specify a Check Totals option");
        }
        
        if(isBlank(forceSettleInd))
        {
          addError("Please specify a Force Settle option");
        }
      }
      else if(autoCloseInd.equals("0"))
      {
        this.checkTotalsInd = "";
        this.forceSettleInd = "";
      }


      if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
      {
        addError("Please specify an Auto Close Time in military format 0001 - 2400");
      }
      else if(autoCloseInd.equals("0") && !isBlank(autoCloseTime))
      {
        addError("If No Auto Close selected, Auto Close Time must be blank");
      }
      else if(!isBlank(autoCloseTime))
      {
        try
        {
          int tempClose = Integer.parseInt(autoCloseTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Auto Close Time must be in military format (0001 - 2400)");
        }
      }
      
      if(isBlank(reminderMsgInd))
      {
        addError("Please specify Reminder Message Indicator");
      }

      if(reminderMsgInd.equals("1") && isBlank(reminderMsgTime))
      {
        addError("Please specify a Reminder Message Time in military format 0001 - 2400");
      }
      else if(reminderMsgInd.equals("0") && !isBlank(reminderMsgTime))
      {
        addError("If No Reminder Message selected, Reminder Message Time must be blank");
      }
      else if(!isBlank(reminderMsgTime))
      {
        try
        {
          int tempClose = Integer.parseInt(reminderMsgTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Reminder Message Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Reminder Message Time must be in military format (0001 - 2400)");
        }
        
        if(isBlank(reminderMsg))
        {
          addError("Please specify a Reminder Message");
        }
      }



      //latest additions
      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 16)
      {
        addError("Idle Prompt can be no more than 16 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 25)
      {
        addError("Header Line 4 can be no more than 25 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 25)
      {
        addError("Header Line 5 can be no more than 25 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 40)
      {
        addError("Footer Line 1 can be no more than 40 characters");
      }
      if(!isBlank(this.footerLine2) && this.footerLine2.length() > 40)
      {
        addError("Footer Line 2 can be no more than 40 characters");
      }

      if(isBlank(this.surchargeInd))
      {
        addError("Please select whether there is a surcharge or no surcharge");
      }
      else if(this.surchargeInd.equals("Y"))
      {
        if(isBlank(this.surchargeAmount))
        {
          addError("Please specify a surcharge amount");
        }
        else if(!isValidSurcharge(this.surchargeAmount))
        {
          addError("Please provide a valid surcharge amount greater than 0 and less than 10");
        }
      }

      if(isBlank(this.primaryKey1))
      {
        addError("Please select a primary key 1");
      }

      if(isBlank(this.primaryKey2))
      {
        addError("Please select a primary key 2");
      }
      //end of latest additions


      if(isBlank(merchantAba))
      {
        addError("Please provide a Merchant ABA #");
      }
      else if(merchantAba.length() != 9)
      {
        addError("Merchant ABA # must be 9 digits long");
      }
      else if(!isNumber(merchantAba))
      {
        addError("Merchant ABA # must be a valid 9 digit number");
      }
      else
      {
        if(isBlank(merchantSettlementAgent))
        {
          addError("Please specify Merchant Settlement Agent");
        }
      
        if(isBlank(reimbursementAttribute))
        {
          addError("Please specify Reimbursement Attribute");
        }

        if(isBlank(atmSignLine))
        {
          addError("Please specify ATM Receipt Signature Line");
        }
         
        if(isBlank(atmCancelReturn))
        {
          addError("Please select an Atm Cancel Return option");
        }

        if(isBlank(cashBackTip))
        {
          addError("Please specify Cash Back Tip");
        }

        if(isBlank(cashBackLimit))
        {
          addError("Please provide a Cash Back Limit");
        }
        else
        {
          cashBackLimit = getDigits(cashBackLimit);
        }

        if(isBlank(pinpadTypeAvailable))
        {
          addError("Please select a Pinpad Model");
        }

        if(isBlank(ebtTransType))
        {
          addError("Please select an EBT Trans Type");
        }

        if((ebtTransType.equals("1") || ebtTransType.equals("3")) && isBlank(ebtFcsId))
        {
          addError("Please provide an EBT FCS Id");
        }
        else if((ebtTransType.equals("1") || ebtTransType.equals("3")) && !isBlank(ebtFcsId) && !isNumber(ebtFcsId))
        {
          addError("Please provide a valid EBT FCS Id number");
        }
        else if(!ebtTransType.equals("1") && !ebtTransType.equals("3"))
        {
          this.ebtFcsId = "0000000";
        }

        if(!isBlank(sharingGroups) && sharingGroups.length() >10)
        {
          addError("No More than 10 sharing groups can be selected.  Visa must be one of them.");
        }
        else if(!isBlank(sharingGroups))
        {
          sharingGroups = fixSharingGroups();
        }
      }

      if(isBlank(settleToPc))
      {
        addError("Please select Settle To PC option");
      }
      else if(settleToPc.equals("Y"))
      {
        if(isBlank(parentVnumber))
        {
          addError("Please specify Parent V Number");
        }
      
        if(isBlank(primaryDialAccess))
        {
          addError("Please specify Primary Dial Access");
        }

        if(isBlank(primaryDataCapturePhone))
        {
          addError("Please specify Primary Data Capture Phone");
        }
        else
        {
          primaryDataCapturePhone = getDigits(primaryDataCapturePhone);
        }
         
        if(isBlank(secondaryDialAccess))
        {
          addError("Please specify Secondary Dial Access");
        }

        if(isBlank(secondaryDataCapturePhone))
        {
          addError("Please specify Secondary Data Capture Phone");
        }
        else
        {
          secondaryDataCapturePhone = getDigits(secondaryDataCapturePhone);
        }
      }
      else if(settleToPc.equals("N"))
      {
        this.parentVnumber                = "";
        this.primaryDialAccess            = "";
        this.primaryDataCapturePhone      = "";
        this.secondaryDialAccess          = "";
        this.secondaryDataCapturePhone    = "";
      }

    }
//***********************************************************

    else if(termApp.equals(TERMAPP_VT13DA8))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }

      if(isBlank(fraud))
      {
        addError("Please specify Fraud option");
      }

      if(isBlank(passProtect))
      {
        addError("Please specify Password Protect option");
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(!isBlank(checkPayment) && isBlank(checkMid))
      {
        addError("If selecting a Check Payment Type, please specify Check Mid.");
      }
       
      if(!isBlank(checkPayment) && isBlank(checkVoice))
      {
        addError("If selecting a Check Payment Type, please specify Check Voice Id #.");
      }

      if(!isBlank(checkPayment) && isBlank(checkAuthPhone))
      {
        addError("If selecting a Check Payment Type, please specify Check Authorization Phone #.");
      }

      if(isBlank(checkPayment) && (!isBlank(checkMid) || !isBlank(checkVoice) || !isBlank(checkAuthPhone)))
      {
        addError("If not selecting a Check Payment Type then Check Mid, Check Voice Id #, and Check Authorization Phone # must all be blank.");
      }


      if(isBlank(tipOption))
      {
        addError("Please specify Tip Option");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else if(autoCloseInd.equals("1"))
      {
        if(isBlank(checkTotalsInd))
        {
          addError("Please specify a Check Totals option");
        }
        
        if(isBlank(forceSettleInd))
        {
          addError("Please specify a Force Settle option");
        }
      }
      else if(autoCloseInd.equals("0"))
      {
        this.checkTotalsInd = "0";
        this.forceSettleInd = "0";
      }


      if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
      {
        addError("Please specify an Auto Close Time in military format 0001 - 2400");
      }
      else if(autoCloseInd.equals("0") && !isBlank(autoCloseTime))
      {
        addError("If No Auto Close selected, Auto Close Time must be blank");
      }
      else if(!isBlank(autoCloseTime))
      {
        try
        {
          int tempClose = Integer.parseInt(autoCloseTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Auto Close Time must be in military format (0001 - 2400)");
        }
      }
      
      if(isBlank(reminderMsgInd))
      {
        addError("Please specify Reminder Message Indicator");
      }

      if(reminderMsgInd.equals("1") && isBlank(reminderMsgTime))
      {
        addError("Please specify a Reminder Message Time in military format 0001 - 2400");
      }
      else if(reminderMsgInd.equals("0") && !isBlank(reminderMsgTime))
      {
        addError("If No Reminder Message selected, Reminder Message Time must be blank");
      }
      else if(!isBlank(reminderMsgTime))
      {
        try
        {
          int tempClose = Integer.parseInt(reminderMsgTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Reminder Message Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Reminder Message Time must be in military format (0001 - 2400)");
        }
        
        if(isBlank(reminderMsg))
        {
          addError("Please specify a Reminder Message");
        }
      }



      //latest additions
      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 16)
      {
        addError("Idle Prompt can be no more than 16 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 25)
      {
        addError("Header Line 4 can be no more than 25 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 25)
      {
        addError("Header Line 5 can be no more than 25 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 40)
      {
        addError("Footer Line 1 can be no more than 40 characters");
      }
      if(!isBlank(this.footerLine2) && this.footerLine2.length() > 40)
      {
        addError("Footer Line 2 can be no more than 40 characters");
      }

      if(isBlank(this.surchargeInd))
      {
        addError("Please select whether there is a surcharge or no surcharge");
      }
      else if(this.surchargeInd.equals("Y"))
      {
        if(isBlank(this.surchargeAmount))
        {
          addError("Please specify a surcharge amount");
        }
        else if(!isValidSurcharge(this.surchargeAmount))
        {
          addError("Please provide a valid surcharge amount greater than 0 and less than 10");
        }
      }

      if(isBlank(this.primaryKey1))
      {
        addError("Please select a primary key 1");
      }

      if(isBlank(this.primaryKey2))
      {
        addError("Please select a primary key 2");
      }
      //end of latest additions


      if(isBlank(merchantAba))
      {
        addError("Please provide a Merchant ABA #");
      }
      else if(merchantAba.length() != 9)
      {
        addError("Merchant ABA # must be 9 digits long");
      }
      else if(!isNumber(merchantAba))
      {
        addError("Merchant ABA # must be a valid 9 digit number");
      }
      else
      {
        if(isBlank(merchantSettlementAgent))
        {
          addError("Please specify Merchant Settlement Agent");
        }
      
        if(isBlank(reimbursementAttribute))
        {
          addError("Please specify Reimbursement Attribute");
        }

        if(isBlank(atmSignLine))
        {
          addError("Please specify ATM Receipt Signature Line");
        }
         
        if(isBlank(atmCancelReturn))
        {
          addError("Please select an Atm Cancel Return option");
        }

        if(isBlank(cashBackTip))
        {
          addError("Please specify Cash Back Tip");
        }

        if(isBlank(cashBackLimit))
        {
          addError("Please provide a Cash Back Limit");
        }
        else
        {
          cashBackLimit = getDigits(cashBackLimit);
        }

        if(isBlank(pinpadTypeAvailable))
        {
          addError("Please select a Pinpad Model");
        }

        if(isBlank(ebtTransType))
        {
          addError("Please select an EBT Trans Type");
        }

        if((ebtTransType.equals("1") || ebtTransType.equals("3")) && isBlank(ebtFcsId))
        {
          addError("Please provide an EBT FCS Id");
        }
        else if((ebtTransType.equals("1") || ebtTransType.equals("3")) && !isBlank(ebtFcsId) && !isNumber(ebtFcsId))
        {
          addError("Please provide a valid EBT FCS Id number");
        }
        else if(!ebtTransType.equals("1") && !ebtTransType.equals("3"))
        {
          this.ebtFcsId = "0000000";
        }

        if(!isBlank(sharingGroups) && sharingGroups.length() >10)
        {
          addError("No More than 10 sharing groups can be selected.  Visa must be one of them.");
        }
        else if(!isBlank(sharingGroups))
        {
          sharingGroups = fixSharingGroups();
        }
      }

      if(isBlank(settleToPc))
      {
        addError("Please select Settle To PC option");
      }
      else if(settleToPc.equals("Y"))
      {
        if(isBlank(parentVnumber))
        {
          addError("Please specify Parent V Number");
        }
      
        if(isBlank(primaryDialAccess))
        {
          addError("Please specify Primary Dial Access");
        }

        if(isBlank(primaryDataCapturePhone))
        {
          addError("Please specify Primary Data Capture Phone");
        }
        else
        {
          primaryDataCapturePhone = getDigits(primaryDataCapturePhone);
        }
         
        if(isBlank(secondaryDialAccess))
        {
          addError("Please specify Secondary Dial Access");
        }

        if(isBlank(secondaryDataCapturePhone))
        {
          addError("Please specify Secondary Data Capture Phone");
        }
        else
        {
          secondaryDataCapturePhone = getDigits(secondaryDataCapturePhone);
        }
      }
      else if(settleToPc.equals("N"))
      {
        this.parentVnumber                = "";
        this.primaryDialAccess            = "";
        this.primaryDataCapturePhone      = "";
        this.secondaryDialAccess          = "";
        this.secondaryDataCapturePhone    = "";
      }

      if(isBlank(this.cardPresent))
      {
        addError("Please select card present");
      }
    
      if(isBlank(this.forceDial))
      {
        addError("Please select force dial");
      }

      if(isBlank(this.dupTran))
      {
        addError("Please select dup tran");
      }

      if(isBlank(this.invNumKeys))
      {
        addError("please provide inv num keys");
      }
      else
      {
        this.invNumKeys = this.invNumKeys.toUpperCase();
        if(this.invNumKeys.indexOf("Y") >= 0)
        {
          if(isBlank(this.invPrompt))
          {
            addError("Please provide inv prompt");
          }
        }
      }
      if(isBlank(this.autoTipPercent))
      {
        addError("Please select auto tip percent");
      }

      if(isBlank(this.tipTimeSale))
      {
        addError("Please select tip time sale");
      }
      if(isBlank(this.tipAssist))
      {
        addError("Please select tip assist");
      }
      if(isBlank(this.tipAssistPercent1))
      {
        addError("Please select tip assist percent 1");
      }
      if(isBlank(this.tipAssistPercent2))
      {
        addError("Please select tip assist percent 2");
      }
      if(isBlank(this.tipAssistPercent3))
      {
        addError("Please select tip assist percent 3");
      }

    }
//*********************************************************

    else if(termApp.equals(TERMAPP_VT1H385))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }

      if(isBlank(fraud))
      {
        addError("Please specify Fraud option");
      }

      if(isBlank(passProtect))
      {
        addError("Please specify Password Protect option");
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(!isBlank(checkPayment) && isBlank(checkMid))
      {
        addError("If selecting a Check Payment Type, please specify Check Mid.");
      }
       
      if(!isBlank(checkPayment) && isBlank(checkVoice))
      {
        addError("If selecting a Check Payment Type, please specify Check Voice Id #.");
      }

      if(!isBlank(checkPayment) && isBlank(checkAuthPhone))
      {
        addError("If selecting a Check Payment Type, please specify Check Authorization Phone #.");
      }

      if(isBlank(checkPayment) && (!isBlank(checkMid) || !isBlank(checkVoice) || !isBlank(checkAuthPhone)))
      {
        addError("If not selecting a Check Payment Type then Check Mid, Check Voice Id #, and Check Authorization Phone # must all be blank.");
      }


      if(isBlank(tipOption))
      {
        addError("Please specify Tip Option");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else if(autoCloseInd.equals("1"))
      {
        if(isBlank(checkTotalsInd))
        {
          addError("Please specify a Check Totals option");
        }
        
        if(isBlank(forceSettleInd))
        {
          addError("Please specify a Force Settle option");
        }
      }
      else if(autoCloseInd.equals("0"))
      {
        this.checkTotalsInd = "0";
        this.forceSettleInd = "0";
      }


      if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
      {
        addError("Please specify an Auto Close Time in military format 0001 - 2400");
      }
      else if(autoCloseInd.equals("0") && !isBlank(autoCloseTime))
      {
        addError("If No Auto Close selected, Auto Close Time must be blank");
      }
      else if(!isBlank(autoCloseTime))
      {
        try
        {
          int tempClose = Integer.parseInt(autoCloseTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Auto Close Time must be in military format (0001 - 2400)");
        }
      }
      
      if(isBlank(reminderMsgInd))
      {
        addError("Please specify Reminder Message Indicator");
      }

      if(reminderMsgInd.equals("1") && isBlank(reminderMsgTime))
      {
        addError("Please specify a Reminder Message Time in military format 0001 - 2400");
      }
      else if(reminderMsgInd.equals("0") && !isBlank(reminderMsgTime))
      {
        addError("If No Reminder Message selected, Reminder Message Time must be blank");
      }
      else if(!isBlank(reminderMsgTime))
      {
        try
        {
          int tempClose = Integer.parseInt(reminderMsgTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Reminder Message Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Reminder Message Time must be in military format (0001 - 2400)");
        }
        
        if(isBlank(reminderMsg))
        {
          addError("Please specify a Reminder Message");
        }
      }



      //latest additions
      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 16)
      {
        addError("Idle Prompt can be no more than 16 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 25)
      {
        addError("Header Line 4 can be no more than 25 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 25)
      {
        addError("Header Line 5 can be no more than 25 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 40)
      {
        addError("Footer Line 1 can be no more than 40 characters");
      }
      if(!isBlank(this.footerLine2) && this.footerLine2.length() > 40)
      {
        addError("Footer Line 2 can be no more than 40 characters");
      }

      if(isBlank(settleToPc))
      {
        addError("Please select Settle To PC option");
      }
      else if(settleToPc.equals("Y"))
      {
        if(isBlank(parentVnumber))
        {
          addError("Please specify Parent V Number");
        }
      
        if(isBlank(primaryDialAccess))
        {
          addError("Please specify Primary Dial Access");
        }

        if(isBlank(primaryDataCapturePhone))
        {
          addError("Please specify Primary Data Capture Phone");
        }
        else
        {
          primaryDataCapturePhone = getDigits(primaryDataCapturePhone);
        }
         
        if(isBlank(secondaryDialAccess))
        {
          addError("Please specify Secondary Dial Access");
        }

        if(isBlank(secondaryDataCapturePhone))
        {
          addError("Please specify Secondary Data Capture Phone");
        }
        else
        {
          secondaryDataCapturePhone = getDigits(secondaryDataCapturePhone);
        }
      }
      else if(settleToPc.equals("N"))
      {
        this.parentVnumber                = "";
        this.primaryDialAccess            = "";
        this.primaryDataCapturePhone      = "";
        this.secondaryDialAccess          = "";
        this.secondaryDataCapturePhone    = "";
      }

    }


//**********************************************************************    
    else if(termApp.equals(TERMAPP_DRR))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }
      else
      {
        if(!isBlank(termModel))
        {
          if(termModel.equals("T8") || termModel.equals("T7E"))
          {
            if(printerTypeModel.equals("1*INTEGRATED"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("1*T77 THERMAL ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("7*INTEGRATED ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("8*INTEGRATED SPROCKET"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
          }
          else if(termModel.equals("T7P") && !printerTypeModel.equals("1*INTEGRATED"))
          {
            addError("The T7P terminal model can only have Integrated Printer selected");
          }
          else if(termModel.equals("T77") && !printerTypeModel.equals("1*T77 THERMAL ROLL") && !printerTypeModel.equals("7*INTEGRATED ROLL") && !printerTypeModel.equals("8*INTEGRATED SPROCKET"))
          {
            addError("The T77 terminal model can only have T77 Thermal Roll, Integrated Roll, or Integrated Sprocket selected as a printer");
          }

        }
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else
      {
        if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
        {
          addError("Please specify an Auto Close Time in military format 0001 - 2400");
        }
        else if(autoCloseInd.equals("0") && (!isBlank(autoCloseTime) || !isBlank(autoCloseTime2)))
        {
          addError("If No Auto Close selected, Auto Close Time must be blank");
        }
        else if(!isBlank(autoCloseTime))
        {
          try
          {
            int tempClose = Integer.parseInt(autoCloseTime);
            if(tempClose < 1 || tempClose > 2400)
            {
              addError("Auto Close Time must be in military format (0001 - 2400)");
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }

          try
          {
            if(isBlank(autoCloseTime2))
            {
              autoCloseTime2 = "0000";
            }
            else
            {
              int tempClose = Integer.parseInt(autoCloseTime2);
              if(tempClose !=0 && (tempClose < 1 || tempClose > 2400))
              {
                addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
              }
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
          }
        }
      }

      if(isBlank(autoCloseReports))
      {
        addError("Please select an Auto Close Report");
      }

      if(isBlank(restaurantProcessing))
      {
        addError("Please select if Restaurant Processing or not");
      }

      if(isBlank(srvclkProcessing))
      {
        addError("Please select if SRV/CLK Processing or not");
      }




      //latest additions
      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 20)
      {
        addError("Idle Prompt can be no more than 20 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 23)
      {
        addError("Header Line 4 can be no more than 23 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 23)
      {
        addError("Header Line 5 can be no more than 23 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 23)
      {
        addError("Footer Line 1 can be no more than 23 characters");
      }

      if(isBlank(this.surchargeInd))
      {
        addError("Please select whether there is a surcharge or no surcharge");
      }
      else if(this.surchargeInd.equals("Y"))
      {
        if(isBlank(this.surchargeAmount))
        {
          addError("Please specify a surcharge amount");
        }
        else if(!isValidSurcharge(this.surchargeAmount))
        {
          addError("Please provide a valid surcharge amount greater than 0 and less than 10");
        }
      }
      //end of latest additions





      if(isBlank(merchantAba))
      {
        addError("Please provide a Merchant ABA #");
      }
      else if(merchantAba.length() != 9)
      {
        addError("Merchant ABA # must be 9 digits long");
      }
      else if(!isNumber(merchantAba))
      {
        addError("Merchant ABA # must be a valid 9 digit number");
      }
      else
      {
        if(isBlank(merchantSettlementAgent))
        {
          addError("Please specify Merchant Settlement Agent");
        }
      
        if(isBlank(reimbursementAttribute))
        {
          addError("Please specify Reimbursement Attribute");
        }

        if(isBlank(pinpadTypeAvailable))
        {
          addError("Please select a Pinpad Model");
        }

        if(isBlank(ebtTransType))
        {
          addError("Please select an EBT Trans Type");
        }

        if((ebtTransType.equals("1") || ebtTransType.equals("3")) && isBlank(ebtFcsId))
        {
          addError("Please provide an EBT FCS Id");
        }
        else if((ebtTransType.equals("1") || ebtTransType.equals("3")) && !isBlank(ebtFcsId) && !isNumber(ebtFcsId))
        {
          addError("Please provide a valid EBT FCS Id number");
        }
        else if(!ebtTransType.equals("1") && !ebtTransType.equals("3"))
        {
          this.ebtFcsId = "0000000";
        }

        if(!isBlank(sharingGroups) && sharingGroups.length() >10)
        {
          addError("No More than 10 sharing groups can be selected.  Visa must be one of them.");
        }
        else if(!isBlank(sharingGroups))
        {
          sharingGroups = fixSharingGroups();
        }
      }
    

      if(isBlank(settleToPc))
      {
        addError("Please select Settle To PC option");
      }
      else if(settleToPc.equals("Y"))
      {
        if(isBlank(parentVnumber))
        {
          addError("Please specify Parent V Number");
        }
      
        if(isBlank(primaryDialAccess))
        {
          addError("Please specify Primary Dial Access");
        }

        if(isBlank(primaryDataCapturePhone))
        {
          addError("Please specify Primary Data Capture Phone");
        }
        else
        {
          primaryDataCapturePhone = getDigits(primaryDataCapturePhone);
        }
         
        if(isBlank(secondaryDialAccess))
        {
          addError("Please specify Secondary Dial Access");
        }

        if(isBlank(secondaryDataCapturePhone))
        {
          addError("Please specify Secondary Data Capture Phone");
        }
        else
        {
          secondaryDataCapturePhone = getDigits(secondaryDataCapturePhone);
        }
      }
      else if(settleToPc.equals("N"))
      {
        this.parentVnumber                = "";
        this.primaryDialAccess            = "";
        this.primaryDataCapturePhone      = "";
        this.secondaryDialAccess          = "";
        this.secondaryDataCapturePhone    = "";
      }
    }
    
//************************************************************************    
    
    else if(termApp.equals(TERMAPP_RR))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }
      else
      {
        if(!isBlank(termModel))
        {
          if(termModel.equals("T8") || termModel.equals("T7E"))
          {
            if(printerTypeModel.equals("1*INTEGRATED"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("1*T77 THERMAL ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("7*INTEGRATED ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("8*INTEGRATED SPROCKET"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
          }
          else if(termModel.equals("T7P") && !printerTypeModel.equals("1*INTEGRATED"))
          {
            addError("The T7P terminal model can only have Integrated Printer selected");
          }
          else if(termModel.equals("T77") && !printerTypeModel.equals("1*T77 THERMAL ROLL") && !printerTypeModel.equals("7*INTEGRATED ROLL") && !printerTypeModel.equals("8*INTEGRATED SPROCKET"))
          {
            addError("The T77 terminal model can only have T77 Thermal Roll, Integrated Roll, or Integrated Sprocket selected as a printer");
          }

        }
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else
      {
        if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
        {
          addError("Please specify an Auto Close Time in military format 0001 - 2400");
        }
        else if(autoCloseInd.equals("0") && (!isBlank(autoCloseTime) || !isBlank(autoCloseTime2)))
        {
          addError("If No Auto Close selected, Auto Close Time must be blank");
        }
        else if(!isBlank(autoCloseTime))
        {
          try
          {
            int tempClose = Integer.parseInt(autoCloseTime);
            if(tempClose < 1 || tempClose > 2400)
            {
              addError("Auto Close Time must be in military format (0001 - 2400)");
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }

          try
          {
            if(isBlank(autoCloseTime2))
            {
              autoCloseTime2 = "0000";
            }
            else
            {
              int tempClose = Integer.parseInt(autoCloseTime2);
              if(tempClose !=0 && (tempClose < 1 || tempClose > 2400))
              {
                addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
              }
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
          }
        }
      }

      if(isBlank(autoCloseReports))
      {
        addError("Please select an Auto Close Report");
      }

      if(isBlank(restaurantProcessing))
      {
        addError("Please select if Restaurant Processing or not");
      }

      if(isBlank(srvclkProcessing))
      {
        addError("Please select if SRV/CLK Processing or not");
      }




      //latest additions
      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 20)
      {
        addError("Idle Prompt can be no more than 20 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 23)
      {
        addError("Header Line 4 can be no more than 23 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 23)
      {
        addError("Header Line 5 can be no more than 23 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 23)
      {
        addError("Footer Line 1 can be no more than 23 characters");
      }
      //end of latest additions





      if(isBlank(settleToPc))
      {
        addError("Please select Settle To PC option");
      }
      else if(settleToPc.equals("Y"))
      {
        if(isBlank(parentVnumber))
        {
          addError("Please specify Parent V Number");
        }
      
        if(isBlank(primaryDialAccess))
        {
          addError("Please specify Primary Dial Access");
        }

        if(isBlank(primaryDataCapturePhone))
        {
          addError("Please specify Primary Data Capture Phone");
        }
        else
        {
          primaryDataCapturePhone = getDigits(primaryDataCapturePhone);
        }
         
        if(isBlank(secondaryDialAccess))
        {
          addError("Please specify Secondary Dial Access");
        }

        if(isBlank(secondaryDataCapturePhone))
        {
          addError("Please specify Secondary Data Capture Phone");
        }
        else
        {
          secondaryDataCapturePhone = getDigits(secondaryDataCapturePhone);
        }
      }
      else if(settleToPc.equals("N"))
      {
        this.parentVnumber                = "";
        this.primaryDialAccess            = "";
        this.primaryDataCapturePhone      = "";
        this.secondaryDialAccess          = "";
        this.secondaryDataCapturePhone    = "";
      }

    }

//**********************************************************************    
    else if(termApp.equals(TERMAPP_DHTL))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }
      else
      {
        if(!isBlank(termModel))
        {
          if(termModel.equals("T8") || termModel.equals("T7E"))
          {
            if(printerTypeModel.equals("1*INTEGRATED"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("1*T77 THERMAL ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("7*INTEGRATED ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("8*INTEGRATED SPROCKET"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("5*HYP ROLL") && termModel.equals("T7E"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
          }
          else if(termModel.equals("T7P") && !printerTypeModel.equals("1*INTEGRATED"))
          {
            addError("The T7P terminal model can only have Integrated Printer selected");
          }
          else if(termModel.equals("T77") && !printerTypeModel.equals("1*T77 THERMAL ROLL") && !printerTypeModel.equals("7*INTEGRATED ROLL") && !printerTypeModel.equals("8*INTEGRATED SPROCKET"))
          {
            addError("The T77 terminal model can only have T77 Thermal Roll, Integrated Roll, or Integrated Sprocket selected as a printer");
          }

        }
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else
      {
        if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
        {
          addError("Please specify an Auto Close Time in military format 0001 - 2400");
        }
        else if(autoCloseInd.equals("0") && (!isBlank(autoCloseTime) || !isBlank(autoCloseTime2)))
        {
          addError("If No Auto Close selected, Auto Close Time must be blank");
        }
        else if(!isBlank(autoCloseTime))
        {
          try
          {
            int tempClose = Integer.parseInt(autoCloseTime);
            if(tempClose < 1 || tempClose > 2400)
            {
              addError("Auto Close Time must be in military format (0001 - 2400)");
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }

          try
          {
            if(isBlank(autoCloseTime2))
            {
              autoCloseTime2 = "0000";
            }
            else
            {
              int tempClose = Integer.parseInt(autoCloseTime2);
              if(tempClose !=0 && (tempClose < 1 || tempClose > 2400))
              {
                addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
              }
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
          }
        }
      }

      if(isBlank(autoCloseReports))
      {
        addError("Please select an Auto Close Report");
      }

      if(isBlank(restaurantProcessing))
      {
        addError("Please select if Restaurant Processing or not");
      }

      if(isBlank(srvclkProcessing))
      {
        addError("Please select if SRV/CLK Processing or not");
      }

      if(isBlank(promptForClerk))
      {
        addError("Please select if prompt for clerk");
      }

      if(isBlank(folioGuestCheckProcessing))
      {
        addError("Please select folio/guest check processing");
      }
      else if(folioGuestCheckProcessing.equalsIgnoreCase("N"))
      {
        folioRoomPrompt = "";
      }

      //latest additions
      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 20)
      {
        addError("Idle Prompt can be no more than 20 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 23)
      {
        addError("Header Line 4 can be no more than 23 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 23)
      {
        addError("Header Line 5 can be no more than 23 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 23)
      {
        addError("Footer Line 1 can be no more than 23 characters");
      }

      if(isBlank(this.surchargeInd))
      {
        addError("Please select whether there is a surcharge or no surcharge");
      }
      else if(this.surchargeInd.equals("Y"))
      {
        if(isBlank(this.surchargeAmount))
        {
          addError("Please specify a surcharge amount");
        }
        else if(!isValidSurcharge(this.surchargeAmount))
        {
          addError("Please provide a valid surcharge amount greater than 0 and less than 10");
        }
      }
      //end of latest additions





      if(isBlank(merchantAba))
      {
        addError("Please provide a Merchant ABA #");
      }
      else if(merchantAba.length() != 9)
      {
        addError("Merchant ABA # must be 9 digits long");
      }
      else if(!isNumber(merchantAba))
      {
        addError("Merchant ABA # must be a valid 9 digit number");
      }
      else
      {
        if(isBlank(merchantSettlementAgent))
        {
          addError("Please specify Merchant Settlement Agent");
        }
      
        if(isBlank(reimbursementAttribute))
        {
          addError("Please specify Reimbursement Attribute");
        }

        if(isBlank(pinpadTypeAvailable))
        {
          addError("Please select a Pinpad Model");
        }

        if(!isBlank(sharingGroups) && sharingGroups.length() >10)
        {
          addError("No More than 10 sharing groups can be selected.  Visa must be one of them.");
        }
        else if(!isBlank(sharingGroups))
        {
          sharingGroups = fixSharingGroups();
        }
      }
    

      if(isBlank(settleToPc))
      {
        addError("Please select Settle To PC option");
      }
      else if(settleToPc.equals("Y"))
      {
        if(isBlank(parentVnumber))
        {
          addError("Please specify Parent V Number");
        }
      
        if(isBlank(primaryDialAccess))
        {
          addError("Please specify Primary Dial Access");
        }

        if(isBlank(primaryDataCapturePhone))
        {
          addError("Please specify Primary Data Capture Phone");
        }
        else
        {
          primaryDataCapturePhone = getDigits(primaryDataCapturePhone);
        }
         
        if(isBlank(secondaryDialAccess))
        {
          addError("Please specify Secondary Dial Access");
        }

        if(isBlank(secondaryDataCapturePhone))
        {
          addError("Please specify Secondary Data Capture Phone");
        }
        else
        {
          secondaryDataCapturePhone = getDigits(secondaryDataCapturePhone);
        }
      }
      else if(settleToPc.equals("N"))
      {
        this.parentVnumber                = "";
        this.primaryDialAccess            = "";
        this.primaryDataCapturePhone      = "";
        this.secondaryDialAccess          = "";
        this.secondaryDataCapturePhone    = "";
      }
    }
    
//************************************************************************    
    
    else if(termApp.equals(TERMAPP_HTL))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }
      else
      {
        if(!isBlank(termModel))
        {
          if(termModel.equals("T8") || termModel.equals("T7E"))
          {
            if(printerTypeModel.equals("1*INTEGRATED"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("1*T77 THERMAL ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("7*INTEGRATED ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("8*INTEGRATED SPROCKET"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("5*HYP ROLL") && termModel.equals("T7E"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
          }
          else if(termModel.equals("T7P") && !printerTypeModel.equals("1*INTEGRATED"))
          {
            addError("The T7P terminal model can only have Integrated Printer selected");
          }
          else if(termModel.equals("T77") && !printerTypeModel.equals("1*T77 THERMAL ROLL") && !printerTypeModel.equals("7*INTEGRATED ROLL") && !printerTypeModel.equals("8*INTEGRATED SPROCKET"))
          {
            addError("The T77 terminal model can only have T77 Thermal Roll, Integrated Roll, or Integrated Sprocket selected as a printer");
          }

        }
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else
      {
        if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
        {
          addError("Please specify an Auto Close Time in military format 0001 - 2400");
        }
        else if(autoCloseInd.equals("0") && (!isBlank(autoCloseTime) || !isBlank(autoCloseTime2)))
        {
          addError("If No Auto Close selected, Auto Close Time must be blank");
        }
        else if(!isBlank(autoCloseTime))
        {
          try
          {
            int tempClose = Integer.parseInt(autoCloseTime);
            if(tempClose < 1 || tempClose > 2400)
            {
              addError("Auto Close Time must be in military format (0001 - 2400)");
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }

          try
          {
            if(isBlank(autoCloseTime2))
            {
              autoCloseTime2 = "0000";
            }
            else
            {
              int tempClose = Integer.parseInt(autoCloseTime2);
              if(tempClose !=0 && (tempClose < 1 || tempClose > 2400))
              {
                addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
              }
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
          }
        }
      }

      if(isBlank(autoCloseReports))
      {
        addError("Please select an Auto Close Report");
      }

      if(isBlank(restaurantProcessing))
      {
        addError("Please select if Restaurant Processing or not");
      }

      if(isBlank(srvclkProcessing))
      {
        addError("Please select if SRV/CLK Processing or not");
      }

      if(isBlank(promptForClerk))
      {
        addError("Please select if prompt for clerk");
      }

      if(isBlank(folioGuestCheckProcessing))
      {
        addError("Please select folio/guest check processing");
      }
      else if(folioGuestCheckProcessing.equalsIgnoreCase("N"))
      {
        folioRoomPrompt = "";
      }

      //latest additions
      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 20)
      {
        addError("Idle Prompt can be no more than 20 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 23)
      {
        addError("Header Line 4 can be no more than 23 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 23)
      {
        addError("Header Line 5 can be no more than 23 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 23)
      {
        addError("Footer Line 1 can be no more than 23 characters");
      }

      //end of latest additions


      if(isBlank(settleToPc))
      {
        addError("Please select Settle To PC option");
      }
      else if(settleToPc.equals("Y"))
      {
        if(isBlank(parentVnumber))
        {
          addError("Please specify Parent V Number");
        }
      
        if(isBlank(primaryDialAccess))
        {
          addError("Please specify Primary Dial Access");
        }

        if(isBlank(primaryDataCapturePhone))
        {
          addError("Please specify Primary Data Capture Phone");
        }
        else
        {
          primaryDataCapturePhone = getDigits(primaryDataCapturePhone);
        }
         
        if(isBlank(secondaryDialAccess))
        {
          addError("Please specify Secondary Dial Access");
        }

        if(isBlank(secondaryDataCapturePhone))
        {
          addError("Please specify Secondary Data Capture Phone");
        }
        else
        {
          secondaryDataCapturePhone = getDigits(secondaryDataCapturePhone);
        }
      }
      else if(settleToPc.equals("N"))
      {
        this.parentVnumber                = "";
        this.primaryDialAccess            = "";
        this.primaryDataCapturePhone      = "";
        this.secondaryDialAccess          = "";
        this.secondaryDataCapturePhone    = "";
      }

    }

//******************************************************


    else if(termApp.equals(TERMAPP_VT46DA4))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }

      if(isBlank(fraud))
      {
        addError("Please specify Fraud option");
      }

      if(isBlank(passProtect))
      {
        addError("Please specify Password Protect option");
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(!isBlank(checkPayment) && isBlank(checkMid))
      {
        addError("If selecting a Check Payment Type, please specify Check Mid.");
      }
       
      if(!isBlank(checkPayment) && isBlank(checkVoice))
      {
        addError("If selecting a Check Payment Type, please specify Check Voice Id #.");
      }

      if(!isBlank(checkPayment) && isBlank(checkAuthPhone))
      {
        addError("If selecting a Check Payment Type, please specify Check Authorization Phone #.");
      }

      if(isBlank(checkPayment) && (!isBlank(checkMid) || !isBlank(checkVoice) || !isBlank(checkAuthPhone)))
      {
        addError("If not selecting a Check Payment Type then Check Mid, Check Voice Id #, and Check Authorization Phone # must all be blank.");
      }


      if(isBlank(tipOption))
      {
        addError("Please specify Tip Option");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else if(autoCloseInd.equals("1"))
      {
        if(isBlank(checkTotalsInd))
        {
          addError("Please specify a Check Totals option");
        }
        
        if(isBlank(forceSettleInd))
        {
          addError("Please specify a Force Settle option");
        }
      }
      else if(autoCloseInd.equals("0"))
      {
        this.checkTotalsInd = "";
        this.forceSettleInd = "";
      }


      if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
      {
        addError("Please specify an Auto Close Time in military format 0001 - 2400");
      }
      else if(autoCloseInd.equals("0") && !isBlank(autoCloseTime))
      {
        addError("If No Auto Close selected, Auto Close Time must be blank");
      }
      else if(!isBlank(autoCloseTime))
      {
        try
        {
          int tempClose = Integer.parseInt(autoCloseTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Auto Close Time must be in military format (0001 - 2400)");
        }
      }
      
      if(isBlank(reminderMsgInd))
      {
        addError("Please specify Reminder Message Indicator");
      }

      if(reminderMsgInd.equals("1") && isBlank(reminderMsgTime))
      {
        addError("Please specify a Reminder Message Time in military format 0001 - 2400");
      }
      else if(reminderMsgInd.equals("0") && !isBlank(reminderMsgTime))
      {
        addError("If No Reminder Message selected, Reminder Message Time must be blank");
      }
      else if(!isBlank(reminderMsgTime))
      {
        try
        {
          int tempClose = Integer.parseInt(reminderMsgTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Reminder Message Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Reminder Message Time must be in military format (0001 - 2400)");
        }
        
        if(isBlank(reminderMsg))
        {
          addError("Please specify a Reminder Message");
        }

      }


      //latest additions
      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 16)
      {
        addError("Idle Prompt can be no more than 16 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 25)
      {
        addError("Header Line 4 can be no more than 25 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 25)
      {
        addError("Header Line 5 can be no more than 25 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 40)
      {
        addError("Footer Line 1 can be no more than 40 characters");
      }
      if(!isBlank(this.footerLine2) && this.footerLine2.length() > 40)
      {
        addError("Footer Line 2 can be no more than 40 characters");
      }

      if(isBlank(this.surchargeInd))
      {
        addError("Please select whether there is a surcharge or no surcharge");
      }
      else if(this.surchargeInd.equals("Y"))
      {
        if(isBlank(this.surchargeAmount))
        {
          addError("Please specify a surcharge amount");
        }
        else if(!isValidSurcharge(this.surchargeAmount))
        {
          addError("Please provide a valid surcharge amount greater than 0 and less than 10");
        }
      }

      if(isBlank(this.primaryKey1))
      {
        addError("Please select a primary key 1");
      }

      if(isBlank(this.primaryKey2))
      {
        addError("Please select a primary key 2");
      }
      //end of latest additions



      if(isBlank(merchantAba))
      {
        addError("Please provide a Merchant ABA #");
      }
      else if(merchantAba.length() != 9)
      {
        addError("Merchant ABA # must be 9 digits long");
      }
      else if(!isNumber(merchantAba))
      {
        addError("Merchant ABA # must be a valid 9 digit number");
      }
      else
      {
        if(isBlank(merchantSettlementAgent))
        {
          addError("Please specify Merchant Settlement Agent");
        }
      
        if(isBlank(reimbursementAttribute))
        {
          addError("Please specify Reimbursement Attribute");
        }

        if(isBlank(atmSignLine))
        {
          addError("Please specify ATM Receipt Signature Line");
        }
         
        if(isBlank(atmCancelReturn))
        {
          addError("Please select an Atm Cancel Return option");
        }

        if(isBlank(cashBackTip))
        {
          addError("Please specify Cash Back Tip");
        }

        if(isBlank(cashBackLimit))
        {
          addError("Please provide a Cash Back Limit");
        }
        else
        {
          cashBackLimit = getDigits(cashBackLimit);
        }

        if(isBlank(pinpadTypeAvailable))
        {
          addError("Please select a Pinpad Model");
        }

        if(!isBlank(sharingGroups) && sharingGroups.length() >10)
        {
          addError("No More than 10 sharing groups can be selected.  Visa must be one of them.");
        }
        else if(!isBlank(sharingGroups))
        {
          sharingGroups = fixSharingGroups();
        }
      }
    

      if(isBlank(settleToPc))
      {
        addError("Please select Settle To PC option");
      }
      else if(settleToPc.equals("Y"))
      {
        if(isBlank(parentVnumber))
        {
          addError("Please specify Parent V Number");
        }
      
        if(isBlank(primaryDialAccess))
        {
          addError("Please specify Primary Dial Access");
        }

        if(isBlank(primaryDataCapturePhone))
        {
          addError("Please specify Primary Data Capture Phone");
        }
        else
        {
          primaryDataCapturePhone = getDigits(primaryDataCapturePhone);
        }
         
        if(isBlank(secondaryDialAccess))
        {
          addError("Please specify Secondary Dial Access");
        }

        if(isBlank(secondaryDataCapturePhone))
        {
          addError("Please specify Secondary Data Capture Phone");
        }
        else
        {
          secondaryDataCapturePhone = getDigits(secondaryDataCapturePhone);
        }
      }
      else if(settleToPc.equals("N"))
      {
        this.parentVnumber                = "";
        this.primaryDialAccess            = "";
        this.primaryDataCapturePhone      = "";
        this.secondaryDialAccess          = "";
        this.secondaryDataCapturePhone    = "";
      }

    }

//***************************************************************************

    else if(termApp.equals(TERMAPP_VT1TP08) || termApp.equals(TERMAPP_VT1TPA7) || termApp.equals(TERMAPP_VT1TPH6))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }

      if(isBlank(fraud))
      {
        addError("Please specify Fraud option");
      }

      if(isBlank(passProtect))
      {
        addError("Please specify Password Protect option");
      }

      if(!termApp.equals(TERMAPP_VT1TPH6) && isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(!isBlank(checkPayment) && isBlank(checkMid))
      {
        addError("If selecting a Check Payment Type, please specify Check Mid.");
      }
       
      if(!isBlank(checkPayment) && isBlank(checkVoice))
      {
        addError("If selecting a Check Payment Type, please specify Check Voice Id #.");
      }

      if(!isBlank(checkPayment) && isBlank(checkAuthPhone))
      {
        addError("If selecting a Check Payment Type, please specify Check Authorization Phone #.");
      }

      if(isBlank(checkPayment) && (!isBlank(checkMid) || !isBlank(checkVoice) || !isBlank(checkAuthPhone)))
      {
        addError("If not selecting a Check Payment Type then Check Mid, Check Voice Id #, and Check Authorization Phone # must all be blank.");
      }


      if(isBlank(tipOption))
      {
        addError("Please specify Tip Option");
      }

      if(termApp.equals(TERMAPP_VT1TP08) && isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(invoiceNumber))
      {
        addError("Please specify Invoice Number");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else if(autoCloseInd.equals("1"))
      {
        if(isBlank(checkTotalsInd))
        {
          addError("Please specify a Check Totals option");
        }
        
        if(isBlank(forceSettleInd))
        {
          addError("Please specify a Force Settle option");
        }
      }
      else if(autoCloseInd.equals("0"))
      {
        if(termApp.equals(TERMAPP_VT1TPA7))
        {
          this.checkTotalsInd = "0";
          this.forceSettleInd = "0";
        }
        else
        {
          this.checkTotalsInd = "";
          this.forceSettleInd = "";
        }
      }


      if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
      {
        addError("Please specify an Auto Close Time in military format 0001 - 2400");
      }
      else if(autoCloseInd.equals("0") && !isBlank(autoCloseTime))
      {
        addError("If No Auto Close selected, Auto Close Time must be blank");
      }
      else if(!isBlank(autoCloseTime))
      {
        try
        {
          int tempClose = Integer.parseInt(autoCloseTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Auto Close Time must be in military format (0001 - 2400)");
        }
      }
      
      if(isBlank(reminderMsgInd))
      {
        addError("Please specify Reminder Message Indicator");
      }

      if(reminderMsgInd.equals("1") && isBlank(reminderMsgTime))
      {
        addError("Please specify a Reminder Message Time in military format 0001 - 2400");
      }
      else if(reminderMsgInd.equals("0") && !isBlank(reminderMsgTime))
      {
        addError("If No Reminder Message selected, Reminder Message Time must be blank");
      }
      else if(!isBlank(reminderMsgTime))
      {
        try
        {
          int tempClose = Integer.parseInt(reminderMsgTime);
          if(tempClose < 1 || tempClose > 2400)
          {
            addError("Reminder Message Time must be in military format (0001 - 2400)");
          }
        }
        catch(Exception e)
        {
          addError("Reminder Message Time must be in military format (0001 - 2400)");
        }
        
        if(isBlank(reminderMsg))
        {
          addError("Please specify a Reminder Message");
        }

      }
    
      //latest additions
      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 16)
      {
        addError("Idle Prompt can be no more than 16 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 25)
      {
        addError("Header Line 4 can be no more than 25 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 25)
      {
        addError("Header Line 5 can be no more than 25 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 40)
      {
        addError("Footer Line 1 can be no more than 40 characters");
      }
      if(!isBlank(this.footerLine2) && this.footerLine2.length() > 40)
      {
        addError("Footer Line 2 can be no more than 40 characters");
      }

      if(isBlank(this.primaryKey1))
      {
        addError("Please select a primary key 1");
      }

      if(isBlank(this.primaryKey2))
      {
        addError("Please select a primary key 2");
      }


      if(termApp.equals(TERMAPP_VT1TPA7))
      {
        if(isBlank(this.cardPresent))
        {
          addError("Please select card present");
        }
      
        if(isBlank(this.forceDial))
        {
          addError("Please select force dial");
        }

        if(isBlank(this.autoTipPercent))
        {
          addError("Please select auto tip percent");
        }

        if(isBlank(this.tipTimeSale))
        {
          addError("Please select tip time sale");
        }
        if(isBlank(this.tipAssist))
        {
          addError("Please select tip assist");
        }
        if(isBlank(this.tipAssistPercent1))
        {
          addError("Please select tip assist percent 1");
        }
        if(isBlank(this.tipAssistPercent2))
        {
          addError("Please select tip assist percent 2");
        }
        if(isBlank(this.tipAssistPercent3))
        {
          addError("Please select tip assist percent 3");
        }
      
      }

      //end of latest additions
    
    
    }

    else if(termApp.equals(TERMAPP_VERICENTRE))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }
      else
      {
        if(!isBlank(termModel))
        {
          if(termModel.equals("T8") || termModel.equals("T7E"))
          {
            if(printerTypeModel.equals("1*INTEGRATED"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("1*T77 THERMAL ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("7*INTEGRATED ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("8*INTEGRATED SPROCKET"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
          }
          else if(termModel.equals("T7P") && !printerTypeModel.equals("1*INTEGRATED"))
          {
            addError("The T7P terminal model can only have Integrated Printer selected");
          }
          else if(termModel.equals("T77") && !printerTypeModel.equals("1*T77 THERMAL ROLL") && !printerTypeModel.equals("7*INTEGRATED ROLL") && !printerTypeModel.equals("8*INTEGRATED SPROCKET"))
          {
            addError("The T77 terminal model can only have T77 Thermal Roll, Integrated Roll, or Integrated Sprocket selected as a printer");
          }

        }
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else
      {
        if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
        {
          addError("Please specify an Auto Close Time in military format 0001 - 2400");
        }
        else if(autoCloseInd.equals("0") && (!isBlank(autoCloseTime) || !isBlank(autoCloseTime2)))
        {
          addError("If No Auto Close selected, Auto Close Time must be blank");
        }
        else if(!isBlank(autoCloseTime))
        {
          try
          {
            int tempClose = Integer.parseInt(autoCloseTime);
            if(tempClose < 1 || tempClose > 2400)
            {
              addError("Auto Close Time must be in military format (0001 - 2400)");
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }

          try
          {
            if(isBlank(autoCloseTime2))
            {
              autoCloseTime2 = "0000";
            }
            else
            {
              int tempClose = Integer.parseInt(autoCloseTime2);
              if(tempClose !=0 && (tempClose < 1 || tempClose > 2400))
              {
                addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
              }
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
          }
        }
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 23)
      {
        addError("Header Line 4 can be no more than 23 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 23)
      {
        addError("Header Line 5 can be no more than 23 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 23)
      {
        addError("Footer Line 1 can be no more than 23 characters");
      }

    }

    else if(termApp.equals(TERMAPP_TERMMASTER))
    {

      if(isBlank(termModel))
      {
        addError("Please select a Terminal Model");
      }

      if(isBlank(printerTypeModel))
      {
        addError("Please select a Printer Model");
      }
      else
      {
        if(!isBlank(termModel))
        {
          if(termModel.equals("T8") || termModel.equals("T7E"))
          {
            if(printerTypeModel.equals("1*INTEGRATED"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("1*T77 THERMAL ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("7*INTEGRATED ROLL"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
            else if(printerTypeModel.equals("8*INTEGRATED SPROCKET"))
            {
              addError("The printer selected can not be associated with the terminal model selected");
            }
          }
          else if(termModel.equals("T7P") && !printerTypeModel.equals("1*INTEGRATED"))
          {
            addError("The T7P terminal model can only have Integrated Printer selected");
          }
          else if(termModel.equals("T77") && !printerTypeModel.equals("1*T77 THERMAL ROLL") && !printerTypeModel.equals("7*INTEGRATED ROLL") && !printerTypeModel.equals("8*INTEGRATED SPROCKET"))
          {
            addError("The T77 terminal model can only have T77 Thermal Roll, Integrated Roll, or Integrated Sprocket selected as a printer");
          }

        }
      }

      if(isBlank(receiptTrunc))
      {
        addError("Please specify Receipt Truncation");
      }

      if(isBlank(avs))
      {
        addError("Please specify AVS");
      }

      if(isBlank(purchasingCard))
      {
        addError("Please specify Purchasing Card");
      }

      if(isBlank(autoCloseInd))
      {
        addError("Please specify Auto Close Indicator");
      }
      else
      {
        if(autoCloseInd.equals("1") && isBlank(autoCloseTime))
        {
          addError("Please specify an Auto Close Time in military format 0001 - 2400");
        }
        else if(autoCloseInd.equals("0") && (!isBlank(autoCloseTime) || !isBlank(autoCloseTime2)))
        {
          addError("If No Auto Close selected, Auto Close Time must be blank");
        }
        else if(!isBlank(autoCloseTime))
        {
          try
          {
            int tempClose = Integer.parseInt(autoCloseTime);
            if(tempClose < 1 || tempClose > 2400)
            {
              addError("Auto Close Time must be in military format (0001 - 2400)");
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time must be in military format (0001 - 2400)");
          }

          try
          {
            if(isBlank(autoCloseTime2))
            {
              autoCloseTime2 = "0000";
            }
            else
            {
              int tempClose = Integer.parseInt(autoCloseTime2);
              if(tempClose !=0 && (tempClose < 1 || tempClose > 2400))
              {
                addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
              }
            }
          }
          catch(Exception e)
          {
            addError("Auto Close Time 2 must be in military format (0001 - 2400) or 0000 to disable it");
          }
        }
      }

      if(isBlank(autoCloseReports))
      {
        addError("Please select an Auto Close Report");
      }

      if(isBlank(restaurantProcessing))
      {
        addError("Please select if Restaurant Processing or not");
      }

      if(isBlank(srvclkProcessing))
      {
        addError("Please select if SRV/CLK Processing or not");
      }

      if(!isBlank(this.idlePrompt) && this.idlePrompt.length() > 20)
      {
        addError("Idle Prompt can be no more than 20 characters");
      }

      if(!isBlank(this.headerLine4) && this.headerLine4.length() > 23)
      {
        addError("Header Line 4 can be no more than 23 characters");
      }

      if(!isBlank(this.headerLine5) && this.headerLine5.length() > 23)
      {
        addError("Header Line 5 can be no more than 23 characters");
      }

      if(!isBlank(this.footerLine1) && this.footerLine1.length() > 23)
      {
        addError("Footer Line 1 can be no more than 23 characters");
      }

      
      if(isBlank(settleToPc))
      {
        addError("Please select Settle To PC option");
      }
      else if(settleToPc.equals("Y"))
      {
        if(isBlank(parentVnumber))
        {
          addError("Please specify Parent V Number");
        }
      
        if(isBlank(primaryDialAccess))
        {
          addError("Please specify Primary Dial Access");
        }

        if(isBlank(primaryDataCapturePhone))
        {
          addError("Please specify Primary Data Capture Phone");
        }
        else
        {
          primaryDataCapturePhone = getDigits(primaryDataCapturePhone);
        }
         
        if(isBlank(secondaryDialAccess))
        {
          addError("Please specify Secondary Dial Access");
        }

        if(isBlank(secondaryDataCapturePhone))
        {
          addError("Please specify Secondary Data Capture Phone");
        }
        else
        {
          secondaryDataCapturePhone = getDigits(secondaryDataCapturePhone);
        }
      }
      else if(settleToPc.equals("N"))
      {
        this.parentVnumber                = "";
        this.primaryDialAccess            = "";
        this.primaryDataCapturePhone      = "";
        this.secondaryDialAccess          = "";
        this.secondaryDataCapturePhone    = "";
      }

      if(isBlank(this.debitSettleNum))
      {
        addError("Please specify a Debit Settle Number.");
      }

      if(isBlank(this.downloadSpeed))
      {
        addError("Please specify a Download Speed.");
      }

      if(isBlank(this.dialSpeed))
      {
        addError("Please specify a Dial Speed.");
      }

    }

    return (!hasErrors());
  }

  //this makes sure that visa is at the end... why cause the specs said.. i dont see
  //what the f***ing difference is...
  private String fixSharingGroups()
  {
    String result = "";

    if(isBlank(sharingGroups))
    {
      return result;
    }

    int idx = sharingGroups.indexOf("V");

    if(idx == -1 && sharingGroups.length() > 9)
    {
      addError("Visa must be one of the possible 10 sharing groups");
      return this.sharingGroups;
    }
    else if (idx == -1)
    {
      result = this.sharingGroups + VISA;
    }
    else
    {
      String part1 = this.sharingGroups.substring(0,idx);
      String part2 = this.sharingGroups.substring(idx+1);
      result = part1 + part2 + VISA;
    }

    return result;

  }


  public void submitData(long reqId, String termApp)
  {
    try
    {
      connect();
      
      if(!termApp.equals(TERMAPP_STAGE) && !termApp.equals(TERMAPP_DIALPAY))
      {
        if(recordExists(reqId))
        {
          /*@lineinfo:generated-code*//*@lineinfo:3873^11*/

//  ************************************************************
//  #sql [Ctx] { update  mms_expanded_info
//              set     MANUFACTURER                  =  :manufacturer,
//                      TERM_MODEL                    =  :termModel,              
//                      PRINTER_MODEL                 =  :printerModel,           
//                      PRINTER_TYPE                  =  :printerType,            
//                      FRAUD                         =  :fraud,                  
//                      RECEIPT_TRUNCATION            =  :receiptTrunc,           
//                      CHECK_PAYMENT                 =  :checkPayment,           
//                      CHECK_MID                     =  :checkMid,               
//                      CHECK_VOICE                   =  :checkVoice,             
//                      CHECK_AUTH_PHONE              =  :checkAuthPhone,         
//                      TIP_OPTION                    =  :tipOption,              
//                      AVS                           =  :avs,                    
//                      PURCHASING_CARD               =  :purchasingCard,         
//                      INVOICE_NUMBER                =  :invoiceNumber,          
//                      AUTO_CLOSE_IND                =  :autoCloseInd,
//                      AUTO_CLOSE_TIME               =  :autoCloseTime,          
//                      AUTO_CLOSE_TIME2              =  :autoCloseTime2,
//                      AUTO_CLOSE_REPORTS            =  :autoCloseReports,
//                      RESTAURANT_PROCESSING         =  :restaurantProcessing,
//                      SRV_CLK_PROCESSING            =  :srvclkProcessing,
//                      MERCHANT_ABA                  =  :merchantAba,            
//                      MERCH_SETTLEMENT_AGENT        =  :merchantSettlementAgent,
//                      REIMBURSEMENT_ATTRIBUTES      =  :reimbursementAttribute, 
//                      ATM_RECEIPT_SIGN_LINE         =  :atmSignLine,            
//                      CASH_BACK                     =  :cashBack,
//                      CASH_BACK_TIP                 =  :cashBackTip,
//                      ATM_CANCEL_RETURN             =  :atmCancelReturn, 
//                      CASH_BACK_LIMIT               =  :cashBackLimit, 
//                      CHECK_TOTALS_IND              =  :checkTotalsInd, 
//                      EBT_TRANS_TYPE                =  :ebtTransType, 
//                      EBT_FCS_ID                    =  :ebtFcsId, 
//                      FORCE_SETTLE_IND              =  :forceSettleInd, 
//                      PARENT_VNUMBER                =  :parentVnumber, 
//                      PASS_PROTECT                  =  :passProtect, 
//                      PINPAD_AVAILABLE              =  :pinpadAvailable, 
//                      PINPAD_TYPE                   =  :pinpadType, 
//                      PRIMARY_DATA_CAPTURE_PHONE    =  :primaryDataCapturePhone, 
//                      PRIMARY_DIAL_ACCESS           =  :primaryDialAccess, 
//                      REMINDER_MSG                  =  :reminderMsg, 
//                      REMINDER_MSG_IND              =  :reminderMsgInd, 
//                      REMINDER_MSG_TIME             =  :reminderMsgTime, 
//                      SECONDARY_DATA_CAPTURE_PHONE  =  :secondaryDataCapturePhone, 
//                      SECONDARY_DIAL_ACCESS         =  :secondaryDialAccess, 
//                      SETTLE_TO_PC                  =  :settleToPc, 
//                      SHARING_GROUPS                =  :sharingGroups,                   
//                      IDLE_PROMPT                   =  :idlePrompt, 
//                      HEADER_LINE4                  =  :headerLine4, 
//                      HEADER_LINE5                  =  :headerLine5, 
//                      FOOTER_LINE1                  =  :footerLine1, 
//                      FOOTER_LINE2                  =  :footerLine2, 
//                      PRIM_AUTH_DIAL_ACCESS         =  :primaryAuthDialAccess, 
//                      SEC_AUTH_DIAL_ACCESS          =  :secondaryAuthDialAccess, 
//                      SURCHARGE_IND                 =  :surchargeInd, 
//                      SURCHARGE_AMOUNT              =  :surchargeAmount, 
//                      PRIMARY_KEY1                  =  :primaryKey1, 
//                      PRIMARY_KEY2                  =  :primaryKey2,
//                      CARD_PRESENT                  =  :cardPresent,
//                      FORCE_DIAL                    =  :forceDial,
//                      AUTO_TIP_PERCENT              =  :autoTipPercent,
//                      TIP_TIME_SALE                 =  :tipTimeSale,
//                      TIP_ASSIST                    =  :tipAssist,
//                      TIP_ASSIST_PERCENT_1          =  :tipAssistPercent1,
//                      TIP_ASSIST_PERCENT_2          =  :tipAssistPercent2,
//                      TIP_ASSIST_PERCENT_3          =  :tipAssistPercent3,
//                      INV_NUM_KEYS                  =  :invNumKeys,
//                      INV_PROMPT                    =  :invPrompt,
//                      DUP_TRAN                      =  :dupTran,
//                      PROMPT_FOR_CLERK              =  :promptForClerk,
//                      FOLIO_GUEST_CHECK_PROCESSING  =  :folioGuestCheckProcessing,
//                      FOLIO_ROOM_PROMPT             =  :folioRoomPrompt,
//                      TIME_DIFFERENCE               =  :timeDifference,
//                      ECI_TYPE                      =  :eciType,
//                      CLERK_SERVER_MODE             =  :clerkServerMode,
//                      OVERAGE_PERCENTAGE            =  :overagePercentage,
//                      CHECK_READER                  =  :checkReader,
//                      RIID                          =  :riid,
//                      CHECK_HOST                    =  :checkHost,
//                      CHECK_HOST_FORMAT             =  :checkHostFormat,
//                      AMEX_OPTION                   =  :amexOption,
//                      MERCHANT_ID_AMEX              =  :merchantIdAmex,
//                      BAUD_RATE_HOST1               =  :baudRateHost1,
//                      BAUD_RATE_AMEXHOST            =  :baudRateAmexHost,
//                      AMEX_PABX                     =  :amexPabx,
//                      DEBIT_SETTLE_NUM              =  :debitSettleNum,
//                      DOWNLOAD_SPEED                =  :downloadSpeed,
//                      DIAL_SPEED                    =  :dialSpeed,
//                      TM_OPTIONS                    =  :tmOptions
//              where   REQUEST_ID                    =  :reqId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_expanded_info\n            set     MANUFACTURER                  =   :1 ,\n                    TERM_MODEL                    =   :2 ,              \n                    PRINTER_MODEL                 =   :3 ,           \n                    PRINTER_TYPE                  =   :4 ,            \n                    FRAUD                         =   :5 ,                  \n                    RECEIPT_TRUNCATION            =   :6 ,           \n                    CHECK_PAYMENT                 =   :7 ,           \n                    CHECK_MID                     =   :8 ,               \n                    CHECK_VOICE                   =   :9 ,             \n                    CHECK_AUTH_PHONE              =   :10 ,         \n                    TIP_OPTION                    =   :11 ,              \n                    AVS                           =   :12 ,                    \n                    PURCHASING_CARD               =   :13 ,         \n                    INVOICE_NUMBER                =   :14 ,          \n                    AUTO_CLOSE_IND                =   :15 ,\n                    AUTO_CLOSE_TIME               =   :16 ,          \n                    AUTO_CLOSE_TIME2              =   :17 ,\n                    AUTO_CLOSE_REPORTS            =   :18 ,\n                    RESTAURANT_PROCESSING         =   :19 ,\n                    SRV_CLK_PROCESSING            =   :20 ,\n                    MERCHANT_ABA                  =   :21 ,            \n                    MERCH_SETTLEMENT_AGENT        =   :22 ,\n                    REIMBURSEMENT_ATTRIBUTES      =   :23 , \n                    ATM_RECEIPT_SIGN_LINE         =   :24 ,            \n                    CASH_BACK                     =   :25 ,\n                    CASH_BACK_TIP                 =   :26 ,\n                    ATM_CANCEL_RETURN             =   :27 , \n                    CASH_BACK_LIMIT               =   :28 , \n                    CHECK_TOTALS_IND              =   :29 , \n                    EBT_TRANS_TYPE                =   :30 , \n                    EBT_FCS_ID                    =   :31 , \n                    FORCE_SETTLE_IND              =   :32 , \n                    PARENT_VNUMBER                =   :33 , \n                    PASS_PROTECT                  =   :34 , \n                    PINPAD_AVAILABLE              =   :35 , \n                    PINPAD_TYPE                   =   :36 , \n                    PRIMARY_DATA_CAPTURE_PHONE    =   :37 , \n                    PRIMARY_DIAL_ACCESS           =   :38 , \n                    REMINDER_MSG                  =   :39 , \n                    REMINDER_MSG_IND              =   :40 , \n                    REMINDER_MSG_TIME             =   :41 , \n                    SECONDARY_DATA_CAPTURE_PHONE  =   :42 , \n                    SECONDARY_DIAL_ACCESS         =   :43 , \n                    SETTLE_TO_PC                  =   :44 , \n                    SHARING_GROUPS                =   :45 ,                   \n                    IDLE_PROMPT                   =   :46 , \n                    HEADER_LINE4                  =   :47 , \n                    HEADER_LINE5                  =   :48 , \n                    FOOTER_LINE1                  =   :49 , \n                    FOOTER_LINE2                  =   :50 , \n                    PRIM_AUTH_DIAL_ACCESS         =   :51 , \n                    SEC_AUTH_DIAL_ACCESS          =   :52 , \n                    SURCHARGE_IND                 =   :53 , \n                    SURCHARGE_AMOUNT              =   :54 , \n                    PRIMARY_KEY1                  =   :55 , \n                    PRIMARY_KEY2                  =   :56 ,\n                    CARD_PRESENT                  =   :57 ,\n                    FORCE_DIAL                    =   :58 ,\n                    AUTO_TIP_PERCENT              =   :59 ,\n                    TIP_TIME_SALE                 =   :60 ,\n                    TIP_ASSIST                    =   :61 ,\n                    TIP_ASSIST_PERCENT_1          =   :62 ,\n                    TIP_ASSIST_PERCENT_2          =   :63 ,\n                    TIP_ASSIST_PERCENT_3          =   :64 ,\n                    INV_NUM_KEYS                  =   :65 ,\n                    INV_PROMPT                    =   :66 ,\n                    DUP_TRAN                      =   :67 ,\n                    PROMPT_FOR_CLERK              =   :68 ,\n                    FOLIO_GUEST_CHECK_PROCESSING  =   :69 ,\n                    FOLIO_ROOM_PROMPT             =   :70 ,\n                    TIME_DIFFERENCE               =   :71 ,\n                    ECI_TYPE                      =   :72 ,\n                    CLERK_SERVER_MODE             =   :73 ,\n                    OVERAGE_PERCENTAGE            =   :74 ,\n                    CHECK_READER                  =   :75 ,\n                    RIID                          =   :76 ,\n                    CHECK_HOST                    =   :77 ,\n                    CHECK_HOST_FORMAT             =   :78 ,\n                    AMEX_OPTION                   =   :79 ,\n                    MERCHANT_ID_AMEX              =   :80 ,\n                    BAUD_RATE_HOST1               =   :81 ,\n                    BAUD_RATE_AMEXHOST            =   :82 ,\n                    AMEX_PABX                     =   :83 ,\n                    DEBIT_SETTLE_NUM              =   :84 ,\n                    DOWNLOAD_SPEED                =   :85 ,\n                    DIAL_SPEED                    =   :86 ,\n                    TM_OPTIONS                    =   :87 \n            where   REQUEST_ID                    =   :88";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.ops.MmsExpandedFieldsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,manufacturer);
   __sJT_st.setString(2,termModel);
   __sJT_st.setString(3,printerModel);
   __sJT_st.setString(4,printerType);
   __sJT_st.setString(5,fraud);
   __sJT_st.setString(6,receiptTrunc);
   __sJT_st.setString(7,checkPayment);
   __sJT_st.setString(8,checkMid);
   __sJT_st.setString(9,checkVoice);
   __sJT_st.setString(10,checkAuthPhone);
   __sJT_st.setString(11,tipOption);
   __sJT_st.setString(12,avs);
   __sJT_st.setString(13,purchasingCard);
   __sJT_st.setString(14,invoiceNumber);
   __sJT_st.setString(15,autoCloseInd);
   __sJT_st.setString(16,autoCloseTime);
   __sJT_st.setString(17,autoCloseTime2);
   __sJT_st.setString(18,autoCloseReports);
   __sJT_st.setString(19,restaurantProcessing);
   __sJT_st.setString(20,srvclkProcessing);
   __sJT_st.setString(21,merchantAba);
   __sJT_st.setString(22,merchantSettlementAgent);
   __sJT_st.setString(23,reimbursementAttribute);
   __sJT_st.setString(24,atmSignLine);
   __sJT_st.setString(25,cashBack);
   __sJT_st.setString(26,cashBackTip);
   __sJT_st.setString(27,atmCancelReturn);
   __sJT_st.setString(28,cashBackLimit);
   __sJT_st.setString(29,checkTotalsInd);
   __sJT_st.setString(30,ebtTransType);
   __sJT_st.setString(31,ebtFcsId);
   __sJT_st.setString(32,forceSettleInd);
   __sJT_st.setString(33,parentVnumber);
   __sJT_st.setString(34,passProtect);
   __sJT_st.setString(35,pinpadAvailable);
   __sJT_st.setString(36,pinpadType);
   __sJT_st.setString(37,primaryDataCapturePhone);
   __sJT_st.setString(38,primaryDialAccess);
   __sJT_st.setString(39,reminderMsg);
   __sJT_st.setString(40,reminderMsgInd);
   __sJT_st.setString(41,reminderMsgTime);
   __sJT_st.setString(42,secondaryDataCapturePhone);
   __sJT_st.setString(43,secondaryDialAccess);
   __sJT_st.setString(44,settleToPc);
   __sJT_st.setString(45,sharingGroups);
   __sJT_st.setString(46,idlePrompt);
   __sJT_st.setString(47,headerLine4);
   __sJT_st.setString(48,headerLine5);
   __sJT_st.setString(49,footerLine1);
   __sJT_st.setString(50,footerLine2);
   __sJT_st.setString(51,primaryAuthDialAccess);
   __sJT_st.setString(52,secondaryAuthDialAccess);
   __sJT_st.setString(53,surchargeInd);
   __sJT_st.setString(54,surchargeAmount);
   __sJT_st.setString(55,primaryKey1);
   __sJT_st.setString(56,primaryKey2);
   __sJT_st.setString(57,cardPresent);
   __sJT_st.setString(58,forceDial);
   __sJT_st.setString(59,autoTipPercent);
   __sJT_st.setString(60,tipTimeSale);
   __sJT_st.setString(61,tipAssist);
   __sJT_st.setString(62,tipAssistPercent1);
   __sJT_st.setString(63,tipAssistPercent2);
   __sJT_st.setString(64,tipAssistPercent3);
   __sJT_st.setString(65,invNumKeys);
   __sJT_st.setString(66,invPrompt);
   __sJT_st.setString(67,dupTran);
   __sJT_st.setString(68,promptForClerk);
   __sJT_st.setString(69,folioGuestCheckProcessing);
   __sJT_st.setString(70,folioRoomPrompt);
   __sJT_st.setString(71,timeDifference);
   __sJT_st.setString(72,eciType);
   __sJT_st.setString(73,clerkServerMode);
   __sJT_st.setString(74,overagePercentage);
   __sJT_st.setString(75,checkReader);
   __sJT_st.setString(76,riid);
   __sJT_st.setString(77,checkHost);
   __sJT_st.setString(78,checkHostFormat);
   __sJT_st.setString(79,amexOption);
   __sJT_st.setString(80,merchantIdAmex);
   __sJT_st.setString(81,baudRateHost1);
   __sJT_st.setString(82,baudRateAmexHost);
   __sJT_st.setString(83,amexPabx);
   __sJT_st.setString(84,debitSettleNum);
   __sJT_st.setString(85,downloadSpeed);
   __sJT_st.setString(86,dialSpeed);
   __sJT_st.setString(87,tmOptions);
   __sJT_st.setLong(88,reqId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3964^11*/

        }
        // update existing records
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:3970^11*/

//  ************************************************************
//  #sql [Ctx] { insert into mms_expanded_info
//              ( 
//                MANUFACTURER,              
//                TERM_MODEL,                
//                PRINTER_MODEL,             
//                PRINTER_TYPE,              
//                FRAUD,                     
//                RECEIPT_TRUNCATION,        
//                CHECK_PAYMENT,             
//                CHECK_MID,                 
//                CHECK_VOICE,               
//                CHECK_AUTH_PHONE,          
//                TIP_OPTION,                
//                AVS,                       
//                PURCHASING_CARD,           
//                INVOICE_NUMBER,            
//                AUTO_CLOSE_IND,            
//                AUTO_CLOSE_TIME,           
//                AUTO_CLOSE_TIME2,
//                AUTO_CLOSE_REPORTS,
//                RESTAURANT_PROCESSING,
//                SRV_CLK_PROCESSING,
//                MERCHANT_ABA,              
//                MERCH_SETTLEMENT_AGENT,    
//                REIMBURSEMENT_ATTRIBUTES,  
//                ATM_RECEIPT_SIGN_LINE,     
//                CASH_BACK,                 
//                CASH_BACK_TIP,
//                ATM_CANCEL_RETURN, 
//                CASH_BACK_LIMIT,               
//                CHECK_TOTALS_IND,              
//                EBT_TRANS_TYPE,                
//                EBT_FCS_ID,                    
//                FORCE_SETTLE_IND,
//                PARENT_VNUMBER,                
//                PASS_PROTECT,                  
//                PINPAD_AVAILABLE,              
//                PINPAD_TYPE,                   
//                PRIMARY_DATA_CAPTURE_PHONE,    
//                PRIMARY_DIAL_ACCESS,  
//                REMINDER_MSG,
//                REMINDER_MSG_IND,              
//                REMINDER_MSG_TIME,             
//                SECONDARY_DATA_CAPTURE_PHONE,  
//                SECONDARY_DIAL_ACCESS,
//                SETTLE_TO_PC,
//                SHARING_GROUPS,
//                IDLE_PROMPT,
//                HEADER_LINE4,
//                HEADER_LINE5,
//                FOOTER_LINE1,
//                FOOTER_LINE2,
//                PRIM_AUTH_DIAL_ACCESS,
//                SEC_AUTH_DIAL_ACCESS,
//                SURCHARGE_IND,
//                SURCHARGE_AMOUNT,
//                PRIMARY_KEY1,
//                PRIMARY_KEY2,
//                CARD_PRESENT,                 
//                FORCE_DIAL,                   
//                AUTO_TIP_PERCENT,             
//                TIP_TIME_SALE,                
//                TIP_ASSIST,                   
//                TIP_ASSIST_PERCENT_1,         
//                TIP_ASSIST_PERCENT_2,         
//                TIP_ASSIST_PERCENT_3,         
//                INV_NUM_KEYS,
//                INV_PROMPT,  
//                DUP_TRAN,    
//                PROMPT_FOR_CLERK,
//                FOLIO_GUEST_CHECK_PROCESSING,
//                FOLIO_ROOM_PROMPT,
//                TIME_DIFFERENCE,
//                ECI_TYPE,
//                CLERK_SERVER_MODE,
//                OVERAGE_PERCENTAGE,
//                CHECK_READER,
//                RIID,
//                CHECK_HOST,
//                CHECK_HOST_FORMAT,
//                AMEX_OPTION,
//                MERCHANT_ID_AMEX,
//                BAUD_RATE_HOST1,
//                BAUD_RATE_AMEXHOST,
//                AMEX_PABX,
//                DEBIT_SETTLE_NUM,
//                DOWNLOAD_SPEED,
//                DIAL_SPEED,
//                TM_OPTIONS,
//                REQUEST_ID
//              )
//              values
//              ( 
//                :manufacturer,           
//                :termModel,              
//                :printerModel,           
//                :printerType,            
//                :fraud,                  
//                :receiptTrunc,           
//                :checkPayment,           
//                :checkMid,               
//                :checkVoice,             
//                :checkAuthPhone,         
//                :tipOption,              
//                :avs,                    
//                :purchasingCard,         
//                :invoiceNumber,          
//                :autoCloseInd,
//                :autoCloseTime,          
//                :autoCloseTime2,
//                :autoCloseReports,
//                :restaurantProcessing,
//                :srvclkProcessing,
//                :merchantAba,            
//                :merchantSettlementAgent,
//                :reimbursementAttribute, 
//                :atmSignLine,            
//                :cashBack,               
//                :cashBackTip,
//                :atmCancelReturn, 
//                :cashBackLimit, 
//                :checkTotalsInd, 
//                :ebtTransType, 
//                :ebtFcsId, 
//                :forceSettleInd, 
//                :parentVnumber, 
//                :passProtect, 
//                :pinpadAvailable, 
//                :pinpadType, 
//                :primaryDataCapturePhone, 
//                :primaryDialAccess, 
//                :reminderMsg, 
//                :reminderMsgInd, 
//                :reminderMsgTime, 
//                :secondaryDataCapturePhone, 
//                :secondaryDialAccess, 
//                :settleToPc, 
//                :sharingGroups,
//                :idlePrompt, 
//                :headerLine4, 
//                :headerLine5, 
//                :footerLine1, 
//                :footerLine2, 
//                :primaryAuthDialAccess, 
//                :secondaryAuthDialAccess,
//                :surchargeInd, 
//                :surchargeAmount, 
//                :primaryKey1, 
//                :primaryKey2,
//                :cardPresent,
//                :forceDial,
//                :autoTipPercent,
//                :tipTimeSale,
//                :tipAssist,
//                :tipAssistPercent1,
//                :tipAssistPercent2,
//                :tipAssistPercent3,
//                :invNumKeys,
//                :invPrompt,
//                :dupTran,
//                :promptForClerk,
//                :folioGuestCheckProcessing,
//                :folioRoomPrompt,
//                :timeDifference,
//                :eciType,
//                :clerkServerMode,
//                :overagePercentage,
//                :checkReader,
//                :riid,
//                :checkHost,
//                :checkHostFormat,
//                :amexOption,
//                :merchantIdAmex,
//                :baudRateHost1,
//                :baudRateAmexHost,
//                :amexPabx,
//                :debitSettleNum,
//                :downloadSpeed,
//                :dialSpeed,
//                :tmOptions,
//                :reqId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mms_expanded_info\n            ( \n              MANUFACTURER,              \n              TERM_MODEL,                \n              PRINTER_MODEL,             \n              PRINTER_TYPE,              \n              FRAUD,                     \n              RECEIPT_TRUNCATION,        \n              CHECK_PAYMENT,             \n              CHECK_MID,                 \n              CHECK_VOICE,               \n              CHECK_AUTH_PHONE,          \n              TIP_OPTION,                \n              AVS,                       \n              PURCHASING_CARD,           \n              INVOICE_NUMBER,            \n              AUTO_CLOSE_IND,            \n              AUTO_CLOSE_TIME,           \n              AUTO_CLOSE_TIME2,\n              AUTO_CLOSE_REPORTS,\n              RESTAURANT_PROCESSING,\n              SRV_CLK_PROCESSING,\n              MERCHANT_ABA,              \n              MERCH_SETTLEMENT_AGENT,    \n              REIMBURSEMENT_ATTRIBUTES,  \n              ATM_RECEIPT_SIGN_LINE,     \n              CASH_BACK,                 \n              CASH_BACK_TIP,\n              ATM_CANCEL_RETURN, \n              CASH_BACK_LIMIT,               \n              CHECK_TOTALS_IND,              \n              EBT_TRANS_TYPE,                \n              EBT_FCS_ID,                    \n              FORCE_SETTLE_IND,\n              PARENT_VNUMBER,                \n              PASS_PROTECT,                  \n              PINPAD_AVAILABLE,              \n              PINPAD_TYPE,                   \n              PRIMARY_DATA_CAPTURE_PHONE,    \n              PRIMARY_DIAL_ACCESS,  \n              REMINDER_MSG,\n              REMINDER_MSG_IND,              \n              REMINDER_MSG_TIME,             \n              SECONDARY_DATA_CAPTURE_PHONE,  \n              SECONDARY_DIAL_ACCESS,\n              SETTLE_TO_PC,\n              SHARING_GROUPS,\n              IDLE_PROMPT,\n              HEADER_LINE4,\n              HEADER_LINE5,\n              FOOTER_LINE1,\n              FOOTER_LINE2,\n              PRIM_AUTH_DIAL_ACCESS,\n              SEC_AUTH_DIAL_ACCESS,\n              SURCHARGE_IND,\n              SURCHARGE_AMOUNT,\n              PRIMARY_KEY1,\n              PRIMARY_KEY2,\n              CARD_PRESENT,                 \n              FORCE_DIAL,                   \n              AUTO_TIP_PERCENT,             \n              TIP_TIME_SALE,                \n              TIP_ASSIST,                   \n              TIP_ASSIST_PERCENT_1,         \n              TIP_ASSIST_PERCENT_2,         \n              TIP_ASSIST_PERCENT_3,         \n              INV_NUM_KEYS,\n              INV_PROMPT,  \n              DUP_TRAN,    \n              PROMPT_FOR_CLERK,\n              FOLIO_GUEST_CHECK_PROCESSING,\n              FOLIO_ROOM_PROMPT,\n              TIME_DIFFERENCE,\n              ECI_TYPE,\n              CLERK_SERVER_MODE,\n              OVERAGE_PERCENTAGE,\n              CHECK_READER,\n              RIID,\n              CHECK_HOST,\n              CHECK_HOST_FORMAT,\n              AMEX_OPTION,\n              MERCHANT_ID_AMEX,\n              BAUD_RATE_HOST1,\n              BAUD_RATE_AMEXHOST,\n              AMEX_PABX,\n              DEBIT_SETTLE_NUM,\n              DOWNLOAD_SPEED,\n              DIAL_SPEED,\n              TM_OPTIONS,\n              REQUEST_ID\n            )\n            values\n            ( \n               :1 ,           \n               :2 ,              \n               :3 ,           \n               :4 ,            \n               :5 ,                  \n               :6 ,           \n               :7 ,           \n               :8 ,               \n               :9 ,             \n               :10 ,         \n               :11 ,              \n               :12 ,                    \n               :13 ,         \n               :14 ,          \n               :15 ,\n               :16 ,          \n               :17 ,\n               :18 ,\n               :19 ,\n               :20 ,\n               :21 ,            \n               :22 ,\n               :23 , \n               :24 ,            \n               :25 ,               \n               :26 ,\n               :27 , \n               :28 , \n               :29 , \n               :30 , \n               :31 , \n               :32 , \n               :33 , \n               :34 , \n               :35 , \n               :36 , \n               :37 , \n               :38 , \n               :39 , \n               :40 , \n               :41 , \n               :42 , \n               :43 , \n               :44 , \n               :45 ,\n               :46 , \n               :47 , \n               :48 , \n               :49 , \n               :50 , \n               :51 , \n               :52 ,\n               :53 , \n               :54 , \n               :55 , \n               :56 ,\n               :57 ,\n               :58 ,\n               :59 ,\n               :60 ,\n               :61 ,\n               :62 ,\n               :63 ,\n               :64 ,\n               :65 ,\n               :66 ,\n               :67 ,\n               :68 ,\n               :69 ,\n               :70 ,\n               :71 ,\n               :72 ,\n               :73 ,\n               :74 ,\n               :75 ,\n               :76 ,\n               :77 ,\n               :78 ,\n               :79 ,\n               :80 ,\n               :81 ,\n               :82 ,\n               :83 ,\n               :84 ,\n               :85 ,\n               :86 ,\n               :87 ,\n               :88 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.MmsExpandedFieldsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,manufacturer);
   __sJT_st.setString(2,termModel);
   __sJT_st.setString(3,printerModel);
   __sJT_st.setString(4,printerType);
   __sJT_st.setString(5,fraud);
   __sJT_st.setString(6,receiptTrunc);
   __sJT_st.setString(7,checkPayment);
   __sJT_st.setString(8,checkMid);
   __sJT_st.setString(9,checkVoice);
   __sJT_st.setString(10,checkAuthPhone);
   __sJT_st.setString(11,tipOption);
   __sJT_st.setString(12,avs);
   __sJT_st.setString(13,purchasingCard);
   __sJT_st.setString(14,invoiceNumber);
   __sJT_st.setString(15,autoCloseInd);
   __sJT_st.setString(16,autoCloseTime);
   __sJT_st.setString(17,autoCloseTime2);
   __sJT_st.setString(18,autoCloseReports);
   __sJT_st.setString(19,restaurantProcessing);
   __sJT_st.setString(20,srvclkProcessing);
   __sJT_st.setString(21,merchantAba);
   __sJT_st.setString(22,merchantSettlementAgent);
   __sJT_st.setString(23,reimbursementAttribute);
   __sJT_st.setString(24,atmSignLine);
   __sJT_st.setString(25,cashBack);
   __sJT_st.setString(26,cashBackTip);
   __sJT_st.setString(27,atmCancelReturn);
   __sJT_st.setString(28,cashBackLimit);
   __sJT_st.setString(29,checkTotalsInd);
   __sJT_st.setString(30,ebtTransType);
   __sJT_st.setString(31,ebtFcsId);
   __sJT_st.setString(32,forceSettleInd);
   __sJT_st.setString(33,parentVnumber);
   __sJT_st.setString(34,passProtect);
   __sJT_st.setString(35,pinpadAvailable);
   __sJT_st.setString(36,pinpadType);
   __sJT_st.setString(37,primaryDataCapturePhone);
   __sJT_st.setString(38,primaryDialAccess);
   __sJT_st.setString(39,reminderMsg);
   __sJT_st.setString(40,reminderMsgInd);
   __sJT_st.setString(41,reminderMsgTime);
   __sJT_st.setString(42,secondaryDataCapturePhone);
   __sJT_st.setString(43,secondaryDialAccess);
   __sJT_st.setString(44,settleToPc);
   __sJT_st.setString(45,sharingGroups);
   __sJT_st.setString(46,idlePrompt);
   __sJT_st.setString(47,headerLine4);
   __sJT_st.setString(48,headerLine5);
   __sJT_st.setString(49,footerLine1);
   __sJT_st.setString(50,footerLine2);
   __sJT_st.setString(51,primaryAuthDialAccess);
   __sJT_st.setString(52,secondaryAuthDialAccess);
   __sJT_st.setString(53,surchargeInd);
   __sJT_st.setString(54,surchargeAmount);
   __sJT_st.setString(55,primaryKey1);
   __sJT_st.setString(56,primaryKey2);
   __sJT_st.setString(57,cardPresent);
   __sJT_st.setString(58,forceDial);
   __sJT_st.setString(59,autoTipPercent);
   __sJT_st.setString(60,tipTimeSale);
   __sJT_st.setString(61,tipAssist);
   __sJT_st.setString(62,tipAssistPercent1);
   __sJT_st.setString(63,tipAssistPercent2);
   __sJT_st.setString(64,tipAssistPercent3);
   __sJT_st.setString(65,invNumKeys);
   __sJT_st.setString(66,invPrompt);
   __sJT_st.setString(67,dupTran);
   __sJT_st.setString(68,promptForClerk);
   __sJT_st.setString(69,folioGuestCheckProcessing);
   __sJT_st.setString(70,folioRoomPrompt);
   __sJT_st.setString(71,timeDifference);
   __sJT_st.setString(72,eciType);
   __sJT_st.setString(73,clerkServerMode);
   __sJT_st.setString(74,overagePercentage);
   __sJT_st.setString(75,checkReader);
   __sJT_st.setString(76,riid);
   __sJT_st.setString(77,checkHost);
   __sJT_st.setString(78,checkHostFormat);
   __sJT_st.setString(79,amexOption);
   __sJT_st.setString(80,merchantIdAmex);
   __sJT_st.setString(81,baudRateHost1);
   __sJT_st.setString(82,baudRateAmexHost);
   __sJT_st.setString(83,amexPabx);
   __sJT_st.setString(84,debitSettleNum);
   __sJT_st.setString(85,downloadSpeed);
   __sJT_st.setString(86,dialSpeed);
   __sJT_st.setString(87,tmOptions);
   __sJT_st.setLong(88,reqId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4154^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setData()", e.toString());
      addError(e.toString());
    }
    finally
    {
      cleanUp();
    }
  
  }

  public void getData(long pk, long reqId)
  {
    ResultSetIterator   it  = null;
    ResultSet           rs  = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:4179^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    mms_expanded_info
//          where   request_id = :reqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    mms_expanded_info\n        where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.MmsExpandedFieldsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.MmsExpandedFieldsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4184^7*/
      
      rs = it.getResultSet();
 
      if(rs.next())
      {
        this.manufacturer             = isBlank(rs.getString("MANUFACTURER"))             ? "" : rs.getString("MANUFACTURER");
        this.termModel                = isBlank(rs.getString("TERM_MODEL"))               ? "" : rs.getString("TERM_MODEL");
        this.printerModel             = isBlank(rs.getString("PRINTER_MODEL"))            ? "" : rs.getString("PRINTER_MODEL");
        this.printerType              = isBlank(rs.getString("PRINTER_TYPE"))             ? "" : rs.getString("PRINTER_TYPE");
        
        encodePrinterTypeModel();

        this.fraud                      = isBlank(rs.getString("FRAUD"))                        ? "" : rs.getString("FRAUD");
        this.receiptTrunc               = isBlank(rs.getString("RECEIPT_TRUNCATION"))           ? "" : rs.getString("RECEIPT_TRUNCATION");
        this.checkPayment               = isBlank(rs.getString("CHECK_PAYMENT"))                ? "" : rs.getString("CHECK_PAYMENT");
        this.checkMid                   = isBlank(rs.getString("CHECK_MID"))                    ? "" : rs.getString("CHECK_MID");
        this.checkVoice                 = isBlank(rs.getString("CHECK_VOICE"))                  ? "" : rs.getString("CHECK_VOICE");
        this.checkAuthPhone             = isBlank(rs.getString("CHECK_AUTH_PHONE"))             ? "" : rs.getString("CHECK_AUTH_PHONE");
        this.tipOption                  = isBlank(rs.getString("TIP_OPTION"))                   ? "" : rs.getString("TIP_OPTION");
        this.avs                        = isBlank(rs.getString("AVS"))                          ? "" : rs.getString("AVS");
        this.purchasingCard             = isBlank(rs.getString("PURCHASING_CARD"))              ? "" : rs.getString("PURCHASING_CARD");
        this.invoiceNumber              = isBlank(rs.getString("INVOICE_NUMBER"))               ? "" : rs.getString("INVOICE_NUMBER");
        this.autoCloseInd               = isBlank(rs.getString("AUTO_CLOSE_IND"))               ? "" : rs.getString("AUTO_CLOSE_IND");
        this.autoCloseTime              = isBlank(rs.getString("AUTO_CLOSE_TIME"))              ? "" : rs.getString("AUTO_CLOSE_TIME");

        this.autoCloseTime2             = isBlank(rs.getString("AUTO_CLOSE_TIME2"))             ? "" : rs.getString("AUTO_CLOSE_TIME2");
        this.autoCloseReports           = isBlank(rs.getString("AUTO_CLOSE_REPORTS"))           ? "" : rs.getString("AUTO_CLOSE_REPORTS");
        this.restaurantProcessing       = isBlank(rs.getString("RESTAURANT_PROCESSING"))        ? "" : rs.getString("RESTAURANT_PROCESSING");
        this.srvclkProcessing           = isBlank(rs.getString("SRV_CLK_PROCESSING"))           ? "" : rs.getString("SRV_CLK_PROCESSING");

        this.merchantAba                = isBlank(rs.getString("MERCHANT_ABA"))                 ? "" : rs.getString("MERCHANT_ABA");
        this.merchantSettlementAgent    = isBlank(rs.getString("MERCH_SETTLEMENT_AGENT"))       ? "" : rs.getString("MERCH_SETTLEMENT_AGENT");
        this.reimbursementAttribute     = isBlank(rs.getString("REIMBURSEMENT_ATTRIBUTES"))     ? "" : rs.getString("REIMBURSEMENT_ATTRIBUTES");
        this.atmSignLine                = isBlank(rs.getString("ATM_RECEIPT_SIGN_LINE"))        ? "" : rs.getString("ATM_RECEIPT_SIGN_LINE");
        this.cashBack                   = isBlank(rs.getString("CASH_BACK"))                    ? "" : rs.getString("CASH_BACK");
        this.cashBackTip                = isBlank(rs.getString("CASH_BACK_TIP"))                ? "" : rs.getString("CASH_BACK_TIP");

        this.atmCancelReturn            = isBlank(rs.getString("ATM_CANCEL_RETURN"))            ? "" : rs.getString("ATM_CANCEL_RETURN");
        this.cashBackLimit              = isBlank(rs.getString("CASH_BACK_LIMIT"))              ? "" : rs.getString("CASH_BACK_LIMIT");      
        this.checkTotalsInd             = isBlank(rs.getString("CHECK_TOTALS_IND"))             ? "" : rs.getString("CHECK_TOTALS_IND");
        this.ebtTransType               = isBlank(rs.getString("EBT_TRANS_TYPE"))               ? "" : rs.getString("EBT_TRANS_TYPE");
        this.ebtFcsId                   = isBlank(rs.getString("EBT_FCS_ID"))                   ? "" : rs.getString("EBT_FCS_ID");
        this.forceSettleInd             = isBlank(rs.getString("FORCE_SETTLE_IND"))             ? "" : rs.getString("FORCE_SETTLE_IND");
        this.parentVnumber              = isBlank(rs.getString("PARENT_VNUMBER"))               ? "" : rs.getString("PARENT_VNUMBER");
        this.passProtect                = isBlank(rs.getString("PASS_PROTECT"))                 ? "" : rs.getString("PASS_PROTECT");
        this.pinpadAvailable            = isBlank(rs.getString("PINPAD_AVAILABLE"))             ? "" : rs.getString("PINPAD_AVAILABLE");
        this.pinpadType                 = isBlank(rs.getString("PINPAD_TYPE"))                  ? "" : rs.getString("PINPAD_TYPE");

        //this goes after we get pinpadAvailable and pinpadType
        encodePinpadTypeAvailable();

        this.primaryDataCapturePhone    = isBlank(rs.getString("PRIMARY_DATA_CAPTURE_PHONE"))   ? "" : rs.getString("PRIMARY_DATA_CAPTURE_PHONE");
        this.primaryDialAccess          = isBlank(rs.getString("PRIMARY_DIAL_ACCESS"))          ? "" : rs.getString("PRIMARY_DIAL_ACCESS");
        this.reminderMsg                = isBlank(rs.getString("REMINDER_MSG"))                 ? "" : rs.getString("REMINDER_MSG");
        this.reminderMsgInd             = isBlank(rs.getString("REMINDER_MSG_IND"))             ? "" : rs.getString("REMINDER_MSG_IND");
        this.reminderMsgTime            = isBlank(rs.getString("REMINDER_MSG_TIME"))            ? "" : rs.getString("REMINDER_MSG_TIME");
        this.secondaryDataCapturePhone  = isBlank(rs.getString("SECONDARY_DATA_CAPTURE_PHONE")) ? "" : rs.getString("SECONDARY_DATA_CAPTURE_PHONE");
        this.secondaryDialAccess        = isBlank(rs.getString("SECONDARY_DIAL_ACCESS"))        ? "" : rs.getString("SECONDARY_DIAL_ACCESS");
        this.settleToPc                 = isBlank(rs.getString("SETTLE_TO_PC"))                 ? "" : rs.getString("SETTLE_TO_PC");
        this.sharingGroups              = isBlank(rs.getString("SHARING_GROUPS"))               ? "" : rs.getString("SHARING_GROUPS");
        this.idlePrompt                 = isBlank(rs.getString("IDLE_PROMPT"))                  ? "" : rs.getString("IDLE_PROMPT");
        this.headerLine4                = isBlank(rs.getString("HEADER_LINE4"))                 ? "" : rs.getString("HEADER_LINE4");
        this.headerLine5                = isBlank(rs.getString("HEADER_LINE5"))                 ? "" : rs.getString("HEADER_LINE5");
        this.footerLine1                = isBlank(rs.getString("FOOTER_LINE1"))                 ? "" : rs.getString("FOOTER_LINE1");
        this.footerLine2                = isBlank(rs.getString("FOOTER_LINE2"))                 ? "" : rs.getString("FOOTER_LINE2");
        this.primaryAuthDialAccess      = isBlank(rs.getString("PRIM_AUTH_DIAL_ACCESS"))        ? "" : rs.getString("PRIM_AUTH_DIAL_ACCESS");
        this.secondaryAuthDialAccess    = isBlank(rs.getString("SEC_AUTH_DIAL_ACCESS"))         ? "" : rs.getString("SEC_AUTH_DIAL_ACCESS");
        this.surchargeInd               = isBlank(rs.getString("SURCHARGE_IND"))                ? "" : rs.getString("SURCHARGE_IND");
        this.surchargeAmount            = isBlank(rs.getString("SURCHARGE_AMOUNT"))             ? "" : rs.getString("SURCHARGE_AMOUNT");
        this.primaryKey1                = isBlank(rs.getString("PRIMARY_KEY1"))                 ? "" : rs.getString("PRIMARY_KEY1");
        this.primaryKey2                = isBlank(rs.getString("PRIMARY_KEY2"))                 ? "" : rs.getString("PRIMARY_KEY2");
        this.cardPresent                = isBlank(rs.getString("CARD_PRESENT"))                 ? "" : rs.getString("CARD_PRESENT");        
        this.forceDial                  = isBlank(rs.getString("FORCE_DIAL"))                   ? "" : rs.getString("FORCE_DIAL");                              
        this.autoTipPercent             = isBlank(rs.getString("AUTO_TIP_PERCENT"))             ? "" : rs.getString("AUTO_TIP_PERCENT");    
        this.tipTimeSale                = isBlank(rs.getString("TIP_TIME_SALE"))                ? "" : rs.getString("TIP_TIME_SALE");                        
        this.tipAssist                  = isBlank(rs.getString("TIP_ASSIST"))                   ? "" : rs.getString("TIP_ASSIST");                              
        this.tipAssistPercent1          = isBlank(rs.getString("TIP_ASSIST_PERCENT_1"))         ? "" : rs.getString("TIP_ASSIST_PERCENT_1");          
        this.tipAssistPercent2          = isBlank(rs.getString("TIP_ASSIST_PERCENT_2"))         ? "" : rs.getString("TIP_ASSIST_PERCENT_2");          
        this.tipAssistPercent3          = isBlank(rs.getString("TIP_ASSIST_PERCENT_3"))         ? "" : rs.getString("TIP_ASSIST_PERCENT_3");          
        this.invNumKeys                 = isBlank(rs.getString("INV_NUM_KEYS"))                 ? "" : rs.getString("INV_NUM_KEYS");
        this.invPrompt                  = isBlank(rs.getString("INV_PROMPT"))                   ? "" : rs.getString("INV_PROMPT");
        this.dupTran                    = isBlank(rs.getString("DUP_TRAN"))                     ? "" : rs.getString("DUP_TRAN");
        this.promptForClerk             = isBlank(rs.getString("PROMPT_FOR_CLERK"))             ? "" : rs.getString("PROMPT_FOR_CLERK");             
        this.folioGuestCheckProcessing  = isBlank(rs.getString("FOLIO_GUEST_CHECK_PROCESSING")) ? "" : rs.getString("FOLIO_GUEST_CHECK_PROCESSING"); 
        this.folioRoomPrompt            = isBlank(rs.getString("FOLIO_ROOM_PROMPT"))            ? "" : rs.getString("FOLIO_ROOM_PROMPT");            

        // Vericentre spec fields
        this.timeDifference              = isBlank(rs.getString("TIME_DIFFERENCE"))             ? "" : rs.getString("TIME_DIFFERENCE");
        this.eciType                     = isBlank(rs.getString("ECI_TYPE"))                    ? "" : rs.getString("ECI_TYPE");
        this.clerkServerMode             = isBlank(rs.getString("CLERK_SERVER_MODE"))           ? "" : rs.getString("CLERK_SERVER_MODE");
        this.overagePercentage           = isBlank(rs.getString("OVERAGE_PERCENTAGE"))          ? "" : rs.getString("OVERAGE_PERCENTAGE");
        this.checkReader                 = isBlank(rs.getString("CHECK_READER"))                ? "" : rs.getString("CHECK_READER");
        this.riid                        = isBlank(rs.getString("RIID"))                        ? "" : rs.getString("RIID");
        this.checkHost                   = isBlank(rs.getString("CHECK_HOST"))                  ? "" : rs.getString("CHECK_HOST");
        this.checkHostFormat             = isBlank(rs.getString("CHECK_HOST_FORMAT"))           ? "" : rs.getString("CHECK_HOST_FORMAT");
        this.amexOption                  = isBlank(rs.getString("AMEX_OPTION"))                 ? "" : rs.getString("AMEX_OPTION");
        this.merchantIdAmex              = isBlank(rs.getString("MERCHANT_ID_AMEX"))            ? "" : rs.getString("MERCHANT_ID_AMEX");
        this.baudRateHost1               = isBlank(rs.getString("BAUD_RATE_HOST1"))             ? "" : rs.getString("BAUD_RATE_HOST1");
        this.baudRateAmexHost            = isBlank(rs.getString("BAUD_RATE_AMEXHOST"))          ? "" : rs.getString("BAUD_RATE_AMEXHOST");
        this.amexPabx                    = isBlank(rs.getString("AMEX_PABX"))                   ? "" : rs.getString("AMEX_PABX");
    
        // TermMaster spec fields
        this.debitSettleNum              = isBlank(rs.getString("DEBIT_SETTLE_NUM"))            ? "" : rs.getString("DEBIT_SETTLE_NUM");
        this.downloadSpeed               = isBlank(rs.getString("DOWNLOAD_SPEED"))              ? "" : rs.getString("DOWNLOAD_SPEED");
        this.dialSpeed                   = isBlank(rs.getString("DIAL_SPEED"))                  ? "" : rs.getString("DIAL_SPEED");
        this.tmOptions                   = isBlank(rs.getString("TM_OPTIONS"))                  ? "" : rs.getString("TM_OPTIONS");

        rs.close();
        it.close();
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:4297^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  bank.merchbank_transit_route_num    MERCHANT_ABA,
//                    app.app_type                        APP_TYPE,
//                    pf.BRAND_IND                        BRAND_IND, 
//                    pf.REMINDER_FLAG                    REMINDER_FLAG, 
//                    pf.REMINDER_TIME                    REMINDER_TIME, 
//                    pf.AUTOCLOSE_FLAG                   AUTOCLOSE_FLAG, 
//                    pf.AUTOCLOSE_TIME                   AUTOCLOSE_TIME,
//                    pf.RETAIL_CLERK                     RETAIL_CLERK,
//                    pf.TIP_OPTION                       TIP_OPTION,
//                    pf.ACCESS_CODE                      ACCESS_CODE,
//                    pf.HEADER_LINE4                     HEADER_LINE4,
//                    pf.HEADER_LINE5                     HEADER_LINE5,
//                    pf.ACCESS_CODE_FLAG                 ACCESS_CODE_FLAG, 
//                    pf.AUTO_BATCH_CLOSE_FLAG            AUTO_BATCH_CLOSE_FLAG,
//                    pf.AUTO_BATCH_CLOSE_TIME            AUTO_BATCH_CLOSE_TIME,
//                    pf.HEADER_LINE1                     HEADER_LINE1,
//                    pf.HEADER_LINE2                     HEADER_LINE2, 
//                    pf.FOOTER                           FOOTER,
//                    pf.RESET_REF_FLAG                   RESET_REF_FLAG,
//                    pf.INVOICE_PROMPT_FLAG              INVOICE_PROMPT_FLAG, 
//                    pf.FRAUD_CONTROL_FLAG               FRAUD_CONTROL_FLAG,
//                    pf.PASSWORD_PROTECT_FLAG            PASSWORD_PROTECT_FLAG,
//                    pf.PHONE_TRAINING_FLAG              PHONE_TRAINING_FLAG,
//                    pf.TERMINAL_REMINDER_FLAG           TERMINAL_REMINDER_FLAG,
//                    pf.TERMINAL_REMINDER_TIME           TERMINAL_REMINDER_TIME,
//                    pf.TIP_OPTION_FLAG                  TIP_OPTION_FLAG,
//                    pf.CLERK_ENABLED_FLAG               CLERK_ENABLED_FLAG,
//                    pf.EQUIPMENT_COMMENT                EQUIPMENT_COMMENT,
//                    pf.HEADER_LINE1_FLAG                HEADER_LINE1_FLAG,
//                    pf.HEADER_LINE2_FLAG                HEADER_LINE2_FLAG,
//                    pf.FOOTER_FLAG                      FOOTER_FLAG,
//                    pf.HEADER_LINE4_FLAG                HEADER_LINE4_FLAG,
//                    pf.HEADER_LINE5_FLAG                HEADER_LINE5_FLAG,
//                    pf.ADDRESSTYPE_CODE                 ADDRESSTYPE_CODE,
//                    pf.FRAUD_CONTROL_OPTION             FRAUD_CONTROL_OPTION,
//                    pf.NO_PRINTER_NEEDED                NO_PRINTER_NEEDED,         
//                    pf.AVS_FLAG                         AVS_FLAG,
//                    pf.PHONE_TRAINING                   PHONE_TRAINING, 
//                    pf.CASH_BACK_FLAG                   CASH_BACK_FLAG,
//                    pf.CASH_BACK_LIMIT                  CASH_BACK_LIMIT,
//                    pf.DEBIT_SURCHARGE_FLAG             DEBIT_SURCHARGE_FLAG,
//                    pf.DEBIT_SURCHARGE_AMOUNT           DEBIT_SURCHARGE_AMOUNT,
//                    pf.CUSTOMER_SERVICE_PHONE           CUSTOMER_SERVICE_PHONE,
//                    pf.PURCHASING_CARD_FLAG             PURCHASING_CARD_FLAG,
//                    pf.CARD_TRUNCATION_FLAG             CARD_TRUNCATION_FLAG,
//                    pf.SHIPPING_CONTACT_NAME            SHIPPING_CONTACT_NAME,
//                    pf.SHIPPING_METHOD                  SHIPPING_METHOD,
//                    pf.SHIPPING_COMMENT                 SHIPPING_COMMENT,
//                    pf.TIP_TIME_SALE                    TIP_TIME_SALE
//            from    merchbank       bank,
//                    pos_features    pf,
//                    application     app
//            where   bank.app_seq_num = :pk and
//                    bank.app_seq_num = pf.app_seq_num and
//                    bank.app_seq_num = app.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bank.merchbank_transit_route_num    MERCHANT_ABA,\n                  app.app_type                        APP_TYPE,\n                  pf.BRAND_IND                        BRAND_IND, \n                  pf.REMINDER_FLAG                    REMINDER_FLAG, \n                  pf.REMINDER_TIME                    REMINDER_TIME, \n                  pf.AUTOCLOSE_FLAG                   AUTOCLOSE_FLAG, \n                  pf.AUTOCLOSE_TIME                   AUTOCLOSE_TIME,\n                  pf.RETAIL_CLERK                     RETAIL_CLERK,\n                  pf.TIP_OPTION                       TIP_OPTION,\n                  pf.ACCESS_CODE                      ACCESS_CODE,\n                  pf.HEADER_LINE4                     HEADER_LINE4,\n                  pf.HEADER_LINE5                     HEADER_LINE5,\n                  pf.ACCESS_CODE_FLAG                 ACCESS_CODE_FLAG, \n                  pf.AUTO_BATCH_CLOSE_FLAG            AUTO_BATCH_CLOSE_FLAG,\n                  pf.AUTO_BATCH_CLOSE_TIME            AUTO_BATCH_CLOSE_TIME,\n                  pf.HEADER_LINE1                     HEADER_LINE1,\n                  pf.HEADER_LINE2                     HEADER_LINE2, \n                  pf.FOOTER                           FOOTER,\n                  pf.RESET_REF_FLAG                   RESET_REF_FLAG,\n                  pf.INVOICE_PROMPT_FLAG              INVOICE_PROMPT_FLAG, \n                  pf.FRAUD_CONTROL_FLAG               FRAUD_CONTROL_FLAG,\n                  pf.PASSWORD_PROTECT_FLAG            PASSWORD_PROTECT_FLAG,\n                  pf.PHONE_TRAINING_FLAG              PHONE_TRAINING_FLAG,\n                  pf.TERMINAL_REMINDER_FLAG           TERMINAL_REMINDER_FLAG,\n                  pf.TERMINAL_REMINDER_TIME           TERMINAL_REMINDER_TIME,\n                  pf.TIP_OPTION_FLAG                  TIP_OPTION_FLAG,\n                  pf.CLERK_ENABLED_FLAG               CLERK_ENABLED_FLAG,\n                  pf.EQUIPMENT_COMMENT                EQUIPMENT_COMMENT,\n                  pf.HEADER_LINE1_FLAG                HEADER_LINE1_FLAG,\n                  pf.HEADER_LINE2_FLAG                HEADER_LINE2_FLAG,\n                  pf.FOOTER_FLAG                      FOOTER_FLAG,\n                  pf.HEADER_LINE4_FLAG                HEADER_LINE4_FLAG,\n                  pf.HEADER_LINE5_FLAG                HEADER_LINE5_FLAG,\n                  pf.ADDRESSTYPE_CODE                 ADDRESSTYPE_CODE,\n                  pf.FRAUD_CONTROL_OPTION             FRAUD_CONTROL_OPTION,\n                  pf.NO_PRINTER_NEEDED                NO_PRINTER_NEEDED,         \n                  pf.AVS_FLAG                         AVS_FLAG,\n                  pf.PHONE_TRAINING                   PHONE_TRAINING, \n                  pf.CASH_BACK_FLAG                   CASH_BACK_FLAG,\n                  pf.CASH_BACK_LIMIT                  CASH_BACK_LIMIT,\n                  pf.DEBIT_SURCHARGE_FLAG             DEBIT_SURCHARGE_FLAG,\n                  pf.DEBIT_SURCHARGE_AMOUNT           DEBIT_SURCHARGE_AMOUNT,\n                  pf.CUSTOMER_SERVICE_PHONE           CUSTOMER_SERVICE_PHONE,\n                  pf.PURCHASING_CARD_FLAG             PURCHASING_CARD_FLAG,\n                  pf.CARD_TRUNCATION_FLAG             CARD_TRUNCATION_FLAG,\n                  pf.SHIPPING_CONTACT_NAME            SHIPPING_CONTACT_NAME,\n                  pf.SHIPPING_METHOD                  SHIPPING_METHOD,\n                  pf.SHIPPING_COMMENT                 SHIPPING_COMMENT,\n                  pf.TIP_TIME_SALE                    TIP_TIME_SALE\n          from    merchbank       bank,\n                  pos_features    pf,\n                  application     app\n          where   bank.app_seq_num =  :1  and\n                  bank.app_seq_num = pf.app_seq_num and\n                  bank.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.MmsExpandedFieldsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.MmsExpandedFieldsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4354^9*/

        rs = it.getResultSet();
 
        if(rs.next())
        {

          if(rs.getInt("APP_TYPE") == mesConstants.APP_TYPE_CBT || 
             rs.getInt("APP_TYPE") == mesConstants.APP_TYPE_CBT_NEW)
          {
            this.merchantAba    = "990004556"; //cbt
          }
          else if(rs.getInt("APP_TYPE") == mesConstants.APP_TYPE_BBT)
          {
            this.merchantAba    = "990024552"; //bbt
          }
          else
          {
            this.merchantAba    = "990038865"; //everyone else
          }

          this.reimbursementAttribute = "Z";
          this.tipTimeSale      = isBlank(rs.getString("TIP_TIME_SALE")) ? "" : rs.getString("TIP_TIME_SALE");                        


          if((!isBlank(rs.getString("AUTO_BATCH_CLOSE_FLAG")) && rs.getString("AUTO_BATCH_CLOSE_FLAG").equals("Y")) || !isBlank(rs.getString("AUTO_BATCH_CLOSE_TIME")))
          {
            this.autoCloseInd   = "1";
            this.autoCloseTime  = getMilitaryFormat(rs.getString("AUTO_BATCH_CLOSE_TIME"));
          }
          else
          {
            this.autoCloseInd   = "0";
          }

          if(!isBlank(rs.getString("INVOICE_PROMPT_FLAG")) && rs.getString("INVOICE_PROMPT_FLAG").equals("Y"))
          {
            this.invoiceNumber   = "Y";
          }
          else
          {
            this.invoiceNumber   = "N";
          }

          if(!isBlank(rs.getString("AVS_FLAG")) && rs.getString("AVS_FLAG").equals("Y"))
          {
            this.avs   = "Y";
          }
          else
          {
            this.avs   = "N";
          }

          if(!isBlank(rs.getString("PURCHASING_CARD_FLAG")) && rs.getString("PURCHASING_CARD_FLAG").equals("Y"))
          {
            this.purchasingCard   = "Y";
          }
          else
          {
            this.purchasingCard   = "N";
          }

          if(!isBlank(rs.getString("CARD_TRUNCATION_FLAG")) && rs.getString("CARD_TRUNCATION_FLAG").equals("Y"))
          {
            this.receiptTrunc   = "Y";
          }
          else
          {
            this.receiptTrunc   = "N";
          }

          if(!isBlank(rs.getString("HEADER_LINE4")))
          {
            this.headerLine4 = rs.getString("HEADER_LINE4");
          }

          if(!isBlank(rs.getString("HEADER_LINE5")))
          {
            this.headerLine5 = rs.getString("HEADER_LINE5");
          }

          if(!isBlank(rs.getString("FOOTER")))
          {
            this.footerLine1 = rs.getString("FOOTER");
          }

        }
       
        rs.close();
        it.close();

        /*@lineinfo:generated-code*//*@lineinfo:4445^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  AVS_FLAG                   AVS_FLAG,
//                    CASH_BACK_FLAG             CASH_BACK_FLAG,
//                    CASH_BACK_LIMIT            CASH_BACK_LIMIT,
//                    DEBIT_SURCHARGE_FLAG       DEBIT_SURCHARGE_FLAG,
//                    DEBIT_SURCHARGE_AMOUNT     DEBIT_SURCHARGE_AMOUNT,
//                    PURCHASING_CARD_FLAG       PURCHASING_CARD_FLAG,
//                    CARD_TRUNCATION_FLAG       CARD_TRUNCATION_FLAG
//  
//            from    TRANSCOM_MERCHANT
//    
//            where   app_seq_num = :pk 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  AVS_FLAG                   AVS_FLAG,\n                  CASH_BACK_FLAG             CASH_BACK_FLAG,\n                  CASH_BACK_LIMIT            CASH_BACK_LIMIT,\n                  DEBIT_SURCHARGE_FLAG       DEBIT_SURCHARGE_FLAG,\n                  DEBIT_SURCHARGE_AMOUNT     DEBIT_SURCHARGE_AMOUNT,\n                  PURCHASING_CARD_FLAG       PURCHASING_CARD_FLAG,\n                  CARD_TRUNCATION_FLAG       CARD_TRUNCATION_FLAG\n\n          from    TRANSCOM_MERCHANT\n  \n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.MmsExpandedFieldsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.MmsExpandedFieldsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4458^9*/

        rs = it.getResultSet();
 
        if(rs.next())
        {
          if(!isBlank(rs.getString("AVS_FLAG")) && rs.getString("AVS_FLAG").equals("Y"))
          {
            this.avs   = "Y";
          }
          else
          {
            this.avs   = "N";
          }

          if(!isBlank(rs.getString("PURCHASING_CARD_FLAG")) && rs.getString("PURCHASING_CARD_FLAG").equals("Y"))
          {
            this.purchasingCard   = "Y";
          }
          else
          {
            this.purchasingCard   = "N";
          }

          if(!isBlank(rs.getString("CARD_TRUNCATION_FLAG")) && rs.getString("CARD_TRUNCATION_FLAG").equals("Y"))
          {
            this.receiptTrunc   = "N";
          }
          else
          {
            this.receiptTrunc   = "Y";
          }
        }
       
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
      addError(e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  
  }

  private String getMilitaryFormat(String timeStr)
  {
    String result = "";

    if(isBlank(timeStr))
    {
      return result;
    }

    int     colonIdx  = timeStr.indexOf(":");
    String  hr        = timeStr.substring(0,colonIdx);
    String  mint      = timeStr.substring(colonIdx + 1, colonIdx + 3);
    String  ampm      = timeStr.substring(colonIdx + 4); 

    if(ampm.equals("AM"))
    {
      if(hr.length() == 1)
      {
        hr = "0" + hr;
      }
      
      if(hr.equals("12"))
      {
        hr = "00";
      }
    }
    else if(ampm.equals("PM"))
    {
      if(!hr.equals("12"))
      {
        try
        {
          int tempInt = Integer.parseInt(hr);
          tempInt += 12;
          hr = Integer.toString(tempInt);
        }
        catch(Exception e)
        {
        
        }
      }
    }

    result = hr + mint;
    if(result.equals("0000"))
    {
      result = "0001";
    }
    return result;
  }

  public void setDefaults()
  {
  
  }

  private boolean recordExists(long reqId)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:4576^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  request_id 
//          from    mms_expanded_info
//          where   request_id = :reqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  request_id \n        from    mms_expanded_info\n        where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.MmsExpandedFieldsBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.MmsExpandedFieldsBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4581^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      result = false;
      logEntry("recordExists()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return result;
  }



  public String  getTermModel()
  {
    return this.termModel;
  }

  public String getPrinterTypeModel()
  {             
    return this.printerTypeModel;
  }

  public String getFraud()
  {
    return this.fraud;
  }

  public String getReceiptTrunc()
  {
    return this.receiptTrunc;
  }
  
  public String  getCheckPayment()
  {
    return checkPayment;
  }
  
  public String  getCheckMid()
  {
    return this.checkMid;
  }

  public String  getCheckVoice()
  {
    return this.checkVoice;
  }

  public String getCheckAuthPhone()
  {
    return this.checkAuthPhone;
  }

  public String getTipOption()
  {
    return this.tipOption;
  }

  public String getAvs()
  {
    return this.avs;
  }

  public String getPurchasingCard()
  {
    return this.purchasingCard;
  }
  
  public String getInvoiceNumber()
  {
    return this.invoiceNumber;
  }

  public String getAutoCloseInd()
  {
    return this.autoCloseInd;
  }

  public String getAutoCloseTime()
  {
    return this.autoCloseTime;
  }

  public String getAutoCloseTime2()
  {
    return this.autoCloseTime2;
  }

  public String getAutoCloseReports()
  {
    return this.autoCloseReports;
  }

  public String getMerchantAba()
  {
    return this.merchantAba;
  }

  public String getMerchantSettlementAgent()
  {
    return this.merchantSettlementAgent;
  }

  public String getReimbursementAttribute()
  {
    return this.reimbursementAttribute;
  }

  public String getAtmSignLine()
  {
    return this.atmSignLine;
  }

  public String getCashBack()
  {
    return this.cashBack;
  }
                       
  public String getCashBackTip()
  {
    return this.cashBackTip;
  }


  public String getApplicationShown()
  {
    return this.applicationShown;
  }



  public String getPassProtect()
  {
    return this.passProtect;
  }
  public String getCheckTotalsInd()
  {
    return this.checkTotalsInd;
  }
  public String getForceSettleInd()
  {
    return this.forceSettleInd;
  }
  public String getSettleToPc()
  {
    return this.settleToPc;
  }
  public String getAtmCancelReturn()
  {
    return this.atmCancelReturn;
  }


  public String getPinpadTypeAvailable()
  {
    return this.pinpadTypeAvailable;
  }

  public String getRestaurantProcessing()
  {
    return this.restaurantProcessing;
  }

  public String getSrvclkProcessing()
  {
    return this.srvclkProcessing;
  }


  public String getEbtTransType()
  {
    return this.ebtTransType;
  }
  public String getEbtFcsId()
  {
    return this.ebtFcsId;
  }
  public String getReminderMsgInd()
  {
    return this.reminderMsgInd;
  }
  public String getReminderMsgTime()
  {
    return this.reminderMsgTime;
  }
  public String getReminderMsg()
  {
    return this.reminderMsg;
  }
  public String getParentVnumber()
  {
    return this.parentVnumber;
  }
  public String getPrimaryDialAccess()
  {
    return this.primaryDialAccess;
  }
  public String getPrimaryDataCapturePhone()
  {
    return this.primaryDataCapturePhone;
  }
  public String getSecondaryDialAccess()
  {
    return this.secondaryDialAccess;
  }
  public String getSecondaryDataCapturePhone()
  {
    return this.secondaryDataCapturePhone;
  }
  public String getCashBackLimit()
  {
    return this.cashBackLimit;
  }

  public String getIdlePrompt()
  {
    return this.idlePrompt;
  }

  public String getHeaderLine4()
  {
    return this.headerLine4;
  }

  public String getHeaderLine5()
  {
    return this.headerLine5;
  }

  public String getFooterLine1()
  {
    return this.footerLine1;
  }

  public String getFooterLine2()
  {
    return this.footerLine2;
  }

  public String getPrimaryAuthDialAccess()
  {
    return this.primaryAuthDialAccess;
  }
  
  public String getSecondaryAuthDialAccess()
  {
    return this.secondaryAuthDialAccess;
  }

  public String getSurchargeInd()
  {
    return this.surchargeInd;
  }

  public String getSurchargeAmount()
  {
    return this.surchargeAmount;
  }

  public String getPrimaryKey1()
  {
    return this.primaryKey1;
  }
  
  public String getPrimaryKey2()
  {
    return this.primaryKey2;
  }

  public String getCardPresent()
  {
    return this.cardPresent;
  }
  public String getForceDial()
  {
    return this.forceDial;
  }
  public String getAutoTipPercent()
  {
    return this.autoTipPercent;    
  }
  public String getTipTimeSale()
  {
    return this.tipTimeSale;                        
  }
  public String getTipAssist()
  {
    return this.tipAssist;                              
  }
  public String getTipAssistPercent1()
  {
    return this.tipAssistPercent1;          
  }
  public String getTipAssistPercent2()
  {
    return this.tipAssistPercent2;          
  }
  public String getTipAssistPercent3()
  {
    return this.tipAssistPercent3;          
  }

  public String getInvNumKeys()
  {
    return this.invNumKeys;
  }
  public String getInvPrompt()
  {
    return this.invPrompt;
  }
  public String getDupTran()
  {
    return this.dupTran;
  }

  public String getPromptForClerk()
  {
    return this.promptForClerk;             
  }
  public String getFolioGuestCheckProcessing()
  {
    return this.folioGuestCheckProcessing; 
  }
  public String getFolioRoomPrompt()
  {
    return this.folioRoomPrompt;            
  }


  public String getSharingGroups()
  {
    return this.sharingGroups;
  }

  public String     getDebitSettleNum()
  {
    return debitSettleNum;
  }
  
  public String     getDownloadSpeed()
  {
    return downloadSpeed;
  }

  public String     getDialSpeed()
  {
    return dialSpeed;
  }

  public String     getTmOptions()
  {
    return tmOptions;
  }

  public String getTmOptPasswordProtect()
  {
    return (tmOptions.indexOf(TMOPT_PASSWORDPROTECT)>=0? TMOPT_PASSWORDPROTECT:"");
  }

  public String getTmOptRefund()
  {
    return (tmOptions.indexOf(TMOPT_REFUND)>=0? TMOPT_REFUND:"");
  }

  public String getTmOptSettle()
  {
    return (tmOptions.indexOf(TMOPT_SETTLE)>=0? TMOPT_SETTLE:"");
  }

  public String getTmOptAdjustment()
  {
    return (tmOptions.indexOf(TMOPT_ADJUSTMENT)>=0? TMOPT_ADJUSTMENT:"");
  }

  public String getTmOptVoid()
  {
    return (tmOptions.indexOf(TMOPT_VOID)>=0? TMOPT_VOID:"");
  }

  public String getTmOptOfflineTransactions()
  {
    return (tmOptions.indexOf(TMOPT_OFFLINETRANSACTIONS)>=0? TMOPT_OFFLINETRANSACTIONS:"");
  }

  public String getTmOptReports()
  {
    return (tmOptions.indexOf(TMOPT_REPORTS)>=0? TMOPT_REPORTS:"");
  }

  public String     getTimeDifference()
  {
    return timeDifference;
  }
  public String     getEciType()
  {
    return eciType;
  }
  public String     getClerkServerMode()
  {
    return clerkServerMode;
  }
  public String     getOveragePercentage()
  {
    return overagePercentage;
  }
  public String     getCheckReader()
  {
    return checkReader;
  }
  public String     getRiid()
  {
    return riid;
  }
  public String     getCheckHost()
  {
    return checkHost;
  }
  public String     getCheckHostFormat()
  {
    return checkHostFormat;
  }
  public String     getAmexOption()
  {
    return amexOption;
  }
  public String     getMerchantIdAmex()
  {
    return merchantIdAmex;
  }
  public String     getBaudRateHost1()
  {
    return baudRateHost1;
  }
  public String     getBaudRateAmexHost()
  {
    return baudRateAmexHost;
  }
  public String     getAmexPabx()
  {
    return amexPabx;
  }


//////////////////


  public void setIdlePrompt(String idlePrompt)
  {
    this.idlePrompt = idlePrompt;
  }

  public void setHeaderLine4(String headerLine4)
  {
    this.headerLine4 = headerLine4;
  }

  public void setHeaderLine5(String headerLine5)
  {
    this.headerLine5 = headerLine5;
  }

  public void setFooterLine1(String footerLine1)
  {
    this.footerLine1 = footerLine1;
  }

  public void setFooterLine2(String footerLine2)
  {
    this.footerLine2 = footerLine2;
  }

  public void setPrimaryAuthDialAccess(String primaryAuthDialAccess)
  {
    this.primaryAuthDialAccess = primaryAuthDialAccess;
  }
  
  public void setSecondaryAuthDialAccess(String secondaryAuthDialAccess)
  {
    this.secondaryAuthDialAccess = secondaryAuthDialAccess;
  }

  public void setSurchargeInd(String surchargeInd)
  {
    this.surchargeInd = surchargeInd;
  }

  public void setSurchargeAmount(String surchargeAmount)
  {
    this.surchargeAmount = surchargeAmount;
  }

  public void setPrimaryKey1(String primaryKey1)
  {
    this.primaryKey1 = primaryKey1;
  }
  
  public void setPrimaryKey2(String primaryKey2)
  {
    this.primaryKey2 = primaryKey2;
  }

  public void setPassProtect(String passProtect)
  {
    this.passProtect = passProtect;
  }

  public void setCheckTotalsInd(String checkTotalsInd)
  {
    this.checkTotalsInd = checkTotalsInd;
  }

  public void setForceSettleInd(String forceSettleInd)
  {
    this.forceSettleInd = forceSettleInd;
  }

  public void setSettleToPc(String settleToPc)
  {
    this.settleToPc = settleToPc;
  }

  public void setAtmCancelReturn(String atmCancelReturn)
  {
    this.atmCancelReturn = atmCancelReturn;
  }

  public void setPinpadTypeAvailable(String pinpadTypeAvailable)
  {
    this.pinpadTypeAvailable = pinpadTypeAvailable;
    decodePinpadTypeAvailable();
  }

  private void decodePinpadTypeAvailable()
  {
    try
    {
      if(isBlank(this.pinpadTypeAvailable))
      {
        return;
      }

      int idx = this.pinpadTypeAvailable.indexOf("*");

      if(idx >= 0)
      {
        this.pinpadAvailable  = this.pinpadTypeAvailable.substring(0,idx);
        this.pinpadType = this.pinpadTypeAvailable.substring(idx+1);
      }
    }
    catch(Exception e)
    {
    }
  }

  private void encodePinpadTypeAvailable()
  {
    this.pinpadTypeAvailable = this.pinpadAvailable + "*" + this.pinpadType;
  }


  public void setEbtTransType(String ebtTransType)
  {
    this.ebtTransType = ebtTransType;
  }

  public void setEbtFcsId(String ebtFcsId)
  {
    this.ebtFcsId = ebtFcsId;
  }

  public void setReminderMsgInd(String reminderMsgInd)
  {
    this.reminderMsgInd = reminderMsgInd;
  }

  public void setReminderMsgTime(String reminderMsgTime)
  {
    this.reminderMsgTime = reminderMsgTime;
  }

  public void setReminderMsg(String reminderMsg)
  {
    this.reminderMsg = reminderMsg;
  }

  public void setParentVnumber(String parentVnumber)
  {
    this.parentVnumber = parentVnumber;
  }

  public void setPrimaryDialAccess(String primaryDialAccess)
  {
    this.primaryDialAccess = primaryDialAccess;
  }

  public void setPrimaryDataCapturePhone(String primaryDataCapturePhone)
  {
    this.primaryDataCapturePhone = primaryDataCapturePhone;
  }

  public void setSecondaryDialAccess(String secondaryDialAccess)
  {
    this.secondaryDialAccess = secondaryDialAccess;
  }

  public void setSecondaryDataCapturePhone(String secondaryDataCapturePhone)
  {
    this.secondaryDataCapturePhone = secondaryDataCapturePhone;
  }

  public void setCashBackLimit(String cashBackLimit)
  {
    this.cashBackLimit = cashBackLimit;
  }

  public void setApplicationShown(String applicationShown)
  {
    this.applicationShown = applicationShown;
  }

  public void setManufacturer(String manufacturer)
  {
    this.manufacturer = manufacturer;
  }

  public void  setTermModel(String termModel)
  {
    this.termModel = termModel;
  }

  public void setPrinterTypeModel(String printerTypeModel)
  {
    this.printerTypeModel = printerTypeModel;
    decodePrinterTypeModel();
  }

  private void decodePrinterTypeModel()
  {
    try
    {
      if(isBlank(this.printerTypeModel))
      {
        return;
      }

      int idx = this.printerTypeModel.indexOf("*");

      if(idx >= 0)
      {
        this.printerType  = this.printerTypeModel.substring(0,idx);
        this.printerModel = this.printerTypeModel.substring(idx+1);
      }
    }
    catch(Exception e)
    {
    }
  }

  private void encodePrinterTypeModel()
  {
    this.printerTypeModel = this.printerType + "*" + this.printerModel;
  }

  public void setFraud(String fraud)
  {
    this.fraud = fraud;
  }

  public void setReceiptTrunc(String receiptTrunc)
  {
    this.receiptTrunc = receiptTrunc;
  }
  
  public void  setCheckPayment(String checkPayment)
  {
    this.checkPayment = checkPayment;
  }
  
  public void  setCheckMid(String checkMid)
  {
    this.checkMid = checkMid;
  }

  public void  setCheckVoice(String checkVoice)
  {
    this.checkVoice = checkVoice;
  }

  public void setCheckAuthPhone(String checkAuthPhone)
  {
    this.checkAuthPhone = checkAuthPhone;
  }

  public void setTipOption(String tipOption)
  {
    this.tipOption = tipOption;
  }

  public void setAvs(String avs)
  {
    this.avs = avs;
  }

  public void setPurchasingCard(String purchasingCard)
  {
    this.purchasingCard = purchasingCard;
  }
  
  public void setInvoiceNumber(String invoiceNumber)
  {
    this.invoiceNumber = invoiceNumber;
  }

  public void setRestaurantProcessing(String restaurantProcessing)
  {
    this.restaurantProcessing = restaurantProcessing;
  }
  public void setSrvclkProcessing(String srvclkProcessing)
  {
    this.srvclkProcessing = srvclkProcessing;
  }

  public void setAutoCloseInd(String autoCloseInd)
  {
    this.autoCloseInd = autoCloseInd;
  }

  public void setAutoCloseTime(String autoCloseTime)
  {
    this.autoCloseTime = autoCloseTime;
  }

  public void setAutoCloseTime2(String autoCloseTime2)
  {
    this.autoCloseTime2 = autoCloseTime2;
  }

  public void setAutoCloseReports(String autoCloseReports)
  {
    this.autoCloseReports = autoCloseReports;
  }

  public void setMerchantAba(String merchantAba)
  {
    this.merchantAba = merchantAba;
  }

  public void setMerchantSettlementAgent(String merchantSettlementAgent)
  {
    this.merchantSettlementAgent = merchantSettlementAgent;
  }

  public void setReimbursementAttribute(String reimbursementAttribute)
  {
    this.reimbursementAttribute = reimbursementAttribute;
  }

  public void setAtmSignLine(String atmSignLine)
  {
    this.atmSignLine = atmSignLine;
  }

  public void setCashBack(String cashBack)
  {
    this.cashBack = cashBack;
  }

  public void setCashBackTip(String cashBackTip)
  {
    this.cashBackTip = cashBackTip;
  }


  public void setCardPresent(String cardPresent)
  {
    this.cardPresent = cardPresent;
  }
  public void setForceDial(String forceDial)
  {
    this.forceDial = forceDial;
  }
  public void setAutoTipPercent(String autoTipPercent)
  {
    this.autoTipPercent = autoTipPercent;    
  }
  public void setTipTimeSale(String tipTimeSale)
  {
    this.tipTimeSale = tipTimeSale;
  }
  public void setTipAssist(String tipAssist)
  {
    this.tipAssist = tipAssist;  
  }
  public void setTipAssistPercent1(String tipAssistPercent1)
  {
    this.tipAssistPercent1 = tipAssistPercent1;          
  }
  public void setTipAssistPercent2(String tipAssistPercent2)
  {
    this.tipAssistPercent2 = tipAssistPercent2;          
  }
  public void setTipAssistPercent3(String tipAssistPercent3)
  {
    this.tipAssistPercent3 = tipAssistPercent3;          
  }

  public void setInvNumKeys(String invNumKeys)
  {
    this.invNumKeys = invNumKeys;
  }
  public void setInvPrompt(String invPrompt)
  {
    this.invPrompt = invPrompt;
  }
  public void setDupTran(String dupTran)
  {
    this.dupTran = dupTran;
  }

  public void setPromptForClerk(String promptForClerk)
  {
    this.promptForClerk = promptForClerk;             
  }
  public void setFolioGuestCheckProcessing(String folioGuestCheckProcessing)
  {
    this.folioGuestCheckProcessing = folioGuestCheckProcessing; 
  }
  public void setFolioRoomPrompt(String folioRoomPrompt)
  {
    this.folioRoomPrompt = folioRoomPrompt;            
  }


  public String getChecked(String code)
  {
    return _getChecked(sharingGroups,code);
  }

  public String getCheckedTmOptions(String code)
  {
    return _getChecked(tmOptions,code);
  }

  private String _getChecked(String dataMbr, String code)
  {
    String result = "";
    if(dataMbr.indexOf(code) > -1)
    {
      result = "checked";
    }
    return result;
  }

  public void setAlaskaOption(String alaskaOption)
  {
    sharingGroups += ALASKA_OPTION;  
  }

  public void setVisaCheckCard2(String visaCheckCard2)
  {
    sharingGroups += VISA_CHECK_CARD2;
  }  
 
  public void setShazam(String shazam)
  {
    sharingGroups += SHAZAM;
  }
  public void setMaestro(String maestro)
  {
    sharingGroups += MAESTRO;
  }

  public void setAccel(String accel)
  {
    sharingGroups += ACCEL;
  }
  public void setInterlink(String interlink)
  {
    sharingGroups += INTERLINK;
  }
  public void setTyme(String tyme)
  {
    sharingGroups += TYME;
  } 
  public void setEbtNetwork(String ebtNetwork)
  {
    sharingGroups += EBT_NETWORK;
  }

  public void setPulse(String pulse)
  {
    sharingGroups += PULSE;
  }

  public void setCashStation(String cashStation)
  {
    sharingGroups += CASH_STATION;
  }

  public void setStar(String star)
  {
    sharingGroups += STAR;
  }

  public void setMoneyStation(String moneyStation)
  {
    sharingGroups += MONEY_STATION;
  }

  public void setAffn(String affn)
  {
    sharingGroups += AFFN;
  }

  public void setVisa(String visa)
  {
    sharingGroups += VISA;
  } 

  public void setHonor(String honor)
  {
    sharingGroups += HONOR;
  }

  public void setInfYank24Nyce(String infYank24Nyce)
  {
    sharingGroups += INFYANK24NYCE;
  }

  public void setMac(String mac)
  {
    sharingGroups += MAC;
  } 

  public void setDebitSettleNum(String v)
  {
    debitSettleNum = v;
  }
  
  public void setDownloadSpeed(String v)
  {
    downloadSpeed = v;
  }

  public void setDialSpeed(String v)
  {
    dialSpeed = v;
  }

  public void setTmOptions(String v)
  {
    tmOptions = v;
  }

  public void setTmOptPasswordProtect(String v)
  {
    if(tmOptions.indexOf(TMOPT_PASSWORDPROTECT)<0)
        tmOptions += TMOPT_PASSWORDPROTECT;
  }

  public void setTmOptRefund(String v)
  {
    if(tmOptions.indexOf(TMOPT_REFUND)<0)
        tmOptions += TMOPT_REFUND;
  }

  public void setTmOptSettle(String v)
  {
    if(tmOptions.indexOf(TMOPT_SETTLE)<0)
        tmOptions += TMOPT_SETTLE;
  }

  public void setTmOptAdjustment(String v)
  {
    if(tmOptions.indexOf(TMOPT_ADJUSTMENT)<0)
        tmOptions += TMOPT_ADJUSTMENT;
  }

  public void setTmOptVoid(String v)
  {
    if(tmOptions.indexOf(TMOPT_VOID)<0)
        tmOptions += TMOPT_VOID;
  }

  public void setTmOptOfflineTransactions(String v)
  {
    if(tmOptions.indexOf(TMOPT_OFFLINETRANSACTIONS)<0)
        tmOptions += TMOPT_OFFLINETRANSACTIONS;
  }

  public void setTmOptReports(String v)
  {
    if(tmOptions.indexOf(TMOPT_REPORTS)<0)
        tmOptions += TMOPT_REPORTS;
  }

////////////////

  public void setTimeDifference(String v)
  {
    timeDifference = v;
  }
  public void setEciType(String v)
  {
    eciType = v;
  }
  public void setClerkServerMode(String v)
  {
    clerkServerMode = v;
  }
  public void setOveragePercentage(String v)
  {
    overagePercentage = v;
  }
  public void setCheckReader(String v)
  {
    checkReader = v;
  }
  public void setRiid(String v)
  {
    riid = v;
  }
  public void setCheckHost(String v)
  {
    checkHost = v;
  }
  public void setCheckHostFormat(String v)
  {
    checkHostFormat = v;
  }
  public void setAmexOption(String v)
  {
    amexOption = v;
  }
  public void setMerchantIdAmex(String v)
  {
    merchantIdAmex = v;
  }
  public void setBaudRateHost1(String v)
  {
    baudRateHost1 = v;
  }
  public void setBaudRateAmexHost(String v)
  {
    baudRateAmexHost = v;
  }
  public void setAmexPabx(String v)
  {
    amexPabx = v;
  }

//////////////

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }

  public boolean isNumber(String test)
  {
    boolean pass = false;
    
    try
    {
      long i = Long.parseLong(test);
      pass = true;
    }
    catch(Exception e)
    {
    }
    
    return pass;
  }

  public boolean isValidSurcharge(String test)
  {
    boolean pass = false;
    
    try
    {
      float i = Float.parseFloat(test);

      if(i>=0.0 && i<10.0)
      {
        pass = true;
      }
    }
    catch(Exception e)
    {
    }
    
    return pass;
  }

  public String getDigits(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");
    String        result      = "";

    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = digits.toString();
    }
    catch(Exception e)
    {
      if(raw != null && !raw.equals(""))
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDigits('" + raw + "'): " + e.toString());
      }
    }

    return result;

  }


}/*@lineinfo:generated-code*/