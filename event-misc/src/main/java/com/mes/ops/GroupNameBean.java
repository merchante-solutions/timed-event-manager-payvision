/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/GroupNameBean.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/15/01 3:58p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.ops;


import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class GroupNameBean extends com.mes.screens.SequenceDataBean
{
  // private data members
  private int           bankNumber          = 0;
  private int           groupNumber         = 0;
  private long          hierarchyNode       = 0L;
  private String        hierarchyName       = "";
  private String        groupName           = "";
  private int           searchCriteria      = -1;
  private boolean       submit              = false;
  
  /*
  ** CONSTRUCTOR
  */
  public GroupNameBean()
  {
  }

  public void setBankNumber(String bankNumber)
  {
    try
    {
      this.bankNumber = Integer.parseInt(bankNumber);
    }
    catch(Exception e)
    {
    }
  }
  public String getBankNumber()
  {
    String retVal = "";
    
    if(this.bankNumber > 0)
    {
      retVal = Integer.toString(this.bankNumber);
    }
    return retVal;
  }
  
  public void setGroupName(String groupName)
  {
    this.groupName = groupName;
  }
  public String getGroupName()
  {
    return this.groupName;
  }
  
  public void setGroupNumber(String groupNumber)
  {
    try
    {
      this.groupNumber = Integer.parseInt(groupNumber);
    }
    catch(Exception e)
    {
    }
  }
  public String getGroupNumber()
  {
    String retVal = "";
    
    if(this.groupNumber > 0)
    {
      retVal = Integer.toString(this.groupNumber);
    }
    return retVal;
  }
  
  public void setHierarchyNode(String hierarchyNode)
  {
    try
    {
      this.hierarchyNode = Long.parseLong(hierarchyNode);
    }
    catch(Exception e)
    {
    }
  }
  public String getHierarchyNode()
  {
    String retVal = "";
    
    if(this.hierarchyNode > 0L)
    {
      retVal = Long.toString(this.hierarchyNode);
    }
    return retVal;
  }
  
  public void setHierarchyName(String hierarchyName)
  {
    this.hierarchyName = hierarchyName;
    
    if(this.hierarchyName == null)
    {
      this.hierarchyName = "";
    }
  }
  public String getHierarchyName()
  {
    return this.hierarchyName;
  }
  
  /*
  ** METHOD getNameData
  **
  ** Retrieves current group name data from the database
  */
  public void getNameData()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      if(hierarchyNode > 0L)
      {
        qs.append("select group_name ");
        qs.append("from   group_names ");
        qs.append("where  group_number = ?");
        ps = getPreparedStatement(qs.toString());
        ps.setLong(1, this.hierarchyNode);
        rs = ps.executeQuery();
        
        if(rs.next())
        {
          this.hierarchyName = rs.getString("group_name");
          if(this.hierarchyName == null)
          {
            this.hierarchyName = "";
          }
        }
        
        rs.close();
        ps.close();
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getNameData: " + e.toString());
      addError("getNameData: " + e.toString());
    }
  }
  
  /*
  ** METHOD submitNameData
  **
  ** Updates exising name data in the group_names table
  */
  public void submitNameData()
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    boolean           update  = false;
    
    try
    {
      if(this.hierarchyNode > 0L)
      {
        qs.append("select group_name ");
        qs.append("from   group_names ");
        qs.append("where  group_number = ?");
        
        ps = getPreparedStatement(qs.toString());
        ps.setLong(1, hierarchyNode);
        
        rs = ps.executeQuery();
        
        if(rs.next())
        {
          update = true;
        }
        
        rs.close();
        ps.close();
        qs.setLength(0);
        
        if(update)
        {
          qs.append("update   group_names ");
          qs.append("set      group_name = ? ");
          qs.append("where    group_number = ?");
        }
        else
        {
          qs.append("insert into group_names (");
          qs.append("            group_name, ");
          qs.append("            group_number) ");
          qs.append("values     (?, ?)");
        }
        
        ps = getPreparedStatement(qs.toString());
        ps.setString(1, this.hierarchyName);
        ps.setLong(2, this.hierarchyNode);
        
        ps.executeUpdate();
      }
      else
      {
        addError("Please enter a valid 10-digit hierarchy node");
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitNameData: " + e.toString());
      addError("submitNameData: " + e.toString());
    }
  }
    
  /*
  ** METHOD getResultSet
  **
  ** Performs the search query and returns the result set
  */
  public ResultSet getResultSet()
  {
    PreparedStatement ps      = null;
    StringBuffer      qs      = new StringBuffer("");
    ResultSet         rs      = null;
    
    try
    {
      // build base query
      qs.setLength(0);
      qs.append("select   group_number, ");
      qs.append("         group_name ");
      qs.append("from     group_names ");

      // add conditional query elements
      switch(this.searchCriteria)
      {
        case 1:
          qs.append("where trunc(group_number / 1000000) = ? ");
          qs.append("order by group_number");
          ps = getPreparedStatement(qs.toString());
          ps.setInt(1, this.bankNumber);
          break;
          
        case 2:
          qs.append("where mod(group_number, 1000000) = ? ");
          qs.append("order by group_number");
          ps = getPreparedStatement(qs.toString());
          ps.setInt(1, this.groupNumber);
          break;
          
        case 3:
          qs.append("where group_number = ? ");
          qs.append("order by group_number");
          ps = getPreparedStatement(qs.toString());
          ps.setLong(1, this.hierarchyNode);
          break;
          
        case 4:
          qs.append("where upper(group_name) like ? escape \'\\\' ");
          qs.append("order by group_number");
          ps = getPreparedStatement(qs.toString());
          ps.setString(1, "%" + this.groupName.toUpperCase() + "%");
          break;
          
        default:
          qs.append("order by group_number");
          ps = getPreparedStatement(qs.toString());
          break;
      }
      
      rs = ps.executeQuery();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getResultSet: " + e.toString());
      addError("getData: " + e.toString());
    }
    
    return rs;
  }  

  /*
  ** METHOD validateData
  **
  ** Checks to make sure that search criteria are valid
  */  
  public boolean validateData()
  {
    boolean result = true;
    
    switch(this.searchCriteria)
    {
      case 1:
        if(bankNumber < 1000 || bankNumber > 9999)
        {
          addError("Please enter a valid 4-digit bank number");
        }
        break;
      case 2:
        if(groupNumber < 100000 || groupNumber > 999999)
        {
          addError("Please enter a valid 6-digit group or association number");
        }
        break;
      case 3:
        if(hierarchyNode < 1000000000L || groupNumber > 9999999999L)
        {
          addError("Please enter a valid 10-digit hierarchy node");
        }
        break;

      default:
        break;
    }
    
    result = !hasErrors();
    
    return result;
  }
  
  public void setSearchCriteria(String searchCriteria)
  {
    try
    {
      this.searchCriteria = Integer.parseInt(searchCriteria); 
    }
    catch(Exception e)
    {
      this.searchCriteria = -1;
    }
  }
  public int getSearchCriteria()
  {
    return this.searchCriteria;
  }

  public void setSubmit(String submit)
  {
    this.submit = true;
  }
  public boolean isSubmitted()
  {
    return this.submit;
  }

  public boolean getHasErrors()
  {
    return hasErrors();
  }
}
