/*@lineinfo:filename=ActivationEmployeeReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ActivationEmployeeReport.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.io.Serializable;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class ActivationEmployeeReport extends DateSQLJBean
  implements Serializable
{
  public final static int NUM_SCHEDULED           = 0;
  public final static int NUM_COMPLETED           = 1;
  public final static int NUM_TRAINING_COMPLETE   = 2;
  public final static int NUM_REFERRED_TO_REP     = 3;
  public final static int NUM_CUSTOMER_CANCELLED  = 4;

  public final static int NUM_NOT_COMPLETED       = 5;
  public final static int NUM_FOLLOW_UP_REQ       = 6;
  public final static int NUM_RESCHEDULING_REQ    = 7;
  public final static int NUM_NO_CONTACT          = 8;
  public final static int NUM_MISSED_CALLS        = 9;  
  public final static int NUM_INITIAIL_SCHEDULING = 10;

  private Date fromSqlDate  = null;
  private Date toSqlDate    = null; 
  private long fromLongDate = -1L;
  private long toLongDate   = -1L; 

  private HashMap employeeStatHash  = new HashMap();
  private Vector  employeeList      = new Vector();

  public ActivationEmployeeReport()
  {
    //initializes calendar to be current date/time with default schedule type (activation)
    super();
  }

  private void setDates()
  {
    //set the from date and too date.. 
    setFromSqlDate(getSqlFromDate());
    setToSqlDate(getSqlToDate());
    setFromLongDate(getFromDate().getTime());
    setToLongDate(getToDate().getTime());
  }

  //gets data to build report
  public void getData()
  {
    ResultSetIterator   it         = null;
    ResultSet           rs         = null;

    //clear hashmap and vector.. get fresh data
    employeeStatHash  = new HashMap();
    employeeList      = new Vector();

    setDates();

    try
    {
      connect();
     
      //number scheduled
      /*@lineinfo:generated-code*//*@lineinfo:109^7*/

//  ************************************************************
//  #sql [Ctx] it = { select user_id, count(id) data_value 
//          from  schedule_data
//          where type = 1 and id between :fromLongDate and :toLongDate and
//                function_id != :ScheduleIntervalBean.BLOCK_OUT_FUNCTION_ID
//          group by user_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select user_id, count(id) data_value \n        from  schedule_data\n        where type = 1 and id between  :1  and  :2  and\n              function_id !=  :3 \n        group by user_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.ActivationEmployeeReport",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,fromLongDate);
   __sJT_st.setLong(2,toLongDate);
   __sJT_st.setString(3,ScheduleIntervalBean.BLOCK_OUT_FUNCTION_ID);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.ActivationEmployeeReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:116^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {
        getRecordData(rs, NUM_SCHEDULED);
      }
      
      rs.close();
      it.close();

      //number completed
      /*@lineinfo:generated-code*//*@lineinfo:129^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    login_name user_id,
//                    count(sequence) data_value
//          from      SERVICE_CALL_ACTIVATION
//          where     trunc(CALL_DATE) between :fromSqlDate and :toSqlDate and
//                    activation_status = :mesConstants.TRAINING_CALL_COMPLETED
//          group by  login_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    login_name user_id,\n                  count(sequence) data_value\n        from      SERVICE_CALL_ACTIVATION\n        where     trunc(CALL_DATE) between  :1  and  :2  and\n                  activation_status =  :3 \n        group by  login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.ActivationEmployeeReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   __sJT_st.setString(3,mesConstants.TRAINING_CALL_COMPLETED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.ActivationEmployeeReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:137^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs, NUM_COMPLETED);
      }
      
      rs.close();
      it.close();

      //number not completed
      /*@lineinfo:generated-code*//*@lineinfo:150^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    login_name user_id,
//                    count(sequence) data_value
//          from      SERVICE_CALL_ACTIVATION
//          where     trunc(CALL_DATE) between :fromSqlDate and :toSqlDate and
//                    activation_status_type = :mesConstants.CALL_STATUS_OPEN
//          group by  login_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    login_name user_id,\n                  count(sequence) data_value\n        from      SERVICE_CALL_ACTIVATION\n        where     trunc(CALL_DATE) between  :1  and  :2  and\n                  activation_status_type =  :3 \n        group by  login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.ActivationEmployeeReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   __sJT_st.setInt(3,mesConstants.CALL_STATUS_OPEN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.ActivationEmployeeReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:158^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs, NUM_NOT_COMPLETED);
      }
      
      rs.close();
      it.close();

      //number training completed
      /*@lineinfo:generated-code*//*@lineinfo:171^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    login_name user_id,
//                    count(sequence) data_value
//          from      SERVICE_CALL_ACTIVATION
//          where     trunc(CALL_DATE) between :fromSqlDate and :toSqlDate and
//                    activation_status = :mesConstants.TRAINING_CALL_COMPLETED
//          group by  login_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    login_name user_id,\n                  count(sequence) data_value\n        from      SERVICE_CALL_ACTIVATION\n        where     trunc(CALL_DATE) between  :1  and  :2  and\n                  activation_status =  :3 \n        group by  login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.ActivationEmployeeReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   __sJT_st.setString(3,mesConstants.TRAINING_CALL_COMPLETED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.ActivationEmployeeReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:179^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs, NUM_TRAINING_COMPLETE);
      }
      
      rs.close();
      it.close();

      //number referred to rep
      /*@lineinfo:generated-code*//*@lineinfo:192^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    login_name user_id,
//                    count(sequence) data_value
//          from      SERVICE_CALL_ACTIVATION
//          where     trunc(CALL_DATE) between :fromSqlDate and :toSqlDate and
//                    activation_status in
//                    (
//                      :mesConstants.REFER_TO_SALES_REP,
//                      :mesConstants.SCHEDULING_CALL_REFER_TO_REP
//                    )
//          group by  login_name
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    login_name user_id,\n                  count(sequence) data_value\n        from      SERVICE_CALL_ACTIVATION\n        where     trunc(CALL_DATE) between  :1  and  :2  and\n                  activation_status in\n                  (\n                     :3 ,\n                     :4 \n                  )\n        group by  login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.ActivationEmployeeReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   __sJT_st.setString(3,mesConstants.REFER_TO_SALES_REP);
   __sJT_st.setString(4,mesConstants.SCHEDULING_CALL_REFER_TO_REP);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.ActivationEmployeeReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:205^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs, NUM_REFERRED_TO_REP);
      }
      
      rs.close();
      it.close();

      //number customer cancelled
      /*@lineinfo:generated-code*//*@lineinfo:218^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    login_name user_id,
//                    count(sequence) data_value
//          from      SERVICE_CALL_ACTIVATION
//          where     trunc(CALL_DATE) between :fromSqlDate and :toSqlDate and
//                    activation_status = :mesConstants.CUSTOMER_CANCELLED_ACCOUNT
//          group by  login_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    login_name user_id,\n                  count(sequence) data_value\n        from      SERVICE_CALL_ACTIVATION\n        where     trunc(CALL_DATE) between  :1  and  :2  and\n                  activation_status =  :3 \n        group by  login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.ActivationEmployeeReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   __sJT_st.setString(3,mesConstants.CUSTOMER_CANCELLED_ACCOUNT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.ActivationEmployeeReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs, NUM_CUSTOMER_CANCELLED);
      }
      
      rs.close();
      it.close();

      //number follow up required
      /*@lineinfo:generated-code*//*@lineinfo:239^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    login_name user_id,
//                    count(sequence) data_value
//          from      SERVICE_CALL_ACTIVATION
//          where     trunc(CALL_DATE) between :fromSqlDate and :toSqlDate and
//                    activation_status = :mesConstants.FOLLOW_UP_REQUIRED
//          group by  login_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    login_name user_id,\n                  count(sequence) data_value\n        from      SERVICE_CALL_ACTIVATION\n        where     trunc(CALL_DATE) between  :1  and  :2  and\n                  activation_status =  :3 \n        group by  login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.ActivationEmployeeReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   __sJT_st.setString(3,mesConstants.FOLLOW_UP_REQUIRED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.ActivationEmployeeReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:247^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs, NUM_FOLLOW_UP_REQ);
      }
      
      rs.close();
      it.close();

      //number rescheduling required
      /*@lineinfo:generated-code*//*@lineinfo:260^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    login_name user_id,
//                    count(sequence) data_value
//          from      SERVICE_CALL_ACTIVATION
//          where     trunc(CALL_DATE) between :fromSqlDate and :toSqlDate and
//                    activation_status = :mesConstants.RESCHEDULING_REQUESTED
//          group by  login_name
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    login_name user_id,\n                  count(sequence) data_value\n        from      SERVICE_CALL_ACTIVATION\n        where     trunc(CALL_DATE) between  :1  and  :2  and\n                  activation_status =  :3 \n        group by  login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.ActivationEmployeeReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   __sJT_st.setString(3,mesConstants.RESCHEDULING_REQUESTED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.ActivationEmployeeReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:269^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs, NUM_RESCHEDULING_REQ);
      }
      
      rs.close();
      it.close();

      //number initial scheduling
      /*@lineinfo:generated-code*//*@lineinfo:282^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    login_name user_id,
//                    count(sequence) data_value
//          from      SERVICE_CALL_ACTIVATION
//          where     trunc(CALL_DATE) between :fromSqlDate and :toSqlDate and
//                    activation_status = :mesConstants.INITIAL_SCHEDULING
//          group by  login_name
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    login_name user_id,\n                  count(sequence) data_value\n        from      SERVICE_CALL_ACTIVATION\n        where     trunc(CALL_DATE) between  :1  and  :2  and\n                  activation_status =  :3 \n        group by  login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.ActivationEmployeeReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   __sJT_st.setString(3,mesConstants.INITIAL_SCHEDULING);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.ActivationEmployeeReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:291^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs, NUM_INITIAIL_SCHEDULING);
      }
      
      rs.close();
      it.close();

      //number no contact
      /*@lineinfo:generated-code*//*@lineinfo:304^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    login_name user_id,
//                    count(sequence) data_value
//          from      SERVICE_CALL_ACTIVATION
//          where     trunc(CALL_DATE) between :fromSqlDate and :toSqlDate and
//                    activation_status = :mesConstants.NO_CONTACT_AT_SCHEDULED_TIME
//          group by  login_name
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    login_name user_id,\n                  count(sequence) data_value\n        from      SERVICE_CALL_ACTIVATION\n        where     trunc(CALL_DATE) between  :1  and  :2  and\n                  activation_status =  :3 \n        group by  login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.ActivationEmployeeReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   __sJT_st.setString(3,mesConstants.NO_CONTACT_AT_SCHEDULED_TIME);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.ops.ActivationEmployeeReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:313^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs, NUM_NO_CONTACT);
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }
      
  private void getRecordData(ResultSet rs, int dataDesc) throws Exception
  {
    
    String employee = rs.getString("user_id");

    //adds employee to vector containing list of employees (each entry unique)
    addEmployee(employee);
  
    EmployeeStats stats = null;

    //gets rec for employee.. gets old one if already exists or starts new one if it doesnt exist
    stats = getRecFromHash(employee);

    switch(dataDesc)
    {
      case NUM_SCHEDULED:
        stats.setNumScheduled(rs.getInt("data_value"));
      break;
     
      case NUM_COMPLETED:
        stats.setNumCompleted(rs.getInt("data_value"));
      break;

      case NUM_TRAINING_COMPLETE:
        stats.setNumTrainingComplete(rs.getInt("data_value"));
      break;

      case NUM_REFERRED_TO_REP:
        stats.setNumReferredToRep(rs.getInt("data_value"));
      break;

      case NUM_CUSTOMER_CANCELLED:
        stats.setNumCustomerCancelled(rs.getInt("data_value"));
      break;

      case NUM_NOT_COMPLETED:
        stats.setNumNotCompleted(rs.getInt("data_value"));
      break;

      case NUM_FOLLOW_UP_REQ:
        stats.setNumFollowUpReq(rs.getInt("data_value"));
      break;

      case NUM_RESCHEDULING_REQ:
        stats.setNumReschedulingReq(rs.getInt("data_value"));
      break;

      case NUM_INITIAIL_SCHEDULING:
        stats.setNumInitialScheduling(rs.getInt("data_value"));
      break;

      case NUM_NO_CONTACT:
        stats.setNumNoContact(rs.getInt("data_value"));
      break;

      case NUM_MISSED_CALLS:
        stats.setNumMissedCalls(rs.getInt("data_value"));
      break;
    }

    putRecInHash(stats, employee);
    
    stats = null;
  }

  private void putRecInHash(EmployeeStats stats, String employee)
  {
    employeeStatHash.put(employee, stats);
  }

  public EmployeeStats getRecFromHash(String employee)
  {
    if(employeeStatHash.containsKey(employee))
    {
      return ((EmployeeStats)employeeStatHash.get(employee));
    }
    else
    {
      return (new EmployeeStats(employee));
    }
  }


  private void addEmployee(String employee)
  {
    if(!employeeList.contains(employee))
    {
      employeeList.add(employee);
    }
  }      
      
  public Vector getEmployeeListVector()
  {
    return this.employeeList;
  }
 
                            
  public void setFromSqlDate(Date  fromDate)
  {
    this.fromSqlDate = fromDate;
  }

  public void setToSqlDate(Date toDate)
  {
    this.toSqlDate = toDate;
  }

  public void setFromLongDate(long fromDate)
  {
    this.fromLongDate = fromDate;
  }
  public void setToLongDate(long toDate)
  {
    this.toLongDate = toDate;
  } 

  public class EmployeeStats
  {
    private String employeeName           = "";
    private int    numScheduled           = 0;

    private int    numCompleted           = 0;
    private int    numTrainingComplete    = 0;
    private int    numReferredToRep       = 0;
    private int    numCustomerCancelled   = 0;

    private int    numNotCompleted        = 0;
    private int    numFollowUpReq         = 0;
    private int    numInitialScheduling   = 0;
    private int    numReschedulingReq     = 0;
    private int    numNoContact           = 0;

    private int    numMissedCalls         = 0;  
  
    public EmployeeStats(String employeeName)
    {
      this.employeeName = employeeName;
    }

    public String getEmployeeName()
    {
      return this.employeeName;
    }

    public int getNumScheduled()
    {
      return this.numScheduled;
    }

    public int getNumCompleted()
    {
      return this.numCompleted;
    }

    public int getNumTrainingComplete()
    {
      return this.numTrainingComplete;
    }

    public int getNumReferredToRep()
    {
      return this.numReferredToRep;
    }

    public int getNumCustomerCancelled()
    {
      return this.numCustomerCancelled;
    }

    public int getNumNotCompleted()
    {
      return this.numNotCompleted;
    }

    public int getNumFollowUpReq()
    {
      return this.numFollowUpReq;
    }
    public int getNumReschedulingReq()
    {
      return this.numReschedulingReq;
    }

    public int getNumInitialScheduling()
    {
      return this.numInitialScheduling;
    }

    public int getNumNoContact()
    {
      return this.numNoContact;
    }
    public int getNumMissedCalls()
    {
      return this.numMissedCalls;
    }
 
    //setters
    public void setEmployeeName(String employeeName)
    {
      this.employeeName = employeeName;
    }

    public void setNumScheduled(int numScheduled)
    {
      this.numScheduled = numScheduled;
    }

    public void setNumCompleted(int numCompleted)
    {
      this.numCompleted = numCompleted;
    }

    public void setNumTrainingComplete(int numTrainingCompleted)
    {
      this.numTrainingComplete = numTrainingCompleted;
    }

    public void setNumReferredToRep(int numReferredToRep)
    {
      this.numReferredToRep = numReferredToRep;
    }

    public void setNumCustomerCancelled(int numCustomerCancelled)
    {
      this.numCustomerCancelled = numCustomerCancelled;
    }

    public void setNumNotCompleted(int numNotCompleted)
    {
      this.numNotCompleted = numNotCompleted;
    }

    public void setNumFollowUpReq(int numFollowUpReq)
    {
      this.numFollowUpReq = numFollowUpReq;
    }
    public void setNumReschedulingReq(int numReschedulingReq)
    {
      this.numReschedulingReq = numReschedulingReq;
    }

    public void setNumInitialScheduling(int numInitialScheduling)
    {
      this.numInitialScheduling = numInitialScheduling;
    }

    public void setNumNoContact(int numNoContact)
    {
      this.numNoContact = numNoContact;
    }
    public void setNumMissedCalls(int numMissedCalls)
    {
      this.numMissedCalls = numMissedCalls;
    }

 
 
 
  }

}/*@lineinfo:generated-code*/