/*@lineinfo:filename=ActivationSalesChannelReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ActivationSalesChannelReport.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/16/03 11:31a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.io.Serializable;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class ActivationSalesChannelReport extends DateSQLJBean
  implements Serializable
{
  public final static int NUM_COMPLETED           = 0;
  public final static int NUM_REFERRED_TO_REP     = 1;
  public final static int NUM_PENDING             = 2;
  public final static int NUM_INITIAIL_SCHEDULING = 3;

  private Date fromSqlDate  = null;
  private Date toSqlDate    = null; 
  private long fromLongDate = -1L;
  private long toLongDate   = -1L; 

  private HashMap salesChannelStatHash  = new HashMap();
  private Vector  salesChannelList      = new Vector();

  public ActivationSalesChannelReport()
  {
    //initializes calendar to be current date/time with default schedule type (activation)
    super();
  }

  private void setDates()
  {
    //set the from date and too date.. 
    setFromSqlDate  ( getSqlFromDate()        );
    setToSqlDate    ( getSqlToDate()          );
    setFromLongDate ( getFromDate().getTime() );
    setToLongDate   ( getToDate().getTime()   );
  }

  //gets data to build report
  public void getData()
  {
    ResultSetIterator   it         = null;
    ResultSet           rs         = null;

    //clear hashmap and vector.. get fresh data
    salesChannelStatHash  = new HashMap();
    salesChannelList      = new Vector();

    setDates();

    try
    {
      connect();
     
      //number completed
      /*@lineinfo:generated-code*//*@lineinfo:101^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    sca.*, 
//                    decode(app.app_type, null, -1, app.app_type) app_type,
//                    decode(app.appsrctype_code, null, 'unknwn', app.appsrctype_code) sales_channel
//          from      SERVICE_CALL_ACTIVATION sca,
//                    merchant merch,
//                    application app
//          where     trunc(CALL_DATE) between :fromSqlDate and :toSqlDate
//                    and  sca.merchant_number = merch.merch_number(+) 
//                    and  merch.app_seq_num   = app.app_seq_num(+)
//          order by  app.appsrctype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sca.*, \n                  decode(app.app_type, null, -1, app.app_type) app_type,\n                  decode(app.appsrctype_code, null, 'unknwn', app.appsrctype_code) sales_channel\n        from      SERVICE_CALL_ACTIVATION sca,\n                  merchant merch,\n                  application app\n        where     trunc(CALL_DATE) between  :1  and  :2 \n                  and  sca.merchant_number = merch.merch_number(+) \n                  and  merch.app_seq_num   = app.app_seq_num(+)\n        order by  app.appsrctype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.ActivationSalesChannelReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.ActivationSalesChannelReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs);
      }
      
      rs.close();
      it.close();

      //number scheduled
      /*@lineinfo:generated-code*//*@lineinfo:126^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   decode(app.APPSRCTYPE_CODE, null, 'unknwn', app.APPSRCTYPE_CODE) sales_channel,
//                   decode(app.APP_TYPE, null, -1, app.APP_TYPE) app_type, 
//                   count(sd.id) num_scheduled 
//          from     schedule_data sd,
//                   merchant merch,
//                   application app
//          where    sd.type = 1 and sd.id between :fromLongDate and :toLongDate 
//                   and sd.function_id != :ScheduleIntervalBean.BLOCK_OUT_FUNCTION_ID 
//                   and sd.FUNCTION_ID = merch.merch_number(+)
//                   and merch.app_seq_num = app.app_seq_num(+)
//          group by app.appsrctype_code,
//                   app.app_type
//          order by app.appsrctype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   decode(app.APPSRCTYPE_CODE, null, 'unknwn', app.APPSRCTYPE_CODE) sales_channel,\n                 decode(app.APP_TYPE, null, -1, app.APP_TYPE) app_type, \n                 count(sd.id) num_scheduled \n        from     schedule_data sd,\n                 merchant merch,\n                 application app\n        where    sd.type = 1 and sd.id between  :1  and  :2  \n                 and sd.function_id !=  :3  \n                 and sd.FUNCTION_ID = merch.merch_number(+)\n                 and merch.app_seq_num = app.app_seq_num(+)\n        group by app.appsrctype_code,\n                 app.app_type\n        order by app.appsrctype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.ActivationSalesChannelReport",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,fromLongDate);
   __sJT_st.setLong(2,toLongDate);
   __sJT_st.setString(3,ScheduleIntervalBean.BLOCK_OUT_FUNCTION_ID);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.ActivationSalesChannelReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^7*/
      
      rs = it.getResultSet();

      while(rs.next())
      {
        getRecordData(rs);
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
      
  private void getRecordData(ResultSet rs) throws Exception
  {
    String salesChannel = rs.getString("sales_channel");
    int    appType      = rs.getInt("app_type");

    //adds sales channel to vector containing list of sales channels (each entry unique)
    addSalesChannel(salesChannel);
  
    SalesChannelStats stats = null;

    //gets rec for sales channel.. gets old one if already exists or starts new one if it doesnt exist
    stats = getRecFromHash(salesChannel, appType);

    String status = "";
    
    try
    {
      status = rs.getString("activation_status");
    }
    catch(Exception e)
    {
      status = "NUM_SCHEDULED";
    }

    if(status.equals(mesConstants.TRAINING_CALL_COMPLETED))
    {
      stats.incrementNumCompleted();
    }
    else if(status.equals(mesConstants.SCHEDULING_CALL_REFER_TO_REP))
    {
      stats.incrementNumReferredToRep();
    }
    else if(status.equals(mesConstants.SCHEDULING_CALL_LEFT_MESSAGE)            || 
            status.equals(mesConstants.SCHEDULING_CALL_TOLD_TO_CALL_BACK)       || 
            status.equals(mesConstants.TRAINING_CALL_NOT_AVAILABLE_RESCHEDULE)  ||
            status.equals(mesConstants.TRAINING_CALL_MAINTENANCE_REQUIRED)      ||
            status.equals(mesConstants.TRAINING_CALL_RESCHEDULE))
    {
      stats.incrementNumPending();
    }
    else if(status.equals(mesConstants.SCHEDULING_CALL_SCHEDULED))
    {
      stats.incrementNumInitialScheduling();
    }
    else if(status.equals("NUM_SCHEDULED"))
    {
      stats.setNumScheduled(rs.getInt("num_scheduled"));
    }

    putRecInHash(stats, salesChannel);
    
    stats = null;
  }

  private void putRecInHash(SalesChannelStats stats, String salesChannel)
  {
    salesChannelStatHash.put(salesChannel, stats);
  }

  public SalesChannelStats getRecFromHash(String salesChannel, int appType)
  {
    if(salesChannelStatHash.containsKey(salesChannel))
    {
      return ((SalesChannelStats)salesChannelStatHash.get(salesChannel));
    }
    else
    {
      return (new SalesChannelStats(salesChannel, appType));
    }
  }

  public SalesChannelStats getRecFromHash(String salesChannel)
  {
      return getRecFromHash(salesChannel, -1);
  }

  private void addSalesChannel(String salesChannel)
  {
    if(!salesChannelList.contains(salesChannel))
    {
      salesChannelList.add(salesChannel);
    }
  }      
      
  public Vector getSalesChannelListVector()
  {
    return this.salesChannelList;
  }
 
  public void setFromSqlDate(Date  fromDate)
  {
    this.fromSqlDate = fromDate;
  }

  public void setToSqlDate(Date toDate)
  {
    this.toSqlDate = toDate;
  }

  public void setFromLongDate(long fromDate)
  {
    this.fromLongDate = fromDate;
  }
  public void setToLongDate(long toDate)
  {
    this.toLongDate = toDate;
  } 

  public class SalesChannelStats
  {
    private String salesChannelName       = "";
    private int    appType                = -1;

    private int    numScheduled           = 0;
    private int    numCompleted           = 0;
    private int    numReferredToRep       = 0;
    private int    numPending             = 0;
    private int    numInitialScheduling   = 0;
  
    public SalesChannelStats(String salesChannelName, int appType)
    {
      this.salesChannelName = salesChannelName;
      this.appType          = appType;
    }

    public String getSalesChannelName()
    {
      return this.salesChannelName;
    }

    public int getAppType()
    {
      return this.appType;
    }

    public int getNumCompleted()
    {
      return this.numCompleted;
    }

    public int getNumReferredToRep()
    {
      return this.numReferredToRep;
    }

    public int getNumPending()
    {
      return this.numPending;
    }

    public int getNumInitialScheduling()
    {
      return this.numInitialScheduling;
    }

    public int getNumScheduled()
    {
      return this.numScheduled;
    }

    //setters
    public void setSalesChannelName(String salesChannelName)
    {
      this.salesChannelName = salesChannelName;
    }

    public void setAppType(int appType)
    {
      this.appType = appType;
    }

    public void incrementNumCompleted()
    {
      this.numCompleted++;
    }

    public void incrementNumReferredToRep()
    {
      this.numReferredToRep++;
    }

    public void incrementNumPending()
    {
      this.numPending++;
    }

    public void incrementNumInitialScheduling()
    {
      this.numInitialScheduling++;
    }

    public void setNumScheduled(int numScheduled)
    {
      this.numScheduled = numScheduled;
    }
  }
}/*@lineinfo:generated-code*/