/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ReportMenuItem.java $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/16/01 5:12p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import com.mes.constants.mesConstants;

public class ReportMenuItem
{
  protected String        BaseUrl;
  protected String        Description;
  protected String        DisplayName;
  protected long          RequiredUserType;
  protected String        target;
  
  public ReportMenuItem( String displayName, String baseUrl )
  {
    init( displayName, "", baseUrl, mesConstants.USER_NONE, "" );
  }
  
  public ReportMenuItem( String displayName, String desc, String baseUrl )
  {
    init( displayName, desc, baseUrl, mesConstants.USER_NONE, "" );
  }
  
  public ReportMenuItem( String displayName, String desc, String baseUrl, long requiredUserType )
  {
    init( displayName, desc, baseUrl, requiredUserType, "" );
  }
  
  public ReportMenuItem( String displayName, String desc, String baseUrl, long requiredUserType, String target)
  {
    init( displayName, desc, baseUrl, requiredUserType, target );
  }
  
  public String getBaseUrl( )
  {
    return( BaseUrl );
  }
  
  public String getDescription( )
  {
    return( Description );
  }
    
  public String getDisplayName( )
  {
    return( DisplayName );
  }
  
  void init( String displayName, String desc, String baseUrl, long requiredUserType, String target )
  {
    setBaseUrl( baseUrl );
    setDescription( desc );
    setDisplayName( displayName );
    setRequiredUserType( requiredUserType );
    this.target = target;
  }
  
  public boolean isMenuDisplayed( long userType )
  {
    boolean       retVal = true;
    
    //
    // if security is on for this menu item, then
    // the user type needs to match or the user type
    // needs to be a MES user.
    //
    if ( ( RequiredUserType != mesConstants.USER_NONE ) &&
         ( RequiredUserType != userType )               &&
         ( userType         != mesConstants.USER_MES ) )
    {
      retVal = false;
    }
    return( retVal );
  }
  
  public void setBaseUrl( String baseUrl )
  {
    BaseUrl = baseUrl;
  }
  
  public void setDescription( String desc )
  {
    Description = desc;
  }
  
  public void setDisplayName( String displayName )
  {
    DisplayName = displayName;
  }
  
  protected void setRequiredUserType( long userType )
  {
    RequiredUserType = userType;
  }
  
  public String getTarget()
  {
    return this.target;
  }
}