/*@lineinfo:filename=ApplicationTypeBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ApplicationTypeBean.java $

  Description:
    Base class for application data beans

    This class should maintain any data that is common to all of the
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 2/04/04 4:11p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.mes.database.DBConnect;

public class ApplicationTypeBean
{
  // constants
  public final static String    DB_CONNECT_STRING       = "pool;ApplicationPool";
  public final static int       DEFAULT_BANK_NUMBER     = 3941;
  public final static String    DEFAULT_APP_DESCRIPTION = "Default MES Application";
  public final static String    DEFAULT_APP_SOURCE      = "M";

  // data members
  private int       appBankNumber;
  private String    appDescription;
  private String    appSource;
  private int       appType = -1;

  public ApplicationTypeBean()
  {
    appBankNumber   = 0;
    appDescription  = "";
    appSource       = "";
  }
  
  public void getDataApp(int appType)
  {
    DBConnect         db      = new DBConnect(this, "getDataApp");
    StringBuffer      qs      = new StringBuffer("");
    Connection        con     = null;
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      con = db.getConnection(DB_CONNECT_STRING);
      
      // get the app type data from APP_TYPE
      qs.append("select ");
      qs.append("app_bank_number, app_name, app_source ");
      qs.append("from org_app ");
      qs.append("where app_type = ?");
      
      ps = con.prepareStatement(qs.toString());
      ps.setInt(1, appType);
      
      rs = ps.executeQuery();
      if(rs.next())
      {
        this.appBankNumber  = rs.getInt   ("app_bank_number");
        this.appDescription = rs.getString("app_name");
        this.appSource      = rs.getString("app_source");
      }
      else
      {
        this.appBankNumber  = DEFAULT_BANK_NUMBER;
        this.appDescription = DEFAULT_APP_DESCRIPTION;
        this.appSource      = DEFAULT_APP_SOURCE;
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "fillData: " + e.toString());
    }
    finally
    {
      try
      {
        rs.close();
      }
      catch(Exception e)
      {
      }
      
      try
      {
        ps.close();
      }
      catch(Exception e)
      {
      }
      
      try
      {
        db.releaseConnection();
      }
      catch(Exception e)
      {
      }
    }
  }
  
  public void fillDataApp(long appSeqNum)
  {
    DBConnect         db      = new DBConnect(this, "fillDataApp");
    StringBuffer      qs      = new StringBuffer("");
    Connection        con     = null;
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      con = db.getConnection(DB_CONNECT_STRING);
      
      // get the org num of the user that submitted this app
      qs.append("select ");
      qs.append("app_type from ");
      qs.append("application ");
      qs.append("where ");
      qs.append("app_seq_num = ?");
      
      ps = con.prepareStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        this.appType = rs.getInt("app_Type");
      }
      
      rs.close();
      ps.close();
      
      getDataApp(this.appType);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "fillData: " + e.toString());
    }
    finally
    {
      try
      {
        rs.close();
      }
      catch(Exception e)
      {
      }
      
      try
      {
        ps.close();
      }
      catch(Exception e)
      {
      }
      
      try
      {
        db.releaseConnection();
      }
      catch(Exception e)
      {
      }
    }
  }
  
  public void setAppBankNumber(String appBankNumber)
  {
    try
    {
      setAppBankNumber(Integer.parseInt(appBankNumber));
    }
    catch(Exception e)
    {
    }
  }
  public void setAppBankNumber(int appBankNumber)
  {
    this.appBankNumber = appBankNumber;
  }
  public int getAppBankNumber()
  {
    return this.appBankNumber;
  }
  
  public void setAppSource(String appSource)
  {
    this.appSource = appSource;
  }
  public String getAppSource()
  {
    return this.appSource;
  }

  public void setAppDescription(String appDescription)
  {
    this.appDescription = appDescription;
  }
  public String getAppDescription()
  {
    return this.appDescription;
  }

  public void setAppType(int appType)
  {
    this.appType = appType;
  }
  public int getAppType()
  {
    return this.appType;
  }


}/*@lineinfo:generated-code*/