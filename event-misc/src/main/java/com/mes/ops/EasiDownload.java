package com.mes.ops;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.mes.database.SQLJConnectionBase;


public class EasiDownload extends SQLJConnectionBase
{

  private static final int    ERROR_9966                  = 1;
  private static final int    RESPONSE_9966               = 2;
  private static final int    ERROR_9968                  = 3;
  private static final int    RESPONSE_9968               = 4;
  private static final int    ERROR_9849                  = 5;
  private static final int    RESPONSE_9849               = 6;

  private static final String ERROR_FILE_9966             = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\ERROR_9966_.txt";
  private static final String RESPONSE_FILE_9966          = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\RESPONSE_9966_.txt";
  private static final String ERROR_FILE_9968             = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\ERROR_9968_.txt";
  private static final String RESPONSE_FILE_9968          = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\RESPONSE_9968_.txt";
  private static final String ERROR_FILE_9849             = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\ERROR_9849_.txt";
  private static final String RESPONSE_FILE_9849          = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\RESPONSE_9849_.txt";

  private static final String ARCHIVE_ERROR_FILE_9966     = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\Production Discover Files 9966\\ERROR_9966_";
  private static final String ARCHIVE_RESPONSE_FILE_9966  = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\Production Discover Files 9966\\RESPONSE_9966_";
  private static final String ARCHIVE_ERROR_FILE_9968     = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\Production Discover Files 9968\\ERROR_9968_";
  private static final String ARCHIVE_RESPONSE_FILE_9968  = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\Production Discover Files 9968\\RESPONSE_9968_";
  private static final String ARCHIVE_ERROR_FILE_9849     = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\Production Discover Files 9849\\ERROR_9849_";
  private static final String ARCHIVE_RESPONSE_FILE_9849  = "C:\\Program Files\\Symantec\\Procomm Plus\\Download\\Production Discover Files 9849\\RESPONSE_9849_";

  private String dateString = "";
  private String timeString = "";

  
  public EasiDownload()
  {
    super(SQLJConnectionBase.getDirectConnectString());
    
    // get current date and time
    Calendar   cal  = Calendar.getInstance();
    dateString = (new SimpleDateFormat("MMddyy")).format(cal.getTime());
    timeString = (new SimpleDateFormat("HHmm")).format(cal.getTime());
  }
  
  public void run()
  {
    readFile(ERROR_9966);
    readFile(RESPONSE_9966);
    readFile(ERROR_9968);
    readFile(RESPONSE_9968);
    readFile(ERROR_9849);
    readFile(RESPONSE_9849);
  }

  private void readFile(int fileType)
  {
    String              archiveFile = "";
    File                f           = null;
    SQLJConnectionBase  base        = null;

    try
    {
      //connect();

      switch(fileType)
      {
        case ERROR_9966:
          f           = new File(ERROR_FILE_9966); //link to file
          archiveFile = ARCHIVE_ERROR_FILE_9966    + dateString + "_" + timeString + ".txt"; //if file exists it is to be renamed
          if(f.exists())
          {
            if((new EasiDownloadError()).getData(f))
            {
              f.renameTo(new File(archiveFile));
            }
          }
        break;

        case RESPONSE_9966:
          f           = new File(RESPONSE_FILE_9966);
          archiveFile = ARCHIVE_RESPONSE_FILE_9966 + dateString + "_" + timeString + ".txt";
          if(f.exists())
          {
            if((new EasiDownloadResponse()).getData(f))
            {
              f.renameTo(new File(archiveFile));
            }
          }
        break;

        case ERROR_9968:
          f           = new File(ERROR_FILE_9968);
          archiveFile = ARCHIVE_ERROR_FILE_9968    + dateString + "_" + timeString + ".txt";
          if(f.exists())
          {
            if((new EasiDownloadError()).getData(f))
            {
              f.renameTo(new File(archiveFile));
            }
          }
        break;

        case RESPONSE_9968:
          f           = new File(RESPONSE_FILE_9968);
          archiveFile = ARCHIVE_RESPONSE_FILE_9968 + dateString + "_" + timeString + ".txt";
          if(f.exists())
          {
            if((new EasiDownloadResponse()).getData(f))
            {
              f.renameTo(new File(archiveFile));
            }
          }
        break;

        case ERROR_9849:
          f           = new File(ERROR_FILE_9849);
          archiveFile = ARCHIVE_ERROR_FILE_9849    + dateString + "_" + timeString + ".txt";
          if(f.exists())
          {
            if((new EasiDownloadError()).getData(f))
            {
              f.renameTo(new File(archiveFile));
            }
          }
        break;

        case RESPONSE_9849:
          f           = new File(RESPONSE_FILE_9849);
          archiveFile = ARCHIVE_RESPONSE_FILE_9849 + dateString + "_" + timeString + ".txt";
          if(f.exists())
          {
            if((new EasiDownloadResponse()).getData(f))
            {
              f.renameTo(new File(archiveFile));
            }
          }
        break;

      }
    }
    catch (Exception e)
    {
      System.out.println("Exception: " + e.toString());
    }
    finally
    {
      //cleanUp();
    }
  }



  
  public static void main(String[] args)
  {
    try
    {
      EasiDownload engine = new EasiDownload();
      engine.run();
    }
    catch(Exception e)
    {
      System.out.println("ERROR: " + e.toString());
    }
    finally
    {
    }
  }

}