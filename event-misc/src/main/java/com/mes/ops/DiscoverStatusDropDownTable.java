/*@lineinfo:filename=DiscoverStatusDropDownTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/DiscoverStatusDropDownTable.sqlj $

  Description:
  
  StateDropDownTable
  
  Loads a list of state abbreviations from the countrystate db table
  into a drop down table.
  
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/14/02 9:12a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
//import com.mes.prospects.*;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class DiscoverStatusDropDownTable extends DropDownTable
{
  public DiscoverStatusDropDownTable()
  {
    getData();
  }

  protected boolean getData()
  {
    boolean             getOk   = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      addElement("-1","All Statuses");
      
      connect();
  
      /*@lineinfo:generated-code*//*@lineinfo:58^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    DECISION_CODE c,
//                    DECISION_DESC d
//          from      RAP_APP_DECISION_STATUS
//          order by  DECISION_DESC
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    DECISION_CODE c,\n                  DECISION_DESC d\n        from      RAP_APP_DECISION_STATUS\n        order by  DECISION_DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.DiscoverStatusDropDownTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.DiscoverStatusDropDownTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:64^7*/
  
      rs = it.getResultSet();
  
      while (rs.next())
      {
        addElement(rs);
        getOk = true;
      }
  
      it.close();
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() 
        + "::getData()";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    finally
    {
      cleanUp();
    }

    return getOk;
  }
}/*@lineinfo:generated-code*/