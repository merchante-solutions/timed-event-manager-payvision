/*@lineinfo:filename=TrackingFormStatistics*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/TrackingFormStatistics.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 4/11/02 5:16p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class TrackingFormStatistics extends DateSQLJBean
{
  private boolean               submitted           = false;
  private boolean               refresh             = false;

  public  Vector                stats               = new Vector();
  
  //search criteria
  private int                   lastFromMonth       = -1;
  private int                   lastFromDay         = -1;
  private int                   lastFromYear        = -1;
  private int                   lastToMonth         = -1;
  private int                   lastToDay           = -1;
  private int                   lastToYear          = -1;
  
  
  public TrackingFormStatistics()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:68^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    dept_code, dept_desc
//          from      departments
//          where     dept_code != -1
//          order by  dept_code 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    dept_code, dept_desc\n        from      departments\n        where     dept_code != -1\n        order by  dept_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.TrackingFormStatistics",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.TrackingFormStatistics",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:74^7*/
      
      rs = it.getResultSet();
    
      while(rs.next())
      {
        Stat tempStat = new Stat();
        
        tempStat.setDeptCode(rs.getInt("dept_code"));
        tempStat.setDeptDesc(rs.getString("dept_desc"));

        stats.add(tempStat);
      }
    
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("TrackingFormStatistics: Constructor", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public void getData()
  {
    try
    {
      if (true)
      {
        getStats();
      }
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
    }
  }

  /*
  ** METHOD public void getRepairData()
  **
  */
  private void getStats()
  {
    try
    {
      connect();

      Date fromDate = getSqlFromDate();
      Date toDate   = getSqlToDate(); 
      
      for(int x=0; x<stats.size(); x++)
      {
        Stat tempStat = (Stat)stats.elementAt(x);

        tempStat.setNumReceived(getReceivedCount(tempStat.getDeptCode(), fromDate, toDate));
        tempStat.setNumCompleted(getCompletedCount(tempStat.getDeptCode(), fromDate, toDate));
        tempStat.setAveTime(getAverageTime(tempStat.getDeptCode(), fromDate, toDate));
      }
    }
    catch(Exception e)
    {
      logEntry("getRepairData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public class Stat
  {
    private int     deptCode      = 0;
    private String  deptDesc      = "";
    private long    numReceived   = 0;
    private long    numCompleted  = 0;
    private double  aveTime       = 0;

    public void setDeptCode(int deptCode)
    {
      this.deptCode = deptCode;
    }
    public void setDeptDesc(String deptDesc)
    {
      this.deptDesc = deptDesc;
    }
    public void setNumReceived(long numReceived)
    {
      this.numReceived = numReceived;
    }
    public void setNumCompleted(long numCompleted)
    {
      this.numCompleted = numCompleted;
    }
    public void setAveTime(double aveTime)
    {
      this.aveTime = aveTime;
    }
    public int getDeptCode()
    {
      return this.deptCode;
    }
    public String getDeptDesc()
    {
      return this.deptDesc;
    }
    public long getNumReceived()
    {
      return this.numReceived;
    }
    public long getNumCompleted()
    {
      return this.numCompleted;
    }
    public double getAveTime()
    {
      return this.aveTime;
    }
  }

  private long getReceivedCount(int dept, Date fromDate, Date toDate)
  {
    long result = 0;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:215^7*/

//  ************************************************************
//  #sql [Ctx] it = { select count(*) 
//                          
//          from    app_tracking
//  
//          where   dept_code = :dept and date_received is not null and trunc(date_received) between :fromDate and :toDate 
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select count(*) \n                        \n        from    app_tracking\n\n        where   dept_code =  :1  and date_received is not null and trunc(date_received) between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.TrackingFormStatistics",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,dept);
   __sJT_st.setDate(2,fromDate);
   __sJT_st.setDate(3,toDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.TrackingFormStatistics",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:223^7*/
      
      rs = it.getResultSet();
    
      if(rs.next())
      {
        result = rs.getLong(1);
      }
    
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return result;
  }
  
  private double getAverageTime(int dept, Date fromDate, Date toDate)
  {
    double result = 0.0;
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:255^7*/

//  ************************************************************
//  #sql [Ctx] it = { select avg((months_between((date_completed + 1), date_received) * 744) - 24) 
//          from app_tracking 
//          where dept_code = :dept and date_completed is not null and date_received is not null
//          and (trunc(date_received) between :fromDate and :toDate) and (trunc(date_completed) between :fromDate and :toDate)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select avg((months_between((date_completed + 1), date_received) * 744) - 24) \n        from app_tracking \n        where dept_code =  :1  and date_completed is not null and date_received is not null\n        and (trunc(date_received) between  :2  and  :3 ) and (trunc(date_completed) between  :4  and  :5 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.TrackingFormStatistics",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,dept);
   __sJT_st.setDate(2,fromDate);
   __sJT_st.setDate(3,toDate);
   __sJT_st.setDate(4,fromDate);
   __sJT_st.setDate(5,toDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.TrackingFormStatistics",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:261^7*/
      
      rs = it.getResultSet();
    
      if(rs.next())
      {
        result = rs.getDouble(1);
      }
    
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return result;
  }

  private long getCompletedCount(int dept, Date fromDate, Date toDate)
  {
    long result = 0;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:293^7*/

//  ************************************************************
//  #sql [Ctx] it = { select count(*) 
//                          
//          from    app_tracking
//  
//          where   dept_code = :dept and date_completed is not null and trunc(date_completed) between :fromDate and :toDate 
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select count(*) \n                        \n        from    app_tracking\n\n        where   dept_code =  :1  and date_completed is not null and trunc(date_completed) between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.TrackingFormStatistics",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,dept);
   __sJT_st.setDate(2,fromDate);
   __sJT_st.setDate(3,toDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.TrackingFormStatistics",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:301^7*/
      
      rs = it.getResultSet();
    
      if(rs.next())
      {
        result = rs.getLong(1);
      }
    
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return result;
  }

  public void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
 
  public boolean isSubmitted()
  {
    return this.submitted;
  }

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }


}/*@lineinfo:generated-code*/