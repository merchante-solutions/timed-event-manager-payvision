/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/DocQueueBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/16/01 5:29p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

public class DocQueueBean extends QueueBean
{
  public DocQueueBean()
  {
    super("DocQueueBean");
    fillReportMenuItems();
  }
  
  public DocQueueBean(String msg)
  {
    super(msg);
    fillReportMenuItems();
  }
  
  public boolean getQueueData(long queueKey, int queueType)
  {
    return(super.getQueueData(
      queueKey,
      queueType,
      "app_queue_document c, merchant d",
      "c.*, d.merc_cntrl_number, d.merch_business_name",
      "and a.app_seq_num = c.app_seq_num and a.app_seq_num = d.app_seq_num"));
  }
  
  public void fillReportMenuItems()
  {
    addReportMenuItem(new ReportMenuItem("Documentation Queue", "mes_queue.jsp?queueType=" +  QueueConstants.QUEUE_DOCUMENTATION + "&queueStage=" + QueueConstants.Q_DOCUMENT_PENDING));
  }
  
  public void getFunctionOptions(javax.servlet.jsp.JspWriter out, long appSeqNum)
  {
    try
    {
      out.println("<ul>");
      out.println("  <li><a href=\"mes_doc_review.jsp?appSeqNum=" + appSeqNum + "\">Documentation Review</a></li>");
      out.println("  <li><a href=\"mes_credit_notes.jsp?appSeqNum=" + appSeqNum + "&queueStage=" + queueStage + "\">Notes</a></li>");
      out.println("</ul>");
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFunctionOptions: " + e.toString());
    }
  }
}
