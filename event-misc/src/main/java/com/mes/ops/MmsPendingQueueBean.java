/*@lineinfo:filename=MmsPendingQueueBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/MmsPendingQueueBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 6/05/02 3:15p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Locale;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class MmsPendingQueueBean extends SQLJConnectionBase
{
  

  public  Vector              queueList               = new Vector();
  
  public MmsPendingQueueBean()
  {
  }

  public int numQueueList()
  {
    return queueList.size();
  }

  public void getQueueData()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:67^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   mms.app_seq_num,
//                   mms.request_id,
//                   mms.app_date,
//                   mms.process_end_date,
//                   mms.process_response, 
//                   mms.term_application, 
//                   mms.merch_number, 
//                   merch.merc_cntrl_number, 
//                   merch.merch_business_name,
//                   app.appsrctype_code
//  
//          from     mms_stage_info mms, 
//                   merchant merch,
//                   application app
//  
//          where    mms.app_seq_num = merch.app_seq_num and mms.app_seq_num = app.app_seq_num and
//                   mms.process_response is not null and mms.process_response != 'success'
//  
//          order by mms.app_date asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   mms.app_seq_num,\n                 mms.request_id,\n                 mms.app_date,\n                 mms.process_end_date,\n                 mms.process_response, \n                 mms.term_application, \n                 mms.merch_number, \n                 merch.merc_cntrl_number, \n                 merch.merch_business_name,\n                 app.appsrctype_code\n\n        from     mms_stage_info mms, \n                 merchant merch,\n                 application app\n\n        where    mms.app_seq_num = merch.app_seq_num and mms.app_seq_num = app.app_seq_num and\n                 mms.process_response is not null and mms.process_response != 'success'\n\n        order by mms.app_date asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.MmsPendingQueueBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.MmsPendingQueueBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {

        ActivationRec rec = new ActivationRec();

        rec.setAppSeqNum      (rs.getString("app_seq_num"));
        rec.setRequestId      (rs.getString("request_id"));
        rec.setControlNum     (rs.getString("merc_cntrl_number"));
        rec.setDba            (rs.getString("merch_business_name"));
        rec.setAppTypeDesc    (rs.getString("appsrctype_code"));
        rec.setTermApplication(rs.getString("term_application"));
        rec.setStatus         (rs.getString("process_response"));
        rec.setReqDate        (java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("app_date")));
        rec.setResDate        (java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("process_end_date")));
        rec.setMerchNumber    (rs.getString("merch_number"));
        
        queueList.add(rec);
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      logEntry("getQueueData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  


     public String getAppSeqNum(int idx)
     {
       ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
       return rec.getAppSeqNum();
     }
     public String getRequestId(int idx)
     {
       ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
       return rec.getRequestId();
     }

     public String getControlNum(int idx)
     {
       ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
       return rec.getControlNum();
     }

     public String getDba(int idx)
     {
       ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
       return rec.getDba();
     }

     public String getAppTypeDesc(int idx)
     {
       ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
       return rec.getAppTypeDesc();
     }


     public String getTermApplication(int idx)
     {
       ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
       return rec.getTermApplication();
     }

     public String getStatus(int idx)
     {
       ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
       return rec.getStatus();
     }
     
     public String getReqDate(int idx)
     {
       ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
       return rec.getReqDate();
     }

     public String getResDate(int idx)
     {
       ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
       return rec.getResDate();
     }

     public String getMerchNumber(int idx)
     {
       ActivationRec rec = (ActivationRec)queueList.elementAt(idx);
       return rec.getMerchNumber();
     }


   public class  ActivationRec
   {
     private String appSeqNum         = "";
     private String requestId         = "";
     private String controlNum        = "";
     private String dba               = "";
     private String termApplication   = "";
     private String status            = "";
     private String reqDate           = "";
     private String resDate           = "";
     private String merchNumber       = "";
     private String appTypeDesc       = "";

     public void setAppSeqNum(String appSeqNum)
     {
       this.appSeqNum = appSeqNum;
     }
     public void setRequestId(String requestId)
     {
       this.requestId = requestId;
     }

     public void setAppTypeDesc(String appTypeDesc)
     {
       this.appTypeDesc = appTypeDesc;
     }
     public void setControlNum(String controlNum)
     {
       this.controlNum =  controlNum;
     }

     public void setDba(String dba)
     {
       this.dba = dba;
     }

     public void setTermApplication(String termApplication)
     {
       this.termApplication = termApplication;
     }

     public void setStatus(String status)
     {
       this.status = status;
     }
     
     public void setReqDate(String reqDate)
     {
       this.reqDate = reqDate;
     }

     public void setResDate(String resDate)
     {
       this.resDate = resDate;
     }

     public void setMerchNumber(String merchNumber)
     {
       this.merchNumber = merchNumber;
     }

     public String getMerchNumber()
     {
       return this.merchNumber;
     }



     public String getAppSeqNum()
     {
       return this.appSeqNum;
     }
     public String getRequestId()
     {
       return this.requestId;
     }

     public String getAppTypeDesc()
     {
       return this.appTypeDesc;
     }

     public String getControlNum()
     {
       return this.controlNum;
     }

     public String getDba()
     {
       return this.dba;
     }

     public String getTermApplication()
     {
       return this.termApplication;
     }

     public String getStatus()
     {
       return this.status;
     }
     
     public String getReqDate()
     {
       return this.reqDate;
     }

     public String getResDate()
     {
       return this.resDate;
     }

   }

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }


}/*@lineinfo:generated-code*/