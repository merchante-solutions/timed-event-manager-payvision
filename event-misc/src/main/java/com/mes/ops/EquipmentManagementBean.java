/*@lineinfo:filename=EquipmentManagementBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/EquipmentManagementBean.sqlj $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-08-07 17:15:01 -0700 (Tue, 07 Aug 2007) $
  Version            : $Revision: 13968 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class EquipmentManagementBean extends com.mes.screens.SequenceDataBean
{
  public static final int DEPLOYED                          = 1;
  public static final int NEEDED                            = 2;
  
  private Vector          deployedEquip                     = new Vector();
  private Vector          equipToDeploy                     = new Vector();
    
  private Timestamp       today                             = null;
  private boolean         primaryKey                        = false;
  private long            primaryKeyValue                   = -1;
  private String          merchantName                      = "";
  private String          controlNumber                     = "";

  public EquipmentManagementBean()
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:62^7*/

//  ************************************************************
//  #sql [Ctx] { select    sysdate 
//          from      dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    sysdate  \n        from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.EquipmentManagementBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   today = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:66^7*/
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void getMerchantEquipment()
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    boolean           wasStale  = false;
    
    try
    {
      if(isConnectionStale())
      {
        wasStale = true;
        connect();
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:92^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  me.merchequip_equip_quantity,
//                  me.equip_model,
//                  me.equiplendtype_code,
//                  me.quantity_deployed,
//                  me.merchequip_amount,
//                  me.equiptype_code,
//                  eq.equip_descriptor,
//                  mfr.equipmfgr_mfr_name,
//                  lt.equiplendtype_description
//          from    merchequipment  me,
//                  equipment       eq,
//                  equipmfgr       mfr,
//                  equiplendtype   lt
//          where   me.app_seq_num  = :primaryKeyValue and
//                  me.equiplendtype_code != :mesConstants.APP_EQUIP_OWNED and
//                  me.equiptype_code not in (7, 8) and
//                  eq.equip_model  = me.equip_model and
//                  eq.equipmfgr_mfr_code = mfr.equipmfgr_mfr_code and
//                  me.equiplendtype_code = lt.equiplendtype_code
//          order by mfr.equipmfgr_mfr_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  me.merchequip_equip_quantity,\n                me.equip_model,\n                me.equiplendtype_code,\n                me.quantity_deployed,\n                me.merchequip_amount,\n                me.equiptype_code,\n                eq.equip_descriptor,\n                mfr.equipmfgr_mfr_name,\n                lt.equiplendtype_description\n        from    merchequipment  me,\n                equipment       eq,\n                equipmfgr       mfr,\n                equiplendtype   lt\n        where   me.app_seq_num  =  :1  and\n                me.equiplendtype_code !=  :2  and\n                me.equiptype_code not in (7, 8) and\n                eq.equip_model  = me.equip_model and\n                eq.equipmfgr_mfr_code = mfr.equipmfgr_mfr_code and\n                me.equiplendtype_code = lt.equiplendtype_code\n        order by mfr.equipmfgr_mfr_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.EquipmentManagementBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKeyValue);
   __sJT_st.setInt(2,mesConstants.APP_EQUIP_OWNED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.EquipmentManagementBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:114^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        int needed   = 0;
        int deployed = 0;
    
        if(!isBlank(rs.getString("merchequip_equip_quantity")))
        {
          needed = rs.getInt("merchequip_equip_quantity");
        }
    
        /***************************************************************************************
        with code uncommented it will display equipment that is yet to be deployed
        with code commented it displays all equipment requested on app.. whether deployed or not
        ****************************************************************************************/
        
        deployed = getDeployedQuan(primaryKeyValue,rs.getString("equip_model"),rs.getString("equiplendtype_code"),rs.getString("quantity_deployed"));
   
        int quan = needed - deployed;
        //if(quan > 0)
        //{
          EquipRec tempRec =  new EquipRec();
          tempRec.setPartNum        (rs.getString("equip_model"));
          tempRec.setProductName    (rs.getString("equipmfgr_mfr_name"));
          tempRec.setProductDesc    (rs.getString("equip_descriptor"));
          tempRec.setUnitPrice      (formatCurr(rs.getString("merchequip_amount")));
          tempRec.setLendTypeDesc   (rs.getString("equiplendtype_description"));
          tempRec.setLendTypeCode   (rs.getString("equiplendtype_code"));
          tempRec.setEquipTypeCode  (rs.getInt("equiptype_code"));
          tempRec.setQuanRequested  (needed);
          tempRec.setQuanDeployed   (deployed);
          tempRec.setQuanNeeded     (quan);
          equipToDeploy.add(tempRec);
        //}  
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMerchantEquipment(" + primaryKeyValue + ")", e.toString());
      addError("getMerchantEquipment: " + e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
  }
  
  private int getDeployedQuan(long primaryKey, String model, String lendTypeCode, String defaultDeployed)
  {
    long              merchNum = 0L;
    int               result   = 0;

    try
    {
      int mNumCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:179^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merch_number)
//          
//          from    merchant
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merch_number)\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.EquipmentManagementBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mNumCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:185^7*/
      
      if(mNumCount == 0)
      {
        if(isBlank(defaultDeployed))
        {
          result = 0;
        }
        else
        {
          result = Integer.parseInt(defaultDeployed);
        }
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:200^9*/

//  ************************************************************
//  #sql [Ctx] { select  merch_number
//            
//            from    merchant
//            where   app_seq_num = :primaryKey
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_number\n           \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.EquipmentManagementBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:206^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:208^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(td.merch_num)
//            
//            from    tmg_deployment td,
//                    tmg_part_states tps,
//                    tmg_part_types tpt
//            where   td.merch_num = :merchNum and
//                    td.end_ts is null and
//                    td.deploy_id = tps.deploy_id and
//                    tps.pt_id = tpt.pt_id and
//                    tpt.model_code = :model        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(td.merch_num)\n           \n          from    tmg_deployment td,\n                  tmg_part_states tps,\n                  tmg_part_types tpt\n          where   td.merch_num =  :1  and\n                  td.end_ts is null and\n                  td.deploy_id = tps.deploy_id and\n                  tps.pt_id = tpt.pt_id and\n                  tpt.model_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.EquipmentManagementBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchNum);
   __sJT_st.setString(2,model);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:220^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("getDeployedQuan(" + primaryKey + ")", e.toString());
      addError("getDeployedQuan: " + e.toString());
      result = 0;
    }
    
    return result;
  }

  public void getDeployedEquipment(String merchNum)
  {
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
   
    boolean           wasStale  = false;
    
    try
    {
      if(isConnectionStale())
      {
        wasStale = true;
        connect();
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:248^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tpt.pt_name                         equipment_type,
//                  tps.serial_num                      serial_number,
//                  tpc.pc_name                         equipment_class,
//                  tpd.disp_name                       equipment_disposition,
//                  to_char(td.start_ts, 'mm/dd/yyyy')  date_deployed,
//                  tps.state_id                        state_id
//          from    tmg_deployment td,
//                  tmg_part_states tps,
//                  tmg_part_types tpt,
//                  tmg_part_classes tpc,
//                  tmg_part_dispositions tpd,
//                  tmg_op_log tol
//          where   td.merch_num = :merchNum and
//                  td.end_ts is null and
//                  td.deploy_id = tps.deploy_id and
//                  tps.pt_id = tpt.pt_id and
//                  tps.pc_code = tpc.pc_code and
//                  tps.disp_code = tpd.disp_code and
//                  tps.op_id = tol.op_id and
//                  tol.op_code in ( 'PART_DEPLOY', 'PART_DISP_CHANGE' )
//          order by td.start_ts desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tpt.pt_name                         equipment_type,\n                tps.serial_num                      serial_number,\n                tpc.pc_name                         equipment_class,\n                tpd.disp_name                       equipment_disposition,\n                to_char(td.start_ts, 'mm/dd/yyyy')  date_deployed,\n                tps.state_id                        state_id\n        from    tmg_deployment td,\n                tmg_part_states tps,\n                tmg_part_types tpt,\n                tmg_part_classes tpc,\n                tmg_part_dispositions tpd,\n                tmg_op_log tol\n        where   td.merch_num =  :1  and\n                td.end_ts is null and\n                td.deploy_id = tps.deploy_id and\n                tps.pt_id = tpt.pt_id and\n                tps.pc_code = tpc.pc_code and\n                tps.disp_code = tpd.disp_code and\n                tps.op_id = tol.op_id and\n                tol.op_code in ( 'PART_DEPLOY', 'PART_DISP_CHANGE' )\n        order by td.start_ts desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.EquipmentManagementBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.EquipmentManagementBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:271^7*/
        
      rs = it.getResultSet();
        
      while(rs.next())
      {
        EquipRec tempRec =  new EquipRec();
        tempRec.setProductDesc( rs.getString("equipment_type") );
        tempRec.setLendTypeDesc(  rs.getString("equipment_disposition") );
        tempRec.setClassTypeDesc( rs.getString("equipment_class") );
        tempRec.setDeployedDate(  rs.getString("date_deployed") );
        tempRec.setSerialNumber(  rs.getString("serial_number") );
        tempRec.setProductName( rs.getString("state_id") );
        deployedEquip.add(tempRec);
      }
       
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getDeployedEquipment(" + merchNum + ")", e.toString());
      addError("getDeployedEquipment: " + e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
  }  
  
  public boolean hasPrimaryKey()
  {
    return this.primaryKey;
  }
  
  public long getPrimaryKeyValue()
  {
    return this.primaryKeyValue;
  }

  public String getMerchantName()
  {
    return this.merchantName;
  }

  public String getControlNumber()
  {
    return this.controlNumber;
  }

  private void getPrimaryKeyFromMerch(String merchantNumber)
  {
    boolean wasStale  = false;

    try
    {
      if(isConnectionStale())
      {
        wasStale = true;
        connect();
      }

      /*@lineinfo:generated-code*//*@lineinfo:336^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num,
//                  merch_business_name,
//                  merc_cntrl_number
//          
//          from    merchant
//          where   merch_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num,\n                merch_business_name,\n                merc_cntrl_number\n         \n        from    merchant\n        where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.EquipmentManagementBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   primaryKeyValue = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   merchantName = (String)__sJT_rs.getString(2);
   controlNumber = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:346^7*/

      primaryKey = true;
    }
    catch(Exception e)
    {
      logEntry("getPrimaryKeyFromMerch(" + merchantNumber + ")", e.toString());
      //no value to put into primary key...
      primaryKey = false;
      getMerchantNameFromMif(merchantNumber);
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
  }

  private void getMerchantNameFromMif(String merchantNumber)
  {

    boolean wasStale  = false;

    try
    {
      if(isConnectionStale())
      {
        wasStale = true;
        connect();
      }

      /*@lineinfo:generated-code*//*@lineinfo:379^7*/

//  ************************************************************
//  #sql [Ctx] { select  dba_name
//          
//          from    mif
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  dba_name\n         \n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.EquipmentManagementBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:385^7*/
    }
    catch(Exception e)
    {
      logEntry("getMerchantNameFromMif(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
  }

  public void moveToNewStage(String merchantNumber, int stage)
  {
    moveToNewStage(merchantNumber, stage, "SYSTEM");
  }

  public void moveToNewStage(String merchantNumber, int stage, String userName)
  {
    moveToNewStage(merchantNumber, stage, userName, true);
  }


  public void moveToNewStage(String merchantNumber, int stage, String userName, boolean deploymentCompleted)
  {
    getPrimaryKeyFromMerch(merchantNumber);

    if(!hasPrimaryKey())
    {
      return;
    }

    boolean           wasStale  = false;
    
    try
    {
      if(isConnectionStale())
      {
        wasStale = true;
        connect();
      }

      /*@lineinfo:generated-code*//*@lineinfo:430^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_queue
//          set     app_queue_stage = :QueueConstants.Q_EQUIPMENT_COMPLETE
//          where   app_seq_num     = :primaryKeyValue and 
//                  app_queue_type  = :QueueConstants.QUEUE_EQUIPMENT
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_queue\n        set     app_queue_stage =  :1 \n        where   app_seq_num     =  :2  and \n                app_queue_type  =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.ops.EquipmentManagementBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.Q_EQUIPMENT_COMPLETE);
   __sJT_st.setLong(2,primaryKeyValue);
   __sJT_st.setInt(3,QueueConstants.QUEUE_EQUIPMENT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:436^7*/

      if(stage == QueueConstants.Q_EQUIPMENT_COMPLETE && deploymentCompleted)
      {
      
        /*@lineinfo:generated-code*//*@lineinfo:441^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//            set     date_completed  = :today,
//                    status_code     = 502,
//                    contact_name    = :userName
//            where   app_seq_num     = :primaryKeyValue and 
//                    dept_code       = :QueueConstants.DEPARTMENT_DEPLOYMENT and
//                    date_completed is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n          set     date_completed  =  :1 ,\n                  status_code     = 502,\n                  contact_name    =  :2 \n          where   app_seq_num     =  :3  and \n                  dept_code       =  :4  and\n                  date_completed is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.ops.EquipmentManagementBean",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setString(2,userName);
   __sJT_st.setLong(3,primaryKeyValue);
   __sJT_st.setInt(4,QueueConstants.DEPARTMENT_DEPLOYMENT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:450^9*/


        //put note in account status and in call tracking...
        try
        {
          AppNoteBean  anb = new AppNoteBean();
          anb.setAppSeqNum(primaryKeyValue);
          anb.setQueueStage(0);
          anb.setUserId(userName);
          anb.setAppNote("Deployment moved to completed");
          anb.setDepartment(QueueConstants.DEPARTMENT_DEPLOYMENT);
          anb.submitData();
        }
        catch(Exception e)
        {
          com.mes.support.SyncLog.LogEntry("AppNoteBean.submitData(" + primaryKeyValue + ")", e.toString());
        }
   
      }
    }
    catch(Exception e)
    {
      logEntry("moveToNewStage(" + primaryKeyValue + ")", e.toString());
      addError("moveToNewStage: " + e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
  }

  public int getNumNeeded()
  {
    return equipToDeploy.size();
  }
  public int getNumDeployed()
  {
    return deployedEquip.size();
  }

  public void getData(String merchantNumber)
  {
    getPrimaryKeyFromMerch(merchantNumber);
    
    if(hasPrimaryKey())
    {
      getMerchantEquipment();
    }
    
    getDeployedEquipment(merchantNumber);  
  }
  
  public String formatDate(Date raw)
  {
    String result   = "";
    if(raw == null)
    {
      return result;
    }
    try
    {
      DateFormat df   = new SimpleDateFormat("MM/dd/yy");
      result = df.format(raw);
      return result;
    }
    catch(Exception e)
    {
    }
    return result;
  }

  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }
  
  public String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;
      
      index = body.indexOf(' ');
      
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
      System.out.println("error will robinson %20");
    }
    return body;

  }
  
  public String getClassTypeDesc(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getClassTypeDesc());
  }

  public String getUnitPrice(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getUnitPrice());
  }
  
  public String getUnitCost(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getUnitCost());
  }
  
  public String getPartNum(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getPartNum());
  }
  public String getProductName(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getProductName());
  }
  public String getLendTypeDesc(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getLendTypeDesc());
  }

  public String getLendTypeCode(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getLendTypeCode());
  }

  public String getProductDesc(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getProductDesc());
  }
  
  public String getDeployedDate(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getDeployedDate());
  }

  public String getSerialNumber(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getSerialNumber());
  }

  public int getQuanNeeded(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getQuanNeeded());
  }
    
  public int getQuanDeployed(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getQuanDeployed());
  }

  public int getQuanRequested(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getQuanRequested());
  }

  public int getEquipTypeCode(int idx, int vec)
  {
    EquipRec equip = null;
    switch(vec)
    {
      case NEEDED:
        equip = (EquipRec)equipToDeploy.elementAt(idx);
      break;
      case DEPLOYED:
        equip = (EquipRec)deployedEquip.elementAt(idx);
      break;
    }
    return(equip.getEquipTypeCode());
  }


  public class EquipRec
  { 
    private String unitPrice      = "";
    private String partNum        = "";
    private String productName    = "";
    private String productDesc    = "";
    private String lendTypeDesc   = "";
    private String lendTypeCode   = "";
    private int    quanNeeded     = 0;
    private int    quanDeployed   = 0;
    private int    quanRequested  = 0;
    private int    equipTypeCode  = -1;

    //for equipment that has already been assigned
    private String serialNumber   = "";
    private String deployedDate   = "";
    private String unitCost       = "";
    private String classTypeDesc  = "";
   
    EquipRec()
    {}

    public void setQuanNeeded(int quanNeeded)
    {
      this.quanNeeded = quanNeeded;
    }
    public void setQuanDeployed(int quanDeployed)
    {
      this.quanDeployed = quanDeployed;
    }
    public void setQuanRequested(int quanRequested)
    {
      this.quanRequested = quanRequested;
    }
    
    public void setClassTypeDesc(String classTypeDesc)
    {
      this.classTypeDesc = classTypeDesc;
    }
    public void setUnitPrice(String unitPrice)
    {
      this.unitPrice = unitPrice;
    }
    public void setUnitCost(String unitCost)
    {
      this.unitCost = unitCost;
    }
    public void setPartNum(String partNum)
    {
      this.partNum = partNum;
    }
    public void setProductName(String productName)
    {
      this.productName = productName;
    }
    
    public void setProductDesc(String productDesc)
    {
      this.productDesc = productDesc;
    }
    public void setLendTypeDesc(String lendTypeDesc)
    {
      this.lendTypeDesc = lendTypeDesc;
    }
    public void setLendTypeCode(String lendTypeCode)
    {
      this.lendTypeCode = lendTypeCode;
    }
    public void setEquipTypeCode(int equipTypeCode)
    {
      this.equipTypeCode = equipTypeCode;
    }

    public void setSerialNumber(String serialNumber)
    {
      this.serialNumber = serialNumber;
    }
    public void setDeployedDate(String deployedDate)
    {
      this.deployedDate = deployedDate;
    }
    
    public int getQuanNeeded()
    {
      return this.quanNeeded;
    }

    public int getQuanDeployed()
    {
      return this.quanDeployed;
    }

    public int getQuanRequested()
    {
      return this.quanRequested;
    }

    public int getEquipTypeCode()
    {
      return this.equipTypeCode;
    }

    public String getClassTypeDesc()
    {
      return this.classTypeDesc;
    }
    public String getUnitPrice()
    {
      return this.unitPrice;
    }
    public String getUnitCost()
    {
      return this.unitCost;
    }
    public String getPartNum()
    {
      return this.partNum;
    }
    public String getProductName()
    {
      return this.productName;
    }
    public String getLendTypeDesc()
    {
      return this.lendTypeDesc;
    }
    public String getLendTypeCode()
    {
      return this.lendTypeCode;
    }
    public String getProductDesc()
    {
      return this.productDesc;
    }
    public String getSerialNumber()
    {
      return this.serialNumber;
    }
    public String getDeployedDate()
    {
      return this.deployedDate;
    }
  }
 
}/*@lineinfo:generated-code*/