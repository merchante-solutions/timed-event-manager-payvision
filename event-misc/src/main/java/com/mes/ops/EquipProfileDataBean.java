/*@lineinfo:filename=EquipProfileDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/EquipProfileDataBean.sqlj $

  Last Modified By   : $Author: Jkirton $
  Last Modified Date : $Date: 9/25/03 2:48p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.lang.reflect.Method;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.queues.ProgrammingQueue;
import com.mes.queues.QueueTools;


public class EquipProfileDataBean extends SQLJConnectionBase
{
  // create class log category
  static Category log = Category.getInstance(EquipProfileDataBean.class.getName());

  // constants
  // (NONE)
  
  
  // data members
  protected   long                        requestId           = 0L;
  protected   int                         residentQueue       = MesQueues.Q_NONE;
  protected   long                        appSeqNum           = 0L;
  protected   boolean                     loaded              = false;
  protected   boolean                     hasExpandedFields   = false;
  protected   String                      expndInclFilename   = "";
  protected   MmsStageSetupBean           stageInfo           = new MmsStageSetupBean();
  protected   MmsExpandedFieldsBean       expndInfo           = new MmsExpandedFieldsBean();
  protected   int                         profGenCode         = mesConstants.PROFGEN_UNDEFINED;
  protected   String                      profGenName         = "";
  protected   String                      vnumber             = "";
  protected   String                      tid                 = "";
  protected   String                      profCmpltdDate      = ""; // profile completed date string
  protected   boolean                     profileCompleted    = false;
  
    
  public static void markProfileCompleted(long requestId)
  {
    (new EquipProfileDataBean())._markProfileCompleted(requestId);
  }
  private void _markProfileCompleted(long requestId)
  {
    try {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:70^7*/

//  ************************************************************
//  #sql [Ctx] { update    merch_vnumber
//          set       profile_completed_date=sysdate
//          where     request_id = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update    merch_vnumber\n        set       profile_completed_date=sysdate\n        where     request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.ops.EquipProfileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:75^7*/
    }
    catch(Exception e) {
      logEntry("_markProfileCompleted("+requestId+")", e.toString());
    }
    finally {
      cleanUp();
    }
  }

  // construction
  public EquipProfileDataBean()
  {
  }

  public boolean isLoaded()
  {
    return loaded;
  }
  
  public void unload()
  {
    clear();
  }
  
  protected void clear()
  {
    requestId           = 0L;
    residentQueue       = MesQueues.Q_NONE;
    appSeqNum           = 0L;
    loaded              = false;
    hasExpandedFields   = false;
    expndInclFilename   = "";
    stageInfo           = new MmsStageSetupBean();
    expndInfo           = new MmsExpandedFieldsBean();
    profGenCode         = mesConstants.PROFGEN_UNDEFINED;
    profGenName         = "";
    vnumber             = "";
    tid                 = "";
    profCmpltdDate      = "";
    profileCompleted    = false;
  }
  
  public void reLoad()
  {
    if(!loaded)
      return;

    long rid = this.requestId;
    unload();
    load(rid,MesQueues.Q_NONE);
  }
  
  public boolean load(long requestId, int residentQueue)
  {
    if(loaded && this.requestId == requestId && this.residentQueue == residentQueue)
      return true;

    try {
    
      if(residentQueue == MesQueues.Q_NONE) {
        residentQueue = QueueTools.getCurrentQueueType(requestId,MesQueues.Q_ITEM_TYPE_PROGRAMMING);
        if(residentQueue == MesQueues.Q_NONE) {
          log.error("Load failed (requestId:"+requestId+") Unable to determine the resident queue.");
          return false;
        }
      }

      long asn = stageInfo.figureOutPrimaryKey(requestId);
      if(asn<1L) {
        log.error("Load failed (requestId:"+requestId+") Unable to determine the app seq num.");
        return false;
      }

      unload();

      stageInfo.getData(asn,requestId);
      expndInfo.getData(asn,requestId);
    
      log.debug("stageInfo.getTermAppProfGen()="+stageInfo.getTermAppProfGen());
      hasExpandedFields = stageInfo.hasExpandedFields(stageInfo.getTermAppProfGen());
    
      stageInfo.setIncludeFileName();
    
      // set expndInclFilename
      String stageInclFilename = stageInfo.getIncludeFileName();
      log.debug("load() stageInclFilename="+stageInclFilename);
      if(stageInclFilename.length()<1 || stageInclFilename.equals(MmsStageSetupBean.NOFILE))
        expndInclFilename="";
      else {
        int lastSlashIndx = ((stageInclFilename.lastIndexOf('\\')<0)? stageInclFilename.lastIndexOf('/'):stageInclFilename.lastIndexOf('\\'));
        log.debug("load() lastSlashIndx="+lastSlashIndx);
        if(lastSlashIndx > 0) {
          // dir path prefixing
          expndInclFilename = 
            stageInclFilename.substring(0,lastSlashIndx)
            + "/epincl_"
            + stageInclFilename.substring(lastSlashIndx+1);
        } else {
          // file name only
          expndInclFilename = 
            "epincl_"
            + stageInclFilename;
        }
      }
      // load the profile specific items
      int pc;
      try {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:185^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(mv.profile_generator_code,:mesConstants.PROFGEN_MMS)
//                    ,epg.name
//                    ,mv.vnumber
//                    ,mv.tid
//                    ,to_char(mv.profile_completed_date,'DY, MM/DD/YYYY HH:MI.SS')
//                    ,decode(mv.profile_completed_date,null,'0','1')
//            
//            from    merch_vnumber              mv
//                    ,EQUIP_PROFILE_GENERATORS  epg
//            where   request_id = :requestId
//                    and nvl(mv.profile_generator_code,1) = epg.code(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(mv.profile_generator_code, :1 )\n                  ,epg.name\n                  ,mv.vnumber\n                  ,mv.tid\n                  ,to_char(mv.profile_completed_date,'DY, MM/DD/YYYY HH:MI.SS')\n                  ,decode(mv.profile_completed_date,null,'0','1')\n           \n          from    merch_vnumber              mv\n                  ,EQUIP_PROFILE_GENERATORS  epg\n          where   request_id =  :2 \n                  and nvl(mv.profile_generator_code,1) = epg.code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.EquipProfileDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.PROFGEN_MMS);
   __sJT_st.setLong(2,requestId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 6) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(6,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.profGenCode = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   this.profGenName = (String)__sJT_rs.getString(2);
   this.vnumber = (String)__sJT_rs.getString(3);
   this.tid = (String)__sJT_rs.getString(4);
   this.profCmpltdDate = (String)__sJT_rs.getString(5);
   pc = __sJT_rs.getInt(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:203^9*/

      }
      catch(Exception e) {
        logEntry("load("+requestId+") MERCH_VNUMBER", e.toString());
        unload();
        return false;
      }
      finally {
        cleanUp();
      }

      this.profileCompleted = ((pc==1) || ProgrammingQueue.isCompletedProgrammingQueue(residentQueue));
      this.requestId = requestId;
      this.residentQueue = residentQueue;
      this.appSeqNum = asn;

      loaded = true;
    }
    catch(Exception e) {
      unload();
    }
    
    log.debug("load(requestId:"+requestId+",residentQueue:"+residentQueue+") - "+(loaded? "SUCCESS":"FAIL")+" - hasExpandedFields:"+hasExpandedFields+",expndInclFilename:"+expndInclFilename);
    return loaded;
  }
  
  public String getFldVal(String dataMemberNme)
  {
    if(dataMemberNme==null || dataMemberNme.length()<1)
      return "";
    
    String accessor = "get" + dataMemberNme.substring(0,1).toUpperCase() + dataMemberNme.substring(1,dataMemberNme.length());
    //log.debug("getFldVal() accessor = "+accessor);
    
    Method method;
    Object obj;
    
    // seek the accessor in the stage info first
    obj = stageInfo;
    try {
      method=MmsStageSetupBean.class.getDeclaredMethod(accessor,null);
    }
    catch(Exception e) {
      method=null;
    }
    
    // seek the accessor in the extended fields bean
    if(method == null) {
      obj = expndInfo;
      try {
        method=MmsExpandedFieldsBean.class.getDeclaredMethod(accessor,null);
      }
      catch(Exception e) {
        method=null;
      }
    }
    
    if(method==null)
      return "";
    
    //log.debug("getFldVal() got the method object!");

    String val;
    
    try {
      val = method.invoke(obj,null).toString();
    }
    catch(Exception e) {
      val="";
    }
    
    return getDisplayString(val);
  }

  private final String getDisplayString(String v)
  {
    if(v!=null)
      v=v.trim();
    
    return (v==null || v.length()<1)? "---":v;
  }

  public int getProfileGeneratorCode()
  {
    return stageInfo.getProfileGeneratorCode();
  }
  
  public String getProfileGeneratorName()
  {
    return stageInfo.getProfileGeneratorName();
  }
  
  public boolean hasExpandedInfo()
  {
    return hasExpandedFields;
  }
  
  public String getExpandedIncludeFilename()
  {
    return expndInclFilename;
  }
  
  public boolean hasProfGenSpecInfo()
  {
    return (stageInfo.getProfileGeneratorCode() > mesConstants.PROFGEN_MMS);
  }
  
  public long getRequestId()
  {
    return requestId;
  }
  
  public int getResidentQueue()
  {
    return residentQueue;
  }

  public long getAppSeqNum()
  {
    return this.appSeqNum;
  }
  
  public boolean isProfileCompleted()
  {
    return profileCompleted;
  }

  public String getProfileCompletedDate()
  {
    return getDisplayString(profCmpltdDate);
  }

  public String getVNumber()
  {
    return getDisplayString(vnumber);
  }

  public String getTid()
  {
    return getDisplayString(tid);
  }

  public int getProfGenCode()
  {
    return profGenCode;
  }

  public String getProfGenName()
  {
    return getDisplayString(profGenName);
  }

  public String getTermAppDescriptor()
  {
    if(!loaded)
      return "";

    return getDisplayString(stageInfo.getTermAppDescriptor());
  }

  
}/*@lineinfo:generated-code*/