/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/CreditReviewBean.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import javax.servlet.http.HttpServletRequest;

public class CreditReviewBean extends ExpandDataBean
{
  // data members
  private   boolean     resend      = false;
  
  public CreditReviewBean()
  {
  }
  
  public void getData(long primaryKey)
  {
  }
  
  public void setDefaults(String primaryKey)
  {
    try
    {
      setDefaults(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
    }
  }
  public void setDefaults(long primaryKey)
  {
  }
  
  public void submitData(HttpServletRequest req, long primaryKey)
  {
  }
  
  public boolean validate()
  {
    return (! hasErrors());
  }
  
  public void setResend(String resend)
  {
    this.resend = true;
  }
  
  public boolean mustResend()
  {
    return this.resend;
  }
}
