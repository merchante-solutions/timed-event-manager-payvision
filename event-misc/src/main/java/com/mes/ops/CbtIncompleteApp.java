/*@lineinfo:filename=CbtIncompleteApp*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/CbtIncompleteApp.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  CbtIncompleteApp

  Supports CB&T incomplete app review function screen.

  Change History:
     See VSS database

  Copyright (C) 2003 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.forms.ButtonField;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.queues.QueueTools;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class CbtIncompleteApp extends FieldBean
{
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      fields.add(new HiddenField("type"));
      fields.add(new HiddenField("itemType"));
      fields.add(new HiddenField("startType"));
      fields.add(new HiddenField("id"));
      fields.addAlias("id","appSeqNum");
      
      fields.add(new NumberField("sicCode",   "SIC",4,6,true,0));
      fields.add(new NumberField("assocNum",  "Association #",6,6,true,0));

      fields.add(new ButtonField("submit","Submit"));
      fields.add(new ButtonField("cancel","Cancel"));
    
      // set the fields to have formText style class
      fields.setHtmlExtra("class=\"formText\"");
      
      // alternate error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  /*
  ** protected void postHandleRequest(HttpServletRequest request)
  **
  ** Set startType if it's blank.
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    // copy type into startType if startType is not set
    if (fields.getField("startType").isBlank())
    {
      fields.setData("startType",fields.getData("type"));
    }
  }

  /*
  ** private void updateMerchCreditStatus(long appSeqNum, int newStatus)
  **
  ** Updates the merch_credit_status field in merchant to a new status.
  */
  private void updateMerchCreditStatus(long appSeqNum, int newStatus)
  {
    try
    {
      connect();
      
      // determine if the status should be modified
      // (not an mes production status yet)
      int curStatus = -1;
      /*@lineinfo:generated-code*//*@lineinfo:104^7*/

//  ************************************************************
//  #sql [Ctx] { select  merch_credit_status
//          
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_credit_status\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.CbtIncompleteApp",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   curStatus = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:110^7*/
      
      // if currently a cbt specific status, then update it
      if (curStatus >= QueueConstants.CBT_CREDIT_NEW &&
          curStatus <= QueueConstants.CBT_REVIEW_COMPLETE)
      {
        // update the merch_credit_status column in the merchant table
        /*@lineinfo:generated-code*//*@lineinfo:117^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_credit_status = :newStatus
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n          set     merch_credit_status =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.CbtIncompleteApp",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,newStatus);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:122^9*/
      }
    }
    catch (Exception e)
    {
      logEntry("updateMerchCreditStatus(" + appSeqNum + ", " + newStatus + ")",
        e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** public boolean autoSubmit()
  **
  ** RETURNS: true if submit ok, else false.
  */
  public boolean autoSubmit()
  {
    boolean submitOk = false;

    try
    {
      connect();
      
      int curQType    = fields.getField("type").asInteger();
      long appSeqNum  = fields.getField("appSeqNum").asInteger();
      int assocNum    = fields.getField("assocNum").asInteger();
      int sicCode     = fields.getField("sicCode").asInteger();
      
      // update database
      /*@lineinfo:generated-code*//*@lineinfo:155^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     asso_number       = :assocNum,
//                  app_sic_code      = :sicCode
//          where   app_seq_num       = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     asso_number       =  :1 ,\n                app_sic_code      =  :2 \n        where   app_seq_num       =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.CbtIncompleteApp",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,assocNum);
   __sJT_st.setInt(2,sicCode);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^7*/
      
      // determine whether to complete or cancel the app
      boolean isSubmit = !fields.getField("submit").isBlank();
      boolean isCancel = !fields.getField("cancel").isBlank();
      boolean isComplete = (assocNum > 0 && sicCode > 0);
      
      // if application data is complete then move
      // queue item into the complete queue if it 
      // hasn't been already
      if (((isSubmit && isComplete ) || isCancel) &&  
          curQType != MesQueues.Q_CBT_COMPLETE)
      {
        int newType   = (isSubmit ? 
                         MesQueues.Q_CBT_COMPLETE : 
                         MesQueues.Q_CBT_REVIEW_CANCELLED);
                         
        int newStatus = (isSubmit ? 
                         QueueConstants.CBT_REVIEW_COMPLETE : 
                         QueueConstants.CBT_REVIEW_CANCEL);
                        
        // move queue item from incomplete to complete
        QueueTools.moveQueueItem(appSeqNum,
                                 MesQueues.Q_CBT_INCOMPLETE,
                                 newType,
                                 user,
                                 null);

        // update merch credit status to show in complete queue
        updateMerchCreditStatus(appSeqNum,newStatus);

        // update the type id to be the complete queue type
        String newTypeString 
          = Integer.toString(newType);
        fields.setData("type",newTypeString);

        // add a queue type override flag to the request
        request.setAttribute("overrideType",newTypeString);
        
        // if this is submit
        if (isSubmit)
        {
          // load a user object with original app submitter
          long sourceUserId = 0L;
          /*@lineinfo:generated-code*//*@lineinfo:205^11*/

//  ************************************************************
//  #sql [Ctx] { select  app_user_id
//              
//              from    application
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_user_id\n             \n            from    application\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.CbtIncompleteApp",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sourceUserId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:211^11*/
          UserBean sourceUser = new UserBean();
          sourceUser.getData(sourceUserId);
        
          // does this fall into a cbt credit queue?
          InsertCreditQueue icq = new InsertCreditQueue();
          //if (icq.isCbtReviewQueuesUser(sourceUser))
          {
            //icq.addToCbtCreditQueue(appSeqNum,sourceUser);
          }
          // not cbt credit, place into mes credit queue
          //else
          {
            //icq.addToMesCreditQueue(appSeqNum,sourceUser);
          }
        }
      }
      else if (isCancel)
      {
        // move queue item from incomplete to complete
        QueueTools.moveQueueItem(appSeqNum,
                                 MesQueues.Q_CBT_INCOMPLETE,
                                 MesQueues.Q_CBT_REVIEW_CANCELLED,
                                 user,
                                 null);

        // update merch credit status to show in complete queue
        updateMerchCreditStatus(appSeqNum,QueueConstants.CBT_REVIEW_CANCEL);

        // update the type id to be the complete queue type
        String newTypeString 
          = Integer.toString(MesQueues.Q_CBT_REVIEW_CANCELLED);
        fields.setData("type",newTypeString);

        // add a queue type override flag to the request
        request.setAttribute("overrideType",newTypeString);
      }

      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::autoSubmit(): " + e);
      logEntry("autoSubmit()",e.toString());
    }
    finally
    {
      cleanUp();
    }

    return submitOk;
  }

  /*
  ** public boolean autoLoad()
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean autoLoad()
  {
    ResultSetIterator it  = null;

    boolean loadOk = false;

    try
    {
      connect();
      
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      /*@lineinfo:generated-code*//*@lineinfo:281^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  asso_number     assoc_num,
//                  app_sic_code    sic_code
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  asso_number     assoc_num,\n                app_sic_code    sic_code\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.CbtIncompleteApp",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.CbtIncompleteApp",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:287^7*/
      setFields(it.getResultSet());

      loadOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::autoLoad(): " + e);
      logEntry("autoLoad()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }

    return loadOk;
  }
}/*@lineinfo:generated-code*/