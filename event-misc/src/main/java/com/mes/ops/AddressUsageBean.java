/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AddressUsageBean.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 3/12/04 3:00p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

public class AddressUsageBean extends ExpandDataBean
{

  private static final int BUSINESS_ADDRESS = mesConstants.ADDR_TYPE_BUSINESS;
  private static final int MAILING_ADDRESS  = mesConstants.ADDR_TYPE_MAILING;
  private static final int SHIPPING_ADDRESS = mesConstants.ADDR_TYPE_SHIPPING;

  private static final int ADDRESS_IND00    = 0;
  private static final int ADDRESS_IND01    = 1;
  private static final int ADDRESS_IND02    = 2;


  private   boolean   mes                   =   false;
  
  private   String    address00Name         =   "";
  private   String    address00Line1        =   "";
  private   String    address00Line2        =   "";
  private   String    address00City         =   "";
  private   String    address00State        =   "";
  private   String    address00Zip          =   "";
  private   boolean   address00Used         =   false;

  private   String    address01Name         =   "";
  private   String    address01Line1        =   "";
  private   String    address01Line2        =   "";
  private   String    address01City         =   "";
  private   String    address01State        =   "";
  private   String    address01Zip          =   "";
  private   boolean   address01Used         =   false;

  private   String    address02Name         =   "";
  private   String    address02Line1        =   "";
  private   String    address02Line2        =   "";
  private   String    address02City         =   "";
  private   String    address02State        =   "";
  private   String    address02Zip          =   "";
  private   boolean   address02Used         =   false;

  private   boolean   add00Dis              = false;
  private   boolean   add00Rcl              = false;
  private   boolean   add00Crb              = false;
  private   boolean   add00Mlr              = false;
  private   boolean   add00Ccd              = false;
  private   boolean   add00Imp              = false;
  private   boolean   add00Mem              = false;
  private   boolean   add00Pos              = false;
  private   boolean   add00Mis              = false;
  private   boolean   add00Nsg              = false;
  private   boolean   add00Ajf              = false;
  private   boolean   add00Cbf              = false;
  private   boolean   add00Bt1              = false;
  private   boolean   add00Bt2              = false;
  private   boolean   add00Bt3              = false;

  private   boolean   add01Dis              = false;
  private   boolean   add01Rcl              = false;
  private   boolean   add01Crb              = false;
  private   boolean   add01Mlr              = false;
  private   boolean   add01Ccd              = false;
  private   boolean   add01Imp              = false;
  private   boolean   add01Mem              = false;
  private   boolean   add01Pos              = false;
  private   boolean   add01Mis              = false;
  private   boolean   add01Nsg              = false;
  private   boolean   add01Ajf              = false;
  private   boolean   add01Cbf              = false;
  private   boolean   add01Bt1              = false;
  private   boolean   add01Bt2              = false;
  private   boolean   add01Bt3              = false;

  private   boolean   add02Dis              = false;
  private   boolean   add02Rcl              = false;
  private   boolean   add02Crb              = false;
  private   boolean   add02Mlr              = false;
  private   boolean   add02Ccd              = false;
  private   boolean   add02Imp              = false;
  private   boolean   add02Mem              = false;
  private   boolean   add02Pos              = false;
  private   boolean   add02Mis              = false;
  private   boolean   add02Nsg              = false;
  private   boolean   add02Ajf              = false;
  private   boolean   add02Cbf              = false;
  private   boolean   add02Bt1              = false;
  private   boolean   add02Bt2              = false;
  private   boolean   add02Bt3              = false;

  public    Vector    stateCode             = new Vector();
  public    Vector    stateDesc             = new Vector();

  public AddressUsageBean()
  {

    PreparedStatement ps          =   null;
    ResultSet         rs          =   null;

    try 
    {
      ps = getPreparedStatement("select * from countrystate");

      rs = ps.executeQuery();

      while(rs.next())
      {
        stateCode.add(rs.getString("countrystate_code"));
        stateDesc.add(rs.getString("countrystate_desc"));
      }

      rs.close();
      ps.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "constructor: " + e.toString());
      addError("constructor: " + e.toString());
    }

  }
  
  public void getData(long primaryKey)
  {
    getUsages(primaryKey);
    getAddresses(primaryKey);
  }

  public void getUsages(long primaryKey)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try 
    {
      ps = getPreparedStatement("select * from ADDRESS_INDICATOR_USAGES where app_seq_num = ? ");
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      if(rs.next())
      {
        if(!isBlank(rs.getString("IND_DIS")))
        {
          if(rs.getString("IND_DIS").equals("00"))
          {
            this.add00Dis = true;
          }
          else if(rs.getString("IND_DIS").equals("01"))
          {
            this.add01Dis = true;
          }
          else if(rs.getString("IND_DIS").equals("02"))
          {
            this.add02Dis = true;
          }
        }
        if(!isBlank(rs.getString("IND_RCL")))
        {
          if(rs.getString("IND_RCL").equals("00"))
          {
            this.add00Rcl = true;
          }
          else if(rs.getString("IND_RCL").equals("01"))
          {
            this.add01Rcl = true;
          }
          else if(rs.getString("IND_RCL").equals("02"))
          {
            this.add02Rcl = true;
          }
        }
        if(!isBlank(rs.getString("IND_CRB")))
        {
          if(rs.getString("IND_CRB").equals("00"))
          {
            this.add00Crb = true;
          }
          else if(rs.getString("IND_CRB").equals("01"))
          {
            this.add01Crb = true;
          }
          else if(rs.getString("IND_CRB").equals("02"))
          {
            this.add02Crb = true;
          }
        }
        if(!isBlank(rs.getString("IND_MLR")))
        {
          if(rs.getString("IND_MLR").equals("00"))
          {
            this.add00Mlr = true;
          }
          else if(rs.getString("IND_MLR").equals("01"))
          {
            this.add01Mlr = true;
          }
          else if(rs.getString("IND_MLR").equals("02"))
          {
            this.add02Mlr = true;
          }
        }
        if(!isBlank(rs.getString("IND_CCD")))
        {
          if(rs.getString("IND_CCD").equals("00"))
          {
            this.add00Ccd = true;
          }
          else if(rs.getString("IND_CCD").equals("01"))
          {
            this.add01Ccd = true;
          }
          else if(rs.getString("IND_CCD").equals("02"))
          {
            this.add02Ccd = true;
          }
        }
        if(!isBlank(rs.getString("IND_IMP")))
        {
          if(rs.getString("IND_IMP").equals("00"))
          {
            this.add00Imp = true;
          }
          else if(rs.getString("IND_IMP").equals("01"))
          {
            this.add01Imp = true;
          }
          else if(rs.getString("IND_IMP").equals("02"))
          {
            this.add02Imp = true;
          }
        }
        if(!isBlank(rs.getString("IND_MEM")))
        {
          if(rs.getString("IND_MEM").equals("00"))
          {
            this.add00Mem = true;
          }
          else if(rs.getString("IND_MEM").equals("01"))
          {
            this.add01Mem = true;
          }
          else if(rs.getString("IND_MEM").equals("02"))
          {
            this.add02Mem = true;
          }
        }
        if(!isBlank(rs.getString("IND_POS")))
        {
          if(rs.getString("IND_POS").equals("00"))
          {
            this.add00Pos = true;
          }
          else if(rs.getString("IND_POS").equals("01"))
          {
            this.add01Pos = true;
          }
          else if(rs.getString("IND_POS").equals("02"))
          {
            this.add02Pos = true;
          }
        }
        if(!isBlank(rs.getString("IND_MIS")))
        {
          if(rs.getString("IND_MIS").equals("00"))
          {
            this.add00Mis = true;
          }
          else if(rs.getString("IND_MIS").equals("01"))
          {
            this.add01Mis = true;
          }
          else if(rs.getString("IND_MIS").equals("02"))
          {
            this.add02Mis = true;
          }
        }
        if(!isBlank(rs.getString("IND_NSG")))
        {
          if(rs.getString("IND_NSG").equals("00"))
          {
            this.add00Nsg = true;
          }
          else if(rs.getString("IND_NSG").equals("01"))
          {
            this.add01Nsg = true;
          }
          else if(rs.getString("IND_NSG").equals("02"))
          {
            this.add02Nsg = true;
          }
        }
        if(!isBlank(rs.getString("IND_AJF")))
        {
          if(rs.getString("IND_AJF").equals("00"))
          {
            this.add00Ajf = true;
          }
          else if(rs.getString("IND_AJF").equals("01"))
          {
            this.add01Ajf = true;
          }
          else if(rs.getString("IND_AJF").equals("02"))
          {
            this.add02Ajf = true;
          }
        }
        if(!isBlank(rs.getString("IND_CBF")))
        {
          if(rs.getString("IND_CBF").equals("00"))
          {
            this.add00Cbf = true;
          }
          else if(rs.getString("IND_CBF").equals("01"))
          {
            this.add01Cbf = true;
          }
          else if(rs.getString("IND_CBF").equals("02"))
          {
            this.add02Cbf = true;
          }
        }
        if(!isBlank(rs.getString("IND_BT1")))
        {
          if(rs.getString("IND_BT1").equals("00"))
          {
            this.add00Bt1 = true;
          }
          else if(rs.getString("IND_BT1").equals("01"))
          {
            this.add01Bt1 = true;
          }
          else if(rs.getString("IND_BT1").equals("02"))
          {
            this.add02Bt1 = true;
          }
        }
        if(!isBlank(rs.getString("IND_BT2")))
        {
          if(rs.getString("IND_BT2").equals("00"))
          {
            this.add00Bt2 = true;
          }
          else if(rs.getString("IND_BT2").equals("01"))
          {
            this.add01Bt2 = true;
          }
          else if(rs.getString("IND_BT2").equals("02"))
          {
            this.add02Bt2 = true;
          }
        }
        if(!isBlank(rs.getString("IND_BT3")))
        {
          if(rs.getString("IND_BT3").equals("00"))
          {
            this.add00Bt3 = true;
          }
          else if(rs.getString("IND_BT3").equals("01"))
          {
            this.add01Bt3 = true;
          }
          else if(rs.getString("IND_BT3").equals("02"))
          {
            this.add02Bt3 = true;
          }
        }
      }
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getUsages: " + e.toString());
      addError("getUsages: " + e.toString());
    }
  }


  public String getAddressName(long primaryKey, int addInd)  
  {

    PreparedStatement ps          =   null;
    ResultSet         rs          =   null;
    int               i           =   0;
    String            result      =   "";
    String            query       =   "";

    try 
    {
      switch(addInd)
      {
        case MAILING_ADDRESS:
          query = "select merch_mailing_name from merchant where app_seq_num = ?";
        break;
        
        case BUSINESS_ADDRESS:
          query = "select merch_business_name from merchant where app_seq_num = ?";        
        break;
      }

      ps = getPreparedStatement(query);
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();

      if(rs.next())
      {
        result = isBlank(rs.getString(1))   ? "" : rs.getString(1);
      }

      rs.close();
      ps.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAddressName: " + e.toString());
      addError("getAddressName: " + e.toString());
    }

    return result;
  }


  private void getAddresses(long primaryKey)
  {

    PreparedStatement ps            = null;
    ResultSet         rs            = null;
    boolean           addressExist  = false;

    try 
    {
      ps = getPreparedStatement("select * from app_setup_address where app_seq_num = ? ");
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();

      while(rs.next())
      {
      
        switch(rs.getInt("address_ind"))
        {
          case ADDRESS_IND00:
            addressExist        =   true;
            address00Name       =   isBlank(rs.getString("ADDRESS_NAME"))     ? "" : rs.getString("ADDRESS_NAME");
            address00Line1      =   isBlank(rs.getString("address_line1"))    ? "" : rs.getString("address_line1");
            address00Line2      =   isBlank(rs.getString("address_line2"))    ? "" : rs.getString("address_line2");
            address00City       =   isBlank(rs.getString("address_city"))     ? "" : rs.getString("address_city");
            address00State      =   isBlank(rs.getString("ADDRESS_STATE"))    ? "" : rs.getString("ADDRESS_STATE");
            address00Zip        =   isBlank(rs.getString("address_zip"))      ? "" : rs.getString("address_zip");
          break;

          case ADDRESS_IND01:
            addressExist        =   true;
            address01Name       =   isBlank(rs.getString("ADDRESS_NAME"))     ? "" : rs.getString("ADDRESS_NAME");
            address01Line1      =   isBlank(rs.getString("address_line1"))    ? "" : rs.getString("address_line1");
            address01Line2      =   isBlank(rs.getString("address_line2"))    ? "" : rs.getString("address_line2");
            address01City       =   isBlank(rs.getString("address_city"))     ? "" : rs.getString("address_city");
            address01State      =   isBlank(rs.getString("ADDRESS_STATE"))    ? "" : rs.getString("ADDRESS_STATE");
            address01Zip        =   isBlank(rs.getString("address_zip"))      ? "" : rs.getString("address_zip");
          break;

          case ADDRESS_IND02:
            addressExist        =   true;
            address02Name       =   isBlank(rs.getString("address_name"))     ? "" : rs.getString("address_name");
            address02Line1      =   isBlank(rs.getString("address_line1"))    ? "" : rs.getString("address_line1");
            address02Line2      =   isBlank(rs.getString("address_line2"))    ? "" : rs.getString("address_line2");
            address02City       =   isBlank(rs.getString("address_city"))     ? "" : rs.getString("address_city");
            address02State      =   isBlank(rs.getString("ADDRESS_STATE"))    ? "" : rs.getString("ADDRESS_STATE");
            address02Zip        =   isBlank(rs.getString("address_zip"))      ? "" : rs.getString("address_zip");
          break;
        }
      
      }
      rs.close();
      ps.close();
      
      if(!addressExist)
      {
        getAppAddresses(primaryKey);
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAddresses: " + e.toString());
      addError("getAddresses: " + e.toString());
    }
  }

  /*
    address indicator legend
    mes:
    00 business physical address
    01 business mailing address
    02 business shipping address

    cbt:
    00 business mailing address
    02 business physical address
  */
  
  private void getAppAddresses(long primaryKey)
  {

    PreparedStatement ps      = null;
    ResultSet         rs      = null;

    try 
    {
      ps = getPreparedStatement("select * from address where app_seq_num = ? and addresstype_code in (?,?,?) order by addresstype_code");
      ps.setLong(1, primaryKey);
      ps.setInt(2,  BUSINESS_ADDRESS);
      ps.setInt(3,  MAILING_ADDRESS);
      ps.setInt(4,  SHIPPING_ADDRESS);

      rs = ps.executeQuery();
      while(rs.next())
      {
        switch(rs.getInt("addresstype_code"))
        {

          case BUSINESS_ADDRESS:
            if(isMes())
            {
              address00Name       =   getAddressName(primaryKey, BUSINESS_ADDRESS);
              address00Line1      =   isBlank(rs.getString("address_line1"))     ? "" : rs.getString("address_line1");
              address00Line2      =   isBlank(rs.getString("address_line2"))     ? "" : rs.getString("address_line2");
              address00City       =   isBlank(rs.getString("address_city"))      ? "" : rs.getString("address_city");
              address00State      =   isBlank(rs.getString("countrystate_code")) ? "" : rs.getString("countrystate_code");
              address00Zip        =   isBlank(rs.getString("address_zip"))       ? "" : rs.getString("address_zip");
            }
            else
            {
              address02Name       =   getAddressName(primaryKey, BUSINESS_ADDRESS);
              address02Line1      =   isBlank(rs.getString("address_line1"))     ? "" : rs.getString("address_line1");
              address02Line2      =   isBlank(rs.getString("address_line2"))     ? "" : rs.getString("address_line2");
              address02City       =   isBlank(rs.getString("address_city"))      ? "" : rs.getString("address_city");
              address02State      =   isBlank(rs.getString("countrystate_code")) ? "" : rs.getString("countrystate_code");
              address02Zip        =   isBlank(rs.getString("address_zip"))       ? "" : rs.getString("address_zip");

              //default business address in mailing address indicator.. 
              //if mailing address does exist this will get over written anyway
              address00Name       =   address02Name;
              address00Line1      =   address02Line1;
              address00Line2      =   address02Line2;
              address00City       =   address02City;
              address00State      =   address02State;
              address00Zip        =   address02Zip;
            }
          break;

          case MAILING_ADDRESS:
            if(isMes())
            {
              address01Name       =   getAddressName(primaryKey, MAILING_ADDRESS);
              address01Line1      =   isBlank(rs.getString("address_line1"))     ? "" : rs.getString("address_line1");
              address01Line2      =   isBlank(rs.getString("address_line2"))     ? "" : rs.getString("address_line2");
              address01City       =   isBlank(rs.getString("address_city"))      ? "" : rs.getString("address_city");
              address01State      =   isBlank(rs.getString("countrystate_code")) ? "" : rs.getString("countrystate_code");
              address01Zip        =   isBlank(rs.getString("address_zip"))       ? "" : rs.getString("address_zip");
            }
            else
            {
              address00Name       =   getAddressName(primaryKey, MAILING_ADDRESS);
              address00Line1      =   isBlank(rs.getString("address_line1"))     ? "" : rs.getString("address_line1");
              address00Line2      =   isBlank(rs.getString("address_line2"))     ? "" : rs.getString("address_line2");
              address00City       =   isBlank(rs.getString("address_city"))      ? "" : rs.getString("address_city");
              address00State      =   isBlank(rs.getString("countrystate_code")) ? "" : rs.getString("countrystate_code");
              address00Zip        =   isBlank(rs.getString("address_zip"))       ? "" : rs.getString("address_zip");
            }
          break;

          case SHIPPING_ADDRESS:
            if(isMes())
            {
              address02Name       =   isBlank(rs.getString("address_name"))      ? "" : rs.getString("address_name");
              address02Line1      =   isBlank(rs.getString("address_line1"))     ? "" : rs.getString("address_line1");
              address02Line2      =   isBlank(rs.getString("address_line2"))     ? "" : rs.getString("address_line2");
              address02City       =   isBlank(rs.getString("address_city"))      ? "" : rs.getString("address_city");
              address02State      =   isBlank(rs.getString("countrystate_code")) ? "" : rs.getString("countrystate_code");
              address02Zip        =   isBlank(rs.getString("address_zip"))       ? "" : rs.getString("address_zip");
            }
          break;
        }
      }
      
      setFlagDefaults();

      rs.close();
      ps.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getAppAddresses: " + e.toString());
      addError("getAppAddresses: " + e.toString());
    }
  }

  private void setFlagDefaults()
  {

    if(isMes())
    {
      //there is always a business address so this should always be set to true
      add00Rcl = true;
      
      //if a mailing address exists set 01 indicators
      if(!isBlank(address01Name))
      {
        add01Dis              = true;
        add01Crb              = true;
        add01Mlr              = true;
        add01Ccd              = true;
        add01Imp              = true;
        add01Mem              = true;
        add01Pos              = true;
        add01Mis              = true;
        add01Nsg              = true;
        add01Ajf              = true;
        add01Cbf              = true;
        add01Bt1              = true;
        add01Bt2              = true;
        add01Bt3              = true;
      }
      else //set all of 00 indicator to true
      {
        add00Dis              = true;
        add00Crb              = true;
        add00Mlr              = true;
        add00Ccd              = true;
        add00Imp              = true;
        add00Mem              = true;
        add00Pos              = true;
        add00Mis              = true;
        add00Nsg              = true;
        add00Ajf              = true;
        add00Cbf              = true;
        add00Bt1              = true;
        add00Bt2              = true;
        add00Bt3              = true;
      }

      if(!isBlank(address02Name))
      {
        add00Crb              = false;
        add01Crb              = false;
        add02Crb              = true;
      }

    }
    else
    {
      add00Rcl                = true;
      add00Dis                = true;
      add00Crb                = true;
      add00Mlr                = true;
      add00Ccd                = true;
      add00Imp                = true;
      add00Mem                = true;
      add00Pos                = true;
      add00Mis                = true;
      add00Nsg                = true;
      add00Ajf                = true;
      add00Cbf                = true;
      add00Bt1                = true;
      add02Bt2                = true;
      add00Bt3                = true;
    }

  }

  public void setPageDefaults(String primaryKey)
  {
    try
    {
      setPageDefaults(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
    }
  }
  public void setPageDefaults(long primaryKey)  
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try 
    {
      ps = getPreparedStatement("select app_type from application where app_seq_num = ?");
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
      if(rs.next())
      {
        switch(rs.getInt("app_type"))
        {
          case mesConstants.APP_TYPE_CBT:
          case mesConstants.APP_TYPE_CBT_NEW:
            this.mes = false;
          break;
          default:
            this.mes = true;
          break;
        }
      }
      ps.close();
      rs.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setPageDefaults: " + e.toString());
      addError("setPageDefaults: " + e.toString());
    }
  }
  
  public void submitData(HttpServletRequest req, long primaryKey)
  {
    submitUsageData(primaryKey);
    submitAddressData(primaryKey);
  }


  public void submitAddressData(long primaryKey)
  {

    StringBuffer      qs      = new StringBuffer();
    PreparedStatement ps      = null;

    try 
    {

      ps = getPreparedStatement("delete from APP_SETUP_ADDRESS where app_seq_num = ? ");
      ps.setLong(1, primaryKey);
      
      ps.executeUpdate();
  
      qs.append("insert into APP_SETUP_ADDRESS (");
      qs.append("address_name,    ");
      qs.append("address_line1,   ");
      qs.append("address_line2,   ");
      qs.append("address_city,    ");
      qs.append("address_state,   ");
      qs.append("address_zip,     ");
      qs.append("address_ind,     ");
      qs.append("app_seq_num) values (?,?,?,?,?,?,?,?) ");
    
      ps = getPreparedStatement(qs.toString());

      if(address00Used)
      {
        ps.setString(1, address00Name);
        ps.setString(2, address00Line1);
        ps.setString(3, address00Line2);
        ps.setString(4, address00City);
        ps.setString(5, address00State);
        ps.setString(6, address00Zip);

        ps.setInt(7,    ADDRESS_IND00);
        ps.setLong(8,   primaryKey);

        ps.executeUpdate();
      }

      if(address01Used)
      {
        ps.setString(1, address01Name);
        ps.setString(2, address01Line1);
        ps.setString(3, address01Line2);
        ps.setString(4, address01City);
        ps.setString(5, address01State);
        ps.setString(6, address01Zip);

        ps.setInt(7,    ADDRESS_IND01);
        ps.setLong(8,   primaryKey);

        ps.executeUpdate();
      }

      if(address02Used)
      {
        ps.setString(1, address02Name);
        ps.setString(2, address02Line1);
        ps.setString(3, address02Line2);
        ps.setString(4, address02City);
        ps.setString(5, address02State);
        ps.setString(6, address02Zip);

        ps.setInt(7,    ADDRESS_IND02);
        ps.setLong(8,   primaryKey);

        ps.executeUpdate();
      }

      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitAddressData: " + e.toString());
      addError("submitAddressData: " + e.toString());
    }
  }

  
  public void submitUsageData(long primaryKey)
  {

    StringBuffer      qs      = new StringBuffer();
    PreparedStatement ps      = null;

    try 
    {

      ps = getPreparedStatement("delete from ADDRESS_INDICATOR_USAGES where app_seq_num = ? ");
      ps.setLong(1, primaryKey);
      
      ps.executeUpdate();
  
      qs.append("insert into ADDRESS_INDICATOR_USAGES (");
      qs.append("IND_DIS, ");
      qs.append("IND_RCL, ");
      qs.append("IND_CRB, ");
      qs.append("IND_MLR, ");
      qs.append("IND_CCD, ");
      qs.append("IND_IMP, ");
      qs.append("IND_MEM, ");
      qs.append("IND_POS, ");
      qs.append("IND_MIS, ");
      qs.append("IND_NSG, ");
      qs.append("IND_AJF, ");
      qs.append("IND_CBF, ");
      qs.append("IND_BT1, ");
      qs.append("IND_BT2, ");
      qs.append("IND_BT3, ");
      qs.append("app_seq_num) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
    
      ps = getPreparedStatement(qs.toString());

      if(this.add00Dis)
      {
        ps.setString(1,"00");
      }
      else if(this.add01Dis)
      {
        ps.setString(1,"01");
      }
      else if(this.add02Dis)
      {
        ps.setString(1,"02");
      }

      if(this.add00Rcl)
      {
        ps.setString(2,"00");
      }
      else if(this.add01Rcl)
      {
        ps.setString(2,"01");
      }
      else if(this.add02Rcl)
      {
        ps.setString(2,"02");
      }

      if(this.add00Crb)
      {
        ps.setString(3,"00");
      }
      else if(this.add01Crb)
      {
        ps.setString(3,"01");
      }
      else if(this.add02Crb)
      {
        ps.setString(3,"02");
      }

      if(this.add00Mlr)
      {
        ps.setString(4,"00");
      }
      else if(this.add01Mlr)
      {
        ps.setString(4,"01");
      }
      else if(this.add02Mlr)
      {
        ps.setString(4,"02");
      }

      if(this.add00Ccd)
      {
        ps.setString(5,"00");
      }
      else if(this.add01Ccd)
      {
        ps.setString(5,"01");
      }
      else if(this.add02Ccd)
      {
        ps.setString(5,"02");
      }

      if(this.add00Imp)
      {
        ps.setString(6,"00");
      }
      else if(this.add01Imp)
      {
        ps.setString(6,"01");
      }
      else if(this.add02Imp)
      {
        ps.setString(6,"02");
      }

      if(this.add00Mem)
      {
        ps.setString(7,"00");
      }
      else if(this.add01Mem)
      {
        ps.setString(7,"01");
      }
      else if(this.add02Mem)
      {
        ps.setString(7,"02");
      }

      if(this.add00Pos)
      {
        ps.setString(8,"00");
      }
      else if(this.add01Pos)
      {
        ps.setString(8,"01");
      }
      else if(this.add02Pos)
      {
        ps.setString(8,"02");
      }

      if(this.add00Mis)
      {
        ps.setString(9,"00");
      }
      else if(this.add01Mis)
      {
        ps.setString(9,"01");
      }
      else if(this.add02Mis)
      {
        ps.setString(9,"02");
      }

      if(this.add00Nsg)
      {
        ps.setString(10,"00");
      }
      else if(this.add01Nsg)
      {
        ps.setString(10,"01");
      }
      else if(this.add02Nsg)
      {
        ps.setString(10,"02");
      }

      if(this.add00Ajf)
      {
        ps.setString(11,"00");
      }
      else if(this.add01Ajf)
      {
        ps.setString(11,"01");
      }
      else if(this.add02Ajf)
      {
        ps.setString(11,"02");
      }

      if(this.add00Cbf)
      {
        ps.setString(12,"00");
      }
      else if(this.add01Cbf)
      {
        ps.setString(12,"01");
      }
      else if(this.add02Cbf)
      {
        ps.setString(12,"02");
      }

      if(this.add00Bt1)
      {
        ps.setString(13,"00");
      }
      else if(this.add01Bt1)
      {
        ps.setString(13,"01");
      }
      else if(this.add02Bt1)
      {
        ps.setString(13,"02");
      }

      if(this.add00Bt2)
      {
        ps.setString(14,"00");
      }
      else if(this.add01Bt2)
      {
        ps.setString(14,"01");
      }
      else if(this.add02Bt2)
      {
        ps.setString(14,"02");
      }

      if(this.add00Bt3)
      {
        ps.setString(15,"00");
      }
      else if(this.add01Bt3)
      {
        ps.setString(15,"01");
      }
      else if(this.add02Bt3)
      {
        ps.setString(15,"02");
      }

      ps.setLong(16,primaryKey);

      ps.executeUpdate();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitUsageData: " + e.toString());
      addError("submitUsageData: " + e.toString());
    }
  }
  
  public boolean validate()
  {

    if(isBlank(address00Name)  || 
       isBlank(address00Line1) || 
       isBlank(address00City)  || 
       isBlank(address00State) || 
       isBlank(address00Zip))
    {
      addError("Address Indicator 00 can not have a blank name, address line 1, city, state, or zip");
    }
    else
    {
     address00Used = true;
    }
    
    if(!isBlank(address01Name)  &&
       !isBlank(address01Line1) &&
       !isBlank(address01City)  &&
       !isBlank(address01State) &&
       !isBlank(address01Zip))
    {
      address01Used = true;
    }
    else if(
       isBlank(address01Name)  &&
       isBlank(address01Line1) &&
       isBlank(address01City)  &&
       isBlank(address01State) &&
       isBlank(address01Zip))
    {
      address01Used = false;
    }
    else
    {
      addError("If you would like to use Address Indicator 01, please specify Name, Address Line 1, City, State, and Zip, or clear all fields");
    }
   
    if(!isBlank(address02Name)  &&
       !isBlank(address02Line1) &&
       !isBlank(address02City)  &&
       !isBlank(address02State) &&
       !isBlank(address02Zip))
    {
      address02Used = true;
    }
    else if(
       isBlank(address02Name)  &&
       isBlank(address02Line1) &&
       isBlank(address02City)  &&
       isBlank(address02State) &&
       isBlank(address02Zip))
    {
      address02Used = false;
    }
    else
    {
      addError("If you would like to use Address Indicator 02, please specify Name, Address Line 1, City, State, and Zip, or clear all fields");
    }


    if(address00Used && 
       !add00Dis     && 
       !add00Rcl     && 
       !add00Crb     && 
       !add00Mlr     && 
       !add00Ccd     && 
       !add00Imp     &&
       !add00Mem     &&
       !add00Pos     &&
       !add00Mis     &&
       !add00Nsg     &&
       !add00Ajf     &&
       !add00Cbf     &&
       !add00Bt1     &&
       !add00Bt2     &&
       !add00Bt3)
     {
       addError("If supplying An Address 00, you must assign atleast 1 usage code to this address.");
     }
     else if(!address00Used && 
       (add00Dis     || 
        add00Rcl     || 
        add00Crb     || 
        add00Mlr     || 
        add00Ccd     || 
        add00Imp     ||
        add00Mem     ||
        add00Pos     ||
        add00Mis     ||
        add00Nsg     ||
        add00Ajf     ||
        add00Cbf     ||
        add00Bt1     ||
        add00Bt2     ||
        add00Bt3))
    {
      addError("You can not select usage codes for Address Indicator 00, unless an Address 00 is provided");
    }

    if(address01Used && 
       !add01Dis     && 
       !add01Rcl     && 
       !add01Crb     && 
       !add01Mlr     && 
       !add01Ccd     && 
       !add01Imp     &&
       !add01Mem     &&
       !add01Pos     &&
       !add01Mis     &&
       !add01Nsg     &&
       !add01Ajf     &&
       !add01Cbf     &&
       !add01Bt1     &&
       !add01Bt2     &&
       !add01Bt3)
     {
       addError("If supplying An Address 01, you must assign atleast 1 usage code to this address.");
     }
     else if(!address01Used && 
       (add01Dis     || 
        add01Rcl     || 
        add01Crb     || 
        add01Mlr     || 
        add01Ccd     || 
        add01Imp     ||
        add01Mem     ||
        add01Pos     ||
        add01Mis     ||
        add01Nsg     ||
        add01Ajf     ||
        add01Cbf     ||
        add01Bt1     ||
        add01Bt2     ||
        add01Bt3))
    {
      addError("You can not select usage codes for Address Indicator 01, unless an Address 01 is provided");
    }


    if(address02Used && 
       !add02Dis     && 
       !add02Rcl     && 
       !add02Crb     && 
       !add02Mlr     && 
       !add02Ccd     && 
       !add02Imp     &&
       !add02Mem     &&
       !add02Pos     &&
       !add02Mis     &&
       !add02Nsg     &&
       !add02Ajf     &&
       !add02Cbf     &&
       !add02Bt1     &&
       !add02Bt2     &&
       !add02Bt3)
     {
       addError("If supplying An Address 02, you must assign atleast 1 usage code to this address.");
     }
     else if(!address02Used && 
       (add02Dis     || 
        add02Rcl     || 
        add02Crb     || 
        add02Mlr     || 
        add02Ccd     || 
        add02Imp     ||
        add02Mem     ||
        add02Pos     ||
        add02Mis     ||
        add02Nsg     ||
        add02Ajf     ||
        add02Cbf     ||
        add02Bt1     ||
        add02Bt2     ||
        add02Bt3))
    {
      addError("You can not select usage codes for Address Indicator 02, unless an Address 02 is provided");
    }


    if(!validZip(address00Zip))
    {
      addError("Address 00 Zip invalid");
    }
    
    if(!validZip(address01Zip))
    {
      addError("Address 01 Zip invalid");
    }

    if(!validZip(address02Zip))
    {
      addError("Address 02 Zip invalid");
    }


    if((add00Dis && add01Dis) || (add00Dis && add02Dis) || (add01Dis && add02Dis))
    {
      addError("DIS address indicator can only be assigned to one address.");
    }
    else if(!add00Dis && !add01Dis && !add02Dis)
    {
      addError("Please assign an address to DIS");
    }
     
    if((add00Rcl && add01Rcl) || (add00Rcl && add02Rcl) || (add01Rcl && add02Rcl))
    {
      addError("RCL address indicator can only be assigned to one address.");
    }
    else if(!add00Rcl && !add01Rcl && !add02Rcl)
    {
      addError("Please assign an address to RCL");
    }

    if((add00Crb && add01Crb) || (add00Crb && add02Crb) || (add01Crb && add02Crb))
    {
      addError("CRB address indicator can only be assigned to one address.");
    }
    else if(!add00Crb && !add01Crb && !add02Crb)
    {
      addError("Please assign an address to CRB");
    }

    if((add00Mlr && add01Mlr) || (add00Mlr && add02Mlr) || (add01Mlr && add02Mlr))
    {
      addError("MLR address indicator can only be assigned to one address.");
    }
    else if(!add00Mlr && !add01Mlr && !add02Mlr)
    {
      addError("Please assign an address to MLR");
    }

    if((add00Ccd && add01Ccd) || (add00Ccd && add02Ccd) || (add01Ccd && add02Ccd))
    {
      addError("CCD address indicator can only be assigned to one address.");
    }
    else if(!add00Ccd && !add01Ccd && !add02Ccd)
    {
      addError("Please assign an address to CCD");
    }

    if((add00Imp && add01Imp) || (add00Imp && add02Imp) || (add01Imp && add02Imp))
    {
      addError("IMP address indicator can only be assigned to one address.");
    }
    else if(!add00Imp && !add01Imp && !add02Imp)
    {
      addError("Please assign an address to IMP");
    }

    if((add00Mem && add01Mem) || (add00Mem && add02Mem) || (add01Mem && add02Mem))
    {
      addError("MEM address indicator can only be assigned to one address.");
    }
    else if(!add00Mem && !add01Mem && !add02Mem)
    {
      addError("Please assign an address to MEM");
    }

    if((add00Pos && add01Pos) || (add00Pos && add02Pos) || (add01Pos && add02Pos))
    {
      addError("POS address indicator can only be assigned to one address.");
    }
    else if(!add00Pos && !add01Pos && !add02Pos)
    {
      addError("Please assign an address to POS");
    }

    if((add00Mis && add01Mis) || (add00Mis && add02Mis) || (add01Mis && add02Mis))
    {
      addError("MIS address indicator can only be assigned to one address.");
    }
    else if(!add00Mis && !add01Mis && !add02Mis)
    {
      addError("Please assign an address to MIS");
    }

    if((add00Nsg && add01Nsg) || (add00Nsg && add02Nsg) || (add01Nsg && add02Nsg))
    {
      addError("NSG address indicator can only be assigned to one address.");
    }
    else if(!add00Nsg && !add01Nsg && !add02Nsg)
    {
      addError("Please assign an address to NSG");
    }

    if((add00Ajf && add01Ajf) || (add00Ajf && add02Ajf) || (add01Ajf && add02Ajf))
    {
      addError("AJF address indicator can only be assigned to one address.");
    }
    else if(!add00Ajf && !add01Ajf && !add02Ajf)
    {
      addError("Please assign an address to AJF");
    }

    if((add00Cbf && add01Cbf) || (add00Cbf && add02Cbf) || (add01Cbf && add02Cbf))
    {
      addError("CBF address indicator can only be assigned to one address.");
    }
    else if(!add00Cbf && !add01Cbf && !add02Cbf)
    {
      addError("Please assign an address to CBF");
    }

    if((add00Bt1 && add01Bt1) || (add00Bt1 && add02Bt1) || (add01Bt1 && add02Bt1))
    {
      addError("BT1 address indicator can only be assigned to one address.");
    }
    else if(!add00Bt1 && !add01Bt1 && !add02Bt1)
    {
      addError("Please assign an address to BT1");
    }

    if((add00Bt2 && add01Bt2) || (add00Bt2 && add02Bt2) || (add01Bt2 && add02Bt2))
    {
      addError("BT2 address indicator can only be assigned to one address.");
    }
    else if(!add00Bt2 && !add01Bt2 && !add02Bt2)
    {
      addError("Please assign an address to BT2");
    }

    if((add00Bt3 && add01Bt3) || (add00Bt3 && add02Bt3) || (add01Bt3 && add02Bt3))
    {
      addError("BT3 address indicator can only be assigned to one address.");
    }
    else if(!add00Bt3 && !add01Bt3 && !add02Bt3)
    {
      addError("Please assign an address to BT3");
    }

    return(! hasErrors());
  }

  private boolean validZip(String zip)
  {

    boolean result = false;
    String  zip1   = "";
    String  zip4   = "";

    if(!isBlank(zip))
    {  
      if(zip.length() == 10 && zip.charAt(5) == '-')
      {

        zip1    = zip.substring(0,5);
        zip4    = zip.substring(6);
        result  = isNumber((zip1 + zip4));

      }
      else if(zip.length() == 9 && isNumber(zip))
      {

        result = true;

      }
      else if(zip.length() == 5 && isNumber(zip))
      {
        result = true;        
      }
    }
    else
    {
      //if its blank we just let it pass....
      result = true;
    }

    return result;
  }

  public void setMes(String temp)
  {
    this.mes = true;
  }
  public boolean isMes()
  {
    return this.mes;
  }

  public String getAddress00Name()
  {
    return address00Name;
  }
  public String getAddress00Line1()
  {
    return address00Line1;
  }
  public String getAddress00Line2()
  {
    return address00Line2;
  }
  public String getAddress00City()
  {
    return address00City;
  }
  public String getAddress00State()
  {
    return address00State;
  }
  public String getAddress00Zip()
  {
    return address00Zip;
  }

  public String getAddress01Name()
  {
    return address01Name;
  }
  public String getAddress01Line1()
  {
    return address01Line1;
  }
  public String getAddress01Line2()
  {
    return address01Line2;
  }
  public String getAddress01City()
  {
    return address01City;
  }
  public String getAddress01State()
  {
    return address01State;
  }
  public String getAddress01Zip()
  {
    return address01Zip;
  }

  public String getAddress02Name()
  {
    return address02Name;
  }
  public String getAddress02Line1()
  {
    return address02Line1;
  }
  public String getAddress02Line2()
  {
    return address02Line2;
  }
  public String getAddress02City()
  {
    return address02City;
  }
  public String getAddress02State()
  {
    return address02State;
  }
  public String getAddress02Zip()
  {
    return address02Zip;
  }


  public void setAddress00Name(String address00Name)
  {
    this.address00Name = address00Name;
  }
  public void setAddress00Line1(String address00Line1)
  {
    this.address00Line1 = address00Line1;
  }
  public void setAddress00Line2(String address00Line2)
  {
    this.address00Line2 = address00Line2;
  }
  public void setAddress00City(String address00City)
  {
    this.address00City = address00City;
  }
  public void setAddress00State(String address00State)
  {
    this.address00State = address00State;
  }
  public void setAddress00Zip(String address00Zip)
  {
    this.address00Zip = address00Zip;
  }

  public void setAddress01Name(String address01Name)
  {
    this.address01Name = address01Name;
  }
  public void setAddress01Line1(String address01Line1)
  {
    this.address01Line1 = address01Line1;
  }
  public void setAddress01Line2(String address01Line2)
  {
    this.address01Line2 = address01Line2;
  }
  public void setAddress01City(String address01City)
  {
    this.address01City = address01City;
  }
  public void setAddress01State(String address01State)
  {
    this.address01State = address01State;
  }
  public void setAddress01Zip(String address01Zip)
  {
    this.address01Zip = address01Zip;
  }

  public void setAddress02Name(String address02Name)
  {
    this.address02Name = address02Name;
  }
  public void setAddress02Line1(String address02Line1)
  {
    this.address02Line1 = address02Line1;
  }
  public void setAddress02Line2(String address02Line2)
  {
    this.address02Line2 = address02Line2;
  }
  public void setAddress02City(String address02City)
  {
    this.address02City = address02City;
  }
  public void setAddress02State(String address02State)
  {
    this.address02State = address02State;
  }
  public void setAddress02Zip(String address02Zip)
  {
    this.address02Zip = address02Zip;
  }

  public void setAdd00Dis(String temp)
  {
    add00Dis = true;
  }
  public void setAdd00Rcl(String temp)
  {
    add00Rcl = true;
  }
  public void setAdd00Crb(String temp)
  {
    add00Crb = true;
  }
  public void setAdd00Mlr(String temp)
  {
    add00Mlr = true;
  }
  public void setAdd00Ccd(String temp)
  {
    add00Ccd = true;
  }
  public void setAdd00Imp(String temp)
  {
    add00Imp = true;
  }
  public void setAdd00Mem(String temp)
  {
    add00Mem = true;
  }
  public void setAdd00Pos(String temp)
  {
    add00Pos = true;
  }
  public void setAdd00Mis(String temp)
  {
    add00Mis = true;
  }
  public void setAdd00Nsg(String temp)
  {
    add00Nsg = true;
  }
  public void setAdd00Ajf(String temp)
  {
    add00Ajf = true;
  }
  public void setAdd00Cbf(String temp)
  {
    add00Cbf = true;
  }
  public void setAdd00Bt1(String temp)
  {
    add00Bt1 = true;
  }
  public void setAdd00Bt2(String temp)
  {
    add00Bt2 = true;
  }
  public void setAdd00Bt3(String temp)
  {
    add00Bt3 = true;
  }


  public void setAdd01Dis(String temp)
  {
    add01Dis = true;
  }
  public void setAdd01Rcl(String temp)
  {
    add01Rcl = true;
  }
  public void setAdd01Crb(String temp)
  {
    add01Crb = true;
  }
  public void setAdd01Mlr(String temp)
  {
    add01Mlr = true;
  }
  public void setAdd01Ccd(String temp)
  {
    add01Ccd = true;
  }
  public void setAdd01Imp(String temp)
  {
    add01Imp = true;
  }
  public void setAdd01Mem(String temp)
  {
    add01Mem = true;
  }
  public void setAdd01Pos(String temp)
  {
    add01Pos = true;
  }
  public void setAdd01Mis(String temp)
  {
    add01Mis = true;
  }
  public void setAdd01Nsg(String temp)
  {
    add01Nsg = true;
  }
  public void setAdd01Ajf(String temp)
  {
    add01Ajf = true;
  }
  public void setAdd01Cbf(String temp)
  {
    add01Cbf = true;
  }
  public void setAdd01Bt1(String temp)
  {
    add01Bt1 = true;
  }
  public void setAdd01Bt2(String temp)
  {
    add01Bt2 = true;
  }
  public void setAdd01Bt3(String temp)
  {
    add01Bt3 = true;
  }

  public void setAdd02Dis(String temp)
  {
    add02Dis = true;
  }
  public void setAdd02Rcl(String temp)
  {
    add02Rcl = true;
  }
  public void setAdd02Crb(String temp)
  {
    add02Crb = true;
  }
  public void setAdd02Mlr(String temp)
  {
    add02Mlr = true;
  }
  public void setAdd02Ccd(String temp)
  {
    add02Ccd = true;
  }
  public void setAdd02Imp(String temp)
  {
    add02Imp = true;
  }
  public void setAdd02Mem(String temp)
  {
    add02Mem = true;
  }
  public void setAdd02Pos(String temp)
  {
    add02Pos = true;
  }
  public void setAdd02Mis(String temp)
  {
    add02Mis = true;
  }
  public void setAdd02Nsg(String temp)
  {
    add02Nsg = true;
  }
  public void setAdd02Ajf(String temp)
  {
    add02Ajf = true;
  }
  public void setAdd02Cbf(String temp)
  {
    add02Cbf = true;
  }
  public void setAdd02Bt1(String temp)
  {
    add02Bt1 = true;
  }
  public void setAdd02Bt2(String temp)
  {
    add02Bt2 = true;
  }
  public void setAdd02Bt3(String temp)
  {
    add02Bt3 = true;
  }

  public boolean getAdd00Dis()
  {
    return this.add00Dis;
  }
  public boolean getAdd00Rcl()
  {
    return this.add00Rcl;
  }
  public boolean getAdd00Crb()
  {
    return this.add00Crb;
  }
  public boolean getAdd00Mlr()
  {
    return this.add00Mlr;
  }
  public boolean getAdd00Ccd()
  {
    return this.add00Ccd;
  }
  public boolean getAdd00Imp()
  {
    return this.add00Imp;
  }
  public boolean getAdd00Mem()
  {
    return this.add00Mem;
  }
  public boolean getAdd00Pos()
  {
    return this.add00Pos;
  }
  public boolean getAdd00Mis()
  {
    return this.add00Mis;
  }
  public boolean getAdd00Nsg()
  {
    return this.add00Nsg;
  }
  public boolean getAdd00Ajf()
  {
    return this.add00Ajf;
  }
  public boolean getAdd00Cbf()
  {
    return this.add00Cbf;
  }
  public boolean getAdd00Bt1()
  {
    return this.add00Bt1;
  }
  public boolean getAdd00Bt2()
  {
    return this.add00Bt2;
  }
  public boolean getAdd00Bt3()
  {
    return this.add00Bt3;
  }


  public boolean getAdd01Dis()
  {
    return this.add01Dis;
  }
  public boolean getAdd01Rcl()
  {
    return this.add01Rcl;
  }
  public boolean getAdd01Crb()
  {
    return this.add01Crb;
  }
  public boolean getAdd01Mlr()
  {
    return this.add01Mlr;
  }
  public boolean getAdd01Ccd()
  {
    return this.add01Ccd;
  }
  public boolean getAdd01Imp()
  {
    return this.add01Imp;
  }
  public boolean getAdd01Mem()
  {
    return this.add01Mem;
  }
  public boolean getAdd01Pos()
  {
    return this.add01Pos;
  }
  public boolean getAdd01Mis()
  {
    return this.add01Mis;
  }
  public boolean getAdd01Nsg()
  {
    return this.add01Nsg;
  }
  public boolean getAdd01Ajf()
  {
    return this.add01Ajf;
  }
  public boolean getAdd01Cbf()
  {
    return this.add01Cbf;
  }
  public boolean getAdd01Bt1()
  {
    return this.add01Bt1;
  }
  public boolean getAdd01Bt2()
  {
    return this.add01Bt2;
  }
  public boolean getAdd01Bt3()
  {
    return this.add01Bt3;
  }

  public boolean getAdd02Dis()
  {
    return this.add02Dis;
  }
  public boolean getAdd02Rcl()
  {
    return this.add02Rcl;
  }
  public boolean getAdd02Crb()
  {
    return this.add02Crb;
  }
  public boolean getAdd02Mlr()
  {
    return this.add02Mlr;
  }
  public boolean getAdd02Ccd()
  {
    return this.add02Ccd;
  }
  public boolean getAdd02Imp()
  {
    return this.add02Imp;
  }
  public boolean getAdd02Mem()
  {
    return this.add02Mem;
  }
  public boolean getAdd02Pos()
  {
    return this.add02Pos;
  }
  public boolean getAdd02Mis()
  {
    return this.add02Mis;
  }
  public boolean getAdd02Nsg()
  {
    return this.add02Nsg;
  }
  public boolean getAdd02Ajf()
  {
    return this.add02Ajf;
  }
  public boolean getAdd02Cbf()
  {
    return this.add02Cbf;
  }
  public boolean getAdd02Bt1()
  {
    return this.add02Bt1;
  }
  public boolean getAdd02Bt2()
  {
    return this.add02Bt2;
  }
  public boolean getAdd02Bt3()
  {
    return this.add02Bt3;
  }

}







