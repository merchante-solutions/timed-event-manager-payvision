/*@lineinfo:filename=CallTrackingReport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/CallTrackingReport.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 12/06/02 4:39p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class CallTrackingReport extends DateSQLJBean
{
  private Date fromSqlDate  = null;
  private Date toSqlDate    = null; 
  private long fromLongDate = -1L;
  private long toLongDate   = -1L; 

  private HashMap callTrackingStatHash  = new HashMap();
  private Vector  callTrackingList      = new Vector();

  public CallTrackingReport()
  {
    //initializes calendar to be current date/time with default schedule type (activation)
    super();
  }

  private void setDates()
  {
    //set the from date and too date.. 
    setFromSqlDate  ( getSqlFromDate()        );
    setToSqlDate    ( getSqlToDate()          );
    setFromLongDate ( getFromDate().getTime() );
    setToLongDate   ( getToDate().getTime()   );
  }

  //gets data to build report
  public void getData()
  {
    ResultSetIterator   it         = null;
    ResultSet           rs         = null;

    //clear hashmap and vector.. get fresh data
    callTrackingStatHash  = new HashMap();
    callTrackingList      = new Vector();

    setDates();

    try
    {
      connect();

      //number closed calls
      /*@lineinfo:generated-code*//*@lineinfo:87^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    login_name, 
//                    count(sequence) closed_calls
//          from      service_calls
//          where     trunc(call_date) between :fromSqlDate and :toSqlDate
//                    and  status = 2
//          group by  login_name
//          order by  login_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    login_name, \n                  count(sequence) closed_calls\n        from      service_calls\n        where     trunc(call_date) between  :1  and  :2 \n                  and  status = 2\n        group by  login_name\n        order by  login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.CallTrackingReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.CallTrackingReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:96^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs);
      }
      
      rs.close();
      it.close();
     
      //number open calls
      /*@lineinfo:generated-code*//*@lineinfo:109^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    login_name, 
//                    count(sequence) open_calls
//          from      service_calls
//          where     trunc(call_date) between :fromSqlDate and :toSqlDate
//                    and  status = 1
//          group by  login_name
//          order by  login_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    login_name, \n                  count(sequence) open_calls\n        from      service_calls\n        where     trunc(call_date) between  :1  and  :2 \n                  and  status = 1\n        group by  login_name\n        order by  login_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.CallTrackingReport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromSqlDate);
   __sJT_st.setDate(2,toSqlDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.CallTrackingReport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:118^7*/

      rs = it.getResultSet();
    
      while(rs.next())
      {
        getRecordData(rs);
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
      
  private void getRecordData(ResultSet rs) throws Exception
  {
    String loginName = rs.getString("login_name");

    //adds loginName to vector containing list of loginNames (each entry unique)
    addCallTracking(loginName);
  
    CallTrackingStats stats = null;

    //gets rec for login name.. gets old one if already exists or starts new one if it doesnt exist
    stats = getRecFromHash(loginName);

    try
    {
      stats.setNumOpenCalls(rs.getInt("open_calls"));
    }
    catch(Exception e)
    {}
    
    try
    {
      stats.setNumClosedCalls(rs.getInt("closed_calls"));
    }
    catch(Exception e)
    {}

    putRecInHash(stats, loginName);
    
    stats = null;
  }

  private void putRecInHash(CallTrackingStats stats, String loginName)
  {
    callTrackingStatHash.put(loginName, stats);
  }

  public CallTrackingStats getRecFromHash(String loginName)
  {
    if(callTrackingStatHash.containsKey(loginName))
    {
      return ((CallTrackingStats)callTrackingStatHash.get(loginName));
    }
    else
    {
      return (new CallTrackingStats(loginName));
    }
  }

  private void addCallTracking(String loginName)
  {
    if(!callTrackingList.contains(loginName))
    {
      callTrackingList.add(loginName);
    }
  }      
      
  public Vector getCallTrackingListVector()
  {
    return this.callTrackingList;
  }
 
  public void setFromSqlDate(Date  fromDate)
  {
    this.fromSqlDate = fromDate;
  }

  public void setToSqlDate(Date toDate)
  {
    this.toSqlDate = toDate;
  }

  public void setFromLongDate(long fromDate)
  {
    this.fromLongDate = fromDate;
  }
  public void setToLongDate(long toDate)
  {
    this.toLongDate = toDate;
  } 

  public class CallTrackingStats
  {
    private String loginName              = "";
    private int    numOpenCalls           = 0;
    private int    numClosedCalls         = 0;
  
    public CallTrackingStats(String loginName)
    {
      this.loginName = loginName;
    }

    public String getLoginName()
    {
      return this.loginName;
    }

    public int getNumOpenCalls()
    {
      return this.numOpenCalls;
    }

    public int getNumClosedCalls()
    {
      return this.numClosedCalls;
    }

    //setters
    public void setLoginName(String loginName)
    {
      this.loginName = loginName;
    }

    public void setNumOpenCalls(int numOpenCalls)
    {
      this.numOpenCalls = numOpenCalls;
    }

    public void setNumClosedCalls(int numClosedCalls)
    {
      this.numClosedCalls = numClosedCalls;
    }
  }
}/*@lineinfo:generated-code*/