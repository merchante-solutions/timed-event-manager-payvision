/*@lineinfo:filename=EquipmentSelectionNewBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/EquipmentSelectionNewBean.sqlj $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 7/23/04 4:48p $
  Version            : $Revision: 15 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class EquipmentSelectionNewBean extends InventoryBase
{
  private Vector  equipToDeploy         = new Vector();
  private int     needed                = 0;
  private String  name                  = "";
  private String  model                 = "";
  private String  application           = "";
  private String  desc                  = "";
  private String  lendCode              = "";
  private String  price                 = "";
  private String  merchantNum           = "";
  private String  serialNumber          = "";
  private boolean newSetup              = false;

  private boolean allowSelection        = true;
  public  Vector  descriptions          = new Vector();
  public  Vector  models                = new Vector();
  public  Vector  lendType              = new Vector();
  public  Vector  lendTypeCode          = new Vector();
  public  Vector  appType               = new Vector();
  public  Vector  appTypeCode           = new Vector();

  public EquipmentSelectionNewBean()
  {
    ResultSetIterator   it      = null;
    
    try
    {                                  
      boolean           firstpin           = true;
      boolean           firstterm          = true;
      boolean           firstperiph        = true;
      boolean           firstprinter       = true;
      boolean           firstimprinter     = true;
      boolean           firsttermprinter   = true;
      boolean           firsttermprintpin  = true;
      boolean           firstaccessory     = true;

      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:80^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model,
//                  equip_descriptor,
//                  equiptype_code
//          from    equipment
//          where   used_in_inventory = 'Y'
//          order by equiptype_code, equip_descriptor
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model,\n                equip_descriptor,\n                equiptype_code\n        from    equipment\n        where   used_in_inventory = 'Y'\n        order by equiptype_code, equip_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.EquipmentSelectionNewBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.EquipmentSelectionNewBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^7*/
      
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {

        switch(rs.getInt("equiptype_code"))
        {
          case mesConstants.APP_EQUIP_TYPE_TERMINAL:
            if(firstterm)
            {
              descriptions.add("****TERMINALS****");
              models.add("*");
              firstterm = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_PRINTER:
            if(firstprinter)
            {
              descriptions.add("****PRINTERS****");
              models.add("*");
              firstprinter = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_PINPAD:
            if(firstpin)
            {
              descriptions.add("****PINPADS****");
              models.add("*");
              firstpin = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_IMPRINTER:
            if(firstimprinter)
            {
              descriptions.add("****IMPRINTERS****");
              models.add("*");
              firstimprinter = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINTER:
            if(firsttermprinter)
            {
              descriptions.add("****TERMINAL/PRINTERS****");
              models.add("*");
              firsttermprinter = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_TERM_PRINT_PINPAD:
            if(firsttermprintpin)
            {
              descriptions.add("****TERMINAL/PRINTER/PINPADS****");
              models.add("*");
              firsttermprintpin = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_PERIPHERAL:
            if(firstperiph)
            {
              descriptions.add("****PERIPHERALS****");
              models.add("*");
              firstperiph = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
          break;
          case mesConstants.APP_EQUIP_TYPE_ACCESSORIES:
            if(firstaccessory)
            {
              descriptions.add("****ACCESSORIES****");
              models.add("*");
              firstaccessory = false;
            }
            descriptions.add(rs.getString("equip_descriptor"));
            models.add(rs.getString("equip_model"));
          break;
        }  
      }
      
      it.close();
   
      lendType.add("Purchase");
      lendTypeCode.add("1");
      lendType.add("Rent");
      lendTypeCode.add("2");
      lendType.add("Lease");
      lendTypeCode.add("5");
      lendType.add("Loan");
      lendTypeCode.add("7");
      lendType.add("PP Exchange");
      lendTypeCode.add("20");
      lendType.add("Merchant-Owned");
      lendTypeCode.add("26");
      lendType.add("Sold As Is");
      lendTypeCode.add("27");
    }
    catch(Exception e)
    {
      logEntry("constructor--filling grids()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public String getMerchType(String mercNum)
  {
    String            result  = "unknown";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:215^7*/

//  ************************************************************
//  #sql [Ctx] { select  merchant_type_desc 
//          
//          from    merchant_types
//          where   merchant_number = :mercNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merchant_type_desc \n         \n        from    merchant_types\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.EquipmentSelectionNewBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,mercNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:221^7*/
    }
    catch(Exception e)
    {
      logEntry("getMerchType()", e.toString());
      addError("getMerchType: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }

  public void findEquipment(String serialNum, String mercNum)
  {
    ResultSetIterator it      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:244^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  class.ec_class_name,
//                  equip.equip_descriptor,
//                  mfgr.equipmfgr_mfr_name,
//                  inv.*,
//                  at.inventory_owner_name
//          from    equipment       equip,
//                  equipmfgr       mfgr,
//                  equip_class     class,
//                  equip_inventory inv,
//                  app_type        at,
//                  merchant_types  mt
//          where   inv.ei_part_number = :this.model and
//                  inv.ei_serial_number = :serialNum and
//                  inv.ei_deployed_date is null and
//                  inv.ei_owner = mt.merchant_type and
//                  mt.merchant_number = :mercNum and
//                  inv.ei_part_number = equip.equip_model and
//                  class.ec_class_id = inv.ei_class and
//                  equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and
//                  inv.ei_owner = at.app_type_code
//          order by inv.ei_received_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  class.ec_class_name,\n                equip.equip_descriptor,\n                mfgr.equipmfgr_mfr_name,\n                inv.*,\n                at.inventory_owner_name\n        from    equipment       equip,\n                equipmfgr       mfgr,\n                equip_class     class,\n                equip_inventory inv,\n                app_type        at,\n                merchant_types  mt\n        where   inv.ei_part_number =  :1  and\n                inv.ei_serial_number =  :2  and\n                inv.ei_deployed_date is null and\n                inv.ei_owner = mt.merchant_type and\n                mt.merchant_number =  :3  and\n                inv.ei_part_number = equip.equip_model and\n                class.ec_class_id = inv.ei_class and\n                equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and\n                inv.ei_owner = at.app_type_code\n        order by inv.ei_received_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.EquipmentSelectionNewBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.model);
   __sJT_st.setString(2,serialNum);
   __sJT_st.setString(3,mercNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.EquipmentSelectionNewBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:267^7*/

      ResultSet rs = it.getResultSet();
    
      int i = 0;
      while(rs.next() && i < 12) //only get oldest 12...
      {
        EquipRec tempRec =  new EquipRec();
        tempRec.setSerialNumber(rs.getString("ei_serial_number"));
        tempRec.setProductName(rs.getString("equipmfgr_mfr_name"));
        tempRec.setProductDesc(rs.getString("equip_descriptor"));
        tempRec.setOwnerDesc(rs.getString("inventory_owner_name"));
        tempRec.setUnitCost(formatCurr(rs.getString("ei_unit_cost")));
        tempRec.setClassTypeDesc(rs.getString("ec_class_name"));
        tempRec.setReceiveDate(rs.getDate("ei_received_date"));
        equipToDeploy.add(tempRec);
        i++;
      }
      
      it.close();
      allowSelection = false;
    }
    catch(Exception e)
    {
      logEntry("findEquipment()", e.toString());
      addError("findEquipment: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void getEquipment(String mercNum)
  {
    ResultSetIterator   it      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:308^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  class.ec_class_name,
//                  equip.equip_descriptor,
//                  mfgr.equipmfgr_mfr_name,
//                  inv.*,
//                  at.inventory_owner_name
//          from    equipment equip, 
//                  equipmfgr mfgr, 
//                  equip_class class, 
//                  equip_inventory inv, 
//                  app_type at, 
//                  merchant_types mt
//          where   inv.ei_part_number = :this.model and
//                  inv.ei_deployed_date is null and
//                  inv.ei_owner = mt.merchant_type and
//                  mt.merchant_number = :mercNum and
//                  inv.ei_part_number = equip.equip_model and
//                  class.ec_class_id = inv.ei_class and
//                  equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and
//                  inv.ei_owner = at.app_type_code
//          order by inv.ei_received_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  class.ec_class_name,\n                equip.equip_descriptor,\n                mfgr.equipmfgr_mfr_name,\n                inv.*,\n                at.inventory_owner_name\n        from    equipment equip, \n                equipmfgr mfgr, \n                equip_class class, \n                equip_inventory inv, \n                app_type at, \n                merchant_types mt\n        where   inv.ei_part_number =  :1  and\n                inv.ei_deployed_date is null and\n                inv.ei_owner = mt.merchant_type and\n                mt.merchant_number =  :2  and\n                inv.ei_part_number = equip.equip_model and\n                class.ec_class_id = inv.ei_class and\n                equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code and\n                inv.ei_owner = at.app_type_code\n        order by inv.ei_received_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.EquipmentSelectionNewBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.model);
   __sJT_st.setString(2,mercNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.EquipmentSelectionNewBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:330^7*/

      ResultSet rs = it.getResultSet();

      int i = 0;
      while(rs.next() && i < 12) //only get oldest 12...
      {
        EquipRec tempRec =  new EquipRec();
        tempRec.setSerialNumber(rs.getString("ei_serial_number"));
        tempRec.setProductName(rs.getString("equipmfgr_mfr_name"));
        tempRec.setProductDesc(rs.getString("equip_descriptor"));
        tempRec.setOwnerDesc(rs.getString("inventory_owner_name"));
        tempRec.setUnitCost(formatCurr(rs.getString("ei_unit_cost")));
        tempRec.setClassTypeDesc(rs.getString("ec_class_name"));
        tempRec.setReceiveDate(rs.getDate("ei_received_date"));
        equipToDeploy.add(tempRec);
        i++;
      }

      it.close();
      allowSelection = false;
      if(isBlank(this.name) || isBlank(this.desc))
      {
        getNameAndDesc();
      }
    }
    catch(Exception e)
    {
      logEntry("getEquipment()", e.toString());
      addError("getEquipment: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void getNameAndDesc()
  {
    ResultSetIterator   it      = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:375^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip.equip_descriptor, 
//                  mfgr.equipmfgr_mfr_name
//          from    equipment equip, 
//                  equipmfgr mfgr
//          where   equip.equip_model = :this.model and
//                  equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip.equip_descriptor, \n                mfgr.equipmfgr_mfr_name\n        from    equipment equip, \n                equipmfgr mfgr\n        where   equip.equip_model =  :1  and\n                equip.equipmfgr_mfr_code = mfgr.equipmfgr_mfr_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.EquipmentSelectionNewBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.model);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.EquipmentSelectionNewBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:383^7*/

      ResultSet rs = it.getResultSet();

      if(rs.next())
      {
        this.name = rs.getString("equipmfgr_mfr_name");
        this.desc = rs.getString("equip_descriptor");
      }

      it.close();
    }
    catch(Exception e)
    {
      logEntry("getNameAndDesc()", e.toString());
      addError("getNameAndDesc: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }

  public int getNumNeeded()
  {
    return equipToDeploy.size();
  }

  public void submitEquipment(long userId)
  {
    try
    {
      connect();
      
      if(isBlank(this.serialNumber))
      {
        this.needed = 0;
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:438^9*/

//  ************************************************************
//  #sql [Ctx] { update  equip_inventory set
//                    ei_lrb               = :this.lendCode, 
//                    ei_status            = :this.lendCode, 
//                    ei_deployed_date     = sysdate,
//                    ei_merchant_number   = :this.merchantNum,
//                    ei_lrb_price         = :this.price 
//            where   ei_part_number = :this.model and 
//                    ei_serial_number = :this.serialNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  equip_inventory set\n                  ei_lrb               =  :1 , \n                  ei_status            =  :2 , \n                  ei_deployed_date     = sysdate,\n                  ei_merchant_number   =  :3 ,\n                  ei_lrb_price         =  :4  \n          where   ei_part_number =  :5  and \n                  ei_serial_number =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.ops.EquipmentSelectionNewBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.lendCode);
   __sJT_st.setString(2,this.lendCode);
   __sJT_st.setString(3,this.merchantNum);
   __sJT_st.setString(4,this.price);
   __sJT_st.setString(5,this.model);
   __sJT_st.setString(6,this.serialNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:448^9*/

        this.needed--;
        addToTracking();
        addToHistory(this.model, this.serialNumber, userId, Integer.parseInt(this.lendCode), ("Deploy-" + getLend() + " to merchant #: " + this.merchantNum) );
      }
    }
    catch(Exception e)
    {
      logEntry("submitMerchantData()", e.toString());
      addError("submitMerchantData: "+ e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void addToTracking()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:470^7*/

//  ************************************************************
//  #sql [Ctx] { insert into equip_merchant_tracking 
//          ( 
//            merchant_number, 
//            action_date, 
//            action, 
//            ref_num_serial_num, 
//            action_desc 
//          ) 
//          values
//          (
//            :this.merchantNum,
//            sysdate,
//            :this.newSetup ? mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP : mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL,
//            :this.serialNumber,
//            :this.lendCode
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_789 = this.newSetup ? mesConstants.ACTION_CODE_DEPLOYMENT_NEW_SETUP : mesConstants.ACTION_CODE_DEPLOYMENT_ADDITIONAL;
   String theSqlTS = "insert into equip_merchant_tracking \n        ( \n          merchant_number, \n          action_date, \n          action, \n          ref_num_serial_num, \n          action_desc \n        ) \n        values\n        (\n           :1 ,\n          sysdate,\n           :2 ,\n           :3 ,\n           :4 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.ops.EquipmentSelectionNewBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.merchantNum);
   __sJT_st.setInt(2,__sJT_789);
   __sJT_st.setString(3,this.serialNumber);
   __sJT_st.setString(4,this.lendCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:488^7*/
    }
    catch(Exception e)
    {
      logEntry("addToTracking()", e.toString());
      addError("addToTracking: " + e.toString());
    }
  }

  public boolean validate()
  {

    if(isBlank(this.model) || model.equals("*"))
    {
      addError("Please select the equipment you wish to deploy.");
    }
    if(isBlank(this.lendCode))
    {
      addError("Please select the lend type for this item.");
    }
    if(isBlank(this.price) || !isValidPrice(this.price))
    {
      addError("Please provide a valid lend price.");
    }
    if(this.needed <= 0)
    {
      addError("Please select the quantity needed.");
    }

    return(! hasErrors());
  }

  private boolean isValidPrice(String price)
  {
    boolean result = false;
    try
    {
      double temp = Double.parseDouble(price);
      result = true;
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }

  public boolean allowSelection()
  {
    return this.allowSelection;
  }
  public boolean shouldGoBack()
  {
    return (this.needed == 0);
  }
  public void setName(String name)
  {
    this.name = name;
  }
  public void setApplication(String application)
  {
    this.application = application;
  }

  public void setDesc(String desc)
  {
    this.desc = desc;
  }
  public void setLendCode(String lendCode)
  {
    this.lendCode = lendCode;
  }
  public void setPrice(String price)
  {
    this.price = price;
  }
  public void setMerchantNum(String merchantNum)
  {
    this.merchantNum = merchantNum;
  }
  public void setModel(String model)
  {
    this.model = model;
  }
  public void setNeeded(String needed)
  {
    try
    {
      this.needed = Integer.parseInt(needed);
    }
    catch(Exception e)
    {
      this.needed = 0;
    }
  }

  public String getModel()
  {
    return this.model;
  }

  public String getApplication()
  {
    return this.application;
  }

  public int getNeeded()
  {
    return this.needed;
  }

  public String getName()
  {
    return this.name;
  }
  public String getDesc()
  {
    return this.desc;
  }
  public String getLend()
  {
    String result = "";
    switch(Integer.parseInt(this.lendCode))
    {

      case mesConstants.APP_EQUIP_PURCHASE:
        result = "Purchase";
      break;
      case mesConstants.APP_EQUIP_RENT:
        result = "Rent";
      break;
      case mesConstants.APP_EQUIP_LEASE:
        result = "Lease";
      break;
      case mesConstants.APP_EQUIP_LOAN:
        result = "Loan";
      break;
      default:
        result = "Unknown";
      break;

    }
    return result;
  }
  public String getLendCode()
  {
    return this.lendCode;
  }
  public String getPrice()
  {
    return this.price;
  }
  public String getMerchantNum()
  {
    return this.merchantNum;
  }

  public void setEquipment(String serialNumber)
  {
    if(isBlank(serialNumber))
    {
      this.serialNumber = "";
      return;
    }

    if(serialNumber.startsWith("NEW*"))
    {
      this.newSetup = true;
    }

    this.serialNumber = serialNumber.substring(4);
  }

  public String getClassTypeDesc(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getClassTypeDesc());
  }
  public String getUnitCost(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getUnitCost());
  }
  public String getProductName(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getProductName());
  }
  public String getProductDesc(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getProductDesc());
  }
  public String getOwnerDesc(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getOwnerDesc());
  }
  public String getSerialNumber(int idx)
  {
    EquipRec equip = (EquipRec)equipToDeploy.elementAt(idx);
    return(equip.getSerialNumber());
  }

  public String getReceiveDate(int idx)
  {
    EquipRec    equip  = (EquipRec)equipToDeploy.elementAt(idx);
    DateFormat  df     = new SimpleDateFormat("MM/dd/yy");
    String      result = df.format(equip.getReceiveDate());
    return result;
  }


  public class EquipRec
  {
    private String productName    = "";
    private String productDesc    = "";
    private String ownerDesc      = "";
    private String serialNumber   = "";
    private String unitCost       = "";
    private String classTypeDesc  = "";
    private Date   receiveDate    = null;

    EquipRec()
    {}

    public void setClassTypeDesc(String classTypeDesc)
    {
      this.classTypeDesc = classTypeDesc;
    }
    public void setUnitCost(String unitCost)
    {
      this.unitCost = unitCost;
    }
    public void setProductName(String productName)
    {
      this.productName = productName;
    }
    public void setProductDesc(String productDesc)
    {
      this.productDesc = productDesc;
    }
    public void setOwnerDesc(String ownerDesc)
    {
      this.ownerDesc = ownerDesc;
    }
    public void setSerialNumber(String serialNumber)
    {
      this.serialNumber = serialNumber;
    }
    public void setReceiveDate(Date tempDate)
    {
      this.receiveDate = tempDate;
    }

    public String getClassTypeDesc()
    {
      return this.classTypeDesc;
    }
    public String getUnitCost()
    {
      return this.unitCost;
    }
    public String getProductName()
    {
      return this.productName;
    }
    public String getProductDesc()
    {
      return this.productDesc;
    }
    public String getOwnerDesc()
    {
      return this.ownerDesc;
    }
    public String getSerialNumber()
    {
      return this.serialNumber;
    }
    public Date getReceiveDate()
    {
      return this.receiveDate;
    }
  }



}/*@lineinfo:generated-code*/