/*@lineinfo:filename=BbtAutoApprove*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/BbtAutoApprove.sqlj $

  Description:

    AutoApprove

    Tools for auto approving (and auto uploading if necessary) an
    online application

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 5/20/04 2:26p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class BbtAutoApprove extends SQLJConnectionBase
{
  private long      appSeqNum           = 0L;
  private int       appType             = -1;
  
  private String    invCode             = "0011";
  private int       creditDelay         = 0;
  private int       debitDelay          = 0;
  private int       daysSuspend         = 0;
  private int       bankNumber          = 3867;
  private int       incStatus           = 1;
  private String    statusIndicator     = "";
  private String    appSource           = "";
  
  private String    userData1           = "";
  private String    userData2           = "";
  private String    userData3           = "";
  private String    userData4           = "";
  private String    userData5           = "";
  private String    userAccount1        = "";
  private String    userAccount2        = "";
  
   
  /*
  ** BbtAutoApprove()
  */ 
  public BbtAutoApprove()
  {
    super();
  }
  
  /*
  ** BbtAutoApprove(DefaultContext Ctx) constructor
  **
  ** Latches on to passed default context for database interaction
  */
  public BbtAutoApprove(DefaultContext defCtx)
  {
    super();
    
    if(defCtx != null)
    {
      Ctx = defCtx;
    }
  }
  
  /*
  ** moveToMms()
  **
  ** inserts application into mms queue
  */


  
  
  /*
  ** getUserData()
  **
  ** Generates user data fields and stores them.
  **
  ** NOTE:  
  */
  private void getUserData() throws Exception
  {
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    
      switch(bankNumber)
      {
        case mesConstants.BANK_ID_BBT:
          /*@lineinfo:generated-code*//*@lineinfo:113^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    decode(merch.merch_referring_bank,null,'',merch.merch_referring_bank) referring_bank,
//                        decode(merch.processor,null,-1,merch.processor)                       processor,
//                        decode(merch.web_reporting,null,'N',merch.web_reporting)              web_reporting,
//                        poscat.pos_type                          
//              from      merchant     merch,                      
//                        merch_pos    pos,                        
//                        pos_category poscat                      
//              where     merch.app_seq_num  = pos.app_seq_num and 
//                        pos.pos_code       = poscat.pos_code and 
//                        merch.app_seq_num  = :appSeqNum                   
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    decode(merch.merch_referring_bank,null,'',merch.merch_referring_bank) referring_bank,\n                      decode(merch.processor,null,-1,merch.processor)                       processor,\n                      decode(merch.web_reporting,null,'N',merch.web_reporting)              web_reporting,\n                      poscat.pos_type                          \n            from      merchant     merch,                      \n                      merch_pos    pos,                        \n                      pos_category poscat                      \n            where     merch.app_seq_num  = pos.app_seq_num and \n                      pos.pos_code       = poscat.pos_code and \n                      merch.app_seq_num  =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.BbtAutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.BbtAutoApprove",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^11*/
      
          rs = it.getResultSet();
          
          if(rs.next())
          {
            userAccount2       = rs.getString("referring_bank");

            if(rs.getString("web_reporting").toUpperCase().equals("Y"))
            {
              userData3 = "WEB";
            }

            switch(rs.getInt("pos_type"))
            {
              case mesConstants.POS_TOUCHTONECAPTURE:
                userData1 = "TETC";
              break;
              case mesConstants.POS_DIAL_AUTH:
                userData1 = "DPAY";
              break;
              case mesConstants.POS_DIAL_TERMINAL:
              default:
                if(rs.getInt("processor") == mesConstants.APP_PROCESSOR_TYPE_VITAL)
                {
                  userData1 = "VNET";
                }
                else if(rs.getInt("processor") == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_CENTRAL)
                {
                  userData1 = "MAPP";
                }
                else if(rs.getInt("processor") == mesConstants.APP_PROCESSOR_TYPE_GLOBAL_EAST)
                {
                  userData1 = "NDC";
                }
                else if(rs.getInt("processor") == mesConstants.APP_PROCESSOR_TYPE_PAYMENTECH)
                {
                  userData1 = "PAYT";
                }
              break;
            }
          }
          
          rs.close();
          it.close();

          /*@lineinfo:generated-code*//*@lineinfo:171^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(referring_officer,null,'',referring_officer) referring_officer,  
//                      check_provider      
//              from    app_merch_bbt       
//              where   app_seq_num = :appSeqNum     
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(referring_officer,null,'',referring_officer) referring_officer,  \n                    check_provider      \n            from    app_merch_bbt       \n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.BbtAutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.BbtAutoApprove",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:177^11*/
      
          rs = it.getResultSet();
          
          if(rs.next())
          {
            userData4 = rs.getString("referring_officer");
            if(rs.getString("check_provider") != null && !rs.getString("check_provider").equals(""))
            {
              if(rs.getString("check_provider").equals(mesConstants.APP_BBT_CHECK_PROVIDER_GLOBAL))
              {
                userData2 = "GCHK";  
              }
              else if(rs.getString("check_provider").equals(mesConstants.APP_BBT_CHECK_PROVIDER_TTECH))
              {
                userData2 = "TTEC";
              }
            }

          }
          
          rs.close();
          it.close();
          break;

        // add other bank numbers as necessary
        default:
          break;
      }
      
      // update user data in merchant_data table
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:210^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant_data
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant_data\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.BbtAutoApprove",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:216^7*/
      
      if(recCount == 0)
      {
        // insert
        /*@lineinfo:generated-code*//*@lineinfo:221^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_data
//            (
//              app_seq_num,
//              user_data_1,
//              user_data_2,
//              user_data_3,
//              user_data_4,
//              user_data_5,
//              user_account_1,
//              user_account_2
//            )
//            values
//            (
//              :appSeqNum,
//              :userData1,
//              :userData2,
//              :userData3,
//              :userData4,
//              :userData5,
//              :userAccount1,
//              :userAccount2
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant_data\n          (\n            app_seq_num,\n            user_data_1,\n            user_data_2,\n            user_data_3,\n            user_data_4,\n            user_data_5,\n            user_account_1,\n            user_account_2\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.BbtAutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,userData1);
   __sJT_st.setString(3,userData2);
   __sJT_st.setString(4,userData3);
   __sJT_st.setString(5,userData4);
   __sJT_st.setString(6,userData5);
   __sJT_st.setString(7,userAccount1);
   __sJT_st.setString(8,userAccount2);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:245^9*/
      }
      else
      {
        // update
        /*@lineinfo:generated-code*//*@lineinfo:250^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_data
//            set     user_data_1     = :userData1,
//                    user_data_2     = :userData2,
//                    user_data_3     = :userData3,
//                    user_data_4     = :userData4,
//                    user_data_5     = :userData5,
//                    user_account_1  = :userAccount1,
//                    user_account_2  = :userAccount2
//            where   app_seq_num     = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant_data\n          set     user_data_1     =  :1 ,\n                  user_data_2     =  :2 ,\n                  user_data_3     =  :3 ,\n                  user_data_4     =  :4 ,\n                  user_data_5     =  :5 ,\n                  user_account_1  =  :6 ,\n                  user_account_2  =  :7 \n          where   app_seq_num     =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ops.BbtAutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,userData1);
   __sJT_st.setString(2,userData2);
   __sJT_st.setString(3,userData3);
   __sJT_st.setString(4,userData4);
   __sJT_st.setString(5,userData5);
   __sJT_st.setString(6,userAccount1);
   __sJT_st.setString(7,userAccount2);
   __sJT_st.setLong(8,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:261^9*/
      }
  }
  
  /*
  ** expandData()
  **
  ** Generates account-specific data based on a combination of app-type defaults
  ** and app-specific data
  */
  private void expandData() throws Exception
  {
    
     
      // generate user data fields
      getUserData();

      int sicCode = 0;

      //get sic code to put into mcc.. get business type to put into inc status and check to see
      //if status indicator already has a value in the db.. if so.. perserve it.. if not.. overwrite it..
      /*@lineinfo:generated-code*//*@lineinfo:282^7*/

//  ************************************************************
//  #sql [Ctx] { select  sic_code,
//                  decode(bustype_code,null,1,1,1,3,2,2,3,6,3),
//                  decode(status_ind,null,' ',status_ind)
//          
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sic_code,\n                decode(bustype_code,null,1,1,1,3,2,2,3,6,3),\n                decode(status_ind,null,' ',status_ind)\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.BbtAutoApprove",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sicCode = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   incStatus = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   statusIndicator = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:292^7*/

      
      // update merchant table to show values
      /*@lineinfo:generated-code*//*@lineinfo:296^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     allow_auto_upload         = 'Y',
//                  merch_mcc                 = :sicCode,
//                  merch_bank_number         = :bankNumber,
//                  status_ind                = :statusIndicator,
//                  merch_visainc_status_ind  = :incStatus,
//                  merch_invg_code           = :invCode
//          where   app_seq_num               = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     allow_auto_upload         = 'Y',\n                merch_mcc                 =  :1 ,\n                merch_bank_number         =  :2 ,\n                status_ind                =  :3 ,\n                merch_visainc_status_ind  =  :4 ,\n                merch_invg_code           =  :5 \n        where   app_seq_num               =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.ops.BbtAutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sicCode);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setString(3,statusIndicator);
   __sJT_st.setInt(4,incStatus);
   __sJT_st.setString(5,invCode);
   __sJT_st.setLong(6,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:306^7*/
      
      int recCount = 0;
      // update credit data in merchcredit table
      /*@lineinfo:generated-code*//*@lineinfo:310^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchcredit
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchcredit\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.BbtAutoApprove",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:316^7*/
      
      if(recCount ==  0)
      {
        // insert
        /*@lineinfo:generated-code*//*@lineinfo:321^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcredit
//            (
//              app_seq_num,
//              merch_credit_achpost_days,
//              merch_debit_achpost_days,
//              merch_days_suspend
//            )
//            values
//            (
//              :appSeqNum,
//              :creditDelay,
//              :debitDelay,
//              :daysSuspend
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchcredit\n          (\n            app_seq_num,\n            merch_credit_achpost_days,\n            merch_debit_achpost_days,\n            merch_days_suspend\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.ops.BbtAutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,creditDelay);
   __sJT_st.setInt(3,debitDelay);
   __sJT_st.setInt(4,daysSuspend);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:337^9*/
      }
      else
      {
        // update
        /*@lineinfo:generated-code*//*@lineinfo:342^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchcredit
//            set     merch_credit_achpost_days = :creditDelay,
//                    merch_debit_achpost_days  = :debitDelay,
//                    merch_days_suspend        = :daysSuspend
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchcredit\n          set     merch_credit_achpost_days =  :1 ,\n                  merch_debit_achpost_days  =  :2 ,\n                  merch_days_suspend        =  :3 \n          where   app_seq_num =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.ops.BbtAutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,creditDelay);
   __sJT_st.setInt(2,debitDelay);
   __sJT_st.setInt(3,daysSuspend);
   __sJT_st.setLong(4,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:349^9*/
      }
  }
  
  private boolean doAutoApprove(long appSeqNum)
  {
    boolean result = false;
    
    try
    {
      connect();
      
      this.appSeqNum = appSeqNum;
      
      // get app type
      /*@lineinfo:generated-code*//*@lineinfo:364^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//          
//          from    application
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n         \n        from    application\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.BbtAutoApprove",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:370^7*/
      
      // assign merchant number
      (new AssignMidBean()).assignMid(appSeqNum);
        
      // data expansion
      expandData();
        
      // auto-upload
      AutoUploadIndividualMerchant       autoUpload1 = new AutoUploadIndividualMerchant    (appSeqNum);
      AutoUploadMerchantDiscount         autoUpload2 = new AutoUploadMerchantDiscount      (appSeqNum);
      AutoUploadBetInfo                  autoUpload3 = new AutoUploadBetInfo               (appSeqNum);
      AutoUploadChargeRecords            autoUpload4 = new AutoUploadChargeRecords         (appSeqNum);
      AutoUploadTransactionDestination   autoUpload5 = new AutoUploadTransactionDestination(appSeqNum);
      AutoUploadAddressUsage             autoUpload6 = new AutoUploadAddressUsage          (appSeqNum);

      //flag for autoupload
      /*@lineinfo:generated-code*//*@lineinfo:387^7*/

//  ************************************************************
//  #sql [Ctx] { insert into app_setup_complete
//          (
//            app_seq_num,
//            date_completed
//          )
//          values
//          (
//            :appSeqNum,
//            sysdate
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_setup_complete\n        (\n          app_seq_num,\n          date_completed\n        )\n        values\n        (\n           :1 ,\n          sysdate\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.ops.BbtAutoApprove",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:399^7*/

      //call moveToMms();

      result = true;
    }
    catch(Exception e)
    {
      logEntry("doAutoApprove(" + appSeqNum + ")", e.toString());
      result = false;
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public static boolean bbtAutoApprove(long appSeqNum)
  {
    BbtAutoApprove baa = new BbtAutoApprove();
    return baa.doAutoApprove(appSeqNum);
  }
  
  public static boolean bbtAutoApprove(long appSeqNum, DefaultContext Ctx)
  {
    BbtAutoApprove baa = new BbtAutoApprove(Ctx);
    return baa.doAutoApprove(appSeqNum);
  }
}/*@lineinfo:generated-code*/