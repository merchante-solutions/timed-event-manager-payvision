/*@lineinfo:filename=DceFile*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.ops;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.mes.database.SQLJConnectionBase;

public class DceFile extends SQLJConnectionBase
{
  private int               LinesWritten      = 0;
  private File              UploadFile       = null;
  private FileWriter        Fwriter          = null;
  private String            FileIdentifier   = null;
  private boolean           FileCreated      = false;

  /*
  ** CONSTRUCTOR public DceFileManager()
  **
  ** Default constructor.  Turns off autocommit.
  */
  public DceFile()
  {
    super(SQLJConnectionBase.getDirectConnectString(), false);
  }

  /*
  ** CONSTRUCTOR public DceFileManager(String connectionString)
  **
  ** Accepts a connection string.  Turns off autocommit.
  */
  public DceFile(String fileIdentifier)
  {
    // disable auto commit
    super(SQLJConnectionBase.getDirectConnectString(), false);
    FileIdentifier = fileIdentifier.trim();
  }

  private void createFile() throws Exception
  {
    Calendar     cal          = Calendar.getInstance();
    DateFormat   df           = new SimpleDateFormat("MMddyy");

    String storedFileName   = "U:\\\\dcefile\\old_files\\"  + FileIdentifier + "DCEFILE_"     + df.format(cal.getTime()) + "_" + getIdentifierCode() + ".txt";
    String currentFileName  = "U:\\\\dcefile\\"             + FileIdentifier + "DCEFILE.txt";
        
    File currentFile = new File(currentFileName);
      
/*
    Here we check to see if a file already exists in the working directory.  If so, we
    rename it and move it to the archive old_files directory.  
*/      
    
    if(currentFile.exists())
    {
      File storedFile = new File(storedFileName);
      currentFile.renameTo(storedFile);
      currentFile.delete();
    }

/*    
    Then we create a new file in
    the working directory and associate a new file writer to it.
*/

    UploadFile  = new File(currentFileName);
    if(UploadFile.createNewFile())
    {
      Fwriter     = new FileWriter(UploadFile);
      FileCreated = true;
    }
  
  }

  public void writeLineToFile(String[] fileDataLine) throws Exception
  {
    if(!isFileCreated())
    {
      createFile();
    }

    for(int i=0; i < fileDataLine.length; i++)
    {
      //System.out.println("writing  filedataline[" + i + "] = " + fileDataLine[i]);
      Fwriter.write(fileDataLine[i]);
    }

    Fwriter.write("\n");
    LinesWritten++;

  }

  public String getFileName()
  {
    String fileName = "";

    try
    {
      if(isFileCreated())
      {
        fileName = UploadFile.getName();
      }
    }
    catch(Exception e)
    {
      System.out.println("getFileName: " + e.toString());
    }
    
    return fileName;
  }

  public String getFileIdentifier()
  {
    return FileIdentifier;
  }

  public boolean deleteFile()
  {
    boolean fileDeleted = false;

    try
    {
      if(isFileCreated())
      {
        fileDeleted = UploadFile.delete();
      }
    }
    catch(Exception e)
    {
      System.out.println("deleteFile: " + e.toString());
      //logEntry("deleteFile()",e.toString());
    }

    return fileDeleted;
  }

  public boolean isFileCreated()
  {
    return this.FileCreated;
  }
  
  public int getLinesWritten()
  {
    return this.LinesWritten;
  }

  public void closeFile()
  {
    try
    {
      Fwriter.close();
    }
    catch(Exception e)
    {
      System.out.println("Close File: " + e.toString());
    }
  }

  private String getIdentifierCode()
  {
    //default identifier code to be current date month and day...
    
    String       result     = "0000";
    boolean      useCleanUp = false;

    try
    {
      Calendar     cal      = Calendar.getInstance();
      DateFormat   df       = new SimpleDateFormat("MMdd");
      result                = df.format(cal.getTime());
    }
    catch(Exception e)
    {
      result = "0000";
    }

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:194^7*/

//  ************************************************************
//  #sql [Ctx] { select    dce_identifier_code.nextval 
//          from      dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    dce_identifier_code.nextval  \n        from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.DceFile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:198^7*/
            
      if(result.length() == 1)
        result = "000" + result;
      else if(result.length() == 2)
        result = "00" + result;
      else if(result.length() == 3)
        result = "0" + result;

    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::getIdentifierCode(): " + e);
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
    
    
    return result;
  }

}/*@lineinfo:generated-code*/