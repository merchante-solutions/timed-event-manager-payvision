/*@lineinfo:filename=BbtQaSetup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/BbtQaSetup.sqlj $

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 9/20/04 2:10p $
  Version            : $Revision: 13 $

  BbtQaSetup

  Supports BB&T qa setup queue screen (bbt_qa_setup.jsp).

  Change History:
     See VSS database

  Copyright (C) 2003 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.TextareaField;
import com.mes.queues.QueueTools;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public final class BbtQaSetup extends FieldBean
{

  //status prefix names
  public static final String DECLINE    = "decline";
  public static final String PEND       = "pend";
  public static final String CANCEL     = "cancel";
  public static final String APPROVE    = "approve";
  public static final String NEW_TYPE   = "new";
  public static final String DOC_ITEM   = "docitem";
  public static final String UPDATE     = "update";

  //status reason group names
  public static final String DECLINE_REASONS    = "declineReasons";
  public static final String PEND_REASONS       = "pendReasons";
  public static final String CANCEL_REASONS     = "cancelReasons";
  public static final String ADDITIONAL_DOCS    = "additionalDocs";

  /*
  ** public BbtQaSetup(UserBean user)
  **
  ** Constructor.
  **
  ** Initializes fields used by credit decision screen form.
  */
  public BbtQaSetup(UserBean user)
  {
    super(user);

    fields.add(new HiddenField("type"));
    fields.add(new HiddenField("id"));
    fields.add(new HiddenField("itemType"));
    fields.add(new HiddenField("startType"));

    fields.add(new ButtonField(UPDATE));
    fields.add(new ButtonField(APPROVE));
    fields.add(new ButtonField(PEND));
    fields.add(new ButtonField(CANCEL));

    //not providing decline option for now
    //fields.add(new ButtonField(DECLINE));

    fields.add(new TextareaField("dataEntryErrorDesc","Error Description",512,5,100,true));
    fields.add(new TextareaField("documentControlErrorDesc","Error Description",512,5,100,true));
    fields.add(new NumberField("dataEntryErrors",3,3,true,0));
    fields.add(new NumberField("documentControlErrors",3,3,true,0));

    fields.setGroupHtmlExtra("class=\"smallText\"");
  }

  /*
  ** private void createCreditReasonFields(int type, String groupName,
  **   String itemName)
  **
  ** Loads all reason codes from q_credit_reasons that have the type
  ** assigned to the queue type in q_credit_types.  A checkbox field 
  ** is generated for each reason code found, named by appending the 
  ** code to the fieldName prefix.  The fields are placed in a field 
  ** group named with groupName, which is then placed into the beans 
  ** fields.
  */
  
  private void createCreditReasonFields(int qType, String groupName, String fieldPrefix)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      FieldGroup reasonGroup = new FieldGroup(groupName);

      connect();

      // load reasons
      /*@lineinfo:generated-code*//*@lineinfo:116^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  :fieldPrefix||'_'||qr.code field_name,
//                  qr.label
//          from    q_credit_reasons  qr,
//                  q_credit_types    qt
//          where   qt.type = :qType
//                  and qt.reason_type = qr.type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1 ||'_'||qr.code field_name,\n                qr.label\n        from    q_credit_reasons  qr,\n                q_credit_types    qt\n        where   qt.type =  :2 \n                and qt.reason_type = qr.type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.BbtQaSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fieldPrefix);
   __sJT_st.setInt(2,qType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.BbtQaSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:124^7*/

      // generate a checkbox field for each reason
      rs = it.getResultSet();
      while(rs.next())
      {
        String fieldName  = rs.getString("field_name");
        String label      = rs.getString("label");
        Field reason      = null;

        //cancel reasons is always other so we set it with a hidden field
        if(groupName.equals(CANCEL_REASONS))
        {
          reason  = new HiddenField(fieldName);
        }
        else
        {
          reason  = new CheckboxField(fieldName,label,false);
        }
       
        reasonGroup.add(reason);
      }

      reasonGroup.add(new TextareaField((fieldPrefix+"_other"), (fieldPrefix+" reason"), 512, 4, 50, true));

      reasonGroup.setGroupHtmlExtra("class=\"smallText\"");
      fields.add(reasonGroup);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::createCreditReasonFields(" + groupName + "): " + e);
      logEntry("createCreditReasonFields(" + groupName + ")",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /*
  ** protected void preHandleRequest(HttpServletRequest request)
  **
  ** Create additional reason fields using type found in request.
  */
  protected void preHandleRequest(HttpServletRequest request)
  {
    // create reason code fields based on queue type
    createCreditReasonFields(MesQueues.Q_BBT_QA_PENDING,   PEND_REASONS,    PEND);
    createCreditReasonFields(MesQueues.Q_BBT_QA_CANCELLED, CANCEL_REASONS,  CANCEL);

    //not using decline status for now
    //createCreditReasonFields(MesQueues.Q_BBT_QA_DECLINED,  DECLINE_REASONS, DECLINE);
    
  }
  
  /*
  ** protected void postHandleRequest(HttpServletRequest request)
  **
  ** Set startType if it's blank.
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    // copy type into startType if startType is not set
    if (fields.getField("startType").isBlank())
    {
      fields.setData("startType",fields.getData("type"));
    }
  }

  /*
  ** private String makeCodeString(int newType)
  **
  ** Takes a groupName, gets the field group with that name and iterates
  ** through the fields contained by it.  Constructs a comma delimitted
  ** list of the codes contained in the field names (code is assumed to
  ** be the portion of the field name following the first underscore).
  **
  ** RETURNS: a String containing the codes separated by commas.
  */
  private String makeCodeString(int newType)
  {
    StringBuffer codeString = new StringBuffer();
    String       groupName  = "";

    switch(newType)
    {
        case MesQueues.Q_BBT_QA_PENDING:
          groupName = PEND_REASONS;
        break;
        
        case MesQueues.Q_BBT_QA_DECLINED:
          groupName = DECLINE_REASONS;
        break;
        
        case MesQueues.Q_BBT_QA_CANCELLED:
          groupName = CANCEL_REASONS;
        break;
    }
    
    if(groupName.equals(""))
    {
      return "";
    }

    try
    {


      boolean firstCode = true;

      for (Iterator i = getGroupIterator(groupName); i.hasNext(); )
      {
        Field field = (Field)i.next();
        if ((field instanceof CheckboxField && ((CheckboxField)field).isChecked()) || field instanceof HiddenField)
        {
          if (!firstCode)
          {
            codeString.append(",");
          }
          else
          {
            firstCode = false;
          }
          String name = field.getName();
          String code = name.substring(name.indexOf('_') + 1);
          codeString.append(code);
        }
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::makeCodeString(" + groupName + "): " + e);
      logEntry("makeCodeString(" + groupName + ")",e.toString());
    }
    return codeString.toString();
  }

  /*
  ** private String getOtherReason(int newType)
  **
  ** Takes a groupName, gets the field group with that name and iterates
  ** through the fields contained by it, looking for textareas, then takes data from them 
  **
  ** RETURNS: a String containing the codes separated by commas.
  */
  private String getOtherReason(int newType)
  {
    String otherReason  = "";
    String groupName    = "";

    switch(newType)
    {
        case MesQueues.Q_BBT_QA_PENDING:
          groupName = PEND_REASONS;
        break;
        
        case MesQueues.Q_BBT_QA_DECLINED:
          groupName = DECLINE_REASONS;
        break;
        
        case MesQueues.Q_BBT_QA_CANCELLED:
          groupName = CANCEL_REASONS;
        break;
    }
    
    if(groupName.equals(""))
    {
      return "";
    }

    try
    {
      for (Iterator i = getGroupIterator(groupName); i.hasNext(); )
      {
        Field field = (Field)i.next();
        if (field instanceof TextareaField && !field.isBlank())
        {
          otherReason = field.getData();
        }
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::getOtherReason(" + groupName + "): " + e);
      logEntry("getOtherReason(" + groupName + ")",e.toString());
    }
    return otherReason;
  }


  /*
  ** private void resetTypeFields(int queueType)
  **
  ** Resets all fields contained within a field group corresponding with
  ** the given queue type.
  */

  private void resetTypeFields(int queueType)
  {
    // determine the corresponding field group name from the queue type
    String groupName = null;
    switch (queueType)
    {
      case MesQueues.Q_BBT_QA_PENDING:
        groupName = PEND_REASONS;
        break;

      case MesQueues.Q_BBT_QA_CANCELLED:
        groupName = CANCEL_REASONS;
        break;

      case MesQueues.Q_BBT_QA_DECLINED:
        groupName = DECLINE_REASONS;
        break;

      case MesQueues.Q_BBT_QA_COMPLETED:
        groupName = ADDITIONAL_DOCS;
        break;

      default:
        break;
    }
    
    if (groupName != null)
    {
      FieldGroup group = (FieldGroup)fields.getField(groupName);
      if (group != null)
      {
        group.reset();
      }
    }
  }


  /*
  ** private void updateMerchCreditStatus(long appSeqNum, int bbtQaType)
  **
  ** Updates the merch_credit_status field in merchant to a new status.
  **
  ** NOTE: connect() and cleanUp() NOT called.
  **
  ** RETURNS: nothing.
  */
  private void updateMerchCreditStatus(long appSeqNum, int bbtQaType)
  {
    int newStatus = -1;

    switch (bbtQaType)
    {
      case MesQueues.Q_BBT_QA_SETUP:
        newStatus = QueueConstants.CREDIT_NEW;
        break;
        
      case MesQueues.Q_BBT_QA_PENDING:
        newStatus = QueueConstants.CREDIT_PEND;
        break;
        
      case MesQueues.Q_BBT_QA_DECLINED:
        newStatus = QueueConstants.CREDIT_DECLINE;
        break;
        
      case MesQueues.Q_BBT_QA_CANCELLED:
        newStatus = QueueConstants.CREDIT_CANCEL;
        break;
        
      case MesQueues.Q_BBT_QA_COMPLETED:
        newStatus = QueueConstants.CREDIT_APPROVE;
        break;
    }
    
    try
    {
     
      // update the merch_credit_status column in the merchant table
      if(newStatus > -1)
      {
        /*@lineinfo:generated-code*//*@lineinfo:405^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_credit_status = :newStatus
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n          set     merch_credit_status =  :1 \n          where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.BbtQaSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,newStatus);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:410^9*/
      }

    }
    catch (Exception e)
    {
      logEntry("updateMerchCreditStatus(" + appSeqNum + ", " + newStatus + ")",
        e.toString());
    }
  }

  /*
  ** public boolean autoSubmit()
  **
  ** Called during bean auto set.  Stores the credit data.
  **
  ** RETURNS: true if submit ok, else false.
  */
  public boolean autoSubmit()
  {
    _isAutoSubmitOk = false;

    try
    {
      int     oldType     = Integer.parseInt(fields.getData("type"));
      int     newType     = oldType;
      long    id          = Long.parseLong(fields.getData("id"));
      String  codeString  = "";
      String  otherReason = "";

      // determine new type and reason code string based on submit type
      if (autoSubmitName.equals(PEND))
      {
        newType       = MesQueues.Q_BBT_QA_PENDING;
      }
      else if (autoSubmitName.equals(DECLINE))
      {
        newType       = MesQueues.Q_BBT_QA_DECLINED;
      }
      else if (autoSubmitName.equals(CANCEL))
      {
        newType       = MesQueues.Q_BBT_QA_CANCELLED;
      }
      else if (autoSubmitName.equals(APPROVE))
      {
        newType       = MesQueues.Q_BBT_QA_COMPLETED;
      }
      else if (autoSubmitName.equals(NEW_TYPE))
      {
        newType       = MesQueues.Q_BBT_QA_SETUP;
      }

      codeString    = makeCodeString(newType);
      otherReason   = getOtherReason(newType);

      // insert/update q_credit_data record
      connect();

      try
      {
        // assume this is new credit data, try to insert
        /*@lineinfo:generated-code*//*@lineinfo:471^9*/

//  ************************************************************
//  #sql [Ctx] { insert into q_credit_data
//            ( id,
//              codes,
//              description )
//            values
//            ( :id,
//              :codeString,
//              :otherReason )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_credit_data\n          ( id,\n            codes,\n            description )\n          values\n          (  :1 ,\n             :2 ,\n             :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.BbtQaSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setString(2,codeString);
   __sJT_st.setString(3,otherReason);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:481^9*/
      }
      catch (Exception e)
      {
        // record for this credit item must already exist, update it
        /*@lineinfo:generated-code*//*@lineinfo:486^9*/

//  ************************************************************
//  #sql [Ctx] { update  q_credit_data
//            set     codes       = :codeString,
//                    description = :otherReason
//            where   id = :id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  q_credit_data\n          set     codes       =  :1 ,\n                  description =  :2 \n          where   id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.BbtQaSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,codeString);
   __sJT_st.setString(2,otherReason);
   __sJT_st.setLong(3,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:492^9*/
      }

      /*@lineinfo:generated-code*//*@lineinfo:495^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_merch_bbt
//          set     data_entry_errors           = :fields.getField("dataEntryErrors").getData(),
//                  data_entry_error_desc       = :fields.getField("dataEntryErrorDesc").getData(),
//                  document_control_errors     = :fields.getField("documentControlErrors").getData(),
//                  document_control_error_desc = :fields.getField("documentControlErrorDesc").getData()
//          where   app_seq_num = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_728 = fields.getField("dataEntryErrors").getData();
 String __sJT_729 = fields.getField("dataEntryErrorDesc").getData();
 String __sJT_730 = fields.getField("documentControlErrors").getData();
 String __sJT_731 = fields.getField("documentControlErrorDesc").getData();
   String theSqlTS = "update  app_merch_bbt\n        set     data_entry_errors           =  :1 ,\n                data_entry_error_desc       =  :2 ,\n                document_control_errors     =  :3 ,\n                document_control_error_desc =  :4 \n        where   app_seq_num =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ops.BbtQaSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_728);
   __sJT_st.setString(2,__sJT_729);
   __sJT_st.setString(3,__sJT_730);
   __sJT_st.setString(4,__sJT_731);
   __sJT_st.setLong(5,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:503^7*/

     
      // if a newType is different than old type, use QueueTools to move
      // the queue item from the old queue to the new queue 
      if (oldType != newType)
      {

        //**********************NOT USING RIGHT NOW*****************
        // take action as required on the documentation queue item
        //handleDocQueue(id,oldType,newType);
        //**********************************************************

        // move the queue item from old to new queue
        QueueTools.moveQueueItem(id,oldType,newType,user,null);

        // set the queue type in the type field to be the new queue
        fields.setData("type", Integer.toString(newType));

        // add a queue type override flag to the request
        request.setAttribute("overrideType", Integer.toString(newType));
        
        // reset the checkboxes corresponding with the old type
        resetTypeFields(oldType);
        
        // update the merch credit status
        updateMerchCreditStatus(id,newType);

        //run the autoupload code and insert the item into the mms queue
        if(newType == MesQueues.Q_BBT_QA_COMPLETED)
        {
          if(!BbtAutoApprove.bbtAutoApprove(id))
          {
            throw new Exception("Application failed within auto approve bean appSeqNum = " + id);
          }

          if(!BbtDiscoverBean.bbtDiscoverBean(id,user))
          {
            throw new Exception("Application failed within discover account setup bean appSeqNum = " + id);
          }
 
          if(!BbtMmsSetup.bbtMmsSetup(id))
          {
            throw new Exception("Application failed within mms setup bean appSeqNum = " + id);
          }

          // add to the amex esa queue (if necessary)
          AmexESADataBean amexBean = new AmexESADataBean();
          if ( amexBean.needsEsaApp(id) )
          {
            amexBean.submitEsaApp(id);
          }
          amexBean = null;
        }
      }

      _isAutoSubmitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::autoSubmit(): " + e);
      logEntry("autoSubmit()",e.toString());
    }
    finally
    {
      cleanUp();
    }

    return _isAutoSubmitOk;
  }

  
  /*
  ** public void loadCodes(int queueType, String codeString)
  **
  ** Determines a field name prefix to use based on queueType, then
  ** parses codeString and sets each corresponding checkbox field whose
  ** name begins with the given prefix.
  */
  public void loadCodes(int queueType, String codeString, String otherReason)
  {
    String prefix = null;
    switch (queueType)
    {
      case MesQueues.Q_BBT_QA_PENDING:
        prefix = PEND;
        break;

      case MesQueues.Q_BBT_QA_CANCELLED:
        prefix = CANCEL;
        break;

      case MesQueues.Q_BBT_QA_DECLINED:
        prefix = DECLINE;
        break;

      case MesQueues.Q_BBT_QA_COMPLETED:
        prefix = DOC_ITEM;
        break;

      default:
        break;
    }

    if (prefix != null && codeString != null && codeString.length() > 0)
    {
      StringTokenizer tok = new StringTokenizer(codeString,",");
      while (tok.hasMoreTokens())
      {
        String code = tok.nextToken();
        Field field = fields.getField(prefix + "_" + code);
        if (field != null)
        {
          field.setData("y");
        }
      }
    }

    if(otherReason != null && otherReason.length() > 0)
    {
      Field field = fields.getField(prefix + "_other");
      if (field != null)
      {
        field.setData(otherReason);
      }
    }
  }

  /*
  ** public boolean autoLoad()
  **
  ** Called during bean auto set.  Loads the credit data.
  **
  ** RETURNS: true if successful, else false.
  */
  public boolean autoLoad()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    _isAutoLoadOk = false;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:650^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  codes, 
//                  description
//          from    q_credit_data
//          where   id = :fields.getData("id")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_732 = fields.getData("id");
  try {
   String theSqlTS = "select  codes, \n                description\n        from    q_credit_data\n        where   id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.BbtQaSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_732);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.BbtQaSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:656^7*/

      rs = it.getResultSet();

      if (rs.next())
      {
        String  codeString  = rs.getString("codes");
        String  otherReason = rs.getString("description");
        int     queueType   = Integer.parseInt(fields.getData("type"));
        
        loadCodes(queueType,codeString,otherReason);
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:672^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  data_entry_errors,
//                  data_entry_error_desc,
//                  document_control_errors,
//                  document_control_error_desc
//          from    app_merch_bbt
//          where   app_seq_num = :fields.getData("id")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_733 = fields.getData("id");
  try {
   String theSqlTS = "select  data_entry_errors,\n                data_entry_error_desc,\n                document_control_errors,\n                document_control_error_desc\n        from    app_merch_bbt\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.BbtQaSetup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_733);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.BbtQaSetup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:680^7*/

      setFields(it.getResultSet());

      _isAutoLoadOk = true;
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName()
        + "::autoLoad(): " + e);
      logEntry("autoLoad()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }

    return _isAutoLoadOk;
  }
}/*@lineinfo:generated-code*/