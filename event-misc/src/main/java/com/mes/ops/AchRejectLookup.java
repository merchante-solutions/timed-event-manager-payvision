/*@lineinfo:filename=AchRejectLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AchRejectLookup.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/23/04 4:20p $
  Version            : $Revision: 10 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.io.Serializable;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.tools.DateSQLJBean;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class AchRejectLookup extends DateSQLJBean
  implements Serializable
{
  private String                lookupValue           = "";
  private String                lastLookupValue       = "";
  
  private int                   action                = mesConstants.LU_ACTION_INVALID;
  private String                actionDescription     = "Invalid Action";
  
  private RejectDataComparator  rdc                   = new RejectDataComparator();
  private Vector                lookupResults         = new Vector();
  private TreeSet               sortedResults         = null;
  
  private boolean               submitted             = false;

  private int                   achOpen               = -1;
  private int                   achCompleted          = -1;

  private int                   riskOpen              = -1;
  private int                   riskCompleted         = -1;

  private int                   collectionsOpen       = -1;
  private int                   collectionsCompleted  = -1;

  private int                   legalOpen             = -1;
  private int                   legalCompleted        = -1;

  
  private int                   lastFromMonth         = -1;
  private int                   lastFromDay           = -1;
  private int                   lastFromYear          = -1;
  private int                   lastToMonth           = -1;
  private int                   lastToDay             = -1;
  private int                   lastToYear            = -1;
  
  public void AchRejectLookup()
  {
    fillDropDowns();
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the "App Status" and "App Type" drop down boxes
  */  
  public void fillDropDowns()
  {
  }
  
  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
 
  /*
  ** METHOD public void getData()
  **
  */
  
  public void getData(UserBean user)
  {
    lookupResults.clear();

    if(achOpen == 1)
    {
      getIndividualData(user, MesQueues.Q_ACH_REJECT_CATEGORY_1);
      getIndividualData(user, MesQueues.Q_ACH_REJECT_CATEGORY_2);
      getIndividualData(user, MesQueues.Q_ACH_REJECT_CATEGORY_3);

      //we dont want to look up comment stuff.. c?? codes
      //getIndividualData(user, MesQueues.Q_ACH_REJECT_CATEGORY_MISC);

      getIndividualData(user, MesQueues.Q_ACH_REJECT_CATEGORY_URO);
    }

    if(achCompleted == 1)
    {
      getIndividualData(user, MesQueues.Q_ACH_REJECT_COMPLETED);
    }

    if(riskOpen == 1)
    {
      getIndividualData(user, MesQueues.Q_RISK_NEW);
    }

    if(riskCompleted == 1)
    {
      getIndividualData(user, MesQueues.Q_RISK_COMPLETED);
    }

    if(collectionsOpen == 1)
    {
      getIndividualData(user, MesQueues.Q_COLLECTIONS_NEW);
    }

    if(collectionsCompleted == 1)
    {
      getIndividualData(user, MesQueues.Q_COLLECTIONS_COMPLETED);
    }

    if(legalOpen == 1)
    {
      getIndividualData(user, MesQueues.Q_LEGAL_NEW);
    }

    if(legalCompleted == 1)
    {
      getIndividualData(user, MesQueues.Q_LEGAL_COMPLETED);
    }
   
    createSortedTree();
  }

 
  private void getIndividualData(UserBean user, int queueType)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect(true);
      
      String appTypeDesc = user.getAppDesc();
      
      if(appTypeDesc.equals("MES"))
      {
        appTypeDesc = "MESN";
      }

      // set up longLookup value (numeric version of lookup value)
      long longLookup;

      try
      {
        longLookup = Long.parseLong(lookupValue.trim());
      }
      catch (Exception e) 
      {
        longLookup = -2;
      }
      
      // add wildcards to stringLookup so we can match partial strings
      String stringLookup = "";
      if(lookupValue.trim().equals(""))
      {
        stringLookup = "passall";
      }
      else
      {
        stringLookup = "%" + lookupValue.toUpperCase() + "%";
      }

      Date fromDate = getSqlFromDate();
      Date toDate   = getSqlToDate();
      
      /*@lineinfo:generated-code*//*@lineinfo:215^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  qd.owner                owner,
//                  ar.settled_date         date_created,
//                  qd.source               source,
//                  qd.affiliate            affiliate,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  ar.merchant_name        description,
//                  ar.reject_seq_num       control,
//                  qd.locked_by            locked_by,
//                  get_queue_note_count(ar.reject_seq_num,qd.type)   note_count,
//                  qt.status               status,
//                  qt.description          type_desc,
//                  ar.merchant_number      merchant_number,
//                  ar.reason_code          reason_code,
//                  arrc.description        code_description,
//                  ar.amount               amount,
//                  decode(ars.reject_status, null, 'NEW', ars.reject_status) reject_status,
//                  m.dmagent               association,
//                  m.transit_routng_num    transit_routing_num
//          from    q_data                  qd,
//                  q_types                 qt,
//                  ach_rejects             ar,
//                  ach_reject_reason_codes arrc,
//                  ach_reject_status       ars,
//                  mif                     m
//          where   ar.settled_date between :fromDate and :toDate                     and
//                  :queueType = qd.type                                             and
//                  qd.id                  = ar.reject_seq_num                       and
//                  ('passall' = :stringLookup or ar.merchant_number = :longLookup)  and
//                  ar.reject_seq_num      = ars.reject_seq_num(+)                   and
//                  ar.reason_code         = arrc.reason_code(+)                     and
//                  ar.merchant_number     = m.merchant_number                       and
//                  qd.type                = qt.type                                 and
//                  ('MESN' = :appTypeDesc or qd.affiliate = :appTypeDesc)
//          group by qd.id,
//                   qd.type,
//                   qd.item_type,
//                   qd.owner,
//                   ar.settled_date,
//                   qd.source,
//                   qd.affiliate,
//                   qd.last_changed,
//                   qd.last_user,
//                   ar.merchant_name,
//                   ar.reject_seq_num,
//                   qd.locked_by,
//                   qt.status,
//                   qt.description,
//                   ar.merchant_number,
//                   ar.reason_code,
//                   arrc.description,
//                   ar.amount,
//                   ar.merchant_number,
//                   ars.reject_status,
//                   m.dmagent,
//                   m.transit_routng_num
//          order by ar.settled_date asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                qd.type                 type,\n                qd.item_type            item_type,\n                qd.owner                owner,\n                ar.settled_date         date_created,\n                qd.source               source,\n                qd.affiliate            affiliate,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                ar.merchant_name        description,\n                ar.reject_seq_num       control,\n                qd.locked_by            locked_by,\n                get_queue_note_count(ar.reject_seq_num,qd.type)   note_count,\n                qt.status               status,\n                qt.description          type_desc,\n                ar.merchant_number      merchant_number,\n                ar.reason_code          reason_code,\n                arrc.description        code_description,\n                ar.amount               amount,\n                decode(ars.reject_status, null, 'NEW', ars.reject_status) reject_status,\n                m.dmagent               association,\n                m.transit_routng_num    transit_routing_num\n        from    q_data                  qd,\n                q_types                 qt,\n                ach_rejects             ar,\n                ach_reject_reason_codes arrc,\n                ach_reject_status       ars,\n                mif                     m\n        where   ar.settled_date between  :1  and  :2                      and\n                 :3  = qd.type                                             and\n                qd.id                  = ar.reject_seq_num                       and\n                ('passall' =  :4  or ar.merchant_number =  :5 )  and\n                ar.reject_seq_num      = ars.reject_seq_num(+)                   and\n                ar.reason_code         = arrc.reason_code(+)                     and\n                ar.merchant_number     = m.merchant_number                       and\n                qd.type                = qt.type                                 and\n                ('MESN' =  :6  or qd.affiliate =  :7 )\n        group by qd.id,\n                 qd.type,\n                 qd.item_type,\n                 qd.owner,\n                 ar.settled_date,\n                 qd.source,\n                 qd.affiliate,\n                 qd.last_changed,\n                 qd.last_user,\n                 ar.merchant_name,\n                 ar.reject_seq_num,\n                 qd.locked_by,\n                 qt.status,\n                 qt.description,\n                 ar.merchant_number,\n                 ar.reason_code,\n                 arrc.description,\n                 ar.amount,\n                 ar.merchant_number,\n                 ars.reject_status,\n                 m.dmagent,\n                 m.transit_routng_num\n        order by ar.settled_date asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AchRejectLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,queueType);
   __sJT_st.setString(4,stringLookup);
   __sJT_st.setLong(5,longLookup);
   __sJT_st.setString(6,appTypeDesc);
   __sJT_st.setString(7,appTypeDesc);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AchRejectLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:277^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        lookupResults.add(new RejectData(rs));
      }
      
      rs.close();
      it.close();
      
      lastFromMonth   = fromMonth;
      lastFromDay     = fromDay;
      lastFromYear    = fromYear;
      lastToMonth     = toMonth;
      lastToDay       = toDay;
      lastToYear      = toYear;
      lastLookupValue = lookupValue;
    }
    catch(Exception e)
    {
      logEntry("getIndividualData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      
      cleanUp();
    }
  }


  private void createSortedTree()
  {
    sortedResults = null;
    sortedResults = new TreeSet(rdc);
    sortedResults.addAll(lookupResults);
  }

  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** ACCESSORS
  */
  public String getLookupValue()
  {
    return lookupValue;
  }
  public void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public int getAction()
  {
    return this.action;
  }
  public void setAction(int action)
  {
    this.actionDescription = "Ach Reject";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public void resetFlags()
  {
    this.achOpen              = -1;
    this.achCompleted         = -1;

    this.riskOpen             = -1;
    this.riskCompleted        = -1;

    this.collectionsOpen      = -1;
    this.collectionsCompleted = -1;

    this.legalOpen            = -1;
    this.legalCompleted       = -1;
  }

  public void  setAchOpen()
  {
    this.achOpen = 1;
  }

  public void  setAchCompleted()
  {
    this.achCompleted = 1;
  }

  public void  setRiskOpen()
  {
    this.riskOpen = 1;
  }

  public void  setRiskCompleted()
  {
    this.riskCompleted = 1;
  }

  public void  setCollectionsOpen()
  {
    this.collectionsOpen = 1;
  }

  public void  setCollectionsCompleted()
  {
    this.collectionsCompleted = 1;
  }

  public void  setLegalOpen()
  {
    this.legalOpen = 1;
  }

  public void  setLegalCompleted()
  {
    this.legalCompleted = 1;
  }


  public void setSortBy(String sortBy)
  {
    try
    {
      rdc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
  
  public void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
  public boolean isSubmitted()
  {
    return this.submitted;
  }
  
  /*********************************************************
  **
  ** SUBCLASS RejectData
  **
  **********************************************************/ 
  public class RejectData
    implements Serializable
  {
    public String Id;
    public String Type;
    public String TypeDesc;
    public String ItemType;
    public String Owner;
    public String DateCreated;
    public String Source;
    public String Affiliate;
    public String LastChanged;
    public String LastUser;
    public String Description;
    public String Control;
    public String LockedBy;
    public String NoteCount;
    public String Status;
    public String ReasonCode;
    public String CodeDescription;
    public double Amount;
    public String AmountString;
    public String MerchantNumber;
    public String RejectStatus;
    public String Association;
    public String TransitRoutingNum;

    public RejectData()
    {
      Id                = "";
      Type              = "";
      TypeDesc          = "";
      ItemType          = "";
      Owner             = "";
      DateCreated       = "";
      Source            = "";
      Affiliate         = "";
      LastChanged       = "";
      LastUser          = "";
      Description       = "";
      Control           = "";
      LockedBy          = "";
      NoteCount         = "";
      Status            = "";
      ReasonCode        = "";
      CodeDescription   = "";
      Amount            = 0.0;
      AmountString      = "";
      MerchantNumber    = "";
      RejectStatus      = "";
      Association       = "";
      TransitRoutingNum = "";
    }
    
    public RejectData(ResultSet rs)
    {
      try
      {
        Id                   = processStringField(rs.getString("id"));
        Type                 = processStringField(rs.getString("type"));
        TypeDesc             = processStringField(rs.getString("type_desc"));
        ItemType             = processStringField(rs.getString("item_type"));
        Owner                = processStringField(rs.getString("owner"));
        DateCreated          = DateTimeFormatter.getFormattedDate(rs.getTimestamp("date_created"), "MM/dd/yy HH:mm");
        Source               = processStringField(rs.getString("source"));
        Affiliate            = processStringField(rs.getString("affiliate"));
        LastChanged          = DateTimeFormatter.getFormattedDate(rs.getTimestamp("last_changed"), "MM/dd/yy HH:mm");
        LastUser             = processStringField(rs.getString("last_user"));
        Description          = processStringField(rs.getString("description"));
        Control              = processStringField(rs.getString("control"));
        LockedBy             = processStringField(rs.getString("locked_by"));
        NoteCount            = processStringField(rs.getString("note_count"));
        Status               = processStringField(rs.getString("status"));
        ReasonCode           = processStringField(rs.getString("reason_code"));
        CodeDescription      = processStringField(rs.getString("code_description"));
        Amount               = rs.getDouble("amount");
        AmountString         = MesMath.toCurrency(rs.getDouble("amount"));
        MerchantNumber       = processStringField(rs.getString("merchant_number"));
        RejectStatus         = processStringField(rs.getString("reject_status"));
        Association          = processStringField(rs.getString("association"));
        TransitRoutingNum    = processStringField(rs.getString("transit_routing_num"));
      }
      catch(Exception e)
      {
        logEntry("reading rs()", e.toString());
      }
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "" : fieldData);
    }
  }
  
  public class RejectDataComparator
    implements Comparator, Serializable
  {
    public final static int   SB_ID                   = 1;
    public final static int   SB_TYPE                 = 2;
    public final static int   SB_ITEM_TYPE            = 3;
    public final static int   SB_OWNER                = 4;
    public final static int   SB_DATE_CREATED         = 5;
    public final static int   SB_SOURCE               = 6;
    public final static int   SB_AFFILIATE            = 7;
    public final static int   SB_LAST_CHANGED         = 8;
    public final static int   SB_LAST_USER            = 9;
    public final static int   SB_DESCRIPTION          = 10;
    public final static int   SB_CONTROL              = 11;
    public final static int   SB_LOCKED_BY            = 12;
    public final static int   SB_NOTE_COUNT           = 13;
    public final static int   SB_STATUS               = 14;
    public final static int   SB_MERCHANT_NUMBER      = 15;
    public final static int   SB_REASON_CODE          = 16;
    public final static int   SB_CODE_DESCRIPTION     = 17;
    public final static int   SB_AMOUNT               = 18;
    public final static int   SB_REJECT_STATUS        = 19;
    public final static int   SB_ASSOCIATION          = 20;
    public final static int   SB_TRANSIT_ROUTING_NUM  = 21;

    private int               sortBy;
    
    private boolean           sortAscending       = false;
    
    public RejectDataComparator()
    {
      this.sortBy = SB_DATE_CREATED;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_ID && sortBy <= SB_TRANSIT_ROUTING_NUM)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(RejectData o1, RejectData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_ID:
            compareString1 = o1.Id + o1.DateCreated;
            compareString2 = o2.Id + o2.DateCreated;
            break;
          
          case SB_TYPE:
            compareString1 = o1.Type + o1.DateCreated;
            compareString2 = o2.Type + o2.DateCreated;
            break;
          
          case SB_ITEM_TYPE:
            compareString1 = o1.ItemType + o1.DateCreated;
            compareString2 = o2.ItemType + o2.DateCreated;
            break;
          
          case SB_OWNER:
            compareString1 = o1.Owner + o1.DateCreated;
            compareString2 = o2.Owner + o2.DateCreated;
            break;

          case SB_DATE_CREATED:
            compareString1 = o1.DateCreated + o1.Id + o1.DateCreated;
            compareString2 = o2.DateCreated + o2.Id + o2.DateCreated;
            break;

          case SB_SOURCE:
            compareString1 = o1.Source + o1.DateCreated;
            compareString2 = o2.Source + o2.DateCreated;
            break;

          case SB_AFFILIATE:
            compareString1 = o1.Affiliate + o1.DateCreated;
            compareString2 = o2.Affiliate + o2.DateCreated;
            break;

          case SB_LAST_CHANGED:
            compareString1 = o1.LastChanged + o1.DateCreated;
            compareString2 = o2.LastChanged + o2.DateCreated;
            break;

          case SB_LAST_USER:
            compareString1 = o1.LastUser + o1.DateCreated;
            compareString2 = o2.LastUser + o2.DateCreated;
            break;

          case SB_DESCRIPTION:
            compareString1 = o1.Description + o1.DateCreated;
            compareString2 = o2.Description + o2.DateCreated;
            break;

          case SB_CONTROL:
            compareString1 = o1.Control + o1.DateCreated;
            compareString2 = o2.Control + o2.DateCreated;
            break;

          case SB_LOCKED_BY:
            compareString1 = o1.LockedBy + o1.DateCreated;
            compareString2 = o2.LockedBy + o2.DateCreated;
            break;

          case SB_NOTE_COUNT:
            compareString1 = o1.NoteCount + o1.DateCreated;
            compareString2 = o2.NoteCount + o2.DateCreated;
            break;

          case SB_STATUS:
            compareString1 = o1.Status + o1.DateCreated;
            compareString2 = o2.Status + o2.DateCreated;
            break;

          case SB_MERCHANT_NUMBER:
            compareString1 = o1.MerchantNumber + o1.DateCreated;
            compareString2 = o2.MerchantNumber + o2.DateCreated;
            break;

          case SB_REASON_CODE:
            compareString1 = o1.ReasonCode + o1.DateCreated;
            compareString2 = o2.ReasonCode + o2.DateCreated;
            break;

          case SB_CODE_DESCRIPTION:
            compareString1 = o1.CodeDescription + o1.DateCreated;
            compareString2 = o2.CodeDescription + o2.DateCreated;
            break;

          case SB_AMOUNT:
            compareString1 = o1.Amount + o1.DateCreated;
            compareString2 = o2.Amount + o2.DateCreated;
            break;

          case SB_REJECT_STATUS:
            compareString1 = o1.RejectStatus + o1.DateCreated;
            compareString2 = o2.RejectStatus + o2.DateCreated;
            break;

          case SB_ASSOCIATION:
            compareString1 = o1.Association + o1.DateCreated;
            compareString2 = o2.Association + o2.DateCreated;
            break;

          case SB_TRANSIT_ROUTING_NUM:
            compareString1 = o1.TransitRoutingNum + o1.DateCreated;
            compareString2 = o2.TransitRoutingNum + o2.DateCreated;
            break;
         
          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(RejectData o1, RejectData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_ID:
            compareString1 = o1.Id + o1.DateCreated;
            compareString2 = o2.Id + o2.DateCreated;
            break;
          
          case SB_TYPE:
            compareString1 = o1.Type + o1.DateCreated;
            compareString2 = o2.Type + o2.DateCreated;
            break;
          
          case SB_ITEM_TYPE:
            compareString1 = o1.ItemType + o1.DateCreated;
            compareString2 = o2.ItemType + o2.DateCreated;
            break;
          
          case SB_OWNER:
            compareString1 = o1.Owner + o1.DateCreated;
            compareString2 = o2.Owner + o2.DateCreated;
            break;

          case SB_DATE_CREATED:
            compareString1 = o1.DateCreated + o1.Id + o1.DateCreated;
            compareString2 = o2.DateCreated + o2.Id + o2.DateCreated;
            break;

          case SB_SOURCE:
            compareString1 = o1.Source + o1.DateCreated;
            compareString2 = o2.Source + o2.DateCreated;
            break;

          case SB_AFFILIATE:
            compareString1 = o1.Affiliate + o1.DateCreated;
            compareString2 = o2.Affiliate + o2.DateCreated;
            break;

          case SB_LAST_CHANGED:
            compareString1 = o1.LastChanged + o1.DateCreated;
            compareString2 = o2.LastChanged + o2.DateCreated;
            break;

          case SB_LAST_USER:
            compareString1 = o1.LastUser + o1.DateCreated;
            compareString2 = o2.LastUser + o2.DateCreated;
            break;

          case SB_DESCRIPTION:
            compareString1 = o1.Description + o1.DateCreated;
            compareString2 = o2.Description + o2.DateCreated;
            break;

          case SB_CONTROL:
            compareString1 = o1.Control + o1.DateCreated;
            compareString2 = o2.Control + o2.DateCreated;
            break;

          case SB_LOCKED_BY:
            compareString1 = o1.LockedBy + o1.DateCreated;
            compareString2 = o2.LockedBy + o2.DateCreated;
            break;

          case SB_NOTE_COUNT:
            compareString1 = o1.NoteCount + o1.DateCreated;
            compareString2 = o2.NoteCount + o2.DateCreated;
            break;

          case SB_STATUS:
            compareString1 = o1.Status + o1.DateCreated;
            compareString2 = o2.Status + o2.DateCreated;
            break;

          case SB_MERCHANT_NUMBER:
            compareString1 = o1.MerchantNumber + o1.DateCreated;
            compareString2 = o2.MerchantNumber + o2.DateCreated;
            break;

          case SB_REASON_CODE:
            compareString1 = o1.ReasonCode + o1.DateCreated;
            compareString2 = o2.ReasonCode + o2.DateCreated;
            break;

          case SB_CODE_DESCRIPTION:
            compareString1 = o1.CodeDescription + o1.DateCreated;
            compareString2 = o2.CodeDescription + o2.DateCreated;
            break;

          case SB_AMOUNT:
            compareString1 = o1.Amount + o1.DateCreated;
            compareString2 = o2.Amount + o2.DateCreated;
            break;

          case SB_REJECT_STATUS:
            compareString1 = o1.RejectStatus + o1.DateCreated;
            compareString2 = o2.RejectStatus + o2.DateCreated;
            break;

          case SB_ASSOCIATION:
            compareString1 = o1.Association + o1.DateCreated;
            compareString2 = o2.Association + o2.DateCreated;
            break;

          case SB_TRANSIT_ROUTING_NUM:
            compareString1 = o1.TransitRoutingNum + o1.DateCreated;
            compareString2 = o2.TransitRoutingNum + o2.DateCreated;
            break;
          
          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((RejectData)o1, (RejectData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((RejectData)o1, (RejectData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }
}/*@lineinfo:generated-code*/