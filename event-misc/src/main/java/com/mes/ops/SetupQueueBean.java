/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/SetupQueueBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 12/06/02 3:45p $
  Version            : $Revision: 14 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

public class SetupQueueBean extends QueueBean
{
  public void fillReportMenuItems()
  {
    addReportMenuItem(new ReportMenuItem("Account Setup Queue",               "mes_queue.jsp?queueType="  + QueueConstants.QUEUE_SETUP + "&queueStage=" + QueueConstants.Q_SETUP_NEW));
    addReportMenuItem(new ReportMenuItem("Account Setup Pending Queue",       "mes_queue.jsp?queueType="  + QueueConstants.QUEUE_SETUP + "&queueStage=" + QueueConstants.Q_SETUP_PENDING));
    addReportMenuItem(new ReportMenuItem("Account Setup QA Queue",            "mes_queue.jsp?queueType="  + QueueConstants.QUEUE_SETUP + "&queueStage=" + QueueConstants.Q_SETUP_QA));
    addReportMenuItem(new ReportMenuItem("Account Setup QA Correction Queue", "mes_queue.jsp?queueType="  + QueueConstants.QUEUE_SETUP + "&queueStage=" + QueueConstants.Q_SETUP_QA_CORRECTION));
    addReportMenuItem(new ReportMenuItem("Account Setup Export Queue",        "mes_queue.jsp?queueType="  + QueueConstants.QUEUE_SETUP + "&queueStage=" + QueueConstants.Q_SETUP_EXPORT));
    addReportMenuItem(new ReportMenuItem("Account Setup Manual Entry Queue",  "mes_queue.jsp?queueType="  + QueueConstants.QUEUE_SETUP + "&queueStage=" + QueueConstants.Q_SETUP_MANUAL));
    addReportMenuItem(new ReportMenuItem("Account Setup Data Overflow Queue", "mes_queue.jsp?queueType="  + QueueConstants.QUEUE_SETUP + "&queueStage=" + QueueConstants.Q_SETUP_DATA_OVERFLOW));
    addReportMenuItem(new ReportMenuItem("Account Setup Research",            "acctsetupresearch.jsp"));

    //addReportMenuItem(new ReportMenuItem("V-Number Queue", "mes_queue.jsp?queueType=" + QueueConstants.QUEUE_SETUP + "&queueStage=" + QueueConstants.Q_SETUP_VNUMBER));
    //addReportMenuItem(new ReportMenuItem("Manual Data Entry Queue", "mes_queue.jsp?queueType=" + QueueConstants.QUEUE_SETUP + "&queueStage=" + QueueConstants.Q_SETUP_MANUAL));
    //addReportMenuItem(new ReportMenuItem("Completed Queue", "mes_queue.jsp?queueType=" + QueueConstants.QUEUE_SETUP + "&queueStage=" + QueueConstants.Q_SETUP_COMPLETE));
  }
  public SetupQueueBean()
  {
    super("SetupQueueBean");
    fillReportMenuItems();
  }
  
  public SetupQueueBean(String msg)
  {
    super(msg);
    fillReportMenuItems();
  }
  
  public String getLockInfo(long controlNumber)
  {
    // get application sequence number from controlnumber
    return super.getLockInfo(this.getAppSeqNum(controlNumber));
  }
  
  public boolean setLock(String lockedBy, long controlNumber)
  {
    return super.setLock(this.getAppSeqNum(controlNumber), com.mes.ops.QueueConstants.QUEUE_SETUP, lockedBy);
  }
  
  public void getFunctionOptions(javax.servlet.jsp.JspWriter out, long appSeqNum)
  {
    try
    {
      out.println("<ul>");


      //out.println("  <li><a href=\"mes_app_assign.jsp?primaryKey=" + appSeqNum + "\">Terminal Application Assignment</a></li>");
      
      if(getQueueStage() == QueueConstants.Q_SETUP_EXPORT)
      {
        out.println("  <li><a href=\"mes_export_file.jsp?file=true&primaryKey="   + appSeqNum + "\">Export To File</a></li>");
        out.println("  <li><a href=\"mes_export_file.jsp?manual=true&primaryKey=" + appSeqNum + "\">Export To Manual Entry Queue</a></li>");
      }
      else if(getQueueStage() == QueueConstants.Q_SETUP_QA)
      {
        out.println("  <li><a href=\"mes_dataexpand.jsp?primaryKey="    + appSeqNum + "\">Review & Verify Data Expansion</a></li>");
      }
      else if(getQueueStage() == QueueConstants.Q_SETUP_MANUAL)
      {
        out.println("  <li><a href=\"mes_dataexpand.jsp?primaryKey="    + appSeqNum + "\">View Data Expansion Info To Manually Enter Into TSYS</a></li>");
      }
      else if(getQueueStage() == QueueConstants.Q_SETUP_DATA_OVERFLOW)
      {
        out.println("  <li><a href=\"mes_dataoverflow.jsp?primaryKey="  + appSeqNum + "\">View Tape File Overflow Data</a></li>");
      }
      else
      {
        out.println("  <li><a href=\"mes_dataexpand.jsp?primaryKey="    + appSeqNum + "\">Data Expansion</a></li>");
      }

      out.println("  <li><a href=\"mes_credit_notes.jsp?appSeqNum="     + appSeqNum + "&queueStage=" + queueStage + "\">Notes</a></li>");
      out.println("</ul>");

      //out.println("  <li><a href=\"mes_fax_file.jsp?primaryKey=" + appSeqNum + "\">Fax File Export</a></li>");
      //out.println("  <li><a href=\"mes_setup_status.jsp?primaryKey=" + appSeqNum + "\">Account Setup Status</a></li>");
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFunctionOptions: " + e.toString());
    }
  }
  
}
