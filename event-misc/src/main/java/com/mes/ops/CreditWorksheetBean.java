/*@lineinfo:filename=CreditWorksheetBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/CreditWorksheetBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 3/24/03 10:05a $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class CreditWorksheetBean extends SQLJConnectionBase
{

  public static final String  PRODUCT_TYPE_RETAIL         = "RETAIL";
  public static final String  PRODUCT_TYPE_RESTAURANT     = "RESTAURANT";
  public static final String  PRODUCT_TYPE_MOTO           = "MOTO";
  public static final String  PRODUCT_TYPE_OTHER          = "OTHER";

  public static final String  SITE_INSPEC_OOH             = "OOH";
  public static final String  SITE_INSPEC_SM              = "SM";
  public static final String  SITE_INSPEC_OB              = "OB";
  public static final String  SITE_INSPEC_IP              = "IP";
  public static final String  SITE_INSPEC_OTHER           = "OTHER";

  private final int           DANDB_MAX_CHARS             = 200;
  private final int           NOTES_MAX_CHARS             = 512;
  private final int           CREDIT_NOTES_MAX_CHARS      = 512;

  private Vector              errors                      = new Vector();

//********************static variables*************************
  private String              controlNumber               = "";
  private String              merchNumber                 = "";
  private String              dba                         = "";
  private String              monthlyVol                  = "";
  private String              averageTicket               = "";
  private String              appTypeDesc                 = "";
  private String              bankName                    = "";
  private String              creditScore                 = "";
  private String              mesCreditScore              = "";
  private String              mesAutoApproveThreshold     = "";
  private String              mesAutoDeclineThreshold     = "";
  private String              mesCreditScoreDetail        = "";
  private String              matchNumber                 = "";
  private String              repName                     = "";
//*************************************************************

  private String              analystLoginId              = "";
  private String              tradelines                  = "";
  private String              chargeOffs                  = "";
  private String              ho                          = "";
  private String              collections                 = "";
  private String              judgementsLiens             = "";
  private String              safeScan                    = "";
  private String              delinq30                    = "";
  private String              delinq60                    = "";
  private String              delinq90                    = "";
  private String              dandb                       = "";
  private String              priorProcessor              = "";
  private String              nameOfPrior                 = "";
  private String              statementsProvided          = "";
  private String              siteInspection              = "";
  private String              domainRegistration          = "";
  private String              websiteUp                   = "";
  private String              phoneConfirmed              = "";
  private String              merchantContacted           = "";
  private String              businessLicense             = "";
  private String              signaturePage               = "";
  private String              voidedCheck                 = "";
  private String              taxReturn                   = "";
  private String              financials                  = "";
  private String              cashReserve                 = "";
  private String              otherSupport                = "";
  private String              notes                       = "";
  private String              creditNotes                 = "";
  private String              productType                 = "";
  private String              percentMoto                 = "";
  private String              homeAddressMatch            = "";
  private String              bankAddressMatch            = "";

  public CreditWorksheetBean()
  {
  }



  public void getStaticData(long pk)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {
      
      connect();
   
      /*@lineinfo:generated-code*//*@lineinfo:132^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merc_cntrl_number,
//                  merch_number,
//                  merch_business_name,
//                  MERCH_MONTH_VISA_MC_SALES,
//                  merch_average_cc_tran
//          from    merchant
//          where   app_seq_num   = :pk 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merc_cntrl_number,\n                merch_number,\n                merch_business_name,\n                MERCH_MONTH_VISA_MC_SALES,\n                merch_average_cc_tran\n        from    merchant\n        where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.CreditWorksheetBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.CreditWorksheetBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        controlNumber      = isBlank(rs.getString("merc_cntrl_number"))          ? "" :  rs.getString("merc_cntrl_number"); 
        merchNumber        = isBlank(rs.getString("merch_number"))               ? "" :  rs.getString("merch_number");
        dba                = isBlank(rs.getString("merch_business_name"))        ? "" :  rs.getString("merch_business_name");
        monthlyVol         = isBlank(rs.getString("MERCH_MONTH_VISA_MC_SALES"))  ? "" :  rs.getString("MERCH_MONTH_VISA_MC_SALES");
        averageTicket      = isBlank(rs.getString("merch_average_cc_tran"))      ? "" :  rs.getString("merch_average_cc_tran");
      }
 
      it.close();


      /*@lineinfo:generated-code*//*@lineinfo:157^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  appsrctype_code,
//                  app_user_login
//          from    application
//          where   app_seq_num   = :pk 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  appsrctype_code,\n                app_user_login\n        from    application\n        where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.CreditWorksheetBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.CreditWorksheetBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:163^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        appTypeDesc        = isBlank(rs.getString("appsrctype_code"))            ? "" :  rs.getString("appsrctype_code"); 
        repName            = isBlank(rs.getString("app_user_login"))             ? "" :  rs.getString("app_user_login");
      }
 
      it.close();


      /*@lineinfo:generated-code*//*@lineinfo:176^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchbank_name
//          from    merchbank
//          where   app_seq_num   = :pk 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchbank_name\n        from    merchbank\n        where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.CreditWorksheetBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.CreditWorksheetBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:181^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        bankName           = isBlank(rs.getString("merchbank_name"))             ? "" :  rs.getString("merchbank_name"); 
      }
 
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:192^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  score
//          from    credit_scores
//          where   app_seq_num   = :pk 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  score\n        from    credit_scores\n        where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.CreditWorksheetBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.CreditWorksheetBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        creditScore        = isBlank(rs.getString("score"))                         ? "" :  rs.getString("score"); 
      }
 
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:208^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  match_seq_num
//          from    match_requests
//          where   app_seq_num   = :pk 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  match_seq_num\n        from    match_requests\n        where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.CreditWorksheetBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.CreditWorksheetBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:213^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        matchNumber       = isBlank(rs.getString("match_seq_num"))                  ? "" :  rs.getString("match_seq_num"); 
      }
 
      it.close();

/*
      #sql [Ctx] it =
      {
        select  cms.score, 
                cms.score_detailtext, 
                cm.autoapprove_threshold, 
                cm.autodecline_threshold
        from    credit_matrix_score cms, 
                credit_matrix cm
        where   cms.app_seq_num = :pk and
                cms.cm_id = cm.cm_id
      };

      rs = it.getResultSet();

      if(rs.next())
      {
        mesCreditScore            = isBlank(rs.getString("score"))                  ? "" :  rs.getString("score"); 
        mesAutoApproveThreshold   = isBlank(rs.getString("autoapprove_threshold"))  ? "" :  rs.getString("autoapprove_threshold");
        mesAutoDeclineThreshold   = isBlank(rs.getString("autodecline_threshold"))  ? "" :  rs.getString("autodecline_threshold");
        mesCreditScoreDetail      = isBlank(rs.getString("score_detailtext"))       ? "" :  StringUtilities.replace(rs.getString("score_detailtext"),"\n","<br>");
      }
 
      it.close();

      #sql [Ctx] it =
      {
        select  pc.pos_desc
        from    merch_pos    mp, 
                pos_category pc
        where   mp.app_seq_num   = :pk and 
                mp.pos_code      = pc.pos_code
      };

      rs = it.getResultSet();

      if(rs.next())
      {
        productType      = isBlank(rs.getString("pos_desc"))          ? "" :  rs.getString("pos_desc"); 
      }
 
      it.close();
*/

    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }

  public void getData(long pk)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {
      
      connect();
   
      /*@lineinfo:generated-code*//*@lineinfo:290^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//  
//          from    credit_worksheet_info
//  
//          where   app_seq_num   = :pk 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n\n        from    credit_worksheet_info\n\n        where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.CreditWorksheetBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.CreditWorksheetBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:297^7*/

      rs = it.getResultSet();

      if(rs.next())
      {

        analystLoginId      = isBlank(rs.getString("ANALYST_LOGIN_ID"))     ? "" :   rs.getString("ANALYST_LOGIN_ID"); 
        tradelines          = isBlank(rs.getString("TRADELINES"))           ? "" :  rs.getString("TRADELINES");
        chargeOffs          = isBlank(rs.getString("CHARGE_OFFS"))          ? "" :  rs.getString("CHARGE_OFFS");
        ho                  = isBlank(rs.getString("HO"))                   ? "" :  rs.getString("HO");
        collections         = isBlank(rs.getString("COLLECTIONS"))          ? "" :  rs.getString("COLLECTIONS");
        judgementsLiens     = isBlank(rs.getString("JUDGEMENT_LIENS"))      ? "" :  rs.getString("JUDGEMENT_LIENS");
        safeScan            = isBlank(rs.getString("SAFESCAN"))             ? "" :  rs.getString("SAFESCAN");
        delinq30            = isBlank(rs.getString("DELINQ30"))             ? "" :  rs.getString("DELINQ30");
        delinq60            = isBlank(rs.getString("DELINQ60"))             ? "" :  rs.getString("DELINQ60");
        delinq90            = isBlank(rs.getString("DELINQ90"))             ? "" :  rs.getString("DELINQ90");
        dandb               = isBlank(rs.getString("DANDB"))                ? "" :  rs.getString("DANDB");
        priorProcessor      = isBlank(rs.getString("PRIOR_PROCESSOR"))      ? "" :  rs.getString("PRIOR_PROCESSOR");
        nameOfPrior         = isBlank(rs.getString("NAME_OF_PRIOR"))        ? "" :  rs.getString("NAME_OF_PRIOR");
        statementsProvided  = isBlank(rs.getString("STATEMENTS_PROVIDED"))  ? "" :  rs.getString("STATEMENTS_PROVIDED");
        siteInspection      = isBlank(rs.getString("SITE_INSPECTION"))      ? "" :  rs.getString("SITE_INSPECTION");
        domainRegistration  = isBlank(rs.getString("DOMAIN_REGISTRATION"))  ? "" :  rs.getString("DOMAIN_REGISTRATION");
        websiteUp           = isBlank(rs.getString("WEBSITE_UP"))           ? "" :  rs.getString("WEBSITE_UP");
        phoneConfirmed      = isBlank(rs.getString("PHONE_CONFIRMED"))      ? "" :  rs.getString("PHONE_CONFIRMED");
        merchantContacted   = isBlank(rs.getString("MERCHANT_CONTACTED"))   ? "" :  rs.getString("MERCHANT_CONTACTED");
        businessLicense     = isBlank(rs.getString("BUSINESS_LICENSE"))     ? "" :  rs.getString("BUSINESS_LICENSE");
        signaturePage       = isBlank(rs.getString("SIGNATURE_PAGE"))       ? "" :  rs.getString("SIGNATURE_PAGE");
        voidedCheck         = isBlank(rs.getString("VOIDED_CHECK"))         ? "" :  rs.getString("VOIDED_CHECK");
        taxReturn           = isBlank(rs.getString("TAX_RETURN"))           ? "" :  rs.getString("TAX_RETURN");
        financials          = isBlank(rs.getString("FINANCIALS"))           ? "" :  rs.getString("FINANCIALS");
        cashReserve         = isBlank(rs.getString("CASH_RESERVE"))         ? "" :  rs.getString("CASH_RESERVE");
        otherSupport        = isBlank(rs.getString("OTHER_SUPPORT"))        ? "" :  rs.getString("OTHER_SUPPORT");
        notes               = isBlank(rs.getString("NOTES"))                ? "" :  rs.getString("NOTES");
        creditNotes         = isBlank(rs.getString("CREDIT_NOTES"))         ? "" :  rs.getString("CREDIT_NOTES");
        productType         = isBlank(rs.getString("PRODUCT_TYPE"))         ? "" :  rs.getString("PRODUCT_TYPE");
        percentMoto         = isBlank(rs.getString("PERCENT_MOTO"))         ? "" :  rs.getString("PERCENT_MOTO");
        homeAddressMatch    = isBlank(rs.getString("HOME_ADDRESS_MATCH"))   ? "" :  rs.getString("HOME_ADDRESS_MATCH");
        bankAddressMatch    = isBlank(rs.getString("BANK_ADDRESS_MATCH"))   ? "" :  rs.getString("BANK_ADDRESS_MATCH");

      }
      else
      {

        /*@lineinfo:generated-code*//*@lineinfo:341^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  BK_COUNT,
//                    MORT_COUNT, 
//                    DEROG_COUNT 
//        
//            from    credit_scores
//  
//            where   app_seq_num   = :pk 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  BK_COUNT,\n                  MORT_COUNT, \n                  DEROG_COUNT \n      \n          from    credit_scores\n\n          where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.CreditWorksheetBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.CreditWorksheetBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:350^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          creditNotes   = "Bankruptcy Count = " + rs.getString("bk_count") + "\n" + "Mortgage Count = " + rs.getString("mort_count") + "\n" + "Derog Count = " + rs.getString("derog_count") + "\n";
          delinq90      = rs.getString("derog_count");
        }

        it.close();
 
        /*@lineinfo:generated-code*//*@lineinfo:362^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_prior_cc_accp_flag,
//                    merch_prior_processor,
//                    merch_prior_statements,
//                    industype_code,
//                    merch_mail_phone_sales
//          
//            from    merchant
//  
//            where   app_seq_num   = :pk 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_prior_cc_accp_flag,\n                  merch_prior_processor,\n                  merch_prior_statements,\n                  industype_code,\n                  merch_mail_phone_sales\n        \n          from    merchant\n\n          where   app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.CreditWorksheetBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.CreditWorksheetBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:373^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          priorProcessor      = isBlank(rs.getString("merch_prior_cc_accp_flag"))   ? "" :  rs.getString("merch_prior_cc_accp_flag");
          nameOfPrior         = isBlank(rs.getString("merch_prior_processor"))      ? "" :  rs.getString("merch_prior_processor");
          statementsProvided  = isBlank(rs.getString("merch_prior_statements"))     ? "" :  rs.getString("merch_prior_statements");
          percentMoto         = isBlank(rs.getString("merch_mail_phone_sales"))     ? "" :  rs.getString("merch_mail_phone_sales");
         
          switch(rs.getInt("industype_code"))
          {
            case mesConstants.APP_INDUSTYPE_RETAIL:
            case mesConstants.APP_INDUSTYPE_RETAIL_GOV_PUR_CARD:
              productType = PRODUCT_TYPE_RETAIL;
            break;
            
            case mesConstants.APP_INDUSTYPE_RESTAURANT:
              productType = PRODUCT_TYPE_RESTAURANT;
            break;
            
            case mesConstants.APP_INDUSTYPE_INTERNET:
            case mesConstants.APP_INDUSTYPE_MOTO_GOV_PUR_CARD:
            case mesConstants.APP_INDUSTYPE_MOTO:
              productType = PRODUCT_TYPE_MOTO;
            break;

            default:
              productType = PRODUCT_TYPE_OTHER;
            break;
          }

        }

      }
 
      it.close();

      //if percent moto greater than 50% we make this guy a moto product type no matter what
      if(!isBlank(percentMoto))
      {

        if(Double.parseDouble(percentMoto) > 50.0)
        {
          productType = PRODUCT_TYPE_MOTO;
        }

      }

    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }




  /*
  ** METHOD public void submitData()
  **
  */

  public void submitData(long pk, UserBean user)
  {
    try
    {

      if(recordExists(pk))
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:452^9*/

//  ************************************************************
//  #sql [Ctx] { update  credit_worksheet_info
//            set     ANALYST_LOGIN_ID      =    :analystLoginId,
//                    TRADELINES            =    :tradelines,
//                    CHARGE_OFFS           =    :chargeOffs,
//                    HO                    =    :ho,
//                    COLLECTIONS           =    :collections,
//                    JUDGEMENT_LIENS       =    :judgementsLiens,
//                    SAFESCAN              =    :safeScan,
//                    DELINQ30              =    :delinq30,
//                    DELINQ60              =    :delinq60,
//                    DELINQ90              =    :delinq90,
//                    DANDB                 =    :dandb,
//                    PRIOR_PROCESSOR       =    :priorProcessor,
//                    NAME_OF_PRIOR         =    :nameOfPrior,
//                    STATEMENTS_PROVIDED   =    :statementsProvided,
//                    SITE_INSPECTION       =    :siteInspection,
//                    DOMAIN_REGISTRATION   =    :domainRegistration,
//                    WEBSITE_UP            =    :websiteUp,
//                    PHONE_CONFIRMED       =    :phoneConfirmed,
//                    MERCHANT_CONTACTED    =    :merchantContacted,
//                    BUSINESS_LICENSE      =    :businessLicense,
//                    SIGNATURE_PAGE        =    :signaturePage,
//                    VOIDED_CHECK          =    :voidedCheck,
//                    TAX_RETURN            =    :taxReturn,
//                    FINANCIALS            =    :financials,
//                    CASH_RESERVE          =    :cashReserve,
//                    OTHER_SUPPORT         =    :otherSupport,
//                    NOTES                 =    :notes,
//                    CREDIT_NOTES          =    :creditNotes,
//                    PRODUCT_TYPE          =    :productType,
//                    PERCENT_MOTO          =    :percentMoto,
//                    HOME_ADDRESS_MATCH    =    :homeAddressMatch,
//                    BANK_ADDRESS_MATCH    =    :bankAddressMatch
//            where   APP_SEQ_NUM           =    :pk
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  credit_worksheet_info\n          set     ANALYST_LOGIN_ID      =     :1 ,\n                  TRADELINES            =     :2 ,\n                  CHARGE_OFFS           =     :3 ,\n                  HO                    =     :4 ,\n                  COLLECTIONS           =     :5 ,\n                  JUDGEMENT_LIENS       =     :6 ,\n                  SAFESCAN              =     :7 ,\n                  DELINQ30              =     :8 ,\n                  DELINQ60              =     :9 ,\n                  DELINQ90              =     :10 ,\n                  DANDB                 =     :11 ,\n                  PRIOR_PROCESSOR       =     :12 ,\n                  NAME_OF_PRIOR         =     :13 ,\n                  STATEMENTS_PROVIDED   =     :14 ,\n                  SITE_INSPECTION       =     :15 ,\n                  DOMAIN_REGISTRATION   =     :16 ,\n                  WEBSITE_UP            =     :17 ,\n                  PHONE_CONFIRMED       =     :18 ,\n                  MERCHANT_CONTACTED    =     :19 ,\n                  BUSINESS_LICENSE      =     :20 ,\n                  SIGNATURE_PAGE        =     :21 ,\n                  VOIDED_CHECK          =     :22 ,\n                  TAX_RETURN            =     :23 ,\n                  FINANCIALS            =     :24 ,\n                  CASH_RESERVE          =     :25 ,\n                  OTHER_SUPPORT         =     :26 ,\n                  NOTES                 =     :27 ,\n                  CREDIT_NOTES          =     :28 ,\n                  PRODUCT_TYPE          =     :29 ,\n                  PERCENT_MOTO          =     :30 ,\n                  HOME_ADDRESS_MATCH    =     :31 ,\n                  BANK_ADDRESS_MATCH    =     :32 \n          where   APP_SEQ_NUM           =     :33";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.ops.CreditWorksheetBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,analystLoginId);
   __sJT_st.setString(2,tradelines);
   __sJT_st.setString(3,chargeOffs);
   __sJT_st.setString(4,ho);
   __sJT_st.setString(5,collections);
   __sJT_st.setString(6,judgementsLiens);
   __sJT_st.setString(7,safeScan);
   __sJT_st.setString(8,delinq30);
   __sJT_st.setString(9,delinq60);
   __sJT_st.setString(10,delinq90);
   __sJT_st.setString(11,dandb);
   __sJT_st.setString(12,priorProcessor);
   __sJT_st.setString(13,nameOfPrior);
   __sJT_st.setString(14,statementsProvided);
   __sJT_st.setString(15,siteInspection);
   __sJT_st.setString(16,domainRegistration);
   __sJT_st.setString(17,websiteUp);
   __sJT_st.setString(18,phoneConfirmed);
   __sJT_st.setString(19,merchantContacted);
   __sJT_st.setString(20,businessLicense);
   __sJT_st.setString(21,signaturePage);
   __sJT_st.setString(22,voidedCheck);
   __sJT_st.setString(23,taxReturn);
   __sJT_st.setString(24,financials);
   __sJT_st.setString(25,cashReserve);
   __sJT_st.setString(26,otherSupport);
   __sJT_st.setString(27,notes);
   __sJT_st.setString(28,creditNotes);
   __sJT_st.setString(29,productType);
   __sJT_st.setString(30,percentMoto);
   __sJT_st.setString(31,homeAddressMatch);
   __sJT_st.setString(32,bankAddressMatch);
   __sJT_st.setLong(33,pk);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:488^9*/

      }
      // update existing records
      else
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:496^9*/

//  ************************************************************
//  #sql [Ctx] { insert into credit_worksheet_info
//            ( 
//              ANALYST_LOGIN_ID,
//              TRADELINES,
//              CHARGE_OFFS,
//              HO,
//              COLLECTIONS,
//              JUDGEMENT_LIENS,
//              SAFESCAN,
//              DELINQ30,
//              DELINQ60,
//              DELINQ90,
//              DANDB,
//              PRIOR_PROCESSOR,
//              NAME_OF_PRIOR,
//              STATEMENTS_PROVIDED,
//              SITE_INSPECTION,
//              DOMAIN_REGISTRATION,
//              WEBSITE_UP,
//              PHONE_CONFIRMED,
//              MERCHANT_CONTACTED,
//              BUSINESS_LICENSE,
//              SIGNATURE_PAGE,
//              VOIDED_CHECK,
//              TAX_RETURN,
//              FINANCIALS,
//              CASH_RESERVE,
//              OTHER_SUPPORT,
//              NOTES,
//              CREDIT_NOTES,
//              PRODUCT_TYPE,
//              PERCENT_MOTO,
//              HOME_ADDRESS_MATCH,
//              BANK_ADDRESS_MATCH,
//              APP_SEQ_NUM
//            )
//            
//            values
//            ( 
//              :analystLoginId,
//              :tradelines,
//              :chargeOffs,
//              :ho,
//              :collections,
//              :judgementsLiens,
//              :safeScan,
//              :delinq30,
//              :delinq60,
//              :delinq90,
//              :dandb,
//              :priorProcessor,
//              :nameOfPrior,
//              :statementsProvided,
//              :siteInspection,
//              :domainRegistration,
//              :websiteUp,
//              :phoneConfirmed,
//              :merchantContacted,
//              :businessLicense,
//              :signaturePage,
//              :voidedCheck,
//              :taxReturn,
//              :financials,
//              :cashReserve,
//              :otherSupport,
//              :notes,
//              :creditNotes,
//              :productType,
//              :percentMoto,
//              :homeAddressMatch,
//              :bankAddressMatch,
//              :pk
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into credit_worksheet_info\n          ( \n            ANALYST_LOGIN_ID,\n            TRADELINES,\n            CHARGE_OFFS,\n            HO,\n            COLLECTIONS,\n            JUDGEMENT_LIENS,\n            SAFESCAN,\n            DELINQ30,\n            DELINQ60,\n            DELINQ90,\n            DANDB,\n            PRIOR_PROCESSOR,\n            NAME_OF_PRIOR,\n            STATEMENTS_PROVIDED,\n            SITE_INSPECTION,\n            DOMAIN_REGISTRATION,\n            WEBSITE_UP,\n            PHONE_CONFIRMED,\n            MERCHANT_CONTACTED,\n            BUSINESS_LICENSE,\n            SIGNATURE_PAGE,\n            VOIDED_CHECK,\n            TAX_RETURN,\n            FINANCIALS,\n            CASH_RESERVE,\n            OTHER_SUPPORT,\n            NOTES,\n            CREDIT_NOTES,\n            PRODUCT_TYPE,\n            PERCENT_MOTO,\n            HOME_ADDRESS_MATCH,\n            BANK_ADDRESS_MATCH,\n            APP_SEQ_NUM\n          )\n          \n          values\n          ( \n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.ops.CreditWorksheetBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,analystLoginId);
   __sJT_st.setString(2,tradelines);
   __sJT_st.setString(3,chargeOffs);
   __sJT_st.setString(4,ho);
   __sJT_st.setString(5,collections);
   __sJT_st.setString(6,judgementsLiens);
   __sJT_st.setString(7,safeScan);
   __sJT_st.setString(8,delinq30);
   __sJT_st.setString(9,delinq60);
   __sJT_st.setString(10,delinq90);
   __sJT_st.setString(11,dandb);
   __sJT_st.setString(12,priorProcessor);
   __sJT_st.setString(13,nameOfPrior);
   __sJT_st.setString(14,statementsProvided);
   __sJT_st.setString(15,siteInspection);
   __sJT_st.setString(16,domainRegistration);
   __sJT_st.setString(17,websiteUp);
   __sJT_st.setString(18,phoneConfirmed);
   __sJT_st.setString(19,merchantContacted);
   __sJT_st.setString(20,businessLicense);
   __sJT_st.setString(21,signaturePage);
   __sJT_st.setString(22,voidedCheck);
   __sJT_st.setString(23,taxReturn);
   __sJT_st.setString(24,financials);
   __sJT_st.setString(25,cashReserve);
   __sJT_st.setString(26,otherSupport);
   __sJT_st.setString(27,notes);
   __sJT_st.setString(28,creditNotes);
   __sJT_st.setString(29,productType);
   __sJT_st.setString(30,percentMoto);
   __sJT_st.setString(31,homeAddressMatch);
   __sJT_st.setString(32,bankAddressMatch);
   __sJT_st.setLong(33,pk);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:571^9*/

      }
    }
    catch(Exception e)
    {
      logEntry("setData()", e.toString());
      addError("Error: Complications occurred while storing data!  Please resubmit.");
    }
    finally
    {
      cleanUp();
    }
  }

  public boolean appApproved(long primaryKey)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
   
      /*@lineinfo:generated-code*//*@lineinfo:601^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_credit_status
//          from    merchant            
//          where   app_seq_num = :primaryKey                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_credit_status\n        from    merchant            \n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.CreditWorksheetBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.CreditWorksheetBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:606^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        if(rs.getInt("merch_credit_status") == QueueConstants.CREDIT_APPROVE)
        {
          result = true;
        }
      }
 
      it.close();

    }
    catch(Exception e)
    {
      logEntry("appApproved()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;

  }

  private boolean recordExists(long primaryKey)
  {
    int                 recCount    = 0;
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:651^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//          from    credit_worksheet_info
//          where   app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n        from    credit_worksheet_info\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.CreditWorksheetBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:656^7*/
    
      result = (recCount > 0);

    }
    catch(Exception e)
    {
      result = false;
      logEntry("recordExists()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }

  /*
  **VALIDATION
  */  
  public boolean validate(long pk)
  {

     //if(!appApproved(pk))
     //{
       //addError("This application has been declined, please do not continue further");
     //}
     
    if(!isBlank(dandb) && dandb.length() > DANDB_MAX_CHARS)
    {
      addError("D & B must be limited to " + DANDB_MAX_CHARS + " characters.  Currently it is " + dandb.length()  + " characters long");   
    }
    if(!isBlank(notes) && notes.length() > NOTES_MAX_CHARS)
    {
      addError("General Comments/Notes must be limited to " + NOTES_MAX_CHARS + " characters.  Currently it is " + notes.length()  + " characters long");   
    }
    if(!isBlank(creditNotes) && creditNotes.length() > CREDIT_NOTES_MAX_CHARS)
    {
      addError("Credit Bureau Notes must be limited to " + CREDIT_NOTES_MAX_CHARS + " characters.  Currently it is " + creditNotes.length()  + " characters long");   
    }

    return (!hasErrors());
  }


  public String getControlNumber()
  {
    return controlNumber;
  }
  
  public String getMerchNumber()
  {
    return merchNumber;
  }
  
  public String getDba()
  {
    return dba;
  }
  
  public String getMonthlyVol()
  {
    return monthlyVol;
  }
  
  public String getAverageTicket()
  {
    return averageTicket;
  }
  
  public String getAppTypeDesc()
  {
    return appTypeDesc;
  }
  
  public String getBankName()
  {
    return bankName;
  }
  
  public String getCreditScore()
  {
    return creditScore;
  }
  public String getMesCreditScore()
  {
    return mesCreditScore;
  }
  public String getMesAutoApproveThreshold()
  {
    return mesAutoApproveThreshold;
  }
  public String getMesAutoDeclineThreshold()
  {
    return mesAutoDeclineThreshold;
  }
  public String getMesCreditScoreDetail()
  {
    return mesCreditScoreDetail;
  }

  public String getMatchNumber()
  {
    return matchNumber;
  }
  public String getRepName()
  {
    return repName;
  }

  public String getAnalystLoginId()
  {
    return analystLoginId;
  }
  public String getTradelines()
  {
    return tradelines;
  }
  public String getChargeOffs()
  {
    return chargeOffs;
  }
  public String getHo()
  {
    return ho;
  }
  public String getCollections()
  {
    return collections;
  }
  public String getJudgementsLiens()
  {
    return judgementsLiens;
  }
  public String getSafeScan()
  {
    return safeScan;
  }
  public String getDelinq30()
  {
    return delinq30;
  }
  public String getDelinq60()
  {
    return delinq60;
  }
  public String getDelinq90()
  {
    return delinq90;
  }
  public String getDandb()
  {
    return dandb;
  }
  public String getPriorProcessor()
  {
    return priorProcessor;
  }
  public String getNameOfPrior()
  {
    return nameOfPrior;
  }
  public String getStatementsProvided()
  {
    return statementsProvided;
  }
  public String getSiteInspection()
  {
    return siteInspection;
  }
  public String getDomainRegistration()
  {
    return domainRegistration;
  }
  public String getWebsiteUp()
  {
    return websiteUp;
  }
  public String getPhoneConfirmed()
  {
    return phoneConfirmed;
  }
  public String getMerchantContacted()
  {
    return merchantContacted;
  }
  public String getBusinessLicense()
  {
    return businessLicense;
  }
  public String getSignaturePage()
  {
    return signaturePage;
  }
  public String getVoidedCheck()
  {
    return voidedCheck;
  }
  public String getTaxReturn()
  {
    return taxReturn;
  }
  public String getFinancials()
  {
    return financials;
  }
  public String getCashReserve()
  {
    return cashReserve;
  }
  public String getOtherSupport()
  {
    return otherSupport;
  }
  public String getNotes()
  {
    return notes;
  }
  public String getCreditNotes()
  {
    return creditNotes;
  }
  public String getProductType()
  {
    return productType;
  }
  public String getPercentMoto()
  {
    return percentMoto;
  }
  public String getHomeAddressMatch()
  {
    return homeAddressMatch;
  }
  public String getBankAddressMatch()
  {
    return bankAddressMatch;
  }


  public void setAnalystLoginId(String val)
  {
    analystLoginId = val;
  }
  public void setTradelines(String val)
  {
    tradelines = val;
  }
  public void setChargeOffs(String val)
  {
    chargeOffs = val;
  }
  public void setHo(String val)
  {
    ho = val;
  }
  public void setCollections(String val)
  {
    collections = val;
  }
  public void setJudgementsLiens(String val)
  {
    judgementsLiens = val;
  }
  public void setSafeScan(String val)
  {
    safeScan = val;
  }
  public void setDelinq30(String val)
  {
    delinq30 = val;
  }
  public void setDelinq60(String val)
  {
    delinq60 = val;
  }
  public void setDelinq90(String val)
  {
    delinq90 = val;
  }
  public void setDandb(String val)
  {
    dandb = val;
  }
  public void setPriorProcessor(String val)
  {
    priorProcessor = val;
  }
  public void setNameOfPrior(String val)
  {
    nameOfPrior = val;
  }
  public void setStatementsProvided(String val)
  {
    statementsProvided = val;
  }
  public void setSiteInspection(String val)
  {
    siteInspection = val;
  }
  public void setDomainRegistration(String val)
  {
    domainRegistration = val;
  }
  public void setWebsiteUp(String val)
  {
    websiteUp = val;
  }
  public void setPhoneConfirmed(String val)
  {
    phoneConfirmed = val;
  }
  public void setMerchantContacted(String val)
  {
    merchantContacted = val;
  }
  public void setBusinessLicense(String val)
  {
    businessLicense = val;
  }
  public void setSignaturePage(String val)
  {
    signaturePage = val;
  }
  public void setVoidedCheck(String val)
  {
    voidedCheck = val;
  }
  public void setTaxReturn(String val)
  {
    taxReturn = val;
  }
  public void setFinancials(String val)
  {
    financials = val;
  }
  public void setCashReserve(String val)
  {
    cashReserve = val;
  }
  public void setOtherSupport(String val)
  {
    otherSupport = val;
  }
  public void setNotes(String val)
  {
    notes = val;
  }
  public void setCreditNotes(String val)
  {
    creditNotes = val;
  }
  public void setProductType(String val)
  {
    productType = val;
  }
  public void setPercentMoto(String val)
  {
    percentMoto = val;
  }

  public void setHomeAddressMatch(String val)
  {
    homeAddressMatch = val;
  }
  public void setBankAddressMatch(String val)
  {
    bankAddressMatch = val;
  }


  public Vector getErrors()
  {
    return errors;
  }
  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }

  public void addError(String err)
  {
    try
    {
      errors.add(err);
    }
    catch(Exception e)
    {
    }
  } 

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
  


}/*@lineinfo:generated-code*/