/*@lineinfo:filename=DataOverflowBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/DataOverflowBean.sqlj $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 8/08/02 12:09p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class DataOverflowBean extends SQLJConnectionBase
{
  private Vector  bets                             = new Vector();
  private boolean sent                             = false;

  private String  dbaName                          = "";
  private String  merchNum                         = "";
  private String  controlNum                       = "";
  private String  appTypeDesc                      = "";

  public DataOverflowBean()
  {
  }
  
  public int getNumBets()
  {
    return bets.size();
  }

  public void getData(long primaryKey)
  {
    getMerchantGeneralInfo(primaryKey);
    getBetInfo(primaryKey);
  }
  
  private void getMerchantGeneralInfo(long primaryKey)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {

      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:78^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   
//                    merch.merch_business_name,
//                    merch.merch_number,
//                    merch.merc_cntrl_number,
//                    app.appsrctype_code
//                     
//          from     
//                    merchant    merch,
//                    application app
//  
//          where     merch.app_seq_num = :primaryKey and
//                    merch.app_seq_num = app.app_seq_num 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   \n                  merch.merch_business_name,\n                  merch.merch_number,\n                  merch.merc_cntrl_number,\n                  app.appsrctype_code\n                   \n        from     \n                  merchant    merch,\n                  application app\n\n        where     merch.app_seq_num =  :1  and\n                  merch.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.DataOverflowBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.DataOverflowBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^7*/
    
      rs = it.getResultSet();
    
      if(rs.next())
      {
        dbaName         = isBlank(rs.getString("merch_business_name"))  ? "" : rs.getString("merch_business_name");
        merchNum        = isBlank(rs.getString("merch_number"))         ? "" : rs.getString("merch_number");
        controlNum      = isBlank(rs.getString("merc_cntrl_number"))    ? "" : rs.getString("merc_cntrl_number");
        appTypeDesc     = isBlank(rs.getString("appsrctype_code"))      ? "" : rs.getString("appsrctype_code");
      }
      

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMerchantGeneralInfo: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  private void getBetInfo(long primaryKey)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {

      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:127^7*/

//  ************************************************************
//  #sql [Ctx] it = { select 
//                    * 
//          from 
//                    billcard_auth_bet 
//          where 
//                    app_seq_num = :primaryKey and
//                    num_of_bet > 12 
//          order by    
//                    plan_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select \n                  * \n        from \n                  billcard_auth_bet \n        where \n                  app_seq_num =  :1  and\n                  num_of_bet > 12 \n        order by    \n                  plan_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.DataOverflowBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.DataOverflowBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:138^7*/
    
      rs = it.getResultSet();
    
      while(rs.next())
      {
        Bet tempBet = new Bet();

        tempBet.setVendorId(isBlank(rs.getString("vendor_id"))    ? "" : rs.getString("vendor_id"));
        tempBet.setMediaType(isBlank(rs.getString("media_type"))  ? "" : rs.getString("media_type"));
        tempBet.setPlanType(isBlank(rs.getString("plan_type"))    ? "" : rs.getString("plan_type"));
        tempBet.setBet(isBlank(rs.getString("bet"))               ? "" : rs.getString("bet"));

        bets.add(tempBet);
      }
      

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getBetInfo: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  public void moveToCompletedQueue(long pk)
  {

    try
    {
      connect();
       
      /*@lineinfo:generated-code*//*@lineinfo:174^7*/

//  ************************************************************
//  #sql [Ctx] { update  
//                  app_queue
//          set     
//                  app_queue_stage         = :QueueConstants.Q_SETUP_COMPLETE
//          where   
//                  APP_SEQ_NUM             = :pk and
//                  app_queue_type          = :QueueConstants.QUEUE_SETUP and
//                  app_queue_stage         = :QueueConstants.Q_SETUP_DATA_OVERFLOW
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  \n                app_queue\n        set     \n                app_queue_stage         =  :1 \n        where   \n                APP_SEQ_NUM             =  :2  and\n                app_queue_type          =  :3  and\n                app_queue_stage         =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.DataOverflowBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.Q_SETUP_COMPLETE);
   __sJT_st.setLong(2,pk);
   __sJT_st.setInt(3,QueueConstants.QUEUE_SETUP);
   __sJT_st.setInt(4,QueueConstants.Q_SETUP_DATA_OVERFLOW);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:184^7*/
    
    }
    catch(Exception e)
    {
      logEntry("moveToCompletedQueue()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  public String getDbaName()
  {
    return dbaName;
  }
  public String getMerchNum()
  {
    return merchNum;
  }
  public String getControlNum()
  {
    return controlNum;
  }
  public String getAppTypeDesc()
  {
    return appTypeDesc;
  }

  public void setSent(String temp)
  {
    this.sent = true;
  }

  public boolean getSent()
  {
    return this.sent;
  }

  public boolean isSent()
  {
    return this.sent;
  }

  public String getVendorId(int idx)
  {
    Bet betRec = (Bet)bets.elementAt(idx);
    return(betRec.getVendorId());
  }
  
  public String getMediaType(int idx)
  {
    Bet betRec = (Bet)bets.elementAt(idx);
    return(betRec.getMediaType());
  }
  public String getPlanType(int idx)
  {
    Bet betRec = (Bet)bets.elementAt(idx);
    return(betRec.getPlanType());
  }
  public String getBet(int idx)
  {                                        
    Bet betRec = (Bet)bets.elementAt(idx);
    return(betRec.getBet());
  }

  public class Bet
  { 
    private String    vendorId        = "";
    private String    mediaType       = "";
    private String    planType        = "";
    private String    bet             = "";

    Bet()
    {}

    public void setVendorId(String vendorId)
    {
      this.vendorId = vendorId;
    }
    public void setMediaType(String mediaType)
    {
      this.mediaType = mediaType;
    }
    public void setPlanType(String planType)
    {
      this.planType = planType;
    }
    public void setBet(String bet)
    {
      this.bet = bet;
    }

    public String getVendorId()
    {
      return this.vendorId;
    }
    public String getMediaType()
    {
      return this.mediaType;
    }
    public String getPlanType()
    {
      return this.planType;
    }
    public String getBet()
    {
      return this.bet;
    }
    
  }

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }


}/*@lineinfo:generated-code*/