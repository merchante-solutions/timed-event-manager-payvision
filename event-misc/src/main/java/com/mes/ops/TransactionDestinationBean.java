/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/TransactionDestinationBean.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;

public class TransactionDestinationBean extends ExpandDataBean
{

  private boolean globe               = false;
  private boolean assignmentSelected  = false;
  private boolean mes                 = false;

  private String global               = "";
  private String deposits             = "";
  private String adjustments          = "";
  private String chargebacks          = "";
  private String reversals            = "";
  private String chargebackReversals  = "";
  private String ddaAdjustments       = "";
  private String batchrc              = "";
  private String otherTranType1       = "";
  private String otherTranType2       = "";
  private String otherTranType3       = "";

  public TransactionDestinationBean()
  {

  }
  
  public void getData(long primaryKey)
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    StringBuffer      qs      = new StringBuffer();

    try 
    {
      qs.append("select ");
      qs.append("DEST_DEPOSITS, ");
      qs.append("DEST_ADJUSTMENTS, ");
      qs.append("DEST_CHARGEBACKS, ");
      qs.append("DEST_REVERSALS, ");
      qs.append("DEST_CHARGEBACK_REVERSALS, ");
      qs.append("DEST_DDA_ADJUSTMENTS, ");
      qs.append("DEST_BATCH_RC, ");
      qs.append("DEST_OTHER_TRAN_TYPE1, ");
      qs.append("DEST_OTHER_TRAN_TYPE2, ");
      qs.append("DEST_OTHER_TRAN_TYPE3, ");
      qs.append("DEST_GLOBAL ");
      qs.append("from merchant_data where app_seq_num = ? ");
    
      ps = getPreparedStatement(qs.toString());

      ps.setLong(1, primaryKey);
      rs = ps.executeQuery();

      if(rs.next())
      {
        deposits            = isBlank(rs.getString("DEST_DEPOSITS"))             ? "" : rs.getString("DEST_DEPOSITS");
        adjustments         = isBlank(rs.getString("DEST_ADJUSTMENTS"))          ? "" : rs.getString("DEST_ADJUSTMENTS");
        chargebacks         = isBlank(rs.getString("DEST_CHARGEBACKS"))          ? "" : rs.getString("DEST_CHARGEBACKS");
        reversals           = isBlank(rs.getString("DEST_REVERSALS"))            ? "" : rs.getString("DEST_REVERSALS");
        chargebackReversals = isBlank(rs.getString("DEST_CHARGEBACK_REVERSALS")) ? "" : rs.getString("DEST_CHARGEBACK_REVERSALS");
        ddaAdjustments      = isBlank(rs.getString("DEST_DDA_ADJUSTMENTS"))      ? "" : rs.getString("DEST_DDA_ADJUSTMENTS");
        batchrc             = isBlank(rs.getString("DEST_BATCH_RC"))             ? "" : rs.getString("DEST_BATCH_RC");
        otherTranType1      = isBlank(rs.getString("DEST_OTHER_TRAN_TYPE1"))     ? "" : rs.getString("DEST_OTHER_TRAN_TYPE1");
        otherTranType2      = isBlank(rs.getString("DEST_OTHER_TRAN_TYPE2"))     ? "" : rs.getString("DEST_OTHER_TRAN_TYPE2");
        otherTranType3      = isBlank(rs.getString("DEST_OTHER_TRAN_TYPE3"))     ? "" : rs.getString("DEST_OTHER_TRAN_TYPE3");
        global              = isBlank(rs.getString("DEST_GLOBAL"))               ? "" : rs.getString("DEST_GLOBAL");
        if(!isBlank(global))
        {
          this.globe = true;
        }
      }
      ps.close();
      rs.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      addError("getData: " + e.toString());
    }
  }


  
  public void setPageDefaults(String primaryKey)
  {
    try
    {
      setPageDefaults(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
    }
  }
  public void setPageDefaults(long primaryKey)  
  {
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    int               i       = 0;

    try 
    {
      ps = getPreparedStatement("select app_type from application where app_seq_num = ?");
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();

      if(rs.next())
      {
        switch(rs.getInt("app_type"))
        {
          case mesConstants.APP_TYPE_CBT:
          case mesConstants.APP_TYPE_CBT_NEW:
            this.mes = false;
          break;
          default:
            this.mes = true;
          break;
        }
      }
      ps.close();
      rs.close();

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setPageDefaults: " + e.toString());
      addError("setPageDefaults: " + e.toString());
    }
  }

  public void submitData(HttpServletRequest req, long primaryKey)
  {

    StringBuffer      qs      = new StringBuffer();
    PreparedStatement ps      = null;

    try 
    {
      qs.append("update MERCHANT_DATA set        ");
      qs.append("DEST_DEPOSITS              = ?, ");
      qs.append("DEST_ADJUSTMENTS           = ?, ");
      qs.append("DEST_CHARGEBACKS           = ?, ");
      qs.append("DEST_REVERSALS             = ?, ");
      qs.append("DEST_CHARGEBACK_REVERSALS  = ?, ");
      qs.append("DEST_DDA_ADJUSTMENTS       = ?, ");
      qs.append("DEST_BATCH_RC              = ?, ");
      qs.append("DEST_OTHER_TRAN_TYPE1      = ?, ");
      qs.append("DEST_OTHER_TRAN_TYPE2      = ?, ");
      qs.append("DEST_OTHER_TRAN_TYPE3      = ?, ");
      qs.append("DEST_GLOBAL                = ?  ");
      qs.append("where app_seq_num          = ?  ");
    
      ps = getPreparedStatement(qs.toString());


      if(isGlobal()) //clear out flags not used
      {
       deposits             = "";
       adjustments          = "";
       chargebacks          = "";
       reversals            = "";
       chargebackReversals  = "";
       ddaAdjustments       = "";
       batchrc              = "";
       otherTranType1       = "";
       otherTranType2       = "";
       otherTranType3       = "";
      }
      else
      {
       global               = "";
      }

      ps.setString(1,  deposits);
      ps.setString(2,  adjustments);
      ps.setString(3,  chargebacks);
      ps.setString(4,  reversals);
      ps.setString(5,  chargebackReversals);
      ps.setString(6,  ddaAdjustments);
      ps.setString(7,  batchrc);
      ps.setString(8,  otherTranType1);
      ps.setString(9,  otherTranType2);
      ps.setString(10, otherTranType3);
      ps.setString(11, global);
  
      ps.setLong(12,primaryKey);

      if(ps.executeUpdate() != 1)
      {
        addError("Unable to update merchant_data table!");
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
      addError("submitData: " + e.toString());
    }
  }
  
  
  
  public boolean validate()
  {
    if(!this.assignmentSelected)
    {
      addError("Please select either Global Assignment or Individual Assignment.");
    }
    else
    {
      if(isGlobal() && isBlank(global))
      {
        addError("Global Assignment can not be blank.");
      }      
      
      if(!isGlobal() && isBlank(deposits))
      {
        addError("Deposits Assignment can not be blank.");
      }
      if(!isGlobal() && isBlank(adjustments))
      {
        addError("Adjustments Assignment can not be blank.");
      }
      if(!isGlobal() && isBlank(chargebacks))
      {
        addError("Chargebacks Assignment can not be blank.");
      }
      if(!isGlobal() && isBlank(reversals))
      {
        addError("Reversals Assignment can not be blank.");
      }
      if(!isGlobal() && isBlank(chargebackReversals))
      {
        addError("Chargeback Reversals Assignment can not be blank.");
      }
      if(!isGlobal() && isBlank(ddaAdjustments))
      {
        addError("DDA Adjustments Assignment can not be blank.");
      }
      if(!isGlobal() && isBlank(batchrc))
      {
        addError("Batch R/C Assignment can not be blank.");
      }
      if(!isGlobal() && isBlank(otherTranType1))
      {
        addError("Other Tran Type 1 Assignment can not be blank.");
      }
      if(!isGlobal() && isBlank(otherTranType2))
      {
        addError("Other Tran Type 2 Assignment can not be blank.");
      }
      if(!isGlobal() && isBlank(otherTranType3))
      {
        addError("Other Tran Type 3 Assignment can not be blank.");
      }
    }

    return(! hasErrors());
  }



  public String getGlobal()
  {
    return this.global;
  }
  public String getDeposits()
  {
    return this.deposits;
  }
  public String getAdjustments()
  {
    return this.adjustments;
  }
  public String getChargebacks()
  {
    return this.chargebacks;
  }
  public String getReversals()
  {
    return this.reversals;
  }
  public String getChargebackReversals()
  {
    return this.chargebackReversals;
  }
  public String getDdaAdjustments()
  {
    return this.ddaAdjustments;
  }
  public String getBatchrc()
  {
    return this.batchrc;
  }
  public String getOtherTranType1()
  {
    return this.otherTranType1;
  }
  public String getOtherTranType2()
  {
    return this.otherTranType2;
  }
  public String getOtherTranType3()
  {
    return this.otherTranType3;
  }


  public void setGlobal(String global)
  {
    this.global = global;
  }
  public void setDeposits(String deposits)
  {
    this.deposits = deposits;
  }
  public void setAdjustments(String adjustments)
  {
    this.adjustments = adjustments;
  }
  public void setChargebacks(String chargebacks)
  {
    this.chargebacks = chargebacks;
  }
  public void setReversals(String reversals)
  {
    this.reversals = reversals;
  }
  public void setChargebackReversals(String chargebackReversals)
  {
    this.chargebackReversals = chargebackReversals;
  }
  public void setDdaAdjustments(String ddaAdjustments)
  {
    this.ddaAdjustments = ddaAdjustments;
  }
  public void setBatchrc(String batchrc)
  {
    this.batchrc = batchrc;
  }
  public void setOtherTranType1(String otherTranType1)
  {
    this.otherTranType1 = otherTranType1;
  }
  public void setOtherTranType2(String otherTranType2)
  {
    this.otherTranType2 = otherTranType2;
  }
  public void setOtherTranType3(String otherTranType3)
  {
    this.otherTranType3 = otherTranType3;
  }
  public void setAssignment(String assignment)
  {
    if(assignment.equals("G"))
    {
      this.globe = true;
    }
    else if(assignment.equals("I"))
    {
      this.globe = false;
    }
    this.assignmentSelected = true;
  }
  public boolean isGlobal()
  {
    return this.globe;
  }

  public boolean isMes()
  {
    return this.mes;
  }  

}







