/*@lineinfo:filename=EasiDownloadResponse*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/EasiDownloadResponse.sqlj $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Vector;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;

public class EasiDownloadResponse extends SQLJConnectionBase
{
  //error codes
  private static final int  NO_ERROR                      = -1;
  private static final int  NUM_RECS_DONT_MATCH           = 0;
  private static final int  NUM_RECS_CHECK_CRASH          = 1;
  private static final int  RECORD_TYPE_UNKNOWN           = 2;

  private static final int  MISSING_DATA                  = 10;
  private static final int  DISC_MERCH_NUM_NOT_BLANK      = 11;
  private static final int  DECIS_DATE_NOT_BLANK          = 12;
  private static final int  BLANK_DIS_CODE                = 13;
  private static final int  APP_SEQ_NUM_NOT_FOUND         = 14;
  private static final int  BLANK_ERROR_FIELD             = 15;

  //reset error count
  private static final int RESET_RECORD_COUNT             = 0;


  private Vector      records                             = new Vector();
  private Vector      emailRecs                           = new Vector();
  
  //this bean is called for each file and each file only has one partner num.. 
  //filePartnerNum makes currentPartnerNum obsolete, but well leave it alone cause it aint hurting anything
  private String      filePartnerNum                      = "";

  private String      currentPartnerNum                   = "";
  private String      headerDateTimestamp                 = "";
  private String      currentHeader                       = "";

  private int         recordCounter                       = RESET_RECORD_COUNT;
  private boolean     hasError                            = false;

  private Timestamp   today                               = null;

  public EasiDownloadResponse()
  {
    super(SQLJConnectionBase.getDirectConnectString());

    try
    { 
  
      //record error in database;
      connect();

      //gets the current data/time
      /*@lineinfo:generated-code*//*@lineinfo:100^7*/

//  ************************************************************
//  #sql [Ctx] { select    sysdate 
//          from      dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    sysdate  \n        from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.EasiDownloadResponse",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   today = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:104^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "EasiDownloadResponse: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public boolean getData(File f)  
  {
    String            lineInfo  = "";
    boolean           result    = false;

    try 
    {
      LineNumberReader in  = new LineNumberReader(new FileReader(f));

      int i = 0;

      if(in != null)
      { 
        
        //clears all email recs
        emailRecs.setSize(0);
        
        lineInfo = in.readLine();
        while(lineInfo != null)
        {
          i++;
          
          if(parseLine(lineInfo, i)) //if true an error occurred
          {
            //System.out.println(lineInfo); // prints error records
          }

          lineInfo = in.readLine();
        }


        //once we read entire file and update database we send email with records to show what has been updated and is ready to go
        if(emailRecs.size() > 0)
        {
          sendMail();
        }

      }

      in.close();

      result = true;

      System.out.println("number of lines read " + i);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getData: " + e.toString());
      System.out.println("getData: " + e.toString());
    }
    return result;
  }
  


  private boolean parseLine(String line, int linenum)
  {
    ResponseRec     rec                 = null;

    String          numRecs             = "";
    boolean         feeError            = false;
    int             errorCode           = NO_ERROR;

    String          recordType          = "";
    String          discoverMerchNumber = "";
    String          acquirerMerchNumber = "";
    String          decisionStatus      = "";
    String          dispositionCode     = "";
    String          receivedDate        = "";
    String          decisionDate        = "";

    recordType          = line.substring(0,2);
    
    if(recordType.equals("00")) //header
    {
      //header info
      currentPartnerNum   = line.substring(2,6);

      //the purpose of filePartnerNum is that it doesnt get reset until the bean's life is over...
      filePartnerNum      = currentPartnerNum;

      headerDateTimestamp = line.substring(8,32);
      currentHeader       = line;
    }
    else if(recordType.equals("05")) //record
    {
      
      recordCounter++;

      //record info
      discoverMerchNumber = line.substring(2,17);
      acquirerMerchNumber = line.substring(17,37);
      decisionStatus      = line.substring(52,53);
      dispositionCode     = line.substring(53,55);
      receivedDate        = line.substring(55,65);
      decisionDate        = line.substring(65,75);

      rec = new ResponseRec();
      rec.setDiscoverMerchNumber(discoverMerchNumber.trim());
      rec.setAcquirerMerchNumber(acquirerMerchNumber.trim());
      
      rec.setAppSeqNum(getAppSeqNum(acquirerMerchNumber.trim()));

//*************************************************************
      //this is for testing only
      //rec.setAppSeqNum(Integer.toString(recordCounter));
//*************************************************************
      
      rec.setDecisionStatus     (decisionStatus.trim());
      rec.setDispositionCode    (dispositionCode.trim());
      rec.setReceivedDate       (receivedDate.trim());
      rec.setDecisionDate       (decisionDate.trim());

      if(rec.isValid())
      {
        records.add(rec);
      }
      else
      {
        feeError  = true;
        errorCode = rec.getErrorCode();
      }

    }
    else if(recordType.equals("99")) //trailer
    {
      numRecs             = line.substring(6,11);

      //check to see that trailer num of recs and actual read num of recs are the same
      try
      {
        int tempNumRecs = Integer.parseInt(numRecs);

        if(recordCounter == tempNumRecs)
        {
          //if its all good.. we can update db and put these files up 
          submitData();

          if(hasError)//if a file format error occured.. notify admin.. they can check db_log
          {
            sendMailToAdmin(currentHeader);
          }
          
          //clear everything for next header/trailer set if exists
          currentPartnerNum         = "";
          headerDateTimestamp       = "";
          currentHeader             = "";
          
          records                   = new Vector();
          recordCounter             = RESET_RECORD_COUNT;
          hasError                  = false;
        }
        else
        {
          feeError  = true;
          errorCode = NUM_RECS_DONT_MATCH;
        }

        if(feeError)//if slipped through above.. then we send error email here
        {
          sendMailToAdmin(currentHeader);
        }

      }
      catch(Exception e)
      {
        feeError  = true;
        errorCode = NUM_RECS_CHECK_CRASH;
      }
      
      //reset current partner num and record counter because we hit the trailer.. a new partner num will be coming in now...
      currentPartnerNum   = "";
      recordCounter       = 0;
    }
    else if(recordType.startsWith("+"))
    {
      return false; //no error
    }
    else //error.. file record type not known
    {
      feeError  = true;
      errorCode = RECORD_TYPE_UNKNOWN;
    }

    //write to db showing header for error record and line of error record and error desc
    if(feeError)
    {
      try
      {
        //this says that an error occurred while reading records for one header/trailer set
        this.hasError = true;

        //record error in database;
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:311^9*/

//  ************************************************************
//  #sql [Ctx] { insert into RAP_UPLOAD_ERROR_LOG
//            ( 
//              ERROR_DATE,
//              HEADER,
//              ERROR_DESC,
//              LINE,
//              UPLOAD_TYPE
//            )
//            values
//            ( 
//              :today,
//              :currentHeader.substring(0,50),
//              :decodeError(errorCode),
//              :line,
//              :mesConstants.RAP_UPLOAD_TYPE_RESPONSE
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_762 = currentHeader.substring(0,50);
 String __sJT_763 = decodeError(errorCode);
   String theSqlTS = "insert into RAP_UPLOAD_ERROR_LOG\n          ( \n            ERROR_DATE,\n            HEADER,\n            ERROR_DESC,\n            LINE,\n            UPLOAD_TYPE\n          )\n          values\n          ( \n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.EasiDownloadResponse",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setString(2,__sJT_762);
   __sJT_st.setString(3,__sJT_763);
   __sJT_st.setString(4,line);
   __sJT_st.setInt(5,mesConstants.RAP_UPLOAD_TYPE_RESPONSE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:329^9*/
      }
      catch(Exception e)
      {
        System.out.println(e.toString());
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "rap_upload_errorlog failed: " + e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    return feeError;
  }

  public boolean submitData()
  {
    boolean           result        = true;

    try
    {

      connect(); //open up a connection which will be used over and over again...
      
      for(int x=0; x< this.records.size(); x++)
      {
        ResponseRec rec =  (ResponseRec)records.elementAt(x);
        
        if(statusChanged(rec))
        {
          updateTrackingStatus(rec.getAppSeqNum(), rec.getDecisionStatus());
          emailRecs.add(rec.getDecisionStatus() + "\t\t" + rec.getAcquirerMerchNumber() + "\t\t\t" + rec.getDiscoverMerchNumber() + "\n" + "https://www.merchante-solutions.com/jsp/credit/discover_rap_screen.jsp?primaryKey=" + rec.getAppSeqNum() + "\n\n");
        }
        
        
        if(recordExists(rec.getAppSeqNum()))
        {
          /*@lineinfo:generated-code*//*@lineinfo:366^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchant_discover_app_response
//              set     discover_merchant_num   = :rec.getDiscoverMerchNumber(),          
//                      decision_status         = :rec.getDecisionStatus(),      
//                      disposition_reason_code = :rec.getDispositionCode(),         
//                      received_date           = :getSqlTimeStamp(rec.getReceivedDate()),        
//                      decision_date           = :getSqlTimeStamp(rec.getDecisionDate()),
//                      date_last_updated       = :today,
//                      error_flags             = null
//              where   app_seq_num             = :rec.getAppSeqNum() and
//                      acquirer_merchant_num   = :rec.getAcquirerMerchNumber()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_764 = rec.getDiscoverMerchNumber();
 String __sJT_765 = rec.getDecisionStatus();
 String __sJT_766 = rec.getDispositionCode();
 java.sql.Timestamp __sJT_767 = getSqlTimeStamp(rec.getReceivedDate());
 java.sql.Timestamp __sJT_768 = getSqlTimeStamp(rec.getDecisionDate());
 String __sJT_769 = rec.getAppSeqNum();
 String __sJT_770 = rec.getAcquirerMerchNumber();
   String theSqlTS = "update  merchant_discover_app_response\n            set     discover_merchant_num   =  :1 ,          \n                    decision_status         =  :2 ,      \n                    disposition_reason_code =  :3 ,         \n                    received_date           =  :4 ,        \n                    decision_date           =  :5 ,\n                    date_last_updated       =  :6 ,\n                    error_flags             = null\n            where   app_seq_num             =  :7  and\n                    acquirer_merchant_num   =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.EasiDownloadResponse",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_764);
   __sJT_st.setString(2,__sJT_765);
   __sJT_st.setString(3,__sJT_766);
   __sJT_st.setTimestamp(4,__sJT_767);
   __sJT_st.setTimestamp(5,__sJT_768);
   __sJT_st.setTimestamp(6,today);
   __sJT_st.setString(7,__sJT_769);
   __sJT_st.setString(8,__sJT_770);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:378^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:382^11*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_discover_app_response
//              ( 
//                discover_merchant_num,
//                decision_status,
//                disposition_reason_code,
//                received_date,
//                decision_date,
//                date_last_updated,
//                error_flags,
//                app_seq_num,
//                acquirer_merchant_num
//              )
//              values
//              ( 
//                :rec.getDiscoverMerchNumber(),
//                :rec.getDecisionStatus(),
//                :rec.getDispositionCode(),
//                :getSqlTimeStamp(rec.getReceivedDate()),
//                :getSqlTimeStamp(rec.getDecisionDate()),
//                :today,
//                null,
//                :rec.getAppSeqNum(),
//                :rec.getAcquirerMerchNumber()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_771 = rec.getDiscoverMerchNumber();
 String __sJT_772 = rec.getDecisionStatus();
 String __sJT_773 = rec.getDispositionCode();
 java.sql.Timestamp __sJT_774 = getSqlTimeStamp(rec.getReceivedDate());
 java.sql.Timestamp __sJT_775 = getSqlTimeStamp(rec.getDecisionDate());
 String __sJT_776 = rec.getAppSeqNum();
 String __sJT_777 = rec.getAcquirerMerchNumber();
   String theSqlTS = "insert into merchant_discover_app_response\n            ( \n              discover_merchant_num,\n              decision_status,\n              disposition_reason_code,\n              received_date,\n              decision_date,\n              date_last_updated,\n              error_flags,\n              app_seq_num,\n              acquirer_merchant_num\n            )\n            values\n            ( \n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n              null,\n               :7 ,\n               :8 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.EasiDownloadResponse",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_771);
   __sJT_st.setString(2,__sJT_772);
   __sJT_st.setString(3,__sJT_773);
   __sJT_st.setTimestamp(4,__sJT_774);
   __sJT_st.setTimestamp(5,__sJT_775);
   __sJT_st.setTimestamp(6,today);
   __sJT_st.setString(7,__sJT_776);
   __sJT_st.setString(8,__sJT_777);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:408^11*/
        }

        //if approved, update application with new discover number.. needed so that disc num gets sent to mms
        if(rec.getDecisionStatus().equals("A"))
        {
          int recCount = 0;
          
          // update merchpayoption if it exists
          /*@lineinfo:generated-code*//*@lineinfo:417^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchpayoption
//              set     merchpo_card_merch_number = :rec.getDiscoverMerchNumber()
//              where   app_seq_num     = :rec.getAppSeqNum() and
//                      cardtype_code   = 14
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_778 = rec.getDiscoverMerchNumber();
 String __sJT_779 = rec.getAppSeqNum();
   String theSqlTS = "update  merchpayoption\n            set     merchpo_card_merch_number =  :1 \n            where   app_seq_num     =  :2  and\n                    cardtype_code   = 14";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.ops.EasiDownloadResponse",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_778);
   __sJT_st.setString(2,__sJT_779);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:423^11*/
          
          try
          {
            // update mif_nonbank_cards to ensure that the number will show up on the CIF
            /*@lineinfo:generated-code*//*@lineinfo:428^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//                
//                from    mif_non_bank_cards
//                where   merchant_number = :rec.getAcquirerMerchNumber()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_780 = rec.getAcquirerMerchNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n               \n              from    mif_non_bank_cards\n              where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.EasiDownloadResponse",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_780);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:434^13*/
          
            if(recCount > 0)
            {
              /*@lineinfo:generated-code*//*@lineinfo:438^15*/

//  ************************************************************
//  #sql [Ctx] { update  mif_non_bank_cards
//                  set     discover = :rec.getDiscoverMerchNumber()
//                  where   merchant_number = :rec.getAcquirerMerchNumber()
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_781 = rec.getDiscoverMerchNumber();
 String __sJT_782 = rec.getAcquirerMerchNumber();
   String theSqlTS = "update  mif_non_bank_cards\n                set     discover =  :1 \n                where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.ops.EasiDownloadResponse",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_781);
   __sJT_st.setString(2,__sJT_782);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:443^15*/
            }
            else
            {
              /*@lineinfo:generated-code*//*@lineinfo:447^15*/

//  ************************************************************
//  #sql [Ctx] { insert into mif_non_bank_cards
//                  (
//                    merchant_number,
//                    discover
//                  )
//                  values
//                  (
//                    :rec.getAcquirerMerchNumber(),
//                    :rec.getDiscoverMerchNumber()
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_783 = rec.getAcquirerMerchNumber();
 String __sJT_784 = rec.getDiscoverMerchNumber();
   String theSqlTS = "insert into mif_non_bank_cards\n                (\n                  merchant_number,\n                  discover\n                )\n                values\n                (\n                   :1 ,\n                   :2 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ops.EasiDownloadResponse",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_783);
   __sJT_st.setString(2,__sJT_784);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:459^15*/
            }
          }
          catch(Exception nbe)
          {
          }
        }
      
        //if approved or declined.. automatically move the account into mms.. this is only for bbt
        //but since nothing will happen if the discover request type is not new.. and only bbt accounts have a new
        //discover request type.. only bbt accounts will be affected by this.. so we dont have to worry about anything..
        if(rec.getDecisionStatus().equals("A") || 
           rec.getDecisionStatus().equals("D"))
        {
          BbtMmsSetup.bbtMmsSetup(Long.parseLong(rec.getAppSeqNum()), Ctx);
        }
      
      }
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitData: " + e.toString());
      System.out.println("error: " + e.toString());
      result = false;
    } 
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  private boolean statusChanged(ResponseRec tempRec)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = true;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp  = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:507^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    MERCHANT_DISCOVER_APP_RESPONSE
//          where   APP_SEQ_NUM = :tempRec.getAppSeqNum()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_785 = tempRec.getAppSeqNum();
  try {
   String theSqlTS = "select  *\n        from    MERCHANT_DISCOVER_APP_RESPONSE\n        where   APP_SEQ_NUM =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.EasiDownloadResponse",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_785);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.EasiDownloadResponse",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:512^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        String tempStatus = isBlank(rs.getString("DECISION_STATUS"))          ? "" : rs.getString("DECISION_STATUS");
        String tempDisp   = isBlank(rs.getString("DISPOSITION_REASON_CODE"))  ? "" : rs.getString("DISPOSITION_REASON_CODE");
        result = !(tempStatus.equals(tempRec.getDecisionStatus()) && tempDisp.equals(tempRec.getDispositionCode()));
      }
 
      it.close();
    }
    catch(Exception e)
    {
      result = true;
      logEntry("statusChanged()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;

  }

  private void sendMailToAdmin(String curHeader)
  {
    String message = "";
    String subject = "";
    
    subject = "Discover Rap Error Occurred (response file) -- " + today.toString();

    message = "Error occurred for this header:\n\n" + curHeader;

    try
    {
      MailMessage msg = new MailMessage(true);
      msg.setAddresses(MesEmails.MSG_ADDRS_RAP_RESPONSE_ADMIN);
      msg.setSubject(subject);
      msg.setText(message);
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendMail()",e.toString());
      System.out.println("discover Error Status email failed to send.");
    }  
  }

  private void sendMail()
  {
    String message = "";
    String subject = "";
    
    subject = "Discover Rap Responses -- " + today.toString();

    message = "Status \t Acquirer Merch # \t\t Discover Merch # \n\n";

    for(int p=0; p<emailRecs.size(); p++)
    {
      message += (String)emailRecs.elementAt(p);
    }

    try
    {
      MailMessage msg = new MailMessage(true);

      if(filePartnerNum.equals("9849"))
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_RAP_RESPONSE_ADMIN);
      }
      else
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_RAP_RESPONSE_OPERATIONS);
      }

      msg.setSubject(subject);
      msg.setText(message);
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendMail()",e.toString());
      System.out.println("discover Response Status email failed to send.");
    }  
  }

  private void updateTrackingStatus(String appSeqNum, String status)
  {
    try
    {
      int statusCode = QueueConstants.DEPT_STATUS_DISCOVER_RAP_TRANSMISSION_ERRORS;

      if(status.equals("A"))
      {
        statusCode = QueueConstants.DEPT_STATUS_DISCOVER_RAP_LIVE_AUTO;
      }
      else if(status.equals("D"))
      {
        statusCode = QueueConstants.DEPT_STATUS_DISCOVER_RAP_DECLINED;
      }
      else if(status.equals("P"))
      {
        statusCode = QueueConstants.DEPT_STATUS_DISCOVER_RAP_PENDING;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:623^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//        
//          set     status_code   = :statusCode
//  
//          where   APP_SEQ_NUM   = :appSeqNum and
//                  dept_code     = :QueueConstants.DEPARTMENT_DISCOVER_RAP
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n      \n        set     status_code   =  :1 \n\n        where   APP_SEQ_NUM   =  :2  and\n                dept_code     =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.ops.EasiDownloadResponse",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,statusCode);
   __sJT_st.setString(2,appSeqNum);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_DISCOVER_RAP);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:631^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updateTrackingStatus: " + e.toString());
      System.out.println("updateTrackingStatus() error: " + e.toString());
    }
    finally
    {
    }
  }

  private String decodeError(int err)
  {
    String result = "";

    switch(err)
    {
      case NUM_RECS_DONT_MATCH:
        result = "Number of records read and number of records in trailer field 3 don't match.  No Records Updated!";
      break;
      case NUM_RECS_CHECK_CRASH:
        result = "Number of records check crashed.  No Records Updated!";
      break;
      case RECORD_TYPE_UNKNOWN:
        result = "Unknown record type";
      break;
      case MISSING_DATA:
        result = "Required data missing from record";
      break;
      case DISC_MERCH_NUM_NOT_BLANK:
        result = "Account pending, yet discover merchant number was present";
      break;
      case DECIS_DATE_NOT_BLANK:
        result = "Account pending, yet decision date was present";
      break;
      case BLANK_DIS_CODE:
        result = "Disposition code was PM and decision status was not pending";
      break;
      case APP_SEQ_NUM_NOT_FOUND:
        result = "Could not find appSeqNum from acquirer merchant account number";
      break;

    }
    return result;
  }

  private boolean recordExists(String appseqnum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {

      //***************************
      // no need for connect() and cleanUp() cause its already done in calling method
      //***************************     

      /*@lineinfo:generated-code*//*@lineinfo:691^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num 
//          from    MERCHANT_DISCOVER_APP_RESPONSE
//          where   app_seq_num = :appseqnum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num \n        from    MERCHANT_DISCOVER_APP_RESPONSE\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.EasiDownloadResponse",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,appseqnum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.EasiDownloadResponse",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:696^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      it.close();
    }
    catch(Exception e)
    {
      result = false;
      logEntry("recordExists()", e.toString());
    }
    finally
    {
    }

    return result;
  }


  private String getAppSeqNum(String merchNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    String              result  = "";

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:730^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num 
//          from    MERCHANT_DISCOVER_APP
//          where   ACQUIRER_MERCHANT_NUM = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num \n        from    MERCHANT_DISCOVER_APP\n        where   ACQUIRER_MERCHANT_NUM =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.EasiDownloadResponse",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.ops.EasiDownloadResponse",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:735^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = rs.getString("app_seq_num");
      }
 
      it.close();
    }
    catch(Exception e)
    {
      result = "";
      logEntry("getAppSeqNum()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  private Date makeDate(String tempDate)
  {
    Date       aDate  = null;
    DateFormat fmt    = DateFormat.getDateInstance(DateFormat.SHORT);
    
    try
    {
      aDate = fmt.parse(tempDate);
    }
    catch(ParseException e)
    {
      aDate = null;
    }
    return aDate;
  }
  
  private Timestamp getSqlTimeStamp(String ts)
  {
    Timestamp timest = null;

    if(isBlank(ts))
    {
      return timest;
    }

    try
    {
      timest = Timestamp.valueOf(ts);
    }
    catch(Exception e)
    {
      System.out.println("timestamp crashing: " + e.toString());
    }
    return timest;
  } 

  public boolean isBlank(String test)
  {
    boolean pass = false;
  
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
  
    return pass;
  }


  public class ResponseRec
  {
    private static final int  NO_ERROR                       = -1;
    private static final int  MISSING_DATA                   = 10;
    private static final int  DISC_MERCH_NUM_NOT_BLANK       = 11;
    private static final int  DECIS_DATE_NOT_BLANK           = 12;
    private static final int  BLANK_DIS_CODE                 = 13;
    private static final int  APP_SEQ_NUM_NOT_FOUND          = 14;


    private String            appSeqNum                      = "";
    private String            discoverMerchNumber            = "";
    private String            acquirerMerchNumber            = "";
    private String            decisionStatus                 = "";
    private String            dispositionCode                = "";
    private String            receivedDate                   = "";
    private String            decisionDate                   = "";

    private int               errorCode                      = NO_ERROR;

    ResponseRec()
    {}

    public void setAppSeqNum(String appSeqNum)
    {
      this.appSeqNum = appSeqNum;
    }
    public void setDiscoverMerchNumber(String discoverMerchNumber)
    {
      this.discoverMerchNumber = discoverMerchNumber;
    }
    public void setAcquirerMerchNumber(String acquirerMerchNumber)
    {
      this.acquirerMerchNumber = acquirerMerchNumber;
    }
    public void setDecisionStatus(String decisionStatus)
    {
      this.decisionStatus = decisionStatus;
    }
    public void setDispositionCode(String dispositionCode)
    {
      this.dispositionCode = dispositionCode;
    }

    public void setReceivedDate(String receivedDate)
    {
      //this.receivedDate = formatDate(receivedDate);
      this.receivedDate = formatForTimestamp(receivedDate);
    }

    public void setDecisionDate(String decisionDate)
    {
      //this.decisionDate = formatDate(decisionDate);
      this.decisionDate = formatForTimestamp(decisionDate);
    }

    public String formatForTimestamp(String td)
    {
      String result = "";

      if(isBlank(td))
      {
        return result;
      }
      else
      {
        //must add this to date so we can change it to timestamp format must be yyyy-mm-dd hh:mm:ss.fffffffff
        result = td + " 00:00:00.000000000";
      }
      return result;
    }

    public String formatDate(String tempDate)
    {
      String result = "";

      if(isBlank(tempDate))
      {
        return result;
      }
      else
      {
        if(tempDate.length() == 10)
        {
          String yyyy = tempDate.substring(0,4);
          String mm   = tempDate.substring(5,7);
          String dd   = tempDate.substring(8);
          result = mm + "/" + dd + "/" + yyyy;
        }
      }
      return result;
    }

    public void setErrorCode(String errorCode)
    {
      try
      {
        setErrorCode(Integer.parseInt(errorCode));
      }
      catch(Exception e)
      {
        logEntry("setErrorCode(" + errorCode + ")", e.toString());
      }
    }
    public void setErrorCode(int errorCode)
    {
      this.errorCode = errorCode;
    }

    public String getAppSeqNum()
    {
      return this.appSeqNum;
    }
    public String getDiscoverMerchNumber()
    {
      return this.discoverMerchNumber;
    }
    public String getAcquirerMerchNumber()
    {
      return this.acquirerMerchNumber;
    }
    public String getDecisionStatus()
    {
      return this.decisionStatus;
    }
    public String getDispositionCode()
    {
      return this.dispositionCode;
    }
    public String getReceivedDate()
    {
      return this.receivedDate;
    }
    public String getDecisionDate()
    {
      return this.decisionDate;
    }

    public int getErrorCode()
    {
      return this.errorCode;
    }


    public boolean isValid()
    {
      boolean result = true;

      if(decisionStatus.equals("P") && isBlank(dispositionCode))
      {
        this.dispositionCode = "PM";
      }

      if(decisionStatus.equals("D") && dispositionCode.equals("RD"))
      {
        this.decisionStatus  = "A";
        this.dispositionCode = "AE";
      }

      if(isBlank(appSeqNum))
      {
        result = false;
        setErrorCode(APP_SEQ_NUM_NOT_FOUND);
      }  
      else if(isBlank(acquirerMerchNumber) || isBlank(decisionStatus) || isBlank(receivedDate) || isBlank(dispositionCode))
      {
        result = false;
        setErrorCode(MISSING_DATA);
      }
/*
      else if((dispositionCode.equals("PD") || dispositionCode.equals("IE")) && !isBlank(discoverMerchNumber))
      {
        result = false;
        setErrorCode(DISC_MERCH_NUM_NOT_BLANK);
      }
*/
      else if(decisionStatus.equals("P") && !isBlank(decisionDate))
      {
        result = false;
        setErrorCode(DECIS_DATE_NOT_BLANK);
      }
      else if(dispositionCode.equals("PM") && !decisionStatus.equals("P"))
      {
        result = false;
        setErrorCode(BLANK_DIS_CODE);
      }
      
      return result;
    }


    public boolean isBlank(String test)
    {
      boolean pass = false;
    
      if(test == null || test.equals("") || test.length() == 0)
      {
        pass = true;
      }
    
      return pass;
    }




  }






}/*@lineinfo:generated-code*/