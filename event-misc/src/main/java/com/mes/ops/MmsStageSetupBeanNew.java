/*@lineinfo:filename=MmsStageSetupBeanNew*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/MmsStageSetupBeanNew.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-01-09 15:58:49 -0800 (Fri, 09 Jan 2009) $
  Version            : $Revision: 15700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Locale;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.equipment.TermAppProfGenRelationship;
import com.mes.equipment.profile.EquipProfileGeneratorDictator;
import com.mes.queues.QueueTools;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class MmsStageSetupBeanNew extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(MmsStageSetupBeanNew.class);

  public  static final String NOFILE                      = "NOFILE";
  public  static final String CARD_AC_AUTH_ONLY           = "0";
  public  static final String CARD_AC_EDC                 = "1";

  public  static final String YES                         = "Y";
  public  static final String NO                          = "N";

  //default values
  public  static final String BIN_NUMBER_CBT                    = "413747";
  public  static final String BIN_NUMBER_NBSC                   = "448203";
  public  static final String BIN_NUMBER_3942                   = "430193";
  public  static final String BIN_NUMBER_DEFAULT                = "433239";
  public  static final String CHAIN_NUMBER_NSI                  = "600019";
  public  static final String CHAIN_NUMBER_VERISIGN             = "600055";
  public  static final String CHAIN_NUMBER_DEFAULT              = "000000";
  public  static final String AGENT_NUMBER_DEFAULT              = "000000";
  public  static final String LOCATION_NUMBER_DEFAULT           = "00001";
  public  static final String STORE_NUMBER_DEFAULT              = "0001";
  public  static final String TERMINAL_NUMBER_DEFAULT           = "0001";
  public  static final String SERVICE_LEVEL_DEFAULT             = "RNO";
  public  static final String ATTACH_CODE_DEFAULT               = "IT";
  public  static final String VOICE_AUTH_PHONE_DEFAULT          = "18002914840";
  public  static final String CDSET_DEFAULT                     = "00";
  public  static final String NO_TERMINAL_SELECTED              = "na";
  public  static final String AUTO_FLAG_DEFAULT                 = "Y";
  public  static final String CUSTOMER_SERVICE_PHONE_DEFAULT    = "8882882692";
  public  static final String TERM_APP_DEFAULT                  = mesConstants.MMS_STAGE_BUILD_TERM_APP;

  private static final int    FIELD_LENGTH_BIN                  = 6;
  private static final int    FIELD_LENGTH_AGENT                = 6;
  private static final int    FIELD_LENGTH_CHAIN                = 6;
  private static final int    FIELD_LENGTH_MERCHNUM             = 12;
  private static final int    FIELD_LENGTH_STORENUM             = 4;
  private static final int    FIELD_LENGTH_TERMINALNUM          = 4;
  private static final int    FIELD_LENGTH_MERCHNAME            = 23;
  private static final int    FIELD_LENGTH_ADDRESS              = 23;
  private static final int    FIELD_LENGTH_CITY                 = 13;
  private static final int    FIELD_LENGTH_STATE                = 2;
  private static final int    FIELD_LENGTH_ZIP                  = 5;
  private static final int    FIELD_LENGTH_MERCHPHONE           = 12;
  private static final int    FIELD_LENGTH_SERVICELEVEL         = 3;
  private static final int    FIELD_LENGTH_ATTACHCODE           = 2;
  private static final int    FIELD_LENGTH_MCC                  = 4;
  private static final int    FIELD_LENGTH_TERMAPP              = 7;
  private static final int    FIELD_LENGTH_DISCNUM              = 15;
  private static final int    FIELD_LENGTH_DISCAC               = 1;
  private static final int    FIELD_LENGTH_AMEXNUM              = 10;
  private static final int    FIELD_LENGTH_AMEXAC               = 1;
  private static final int    FIELD_LENGTH_AMEXSPLIT            = 1;
  private static final int    FIELD_LENGTH_DINERSNUM            = 10;
  private static final int    FIELD_LENGTH_DINERSAC             = 1;
  private static final int    FIELD_LENGTH_JCBNUM               = 10;
  private static final int    FIELD_LENGTH_JCBAC                = 1;
  private static final int    FIELD_LENGTH_TERMVOICEID          = 16;
  private static final int    FIELD_LENGTH_VOICEAUTHPHONE       = 12;
  private static final int    FIELD_LENGTH_CDSET                = 2;
  private static final int    FIELD_LENGTH_PROJECTNUM           = 4;
  private static final int    FIELD_LENGTH_CUSTOMERSERVICENUM   = 10;
  private static final int    FIELD_LENGTH_DST                  = 1;
  private static final int    FIELD_LENGTH_CONTACTNAME          = 20;
  private static final int    FIELD_LENGTH_MCFS                 = 15;

  private long                primaryKey                  = 0L;
  private long                requestId                   = 0L;

  private int                 appType                     = -1;
  private String              appTypeDesc                 = "";
  private String              appTypeNonDeploy            = "";
  private String              includeFileName             = NOFILE;

  private ResultSetIterator   it                          = null;

  private boolean             submitted                   = false;
  private boolean             reSubmitted                 = false;
  private boolean             manSubmitted                = false;
  private boolean             allowManUpdate              = false;
  private boolean             okToGetGets                 = true;

  private boolean             queuedForTrans              = false;
  private boolean             errorStatus                 = false;

  private boolean             showTimestamps              = false;


  private Timestamp           today                       = null;

  private String              vNum                        = "";
  private String              binNum                      = "";
  private String              agentNum                    = "";
  private String              chainNum                    = "";
  private String              merchNum                    = "";
  private String              storeNum                    = "";
  private String              terminalNum                 = "";
  private String              equipModel                  = "";
  private String              equipModelRefurb            = "";

  private String              merchName                   = "";
  private String              merchAddress                = "";
  private String              merchCity                   = "";
  private String              merchState                  = "";
  private String              merchZip                    = "";
  private String              merchPhone                  = "";
  private String              serviceLevel                = "";
  private String              attachCode                  = "";
  private String              mcc                         = "";
  private String              termApp                     = "";
  private String              termAppProfGen              = "";
  private String              discNum                     = "";
  private String              discAc                      = "";
  private String              amexNum                     = "";
  private String              amexAc                      = "";
  private String              amexSplit                   = "";
  private String              dinersNum                   = "";
  private String              dinersAc                    = "";
  private String              jcbNum                      = "";
  private String              jcbAc                       = "";
  private String              debitAc                     = "";
  private String              termVoiceId                 = "";
  private String              voiceAuthPhone              = "";
  private String              cdSet                       = "";
  private String              timeZone                    = "";
  private String              locationNum                 = "";
  private int                 frontEnd                    = -1;
  private int                 commType                    = -1;
  private String              merchKeyRequired            = "n";

  // Certegy Check and Valutec
  private boolean             CheckAccepted               = false;
  private String              CheckProvider               = "";
  private String              CheckMerchId                = "";
  private String              CheckTermId                 = "";
  private boolean             ValutecAccepted             = false;
  private String              ValutecMerchId              = "";
  private String              ValutecTermId               = "";

  private String              dst                         = "";
  private String              contactName                 = "";
  private String              projectNum                  = "";
  private String              mcfs                        = "";
  private String              customerServiceNum          = "";


  private String              transmissionMethod          = "";
  private String              transmissionStatus          = "";
  private String              transmissionResponse        = "";

  private String              dateTimeOfTransmission      = "";
  private String              dateTimeOfResponse          = "";

  private String              specialHandling             = "";

  private int                 profileGeneratorCode        = mesConstants.PROFGEN_UNDEFINED;
    // the entity [company] slated to generate the equipment profile (not material to the actual mms request)

  //variable for updating status info manually
  private String              manVnum                     = "";
  private String              manStatus                   = "";
  private String              manResponse                 = "";

  private String              errorDescriptions           = "";

  public  Vector              states                      = new Vector();
  public  Vector              errors                      = new Vector();
  public  Vector              msgs                        = new Vector();

  public  Vector              sicCodes                    = new Vector();
  public  Vector              sicCodeDesc                 = new Vector();

  public  Vector              attachCodes                 = new Vector();
  public  Vector              attachCodeDesc              = new Vector();

  public  Vector              binNums                     = new Vector();

  public  Vector              termApps                    = new Vector();
  public  Vector              termAppDesc                 = new Vector();

  public  Vector              cardAcCodes                 = new Vector();
  public  Vector              cardAcDesc                  = new Vector();

  public  Vector              status                      = new Vector();
  public  Vector              response                    = new Vector();

  public  Vector              timeZoneCodes               = new Vector();
  public  Vector              timeZoneDesc                = new Vector();

  public  Vector              equipModelDesc              = new Vector();
  public  Vector              equipModelDescCode          = new Vector();

  public  Vector              profGenNames                = new Vector();
  public  Vector              profGenCodes                = new Vector();

  private String              action                      = "";


  public String[][] nv_eciType =
  {
     {"Select",""}
    ,{"Off","Off"}
    ,{"Prompt","Prompt"}
    ,{"Secure","Secure"}
  };

  public String[][] nv_clerkServerMode =
  {
     {"Select",""}
    ,{"None","None"}
    ,{"Logon","Logon"}
    ,{"Prompt","Prompt"}
  };

  public String[][] nv_amexOption =
  {
     {"Select",""}
    ,{"None","None"}
    ,{"Split","Split"}
    ,{"PIP","PIP"}
  };

  public String[][] nv_baudRateHost1 =
  {
     {"Select",""}
    ,{"300","300"}
    ,{"600","600"}
    ,{"1200","1200"}
    ,{"2400","2400"}
  };

  public String[][] nv_baudRateAmexHost =
  {
     {"Select",""}
    ,{"300","300"}
    ,{"600","600"}
    ,{"1200","1200"}
    ,{"2400","2400"}
  };

  public String[][] FrontEndList =
  {
    {"Select",  "-1"},
    {"Vital",   Integer.toString(mesConstants.HOST_VITAL)},
    {"Trident", Integer.toString(mesConstants.HOST_TRIDENT)}
  };

  public String[][] CommTypeList =
  {
    {"Select",  "-1"},
    {"Dial",    Integer.toString(mesConstants.TERM_COMM_TYPE_DIAL)  },
    {"IP",      Integer.toString(mesConstants.TERM_COMM_TYPE_IP)    },
    {"Wireless",Integer.toString(mesConstants.TERM_COMM_TYPE_WIRELESS)}
  };

  public MmsStageSetupBeanNew()
  {
    fillDropDowns();
  }

  public String getTitle()
  {
    // based on the resident mms setup queue of the this mms request
    String title;
    switch(QueueTools.getCurrentQueueType(requestId,MesQueues.Q_ITEM_TYPE_MMS)) {
      default:
      case MesQueues.Q_MMS_NEW:
        title = "POS Setup Screen";
        break;
      case MesQueues.Q_MMS_VCTM_NEW:
        title = "VC/TM - POS Setup Screen";
        break;
    }

    return title;
  }

  private void updateAllFails()
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:339^7*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//          set     process_response   = :mesConstants.ETPPR_FAIL,
//                  process_status     = :mesConstants.ETPPS_NO_RESPONSE
//          where   process_start_date is not null and process_end_date is null and (months_between(:today,process_start_date) > .07)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n        set     process_response   =  :1 ,\n                process_status     =  :2 \n        where   process_start_date is not null and process_end_date is null and (months_between( :3 ,process_start_date) > .07)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mesConstants.ETPPR_FAIL);
   __sJT_st.setString(2,mesConstants.ETPPS_NO_RESPONSE);
   __sJT_st.setTimestamp(3,today);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:345^7*/
    }
    catch(Exception e)
    {
      logEntry("updateAllFails()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public String getTermAppDescriptor()
  {
    StringBuffer sb = new StringBuffer();

    if(profileGeneratorCode == mesConstants.PROFGEN_MMS)
    {
      sb.append(termAppProfGen);
    }
    else
    {
      sb.append("MMS: '");
      sb.append(termApp);
      sb.append("' - ");
      sb.append(EquipProfileGeneratorDictator.getEquipmentProfileGeneratorName(profileGeneratorCode));
      sb.append(": '");
      sb.append(termAppProfGen);
      sb.append("'");
    }

    return sb.toString();
  }

  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */
  public void fillDropDowns()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:393^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  countrystate_code
//          from    countrystate
//          order by countrystate_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  countrystate_code\n        from    countrystate\n        order by countrystate_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:398^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        states.add(rs.getString("countrystate_code"));
      }

      rs.close();
      it.close();


      /*@lineinfo:generated-code*//*@lineinfo:411^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sic_code,merchant_type
//          from    sic_codes
//          order by sic_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sic_code,merchant_type\n        from    sic_codes\n        order by sic_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:416^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        sicCodes.add(rs.getString("sic_code"));
        sicCodeDesc.add(rs.getString("merchant_type"));
      }

      rs.close();
      it.close();


      /*@lineinfo:generated-code*//*@lineinfo:430^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  attachment_code,
//                  attachment_code_desc
//          from    mms_attachment_codes
//          order by attachment_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  attachment_code,\n                attachment_code_desc\n        from    mms_attachment_codes\n        order by attachment_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:436^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        attachCodes.add(rs.getString("attachment_code"));
        attachCodeDesc.add(rs.getString("attachment_code_desc"));
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:449^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  code,
//                  description
//          from    time_zones
//          order by code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  code,\n                description\n        from    time_zones\n        order by code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:455^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        timeZoneCodes.add(rs.getString("code"));
        timeZoneDesc.add(rs.getString("description"));
      }

      rs.close();
      it.close();


      /*@lineinfo:generated-code*//*@lineinfo:469^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bin_number,
//                  bank_name
//          from    mms_bin_numbers
//          order by sort_order asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bin_number,\n                bank_name\n        from    mms_bin_numbers\n        order by sort_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:475^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        String[] binNumArray = { rs.getString("bin_number"), rs.getString("bank_name") };
        binNums.add(binNumArray);
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:488^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  terminal_application,
//                  terminal_application_desc
//          from    mms_terminal_applications
//          where   nvl(active, 'Y') != 'N'
//          order by terminal_application asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  terminal_application,\n                terminal_application_desc\n        from    mms_terminal_applications\n        where   nvl(active, 'Y') != 'N'\n        order by terminal_application asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:495^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        termApps.add(rs.getString("terminal_application"));
        termAppDesc.add(rs.getString("terminal_application_desc"));
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:508^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model,
//                  equip_descriptor
//          from    equipment
//          where   equiptype_code in (1,5,6)
//          order by equip_descriptor asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model,\n                equip_descriptor\n        from    equipment\n        where   equiptype_code in (1,5,6)\n        order by equip_descriptor asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:515^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        equipModelDesc.add(rs.getString("equip_descriptor"));
        equipModelDescCode.add(rs.getString("equip_model"));
      }

      rs.close();
      it.close();

      int pgc_exclude   = mesConstants.PROFGEN_UNDEFINED;
      int pgc_exclude2  = mesConstants.PROFGEN_VCTM;

      /*@lineinfo:generated-code*//*@lineinfo:531^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  code,
//                  name
//          from    equip_profile_generators
//          where   code <> :pgc_exclude
//                  and code <> :pgc_exclude2
//          order by code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  code,\n                name\n        from    equip_profile_generators\n        where   code <>  :1 \n                and code <>  :2 \n        order by code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,pgc_exclude);
   __sJT_st.setInt(2,pgc_exclude2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:539^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        profGenNames.add(rs.getString("name"));
        profGenCodes.add(rs.getString("code"));
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:552^7*/

//  ************************************************************
//  #sql [Ctx] { select  sysdate 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sysdate  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   today = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:556^7*/

      cardAcCodes.add(CARD_AC_AUTH_ONLY);
      cardAcDesc.add("Auth Only");
      cardAcCodes.add(CARD_AC_EDC);
      cardAcDesc.add("EDC");
    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    //statuses
    status.add(mesConstants.ETPPS_NEW);
    status.add(mesConstants.ETPPS_INPROCESS);
    status.add(mesConstants.ETPPS_RECEIVED);
    status.add(mesConstants.ETPPS_RESUBMITTED);
    status.add(mesConstants.ETPPS_NO_RESPONSE);

    //RESPONSES
    response.add(mesConstants.ETPPR_PENDING);
    response.add(mesConstants.ETPPR_SUCCESS);
    response.add(mesConstants.ETPPR_FAIL);

  }


  public void setDefaults()
  {
    switch(this.appType)
    {
      case mesConstants.APP_TYPE_CBT:
      case mesConstants.APP_TYPE_CBT_NEW:
        binNum  = isBlank(binNum) ? BIN_NUMBER_CBT : binNum;
        break;
      case mesConstants.APP_TYPE_NBSC:
        binNum  = isBlank(binNum) ? BIN_NUMBER_NBSC : binNum;
        break;
      case mesConstants.APP_TYPE_STERLING_3942:
        binNum  = isBlank(binNum) ? BIN_NUMBER_3942 : binNum;
        break;
      default:
        binNum  = isBlank(binNum) ? BIN_NUMBER_DEFAULT : binNum;
        break;
    }

    switch(this.appType)
    {
      case mesConstants.APP_TYPE_VERISIGN:
        chainNum         = isBlank(chainNum)        ?   CHAIN_NUMBER_VERISIGN     : chainNum;
      break;
      case mesConstants.APP_TYPE_NSI:
        chainNum         = isBlank(chainNum)        ?   CHAIN_NUMBER_NSI          : chainNum;
      break;
      default:
        chainNum         = isBlank(chainNum)        ?   CHAIN_NUMBER_DEFAULT      : chainNum;
      break;
    }

    termApp              = isBlank(termApp)             ?   TERM_APP_DEFAULT : termApp;
    termAppProfGen       = isBlank(termAppProfGen)      ?   TermAppProfGenRelationship.getDefaultProfGenTermApp(profileGeneratorCode) : termAppProfGen;

    agentNum             = isBlank(agentNum)            ?   AGENT_NUMBER_DEFAULT            : agentNum;
    storeNum             = isBlank(storeNum)            ?   STORE_NUMBER_DEFAULT            : storeNum;
    locationNum          = isBlank(locationNum)         ?   LOCATION_NUMBER_DEFAULT         : locationNum;
    terminalNum          = isBlank(terminalNum)         ?   TERMINAL_NUMBER_DEFAULT         : terminalNum;
    serviceLevel         = isBlank(serviceLevel)        ?   SERVICE_LEVEL_DEFAULT           : serviceLevel;
    attachCode           = isBlank(attachCode)          ?   ATTACH_CODE_DEFAULT             : attachCode;
    voiceAuthPhone       = isBlank(voiceAuthPhone)      ?   VOICE_AUTH_PHONE_DEFAULT        : voiceAuthPhone;
    cdSet                = isBlank(cdSet)               ?   CDSET_DEFAULT                   : cdSet;
    specialHandling      = isBlank(specialHandling)     ?   NO                              : specialHandling;
    customerServiceNum   = isBlank(customerServiceNum)  ?   CUSTOMER_SERVICE_PHONE_DEFAULT  : customerServiceNum;
  }

  public long startNewApplication()
  {
    return setReqId();
  }

  public long figureOutPrimaryKey(long reqId, long merchantNumber)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    long                result      = 0L;

    try
    {
      connect();

      if(merchantNumber > 0L)
      {
        /*@lineinfo:generated-code*//*@lineinfo:653^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num
//            from    merchant
//            where   merch_number = :merchantNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num\n          from    merchant\n          where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:658^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          result = rs.getLong("app_seq_num");
        }

        rs.close();
        it.close();
      }

      if(result == 0L)
      {
        /*@lineinfo:generated-code*//*@lineinfo:673^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num
//            from    mms_stage_info
//            where   request_id   = :reqId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num\n          from    mms_stage_info\n          where   request_id   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:678^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          result = rs.getLong("app_seq_num");
        }

        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("figureOutPrimaryKey()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }


  public void getData(long pk, long id, long merchantNumber)
  {
    getAppTypeInfo(pk);
    getAppContent (pk,  id, merchantNumber);
    getProductInfo(pk);
  }


  private void getAppTypeInfo(long pk)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:722^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app.app_type,
//                  at.app_description,
//                  at.non_deploy_app
//          from    application app,
//                  app_type at
//          where   app.app_seq_num   = :pk and
//                  app.app_type      = at.app_type_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app.app_type,\n                at.app_description,\n                at.non_deploy_app\n        from    application app,\n                app_type at\n        where   app.app_seq_num   =  :1  and\n                app.app_type      = at.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:731^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        appType           = rs.getInt("app_type");
        appTypeDesc       = rs.getString("app_description");
        appTypeNonDeploy  = isBlank(rs.getString("non_deploy_app")) ? "N" : rs.getString("non_deploy_app");
      }

      it.close();

    }
    catch(Exception e)
    {
      logEntry("getAppTypeInfo()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }

  /*
  ** METHOD public void truncateAppData()
  **
  ** Truncates relevant OLA related data members to the length dictated by the
  ** associated field length constant.
  */
  private void truncateAppData()
  {
    if(merchName.length()>FIELD_LENGTH_MERCHNAME)
      merchName       = merchName.substring(0,FIELD_LENGTH_MERCHNAME);
    if(merchAddress.length()>FIELD_LENGTH_ADDRESS)
      merchAddress    = merchAddress.substring(0,FIELD_LENGTH_ADDRESS);
    if(merchCity.length()>FIELD_LENGTH_CITY)
      merchCity       = merchCity.substring(0,FIELD_LENGTH_CITY);
    if(merchZip.length()>FIELD_LENGTH_ZIP)
      merchZip        = merchZip.substring(0,FIELD_LENGTH_ZIP);
    if(contactName.length()>FIELD_LENGTH_CONTACTNAME)
      contactName     = contactName.substring(0,FIELD_LENGTH_CONTACTNAME);
  }

  private boolean merchantExistsInMif(long merchantNumber)
  {
    boolean result = false;
    try
    {
      connect();

      int merchCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:784^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//          
//          from    mif
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n         \n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:790^7*/

      result = (merchCount > 0);
    }
    catch(Exception e)
    {
      logEntry("merchantExistsInMif(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  /*
  ** METHOD public void getAppContent()
  **
  */
  public void getAppContent(long pk, long id, long merchantNumber)
  {
    String    dateOfTransmission = "";
    String    timeOfTransmission = "";
    String    dateOfResponse     = "";
    String    timeOfResponse     = "";

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:821^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    mms_stage_info
//          where   request_id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    mms_stage_info\n        where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:826^7*/

      ResultSet rs = it.getResultSet();

      if(rs.next())
      {
        queuedForTrans             = true;

        this.primaryKey            = rs.getLong("APP_SEQ_NUM");
        this.requestId             = rs.getLong("request_id");
        this.transmissionMethod    = isBlank(rs.getString("process_method"))      ? ""  : rs.getString("process_method");
        this.transmissionStatus    = isBlank(rs.getString("process_status"))      ? ""  : rs.getString("process_status");
        this.transmissionResponse  = isBlank(rs.getString("process_response"))    ? ""  : rs.getString("process_response");
        this.specialHandling       = isBlank(rs.getString("SPECIAL_HANDLING"))    ? "N" : rs.getString("SPECIAL_HANDLING");

        if((transmissionResponse.equals(mesConstants.ETPPR_PENDING) || transmissionResponse.equals(mesConstants.ETPPR_FAIL)) && !transmissionStatus.equals(mesConstants.ETPPS_RESUBMITTED))
        {
          this.errorStatus = true;
        }

        this.vNum                  = isBlank(rs.getString("VNUM"))                  ? ""  : rs.getString("VNUM");

        this.binNum                = isBlank(rs.getString("BIN"))                   ? ""  : rs.getString("BIN");
        this.agentNum              = isBlank(rs.getString("AGENT"))                 ? ""  : rs.getString("AGENT");
        this.chainNum              = isBlank(rs.getString("CHAIN"))                 ? ""  : rs.getString("CHAIN");
        this.merchNum              = isBlank(rs.getString("MERCH_NUMBER"))          ? ""  : rs.getString("MERCH_NUMBER");
        this.locationNum           = isBlank(rs.getString("LOCATION_NUMBER"))       ? ""  : rs.getString("LOCATION_NUMBER");
        this.storeNum              = isBlank(rs.getString("STORE_NUMBER"))          ? ""  : rs.getString("STORE_NUMBER");
        this.terminalNum           = isBlank(rs.getString("TERMINAL_NUMBER"))       ? ""  : rs.getString("TERMINAL_NUMBER");
        this.equipModel            = isBlank(rs.getString("EQUIP_MODEL"))           ? ""  : rs.getString("EQUIP_MODEL");
        this.equipModelRefurb      = isBlank(rs.getString("EQUIP_REFURB"))          ? ""  : rs.getString("EQUIP_REFURB");
        this.merchName             = isBlank(rs.getString("BUSINESS_NAME"))         ? ""  : rs.getString("BUSINESS_NAME");
        this.merchAddress          = isBlank(rs.getString("BUSINESS_ADDRESS"))      ? ""  : rs.getString("BUSINESS_ADDRESS");
        this.merchCity             = isBlank(rs.getString("BUSINESS_CITY"))         ? ""  : rs.getString("BUSINESS_CITY");
        this.merchState            = isBlank(rs.getString("BUSINESS_STATE"))        ? ""  : rs.getString("BUSINESS_STATE");
        this.merchZip              = isBlank(rs.getString("BUSINESS_ZIP"))          ? ""  : rs.getString("BUSINESS_ZIP");
        this.merchPhone            = isBlank(rs.getString("MERCH_PHONE"))           ? ""  : rs.getString("MERCH_PHONE");
        this.serviceLevel          = isBlank(rs.getString("SERVICE_LEVEL"))         ? ""  : rs.getString("SERVICE_LEVEL");
        this.attachCode            = isBlank(rs.getString("ATTACHMENT_CODE"))       ? ""  : rs.getString("ATTACHMENT_CODE");
        this.mcc                   = isBlank(rs.getString("MCC"))                   ? ""  : rs.getString("MCC");
        this.termApp               = isBlank(rs.getString("TERM_APPLICATION"))      ? ""  : rs.getString("TERM_APPLICATION");
        this.termAppProfGen        = isBlank(rs.getString("TERM_APPLICATION_PROFGEN")) ? termApp  : rs.getString("TERM_APPLICATION_PROFGEN");
        this.timeZone              = isBlank(rs.getString("TIME_ZONE"))             ? ""  : rs.getString("TIME_ZONE");

        this.discNum               = isBlank(rs.getString("DISC"))                  ? ""  : rs.getString("DISC");
        this.discAc                = isBlank(rs.getString("DISC_AC"))               ? ""  : rs.getString("DISC_AC");
        this.amexNum               = isBlank(rs.getString("AMEX"))                  ? ""  : rs.getString("AMEX");
        this.amexAc                = isBlank(rs.getString("AMEX_AC"))               ? ""  : rs.getString("AMEX_AC");
        this.amexSplit             = isBlank(rs.getString("SPLIT_DIAL"))            ? ""  : rs.getString("SPLIT_DIAL");
        this.dinersNum             = isBlank(rs.getString("DINR"))                  ? ""  : rs.getString("DINR");
        this.dinersAc              = isBlank(rs.getString("DINR_AC"))               ? ""  : rs.getString("DINR_AC");
        this.jcbNum                = isBlank(rs.getString("JCB"))                   ? ""  : rs.getString("JCB");
        this.jcbAc                 = isBlank(rs.getString("JCB_AC"))                ? ""  : rs.getString("JCB_AC");
        this.debitAc               = isBlank(rs.getString("DEBIT_ENABLED"))         ? "N" : rs.getString("DEBIT_ENABLED");
        this.termVoiceId           = isBlank(rs.getString("TERM_VOICE_ID"))         ? ""  : rs.getString("TERM_VOICE_ID");
        this.voiceAuthPhone        = isBlank(rs.getString("VOICE_AUTH_PHONE"))      ? ""  : rs.getString("VOICE_AUTH_PHONE");
        this.cdSet                 = isBlank(rs.getString("CDSET"))                 ? ""  : rs.getString("CDSET");

        setMerchKeyRequired(rs.getString("merchant_key_required"));

        dateOfTransmission         = isBlank(rs.getString("process_start_date"))    ? ""  : java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("process_start_date"));
        timeOfTransmission         = isBlank(rs.getString("process_start_date"))    ? ""  : java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("process_start_date"));
        dateTimeOfTransmission     = glueDate(dateOfTransmission,timeOfTransmission);

        dateOfResponse             = isBlank(rs.getString("process_end_date"))      ? ""  : java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("process_end_date"));
        timeOfResponse             = isBlank(rs.getString("process_end_date"))      ? ""  : java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("process_end_date"));
        dateTimeOfResponse         = glueDate(dateOfResponse,timeOfResponse);

        dst                        = isBlank(rs.getString("DST"))                   ? ""  : rs.getString("DST");
        contactName                = isBlank(rs.getString("CONTACT_NAME"))          ? ""  : rs.getString("CONTACT_NAME");
        projectNum                 = isBlank(rs.getString("PROJECT_NUM"))           ? ""  : rs.getString("PROJECT_NUM");
        mcfs                       = isBlank(rs.getString("MCFS"))                  ? ""  : rs.getString("MCFS");
        customerServiceNum         = isBlank(rs.getString("CUSTOMER_SERVICE_NUM"))  ? ""  : rs.getString("CUSTOMER_SERVICE_NUM");

        profileGeneratorCode       = isBlank(rs.getString("PROFILE_GENERATOR_CODE"))? mesConstants.DEFAULT_PROFGEN  : rs.getInt("PROFILE_GENERATOR_CODE");

        rs.close();
        it.close();

        /*@lineinfo:generated-code*//*@lineinfo:905^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    merchpo_provider_name       as provider,
//                      cardtype_code               as app_card_type,
//                      merchpo_card_merch_number   as merchant_number,
//                      merchpo_tid                 as tid
//            from      merchpayoption mpo
//            where     mpo.app_seq_num = :pk and
//                      mpo.cardtype_code in
//                      (
//                        :mesConstants.APP_CT_CHECK_AUTH,        -- 18,
//                        :mesConstants.APP_CT_VALUTEC_GIFT_CARD  -- 24
//                      )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    merchpo_provider_name       as provider,\n                    cardtype_code               as app_card_type,\n                    merchpo_card_merch_number   as merchant_number,\n                    merchpo_tid                 as tid\n          from      merchpayoption mpo\n          where     mpo.app_seq_num =  :1  and\n                    mpo.cardtype_code in\n                    (\n                       :2 ,        -- 18,\n                       :3   -- 24\n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   __sJT_st.setInt(2,mesConstants.APP_CT_CHECK_AUTH);
   __sJT_st.setInt(3,mesConstants.APP_CT_VALUTEC_GIFT_CARD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:918^9*/
        rs = it.getResultSet();

        while( rs.next() )
        {
          switch( rs.getInt("app_card_type") )
          {
            case mesConstants.APP_CT_CHECK_AUTH:
              CheckAccepted = true;
              CheckTermId   = rs.getString("tid");
              CheckMerchId  = rs.getString("merchant_number");
              CheckProvider = MmsManagementBean.decodeCheckProviderName(rs.getString("merchpo_provider_name"));
              break;

            case mesConstants.APP_CT_VALUTEC_GIFT_CARD:
              ValutecAccepted = true;
              ValutecTermId   = rs.getString("tid");
              ValutecMerchId  = rs.getString("merchant_number");
              break;

          }
        }
        rs.close();
        it.close();

        //try to get errors if they exist
        getCurrentErrors(id);
      }
      else
      {
        rs.close();
        if(merchantExistsInMif(merchantNumber))
        {
          /*@lineinfo:generated-code*//*@lineinfo:951^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  mr.app_seq_num                  app_seq_num,
//                      m.dba_name                      business_name,
//                      m.sic_code                      mcc,
//                      decode(m.bank_number,
//                        3858, '0'||m.merchant_number||'000',
//                        m.merchant_number)            merch_number,
//                      decode(m.bank_number,
//                        3858, '0'||m.merchant_number,
//                        m.merchant_number)            term_voice_id,
//                      m.dmaddr                        business_address,
//                      m.dmcity                        business_city,
//                      m.dmstate                       business_state,
//                      m.dmzip                         business_zip,
//                      m.phone_1                       business_phone,
//                      mc.merchcont_prim_first_name    contact_first_name,
//                      mc.merchcont_prim_last_name     contact_last_name,
//                      nvl(mc.merchcont_prim_phone,
//                          m.phone_1)                  merch_phone,
//                      mdar.decision_status            disc_status,
//                      mdar.discover_merchant_num      disc_merchant_num,
//                      nvl(zc.timezone, '-1')          time_zone,
//                      nvl(zc.daylight_savings, '-1')  daylight_savings,
//                      decode(m.debit_plan,
//                        null, 'N',
//                        'Y')                          debit_ac
//              from    merchant                        mr,
//                      mif                             m,
//                      merchcontact                    mc,
//                      merchant_discover_app_response  mdar,
//                      zip_codes                       zc
//              where   m.merchant_number = :merchantNumber and
//                      m.merchant_number = mr.merch_number(+) and
//                      mr.app_seq_num = mc.app_seq_num(+) and
//                      mr.app_seq_num = mdar.app_seq_num(+) and
//                      substr(m.dmzip, 1, 5) = zc.zip_code(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mr.app_seq_num                  app_seq_num,\n                    m.dba_name                      business_name,\n                    m.sic_code                      mcc,\n                    decode(m.bank_number,\n                      3858, '0'||m.merchant_number||'000',\n                      m.merchant_number)            merch_number,\n                    decode(m.bank_number,\n                      3858, '0'||m.merchant_number,\n                      m.merchant_number)            term_voice_id,\n                    m.dmaddr                        business_address,\n                    m.dmcity                        business_city,\n                    m.dmstate                       business_state,\n                    m.dmzip                         business_zip,\n                    m.phone_1                       business_phone,\n                    mc.merchcont_prim_first_name    contact_first_name,\n                    mc.merchcont_prim_last_name     contact_last_name,\n                    nvl(mc.merchcont_prim_phone,\n                        m.phone_1)                  merch_phone,\n                    mdar.decision_status            disc_status,\n                    mdar.discover_merchant_num      disc_merchant_num,\n                    nvl(zc.timezone, '-1')          time_zone,\n                    nvl(zc.daylight_savings, '-1')  daylight_savings,\n                    decode(m.debit_plan,\n                      null, 'N',\n                      'Y')                          debit_ac\n            from    merchant                        mr,\n                    mif                             m,\n                    merchcontact                    mc,\n                    merchant_discover_app_response  mdar,\n                    zip_codes                       zc\n            where   m.merchant_number =  :1  and\n                    m.merchant_number = mr.merch_number(+) and\n                    mr.app_seq_num = mc.app_seq_num(+) and\n                    mr.app_seq_num = mdar.app_seq_num(+) and\n                    substr(m.dmzip, 1, 5) = zc.zip_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:988^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:992^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.app_seq_num                   app_seq_num,
//                      merch.merch_business_name           business_name,
//                      merch.sic_code                      mcc,
//                      decode(length(merch.merch_number),
//                        8, '0'||merch.merch_number||'000',
//                        merch.merch_number)               merch_number,
//                      decode(length(merch.merch_number),
//                        8, '0'||merch.merch_number,
//                        merch.merch_number)               term_voice_id,
//                      add1.address_line1                  business_address,
//                      add1.address_city                   business_city,
//                      add1.countrystate_code              business_state,
//                      add1.address_zip                    business_zip,
//                      add1.address_phone                  business_phone,
//                      cont.merchcont_prim_first_name      contact_first_name,
//                      cont.merchcont_prim_last_name       contact_last_name,
//                      cont.merchcont_prim_phone           merch_phone,
//                      mdar.decision_status                disc_status,
//                      mdar.discover_merchant_num          disc_merchant_num,
//                      nvl(zc.timezone, '-1')              time_zone,
//                      nvl(zc.daylight_savings, '-1')      daylight_savings,
//                      decode(tc.cardtype_code,
//                        null, 'N',
//                        'Y')                              debit_ac
//              from    merchant                            merch,
//                      address                             add1,
//                      merchcontact                        cont,
//                      merchant_discover_app_response      mdar,
//                      zip_codes                           zc,
//                      tranchrg                            tc
//              where   merch.app_seq_num = :pk                 and
//                      merch.app_seq_num = cont.app_seq_num    and
//                      merch.app_seq_num = mdar.app_seq_num(+) and
//                      merch.app_seq_num = add1.app_seq_num    and
//                      add1.addresstype_code  = 1 and
//                      substr(add1.address_zip, 1, 5) = zc.zip_code(+) and
//                      merch.app_seq_num = tc.app_seq_num(+) and
//                      tc.cardtype_code(+) = 17
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.app_seq_num                   app_seq_num,\n                    merch.merch_business_name           business_name,\n                    merch.sic_code                      mcc,\n                    decode(length(merch.merch_number),\n                      8, '0'||merch.merch_number||'000',\n                      merch.merch_number)               merch_number,\n                    decode(length(merch.merch_number),\n                      8, '0'||merch.merch_number,\n                      merch.merch_number)               term_voice_id,\n                    add1.address_line1                  business_address,\n                    add1.address_city                   business_city,\n                    add1.countrystate_code              business_state,\n                    add1.address_zip                    business_zip,\n                    add1.address_phone                  business_phone,\n                    cont.merchcont_prim_first_name      contact_first_name,\n                    cont.merchcont_prim_last_name       contact_last_name,\n                    cont.merchcont_prim_phone           merch_phone,\n                    mdar.decision_status                disc_status,\n                    mdar.discover_merchant_num          disc_merchant_num,\n                    nvl(zc.timezone, '-1')              time_zone,\n                    nvl(zc.daylight_savings, '-1')      daylight_savings,\n                    decode(tc.cardtype_code,\n                      null, 'N',\n                      'Y')                              debit_ac\n            from    merchant                            merch,\n                    address                             add1,\n                    merchcontact                        cont,\n                    merchant_discover_app_response      mdar,\n                    zip_codes                           zc,\n                    tranchrg                            tc\n            where   merch.app_seq_num =  :1                  and\n                    merch.app_seq_num = cont.app_seq_num    and\n                    merch.app_seq_num = mdar.app_seq_num(+) and\n                    merch.app_seq_num = add1.app_seq_num    and\n                    add1.addresstype_code  = 1 and\n                    substr(add1.address_zip, 1, 5) = zc.zip_code(+) and\n                    merch.app_seq_num = tc.app_seq_num(+) and\n                    tc.cardtype_code(+) = 17";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1032^11*/
        }

        rs = it.getResultSet();

        if(rs.next())
        {
          this.primaryKey         = rs.getLong("app_seq_num");
          this.merchNum           = isBlank(rs.getString("merch_number"))        ? "" : rs.getString("merch_number");
          defaultMcfs(merchNum);
          this.merchName          = isBlank(rs.getString("business_name"))       ? "" : rs.getString("business_name");
          this.merchAddress       = isBlank(rs.getString("business_address"))    ? "" : rs.getString("business_address");
          this.merchCity          = isBlank(rs.getString("business_city"))       ? "" : rs.getString("business_city");
          this.merchState         = isBlank(rs.getString("business_state"))      ? "" : rs.getString("business_state");
          this.merchZip           = isBlank(rs.getString("business_zip"))        ? "" : rs.getString("business_zip");
          this.customerServiceNum = isBlank(rs.getString("business_phone"))      ? "" : rs.getString("business_phone");
          this.merchPhone         = isBlank(rs.getString("business_phone"))      ? "" : rs.getString("business_phone");
          this.mcc                = isBlank(rs.getString("mcc"))                 ? "" : rs.getString("mcc");
          this.termVoiceId        = isBlank(rs.getString("term_voice_id"))       ? "" : rs.getString("term_voice_id");
          this.contactName        = isBlank(rs.getString("contact_first_name"))  ? "" : (rs.getString("contact_first_name") + " ");
          this.contactName        += isBlank(rs.getString("contact_last_name"))   ? "" : rs.getString("contact_last_name");
          this.contactName        = this.contactName.trim();
          this.timeZone           = rs.getString("time_zone");
          this.dst                = rs.getString("daylight_savings");
//          this.debitAc            = rs.getString("debit_ac");

          //try to get disc merch # from easi link first..
          if(!isBlank(rs.getString("disc_status")) && rs.getString("disc_status").equals("A"))
          {
            this.discNum = isBlank(rs.getString("disc_merchant_num")) ? "" : rs.getString("disc_merchant_num");
            if(!isBlank(discNum))
            {
              this.discAc = "1";
            }
          }

        }
        else
        {
          this.timeZone = "";
          this.dst = "";
        }

        rs.close();
        it.close();

        // load the accepted cards from the application data
        /*@lineinfo:generated-code*//*@lineinfo:1079^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    merchpayoption
//            where   app_seq_num = :pk
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    merchpayoption\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1084^9*/

        rs = it.getResultSet();

        while(rs.next())
        {
          switch(rs.getInt("cardtype_code"))
          {
            case mesConstants.APP_CT_DISCOVER:
              //if we already got a disc merchant number get out of here...
              if(!isBlank(this.discNum))
              {
                break;
              }

              this.discNum    = isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER"))  ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
              if(isBlank(this.discNum) || this.discNum.equals("0"))
              {
                this.discNum    = "";
                this.discAc     = "";
              }
              else
              {
                this.discAc     = "1";
              }
            break;

            case mesConstants.APP_CT_AMEX:
              this.amexNum    = isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER"))  ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
              if(isBlank(this.amexNum) || this.amexNum.equals("0"))
              {
                this.amexNum    = "";
                this.amexAc     = "";
                this.amexSplit  = "";
              }
              else
              {
                this.amexAc     = "1";
                this.amexSplit  = "N";
              }
            break;

            case mesConstants.APP_CT_DINERS_CLUB:
              this.dinersNum  = isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER"))  ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
              if(isBlank(this.dinersNum) || this.dinersNum.equals("0"))
              {
                this.dinersNum  = "";
                this.dinersAc   = "";
              }
              else
              {
                this.dinersAc   = "1";
              }
            break;

            case mesConstants.APP_CT_JCB:
              this.jcbNum     = isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER"))  ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
              if(isBlank(this.jcbNum) || this.jcbNum.equals("0"))
              {
                this.jcbNum     = "";
                this.jcbAc      = "";
              }
              else
              {
                this.jcbAc      = "1";
              }
            break;

            case mesConstants.APP_CT_DEBIT:
              this.debitAc = "Y";
              break;

            case mesConstants.APP_CT_CHECK_AUTH:
              CheckAccepted     = true;
              CheckTermId       = rs.getString("merchpo_tid");
              CheckMerchId      = rs.getString("merchpo_card_merch_number");
              CheckProvider     = MmsManagementBean.decodeCheckProviderName(rs.getString("merchpo_provider_name"));
              break;

            case mesConstants.APP_CT_VALUTEC_GIFT_CARD:
              ValutecAccepted = true;
              ValutecMerchId  = rs.getString("merchpo_card_merch_number");
              ValutecTermId   = rs.getString("merchpo_tid");
              break;
          }
        }
        it.close();
      }

      truncateAppData();
    }
    catch(Exception e)
    {
      logEntry("getAppContent()", e.toString());
      addError("Error: Complications occurred while retrieving data!  Please refresh.");
    }
    finally
    {
      cleanUp();
    }
  }

  private void defaultMcfs(String mNum)
  {
    this.mcfs = mNum.trim();

    if(mcfs.length() < FIELD_LENGTH_MCFS)
    {

     String mcfsAddOn = "";
     for(int y=0; y<(FIELD_LENGTH_MCFS - mcfs.length()); y++)
     {
       mcfsAddOn += "0";
     }

     this.mcfs += mcfsAddOn.trim();

    }
    else if(mcfs.length() > FIELD_LENGTH_MCFS)
    {
      this.mcfs = this.mcfs.substring(0,16);
    }

  }

  public void getCurrentErrors(long id)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1218^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  error_description
//          from    mms_current_errors
//          where   request_id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  error_description\n        from    mms_current_errors\n        where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1223^7*/

      rs = it.getResultSet();

      errorDescriptions = "";
      while(rs.next())
      {
        errorDescriptions += (rs.getString("error_description") + ";<br>");
      }

      it.close();

    }
    catch(Exception e)
    {
      logEntry("getCurrentErrors()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }

  protected boolean firstTimeTrident()
  {
    boolean result = false;
    ResultSetIterator it   = null;
    ResultSet         rs   = null;

    try
    {
      if(frontEnd == mesConstants.HOST_TRIDENT)
      {
        // get bank number first
        int bankNumber = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:1260^9*/

//  ************************************************************
//  #sql [Ctx] { select  apt.app_bank_number
//            
//            from    application app,
//                    app_type apt
//            where   app.app_seq_num = :primaryKey
//                    and app.app_type = apt.app_type_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  apt.app_bank_number\n           \n          from    application app,\n                  app_type apt\n          where   app.app_seq_num =  :1 \n                  and app.app_type = apt.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1268^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:1270^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(app_seq_num)  app_count
//            from    mms_stage_info
//            where   app_seq_num = :primaryKey and
//                    term_application_profgen = 'STAGE'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(app_seq_num)  app_count\n          from    mms_stage_info\n          where   app_seq_num =  :1  and\n                  term_application_profgen = 'STAGE'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1276^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          result = (rs.getInt("app_count") == 0 && bankNumber != 3941);
        }

        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("firstTimeTrident(" + primaryKey + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }

    return result;
  }

  protected void addVitalStageOnly()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      setAutoCommit(false);

      // get a new request id
      long tempRequestId = 0L;

      /*@lineinfo:generated-code*//*@lineinfo:1314^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mms_application_sequence.nextval  temp_request_id
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mms_application_sequence.nextval  temp_request_id\n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1318^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        tempRequestId = rs.getLong("temp_request_id");
      }
      else
      {
        throw new Exception("could not retrieve new requestId");
      }

      rs.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:1334^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mms_stage_info
//          (
//            request_id,
//            app_seq_num,
//            term_application,
//            special_handling,
//            app_date,
//            process_method,
//            process_status,
//            bin,
//            agent,
//            chain,
//            merch_number,
//            store_number,
//            terminal_number,
//            business_name,
//            business_address,
//            business_city,
//            business_state,
//            business_zip,
//            merch_phone,
//            service_level,
//            attachment_code,
//            mcc,
//            disc,
//            disc_ac,
//            amex,
//            amex_ac,
//            split_dial,
//            dinr,
//            dinr_ac,
//            jcb,
//            jcb_ac,
//            debit_enabled,
//            term_voice_id,
//            voice_auth_phone,
//            cdset,
//            location_number,
//            time_zone,
//            equip_model,
//            dst,
//            contact_name,
//            mcfs,
//            customer_service_num,
//            profile_generator_code,
//            term_application_profgen,
//            front_end,
//            term_comm_type,
//            merchant_key_required
//          )
//          values
//          (
//            :tempRequestId,
//            :primaryKey,
//            'STAGE',
//            'N',
//            :today,
//            'FTP',
//            :mesConstants.ETPPS_NEW,
//            :binNum,
//            :agentNum,
//            :chainNum,
//            :merchNum,
//            '0001',
//            '0001',
//            :merchName,
//            :merchAddress,
//            :merchCity,
//            :merchState,
//            :merchZip,
//            :merchPhone,
//            :serviceLevel,
//            :attachCode,
//            :mcc,
//            :discNum,
//            :discAc,
//            :amexNum,
//            :amexAc,
//            'N',
//            :dinersNum,
//            :dinersAc,
//            :jcbNum,
//            :jcbAc,
//            :debitAc,
//            :termVoiceId,
//            :voiceAuthPhone,
//            :cdSet,
//            '00001',
//            :timeZone,
//            'na',
//            :dst,
//            :contactName,
//            :mcfs,
//            :customerServiceNum,
//            1,
//            'STAGE',
//            0,
//            :commType,
//            :merchKeyRequired
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mms_stage_info\n        (\n          request_id,\n          app_seq_num,\n          term_application,\n          special_handling,\n          app_date,\n          process_method,\n          process_status,\n          bin,\n          agent,\n          chain,\n          merch_number,\n          store_number,\n          terminal_number,\n          business_name,\n          business_address,\n          business_city,\n          business_state,\n          business_zip,\n          merch_phone,\n          service_level,\n          attachment_code,\n          mcc,\n          disc,\n          disc_ac,\n          amex,\n          amex_ac,\n          split_dial,\n          dinr,\n          dinr_ac,\n          jcb,\n          jcb_ac,\n          debit_enabled,\n          term_voice_id,\n          voice_auth_phone,\n          cdset,\n          location_number,\n          time_zone,\n          equip_model,\n          dst,\n          contact_name,\n          mcfs,\n          customer_service_num,\n          profile_generator_code,\n          term_application_profgen,\n          front_end,\n          term_comm_type,\n          merchant_key_required\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          'STAGE',\n          'N',\n           :3 ,\n          'FTP',\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n          '0001',\n          '0001',\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 ,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n           :21 ,\n          'N',\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n           :26 ,\n           :27 ,\n           :28 ,\n           :29 ,\n          '00001',\n           :30 ,\n          'na',\n           :31 ,\n           :32 ,\n           :33 ,\n           :34 ,\n          1,\n          'STAGE',\n          0,\n           :35 ,\n           :36 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,tempRequestId);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setTimestamp(3,today);
   __sJT_st.setString(4,mesConstants.ETPPS_NEW);
   __sJT_st.setString(5,binNum);
   __sJT_st.setString(6,agentNum);
   __sJT_st.setString(7,chainNum);
   __sJT_st.setString(8,merchNum);
   __sJT_st.setString(9,merchName);
   __sJT_st.setString(10,merchAddress);
   __sJT_st.setString(11,merchCity);
   __sJT_st.setString(12,merchState);
   __sJT_st.setString(13,merchZip);
   __sJT_st.setString(14,merchPhone);
   __sJT_st.setString(15,serviceLevel);
   __sJT_st.setString(16,attachCode);
   __sJT_st.setString(17,mcc);
   __sJT_st.setString(18,discNum);
   __sJT_st.setString(19,discAc);
   __sJT_st.setString(20,amexNum);
   __sJT_st.setString(21,amexAc);
   __sJT_st.setString(22,dinersNum);
   __sJT_st.setString(23,dinersAc);
   __sJT_st.setString(24,jcbNum);
   __sJT_st.setString(25,jcbAc);
   __sJT_st.setString(26,debitAc);
   __sJT_st.setString(27,termVoiceId);
   __sJT_st.setString(28,voiceAuthPhone);
   __sJT_st.setString(29,cdSet);
   __sJT_st.setString(30,timeZone);
   __sJT_st.setString(31,dst);
   __sJT_st.setString(32,contactName);
   __sJT_st.setString(33,mcfs);
   __sJT_st.setString(34,customerServiceNum);
   __sJT_st.setInt(35,commType);
   __sJT_st.setString(36,merchKeyRequired);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1436^7*/

      // add place holders in legacy vnumber tables
      /*@lineinfo:generated-code*//*@lineinfo:1439^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_vnumber
//          (
//            app_seq_num,
//            request_id,
//            terminal_number,
//            store_number,
//            time_zone,
//            location_number,
//            app_code,
//            equip_model,
//            profile_generator_code,
//            front_end,
//            term_comm_type
//          )
//          values
//          (
//            :primaryKey,
//            :tempRequestId,
//            '0001',
//            '0001',
//            :timeZone,
//            '00001',
//            'STAGE',
//            'na',
//            :mesConstants.PROFGEN_MMS,
//            :mesConstants.HOST_VITAL,
//            :commType
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_vnumber\n        (\n          app_seq_num,\n          request_id,\n          terminal_number,\n          store_number,\n          time_zone,\n          location_number,\n          app_code,\n          equip_model,\n          profile_generator_code,\n          front_end,\n          term_comm_type\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          '0001',\n          '0001',\n           :3 ,\n          '00001',\n          'STAGE',\n          'na',\n           :4 ,\n           :5 ,\n           :6 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,tempRequestId);
   __sJT_st.setString(3,timeZone);
   __sJT_st.setInt(4,mesConstants.PROFGEN_MMS);
   __sJT_st.setInt(5,mesConstants.HOST_VITAL);
   __sJT_st.setInt(6,commType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1469^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1471^7*/

//  ************************************************************
//  #sql [Ctx] { insert into v_numbers
//          (
//            app_seq_num,
//            request_id,
//            terminal_number,
//            model_index,
//            store_number,
//            time_zone,
//            location_number,
//            app_code,
//            equip_model,
//            pos_code,
//            auto_flag,
//            front_end,
//            term_comm_type
//          )
//          values
//          (
//            :primaryKey,
//            :tempRequestId,
//            1,
//            1,
//            1,
//            :timeZone,
//            1,
//            'STAGE',
//            'na',
//            601,
//            :AUTO_FLAG_DEFAULT,
//            :mesConstants.HOST_VITAL,
//            :commType
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into v_numbers\n        (\n          app_seq_num,\n          request_id,\n          terminal_number,\n          model_index,\n          store_number,\n          time_zone,\n          location_number,\n          app_code,\n          equip_model,\n          pos_code,\n          auto_flag,\n          front_end,\n          term_comm_type\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          1,\n          1,\n          1,\n           :3 ,\n          1,\n          'STAGE',\n          'na',\n          601,\n           :4 ,\n           :5 ,\n           :6 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,tempRequestId);
   __sJT_st.setString(3,timeZone);
   __sJT_st.setString(4,AUTO_FLAG_DEFAULT);
   __sJT_st.setInt(5,mesConstants.HOST_VITAL);
   __sJT_st.setInt(6,commType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1505^7*/

      commit();
    }
    catch(Exception e)
    {
      rollback();

      logEntry("addVitalStageOnly()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** METHOD public void submitData()
  **
  */
  public void submitData(UserBean user)
  {
    try
    {
      connect();

      if(recordExists())
      {
        /*@lineinfo:generated-code*//*@lineinfo:1534^9*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//            set     vnum                      = :vNum,
//                    bin                       = :binNum,
//                    agent                     = :agentNum,
//                    chain                     = :chainNum,
//                    merch_number              = :merchNum,
//                    location_number           = :locationNum,
//                    store_number              = :storeNum,
//                    terminal_number           = :terminalNum,
//                    equip_model               = :equipModel,
//                    equip_refurb              = :equipModelRefurb,
//                    business_name             = :merchName,
//                    business_address          = :merchAddress,
//                    business_city             = :merchCity,
//                    business_state            = :merchState,
//                    business_zip              = :merchZip,
//                    merch_phone               = :merchPhone,
//                    service_level             = :serviceLevel,
//                    attachment_code           = :attachCode,
//                    mcc                       = :mcc,
//                    term_application          = :termApp,
//                    term_application_profgen  = :termAppProfGen,
//                    disc                      = :discNum,
//                    disc_ac                   = :discAc,
//                    amex                      = :amexNum,
//                    amex_ac                   = :amexAc,
//                    split_dial                = :amexSplit,
//                    dinr                      = :dinersNum,
//                    dinr_ac                   = :dinersAc,
//                    jcb                       = :jcbNum,
//                    jcb_ac                    = :jcbAc,
//                    debit_enabled             = :debitAc,
//                    term_voice_id             = :termVoiceId,
//                    voice_auth_phone          = :voiceAuthPhone,
//                    cdset                     = :cdSet,
//                    special_handling          = :specialHandling,
//                    time_zone                 = :timeZone,
//                    dst                       = :dst,
//                    contact_name              = :contactName,
//                    project_num               = :projectNum,
//                    mcfs                      = :mcfs,
//                    customer_service_num      = :customerServiceNum,
//                    profile_generator_code    = :profileGeneratorCode,
//                    front_end                 = :frontEnd,
//                    term_comm_type            = :commType,
//                    merchant_key_required     = :merchKeyRequired
//            where   request_id                = :requestId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n          set     vnum                      =  :1 ,\n                  bin                       =  :2 ,\n                  agent                     =  :3 ,\n                  chain                     =  :4 ,\n                  merch_number              =  :5 ,\n                  location_number           =  :6 ,\n                  store_number              =  :7 ,\n                  terminal_number           =  :8 ,\n                  equip_model               =  :9 ,\n                  equip_refurb              =  :10 ,\n                  business_name             =  :11 ,\n                  business_address          =  :12 ,\n                  business_city             =  :13 ,\n                  business_state            =  :14 ,\n                  business_zip              =  :15 ,\n                  merch_phone               =  :16 ,\n                  service_level             =  :17 ,\n                  attachment_code           =  :18 ,\n                  mcc                       =  :19 ,\n                  term_application          =  :20 ,\n                  term_application_profgen  =  :21 ,\n                  disc                      =  :22 ,\n                  disc_ac                   =  :23 ,\n                  amex                      =  :24 ,\n                  amex_ac                   =  :25 ,\n                  split_dial                =  :26 ,\n                  dinr                      =  :27 ,\n                  dinr_ac                   =  :28 ,\n                  jcb                       =  :29 ,\n                  jcb_ac                    =  :30 ,\n                  debit_enabled             =  :31 ,\n                  term_voice_id             =  :32 ,\n                  voice_auth_phone          =  :33 ,\n                  cdset                     =  :34 ,\n                  special_handling          =  :35 ,\n                  time_zone                 =  :36 ,\n                  dst                       =  :37 ,\n                  contact_name              =  :38 ,\n                  project_num               =  :39 ,\n                  mcfs                      =  :40 ,\n                  customer_service_num      =  :41 ,\n                  profile_generator_code    =  :42 ,\n                  front_end                 =  :43 ,\n                  term_comm_type            =  :44 ,\n                  merchant_key_required     =  :45 \n          where   request_id                =  :46";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,vNum);
   __sJT_st.setString(2,binNum);
   __sJT_st.setString(3,agentNum);
   __sJT_st.setString(4,chainNum);
   __sJT_st.setString(5,merchNum);
   __sJT_st.setString(6,locationNum);
   __sJT_st.setString(7,storeNum);
   __sJT_st.setString(8,terminalNum);
   __sJT_st.setString(9,equipModel);
   __sJT_st.setString(10,equipModelRefurb);
   __sJT_st.setString(11,merchName);
   __sJT_st.setString(12,merchAddress);
   __sJT_st.setString(13,merchCity);
   __sJT_st.setString(14,merchState);
   __sJT_st.setString(15,merchZip);
   __sJT_st.setString(16,merchPhone);
   __sJT_st.setString(17,serviceLevel);
   __sJT_st.setString(18,attachCode);
   __sJT_st.setString(19,mcc);
   __sJT_st.setString(20,termApp);
   __sJT_st.setString(21,termAppProfGen);
   __sJT_st.setString(22,discNum);
   __sJT_st.setString(23,discAc);
   __sJT_st.setString(24,amexNum);
   __sJT_st.setString(25,amexAc);
   __sJT_st.setString(26,amexSplit);
   __sJT_st.setString(27,dinersNum);
   __sJT_st.setString(28,dinersAc);
   __sJT_st.setString(29,jcbNum);
   __sJT_st.setString(30,jcbAc);
   __sJT_st.setString(31,debitAc);
   __sJT_st.setString(32,termVoiceId);
   __sJT_st.setString(33,voiceAuthPhone);
   __sJT_st.setString(34,cdSet);
   __sJT_st.setString(35,specialHandling);
   __sJT_st.setString(36,timeZone);
   __sJT_st.setString(37,dst);
   __sJT_st.setString(38,contactName);
   __sJT_st.setString(39,projectNum);
   __sJT_st.setString(40,mcfs);
   __sJT_st.setString(41,customerServiceNum);
   __sJT_st.setInt(42,profileGeneratorCode);
   __sJT_st.setInt(43,frontEnd);
   __sJT_st.setInt(44,commType);
   __sJT_st.setString(45,merchKeyRequired);
   __sJT_st.setLong(46,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1583^9*/
      }
      // update existing records
      else
      {
        if ( frontEnd == mesConstants.HOST_TRIDENT )
        {
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:1592^13*/

//  ************************************************************
//  #sql [Ctx] { select  (nvl(max(mms.terminal_number),0) + 1)
//                
//                from    mms_stage_info      mms
//                where   bin             = :binNum and
//                        agent           = :agentNum and
//                        chain           = :chainNum and
//                        merch_number    = :merchNum and
//                        store_number    = :storeNum and
//                        terminal_number = :terminalNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  (nvl(max(mms.terminal_number),0) + 1)\n               \n              from    mms_stage_info      mms\n              where   bin             =  :1  and\n                      agent           =  :2  and\n                      chain           =  :3  and\n                      merch_number    =  :4  and\n                      store_number    =  :5  and\n                      terminal_number =  :6";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,binNum);
   __sJT_st.setString(2,agentNum);
   __sJT_st.setString(3,chainNum);
   __sJT_st.setString(4,merchNum);
   __sJT_st.setString(5,storeNum);
   __sJT_st.setString(6,terminalNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   terminalNum = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1603^13*/
          }
          catch( java.sql.SQLException sqe )
          {
            // the above works in TOAD even if
            // there are no matching rows in mms_stage_info
            // just in case, catch any SQL exceptions
            // and default the terminal number to 1
            terminalNum = String.valueOf(1);
          }
        }

        /*@lineinfo:generated-code*//*@lineinfo:1615^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mms_stage_info
//            (
//              app_seq_num,
//              request_id,
//              app_date,
//              vnum,
//              bin,
//              agent,
//              chain,
//              merch_number,
//              location_number,
//              store_number,
//              terminal_number,
//              equip_model,
//              equip_refurb,
//              business_name,
//              business_address,
//              business_city,
//              business_state,
//              business_zip,
//              merch_phone,
//              service_level,
//              attachment_code,
//              mcc,
//              term_application,
//              term_application_profgen,
//              disc,
//              disc_ac,
//              amex,
//              amex_ac,
//              split_dial,
//              dinr,
//              dinr_ac,
//              jcb,
//              jcb_ac,
//              debit_enabled,
//              term_voice_id,
//              voice_auth_phone,
//              cdset,
//              process_method,
//              process_status,
//              process_response,
//              special_handling,
//              time_zone,
//              dst,
//              contact_name,
//              project_num,
//              mcfs,
//              customer_service_num,
//              profile_generator_code,
//              front_end,
//              process_start_date,
//              process_end_date,
//              term_comm_type,
//              merchant_key_required
//            )
//            values
//            (
//              :primaryKey,
//              :requestId,
//              :today,
//              :vNum,
//              :binNum,
//              :agentNum,
//              :chainNum,
//              :merchNum,
//              :locationNum,
//              :storeNum,
//              :terminalNum,
//              :equipModel,
//              :equipModelRefurb,
//              :merchName,
//              :merchAddress,
//              :merchCity,
//              :merchState,
//              :merchZip,
//              :merchPhone,
//              :serviceLevel,
//              :attachCode,
//              :mcc,
//              :termApp,
//              :termAppProfGen,
//              :discNum,
//              :discAc,
//              :amexNum,
//              :amexAc,
//              :amexSplit,
//              :dinersNum,
//              :dinersAc,
//              :jcbNum,
//              :jcbAc,
//              :debitAc,
//              :termVoiceId,
//              :voiceAuthPhone,
//              :cdSet,
//              :transmissionMethod,
//              decode(:frontEnd, :mesConstants.HOST_TRIDENT, :mesConstants.ETPPS_RECEIVED, :mesConstants.ETPPS_NEW),
//              decode(:frontEnd, :mesConstants.HOST_TRIDENT, :mesConstants.ETPPR_SUCCESS, null),
//              :specialHandling,
//              :timeZone,
//              :dst,
//              :contactName,
//              :projectNum,
//              :mcfs,
//              :customerServiceNum,
//              :profileGeneratorCode,
//              :frontEnd,
//              decode(:frontEnd, :mesConstants.HOST_TRIDENT, sysdate, null),
//              decode(:frontEnd, :mesConstants.HOST_TRIDENT, sysdate, null),
//              :commType,
//              :merchKeyRequired
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mms_stage_info\n          (\n            app_seq_num,\n            request_id,\n            app_date,\n            vnum,\n            bin,\n            agent,\n            chain,\n            merch_number,\n            location_number,\n            store_number,\n            terminal_number,\n            equip_model,\n            equip_refurb,\n            business_name,\n            business_address,\n            business_city,\n            business_state,\n            business_zip,\n            merch_phone,\n            service_level,\n            attachment_code,\n            mcc,\n            term_application,\n            term_application_profgen,\n            disc,\n            disc_ac,\n            amex,\n            amex_ac,\n            split_dial,\n            dinr,\n            dinr_ac,\n            jcb,\n            jcb_ac,\n            debit_enabled,\n            term_voice_id,\n            voice_auth_phone,\n            cdset,\n            process_method,\n            process_status,\n            process_response,\n            special_handling,\n            time_zone,\n            dst,\n            contact_name,\n            project_num,\n            mcfs,\n            customer_service_num,\n            profile_generator_code,\n            front_end,\n            process_start_date,\n            process_end_date,\n            term_comm_type,\n            merchant_key_required\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 ,\n             :34 ,\n             :35 ,\n             :36 ,\n             :37 ,\n             :38 ,\n            decode( :39 ,  :40 ,  :41 ,  :42 ),\n            decode( :43 ,  :44 ,  :45 , null),\n             :46 ,\n             :47 ,\n             :48 ,\n             :49 ,\n             :50 ,\n             :51 ,\n             :52 ,\n             :53 ,\n             :54 ,\n            decode( :55 ,  :56 , sysdate, null),\n            decode( :57 ,  :58 , sysdate, null),\n             :59 ,\n             :60 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,requestId);
   __sJT_st.setTimestamp(3,today);
   __sJT_st.setString(4,vNum);
   __sJT_st.setString(5,binNum);
   __sJT_st.setString(6,agentNum);
   __sJT_st.setString(7,chainNum);
   __sJT_st.setString(8,merchNum);
   __sJT_st.setString(9,locationNum);
   __sJT_st.setString(10,storeNum);
   __sJT_st.setString(11,terminalNum);
   __sJT_st.setString(12,equipModel);
   __sJT_st.setString(13,equipModelRefurb);
   __sJT_st.setString(14,merchName);
   __sJT_st.setString(15,merchAddress);
   __sJT_st.setString(16,merchCity);
   __sJT_st.setString(17,merchState);
   __sJT_st.setString(18,merchZip);
   __sJT_st.setString(19,merchPhone);
   __sJT_st.setString(20,serviceLevel);
   __sJT_st.setString(21,attachCode);
   __sJT_st.setString(22,mcc);
   __sJT_st.setString(23,termApp);
   __sJT_st.setString(24,termAppProfGen);
   __sJT_st.setString(25,discNum);
   __sJT_st.setString(26,discAc);
   __sJT_st.setString(27,amexNum);
   __sJT_st.setString(28,amexAc);
   __sJT_st.setString(29,amexSplit);
   __sJT_st.setString(30,dinersNum);
   __sJT_st.setString(31,dinersAc);
   __sJT_st.setString(32,jcbNum);
   __sJT_st.setString(33,jcbAc);
   __sJT_st.setString(34,debitAc);
   __sJT_st.setString(35,termVoiceId);
   __sJT_st.setString(36,voiceAuthPhone);
   __sJT_st.setString(37,cdSet);
   __sJT_st.setString(38,transmissionMethod);
   __sJT_st.setInt(39,frontEnd);
   __sJT_st.setInt(40,mesConstants.HOST_TRIDENT);
   __sJT_st.setString(41,mesConstants.ETPPS_RECEIVED);
   __sJT_st.setString(42,mesConstants.ETPPS_NEW);
   __sJT_st.setInt(43,frontEnd);
   __sJT_st.setInt(44,mesConstants.HOST_TRIDENT);
   __sJT_st.setString(45,mesConstants.ETPPR_SUCCESS);
   __sJT_st.setString(46,specialHandling);
   __sJT_st.setString(47,timeZone);
   __sJT_st.setString(48,dst);
   __sJT_st.setString(49,contactName);
   __sJT_st.setString(50,projectNum);
   __sJT_st.setString(51,mcfs);
   __sJT_st.setString(52,customerServiceNum);
   __sJT_st.setInt(53,profileGeneratorCode);
   __sJT_st.setInt(54,frontEnd);
   __sJT_st.setInt(55,frontEnd);
   __sJT_st.setInt(56,mesConstants.HOST_TRIDENT);
   __sJT_st.setInt(57,frontEnd);
   __sJT_st.setInt(58,mesConstants.HOST_TRIDENT);
   __sJT_st.setInt(59,commType);
   __sJT_st.setString(60,merchKeyRequired);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1729^9*/

        // if this is a new trident setup, add a stage-only vital item
        if(frontEnd == mesConstants.HOST_TRIDENT && firstTimeTrident())
        {
          addVitalStageOnly();
        }

        if(wasSentManual())
        {
          updateStartDate();
        }
      }

      // only add to programming queues if this is an original setup (not if additional)
      if(this.action == null || !this.action.equals("add"))
      {
        // update the check and valutec ids if necessary
        submitCheckValutecData();

        // update app_tracking to show the user login name that submitted this request
        updateAppTrackingMMS(user.getLoginName());

        insertIntoProgramming(user);
      }

      //insert place holder in legacy vnumber tables
      insertIntoMerchVnumber();
      insertIntoVnumbers();
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
      addError("Error: Complications occurred while storing data!  Please resubmit.");
    }
    finally
    {
      cleanUp();
    }
  }

  public void submitCheckValutecData( )
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1776^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchpayoption  mpo
//          set     mpo.merchpo_tid   = :CheckTermId,
//                  mpo.merchpo_card_merch_number = :CheckMerchId
//          where   mpo.app_seq_num = :this.primaryKey and
//                  mpo.cardtype_code = :mesConstants.APP_CT_CHECK_AUTH
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchpayoption  mpo\n        set     mpo.merchpo_tid   =  :1 ,\n                mpo.merchpo_card_merch_number =  :2 \n        where   mpo.app_seq_num =  :3  and\n                mpo.cardtype_code =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,CheckTermId);
   __sJT_st.setString(2,CheckMerchId);
   __sJT_st.setLong(3,this.primaryKey);
   __sJT_st.setInt(4,mesConstants.APP_CT_CHECK_AUTH);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1783^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1785^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchpayoption  mpo
//          set     mpo.merchpo_tid               = :ValutecTermId,
//                  mpo.merchpo_card_merch_number = :ValutecMerchId
//          where   mpo.app_seq_num   = :this.primaryKey and
//                  mpo.cardtype_code = :mesConstants.APP_CT_VALUTEC_GIFT_CARD
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchpayoption  mpo\n        set     mpo.merchpo_tid               =  :1 ,\n                mpo.merchpo_card_merch_number =  :2 \n        where   mpo.app_seq_num   =  :3  and\n                mpo.cardtype_code =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,ValutecTermId);
   __sJT_st.setString(2,ValutecMerchId);
   __sJT_st.setLong(3,this.primaryKey);
   __sJT_st.setInt(4,mesConstants.APP_CT_VALUTEC_GIFT_CARD);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1792^7*/

      commit();
    }
    catch(Exception e)
    {
      logEntry("submitCheckValutecData()", e.toString());
      addError("Error: Complications occurred while storing check/gift card data!  Please resubmit.");
    }
    finally
    {
      cleanUp();
    }
  }

  private void updateAppTrackingMMS(String loginName)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1811^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     contact_name  = :loginName
//          where   app_seq_num   = :primaryKey and
//                  dept_code     = :QueueConstants.DEPARTMENT_MMS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     contact_name  =  :1 \n        where   app_seq_num   =  :2  and\n                dept_code     =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"31com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_MMS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1817^7*/
    }
    catch(Exception e)
    {
      logEntry("updateAppTrackingMMS(" + loginName + ")", e.toString());
    }
  }

  private void insertIntoProgramming(UserBean user)
  {

    int type = MesQueues.Q_NONE;

    if(specialHandling.equals("Y"))
    {
      type = MesQueues.Q_PROGRAMMING_SPECIAL;
    }
    else
    {
      // determine programming queue as a function of the profile generator
      switch(profileGeneratorCode) {
        case mesConstants.PROFGEN_VERICENTRE:
          if(transmissionResponse!=null && transmissionResponse.equals(mesConstants.ETPPR_PENDING))
          {
            type = MesQueues.Q_PROGRAMMING_VC_PENDING;
          }
          else
          {
            type = MesQueues.Q_PROGRAMMING_VC_NEW;
          }
          break;
        case mesConstants.PROFGEN_TERMMASTER:
          if(transmissionResponse!=null && transmissionResponse.equals(mesConstants.ETPPR_PENDING))
          {
            type = MesQueues.Q_PROGRAMMING_TM_PENDING;
          }
          else
          {
            type = MesQueues.Q_PROGRAMMING_TM_NEW;
          }
          break;
        default:
          type = MesQueues.Q_PROGRAMMING_REGULAR;
          break;
      }
    }

    // kill any existing programming queue entries under this request id (e.g.: re-submissions)
    QueueTools.removeQueueByItemType(requestId,MesQueues.Q_ITEM_TYPE_PROGRAMMING);

    //this puts every request into the queue the id must be the requestId
    QueueTools.insertQueue(requestId, type, user);

    ///this is going to be obsolete real soon
    insertIntoProgrammingOld();
  }

  private void insertIntoProgrammingOld()
  {
    try
    {
      connect();

      if(!alreadyExistsInQueue(requestId))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1882^9*/

//  ************************************************************
//  #sql [Ctx] { insert into temp_activation_queue
//            (
//              APP_SEQ_NUM,
//              REQUEST_ID,
//              DATE_IN_QUEUE,
//              Q_STAGE,
//              Q_TYPE
//            )
//            values
//            (
//              :primaryKey,
//              :requestId,
//              :today,
//              :QueueConstants.Q_PROGRAMMING_NEW,
//              :QueueConstants.QUEUE_PROGRAMMING
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into temp_activation_queue\n          (\n            APP_SEQ_NUM,\n            REQUEST_ID,\n            DATE_IN_QUEUE,\n            Q_STAGE,\n            Q_TYPE\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,requestId);
   __sJT_st.setTimestamp(3,today);
   __sJT_st.setInt(4,QueueConstants.Q_PROGRAMMING_NEW);
   __sJT_st.setInt(5,QueueConstants.QUEUE_PROGRAMMING);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1900^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("insertIntoProgrammingOld()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void updateStartDate()
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1919^7*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//          set     process_start_date  = :today
//          where   REQUEST_ID          = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n        set     process_start_date  =  :1 \n        where   REQUEST_ID          =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"33com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setLong(2,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1924^7*/
    }
    catch(Exception e)
    {
      logEntry("updateStartDate()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** METHOD public void resubmitData()
  **
  */
  public void reSubmitData(UserBean user)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1946^7*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//          set     vnum                      = :vNum,
//                  bin                       = :binNum,
//                  agent                     = :agentNum,
//                  chain                     = :chainNum,
//                  merch_number              = :merchNum,
//                  location_number           = :locationNum,
//                  store_number              = :storeNum,
//                  terminal_number           = :terminalNum,
//                  equip_model               = :equipModel,
//                  equip_refurb              = :equipModelRefurb,
//                  business_name             = :merchName,
//                  business_address          = :merchAddress,
//                  business_city             = :merchCity,
//                  business_state            = :merchState,
//                  business_zip              = :merchZip,
//                  merch_phone               = :merchPhone,
//                  service_level             = :serviceLevel,
//                  attachment_code           = :attachCode,
//                  mcc                       = :mcc,
//                  term_application          = :termApp,
//                  term_application_profgen  = :termAppProfGen,
//                  disc                      = :discNum,
//                  disc_ac                   = :discAc,
//                  amex                      = :amexNum,
//                  amex_ac                   = :amexAc,
//                  split_dial                = :amexSplit,
//                  dinr                      = :dinersNum,
//                  dinr_ac                   = :dinersAc,
//                  jcb                       = :jcbNum,
//                  jcb_ac                    = :jcbAc,
//                  debit_enabled             = :debitAc,
//                  term_voice_id             = :termVoiceId,
//                  voice_auth_phone          = :voiceAuthPhone,
//                  cdset                     = :cdSet,
//                  process_status            = decode(:frontEnd, :mesConstants.HOST_TRIDENT, :mesConstants.ETPPS_RECEIVED, :mesConstants.ETPPS_RESUBMITTED),
//                  special_handling          = :specialHandling,
//                  time_zone                 = :timeZone,
//                  dst                       = :dst,
//                  contact_name              = :contactName,
//                  project_num               = :projectNum,
//                  mcfs                      = :mcfs,
//                  customer_service_num      = :customerServiceNum,
//                  profile_generator_code    = :profileGeneratorCode,
//                  front_end                 = :frontEnd
//          where   request_id                = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n        set     vnum                      =  :1 ,\n                bin                       =  :2 ,\n                agent                     =  :3 ,\n                chain                     =  :4 ,\n                merch_number              =  :5 ,\n                location_number           =  :6 ,\n                store_number              =  :7 ,\n                terminal_number           =  :8 ,\n                equip_model               =  :9 ,\n                equip_refurb              =  :10 ,\n                business_name             =  :11 ,\n                business_address          =  :12 ,\n                business_city             =  :13 ,\n                business_state            =  :14 ,\n                business_zip              =  :15 ,\n                merch_phone               =  :16 ,\n                service_level             =  :17 ,\n                attachment_code           =  :18 ,\n                mcc                       =  :19 ,\n                term_application          =  :20 ,\n                term_application_profgen  =  :21 ,\n                disc                      =  :22 ,\n                disc_ac                   =  :23 ,\n                amex                      =  :24 ,\n                amex_ac                   =  :25 ,\n                split_dial                =  :26 ,\n                dinr                      =  :27 ,\n                dinr_ac                   =  :28 ,\n                jcb                       =  :29 ,\n                jcb_ac                    =  :30 ,\n                debit_enabled             =  :31 ,\n                term_voice_id             =  :32 ,\n                voice_auth_phone          =  :33 ,\n                cdset                     =  :34 ,\n                process_status            = decode( :35 ,  :36 ,  :37 ,  :38 ),\n                special_handling          =  :39 ,\n                time_zone                 =  :40 ,\n                dst                       =  :41 ,\n                contact_name              =  :42 ,\n                project_num               =  :43 ,\n                mcfs                      =  :44 ,\n                customer_service_num      =  :45 ,\n                profile_generator_code    =  :46 ,\n                front_end                 =  :47 \n        where   request_id                =  :48";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"34com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,vNum);
   __sJT_st.setString(2,binNum);
   __sJT_st.setString(3,agentNum);
   __sJT_st.setString(4,chainNum);
   __sJT_st.setString(5,merchNum);
   __sJT_st.setString(6,locationNum);
   __sJT_st.setString(7,storeNum);
   __sJT_st.setString(8,terminalNum);
   __sJT_st.setString(9,equipModel);
   __sJT_st.setString(10,equipModelRefurb);
   __sJT_st.setString(11,merchName);
   __sJT_st.setString(12,merchAddress);
   __sJT_st.setString(13,merchCity);
   __sJT_st.setString(14,merchState);
   __sJT_st.setString(15,merchZip);
   __sJT_st.setString(16,merchPhone);
   __sJT_st.setString(17,serviceLevel);
   __sJT_st.setString(18,attachCode);
   __sJT_st.setString(19,mcc);
   __sJT_st.setString(20,termApp);
   __sJT_st.setString(21,termAppProfGen);
   __sJT_st.setString(22,discNum);
   __sJT_st.setString(23,discAc);
   __sJT_st.setString(24,amexNum);
   __sJT_st.setString(25,amexAc);
   __sJT_st.setString(26,amexSplit);
   __sJT_st.setString(27,dinersNum);
   __sJT_st.setString(28,dinersAc);
   __sJT_st.setString(29,jcbNum);
   __sJT_st.setString(30,jcbAc);
   __sJT_st.setString(31,debitAc);
   __sJT_st.setString(32,termVoiceId);
   __sJT_st.setString(33,voiceAuthPhone);
   __sJT_st.setString(34,cdSet);
   __sJT_st.setInt(35,frontEnd);
   __sJT_st.setInt(36,mesConstants.HOST_TRIDENT);
   __sJT_st.setString(37,mesConstants.ETPPS_RECEIVED);
   __sJT_st.setString(38,mesConstants.ETPPS_RESUBMITTED);
   __sJT_st.setString(39,specialHandling);
   __sJT_st.setString(40,timeZone);
   __sJT_st.setString(41,dst);
   __sJT_st.setString(42,contactName);
   __sJT_st.setString(43,projectNum);
   __sJT_st.setString(44,mcfs);
   __sJT_st.setString(45,customerServiceNum);
   __sJT_st.setInt(46,profileGeneratorCode);
   __sJT_st.setInt(47,frontEnd);
   __sJT_st.setLong(48,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1994^7*/

      //once an mms has been resubmitted delete it from the error queue.
      QueueTools.removeQueue(requestId, MesQueues.Q_MMS_ERROR);

      //insert place holder in legacy vnumber tables
      insertIntoMerchVnumber();
      insertIntoVnumbers();

      //updateRetransmissionStatus(primaryKey);

    }
    catch(Exception e)
    {
      logEntry("resubmitData()", e.toString());
      addError("Error: Complications occurred while resubmitting data!  Please resubmit.");
    }
    finally
    {
      insertIntoProgramming(user);  // i.e. delete current and re-insert
      cleanUp();
    }
  }

  private void insertIntoMerchVnumber()
  {
    try
    {
      connect();

      if(alreadyExistsInMerchVnumber(requestId))
      {
        /*@lineinfo:generated-code*//*@lineinfo:2026^9*/

//  ************************************************************
//  #sql [Ctx] { update  merch_vnumber
//            set     terminal_number         = :terminalNum,
//                    store_number            = :storeNum,
//                    time_zone               = :timeZone,
//                    location_number         = :locationNum,
//                    app_code                = :termAppProfGen,
//                    equip_model             = :equipModel,
//                    profile_generator_code  = :profileGeneratorCode,
//                    front_end               = :frontEnd
//            where   request_id              = :requestId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merch_vnumber\n          set     terminal_number         =  :1 ,\n                  store_number            =  :2 ,\n                  time_zone               =  :3 ,\n                  location_number         =  :4 ,\n                  app_code                =  :5 ,\n                  equip_model             =  :6 ,\n                  profile_generator_code  =  :7 ,\n                  front_end               =  :8 \n          where   request_id              =  :9";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"35com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,terminalNum);
   __sJT_st.setString(2,storeNum);
   __sJT_st.setString(3,timeZone);
   __sJT_st.setString(4,locationNum);
   __sJT_st.setString(5,termAppProfGen);
   __sJT_st.setString(6,equipModel);
   __sJT_st.setInt(7,profileGeneratorCode);
   __sJT_st.setInt(8,frontEnd);
   __sJT_st.setLong(9,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2038^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:2042^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_vnumber
//            (
//              app_seq_num,
//              request_id,
//              terminal_number,
//              store_number,
//              time_zone,
//              location_number,
//              app_code,
//              equip_model,
//              profile_generator_code,
//              front_end,
//              term_comm_type
//            )
//            values
//            (
//              :primaryKey,
//              :requestId,
//              :terminalNum,
//              :storeNum,
//              :timeZone,
//              :locationNum,
//              :termAppProfGen,
//              :equipModel,
//              :profileGeneratorCode,
//              :frontEnd,
//              :commType
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_vnumber\n          (\n            app_seq_num,\n            request_id,\n            terminal_number,\n            store_number,\n            time_zone,\n            location_number,\n            app_code,\n            equip_model,\n            profile_generator_code,\n            front_end,\n            term_comm_type\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"36com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,requestId);
   __sJT_st.setString(3,terminalNum);
   __sJT_st.setString(4,storeNum);
   __sJT_st.setString(5,timeZone);
   __sJT_st.setString(6,locationNum);
   __sJT_st.setString(7,termAppProfGen);
   __sJT_st.setString(8,equipModel);
   __sJT_st.setInt(9,profileGeneratorCode);
   __sJT_st.setInt(10,frontEnd);
   __sJT_st.setInt(11,commType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2072^9*/
      }

    }
    catch(Exception e)
    {
      logEntry("insertIntoMerchVnumber()", e.toString());
    }
    finally
    {
      cleanUp();
    }

  }

  private void insertIntoVnumbers()
  {
    try
    {
      connect();

      if(alreadyExistsInVnumbers(requestId))
      {
        /*@lineinfo:generated-code*//*@lineinfo:2095^9*/

//  ************************************************************
//  #sql [Ctx] { update  v_numbers
//            set     terminal_number = :terminalNum,
//                    model_index     = :terminalNum,
//                    store_number    = :storeNum,
//                    time_zone       = :timeZone,
//                    location_number = :locationNum,
//                    app_code        = :termAppProfGen,
//                    equip_model     = :equipModel,
//                    front_end       = :frontEnd,
//                    term_comm_type  = :commType
//            where   request_id      = :requestId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  v_numbers\n          set     terminal_number =  :1 ,\n                  model_index     =  :2 ,\n                  store_number    =  :3 ,\n                  time_zone       =  :4 ,\n                  location_number =  :5 ,\n                  app_code        =  :6 ,\n                  equip_model     =  :7 ,\n                  front_end       =  :8 ,\n                  term_comm_type  =  :9 \n          where   request_id      =  :10";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"37com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,terminalNum);
   __sJT_st.setString(2,terminalNum);
   __sJT_st.setString(3,storeNum);
   __sJT_st.setString(4,timeZone);
   __sJT_st.setString(5,locationNum);
   __sJT_st.setString(6,termAppProfGen);
   __sJT_st.setString(7,equipModel);
   __sJT_st.setInt(8,frontEnd);
   __sJT_st.setInt(9,commType);
   __sJT_st.setLong(10,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2108^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:2112^9*/

//  ************************************************************
//  #sql [Ctx] { insert into v_numbers
//            (
//              app_seq_num,
//              request_id,
//              terminal_number,
//              model_index,
//              store_number,
//              time_zone,
//              location_number,
//              app_code,
//              equip_model,
//              pos_code,
//              auto_flag,
//              front_end,
//              term_comm_type
//            )
//            values
//            (
//              :primaryKey,
//              :requestId,
//              :terminalNum,
//              :terminalNum,
//              :storeNum,
//              :timeZone,
//              :locationNum,
//              :termAppProfGen,
//              :equipModel,
//              :getPosCode(),
//              :AUTO_FLAG_DEFAULT,
//              :frontEnd,
//              :commType
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_796 = getPosCode();
   String theSqlTS = "insert into v_numbers\n          (\n            app_seq_num,\n            request_id,\n            terminal_number,\n            model_index,\n            store_number,\n            time_zone,\n            location_number,\n            app_code,\n            equip_model,\n            pos_code,\n            auto_flag,\n            front_end,\n            term_comm_type\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"38com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,requestId);
   __sJT_st.setString(3,terminalNum);
   __sJT_st.setString(4,terminalNum);
   __sJT_st.setString(5,storeNum);
   __sJT_st.setString(6,timeZone);
   __sJT_st.setString(7,locationNum);
   __sJT_st.setString(8,termAppProfGen);
   __sJT_st.setString(9,equipModel);
   __sJT_st.setInt(10,__sJT_796);
   __sJT_st.setString(11,AUTO_FLAG_DEFAULT);
   __sJT_st.setInt(12,frontEnd);
   __sJT_st.setInt(13,commType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2146^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("insertIntoVnumbers()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private int getPosCode()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    int                 result      = -1;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2169^7*/

//  ************************************************************
//  #sql [Ctx] { select POS_CODE
//          
//          from   merch_pos
//          where  APP_SEQ_NUM = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select POS_CODE\n         \n        from   merch_pos\n        where  APP_SEQ_NUM =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2175^7*/

    }
    catch(Exception e)
    {
      logEntry("getPosCode()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return result;
  }


  public void submitManData()
  {

    try
    {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2198^7*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//          set     process_end_date  = :today,
//                  vnum              = :manVnum,
//                  process_status    = :manStatus,
//                  process_response  = :manResponse
//          where   request_id        = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n        set     process_end_date  =  :1 ,\n                vnum              =  :2 ,\n                process_status    =  :3 ,\n                process_response  =  :4 \n        where   request_id        =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"40com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setString(2,manVnum);
   __sJT_st.setString(3,manStatus);
   __sJT_st.setString(4,manResponse);
   __sJT_st.setLong(5,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2206^7*/

      updateMerchVnumber();
      updateVnumbers();

      //puts in app_acccount_complete and moves to v number assignment queue completed
      VNumberBean.executeAppAccountComplete(primaryKey);


      //updateResponseStatus(primaryKey);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitManData: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }


  //static entry point to update vnumber tables with vnumber from auto mms process.
  //and to add entery to app_account_complete for xml stuff.. and to take out of v number queue
  public static void updateVnumberTables(long reqId, String v)
  {
    MmsStageSetupBeanNew  mssb = new MmsStageSetupBeanNew();
    try
    {
      mssb.setRequestId(Long.toString(reqId));
      mssb.setManVnum(v);

      mssb.updateMerchVnumber();
      mssb.updateVnumbers();

      //puts in app_acccount_complete and moves to v number assignment queue completed
      VNumberBean.executeAppAccountComplete(mssb.figureOutPrimaryKey(reqId, 0L));
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("MmsStageSetupBeanNew.updateVnumberTables(" + reqId + ")", e.toString());
    }
  }


  private void updateMerchVnumber()
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2257^7*/

//  ************************************************************
//  #sql [Ctx] { update  merch_vnumber
//          set     vnumber         = :manVnum
//          where   request_id      = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merch_vnumber\n        set     vnumber         =  :1 \n        where   request_id      =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"41com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,manVnum);
   __sJT_st.setLong(2,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2262^7*/

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updateMerchVnumber: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void updateVnumbers()
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2281^7*/

//  ************************************************************
//  #sql [Ctx] { update  v_numbers
//          set     v_number        = :manVnum
//          where   request_id      = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  v_numbers\n        set     v_number        =  :1 \n        where   request_id      =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"42com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,manVnum);
   __sJT_st.setLong(2,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2286^7*/

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updateVnumbers: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public boolean appApproved()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2309^7*/

//  ************************************************************
//  #sql [Ctx] it = { select merch_credit_status
//          from  merchant
//          where app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select merch_credit_status\n        from  merchant\n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"43com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"43com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2314^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        if(rs.getInt("merch_credit_status") == QueueConstants.CREDIT_APPROVE)
        {
          result = true;
        }
      }

      it.close();

    }
    catch(Exception e)
    {
      logEntry("appApproved()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;

  }


  private boolean alreadyExistsInQueue(long reqId)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2353^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    temp_activation_queue
//          where   request_id   = :reqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    temp_activation_queue\n        where   request_id   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"44com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"44com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2358^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }

      it.close();

    }
    catch(Exception e)
    {
      result = false;
      logEntry("alreadyExistsInQueue()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }


  private boolean alreadyExistsInMerchVnumber(long reqId)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2394^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    merch_vnumber
//          where   request_id   = :reqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    merch_vnumber\n        where   request_id   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"45com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"45com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2399^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }

      it.close();

    }
    catch(Exception e)
    {
      result = false;
      logEntry("alreadyExistsInMerchVnumber()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  private boolean alreadyExistsInVnumbers(long reqId)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2434^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    v_numbers
//          where   request_id   = :reqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    v_numbers\n        where   request_id   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"46com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"46com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2439^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }

      it.close();

    }
    catch(Exception e)
    {
      result = false;
      logEntry("alreadyExistsInVnumbers()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }


  private boolean trackRecordExists(long pk, int dept)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2475^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num
//          from    app_tracking
//          where   app_seq_num   = :pk and
//                  dept_code     = :dept
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num\n        from    app_tracking\n        where   app_seq_num   =  :1  and\n                dept_code     =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"47com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   __sJT_st.setInt(2,dept);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"47com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2481^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }

      it.close();

    }
    catch(Exception e)
    {
      result = false;
      logEntry("trackRecordExists()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  private boolean recordExists()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2516^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num
//          from    mms_stage_info
//          where   request_id = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num\n        from    mms_stage_info\n        where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"48com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,requestId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"48com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2521^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }

      it.close();

    }
    catch(Exception e)
    {
      result = false;
      logEntry("recordExists()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  public void setIncludeFileName()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2555^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  TERMINAL_APPLICATION_FILE_NAME
//          from    mms_terminal_applications
//          where   TERMINAL_APPLICATION   = :termAppProfGen
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  TERMINAL_APPLICATION_FILE_NAME\n        from    mms_terminal_applications\n        where   TERMINAL_APPLICATION   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"49com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,termAppProfGen);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"49com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2560^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        this.includeFileName = isBlank(rs.getString("TERMINAL_APPLICATION_FILE_NAME")) ? NOFILE : rs.getString("TERMINAL_APPLICATION_FILE_NAME");
      }
      else
      {
        this.includeFileName = NOFILE;
      }

      it.close();

    }
    catch(Exception e)
    {
      logEntry("setIncludeFileName()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public String getIncludeFileName()
  {
    return this.includeFileName;
  }

  /*
  **Manual Update VALIDATION
  */

  public boolean manValidate()
  {

    if(isBlank(manStatus))
    {
      addError("Please select a Status");
    }

    if(isBlank(manResponse))
    {
      addError("Please select a Response");
    }

    if(isBlank(manVnum))
    {
      addError("Please provide a V#");
    }
    else
    {
      manVnum = manVnum.toUpperCase();
    }

    return (!hasErrors());
  }

  private void validateTermAppEquipment()
  {
    try
    {
      connect();

      int recCount = 0;

      // see if there is a mapping between the selected terminal application
      // and the selected equipment
      /*@lineinfo:generated-code*//*@lineinfo:2630^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(ete.terminal_application)
//          
//          from    equip_termapp_equipment ete
//          where   ete.terminal_application = :termAppProfGen and
//                  ete.model_code = :equipModel
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ete.terminal_application)\n         \n        from    equip_termapp_equipment ete\n        where   ete.terminal_application =  :1  and\n                ete.model_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"50com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,termAppProfGen);
   __sJT_st.setString(2,equipModel);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2637^7*/

      if(recCount == 0)
      {
        addError("Invalid equipment selection for application " + termAppProfGen + ".");
      }
    }
    catch(Exception e)
    {
      logEntry("validateTermAppEquipment(" + termAppProfGen + ", " + equipModel + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void validateTermAppProfGen()
  {
    try
    {
      connect();

      int recCount = 0;

      /*@lineinfo:generated-code*//*@lineinfo:2662^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(etp.terminal_application)
//          
//          from    equip_termapp_profgen etp
//          where   etp.terminal_application = :termAppProfGen and
//                  etp.profgen_code = :profileGeneratorCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(etp.terminal_application)\n         \n        from    equip_termapp_profgen etp\n        where   etp.terminal_application =  :1  and\n                etp.profgen_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"51com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,termAppProfGen);
   __sJT_st.setInt(2,profileGeneratorCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2669^7*/

      if(recCount == 0)
      {
        addError("Invalid profile generator for application " + termAppProfGen + ".");
      }
    }
    catch(Exception e)
    {
      logEntry("validateTermAppProfGen(" + termAppProfGen + ", " + profileGeneratorCode + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void validateTermAppHost()
  {
    try
    {
      connect();

      int recCount = 0;

      /*@lineinfo:generated-code*//*@lineinfo:2694^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(etf.terminal_application)
//          
//          from    equip_termapp_frontends etf
//          where   etf.terminal_application = :termAppProfGen
//                  and etf.front_end = :frontEnd
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(etf.terminal_application)\n         \n        from    equip_termapp_frontends etf\n        where   etf.terminal_application =  :1 \n                and etf.front_end =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"52com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,termAppProfGen);
   __sJT_st.setInt(2,frontEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2701^7*/

      if( recCount == 0 )
      {
        addError("Invalid host for application " + termAppProfGen + ".");
      }
      // don't allow debit term apps on trident for non-deploy app types
      else if( "Y".equals(appTypeNonDeploy) && frontEnd == mesConstants.HOST_TRIDENT) 
      {
        /*@lineinfo:generated-code*//*@lineinfo:2710^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    mms_terminal_applications
//            where   terminal_application = :termAppProfGen
//                    and debit_enabled = 'Y'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    mms_terminal_applications\n          where   terminal_application =  :1 \n                  and debit_enabled = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"53com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,termAppProfGen);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2717^9*/
        
        if( recCount > 0 )
        {
          addError("Debit application not allowed on Trident for this app type (Non-Deploy)");
        }
      }
      
    }
    catch(Exception e)
    {
      logEntry("validateTermAppHost(" + termAppProfGen + ", " + frontEnd + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void validateTermAppCommType()
  {
    try
    {
      connect();

      int recCount = 0;

      /*@lineinfo:generated-code*//*@lineinfo:2744^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(etc.terminal_application)
//          
//          from    equip_termapp_commtypes etc
//          where   etc.terminal_application = :termAppProfGen and
//                  etc.comm_type = :commType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(etc.terminal_application)\n         \n        from    equip_termapp_commtypes etc\n        where   etc.terminal_application =  :1  and\n                etc.comm_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"54com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,termAppProfGen);
   __sJT_st.setInt(2,commType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2751^7*/

      if(recCount == 0)
      {
        addError("Invalid comm type for application " + termAppProfGen + ".");
      }
    }
    catch(Exception e)
    {
      logEntry("validateTermAppCommType(" + termAppProfGen + ", " + commType + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  **VALIDATION
  */
  public boolean validate()
  {
    if(!appApproved())
    {
      addError("This application has been declined, please do not continue further");
    }

    // make sure a front end (host) was selected
    if(frontEnd == mesConstants.HOST_INVALID)
    {
      addError("Please select a Host that this merchant will use for Auth and Settlement");
    }

    if(isBlank(termAppProfGen))
    {
      addError("Please select a Terminal Application");
    }
    else
    {
      // validate terminal application against equipment selection
      validateTermAppEquipment();

      // validate terminal application against profile generator
      validateTermAppProfGen();

      // validate terminal application against front end host
      validateTermAppHost();

      // validate terminal application against comm types
      validateTermAppCommType();
    }

    if(merchKeyRequired.equals("y"))
    {
      // only valid if profgen = trident, host = trident, termapp = TRI-STAGE or VIR-TERM, and commType = IP
      if(profileGeneratorCode != mesConstants.PROFGEN_TRIDENT)
      {
        addError("Merchant Key cannot be required for non-Trident profile generator");
      }

      if(frontEnd != mesConstants.HOST_TRIDENT)
      {
        addError("Merchant Key cannot be required for non-Trident Host");
      }

      if(commType != mesConstants.TERM_COMM_TYPE_IP)
      {
        if( commType == mesConstants.TERM_COMM_TYPE_WIRELESS &&
            equipModel != null &&
            equipModel.substring(0,2).equals("CA") )
        {
          // this is ok
        }
        else
        {            
          equipModel = equipModel == null ? "---" : equipModel;
          addError("Merchant Key cannot be required for Dial terminals (" + equipModel + ")");
        }
      }

      if( ! termAppProfGen.equals(mesConstants.TRIDENT_STAGE_BUILD_TERM_APP) &&
          ! termAppProfGen.equals(mesConstants.TRIDENT_VIRTUAL_TERMINAL_TERM_APP) &&
          ! termAppProfGen.equals(mesConstants.TRIDENT_API_TERM_APP) &&
          ! termAppProfGen.equals(mesConstants.TRIDENT_VIRTUAL_TERMINAL_LIMITED) )
      {
        addError("Merchant Key can only be required for Term Apps TRI-STAGE, VIR-TERM, and TRI-GATEWAY");
      }
    }
    else
    {
      if( termAppProfGen.equals(mesConstants.TRIDENT_API_TERM_APP) ||
          termAppProfGen.equals(mesConstants.TRIDENT_VIRTUAL_TERMINAL_LIMITED) )
      {
        addError("Merchant Key must be required for Term App TRI-GATEWAY");
      }
    }

    // set mms term app (based on the prof gen and the prof gen term app)
    if(profileGeneratorCode!=mesConstants.PROFGEN_MMS)
    {
      termApp = mesConstants.MMS_STAGE_BUILD_TERM_APP;
    }
    else
    {
      termApp = termAppProfGen;
    }

    // validate trident host selection against terminal application and equipment
    if(frontEnd == mesConstants.HOST_TRIDENT)
    {
      // check to make sure that profile generator is either VeriCentre or TermMaster
      if(profileGeneratorCode != mesConstants.PROFGEN_VERICENTRE &&
         profileGeneratorCode != mesConstants.PROFGEN_TERMMASTER &&
         profileGeneratorCode != mesConstants.PROFGEN_TRIDENT)
      {
        addError("Trident Host requires VeriCentre, TermMaster, or Trident as the Profile Generator");
      }
    }
    else if(frontEnd == mesConstants.HOST_VITAL && profileGeneratorCode == mesConstants.PROFGEN_TRIDENT)
    {
      addError("Applications supported by Trident Profile Generator must use Trident as the Host also.");
    }

    if(!isBlank(termApp) &&
       !termApp.equals(mesConstants.MMS_STAGE_BUILD_TERM_APP) &&
       !termApp.equals(mesConstants.TRIDENT_STAGE_BUILD_TERM_APP) &&
       wasSentFtp())
    {
      addError(termAppProfGen + " Terminal Application can only be sent using the manual option");
    }

    if(commType == -1)
    {
      addError("You must select a Comm Type for this profile");
    }

    if(isBlank(merchName))
    {
      addError("Please provide a Business Dba Name");
    }
    else if(merchName.length() > FIELD_LENGTH_MERCHNAME)
    {
      addError("Business Name (DBA) is too long.  Truncate to " + FIELD_LENGTH_MERCHNAME + " characters.");
    }

    if(isBlank(merchAddress))
    {
      addError("Please provide a Business Street Address");
    }
    else if(merchAddress.length() > FIELD_LENGTH_ADDRESS)
    {
      addError("Business Street Address is too long.  Truncate to "  + FIELD_LENGTH_ADDRESS + " characters.");
    }

    if(isBlank(merchCity))
    {
      addError("Please provide the City of your business address");
    }
    else if(merchCity.length() > FIELD_LENGTH_CITY)
    {
      addError("Business City is too long.  Truncate to 13 characters.");
    }

    if(isBlank(merchState))
    {
      addError("Please select the State of your business address");
    }

    if(isBlank(merchZip))
    {
      addError("Please provide the 5-digit zip code of your business address");
    }
    else if (countDigits(merchZip) != 5)
    {
      addError("Please provide a valid 5-digit business zip code.  e.g. (12345)");
    }
    else if(!isBlank(merchState)) //if zip and state present validate zipcode for state
    {
      merchZip = getDigitsOnly(merchZip);

      if(!validZipForState(merchState,merchZip.substring(0,5)))
      {
        addError("Business Zip Code " + merchZip + " is not a valid zip for Business State " + merchState);
      }
      else
      {
        if(merchZip.length() == 9)
        {
          merchZip = merchZip.substring(0,5) + "-" + merchZip.substring(5);
        }
      }
    }

    if(isBlank(timeZone))
    {
      addError("Please select a Time Zone for this merchant");
    }

    if(isBlank(dst))
    {
      addError("Please select whether or not they participate in day light savings time");
    }

    if(!isBlank(contactName) && contactName.length() > FIELD_LENGTH_CONTACTNAME)
    {
      addError("Contact name is too long, truncate to "+FIELD_LENGTH_CONTACTNAME+" characters");
    }

    if(isBlank(mcfs))
    {
      addError("Please provide an MCFS number");
    }
    else if(mcfs.length() != FIELD_LENGTH_MCFS)
    {
      addError("MCFS Number must be " + FIELD_LENGTH_MCFS + " digits long");
    }


    if(isBlank(customerServiceNum) || countDigits(customerServiceNum) != FIELD_LENGTH_CUSTOMERSERVICENUM)
    {
      addError("Please provide a 10 digit customer service phone number that appears on the customer billing statement");
    }
    else //we get digits only, we reformat when we display it
    {
      customerServiceNum = getDigitsOnly(customerServiceNum);
    }

    if(isBlank(merchPhone))
    {
      addError("Please provide your Merchant Phone Number");
    }
    else if (countDigits(merchPhone) != 10)
    {
      addError("Merchant Phone number must be a valid 10-digit number including area code. e.g. (4159991212)");
    }
    else //we get digits only, we reformat when we display it
    {
      merchPhone = getDigitsOnly(merchPhone);
    }


    if(isBlank(merchNum))
    {
      addError("Please provide the Merchant Number");
    }
    else //no fomatting
    {
      merchNum = getDigitsOnly(merchNum);

      if(merchNum.length() > FIELD_LENGTH_MERCHNUM)
      {
        addError("Merchant Number is too long.  Truncate to " + FIELD_LENGTH_MERCHNUM + " characters.");
      }
    }

    if(isBlank(mcc))
    {
      addError("Please select a MCC/SIC Code");
    }

    if(isBlank(binNum))
    {
      addError("Please select a bin number for this merchant");
    }

    if(isBlank(agentNum))
    {
      addError("Please provide an agent number for this merchant");
    }

    if(isBlank(chainNum))
    {
      addError("Please provide a chain number for this merchant");
    }

    if(isBlank(storeNum) && frontEnd != mesConstants.HOST_TRIDENT)
    {
      addError("Please provide a store number for this merchant");
    }
    else
    {
      storeNum = padWithZero(storeNum, 4);
    }

    if(isBlank(locationNum))
    {
      addError("Please provide a location number");
    }

    if(frontEnd != mesConstants.HOST_TRIDENT)
    {
      if(isBlank(terminalNum))
      {
        addError("Please provide a terminal number for this merchant");
      }
      else
      {
        terminalNum = padWithZero(terminalNum, 4);
        if(terminalNumUsed(merchNum, terminalNum, requestId))
        {
          addError("Terminal Number " + terminalNum + " is already being used by this merchant");
        }
      }
    }

    if(isBlank(serviceLevel))
    {
      addError("Please provide a service level code");
    }

    if(isBlank(termVoiceId))
    {
      addError("Please provide a terminal voice id");
    }
    else if(termVoiceId.length() > FIELD_LENGTH_TERMVOICEID)
    {
      addError("Terminal Voice ID is too long.  Truncate to " + FIELD_LENGTH_TERMVOICEID + " characters.");
    }

    if(isBlank(voiceAuthPhone))
    {
      addError("Please provide a voice auth phone");
    }
    else if(voiceAuthPhone.length() > FIELD_LENGTH_VOICEAUTHPHONE)
    {
      addError("Voice auth phone is too long.  Truncate to " + FIELD_LENGTH_VOICEAUTHPHONE + " characters.");
    }

    if(!isBlank(discNum))
    {
      if(discNum.length() > FIELD_LENGTH_DISCNUM)
      {
        addError("Discover Number is too long.  Truncate to " + FIELD_LENGTH_DISCNUM + " characters.");
      }

      if(isBlank(discAc))
      {
        addError("Please select a discover type");
      }
    }
    else if(!isBlank(discAc))
    {
      addError("Must deselect Discover type or provide a Discover merchant number");
    }

    if(!isBlank(amexNum))
    {
      if(amexNum.length() > FIELD_LENGTH_AMEXNUM)
      {
        addError("Amex Number is too long.  Truncate to " + FIELD_LENGTH_AMEXNUM + " characters.");
      }

      if(isBlank(amexAc))
      {
        addError("Please select an amex type");
      }
      if(isBlank(amexSplit))
      {
        addError("Please specify whether amex split dial or not");
      }
    }
    else if(!isBlank(amexAc))
    {
      addError("Must deselect amex type or provide an amex merchant number");
    }
    else if(!isBlank(amexSplit))
    {
      addError("Must deselect amex split dial or provide an amex merchant number and amex type");
    }

    if(!isBlank(dinersNum))
    {
      if(dinersNum.length() > FIELD_LENGTH_DINERSNUM)
      {
        addError("Diners Number is too long.  Truncate to " + FIELD_LENGTH_DINERSNUM + " characters.");
      }

      if(isBlank(dinersAc))
      {
        addError("Please select a diners type");
      }
    }
    else if(!isBlank(dinersAc))
    {
      addError("Must deselect Diners type or provide a Diners merchant number");
    }


    if(!isBlank(jcbNum))
    {
      if(jcbNum.length() > FIELD_LENGTH_JCBNUM)
      {
        addError("JCB Number is too long.  Truncate to " + FIELD_LENGTH_JCBNUM + " characters.");
      }
      if(isBlank(jcbAc))
      {
        addError("Please select a JCB type");
      }
    }
    else if(!isBlank(jcbAc))
    {
      addError("Must deselect JCB type or provide a JCB merchant number");
    }

    return (!hasErrors());
  }

  private String padWithZero(String var, int len)
  {
    while(var.length() < len)
    {
      var = "0" + var;
    }

    return var;
  }

  private boolean terminalNumUsed(String mNum, String tNum, long reqId)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:3178^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  terminal_number,request_id
//          from    mms_stage_info
//          where   merch_number = :mNum and terminal_number = :tNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  terminal_number,request_id\n        from    mms_stage_info\n        where   merch_number =  :1  and terminal_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"55com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mNum);
   __sJT_st.setString(2,tNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"55com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3183^7*/

      rs = it.getResultSet();

      if(rs.next() && rs.getLong("request_id") != reqId)
      {
        result = true;
      }

      it.close();
    }
    catch(Exception e)
    {
      result = false;
      logEntry("terminalNumUsed()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  private boolean validZipForState(String state, String zip)
  {
    boolean             result  = false;

    try
    {
      connect();

      int recCount = 0;

      /*@lineinfo:generated-code*//*@lineinfo:3217^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(state_code)
//          
//          from    rapp_app_valid_zipcodes
//          where   state_code = :state and
//                  :zip between zip_low and zip_high
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(state_code)\n         \n        from    rapp_app_valid_zipcodes\n        where   state_code =  :1  and\n                 :2  between zip_low and zip_high";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"56com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,state);
   __sJT_st.setString(2,zip);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3224^7*/

      result = (recCount > 0);
    }
    catch(Exception e)
    {
      result = false;
      logEntry("validZipForState()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  private String hostType     = "";
  private String productName  = "";

  private void getProductInfo(long pk)
  {
    log.debug("primary key = "+ pk);

    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:3255^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mp.pos_param,
//                  decode(pc.pos_type, '14','Trident', '15', 'Trident', '17', 'Trident', '5','Vital','TBD')
//                    as host_type,
//                  decode(pc.pos_type, '5',
//                    decode(mp.pos_param,'',pc.pos_desc,mp.pos_param) , pc.pos_desc)
//                      as pos_desc
//          from    merch_pos mp,
//                  pos_category pc
//          where   mp.app_seq_num = :pk
//          and     mp.pos_code = pc.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mp.pos_param,\n                decode(pc.pos_type, '14','Trident', '15', 'Trident', '17', 'Trident', '5','Vital','TBD')\n                  as host_type,\n                decode(pc.pos_type, '5',\n                  decode(mp.pos_param,'',pc.pos_desc,mp.pos_param) , pc.pos_desc)\n                    as pos_desc\n        from    merch_pos mp,\n                pos_category pc\n        where   mp.app_seq_num =  :1 \n        and     mp.pos_code = pc.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"57com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"57com.mes.ops.MmsStageSetupBeanNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3267^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        hostType = rs.getString("host_type");
        productName = rs.getString("pos_desc");
      }

      it.close();
    }
    catch(Exception e)
    {
      logEntry("getProductInfo()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void setHostType(String host)
  {
    hostType = host;
  }

  public String getHostType()
  {
    return hostType;
  }

  public void setProductName(String pName)
  {
    productName = pName;
  }

  public String getProductName()
  {
    return productName;
  }

  /*
  ** ACCESSORS
  */

  public  String  getBinNum()
  {
    return this.binNum;
  }
  public  String  getAgentNum()
  {
    return this.agentNum;
  }
  public String   getChainNum()
  {
    return this.chainNum;
  }
  public String   getMerchNum()
  {
    return this.merchNum;
  }
  public String   getLocationNum()
  {
    return this.locationNum;
  }
  public String   getStoreNum()
  {
    return this.storeNum;
  }
  public String   getTerminalNum()
  {
    return this.terminalNum;
  }
  public String   getEquipModel()
  {
    return this.equipModel;
  }

  public String   getEquipModelRefurb()
  {
    return this.equipModelRefurb;
  }

  public int      getProfileGeneratorCode()
  {
    return this.profileGeneratorCode;
  }
  public String   getProfileGeneratorName()
  {
    return EquipProfileGeneratorDictator.getEquipmentProfileGeneratorName(this.profileGeneratorCode);
  }
  public int getFrontEnd()
  {
    return frontEnd;
  }
  public void setFrontEnd(String frontEnd)
  {
    try
    {
      this.frontEnd = Integer.parseInt(frontEnd);
    }
    catch(Exception e)
    {
      this.frontEnd = -1;
    }
  }
  public int getCommType()
  {
    return commType;
  }
  public void setCommType(String commType)
  {
    try
    {
      this.commType = Integer.parseInt(commType);
    }
    catch(Exception e)
    {
      this.commType = -1;
    }
  }
  public void setMerchKeyRequired(String merchKeyRequired)
  {
    this.merchKeyRequired = (merchKeyRequired == null ? "n" : merchKeyRequired);
  }
  public String getMerchKeyRequired()
  {
    String result = "";
    if(merchKeyRequired.equals("y"))
    {
      result = "checked";
    }

    return result;
  }

  public String   getMerchName()
  {
    return this.merchName;
  }
  public String   getMerchAddress()
  {
    return this.merchAddress;
  }
  public String   getMerchCity()
  {
    return this.merchCity;
  }
  public String   getMerchState()
  {
    return this.merchState;
  }
  public String   getMerchZip()
  {
    return this.merchZip;
  }
  public String   getMerchPhone()
  {
    return this.merchPhone;
  }
  public String   getServiceLevel()
  {
    return this.serviceLevel;
  }
  public String   getAttachCode()
  {
    return this.attachCode;
  }
  public String   getMcc()
  {
    return this.mcc;
  }
  public String   getTermApp()
  {
    return this.termApp;
  }
  public String   getTermAppProfGen()
  {
    return this.termAppProfGen;
  }
  public String   getDiscNum()
  {
    return this.discNum;
  }
  public String   getDiscAc()
  {
    return this.discAc;
  }
  public String   getAmexNum()
  {
    return this.amexNum;
  }
  public String   getAmexAc()
  {
    return this.amexAc;
  }
  public String   getAmexSplit()
  {
    return this.amexSplit;
  }
  public String   getDinersNum()
  {
    return this.dinersNum;
  }
  public String   getDinersAc()
  {
    return this.dinersAc;
  }
  public String   getJcbNum()
  {
    return this.jcbNum;
  }
  public String   getJcbAc()
  {
    return this.jcbAc;
  }
  public String   getDebitAc()
  {
    return (debitAc != null && debitAc.equals("Y")) ? "checked" : "";
  }
  public String   getTermVoiceId()
  {
    return this.termVoiceId;
  }
  public String   getVoiceAuthPhone()
  {
    return this.voiceAuthPhone;
  }
  public String   getCdSet()
  {
    return this.cdSet;
  }

  public String getSpecialHandling()
  {
    return this.specialHandling;
  }

  public String getTimeZone()
  {
    return this.timeZone;
  }

  public String getDst()
  {
    return this.dst;
  }
  public String getContactName()
  {
    return this.contactName;
  }
  public String getProjectNum()
  {
    return this.projectNum;
  }
  public String getMcfs()
  {
    return this.mcfs;
  }
  public String getCustomerServiceNum()
  {
    return this.customerServiceNum;
  }

  //setters

  public void setDst(String dst)
  {
    this.dst = dst;
  }
  public void setContactName(String contactName)
  {
    this.contactName = contactName;
  }
  public void setProjectNum(String projectNum)
  {
    this.projectNum = projectNum;
  }
  public void setMcfs(String mcfs)
  {
    this.mcfs = mcfs;
  }
  public void setCustomerServiceNum(String customerServiceNum)
  {
    this.customerServiceNum = customerServiceNum;
  }

  public void setTimeZone(String timeZone)
  {
    this.timeZone = timeZone;
  }

  public void setSpecialHandling(String specialHandling)
  {
    this.specialHandling = specialHandling;
  }

  public  void  setBinNum(String binNum)
  {
    this.binNum = binNum;
  }
  public  void  setAgentNum(String agentNum)
  {
    this.agentNum = agentNum;
  }
  public void   setChainNum(String chainNum)
  {
    this.chainNum = chainNum;
  }
  public void   setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public void setMerchant(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public void   setLocationNum(String locationNum)
  {
    this.locationNum = locationNum;
  }
  public void   setStoreNum(String storeNum)
  {
    this.storeNum = storeNum;
  }
  public void   setTerminalNum(String terminalNum)
  {
    this.terminalNum = terminalNum;
  }
  public void setEquipModel(String equipModel)
  {
    this.equipModel = equipModel;
  }
  public void setEquipModelRefurb(String equipModelRefurb)
  {
    this.equipModelRefurb = equipModelRefurb;
  }
  public void setProfileGeneratorCode(int profileGeneratorCode)
  {
    this.profileGeneratorCode = profileGeneratorCode;
  }
  public void   setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public void   setMerchAddress(String merchAddress)
  {
    this.merchAddress = merchAddress;
  }
  public void   setMerchCity(String merchCity)
  {
    this.merchCity = merchCity;
  }
  public void   setMerchState(String merchState)
  {
    this.merchState = merchState;
  }
  public void   setMerchZip(String merchZip)
  {
    this.merchZip = merchZip;
  }
  public void   setMerchPhone(String merchPhone)
  {
    this.merchPhone = merchPhone;
  }
  public void   setServiceLevel(String serviceLevel)
  {
    this.serviceLevel = serviceLevel;
  }
  public void   setAttachCode(String attachCode)
  {
    this.attachCode = attachCode;
  }
  public void   setMcc(String mcc)
  {
    this.mcc = mcc;
  }
  public void   setTermApp(String termApp)
  {
    this.termApp = termApp;
  }
  public void   setTermAppProfGen(String termAppProfGen)
  {
    this.termAppProfGen = termAppProfGen;
  }
  public void   setDiscNum(String discNum)
  {
    this.discNum = discNum;
  }
  public void   setDiscAc(String discAc)
  {
    this.discAc = discAc;
  }
  public void   setAmexNum(String amexNum)
  {
    this.amexNum = amexNum;
  }
  public void   setAmexAc(String amexAc)
  {
    this.amexAc = amexAc;
  }
  public void   setAmexSplit(String amexSplit)
  {
    this.amexSplit = amexSplit;
  }
  public void   setDinersNum(String dinersNum)
  {
    this.dinersNum = dinersNum;
  }
  public void   setDinersAc(String dinersAc)
  {
    this.dinersAc = dinersAc;
  }
  public void   setJcbNum(String jcbNum)
  {
    this.jcbNum = jcbNum;
  }
  public void   setJcbAc(String jcbAc)
  {
    this.jcbAc = jcbAc;
  }
  public void setDebitAc(String debitAc)
  {
    System.out.println("setting debitAc: " + debitAc);
    this.debitAc = debitAc;
  }
  public void   setTermVoiceId(String termVoiceId)
  {
    this.termVoiceId = termVoiceId;
  }
  public void   setVoiceAuthPhone(String voiceAuthPhone)
  {
    this.voiceAuthPhone = voiceAuthPhone;
  }
  public void   setCdSet(String cdSet)
  {
    this.cdSet = cdSet;
  }

  public int getAppType()
  {
    return this.appType;
  }

  public void setAppType(String appType)
  {
    try
    {
      this.appType = Integer.parseInt(appType);
    }
    catch(Exception e)
    {
      this.appType = -1;
    }
  }

  public String getAppTypeDesc()
  {
    return this.appTypeDesc;
  }

  public void setAppTypeDesc(String appTypeDesc)
  {
    this.appTypeDesc = appTypeDesc;
  }

  public String getAppTypeNonDeploy()
  {
    return this.appTypeNonDeploy;
  }

  public void setAppTypeNonDeploy(String appTypeNonDeploy)
  {
    this.appTypeNonDeploy = appTypeNonDeploy;
  }


  public long getPrimaryKey()
  {
    return this.primaryKey;
  }

  public void setPrimaryKey(String primaryKey)
  {
    try
    {
      this.primaryKey = Long.parseLong(primaryKey);
    }
    catch(Exception e)
    {
      addError("Error: Please exit and restart application - Primary key not valid");
      this.primaryKey = 0L;
    }
  }

  public long getRequestId()
  {
    return this.requestId;
  }

  public void setRequestId(String requestId)
  {
    try
    {
      this.requestId = Long.parseLong(requestId);
    }
    catch(Exception e)
    {
      addError("Error: Please exit and restart application - Request Id not valid");
      this.requestId = 0L;
    }
  }


/* get all status info */
  public String getDateTimeOfTransmission()
  {
    return formatResponseData(dateTimeOfTransmission);
  }
  public String getDateTimeOfResponse()
  {
    return formatResponseData(dateTimeOfResponse);
  }

  private String formatResponseData(String data)
  {
    if(isBlank(data))
    {
      return "----";
    }

    return data;
  }



/* manual changes variables*/
  public String getManVnum()
  {
    return this.manVnum;
  }

  public void setManVnum(String manVnum)
  {
    this.manVnum = manVnum;
  }

  public String getManStatus()
  {
    return this.manStatus;
  }
  public void setManStatus(String manStatus)
  {
    this.manStatus = manStatus;
  }

  public String getManResponse()
  {
    return this.manResponse;
  }
  public void setManResponse(String manResponse)
  {
    this.manResponse = manResponse;
  }

  public boolean isSubmitted()
  {
    return this.submitted;
  }

  public boolean isErrorStatus()
  {
    return this.errorStatus;
  }

  public boolean isOkToGetGets()
  {
    return this.okToGetGets;
  }

  public boolean isReSubmitted()
  {
    return this.reSubmitted;
  }

  public boolean isManSubmitted()
  {
    return this.manSubmitted;
  }

  public boolean isAllowManUpdate()
  {
    return this.allowManUpdate;
  }

  public boolean isShowTimestamps()
  {
    return this.showTimestamps;
  }

  public void dontAllowManUpdate()
  {
    this.allowManUpdate = false;
  }

  public void setAllowManUpdate(String temp)
  {
    this.allowManUpdate = true;
  }

  public boolean isQueuedForTrans()
  {
    return this.queuedForTrans;
  }

  public void setSubmit(String temp)
  {
    this.submitted = true;
  }

  public void setReSubmit(String temp)
  {
    this.reSubmitted = true;
  }

  public void setManSubmit(String temp)
  {
    this.manSubmitted = true;
  }

  public void setTransSubmit(String temp)
  {
    this.transmissionMethod = mesConstants.ETPPM_FTP;
    this.submitted = true;
  }
  public void setManualSubmit(String temp)
  {
    this.transmissionMethod = mesConstants.ETPPM_MANUAL;
    this.submitted = true;
  }

  public boolean wasSentFtp()
  {
    return this.transmissionMethod.equals(mesConstants.ETPPM_FTP);
  }

  public boolean wasSentManual()
  {
    return this.transmissionMethod.equals(mesConstants.ETPPM_MANUAL);
  }


  public String getTransmissionMethod()
  {
    return this.transmissionMethod;
  }

  public String getTransmissionStatus()
  {
    return formatResponseData(this.transmissionStatus);
  }
  public String getTransmissionResponse()
  {
    return formatResponseData(this.transmissionResponse);
  }

  public String getVnum()
  {
    return formatResponseData(this.vNum);
  }

  public String getErrorDescriptions()
  {
    return this.errorDescriptions;
  }

  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }

  public void addError(String err)
  {
    try
    {
      errors.add(err);
    }
    catch(Exception e)
    {
    }
  }

  public boolean hasMsgs()
  {
    return(msgs.size() > 0);
  }

  public void addMsg(String msg)
  {
    try
    {
      msgs.add(msg);
    }
    catch(Exception e)
    {
    }
  }

  private String glueDate(String dateStr, String timeStr)
  {
    String result = "";

    if(!isBlank(dateStr) && !isBlank(timeStr))
    {
      result = dateStr + " " + timeStr;
    }
    else if(!isBlank(dateStr))
    {
      result = dateStr;
    }

    return result;

  }

  public boolean isBlank(String test)
  {
    boolean pass = false;

    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }

    return pass;
  }

  public boolean isNumber(String test)
  {
    boolean pass = false;

    try
    {
      long i = Long.parseLong(test);
      pass = true;
    }
    catch(Exception e)
    {
    }

    return pass;
  }

  public boolean isRealNumber(String test)
  {
    boolean pass = false;

    try
    {
      double i = Double.parseDouble(test);
      pass = true;
    }
    catch(Exception e)
    {
    }

    return pass;
  }

  public long getDigits(String raw)
  {
    long          result      = 0L;
    StringBuffer  digits      = new StringBuffer("");

    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }

      result = Long.parseLong(digits.toString());
    }
    catch(Exception e)
    {
      if(raw != null && !raw.equals(""))
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDigits('" + raw + "'): " + e.toString());
      }
    }

    return result;
  }

  public String getDigitsOnly(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");
    String        result      = "";

    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }

      result = digits.toString();
    }
    catch(Exception e)
    {}

    return result;
  }


  public int countDigits(String raw)
  {
    int result = 0;

    for(int i=0; i<raw.length(); ++i)
    {
      if(Character.isDigit(raw.charAt(i)))
      {
        ++result;
      }
    }

    return result;
  }

  public boolean isCheckAccepted()
  {
    return( CheckAccepted );
  }

  public void setAction(String action)
  {
    this.action = action;
  }

  public String getAction()
  {
    return this.action;
  }

  public String getCheckProviderName( )
  {
    return( CheckProvider );
  }

  public String getCheckMerchId( )
  {
    return( CheckMerchId );
  }

  public String getCheckTermId( )
  {
    return( CheckTermId );
  }

  public boolean isValutecAccepted()
  {
    return( ValutecAccepted );
  }

  public String getValutecMerchId( )
  {
    return( ValutecMerchId );
  }

  public String getValutecTermId( )
  {
    return( ValutecTermId );
  }

  public boolean isEmail(String test)
  {
    boolean pass = false;

    int firstAt = test.indexOf('@');
    if (!isBlank(test) && firstAt > 0 &&
        test.indexOf('@',firstAt + 1) == -1)
    {
      pass = true;
    }

    return pass;
  }

  public boolean isTrident()
  {
    return (frontEnd == mesConstants.HOST_TRIDENT);
  }

  private String formatPhone(String phone)
  {
    if(isBlank(phone))
    {
      return "";
    }
    if(phone.length() != 10)
    {
      return "";
    }
    String areaCode   = phone.substring(0,3);
    String firstThree = phone.substring(3,6);
    String lastFour   = phone.substring(6);
    return ("(" + areaCode + ")" + " " + firstThree + "-" + lastFour);
  }

  private String formatSsn(String ssn)
  {
    if(isBlank(ssn))
    {
      return "";
    }
    if(ssn.length() != 9)
    {
      return ssn;
    }
    String part1   = ssn.substring(0,3);
    String part2   = ssn.substring(3,5);
    String part3   = ssn.substring(5);
    return (part1 + "-" + part2 + "-" + part3);
  }

  /*
  ** METHOD setReqId
  */
  private long setReqId()
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:4209^7*/

//  ************************************************************
//  #sql [Ctx] { select  mms_application_sequence.nextval reqId
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mms_application_sequence.nextval reqId\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"58com.mes.ops.MmsStageSetupBeanNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   requestId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4214^7*/
    }
    catch(Exception e)
    {
      logEntry("(setReqId)", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return this.requestId;
  }

}/*@lineinfo:generated-code*/