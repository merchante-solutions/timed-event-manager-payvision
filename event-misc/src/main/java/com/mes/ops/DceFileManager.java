package com.mes.ops;

import java.util.HashMap;
import java.util.Vector;


public class DceFileManager
{

  private HashMap           dceFileHash         = new HashMap();
  private Vector            fileIdentifierList  = new Vector();
  private DceFile           currentFile         = null;

  /*
  ** CONSTRUCTOR public DceFileManager()
  **
  ** Default constructor.
  */
  public DceFileManager()
  {
    
  }

  public DceFile getDceFile(String fileIdentifier)
  {
    return getDfFromHash(fileIdentifier, true);
  }

  public DceFile getDceFile(String fileIdentifier, boolean getNewDf)
  {
    return getDfFromHash(fileIdentifier, getNewDf);
  }

  public void saveDceFile(DceFile df)
  {
    putDfInHash(df);
  }


  public int getFileIdentifierListSize()
  {
    return fileIdentifierList.size();
  }

  public String getFileIdentifierFromList(int idx)
  {
    return (String)fileIdentifierList.elementAt(idx);
  }

  public void closeFiles()
  {
    for(int x=0; x<getFileIdentifierListSize(); x++)
    {
      currentFile = getDfFromHash(getFileIdentifierFromList(x), false);
      if(currentFile != null)
      {
        currentFile.closeFile();
        putDfInHash(currentFile);
      }
    }
  }

  public void deleteFiles()
  {
    for(int x=0; x<getFileIdentifierListSize(); x++)
    {
      currentFile = getDfFromHash(getFileIdentifierFromList(x), false);
      if(currentFile != null)
      {
        currentFile.deleteFile();
        putDfInHash(currentFile);
      }
    }
  }

  private DceFile getDfFromHash(String fileIdentifier)
  {
    return getDfFromHash(fileIdentifier, true);
  }

  private DceFile getDfFromHash(String fileIdentifier, boolean getNewDf)
  {
    if(dceFileHash.containsKey(fileIdentifier))
    {
      return ((DceFile)dceFileHash.get(fileIdentifier));
    }
    else if(getNewDf)
    {
      return (new DceFile(fileIdentifier));
    }
    else
    {
      return null;
    }
  }

  private void putDfInHash(DceFile df)
  {
    dceFileHash.put(df.getFileIdentifier(), df);
    addFileIdentifier(df.getFileIdentifier());
  }

  private void addFileIdentifier(String fileIdentifier)
  {
    if(!fileIdentifierList.contains(fileIdentifier))
    {
      fileIdentifierList.add(fileIdentifier);
    }
  }      

}
