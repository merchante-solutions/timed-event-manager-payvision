/*@lineinfo:filename=ScheduleMonthBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ScheduleMonthBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.util.Calendar;

public class ScheduleMonthBean extends ScheduleBaseBean
{
  public  final static long SQUARE_NOT_USED   = -1L;

  private long[][]        month               = new long[6][7];
  private int[]           dropDownMonths      = new int[12];
  private int             selectedDay         = -1;

  
  
  public ScheduleMonthBean()
  {
    //initializes calendar to be current date/time
    super();
    initializeMonths();
  }

  public ScheduleMonthBean(int scheduleType)
  {
    //initializes calendar to be current date/time
    super(scheduleType);
    initializeMonths();
  }
  
  private void initializeMonths()
  {
    for(int row=0; row < month.length; row++)
    {
      for(int col=0; col < month[row].length; col++)
      {
        month[row][col] = SQUARE_NOT_USED;
      }
    }

    dropDownMonths[0]  = Calendar.JANUARY;
    dropDownMonths[1]  = Calendar.FEBRUARY;
    dropDownMonths[2]  = Calendar.MARCH;
    dropDownMonths[3]  = Calendar.APRIL;
    dropDownMonths[4]  = Calendar.MAY;
    dropDownMonths[5]  = Calendar.JUNE;
    dropDownMonths[6]  = Calendar.JULY;
    dropDownMonths[7]  = Calendar.AUGUST;
    dropDownMonths[8]  = Calendar.SEPTEMBER;
    dropDownMonths[9]  = Calendar.OCTOBER;
    dropDownMonths[10] = Calendar.NOVEMBER;
    dropDownMonths[11] = Calendar.DECEMBER;
  } 

  public void setMonthCalendar(String momentInTime)
  {
    try
    {
      setMonthCalendar(Long.parseLong(momentInTime));
    }
    catch(Exception e)
    {
      logEntry("setMonthCalendar(" + momentInTime + ")", e.toString());
    }
  }
  public void setMonthCalendar(long momentInTime)
  {
    //setup calendar
    setCalendarDate(momentInTime);

    //this gets the day they selected so we can show on the calendar that this day is selected
    this.selectedDay = getCalendarDay();
    
    //setup month
    setMonthCalendar(getCalendarYear(), getCalendarMonth());
  }


  public void setMonthCalendar(int selectedYear, int selectedMonth)
  {

    //clears out the month grid
    initializeMonths();
    
    setCalendarDate(selectedYear, selectedMonth);

    int  dayOfWeek        = getCalendarFieldValue(Calendar.DAY_OF_WEEK) - 1;
    int  row              = 0;
    
    for(int numOfDays = 0; numOfDays < getActualMaximum(Calendar.DAY_OF_MONTH); numOfDays++)
    {

      //System.out.println("The time has been set to: "                 + getCalendarString());
      //System.out.println("The Long representation of this date is: "  + getLongTimeOfCalendar());


      month[row][dayOfWeek] = getLongTimeOfCalendar();

      //increment the day by one
      rollCalendarFieldOneUnit(Calendar.DAY_OF_MONTH ,true);
  
      //increment dayOfWeek
      ++dayOfWeek;
      
      if(dayOfWeek > 6)
      {
        //increment row
        ++row;

        //reset dayOfWeek
        dayOfWeek = 0;
      }

    }
  }


  public int getMonthMaxRows()
  {
    return month.length;
  }

  public int getMonthMaxCols()
  {
    return month[0].length;
  }

  public int dropDownMonthsMax()
  {
    return dropDownMonths.length;
  }

  public int getDropDownMonth(int idx)
  {
    return dropDownMonths[idx];
  }

  public long getMonthData(int row, int col)
  {
    return month[row][col];
  }

  public int getSelectedDay()
  {
    return this.selectedDay;
  }

  //this is only here temp so i can compile it.. it needs to have sql code in an sqlj or it wont compile or somethng..
  public boolean updateAllFailed()
  {
    try
    {
      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:191^7*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info2
//  
//          set     process_response   = 1,
//                  process_status     = 2
//  
//          where   process_start_date is not null and 
//                  process_end_date   is null 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info2\n\n        set     process_response   = 1,\n                process_status     = 2\n\n        where   process_start_date is not null and \n                process_end_date   is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.ops.ScheduleMonthBean",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^7*/
    }
    catch(Exception e)
    {
      logEntry("updateAllFails()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return true;
  }


}/*@lineinfo:generated-code*/