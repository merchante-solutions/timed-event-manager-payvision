/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/MmsQueueBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 6/07/02 4:17p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

public class MmsQueueBean extends QueueBean
{
  public void fillReportMenuItems()
  {
    addReportMenuItem(new ReportMenuItem("MMS New Queue"      , "mes_queue.jsp?queueType=" + QueueConstants.QUEUE_MMS + "&queueStage=" + QueueConstants.Q_MMS_NEW));
    addReportMenuItem(new ReportMenuItem("MMS Pend Queue"     , "mes_queue.jsp?queueType=" + QueueConstants.QUEUE_MMS + "&queueStage=" + QueueConstants.Q_MMS_PEND));
    addReportMenuItem(new ReportMenuItem("MMS Error Queue"    , "mmspendingqueue.jsp"));
    addReportMenuItem(new ReportMenuItem("MMS Completed Queue", "mes_queue.jsp?queueType=" + QueueConstants.QUEUE_MMS + "&queueStage=" + QueueConstants.Q_MMS_COMPLETED));
  }

  public MmsQueueBean()
  {
    super("MmsQueueBean");
    fillReportMenuItems();
  }
  
  public MmsQueueBean(String msg)
  {
    super(msg);
    fillReportMenuItems();
  }
  
  public String getLockInfo(long controlNumber)
  {
    // get application sequence number from controlnumber
    return super.getLockInfo(this.getAppSeqNum(controlNumber));
  }
  
  public boolean setLock(String lockedBy, long controlNumber)
  {
    return super.setLock(this.getAppSeqNum(controlNumber), com.mes.ops.QueueConstants.QUEUE_MMS, lockedBy);
  }
  
  public void getFunctionOptions(javax.servlet.jsp.JspWriter out, long appSeqNum)
  {
    try
    {
      out.println("<ul>");


      //out.println("  <li><a href=\"mes_app_assign.jsp?primaryKey=" + appSeqNum + "\">Terminal Application Assignment</a></li>");
      
      //if(getQueueStage() == QueueConstants.Q_SETUP_EXPORT)
      //{
        //out.println("  <li><a href=\"mes_export_file.jsp?primaryKey=" + appSeqNum + "\">Output File Export</a></li>");
      //}
      //else if(getQueueStage() == QueueConstants.Q_SETUP_QA)
      //{
        //out.println("  <li><a href=\"mes_dataexpand.jsp?primaryKey=" + appSeqNum + "\">Review & Verify Data Expansion</a></li>");
      //}
      //else
      //{
        out.println("  <li><a href=\"mms_management_screen.jsp?primaryKey=" + appSeqNum + "\">MMS Management Screen</a></li>");
      //}
      //out.println("  <li><a href=\"mes_fax_file.jsp?primaryKey=" + appSeqNum + "\">Fax File Export</a></li>");
      //out.println("  <li><a href=\"mes_setup_status.jsp?primaryKey=" + appSeqNum + "\">Account Setup Status</a></li>");
      out.println("  <li><a href=\"mes_credit_notes.jsp?appSeqNum=" + appSeqNum + "&queueStage=" + queueStage + "\">Notes</a></li>");
      out.println("</ul>");
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFunctionOptions: " + e.toString());
    }
  }
  
}
