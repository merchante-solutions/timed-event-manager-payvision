/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/EquipmentManagementQueueBean.java $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 11/12/02 4:48p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

public class EquipmentManagementQueueBean extends QueueBean
{
  public void fillReportMenuItems()
  {
    addReportMenuItem(new ReportMenuItem("Equipment Deployment Queue", "mes_queue.jsp?queueType=" + QueueConstants.QUEUE_EQUIPMENT + "&queueStage=" + QueueConstants.Q_EQUIPMENT_NEW));
  }
  
  public EquipmentManagementQueueBean()
  {
    super("EquipmentManagementQueueBean");
    fillReportMenuItems();
  }
  
  public EquipmentManagementQueueBean(String msg)
  {
    super(msg);
    fillReportMenuItems();
  }
  
  public boolean getQueueData(long queueKey, int queueType)
  {
    return(super.getQueueData(
            queueKey,
            queueType,
            "merchant merch",
            "merch.merc_cntrl_number, merch.merch_business_name, merch.merch_number",
            "and a.app_seq_num = merch.app_seq_num"));
  }
  
  public void getFunctionOptions(javax.servlet.jsp.JspWriter out, long appSeqNum)
  {
    try
    {
      if(getQueueResultSet() != null)
      {
        setMerchNum(getQueueResultSet().getString("merch_number"));
      }
      
      out.println("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" width=\"80%\">");

      out.println("  <tr>");
      out.println("    <td height=\"19\" valign=\"center\">");
      out.println("      &nbsp;");
      out.println("    </td>");
      out.println("    <td height=\"19\" valign=\"left\">");
      out.println("      <a href=\"equipmentdeployment.jsp?merchnum=" + getMerchNum() + "\">Deploy Equipment</a>");
      out.println("    </td>");
      out.println("  </tr>");

      out.println("  <tr>");
      out.println("    <td height=\"19\" valign=\"center\">");
      out.println("      &nbsp;");
      out.println("    </td>");
      out.println("    <td height=\"19\" valign=\"left\">");
      out.println("      <a href=\"/jsp/inventory/merchantswap.jsp?merchnum=" + getMerchNum() + "\">Equipment Swap</a>");
      out.println("    </td>");
      out.println("  </tr>");

      out.println("  <tr>");
      out.println("    <td height=\"19\" valign=\"center\">");
      out.println("      &nbsp;");
      out.println("    </td>");
      out.println("    <td height=\"19\" valign=\"left\">");
      out.println("      <a href=\"/jsp/inventory/pinpadexchange.jsp?merchnum=" + getMerchNum() + "\">Pinpad Exchange</a>");
      out.println("    </td>");
      out.println("  </tr>");

      out.println("  <tr>");
      out.println("    <td height=\"19\" valign=\"center\">");
      out.println("      &nbsp;");
      out.println("    </td>");
      out.println("    <td height=\"19\" valign=\"left\">");
      out.println("      <a href=\"packingslip.jsp?primaryKey=" + appSeqNum + "\">View Packing Slip</a>");
      out.println("    </td>");
      out.println("  </tr>");


      out.println("  <tr>");
      out.println("    <td height=\"19\" valign=\"center\">");
      out.println("      &nbsp;");
      out.println("    </td>");
      out.println("    <td height=\"19\" valign=\"left\">");
      out.println("      <a href=\"mes_tracking_notes.jsp?primaryKey=" + appSeqNum + "&department=" + com.mes.ops.QueueConstants.DEPARTMENT_DEPLOYMENT + "&sortorder=1" + "\">Notes</a>");
      out.println("    </td>");
      out.println("  </tr>");
      out.println("</table>");
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getFunctionOptions: " + e.toString());
    }
  }
}
