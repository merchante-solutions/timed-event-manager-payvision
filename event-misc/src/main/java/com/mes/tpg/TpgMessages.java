/*@lineinfo:filename=TpgMessages*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tpg/TpgMessages.sqlj $

  Description:  


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tpg;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.TextareaField;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class TpgMessages extends TpgBaseDataBean
{
  public static final String      MT_INDUSTRY_NEWS      = "IN";
  public static final String      MT_SERVICE_ALERTS     = "SA";
  
  public static class MessageTypeTable extends DropDownTable
  {
    public MessageTypeTable()
    {
      addElement(MT_INDUSTRY_NEWS, "Industry News");
      addElement(MT_SERVICE_ALERTS,"Service Alerts");
    }
  }
  
  public TpgMessages( )
  {
  }
  
  protected boolean autoSubmit( )
  {
    String        fname       = getAutoSubmitName();
    long          recId       = getLong("recId");
    boolean       retVal      = true;
    String        value       = null;
    
    try
    {
      if ( fname.equals("btnSave") )
      {
        Date expDate = ((DateField)getField("msgExpDate")).getSqlDate();
        /*@lineinfo:generated-code*//*@lineinfo:90^9*/

//  ************************************************************
//  #sql [Ctx] { update  trident_api_messages
//            set     bank_number = :getData("bankNumber"),
//                    message_type = :getData("msgType"),
//                    message_expiration_date = :expDate,
//                    message_text = :getData("msgText")
//            where   rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1436 = getData("bankNumber");
 String __sJT_1437 = getData("msgType");
 String __sJT_1438 = getData("msgText");
   String theSqlTS = "update  trident_api_messages\n          set     bank_number =  :1 ,\n                  message_type =  :2 ,\n                  message_expiration_date =  :3 ,\n                  message_text =  :4 \n          where   rec_id =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.tpg.TpgMessages",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1436);
   __sJT_st.setString(2,__sJT_1437);
   __sJT_st.setDate(3,expDate);
   __sJT_st.setString(4,__sJT_1438);
   __sJT_st.setLong(5,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:98^9*/
        
        // no records updated and no exception, try insert
        if ( Ctx.getExecutionContext().getUpdateCount() == 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:103^11*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_api_messages
//              ( 
//                bank_number, 
//                message_type, 
//                message_expiration_date, 
//                message_text 
//              )
//              values
//              (
//                :getData("bankNumber"),
//                :getData("msgType"),
//                :expDate,
//                :getData("msgText")
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1439 = getData("bankNumber");
 String __sJT_1440 = getData("msgType");
 String __sJT_1441 = getData("msgText");
   String theSqlTS = "insert into trident_api_messages\n            ( \n              bank_number, \n              message_type, \n              message_expiration_date, \n              message_text \n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.tpg.TpgMessages",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1439);
   __sJT_st.setString(2,__sJT_1440);
   __sJT_st.setDate(3,expDate);
   __sJT_st.setString(4,__sJT_1441);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:119^11*/
        }
      }
      else if ( fname.equals("btnDelete") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:124^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    trident_api_messages
//            where   rec_id = :recId 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    trident_api_messages\n          where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tpg.TpgMessages",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:129^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("autoSubmit(" + recId + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    Field         field;

    super.createFields(request);
    
    fgroup = new FieldGroup("addMessageGroup");
    
    fgroup.add( new NumberField("bankNumber",4,4,false,0) );   
    fgroup.add(new DateField("msgExpDate",false));
    ((DateField)fgroup.getField("msgExpDate")).setDateFormat("MM/dd/yyyy");
    fgroup.add( new DropDownField("msgType", "Message Type", new MessageTypeTable(), false) );
    fgroup.add( new TextareaField("msgText", "Message Text", 4096, 6, 32, true) );
    fgroup.add(new HiddenField("recId"));
    setData("recId","0");
    fields.add(fgroup);
    
    fields.add( new ButtonField( "btnSave", "Save" ) );
    fields.add( new ButtonField( "btnCancel", "Cancel" ) );
    fields.add( new ButtonField( "btnDelete", "Delete" ) );
  }
  
  public void loadData( )
  {
    loadData( getReportHierarchyNode(), ReportDateBegin, ReportDateEnd );
  }
  
  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
  }
  
  public void loadIndustryNews()
  {
    loadMessages( MT_INDUSTRY_NEWS );
  }
  
  public void loadMessages( String msgType )
  {
    int                 bankNumber    = getUser().getOrgBankId();
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    
    try
    {
      ReportRows.removeAllElements();
      
      /*@lineinfo:generated-code*//*@lineinfo:189^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tam.message_text          as msg_text
//          from    trident_api_messages      tam
//          where   tam.bank_number = decode(:bankNumber,9999,3941,:bankNumber) and
//                  tam.message_expiration_date >= trunc(sysdate) and
//                  tam.message_type = :msgType
//          order by tam.message_creation_date desc                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tam.message_text          as msg_text\n        from    trident_api_messages      tam\n        where   tam.bank_number = decode( :1 ,9999,3941, :2 ) and\n                tam.message_expiration_date >= trunc(sysdate) and\n                tam.message_type =  :3 \n        order by tam.message_creation_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tpg.TpgMessages",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setString(3,msgType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.tpg.TpgMessages",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.add( resultSet.getString("msg_text") );
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadMessages()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadServiceAlerts()
  {
    loadMessages( MT_SERVICE_ALERTS );
  }
  
  public static void main(String[] args)
  {
    TpgMessages bean = null;
    
    try
    {
      bean = new TpgMessages();
      bean.createFields(null);
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
  }
}/*@lineinfo:generated-code*/