/*@lineinfo:filename=VoiceAuthDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tpg/VoiceAuthDataBean.sqlj $

  Description:  


  Last Modified By   : 
  Last Modified Date : 
  Version            : 

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tpg;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.safehaus.uuid.UUID;
import org.safehaus.uuid.UUIDGenerator;
import com.mes.api.StoreTransactionTask;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiTranBase;
import com.mes.api.TridentApiTransaction;
import com.mes.forms.ButtonField;
import com.mes.forms.ButtonReset;
import com.mes.forms.Condition;
import com.mes.forms.CurrencyField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.forms.ZipField;
import com.mes.support.HttpHelper;
import com.mes.support.TridentTools;
import sqlj.runtime.ResultSetIterator;

public class VoiceAuthDataBean extends TpgBaseDataBean 
{
  private Vector  ErrorMessage = new Vector();
  private TridentApiTransaction ApiTran = null; 
  
  protected static final String[][] TransactionType = 
  {
    { "Sale", TridentApiTranBase.TT_DEBIT },
    { "Credit", TridentApiTranBase.TT_CREDIT },
    { "Offline", TridentApiTranBase.TT_FORCE },
    { "Authorization Only", TridentApiTranBase.TT_PRE_AUTH },
  };
  
  private class MerchantValidation 
    implements Validation
  {
    private String ErrorMessage = null;
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate( String fdata )
    {
      int recCount = 0;
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:93^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(mf.merchant_number) 
//            from    mif  mf
//            where   mf.merchant_number = :fdata
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mf.merchant_number)  \n          from    mif  mf\n          where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tpg.VoiceAuthDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fdata);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:98^9*/        
      }
      catch( java.sql.SQLException e )
      {
        logEntry("validate(" + fdata + ")", e.toString());
        ErrorMessage = "Merchant validation failed";
      }
      
      if ( recCount == 0 )
      {
        ErrorMessage = "Invalid Merchant ID";
      } 

      return( ErrorMessage == null );
    }
  }

  private class AllTransactionCondition
    implements Condition
  {
    Field     TranType    = null;
    
    public AllTransactionCondition( Field tranTypeField )
    {
      TranType = tranTypeField;  
    }

    public boolean isMet() 
    { 
      boolean     retVal    = false;
    
      if ( TranType != null )
      {
        String fData = TranType.getData();
        if ( fData.equals(TridentApiTranBase.TT_DEBIT) || fData.equals(TridentApiTranBase.TT_PRE_AUTH) ||
             fData.equals(TridentApiTranBase.TT_FORCE) || fData.equals(TridentApiTranBase.TT_CREDIT) )
        {
          retVal = true;      
        }
      }
      return( retVal );
    }
  }
  
  private class OfflineTransactionCondition
    implements Condition
  {
    Field     TranType    = null;
  
    public OfflineTransactionCondition( Field tranTypeField )
    {
      TranType = tranTypeField;  
    }

    public boolean isMet() 
    { 
      boolean     retVal    = false;
  
      if ( TranType != null )
      {
        if ( TranType.getData().equals(TridentApiTranBase.TT_FORCE) )
        {
          retVal = true;      
        }
      }
      return( retVal );
    }
  }
  
  private class AVSZipCondition
  implements Condition
  {
    Field     TranType    = null;

    public AVSZipCondition( Field tranTypeField )
    {
      TranType = tranTypeField;  
    }

    public boolean isMet() 
    { 
      boolean     retVal    = false;

      if ( TranType != null )
      {
        String fData = TranType.getData();
        if ( fData.equals(TridentApiTranBase.TT_DEBIT) )
        {
          retVal = true;      
        }
      }
      return( retVal );
    }
  }
  


  public VoiceAuthDataBean( )
  {
  }

  public Vector getSubmitErrors()
  {
    return( ErrorMessage );
  }
  
  
  public TridentApiTransaction getTridentApiTran()
  {
    return( ApiTran );
  }
    
  protected boolean autoSubmit( )
  {
    boolean   prodServer     = false;
    boolean   retVal         = true;
    String    fname          = getAutoSubmitName();
    String    merchantId     = getData("merchant_id");
    String    tranType       = getData(TridentApiConstants.FN_TRAN_TYPE);
    String    pId            = getData(TridentApiConstants.FN_TID);
    
    if ( fname.equals("midSubmit") || fname.equals("mid_submit") )
    {
      if ( !loadVauthProfile(merchantId) )
      {
        retVal = false;
        ErrorMessage.add("No Valid Profile ID found!");
      }
    }
    else if ( fname.equals("tranSubmit") )
    {
      String serverName = ServletRequest.getServerName();
      String hostName = this.toString() + System.currentTimeMillis();
      UUID uuid = UUIDGenerator.getInstance().generateNameBasedUUID(null, hostName);
      prodServer = HttpHelper.isProdServer(null);

      // build an api transaction
      ApiTran = new TridentApiTransaction( uuid.toString().replaceAll("-", "") );
      ApiTran.setProperties(ServletRequest);
      ApiTran.setProfile( loadProfile() );
            
      if ( tranType.equals(TridentApiTranBase.TT_FORCE) )
      {
        if ( isCardTypeAccepted(ApiTran.getCardType()) )
        {
          if ( TridentTools.mod10Check(ApiTran.getCardNumberFull()) )
          {
            StoreTransactionTask task = new StoreTransactionTask(ApiTran);
            task.setProdFlagDefault(prodServer);
            retVal = task.doTask();
            if ( !retVal )
            {
              ApiTran.setErrorCode(task.getErrorCode());
              ApiTran.setErrorDesc(task.getErrorDesc());
            }
          }
          else
          {
            retVal = false;
            ApiTran.setErrorCode(TridentApiConstants.ER_INVALID_CARD_NUMBER);
            ApiTran.setErrorDesc("Invalid card number.");
          }                        
        }
        else
        {
          retVal = false;
          ApiTran.setErrorCode(TridentApiConstants.ER_CT_NOT_ACCEPTED);
          ApiTran.setErrorDesc("Card Type Not Accepted.");        
        }
      }
      else if ( tranType.equals(TridentApiTranBase.TT_CREDIT))
      {
        if ( isCardTypeAccepted(ApiTran.getCardType()) )
        {
          if ( TridentTools.mod10Check(ApiTran.getCardNumberFull()) )
          {
            StoreTransactionTask task = new StoreTransactionTask(ApiTran);
            task.setProdFlagDefault(prodServer);
            retVal = task.doTask();
            if ( !retVal )
            {
              ApiTran.setErrorCode(task.getErrorCode());
              ApiTran.setErrorDesc(task.getErrorDesc());
            }
          }
          else
          {
            retVal = false;
            ApiTran.setErrorCode(TridentApiConstants.ER_INVALID_CARD_NUMBER);
            ApiTran.setErrorDesc("Invalid card number.");
          }        
        }
        else
        {
          retVal = false;
          ApiTran.setErrorCode(TridentApiConstants.ER_CT_NOT_ACCEPTED);
          ApiTran.setErrorDesc("Card Type Not Accepted.");        
        }
      }
      else if ( tranType.equals(TridentApiTranBase.TT_DEBIT) ||
          tranType.equals(TridentApiTranBase.TT_PRE_AUTH) )
      {        
        if( prodServer )
        {
          ApiTran.setAuthorizationHost( TridentApiConstants.DESKTOP_HOST_PRODUCTION );
        }
        else if ( isProductionRequest() )
        {
          ApiTran.setAuthorizationHost( TridentApiConstants.DESKTOP_HOST_PRODUCTION );
        }
        else  // default to the auth simulator
        {
          ApiTran.setAuthorizationHost( TridentApiConstants.DESKTOP_HOST_SIMULATOR );
        }
        ApiTran.setDeveloperId( TridentApiConstants.VOICE_AUTH_DEVELOPER_ID);
        ApiTran.loadData();   // load any required data from Oracle
        
        try
        {
          retVal = ApiTran.requestAuthorization();
          if ( retVal == true && tranType.equals(TridentApiTranBase.TT_DEBIT)) 
          {
            if( getData(TridentApiConstants.FN_INV_NUM).trim().equals("") )
            {
              String tmpStr = ApiTran.VisaDResponse.getRetrievalReferenceNumber();
              if( tmpStr.length() > 6 )
              {
                ApiTran.setPurchaseId( tmpStr.substring( tmpStr.length() - 6, tmpStr.length()));
              }
              else
              {
                ApiTran.setPurchaseId("999999");                        
              }
            }
            
            // approved, store Sale Transaction into the database
            StoreTransactionTask task = new StoreTransactionTask(ApiTran);
            task.setProdFlagDefault(prodServer);
            retVal = task.doTask();
            if ( !retVal )
            {
              ApiTran.setErrorCode(task.getErrorCode());
              ApiTran.setErrorDesc(task.getErrorDesc());
            }
          }
        }
        catch( java.io.IOException ioe )
        {
          //ioe.printStackTrace();
          System.out.println("autoSubmit()" + ioe.toString()); 
          logEntry("autoSubmit()",ioe.toString());
          retVal = false;   // failed to get auth
          ApiTran.setError(TridentApiConstants.ER_INTERNAL_ERROR);
        }                  
      }             
    }

    return( retVal );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup           fgroup;
    Field                field;
    Condition            condition;
    Validation           validation;
    boolean              nullAllowed = true;
    RadioButtonField     rbtn = null;

    super.createFields(request);
        
    fgroup = (FieldGroup)getField("searchFields");
    
    field = new Field( TridentApiConstants.FN_TID, "Profile ID", 32, 32, true);
    field.makeReadOnly();
    fgroup.add( field );    
    field = new Field( TridentApiConstants.FN_DBA_NAME, "DBA Name", TridentApiConstants.FLM_MERCHANT_NAME, 32, true);
    field.makeReadOnly();
    fgroup.add( field );
    field = new Field( TridentApiConstants.FN_DBA_CITY, "DBA City", TridentApiConstants.FLM_MERCHANT_CITY, 32, true);
    field.makeReadOnly();
    fgroup.add( field );
    field = new Field( TridentApiConstants.FN_DBA_STATE, "DBA State", TridentApiConstants.FLM_MERCHANT_STATE, 32, true);
    field.makeReadOnly();
    fgroup.add( field );
    field = new Field( "total_unsettled_amount","Total Unsettled Amount", 16, 16, true);
    field.makeReadOnly();
    fgroup.add(field);
    fgroup.add( new Field( TridentApiConstants.FN_CARD_NUMBER, "Account Number", 16, 32, false) );
    fgroup.add( new Field( TridentApiConstants.FN_EXP_DATE, "Expiration Date", 4, 4, false) );
    fgroup.add( new Field( TridentApiConstants.FN_AVS_STREET, "Address", 32, 40, true) );      
    fgroup.add( new Field( TridentApiConstants.FN_AUTH_CODE, "Approval Code", 6, 8, false) );
    fgroup.add( new Field( TridentApiConstants.FN_INV_NUM, "Invoice Number", 17, 20, true) );  
    fgroup.add( new ButtonField( "midSubmit", "Submit" ) );
    fgroup.add( new ButtonField( "tranSubmit", "Submit Transaction" ) );    
    fgroup.add( new ButtonReset( "btnReset","Reset"));
    fgroup.add( new CurrencyField( TridentApiConstants.FN_AMOUNT, "Amount",12,12,false) );     
    fgroup.add( new CurrencyField( TridentApiConstants.FN_TAX, "Tax Amount",12,12,true) );        
    fgroup.add( new HiddenField( TridentApiConstants.FN_DBA_ZIP) );
    fgroup.add( new HiddenField( TridentApiConstants.FN_SIC) );
    fgroup.add( new HiddenField( TridentApiConstants.FN_PHONE) );
    fgroup.add( new HiddenField( TridentApiConstants.FN_MOTO_IND, "1") );  
    fgroup.add( new HiddenField( "product_code" ) );    
    fgroup.add( new HiddenField( "vmc_accept" ) );    
    fgroup.add( new HiddenField( "amex_accept" ) );    
    fgroup.add( new HiddenField( "disc_accept" ) );        
    fgroup.add( new HiddenField( "mid_submit") );    
    fgroup.add( new NumberField( "unsettled_tran_count", "Unsettled Tran Count", 16, 16, true, 0) );
    fgroup.add( new NumberField( "merchant_id", "Merchant ID", 16, 28, false, 0) );
    fgroup.add( new ZipField( TridentApiConstants.FN_AVS_ZIP, "Zip Code",false) );         
    fgroup.add( new RadioButtonField(TridentApiConstants.FN_TRAN_TYPE, "Transaction Type",TransactionType, -1, true, "Required", 2) );

    rbtn = (RadioButtonField)(fgroup.getField(TridentApiConstants.FN_TRAN_TYPE));
    rbtn.setOnClick(0, "loadAuthCodeField()");
    rbtn.setOnClick(1, "loadAuthCodeField()");
    rbtn.setOnClick(2, "loadAuthCodeField()");
    
    fgroup.getField("merchant_id").addValidation( new MerchantValidation());    
    condition = new AllTransactionCondition(fgroup.getField(TridentApiConstants.FN_TRAN_TYPE));
    fgroup.getField(TridentApiConstants.FN_AMOUNT).setOptionalCondition( condition );
    fgroup.getField(TridentApiConstants.FN_CARD_NUMBER).setOptionalCondition( condition ); 
    fgroup.getField(TridentApiConstants.FN_EXP_DATE).setOptionalCondition( condition ); 
    
    condition = new AVSZipCondition(fgroup.getField(TridentApiConstants.FN_TRAN_TYPE));
    fgroup.getField(TridentApiConstants.FN_AVS_ZIP).setOptionalCondition( condition );
    
    condition = new OfflineTransactionCondition(fgroup.getField(TridentApiConstants.FN_TRAN_TYPE));
    fgroup.getField(TridentApiConstants.FN_AUTH_CODE).setOptionalCondition( condition );

    fgroup.setHtmlExtra("class=\"tpgFormLabel\"");    
  }
  
  public void loadProfileData( String profileId ) 
  {
    loadTridentProfile( profileId );
    getPendingSettlementTotals( profileId );
  }
  
  public void loadTridentProfile( String profileId ) 
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:443^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.merchant_number  as merchant_id,
//                  tp.terminal_id      as profile_id,
//                  tp.merchant_name    as merchant_name,
//                  tp.addr_city        as merchant_city,
//                  tp.addr_state       as merchant_state,
//                  tp.addr_zip         as merchant_zip,
//                  tp.sic_code         as merchant_category_code,
//                  tp.phone_number     as merchant_phone,
//                  tp.product_code     as product_code,
//                  upper(nvl(tp.vmc_accept,'N'))    as vmc_accept,
//                  upper(nvl(tp.amex_accept,'N'))   as amex_accept,
//                  upper(nvl(tp.disc_accept,'N'))   as disc_accept
//          from    trident_profile        tp
//          where   tp.terminal_id =:profileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tp.merchant_number  as merchant_id,\n                tp.terminal_id      as profile_id,\n                tp.merchant_name    as merchant_name,\n                tp.addr_city        as merchant_city,\n                tp.addr_state       as merchant_state,\n                tp.addr_zip         as merchant_zip,\n                tp.sic_code         as merchant_category_code,\n                tp.phone_number     as merchant_phone,\n                tp.product_code     as product_code,\n                upper(nvl(tp.vmc_accept,'N'))    as vmc_accept,\n                upper(nvl(tp.amex_accept,'N'))   as amex_accept,\n                upper(nvl(tp.disc_accept,'N'))   as disc_accept\n        from    trident_profile        tp\n        where   tp.terminal_id = :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tpg.VoiceAuthDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,profileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.tpg.VoiceAuthDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:459^7*/
      resultSet = it.getResultSet();
      
      if( resultSet.next() ) 
      {
        setFields(resultSet,false,false);
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadProfileData(" + String.valueOf(profileId) + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  public void getPendingSettlementTotals( String profileId )
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    Date                today         = new java.sql.Date(Calendar.getInstance().getTime().getTime());

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:487^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(tapi.transaction_amount)   as unsettled_tran_count,
//                  nvl(sum( tapi.transaction_amount * 
//                      decode(tapi.DEBIT_CREDIT_INDICATOR,'C',-1,1)), 0)
//                                                   as total_unsettled_amount
//          from    trident_capture_api    tapi
//          where   tapi.terminal_id = :profileId and 
//                  tapi.transaction_date >= (sysdate-60) and
//                  tapi.transaction_type in ( 'P','V','D','C','O' ) and 
//                  (
//                    tapi.transaction_type != 'V' or
//                    to_date(tapi.last_modified_date) = :today
//                  ) and
//                  nvl(tapi.load_file_id,0) = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(tapi.transaction_amount)   as unsettled_tran_count,\n                nvl(sum( tapi.transaction_amount * \n                    decode(tapi.DEBIT_CREDIT_INDICATOR,'C',-1,1)), 0)\n                                                 as total_unsettled_amount\n        from    trident_capture_api    tapi\n        where   tapi.terminal_id =  :1  and \n                tapi.transaction_date >= (sysdate-60) and\n                tapi.transaction_type in ( 'P','V','D','C','O' ) and \n                (\n                  tapi.transaction_type != 'V' or\n                  to_date(tapi.last_modified_date) =  :2 \n                ) and\n                nvl(tapi.load_file_id,0) = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tpg.VoiceAuthDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,profileId);
   __sJT_st.setDate(2,today);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.tpg.VoiceAuthDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:502^7*/
      resultSet = it.getResultSet();
      
      if( resultSet.next() ) 
      {
        setFields(resultSet,false,false);
      }

      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("getPendingSettlementTotals(" + String.valueOf(profileId) + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  public String getAvsResultDesc( String avsResultCode )
  {
    String    value          = "";
    String    resultCode     = avsResultCode.toUpperCase();
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:529^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(response_message,'') 
//          from    tc33_authorization_avs_code   ac
//          where   ac.avs_result_code = :resultCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(response_message,'')  \n        from    tc33_authorization_avs_code   ac\n        where   ac.avs_result_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tpg.VoiceAuthDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,resultCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   value = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:534^7*/
      
      if ( value == null )
      {
        value = "";
      }
    }
    catch( Exception e )
    {
      // ignore
      //logEntry("getAvsResultDesc()",e.toString());
    }
    return( value );
  }
  
  public boolean isCardTypeAccepted( String cardType )
  {
    boolean    retVal     = false;

    if( cardType.equals("VS") || cardType.equals("MC") )
    {
      retVal = getData("vmc_accept").equals("Y");
    }
    else if( cardType.equals("AM") )
    {
      retVal = getData("amex_accept").equals("Y");
    }
    else if( cardType.equals("DS") )
    {
      retVal = getData("disc_accept").equals("Y");
    }
    
    return retVal;
  }
  
  public boolean loadVauthProfile( String merchantId ) 
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    String              productCode   = null;
    boolean             retVal        = false;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:578^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.merchant_number  as merchant_id,
//                  tp.terminal_id      as profile_id,
//                  tp.product_code     as product_code
//          from    mif                 mf,
//                  trident_profile     tp
//          where   mf.merchant_number = :merchantId
//                  and tp.merchant_number = mf.merchant_number
//                  and tp.profile_status = 'A'
//          order by tp.terminal_id      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tp.merchant_number  as merchant_id,\n                tp.terminal_id      as profile_id,\n                tp.product_code     as product_code\n        from    mif                 mf,\n                trident_profile     tp\n        where   mf.merchant_number =  :1 \n                and tp.merchant_number = mf.merchant_number\n                and tp.profile_status = 'A'\n        order by tp.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tpg.VoiceAuthDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.tpg.VoiceAuthDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:589^7*/
      resultSet = it.getResultSet();
      
      int index = 0;
      boolean exitLoop = false;
      while( resultSet.next() && exitLoop == false )
      {
        if( index == 0 )
        {
          setFields(resultSet,false,false);
          retVal = true;
        }
        productCode = processString(resultSet.getString("product_code"));
        if( productCode.equals("TP"))
        {
          setFields(resultSet,false,false);
          exitLoop = true;
        }
        ++ index;
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadVauthProfile(" + String.valueOf(merchantId) + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( retVal );
  }
    
  protected void postHandleRequest( HttpServletRequest request )
  {
    loadProfileData(getData("profile_id"));
  }
}/*@lineinfo:generated-code*/