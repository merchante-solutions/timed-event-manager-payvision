/*@lineinfo:filename=TpgPreAuthDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.61.151/svn/mesweb/branches/te/src/main/com/mes/tpg/TpgPreAuthDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-01-23 15:10:34 -0800 (Thu, 23 Jan 2014) $
  Version            : $Revision: 22205 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

 **************************************************************************/
package com.mes.tpg;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.mes.api.ApiDb;
import com.mes.api.ApiRequestFactory;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiProfile;
import com.mes.api.TridentApiRequest;
import com.mes.api.TridentApiResponse;
import com.mes.api.TridentApiTranBase;
import com.mes.api.bml.BmlAuthorizeTran;
import com.mes.api.bml.BmlBaseTran;
import com.mes.api.bml.BmlCaptureTran;
import com.mes.api.bml.BmlDb;
import com.mes.api.bml.BmlReauthorizeTran;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class TpgPreAuthDataBean extends TpgBaseDataBean {
  static Logger log = Logger.getLogger(TpgPreAuthDataBean.class);

  protected String TridentTranId = null;

  public TpgPreAuthDataBean() {
  }

  public static class DashBoardTpgTotals {

    double preAuthAmount;
    int preAuthCount;
    double pendingSettlementAmount;
    int pendingSettlementCount;
    double lastSettledBatchAmount;
    int lastSettledBatchCount;
    String lastSettlementDate;
    String currentBatchDate;

    public void setPreAuthAmount(double preAuthAmount) {
      this.preAuthAmount = preAuthAmount;
    }

    public void setPreAuthCount(int preAuthCount) {
      this.preAuthCount = preAuthCount;
    }

    public void setPendingSettlementAmount(double pendingSettlementAmount) {
      this.pendingSettlementAmount = pendingSettlementAmount;
    }

    public void setPendingSettlementCount(int pendingSettlementCount) {
      this.pendingSettlementCount = pendingSettlementCount;
    }

    public void setLastSettledBatchAmount(double lastSettledBatchAmount) {
      this.lastSettledBatchAmount = lastSettledBatchAmount;
    }

    public void setLastSettledBatchCount(int lastSettledBatchCount) {
      this.lastSettledBatchCount = lastSettledBatchCount;
    }

    public void setLastSettlementDate(String lastSettlementDate) {
      this.lastSettlementDate = lastSettlementDate;
    }

    public void setCurrentBatchDate() {
      DateFormat dateFormat = new SimpleDateFormat("mm/dd/yyyy");
      Calendar cal = Calendar.getInstance();
      this.currentBatchDate = dateFormat.format(cal.getTime());
    }

    public double getPreAuthAmount() {
      return preAuthAmount;
    }

    public int getPreAuthCount() {
      return preAuthCount;
    }

    public double getPendingSettlementAmount() {
      return pendingSettlementAmount;
    }

    public int getPendingSettlementCount() {
      return pendingSettlementCount;
    }

    public double getLastSettledBatchAmount() {
      return lastSettledBatchAmount;
    }

    public int getLastSettledBatchCount() {
      return lastSettledBatchCount;
    }

    public String getLastSettlementDate() {
      return lastSettlementDate;
    }

    public String getCurrentBatchDate() {
      return currentBatchDate;
    }

  }

  private void doBmlReauth(String tranId, String amountStr) {
    // load bml auth
    BmlAuthorizeTran auth = BmlDb.lookupRefAuth(tranId);

    // HACK: determine URL str based on auth server name
    String tapiUrlStr = getTapiUrlFromServerName(auth.getServerName());

    // fetch profile data
    String pid = auth.getProfileId();
    TridentApiProfile profile = ApiDb.loadProfile(pid);

    // create reauth request
    TridentApiRequest request = new TridentApiRequest(tapiUrlStr);
    request.addArg(TridentApiConstants.FN_TRAN_TYPE, TridentApiTranBase.TT_BML_REQUEST);
    request.addArg(BmlBaseTran.FN_BML_REQUEST, BmlBaseTran.BR_REAUTHORIZE);
    request.addArg(TridentApiConstants.FN_TID, pid);
    request.addArg(TridentApiConstants.FN_TERM_PASS, profile.getProfileKey());
    request.addArg(BmlReauthorizeTran.FN_BML_AMOUNT, amountStr);
    request.addArg(BmlBaseTran.FN_REF_TRAN_ID, tranId);
    request.addArg(BmlBaseTran.FN_CENTINEL_URL_OVERRIDE, auth.getCardinalCentinelUrl());

    // send the request
    boolean sendOk = false;
    try {
      log.debug("sending reauthorize request: " + request);
      TridentApiResponse response = request.post();
    } catch (Exception e) {
      log.error("Error sending request: " + e);
      e.printStackTrace();
    }
  }

  private void doBmlCapture(String tranId, String amountStr) {
    // load bml auth
    BmlAuthorizeTran auth = BmlDb.lookupRefAuth(tranId);

    // HACK: determine URL str based on auth server name
    String tapiUrlStr = getTapiUrlFromServerName(auth.getServerName());

    // fetch profile data
    String pid = auth.getProfileId();
    TridentApiProfile profile = ApiDb.loadProfile(pid);

    // create capture request
    TridentApiRequest request = new TridentApiRequest(tapiUrlStr);
    request.addArg(TridentApiConstants.FN_TRAN_TYPE, TridentApiTranBase.TT_BML_REQUEST);
    request.addArg(BmlBaseTran.FN_BML_REQUEST, BmlBaseTran.BR_CAPTURE);
    request.addArg(TridentApiConstants.FN_TID, pid);
    request.addArg(TridentApiConstants.FN_TERM_PASS, profile.getProfileKey());
    request.addArg(BmlCaptureTran.FN_BML_AMOUNT, amountStr);
    request.addArg(BmlBaseTran.FN_REF_TRAN_ID, tranId);
    request.addArg(BmlCaptureTran.FN_BML_AUTO_CAP, "N");
    request.addArg(BmlBaseTran.FN_CENTINEL_URL_OVERRIDE, auth.getCardinalCentinelUrl());

    // send the request
    boolean sendOk = false;
    try {
      log.debug("sending capture request: " + request);
      TridentApiResponse response = request.post();

      // HACK: need to give tapi server a few seconds to update oracle
      Thread.sleep(5000L);
    } catch (InterruptedException ie) {
    } catch (Exception e) {
      log.error("Error sending request: " + e);
      e.printStackTrace();
    }
  }

  protected boolean autoSubmit() {
    String apiHost = null;
    String fname = getAutoSubmitName();
    boolean retVal = true;
    double tranAmount = 0.0;
    double fxAmount = 0.0;
    boolean approved = false;

    if (HttpHelper.isProdServer(null) || isProductionRequest()) {
      apiHost = "https://api.merchante-solutions.com/mes-api/tridentApi";
    } else // default to the auth simulator
    {
      apiHost = "https://test.merchante-solutions.com/mes-api/tridentApi";
    }

    if (fname.equals("btnReauth")) {
      doBmlReauth(getData("tridentTranId"), getData("reauthAmount"));
      retVal = true;
    } else if (fname.equals("btnCapture")) {
      doBmlCapture(getData("tridentTranId"), getData("capAmount"));
      retVal = true;
    } else if (fname.equals("btnSettle")) {
      if (!getField("settleAmount").isBlank()) {
        retVal = true; // assume this passes
        StringTokenizer amounts = new StringTokenizer(getData("settleAmount"), "-");
        StringTokenizer fxAmounts = new StringTokenizer(getData("settleFxAmount"), "-");
        StringTokenizer tranIds = new StringTokenizer(getData("tridentTranId"), "-");
        StringTokenizer emulations = new StringTokenizer(getData("emulation"), "-");
        String lastTranId = null;
        String[] credentials = loadCredentials();
        TridentApiRequest apiRequest = new TridentApiRequest(apiHost);

        while (tranIds.hasMoreTokens() && amounts.hasMoreTokens() && fxAmounts.hasMoreTokens()
            && emulations.hasMoreTokens()) {
          String amount = (String) amounts.nextToken();
          String fxAmt = (String) fxAmounts.nextToken();
          String tranId = (String) tranIds.nextToken();
          String emulation = (String) emulations.nextToken();

          // attempt to parse the double
          try {
            tranAmount = Double.parseDouble(amount);
            fxAmount = Double.parseDouble(fxAmt);
          } catch (Exception nfe) {
            tranAmount = 0.0;
            fxAmount = 0.0;
          }

          approved = false;
          apiRequest.resetParms();
          if (emulation.equals("AUTHNET")) {
            apiRequest.setArg(TridentApiConstants.FN_AN_TID, credentials[0]);
            apiRequest.setArg(TridentApiConstants.FN_AN_PASSWORD, credentials[1]);
            apiRequest.setArg(TridentApiConstants.FN_AN_VERSION, "3.1");
            apiRequest.setArg(TridentApiConstants.FN_AN_TRAN_TYPE, "prior_auth_capture");
            apiRequest.setArg(TridentApiConstants.FN_AN_TRAN_ID, getTranIdNumber(tranId));
            apiRequest.setArg(TridentApiConstants.FN_AN_AMOUNT, tranAmount);
          } else {
            apiRequest.setArg(TridentApiConstants.FN_TID, credentials[0]);
            apiRequest.setArg(TridentApiConstants.FN_TERM_PASS, credentials[1]);
            apiRequest.setArg(TridentApiConstants.FN_TRAN_TYPE, TridentApiTranBase.TT_SETTLE);
            apiRequest.setArg(TridentApiConstants.FN_TRAN_ID, tranId);
            apiRequest.setArg(TridentApiConstants.FN_AMOUNT, tranAmount);
            apiRequest.setArg(TridentApiConstants.FN_FX_AMOUNT, fxAmount);
          }

          TridentApiResponse resp = apiRequest.post();

          if (emulation.equals("AUTHNET")) {
            String rawResponse = (String) resp.getParameterList().get(0);
            String items[] = rawResponse.split(",");
            if (items[2].equals(TridentApiConstants.RC_AN_APPROVED)) {
              approved = true;
            }
          } else {
            if (TridentApiConstants.ER_NONE.equals(resp
                .getParameter(TridentApiConstants.FN_ERROR_CODE))) {
              approved = true;
            }
          }

          if (approved) {
            lastTranId = tranId;
          }
        }

        if (lastTranId != null) {
          ApiDb.waitForUpdate(lastTranId, TridentApiTranBase.TT_DEBIT, 120000L);
          log.debug("waitForUpdate tranId: " + lastTranId);
        }
      }
    } else if (fname.equals("btnVoid")) {
      retVal = true; // assume this passes

      TridentApiRequest apiRequest = ApiRequestFactory.createVoidRequest(loadCredentials(), null);
      apiRequest.setPostUrl(apiHost);

      StringTokenizer tokens = new StringTokenizer(getData("tridentTranId"), "-");
      while (tokens.hasMoreTokens()) {
        String tranId = (String) tokens.nextToken();
        apiRequest.setArg(TridentApiConstants.FN_TRAN_ID, tranId);
        TridentApiResponse resp = apiRequest.post();
        ApiDb.waitForUpdate(tranId, TridentApiTranBase.TT_VOID);
      }
    }

    return (retVal);
  }

  public String rejectTransactions() {
    String apiHost = null;
    if (HttpHelper.isProdServer(null) || isProductionRequest()) {
      apiHost = "https://api.merchante-solutions.com/mes-api/tridentApi";
    } else // default to the auth simulator
    {
      apiHost = "https://test.merchante-solutions.com/mes-api/tridentApi";
    }

    String transData = HttpHelper.getString(request, "transData", "[]");

    JSONParser parser = new JSONParser();
    JSONArray array = new JSONArray();
    try {
      array = (JSONArray) parser.parse(transData);
    } catch (Exception e) {
      log.error("rejectTransactions() - Error parsing transData in HTTP request: " + e.toString());
    }

    JSONArray responseArray = new JSONArray();
    TridentApiRequest apiRequest = null;
    TridentApiResponse resp = null;
    for (int i = 0; i < array.size(); i++) {
      String transId = (String) array.get(i);

      if (i == 0) {
        apiRequest = ApiRequestFactory.createVoidRequest(loadCredentialsForTrans(transId), null);
        apiRequest.setPostUrl(apiHost);
      }

      apiRequest.setArg(TridentApiConstants.FN_TRAN_ID, transId);
      resp = apiRequest.post();
      ApiDb.waitForUpdate(transId, TridentApiTranBase.TT_VOID);

      JSONObject responseObj = new JSONObject();
      responseObj.put("responseText", resp.getParameter(TridentApiConstants.FN_AUTH_RESP_TEXT));
      responseObj.put("transId", resp.getParameter(TridentApiConstants.FN_TRAN_ID));
      responseObj.put("responseCode", resp.getParameter(TridentApiConstants.FN_ERROR_CODE));
      responseArray.add(responseObj);
    }

    return responseArray.toString();
  }

  public String acceptTransactions() {
    String apiHost = null;
    if (HttpHelper.isProdServer(null) || isProductionRequest()) {
      apiHost = "https://api.merchante-solutions.com/mes-api/tridentApi";
    } else // default to the auth simulator
    {
      apiHost = "https://test.merchante-solutions.com/mes-api/tridentApi";
    }

    String transData = HttpHelper.getString(request, "transData", "[]");

    JSONParser parser = new JSONParser();
    JSONArray array = new JSONArray();
    try {
      array = (JSONArray) parser.parse(transData);
    } catch (Exception e) {
      log.error("acceptTransactions() - Error parsing transData in HTTP request: " + e.toString());
    }

    String lastTranId = null;
    JSONArray responseArray = new JSONArray();
    String[] credentials = null;
    TridentApiRequest apiRequest = null;
    for (int i = 0; i < array.size(); i++) {
      JSONObject trans = (JSONObject) array.get(i);
      double amount = Double.parseDouble(String.valueOf(trans.get("amount")));
      double fxAmt = Double.parseDouble(String.valueOf(trans.get("fxAmt")));
      String transId = String.valueOf(trans.get("transId"));
      String emulation = String.valueOf(trans.get("emulation"));

      if (i == 0) {
        credentials = loadCredentialsForTrans(transId);
        apiRequest = new TridentApiRequest(apiHost);
      }

      boolean approved = false;
      apiRequest.resetParms();
      if (emulation.equals("AUTHNET")) {
        apiRequest.setArg(TridentApiConstants.FN_AN_TID, credentials[0]);
        apiRequest.setArg(TridentApiConstants.FN_AN_PASSWORD, credentials[1]);
        apiRequest.setArg(TridentApiConstants.FN_AN_VERSION, "3.1");
        apiRequest.setArg(TridentApiConstants.FN_AN_TRAN_TYPE, "prior_auth_capture");
        apiRequest.setArg(TridentApiConstants.FN_AN_TRAN_ID, getTranIdNumber(transId));
        apiRequest.setArg(TridentApiConstants.FN_AN_AMOUNT, amount);
      } else {
        apiRequest.setArg(TridentApiConstants.FN_TID, credentials[0]);
        apiRequest.setArg(TridentApiConstants.FN_TERM_PASS, credentials[1]);
        apiRequest.setArg(TridentApiConstants.FN_TRAN_TYPE, TridentApiTranBase.TT_SETTLE);
        apiRequest.setArg(TridentApiConstants.FN_TRAN_ID, transId);
        apiRequest.setArg(TridentApiConstants.FN_AMOUNT, amount);
        apiRequest.setArg(TridentApiConstants.FN_FX_AMOUNT, fxAmt);
      }

      TridentApiResponse resp = apiRequest.post();

      if (emulation.equals("AUTHNET")) {
        String rawResponse = (String) resp.getParameterList().get(0);
        String items[] = rawResponse.split(",");
        if (items[2].equals(TridentApiConstants.RC_AN_APPROVED)) {
          approved = true;
        }
      } else {
        if (TridentApiConstants.ER_NONE
            .equals(resp.getParameter(TridentApiConstants.FN_ERROR_CODE))) {
          approved = true;
        }
      }

      if (approved) {
        lastTranId = transId;
      }

      JSONObject responseObj = new JSONObject();
      responseObj.put("responseText", resp.getParameter(TridentApiConstants.FN_AUTH_RESP_TEXT));
      responseObj.put("transId", resp.getParameter(TridentApiConstants.FN_TRAN_ID));
      responseObj.put("responseCode", resp.getParameter(TridentApiConstants.FN_ERROR_CODE));
      responseArray.add(responseObj);
    }

    if (lastTranId != null) {
      ApiDb.waitForUpdate(lastTranId, TridentApiTranBase.TT_DEBIT, 120000L);
      log.debug("waitForUpdate tranId: " + lastTranId);
    }

    return responseArray.toString();
  }

  public String storeData(long orgId) {
    String response = "";
    String messageType = HttpHelper.getString(request, "messageType", "");
    if ("transaction".equals(messageType)) {
      String action = HttpHelper.getString(request, "action", "");
      if ("reject".equals(action)) {
        response = rejectTransactions();
      } else if ("accept".equals(action)) {
        response = acceptTransactions();
      } else {
        response = "Invalid action type!";
      }
    }

    return response;
  }

  protected void createFields(HttpServletRequest request) {
    FieldGroup fgroup;
    Field field;

    super.createFields(request);

    fgroup = (FieldGroup) getField("searchFields");

    if (HttpHelper.getBoolean(request, "ajax", false)) {
      fgroup.add(new HiddenField("tridentTranId"));
      fgroup.add(new HiddenField("emulation"));

      if (HttpHelper.getString(request, "btnReauth", "").equals("reauth")) {
        fgroup.add(new HiddenField("reauthAmount"));
      } else if (HttpHelper.getString(request, "btnCapture", "").equals("capture")) {
        fgroup.add(new HiddenField("capAmount"));
      } else if (HttpHelper.getString(request, "btnSettle", "").equals("settleGroup")) {
        fgroup.add(new HiddenField("settleAmount"));
        fgroup.add(new HiddenField("settleFxAmount"));
      } else // full request, use a currency field
      {
        fgroup.add(new CurrencyField("settleAmount", "Settle Amount", 12, 12, true));
        fgroup.add(new CurrencyField("settleFxAmount", "Settle Fx Amount", 12, 12, true));
      }
    } else // add a drop down for the user to use
    {
      fgroup.add(new DropDownField("mode", "Mode", new ModeTable(), true));
    }

    fgroup.add(new CheckboxField("hideTestItems", "Hide Test Transactions", false));
    fgroup.add(new ButtonField("btnSettle", "save"));
    fgroup.add(new ButtonField("btnVoid", "void"));
    fgroup.add(new ButtonField("btnReauth", "reauth"));
    fgroup.add(new ButtonField("btnCapture", "settle"));
    fgroup.add(new DropDownField("searchCriteria", "Search Criteria (optional)",
        new SearchOptionsTable(), true));
    fgroup.add(new Field("searchValue", "Search Value (optional)", 22, 0, true));
    fgroup.add(new ButtonField("btnRefresh", "Refresh"));
    fgroup.add(new HiddenField("productCode", "ALL"));
    fgroup.add(new HiddenField("tips_allowed", "N"));
    fgroup.add(new HiddenField("heldItemsOnly", "N"));
    // @
    // getField("btnRefresh").addHtmlExtra("onclick=\"return(loadPreAuths())\"");
  }

  protected void encodeHeaderCSV(StringBuffer line) {
    line.setLength(0);
    line.append("\"Tran ID\",");
    line.append("\"Tran Date\",");
    line.append("\"Card Type\",");
    line.append("\"Card Number\",");
    line.append("\"Reference Number\",");
    line.append("\"Invoice Number\",");
    line.append("\"Auth Code\",");
    line.append("\"Status\",");
    line.append("\"Currency Code\",");
    line.append("\"Amount\"");
    if (hasFxData()) {
      line.append(",\"USD Amount\"");
    }
  }

  protected void encodeLineCSV(Object obj, StringBuffer line) {
    RowData record = (RowData) obj;

    // clear the line buffer and add
    // the merchant specific data.
    line.setLength(0);
    line.append(record.getTridentTranId());
    line.append(",");
    line.append(DateTimeFormatter.getFormattedDate(record.getTranDate(), "MM/dd/yyyy"));
    line.append(",");
    line.append(record.getCardType());
    line.append(",");
    line.append(record.getCardNumber());
    line.append(",");
    line.append(record.getReferenceNumber());
    line.append(",");
    line.append(record.getPurchaseId());
    line.append(",");
    line.append(record.getAuthCode());
    line.append(",");
    line.append(record.getTranType());
    line.append(",\"");
    line.append(record.getCurrencyCodeAlpha());
    line.append("\",");
    line.append(record.getTranAmount());
    if (hasFxData()) {
      line.append(",");
      line.append(record.getFxAmount());
    }
  }

  // **************************************************************************
  // overload the base class method to allow for an
  // array of arrays (one per currency) to be returned
  // **************************************************************************
  public String encodeDataAsJSON() {
    JSONArray currencyArray = null;
    JSONArray jsonArray = new JSONArray();
    String lastCC = "";
    Vector rows = getReportRows();
    String messsageType = HttpHelper.getString(request, "messageType", "");

    if ("dashboard".equals(messsageType)) {
      for (int i = 0; i < rows.size(); ++i) {
        JSONObject jsonObj = encodeJSONDashboard(ReportRows.elementAt(i));
        if (jsonObj != null) {
          jsonArray.add(jsonObj);
        }
      }
      return jsonArray.toString();
    }

    for (int i = 0; i < rows.size(); ++i) {
      RowData row = (RowData) rows.elementAt(i);
      String cc = row.getCurrencyCodeAlpha();

      if (!lastCC.equals(cc)) {
        if (currencyArray != null) {
          jsonArray.add(currencyArray);
        }
        currencyArray = new JSONArray();
        lastCC = cc;
      }
      JSONObject jsonObj = encodeJSON(row);
      if (jsonObj != null) {
        currencyArray.add(jsonObj);
      }
    }
    if (currencyArray != null) // boundary condition
    {
      jsonArray.add(currencyArray);
    }
    return (jsonArray.toString());
  }

  private JSONObject encodeJSONDashboard(Object obj) {
    JSONObject response = new JSONObject();
    createDashboardDataJsonObj(obj, response);
    return response;
  }

  private void createDashboardDataJsonObj(Object obj, JSONObject response) {
    DashBoardTpgTotals dt = (DashBoardTpgTotals) obj;
    response.put("preAuthAmount", dt.getPreAuthAmount());
    response.put("preAuthCount", dt.getPreAuthCount());
    response.put("pendingSettlementAmount", dt.getPendingSettlementAmount());
    response.put("pendingSettlementCount", dt.getPendingSettlementCount());
    response.put("lastSettledBatchAmount", dt.getLastSettledBatchAmount());
    response.put("lastSettledBatchCount", dt.getLastSettledBatchCount());
    response.put("lastSettlementDate", dt.getLastSettlementDate());

  }

  protected JSONObject encodeJSON(Object obj) {
    RowData record = (RowData) obj;
    JSONObject response = new JSONObject();

    response.put("transId", record.getTridentTranId());
    response.put("date", DateTimeFormatter.getFormattedDate(record.getTranDate(), "MM/dd/yyyy"));
    response.put("type", record.getTranTypeDesc());
    response.put("dba", record.getDbaName());
    response.put("purchaseId", record.getPurchaseId());
    response.put("clientRef", record.getClientReferenceNumber());
    response.put("cardNum", record.getCardNumber());
    response.put("authCode", record.getAuthCode());
    response.put("entry", record.getPosEntryMode());
    response.put("amount", String.valueOf(record.getTranAmount()));
    response.put("currType", record.getCurrencyCodeAlpha());
    response.put("emulation", record.getEmulationString());

    if (hasFxData()) {
      response.put("fxAmt", String.valueOf(record.getFxAmount()));
    }
    return (response);
  }

  public void encodeNodeUrl(StringBuffer buffer, long nodeId, Date beginDate, Date endDate) {
    super.encodeNodeUrl(buffer, nodeId, beginDate, endDate);
    buffer.append("&hideTestItems=");
    buffer.append(getData("hideTestItems"));
  }

  public String getDownloadFilenameBase() {
    StringBuffer filename = new StringBuffer("");

    filename.append("tpg_pre_auths");
    return (filename.toString());
  }

  public TpgTotals getPendingSettlementTotals()
  {
    ResultSetIterator   it        = null;
    long                nodeId    = getReportHierarchyNode();
    ResultSet           resultSet = null;
    TpgTotals           retVal    = new TpgTotals();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:692^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (tapi idx_trident_capture_api) */      
//                  count(tapi.merchant_number)                         as item_count,
//                  sum(decode(tapi.currency_code,'840',tapi.transaction_amount,nvl(tapi.fx_amount_base,0))
//                      * decode(tapi.debit_credit_indicator,'C',-1,1)) as item_amount
//          from    trident_capture_api             tapi,
//                  trident_capture_api_tran_type   tt
//          where   tapi.merchant_number = :nodeId
//                  and tapi.transaction_date between trunc(sysdate-15) and trunc(sysdate)
//                  and tapi.mesdb_timestamp >= (sysdate-45) 
//                  and tapi.batch_id is null 
//                  and tapi.test_flag = 'N' 
//                  and tt.tran_type = tapi.transaction_type 
//                  and nvl(tt.settle,'N') = 'Y'      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (tapi idx_trident_capture_api) */      \n                count(tapi.merchant_number)                         as item_count,\n                sum(decode(tapi.currency_code,'840',tapi.transaction_amount,nvl(tapi.fx_amount_base,0))\n                    * decode(tapi.debit_credit_indicator,'C',-1,1)) as item_amount\n        from    trident_capture_api             tapi,\n                trident_capture_api_tran_type   tt\n        where   tapi.merchant_number =  :1 \n                and tapi.transaction_date between trunc(sysdate-15) and trunc(sysdate)\n                and tapi.mesdb_timestamp >= (sysdate-45) \n                and tapi.batch_id is null \n                and tapi.test_flag = 'N' \n                and tt.tran_type = tapi.transaction_type \n                and nvl(tt.settle,'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tpg.TpgPreAuthDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tpg.TpgPreAuthDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:707^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        retVal.setTotals(resultSet.getInt("item_count"),
                         resultSet.getDouble("item_amount"));
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("getPendingSettlementTotals()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
    return( retVal );
  }

  public TpgTotals getPreAuthTotals()
  {
    ResultSetIterator   it        = null;
    long                nodeId    = getReportHierarchyNode();
    ResultSet           resultSet = null;
    TpgTotals           retVal    = new TpgTotals();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:736^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index(tapi idx_trident_capture_api) */      
//                  count(tapi.merchant_number)                         as item_count,
//                  sum(decode(tapi.currency_code,'840',tapi.transaction_amount,nvl(tapi.fx_amount_base,0)) 
//                      * decode(tapi.debit_credit_indicator,'C',-1,1)) as item_amount
//          from    trident_capture_api   tapi
//          where   tapi.merchant_number = :nodeId
//                  and tapi.transaction_date between trunc(sysdate-30) and trunc(sysdate)
//                  and tapi.mesdb_timestamp >= (sysdate-45)
//                  and tapi.transaction_type = 'P' 
//                  and tapi.test_flag = 'N'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index(tapi idx_trident_capture_api) */      \n                count(tapi.merchant_number)                         as item_count,\n                sum(decode(tapi.currency_code,'840',tapi.transaction_amount,nvl(tapi.fx_amount_base,0)) \n                    * decode(tapi.debit_credit_indicator,'C',-1,1)) as item_amount\n        from    trident_capture_api   tapi\n        where   tapi.merchant_number =  :1 \n                and tapi.transaction_date between trunc(sysdate-30) and trunc(sysdate)\n                and tapi.mesdb_timestamp >= (sysdate-45)\n                and tapi.transaction_type = 'P' \n                and tapi.test_flag = 'N'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tpg.TpgPreAuthDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.tpg.TpgPreAuthDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:748^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        retVal.setTotals(resultSet.getInt("item_count"),
                         resultSet.getDouble("item_amount"));
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("getPreAuthTotals()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
    return( retVal );
  }

  public TpgTotals getLastSettledBatchTotals()
  {
    ResultSetIterator   it        = null;
    long                nodeId    = getReportHierarchyNode();
    ResultSet           resultSet = null;
    TpgTotals           retVal    = new TpgTotals();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:777^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tpa.terminal_id , mb.total_count as item_count, 
//            mb.net_amount as item_amount, mb.merchant_batch_date as batch_date
//    from    trident_profile tp, mbs_batches mb, trident_profile_api tpa ,organization  o , group_merchant   gm
//    where   o.org_group = :nodeId
//    and  gm.org_num = o.org_num
//            and tpa.terminal_id = tp.terminal_id
//            and tp.merchant_number = gm.merchant_number
//            and mb.merchant_number = tp.merchant_number
//            and mb.merchant_batch_date = tpa.last_batch_date
//            and mb.mbs_batch_type =2
//            and mb.batch_number = tpa.last_batch_number        
//            order by terminal_id desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tpa.terminal_id , mb.total_count as item_count, \n          mb.net_amount as item_amount, mb.merchant_batch_date as batch_date\n  from    trident_profile tp, mbs_batches mb, trident_profile_api tpa ,organization  o , group_merchant   gm\n  where   o.org_group =  :1 \n  and  gm.org_num = o.org_num\n          and tpa.terminal_id = tp.terminal_id\n          and tp.merchant_number = gm.merchant_number\n          and mb.merchant_number = tp.merchant_number\n          and mb.merchant_batch_date = tpa.last_batch_date\n          and mb.mbs_batch_type =2\n          and mb.batch_number = tpa.last_batch_number        \n          order by terminal_id desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tpg.TpgPreAuthDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.tpg.TpgPreAuthDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:791^7*/
      resultSet = it.getResultSet();
      if ( resultSet.next() )
      {
        retVal.setTotals(resultSet.getInt("item_count"),
                         resultSet.getDouble("item_amount"));
        retVal.setBatchDate(resultSet.getDate("batch_date").toString());
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("getPreAuthTotals()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
    return( retVal );
  }

  public String getTranIdNumber(String tridentTranId )
  {
    String retVal = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:818^7*/

//  ************************************************************
//  #sql [Ctx] { select  trident_tran_id_number
//          
//          from    trident_capture_api
//          where   trident_tran_id = :tridentTranId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trident_tran_id_number\n         \n        from    trident_capture_api\n        where   trident_tran_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tpg.TpgPreAuthDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tridentTranId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:824^7*/
    }
    catch( Exception e )
    {
      logEntry( "getTridentTranIdNumber()",e.toString());
    }
    
    return( retVal );
  }

  public boolean isEncodingSupported(int fileFormat) {
    boolean retVal = false;
    switch (fileFormat) {
    case FF_CSV:
      retVal = true;
      break;
    }
    return (retVal);
  }

  public void loadData() {
    String messageType = HttpHelper.getString(request, "messageType", "");

    if ("dashboard".equals(messageType)) {
      loadDashboardData();
    } else {
      loadData(getReportHierarchyNode(), getReportDateBegin(), getReportDateEnd());
    }

  }

  /*
   * private void loadDashboardData() { HashMap<String, TpgTotals> map = new
   * HashMap<String, TpgTotals>();
   * 
   * TpgTotals preAuthTotals = getPreAuthTotals(); if (preAuthTotals != null) {
   * map.put("preAuthTotals", preAuthTotals);
   * 
   * }
   * 
   * TpgTotals pendingSettlementTotals = getPendingSettlementTotals(); if
   * (pendingSettlementTotals != null) { map.put("pendingSettlementTotals",
   * pendingSettlementTotals); }
   * 
   * ReportRows.addElement(map); // TODO - get last settled batch total
   * 
   * }
   */

  private void loadDashboardData() {
    DashBoardTpgTotals dt = new DashBoardTpgTotals();
    TpgTotals preAuthTotals = getPreAuthTotals();
    if (preAuthTotals != null) {
      dt.setPreAuthAmount(preAuthTotals.getAmount());
      dt.setPreAuthCount(preAuthTotals.getCount());
    }

    TpgTotals pendingSettlementTotals = getPendingSettlementTotals();

    if (pendingSettlementTotals != null) {
      dt.setPendingSettlementAmount(pendingSettlementTotals.getAmount());
      dt.setPendingSettlementCount(pendingSettlementTotals.getCount());
    }

    TpgTotals lastSettledBatchTotals = getLastSettledBatchTotals();

    if (lastSettledBatchTotals != null) {
      dt.setLastSettledBatchAmount(lastSettledBatchTotals.getAmount());
      dt.setLastSettledBatchCount(lastSettledBatchTotals.getCount());
      dt.setLastSettlementDate(lastSettledBatchTotals.getBatchDate());

    }
    
    ReportRows.add(dt);

  }

  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
    String              cardNumber        = null;
    double              fxAmount          = 0.0;
    String              heldWhereClause   = null;
    String              invoiceNumber     = null;
    ResultSetIterator   it                = null;
    String              loadFilename      = null;
    String              productCode       = getData("productCode");
    int                 profileCount      = 0;
    String              profileId         = getData("profile_id");
    long                reportNode        = getReportHierarchyNodeDefault();
    ResultSet           resultSet         = null;
    String              showTest          = null;
    String              searchCriteria    = getData("searchCriteria");
    String              searchValue       = getData("searchValue").trim();
    String              tranAmount        = null;

    if ( searchValue.equals("") )
    {
      cardNumber = "passall";
    }
    else
    {
      if (searchCriteria.equals("CN"))
      {
        cardNumber = searchValue;
      }
      else if (searchCriteria.equals("IN"))
      {
        invoiceNumber = searchValue;
      }
      else if (searchCriteria.equals("TA"))
      {
        tranAmount = searchValue;
      }
    }

    try
    {
      if ( getData("heldItemsOnly").toUpperCase().equals("Y") ||
           getData("mode").toUpperCase().equals("HL") )
      {
        heldWhereClause = " and nvl(tapi.load_filename,'none') like 'hold-fraud%' ";
      }
      else if ( TridentTranId != null )
      {
        heldWhereClause = "";   // no filter when loading a specific tran
      }
      else
      {
        heldWhereClause = " and nvl(tapi.load_filename,'none') not like 'hold-fraud%' ";
      }
      
      if ( getData("hideTestItems").toUpperCase().equals("Y") )
      {
        showTest = "N";
      }
      else
      {
        showTest = "Y";
      }
      ReportRows.removeAllElements();
      
      // validate that this profile exists under the 
      // current report hierarchy node
      /*@lineinfo:generated-code*//*@lineinfo:967^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(tp.terminal_id) 
//          from    organization        o,
//                  group_merchant      gm,
//                  trident_profile     tp
//          where   o.org_group = :reportNode 
//                  and gm.org_num = o.org_num 
//                  and tp.merchant_number = gm.merchant_number 
//                  and tp.terminal_id = :profileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(tp.terminal_id)  \n        from    organization        o,\n                group_merchant      gm,\n                trident_profile     tp\n        where   o.org_group =  :1  \n                and gm.org_num = o.org_num \n                and tp.merchant_number = gm.merchant_number \n                and tp.terminal_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tpg.TpgPreAuthDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,reportNode);
   __sJT_st.setString(2,profileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   profileCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:977^7*/
      
      if ( profileCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:981^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                        use_nl(tapi)
//                     */
//                    tapi.terminal_id                as terminal_id,
//                    tapi.trident_tran_id            as trident_tran_id,
//                    tapi.trident_tran_id_number     as trident_tran_id_num,
//                    tapi.external_tran_id           as external_tran_id,
//                    tapi.dba_name                   as dba_name,
//                    tapi.dba_city                   as dba_city,
//                    tapi.dba_state                  as dba_state,
//                    tapi.dba_zip                    as dba_zip,
//                    tapi.debit_credit_indicator     as debit_credit_ind,
//                    tapi.sic_code                   as sic_code,
//                    tapi.phone_number               as phone_number,
//                    tapi.card_number                as card_number,
//                    tapi.batch_date                 as batch_date,
//                    tapi.transaction_date           as tran_date,
//                    tapi.transaction_amount         as tran_amount,
//                    tapi.auth_amount                as auth_amount,
//                    tapi.auth_returned_aci          as returned_aci,
//                    tapi.auth_code                  as auth_code,
//                    tapi.reference_number           as reference_number,
//                    tapi.purchase_id                as purchase_id,
//                    tapi.transaction_type           as tran_type,
//                    tapi.merchant_number            as merchant_number,
//                    tapi.card_type                  as card_type,
//                    nvl(emd.pos_entry_desc_short,
//                        tapi.pos_entry_mode)        as pos_entry_mode,
//                    tapi.currency_code              as currency_code,
//                    tapi.debit_network_id           as debit_network_id,
//                    tapi.avs_zip                    as avs_zip,
//                    decode( nvl(tapi.emulation,'TPG'),
//                            'AUTHNET',1,  /* EMU_AUTHORIZE_NET */
//                            0 )  /* EMU_TPG */      as emulation,
//                    tapi.fx_rate_id                 as fx_rate_id,
//                    tapi.fx_amount_base             as fx_amount,
//                    tapi.fx_rate                    as fx_rate,
//                    tapi.fx_reference_number        as fx_ref_num,
//                    nvl(tapi.test_flag,'Y')         as test_flag,
//                    tapi.fraud_result               as fraud_result,
//                    tapi.card_number_enc            as card_number_enc,
//                    tapi.exp_date                   as exp_date
//            from    trident_capture_api             tapi,
//                    trident_profile                 tp,
//                    pos_entry_mode_desc             emd,
//                    bml_api_auths                   ba
//            where   tapi.mesdb_timestamp >= (sysdate-45)
//                    and tapi.terminal_id = :profileId
//                    and tapi.transaction_date >= (sysdate-30)
//                    and tapi.transaction_type in ( 'P','V','D','C','O' )
//                    and 
//                    ( 
//                      tapi.transaction_type != 'V' 
//                      or trunc(tapi.last_modified_date) = trunc(sysdate)
//                    ) 
//                    and tapi.batch_id is null   -- not settled
//                    and nvl(:TridentTranId,'ALL') in ('ALL', tapi.trident_tran_id)
//                    and nvl(tapi.test_flag,'Y') in (:showTest,'N')
//                    and
//                    (
//                      'passall' = :cardNumber 
//                      or tapi.purchase_id = :invoiceNumber 
//                      or tapi.client_reference_number = :invoiceNumber 
//                      or tapi.transaction_amount = :tranAmount 
//                      or substr(tapi.card_number,-4) = :cardNumber
//                    ) 
//                    and tp.terminal_id = tapi.terminal_id  
//                    and nvl(:productCode,'ALL') in ( 'ALL', tp.product_code )
//                    and emd.pos_entry_code(+) = tapi.pos_entry_mode
//                    -- don't show bml pre-auths that are flagged for auto capture
//                    and ba.tran_id(+) = tapi.trident_tran_id and
//                    (
//                      nvl(ba.sale_flag,'N') = 'N' or
//                      tapi.transaction_type <> 'P'
//                    )
//                    :heldWhereClause
//            order by emulation, tapi.mesdb_timestamp desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  /*+\n                      use_nl(tapi)\n                   */\n                  tapi.terminal_id                as terminal_id,\n                  tapi.trident_tran_id            as trident_tran_id,\n                  tapi.trident_tran_id_number     as trident_tran_id_num,\n                  tapi.external_tran_id           as external_tran_id,\n                  tapi.dba_name                   as dba_name,\n                  tapi.dba_city                   as dba_city,\n                  tapi.dba_state                  as dba_state,\n                  tapi.dba_zip                    as dba_zip,\n                  tapi.debit_credit_indicator     as debit_credit_ind,\n                  tapi.sic_code                   as sic_code,\n                  tapi.phone_number               as phone_number,\n                  tapi.card_number                as card_number,\n                  tapi.batch_date                 as batch_date,\n                  tapi.transaction_date           as tran_date,\n                  tapi.transaction_amount         as tran_amount,\n                  tapi.auth_amount                as auth_amount,\n                  tapi.auth_returned_aci          as returned_aci,\n                  tapi.auth_code                  as auth_code,\n                  tapi.reference_number           as reference_number,\n                  tapi.purchase_id                as purchase_id,\n                  tapi.transaction_type           as tran_type,\n                  tapi.merchant_number            as merchant_number,\n                  tapi.card_type                  as card_type,\n                  nvl(emd.pos_entry_desc_short,\n                      tapi.pos_entry_mode)        as pos_entry_mode,\n                  tapi.currency_code              as currency_code,\n                  tapi.debit_network_id           as debit_network_id,\n                  tapi.avs_zip                    as avs_zip,\n                  decode( nvl(tapi.emulation,'TPG'),\n                          'AUTHNET',1,  /* EMU_AUTHORIZE_NET */\n                          0 )  /* EMU_TPG */      as emulation,\n                  tapi.fx_rate_id                 as fx_rate_id,\n                  tapi.fx_amount_base             as fx_amount,\n                  tapi.fx_rate                    as fx_rate,\n                  tapi.fx_reference_number        as fx_ref_num,\n                  nvl(tapi.test_flag,'Y')         as test_flag,\n                  tapi.fraud_result               as fraud_result,\n                  tapi.card_number_enc            as card_number_enc,\n                  tapi.exp_date                   as exp_date\n          from    trident_capture_api             tapi,\n                  trident_profile                 tp,\n                  pos_entry_mode_desc             emd,\n                  bml_api_auths                   ba\n          where   tapi.mesdb_timestamp >= (sysdate-45)\n                  and tapi.terminal_id =  ? \n                  and tapi.transaction_date >= (sysdate-30)\n                  and tapi.transaction_type in ( 'P','V','D','C','O' )\n                  and \n                  ( \n                    tapi.transaction_type != 'V' \n                    or trunc(tapi.last_modified_date) = trunc(sysdate)\n                  ) \n                  and tapi.batch_id is null   -- not settled\n                  and nvl( ? ,'ALL') in ('ALL', tapi.trident_tran_id)\n                  and nvl(tapi.test_flag,'Y') in ( ? ,'N')\n                  and\n                  (\n                    'passall' =  ?  \n                    or tapi.purchase_id =  ?  \n                    or tapi.client_reference_number =  ?  \n                    or tapi.transaction_amount =  ?  \n                    or substr(tapi.card_number,-4) =  ? \n                  ) \n                  and tp.terminal_id = tapi.terminal_id  \n                  and nvl( ? ,'ALL') in ( 'ALL', tp.product_code )\n                  and emd.pos_entry_code(+) = tapi.pos_entry_mode\n                  -- don't show bml pre-auths that are flagged for auto capture\n                  and ba.tran_id(+) = tapi.trident_tran_id and\n                  (\n                    nvl(ba.sale_flag,'N') = 'N' or\n                    tapi.transaction_type <> 'P'\n                  )\n                   ");
   __sjT_sb.append(heldWhereClause);
   __sjT_sb.append(" \n          order by emulation, tapi.mesdb_timestamp desc");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "5com.mes.tpg.TpgPreAuthDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,profileId);
   __sJT_st.setString(2,TridentTranId);
   __sJT_st.setString(3,showTest);
   __sJT_st.setString(4,cardNumber);
   __sJT_st.setString(5,invoiceNumber);
   __sJT_st.setString(6,invoiceNumber);
   __sJT_st.setString(7,tranAmount);
   __sJT_st.setString(8,cardNumber);
   __sJT_st.setString(9,productCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1060^9*/
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          ReportRows.addElement( new RowData( resultSet ) );
          fxAmount += resultSet.getDouble("fx_amount");
        }
        resultSet.close();
        it.close();
      }
      
      setFxDataIncluded( (fxAmount == 0.0) ? false : true );
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }

  public RowData loadTransaction() {
    return (loadTransaction(getData("tridentTranId")));
  }

  public RowData loadTransaction( String tranId )
  {
    RowData             retVal        = null;
    
    try
    {
      TridentTranId = tranId;
      if ( !isBlank(TridentTranId) )
      {
        String tid = null;
        /*@lineinfo:generated-code*//*@lineinfo:1098^9*/

//  ************************************************************
//  #sql [Ctx] { select  terminal_id
//            
//            from    trident_capture_api tapi
//            where   tapi.trident_tran_id = :tranId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  terminal_id\n           \n          from    trident_capture_api tapi\n          where   tapi.trident_tran_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.tpg.TpgPreAuthDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tranId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   tid = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1104^9*/
        setData("profile_id",tid);
        
        loadData();   // load the data from Oracle
      }      
      if ( ReportRows.size() > 0 )
      {
        retVal = (RowData)ReportRows.elementAt(0);
      
        String serverName = ServletRequest.getServerName();
        if( serverName.indexOf("www.merchante-solutions.com") >= 0 ||
            serverName.indexOf("ops.merchante-solutions.com") >= 0 ||
            isProductionRequest() )
        {
          retVal.setAuthorizationHost( TridentApiConstants.DESKTOP_HOST_PRODUCTION );
        }
        else  // default to the auth simulator
        {
          retVal.setAuthorizationHost( TridentApiConstants.DESKTOP_HOST_SIMULATOR );
        }
        retVal.setProfile( loadProfile() );
      }
      TridentTranId = null;   // clear the specific id
    }
    catch( Exception e )
    {
      logEntry("loadTransaction(" + tranId + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }

  protected void postHandleRequest(HttpServletRequest request) {
    setData("tips_allowed", tipsAllowed());
  }

  protected String tipsAllowed()
  {
    String    tid     = getData(TridentApiConstants.FN_TID);
    String    value   = "N";

    if( tid != null && !tid.equals("") )
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1151^9*/

//  ************************************************************
//  #sql [Ctx] { select  upper(nvl(sc.tips_allowed, 'N')) 
//            from    sic_codes        sc,
//                    trident_profile  tp
//            where   tp.terminal_id = :tid and
//                    sc.sic_code = tp.sic_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  upper(nvl(sc.tips_allowed, 'N'))  \n          from    sic_codes        sc,\n                  trident_profile  tp\n          where   tp.terminal_id =  :1  and\n                  sc.sic_code = tp.sic_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.tpg.TpgPreAuthDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   value = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1158^9*/
      
        if ( value == null )
        {
          value = "N";
        }
      }
      catch( Exception e )
      {
        logEntry("tipsAllowed()",e.toString());
      }      
    }
    return( value );
  }
}/*@lineinfo:generated-code*/