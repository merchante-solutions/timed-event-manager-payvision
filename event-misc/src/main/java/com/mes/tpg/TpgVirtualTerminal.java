/*@lineinfo:filename=TpgVirtualTerminal*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tpg/TpgVirtualTerminal.sqlj $

  Description:  


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tpg;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.safehaus.uuid.UUID;
import org.safehaus.uuid.UUIDGenerator;
import com.mes.api.RefundTransactionTask;
import com.mes.api.StoreTransactionTask;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiTranBase;
import com.mes.api.TridentApiTransaction;
import com.mes.constants.MesMenus;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.Condition;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.PasswordField;
import com.mes.forms.PhoneField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.forms.ZipField;
import com.mes.support.HttpHelper;
import com.mes.support.TridentTools;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class TpgVirtualTerminal extends TpgBaseDataBean
{
  public static final int     VT_MINI_MENU          = 2;
  
  public static class TranTypeTable extends DropDownTable
  {
    public TranTypeTable(String profileId, int menuType)
    {
      String apiCreditAllowed = null;
      
      if (menuType == 2)
      {
        addElement(TridentApiTranBase.TT_DEBIT,   "Sale");
        addElement(TridentApiTranBase.TT_CREDIT,  "Credit");
        addElement(TridentApiTranBase.TT_FORCE,   "Offline");
      }
      else
      {
        addElement(TridentApiTranBase.TT_DEBIT,   "Sale");
        addElement(TridentApiTranBase.TT_PRE_AUTH,"Pre-Auth");
        addElement(TridentApiTranBase.TT_REFUND,  "Refund");
        addElement(TridentApiTranBase.TT_FORCE,   "Offline");
        
        if (profileId != null && profileId != "")
        {
          try
          {
            connect();

            /*@lineinfo:generated-code*//*@lineinfo:104^13*/

//  ************************************************************
//  #sql [Ctx] { select nvl (upper(tp.api_credits_allowed),'N')     
//                from   trident_profile     tp
//                where  tp.terminal_id = :profileId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl (upper(tp.api_credits_allowed),'N')      \n              from   trident_profile     tp\n              where  tp.terminal_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tpg.TpgVirtualTerminal",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,profileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   apiCreditAllowed = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:109^13*/
            if (apiCreditAllowed.equals("Y") || apiCreditAllowed.equals("B") )
            {
              addElement(TridentApiTranBase.TT_CREDIT, "Credit");
            }
          }
          catch (Exception e)
          {
            logEntry("TranTypeTable()",e.toString());
          }
          finally
          {
            cleanUp();
          }
        }     	
      }
    }
  }

  public static class MOTOEcommIndTable extends DropDownTable
  {
    public MOTOEcommIndTable()
    {
      addElement(TridentApiConstants.FV_MOTO_ONE_TIME_MOTO, "Mail/Phone Order");
      addElement(TridentApiConstants.FV_NOT_A_MOTO, "Card Present");
    }
  }
  
  private class NotRefundCondition
    implements Condition
  {
    Field     TranType    = null;
    
    public NotRefundCondition( Field tranTypeField )
    {
      TranType = tranTypeField;  
    }
    
    public boolean isMet() 
    { 
      boolean     retVal    = true;
      
      if ( TranType != null )
      {
        retVal = !TranType.getData().equals(TridentApiTranBase.TT_REFUND);
      }
      return( retVal );
    }
  }
  
  private class AvsRequiredCondition
    implements Condition
  {
    Field   TranType       = null;
  
    public AvsRequiredCondition( Field tranTypeField )
    {
      TranType = tranTypeField;
    }
    
    public boolean isMet() 
    { 
      boolean     retVal    = false;
      if ( TranType.getData().equals(TridentApiTranBase.TT_DEBIT) || TranType.getData().equals(TridentApiTranBase.TT_PRE_AUTH) ) 
      {
        retVal = isAvsRequired();    	  
      }
      return( retVal );
    }
  }
  
  private class Cvv2RequiredCondition
    implements Condition
  {
    Field   TranType     = null;
    int     MenuType     = 0;
  
    public Cvv2RequiredCondition( Field tranTypeField, int menuType )
    {
      TranType = tranTypeField;
      MenuType = menuType;
    }
    
    public boolean isMet() 
    { 
      boolean     retVal    = false;
      
      if ( (MenuType != VT_MINI_MENU) && (TranType.getData().equals(TridentApiTranBase.TT_DEBIT) || 
           TranType.getData().equals(TridentApiTranBase.TT_PRE_AUTH) ||
           TranType.getData().equals(TridentApiTranBase.TT_FORCE)) )
      {
        retVal = isCvv2Required();
      }
      return( retVal );
    }
  }

  
  private class ForcedTransactionCondition
    implements Condition
  {
    Field     TranType    = null;
    
    public ForcedTransactionCondition( Field tranTypeField )
    {
      TranType = tranTypeField;  
    }
    
    public boolean isMet() 
    { 
      boolean     retVal    = false;
      
      if ( TranType != null )
      {
        retVal = TranType.getData().equals(TridentApiTranBase.TT_FORCE);
      }
      return( retVal );
    }
  }
  
  
  private class MotoEcommCondition
    implements Condition
  {
    Field     TranType    = null;
    int       MenuType    = 0;
  
    public MotoEcommCondition( Field tranTypeField, int menuType )
    {
      TranType = tranTypeField;  
      MenuType = menuType;
    }
  
    public boolean isMet() 
    { 
      boolean     retVal    = true;
    
      if ( TranType != null )
      {
        if ( (MenuType == VT_MINI_MENU) || TranType.getData().equals(TridentApiTranBase.TT_CREDIT) || TranType.getData().equals(TridentApiTranBase.TT_REFUND) )
        {
          retVal = false;      
        }
      }
      return( retVal );
    }
  }

  public static class CardDataValidation implements Validation
  {
    private Field   CardIdField     = null;
    private Field   CardSwipeField  = null;
    private String  ErrorMessage    = null;
    private Field   TranTypeField   = null;

    public CardDataValidation( Field tranType, Field swipeField, Field idField )
    {
      CardIdField    = idField;
      CardSwipeField = swipeField;
      TranTypeField  = tranType;
    }
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate(String fdata)
    {
      ErrorMessage = null;
      
      // if current request is not a refund and not a forced transaction
      // and swipe and id fields are blank, card data is required
      if ( !TranTypeField.getData().equals(TridentApiTranBase.TT_REFUND) && 
           CardSwipeField.isBlank() && CardIdField.isBlank() &&
           (fdata == null || fdata.equals("")) )
      {
        ErrorMessage = "Field required";
      }
      return( ErrorMessage == null );
    }
  }
  
  public static class CardExpDateValidation implements Validation
  {
    private String  ErrorMessage    = null;
    private Field   TranTypeField   = null;

    public CardExpDateValidation( Field tranType)
    {
      TranTypeField  = tranType;
    }
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate(String fdata)
    {
      ErrorMessage = null;
      
      if ( !TranTypeField.getData().equals(TridentApiTranBase.TT_REFUND) && 
           (fdata == null || fdata.equals("")) )
      {
        ErrorMessage = "Field required";
      }
      return( ErrorMessage == null );
    }
  }
  

  
  TridentApiTransaction       ApiTran         = null;
 
  public TpgVirtualTerminal( )
  {
  }
  
  protected boolean autoSubmit( )
  {
    String                  fname       = getAutoSubmitName();
    boolean                 prodServer  = false;
    boolean                 retVal      = true;
    String                  tranType    = getData(TridentApiConstants.FN_TRAN_TYPE);
    UUID                    uuid        = null;
    String                  tranId      = null;
    
    if ( fname.equals("btnSubmit") )
    {
      String serverName = ServletRequest.getServerName();
      String hostName = this.toString() + System.currentTimeMillis();
      uuid = UUIDGenerator.getInstance().generateNameBasedUUID(null, hostName);
      prodServer = HttpHelper.isProdServer(null);
                     
      // build an api transaction
      ApiTran = new TridentApiTransaction( uuid.toString().replaceAll("-", "") );
      ApiTran.setProperties(ServletRequest);
      ApiTran.setProfile( loadProfile() );

      if ( tranType.equals(TridentApiTranBase.TT_REFUND) )
      {
        tranId = ApiTran.getTridentTranId();
        
        if( prodServer )
        {
          ApiTran.setAuthorizationHost( TridentApiConstants.DESKTOP_HOST_PRODUCTION );
        }
        else if ( isProductionRequest() )
        {
          ApiTran.setAuthorizationHost( TridentApiConstants.DESKTOP_HOST_PRODUCTION );
        }
        else  // default to the auth simulator
        {
          ApiTran.setAuthorizationHost( TridentApiConstants.DESKTOP_HOST_SIMULATOR );
        }
        
        ApiTran.setDeveloperId(TridentApiConstants.AUTH_DEVELOPER_ID);
        RefundTransactionTask task = new RefundTransactionTask(ApiTran);
        retVal = task.doTask();
        if ( !retVal )
        {
          ApiTran.setErrorCode(task.getErrorCode());
          ApiTran.setErrorDesc(task.getErrorDesc());
        }

        if ( task.getRefundAction().indexOf("Credit") != 0 )
        {
          ApiTran.setTridentTranId( tranId );          
        }
      }
      else if ( tranType.equals(TridentApiTranBase.TT_FORCE) )
      {
        StoreTransactionTask task = new StoreTransactionTask(ApiTran);
        task.setProdFlagDefault(prodServer);
        retVal = task.doTask();
        if ( !retVal )
        {
          ApiTran.setErrorCode(task.getErrorCode());
          ApiTran.setErrorDesc(task.getErrorDesc());
        }
      }
      else if ( tranType.equals(TridentApiTranBase.TT_CREDIT))
      {
        if ( TridentTools.mod10Check(ApiTran.getCardNumberFull()) )
        {
          StoreTransactionTask task = new StoreTransactionTask(ApiTran);
          task.setProdFlagDefault(prodServer);
          retVal = task.doTask();
          if ( !retVal )
          {
            ApiTran.setErrorCode(task.getErrorCode());
            ApiTran.setErrorDesc(task.getErrorDesc());
          }
        }
        else
        {
          ApiTran.setErrorCode(TridentApiConstants.ER_INVALID_CARD_NUMBER);
          ApiTran.setErrorDesc("Invalid card number");
        }
      }
      else if ( tranType.equals(TridentApiTranBase.TT_DEBIT) ||
                tranType.equals(TridentApiTranBase.TT_PRE_AUTH) )
      {
        if( prodServer )
        {
          ApiTran.setAuthorizationHost( TridentApiConstants.DESKTOP_HOST_PRODUCTION );
        }
        else if ( isProductionRequest() )
        {
          ApiTran.setAuthorizationHost( TridentApiConstants.DESKTOP_HOST_PRODUCTION );
        }
        else  // default to the auth simulator
        {
          ApiTran.setAuthorizationHost( TridentApiConstants.DESKTOP_HOST_SIMULATOR );
        }
        
        ApiTran.setDeveloperId(TridentApiConstants.AUTH_DEVELOPER_ID);
        ApiTran.loadData();   // load any required data from Oracle

        try
        {
          retVal = ApiTran.requestAuthorization();
          
          if ( retVal ) 
          {
            // approved, store into the database
            StoreTransactionTask task = new StoreTransactionTask(ApiTran);
            task.setProdFlagDefault(prodServer);
            retVal = task.doTask();

            if ( !retVal )
            {
              ApiTran.setErrorCode(task.getErrorCode());
              ApiTran.setErrorDesc(task.getErrorDesc());
            }
          }
        }
        catch( java.io.IOException ioe )
        {
          logEntry("autoSubmit()",ioe.toString());
          retVal = false;   // failed to get auth
          ApiTran.setError(TridentApiConstants.ER_INTERNAL_ERROR);
        }
      }
    }
//@    else if ( fname.equals("btnSave") )
//@    {
//@      retVal = saveTransaction();
//@    }
    else if ( fname.equals("btnReset") )
    {
      // do nothing, just let the form reset
      fields.resetDefaults(true);
    }
    return( retVal );
  }

  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    Field         field;
    boolean       valRequired      = true;
    String pId = HttpHelper.getString(request,"profile_id","");
    int menuType = HttpHelper.getInt(request, "menu_type", MesMenus.MENU_ID_TPG_MAIN);
    
    super.createFields(request);
        
    fgroup = (FieldGroup)getField("searchFields");
        
    if ( HttpHelper.getBoolean(request,"ajax",false) )
    {
    }
    else    // add a drop down for the user to use
    {
    }

    fgroup.add( new DropDownField( TridentApiConstants.FN_TRAN_TYPE, "Txn Type", new TranTypeTable(pId, menuType), false) );
    fgroup.add( new Field( TridentApiConstants.FN_TRAN_ID, "Transaction ID", 36, 36, true) );
    fgroup.add( new DropDownField( TridentApiConstants.FN_MOTO_IND, "EC Indicator", new MOTOEcommIndTable(), false) );
    fgroup.add( new PasswordField( TridentApiConstants.FN_CARD_SWIPE, "Card Swipe", 128, 32, true) );
    fgroup.add( new Field( TridentApiConstants.FN_CARD_ID, "Card Store ID", 32, 32, true) );
    fgroup.add( new Field( TridentApiConstants.FN_CARD_NUMBER, "Card Number", 16, 32, true) );
    fgroup.add( new Field( TridentApiConstants.FN_CVV2, "Validation Code", 4, 4, false) );
    fgroup.add( new Field( TridentApiConstants.FN_EXP_DATE, "Exp Date (MMYY)", 4, 4, true) );
    fgroup.add( new Field( TridentApiConstants.FN_AUTH_CODE, "Approval Code", 6, 6, false) );
    fgroup.add( new CurrencyField( TridentApiConstants.FN_AMOUNT, "Amount",12,12,false) );
    fgroup.add( new CurrencyField( TridentApiConstants.FN_TAX, "Tax Amount",12,12,true) );
    fgroup.add( new Field( TridentApiConstants.FN_INV_NUM, "Invoice Number", 17, 20, false) );  
    fgroup.add( new Field( TridentApiConstants.FN_AVS_STREET, "Address", 32, 40, false) );      
    fgroup.add( new Field( "ch_addr_line2", "", 32, 40, true) );
    fgroup.add( new Field( "ch_city", "City", 19, 20, true) );
    fgroup.add( new DropDownField( "ch_state", "State", new StateDropDownTable(), true) );
    fgroup.add( new Field( "ch_first_name", "First Name", 20,20, true) );
    fgroup.add( new Field( "ch_last_name", "Last Name", 20,20, true) );
    fgroup.add( new ZipField( TridentApiConstants.FN_AVS_ZIP, "Zip Code",false) ); 
    fgroup.add( new HiddenField(TridentApiConstants.FN_TRIDENT_TRAN_ID) );
    fgroup.add( new DateField(TridentApiConstants.FN_TRAN_DATE, true) ); 
    fgroup.add( new HiddenField(TridentApiConstants.FN_REF_NUM) );
    fgroup.add( new HiddenField(TridentApiConstants.FN_ORG_TRIDENT_TRAN_ID) );
    fgroup.add( new ButtonField( "btnSubmit", "Submit Transaction" ) );
    fgroup.add( new ButtonField( "btnSave",   "Save" ) );
    // dynamic DBA information
    fgroup.add( new Field( TridentApiConstants.FN_DBA_NAME, "DBA Name", TridentApiConstants.FLM_MERCHANT_NAME, 32, true) );
    fgroup.add( new Field( TridentApiConstants.FN_DBA_CITY, "DBA City", TridentApiConstants.FLM_MERCHANT_CITY, 32, true) );
    fgroup.add( new Field( TridentApiConstants.FN_DBA_STATE, "DBA State", TridentApiConstants.FLM_MERCHANT_STATE, 32, true) );
    fgroup.add( new Field( TridentApiConstants.FN_DBA_ZIP, "DBA Zip", TridentApiConstants.FLM_MERCHANT_ZIP, 32, true) );
    fgroup.add( new NumberField( TridentApiConstants.FN_SIC, "MCC", TridentApiConstants.FLM_SIC_CODE, 6, true, 0) );
    fgroup.add( new PhoneField( TridentApiConstants.FN_PHONE, "DBA Phone", true) );
    fields.getField(TridentApiConstants.FN_PHONE).setHtmlSize(15);
    
    fgroup.add( new CheckboxField( "shipSameAsBillAddr", "Same as billing address",false ) );
    fgroup.add( new Field( "shippingAddrLine1", "Address", 32, 40, true) );
    fgroup.add( new Field( "shippingAddrLine2", "", 32, 40, true) );
    fgroup.add( new Field( "shippingCity", "City", 19, 20, true) );
    fgroup.add( new DropDownField( "shippingState", "State", new StateDropDownTable(), true) );
    fgroup.add( new ZipField( "shippingZip", "Zip Code",true) );
    fgroup.add( new TextareaField( "comments", "Comments", 1024, 6, 32, true) );
    
    Condition condition;
    
    // Amount not required for Refund Tran
    //condition = new NotRefundCondition(fgroup.getField(TridentApiConstants.FN_TRAN_TYPE));
    //fgroup.getField(TridentApiConstants.FN_AMOUNT).setOptionalCondition( condition );

    // EC Ind. not required for Refund and Credit Tran
    condition = new MotoEcommCondition(fgroup.getField(TridentApiConstants.FN_TRAN_TYPE), menuType);
    fgroup.getField(TridentApiConstants.FN_MOTO_IND).setOptionalCondition( condition );

    // Avs required conditional for Sale, Pre-Auth
    condition = new AvsRequiredCondition(fgroup.getField(TridentApiConstants.FN_TRAN_TYPE));
    fgroup.getField(TridentApiConstants.FN_AVS_STREET).setOptionalCondition( condition );
    fgroup.getField(TridentApiConstants.FN_AVS_ZIP).setOptionalCondition( condition ); 
    fgroup.getField(TridentApiConstants.FN_INV_NUM).setOptionalCondition( condition );

    // cvv2 required conditional for Sale, Pre-Auth, Offline
    condition = new Cvv2RequiredCondition(fgroup.getField(TridentApiConstants.FN_TRAN_TYPE), menuType);
    fgroup.getField(TridentApiConstants.FN_CVV2).setOptionalCondition( condition );
    
    //Auth Code required for Offline Tran
    condition = new ForcedTransactionCondition(fgroup.getField(TridentApiConstants.FN_TRAN_TYPE));
    fgroup.getField(TridentApiConstants.FN_AUTH_CODE).setOptionalCondition( condition );

    if ( menuType == VT_MINI_MENU )
    {
      fgroup.getField(TridentApiConstants.FN_AVS_STREET).setNullAllowed(true);
    }

    Validation val;
    val = new CardDataValidation(fgroup.getField(TridentApiConstants.FN_TRAN_TYPE),
        fgroup.getField(TridentApiConstants.FN_CARD_SWIPE),
        fgroup.getField(TridentApiConstants.FN_CARD_ID));
    fgroup.getField(TridentApiConstants.FN_CARD_NUMBER).addValidation(val);
        
    val = new CardExpDateValidation(fgroup.getField(TridentApiConstants.FN_TRAN_TYPE));
    fgroup.getField(TridentApiConstants.FN_EXP_DATE).addValidation(val);
  }
  
  public TridentApiTransaction getApiTran()
  {
    return( ApiTran );
  }
  

  
  public boolean isAvsRequired()
  {
    String    tid     = getData(TridentApiConstants.FN_TID);
    String    value   = "y";
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:580^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(avs_required,'y') 
//          from    trident_profile_api tpa
//          where   tpa.terminal_id(+) = :tid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(avs_required,'y')  \n        from    trident_profile_api tpa\n        where   tpa.terminal_id(+) =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tpg.TpgVirtualTerminal",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   value = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:585^7*/
      
      if ( value == null )
      {
        value = "y";
      }
    }
    catch( Exception e )
    {
      logEntry("isAvsRequired()",e.toString());
    }
    return( value.equals("y") );
  }
  
  public boolean isAvsRequired(String tid)
  {
    String    value   = "y";
    
       try
      {
        /*@lineinfo:generated-code*//*@lineinfo:605^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(avs_required,'y') 
//            from    trident_profile_api tpa
//            where   tpa.terminal_id(+) = :tid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(avs_required,'y')  \n          from    trident_profile_api tpa\n          where   tpa.terminal_id(+) =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tpg.TpgVirtualTerminal",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   value = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:610^9*/
      
        if ( value == null )
        {
          value = "y";
        }
      }
      catch( Exception e )
      {
        logEntry("isAvsRequired(tid)",e.toString());
      }
    return( value.equals("y") );
  }


  public boolean isCvv2Required()
  {
    String    tid     = getData(TridentApiConstants.FN_TID);
    String    value   = "n";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:632^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(cvv2_required,'n') 
//          from    trident_profile_api tpa
//          where   tpa.terminal_id(+) = :tid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(cvv2_required,'n')  \n        from    trident_profile_api tpa\n        where   tpa.terminal_id(+) =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tpg.TpgVirtualTerminal",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   value = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:637^7*/
    
      if ( value == null )
      {
        value = "n";
      }
    }
    catch( Exception e )
    {
      logEntry("isCvv2Required()",e.toString());
    }
    return( value.equals("y") );
  }
  

  public boolean isCvv2Required(String tid)
  {
    String    value   = "n";
    
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:658^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(cvv2_required,'n') 
//            from    trident_profile_api tpa
//            where   tpa.terminal_id(+) = :tid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(cvv2_required,'n')  \n          from    trident_profile_api tpa\n          where   tpa.terminal_id(+) =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tpg.TpgVirtualTerminal",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,tid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   value = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:663^9*/
      
        if ( value == null )
        {
          value = "n";
        }
      }
      catch( Exception e )
      {
        logEntry("isCvv2Required(tid)",e.toString());
      }
    return( value.equals("y") );
  }
  

  
  public boolean isEncodingSupported( int fileFormat )
  {
    return(false);  // no download
  }
  
  public void loadData( )
  {
    loadData( getReportHierarchyNode(), ReportDateBegin, ReportDateEnd );
  }
  
  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator     it              = null;
    long                  reportNode      = getReportHierarchyNodeDefault();
    ResultSet             resultSet       = null;

    try
    {
      String tranId = getData( TridentApiConstants.FN_TRAN_ID );

      /*@lineinfo:generated-code*//*@lineinfo:699^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tapi.terminal_id                as profile_id,
//                  tapi.trident_tran_id            as transaction_id,
//                  tapi.dba_name                   as merchant_name,
//                  tapi.dba_city                   as merchant_city,
//                  tapi.dba_state                  as merchant_state,
//                  tapi.dba_zip                    as merchant_zip,
//                  tapi.sic_code                   as merchant_category_code,
//                  tapi.phone_number               as merchant_phone,
//                  tapi.card_number                as card_number,
//                  tapi.transaction_amount         as transaction_amount,
//                  tapi.transaction_type           as transaction_type,
//                  tapi.auth_code                  as auth_code,
//                  tapi.purchase_id                as invoice_number,
//                  tapi.exp_date                   as card_exp_date,
//                  tapi.avs_zip                    as cardholder_zip,
//                  tapi.tax_amount                 as tax_amount,
//              	tapi.transaction_date           as transaction_date,
//              	tapi.auth_ref_num               as reference_number,
//                  tapi.original_trident_tran_id   as org_trident_tran_id
//          from    organization              o,
//                  group_merchant            gm,
//                  trident_capture_api       tapi
//          where   o.org_group = :reportNode and
//                  gm.org_num = o.org_num and
//                  tapi.merchant_number = gm.merchant_number and
//                  tapi.trident_tran_id = :tranId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tapi.terminal_id                as profile_id,\n                tapi.trident_tran_id            as transaction_id,\n                tapi.dba_name                   as merchant_name,\n                tapi.dba_city                   as merchant_city,\n                tapi.dba_state                  as merchant_state,\n                tapi.dba_zip                    as merchant_zip,\n                tapi.sic_code                   as merchant_category_code,\n                tapi.phone_number               as merchant_phone,\n                tapi.card_number                as card_number,\n                tapi.transaction_amount         as transaction_amount,\n                tapi.transaction_type           as transaction_type,\n                tapi.auth_code                  as auth_code,\n                tapi.purchase_id                as invoice_number,\n                tapi.exp_date                   as card_exp_date,\n                tapi.avs_zip                    as cardholder_zip,\n                tapi.tax_amount                 as tax_amount,\n            \ttapi.transaction_date           as transaction_date,\n            \ttapi.auth_ref_num               as reference_number,\n                tapi.original_trident_tran_id   as org_trident_tran_id\n        from    organization              o,\n                group_merchant            gm,\n                trident_capture_api       tapi\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                tapi.merchant_number = gm.merchant_number and\n                tapi.trident_tran_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tpg.TpgVirtualTerminal",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reportNode);
   __sJT_st.setString(2,tranId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.tpg.TpgVirtualTerminal",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:727^7*/
      resultSet = it.getResultSet();
    
      if( resultSet.next() )
      {
        // set using the data, do not advance, do not remove underscores
        setFields(resultSet,false,false);
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void loadProfileData( String profileId )
  {
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:754^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.merchant_name    as merchant_name,
//                  tp.addr_city        as merchant_city,
//                  tp.addr_state       as merchant_state,
//                  tp.addr_zip         as merchant_zip,
//                  tp.sic_code         as merchant_category_code,
//                  tp.phone_number     as merchant_phone
//          from    trident_profile   tp
//          where   tp.terminal_id = :profileId      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tp.merchant_name    as merchant_name,\n                tp.addr_city        as merchant_city,\n                tp.addr_state       as merchant_state,\n                tp.addr_zip         as merchant_zip,\n                tp.sic_code         as merchant_category_code,\n                tp.phone_number     as merchant_phone\n        from    trident_profile   tp\n        where   tp.terminal_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.tpg.TpgVirtualTerminal",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,profileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.tpg.TpgVirtualTerminal",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:764^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        setFields(resultSet,false,false); // do not advance or remove underscores
      }
    }
    catch( Exception e )
    {
      logEntry("loadProfileData(" + profileId + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ){}
    }
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    loadProfileData(getData("profile_id"));
  }
  
  protected boolean saveTransaction()
  {
    boolean     retVal    = false;
    
    return( retVal );
  }
}/*@lineinfo:generated-code*/