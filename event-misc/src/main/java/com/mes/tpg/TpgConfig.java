/*@lineinfo:filename=TpgConfig*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL$

  Description:  


  Last Modified By   : $Author$
  Last Modified Date : $LastChangedDate$
  Version            : $Revision$

  Change History:
     See SVN database

  Copyright (C) 2007-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tpg;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.api.TridentApiConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.support.HttpHelper;
import com.mes.support.NumberFormatter;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class TpgConfig extends TpgBaseDataBean
{
  public static class BatchCloseTimeTable extends DropDownTable
  {
    public BatchCloseTimeTable( )
    {
      String    hourStr   = null;
      String    minuteStr = null;
      
      hourStr   = NumberFormatter.getPaddedInt(23,2);
      minuteStr = NumberFormatter.getPaddedInt(0,2);
      addElement((hourStr + minuteStr),(hourStr + ":" + minuteStr));
      hourStr   = NumberFormatter.getPaddedInt(23,2);
      minuteStr = NumberFormatter.getPaddedInt(30,2);
      addElement((hourStr + minuteStr),(hourStr + ":" + minuteStr));
      
    
      for( int hour = 0; hour < 23; hour++ )
      {
        for ( int minute = 0; minute <= 30; minute += 30 )
        {
          hourStr   = NumberFormatter.getPaddedInt(hour,2);
          minuteStr = NumberFormatter.getPaddedInt(minute,2);
          addElement((hourStr + minuteStr),(hourStr + ":" + minuteStr));
        }
      }
      addElement("9999","Disabled");
    }
  }
  
  public TpgConfig( )
  {
  }
  
  protected boolean autoSubmit( )
  {
    StringBuffer  buffer      = new StringBuffer();
    String        fname       = getAutoSubmitName();
    String        groupName   = null;
    String        profileId   = getData(TridentApiConstants.FN_TID);
    boolean       retVal      = true;
    String        value       = null;
    String        emailVal    = null;
    
    try
    {
      if ( fname.equals("btnSaveCloseTime") )
      {
        String tmpAddr1 = getData("emailAddr1");
        String tmpAddr2 = getData("emailAddr2");
        buffer.append(tmpAddr1);
        if ((tmpAddr1.length()>0) && (tmpAddr2.length()>0))
        {
          buffer.append(";");
        }
        buffer.append(tmpAddr2);
        emailVal = buffer.toString();
       
        /*@lineinfo:generated-code*//*@lineinfo:118^9*/

//  ************************************************************
//  #sql [Ctx] { update  trident_profile_api
//            set     batch_close_time    = :getData("batchCloseTime"),
//                    avs_required        = :getData("vtRequireAvs"),
//                    cvv2_required       = :getData("vtRequireCvv2"),
//                    email_addresses     = :emailVal,
//                    email_batch_receipt = :getData("emailRequired")
//            where   terminal_id = :profileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1428 = getData("batchCloseTime");
 String __sJT_1429 = getData("vtRequireAvs");
 String __sJT_1430 = getData("vtRequireCvv2");
 String __sJT_1431 = getData("emailRequired");
   String theSqlTS = "update  trident_profile_api\n          set     batch_close_time    =  :1 ,\n                  avs_required        =  :2 ,\n                  cvv2_required       =  :3 ,\n                  email_addresses     =  :4 ,\n                  email_batch_receipt =  :5 \n          where   terminal_id =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.tpg.TpgConfig",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1428);
   __sJT_st.setString(2,__sJT_1429);
   __sJT_st.setString(3,__sJT_1430);
   __sJT_st.setString(4,emailVal);
   __sJT_st.setString(5,__sJT_1431);
   __sJT_st.setString(6,profileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:127^9*/
        
        // no records updated and no exception, try insert
        if ( Ctx.getExecutionContext().getUpdateCount() == 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:132^11*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_profile_api
//              ( 
//                terminal_id, 
//                batch_close_time, 
//                avs_required, 
//                cvv2_required,
//                email_addresses,
//                email_batch_receipt
//              )
//              values
//              ( 
//                :profileId, 
//                :getData("batchCloseTime"),
//                :getData("vtRequireAvs"),
//                :getData("vtRequireCvv2"),
//                :emailVal,
//                :getData("emailRequired")
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1432 = getData("batchCloseTime");
 String __sJT_1433 = getData("vtRequireAvs");
 String __sJT_1434 = getData("vtRequireCvv2");
 String __sJT_1435 = getData("emailRequired");
   String theSqlTS = "insert into trident_profile_api\n            ( \n              terminal_id, \n              batch_close_time, \n              avs_required, \n              cvv2_required,\n              email_addresses,\n              email_batch_receipt\n            )\n            values\n            ( \n               :1 , \n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.tpg.TpgConfig",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,profileId);
   __sJT_st.setString(2,__sJT_1432);
   __sJT_st.setString(3,__sJT_1433);
   __sJT_st.setString(4,__sJT_1434);
   __sJT_st.setString(5,emailVal);
   __sJT_st.setString(6,__sJT_1435);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:152^11*/
        }
      }
      else  // AVS or CVV2 save
      {
        if ( fname.equals("btnSaveAvs") )
        {
          groupName = "avsGroup";
        }
        else if ( fname.equals("btnSaveCvv2") )
        {
          groupName = "cvv2Group";
        }
      
        Vector fields = ((FieldGroup)getField(groupName)).getFieldsVector();
    
        for( int i = 0; i < fields.size(); ++i )
        {
          Field field = (Field)fields.elementAt(i);
          if ( field instanceof CheckboxField )
          {
            if ( ((CheckboxField)field).isChecked() )
            {
              String cbFieldName = field.getName();
              if( buffer.length() > 0 )
              {
                buffer.append(",");
              }
              buffer.append( cbFieldName.substring(cbFieldName.indexOf("_")+1) );
            }
          }
        }
        value = buffer.toString();
    
        if ( fname.equals("btnSaveAvs") )
        {
          /*@lineinfo:generated-code*//*@lineinfo:188^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_profile_api
//              set     avs_results_to_decline = :value
//              where   terminal_id = :profileId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_profile_api\n            set     avs_results_to_decline =  :1 \n            where   terminal_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.tpg.TpgConfig",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,value);
   __sJT_st.setString(2,profileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:193^11*/
        
          // no records updated and no exception, try insert
          if ( Ctx.getExecutionContext().getUpdateCount() == 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:198^13*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_profile_api
//                ( terminal_id, avs_results_to_decline )
//                values
//                ( :profileId, :value )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into trident_profile_api\n              ( terminal_id, avs_results_to_decline )\n              values\n              (  :1 ,  :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.tpg.TpgConfig",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,profileId);
   __sJT_st.setString(2,value);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:204^13*/
          }
        }
        else if ( fname.equals("btnSaveCvv2") )
        {
          /*@lineinfo:generated-code*//*@lineinfo:209^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_profile_api
//              set     cvv2_results_to_decline = :value
//              where   terminal_id = :profileId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_profile_api\n            set     cvv2_results_to_decline =  :1 \n            where   terminal_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.tpg.TpgConfig",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,value);
   __sJT_st.setString(2,profileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:214^11*/
        
          // no records updated and no exception, try insert
          if ( Ctx.getExecutionContext().getUpdateCount() == 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:219^13*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_profile_api
//                ( terminal_id, cvv2_results_to_decline )
//                values
//                ( :profileId, :value )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into trident_profile_api\n              ( terminal_id, cvv2_results_to_decline )\n              values\n              (  :1 ,  :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.tpg.TpgConfig",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,profileId);
   __sJT_st.setString(2,value);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:225^13*/
          }
        }
      }
      retVal = (Ctx.getExecutionContext().getUpdateCount() != 0);
    }
    catch( Exception e )
    {
      logEntry("autoSubmit(" + profileId + ")",e.toString());
    }
    finally
    {
    }
    
    return( retVal );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    Field         field;
    boolean       isChecked = true;
    
    if (HttpHelper.getInt(request, "menu_type", -1) == 2)
    {
      isChecked = false;
    }
    
    super.createFields(request);
    
    getField(TridentApiConstants.FN_TID).makeRequired();
    
    fgroup = new FieldGroup("avsGroup");
    fgroup.add( new CheckboxField("avsResp_B","Street Address match for international transaction.  Postal Code not verified because of incompatible formats. (B)", false) );
    fgroup.add( new CheckboxField("avsResp_E","AVS Error (E)", false) );
    fgroup.add( new CheckboxField("avsResp_G","Non-U.S. Card Issuer (G)", false) );
    fgroup.add( new CheckboxField("avsResp_R","Retry, Issuer system unavailable (R)", false) );
    fgroup.add( new CheckboxField("avsResp_S","AVS not supported by issuer (S)", false) );
    fgroup.add( new CheckboxField("avsResp_U","Address unavailable (U)", false) );
    fgroup.add( new CheckboxField("avsResp_Y","Exact match, 5-character numeric ZIP (Y)", false) );
    fgroup.add( new CheckboxField("avsResp_A","Address match only (A)", false) );
    fgroup.add( new CheckboxField("avsResp_W","9-character numeric ZIP match only (W)", false) );
    fgroup.add( new CheckboxField("avsResp_Z","5-character numeric Zip match only (Z)", false) );
    fgroup.add( new CheckboxField("avsResp_N","No address or ZIP match (N)", false) );
    fields.add(fgroup);
    
    fgroup = new FieldGroup("cvv2Group");
    fgroup.add( new CheckboxField("cvv2Resp_N","Does NOT Match (N)", false) );
    fgroup.add( new CheckboxField("cvv2Resp_P","Is NOT Processed (P)", false) );
    fgroup.add( new CheckboxField("cvv2Resp_S","Should be on card, but is not provided (S)", false) );
    fgroup.add( new CheckboxField("cvv2Resp_U","Issuer is not certified and/or has not provided encryption keys (U)", false) );
    fields.add(fgroup);
    
    fields.add( new DropDownField( "batchCloseTime", "Batch Close Time", new BatchCloseTimeTable(), true) );
    
    fields.add( new CheckboxField("vtRequireAvs","Require AVS data during transaction entry", isChecked) );
    fields.add( new CheckboxField("vtRequireCvv2","Require CVV2 data during transaction entry",false) );
    fields.add( new CheckboxField("emailRequired","Batch summary email notification ",false) );
    
    fields.add( new EmailField("emailAddr1", "Email Address 1", true ) );
    fields.add( new EmailField("emailAddr2", "Email Address 2", true ) );
    
    fields.getField("vtRequireAvs").addHtmlExtra("class=\"formLabelTextBold\"");
    fields.getField("vtRequireCvv2").addHtmlExtra("class=\"formLabelTextBold\"");
    fields.getField("emailRequired").addHtmlExtra("class=\"formLabelTextBold\"");
    
    fields.add( new ButtonField( "btnSaveCloseTime", "Save" ) );
    fields.add( new ButtonField( "btnSaveAvs", "Save" ) );
    fields.add( new ButtonField( "btnSaveCvv2","Save" ) );
  }
  
  public void loadData( )
  {
    loadData( getReportHierarchyNode(), ReportDateBegin, ReportDateEnd );
  }
  
  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
  }
  
  public void loadSettings()
  {
    ResultSetIterator it              = null;
    String            profileId       = getData(TridentApiConstants.FN_TID);
    long              reportNode      = getReportHierarchyNodeDefault();
    ResultSet         resultSet       = null;
    String            tokenSeparator  = ",";

    try
    {
      ReportRows.removeAllElements();
      
      /*@lineinfo:generated-code*//*@lineinfo:317^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tpa.avs_results_to_decline        as avs_results_to_decline,
//                  tpa.cvv2_results_to_decline       as cvv2_results_to_decline,
//                  lpad(tpa.batch_close_time,4,'0')  as batch_close_time,
//                  nvl(tpa.avs_required,'y')         as require_avs,
//                  nvl(tpa.cvv2_required,'n')        as require_cvv2,
//                  tpa.email_addresses               as email_addresses,
//                  nvl(tpa.email_batch_receipt,'n')  as require_email
//                  
//          from    organization            o,
//                  group_merchant          gm,
//                  trident_profile         tp,
//                  trident_profile_api     tpa
//          where   o.org_group = :reportNode and
//                  gm.org_num = o.org_num and
//                  tp.merchant_number = gm.merchant_number and
//                  tp.terminal_id = :profileId and
//                  tpa.terminal_id = tp.terminal_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tpa.avs_results_to_decline        as avs_results_to_decline,\n                tpa.cvv2_results_to_decline       as cvv2_results_to_decline,\n                lpad(tpa.batch_close_time,4,'0')  as batch_close_time,\n                nvl(tpa.avs_required,'y')         as require_avs,\n                nvl(tpa.cvv2_required,'n')        as require_cvv2,\n                tpa.email_addresses               as email_addresses,\n                nvl(tpa.email_batch_receipt,'n')  as require_email\n                \n        from    organization            o,\n                group_merchant          gm,\n                trident_profile         tp,\n                trident_profile_api     tpa\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                tp.merchant_number = gm.merchant_number and\n                tp.terminal_id =  :2  and\n                tpa.terminal_id = tp.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.tpg.TpgConfig",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reportNode);
   __sJT_st.setString(2,profileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.tpg.TpgConfig",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:336^7*/
      resultSet = it.getResultSet();
    
      if( resultSet.next() )
      {
        String  base        = null;
        String  fname       = null;
        int     emailCount  = 0;
      
        for( int loop = 0; loop <4; ++loop )    
        {
          tokenSeparator = ",";   // default separator is a comma
          switch(loop)
          {
            case 0:
              fname = "avs_results_to_decline";
              base  = "avsResp_";
              break;
              
            case 1:
              fname = "cvv2_results_to_decline";
              base  = "cvv2Resp_";
              break;
 
            case 2:
              tokenSeparator = ";";
              if ((fields.getField("emailAddr1").getHasError() == true) || (fields.getField("emailAddr2").getHasError() == true))
              {
                setData("emailAddr1",getData("emailAddr1"));
                setData("emailAddr2",getData("emailAddr2"));
              }
              else
              {
                fname = "email_addresses";
                base  = "emailAddr";
              }
              break;
              
            default:
              continue;
          }
          
          String values = resultSet.getString(fname);
          if ( values != null && !values.equals("") )
          {
            StringTokenizer tokens = new StringTokenizer(values,tokenSeparator);
            while( tokens.hasMoreTokens() )
            {
              String token = (String)tokens.nextToken();
              if (fname == "email_addresses")
              {
                emailCount++;
                setData((base + Integer.toString(emailCount)),token);
              }
              else
              {
                setData((base + token),"y");
              }
            }
          }
        }
        setData("batchCloseTime",resultSet.getString("batch_close_time"));
        setData("vtRequireAvs",resultSet.getString("require_avs"));
        setData("vtRequireCvv2",resultSet.getString("require_cvv2"));
        setData("emailRequired",resultSet.getString("require_email"));
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadSettings()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
}/*@lineinfo:generated-code*/