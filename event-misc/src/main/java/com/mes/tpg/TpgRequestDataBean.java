/*@lineinfo:filename=TpgRequestDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tpg/TpgRequestDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2007-08-30 $
  Version            : $Revision:  $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tpg;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.api.TridentApiConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.Validation;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import com.mes.support.SyncLog;
import sqlj.runtime.ResultSetIterator;


public class TpgRequestDataBean extends TpgBaseDataBean
{
  public static final String    REQUEST_PROFILE_KEY           = "profile_key";
  public static final String    REQUEST_CARD_NUMBER           = "card_number";
  public static final String    REQUEST_CARD_EXP_DATE         = "card_exp_date";
  public static final String    REQUEST_CARD_HOLDER_ADDRESS   = "cardholder_street_address";
  public static final String    REQUEST_CARD_HOLDER_ZIP       = "cardholder_zip";
  public static final String    REQUEST_TRANS_TYPE            = "transaction_type";
  public static final String    REQUEST_TRANS_AMOUNT          = "transaction_amount";
  public static final String    REQUEST_INVOICE_NUMBER        = "invoice_number";
  public static final String    REQUEST_REFERENCE_NUMBER      = "reference_number";
  public static final String    REQUEST_TAX_AMOUNT            = "tax_amount";
  public static final String    RESPONSE_TRANS_ID             = "transaction_id";
  public static final String    RESPONSE_ERROR_CODE           = "error_code";
  public static final String    RESPONSE_AUTH_RESPONSE        = "auth_response_text";
  public static final String    RESPONSE_AVS_RESULT           = "avs_result";
  public static final String    RESPONSE_CVV2_RESULT          = "cvv2_result";

  public static final String    RESPONSE_FIELD_NAMES[] = 
  {
    TridentApiConstants.FN_RESPONSE_CODE,
    TridentApiConstants.FN_RESPONSE_SUBCODE,
    TridentApiConstants.FN_RESPONSE_REASON_CODE,
    TridentApiConstants.FN_RESPONSE_REASON_TEXT,
    TridentApiConstants.FN_RSP_APPROVAL_CODE,
    TridentApiConstants.FN_RSP_AVS_RESULT_CODE,
    TridentApiConstants.FN_RSP_TRANSACTION_ID,
    TridentApiConstants.FN_RSP_INVOICE_NUMBER,
    TridentApiConstants.FN_RSP_DESCRIPTION,
    TridentApiConstants.FN_RSP_AMOUNT,
    TridentApiConstants.FN_RSP_METHOD,
    TridentApiConstants.FN_RSP_TRANSACTION_TYPE,
    TridentApiConstants.FN_RSP_CUSTOMER_ID,
    TridentApiConstants.FN_RSP_CARDHOLDER_FIRST_NAME,
    TridentApiConstants.FN_RSP_CARDHOLDER_LAST_NAME,
    TridentApiConstants.FN_RSP_COMPANY,
    TridentApiConstants.FN_RSP_BILLING_ADDDRESS,
    TridentApiConstants.FN_RSP_CITY,
    TridentApiConstants.FN_RSP_STATE,
    TridentApiConstants.FN_RSP_ZIP,
    TridentApiConstants.FN_RSP_COUNTRY,
    TridentApiConstants.FN_RSP_PHONE,
    TridentApiConstants.FN_RSP_FAX,
    TridentApiConstants.FN_RSP_EMAIL,
    TridentApiConstants.FN_RSP_SHIP_TO_FIRST_NAME,
    TridentApiConstants.FN_RSP_SHIP_TO_LAST_NAME,
    TridentApiConstants.FN_RSP_SHIP_TO_COMPANY,
    TridentApiConstants.FN_RSP_SHIP_TO_ADDRESS,
    TridentApiConstants.FN_RSP_SHIP_TO_CITY,
    TridentApiConstants.FN_RSP_SHIP_TO_STATE,
    TridentApiConstants.FN_RSP_SHIP_TO_ZIP,
    TridentApiConstants.FN_RSP_SHIP_TO_COUNTRY,
    TridentApiConstants.FN_RSP_TAX_AMOUNT,
    TridentApiConstants.FN_RSP_DUTY_AMOUNT,
    TridentApiConstants.FN_RSP_FREIGHT_AMOUNT,
    TridentApiConstants.FN_RSP_TAX_EXEMPT_FLAG,
    TridentApiConstants.FN_RSP_PO_NUMBER,
    TridentApiConstants.FN_RSP_MD5_HASH,
    TridentApiConstants.FN_RSP_CVV2_RESPONSE_CODE,
    TridentApiConstants.FN_RSP_CAVV_RESPONSE_CODE,
  };
  

  public static class TpgParam
  {
    public String       Name      = null;
    public String       Value     = null;
    
    public TpgParam( String name, String value )
    {
      Name  = name;
      Value = value;
    }
  }
 
  public class RowData
  {
    public String    ProfileId          = null;
    public long      MerchantNumber     = 0L;
    public Date      RequestDate        = null;
    public Timestamp RequestTime        = null;
    public String    RequestParams      = null;
    public String    Response           = null;  
    public long      RecId              = 0L;  
    public String    ProfileKey         = null;
    public String    CardNumber         = null;
    public String    CardExpDate        = null;
    public String    CardHolderAddress  = null;
    public String    CardHolderZip      = null;
    public String    TransType          = null;
    public String    TransAmount        = null;
    public String    InvoiceNumber      = null;
    public String    ReferenceNumber    = null;
    public String    TaxAmount          = null;
    public String    TransId            = null;
    public String    ErrorCode          = null;
    public String    AuthResponse       = null;
    public String    AVSResult          = null;
    public String    CVV2Result         = null;
    public String    DbaName            = null;
    public String    TranTypeDesc       = null;
    public double    Amount             = 0.0;
    public String    TransactionKey     = null;
    public String    CustomerId         = null;
    public String    RespDelimiter      = null;
    public String    ApprovalCode       = null;
    public String    RespCode           = null;
    public String    RespSubcode        = null;
    public String    RespReasonCode     = null;
    public String    RespReasonText     = null;
    public String    ShipToFirstName    = null;
    public String    ShipToLastName     = null;
    public String    ShipToCompany      = null;
    public String    ShipToAddress      = null;
    public String    ShipToCity         = null;
    public String    ShipToState        = null;
    public String    ShipToZip          = null;
    public String    CardholderFirstName = null;
    public String    CardholderLastName = null;
    public String    MD5Hash            = null;
    public boolean   isAuthNetEmulation = false;
    
    protected Vector    RequestParamsVector   = new Vector();
    protected Vector    ResponseParamsVector  = new Vector();
    
    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ProfileId       = resultSet.getString("profile_id");
      MerchantNumber  = resultSet.getLong("merchant_number");
      RequestDate     = resultSet.getDate("request_date");
      RequestTime     = resultSet.getTimestamp("request_time");
      RequestParams   = resultSet.getString("request_params");
      Response        = resultSet.getString("response");
      RecId           = resultSet.getLong("rec_id");
      DbaName         = resultSet.getString("dba_name");
      
      if ( !processData( RequestParams, TridentApiConstants.FN_AN_VERSION).equals("n/a") )
      {
        isAuthNetEmulation = true;
      }

      RespDelimiter = processData( RequestParams, TridentApiConstants.FN_AN_DELIM_CHAR);
              
      if ( isAuthNetEmulation )   
      {
        CardNumber          = processData( RequestParams, TridentApiConstants.FN_AN_CARD_NUMBER );
        CardExpDate         = processData( RequestParams, TridentApiConstants.FN_AN_EXP_DATE ); 
        CustomerId          = processData( RequestParams, TridentApiConstants.FN_AN_CUST_ID );
        CardHolderAddress   = processData( RequestParams, TridentApiConstants.FN_AN_AVS_STREET );
        CardholderFirstName = processData( RequestParams, TridentApiConstants.FN_AN_FIRST_NAME );
        CardholderLastName  = processData( RequestParams, TridentApiConstants.FN_AN_LAST_NAME );
        CardHolderZip       = processData( RequestParams, TridentApiConstants.FN_AN_AVS_ZIP );   
        InvoiceNumber       = processData( RequestParams, TridentApiConstants.FN_AN_INV_NUM );  
        ShipToFirstName     = processData( RequestParams, TridentApiConstants.FN_AN_SHIP_TO_FIRST_NAME );
        ShipToLastName      = processData( RequestParams, TridentApiConstants.FN_AN_SHIP_TO_LAST_NAME );
        ShipToCompany       = processData( RequestParams, TridentApiConstants.FN_AN_SHIP_TO_COMPANY ); 
        ShipToAddress       = processData( RequestParams, TridentApiConstants.FN_AN_SHIP_TO_ADDRESS );
        ShipToCity          = processData( RequestParams, TridentApiConstants.FN_AN_SHIP_TO_CITY );
        ShipToState         = processData( RequestParams, TridentApiConstants.FN_AN_SHIP_TO_STATE );
        ShipToZip           = processData( RequestParams, TridentApiConstants.FN_AN_SHIP_TO_ZIP ); 
        TransAmount         = processData( RequestParams, TridentApiConstants.FN_AN_AMOUNT );
        TranTypeDesc        = processData( RequestParams, TridentApiConstants.FN_AN_TRAN_TYPE );
        TransactionKey      = processData( RequestParams, TridentApiConstants.FN_AN_TERM_PASS );        
        AVSResult      = parseParams( Response, RespDelimiter, TridentApiConstants.FN_RSP_AVS_RESULT_CODE );
        CVV2Result     = parseParams( Response, RespDelimiter, TridentApiConstants.FN_RSP_CVV2_RESPONSE_CODE );
        TransId        = parseParams( Response, RespDelimiter, TridentApiConstants.FN_RSP_TRANSACTION_ID );
        ErrorCode      = parseParams( Response, RespDelimiter, TridentApiConstants.FN_RESPONSE_REASON_CODE );
        RespCode       = parseParams( Response, RespDelimiter, TridentApiConstants.FN_RESPONSE_CODE );
        RespSubcode    = parseParams( Response, RespDelimiter, TridentApiConstants.FN_RESPONSE_SUBCODE );
        RespReasonText = parseParams( Response, RespDelimiter, TridentApiConstants.FN_RESPONSE_REASON_TEXT );
        ApprovalCode   = parseParams( Response, RespDelimiter, TridentApiConstants.FN_RSP_APPROVAL_CODE );
        MD5Hash        = parseParams( Response, RespDelimiter, TridentApiConstants.FN_RSP_MD5_HASH );
        setAuthResponse( parseParams( Response, RespDelimiter, TridentApiConstants.FN_RESPONSE_CODE ));
      }
      else
      {
        CardNumber      = processData( RequestParams, REQUEST_CARD_NUMBER );
        CardExpDate     = processData( RequestParams, REQUEST_CARD_EXP_DATE ); 
        CardHolderAddress  = processData( RequestParams, REQUEST_CARD_HOLDER_ADDRESS ); 
        CardHolderZip   = processData( RequestParams, REQUEST_CARD_HOLDER_ZIP );        
        TransAmount     = processData( RequestParams, REQUEST_TRANS_AMOUNT);
        InvoiceNumber   = processData( RequestParams, REQUEST_INVOICE_NUMBER );  
        TransType       = processData( RequestParams, REQUEST_TRANS_TYPE);
        TranTypeDesc    = tranTypeDesc( TransType.charAt(0));
        AuthResponse    = processData( Response, RESPONSE_AUTH_RESPONSE);
        CVV2Result      = processData( Response, RESPONSE_CVV2_RESULT);
        AVSResult       = processData( Response, RESPONSE_AVS_RESULT);
        TransId         = processData( Response, RESPONSE_TRANS_ID );
        ErrorCode       = processData( Response, RESPONSE_ERROR_CODE );
        ProfileKey      = processData( RequestParams, REQUEST_PROFILE_KEY );       
        ReferenceNumber = processData( RequestParams, REQUEST_REFERENCE_NUMBER );  
        TaxAmount       = processData( RequestParams, REQUEST_TAX_AMOUNT );     
      }
      
      try
      { 
        Amount = Double.parseDouble(TransAmount); 
      } 
      catch( Exception nfe ) 
      { 
        Amount = 0.0; 
      }

      parseRequestParams(RequestParams);
      parseResponseParams(Response);        
    }
    
    public void setAuthResponse( String authResponse )
    {
      StringBuffer buff = new StringBuffer();

      buff.append(authResponse);
      buff.append(" - ");

      if (authResponse.equals("1"))
      {
        buff.append("Approved");
      }
      else if (authResponse.equals("2"))
      {
        buff.append("Declined");
      }
      else if (authResponse.equals("3"))
      {
        buff.append("Error");
      }
      AuthResponse = buff.toString();
      
    }
    
    public Vector getRequestParamsVector()
    {
      return( RequestParamsVector );
    }
    
    public Vector getResponseParamsVector()
    {
      return( ResponseParamsVector );
    }
    
    protected void parseParams( Vector paramVector, String paramString )
    {
      String      name        = null;
      String      temp        = null;
      String      value       = null;
      
      paramVector.removeAllElements();
      
      StringTokenizer nameValueTokens = null;
      
      if (paramString != null)
      {
        StringTokenizer tokens          = new StringTokenizer(paramString,"&");
        while( tokens.hasMoreTokens() )
        {
          nameValueTokens = new StringTokenizer( tokens.nextToken(), "=" );
          for ( int i = 0; i < 2; ++i )
          {
            try
            {
              temp = nameValueTokens.nextToken();
            }
            catch( Exception e )
            {
              temp = "";
            }
            switch( i ) 
            {
              case 0: 
                name = temp;
                break;
                
              case 1: 
                value = temp; 
                break;
                
              default: 
                break;
            }
          }
          if ( name != null && !name.equals("") )
          {
            paramVector.addElement( new TpgParam(name,value) );
          }          
        } 
      }
    }
    
    protected void parseParams( Vector paramVector, String paramString, String delimiter )
    {
      String      name        = null;
      String      token       = null;
      String      value       = null;
      int         i           = 0;
      boolean     lastTokenEqualsDelim = false;
      
      paramVector.removeAllElements();
      
      StringTokenizer nameValueTokens = null;
      
      if (paramString != null)
      {
        StringTokenizer st = new StringTokenizer(paramString, delimiter, true);

        while( st.hasMoreTokens() )
        {
          try
          {
            token = st.nextToken();
            name = RESPONSE_FIELD_NAMES[i];
            if (token.equals( delimiter ))
            {
              token = "";
              
              if (lastTokenEqualsDelim)
              {
                i++; 
              }
              lastTokenEqualsDelim = true;              
            }
            else
            {
              lastTokenEqualsDelim = false;
              i++;
            }
            
          }
          catch( Exception e )
          {
            token = "";
          }
          
          value = token;
          if ( value != null && !value.equals(""))
          {
            paramVector.addElement( new TpgParam(name,value) );
          }          
        }
      }
    }
    
    protected String parseParams( String paramString, String delimiter, String paramName )
    {
      String      name        = null;
      String      token       = null;
      String      retVal      = null;
      int         i           = 0;
      boolean     lastTokenEqualsDelim = false;
      
      StringTokenizer nameValueTokens = null;
      
      if (paramString != null)
      {
        StringTokenizer st = new StringTokenizer(paramString, delimiter, true);

        while( st.hasMoreTokens() )
        {
          try
          {
            token = st.nextToken();
            name = RESPONSE_FIELD_NAMES[i];
            if (token.equals( delimiter ))
            {
              token = "";
              
              if (lastTokenEqualsDelim)
              {
                i++; 
              }
              lastTokenEqualsDelim = true;              
            }
            else
            {
              lastTokenEqualsDelim = false;
              i++;
            }            
          }
          catch( Exception e )
          {
            token = "";
          }
          
          if( name == paramName )
          {
            retVal = token;
          }          
        }
      }
      return retVal;
    }
        
    protected void parseRequestParams( String requestParams )
    {
      parseParams( RequestParamsVector, requestParams );
    }
    
    protected void parseResponseParams( String response )
    {
      if ( isAuthNetEmulation )
      {
        parseParams( ResponseParamsVector, response, RespDelimiter );                
      }
      else
      {
        parseParams( ResponseParamsVector, response );        
      }
    }
    
  }
  
  public class ProfileValidation implements Validation
  {
    private String ErrorMessage     = null;
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate( String fdata )
    {
      int     recCount    = 0;
      
      ErrorMessage = null;
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:491^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(terminal_id) 
//            from    trident_profile
//            where   terminal_id = :fdata
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(terminal_id)  \n          from    trident_profile\n          where   terminal_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tpg.TpgRequestDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fdata);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:496^9*/
      }
      catch( java.sql.SQLException e )
      {
        logEntry("validate(" + fdata + ")", e.toString());
        ErrorMessage = "Profile validation failed";
      }
      
      if ( recCount == 0 )
      {
        ErrorMessage = "Invalid profile id";
      }  
      
      return( ErrorMessage == null );
    }
  }
  
  public TpgRequestDataBean( )
  {
  }
  
  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    Field         field;
    int           reportType = HttpHelper.getInt(request,"reportType",-1);
    
    super.createFields(request);
    fgroup = (FieldGroup)getField("searchFields");
    
     switch (reportType)
    {
      case 1:
        fields.add( new DateField( "fromDate", "Report Begin Date", false));
        fields.add( new DateField( "toDate", "End Date", false));
        fields.add( new ButtonField ("btnRefresh", "Refresh" ) );
        ((DateField)fields.getField( "fromDate")).setDateFormat("MM/dd/yyyy");
        ((DateField)fields.getField( "toDate")).setDateFormat("MM/dd/yyyy");
        break;
        
      case 2:
        fgroup.deleteField("portfolioId");
        fgroup.deleteField("zip2Node");
        fgroup.deleteField("profile_id");
        fgroup.add( new Field("profileId", "Profile ID (required)", 25, 0, false ) );
        fgroup.add( new Field("searchValue", "Search Value" ,25, 0, true));
        fgroup.getField("profileId").addValidation( new ProfileValidation() );
        break;
        
      default:
        break;
    }
  }
  
  
  protected void encodeHeaderCSV( StringBuffer line )
  {    
    switch (getReportType())
    {
      case 1:
        line.setLength(0);
        line.append("\"Date\",");
        line.append("\"Tran Type\",");
        line.append("\"DBA Name\",");
        line.append("\"Account#\",");
        line.append("\"Tran Amount\",");
        line.append("\"Error Code and Error Description\",");
        line.append("\"AVS Result Code\",");
        line.append("\"CVV2 Result Code\"");
        break;
        
      case 2:
        line.setLength(0);
        line.append("\"Profile Id\",");
        line.append("\"Merchant Number\",");
        line.append("\"Request Date\",");
        line.append("\"Card Number\",");
        line.append("\"Card Exp Date\",");
        line.append("\"Card Holder Address\",");
        line.append("\"Card Holder Zip\",");
        line.append("\"Trans Type\",");
        line.append("\"Trans Amount\",");
        line.append("\"Invoice Number\",");
        line.append("\"Reference Number\",");
        line.append("\"Tax Amount\",");
        line.append("\"Trans Id\",");
        line.append("\"Error Code\",");
        line.append("\"Auth Response\",");
        line.append("\"AVS Result\",");
        line.append("\"CVV2 Result\"");
        break;
        
      default:
        break;
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData   record         = (RowData)obj;
    String    acceptCode     = "000";
    String    valueNa        = "n/a";
    String    valueBlank     = "-";
    
    line.setLength(0);
    
    switch ( getReportType() )
    {
      case 1:
        if ( !record.ErrorCode.equals(acceptCode) ) 
        {
          line.append( DateTimeFormatter.getFormattedDate (record.RequestTime, "MM/dd/yyyy HH:mm:ss") );
          line.append( "," );
          line.append( record.TranTypeDesc );
          line.append( "," );
          line.append( record.DbaName );
          line.append( "," );
          line.append( record.CardNumber );
          line.append( "," );
          line.append( MesMath.toCurrency(record.TransAmount) );
          line.append( "," );
          line.append( record.ErrorCode + " - " + record.AuthResponse );
          line.append( "," );
          line.append( (record.AVSResult.equals(valueNa)) ? valueBlank : record.AVSResult );
          line.append( "," );
          line.append( (record.CVV2Result.equals(valueNa)) ? valueBlank : record.CVV2Result );
        }
        break;
        
      case 2:
        line.append( "'" + record.ProfileId );
        line.append( "," );
        line.append( encodeHierarchyNode(record.MerchantNumber) );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate( record.RequestTime, "MM/dd/yyyy HH:mm:ss" ) );
        line.append( "," );
        line.append( record.CardNumber );
        line.append( ",\"" );
        line.append( record.CardExpDate );
        line.append( "\",\"" );
        line.append( record.CardHolderAddress );
        line.append( "\",\"" );
        line.append( record.CardHolderZip );      
        line.append( "\"," );
        line.append( record.TranTypeDesc );
        line.append( ",\"" );
        line.append( MesMath.toCurrency(record.TransAmount) );
        line.append( "\"," );
        line.append( record.InvoiceNumber ); 
        line.append( ",\"" );
        line.append( record.ReferenceNumber ); 
        line.append( "\"," );
        line.append( MesMath.toCurrency(record.TaxAmount) );
        line.append( ",\"" );
        line.append( record.TransId ); 
        line.append( "\"," );
        line.append( record.ErrorCode );
        line.append( ",\"" );
        line.append( record.AuthResponse );
        line.append( "\"," );
        line.append( record.AVSResult );
        line.append( "," );
        line.append( record.CVV2Result );
        break;
        
      default:
        break;
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Date                    beginDate  = null;
    Date                    endDate    = null;
    StringBuffer            filename   = new StringBuffer("");
    
    if ( getReportType() == 1 )
    {
      filename.append("rejected_transactions_");
      beginDate  = reportFromDate();
      endDate  = reportToDate();
    }
    else if ( getReportType() == 2 )
    {
      filename.append("tpg_request_log_");
       beginDate = getReportDateBegin();
       endDate = getReportDateEnd();
    }
    filename.append( DateTimeFormatter.getFormattedDate(beginDate,"MMddyy") );
    if ( beginDate != endDate )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(endDate,"MMddyy") );
    }
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
        
      case FF_FIXED:
        break;
    }
    return(retVal);
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl(buffer,nodeId,beginDate,endDate);
    buffer.append("&fromDate=");
    buffer.append(getData("fromDate"));
    buffer.append("&toDate=");
    buffer.append(getData("toDate"));
  }

  public Date reportFromDate( )
  {
    Date            retVal    = null;
 
    retVal  = ((DateField)fields.getField("fromDate")).getSqlDate();
    
    return( retVal );
  }
  
  
  public Date reportToDate( )
  {
    Date            retVal    = null;
 
    retVal  = ((DateField)fields.getField("toDate")).getSqlDate();
    
    return( retVal );
  }
  
  
  public void loadData( )
  {
    String     lookupVal    = "";
    String     profileId    = null;
    Date       beginDate    = null;
    Date       endDate      = null;
    int        reportType   = HttpHelper.getInt(request,"reportType",-1);
  
    switch ( reportType )
    {
      case 1:
        profileId = getData("profile_id");
        beginDate = reportFromDate();
        endDate   = reportToDate();
        break;
        
      case 2:
        profileId = getData("profileId");
        lookupVal = getData("searchValue").toLowerCase().trim();
        beginDate = getReportDateBegin();
        endDate   = getReportDateEnd();
        break;
        
      default:
        break;
    }
    searchData( profileId, lookupVal, beginDate, endDate );
  }

  public void searchData(String profileId, String stringLookup, Date beginDate, Date endDate )  
  {
    ResultSetIterator it          = null;
    long              reportNode  = getReportHierarchyNodeDefault();
    ResultSet         resultSet   = null;
    RowData           row         = null;

    try
    {
      ReportRows.clear();       // empty the current contents
      
      if ( stringLookup != "" )
      {
        stringLookup =  "%" + stringLookup + "%";
      }
      else
      {
        stringLookup = "passall";
      }
      /*@lineinfo:generated-code*//*@lineinfo:786^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    al.profile_id                 as profile_id,
//                    al.merchant_number            as merchant_number,
//                    al.request_date               as request_date,
//                    al.request_time               as request_time,
//                    al.rec_id                     as rec_id,
//                    al.request_params             as request_params,
//                    al.response                   as response,
//                    nvl(tp.merchant_name,'N/A')   as dba_name
//          from      trident_api_access_log  al,
//                    trident_profile         tp
//          where     al.profile_id = :profileId and
//                    exists
//                    (
//                      select  terminal_id
//                      from    organization    o,
//                              group_merchant  gm,
//                              trident_profile tp
//                      where   o.org_group = :reportNode and
//                              gm.org_num = o.org_num and
//                              tp.merchant_number = gm.merchant_number and
//                              tp.terminal_id = :profileId
//                    ) and
//                    al.request_date between :beginDate and :endDate and
//                    ( 
//                      'passall' = :stringLookup or
//                      lower(al.request_params) like :stringLookup or 
//                      lower(al.response) like :stringLookup or
//                      al.RETRIEVAL_REF_NUM like :stringLookup
//                    ) and
//                    tp.terminal_id(+) = al.profile_id
//          order by  al.request_time desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    al.profile_id                 as profile_id,\n                  al.merchant_number            as merchant_number,\n                  al.request_date               as request_date,\n                  al.request_time               as request_time,\n                  al.rec_id                     as rec_id,\n                  al.request_params             as request_params,\n                  al.response                   as response,\n                  nvl(tp.merchant_name,'N/A')   as dba_name\n        from      trident_api_access_log  al,\n                  trident_profile         tp\n        where     al.profile_id =  :1  and\n                  exists\n                  (\n                    select  terminal_id\n                    from    organization    o,\n                            group_merchant  gm,\n                            trident_profile tp\n                    where   o.org_group =  :2  and\n                            gm.org_num = o.org_num and\n                            tp.merchant_number = gm.merchant_number and\n                            tp.terminal_id =  :3 \n                  ) and\n                  al.request_date between  :4  and  :5  and\n                  ( \n                    'passall' =  :6  or\n                    lower(al.request_params) like  :7  or \n                    lower(al.response) like  :8  or\n                    al.RETRIEVAL_REF_NUM like  :9 \n                  ) and\n                  tp.terminal_id(+) = al.profile_id\n        order by  al.request_time desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tpg.TpgRequestDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,profileId);
   __sJT_st.setLong(2,reportNode);
   __sJT_st.setString(3,profileId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setString(6,stringLookup);
   __sJT_st.setString(7,stringLookup);
   __sJT_st.setString(8,stringLookup);
   __sJT_st.setString(9,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.tpg.TpgRequestDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:819^7*/

      resultSet = it.getResultSet();
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData(resultSet) );
      }
      it.close();   // this will also close the resultSet
    } 
    catch (java.sql.SQLException e)
    {
      logEntry( "searchData", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
    }
  }
  
  public RowData loadDetailData( long recId )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    RowData                       row               = null;

    try
    {
      ReportRows.clear();    // empty the current contents
 
      /*@lineinfo:generated-code*//*@lineinfo:848^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  al.profile_id                 as profile_id,
//                  al.merchant_number            as merchant_number,
//                  al.request_date               as request_date,
//                  al.request_time               as request_time,
//                  al.rec_id                     as rec_id,
//                  al.request_params             as request_params,
//                  al.response                   as response,
//                  nvl(tp.merchant_name,'N/A')   as dba_name
//          from    trident_api_access_log    al,
//                  trident_profile           tp
//          where   al.rec_id = :recId and
//                  tp.terminal_id(+) = al.profile_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  al.profile_id                 as profile_id,\n                al.merchant_number            as merchant_number,\n                al.request_date               as request_date,\n                al.request_time               as request_time,\n                al.rec_id                     as rec_id,\n                al.request_params             as request_params,\n                al.response                   as response,\n                nvl(tp.merchant_name,'N/A')   as dba_name\n        from    trident_api_access_log    al,\n                trident_profile           tp\n        where   al.rec_id =  :1  and\n                tp.terminal_id(+) = al.profile_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tpg.TpgRequestDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.tpg.TpgRequestDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:862^7*/ 
      resultSet = it.getResultSet();

      if( resultSet.next() )
      {
        row = new RowData(resultSet);
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadDetailData", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
    return( row );
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
  
  public String processData(String stringParams, String paramsName)
  {
    StringTokenizer   tokenizer   = null;
    String            parmName    = null;
    String            parmData    = null;
    String            nextParams  = null;
    String            value       = "n/a";
    boolean           getData     = false;
    
    if (stringParams != null)
    {
      try
      {
         tokenizer = new StringTokenizer( stringParams, "&");
         while( tokenizer.hasMoreTokens() && (getData == false) )
         {
           try
           {
             nextParams = tokenizer.nextToken();
             StringTokenizer token = new StringTokenizer(nextParams,"=");
             int tc = token.countTokens();
             if (tc == 2)
             {
               parmName  = token.nextToken();
               parmData  = token.nextToken();
               
               if (parmName.equals(paramsName))
               {
                 getData = true;
               }
             } //end if
           }
           catch( NoSuchElementException e )
           {
           }
         } //end while
         if (getData == true)
         {
          value = parmData;
         }
      }
      catch( Exception e )
      {
        SyncLog.LogEntry( "TpgRequestDataBean.processData()", e.toString() );
      }
    } //end if

    return(value);
  }


  public String tranTypeDesc(char tranType)
  {
    String retVal = null;
    
    switch (tranType)
    {
      case 'D':
        retVal = "Sale";
        break;
      
      case 'C':
        retVal = "Credit";
        break;
    
      case 'P':
        retVal = "Pre-Auth";
        break;
    
      case 'V':
        retVal = "Void";
        break;
    
      case 'U':
        retVal = "Refund";
        break;
    
      case 'O':
        retVal = "Offline";
        break;
    
      default:
        retVal = "";
        break;
    }
    return(retVal);
  }
  
}/*@lineinfo:generated-code*/