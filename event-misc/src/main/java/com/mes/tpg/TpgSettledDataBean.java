/*@lineinfo:filename=TpgSettledDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tpg/TpgSettledDataBean.sqlj $

  Description:


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tpg;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class TpgSettledDataBean extends TpgBaseDataBean
{
  public class SettledFilesTable extends DropDownTable
  {
    public SettledFilesTable( long nodeId )
    {
      ResultSetIterator       it = null;
      ResultSet               rs = null;

      try
      {
        connect();

        addElement("","select");

        /*@lineinfo:generated-code*//*@lineinfo:75^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct
//                    to_char(tapi.batch_date,'mmddyyyy')       as batch_id,
//                    to_char(tapi.batch_date,'Dy dd-Mon-yyyy') as batch_date_string,
//                    tapi.batch_date                           as batch_date
//            from    organization          o,
//                    group_merchant        gm,
//                    trident_profile       tp,
//                    trident_capture_api   tapi
//            where   o.org_group = :nodeId and
//                    gm.org_num = o.org_num and
//                    tp.merchant_number = gm.merchant_number and
//                    nvl(tp.api_enabled,'N') = 'Y' and
//                    tapi.terminal_id = tp.terminal_id and
//                    tapi.batch_date >= (sysdate-365)
//            order by tapi.batch_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct\n                  to_char(tapi.batch_date,'mmddyyyy')       as batch_id,\n                  to_char(tapi.batch_date,'Dy dd-Mon-yyyy') as batch_date_string,\n                  tapi.batch_date                           as batch_date\n          from    organization          o,\n                  group_merchant        gm,\n                  trident_profile       tp,\n                  trident_capture_api   tapi\n          where   o.org_group =  :1  and\n                  gm.org_num = o.org_num and\n                  tp.merchant_number = gm.merchant_number and\n                  nvl(tp.api_enabled,'N') = 'Y' and\n                  tapi.terminal_id = tp.terminal_id and\n                  tapi.batch_date >= (sysdate-365)\n          order by tapi.batch_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tpg.TpgSettledDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tpg.TpgSettledDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^9*/
        rs = it.getResultSet();

        while( rs.next() )
        {
          addElement(rs);
        }
        rs.close();
        it.close();
      }
      catch( Exception e )
      {
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }

  public TpgSettledDataBean( )
  {
  }

  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    Field         field;

    super.createFields(request);

    fgroup = (FieldGroup)getField("searchFields");

    if ( HttpHelper.getBoolean(request,"ajax",false) )
    {
      fgroup.add( new HiddenField( "batchDateFrom") );
      fgroup.add( new HiddenField( "batchDateTo") );
    }
    else    // add a drop down for the user to use
    {
      fields.add( new DateField( "batchDateFrom", "From Date", false ) );
      ((DateField)fields.getField("batchDateFrom")).setDateFormat("MM/dd/yyyy");
      fields.add( new DateField( "batchDateTo", "To Date", false ) );
      ((DateField)fields.getField("batchDateTo")).setDateFormat("MM/dd/yyyy");
    }
    fgroup.add( new ButtonField( "btnRefresh", "Refresh" ) );
  }

  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Tran ID\",");
    line.append("\"Tran Date\",");
    line.append("\"Settle Date\",");
    line.append("\"Card Type\",");
    line.append("\"Card Number\",");
    line.append("\"Reference Number\",");
    line.append("\"Invoice Number\",");
    line.append("\"Auth Code\",");
    line.append("\"Currency Code\",");
    line.append("\"Amount\"");
    if ( hasFxData() )
    {
      line.append(",\"USD Amount\"");
    }
  }

  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;

    // clear the line buffer and add
    // the merchant specific data.
    line.setLength(0);
    line.append( record.getTridentTranId() );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.getTranDate(),"MM/dd/yyyy") );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.getBatchDate(),"MM/dd/yyyy") );
    line.append( "," );
    line.append( record.getCardType() );
    line.append( "," );
    line.append( record.getCardNumber() );
    line.append( "," );
    line.append( record.getReferenceNumber() );
    line.append( "," );
    line.append( record.getPurchaseId() );
    line.append( "," );
    line.append( record.getAuthCode() );
    line.append( ",\"" );
    line.append( record.getCurrencyCodeAlpha() );
    line.append( "\"," );
    line.append( record.getTranAmount() );
    if ( hasFxData() )
    {
      line.append( "," );
      line.append( record.getFxAmount() );
    }
  }

  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl(buffer,nodeId,beginDate,endDate);
    buffer.append("&batchDateFrom=");
    buffer.append(getData("batchDateFrom"));
    buffer.append("&batchDateTo=");
    buffer.append(getData("batchDateTo"));
  }

  public Date getBatchDate( String fname )
  {
    Date            retVal    = null;

    retVal  = ((DateField)fields.getField(fname)).getSqlDate();

    return( retVal );
  }


  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");

    filename.append("tpg_settled_transactions");
    return ( filename.toString() );
  }

  public Date getLastSettleDate()
  {
    long        nodeId        = getReportHierarchyNode();
    Date        settleDate    = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:228^7*/

//  ************************************************************
//  #sql [Ctx] { select  /*+
//                    ordered
//                    use_nl(gm tapi)
//                    index(tapi idx_tapi_merch_batch_date)
//                  */
//                  max(tapi.batch_date) 
//          from    organization          o,
//                  group_merchant        gm,
//                  trident_capture_api   tapi
//          where   o.org_group = :nodeId and
//                  gm.org_num = o.org_num and
//                  tapi.merchant_number = gm.merchant_number and
//                  tapi.batch_date >= (sysdate-28)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  /*+\n                  ordered\n                  use_nl(gm tapi)\n                  index(tapi idx_tapi_merch_batch_date)\n                */\n                max(tapi.batch_date)  \n        from    organization          o,\n                group_merchant        gm,\n                trident_capture_api   tapi\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                tapi.merchant_number = gm.merchant_number and\n                tapi.batch_date >= (sysdate-28)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tpg.TpgSettledDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   settleDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^7*/
    }
    catch( Exception e )
    {
      logEntry("getLastSettleDate()",e.toString());
    }
    return( settleDate );
  }

  public void loadData( )
  {
    loadData( getReportHierarchyNode(), ReportDateBegin, ReportDateEnd );
  }

  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator     it              = null;
    Date                  batchDateFrom   = getBatchDate("batchDateFrom");
    Date                  batchDateTo     = getBatchDate("batchDateTo");
    double                fxAmount        = 0.0;
    String                profileId       = getData("profile_id");
    long                  reportNode      = getReportHierarchyNodeDefault();
    ResultSet             resultSet       = null;

    try
    {
      ReportRows.removeAllElements();

      if ( batchDateFrom != null && batchDateTo != null )
      {
        /*@lineinfo:generated-code*//*@lineinfo:273^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tapi.terminal_id                as terminal_id,
//                    tapi.trident_tran_id            as trident_tran_id,
//                    tapi.trident_tran_id_number     as trident_tran_id_num,
//                    tapi.external_tran_id           as external_tran_id,
//                    tapi.dba_name                   as dba_name,
//                    tapi.dba_city                   as dba_city,
//                    tapi.dba_state                  as dba_state,
//                    tapi.dba_zip                    as dba_zip,
//                    tapi.debit_credit_indicator     as debit_credit_ind,
//                    tapi.sic_code                   as sic_code,
//                    tapi.phone_number               as phone_number,
//                    tapi.card_number                as card_number,
//                    tapi.batch_date                 as batch_date,
//                    tapi.transaction_date           as tran_date,
//                    (
//                      tapi.transaction_amount *
//                      decode(tapi.debit_credit_indicator,'C',-1,1)
//                    )                               as tran_amount,
//                    tapi.auth_amount                as auth_amount,
//                    tapi.auth_returned_aci          as returned_aci,
//                    tapi.auth_code                  as auth_code,
//                    tapi.reference_number           as reference_number,
//                    decode(tapi.card_type,'BL',tapi.client_reference_number,tapi.purchase_id) as purchase_id,
//                    --tapi.purchase_id                as purchase_id,
//                    tapi.transaction_type           as tran_type,
//                    tapi.merchant_number            as merchant_number,
//                    tapi.card_type                  as card_type,
//                    nvl(emd.pos_entry_desc,
//                        tapi.pos_entry_mode)        as pos_entry_mode,
//                    tapi.currency_code              as currency_code,
//                    tapi.debit_network_id           as debit_network_id,
//                    tapi.avs_zip                    as avs_zip,
//                    decode( nvl(tapi.emulation,'TPG'),
//                            'AUTHNET',1,  /* EMU_AUTHORIZE_NET */
//                            0 )  /* EMU_TPG */      as emulation,
//                    tapi.fx_rate_id                 as fx_rate_id,
//                    (
//                      tapi.fx_amount_base *
//                      decode(tapi.debit_credit_indicator,'C',-1,1)
//                    )                               as fx_amount,
//                    tapi.fx_rate                    as fx_rate,
//                    tapi.fx_reference_number        as fx_ref_num,
//                    nvl(tapi.test_flag,'Y')         as test_flag
//            from    organization            o,
//                    group_merchant          gm,
//                    trident_capture_api     tapi,
//                    pos_entry_mode_desc     emd
//            where   o.org_group = :reportNode and
//                    gm.org_num = o.org_num and
//                    tapi.merchant_number = gm.merchant_number and
//                    tapi.terminal_id = :profileId and
//                    tapi.batch_date between :batchDateFrom and :batchDateTo and
//                    (
//                      ( not tapi.card_type in ( 'AM','DS' ) and not tapi.load_file_id is null ) or
//                      ( tapi.card_type = 'AM' and not tapi.amex_load_filename is null ) or
//                      ( tapi.card_type = 'DS' and not tapi.discover_load_file_id is null )
//                    ) and
//                    emd.pos_entry_code(+) = tapi.pos_entry_mode
//            order by batch_date desc, currency_code, emulation, tapi.mesdb_timestamp desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tapi.terminal_id                as terminal_id,\n                  tapi.trident_tran_id            as trident_tran_id,\n                  tapi.trident_tran_id_number     as trident_tran_id_num,\n                  tapi.external_tran_id           as external_tran_id,\n                  tapi.dba_name                   as dba_name,\n                  tapi.dba_city                   as dba_city,\n                  tapi.dba_state                  as dba_state,\n                  tapi.dba_zip                    as dba_zip,\n                  tapi.debit_credit_indicator     as debit_credit_ind,\n                  tapi.sic_code                   as sic_code,\n                  tapi.phone_number               as phone_number,\n                  tapi.card_number                as card_number,\n                  tapi.batch_date                 as batch_date,\n                  tapi.transaction_date           as tran_date,\n                  (\n                    tapi.transaction_amount *\n                    decode(tapi.debit_credit_indicator,'C',-1,1)\n                  )                               as tran_amount,\n                  tapi.auth_amount                as auth_amount,\n                  tapi.auth_returned_aci          as returned_aci,\n                  tapi.auth_code                  as auth_code,\n                  tapi.reference_number           as reference_number,\n                  decode(tapi.card_type,'BL',tapi.client_reference_number,tapi.purchase_id) as purchase_id,\n                  --tapi.purchase_id                as purchase_id,\n                  tapi.transaction_type           as tran_type,\n                  tapi.merchant_number            as merchant_number,\n                  tapi.card_type                  as card_type,\n                  nvl(emd.pos_entry_desc,\n                      tapi.pos_entry_mode)        as pos_entry_mode,\n                  tapi.currency_code              as currency_code,\n                  tapi.debit_network_id           as debit_network_id,\n                  tapi.avs_zip                    as avs_zip,\n                  decode( nvl(tapi.emulation,'TPG'),\n                          'AUTHNET',1,  /* EMU_AUTHORIZE_NET */\n                          0 )  /* EMU_TPG */      as emulation,\n                  tapi.fx_rate_id                 as fx_rate_id,\n                  (\n                    tapi.fx_amount_base *\n                    decode(tapi.debit_credit_indicator,'C',-1,1)\n                  )                               as fx_amount,\n                  tapi.fx_rate                    as fx_rate,\n                  tapi.fx_reference_number        as fx_ref_num,\n                  nvl(tapi.test_flag,'Y')         as test_flag\n          from    organization            o,\n                  group_merchant          gm,\n                  trident_capture_api     tapi,\n                  pos_entry_mode_desc     emd\n          where   o.org_group =  :1  and\n                  gm.org_num = o.org_num and\n                  tapi.merchant_number = gm.merchant_number and\n                  tapi.terminal_id =  :2  and\n                  tapi.batch_date between  :3  and  :4  and\n                  (\n                    ( not tapi.card_type in ( 'AM','DS' ) and not tapi.load_file_id is null ) or\n                    ( tapi.card_type = 'AM' and not tapi.amex_load_filename is null ) or\n                    ( tapi.card_type = 'DS' and not tapi.discover_load_file_id is null )\n                  ) and\n                  emd.pos_entry_code(+) = tapi.pos_entry_mode\n          order by batch_date desc, currency_code, emulation, tapi.mesdb_timestamp desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tpg.TpgSettledDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reportNode);
   __sJT_st.setString(2,profileId);
   __sJT_st.setDate(3,batchDateFrom);
   __sJT_st.setDate(4,batchDateTo);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.tpg.TpgSettledDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:334^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          ReportRows.addElement( new RowData( resultSet ) );
          fxAmount += resultSet.getDouble("fx_amount");
        }
        resultSet.close();
        it.close();

        setFxDataIncluded( (fxAmount == 0.0) ? false : true );
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
}/*@lineinfo:generated-code*/