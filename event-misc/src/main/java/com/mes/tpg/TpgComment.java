/*@lineinfo:filename=TpgComment*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tpg/TpgComment.sqlj $

  Description:  


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tpg;

import java.sql.Date;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesEmails;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.net.MailMessage;

public class TpgComment extends TpgBaseDataBean
{
  protected static final String[][] MessageTypes = 
  {
    { "Comment",  "Comment" },
    { "Question", "Question" },
    { "Problem",  "Problem" },
  };

  public TpgComment( )
  {
  }
  
  protected boolean autoSubmit( )
  {
    String    fname   = getAutoSubmitName();
    boolean   retVal  = false;
    
    try
    {
      if ( fname.equals("btnSend") )
      {
        retVal = sendComment();
      }
    }
    catch( Exception e )
    {
      logEntry("autoSubmit()",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    fields.add( new RadioButtonField ("commentType", "I have a", MessageTypes,0,false,"Required") );
    fields.add( new NumberField("merchantId", "Merchant ID", 16, 16, false, 0) );
    fields.add( new Field("firstName", "First Name", 32, 40, false) );
    fields.add( new Field("lastName", "Last Name", 32, 40, false) );
    fields.add( new Field("city", "City", 32, 40, true) );
    fields.add( new DropDownField( "state", "State", new StateDropDownTable(), true) );
    fields.add( new PhoneField("phone", "Phone", false) );
    fields.add( new Field("email", "Email", 32, 40, false) );
    fields.add( new TextareaField( "comments", "Comments", 1024, 10, 45, false) );
    fields.add( new ButtonField( "btnSend", "Send" ) );
  }
  
  public void loadData( )
  {
    loadData( getReportHierarchyNode(), ReportDateBegin, ReportDateEnd );
  }
  
  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest(request);
    
    long merchantId = getReportMerchantId();
    if ( merchantId != 0L )
    {
      setData("merchantId",String.valueOf(merchantId));
    }
  }
  
  protected boolean sendComment( )
  {
    StringBuffer  body          = new StringBuffer("");
    String        crlf          = "\r\n";
    boolean       retVal        = false;
    
    try
    {
      body.append("Merchant ID  : ");
      body.append(getData("merchantId"));
      body.append(crlf);
      
      body.append("Name         : ");
      body.append(getData("firstName"));
      body.append(" ");
      body.append(getData("lastName"));
      body.append(crlf);
      
      body.append("City, State  : ");
      body.append(getData("city"));
      body.append(" ");
      body.append(getData("state"));
      body.append(crlf);
      
      body.append("Phone Number : ");
      body.append(PhoneField.format(getData("phone")));
      body.append(crlf);
      
      body.append("Email Address: ");
      body.append(getData("email"));
      body.append(crlf);
      body.append(crlf);
      
      body.append(getData("comments"));
      
      MailMessage msg = new MailMessage();
      msg.setAddresses( MesEmails.MSG_ADDRS_TPG_COMMENT );
      msg.setSubject("TPG - " + getData("commentType"));
      
      msg.setText(body.toString());
      msg.send();
      
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("sendComment()", e.toString());
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/