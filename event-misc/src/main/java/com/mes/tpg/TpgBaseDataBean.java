/*@lineinfo:filename=TpgBaseDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.61.151/svn/mesweb/branches/te/src/main/com/mes/tpg/TpgBaseDataBean.sqlj $

  Description:


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-01-23 15:10:34 -0800 (Thu, 23 Jan 2014) $
  Version            : $Revision: 22205 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tpg;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.api.ApiDb;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiProfile;
import com.mes.api.TridentApiTransaction;
import com.mes.constants.MesMenus;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.reports.ReportSQLJBean;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public abstract class TpgBaseDataBean extends ReportSQLJBean
{
  public static final int         SB_HOLD         = 1;
  public static final int         SB_DATE         = 2;
  public static final int         SB_TYPE         = 3;
  public static final int         SB_STATUS       = 4;
  public static final int         SB_CARD_NUMBER  = 5;
  public static final int         SB_AMOUNT       = 6;
  public static final int         SB_AUTH_CODE    = 7;
  public static final int         SB_INVOICE      = 8;
  public static final int         SB_AVS          = 9;
  public static final int         SB_CVV2         = 10;
  public static final int         SB_CUSTOMER     = 11;
  public static final int         SB_FLAG         = 12;
  public static final int         SB_MEMO         = 13;
  public static final int         SB_ENTRY        = 14;
  public static final int         SB_DBA_NAME     = 15;
  public static final int         SB_FX_AMOUNT    = 16;

  public static class TpgTotals
  {
    private double  Amount    = 0.0;
    private int     Count     = 0;
    String batchDate = "";

    public TpgTotals()
    {
    }

    public TpgTotals( int count, double amount )
    {
      setTotals(count,amount);
    }

    public double getAmount() { return(Amount); }
    public int getCount() { return(Count); }

    public void setAmount(double amount) { Amount = amount; }
    public void setCount(int count) { Count = count; }
    
    public String getBatchDate() {
      return batchDate;
    }

    public void setBatchDate(String date) {
      this.batchDate = date;
    }

    public void setTotals( int count, double amount )
    {
      setAmount(amount);
      setCount(count);
    }
  }

  public class RowData extends TridentApiTransaction
    implements Comparable
  {
    public    Date        BatchDate     = null;
    public    String      cardType      = null;

    public RowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super(resultSet);

      BatchDate = resultSet.getDate("batch_date");
      cardType  = resultSet.getString("card_type");
    }

    public String getCardType()
    {
      if(cardType != null)
      {
        return cardType;
      }
      return super.getCardType();
    }

    public int compareTo( Object obj )
    {
      double    amountDelta = 0.0;
      RowData   compareObj  = (RowData)obj;
      int       curOrder    = getInt("sortOrder");
      int       order       = Math.abs(curOrder);
      int       retVal      = 0;

      // first level sort is always by currency
      if ( (retVal = getCurrencyCodeAlpha().compareTo(compareObj.getCurrencyCodeAlpha())) == 0 )
      {
        switch( order )
        {
          case SB_HOLD:
            break;

          case SB_DATE:
            retVal = getTranDate().compareTo(compareObj.getTranDate());
            break;

          case SB_TYPE:
            retVal = getTranTypeDesc().compareTo(compareObj.getTranTypeDesc());
            break;

          case SB_DBA_NAME:
            retVal = getDbaName().compareTo(compareObj.getDbaName());
            break;

          case SB_STATUS:
            break;

          case SB_CARD_NUMBER:
            retVal = getCardNumberShort().compareTo(compareObj.getCardNumberShort());
            break;

          case SB_AMOUNT:
            amountDelta = (getTranAmount() - compareObj.getTranAmount());
            if ( amountDelta > 0.0 )
            {
              retVal = 1;
            }
            else if ( amountDelta < 0.0 )
            {
              retVal = -1;
            }
            break;

          case SB_AUTH_CODE:
            retVal = getAuthCode().compareTo(compareObj.getAuthCode());
            break;

          case SB_INVOICE:
            retVal = getPurchaseId().compareTo(compareObj.getPurchaseId());
            break;

          case SB_AVS:
            retVal = getAuthAvsResponse().compareTo(compareObj.getAuthAvsResponse());
            break;

          case SB_CVV2:
            break;

          case SB_CUSTOMER:
            break;

          case SB_FLAG:
            retVal = getTestFlag().compareTo(compareObj.getTestFlag());
            break;

          case SB_MEMO:
            break;

          case SB_ENTRY:
            retVal = getPosEntryMode().compareTo(compareObj.getPosEntryMode());
            break;

          case SB_FX_AMOUNT:
            amountDelta = (getFxAmount() - compareObj.getFxAmount());
            if ( amountDelta > 0.0 )
            {
              retVal = 1;
            }
            else if ( amountDelta < 0.0 )
            {
              retVal = -1;
            }
            break;
        }

        if ( retVal == 0 )
        {
          Date tranDate = getTranDate();
          if ( (retVal = tranDate.compareTo(compareObj.getTranDate())) == 0 )
          {
            String tranId = getTridentTranId();
            retVal = tranId.compareTo(compareObj.getTridentTranId());
          }
        }

        if ( curOrder < 0 )
        {
          retVal *= -1;
        }
      }

      return( retVal );
    }

    public Date getBatchDate()
    {
      return( BatchDate );
    }

    public String getCardNumberShort()
    {
      StringBuffer    retVal    = new StringBuffer();
      String          temp      = getCardNumber();

      if ( temp != null && temp.length() >= 8 )
      {
        retVal.append(temp.substring(0,4));
        retVal.append("xx");
        retVal.append(temp.substring( temp.length()-4 ) );
      }
      return( retVal.toString() );
    }
  }

  public class ModeTable extends DropDownTable
  {
    int menuType = HttpHelper.getInt(request, "menu_type", -1);

    public ModeTable( )
    {
      addElement("",  "Normal");
      if (menuType != 2 )
      {
        addElement("GS",  "Group Settle");
      }
      addElement("GV",  "Group Void");
    }
  }

  public class SearchOptionsTable extends DropDownTable
  {
    public SearchOptionsTable( )
    {
      addElement ("select", "select");
      addElement ("IN", "Invoice Number");
      addElement ("CN", "Card #-Last 4");
      addElement ("TA", "Tran Amount");
    }
  }

  public class ProfileTable extends DropDownTable
  {
    private String  PrimaryProfile    = null;
    private int     ProfileCount      = 0;

    public ProfileTable( long nodeId, int menuId )
    {
      ResultSetIterator   it        = null;
      ResultSet           rs        = null;

      try
      {
        connect();

        addElement("",  "select");
        ProfileCount = 0;

        /*@lineinfo:generated-code*//*@lineinfo:312^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.terminal_id                              as profile_id,
//                    tp.terminal_id || ' - ' || tp.merchant_name as profile_desc
//            from    organization          o,
//                    group_merchant        gm,
//                    trident_profile       tp,
//                    trident_product_codes tpc
//            where   o.org_group = :nodeId 
//                    and gm.org_num = o.org_num 
//                    and tp.merchant_number = gm.merchant_number 
//                    and nvl(tp.api_enabled,'N') = 'Y' 
//                    and tp.profile_status != 'C' 
//                    and
//                    ( :menuId = :MesMenus.MENU_ID_TPG_MAIN or
//                      :menuId = :MesMenus.MENU_ID_MINI_VT_MAIN and tp.product_code = 'VM' )
//                    and tpc.product_code = tp.product_code
//                    and nvl(tpc.trident_payment_gateway,'N') = 'Y'
//            order by tp.terminal_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tp.terminal_id                              as profile_id,\n                  tp.terminal_id || ' - ' || tp.merchant_name as profile_desc\n          from    organization          o,\n                  group_merchant        gm,\n                  trident_profile       tp,\n                  trident_product_codes tpc\n          where   o.org_group =  :1  \n                  and gm.org_num = o.org_num \n                  and tp.merchant_number = gm.merchant_number \n                  and nvl(tp.api_enabled,'N') = 'Y' \n                  and tp.profile_status != 'C' \n                  and\n                  (  :2  =  :3  or\n                     :4  =  :5  and tp.product_code = 'VM' )\n                  and tpc.product_code = tp.product_code\n                  and nvl(tpc.trident_payment_gateway,'N') = 'Y'\n          order by tp.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tpg.TpgBaseDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,menuId);
   __sJT_st.setInt(3,MesMenus.MENU_ID_TPG_MAIN);
   __sJT_st.setInt(4,menuId);
   __sJT_st.setInt(5,MesMenus.MENU_ID_MINI_VT_MAIN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tpg.TpgBaseDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:331^9*/
        rs = it.getResultSet();

        while( rs.next() )
        {
          addElement(rs);
          ++ProfileCount;

          if ( ProfileCount == 1 )    // first profile
          {
            PrimaryProfile = rs.getString("profile_id");
          }
        }
        rs.close();
        it.close();
      }
      catch( Exception e )
      {
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }

    public String getPrimaryProfile()
    {
      return( PrimaryProfile );
    }

    public int getProfileCount()
    {
      return( ProfileCount );
    }
  }

  public TpgBaseDataBean( )
  {
    super(true);    // use FieldBean support
  }

  protected void createFields(HttpServletRequest request)
  {
    FieldGroup    fgroup;
    Field         field;
    
    super.createFields(request);

    fgroup = (FieldGroup)getField("searchFields");
    
    if ( HttpHelper.getBoolean(request,"ajax",false) )
    {
      // ajax requests just need a field to hold the value
      fgroup.add( new HiddenField(TridentApiConstants.FN_TID) );
    }
    else    // add a drop down for the user to use
    {
      long hierarchyNode = HttpHelper.getLong(request,"nodeId",user.getHierarchyNode());
      int  menuId        = HttpHelper.getInt(request,"menu_type",MesMenus.MENU_ID_TPG_MAIN);
      ProfileTable table = new ProfileTable( hierarchyNode, menuId );
      field = new DropDownField( TridentApiConstants.FN_TID, "Profile ID", table, true );
      if ( table.getProfileCount() == 1 )
      {
        field.setData( table.getPrimaryProfile() );
        field.makeReadOnly();
      }
      fgroup.add( field );
    }
    fgroup.add( new HiddenField("sortOrder") );
    setData("sortOrder",String.valueOf(-SB_DATE));
    fgroup.add( new HiddenField("menu_type",String.valueOf(MesMenus.MENU_ID_TPG_MAIN)) );
  }

  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl(buffer,nodeId,beginDate,endDate);
    buffer.append("&profile_id=");
    buffer.append(getData("profile_id"));
  }

  protected void encodeTrailerCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\n\"Report Creation Date\",\"");
    line.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(), "dd-MMM-yyyy HH:mm:ss"));
    line.append("\"");
  }

  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }

  protected boolean isProductionRequest()
  {
    int         matchCount  = 0;

    try
    {
      String prodKey = HttpHelper.getString(ServletRequest,TridentApiConstants.FN_PROD_AUTH_KEY,null);

      if ( prodKey != null && !prodKey.equals("") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:443^9*/

//  ************************************************************
//  #sql [Ctx] { select sum( decode( generate_merchant_key(394100000),
//                                :prodKey, 1, 0 ) )    
//            from dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum( decode( generate_merchant_key(394100000),\n                               :1 , 1, 0 ) )     \n          from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tpg.TpgBaseDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,prodKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   matchCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:448^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("isProductionRequest()",e.toString());
    }
    return( (matchCount != 0) );
  }
  
  public boolean isValidProfileId( String pid )
  {
    int     recCount      = 0;
    long    reportNode    = getReportHierarchyNodeDefault();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:465^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    organization      o,
//                  group_merchant    gm,
//                  trident_profile   tp
//          where   o.org_group = :reportNode
//                  and gm.org_num = o.org_num
//                  and tp.merchant_number = gm.merchant_number
//                  and tp.terminal_id = :pid                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    organization      o,\n                group_merchant    gm,\n                trident_profile   tp\n        where   o.org_group =  :1 \n                and gm.org_num = o.org_num\n                and tp.merchant_number = gm.merchant_number\n                and tp.terminal_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tpg.TpgBaseDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,reportNode);
   __sJT_st.setString(2,pid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:476^7*/
    }
    catch( Exception e )
    {
      logEntry("isValidProfileId(" + pid + ")",e.toString());
    }
    return( recCount != 0 );
  }

  public TridentApiProfile loadProfile()
  {
    String profileId  = getData(TridentApiConstants.FN_TID);
    String nodeId     = ""+getReportHierarchyNodeDefault();
    return ApiDb.loadProfile(profileId,nodeId);
  }

  /**
   * HACK: determine URL str based on logged server name
   * Not sure what a better way to do this is...
   */
  protected String getTapiUrlFromServerName(String serverName)
  {
    if (serverName.indexOf("eng1078") > -1)
    {
      return "http://eng1078:8080/mes-api/tridentApi";
    }
    else if (serverName.indexOf("mesapi") > -1)
    {
      return "https://api.merchante-solutions.com/mes-api/tridentApi";
    }
    return "https://test.merchante-solutions.com/mes-api/tridentApi";
  }

  public String[] loadCredentials()
  {
    return loadCredentials( getData("profile_id") );
  }

  public String[] loadCredentials(String profileId)
  {
    String[]      credentials     = new String[2];
    String        pid             = profileId;

    try
    {
      if ( isValidProfileId(pid) )
      {
        credentials[0] = pid;
      
        /*@lineinfo:generated-code*//*@lineinfo:525^9*/

//  ************************************************************
//  #sql [Ctx] { select  merchant_key
//            
//            from    trident_profile   tp
//            where   tp.terminal_id = :credentials[0]      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1426 = credentials[0];
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merchant_key\n           \n          from    trident_profile   tp\n          where   tp.terminal_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tpg.TpgBaseDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1426);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   credentials[1] = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:531^9*/
      }
      else
      {
        credentials[0] = "invalid-profile-id";
        credentials[1] = "invalid-profile-key";
      }
    }
    catch( Exception e )
    {
      logEntry( "loadCredentials()",e.toString());
    }
    return( credentials );
  }

  public String[] loadCredentialsForTrans(String transId) {
    String pid = "";
    if(transId!=null && transId.length()>0) {
      try {
        /*@lineinfo:generated-code*//*@lineinfo:550^9*/

//  ************************************************************
//  #sql [Ctx] { select  terminal_id
//            
//            from    trident_capture_api
//            where   trident_tran_id = :transId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  terminal_id\n           \n          from    trident_capture_api\n          where   trident_tran_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tpg.TpgBaseDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,transId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   pid = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:556^9*/
      } catch( Exception e )
      {
        logEntry( "loadCredentialsForTrans()",e.toString());
      }
    }

    return loadCredentials(pid);
  }

  public String[] loadCredentialsForTrans() {
    String transId = HttpHelper.getString(ServletRequest,"transId",null);

    return loadCredentialsForTrans(transId);
  }
  
  public int profileCount( long merchantId, int menuId )
  {
    int profileCount = 0;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:580^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(tp.terminal_id) 
//          from    organization          o,
//                  group_merchant        gm,
//                  trident_profile       tp
//          where   o.org_group = :getReportHierarchyNode() and
//                  gm.org_num = o.org_num and
//                  gm.merchant_number = :merchantId and
//                  tp.merchant_number = gm.merchant_number and
//                  nvl(tp.api_enabled,'N') = 'Y' and
//                  tp.profile_status != 'C' and
//                  ( :menuId = :MesMenus.MENU_ID_TPG_MAIN or
//                    :menuId = :MesMenus.MENU_ID_MINI_VT_MAIN and tp.product_code = 'VM' )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1427 = getReportHierarchyNode();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(tp.terminal_id)  \n        from    organization          o,\n                group_merchant        gm,\n                trident_profile       tp\n        where   o.org_group =  :1  and\n                gm.org_num = o.org_num and\n                gm.merchant_number =  :2  and\n                tp.merchant_number = gm.merchant_number and\n                nvl(tp.api_enabled,'N') = 'Y' and\n                tp.profile_status != 'C' and\n                (  :3  =  :4  or\n                   :5  =  :6  and tp.product_code = 'VM' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tpg.TpgBaseDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1427);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setInt(3,menuId);
   __sJT_st.setInt(4,MesMenus.MENU_ID_TPG_MAIN);
   __sJT_st.setInt(5,menuId);
   __sJT_st.setInt(6,MesMenus.MENU_ID_MINI_VT_MAIN);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   profileCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:594^7*/
    }
    catch( Exception e )
    {
    }
    
    return( profileCount );
  }
}/*@lineinfo:generated-code*/