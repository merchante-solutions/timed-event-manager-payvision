package com.mes.acr;

import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;

public final class ACRDataBean_EquipSwap_CBT extends ACRDataBean_EquipSwap
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_EquipSwap_CBT.class);

  public ACRDataBean_EquipSwap_CBT()
  {
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    // equip call tag table
    int[] ecctColumns = new int[]
      {
         EquipCallTagTable.COL_SENDCALLTAG
        ,EquipCallTagTable.COL_EQUIPTYPE
        ,EquipCallTagTable.COL_DESCRIPTION
        ,EquipCallTagTable.COL_VNUMBER
      };
    String[][] ecctMap = new String[][]
      {
         {EquipCallTagTable.NCN_SENDCALLTAG,   "equipsel"}
        ,{EquipCallTagTable.NCN_EQUIPTYPE,     "tt"}
        ,{EquipCallTagTable.NCN_DESCRIPTION,   "equip_descriptor"}
        ,{EquipCallTagTable.NCN_VNUMBER,       "vnum"}
      };
    ectt = new EquipCallTagTable(this, ecctColumns, ecctMap, MAX_NUM_SWAPS, true);
  }

  // construction
  public ACRDataBean_EquipSwap_CBT(ACR acr,MerchantInfo merchantInfo,Requestor requestor,boolean editable)
    throws Exception
  {
    super(acr,merchantInfo,requestor,editable);

    // equip call tag table
    int[] ecctColumns = new int[]
      {
         EquipCallTagTable.COL_SENDCALLTAG
        ,EquipCallTagTable.COL_EQUIPTYPE
        ,EquipCallTagTable.COL_DESCRIPTION
        ,EquipCallTagTable.COL_VNUMBER
      };
    String[][] ecctMap = new String[][]
      {
         {EquipCallTagTable.NCN_SENDCALLTAG,   "equipsel"}
        ,{EquipCallTagTable.NCN_EQUIPTYPE,     "tt"}
        ,{EquipCallTagTable.NCN_DESCRIPTION,   "equip_descriptor"}
        ,{EquipCallTagTable.NCN_VNUMBER,       "vnum"}
      };
    ectt = new EquipCallTagTable(this, ecctColumns, ecctMap, MAX_NUM_SWAPS, true);
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Ship To");
    adrsHelper.setAdrsSlctnFldName("staddress");
    //adrsHelper.setAdrsName("");
    adrsHelper.setAdrsAddressFldNme("sh_address");
    adrsHelper.setAdrsAttnFldNme("sh_attn");
    adrsHelper.setAdrsBankNameFldNme("sh_bnknme");
    adrsHelper.setAdrsCityFldNme("sh_city");
    adrsHelper.setAdrsStateFldNme("sh_state");
    adrsHelper.setAdrsZipFldNme("sh_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(false);

    // cb&t specific filtering
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_BUSADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_MAILINGADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_EQUIPDELADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_OWNER1ADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_OWNER2ADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_OWNER3ADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_FULFILLADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_CONTACTADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_BANDADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MIF_BT1);

    return adrsHelper;
  }

  protected void setDefaults()
  {
    // parent related info
    if(getMerchantNumber().length()>0) {

      // merchant address
      _getAddressHelper().setACRAdrsInfo();

    }

    // default to rush request
    acr.setACRItem("Rush","y");
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String acrdiname=null;

    // address fields
    _getAddressHelper().createAddressFields();

    CBTSwapReasonDropDownTable srddt = new CBTSwapReasonDropDownTable();
    //RepairHandlingDropDownTable rhddt = new RepairHandlingDropDownTable();
    CBTShippingFeeDropDownTable sfddt = new CBTShippingFeeDropDownTable();

    if((acrdi=acrd.getACRDefItem("swap_reason"))!=null) {
      field=new DropDownField(acrdi.getName(),srddt,!acrdi.isRequired());
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("swap_reason_details"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    /*
    if((acrdi=acrd.getACRDefItem("repair_handling"))!=null) {
      field=new DropDownField(acrdi.getName(),rhddt,!acrdi.isRequired());
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    */
    if((acrdi=acrd.getACRDefItem("shipping_fee"))!=null) {
      field=new DropDownField(acrdi.getName(),sfddt,true);
      field.setShowName("Repair Fee");
      fields.add(field);
    }

    ectt.createFields(this.fields);

    Vector swaps = new Vector(MAX_NUM_SWAPS,1);

    // equip swap fields
    for(int i=1;i<=MAX_NUM_SWAPS;i++) {

      if((acrdi=acrd.getACRDefItem("equipsel_"+i))!=null) {
        field = getField("equipsel_"+i);
        swaps.addElement(field);
      }

    }

    // add "at least one" validation for swaps
    fields.addValidation(new FieldBean.AtLeastOneValidation("At least one equipment swap must be specified to proceed.",swaps));
    fields.addValidation(new CBTEntryValidation());
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}

      // link to Activation queue from any CBT queue
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      // link to Cancelled queue from any CBT queue
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected class CBTEntryValidation implements Validation
  {
    private String errorText = "";

    public CBTEntryValidation()
    {
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld;
      String data;

      // require the $150 repair fee for merchant owned failed equipment

      fld = fields.getField("swap_reason");
      data = fld.getData();

      if(data.equals("Equipment not working - merchant owned")) {
        fld = fields.getField("shipping_fee");
        if(fld.isBlank()) {
          errorText = "The repair fee must be specified for merchant owned equipment.";
          return false;
        }
      }

      return true;
    }

  } // class CBTEntryValidation

  private class CBTSwapReasonDropDownTable extends DropDownTable
  {
    public CBTSwapReasonDropDownTable()
    {
      addElement("","");

      addElement("Equipment not working - merchant owned","Equipment not working - merchant owned");
      addElement("Equipment not working - rented","Equipment not working - rented");
      addElement("Equipment not working - purchased and under warranty","Equipment not working - purchased and under warranty");
      addElement("Encryption for pinpad","Encryption for pinpad");
    }

  } // SwapReasonDropDownTable class

  private class CBTShippingFeeDropDownTable extends DropDownTable
  {
    public CBTShippingFeeDropDownTable()
    {
      addElement("","");

      addElement("$150","$150");
    }

  } // CBTShippingFeeDropDownTable

  public String getHeadingName()
  {
    return "Order Equipment Swap for CB&T Merchant";
  }

  public String getJSPIncludeFilename()
  {
    return (acr==null)? "":"acr_incl_"+acr.getDefinition().getShortName()+"_cbt.jsp";
  }

} // class ACRDataBean_EquipSwap_CBT
