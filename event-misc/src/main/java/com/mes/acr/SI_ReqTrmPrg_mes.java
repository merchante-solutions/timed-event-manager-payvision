package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.equipment.EquipSelectorItem;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HtmlBlock;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.HtmlSpan;
import com.mes.forms.HtmlText;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;

public class SI_ReqTrmPrg_mes extends EquipSelectorItem
  implements ACRTranslator
{
  static Logger log = Logger.getLogger(SI_ReqTrmPrg.class);

  public SI_ReqTrmPrg_mes()
  {
  }

  public abstract static class TextRenderer implements HtmlRenderable
  {
    public String renderHtml()
    {
      HtmlSpan span = new HtmlSpan();
      span.setProperty("style","line-height: 1.8em");
      span.add(getText());
      return span.renderHtml();
    }

    public abstract String getText();
  }

  public void init()
  {
    if (type == T_STANDARD)
    {
      setColumnRenderer("col1", new TextRenderer()
        { public String getText() { return getDescription(); } } );
      setColumnRenderer("col2", new TextRenderer()
        { public String getText() { return getSerialNum(); } } );
    }
    else if (type == T_OTHER)
    {
      Field description
        = new Field(value + "_description","Description",40,16,true);
      HtmlBlock newDesc = new HtmlBlock();
      newDesc.add(new HtmlText("New: "));
      newDesc.add(description);
      setColumnRenderer("col1",newDesc);
      fields.add(description);

      Field serialNum = new Field(value + "_serNum","Serial number",20,11,true);
      setColumnRenderer("col2",serialNum);
      fields.add(serialNum);
    }

    Field termApp = new Field(value + "_termApp","Terminal App",10,7,true);
    setColumnRenderer("col3",termApp);
    fields.add(termApp);
/*
    Field termType = new DropDownField(value + "_termType","Terminal Type",new _ddTermTypeTable(),true);
    setColumnRenderer("col4",termType);
    fields.add(termType);
*/
    Field vendor = new DropDownField("Vendor","Vendor",TableFactory.getDropDownTable(TableFactory.VENDOR),true);
    setColumnRenderer("col4",vendor);
    fields.add(vendor);

    Field vNumber = new Field(value + "_vNumber","V Number",8,7,true);
    setColumnRenderer("col5",vNumber);
    fields.add(vNumber);

    fields.setHtmlExtra("class=\"formText\"");
  }

  public void setControlField(Field controlField)
  {
    super.setControlField(controlField);

    // in the case of other items need to make some of the fields required
    if (type == T_OTHER)
    {
      Condition vc = new FieldValueCondition(controlField,value);
      Validation required = new ConditionalRequiredValidation(vc);
      fields.getField(value + "_serNum").addValidation(required);
      fields.getField(value + "_description").addValidation(required);
    }
  }

  public void doPush(ACR acr)
  {
    log.debug("pretending to push acr item data...");
  }

  public void doPull(ACR acr)
  {

    try
    {
      acr.setACRItem(value+"_serNum",getSerialNum());
      acr.setACRItem(value+"_description",getDescription());
    }
    catch(Exception e)
    {
      //do nothing
    }
  }

  private static class _ddTermTypeTable extends DropDownTable
  {
    public _ddTermTypeTable()
    {
      addElement("","Select One");

      addElement("Terminal","Terminal");
      addElement("Terminal/Printer","Terminal/Printer");
      addElement("Terminal/Printer/Pinpad","Terminal/Printer/Pinpad");
      addElement("Pinpad","Pinpad");
      addElement("Printer","Printer");
    }

  } // _ddTermAppTable class
}
