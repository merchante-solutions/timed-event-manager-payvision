/*@lineinfo:filename=ACROBJMESDB*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.sql.ResultSet;
// log4j classes.
import org.apache.log4j.Logger;
import sqlj.runtime.ResultSetIterator;

public class ACROBJMESDB extends com.mes.database.SQLJConnectionBase
{

  // create class log category
  static Logger log = Logger.getLogger(ACROBJMESDB.class);

  // construction
  public ACROBJMESDB()
  {
    super(false);
  }

  public void test()
  throws Exception
  {
    log.debug("START:test()");

    ResultSet rs = null;
    ResultSetIterator rsItr = null;

    try{
      log.debug("before:connect()");

      if(isConnectionStale()){
        connect();
      }

      log.debug("before:sql");

      /*@lineinfo:generated-code*//*@lineinfo:46^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//            supply_item_id AS id
//          FROM
//            supply_item
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n          supply_item_id AS id\n        FROM\n          supply_item";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACROBJMESDB",theSqlTS);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.ACROBJMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:52^7*/

      log.debug("before:rsItr.getResultSet()");

      rs = rsItr.getResultSet();

      log.debug("before:while(rs.next())");

      while(rs.next()){
        log.debug("ID = "+rs.getString("id"));
      }

    }catch(Exception e) {
      e.printStackTrace();
      throw e;
    }finally {
      try { rs.close(); }    catch(Exception e) {}
      try { rsItr.close(); } catch(Exception e) {}
      cleanUp();
    }

    log.debug("STOP:test()");
  }

}/*@lineinfo:generated-code*/