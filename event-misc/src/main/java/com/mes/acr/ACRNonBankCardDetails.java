/*@lineinfo:filename=ACRNonBankCardDetails*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.sql.ResultSet;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class ACRNonBankCardDetails extends com.mes.maintenance.NonBankCardDetails
{
  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(ACRNonBankCardDetails.class);

  private  String  valutecTID        = "N/A";
  private  String  certegyTID        = "N/A";
  private  String  certegyProvider   = "N/A";

  protected void loadData()
  {

    super.loadData();

    log.debug("will add additionals here...");

    ResultSetIterator     it      = null;
    ResultSet             rs      = null;

    try
    {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:38^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mpo.cardtype_code                     as card_type,
//                  nvl(mpo.merchpo_tid,'N/A')            as tid,
//                  nvl(mpo.merchpo_provider_name,'N/A')  as check_provider
//          from    merchpayoption  mpo,
//                  merchant        m
//          where   m.merch_number = :merchantNumber and
//                  m.app_seq_num = mpo.app_seq_num and
//                  mpo.cardtype_code in (:mesConstants.APP_CT_CHECK_AUTH,
//                                        :mesConstants.APP_CT_VALUTEC_GIFT_CARD)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mpo.cardtype_code                     as card_type,\n                nvl(mpo.merchpo_tid,'N/A')            as tid,\n                nvl(mpo.merchpo_provider_name,'N/A')  as check_provider\n        from    merchpayoption  mpo,\n                merchant        m\n        where   m.merch_number =  :1  and\n                m.app_seq_num = mpo.app_seq_num and\n                mpo.cardtype_code in ( :2 ,\n                                       :3 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRNonBankCardDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setInt(2,mesConstants.APP_CT_CHECK_AUTH);
   __sJT_st.setInt(3,mesConstants.APP_CT_VALUTEC_GIFT_CARD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.ACRNonBankCardDetails",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:49^7*/

      rs = it.getResultSet();

      while(rs.next())
      {

        int cardType = rs.getInt("card_type");

        log.debug("adding info of type..."+cardType);

        switch(cardType)
        {
          case mesConstants.APP_CT_CHECK_AUTH:
            certegyTID = rs.getString("tid");
            certegyProvider = rs.getString("check_provider");
            break;

          case mesConstants.APP_CT_VALUTEC_GIFT_CARD:
            valutecTID = rs.getString("tid");
            break;
        }
      }

      rs.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("loadData(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public String getAmexDetails()
  {
    return amexMerchantNumber;
  }

  public String getDiscoverDetails()
  {
    return discoverMerchantNumber;
  }

  public String getCertegyDetails()
  {
    return certegyProvider +": "+ certegyTID;
  }

  public String getValutecDetails()
  {
    return valutecTID;
  }

}/*@lineinfo:generated-code*/