package com.mes.acr;

public abstract class DAOFactoryBuilder
{
  // constants
  // (none)
  
  // class functions
  
  /**
   * getDAOFactory()
   * 
   * Public access point for obtaining the desired DAO factory.
   */
  public static DAOFactory getDAOFactory(int type)
  {
    switch(type) {
      case DAOFactory.DAO_MESDB:
        return mesdbDAOFactory.getInstance();
      default:
        return null;
    }
  }
 
  // object functions
  
  // construction
  private DAOFactoryBuilder()
  {
  }
  
} // class DAOFactory
