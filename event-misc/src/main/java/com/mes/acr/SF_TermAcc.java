package com.mes.acr;

import com.mes.forms.SelectorColumnDef;

public class SF_TermAcc extends SF_EquipBase
{
  public SF_TermAcc(String title)
  {
    super(title);
  }

  public void setupFrame()
  {
    addColumn(new SelectorColumnDef("col1","Serial No.",      200));
    addColumn(new SelectorColumnDef("col2","Description",     200));
    addColumn(new SelectorColumnDef("col3","Equipment Type",  200));
  }

}