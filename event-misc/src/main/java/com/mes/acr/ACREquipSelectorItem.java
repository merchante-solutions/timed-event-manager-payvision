package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.equipment.EquipSelectorItem;
import com.mes.forms.Condition;
import com.mes.forms.Field;
import com.mes.forms.Validation;

public class ACREquipSelectorItem extends EquipSelectorItem
  implements ACRTranslator
{
  static Logger log = Logger.getLogger(ACREquipSelectorItem.class);

  public ACREquipSelectorItem()
  {
  }

  public static final int COL1 = 105;
  public static final int COL2 = 200;
  public static final int COL3 = 105;
  public static final int COL4 = 125;
  public static final int COL5 = 150;

  public void processData()
  {
    htmlContainer.add(createDiv(getSerialNum(),COL1));
    htmlContainer.add(createDiv(getDescription(),COL2));
    htmlContainer.add(createDiv(getPartNum(),COL3));
    htmlContainer.add(createDiv(getDisposition(),COL4));

    Field reason = new Field(value + "_reason","Reason for calltag",60,20,true);

    fields.add(reason);
    htmlContainer.add(createDiv(reason,COL5));
  }

  public void setControlField(Field controlField)
  {
    super.setControlField(controlField);
    Condition vc = new FieldValueCondition(controlField,value);
    Validation required = new ConditionalRequiredValidation(vc);
    fields.addGroupValidation(required);
  }


  public static ACREquipSelectorItem generateOtherSelector(String prefix, int idx)
  {
    ACREquipSelectorItem other = new ACREquipSelectorItem();

    String value = prefix + "_other_" + idx;
    other.setValue(value);

    Field serialNum = new Field(value + "_serNum","Serial number",20,10,true);
    other.htmlContainer.add(createDiv(serialNum,COL1));
    other.fields.add(serialNum);

    Field description = new Field(value + "_description","Equipment description",60,20,true);
    other.htmlContainer.add(createDiv(description,COL2));
    other.fields.add(description);

    Field partNum = new Field(value + "_partNum","Part number",20,10,true);
    other.htmlContainer.add(createDiv(partNum,COL3));
    other.fields.add(partNum);

    Field disposition = new Field(value + "_disposition","Disposition",20,12,true);
    other.htmlContainer.add(createDiv(disposition,COL4));
    other.fields.add(disposition);

    Field reason = new Field(value + "_reason","Reason for calltag",60,20,true);
    other.htmlContainer.add(createDiv(reason,COL5));
    other.fields.add(reason);

    return other;
  }

  public void doPush(ACR acr)
  {
    log.debug("pretending to push acr item data...");
  }

  public void doPull(ACR acr)
  {
    log.debug("pretending to pull acr item data...");
  }

@Override
public void init() {
	// TODO Auto-generated method stub
	
}
}

