package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.Condition;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.Validation;
import com.mes.support.NumberFormatter;
import com.mes.tools.DropDownTable;


public final class ACRDataBean_FeeReversal_CBT_v2 extends ACRDataBean_FeeReversalBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_FeeReversal_CBT_v2.class);

  // constants
  private int MAX_ALLOWED_ENTRIES = 5;
  private float totalFeeAmount = 0.0f;

  // construction
  public ACRDataBean_FeeReversal_CBT_v2()
  {
  }

  public ACRDataBean_FeeReversal_CBT_v2(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    if(!editable)
    {
      calcTotalFeeReversals();
    }
  }

  //to control the number of changes surfaced on the JSP
  public int getMaxEntries(){
    return MAX_ALLOWED_ENTRIES;
  }

  protected void createExtendedFields()
  {
    if(acr==null)
      return;

    //create line items based on max entries constant
    if(editable)
    {

      //fee type and month dropdowns
      FeeTypeDropDownTable feeTypeDropDown = new FeeTypeDropDownTable();
      MonthDropDownTable monthDropDown = new MonthDropDownTable();

      //placeholders for loop
      String suffix;
      DropDownField feeDDF;
      DropDownField monthDDF;
      Field amtField;
      Condition isBlank;
      Condition isNotBlank;
      Validation isRequired;

      AtLeastOneValidation lVal = new AtLeastOneValidation("You must select at least one Fee Type.");

      //now build a field-construct for each of the max allowed entries
      for (int i=1; i<=getMaxEntries();i++)
      {

       //build suffix
       suffix = "_"+ i;

       //create feeType and month fields
       feeDDF = new DropDownField("feeType"+suffix, feeTypeDropDown, true);
       monthDDF = new DropDownField("month"+suffix, "Month",  monthDropDown, true);

       //use a currency field for the amount
       //8 represents the length of currency 000,000.00
       //and the two floats are lower and upper limits
       amtField = new CurrencyField("amount"+suffix,"Reversal Amount",8,10, true, 0f, 999999f);

       fields.add(feeDDF);
       fields.add(monthDDF);
       fields.add(amtField);

       //add the feeType field to the AtLeastOneValidation
       lVal.addField(feeDDF);

       //add validation for fee type field, if selected
       isBlank = new FieldValueCondition(feeDDF,"");
       isNotBlank = new NotCondition(isBlank);
       isRequired = new ConditionalRequiredValidation(isNotBlank);
       monthDDF.addValidation(isRequired);
       amtField.addValidation(isRequired);

       log.debug("building validation");
     }

     //add the AtLeastOneValidation
     fields.addValidation(lVal);

    }
    else
    {
      super.createExtendedFields();
    }

  }

  public boolean doCommand(String method)
  {
    log.debug("doCommand() method='"+method+"'");
    if(method.equals("Calculate Fee Reversals")) {
      super.doPull();
      calcTotalFeeReversals();
      return true;
    } else {
      log.error("Unhandled Fee Reversal Request: '"+method+"'.");
      return false;
    }
  }

  protected void calcTotalFeeReversals()
  {
    String fVal;
    float total = 0.0f;

    for (int i=1; i<=getMaxEntries();i++)
    {
      if(editable)
      {
        fVal=fields.getData("amount_"+i);
      }
      else
      {
        fVal = getACRValue("amount_"+i);
      }
      try
      {
        if( fVal != null )
        {
          total += Float.valueOf(fVal).floatValue();
        }
      }
      catch(Exception e)
      {
      }
    }

    totalFeeAmount = total;
  }

  public float getTotalFeeReversalAmount()
  {
    return totalFeeAmount;
  }

  public String getTotalFeeReversalAmountString()
  {
    return NumberFormatter.getDoubleString((double)totalFeeAmount,NumberFormatter.CURRENCY_FORMAT);
  }

  //standard CBT queue ops definition
  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

/*
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
*/
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_PRICING}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}

//ASG Mgmt queue
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
   };
  }

  public String getHeadingName()
  {
    return "Reverse Merchant Fees";
  }

  public class MonthDropDownTable extends DropDownTable
  {

    public MonthDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","  --  ");

      addElement("jan","JAN");
      addElement("feb","FEB");
      addElement("mar","MAR");
      addElement("apr","APR");
      addElement("may","MAY");
      addElement("jun","JUN");
      addElement("jul","JUL");
      addElement("aug","AUG");
      addElement("sep","SEP");
      addElement("oct","OCT");
      addElement("nov","NOV");
      addElement("dec","DEC");

    }

  }

} // class ACRDataBean_FeeReversal_CBT_v2
