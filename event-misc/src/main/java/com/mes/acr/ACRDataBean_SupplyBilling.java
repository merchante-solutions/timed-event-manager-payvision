/*@lineinfo:filename=ACRDataBean_SupplyBilling*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/acr/ACRDataBean_SupplyBilling.sqlj $

  Description:

    ACRDataBean_SupplyBilling

    Allows user to change supply billing status.

  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 8/27/04 5:06p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.acr;

import java.sql.ResultSet;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import sqlj.runtime.ResultSetIterator;


public class ACRDataBean_SupplyBilling extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_SupplyBilling.class);
  private double MIN_QUALIFIER = 5000.00d;
  private String grinText = "";

  // construction
  public ACRDataBean_SupplyBilling()
  {
  }

  public ACRDataBean_SupplyBilling(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected void setDefaults()
  {
    // no-op
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    grinText = generateGRIN();

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String fldnme;

    String[][] ApplyExistingRadioButtons =
    {
       {"Supplies charged on a per order basis", "per_order"}
      ,{"Supplies charged at monthly flat rate of $8.95", "monthly"}
      ,{"Waive billing for supply orders ("+getGRINText()+")", "waive"}
    };

    try {

      acrdi=acrd.getACRDefItem("supplyBillingConfig");
      field = new RadioButtonField("supplyBillingConfig", ApplyExistingRadioButtons, 0, false, "Please specify supply order preference" );
      fields.add(field);

      //fields.addValidation(new GRINValidation());
      fields.addValidation(new OutstandingOrderValidation());
    }
    catch(Exception e) {
      return;
    }

    return;
  }

  public boolean doCommand(String method)
  {
    log.info("doCommand() method='"+method+"'");

    if( method.equals("Submit Request") )
    {

      if(isValid()){
        acr.setStatus(ACR.ACRSTATUS_VALID);
      }else{
        acr.setStatus(ACR.ACRSTATUS_INVALID);
      }
      return true;
    }
    else
    {
      log.error("Unhandled Supply Order Request: '"+method+"'.");
      return false;
    }

  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_RESEARCH}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_PRICING}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}

      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Merchant Supply Orders: Billing Configuration";
  }
/*
  private class GRINValidation implements Validation
  {
    public GRINValidation()
    {
    }

    public String getErrorText()
    {
      return "Unable to waive supply charges based on GRIN calculations - please select another billing option.";
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      log.info("GRINValidation.validate..");
      boolean isValid = true;

      Field field= fields.getField("supplyBillingConfig");
      fieldData = field==null?"":field.getData();

      if( fieldData!=null && fieldData.equals("waive"))
      {
        isValid = GRINTest();
      }
      return isValid;
    }

  } // GRINValidation

  private boolean GRINTest()
  {
    log.info("GRINTest..");
    ResultSet rs = null;
    ResultSetIterator it = null;

    boolean passed = false;

    try
    {
      connect();

      #sql[Ctx] it =
      {
        select  sm.active_date                                                  activity_month,
                ((gn.T1_TOT_INC_IND_PLANS) +
                  (gn.t1_tot_calculated_discount) +
                  (gn.T1_TOT_INC_INTERCHANGE) +
                  (gn.T1_TOT_INC_SYS_GENERATED) +
                  (gn.T1_TOT_INC_DEBIT_NETWORKS ) +
                  (gn.T1_TOT_INC_Authorization) +
                  (gn.T1_TOT_INC_Capture)) -
                  ((sm.INTERCHANGE_EXPENSE)+ (sm.VMC_ASSESSMENT_EXPENSE) ) +
                  ((sm.FEE_DCE_ADJ_AMOUNT+
                  sm.DISC_IC_DCE_ADJ_AMOUNT) ) - (.14 * sm.vmc_sales_count)     GRIN
        from    monthly_extract_summary   sm,
                monthly_extract_gn        gn
        where   sm.active_date between add_months(trunc(sysdate, 'MM'), -3) and trunc(trunc(sysdate, 'MM') - 1, 'MM') and
                sm.merchant_number = :(getMerchantNumber()) and
                sm.hh_load_sec = gn.hh_load_sec
      };

      int counter = 0;
      double total = 0d;

      rs = it.getResultSet();

      while (rs.next())
      {
        total += rs.getDouble("GRIN");
        counter++;
      }

      if( total/counter > MIN_QUALIFIER)
      {
        passed = true;
      }

    }
    catch(Exception e)
    {
      //do nothing - test failed
    }
    finally
    {
      cleanUp();
    }

    return passed;
  }
*/

  private class OutstandingOrderValidation implements Validation
  {
    public OutstandingOrderValidation()
    {
    }

    public String getErrorText()
    {
      return "Unable to issue billing change request at this time - outstanding orders found on system";
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      log.info("OutstandingOrderValidation.validate..");
      boolean isValid = true;
      try{

        connect();

        int orderCount=0;

        /*@lineinfo:generated-code*//*@lineinfo:300^9*/

//  ************************************************************
//  #sql [Ctx] { select
//              count(supply_order_id) 
//            from
//              supply_order
//            where
//              merch_num = :getMerchantNumber()
//            and
//            (
//              process_sequence is null or
//              (
//                process_sequence is not null and
//                (sysdate - order_date)*24 < 18 --3/4 day
//              )
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3505 = getMerchantNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select\n            count(supply_order_id)  \n          from\n            supply_order\n          where\n            merch_num =  :1 \n          and\n          (\n            process_sequence is null or\n            (\n              process_sequence is not null and\n              (sysdate - order_date)*24 < 18 --3/4 day\n            )\n          )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBean_SupplyBilling",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3505);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orderCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:316^9*/

        if (orderCount > 0)
        {
          isValid = false;
        }
      }
      catch(Exception e){
        //do nothing
      }
      finally
      {
        cleanUp();
      }

      return isValid;
    }

  } // OutstandingOrderValidation


  private String generateGRIN()
  {
    log.info("GRINTest..");
    ResultSet rs = null;
    ResultSetIterator it = null;

    StringBuffer sb = new StringBuffer("GRIN analysis: ");

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:349^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(sm.active_date,'MON')                                   as activity_month,
//                  ((gn.T1_TOT_INC_IND_PLANS) +
//                    (gn.t1_tot_calculated_discount) +
//                    (gn.T1_TOT_INC_INTERCHANGE) +
//                    (gn.T1_TOT_INC_SYS_GENERATED) +
//                    (gn.T1_TOT_INC_DEBIT_NETWORKS ) +
//                    (gn.T1_TOT_INC_Authorization) +
//                    (gn.T1_TOT_INC_Capture)) -
//                    ((sm.INTERCHANGE_EXPENSE)+ (sm.VMC_ASSESSMENT_EXPENSE) ) +
//                    ((sm.FEE_DCE_ADJ_AMOUNT+
//                    sm.DISC_IC_DCE_ADJ_AMOUNT) ) - (.14 * sm.vmc_sales_count)     as GRIN
//          from    monthly_extract_summary   sm,
//                  monthly_extract_gn        gn
//          where   sm.active_date between add_months(trunc(sysdate, 'MM'), -3)
//                  and trunc(trunc(sysdate, 'MM') - 1, 'MM')
//                  and sm.merchant_number = :getMerchantNumber()
//                  and sm.hh_load_sec = gn.hh_load_sec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3506 = getMerchantNumber();
  try {
   String theSqlTS = "select  to_char(sm.active_date,'MON')                                   as activity_month,\n                ((gn.T1_TOT_INC_IND_PLANS) +\n                  (gn.t1_tot_calculated_discount) +\n                  (gn.T1_TOT_INC_INTERCHANGE) +\n                  (gn.T1_TOT_INC_SYS_GENERATED) +\n                  (gn.T1_TOT_INC_DEBIT_NETWORKS ) +\n                  (gn.T1_TOT_INC_Authorization) +\n                  (gn.T1_TOT_INC_Capture)) -\n                  ((sm.INTERCHANGE_EXPENSE)+ (sm.VMC_ASSESSMENT_EXPENSE) ) +\n                  ((sm.FEE_DCE_ADJ_AMOUNT+\n                  sm.DISC_IC_DCE_ADJ_AMOUNT) ) - (.14 * sm.vmc_sales_count)     as GRIN\n        from    monthly_extract_summary   sm,\n                monthly_extract_gn        gn\n        where   sm.active_date between add_months(trunc(sysdate, 'MM'), -3)\n                and trunc(trunc(sysdate, 'MM') - 1, 'MM')\n                and sm.merchant_number =  :1 \n                and sm.hh_load_sec = gn.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.ACRDataBean_SupplyBilling",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3506);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.acr.ACRDataBean_SupplyBilling",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:368^7*/

      rs = it.getResultSet();

      while (rs.next())
      {
        sb.append(rs.getString("activity_month")).append(": $");
        sb.append(rs.getString("GRIN")).append(" ");
      }

    }
    catch(Exception e)
    {
      sb.append("Unable to complete GRIN analysis.");
    }
    finally
    {
      cleanUp();
    }

    return sb.toString();
  }

  public String renderHtml(String fieldName)
  {
    String htmlCode = null;

    if(!editable && fieldName.equals("supplyBillingConfig"))
    {
      String fieldData = getACRValue(fieldName);
      //Field field= fields.getField("supplyBillingConfig");
      //String fieldData = field==null?"":field.getData();

      if(fieldData.equals("per_order"))
      {
        htmlCode = "Merchant is charged on a \"per order\" basis for supplies";
      }
      else if(fieldData.equals("monthly"))
      {
        htmlCode = "Merchant is on a flat-fee monthly billing schedule.";
      }
      else if(fieldData.equals("waive"))
      {
        htmlCode = "Supply Billing has been waived for this merchant.";
      }
      else
      {
        htmlCode = "No Supply Billing Configuration found.";
      }

    }
    else
    {
      htmlCode = super.renderHtml(fieldName);
    }

    return htmlCode;

  }

  public String getGRINText()
  {
    return grinText;
  }

}/*@lineinfo:generated-code*/