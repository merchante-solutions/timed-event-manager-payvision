package com.mes.acr;

// log4j classes.
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.ValueObjectBase;

/**
 * MerchantInfo class
 *
 * Houses required merchant info associated with a given ACR.
 * This value object is READ ONLY.
 */
public final class MerchantInfo extends ValueObjectBase
{
  // create class log category
  static Logger log = Logger.getLogger(MerchantInfo.class);

  // constants
  // (NONE)


  // data members
  protected String            merchant_num;
  protected long              app_seq_num;
  protected String            dba;
  protected String            type;
  protected int               type_code;
  protected int               merch_bank_number;
  protected String            control_num;
  protected String            contact_name;
  protected String            phone;
  protected String            dda;
  protected String            trn;  // transit routing number
  protected String            association;
  protected String[]          vnums;
  protected int               processorId;

  protected String            acrAppType;
  protected int               acrAppTypeCode;

  // class methods
  // (NONE)


  // object methods

  // construction
  public MerchantInfo()
  {
    super();  // is_dirty should always be false since read only data

    clear();
  }

  public boolean equals(Object obj)
  {
    if(!(obj instanceof MerchantInfo))
      return false;

    return (((MerchantInfo)obj).merchant_num==this.merchant_num);
  }

  public void clear()
  {
    merchant_num="";
    app_seq_num=-1L;
    dba="";
    type="";
    type_code=-1;
    merch_bank_number=-1;
    control_num="";
    contact_name="";
    phone="";
    dda="";
    trn="";
    association="";
    processorId=0;

    vnums=null;

    acrAppType = "";
    acrAppTypeCode = -1;

    //is_dirty=true;
  }

  // accessors

  public int getACRAppTypeCode()
  {
    return acrAppTypeCode;
  }

  public String getACRAppType()
  {
    return acrAppType;
  }

  public String getMerchantNum()
  {
    return merchant_num;
  }
  public long getAppSeqNum()
  {
    return app_seq_num;
  }
  public String getDba()
  {
    return dba;
  }
  public String getType()
  {
    return type;
  }
  public int getTypeCode()
  {
    return type_code;
  }
  public int getMerchBankNumber()
  {
    return merch_bank_number;
  }
  public String getControlNum()
  {
    return control_num;
  }
  public String getContactName()
  {
    return contact_name;
  }
  public String getPhone()
  {
    return phone;
  }
  public String getDda()
  {
    return dda;
  }
  public String getTransitRoutingNum()
  {
    return trn;
  }
  public String getAssociation()
  {
    return association;
  }
  public String[] getVNums()
  {
    return vnums;
  }
  public int getProcessorId()
  {
    return processorId;
  }
  public String getProcessorType()
  {
    String type = "TSYS";
    switch(processorId)
    {
      case mesConstants.PROCESSOR_MES:
        type = "MES";
        break;

      default:
        break;
    }

    return type;
  }

  // mutators

  public void setACRAppType(String v)
  {
    if(v==null || v.equals(acrAppType))
      return;

    acrAppType=v;
    //is_dirty=true;
  }

  public void setACRAppTypeCode(int v)
  {
    if(v==acrAppTypeCode)
      return;

    acrAppTypeCode=v;
    //is_dirty=true;
  }

  public void setMerchantNum(String v)
  {
    if(v==null || v.equals(merchant_num))
      return;

    merchant_num=v;
    //is_dirty=true;
  }
  public void setAppSeqNum(long v)
  {
    if(v==app_seq_num)
      return;

    app_seq_num=v;
    //is_dirty=true;
  }
  public void setDba(String v)
  {
    if(v==null || v.equals(dba))
      return;

    dba=v;
    //is_dirty=true;
  }
  public void setType(String v)
  {
    if(v==null || v.equals(type))
      return;

    type=v;
    //is_dirty=true;
  }
  public void setTypeCode(int v)
  {
    if(v==type_code)
      return;

    type_code=v;
    //is_dirty=true;
  }
  public void setMerchBankNumber(int v)
  {
    if(v==merch_bank_number)
      return;

    merch_bank_number=v;
    //is_dirty=true;
  }
  public void setControlNum(String v)
  {
    if(v==null || v.equals(control_num))
      return;

    control_num=v;
    //is_dirty=true;
  }
  public void setContactName(String v)
  {
    if(v==null || v.equals(contact_name))
      return;

    contact_name=v;
    //is_dirty=true;
  }
  public void setPhone(String v)
  {
    if(v==null || v.equals(phone))
      return;

    phone=v;
    //is_dirty=true;
  }
  public void setDda(String v)
  {
    if(v==null || v.equals(dda))
      return;

    dda=v;
    //is_dirty=true;
  }
  public void setTransitRoutingNum(String v)
  {
    if(v==null || v.equals(trn))
      return;

    trn=v;
    //is_dirty=true;
  }
  public void setAssociation(String v)
  {
    if(v==null || v.equals(association))
      return;

    association=v;
    //is_dirty=true;
  }
  public void setVNums(String[] v)
  {
    if(v==null || v.length==0)
      return;

    vnums=v;
    //is_dirty=true;
  }

  public void setProcessorId(int v)
  {
    if(v==processorId)
      return;

    processorId=v;
    //is_dirty=true;
  }

  //////////////

  public String toString()
  {
    final String nl = System.getProperty("line.separator");

    StringBuffer sb = new StringBuffer(256);

    sb.append(nl);
    sb.append("MerchantInfo: ");

    sb.append("merchant_num="+merchant_num);
    sb.append("app_seq_num="+app_seq_num);
    sb.append(", dba="+dba);
    sb.append(", type="+type);
    sb.append(", type_code="+type_code);
    sb.append(", acrAppType="+acrAppType);
    sb.append(", acrAppTypeCode="+acrAppTypeCode);
    sb.append(", merch_bank_number="+merch_bank_number);
    sb.append(", control_num="+control_num);
    sb.append(", contact_name="+contact_name);
    sb.append(", phone="+phone);
    sb.append(", dda="+dda);
    sb.append(", trn="+trn);
    sb.append(", association="+association);
    sb.append(", processorId="+processorId);

    if(vnums!=null)
      sb.append(", vnums="+vnums.toString());

    return sb.toString();
  }

} // class MerchantInfo
