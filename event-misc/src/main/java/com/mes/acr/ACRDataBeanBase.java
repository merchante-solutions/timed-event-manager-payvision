/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/acr/ACRDataBeanBase.java $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckField;
import com.mes.forms.CheckboxField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldError;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.queues.QueueBase;
import com.mes.queues.QueueData;
import com.mes.queues.QueueTools;
import com.mes.support.HttpHelper;
import com.mes.support.StringUtilities;
import com.mes.support.TextMessageManager;


/**
 * class ACRDataBeanBase
 *
 * Abstract base class to use for ACR data beans.
 */
public abstract class ACRDataBeanBase extends FieldBean
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBeanBase.class);

  // constants
  public static final String    RELPATH_ACR_CONTROLLER        = "/srv/acr";
  public static final String    RELPATH_ACR_QUEUELISTING_PAGE = "/jsp/queues/queue.jsp";
  public static final String    RELPATH_CALLTAG_DETAIL        = "/srv/calltag?method=vect";
  public static final String    CALLTAG_EQUIPMENT_NAME        = "Equipment";

  // data members
  protected ACR                 acr;
  private MerchantInfo          merchInfo;
  // holds string nme/vals representing commands "available".
  // key:    "{Description}"
  // value:  "{URL-based commandstring (in QueryString form)
  //           recognizable by ACRController}"
  protected TreeMap             commands;

  // associates each commmand toa column number
  // key:     "{Description}"
  // value:    {col num}
  protected Hashtable           commandCols;

  // fields are editable?
  protected boolean             editable;

  // ref to ChangeRequestQueue object
  protected QueueBase           queueBean;

  // current queue row used for prev/next links
  protected int                 curRow                = -1;

  protected int                 residentQueue         = MesQueues.Q_NONE;
  protected int                 initialQueue          = MesQueues.Q_NONE;
  protected boolean             showActionHistoryLink = true;
  protected TextMessageManager  tmm                   = null;

  //components
  protected MultiMerchantBean mmBean;
  protected ACRMgmtQInfo mgmtQ;

  /**
   * Address bean for shipping address
   */
  protected AddressHelper addressHelper;

  //links
  protected String bankProcedureLink = "--";


  // class methods
  // (none)

  // object methods

  // construction
  public ACRDataBeanBase()
  {
    // so "baseGroup:" prefix doesn't show up in error message text.
    fields.setShowName("");

  }

  public ACRDataBeanBase(ACR acr, MerchantInfo merchInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    initialize(acr,merchInfo,requestor,editable);
  }

  public final void initialize(ACR acr, MerchantInfo merchInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    // so "baseGroup:" prefix doesn't show up in error message text.
    fields.setShowName("");

    if (acr == null)
    {
      throw new Exception("Cannot initialize data bean with null ACR");
    }

    if (acr.getDefinition() == null)
    {
      throw new Exception("Cannot initialize data bean with ACR, ACR has"
        + " no definition");
    }

    if (editable && merchInfo == null)
    {
      throw new Exception("Cannot initialize data bean as editable with null"
        + " merchant info");
    }

    if (editable && requestor == null)
    {
      throw new Exception("Cannot initialize data bean as editable with null"
        + " requestor");
    }

    this.acr          = acr;
    this.editable     = editable;
    this.merchInfo    = merchInfo;

    // determine which queue the acr is currently in
    //
    // FIX: this logic will break if acr's are allowed to reside in more than
    // one queue at a time, i.e. the case of a parallel cb&t review queue
    // that has been discussed...
    int[] rq = QueueTools
      .getResidentQueues(acr.getID(),MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST);
    this.residentQueue = rq.length > 0 ? rq[0] : MesQueues.Q_NONE;

    if(editable)
    {
      // set parent IDs
      acr.setParentIDs(merchInfo.getMerchantNum(),merchInfo.getAppSeqNum());

      // set ACR Item(s) applicable to all ACRs
      log.debug("Setting Universals for ACRDataBean '" + acr.getDefinition().getName() + "'...");
      setUniversals(merchInfo,requestor);

      // set ACR Item(s) specific to this ACR
      log.debug("Setting Defaults for ACRDataBean '" + acr.getDefinition().getName() + "'...");
      setDefaults();

      mgmtQ = new ACRMgmtQInfo(merchInfo.getMerchantNum());
    }
    else
    {
      mgmtQ = new ACRMgmtQInfo(acr.getMerchantNum());
    }

    //establish mgmt q info
    //currently only used in MES ACRs,,,

    log.debug("Mgmt Q info set: ID = " + mgmtQ.getId()
              +" ("+mgmtQ.getDescription()+") email number:"
              + mgmtQ.getEmailRef() +
              " mgmt Q on?:" + (mgmtQ.isMgmtQOn()?"true":"false"));
    // do any child specific initialization

    extendedInit(acr,merchInfo,requestor,editable);
  }

  /**
   * Place holder, child class should override if special init needed.
   */
  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
  }

  public void acrCreateFields()
  {
    createFields();
  }

  /**
   * Instantiates an address helper, stores internal reference to it.
   */
  protected AddressHelper getAddressHelper(boolean makeRequired, boolean noAddrOption)
  {
    if (addressHelper == null)
    {
      int adType = isCBTBank(getBankNumber())?AddressHelper.FT_CBT_SHIPPING:AddressHelper.FT_NONE;

      addressHelper = new AddressHelper(getMerchantNumber(), "ship", adType, noAddrOption);

      if(makeRequired)
      {
        addressHelper.makeRequired();
      }
    }
    return addressHelper;
  }

  protected AddressHelper getAddressHelper()
  {
    return getAddressHelper(false, true);
  }

  // accessors

  /*******************************************************
   * Methods that allow child classes to
   * manipulate and use components for JSP
   * info display - components based on forms package
   *******************************************************/

  /**
   * Allows child bean to turn off basebean action buttons
   * if not overridden, will never affect buttons
   */
  public boolean disableParentButtons()
  {
    return false;
  }


  /**
   * Allows child bean to surface MultiMerchantBean display and func.
   */
  public boolean hasMultiMerchantComp()
  {
    return true;
  }

  /**
   * Allows child bean to surface MultiMerchantBean display and func.
   */
  public boolean hasEmailComp()
  {
    return true;
  }

  public boolean hasEmailContactInfo()
  {
    if(acr!=null && acr.itemHasValue("emailAttnTo"))
    {
      return true;
    }
    return false;
  }

  public String renderMMHtml()
  {
    return mmBean.renderHtml();
  }

  /**
   * Allows child bean to iterate through various steps before submission
   */
  public boolean hasMoreSteps()
  {
    return false;
  }

  /**
   * Allows child bean to indicate equipment exits
   */
  public boolean equipmentExists()
  {
    return false;
  }

  /**
   * test method for new equip dev
   */
  public boolean _equipmentExists()
  {
    return false;
  }

  /**
   * Allows child bean to indicate call tagged equipment exits
   */
  public boolean calltaggedEquipmentExists()
  {
    return false;
  }

  /**
   * Allows child bean to indicate call tagged equipment exits
   */
  public boolean _calltaggedEquipmentExists()
  {
    return false;
  }

  /**
   * Allows child bean to generate call tag link
   */
  public String generateCallTagLink(String defaultString)
  {
    if(calltaggedEquipmentExists())
    {
      StringBuffer sb = new StringBuffer();
      sb.append("<a href=\"");
      sb.append(RELPATH_CALLTAG_DETAIL);
      sb.append("&acrid=");
      sb.append(getACR().getID());
      sb.append("\">");
      sb.append(defaultString);
      sb.append("</a>");
      return sb.toString();
    }
    else
    {
      return defaultString;
    }
  }

  /**
   * Allows child bean to indicate call tagged equipment exits
   */
  public List getSelectedEquipment()
  {
    return null;
  }

  public ACRMgmtQInfo getMgmtQInfo()
  {
    return mgmtQ;
  }


  /*******************************************************
   *
   *******************************************************/

  public final boolean inStatusMode()
  {
    return !editable; // i.e. status mode is when viewing in read-only state.
  }

  public final boolean inSubmitMode()
  {
    return editable; // i.e. when submitting ACR request(s)
  }

  public long getAppSeqNum()
  {
    return acr==null? -1L:acr.getAppSeqNum();
  }
  public String getMerchantNumber()
  {
    return acr==null? "":acr.getMerchantNum();
  }
  public String getAppCntrlNum()
  {
    return acr==null? "":acr.getACRValue("Application Control #");
  }

  public final ACR getACR()
  {
    return acr;
  }
  public final ACRDefinition getACRDefinition()
  {
    return (acr==null)? null:acr.getDefinition();
  }

  public final String getACRID()
  {
    return acr==null? "":String.valueOf(acr.getID());
  }
  public String getACRStatus()
  {
    return acr==null? "":ACR.getStatusDescriptor(acr.getStatus());
  }
  public final String getACRDateCreated()
  {
    if(acr==null)
      return "";
    SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy h:mm a");
    return sdf.format(acr.getDateCreated());
  }
  public final String getACRDateLastModified()
  {
    if(acr==null)
      return "";
    SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy h:mm a");
    return sdf.format(acr.getDateLastModified());
  }

  public final String getACRValue(String fname)
  {
    return (acr==null || fname==null)? "":acr.getACRValue(fname);
  }

  public boolean showActionHistoryLink() { return showActionHistoryLink; }
  public void setShowActionHistoryLink(boolean v)
  {
    showActionHistoryLink=v;
  }


  /**
   * doPush()
   *
   * Transfers data from member ACR items to the corres. Field object if it exists.
   */
  public void doPush()
  {
    if(acr==null)
      return;

    ACRItem acri;
    Field fld;

    log.debug("Pushing data from ACR '"+acr.getDefinition().getName()+"' to Fields...");

    // map the current ACR data to the detail data bean
    for(Enumeration e=acr.getItemEnumeration();e.hasMoreElements();) {
      acri=(ACRItem)e.nextElement();

      if((fld = getField(acri.getName()))!=null)
      {
        log.debug("push to field: "+acri.getName()+" = "+acri.getValue());
        fld.setData(acri.getValue());
      }
      else
      {
        //log.debug("could not find field: "+acri.getName());
      }
    }

  }

  /**
   * doPull()
   *
   * Transfers from the field objects to the corres. ACR Item of the member ACR object.
   */
  public void doPull()
  {
    if(acr==null)
      return;

    ACRItem acri;
    ACRDefItem acrdi;
    Field fld;

    log.debug("Pulling data from Fields to ACR '"+acr.getDefinition().getName()+"'...");

    // map back the form data to the current ACR
    doPull(fields);

    // do pulling on subcomponent sub beans
    for (Iterator i = subBeans.iterator(); i.hasNext();)
    {
      FieldBean subBean = (FieldBean)i.next();
      if (subBean instanceof ACRTranslator)
      {
        ((ACRTranslator)subBean).doPull(acr);
      }
    }

    // re-format certain fields
    if(getACRValue("Requestor's Phone #").length()>0)
      acr.setACRItem("Requestor's Phone #",PhoneField.format(getACRValue("Requestor's Phone #")));
  }

  private final void doPull(Field fld)
  {
    if(fld instanceof FieldGroup)
    {
      for(Iterator i = ((FieldGroup)fld).iterator(); i.hasNext();)
      {
        doPull((Field)i.next());
      }
    }
    //
    else if ( !fld.isBlank() ||
              ( fld.isBlank() && (fld instanceof CheckboxField ||
                                  fld instanceof CheckField  )
              )
            )
    {
      acr.setACRItem(fld.getName(),fld.getData());
    }
  }

  public boolean hasFields()
  {
    return (fields.size() > 0);
  }

  /**
   * createFields()
   *
   * Creates the Fields and corres. field validations for this bean.
   *
   * @return  Returns true when successful, false otherwise.
   */
  private void createFields()
  {
    createBaseFields();
    createExtendedFields();

    //change fix image - us a smaller version
    fields.setFixImage("/images/arrow1_left.gif",10,10);

    // make fields look pretty
    fields.setHtmlExtra("class=\"formFields\"");
  }

  private void createBaseFields()
  {
    if(editable) {
      // create necessary "fill in the blank" fields for the universals
      fields.add(new Field("Requestor's Name",50,20,false));
      fields.add(new Field("Requestor's Title",50,20,false));
      fields.add(new PhoneField("Requestor's Phone #",false));

      fields.add(new CheckboxField("Rush","Rush?",false));
    }

    // comments editability now determined by hasEditableComments() method
    // (called from acr_detail.jsp) so always add comments field
    fields.add(new TextareaField("Comments",1000,6,140,true));

    //Multi-Merchant component - always built
    //but can be "turned off" by child
    mmBean = new MultiMerchantBean(acr);

    //create fields within component bean
    if(editable)
    {
      mmBean.createFields();

      //(FieldBean)add this bean as subBean
      try
      {
        add(mmBean);
      }
      catch(Exception e)
      {
        log.debug("createExtendedFields = "+ e.toString());
      }
    }

    fields.add(new RadioButtonField("processorX", processorNames, 2, true, ""));
    fields.add(new RadioButtonField("qStateX", qStateNames, 2, true, ""));

    Field field;
    if(editable)
    {
      field = new CheckboxField("cbEmailComp","Provide Information to the following email",false);
      Validation val = new IfYesNotBlankValidation(field,"Please enter email contact information.");
      fields.add(field);

      field = new Field("emailAttnTo","Email Attention To",50,50, true);
      field.addValidation(val);
      fields.add(field);

      field = new EmailField("emailContactList","Email Contact List",100,100,true);
      field.addValidation(val);
      fields.add(field);
    }
  }

  public String renderEmailHtml()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<table width=100%><tr><td colspan=2 class='tableData'>");
    sb.append(renderHtml("cbEmailComp")).append("</td></tr>");
    sb.append("<tr><td class='tableData' width='100'>Attention To:</td><td class='tableData'>");
    sb.append(renderHtml("emailAttnTo")).append("</td></tr>");
    sb.append("<tr><td class='tableData'>Email Address(es):</td><td class='tableData'>");
    sb.append(renderHtml("emailContactList")).append("</td></tr>");
    sb.append("<tr><td class='tableData'>&nbsp;</td><td class='tableData'>Please separate addresses with a semi-colon(;)");
    sb.append("</table>");
    return sb.toString();
  }


  private String[][] processorNames =
  {
    {"Trident","Trident"},
    {"Vital","Vital"},
    {"Unassigned","Unassigned"}
  };

  private String[][] qStateNames =
  {
    {"Activations","Activations"},
    {"Deployment","Deployment"},
    {"Unassigned","Unassigned"}
  };


  /**
   * Determines if comments may be edited.  Called by acr_detail.jsp to
   * determine if the comments form control is to be displayed.  Child
   * classes may override this method to change the default behavior.
    */
  public boolean hasEditableComments()
  {
    // by default consider comments editable if the
    // general editable flag is on
    if (editable)
    {
      return true;
    }

    // if acr is not generally editable the default is to still allow
    // comments to be edited except when in the following queues
    return  residentQueue != MesQueues.Q_NONE &&
            residentQueue != MesQueues.Q_CHANGE_REQUEST_COMPLETED &&
            residentQueue != MesQueues.Q_CHANGE_REQUEST_CANCELLED &&
            residentQueue != MesQueues.Q_CHANGE_REQUEST_BBT_COMPLETED &&
            residentQueue != MesQueues.Q_CHANGE_REQUEST_BBT_CANCELLED;
  }

  /**
   * Returns true if the comments field exists and is not blank.
   */
  public boolean hasComments()
  {
    Field comments = fields.getField("Comments");
    return comments != null && !comments.isBlank();
  }

  protected void createExtendedFields()
  {
    // no-op
  }

  public TextMessageManager getTextMessageManager()
  {
    return tmm;
  }

  public void setTextMessageManager(TextMessageManager tmm)
  {
    if(tmm!=null)
      this.tmm=tmm;
  }

  /**
   * getHeadingName()
   *
   * The heading text that appears at the top of the detail view.
   */
  public abstract String getHeadingName();

  /**
   * setDefaults()
   *
   * This function sets detaults that are loaded into the ACR ref data member upon first load only.
   */
  protected abstract void setDefaults();


  /**
   * getAllowedQueueOps()
   *
   * Returns 2-d integer array containing a set of allowed ACR queue ops.
   * Old ACR types and CBT ACRs override this method call as both don't use
   * mgmt Qs
   */
  public int[][] getAllowedQueueOps()
  {

    int[][] ops;


    //THIS IS FOR INITIAL ROUTING TO MGMT Q
    //ACR mgmt Q routing is turned on
    //merchant mgmt Q usage is turned on
    //ACR is new
    if(acr.getDefinition().initMgmtQ()
            && mgmtQ.isMgmtQOn()
            && getResidentQueue() < 0 )
    {
      ops = mgmtQ.getMgmtQOps(initialQueue);
    }
    //this is in case the resident Q is a Mgmt queue -
    //must return to the submitting Q
    else if(mgmtQ.isCurrentQueue(getResidentQueue()) )
    {
      ops = mgmtQ.getMgmtQOps(acr.getID(),initialQueue);
    }
    //this is the default for all other Qing
    else
    {
      ops = ACRQueueOps.getQueueOps(initialQueue, mgmtQ);
    }

    return ops;
  }

  protected final void setInitialQueue(int queue)
  {
    initialQueue = queue;
  }

  public final int getInitialQueue()
  {
    return initialQueue;
  }

  /**
   * getCancellationReasons()
   *
   * Base class implementation representating the
   * "standard" set of cancellation reasons.
   *
   * Override this function to specify specific cancellation reasons
   * unique to the concrete sub-class.
   */
  protected String[] getCancellationReasons(int crntQ)
  {
    // universal cancellation reasons
    return new String[]
    {
       "Duplicate Request"
      ,"Incomplete Information"
      ,"Request Cancelled"
      ,"Request Declined"
    };
  }

  private final void setUniversals(MerchantInfo merchantInfo, Requestor requestor)
  {
    if(acr==null)
      return;

    Field fld;

    // merchant info
    if(merchantInfo!=null) {
      //acr.setACRItem("Merchant Account Number",merchantInfo.getMerchantNum());
      acr.setACRItem("Merchant DBA",merchantInfo.getDba());
      acr.setACRItem("Type",merchantInfo.getType());
      acr.setACRItem("Type Code",Integer.toString(merchantInfo.getTypeCode()));
      acr.setACRItem("Merchant Bank Number",Integer.toString(merchantInfo.getMerchBankNumber()));
      acr.setACRItem("Application Control #",merchantInfo.getControlNum());
      acr.setACRItem("Merchant Contact Name",merchantInfo.getContactName());
      acr.setACRItem("Merchant Phone #",PhoneField.format(merchantInfo.getPhone()));
      acr.setACRItem("DDA",merchantInfo.getDda());
      acr.setACRItem("Transit Routing #",merchantInfo.getTransitRoutingNum());
      acr.setACRItem("Association #",merchantInfo.getAssociation());
      acr.setACRItem("Processor Type",merchantInfo.getProcessorType());

      acr.setACRItem("ACR Type",merchantInfo.getACRAppType());
      acr.setACRItem("ACR Type Code",Integer.toString(merchantInfo.getACRAppTypeCode()));

      String[] vnums = merchantInfo.getVNums();

      if(vnums!=null && vnums.length>0) {
        StringBuffer sb = new StringBuffer(16*vnums.length);
        for(int i=0;i<vnums.length;i++) {
          if(i>0)
            sb.append(", ");
          sb.append(vnums[i]);
        }
        acr.setACRItem("V Numbers",sb.toString());
      } else
        acr.setACRItem("V Numbers","Not available.");

    }

    // requestor info
    if(requestor!=null) {
      acr.setACRItem("Requestor's Name",requestor.getRequestorName());
      acr.setACRItem("Requestor's Title",requestor.getRequestorTitle());
      acr.setACRItem("Requestor's Phone #",PhoneField.format(requestor.getRequestorPhone()));
      acr.setACRItem("Sales Channel",requestor.getSalesChannel());
      acr.setACRItem("Source Userid",requestor.getSourceUserid());
    }

    //build the non-bank card info
    ACRNonBankCardDetails nbcd = new ACRNonBankCardDetails();
    nbcd.setProperties(merchantInfo.getMerchantNum());
    if(nbcd != null)
    {
      acr.setACRItem("Amex Details", nbcd.getAmexDetails());
      acr.setACRItem("Discover Details", nbcd.getDiscoverDetails());
      acr.setACRItem("Certegy Details", nbcd.getCertegyDetails());
      acr.setACRItem("Valutec Details", nbcd.getValutecDetails());
    }

    setBankProceduresLink(merchantInfo.getMerchantNum());

  }



  private void setBankProceduresLink(String merchantNumber)
  {

    String              qs      = null;
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    try
    {
      connect();

      qs =
      "select  bpu.url, bpu.bank_name                  "+
      "from    bank_procedures_url bpu,                "+
      "        t_hierarchy         th,                 "+
      "        mif                 m                   "+
      "where   m.merchant_number = ? and               "+
      "        m.association_node = th.descendent and  "+
      "        th.hier_type = ? and                    "+
      "        th.ancestor = bpu.hierarchy_node        "+
      "order by relation asc                           ";

      ps = con.prepareStatement(qs);
      ps.setString(1,merchantNumber);
      ps.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
      rs = ps.executeQuery();

      if(rs.next())
      {
        acr.setACRItem("Bank URL","<a href='"+rs.getString("url")+"' class='semiSmall' target='_procedures'>");
        acr.setACRItem("Bank Name",rs.getString("bank_name")+"</a>");
      }
      else
      {
        acr.setACRItem("Bank URL","");
        acr.setACRItem("Bank Name","Merchant e_Solutions");
      }
    }
    catch(Exception e)
    {
      //logEntry("getBankProcedures(" + merchantNumber + ")", e.toString());
      e.printStackTrace();
    }
    finally
    {
      try
      {
        rs.close();
        cleanUp();
      } catch(Exception e) {}
    }
  }

  public String getBankProcedureLink()
  {
    return bankProcedureLink;
  }


  /**
   * renderHtml()
   *
   * Sub-class override to handle case of read-only ("Status" view).
   */
  public String renderHtml(String fname)
  {
    return renderHtml(fname,true,true);
  }

  public String renderHtml(String fname,boolean bShowLabel)
  {
    return renderHtml(fname,bShowLabel,true);
  }
  public String renderHtml(String fname,boolean bShowLabel,boolean bShowValue)
  {
    Field fld=fields.getField(fname);

    if(fld!=null)
    {
      if(fld instanceof CheckboxField && !bShowLabel)
      {
        ((CheckboxField)fld).setLabelText("");
      }
      return fld.renderHtml();
    }
    else
    {
      if(acr==null)
      {
        return "";
      }

      ACRDefinition acrd = acr.getDefinition();

      if(acrd==null && bShowValue)
      {
        return getACRValue(fname);
      }

      ACRDefItem acrdi;

      if((acrdi=acrd.getACRDefItem(fname))==null)
      {
        return bShowValue? getACRValue(fname):"";
      }

      switch(acrdi.getDataType())
      {
        case ACRDefItem.DATATYPE_BOOLEAN:
        {
          StringBuffer sb = new StringBuffer();
          String label,val;

          label = bShowLabel? acrdi.getLabel():"";
          val = bShowValue? (acr.getACRValue(fname).length()>0? (acr.getACRValue(fname).equals("y")? "Yes":"No"):"No"):"";

          if(label.length()>0)
          {
            sb.append(label);
            sb.append(" ");
          }
          if(val.length()>0)
          {
            sb.append(val);
          }

          return sb.toString();
        }
        default:
          return bShowValue? getACRValue(fname):"";
      }
    }
  }

  public String getRequestorSourceLink(String paramName, String paramValue)
  {
    return acr==null? "":getUserProfileLink(acr.getACRValue("Source Userid"), paramName, paramValue);
  }

  public String getStatusLastWorkedByLink(String paramName, String paramValue)
  {
    return getUserProfileLink(getQueueLastUser(), paramName, paramValue);
  }

  public String getStatusSourceLink(String paramName, String paramValue)
  {
    return acr==null? "":getUserProfileLink(acr.getACRValue("Source Userid"), paramName, paramValue);
  }

  public String getActionHistoryLink( String paramName, String paramValue )
  {
    if(!showActionHistoryLink)
      return "";

    StringBuffer sb = new StringBuffer(128);

    sb.append("<a href=\"");
    sb.append(ACRDataBeanBase.RELPATH_ACR_CONTROLLER);
    sb.append("?method=vah");  //\">Action History</a>");

    if ( paramName != null && paramName.length()>0 && paramValue != null && paramName.length()>0)
    {
      sb.append("&");
      sb.append(paramName);
      sb.append("=");
      sb.append(paramValue);
    }
    sb.append("\">Action History</a>");

    return sb.toString();
  }

  public final String getUserProfileLink(String loginName, String paramName, String paramValue)
  {
    StringBuffer sb = new StringBuffer(128);

    sb.append("<a href=\"");
    sb.append(ACRDataBeanBase.RELPATH_ACR_CONTROLLER);
    sb.append("?method=vsrc&editable=");
    sb.append(editable? "1":"0");
    sb.append("&ln=");
    sb.append(HttpHelper.urlEncode(loginName));
    if ( paramName != null && paramName.length()>0 && paramValue != null && paramName.length()>0)
    {
      sb.append("&");
      sb.append(paramName);
      sb.append("=");
      sb.append(paramValue);
    }
    sb.append("\">");
    sb.append(loginName);
    sb.append("</a>");

    return sb.toString();
  }

  public String getAccountLink(String paramName, String paramValue)
  {
    String mn = this.getMerchantNumber();
    if(mn==null || mn.length()<1)
      return "";

    StringBuffer sb = new StringBuffer(128);

    sb.append("<a href=\"");
    sb.append(ACRDataBeanBase.RELPATH_ACR_CONTROLLER);
    sb.append("?method=vacc&editable=");
    sb.append(editable? "1":"0");
    if ( paramName != null && paramName.length()>0 && paramValue != null && paramName.length()>0)
    {
      sb.append("&");
      sb.append(paramName);
      sb.append("=");
      sb.append(paramValue);
    }
    sb.append("\">");
    sb.append(mn);
    sb.append("</a>");

    return sb.toString();
  }

  public String getApplicationLink( String paramName, String paramValue)
  {
    String acn = this.getAppCntrlNum();
    if(acn==null || acn.length()<1)
      return "";

    StringBuffer sb = new StringBuffer(128);

    sb.append("<a href=\"");
    sb.append(ACRDataBeanBase.RELPATH_ACR_CONTROLLER);
    sb.append("?method=vapp&editable=");
    sb.append(editable? "1":"0");
    if ( paramName != null && paramName.length()>0 && paramValue != null && paramName.length()>0)
    {
      sb.append("&");
      sb.append(paramName);
      sb.append("=");
      sb.append(paramValue);
    }
    sb.append("\">");
    sb.append(acn);
    sb.append("</a>");

    return sb.toString();
  }

  public String getHTMLBackLink(String cssClassName, String paramName, String paramValue)
  {
    StringBuffer sb = null;

    if(editable) {

      sb = new StringBuffer(512);

      sb.append("<a");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\" ");
      }
      sb.append("href=\"");
      sb.append(RELPATH_ACR_CONTROLLER);
      sb.append("?method=vdefs");
      if ( paramName != null && paramName.length()>0 && paramValue != null && paramName.length()>0)
      {
        sb.append("&");
        sb.append(paramName);
        sb.append("=");
        sb.append(paramValue);
      }
      sb.append("\">Back to Change Request Options</a>");
    } else {

      if(isQueueBeanLoaded()) {

        sb = new StringBuffer(512);

        sb.append("<a");
        if(cssClassName!=null && cssClassName.length()>0) {
          sb.append(" class=\"");
          sb.append(cssClassName);
          sb.append("\" ");
        }
        sb.append("href=\"");
        sb.append(RELPATH_ACR_QUEUELISTING_PAGE);
        sb.append("?type=");
        sb.append(residentQueue);

        String qs = queueBean.getSearchParamsQueryString();
        if(qs.length()>0) {
          sb.append("&");
          sb.append(qs);
        }

        sb.append("\">");
        sb.append(getResidentQueueDesc());
        sb.append(" - Queue Listing</a>");
      }

    }

    return (sb!=null)? sb.toString():"";
  }

  public String getACRSearchLink(String cssClassName)
  {
      StringBuffer sb = new StringBuffer(512);

      sb.append("<a");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\" ");
      }
      sb.append("href=\"");
      sb.append(RELPATH_ACR_CONTROLLER);
      sb.append("?method=srch\">ACR Search</a>");

      return sb.toString();
  }

  public String getACRSearchLink(String cssClassName, String paramName, String paramValue)
  {
      StringBuffer sb = new StringBuffer(512);

      sb.append("<a");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\" ");
      }
      sb.append("href=\"");
      sb.append(RELPATH_ACR_CONTROLLER);
      sb.append("?method=srch");
      if ( paramName != null && paramName.length()>0 && paramValue != null && paramName.length()>0)
      {
        sb.append("&");
        sb.append(paramName);
        sb.append("=");
        sb.append(paramValue);
      }
      sb.append("\">ACR Search</a>");

      return sb.toString();
  }

  public String getNewACRLink(String cssClassName, String paramName, String paramValue)
  {
      StringBuffer sb = new StringBuffer(512);

      sb.append("<a");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\" ");
      }
      sb.append("href=\"");
      sb.append(RELPATH_ACR_CONTROLLER);
      sb.append("?method=vdefs&mn=");
      sb.append(getMerchantNumber());

      if ( paramName != null && paramName.length()>0 && paramValue != null && paramName.length()>0)
      {
        sb.append("&");
        sb.append(paramName);
        sb.append("=");
        sb.append(paramValue);
      }
      sb.append("\">New ACR</a>");

      return sb.toString();
  }

  /********** Queue functions ********/

  public String getQueueMerchantType()
  {
    if(!isQueueBeanLoaded())
      return "";

    return queueBean.getQueueItem().getAffiliate();
  }

  public String getQueueSource()
  {
    if(!isQueueBeanLoaded())
      return "";

    return queueBean.getQueueItem().getSource();
  }

  public String getQueueLastUser()
  {
    if(!isQueueBeanLoaded())
      return "";

    return queueBean.getQueueItem().getLastUser(true);
  }

  public String getQueueLockedBy()
  {
    if(!isQueueBeanLoaded())
      return "";

    return queueBean.getQueueItem().getLockedBy();
  }

  public String getQueueDateCreated()
  {
    if(!isQueueBeanLoaded())
      return "";

    return queueBean.getQueueItem().getDateCreated()==null?"":queueBean.getQueueItem().getDateCreated();
  }

  public String getQueueLastChanged()
  {
    if(!isQueueBeanLoaded())
      return "";

    return queueBean.getQueueItem().getLastChangedDate();
  }

  public boolean getQueueIsRush()
  {
    if(!isQueueBeanLoaded())
      return false;

    return queueBean.getQueueItem().getIsRush();
  }

  public int getResidentQueue()
  {
    return residentQueue;
  }

  public String getResidentQueueDesc()
  {
    return MesQueues.getACRQueueDescriptor(residentQueue);
  }

  public final boolean isQueueBeanLoaded()
  {
    return (queueBean!=null);
  }

  public final void unloadQueueBean()
  {
    residentQueue=MesQueues.Q_NONE;
    curRow=-1;
    queueBean=null;
  }

  public final QueueBase getQueueBean()
  {
    return queueBean;
  }

  public final void setQueueBean(QueueBase queueBean)
  {
    if(queueBean==null || this.queueBean==queueBean)
      return;

    this.queueBean = queueBean;

    residentQueue = queueBean.getType();

    if(acr != null) {
      // load queue item
      queueBean.loadItem(acr.getID());
      if(queueBean.getQueueItem()==null) {
        log.error("loadQueueBean() FAILED: Unable to load Queue Item of ACR ID: "+acr.getID());
        queueBean=null;
        return;
      }

      // re-set current row to match the current queue item
      curRow=-1;
      for(int i=0;i<queueBean.getRows().size();i++) {
        if(acr.getID()==((QueueData)queueBean.getRows().elementAt(i)).getId()) {
          curRow=i;
          break;
        }
      }
    }

  }

  // ****** queue row iteration functions ******

  public final boolean hasNextRow()
  {
    return (isQueueBeanLoaded() && queueBean.getRows().size()>0 && curRow>-1 && curRow<(queueBean.getRows().size()-1));
  }

  public final boolean hasPrevRow()
  {
    return (isQueueBeanLoaded() && queueBean.getRows().size()>0 && curRow>0);
  }

  public final long getCrntRowID()
  {
    return (isQueueBeanLoaded() && (curRow>-1 && queueBean.getRows().size()>0)? ((QueueData)(queueBean.getRows()).elementAt(curRow)).getId():-1L);
  }

  public final void incrCrntRow()
  {
    curRow++;
  }

  public final void decrCrntRow()
  {
    curRow--;
  }

  public final boolean hasQueueIterationLinks()
  {
    if(!isQueueBeanLoaded())
      return false;

    return (hasNextRow() || hasPrevRow());
  }

  public final String getQueueIterationLinksHTMLTable(String cssClassName)
  {
    if(!hasQueueIterationLinks())
      return "";

    StringBuffer sb = new StringBuffer(1024);

    sb.append("<table border=0 cellpadding=0 cellspacing=0>");
    sb.append("<tr>");

    // prev link
    if(hasPrevRow()) {
      decrCrntRow();

      sb.append("<td");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\"");
      }
      sb.append("><a");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\" ");
      }
      sb.append("href=\"/srv/QueueRouter?type=");
      sb.append(queueBean.getType());
      sb.append("&itemType=10");  // i.e. Change Requests
      sb.append("&id=");
      sb.append(getCrntRowID());
      sb.append("\">Previous Queue Item</a></td>");

      incrCrntRow();
    }

    // spacer
    if(hasNextRow() && hasPrevRow()) {
      sb.append("<td width=5>&nbsp;|&nbsp;</td>");
    }

    // next
    if(hasNextRow()) {
      incrCrntRow();

      sb.append("<td");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\"");
      }
      sb.append("><a");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\" ");
      }
      sb.append("href=\"/srv/QueueRouter?type=");
      sb.append(queueBean.getType());
      sb.append("&itemType=10");  // i.e. Change Requests
      sb.append("&id=");
      sb.append(getCrntRowID());
      sb.append("\">Next Queue Item</a></td>");

      decrCrntRow();
    }

    sb.append("</tr>");
    sb.append("</table>");

    return sb.toString();
  }


  // *** command related functions ***

  /**
   * doCommand()
   *
   * Intended to be overridden and serves as mechanism with which to pass on unhandled
   * requests from the operating ACRController servlet instance.
   */
  public boolean doCommand(String method)
  {
    return false;
  }

  public final boolean hasCommands()
  {
    return (commands!=null && commands.size()>0);
  }

  public final void addCommand(String command,String desc,int colNum)
  {
    if(command==null || desc==null || command.length()<1 || desc.length()<1 || colNum<1 || colNum>3) {
      log.error("addCommand() - Unable to add command: Invalid parameters.");
      return;
    }

    if(commands==null) { // lazy init
      commands = new TreeMap();
      commandCols = new Hashtable();
    }

    // remove any previously existing command
    if(commands.containsKey(desc)) {
      commands.remove(desc);
      commandCols.remove(desc);
    }

    commands.put(desc,command);
    commandCols.put(desc,colNum);
  }

  public final void clearCommand(String desc)
  {
    if(desc==null || desc==null || !commands.containsKey(desc))
      return;

    commands.remove(desc);
    commandCols.remove(desc);
  }

  public final void clearCommands()
  {
    if(commands!=null)
      commands.clear();
    if(commandCols!=null)
      commandCols.clear();
  }

  public String getCommandLinkHTMLTable(String cssClassName, String paramName, String paramValue )
  {
    // Modus Operandi: Commands shall fall within one of three columns.
    //                 Each column is sorted lexiconographically.

    if(commands==null || commands.size()<1)
      return "";

    if(cssClassName==null)
      cssClassName="";

    StringBuffer sb = new StringBuffer(512*commands.size());
    Set set = commands.keySet();
    String key,val;
    int numCols=0;
    int colNum=0;

    // find the highest column number as this dictates the number of columns
    Collection c = commandCols.values();
    Iterator i=c.iterator();
    int n;
    while(i.hasNext()) {
      n=((Integer)i.next()).intValue();
      if(numCols<n)
        numCols=n;
    }
    if(numCols<1)
      return "";
    int spcrColWidthPcnt = (int)((1/(6*(double)numCols-1))*100);
    int colWidthPcnt = 5*spcrColWidthPcnt;

    log.debug("getcommandLinkHTMLTable() - numCols="+numCols+", spcrColWidthPcnt="+spcrColWidthPcnt);

    sb.append("<table border=0 cellpadding=0 cellspacing=0 width='100%'>");
    sb.append("<tr>");

    for(int j=1;j<=numCols;j++) {

      sb.append("<td valign=top width=");
      sb.append(colWidthPcnt);
      sb.append("%>");
      sb.append("<table border=0 cellpadding=0 cellspacing=0>\n");

      for(i=set.iterator();i.hasNext();) {
        key=(String)i.next();
        val=(String)commands.get(key);
        colNum=((Integer)commandCols.get(key)).intValue();

        if(colNum!=j)
          continue;

        sb.append("<tr><td class=\"");
        sb.append(cssClassName);
        sb.append("\"><a class=\"");
        sb.append(cssClassName);
        sb.append("\" href=\"");
        sb.append(val);
        if ( paramName != null && paramName.length()>0 && paramValue != null && paramName.length()>0)
        {
          sb.append("&");
          sb.append(paramName);
          sb.append("=");
          sb.append(paramValue);
        }
        sb.append("\">");
        sb.append(key);
        sb.append("</a></td></tr>\n");
      }

      sb.append("</table>");
      sb.append("</td>\n");

      // spacer col
      if(j<numCols) {
        sb.append("<td width=");
        sb.append(spcrColWidthPcnt);
        sb.append("%>&nbsp;</td>\n");
      }
    }

    sb.append("</tr>");
    sb.append("</table>\n");

    return sb.toString();
  }

  /**
   * ACRDefItemToField()
   *
   * Creates a new instance of com.mes.forms.Field
   *  from a reference to a com.mes.acr.ACRDefItem instance.
   */
  protected final Field ACRDefItemToField(ACRDefItem acrdi)
  {
    Field field=null;
    String s,vldparam;
    int i,length=0,size=0,maxlen=0;
    float lbound=0,ubound=0;

    vldparam=acrdi.getValidationParams();

    // parse validation params
    // declare length and size of assoc. html control
    switch(acrdi.getDataType()) {
      //case ACRDefItem.DATATYPE_BOOLEAN:   // no-op
      //case ACRDefItem.DATATYPE_DATE:      // no-op

      // numeric range vld param
      case ACRDefItem.DATATYPE_MONEY:   // fall through to DATATYPE_NUMERIC
      case ACRDefItem.DATATYPE_NUMERIC:
        if(vldparam.length()>0 && (i=vldparam.indexOf('-'))>=0) {
          s=vldparam.substring(0,i).trim();
          try {
            lbound=Float.parseFloat(s);
          }
          catch(NumberFormatException nfe) {
            lbound=0; // default lbound
          }
          s=vldparam.substring(i+1).trim();
          try {
            ubound=Float.parseFloat(s);
          }
          catch(NumberFormatException nfe) {
            ubound=999999;  // default ubound
          }
        } else {
          // default
          lbound=0;
          ubound=999999;
        }
        // length is dictated by number of chars required to string-wise represent the ubound
        length=String.valueOf(ubound).length();
        size=length+1;  // presume precision of 1
        break;

      // max length vld param
      case ACRDefItem.DATATYPE_STRING:

        if(vldparam.length()>0) {
          maxlen=StringUtilities.stringToInt(vldparam,50);
        } else {
          // default
          maxlen=50;
        }

        if(maxlen<20) {
          size=maxlen;
        } else {
          size=20;
        }

        length=maxlen;

        break;
    }

    // instantiate the appropriate Field sub-class
    switch(acrdi.getDataType()) {

      case ACRDefItem.DATATYPE_BOOLEAN:
        field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),(acrdi.getDefaultValue().equals("1")? true:false));
        break;

      case ACRDefItem.DATATYPE_DATE:
        field = new DateField(acrdi.getName(),!acrdi.isRequired());
        break;

      case ACRDefItem.DATATYPE_MONEY:
        field = new CurrencyField(acrdi.getName(),length,size,!acrdi.isRequired(),lbound,ubound);
        break;

      case ACRDefItem.DATATYPE_NUMERIC:
        field = new NumberField(acrdi.getName(),length,size,!acrdi.isRequired(),1,lbound,ubound);
        break;

      case ACRDefItem.DATATYPE_STRING:
        if(length>50)
          field = new TextareaField(acrdi.getName(),length,(size/50),50,!acrdi.isRequired());
        else
          field = new Field(acrdi.getName(),length,size,!acrdi.isRequired());
        break;

      default:
        field = null;
        break;
    }

    if(field!=null)
      field.setShowName(acrdi.getLabel());

    //set default value if specified
    if(acrdi.hasDefaultValue())
      field.setData(acrdi.getDefaultValue());

    return field;
  }

  /**
   * getJSPIncludeFilename()
   *
   * Returns the name of the JSP include file that is intended to
   *  represent the JSP file whose mission is to present the assoc. data.
   */
  public String getJSPIncludeFilename()
  {
    return (acr==null)? "":"acr_incl_"+acr.getDefinition().getShortName()+".jsp";
  }

  public String getIncludeName()
  {
    return acr==null ? "" : acr.getDefinition().getJspInclude();
  }

  /**
   * prepTextMessages()
   *
   * Transfers FieldBean based messages (usu. error messages) to the TextMessageManager.
   */
  public final void prepTextMessages()
  {
    if(tmm==null)
      return;

    StringBuffer sb = new StringBuffer();

    // iterate through any field errors and add any found as string to tmm
    for (Iterator i = getErrors().iterator(); i.hasNext();)
    {
      FieldError fe = (FieldError)i.next();

      sb.setLength(0);
      sb.append(fe.getErrorText());

      try
      {
        if(fe.getFieldLabel().length()>0)
        {
          sb.insert(0, ": ");
          sb.insert(0, fe.getFieldLabel());
        }
      }
      catch(Exception e){}

      tmm.err(sb.toString());
    }

    /*
    // pump out the transormed field errors
    Vector fldErrors = fields.getErrors();
    //log.debug("prepTextMessages("+this.getClass().getName()+") Num fldErrors="+fldErrors.size());
    if(fldErrors != null && fldErrors.size()>0) {
      for(Enumeration e=fldErrors.elements();e.hasMoreElements();)
      {
        tmm.err(e.nextElement().toString());
    }
    */
  }

  public boolean isMESBank(int bankNum)
  {
    if(bankNum==3941 || bankNum==3942 || bankNum==3943)
    {
      return true;
    }
    return false;
  }

  public boolean isMESBank()
  {
    return isMESBank(getBankNumber());
  }

  public boolean isCBTBank(int bankNum)
  {
    if(bankNum==3858)
    {
      return true;
    }
    return false;
  }

  public boolean isCBTBank()
  {
    return isCBTBank(getBankNumber());
  }

  public int getBankNumber()
  {
    if(null!=merchInfo)
    {
      log.debug("Bank Number = "+merchInfo.getMerchBankNumber());
      return merchInfo.getMerchBankNumber();
    }
    else
    {
      log.debug("Bank Number = merchInfo IS NULL");
      return -1;
    }
  }

  public void setResidentQueue(int queue)
  {
    residentQueue = queue;
  }


} // class ACRDataBeanBase
