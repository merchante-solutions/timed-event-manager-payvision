/*@lineinfo:filename=EquipCallTagTable*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.tools.DropDownTable;
import com.mes.tools.EquipmentKey;
import com.mes.tools.Key;
import com.mes.tools.TableView;
import com.mes.tools.TableViewHelper;
import sqlj.runtime.ResultSetIterator;


public final class EquipCallTagTable extends SQLJConnectionBase
  implements TableView
{
  // create class log category
  static Logger log = Logger.getLogger(EquipCallTagTable.class);

  // constants

  public static final int             EMPTY_ROWS        = 2;

  // column identifiers
  public static final int             COL_SENDCALLTAG       = 1;
  public static final int             COL_PARTNUMBER        = 2;
  public static final int             COL_SERIALNUMBER      = 3;
  public static final int             COL_VNUMBER           = 4;
  public static final int             COL_DISPOSITION       = 5;
  public static final int             COL_DESCRIPTION       = 6;
  public static final int             COL_EQUIPTYPE         = 7;

  // native column names
  public static final String          NCN_SENDCALLTAG       = "send_calltag";
  public static final String          NCN_PARTNUMBER        = "part_number";
  public static final String          NCN_SERIALNUMBER      = "serial_number";
  public static final String          NCN_VNUMBER           = "v_number";
  public static final String          NCN_DISPOSITION       = "disposition";
  public static final String          NCN_DESCRIPTION       = "description";
  public static final String          NCN_EQUIPTYPE         = "equip_type";


  // data members
  protected ACRDataBeanBase   acrdb               = null;
  protected List              list                = null;
  protected TableViewHelper   helper              = null;
  protected int[]             columns             = null;
  protected String[][]        columnData          = null;
  protected String[][]        map                 = null;
  protected int               numRows             = 0;
  protected boolean           showEmptyRows       = false;


  // construction
  public EquipCallTagTable(ACRDataBeanBase acrdb, int[] columns, String[][] map, int numRows, boolean showEmptyRows)
  {
    reset(acrdb,columns,map,numRows,showEmptyRows);
  }

  public void reset(ACRDataBeanBase acrdb, int[] columns, String[][] map, int numRows, boolean showEmptyRows)
  {
    this.acrdb          = acrdb;
    this.columns        = columns;
    this.map            = map;
    this.numRows        = numRows;
    this.showEmptyRows  = showEmptyRows;

    ResultSetIterator it = null;
    ResultSet         rs = null;

    try {

      // fill column data array
      columnData = new String[columns.length][2];
      for(int i=0; i<columns.length; i++) {

        switch(columns[i]) {
          case COL_SENDCALLTAG:
            columnData[i][0] = "Call Tag?";
            columnData[i][1] = "getSendCallTag";
            break;
          case COL_PARTNUMBER:
            columnData[i][0] = "Model";
            columnData[i][1] = "getPartNumber";
            break;
          case COL_SERIALNUMBER:
            columnData[i][0] = "Serial#";
            columnData[i][1] = "getSerialNumber";
            break;
          case COL_VNUMBER:
            columnData[i][0] = "V#";
            columnData[i][1] = "getVNumber";
            break;
          case COL_DISPOSITION:
            columnData[i][0] = "Dsptn";
            columnData[i][1] = "getDisposition";
            break;
          case COL_DESCRIPTION:
            columnData[i][0] = "Desc";
            columnData[i][1] = "getDescription";
            break;
          case COL_EQUIPTYPE:
            columnData[i][0] = "Equip Type";
            columnData[i][1] = "getEquipType";
            break;
          default:
            log.warn("Unknown column constant: "+columns[i]+".  Skipping.");
        }
      }

      // instantiate the table view helper
      this.helper = new TableViewHelper(EquipCallTagData.class, this, columnData);

      list = new Vector(0,1);

      if(acrdb.inSubmitMode()) {

        // load equipment data
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:127^9*/

//  ************************************************************
//  #sql [Ctx] it = { select 
//              pt.description          description, 
//              ps.disp_code            disposition, 
//              pt.model_code           part_number, 
//              ps.serial_num           serial_number, 
//              ''                      v_number,
//              --msi.vnum                v_number, 
//              pf.feat_name            equip_type 
//            from 
//              tmg_part_states ps, 
//              tmg_deployment d, 
//              tmg_part_types pt, 
//              tmg_part_feature_maps pfm, 
//              tmg_part_features pf
//              --mms_stage_info msi 
//            where 
//              ps.deploy_id = d.deploy_id 
//              and d.merch_num = :acrdb.getMerchantNumber() 
//              and ps.pt_id = pt.pt_id 
//              and pt.pt_id = pfm.pt_id 
//              and pfm.feat_id = pf.feat_id 
//              and pf.ft_code = 'CATEGORY' 
//              --and d.merch_num = msi.merch_number(+) 
//              and d.end_ts is null 
//            order by equip_type,description
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3587 = acrdb.getMerchantNumber();
  try {
   String theSqlTS = "select \n            pt.description          description, \n            ps.disp_code            disposition, \n            pt.model_code           part_number, \n            ps.serial_num           serial_number, \n            ''                      v_number,\n            --msi.vnum                v_number, \n            pf.feat_name            equip_type \n          from \n            tmg_part_states ps, \n            tmg_deployment d, \n            tmg_part_types pt, \n            tmg_part_feature_maps pfm, \n            tmg_part_features pf\n            --mms_stage_info msi \n          where \n            ps.deploy_id = d.deploy_id \n            and d.merch_num =  :1  \n            and ps.pt_id = pt.pt_id \n            and pt.pt_id = pfm.pt_id \n            and pfm.feat_id = pf.feat_id \n            and pf.ft_code = 'CATEGORY' \n            --and d.merch_num = msi.merch_number(+) \n            and d.end_ts is null \n          order by equip_type,description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.EquipCallTagTable",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3587);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.EquipCallTagTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:154^9*/

        // generate list from recordset
        rs = it.getResultSet();
        while(rs.next()) {
          list.add(new EquipCallTagData(rs));
          log.debug("serial_number="+rs.getString("serial_number"));
        }

        // set initial values in ACR (map)
        if(list.size() > 0) {

          if(list.size()>this.numRows)
          {
            this.numRows = list.size()+EMPTY_ROWS;
          }

          for(int j=1; j<=this.numRows; j++) {
            for(int i=0; i<columns.length; i++) {
              String fldNme = map(ncn(i+1),j);
              String fldDta = (j <= list.size())? getCell(j, i+1) : "";
              acrdb.getACR().setACRItem(fldNme,fldDta);
            }

          }
        }
      } else {
        // generate list from acr
        int i = 0;
        while(equipmentExists(++i)) {
          list.add( new EquipCallTagData(
             acrdb.getACRValue(map(NCN_PARTNUMBER,i))
            ,acrdb.getACRValue(map(NCN_SERIALNUMBER,i))
            ,acrdb.getACRValue(map(NCN_VNUMBER,i))
            ,acrdb.getACRValue(map(NCN_DISPOSITION,i))
            ,acrdb.getACRValue(map(NCN_DESCRIPTION,i))
            ,acrdb.getACRValue(map(NCN_EQUIPTYPE,i))
            ,acrdb.getACRValue(map(NCN_SENDCALLTAG,i)) )
          );
        }

        if(list.size()>this.numRows)
        {
          this.numRows = list.size()+EMPTY_ROWS;
        }
      }

    }
    catch(Exception e) {
      log.error("reset() EXCEPTION: "+e.toString());
    }
    finally {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void createFields(FieldGroup fields)
  {
    try {

      String fldnme,label;

      for(int i=1;i<=numRows;i++) {

        for(int j=1;j<=columns.length;j++) {

          label = columnData[j-1][0];

          switch(columns[j-1]) {
            case COL_SENDCALLTAG:
              fldnme = map(NCN_SENDCALLTAG,i);
              fields.add( new CheckboxField(fldnme,"",false) );
              break;
            case COL_PARTNUMBER:
              fldnme = map(NCN_PARTNUMBER,i);
              fields.add(new Field(fldnme,label,30,10,true));
              break;
            case COL_SERIALNUMBER:
              fldnme = map(NCN_SERIALNUMBER,i);
              fields.add(new Field(fldnme,label,30,14,true));
              break;
            case COL_VNUMBER:
              fldnme = map(NCN_VNUMBER,i);
              fields.add(new Field(fldnme,label,8,8,true));
              break;
            case COL_DISPOSITION:
              fldnme = map(NCN_DISPOSITION,i);
              fields.add(new DropDownField(fldnme,label,new EquipmentDispositionDownTable(),true));
              break;
            case COL_DESCRIPTION:
              fldnme = map(NCN_DESCRIPTION,i);
              fields.add(new Field(fldnme,label,50,25,true));
              break;
            case COL_EQUIPTYPE:
              fldnme = map(NCN_EQUIPTYPE,i);
              fields.add(new DropDownField(fldnme,label,new EquipmentTypeDropDownTable(),true));
              break;
          }

        }

      }

    }
    catch(Exception e) {
      log.error("createFields() EXCEPTION: "+e.toString());
    }
  }

  public String renderHtmlTable()
  {
    boolean noEquipment = acrdb.inSubmitMode()?
        (!showEmptyRows && (list.size() < 1 || numRows < 1) )
      : (list.size() < 1);

    StringBuffer sb = new StringBuffer();

    sb.append("<div><span class=\"smallBold\">Equipment</span></div>");

    if(noEquipment) {

      sb.append("<span class=\"smallText\">No equipment specified.</span>");

    } else {

      sb.append("<table cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" height=\"29\" class=\"tableFill\" cellspacing=\"0\">");
        sb.append("<tr>");
          sb.append("<td height=\"24\">");
            sb.append("<table border=\"0\" align=\"center\" width=\"100%\" cellpadding=\"4\" cellspacing=\"1\">");

              // column headings
              sb.append("<tr>");
              String[] colNames = getColumnNames();
              for(int i=0; i<colNames.length; i++) {
                sb.append("<th align=left class=tableColumnHead>");
                sb.append(colNames[i]);
                sb.append("</th>");
              }
              sb.append("</tr>");

              // column data
              boolean tgl = false;
              for(int j=0;j<numRows;j++) {
                if(acrdb.inStatusMode() && !equipmentExists(j+1))
                  continue;
                String rowColor = ((tgl=(!tgl)) ? "tableRowHighlightOff" : "tableRowHighlightOn");
                sb.append("<tr class="+rowColor+">");
                for(int i=0; i<colNames.length; i++) {
                 sb.append("<td align=center class=tableDataPlain>");
                  sb.append(getHtmlCell(j+1,i+1));
                  sb.append("</td>");
                }
                sb.append("</tr>");
              }

            sb.append("</table>");
          sb.append("</td>");
        sb.append("</tr>");
      sb.append("</table>");

    }

    sb.append("</div>");

    return sb.toString();
  }

  public boolean equipmentExists(int rowOrdinal)
  {
    try {
      ACR acr = acrdb.getACR();
      return ( acr.itemExists( map(NCN_VNUMBER,rowOrdinal) ) || acr.itemExists( map(NCN_SERIALNUMBER,rowOrdinal) ) );
     }
     catch(Exception e) {
      log.error("equipmentExists() EXCEPTION: "+e.toString());
      return false;
     }
  }

  public boolean equipmentExists()
  {
    for(int i=1; i<=columns.length; i++) {
      if( equipmentExists(i) )
        return true;
    }

    return false;
  }

  public boolean calltaggedEquipmentExists()
  {
    try {
      ACR acr = acrdb.getACR();
      for(int i=1; i<=numRows; i++) {
        if( acr.getACRValue( map(NCN_SENDCALLTAG,i) ).equals("y") || acr.getACRValue( map(NCN_SENDCALLTAG,i) ).equals("Yes") )
          return true;
      }
    }
    catch(Exception e) {
      log.error("calltaggedEquipmentExists() EXCEPTION: "+e.toString());
    }

    return false;
  }


  /**
   * getCallTaggedEquipment()
   *
   * Returns collection of EquipmentKey objects representing the equipment to be call tagged.
   */
  public Vector getCallTaggedEquipment()
  {
    ACR acr = acrdb.getACR();

    if(acr==null)
      return null;

    Vector v = new Vector(0,1);
    //used to be getNumDataRows...?
    for(int i=1; i<=getNumRows(); i++) {
      try {
        String s = acr.getACRValue(map(NCN_SENDCALLTAG,i));
        if( !s.equals("y") && !s.equals("Yes") )
          continue;
        String sn = acr.getACRValue(map(NCN_SERIALNUMBER,i));
        EquipmentKey key = new EquipmentKey(null,sn);
        v.add(key);
      }
      catch(Exception e) {
        log.error("getCallTaggedEquipment() EXCEPTION: "+e.toString()+".  Continuing.");
        continue;
      }
    }

    return v;
  }

  public List           getRows()
  {
    return list;
  }

  public void           setRows(List list)
  {
    this.list = list;
  }

  public Key            getRowKey(int rowOrdinal)
  {
    EquipCallTagData d = (EquipCallTagData)getRowObject(rowOrdinal);
    return new EquipmentKey(d.getPartNumber(),d.getSerialNumber());
  }

  public Object         getRowObject(int rowOrdinal)
  {
    return helper.getRowObject(rowOrdinal);
  }

  public int            getNumRows()
  {
    return numRows;
  }

  public int getNumDataRows()
  {
    return list==null? 0:list.size();
  }

  public int            getNumCols()
  {
    return columnData.length;
  }

  public String         getLinkToDetailColumnName()
  {
    return "";
  }

  public String         getDetailPageBaseUrl()
  {
    return "";
  }

  public String         getQueryStringSuffix()
  {
    return "";
  }

  public void           setQueryStringSuffix(String v)
  {
  }

  public String getDetailPageQueryString(int rowOrdinal)
  {
    return "";
  }

  public Iterator       getRowIterator()
  {
    return list==null? null:list.iterator();
  }

  public String         getColumnName(int colOrdinal)
  {
    return helper.getColumnName(colOrdinal);
  }

  public String[]       getColumnNames()
  {
    return helper.getColumnNames();
  }

  public String         getCell(int rowOrdinal, int colOrdinal)
  {
    return helper.getCell(rowOrdinal, colOrdinal);
  }

  public String         getHtmlCell(int rowOrdinal, int colOrdinal)
  {
    String ncn    = ncn(colOrdinal);
    String fldNme = map(ncn,rowOrdinal);

    if(acrdb.inStatusMode()) {
      String acrValue = acrdb.getACR().getACRValue(fldNme);
      if(ncn.equals("send_calltag")) {
        if(acrValue.equals("y") || acrValue.equals("Yes")) {
          StringBuffer sb = new StringBuffer();
          sb.append("<a href=\"");
          sb.append(ACRDataBeanBase.RELPATH_CALLTAG_DETAIL);
          sb.append("&acrid=");
          sb.append(acrdb.getACR().getID());
          sb.append("\">");
          sb.append("Yes");
          sb.append("</a>");
          return sb.toString();
        } else {
          return "No";
        }
      }
    }

    String s = acrdb.renderHtml(fldNme);

    if(s==null || s.length()<1)
      s = "--";

    return s;
  }

  /**
   * EquipCallTagData class
   */
  public class EquipCallTagData
  {
    protected String     partNumber            = null;
    protected String     serialNumber          = null;
    protected String     vNumber               = null;
    protected String     disposition           = null;
    protected String     description           = null;
    protected String     equipType             = null;
    protected String     sendCallTag           = null;

    public    String     getPartNumber()      {return partNumber; }
    public    String     getSerialNumber()    {return serialNumber; }
    public    String     getVNumber()         {return vNumber; }
    public    String     getDisposition()     {return disposition; }
    public    String     getDescription()     {return description; }
    public    String     getEquipType()       {return equipType; }
    public    String     getSendCallTag()     {return sendCallTag; }

    public EquipCallTagData(ResultSet rs)
    {
      try {
        partNumber            = rs.getString("part_number");
        serialNumber          = rs.getString("serial_number");
        vNumber               = rs.getString("v_number");
        disposition           = rs.getString("disposition");
        description           = rs.getString("description");
        equipType             = rs.getString("equip_type");
        sendCallTag           = (disposition.equalsIgnoreCase("Deployed-Loan")
                              || disposition.equalsIgnoreCase("Deployed-Rent")
                              || disposition.equalsIgnoreCase("Deployed-Unknown"))?
                                    "y" : "n";
      }
      catch(Exception e) {
        log.error("EquipCallTagData construction EXCEPTION: "+e.toString());
      }
    }
    public EquipCallTagData(
       String pn
      ,String sn
      ,String vn
      ,String dsp
      ,String dsc
      ,String et
      ,String sct
      )
    {
      partNumber            = pn;
      serialNumber          = sn;
      vNumber               = vn;
      disposition           = dsp;
      description           = dsc;
      equipType             = et;
      sendCallTag           = sct;
    }

  }

  public String ncn(int colOrdinal)
  {
    String rval = "";
    try {
      switch(columns[colOrdinal-1]) {
        case COL_SENDCALLTAG:
          rval = NCN_SENDCALLTAG;
          break;
        case COL_PARTNUMBER:
          rval = NCN_PARTNUMBER;
          break;
        case COL_SERIALNUMBER:
          rval = NCN_SERIALNUMBER;
          break;
        case COL_VNUMBER:
          rval = NCN_VNUMBER;
          break;
        case COL_DISPOSITION:
          rval = NCN_DISPOSITION;
          break;
        case COL_DESCRIPTION:
          rval = NCN_DESCRIPTION;
          break;
        case COL_EQUIPTYPE:
          rval = NCN_EQUIPTYPE;
          break;
        default:
          rval = "ERROR";
      }
    }
    catch(Exception e) {
      log.error("ncn() EXCEPTION: "+e.toString());
    }
    return rval;
  }

  public String map(String ncn, int rowOrdinal)
  {
    String rval = ncn;
    try {
      for(int i=0; i<map.length; i++) {
        if(map[i][0].equals(ncn)) {
          rval = map[i][1] + "_" + rowOrdinal;
          break;
        }
      }
    }
    catch(Exception e) {
      log.error("map(ncn: "+ncn+", rowOrdinal: "+rowOrdinal+") EXCEPTION: "+e.toString());
    }
    return rval;
  }

  public class EquipmentDispositionDownTable extends DropDownTable
  {
    public EquipmentDispositionDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","");

      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try {

        connect();

        /*@lineinfo:generated-code*//*@lineinfo:637^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    equip_status_desc
//                      ,equip_status_desc
//            from      equip_status
//            order by  equip_status_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    equip_status_desc\n                    ,equip_status_desc\n          from      equip_status\n          order by  equip_status_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.EquipCallTagTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.acr.EquipCallTagTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:643^9*/

        rs = it.getResultSet();

        while (rs.next())
          addElement(rs);
      }
      catch (Exception e) {
        log.error("EquipmentDispositionDownTable.getData() EXCEPTION: "+e.toString());
      }
      finally {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }

    }

  }

  public class EquipmentTypeDropDownTable extends DropDownTable
  {
    public EquipmentTypeDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","");

      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try {

        connect();

        /*@lineinfo:generated-code*//*@lineinfo:681^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    EQUIPTYPE_DESCRIPTION
//            from      equiptype
//            order by  EQUIPTYPE_DESCRIPTION asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    EQUIPTYPE_DESCRIPTION\n          from      equiptype\n          order by  EQUIPTYPE_DESCRIPTION asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.acr.EquipCallTagTable",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.acr.EquipCallTagTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:686^9*/

        rs = it.getResultSet();

        String val,desc;

        while (rs.next()) {
          val = rs.getString(1);
          desc = val;
          addElement(val,desc);
        }

      }
      catch (Exception e) {
        log.error("EquipmentTypeDropDownTable.getData() EXCEPTION: '"+e.getMessage()+"'.");
      }
      finally {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }

    }

  }

}/*@lineinfo:generated-code*/