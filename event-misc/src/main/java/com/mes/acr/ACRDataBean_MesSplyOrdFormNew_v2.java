package com.mes.acr;

// log4j
import org.apache.log4j.Logger;

public class ACRDataBean_MesSplyOrdFormNew_v2 extends ACRDataBean_MesSplyOrdFormNew
{

  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_MesSplyOrdFormNew_v2.class);

  // construction
  public ACRDataBean_MesSplyOrdFormNew_v2()
  {
  }

  public ACRDataBean_MesSplyOrdFormNew_v2(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public String getHeadingName()
  {
    return "Order Supplies for Merchant";
  }

  //overrides to ensure that FT_CBT_SHIPPING is used
  protected AddressHelper getAddressHelper()
  {
    log.debug("...in the CBT version of getAddressHelperNEW");
    if (addressHelper == null)
    {
      addressHelper = new AddressHelper(getMerchantNumber(),"ship",
                                        AddressHelper.FT_CBT_SHIPPING);
    }
    return addressHelper;
  }

} // class ACRDataBean_MesSplyOrdFormNew_v2