package com.mes.acr;

import com.mes.forms.SelectorColumnDef;

public class SF_AcctClose extends SF_EquipBase
{
  public SF_AcctClose(String title)
  {
    super(title);
  }

  public void setupFrame()
  {
    addColumn(new SelectorColumnDef("col1","Serial No.",  105));
    addColumn(new SelectorColumnDef("col2","Description", 125));
    addColumn(new SelectorColumnDef("col3","Equip Type",  185));
    addColumn(new SelectorColumnDef("col4","Part No.",    85));
    addColumn(new SelectorColumnDef("col5","Disposition", 185));
  }
}

