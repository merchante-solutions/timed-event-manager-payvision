package com.mes.acr;

import java.util.Iterator;
import java.util.List;
import com.mes.forms.MultiSelector;
import com.mes.forms.SelectorFrame;

public class ACRMultiSelector extends MultiSelector implements ACRTranslator
{
  public ACRMultiSelector(String name, List selectorItems,
    SelectorFrame selectorFrame)
  {
    super(name,selectorItems,selectorFrame);
  }

  public ACRMultiSelector(String name, List selectorItems,
    SelectorFrame selectorFrame, boolean isRequired)
  {
    super(name,selectorItems,selectorFrame, isRequired);
  }

  public ACRMultiSelector(String name, List selectorItems)
  {
    super(name,selectorItems);
  }

  public void doPush(ACR acr)
  {
    List selected = getSelected();
    for (Iterator i = selected.iterator(); i.hasNext();)
    {
      ((ACRTranslator)i.next()).doPush(acr);
    }
  }

  public void doPull(ACR acr)
  {
    List selected = getSelected();
    for (Iterator i = selected.iterator(); i.hasNext();)
    {
      ((ACRTranslator)i.next()).doPull(acr);
    }
  }
}