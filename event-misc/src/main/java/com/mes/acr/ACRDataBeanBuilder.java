package com.mes.acr;

import java.util.Hashtable;
import java.util.List;
// log4j
import org.apache.log4j.Logger;
import com.mes.user.UserBean;


/**
 * class ACRDataBeanBuilder
 *
 * Builds concrete ACRDataBeanBase sub-classes.
 */
public final class ACRDataBeanBuilder
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBeanBuilder.class);


  // data members
  // (NONE)

  // class methods

  public static ACRDataBean_Main buildMain(MerchantInfo merchantInfo,
    List acrDefs, Hashtable submitBeans, UserBean user)
  {
    try {
      return new ACRDataBean_Main(merchantInfo,acrDefs,submitBeans,user);
    }
    catch(Exception e) {
      log.error("Instantiate ACR Main bean failed: '"+e.getMessage()+"'.");
      return null;
    }
  }

  public static ACRDataBean_Summary buildPreSubmitSummary(MerchantInfo merchantInfo,Hashtable submitBeans)
  {
    try {
      return new ACRDataBean_Summary(merchantInfo,submitBeans,false);
    }
    catch(Exception e) {
      log.error("Instantiate ACR Pre Submit bean failed: '"+e.getMessage()+"'.");
      return null;
    }
  }

  public static ACRDataBean_Summary buildPostSubmitSummary(MerchantInfo merchantInfo,Hashtable submitBeans)
  {
    try {
      return new ACRDataBean_Summary(merchantInfo,submitBeans,true);
    }
    catch(Exception e) {
      log.error("Instantiate ACR Post Submit bean failed: '"+e.getMessage()+"'.");
      return null;
    }
  }

  public static ActionHistory buildActionHistory(ACRDataBeanBase acrdb)
  {
    try {
      return new ActionHistory(acrdb);
    }
    catch(Exception e) {
      log.error("Instantiate ActionHistory bean failed: '"+e.getMessage()+"'.");
      return null;
    }
  }

  public static ACRDataBeanBase buildDetailInSubmitMode(ACR acr,Requestor requestor,MerchantInfo merchantInfo,UserBean user)
  {
    return buildDetail(acr,requestor,merchantInfo,true,user);
  }

  public static ACRDataBeanBase buildDetailInStatusMode(ACR acr,UserBean user)
  {
    return buildDetail(acr,null,null,false,user);
  }

  private static ACRDataBeanBase buildDetail(ACR acr, Requestor requestor,
    MerchantInfo merchInfo, boolean isEditable, UserBean user)
  {
    if (acr == null)
    {
      log.error("Cannot built detail bean with null ACR");
      throw new NullPointerException("Cannot build detail bean with null ACR");
    }

    if (isEditable && requestor == null)
    {
      log.error("Cannot build new detail bean with null requestor");
      throw new NullPointerException("Cannot build detail bean with null"
        + " requestor");
    }

    if (isEditable && merchInfo == null)
    {
      log.error("Cannot build new detail bean with null merch info");
      throw new NullPointerException("Cannot build detail bean with null"
        + " merch info");
    }

    if (user == null)
    {
      log.error("Cannot build detail bean with null user");
      throw new NullPointerException("Cannot build detail bean with null"
        + " user");
    }

    ACRDefinition acrDef = acr.getDefinition();
    String className = acrDef.getBeanClassName();
    if (className == null)
    {
      log.error("Cannot build detail bean because class name in definition"
        + " is null");
      throw new NullPointerException("Cannot build detail bean because"
        + " class name in definition is null");
    }

    log.debug("building ACR detail bean for " + className);

    ACRDataBeanBase bean = null;

    try
    {
      bean = (ACRDataBeanBase)Class.forName(className).newInstance();
      bean.setUser(user);
      bean.initialize(acr,merchInfo,requestor,isEditable);
      bean.acrCreateFields();

      log.debug("ACR detail bean " + className + " built and initialized"
        + " (id: " + acrDef.getID() + ", '" + acrDef.getShortName() + "')");
    }
    catch (Exception e)
    {
      log.error("Failed to create detail bean: " + e.getMessage());
    }

    return bean;

    /*
    ACRDataBeanBase bean=null;
    log.debug("************building ACR Bean");
    try {

      final ACRDefinition acrd = (acr==null)? null:acr.getDefinition();
      final String acrDefShrtNme = (acrd==null)? "":acrd.getShortName();

      final int acrAppTypeCode = (merchantInfo==null)? ((acr==null)? -1:StringUtilities.stringToInt(acr.getACRValue("ACR Type Code"),-1)):merchantInfo.getACRAppTypeCode();

      final boolean isCBT   = (acrAppTypeCode == 1 || acrAppTypeCode == 31);
      final boolean isNBSC  = (acrAppTypeCode == 10);

      log.debug("merchantInfo is " + (merchantInfo == null ? "" : "NOT") + " null");
      log.debug("acrAppTypeCode (aka appType) is " + acrAppTypeCode);
      log.debug("isCBT = " + isCBT + ", isNBSC = " + isNBSC);

      //several objects need to have two definitions, or legacy will break
      //_v2 represents the newer object definitions

      if(acrDefShrtNme.equals("feechng"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_FeeChange_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_FeeChange(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("termacces"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_TerminalAccessory_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_TerminalAccessory(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("termacces_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_TermAcc_CBT_v2(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("ddachng"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_DDA_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_DDA(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("ddachng_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_DDA_CBT(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("cardadds"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_CardAdds_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_CardAdds(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("cardadds_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_CardAdds_CBT_v2(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("acccls"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_AcntClosure_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_AcntClosure(acr,merchantInfo,requestor,editable);
        }
      }
      else if(acrDefShrtNme.equals("acccls_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_AcntClosure_CBT_v2(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("supordfrm"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_MesSplyOrdForm_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_MesSplyOrdForm(acr,merchantInfo,requestor,editable);
        }
      }
      else if(acrDefShrtNme.equals("newsupordfrm"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_MesSplyOrdFormNew_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_MesSplyOrdFormNew(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("newsupordfrm_v2"))
      {
         //if(isCBT)
         {
           bean = new ACRDataBean_MesSplyOrdFormNew_v2(acr,merchantInfo,requestor,editable);
         }
      }
      else if(acrDefShrtNme.equals("adrchng"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_AdrsChng_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_AdrsChng(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("adrchng_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_AdrsChng_CBT_v2(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("dbachng"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_DbaChng_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_DbaChng(acr,merchantInfo,requestor,editable);
        }
      }
      else if(acrDefShrtNme.equals("dbachng_v2"))
      {
        if (isCBT)
        {
          bean = new ACRDataBean_DbaChng_CBT_v2(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("feervsl"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_FeeReversal_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_FeeReversal(acr,merchantInfo,requestor,editable);
        }
      }
      else if(acrDefShrtNme.equals("feervsl_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_FeeReversal_CBT_v2(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("reopcl"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_AcntCloseReopen_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_AcntCloseReopen(acr,merchantInfo,requestor,editable);
        }
      }
      else if(acrDefShrtNme.equals("equipswap"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_EquipSwap_CBT(acr,merchantInfo,requestor,editable);
        }
        else if(isNBSC)
        {
          bean = new ACRDataBean_EquipSwap_NBSC(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_EquipSwap(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("equipswap_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_EquipSwap_CBT_v2(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("equipupg"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_EquipUpgrade_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_EquipUpgrade(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("equipupg_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_EquipUpgrade_CBT_v2(acr,merchantInfo,requestor,editable);
        }
      }
      else if(acrDefShrtNme.equals("reqtrmprg"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_ReqTrmPrg_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_ReqTrmPrg(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("reqtrmprg_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_ReqTrmPrg_CBT_v2(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("equippu"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_EquipPU_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_EquipPU(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("equippu_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_EquipPU_CBT_v2(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("prcchng"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_PriceChng_CBT(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("prcchng_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_PriceChng_CBT_v2(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("rfnewtype"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_RFNewType_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_RFNewType(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("merccntctinfo"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_MerchCntctInfo_CBT(acr,merchantInfo,requestor,editable);
        }
        else
        {
          bean = new ACRDataBean_MerchCntctInfo(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("merccntctinfo_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_MerchCntctInfo_CBT_v2(acr,merchantInfo,requestor,editable);
        }
      }
      else if(acrDefShrtNme.equals("supbilconf"))
      {

        bean = new ACRDataBean_SupplyBilling(acr,merchantInfo,requestor,editable);

      }
      else if(acrDefShrtNme.equals("seasactvt_v2"))
      {
        if(isCBT)
        {
          bean = new ACRDataBean_SeasActvt_CBT(acr,merchantInfo,requestor,editable);
        }

      }
      else if(acrDefShrtNme.equals("reopen_v2"))
      {
         if(isCBT)
         {
           bean = new ACRDataBean_AcntReopen_CBT(acr,merchantInfo,requestor,editable);
         }

      }
      else if(acrDefShrtNme.equals("equipdisp_v2"))
      {
         if(isCBT)
         {
           bean = new ACRDataBean_EquipDisp_CBT(acr,merchantInfo,requestor,editable);
         }
      }
      else if(acrDefShrtNme.equals("othercardmgmt_v2"))
      {
         if(isCBT)
         {
           bean = new ACRDataBean_OtherCardMgmt_CBT(acr,merchantInfo,requestor,editable);
         }
      }

      // Unhandled
      if(bean==null){
        throw new Exception("Unable to instantiate ACR Data Bean: Unhandled ACR Definition short name: '"+acrDefShrtNme+"'.");
      }

      // create the fields
      bean.acrCreateFields();

      // add UserBean ref to ACR
      bean.setUser(user);

    }
    catch(Exception e) {
      e.printStackTrace();
      log.error("Instantiate ACR Detail data bean failed: '"+e.getMessage()+"'.");
      bean=null;
    }

    return bean;
    */
  }

  // object methods

  // construction
  private ACRDataBeanBuilder()
  {
  }

} // class ACRDataBeanBuilder
