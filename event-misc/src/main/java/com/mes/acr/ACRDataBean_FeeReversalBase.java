package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.CurrencyField;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;
import com.mes.tools.DropDownTable;


public abstract class ACRDataBean_FeeReversalBase extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_FeeReversalBase.class);

  // construction
  public ACRDataBean_FeeReversalBase()
  {
  }

  public ACRDataBean_FeeReversalBase(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected void setDefaults()
  {
    if(acr==null)
      return;

    // set default values for accept/amount fields
    acr.setACRItem("requestStatus","Not set");
    acr.setACRItem("acceptAmt","0");
    acr.setACRItem("acceptUserId","");
  }

  public boolean isValid()
  {
    if(!editable) {
      fields.getField("requestStatus").setNullAllowed(residentQueue != MesQueues.Q_CHANGE_REQUEST_RESEARCH);
      fields.getField("acceptAmt").setNullAllowed(residentQueue != MesQueues.Q_CHANGE_REQUEST_RESEARCH);
    }

    return super.isValid();
  }

  protected void createExtendedFields()
  {
    if(acr==null)
      return;

    Field field=null;

    if(!editable) {

      // on hold checkbox only for status view
      field = new CheckboxField("onHold","On hold?",false);
      fields.add(field);

      // accepted/denied (editable in status mode)
      String[][] arrReqStat =
      {
         { "Accepted","Accepted" }
        ,{ "Denied","Denied" }
      };
      field = new RadioButtonField("requestStatus",arrReqStat,false,"Please specify whether the request is accepted or denied.");
      field.setLabel("Request Status");
      ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
      fields.add(field);

      // accept amount (editable in status mode)
      field = new CurrencyField("acceptAmt","Accept Amount",9,9,true,-999999f,999999f);
      field.setLabel("Accept Amount");
      field.setData("0");
      fields.add(field);

    }
  }

  public String renderRequestStatusHtmlTable(String cssClassname)
  {
    if(editable)
      return "";

    if(cssClassname == null)
      cssClassname = "";

    StringBuffer sb = new StringBuffer();

    sb.append("<tr><td>");
      sb.append("<table border=0 cellpadding=0 cellspacing=0>");
        sb.append("<tr>");
          sb.append("<td class=\"");
          sb.append(cssClassname);
          sb.append("\">Request Status</td>");
          sb.append("<td width=50>&nbsp;</td>");
          sb.append("<td class=\"");
          sb.append(cssClassname);
          sb.append("\">Accept Amount</td>");

          if(acr.itemHasValue("acceptUserId")) {
            sb.append("<td width=50>&nbsp;</td>");
            sb.append("<td class=\"");
            sb.append(cssClassname);
            sb.append("\">Accept User Id</td>");
          }

        sb.append("</tr>");
        sb.append("<tr>");
          sb.append("<td class=\"formFields\" valign=top>");
          sb.append((residentQueue == MesQueues.Q_CHANGE_REQUEST_RESEARCH)? renderHtml("requestStatus"):getACRValue("requestStatus"));
          sb.append("</td>");
          sb.append("<td width=50>&nbsp;</td>");
          sb.append("<td class=\"formFields\" valign=top>");
          sb.append((residentQueue == MesQueues.Q_CHANGE_REQUEST_RESEARCH)? renderHtml("acceptAmt"):getACRValue("acceptAmt"));
          sb.append("</td>");

          if(acr.itemHasValue("acceptUserId")) {
            sb.append("<td width=50>&nbsp;</td>");
            sb.append("<td class=\"formFields\">");
            sb.append(acr.getACRValue("acceptUserId"));
            sb.append("</td>");
          }

        sb.append("</tr>");
      sb.append("</table>");
    sb.append("</td></tr>");

    return sb.toString();
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_RESEARCH:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Incomplete Information"
          ,"Request Cancelled"
          ,"ACH Reject"
        };
        break;
      case MesQueues.Q_CHANGE_REQUEST_PRICING:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Incomplete Information"
          ,"Request Cancelled"
          ,"Request Declined"
        };
        break;
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Incomplete Information"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }


  public void doPush()
  {
    super.doPush();

    // fee reversal specific logic
    if(editable)
      calcTotalFeeReversals();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable)
    {
      // grab the userid to associate with the accept amount
      if(residentQueue == MesQueues.Q_CHANGE_REQUEST_RESEARCH)
      {
        acr.setACRItem("acceptUserId",(user==null? "":user.getLoginName()));
      }

    }
  }

  //abstract methods that allow child to process uniquely
  protected abstract void calcTotalFeeReversals();
  public abstract float getTotalFeeReversalAmount();
  public abstract int getMaxEntries();

  public class FeeTypeDropDownTable extends DropDownTable
  {
    public FeeTypeDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","Select One");

      addElement("Cancellation Fee","Cancellation Fee");
      addElement("Chargeback Fee","Chargeback Fee");
      addElement("Discount Rate","Discount Rate");
      addElement("Equipment Fee","Equipment Fee");
      addElement("Interchange Downgrade Fee","Interchange Downgrade Fee");
      addElement("Internet Fee","Internet Fee");
      addElement("Merchant Referral","Merchant Referral");
      addElement("Minimum Discount","Minimum Discount");
      addElement("Miscellaneous Refund","Miscellaneous Refund");
      addElement("NSF Fee Reversal","NSF Fee Reversal");
      addElement("P.R./Good Faith","P.R./Good Faith");
      addElement("Statement Fee","Statement Fee");
    }

    /* legacy - once tried to get these dynamically from DB...

      SELECT
                MISC_CODE
                ,MISC_DESCRIPTION
      FROM
                MISCDESCRS
      ORDER BY
                MISC_DESCRIPTION ASC
    */

  } // FeeTypeDropDownTable class

  public String getHeadingName()
  {
    return "Reverse Merchant Fees";
  }
} // class ACRDataBean_FeeReversalBase
