package com.mes.acr;

import java.util.BitSet;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Logger;

/**
 * mesDBACRDefDAO class.
 *
 * ACRDefDAO implementation for the MES db.
 */
class mesDBACRDefDAO implements ACRDefDAO
{
  // create class log category
  static Logger log = Logger.getLogger(mesDBACRDefDAO.class);

  // constants
  // (none)

  // data members
  protected ACRMESDB acrMesDb;
  protected ACROBJMESDB acrObjMesDb;

  // construction
  public mesDBACRDefDAO()
  {
    acrMesDb = new ACRMESDB();
    acrObjMesDb = new ACROBJMESDB();
  }

  // class methods
  // (none)

  // object methods

  public ACRDefinition createACRDefinition()
  {
    final long id = acrMesDb.getNextAvailID(ACRMESDB.BIT_ACRDEF_PRIMARY);
    if(id==-1L)
      return null;

    ACRDefinition acrd = new ACRDefinition();
    acrd.setID(id);

    return acrd;
  }

  public long insertACRDefinition(ACRDefinition acrd)
  {
    log.error("mesDBACRDAO.insertACRDefinition() NOT IMPLEMENTED.");
    return -1L;
  }

  public boolean deleteACRDefinition(ACRDefinition acrd)
  {
    if(acrd.getID()<0) {
      log.error("Unable to delete ACR Definition: No ID specified.");
      return false;
    }

    BitSet dbBits = new BitSet(4);
    dbBits.set(ACRMESDB.BIT_ACRDEF_PRIMARY);
    // ASSUMPTION: ACR Def items assumed to be cascade deleted at db level

    return acrMesDb.delete(dbBits,acrd.getID());
  }

  public ACRDefinition findACRDefinition(long pkid)
  {
    ACRDefinition acrd = new ACRDefinition();

    // retreive primary and all assoc. items
    BitSet dbBits = new BitSet(4);
    dbBits.set(ACRMESDB.BIT_ACRDEF_PRIMARY);
    dbBits.set(ACRMESDB.BIT_ACRDEF_ITEM);

    try {
      if(!acrMesDb.load(pkid,acrd,dbBits))
        return null;
    }
    catch(Exception e) {
      log.error("mesDBACRDefDAO.findACRDefinition() EXCEPTION: '"+e.getMessage()+"'.");
      acrd=null;
    }

    return acrd;
  }

  public boolean updateACRDefinition(ACRDefinition acrd)
  {
    if(acrd.getID()<0) {
      log.error("Unable to update ACR Definition: No ID specified.");
      return false;
    }

    return persistACRDefinition(acrd);
  }

  public boolean persistACRDefinition(ACRDefinition acrd)
  {
    BitSet dbBits = new BitSet(4);
    dbBits.set(ACRMESDB.BIT_ACRDEF_PRIMARY);
    dbBits.set(ACRMESDB.BIT_ACRDEF_ITEM);

    return acrMesDb.persist(acrd,dbBits,false);
  }

  public Vector selectACRDefinitions(String fldNme,String fldVal,boolean bLoadItems)
  {
    BitSet dbBits = new BitSet(4);
    dbBits.set(ACRMESDB.BIT_ACRDEF_PRIMARY);
    if(bLoadItems)
      dbBits.set(ACRMESDB.BIT_ACRDEF_ITEM);

    try {
      return acrMesDb.load(fldNme,fldVal,dbBits);
    }
    catch(Exception e) {
      log.error("mesDBACRDefDAO.selectACRDefinitions() EXCEPTION: '"+e.getMessage()+"'.");
      return null;
    }
  }

  public ACRDefinition findACRDefinitionByACRID(long pkid)
  {
    BitSet dbBits = new BitSet(4);
    dbBits.set(ACRMESDB.BIT_ACRDEF_PRIMARY);
    dbBits.set(ACRMESDB.BIT_ACRDEF_ITEM);

    try {
      final Vector v = acrMesDb.load("ID_ACR",Long.toString(pkid),dbBits);
      if(v==null)
        throw new Exception("acrMesDb.load() returned NULL.");
      if(v.size()==1)
        return (ACRDefinition)v.firstElement();
      else
        return null;
    }
    catch(Exception e) {
      log.error("mesDBACRDefDAO.findACRDefinitionByACRID() EXCEPTION: '"+e.getMessage()+"'.");
      return null;
    }
  }

  public Object getACRRelatedObject(ACR acr){
    try{

     //return acrObjMesDb.getACRRelatedObject(acr);
     return acrMesDb.getACRRelatedObject(acr);
   }catch (Exception e){
     log.debug("Throwing exception in getACRRelatedObject():"+e.getMessage());
     return null;
   }
  }

} // class mesDBACRDefDAO
