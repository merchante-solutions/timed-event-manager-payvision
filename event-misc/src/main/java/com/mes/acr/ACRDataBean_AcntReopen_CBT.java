package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.RadioButtonField;

public final class ACRDataBean_AcntReopen_CBT extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_AcntReopen_CBT.class);

  String closureDate;
  boolean isOverdue;

  // construction
  public ACRDataBean_AcntReopen_CBT()
  {
  }

  public ACRDataBean_AcntReopen_CBT(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void setDefaults()
  {

    String merchNum = getMerchantNumber();

    //little SQL to get closure date (if it exists.)
    if(acr==null || merchNum.length()<1)
    {
      closureDate = "No date available.";
      return;
    }

    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();

      String qs = "select"
                + " nvl(to_char(DATE_STAT_CHGD_TO_DCB,'mm/dd/yyyy'),"
                + " 'Account does not appear to be closed.')  as close_date,"
                + " trunc(sysdate) - trunc(nvl(DATE_STAT_CHGD_TO_DCB,sysdate)) as is_overdue"
                + " from mif"
                + " where merchant_number = ?";

      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);

      rs = ps.executeQuery();

      if (rs.next())
      {
        closureDate = processString(rs.getString("close_date"));
        isOverdue = ( rs.getInt("is_overdue") > 60 );
      }
      else
      {
        closureDate = "Unable to find Merchant Closure Date.";
      }

    }
    catch (Exception e)
    {
      log.error("setDefaults() exception: " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }

  }

  //turn off multimerchant if overdue
  public boolean hasMultiMerchantComp()
  {
    if(isOverdue)
    {
      return false;
    }
    else
    {
      return super.hasMultiMerchantComp();
    }
  }

 //turn off hasEditableComments if overdue
  public boolean hasEditableComments()
  {
    if(isOverdue)
    {
      return false;
    }
    else
    {
      return super.hasEditableComments();
    }
  }

 //turn on disableParentButtons if overdue
  public boolean disableParentButtons()
  {
    if(isOverdue)
    {
      return true;
    }
    else
    {
      return super.disableParentButtons();
    }
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    //Create a radio selection for account type to change
    //defaults to "Both Accounts"
    String[][] buttons = new String[][]
    {
      {"Closed in Error","Closed in Error"},
      {"Merchant Changed Mind","Merchant Changed Mind"}
    };

    RadioButtonField reopenReason
    = new RadioButtonField( "reopenReason",
                            "Reason for Re-Open",
                            buttons,
                            0,
                            true,
                            "Please select an reason for account to be re-opened.");
    fields.add(reopenReason);
  }

  public String renderHtml(String fName)
  {
    //override the display of the closure Date...
    if(fName.equals("closeDate"))
    {
      return getClosureDate();
    }

    return super.renderHtml(fName);

  }

  public String getClosureDate()
  {
    return closureDate;
  }

  public boolean isOverdue()
  {
    return isOverdue;
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}

//ASG Mgmt queue

      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}

    };
  }

  public String getHeadingName()
  {
    return "Re-Open Merchant Account";
  }


} // class ACRDataBean_AcntReopen_CBT
